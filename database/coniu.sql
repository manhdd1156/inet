-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 24, 2021 at 12:14 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.0.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sll`
--

-- --------------------------------------------------------

--
-- Table structure for table `ads_campaigns`
--

CREATE TABLE `ads_campaigns` (
  `campaign_id` int(10) NOT NULL,
  `campaign_user_id` int(10) UNSIGNED NOT NULL,
  `campaign_title` varchar(255) NOT NULL,
  `campaign_start_date` datetime NOT NULL,
  `campaign_end_date` datetime NOT NULL,
  `campaign_budget` double NOT NULL,
  `campaign_spend` double NOT NULL DEFAULT '0',
  `campaign_bidding` enum('click','view') NOT NULL,
  `audience_countries` text NOT NULL,
  `audience_gender` varchar(32) NOT NULL,
  `audience_relationship` varchar(64) NOT NULL,
  `ads_title` varchar(255) DEFAULT NULL,
  `ads_description` text,
  `ads_type` varchar(32) NOT NULL,
  `ads_url` varchar(255) DEFAULT NULL,
  `ads_page` int(10) UNSIGNED DEFAULT NULL,
  `ads_group` int(10) UNSIGNED DEFAULT NULL,
  `ads_event` int(10) UNSIGNED DEFAULT NULL,
  `ads_placement` enum('newsfeed','sidebar') NOT NULL,
  `ads_image` varchar(255) NOT NULL,
  `campaign_created_date` datetime NOT NULL,
  `campaign_is_active` enum('0','1') NOT NULL DEFAULT '1',
  `campaign_views` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `campaign_clicks` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `ads_system`
--

CREATE TABLE `ads_system` (
  `ads_id` int(10) NOT NULL,
  `title` varchar(255) NOT NULL,
  `place` varchar(32) NOT NULL,
  `code` text NOT NULL,
  `time` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ads_users_wallet_transactions`
--

CREATE TABLE `ads_users_wallet_transactions` (
  `transaction_id` int(10) NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `node_type` varchar(32) NOT NULL,
  `node_id` int(10) UNSIGNED DEFAULT NULL,
  `amount` varchar(32) NOT NULL,
  `type` enum('in','out') NOT NULL,
  `date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `affiliates_payments`
--

CREATE TABLE `affiliates_payments` (
  `payment_id` int(10) NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `email` varchar(64) NOT NULL,
  `amount` varchar(32) NOT NULL,
  `method` varchar(64) NOT NULL,
  `time` datetime NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `announcements`
--

CREATE TABLE `announcements` (
  `announcement_id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `type` varchar(32) NOT NULL,
  `code` text NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `announcements_users`
--

CREATE TABLE `announcements_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `announcement_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `banned_ips`
--

CREATE TABLE `banned_ips` (
  `ip_id` int(10) UNSIGNED NOT NULL,
  `ip` varchar(64) NOT NULL,
  `time` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ci_attendance`
--

CREATE TABLE `ci_attendance` (
  `attendance_id` int(10) NOT NULL,
  `class_id` int(10) UNSIGNED NOT NULL COMMENT 'Trạng thái ngày hôm đó bé có đi học không.\nstatus = 1: bé đi học\nstatus = 0: bé nghỉ học',
  `attendance_date` date NOT NULL COMMENT 'Ngày điểm danh. Thông thường là ngày hiện tại.',
  `checked_user_id` int(10) NOT NULL DEFAULT '0' COMMENT 'ID người điểm danh, thông thường là cô giáo phụ trách lớp.',
  `recorded_at` datetime NOT NULL COMMENT 'Thời gian điểm danh, nó là current time khi lưu vào trong hệ thống.',
  `absence_count` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Số lượng học sinh vắng trong ngày.',
  `present_count` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `is_checked` tinyint(1) UNSIGNED NOT NULL DEFAULT '1' COMMENT 'Giáo viên đã điểm danh chưa? Vì khi phụ huynh xin nghỉ cho con thì hệ thống tự tạo ra bản ghi ở bảng này, lúc đó giá trị này = 0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lưu điểm danh của lớp học chính khóa. Bé nào đi, bé nào nghỉ - lý do nghỉ';

-- --------------------------------------------------------

--
-- Table structure for table `ci_attendance_back`
--

CREATE TABLE `ci_attendance_back` (
  `attendance_back_id` int(10) UNSIGNED NOT NULL,
  `class_id` int(11) UNSIGNED NOT NULL,
  `attendance_back_date` date NOT NULL,
  `came_back_count` int(2) DEFAULT NULL,
  `not_came_back_count` int(2) DEFAULT NULL,
  `recorded_at` datetime NOT NULL,
  `created_user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ci_attendance_back_detail`
--

CREATE TABLE `ci_attendance_back_detail` (
  `attendance_back_detail_id` int(11) UNSIGNED NOT NULL,
  `attendance_back_id` int(11) UNSIGNED NOT NULL,
  `child_id` int(11) NOT NULL,
  `came_back_time` time DEFAULT NULL,
  `came_back_note` varchar(300) DEFAULT NULL COMMENT 'Ghi chú',
  `status` tinyint(1) DEFAULT '0',
  `recorded_at` datetime DEFAULT NULL,
  `recorded_user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ci_attendance_detail`
--

CREATE TABLE `ci_attendance_detail` (
  `attendance_detail_id` int(10) UNSIGNED NOT NULL,
  `attendance_id` int(10) UNSIGNED NOT NULL COMMENT 'ID lớp học',
  `child_id` int(10) UNSIGNED NOT NULL COMMENT 'ID trẻ',
  `start_date` date DEFAULT NULL COMMENT 'Ngày bắt đầu xin nghỉ',
  `end_date` date DEFAULT NULL COMMENT 'Ngày kết thúc xin nghỉ',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Trạng thái điểm danh=0: vắng mặt có lý do =1: có mặt=2: nghỉ không xin phép hoặc xin phép muộn(Ghi đi, tính tiền ăn),3: ghi đi nhưng không tính tiền ăn, 4 : Nghỉ không lý do',
  `feedback` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Trangj th',
  `reason` varchar(512) DEFAULT NULL COMMENT 'Lý do nếu là nghỉ học.',
  `resign_time` datetime DEFAULT NULL COMMENT 'Thời điểm xin nghỉ học',
  `recorded_user_id` int(10) UNSIGNED NOT NULL,
  `is_parent` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lưu điểm danh chi tiết lớp học chính khóa. Bé nào đi, bé nào nghỉ - lý do nghỉ';

-- --------------------------------------------------------

--
-- Table structure for table `ci_background_email`
--

CREATE TABLE `ci_background_email` (
  `email_id` int(10) UNSIGNED NOT NULL,
  `action` varchar(50) NOT NULL COMMENT 'Email được gửi từ hành động tương ứng',
  `receivers` text NOT NULL COMMENT 'Danh sách tài khoản email nhận mail, cách nhau dấu ","',
  `subject` varchar(250) NOT NULL COMMENT 'Tiêu đề email',
  `content` text NOT NULL COMMENT 'Nội dung email cần gửi',
  `file_attachment` varchar(255) DEFAULT NULL COMMENT 'source file',
  `file_attachment_name` varchar(255) DEFAULT NULL COMMENT 'Tên file',
  `is_sent` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Đã gửi rồi hay chưa?\n0: chưa gửi\n1: gửi rồi',
  `retry_count` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Số lần thử gửi lại với mail bị lỗi',
  `delete_after_sending` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Có xóa luôn email sau khi gửi thành công không?\n1: xóa luôn\n0: không xóa',
  `sent_at` datetime DEFAULT NULL COMMENT 'Thời gian email được gửi (thông thường những email được gửi sẽ được xóa luôn), có những email cần giữ lại.',
  `school_id` int(10) UNSIGNED DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Bảng lưu các email cần được gửi của hệ thố';

-- --------------------------------------------------------

--
-- Table structure for table `ci_background_firebase`
--

CREATE TABLE `ci_background_firebase` (
  `firebase_id` int(10) UNSIGNED NOT NULL COMMENT 'Id auto increment ',
  `user_id` int(10) UNSIGNED NOT NULL COMMENT 'ID của user thực hiện thao tác',
  `action` varchar(50) NOT NULL COMMENT 'action để thực hiện chạy background',
  `user_one_id` int(10) DEFAULT '0' COMMENT 'ID user1',
  `user_two_id` int(10) DEFAULT '0' COMMENT 'ID user2',
  `user_name` varchar(64) DEFAULT NULL,
  `user_email` varchar(100) DEFAULT NULL,
  `user_fullname` varchar(255) DEFAULT NULL,
  `user_picture` varchar(255) DEFAULT NULL,
  `is_online` tinyint(1) DEFAULT '0' COMMENT 'Trạng thái online của user',
  `count_chat` int(10) DEFAULT '0' COMMENT 'Số lượng tin nhắn',
  `last_login` datetime DEFAULT NULL COMMENT 'Thời gian đăng nhập lần cuối ',
  `provider` varchar(50) DEFAULT NULL COMMENT 'Đăng ký, đăng nhập thông qua - Sll, facebook, google',
  `is_completed` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Trạng thái đã hoàn thành hay chưa.',
  `retry_count` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Số lần thử lại',
  `delete_after_sending` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Xóa sau khi hoàn thành?',
  `completed_at` datetime DEFAULT NULL COMMENT 'Hoàn thành lúc'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Lưu các tác vụ chạy ngầm với Firebase';

-- --------------------------------------------------------

--
-- Table structure for table `ci_background_notifications`
--

CREATE TABLE `ci_background_notifications` (
  `send_notification_id` int(10) UNSIGNED NOT NULL,
  `type` enum('notification','chat') NOT NULL DEFAULT 'notification',
  `count_notifications` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `message` varchar(255) DEFAULT NULL,
  `notification_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `to_user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `from_user_id` int(10) UNSIGNED NOT NULL,
  `action` varchar(50) NOT NULL,
  `node_type` varchar(50) NOT NULL,
  `node_url` varchar(255) NOT NULL,
  `time` datetime NOT NULL,
  `extra1` varchar(255) DEFAULT NULL COMMENT 'Trường dự phòng',
  `extra2` varchar(255) DEFAULT NULL COMMENT 'Trường dự phòng',
  `extra3` varchar(255) DEFAULT NULL COMMENT 'Trường dự phòng',
  `user_name` varchar(64) NOT NULL,
  `user_fullname` varchar(255) NOT NULL,
  `user_picture` varchar(255) DEFAULT NULL,
  `device_token` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Bảng lưu các notification cần được gửi của hệ thống';

-- --------------------------------------------------------

--
-- Table structure for table `ci_child`
--

CREATE TABLE `ci_child` (
  `child_id` int(10) UNSIGNED NOT NULL,
  `child_parent_id` int(10) UNSIGNED NOT NULL COMMENT 'ID trong bảng quản lý trẻ của phụ huynh',
  `child_code` varchar(30) DEFAULT NULL,
  `first_name` varchar(50) NOT NULL COMMENT 'Tên của trẻ (chỉ tên)',
  `last_name` varchar(50) NOT NULL COMMENT 'Họ và tên đệm',
  `child_name` varchar(100) NOT NULL,
  `child_nickname` varchar(45) DEFAULT NULL COMMENT 'Tên thường gọi của trẻ',
  `birthday` date DEFAULT NULL,
  `gender` enum('male','female') DEFAULT NULL COMMENT 'Giới tính của trẻ\nmale: nam\nfemale: nữ',
  `child_picture` varchar(255) DEFAULT NULL COMMENT 'Lưu avatar của trẻ',
  `parent_name` varchar(100) DEFAULT NULL COMMENT 'Tên phụ huynh',
  `parent_phone` varchar(50) DEFAULT NULL COMMENT 'Số điện thoại bố mẹ, dùng liên hệ khi cần thiết. Có thể lấy từ thông tin cha mẹ khi được quản lý.',
  `parent_job` varchar(500) DEFAULT NULL COMMENT 'lưu nghề nghiệp của mẹ (mặc định là mẹ)',
  `parent_name_dad` varchar(100) DEFAULT NULL COMMENT 'Lưu tên của bố',
  `parent_phone_dad` varchar(50) DEFAULT NULL COMMENT 'Lưu số điện thoại của bố',
  `parent_job_dad` varchar(500) DEFAULT NULL COMMENT 'Lưu nghề nghiệp của bố',
  `parent_email` varchar(100) DEFAULT NULL,
  `parent_img1` varchar(300) DEFAULT NULL COMMENT 'Ảnh cha mẹ, những người có thể đón trẻ về (có thể cỏ cả ảnh bà giúp việc). \n- Trường này chỉ có thể sử dụng khi trẻ bắt đầu đi học. Bình thường ẩn đi.',
  `parent_img2` varchar(300) DEFAULT NULL,
  `parent_img3` varchar(300) DEFAULT NULL,
  `parent_img4` varchar(300) DEFAULT NULL,
  `address` varchar(250) DEFAULT NULL COMMENT 'Địa chỉ nhà của bé. Có thể lấy từ thông tin địa chỉ của cha/mẹ',
  `blood_type` varchar(300) DEFAULT NULL COMMENT 'Nhóm máu của trẻ',
  `hobby` varchar(500) DEFAULT NULL COMMENT 'Sở thich của trẻ',
  `allergy` varchar(500) DEFAULT NULL COMMENT 'Dị ứng',
  `description` varchar(300) DEFAULT NULL COMMENT 'Mô tả về trẻ',
  `created_user_id` int(10) UNSIGNED NOT NULL,
  `name_for_sort` varchar(50) DEFAULT NULL COMMENT 'Tên của trẻ được chuyển thành dạng ký tự tiếng Anh, dùng để sắp xếp khi lấy ra'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Quản lý danh sách trẻ';

-- --------------------------------------------------------

--
-- Table structure for table `ci_child_based_on_month`
--

CREATE TABLE `ci_child_based_on_month` (
  `child_based_on_month_id` int(10) UNSIGNED NOT NULL,
  `month` int(2) UNSIGNED NOT NULL COMMENT 'Tuổi của trẻ (tháng)',
  `link` varchar(300) DEFAULT NULL COMMENT 'Link bài viết',
  `title` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lưu thông tin trẻ theo tháng tuổi';

-- --------------------------------------------------------

--
-- Table structure for table `ci_child_development`
--

CREATE TABLE `ci_child_development` (
  `child_development_id` int(11) NOT NULL,
  `month_push` int(4) NOT NULL COMMENT 'Tháng push thông báo',
  `title` varchar(300) NOT NULL,
  `content` text COMMENT 'Nội dung',
  `link` varchar(255) DEFAULT NULL COMMENT 'Đường dẫn thông báo',
  `image` varchar(255) DEFAULT NULL COMMENT 'Ảnh bài viết',
  `day_push` tinyint(2) NOT NULL COMMENT 'Ngày thứ bao nhiêu trong tháng',
  `time_push` time NOT NULL COMMENT 'Giờ nòa trong ngày',
  `type` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Kiểu tiêm phòng hay là thông tin, 1: tiêm phòng, 2: thông tin',
  `content_type` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Là link hay bài viết text, 1: bài viết, 2: link',
  `is_development` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Có xuất hiện trong quá trình phát triển hay không',
  `is_notice_before` tinyint(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Có thông báo trước hay không?',
  `notice_before_days` int(3) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Thông báo trước bn ngày',
  `is_reminder_before` tinyint(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Có nhắc lại hay không?',
  `reminder_before_days` int(3) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Nhắc lại trước bn ngày',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_user_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lưu thông tin quá trình phát triển của trẻ';

-- --------------------------------------------------------

--
-- Table structure for table `ci_child_development_child`
--

CREATE TABLE `ci_child_development_child` (
  `child_development_child_id` int(10) NOT NULL,
  `child_development_id` int(10) NOT NULL COMMENT 'id của thông tin quá trình tiêm phòng của trẻ',
  `child_parent_id` int(10) NOT NULL COMMENT 'child_id của trẻ đã được tiêm phòng'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lưu những trẻ đã được tiêm phòng';

-- --------------------------------------------------------

--
-- Table structure for table `ci_child_edit_history`
--

CREATE TABLE `ci_child_edit_history` (
  `ci_child_edit_history_id` int(10) NOT NULL,
  `school_id` int(10) NOT NULL,
  `child_id` int(10) NOT NULL,
  `edit_by` tinyint(1) NOT NULL DEFAULT '2' COMMENT 'Chỉnh sửa bởi ai:\n1- Quản lý trường\n2- Giáo viên\n3- Phụ huynh',
  `last_update` date NOT NULL COMMENT 'Ngày cập nhật cuối cùng',
  `updated_user_id` int(10) NOT NULL COMMENT 'ID của user sửa cuối cùng'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lưu thông tin những trẻ đã được sửa trong 1 tháng gần nhất (tạm thời chỉ lưu những trẻ được giáo viên chỉnh sửa)';

-- --------------------------------------------------------

--
-- Table structure for table `ci_child_foetus_growth`
--

CREATE TABLE `ci_child_foetus_growth` (
  `child_foetus_growth_id` int(10) UNSIGNED NOT NULL,
  `child_parent_id` int(10) UNSIGNED NOT NULL COMMENT 'id của trẻ',
  `recorded_at` date NOT NULL COMMENT 'Ngày ghi thông tin',
  `pregnant_week` tinyint(2) NOT NULL COMMENT 'Tuần mang thai',
  `due_date_of_childbearing` date DEFAULT NULL COMMENT 'Ngày dự sinh của trẻ',
  `femur_length` double DEFAULT NULL COMMENT 'Chiều dài xương đùi',
  `from_head_to_hips_length` double DEFAULT NULL COMMENT 'Chiều dài từ đầu đến mông',
  `weight` double DEFAULT NULL,
  `fetal_heart` double DEFAULT NULL COMMENT 'Nhịp tim thai',
  `hospital` varchar(300) DEFAULT NULL COMMENT 'Bệnh viện, nơi khám thai\n',
  `hospital_address` varchar(300) DEFAULT NULL COMMENT 'địa chỉ bệnh viện',
  `doctor_name` varchar(300) DEFAULT NULL COMMENT 'Tên bác sĩ',
  `doctor_phone` varchar(300) DEFAULT NULL COMMENT 'Số điện thoại bác sĩ',
  `description` varchar(300) DEFAULT NULL,
  `created_user_id` int(10) NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Bảng lưu thông tin sức khỏe thai nhi';

-- --------------------------------------------------------

--
-- Table structure for table `ci_child_growth`
--

CREATE TABLE `ci_child_growth` (
  `child_growth_id` int(10) UNSIGNED NOT NULL,
  `recorded_at` date NOT NULL COMMENT 'Thời điểm ghi chép chiều cao, cân nặng',
  `child_parent_id` int(10) NOT NULL,
  `weight` double UNSIGNED NOT NULL COMMENT 'Lưu cân nặng của trẻ. Đơn vị tính là Kg',
  `height` double UNSIGNED DEFAULT NULL COMMENT 'Lưu chiều cao của trẻ. Đơn vị tính là cm.',
  `source_file` tinytext COMMENT 'Nguồn file ảnh',
  `file_name` tinytext COMMENT 'Tên file ảnh',
  `nutriture_status` varchar(300) DEFAULT NULL COMMENT 'Tình trạng dinh dưỡng',
  `heart` varchar(300) DEFAULT NULL COMMENT 'nhịp tim',
  `blood_pressure` varchar(300) DEFAULT NULL COMMENT 'Huyết áp',
  `ear` varchar(300) DEFAULT NULL COMMENT 'tai',
  `eye` varchar(300) CHARACTER SET utf16 DEFAULT NULL COMMENT 'mắt',
  `nose` varchar(300) DEFAULT NULL COMMENT 'Mũi',
  `description` varchar(300) DEFAULT NULL COMMENT 'Mô tả việc ghi chép',
  `created_user_id` int(10) UNSIGNED DEFAULT NULL,
  `is_parent` tinyint(1) UNSIGNED NOT NULL DEFAULT '1' COMMENT 'Có phải phụ huynh tạo hay không: 1: phải, 0: trường tạo'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Quản lý chiều cao, cân nặng của trẻ. Để theo dõi biểu đồ tăng trưởng của trẻ.';

-- --------------------------------------------------------

--
-- Table structure for table `ci_child_info`
--

CREATE TABLE `ci_child_info` (
  `child_info_id` int(10) NOT NULL,
  `child_id` int(10) NOT NULL,
  `picker_name` varchar(100) NOT NULL,
  `picker_relation` varchar(100) NOT NULL,
  `picker_phone` varchar(50) NOT NULL,
  `picker_source_file` tinytext NOT NULL,
  `picker_file_name` tinytext NOT NULL,
  `picker_address` varchar(300) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_user_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ci_child_journal`
--

CREATE TABLE `ci_child_journal` (
  `child_journal_id` int(10) UNSIGNED NOT NULL,
  `child_journal_album_id` int(10) UNSIGNED NOT NULL,
  `source_file` tinytext,
  `file_name` tinytext
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lưu ảnh trong góc nhật ký của trẻ';

-- --------------------------------------------------------

--
-- Table structure for table `ci_child_journal_album`
--

CREATE TABLE `ci_child_journal_album` (
  `child_journal_album_id` int(10) UNSIGNED NOT NULL,
  `child_parent_id` int(10) UNSIGNED NOT NULL,
  `caption` varchar(500) DEFAULT NULL COMMENT 'Tiêu đề',
  `created_at` datetime NOT NULL COMMENT 'Ngày tạo',
  `updated_at` datetime DEFAULT NULL COMMENT 'Ngày update',
  `created_user_id` int(10) UNSIGNED NOT NULL,
  `is_parent` tinyint(1) UNSIGNED NOT NULL DEFAULT '1' COMMENT 'Có phải phụ huynh tạo hay không'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='bảng lưu album nhật ký của trẻ';

-- --------------------------------------------------------

--
-- Table structure for table `ci_child_medical`
--

CREATE TABLE `ci_child_medical` (
  `child_medical_id` int(10) UNSIGNED NOT NULL,
  `child_parent_id` int(10) UNSIGNED NOT NULL COMMENT 'Id của trẻ',
  `recorded_at` date NOT NULL COMMENT 'NGày ghi lại thông tin bệnh',
  `diseased_name` varchar(300) NOT NULL COMMENT 'Tên bệnh',
  `day_use` int(2) DEFAULT NULL COMMENT 'Số ngày sử dụng thuốc',
  `symptom` varchar(300) DEFAULT NULL COMMENT 'Triệu chứng bệnh',
  `medicine_list` varchar(300) NOT NULL COMMENT 'Danh sách thuốc',
  `usage_guide` varchar(300) DEFAULT NULL COMMENT 'Hướng dẫ sử dụng thuốc',
  `hospital` varchar(100) DEFAULT NULL COMMENT 'Bệnh viện',
  `hospital_address` varchar(200) DEFAULT NULL COMMENT 'Địa chỉ bệnh viện',
  `doctor_name` varchar(100) DEFAULT NULL COMMENT 'Tên bác sĩ',
  `doctor_phone` varchar(20) DEFAULT NULL COMMENT 'Số điện thoại liên hệ bác sĩ',
  `description` varchar(300) DEFAULT NULL,
  `created_at` date NOT NULL,
  `updated_at` date DEFAULT NULL,
  `created_user_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Bảng lưu thông tin y bạ của trẻ';

-- --------------------------------------------------------

--
-- Table structure for table `ci_child_of_parent_birthday`
--

CREATE TABLE `ci_child_of_parent_birthday` (
  `parent_id` int(10) UNSIGNED NOT NULL,
  `child_id` int(10) UNSIGNED NOT NULL,
  `child_name` varchar(20) NOT NULL,
  `child_class_id` int(10) UNSIGNED NOT NULL,
  `child_class_name` varchar(255) NOT NULL,
  `child_class_title` varchar(255) NOT NULL,
  `child_school_id` int(10) UNSIGNED NOT NULL,
  `child_school_name` varchar(255) NOT NULL,
  `child_school_title` varchar(255) NOT NULL,
  `child_gender` enum('male','female') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lưu danh sách những trẻ được quản lý bởi 1 phụ huynh';

-- --------------------------------------------------------

--
-- Table structure for table `ci_child_parent`
--

CREATE TABLE `ci_child_parent` (
  `child_parent_id` int(10) UNSIGNED NOT NULL,
  `child_code` varchar(30) DEFAULT NULL,
  `first_name` varchar(50) NOT NULL COMMENT 'Tên của trẻ (chỉ tên)',
  `last_name` varchar(50) NOT NULL COMMENT 'Họ và tên đệm',
  `child_name` varchar(100) NOT NULL,
  `child_nickname` varchar(45) DEFAULT NULL COMMENT 'Tên thường gọi của trẻ',
  `child_admin` int(10) UNSIGNED NOT NULL COMMENT 'admin của trẻ',
  `is_pregnant` tinyint(1) NOT NULL DEFAULT '0',
  `foetus_begin_date` datetime DEFAULT NULL COMMENT 'Ngày bắt đầu mang thai, trường này dựa vào số tuần mang thai phụ huynh nhập vào hệ thống để tính ra, hiển thị lên màn hinh số tuần',
  `due_date_of_childbearing` date DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `gender` enum('male','female') DEFAULT NULL COMMENT 'Giới tính của trẻ\nmale: nam\nfemale: nữ',
  `child_picture` varchar(255) DEFAULT NULL COMMENT 'avatar của trẻ',
  `parent_name` varchar(100) DEFAULT NULL COMMENT 'Tên phụ huynh',
  `parent_phone` varchar(50) DEFAULT NULL COMMENT 'Số điện thoại bố mẹ, dùng liên hệ khi cần thiết. Có thể lấy từ thông tin cha mẹ khi được quản lý.',
  `parent_job` varchar(500) DEFAULT NULL COMMENT 'lưu nghề nghiệp của mẹ (mặc định là mẹ)',
  `parent_name_dad` varchar(100) DEFAULT NULL COMMENT 'Lưu tên của bố',
  `parent_phone_dad` varchar(50) DEFAULT NULL COMMENT 'Lưu số điện thoại của bố',
  `parent_job_dad` varchar(500) DEFAULT NULL COMMENT 'Lưu nghề nghiệp của bố',
  `parent_email` varchar(100) DEFAULT NULL,
  `parent_img1` varchar(300) DEFAULT NULL COMMENT 'Ảnh cha mẹ, những người có thể đón trẻ về (có thể cỏ cả ảnh bà giúp việc). \n- Trường này chỉ có thể sử dụng khi trẻ bắt đầu đi học. Bình thường ẩn đi.',
  `parent_img2` varchar(300) DEFAULT NULL,
  `parent_img3` varchar(300) DEFAULT NULL,
  `parent_img4` varchar(300) DEFAULT NULL,
  `address` varchar(250) DEFAULT NULL COMMENT 'Địa chỉ nhà của bé. Có thể lấy từ thông tin địa chỉ của cha/mẹ',
  `blood_type` varchar(300) DEFAULT NULL COMMENT 'Nhóm máu của trẻ',
  `hobby` varchar(500) DEFAULT NULL COMMENT 'Sở thich của trẻ',
  `allergy` varchar(500) DEFAULT NULL COMMENT 'Dị ứng',
  `description` varchar(300) DEFAULT NULL COMMENT 'Mô tả về trẻ',
  `created_user_id` int(10) UNSIGNED NOT NULL,
  `name_for_sort` varchar(50) DEFAULT NULL COMMENT 'Tên của trẻ được chuyển thành dạng ký tự tiếng Anh, dùng để sắp xếp khi lấy ra'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lưu thông tin trẻ, phụ huynh quản lý';

-- --------------------------------------------------------

--
-- Table structure for table `ci_class_birthday`
--

CREATE TABLE `ci_class_birthday` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `class_id` int(10) UNSIGNED NOT NULL,
  `class_name` varchar(255) NOT NULL,
  `class_title` varchar(255) NOT NULL,
  `school_id` int(10) UNSIGNED NOT NULL,
  `school_name` varchar(255) NOT NULL,
  `school_title` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lưu danh sách lớp của trẻ và giáo viên (birthday)';

-- --------------------------------------------------------

--
-- Table structure for table `ci_class_child`
--

CREATE TABLE `ci_class_child` (
  `class_id` int(10) UNSIGNED NOT NULL COMMENT 'ID lớp',
  `child_id` int(10) UNSIGNED NOT NULL COMMENT 'ID trẻ',
  `begin_at` date DEFAULT NULL COMMENT 'Ngày bé bắt đầu học ở lớp này',
  `end_at` date DEFAULT NULL COMMENT 'Ngày học cuối cùng ở lớp',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Trạng thái học sinh trong lớp. Nếu trạng thái là ACTIVE thì mới thực sự là học sinh của lớp.\n- status = 0: Inactive\n- status = 1: Active\n- status = 2: WaitApproval\nKhi cha mẹ tạo ra t',
  `description` varchar(300) DEFAULT NULL COMMENT 'Mô tả thông tin trẻ trong lớp'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Quản lý danh sách học sinh của một lớp.';

-- --------------------------------------------------------

--
-- Table structure for table `ci_class_level`
--

CREATE TABLE `ci_class_level` (
  `class_level_id` int(10) UNSIGNED NOT NULL,
  `class_level_name` varchar(45) NOT NULL,
  `school_id` int(10) UNSIGNED NOT NULL,
  `description` varchar(300) DEFAULT NULL COMMENT 'Mô tả khối lớp',
  `gov_class_level` tinyint(2) NOT NULL DEFAULT '0' COMMENT 'Lưu thông tin khối của class_level'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Bảng lưu thông tin các khối lớp trong ';

-- --------------------------------------------------------

--
-- Table structure for table `ci_class_level_subject`
--

CREATE TABLE `ci_class_level_subject` (
  `class_level_subject_id` int(11) NOT NULL,
  `school_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL COMMENT 'ID môn học. Lấy từ ID chung của hệ thống (khai báo trong mãng).',
  `gov_class_level` int(11) NOT NULL COMMENT 'Khối lớp (khối lớp 1)',
  `school_year` varchar(9) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lưu danh sách môn học của một khối (khối lớp của một trường. Thường thì theo chương trình của Bộ GD, nhưng trường có thể chọn một vài môn riêng nên phải lưu theo trường.';

-- --------------------------------------------------------

--
-- Table structure for table `ci_class_school_year`
--

CREATE TABLE `ci_class_school_year` (
  `class_school_year_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `class_name` varchar(45) NOT NULL COMMENT 'Tên lớp của năm hiện tại. Vì sao khi lên lớp thì tên sẽ đổi nên giá trị này lưu tên cũ.',
  `school_year` varchar(9) NOT NULL,
  `teacher_id` int(11) DEFAULT NULL COMMENT 'ID giáo viên chủ nhiệm'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lưu thông tin của lớp trong một năm học (tên lớp, năm học, giáo viên chủ nhiệm)';

-- --------------------------------------------------------

--
-- Table structure for table `ci_class_subject_teacher`
--

CREATE TABLE `ci_class_subject_teacher` (
  `class_subject_teacher_id` int(11) NOT NULL,
  `school_year` varchar(9) NOT NULL,
  `class_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `teacher_id` int(11) DEFAULT NULL,
  `semester` int(11) DEFAULT NULL COMMENT 'Lưu thông tin học kỳ (1: học kỳ 1, 2: học kỳ 2)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lưu thông tin giáo viên nào dạy môn gì của một lớp';

-- --------------------------------------------------------

--
-- Table structure for table `ci_comment_c1`
--

CREATE TABLE `ci_comment_c1` (
  `comment_id` int(10) NOT NULL,
  `class_id` int(10) NOT NULL,
  `school_year` varchar(9) NOT NULL,
  `child_id` int(10) NOT NULL,
  `cm1gk` text,
  `cm1ck` text,
  `cm2gk` text,
  `cm2ck` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ci_comment_c2`
--

CREATE TABLE `ci_comment_c2` (
  `comment_id` int(10) NOT NULL,
  `class_id` int(10) NOT NULL,
  `school_year` varchar(9) NOT NULL,
  `child_id` int(10) NOT NULL,
  `cm1` text,
  `cm2` text,
  `cmcn` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ci_conduct`
--

CREATE TABLE `ci_conduct` (
  `conduct_id` int(11) NOT NULL,
  `school_year` varchar(9) NOT NULL COMMENT 'Lưu năm học. Lưu đúng 9 kỳ tự. VD: 2019-2020, 2020-2021',
  `class_id` int(11) NOT NULL,
  `child_id` int(10) NOT NULL COMMENT 'id trẻ ',
  `hk1` varchar(20) DEFAULT NULL COMMENT 'đánh giá hạnh kiểm kì 1',
  `hk2` varchar(20) DEFAULT NULL COMMENT 'đánh giá hạnh kiểm kì 2',
  `ck` varchar(20) DEFAULT NULL COMMENT 'đánh giá hạnh kiểm cả năm'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ci_diary`
--

CREATE TABLE `ci_diary` (
  `diary_id` int(10) UNSIGNED NOT NULL,
  `class_id` int(10) UNSIGNED NOT NULL,
  `school_id` int(10) UNSIGNED NOT NULL,
  `diary_date` datetime DEFAULT NULL COMMENT 'Ngày của nhật ký'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lưu nhật ký 1 ngày của bé.';

-- --------------------------------------------------------

--
-- Table structure for table `ci_diary_item`
--

CREATE TABLE `ci_diary_item` (
  `diary_id` int(10) UNSIGNED NOT NULL,
  `time_of_day` time(6) NOT NULL,
  `activity` varchar(150) NOT NULL,
  `images` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lưu các khuôn mẫu ngày hoạt động của bé.';

-- --------------------------------------------------------

--
-- Table structure for table `ci_diary_template`
--

CREATE TABLE `ci_diary_template` (
  `diary_template_id` int(10) UNSIGNED NOT NULL,
  `template_name` varchar(150) NOT NULL,
  `school_id` int(10) UNSIGNED NOT NULL,
  `class_level_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lưu các khuôn mẫu ngày hoạt động của bé.';

-- --------------------------------------------------------

--
-- Table structure for table `ci_diary_template_item`
--

CREATE TABLE `ci_diary_template_item` (
  `diary_template_id` int(11) NOT NULL,
  `time_of_day` time(6) NOT NULL,
  `activity` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ci_event`
--

CREATE TABLE `ci_event` (
  `event_id` int(10) UNSIGNED NOT NULL,
  `event_name` varchar(500) NOT NULL COMMENT 'Tên sự kiện',
  `location` varchar(250) NOT NULL COMMENT 'Địa điểm nơi xảy ra sự kiện. Mặc định lấy địa chỉ trường (màn hình tạo sự kiện nhập luôn địa chỉ này vào, user có thể sửa nếu cần).',
  `price` double NOT NULL DEFAULT '0' COMMENT 'Lệ phí tham gia nếu đăng ký.',
  `begin` datetime DEFAULT NULL COMMENT 'Thời gian (giờ ngày) bắt đầu sự kiện',
  `end` datetime DEFAULT NULL COMMENT 'Thời gian kết thúc sự kiện',
  `level` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Level áp dụng của sự kiện.\n- level = 1: Áp dụng cả trường\n- level = 2: Áp dụng cho khối\n- level = 3: Áp dụng mức lớp',
  `school_id` int(10) UNSIGNED NOT NULL COMMENT 'ID của trường có thông báo\n- Đối với trường hợp thông báo của lớp, khối: thì school_id lưu ID của trường cho dễ query.\n- Đối với trường hợp thông báo của trường, giá trị trường này = object_id.',
  `class_level_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'ID khối nếu level là khối',
  `class_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'ID của lớp nếu level = lớp',
  `must_register` tinyint(1) DEFAULT '0' COMMENT 'Người tham gia có phải đăng ký không. Hay chỉ thông báo cho để mọi người tham gia tự do.\n- false: không phải đăng ký\n- true: phải đăng ký mới được tham gia',
  `registration_deadline` datetime DEFAULT NULL COMMENT 'Hạn đăng ký',
  `for_teacher` tinyint(1) DEFAULT '0' COMMENT 'Sự kiện này có dành cho giáo viên tham gia không.\n- True: có. Khi đó giáo viên có thể đăng ký tham gia.\n- False: không. Gv không đăng ký tham gia.\nNếu must_register = 0, trường này không dùng đến.',
  `for_parent` tinyint(1) DEFAULT '0' COMMENT 'Sự kiện này có dành cho cha mẹ tham gia không.\n- True: có. Khi đó cha mẹ có thể đăng ký tham gia.\n- False: không. Cha mẹ không đăng ký tham gia.\nNếu must_register = 0, trường này không dùng đến.',
  `for_child` tinyint(1) DEFAULT '1' COMMENT 'Sự kiện này có dành cho các bé tham gia không?\n- True: có. Cha mẹ đăng ký cho con tham gia.\n- False: không. Cha mẹ không đc đăng ký cho con tham gia.\nNếu must_register = 0, trường này không dùng đến.',
  `parent_participants` int(10) UNSIGNED DEFAULT '0' COMMENT 'Đếm số cha mẹ tham gia sự kiện trong trường hợp có đăng ký',
  `child_participants` int(10) UNSIGNED DEFAULT '0' COMMENT 'Đếm số trẻ tham gia sự kiện trong trường hợp có đăng ký',
  `teacher_participants` int(10) NOT NULL DEFAULT '0' COMMENT 'Đếm số giáo viên tham gia sự kiện trong trường hợp có đăng ký.\\nTrường này chỉ áp dụng với level = TEACHER.',
  `post_on_wall` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Trạng thái có post lên tường của trường/lớp không?\n0: không\n1: có',
  `post_ids` varchar(250) DEFAULT NULL COMMENT 'Danh sách ID của các bài post liên quan. Các ID được phân tách nhau bởi dấu phẩy ","',
  `is_notified` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Đã thông báo cho người liên quan chưa?\n1: rồi\n0: chưa',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Trạng thái của sự kiện\n0: Chưa diễn ra\n1: Đã diễn ra\n2: Hết thời gian đăng ký',
  `description` text COMMENT 'Mô tả chi tiết sự kiện',
  `views_count` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Số lượt xem sự kiện',
  `created_at` datetime NOT NULL COMMENT 'Thời gian bản ghi được tạo ra trong hệ thống',
  `updated_at` datetime DEFAULT NULL COMMENT 'Thời gian cập nhật thông tin bản ghi lần cuối cùng',
  `created_user_id` int(10) UNSIGNED NOT NULL COMMENT 'Thời gian bản ghi được tạo ra trong hệ thống'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lưu thông tin các sự kiện của lớp';

-- --------------------------------------------------------

--
-- Table structure for table `ci_event_participate`
--

CREATE TABLE `ci_event_participate` (
  `event_id` int(10) NOT NULL COMMENT 'ID sự kiện',
  `pp_type` tinyint(1) NOT NULL COMMENT 'Loại đối tượng đăng ký tham gia:\ntype=0: trẻ em.\ntype=1: phụ huynh.\ntype=2: giáo viên',
  `pp_id` int(10) NOT NULL COMMENT 'ID của đối tượng tham gia. Giá trị này tùy thuộc vào trường type ở trên.\ntype=0: trẻ em, thì participate_id = child_id\ntype=1: phụ huynh, thì participate_id = user_id (của parent).\ntype=2: giáo viên, thì participate_id = u',
  `is_paid` tinyint(1) DEFAULT '0' COMMENT 'Người đăng ký đã thanh toán tham gia sự kiện chưa?\n0: chưa thanh toán.\n1: đã thanh toán.',
  `created_at` datetime NOT NULL COMMENT 'Thời gian bản ghi được tạo ra trong hệ thống',
  `created_user_id` int(10) NOT NULL COMMENT 'Thời gian bản ghi được tạo ra trong hệ thống'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lưu danh sách người tham gia của 1 sự ';

-- --------------------------------------------------------

--
-- Table structure for table `ci_fee`
--

CREATE TABLE `ci_fee` (
  `fee_id` int(10) UNSIGNED NOT NULL,
  `fee_name` varchar(100) NOT NULL COMMENT 'Tên loại phí',
  `type` tinyint(1) UNSIGNED NOT NULL DEFAULT '1' COMMENT 'Loại phí: Để áp dụng cách tính.\ntype = 1: phí hàng tháng (ví dụ: học phí là phí hàng tháng ko thay đổi)\ntype = 2: phí hàng ngày (tính theo số ngày trẻ đi học, dựa bảng điểm danh trong tháng). (ví dụ: tiề',
  `unit_price` double UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Số tiền',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Trạng thái của loại phí. \n- true: vẫn còn áp dụng\n- false: không còn áp dụng nữa.',
  `level` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Loại phí này được áp dụng cho đối tượng nào. Và nó có các giá trị là:\n= 1: áp dụng cho cả trường\n= 2: áp dụng cho khối\n= 3: áp dụng cho riêng một lớp\n',
  `school_id` int(10) UNSIGNED NOT NULL COMMENT 'ID của trường',
  `class_level_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'ID của khối áp dụng khoản phí',
  `class_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'ID của lớp áp dụng khoản phí',
  `fee_change_id` varchar(255) DEFAULT NULL COMMENT 'Thay thế cho phí có fee_id bằng fee_change_id',
  `description` varchar(300) DEFAULT NULL COMMENT 'Mô tả chi tiết loại fee',
  `created_at` datetime NOT NULL COMMENT 'Thời gian bản ghi được tạo ra trong hệ thống',
  `updated_at` datetime DEFAULT NULL COMMENT 'Thời gian cập nhật thông tin bản ghi lần cuối cùng',
  `created_user_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lưu các loại phí mặc định của t';

-- --------------------------------------------------------

--
-- Table structure for table `ci_feedback`
--

CREATE TABLE `ci_feedback` (
  `feedback_id` int(10) NOT NULL,
  `school_id` int(10) UNSIGNED NOT NULL,
  `class_id` int(10) UNSIGNED NOT NULL,
  `child_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) DEFAULT NULL,
  `email` varchar(250) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `content` text NOT NULL,
  `level` tinyint(1) UNSIGNED NOT NULL DEFAULT '1' COMMENT 'Góp ý này được gửi đến, 1: trường; 2: lớp',
  `is_incognito` enum('0','1') NOT NULL DEFAULT '0',
  `confirm` enum('0','1') NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Lưu trạng thái feedback khi trường xóa',
  `created_at` datetime NOT NULL COMMENT 'Thời gian bản ghi được tạo ra trong hệ thống',
  `updated_at` datetime DEFAULT NULL COMMENT 'Thời gian cập nhật thông tin bản ghi lần cuối cùng',
  `created_user_id` int(11) NOT NULL COMMENT 'Thời gian bản ghi được tạo ra trong hệ thống'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lưu góp ý của phụ huynh cho nhà trường';

-- --------------------------------------------------------

--
-- Table structure for table `ci_fee_child`
--

CREATE TABLE `ci_fee_child` (
  `fee_id` int(10) UNSIGNED NOT NULL COMMENT 'ID của loại phí',
  `child_id` int(10) UNSIGNED NOT NULL COMMENT 'ID của trẻ áp dụng khoản phí'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lưu các loại phí cho trẻ';

-- --------------------------------------------------------

--
-- Table structure for table `ci_foetus_based_on_week`
--

CREATE TABLE `ci_foetus_based_on_week` (
  `foetus_based_on_week_id` int(10) UNSIGNED NOT NULL,
  `week` int(2) UNSIGNED NOT NULL COMMENT 'Tuổi thai (tuần)',
  `link` varchar(300) DEFAULT NULL COMMENT 'link bài viết',
  `title` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lưu thông tin tuần thai của thai nhi';

-- --------------------------------------------------------

--
-- Table structure for table `ci_foetus_info`
--

CREATE TABLE `ci_foetus_info` (
  `foetus_info_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(100) NOT NULL COMMENT 'Tiêu đề của thông báo',
  `content` text COMMENT 'Nội dung',
  `link` varchar(255) DEFAULT NULL COMMENT 'Đường dẫn thông báo',
  `image` varchar(255) DEFAULT NULL COMMENT 'Ảnh bài viết',
  `day_push` int(10) NOT NULL COMMENT 'Lưu ngày gửi thông báo của thai nhi',
  `time` time DEFAULT NULL COMMENT 'Thời gian gửi thông báo, giờ thứ mấy trong ngày',
  `type` tinyint(1) NOT NULL COMMENT '1: Khám thai, 2:thông tin. Trường này bắt buộc',
  `content_type` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Là link hay bài viết text, 1: bài viết, 2: link',
  `is_development` tinyint(1) NOT NULL COMMENT '0: không hiển thị trong quá trình phát triển, 1: Hiển thị trong quá trình phát triển',
  `is_notice_before` tinyint(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Có thông báo trước hay không?',
  `notice_before_days` int(3) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Thông báo trước bn ngày',
  `is_reminder_before` tinyint(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Có nhắc lại hay không?',
  `reminder_before_days` int(3) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Nhắc lại trước bn ngày',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_user_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lưu nội dung thông báo push về cho phụ huynh đang mang thai';

-- --------------------------------------------------------

--
-- Table structure for table `ci_foetus_knowledge`
--

CREATE TABLE `ci_foetus_knowledge` (
  `foetus_knowledge_id` int(10) NOT NULL,
  `title` varchar(300) NOT NULL,
  `link` tinytext NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_user_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ci_interactive`
--

CREATE TABLE `ci_interactive` (
  `interactivel_id` int(10) UNSIGNED NOT NULL,
  `school_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'id của trường',
  `module` varchar(100) NOT NULL COMMENT 'module cần thống kê',
  `date` date NOT NULL COMMENT 'Ngày thống kê',
  `object_id` int(10) NOT NULL DEFAULT '0' COMMENT 'ID của đối tượng được thêm mới hoặc xem, nếu xem danh sách, chỉnh sửa thì object_id = 0',
  `school_view` int(10) NOT NULL COMMENT 'Số lượt xem bên phía phụ huynh',
  `parent_view` int(10) NOT NULL DEFAULT '0' COMMENT 'Số lượt xem của phụ huynh',
  `is_created` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Có phải là thêm mới hay không, dùng để thống kê số lượng thêm mới của từng module, 0: không phải thêm mới, 1: trường thêm mới, 2: phụ huynh thêm mới'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lưu lượt tương tác, thêm mới của nhà trường, phụ huynh';

-- --------------------------------------------------------

--
-- Table structure for table `ci_interactive_school`
--

CREATE TABLE `ci_interactive_school` (
  `view_type` varchar(32) NOT NULL COMMENT 'Mục tương tác ...)',
  `date` date NOT NULL COMMENT 'Ngày lưu lượt tương tác',
  `school_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'ID của trường',
  `interactive_count_in_date` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Số lượt tương tác trong ngày'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Bảng lưu thông tin tương tác trường';

-- --------------------------------------------------------

--
-- Table structure for table `ci_interactive_topic`
--

CREATE TABLE `ci_interactive_topic` (
  `group_name` varchar(64) NOT NULL COMMENT 'group_name của id, nhóm tương tác ...)',
  `date` date NOT NULL COMMENT 'Ngày lưu lượt tương tác',
  `group_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'ID của trường',
  `interactive_count_in_date` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Số lượt tương tác trong ngày'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Bảng lưu thông tin tương tác với group, topic';

-- --------------------------------------------------------

--
-- Table structure for table `ci_medicine`
--

CREATE TABLE `ci_medicine` (
  `medicine_id` int(10) UNSIGNED NOT NULL,
  `child_id` int(10) UNSIGNED NOT NULL COMMENT 'ID trẻ',
  `medicine_list` varchar(400) NOT NULL COMMENT 'Danh sách thuốc phải uống',
  `guide` text NOT NULL COMMENT 'Hướng dẫn sử dụng đơn thuốc (liều lượng, cách dùng).',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Trạng thái sử dụng thuốc:\n0: mới tạo\n1: xác nhận của giáo viên\n2: hủy',
  `editable` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Có thể edit được đơn thuốc này không. Trường này được cập nhật khi có ít nhất một lần cho uống thuốc.',
  `time_per_day` tinyint(1) UNSIGNED NOT NULL DEFAULT '1' COMMENT 'Số lần cho uống thuốc trong 1 ngày',
  `begin` datetime NOT NULL COMMENT 'Thời gian bắt đầu uống thuốc',
  `end` datetime DEFAULT NULL COMMENT 'Thời gian kết thúc uống thuốc.',
  `source_file` tinytext,
  `file_name` tinytext,
  `created_at` datetime NOT NULL COMMENT 'Thời gian bản ghi được tạo ra trong hệ thống',
  `updated_at` datetime DEFAULT NULL COMMENT 'Thời gian cập nhật thông tin bản ghi lần cuối cùng',
  `created_user_id` int(10) UNSIGNED NOT NULL COMMENT 'ID người tạo bản ghi trong hệ thống (ID phụ huynh gửi thuốc)',
  `confirm_user_id` int(10) DEFAULT NULL COMMENT 'Lưu user_id người xác nhận đơn thuốc',
  `views_count` int(10) NOT NULL DEFAULT '0' COMMENT 'Số lượt xem gửi thuốc'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Quản lý lịch sử dùng thuốc của trẻ.';

-- --------------------------------------------------------

--
-- Table structure for table `ci_medicine_detail`
--

CREATE TABLE `ci_medicine_detail` (
  `medicine_id` int(10) UNSIGNED NOT NULL COMMENT 'ID lần gửi thuốc',
  `usage_date` datetime NOT NULL COMMENT 'Ngày sử dụng thuốc',
  `time_on_day` tinyint(1) UNSIGNED NOT NULL DEFAULT '1' COMMENT 'Lần uống thuốc trong ngày',
  `created_at` datetime NOT NULL,
  `created_user_id` int(10) UNSIGNED NOT NULL COMMENT 'Người xác nhận cho trẻ uống thuốc'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lưu chi tiết cho trẻ uống thuốc';

-- --------------------------------------------------------

--
-- Table structure for table `ci_menu`
--

CREATE TABLE `ci_menu` (
  `menu_id` int(10) NOT NULL,
  `menu_name` varchar(100) DEFAULT NULL COMMENT 'Tên thực đơn',
  `applied_for` tinyint(1) NOT NULL DEFAULT '3' COMMENT 'Thực đơn này áp dụng cho đối tượng nào.\n= 1: áp dụng cho toàn trường\n= 2: áp dụng cho khối\n= 3: áp dụng cho lớp',
  `school_id` int(10) NOT NULL COMMENT 'id trường',
  `class_level_id` int(10) NOT NULL COMMENT 'id khối',
  `class_id` int(10) NOT NULL COMMENT 'id lớp',
  `begin` date NOT NULL COMMENT 'Ngày bắt đầu thực đơn',
  `end` date NOT NULL COMMENT 'Ngày cuối thực đơn',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Trạng thái áp dụng. \n- 1: active\n- 0: inactie',
  `description` varchar(300) NOT NULL COMMENT 'Mô tả thực đơn',
  `is_meal` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Có sử dụng bữa ăn hay không',
  `is_saturday` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Có thực đơn vào thứ 7 hay không',
  `is_notified` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Lưu trạng thái đã thông báo thực đơn hay chưa',
  `views_count` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Số lượt xem thực đơn',
  `created_at` datetime DEFAULT NULL COMMENT 'Thời gian bản ghi được tạo ra trong hệ thống',
  `updated_at` datetime DEFAULT NULL COMMENT 'Thời gian cập nhật thông tin bản ghi lần cuối cùng',
  `created_user_id` int(10) NOT NULL COMMENT 'Id người tạo thực đơn'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lưu thông tin thời thực đơn của trường, khối,lớp';

-- --------------------------------------------------------

--
-- Table structure for table `ci_menu_detail`
--

CREATE TABLE `ci_menu_detail` (
  `menu_detail_id` int(10) NOT NULL,
  `menu_id` int(10) NOT NULL COMMENT 'menu id',
  `meal_name` varchar(300) NOT NULL COMMENT 'Tên bữa',
  `meal_time` varchar(100) NOT NULL COMMENT 'Thười gian ăn',
  `monday` text NOT NULL COMMENT 'Danh sách món ăn thứ 2',
  `tuesday` text NOT NULL COMMENT 'Danh sách món ăn thứ 3',
  `wednesday` text NOT NULL COMMENT 'Danh sách món ăn thứ 4',
  `thursday` text NOT NULL COMMENT 'Danh sách món ăn thứ 5',
  `friday` text NOT NULL COMMENT 'Danh sách món ăn thứ 6',
  `saturday` text NOT NULL COMMENT 'Danh sách món ăn thứ 7'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lưu thông tin chi tiết thực đơn';

-- --------------------------------------------------------

--
-- Table structure for table `ci_noga_manage`
--

CREATE TABLE `ci_noga_manage` (
  `noga_manage_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL COMMENT 'ID của user quản lý',
  `school_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'ID trường được quản lý',
  `role_id` int(10) UNSIGNED DEFAULT '1' COMMENT 'Lưu quyền của người quản lý. \n1: Toàn quyền xem thông tin & xử lý',
  `manage_type` tinyint(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Loại hình quản lý của user là gì?\n0: quản lý theo danh sách phân công\n1: quản lý theo tỉnh/thành phố',
  `city_id` int(10) DEFAULT NULL COMMENT 'ID tỉnh thành phố mà user được phân công quản lý. Trường này chỉ được sử dụng khi type = 1.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Quản lý thông tin nhân viên nào của cty NOGA quản lý trường gì';

-- --------------------------------------------------------

--
-- Table structure for table `ci_noga_notification`
--

CREATE TABLE `ci_noga_notification` (
  `noga_notification_id` int(10) UNSIGNED NOT NULL,
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Trạng thái Notification đã gửi hay chưa?\n0 - Chưa gửi\n1- Đã gửi',
  `time` datetime NOT NULL COMMENT 'Thời gian gửi thông báo',
  `type_user` tinyint(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Đối tượng nhận được thông báo\n0 - Không cấu hình\n1- Tất cả user\n2 - Quản lý\n3 - Giáo viên\n4 - Phụ huynh',
  `city_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Thành phố',
  `district_slug` varchar(150) DEFAULT NULL COMMENT 'Quận',
  `gender` tinyint(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Giới tính của người nhận \n0 - Chưa chọn thông tin\n1 - Nam \n2-  Nữ',
  `from_age` int(2) UNSIGNED DEFAULT '0' COMMENT 'Độ tuổi từ ? - đến ?',
  `to_age` int(2) UNSIGNED DEFAULT '0',
  `is_repeat` tinyint(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Có lặp lại hay không?\n0 - Không\n1 - Có',
  `repeat_time` tinyint(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Theo chu kỳ\n0 - Không theo chu kỳ\n1 - Năm\n2 - Tháng\n3 - Tuần\n4 - Ngày',
  `type` tinyint(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Kiểu gửi thông báo\n0 - Text\n1 - Link',
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `link` tinytext,
  `description` text COMMENT 'Ghi chú thêm về thông báo',
  `created_user_id` int(10) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Bảng lưu thông báo của hệ thống';

-- --------------------------------------------------------

--
-- Table structure for table `ci_notification`
--

CREATE TABLE `ci_notification` (
  `notification_id` int(10) UNSIGNED NOT NULL,
  `level` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Phạm vi của thông báo\n=1: trường\n=2: khối\n=3: Lớp\n=4: Phụ huynh của 1 trẻ\n',
  `title` varchar(100) NOT NULL COMMENT 'Tiêu đề của thông báo',
  `content` text COMMENT 'Nội dung thông báo',
  `school_id` int(10) UNSIGNED DEFAULT NULL,
  `class_level_id` int(10) UNSIGNED DEFAULT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `child_id` int(10) UNSIGNED DEFAULT NULL,
  `post_on_wall` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Có post nội dung thông báo lên tường của trường, lớp không?',
  `post_ids` varchar(250) DEFAULT NULL COMMENT 'Danh sách ID bài viết của post nếu Thông báo được post lên tường. Các ID được phân tách nhau bởi dấu phẩy ","',
  `is_need_confirm` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Thông báo này có cần confirm không?\n0: Không\n1: Có',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Trạng thái của thông báo',
  `time` datetime NOT NULL,
  `is_notified` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Thông báo đã thông báo đến người liên quan chưa?\n- 1: đã thông báo\n- 0: chưa thông báo'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lưu các loại thông báo của trường liên quan đến chức năng quản lý';

-- --------------------------------------------------------

--
-- Table structure for table `ci_notifications_setting`
--

CREATE TABLE `ci_notifications_setting` (
  `user_id` int(11) UNSIGNED NOT NULL,
  `school_id` int(11) UNSIGNED NOT NULL,
  `events` tinyint(1) NOT NULL DEFAULT '3' COMMENT 'Lưu trạng thái nhận thông báo',
  `attendance` tinyint(1) NOT NULL DEFAULT '3' COMMENT 'Lưu trạng thái nhận thông báo của điểm danh',
  `feedback` tinyint(1) NOT NULL DEFAULT '3' COMMENT 'Lưu trangjt hái nhận thông báo của góp ý',
  `tuitions` tinyint(1) NOT NULL DEFAULT '3',
  `medicines` tinyint(1) NOT NULL DEFAULT '3',
  `pickup` tinyint(1) NOT NULL DEFAULT '3',
  `reports` tinyint(1) NOT NULL DEFAULT '3',
  `services` tinyint(1) NOT NULL DEFAULT '3',
  `children` tinyint(1) NOT NULL DEFAULT '3'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lưu thông tin cấu hình notification của user quản  lý';

-- --------------------------------------------------------

--
-- Table structure for table `ci_parent_manage`
--

CREATE TABLE `ci_parent_manage` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `child_parent_id` int(10) UNSIGNED NOT NULL COMMENT 'ID trong bảng quản lý trẻ của phụ huynh',
  `child_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'ID trẻ trong trường',
  `school_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'ID của trường (nếu có)',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Trạng thái quản lý của user với đối tượng. \n- status = 0: Inactive\n- status = 1: Active',
  `description` varchar(300) DEFAULT NULL COMMENT 'Mô tả việc quản lý đối tượng'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lưu đối tượng trẻ mà phụ huynh quản lý';

-- --------------------------------------------------------

--
-- Table structure for table `ci_parent_of_child_birthday`
--

CREATE TABLE `ci_parent_of_child_birthday` (
  `child_id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED NOT NULL,
  `parent_phone` varchar(20) NOT NULL,
  `parent_email` varchar(255) NOT NULL,
  `parent_name` varchar(255) NOT NULL,
  `parent_fullname` varchar(255) NOT NULL,
  `parent_gender` enum('male','female') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lưu danh sách phụ huynh của trẻ';

-- --------------------------------------------------------

--
-- Table structure for table `ci_pickup`
--

CREATE TABLE `ci_pickup` (
  `pickup_id` int(10) NOT NULL COMMENT 'ID của lần trông muộn',
  `pickup_class_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'ID của lớp trông muộn',
  `school_id` int(10) UNSIGNED NOT NULL COMMENT 'ID của trường',
  `total_child` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Tổng số trẻ trong lớp',
  `total` double UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Tổng tiền của lớp',
  `pickup_time` date NOT NULL COMMENT 'Ngày trông muộn',
  `pickup_day` int(1) UNSIGNED NOT NULL,
  `status` int(1) UNSIGNED DEFAULT '0',
  `description` text COMMENT 'Mô tả thêm',
  `recorded_user_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Người tạo bản ghi',
  `recorded_at` datetime DEFAULT NULL COMMENT 'Thời gian update bản ghi'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lưu thông tin các lớp trông muộn của trường';

-- --------------------------------------------------------

--
-- Table structure for table `ci_pickup_assign`
--

CREATE TABLE `ci_pickup_assign` (
  `pickup_id` int(10) UNSIGNED NOT NULL COMMENT 'ID lần trông muộn',
  `user_id` int(10) NOT NULL COMMENT 'ID người được phân công',
  `assign_time` date NOT NULL COMMENT 'Ngày được phân công',
  `pickup_class_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'ID của lớp trông muộn',
  `school_id` int(10) UNSIGNED NOT NULL COMMENT 'ID trường',
  `assign_day` int(1) UNSIGNED NOT NULL DEFAULT '2' COMMENT 'Ngày được phân công'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lưu thông tin phân công lớp trông muộn';

-- --------------------------------------------------------

--
-- Table structure for table `ci_pickup_child`
--

CREATE TABLE `ci_pickup_child` (
  `pickup_child_id` int(10) UNSIGNED NOT NULL,
  `pickup_id` int(10) UNSIGNED NOT NULL COMMENT 'ID của lần trông muộn',
  `pickup_class_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'ID của lớp trông muộn',
  `school_id` int(10) UNSIGNED NOT NULL COMMENT 'ID trường',
  `class_id` int(10) UNSIGNED NOT NULL COMMENT 'ID lớp',
  `child_id` int(10) UNSIGNED NOT NULL COMMENT 'ID trẻ',
  `total_amount` double UNSIGNED DEFAULT '0' COMMENT 'Tổng tiền phải trả',
  `late_pickup_fee` double UNSIGNED DEFAULT '0' COMMENT 'Tiền trông muộn',
  `using_service_fee` double UNSIGNED DEFAULT '0' COMMENT 'Tiền sử dụng dịch vụ',
  `pickup_at` time DEFAULT NULL COMMENT 'Thời gian trả trẻ',
  `type` int(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '0: giáo viên lớp ghi sử dụng\n1: quản lý trường ghi sử dụng\n2: phụ huynh đăng ký\n',
  `status` int(1) UNSIGNED NOT NULL COMMENT 'Trạng thái của trẻ trong lớp trông muộn:\n0: Chưa trả\n1: Đã trả\n2: Đã hủy',
  `description` text COMMENT 'Mô tả cho từng trẻ',
  `subscriber` varchar(255) NOT NULL COMMENT 'Người đăng ký',
  `registered_time` datetime NOT NULL,
  `giveback_person` varchar(255) DEFAULT NULL COMMENT 'Người trả trẻ'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lưu thông tin chi tiết trông muộn của từng trẻ';

-- --------------------------------------------------------

--
-- Table structure for table `ci_pickup_class`
--

CREATE TABLE `ci_pickup_class` (
  `pickup_class_id` int(10) UNSIGNED NOT NULL,
  `school_id` int(10) UNSIGNED NOT NULL,
  `class_name` varchar(100) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT '1' COMMENT 'Trạng thái của lớp trả muộn, có còn được sử dụng hay ko?0: Không1: Có',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_user_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lưu thông tin các lớp trông muộn của 1 trường (có nhiều lớp trông muộn)';

-- --------------------------------------------------------

--
-- Table structure for table `ci_pickup_price_list`
--

CREATE TABLE `ci_pickup_price_list` (
  `unit_price_id` int(10) NOT NULL,
  `school_id` int(10) UNSIGNED NOT NULL,
  `beginning_time` time NOT NULL,
  `ending_time` time NOT NULL,
  `unit_price` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Bảng lưu thông tin giờ đón muộn và giá';

-- --------------------------------------------------------

--
-- Table structure for table `ci_pickup_register`
--

CREATE TABLE `ci_pickup_register` (
  `pickup_register_id` int(10) UNSIGNED NOT NULL COMMENT 'ID',
  `school_id` int(10) UNSIGNED NOT NULL COMMENT 'ID của trường',
  `class_id` int(10) UNSIGNED NOT NULL COMMENT 'ID của lớp',
  `child_id` int(10) UNSIGNED NOT NULL COMMENT 'ID của trẻ',
  `description` varchar(300) DEFAULT NULL COMMENT 'Mô tả',
  `registered_date` date NOT NULL COMMENT 'Ngày đăng ký',
  `registered_user_id` int(10) NOT NULL COMMENT 'ID người đăng ký',
  `registered_user_fullname` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL COMMENT 'Thời gian đăng ký',
  `updated_at` datetime DEFAULT NULL COMMENT 'Thời gian cập nhật bản ghi'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lưu thông tin đăng ký trả muộn của phụ huynh';

-- --------------------------------------------------------

--
-- Table structure for table `ci_pickup_service`
--

CREATE TABLE `ci_pickup_service` (
  `service_id` int(10) NOT NULL COMMENT 'ID dịch vụ',
  `school_id` int(10) UNSIGNED NOT NULL COMMENT 'ID của trường',
  `service_name` varchar(100) NOT NULL COMMENT 'Tên dịch vụ',
  `fee` double UNSIGNED DEFAULT '0' COMMENT 'Giá',
  `description` varchar(300) DEFAULT NULL COMMENT 'Mô tả',
  `is_used` tinyint(1) UNSIGNED NOT NULL DEFAULT '1' COMMENT 'Trạng thái của dịch vụ, có còn được sử dụng hay không?\n0: Không \n1: Có',
  `beginning_date` date NOT NULL COMMENT 'Thời gian bắt đầu sử dụng',
  `ending_date` date DEFAULT NULL COMMENT 'Thời gian kết thúc',
  `latest_using_time` date DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_user_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lưu dịch vụ đi kèm khi trông muộn';

-- --------------------------------------------------------

--
-- Table structure for table `ci_pickup_service_register`
--

CREATE TABLE `ci_pickup_service_register` (
  `pickup_register_id` int(10) UNSIGNED NOT NULL COMMENT 'ID',
  `school_id` int(10) UNSIGNED NOT NULL COMMENT 'ID của trường',
  `child_id` int(10) UNSIGNED NOT NULL COMMENT 'ID của trẻ',
  `service_id` int(10) UNSIGNED NOT NULL COMMENT 'ID của dịch vụ',
  `registered_at` date NOT NULL COMMENT 'Đăng ký ngày',
  `service_name` varchar(100) NOT NULL COMMENT 'Tên dịch vụ',
  `price` double UNSIGNED NOT NULL COMMENT 'Giá dịch vụ'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lưu thông tin đăng ký dịch vụ của phụ huynh';

-- --------------------------------------------------------

--
-- Table structure for table `ci_pickup_service_usage`
--

CREATE TABLE `ci_pickup_service_usage` (
  `service_usage_id` int(10) UNSIGNED NOT NULL COMMENT 'ID',
  `pickup_id` int(10) UNSIGNED NOT NULL COMMENT 'ID của lần trả muộn',
  `service_id` int(10) UNSIGNED NOT NULL COMMENT 'ID của dịch vụ',
  `service_name` varchar(100) NOT NULL COMMENT 'Tên dịch vụ',
  `price` double UNSIGNED NOT NULL COMMENT 'Giá của dịch vụ',
  `child_id` int(10) UNSIGNED NOT NULL COMMENT 'ID của trẻ',
  `using_at` date NOT NULL COMMENT 'Dịch vụ được sử dụng vào lúc',
  `recorded_user_id` int(10) NOT NULL COMMENT 'Người tạo bản ghi',
  `recorded_at` datetime NOT NULL COMMENT 'Bản ghi được tạo lúc'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lưu lịch sử sử dụng dịch vụ trông muộn của trẻ';

-- --------------------------------------------------------

--
-- Table structure for table `ci_pickup_teacher`
--

CREATE TABLE `ci_pickup_teacher` (
  `school_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_fullname` varchar(255) NOT NULL,
  `user_name` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ci_pickup_template`
--

CREATE TABLE `ci_pickup_template` (
  `school_id` int(10) UNSIGNED NOT NULL COMMENT 'ID của trường',
  `is_repeat` tinyint(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Có lặp lịch phân công theo tuần hay không?',
  `beginning_repeat_date` date DEFAULT NULL COMMENT 'Ngày bắt đầu lặp lịch',
  `assign_type` enum('week','month') NOT NULL DEFAULT 'week' COMMENT 'Phân công theo tuần hay tháng',
  `created_at` datetime NOT NULL COMMENT 'Thời gian bản ghi được tạo',
  `updated_at` datetime DEFAULT NULL COMMENT 'Thời gian bản ghi được update',
  `created_user_id` int(10) NOT NULL COMMENT 'Người tạo ra mẫu trông muộn'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Mẫu trông muộn của 1 trường, bao gồm:\n- Thời gian trả trẻ\r\n- Thời gian đón muộn nhất\r\n- Thời gian làm tròn\r\n- Kiểu thu, mức giá\r\n- Dịch vụ đi kèm';

-- --------------------------------------------------------

--
-- Table structure for table `ci_point`
--

CREATE TABLE `ci_point` (
  `point_id` int(11) NOT NULL,
  `school_year` varchar(9) NOT NULL,
  `class_id` int(11) NOT NULL,
  `child_code` varchar(50) NOT NULL,
  `subject_id` int(11) DEFAULT NULL,
  `is_notify_semester_1` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Đã thông báo điểm  kỳ 1 chưa',
  `is_notify_semester_2` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Đã thông báo điểm kỳ 2 chưa',
  `hs11` float DEFAULT NULL COMMENT 'điểm miệng - thường xuyên ( 15'')',
  `hs12` float DEFAULT NULL COMMENT 'điểm miệng - thường xuyên ( 15'')',
  `hs13` float DEFAULT NULL COMMENT 'điểm miệng - thường xuyên ( 15'')',
  `hs14` float DEFAULT NULL COMMENT 'điểm miệng - thường xuyên ( 15'')',
  `hs15` float DEFAULT NULL COMMENT 'điểm miệng - thường xuyên ( 15'')',
  `hs16` float DEFAULT NULL COMMENT 'điểm miệng - thường xuyên ( 15'')',
  `gk11` float DEFAULT NULL COMMENT 'điểm giữa kì 1',
  `gk12` float DEFAULT NULL COMMENT 'điểm giữa kì 1',
  `ck1` float DEFAULT NULL COMMENT 'điểm cuối kì 1',
  `hs21` float DEFAULT NULL COMMENT 'điểm miệng - thường xuyên ( 15'')',
  `hs22` float DEFAULT NULL COMMENT 'điểm miệng - thường xuyên ( 15'')',
  `hs23` float DEFAULT NULL COMMENT 'điểm miệng - thường xuyên ( 15'')',
  `hs24` float DEFAULT NULL COMMENT 'điểm miệng - thường xuyên ( 15'')',
  `hs25` float DEFAULT NULL COMMENT 'điểm miệng - thường xuyên ( 15'')',
  `hs26` float DEFAULT NULL COMMENT 'điểm miệng - thường xuyên ( 15'')',
  `gk21` float DEFAULT NULL COMMENT 'điểm giữa kì 2',
  `gk22` float DEFAULT NULL COMMENT 'điểm giữa kì 2',
  `ck2` float DEFAULT NULL COMMENT 'điểm cuối kì 2',
  `re_exam` float DEFAULT NULL COMMENT 'Điểm thi lại ( nếu có )'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lưu bảng điểm và nhận xét cho cấp tiểu học (C1).';

-- --------------------------------------------------------

--
-- Table structure for table `ci_point_km_c2`
--

CREATE TABLE `ci_point_km_c2` (
  `point_id` int(11) NOT NULL,
  `school_year` varchar(9) NOT NULL COMMENT 'Lưu năm học. Lưu đúng 9 kỳ tự. VD: 2019-2020, 2020-2021',
  `class_id` int(11) NOT NULL,
  `child_code` varchar(50) NOT NULL COMMENT 'Mã trẻ',
  `subject_id` int(11) NOT NULL COMMENT 'Mã môn học',
  `is_notify_semester_1` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Đã thông báo điểm kỳ 1 chưa',
  `is_notify_semester_2` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Đã thông báo điểm kỳ 2 chưa',
  `m11` varchar(4) DEFAULT NULL COMMENT 'Điểm month1 của kì 1',
  `m12` varchar(4) DEFAULT NULL,
  `m13` varchar(4) DEFAULT NULL,
  `d1` varchar(4) DEFAULT NULL COMMENT 'điểm cuối kì 1',
  `m21` varchar(4) DEFAULT NULL,
  `m22` varchar(4) DEFAULT NULL,
  `m23` varchar(4) DEFAULT NULL,
  `d2` varchar(4) DEFAULT NULL COMMENT 'điểm cuối kì 2',
  `re_exam` varchar(4) DEFAULT NULL COMMENT 'điểm thi lại'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lưu bảng điểm THCS' ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `ci_region_manage`
--

CREATE TABLE `ci_region_manage` (
  `region_manage_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `district_slug` varchar(150) DEFAULT NULL,
  `grade` int(11) DEFAULT NULL COMMENT 'Lưu cấp được quản lý (= 0, quản lý tất cả các khối).',
  `role` int(11) DEFAULT '1' COMMENT 'Vai trò của người dùng (cơ quan quản lý) với cấp mà mình được phân. Hiện tại chỉ mới view nên chưa dùng',
  `provider` int(11) DEFAULT '0' COMMENT 'Đơn vị nào cấp tài khoản này. 1: NOGA cấp, 0: phòng cấp'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ci_report`
--

CREATE TABLE `ci_report` (
  `report_id` int(10) NOT NULL,
  `report_name` varchar(300) NOT NULL,
  `child_id` int(10) NOT NULL,
  `school_id` int(10) NOT NULL,
  `is_notified` tinyint(1) NOT NULL,
  `source_file` varchar(200) NOT NULL,
  `file_name` varchar(100) NOT NULL,
  `status` tinyint(10) NOT NULL DEFAULT '1',
  `date` datetime DEFAULT NULL COMMENT 'Ngày gửi thông báo',
  `views_count` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Bao nhiêu lượt xem sổ liên lạc',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_user_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ci_report_category`
--

CREATE TABLE `ci_report_category` (
  `report_category_id` int(10) UNSIGNED NOT NULL,
  `report_id` int(10) NOT NULL,
  `report_category_name` varchar(300) NOT NULL,
  `report_category_content` text,
  `multi_content` text COMMENT 'Lưu các giá trị mà cô giáo tích, cách nhau tập ký tự %$*!*',
  `template_multi_content` text COMMENT 'Lưu các giá trị gợi ý mà mẫu có sẵn'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lưu các hạng mục của sổ liên lạc';

-- --------------------------------------------------------

--
-- Table structure for table `ci_report_template`
--

CREATE TABLE `ci_report_template` (
  `report_template_id` int(10) NOT NULL,
  `template_name` varchar(300) NOT NULL,
  `level` tinyint(1) NOT NULL DEFAULT '1',
  `school_id` int(10) NOT NULL,
  `class_level_id` int(10) DEFAULT '0',
  `class_id` int(10) DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_user_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lưu template của các trường';

-- --------------------------------------------------------

--
-- Table structure for table `ci_report_template_category`
--

CREATE TABLE `ci_report_template_category` (
  `report_template_category_id` int(10) UNSIGNED NOT NULL,
  `category_name` varchar(300) NOT NULL,
  `school_id` int(10) NOT NULL,
  `template_multi_content` text COMMENT 'Lưu các giá trị có thể có của danh mục, cách nhau tập ký tự %$*!*'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lưu các category trường tạo sẵn';

-- --------------------------------------------------------

--
-- Table structure for table `ci_report_template_detail`
--

CREATE TABLE `ci_report_template_detail` (
  `report_template_category_id` int(10) UNSIGNED NOT NULL,
  `report_template_id` int(10) UNSIGNED NOT NULL,
  `template_content` text,
  `position` tinyint(4) DEFAULT NULL COMMENT 'Lưu vị trí của tiêu chí trong mẫu'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lưunhững hạng mục có trong template';

-- --------------------------------------------------------

--
-- Table structure for table `ci_review`
--

CREATE TABLE `ci_review` (
  `review_id` int(10) UNSIGNED NOT NULL,
  `type` enum('school','teacher') NOT NULL COMMENT 'Đối tượng được đánh giá là giáo viên hay trường',
  `school_id` int(10) UNSIGNED NOT NULL COMMENT 'ID của trường',
  `teacher_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'ID của giáo viên',
  `total_review` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Tổng số đánh giá',
  `average_review` float(2,1) DEFAULT '0.0' COMMENT 'Đánh giá trung bình'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Bảng lưu thông tin đánh giá cho trường hoặc giáo viên';

-- --------------------------------------------------------

--
-- Table structure for table `ci_role`
--

CREATE TABLE `ci_role` (
  `role_id` int(10) NOT NULL,
  `role_name` varchar(255) NOT NULL,
  `school_id` int(10) UNSIGNED NOT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Danh sách các vai trò trong một trường.';

-- --------------------------------------------------------

--
-- Table structure for table `ci_role_permission`
--

CREATE TABLE `ci_role_permission` (
  `module` varchar(50) NOT NULL COMMENT 'Tên module trên hệ thống',
  `role_id` int(10) UNSIGNED NOT NULL,
  `value` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Lưu giá trị quyền với module của role này:\n1: chỉ xem\n2: có thể sửa'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lưu các quyền của một vai trò.';

-- --------------------------------------------------------

--
-- Table structure for table `ci_schedule`
--

CREATE TABLE `ci_schedule` (
  `schedule_id` int(10) NOT NULL,
  `schedule_name` varchar(100) DEFAULT NULL COMMENT 'Tên thời khóa biểu. Ví dụ: TKB Học kỳ 1',
  `applied_for` tinyint(1) NOT NULL DEFAULT '3' COMMENT 'Thời khóa biểu này áp dụng cho đối tượng nào.\n= 1: áp dụng cho toàn trường\n= 2: áp dụng cho khối\n= 3: áp dụng cho lớp',
  `school_id` int(10) NOT NULL COMMENT 'Thời khóa biểu này áp dụng cho đối tượng nào.\n= 1: áp dụng cho toàn trường\n= 2: áp dụng cho khối\n= 3: áp dụng cho lớp',
  `class_level_id` int(10) NOT NULL COMMENT 'Thời khóa biểu này áp dụng cho đối tượng nào.\n= 1: áp dụng cho toàn trường\n= 2: áp dụng cho khối\n= 3: áp dụng cho lớp',
  `class_id` int(10) NOT NULL COMMENT 'Thời khóa biểu này áp dụng cho đối tượng nào.\n= 1: áp dụng cho toàn trường\n= 2: áp dụng cho khối\n= 3: áp dụng cho lớp',
  `begin` date DEFAULT NULL COMMENT 'Ngày bắt đầu áp dụng thời khóa biểu. Ngày đó là ngày thời khóa biểu chuyển sang trạng thái active.',
  `end` date DEFAULT NULL COMMENT 'Ngày cuối cùng áp dụng thời khóa biểu. Là ngày thời khóa biểu chuyển từ active sang inactive.',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Trạng thái áp dụng. \n- 1: active\n- 0: inactie',
  `description` varchar(300) DEFAULT NULL COMMENT 'Mô tả thời khóa biểu',
  `is_category` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Có sử dụng chương trình học không',
  `is_saturday` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Có học thứ 7 không',
  `is_notified` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Lưu trạng thái thông báo',
  `views_count` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Số lượt xem lịch học',
  `created_at` datetime NOT NULL COMMENT 'Thời gian bản ghi được tạo ra trong hệ thống',
  `updated_at` datetime DEFAULT NULL COMMENT 'Thời gian cập nhật thông tin bản ghi lần cuối cùng',
  `created_user_id` int(10) NOT NULL COMMENT 'Thời gian bản ghi được tạo ra trong hệ thống'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ci_schedule_detail`
--

CREATE TABLE `ci_schedule_detail` (
  `schedule_detail_id` int(10) NOT NULL,
  `schedule_id` int(10) NOT NULL COMMENT 'ID catalogy',
  `subject_name` varchar(300) NOT NULL COMMENT 'Tên hoạt động',
  `subject_time` varchar(100) NOT NULL COMMENT 'Thười gian bắt đầu hoạt động',
  `monday` text NOT NULL COMMENT 'Tên môn học thứ 2',
  `tuesday` text NOT NULL COMMENT 'Tên môn học thứ 3',
  `wednesday` text NOT NULL COMMENT 'Tên môn học thứ 4',
  `thursday` text NOT NULL COMMENT 'Tên môn học thứ 5',
  `friday` text NOT NULL COMMENT 'Tên môn học vào thứ 6',
  `saturday` text NOT NULL COMMENT 'Tên môn học vào thứ 7'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lưu thông tin chi tiết thời khóa biểu';

-- --------------------------------------------------------

--
-- Table structure for table `ci_school_child`
--

CREATE TABLE `ci_school_child` (
  `school_id` int(10) UNSIGNED NOT NULL COMMENT 'ID trường',
  `child_id` int(10) UNSIGNED NOT NULL,
  `child_parent_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'ID trong bảng quản lý trẻ của phụ huynh',
  `class_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'ID class. Lưu để tiện query (hơi thừa dữ liệu).',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Trạng thái học sinh trong trường:\n0: Inactive\n1: Active\n2: WaitApprove',
  `begin_at` date DEFAULT NULL COMMENT 'Thời gian bắt đầu vào trường của bé',
  `pre_tuition_receive` double DEFAULT '0' COMMENT 'Số tiền học phí thu trước.',
  `end_at` date DEFAULT NULL COMMENT 'Thời gian bé không còn học ở trường',
  `requested_user_id` int(10) UNSIGNED DEFAULT NULL,
  `approved_user_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'ID của cô giáo/user, người approve trẻ vào lớp.',
  `requested_at` datetime DEFAULT NULL,
  `approved_at` datetime DEFAULT NULL,
  `is_delete` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Xem trẻ được xóa trong trường chưa'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Bảng này lưu thông tin học sinh của một trường';

-- --------------------------------------------------------

--
-- Table structure for table `ci_school_configuration`
--

CREATE TABLE `ci_school_configuration` (
  `school_id` int(10) UNSIGNED NOT NULL,
  `teacher_count` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `male_count` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `female_count` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `class_count` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `bank_account` text NOT NULL COMMENT 'Lưu thông tin chuyển khoản của nhà trường',
  `school_allow_comment` tinyint(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Có cho phép comment trang của trường không? =0: không =1: có',
  `class_allow_post` tinyint(1) UNSIGNED NOT NULL DEFAULT '1' COMMENT 'Có cho phép phụ huynh post bài trong group của lớp không?',
  `class_allow_comment` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Lớp cho phép phụ huynh comment bài viết. (Nếu đã cho post bài thì sẽ cho comment)',
  `notification_tuition_interval` tinyint(1) UNSIGNED NOT NULL DEFAULT '10' COMMENT 'Thời gian nhắc nhở nộp tiền học phí',
  `allow_class_event` tinyint(1) UNSIGNED NOT NULL DEFAULT '1' COMMENT 'Cho phép lớp tạo sự kiện không?',
  `allow_parent_register_service` tinyint(1) UNSIGNED NOT NULL DEFAULT '1' COMMENT 'Có cho phụ huynh đăng ký dịch vụ không?',
  `allow_teacher_rolls_days_before` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Có cho giáo viên điểm danh những ngày trước hay không',
  `school_allow_medicate` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Có cho trẻ uống thuốc hay không',
  `allow_class_see_tuition` tinyint(1) UNSIGNED NOT NULL DEFAULT '1' COMMENT 'Cho phép cô giáo của lớp có được xem thông tin học phí không? 0: Không được xem 1: Được xem',
  `display_children_list` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Có hiển thị danh sách lớp ở màn hình của phụ huynh hay không?',
  `display_parent_info_for_others` tinyint(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Hiện thông tin cha mẹ của trẻ trên màn hình của phụ huynh khác có con là bạn cùng lớp. 0: Không được xem 1: Được xem',
  `auto_rollup_time_on_day` time(6) DEFAULT NULL COMMENT 'Giờ tự động điểm danh trong ngày',
  `allow_teacher_edit_pickup_before` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `school_group_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `type` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `lat` float(10,6) NOT NULL DEFAULT '0.000000',
  `lng` float(10,6) NOT NULL DEFAULT '0.000000',
  `city_slug` varchar(150) DEFAULT NULL,
  `city_name` varchar(150) DEFAULT NULL,
  `district_slug` varchar(150) DEFAULT NULL,
  `district_name` varchar(150) DEFAULT NULL,
  `rating` float(2,1) NOT NULL DEFAULT '0.0',
  `count_rate` int(5) UNSIGNED NOT NULL DEFAULT '0',
  `directions` text,
  `short_overview` text,
  `review_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `start_age` int(5) UNSIGNED NOT NULL,
  `end_age` int(5) UNSIGNED NOT NULL,
  `start_tuition_fee` int(10) UNSIGNED DEFAULT '0',
  `end_tuition_fee` varchar(150) DEFAULT NULL,
  `note_for_tuition` text,
  `attendance_allow_delete_old` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Cho phép xoá thông tin điểm danh ngày trong quá khứ',
  `attendance_use_leave_early` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Sử dụng cơ chế trẻ về sớm (không tính tiền ăn)',
  `attendance_use_come_late` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Sử dụng cơ chế trẻ đến muộn (tính tiền ăn)',
  `attendance_absence_no_reason` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Sử dụng cơ chế nghỉ không lý do (tính tiền ăn)',
  `tuition_view_attandance` tinyint(1) UNSIGNED NOT NULL DEFAULT '1' COMMENT 'Hiện thông tin điểm danh khi tạo học phí',
  `admission` int(5) UNSIGNED NOT NULL DEFAULT '0',
  `note_for_admission` text,
  `image_url` varchar(150) DEFAULT NULL,
  `verify` enum('0','1') NOT NULL DEFAULT '0',
  `facility_pool` enum('0','1') NOT NULL DEFAULT '0',
  `facility_playground_out` enum('0','1') NOT NULL DEFAULT '0',
  `facility_playground_in` enum('0','1') NOT NULL DEFAULT '0',
  `facility_library` enum('0','1') NOT NULL DEFAULT '0',
  `facility_camera` enum('0','1') NOT NULL DEFAULT '0',
  `note_for_facility` text,
  `service_breakfast` enum('0','1') NOT NULL DEFAULT '0',
  `service_belated` enum('0','1') NOT NULL DEFAULT '0',
  `service_saturday` enum('0','1') NOT NULL DEFAULT '0',
  `service_bus` enum('0','1') NOT NULL DEFAULT '0',
  `note_for_service` text,
  `info_leader` text,
  `info_method` text,
  `info_teacher` text,
  `info_nutrition` text,
  `tuition_child_report_template` varchar(2) DEFAULT 'A5' COMMENT 'Định dạng khi in ra phiếu học phí của trẻ là A4 hay A5',
  `tuition_use_mdservice_deduction` tinyint(1) DEFAULT '0' COMMENT 'Sử dụng cơ chế tính giảm trừ dịch vụ theo THÁNG và ĐIỂM DANH không.',
  `children_use_no_class` tinyint(1) DEFAULT '0' COMMENT 'Sử dụng cơ chế ''trẻ chưa phân vào lớp''',
  `principal` varchar(300) DEFAULT NULL COMMENT 'Lưu id hiệu trưởng của trường',
  `school_status` int(1) NOT NULL DEFAULT '2' COMMENT 'Lưu trạng thái của trường (page), 1: đang sử dụng Inet, 2: đang có page coniu, 3: đang chờ xác nhận',
  `views_in_thirty_days` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Số lượt xem page trong 30 ngày',
  `display_rate_school` int(1) UNSIGNED NOT NULL DEFAULT '1' COMMENT 'Hiển thị đánh giá trường hay ko?Hiển thị đánh giá trường hay ko?',
  `camera_guide` varchar(300) DEFAULT NULL COMMENT 'Link hướng dẫn camera',
  `score_fomula` varchar(5) NOT NULL DEFAULT 'vn' COMMENT 'Trường lưu công thức tính điểm theo nước nào',
  `school_step` tinyint(2) NOT NULL DEFAULT '0' COMMENT 'Lưu các bước khởi tạo trường',
  `report_cycle` tinyint(1) DEFAULT NULL COMMENT 'Lưu chu kỳ gửi sổ liên lạc của trường',
  `grade` tinyint(2) NOT NULL DEFAULT '0' COMMENT 'Lưu cấp của trường',
  `attendance` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'trường lưu quyền sử dụng đối với từng module',
  `services` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'trường lưu quyền sử dụng đối với từng module',
  `pickup` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'trường lưu quyền sử dụng đối với từng module',
  `pickupteacher` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'trường lưu quyền sử dụng đối với từng module',
  `tuitions` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'trường lưu quyền sử dụng đối với từng module',
  `events` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'trường lưu quyền sử dụng đối với từng module',
  `schedules` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'trường lưu quyền sử dụng đối với từng module',
  `menus` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'trường lưu quyền sử dụng đối với từng module',
  `reports` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'trường lưu quyền sử dụng đối với từng module',
  `medicines` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'trường lưu quyền sử dụng đối với từng module',
  `feedback` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'trường lưu quyền sử dụng đối với từng module',
  `birthdays` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'trường lưu quyền sử dụng đối với từng module',
  `diarys` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'trường lưu quyền sử dụng đối với từng module',
  `healths` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'trường lưu quyền sử dụng đối với từng module',
  `classes` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'trường lưu quyền sử dụng đối với từng module',
  `classlevels` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'trường lưu quyền sử dụng đối với từng module',
  `teachers` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'trường lưu quyền sử dụng đối với từng module',
  `children` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'trường lưu quyền sử dụng đối với từng module',
  `roles` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'trường lưu quyền sử dụng đối với từng module',
  `loginstatistics` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'trường lưu quyền sử dụng đối với từng module',
  `settings` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'trường lưu quyền sử dụng đối với từng module',
  `conducts` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'trường lưu quyền sử dụng đối với từng module',
  `points` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'trường lưu quyền sử dụng đối với từng module',
  `subjects` tinyint(11) NOT NULL DEFAULT '0' COMMENT '	trường lưu quyền sử dụng đối với từng module',
  `courses` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lưu các thông số cấu hình của một trường.';

-- --------------------------------------------------------

--
-- Table structure for table `ci_school_group`
--

CREATE TABLE `ci_school_group` (
  `school_group_id` int(10) NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL,
  `school_group_name` varchar(255) NOT NULL,
  `school_group_title` varchar(255) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `description` text NOT NULL,
  `created_at` datetime NOT NULL COMMENT 'Thời gian bản ghi được tạo ra trong hệ thống',
  `updated_at` datetime DEFAULT NULL COMMENT 'Thời gian cập nhật thông tin bản ghi lần cuối cùng',
  `created_user_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lưu hệ thống trường';

-- --------------------------------------------------------

--
-- Table structure for table `ci_school_photos`
--

CREATE TABLE `ci_school_photos` (
  `photo_id` int(10) UNSIGNED NOT NULL,
  `school_id` int(10) UNSIGNED NOT NULL,
  `album_id` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `source` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Lưu ảnh của trường (tìm trường quanh đây)';

-- --------------------------------------------------------

--
-- Table structure for table `ci_service`
--

CREATE TABLE `ci_service` (
  `service_id` int(11) NOT NULL,
  `service_name` varchar(254) NOT NULL COMMENT 'Tên dịch vụ',
  `school_id` int(11) NOT NULL,
  `type` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Phương thức thanh toán chi phí sử dụng dịch vụ.\ntype = 1: phí hàng tháng (ví dụ: xe đưa đón)\ntype = 2: phí hàng ngày (tính theo ngày trẻ đi học).\ntype = 3: theo số lần sử dụng',
  `must_register` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Dịch vụ có phải đăng ký trước khi sử dụng không?\n1: Có\n0: Không',
  `fee` double NOT NULL COMMENT 'Lệ phí học. Lệ phí phụ thuộc vào hình thức thanh toán (type)\ntype = 1: phí hàng tháng (ví dụ: xe đưa đón)\ntype = 2: phí hàng ngày (tính theo ngày trẻ đi học).\ntype = 3: theo số lần sử dụng',
  `description` varchar(300) DEFAULT NULL COMMENT 'Mô tả lớp học thêm',
  `created_at` datetime NOT NULL COMMENT 'Thời gian bản ghi được tạo ra trong hệ thống',
  `updated_at` datetime DEFAULT NULL COMMENT 'Thời gian cập nhật thông tin bản ghi lần cuối cùng',
  `created_user_id` int(11) NOT NULL COMMENT 'Thời gian bản ghi được tạo ra trong hệ thống',
  `is_food` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Dịch vụ này có phải là dịch vụ liên quan đến ăn uống hay không, để gửi thông tin tới nhà bếp.\\n= 0: Không phải dịch vụ ăn uống\\n= 1: Là dịch vụ ăn uống',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Trạng thái dịch vụ, còn áp dụng hay không:\\n= 1: còn áp dụng\\n= 0: không còn áp dụng',
  `parent_display` tinyint(1) DEFAULT '1' COMMENT 'Có cho hiển thị bên phía phụ huynh không'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lưu thông tin dịch vụ tùy chọn của trường. Ví dụ: xe đưa đón';

-- --------------------------------------------------------

--
-- Table structure for table `ci_service_child`
--

CREATE TABLE `ci_service_child` (
  `service_child_id` int(10) NOT NULL,
  `service_id` int(10) UNSIGNED NOT NULL COMMENT 'ID lớp',
  `child_id` int(10) UNSIGNED NOT NULL COMMENT 'ID trẻ',
  `begin` datetime DEFAULT NULL COMMENT 'Ngày bé bắt đầu sử dụng dịch vụ',
  `end` datetime DEFAULT NULL COMMENT 'Ngày cuối sử dụng dịch vụ',
  `old_begin` datetime DEFAULT NULL COMMENT 'Lưu ngày bắt đầu đăng ký của lần trước',
  `old_end` datetime DEFAULT NULL COMMENT 'Lưu ngày hủy dịch vụ lần trước (dịch vụ có thể đăng ký/hủy nhiều lần)',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Trạng thái học sinh sử dụng dịch vụ. Nếu trạng thái là ACTIVE thì mới thực sự đang sử dụng\n- status = 0: Inactive\n- status = 1: Active\nKhi cha mẹ đăng ký cho con sử dụng dịch vụ thì trạng thái là Inactive. Cô giáo của lớp sẽ accept y/c, lúc đó trạng thái mới chuyển sang Active.',
  `approved_user_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'ID của cô giáo/user, người approve trẻ sử dụng dịch vụ',
  `description` varchar(300) DEFAULT NULL COMMENT 'Mô tả',
  `created_at` datetime NOT NULL COMMENT 'Thời gian bản ghi được tạo ra trong hệ thống',
  `updated_at` datetime DEFAULT NULL COMMENT 'Thời gian cập nhật thông tin bản ghi lần cuối cùng',
  `created_user_id` int(10) NOT NULL COMMENT 'Thời gian bản ghi được tạo ra trong hệ thống'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Quản lý danh sách học sinh đăng ký sử dụng dịch vụ';

-- --------------------------------------------------------

--
-- Table structure for table `ci_service_deduction`
--

CREATE TABLE `ci_service_deduction` (
  `service_id` int(10) NOT NULL,
  `child_id` int(10) NOT NULL,
  `deduction_date` date NOT NULL,
  `recorded_user_id` int(10) NOT NULL,
  `recorded_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lưu thông tin giảm trừ dịch vụ của trẻ đăng ký dịch vụ theo Tháng và Điểm danh';

-- --------------------------------------------------------

--
-- Table structure for table `ci_service_usage`
--

CREATE TABLE `ci_service_usage` (
  `service_usage_id` int(10) UNSIGNED NOT NULL,
  `service_id` int(10) UNSIGNED NOT NULL,
  `child_id` int(10) UNSIGNED NOT NULL,
  `using_at` datetime NOT NULL COMMENT 'Thời gian sử dụng dịch vụ.',
  `recorded_user_id` int(10) NOT NULL COMMENT 'ID người tạo bản ghi',
  `recorded_at` datetime NOT NULL COMMENT 'Thời gian lưu vào hệ thống.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Bảng lưu số lần sử dụng dịch vụ của trường. Chỉ áp dụng với trường hợp dịch vụ tính phí theo số lần sử dụng.';

-- --------------------------------------------------------

--
-- Table structure for table `ci_subject`
--

CREATE TABLE `ci_subject` (
  `subject_id` int(10) NOT NULL,
  `subject_name` varchar(20) NOT NULL COMMENT 'Tên môn học',
  `re_exam` tinyint(4) NOT NULL COMMENT 'Có thi lại hay không',
  `country_code` varchar(2) NOT NULL COMMENT 'mã quốc gia, để biết môn của nước nào'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Bảng môn học';

-- --------------------------------------------------------

--
-- Table structure for table `ci_teacher`
--

CREATE TABLE `ci_teacher` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `school_id` int(10) UNSIGNED NOT NULL,
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Quản lý danh sách giáo viên của trường';

-- --------------------------------------------------------

--
-- Table structure for table `ci_tuition`
--

CREATE TABLE `ci_tuition` (
  `tuition_id` int(10) NOT NULL,
  `tuition_name` varchar(100) DEFAULT NULL COMMENT 'Tên khoản học phí (vd: học phí tháng 08/2016',
  `school_id` int(10) UNSIGNED NOT NULL COMMENT 'ID trường. Cho vào query cho tiện',
  `class_id` int(10) UNSIGNED NOT NULL COMMENT 'ID class',
  `type` tinyint(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Phí thu hàng tháng hay là thu 1 lần\n= 0: hàng tháng\n= 1: một lần',
  `paid_count` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Số lượng trẻ đã trả học phí',
  `total_amount` double UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Số tiền cần thu',
  `total_deduction` double UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Số tiền giảm trừ',
  `paid_amount` double UNSIGNED NOT NULL DEFAULT '0',
  `month` date DEFAULT NULL COMMENT 'Tháng, mà học phí cho tháng đó. Nó sẽ có dạng MM/YYYY',
  `day_of_month` tinyint(2) UNSIGNED DEFAULT NULL COMMENT 'Số ngày học trong một tháng của trường',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Trạng thái khoản thu.\n= 0: Chưa thu đủ\n= 1: Đã thu đủ',
  `description` varchar(300) DEFAULT NULL COMMENT 'Mô tả chi tiết',
  `is_notified` tinyint(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Thông báo cho phụ huynh chưa?',
  `created_at` datetime NOT NULL COMMENT 'Thời gian bản ghi được tạo ra trong hệ thống',
  `updated_at` datetime DEFAULT NULL COMMENT 'Thời gian cập nhật thông tin bản ghi lần cuối cùng',
  `created_user_id` int(10) UNSIGNED NOT NULL,
  `views_count` int(10) NOT NULL DEFAULT '0' COMMENT 'Lưu số lượt xem học phí'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lưu các khoản thu học phí của trường';

-- --------------------------------------------------------

--
-- Table structure for table `ci_tuition_4leave`
--

CREATE TABLE `ci_tuition_4leave` (
  `tuition_4leave_id` int(10) NOT NULL,
  `school_id` int(10) UNSIGNED NOT NULL,
  `child_id` int(10) UNSIGNED NOT NULL COMMENT 'ID của trẻ',
  `final_amount` double NOT NULL DEFAULT '0' COMMENT 'Giá trị quyết toán cuối cùng (nếu âm=>nhà trường trả lại tiền).',
  `debt_amount` double DEFAULT '0' COMMENT 'Tổng nợ tháng trước',
  `total_deduction` double DEFAULT '0' COMMENT 'Tổng giảm trừ tháng trước',
  `paid_amount` double UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Tổng học phí đã nộp đầu kỳ',
  `attendance_count` int(2) DEFAULT '0',
  `cashier_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'ID người thu tiền (người xác nhận thu tiền)',
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lưu quyết toán học phí cho trẻ nghỉ học';

-- --------------------------------------------------------

--
-- Table structure for table `ci_tuition_4leave_detail`
--

CREATE TABLE `ci_tuition_4leave_detail` (
  `tuition_4leave_id` int(10) UNSIGNED NOT NULL COMMENT 'Tên loại phí',
  `fee_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'ID của loại phí. Trường này chỉ sử dụng khi TYPE = 0',
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Loại phí:\n0 - Loại phí bình thường\n1 - Phí sử dụng dịch vụ',
  `service_id` int(10) NOT NULL DEFAULT '0' COMMENT 'ID của dịch vụ được sử dụng. Trường hợp này chỉ áp dụng khi TYPE = 1',
  `service_type` tinyint(1) DEFAULT '0' COMMENT 'Nếu là service thì lưu service_type để về sau hiển thị',
  `quantity` int(10) UNSIGNED NOT NULL DEFAULT '1' COMMENT 'Số lượng',
  `unit_price` double NOT NULL COMMENT 'Đơn giá này được lấy từ bảng FEE, có thể sửa đổi (vì cần sự linh hoạt)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lưu các loại phí/phí dịch vụ chi tiết mà trẻ phải đóng trước khi nghỉ';

-- --------------------------------------------------------

--
-- Table structure for table `ci_tuition_child`
--

CREATE TABLE `ci_tuition_child` (
  `tuition_child_id` int(10) NOT NULL,
  `tuition_id` int(10) UNSIGNED NOT NULL,
  `child_id` int(10) UNSIGNED NOT NULL COMMENT 'ID của trẻ',
  `total_amount` double NOT NULL DEFAULT '0' COMMENT 'Số tiền cần thu, là tổng của các khoản phải thu trong bảng tuition_detail',
  `paid_amount` double DEFAULT '0' COMMENT 'Lưu số tiền phụ huynh đã thanh toán (có thể thanh toán thiếu hoặc thừa tiền lẻ)',
  `total_deduction` double UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Số tiền giảm trừ, là tổng của các khoản giảm trừ trong bảng tuition_detail',
  `pre_month` date DEFAULT NULL COMMENT 'Thông tin tháng trước. Tháng tính giảm trừ cho tháng hiện tại',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Trạng thái khoản thu.\n = 0: chưa nộp\n = 1: phụ huynh đã nộp tiền\n =2: nhà trường xác nhận đã nộp',
  `paid_at` datetime DEFAULT NULL COMMENT 'Thời gian cập nhật thông tin bản ghi lần cuối cùng',
  `payer_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'ID của người nộp tiền',
  `cashier_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'ID người thu tiền (người xác nhận thu tiền)',
  `debt_amount` double NOT NULL DEFAULT '0' COMMENT 'Lưu khoản nợ tháng trước. Nếu:\\n> 0: Phụ huynh nợ trường.\\n< 0: Phụ huynh đóng thừa (trường nợ).',
  `description` varchar(300) DEFAULT NULL,
  `last_notified_at` datetime DEFAULT NULL,
  `views_count` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Số lượt xem học phí'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lưu các khoản thu của trường';

-- --------------------------------------------------------

--
-- Table structure for table `ci_tuition_detail`
--

CREATE TABLE `ci_tuition_detail` (
  `tuition_child_id` int(10) UNSIGNED NOT NULL COMMENT 'Tên loại phí',
  `fee_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'ID của loại phí. Trường này chỉ sử dụng khi TYPE = 0',
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Loại phí:\n0 - Loại phí bình thường\n1 - Phí sử dụng dịch vụ',
  `service_id` int(10) NOT NULL DEFAULT '0' COMMENT 'ID của dịch vụ được sử dụng. Trường hợp này chỉ áp dụng khi TYPE = 1',
  `service_type` tinyint(1) DEFAULT '0' COMMENT 'Nếu là service thì lưu service_type để về sau hiển thị',
  `quantity` int(10) NOT NULL DEFAULT '1' COMMENT 'Số lượng',
  `quantity_deduction` int(10) NOT NULL DEFAULT '0' COMMENT 'Số lượng giảm trừ',
  `unit_price` double NOT NULL COMMENT 'Đơn giá này được lấy từ bảng FEE, có thể sửa đổi (vì cần sự linh hoạt)',
  `unit_price_deduction` double NOT NULL DEFAULT '0' COMMENT 'Đơn giá giảm trừ'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lưu các loại phí/phí dịch vụ mà trẻ phải đóng trong một lần nộp học phí.';

-- --------------------------------------------------------

--
-- Table structure for table `ci_users_online`
--

CREATE TABLE `ci_users_online` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lưu danh sách user onlie của hệ thống';

-- --------------------------------------------------------

--
-- Table structure for table `ci_user_birthday`
--

CREATE TABLE `ci_user_birthday` (
  `user_birthday_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(64) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `gender` enum('male','female') NOT NULL,
  `birthday` date NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(255) NOT NULL,
  `type` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lưu thông tin sinh nhật của trường';

-- --------------------------------------------------------

--
-- Table structure for table `ci_user_manage`
--

CREATE TABLE `ci_user_manage` (
  `user_manage_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL COMMENT 'ID của user quản lý',
  `object_id` int(10) UNSIGNED NOT NULL COMMENT 'ID của đối tượng quản lý. Đối tượng là gì thì xem ở trường TYPE có giá trị bao nhiêu.\ntype = 1: - ID trẻ\ntype = 2: - ID trường\ntype = 3: - ID khối\ntype = 4: - ID lớp',
  `object_type` tinyint(1) UNSIGNED NOT NULL DEFAULT '1' COMMENT 'Chỉ ra loại đối tượng mà user quản lý type= 0:- trẻ tự quản lí bản thân type = 1: - Cha mẹ quản lý trẻ type = 2: - Quản trị trường quản lý thông tin trườngtype = 3: - Cô giáo quản lý lớp',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Trạng thái quản lý của user với đối tượng. Chỉ khi nào trạng thái là ACTIVE thì user mới được quản lý thực sự.\n- status = 0: Inactive\n- status = 1: Active',
  `role_id` int(10) UNSIGNED DEFAULT '1' COMMENT 'Lưu quyền của người quản lý\n1: có tất cả các quyền\n2: có các quyền quản lý nhưng không được xóa/sửa thông tin\n3: chỉ có thể xem',
  `description` varchar(300) DEFAULT NULL COMMENT 'Mô tả việc quản lý đối tượng'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Bảng này chỉ ra user quản lý gì trong hệ thống.';

-- --------------------------------------------------------

--
-- Table structure for table `ci_user_review`
--

CREATE TABLE `ci_user_review` (
  `user_review_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL COMMENT 'ID người đánh giá',
  `time` date NOT NULL COMMENT 'Thời gian (tháng)',
  `type` enum('school','teacher') NOT NULL COMMENT 'Đối tượng đanh giá ( Trường, giáo viên)',
  `school_id` int(10) UNSIGNED NOT NULL COMMENT 'ID của trường',
  `class_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'ID của lớp',
  `teacher_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'ID của giáo viên',
  `post_id` int(10) UNSIGNED DEFAULT '0' COMMENT 'ID bài post',
  `rating` int(1) UNSIGNED NOT NULL COMMENT 'Điểm đánh giá',
  `comment` text COMMENT 'Nhận xét',
  `created_at` datetime NOT NULL COMMENT 'Thời gian tạo',
  `updated_at` datetime DEFAULT NULL COMMENT 'Thời gian update'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Bảng lưu đánh giá chi tiết';

-- --------------------------------------------------------

--
-- Table structure for table `ci_user_role`
--

CREATE TABLE `ci_user_role` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `school_id` int(10) NOT NULL COMMENT 'ID trường. Để xử lý nhiều trường đơn giản hơn.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lưu danh sách vai trò của một user đối với trường.';

-- --------------------------------------------------------

--
-- Table structure for table `ci_views_count`
--

CREATE TABLE `ci_views_count` (
  `school_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'ID của trường',
  `view_type` varchar(32) NOT NULL COMMENT 'Mục tương tác (trang trương, bài viết, lịch học, học phi, sll,..)',
  `created_in_thirty_days` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Đã tạo trong 30 ngày gần nhất',
  `views_in_thirty_days` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Số lượt view 30 ngày gần nhất'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Bảng lưu thông tin lượt xem của trường';

-- --------------------------------------------------------

--
-- Table structure for table `ci_views_count_detail`
--

CREATE TABLE `ci_views_count_detail` (
  `view_type` varchar(32) NOT NULL COMMENT 'Mục tương tác (trang trương, bài viết, lịch học, học phi, sll,..)',
  `date` date NOT NULL COMMENT 'Ngày lưu lượt xem',
  `object_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'ID của đối tượng cần lưu thông tin tương tác',
  `school_id` int(10) UNSIGNED DEFAULT '0' COMMENT 'ID của trường',
  `views_count_in_date` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Số lượt view trong ngày',
  `is_add_new` tinyint(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Là bài viết được thêm mới hay không?'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Bảng lưu thông tin lượt xem';

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id_contact` int(11) NOT NULL,
  `name_contact` varchar(250) NOT NULL,
  `email_contact` varchar(250) NOT NULL,
  `phone_contact` varchar(50) NOT NULL,
  `select_school` tinyint(1) NOT NULL,
  `content_contact` text NOT NULL,
  `ip_contact` varchar(250) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Lưu thông tin liên hệ';

-- --------------------------------------------------------

--
-- Table structure for table `conversations`
--

CREATE TABLE `conversations` (
  `conversation_id` int(10) UNSIGNED NOT NULL,
  `last_message_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `conversations_messages`
--

CREATE TABLE `conversations_messages` (
  `message_id` int(10) UNSIGNED NOT NULL,
  `conversation_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `message` longtext NOT NULL,
  `image` varchar(255) NOT NULL,
  `time` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `conversations_users`
--

CREATE TABLE `conversations_users` (
  `conversation_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `seen` enum('0','1') NOT NULL DEFAULT '0',
  `deleted` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

-- --------------------------------------------------------

--
-- Table structure for table `custom_fields`
--

CREATE TABLE `custom_fields` (
  `field_id` int(10) UNSIGNED NOT NULL,
  `field_for` enum('user','page','group','event') NOT NULL DEFAULT 'user',
  `type` varchar(32) NOT NULL,
  `select_options` text NOT NULL,
  `label` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `place` varchar(32) NOT NULL,
  `length` int(10) NOT NULL DEFAULT '32',
  `field_order` int(10) NOT NULL DEFAULT '1',
  `mandatory` enum('0','1') NOT NULL DEFAULT '0',
  `in_registration` enum('0','1') NOT NULL DEFAULT '0',
  `in_profile` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `custom_fields_values`
--

CREATE TABLE `custom_fields_values` (
  `value_id` int(10) UNSIGNED NOT NULL,
  `value` text NOT NULL,
  `field_id` int(10) UNSIGNED NOT NULL,
  `node_id` int(10) UNSIGNED NOT NULL,
  `node_type` enum('user','page','group','event') NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

-- --------------------------------------------------------

--
-- Table structure for table `emojis`
--

CREATE TABLE `emojis` (
  `emoji_id` int(10) UNSIGNED NOT NULL,
  `pattern` varchar(255) NOT NULL,
  `class` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `event_id` int(10) UNSIGNED NOT NULL,
  `event_privacy` enum('secret','closed','public') DEFAULT 'public',
  `event_admin` int(10) UNSIGNED NOT NULL,
  `event_category` int(10) UNSIGNED NOT NULL,
  `event_title` varchar(255) NOT NULL,
  `event_location` varchar(255) DEFAULT NULL,
  `event_description` text NOT NULL,
  `event_start_date` datetime NOT NULL,
  `event_end_date` datetime NOT NULL,
  `event_cover` varchar(255) DEFAULT NULL,
  `event_cover_id` int(10) UNSIGNED DEFAULT NULL,
  `event_album_covers` int(10) DEFAULT NULL,
  `event_album_timeline` int(10) DEFAULT NULL,
  `event_pinned_post` int(10) DEFAULT NULL,
  `event_invited` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `event_interested` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `event_going` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `event_date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `events_categories`
--

CREATE TABLE `events_categories` (
  `category_id` int(10) UNSIGNED NOT NULL,
  `category_name` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `events_members`
--

CREATE TABLE `events_members` (
  `id` int(10) UNSIGNED NOT NULL,
  `event_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `is_invited` enum('0','1') DEFAULT '0',
  `is_interested` enum('0','1') DEFAULT '0',
  `is_going` enum('0','1') DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

-- --------------------------------------------------------

--
-- Table structure for table `followings`
--

CREATE TABLE `followings` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `following_id` int(10) UNSIGNED NOT NULL,
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

-- --------------------------------------------------------

--
-- Table structure for table `forums`
--

CREATE TABLE `forums` (
  `forum_id` int(10) UNSIGNED NOT NULL,
  `forum_section` int(10) UNSIGNED NOT NULL,
  `forum_name` varchar(255) NOT NULL,
  `forum_description` text,
  `forum_order` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `forum_threads` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `forum_replies` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `forums_replies`
--

CREATE TABLE `forums_replies` (
  `reply_id` int(10) UNSIGNED NOT NULL,
  `thread_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `text` longtext NOT NULL,
  `time` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `forums_threads`
--

CREATE TABLE `forums_threads` (
  `thread_id` int(10) UNSIGNED NOT NULL,
  `forum_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `text` longtext NOT NULL,
  `replies` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `views` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `time` datetime NOT NULL,
  `last_reply` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `friends`
--

CREATE TABLE `friends` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_one_id` int(10) UNSIGNED NOT NULL,
  `user_two_id` int(10) UNSIGNED NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `games`
--

CREATE TABLE `games` (
  `game_id` int(10) NOT NULL,
  `title` varchar(255) CHARACTER SET latin1 NOT NULL,
  `description` text CHARACTER SET latin1 NOT NULL,
  `source` text CHARACTER SET latin1 NOT NULL,
  `thumbnail` varchar(255) CHARACTER SET latin1 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `game_players`
--

CREATE TABLE `game_players` (
  `id` int(10) UNSIGNED NOT NULL,
  `game_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `group_id` int(10) UNSIGNED NOT NULL,
  `group_privacy` enum('secret','closed','public') NOT NULL DEFAULT 'public' COMMENT 'Cấu hình riêng tư của group',
  `group_category` int(10) UNSIGNED NOT NULL COMMENT 'Category của group',
  `group_admin` int(10) UNSIGNED NOT NULL,
  `group_name` varchar(64) NOT NULL,
  `group_title` varchar(255) NOT NULL,
  `group_picture` varchar(255) NOT NULL,
  `group_picture_id` int(10) UNSIGNED NOT NULL,
  `group_cover` varchar(255) NOT NULL,
  `group_cover_id` int(10) UNSIGNED NOT NULL,
  `group_album_pictures` int(10) NOT NULL,
  `group_album_covers` int(10) NOT NULL,
  `group_album_timeline` int(10) NOT NULL,
  `group_pinned_post` int(10) NOT NULL,
  `group_description` text NOT NULL,
  `group_members` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `class_level_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `telephone` varchar(64) DEFAULT NULL,
  `email` varchar(64) DEFAULT NULL,
  `camera_url` varchar(255) DEFAULT NULL COMMENT 'Đường dẫn camera của lớp',
  `group_date` datetime NOT NULL COMMENT 'Thời gian tạo group',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Lưu trạng thái lớp đã tốt nghiệp chưa, 1: chưa tốt nghiệp, 0: tốt nghiệp rồi'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `groups_admins`
--

CREATE TABLE `groups_admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `group_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

-- --------------------------------------------------------

--
-- Table structure for table `groups_categories`
--

CREATE TABLE `groups_categories` (
  `category_id` int(10) UNSIGNED NOT NULL,
  `category_name` varchar(255) NOT NULL COMMENT 'Tên category của group'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Lưu category của group';

-- --------------------------------------------------------

--
-- Table structure for table `groups_members`
--

CREATE TABLE `groups_members` (
  `group_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `approved` enum('0','1') NOT NULL DEFAULT '0',
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

-- --------------------------------------------------------

--
-- Table structure for table `monitor`
--

CREATE TABLE `monitor` (
  `monitor_token` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `notification_id` int(10) UNSIGNED NOT NULL,
  `to_user_id` int(10) UNSIGNED NOT NULL,
  `from_user_id` int(10) UNSIGNED NOT NULL,
  `action` varchar(64) NOT NULL,
  `node_type` varchar(64) NOT NULL,
  `node_url` varchar(255) NOT NULL,
  `notify_id` varchar(255) DEFAULT NULL,
  `time` datetime DEFAULT NULL,
  `seen` enum('0','1') NOT NULL DEFAULT '0',
  `extra1` varchar(255) DEFAULT NULL COMMENT 'Trường dự phòng',
  `extra2` varchar(255) DEFAULT NULL COMMENT 'Trường dự phòng',
  `extra3` varchar(255) DEFAULT NULL COMMENT 'Trường dự phòng',
  `message` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE `packages` (
  `package_id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `price` varchar(32) NOT NULL,
  `period_num` int(10) UNSIGNED NOT NULL,
  `period` varchar(32) NOT NULL,
  `color` varchar(32) NOT NULL,
  `icon` varchar(255) NOT NULL,
  `boost_posts_enabled` enum('0','1') NOT NULL DEFAULT '0',
  `boost_posts` int(10) UNSIGNED NOT NULL,
  `boost_pages_enabled` enum('0','1') NOT NULL DEFAULT '0',
  `boost_pages` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `packages_payments`
--

CREATE TABLE `packages_payments` (
  `payment_id` int(10) NOT NULL,
  `payment_date` datetime NOT NULL,
  `package_name` varchar(255) NOT NULL,
  `package_price` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `page_id` int(10) UNSIGNED NOT NULL,
  `page_admin` int(10) UNSIGNED NOT NULL,
  `page_category` int(10) UNSIGNED NOT NULL,
  `page_name` varchar(64) NOT NULL,
  `page_title` varchar(255) NOT NULL,
  `page_description` text NOT NULL,
  `page_picture` varchar(255) DEFAULT NULL,
  `page_picture_id` int(10) UNSIGNED DEFAULT NULL,
  `page_cover` varchar(255) DEFAULT NULL,
  `page_cover_id` int(10) UNSIGNED DEFAULT NULL,
  `page_album_pictures` int(10) UNSIGNED DEFAULT NULL,
  `page_album_covers` int(10) UNSIGNED DEFAULT NULL,
  `page_album_timeline` int(10) UNSIGNED DEFAULT NULL,
  `page_pinned_post` int(10) UNSIGNED DEFAULT NULL,
  `page_verified` enum('0','1') NOT NULL DEFAULT '0',
  `page_likes` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `views_in_thirty_days` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Số lượt xem trang trong 30 ngày gần nhất',
  `city_id` int(10) DEFAULT NULL,
  `telephone` varchar(64) DEFAULT NULL,
  `website` varchar(64) DEFAULT NULL,
  `email` varchar(64) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `page_date` datetime NOT NULL,
  `page_boosted` enum('0','1') NOT NULL DEFAULT '0',
  `page_boosted_by` int(10) UNSIGNED DEFAULT NULL,
  `page_company` varchar(255) DEFAULT NULL,
  `page_phone` varchar(255) DEFAULT NULL,
  `page_website` varchar(255) DEFAULT NULL,
  `page_location` varchar(255) DEFAULT NULL,
  `page_action_text` varchar(32) DEFAULT NULL,
  `page_action_color` varchar(32) DEFAULT NULL,
  `page_action_url` varchar(255) DEFAULT NULL,
  `page_social_facebook` varchar(255) DEFAULT NULL,
  `page_social_twitter` varchar(255) DEFAULT NULL,
  `page_social_google` varchar(255) DEFAULT NULL,
  `page_social_youtube` varchar(255) DEFAULT NULL,
  `page_social_instagram` varchar(255) DEFAULT NULL,
  `page_social_linkedin` varchar(255) DEFAULT NULL,
  `page_social_vkontakte` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pages_admins`
--

CREATE TABLE `pages_admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `page_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `pages_categories`
--

CREATE TABLE `pages_categories` (
  `category_id` int(10) UNSIGNED NOT NULL,
  `category_name` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pages_invites`
--

CREATE TABLE `pages_invites` (
  `page_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `from_user_id` int(10) UNSIGNED NOT NULL,
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

-- --------------------------------------------------------

--
-- Table structure for table `pages_likes`
--

CREATE TABLE `pages_likes` (
  `page_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `post_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_type` enum('user','page') NOT NULL,
  `in_group` enum('0','1') NOT NULL DEFAULT '0',
  `group_id` int(10) UNSIGNED DEFAULT NULL,
  `in_wall` enum('0','1') NOT NULL DEFAULT '0',
  `wall_id` int(10) UNSIGNED DEFAULT NULL,
  `post_type` varchar(32) NOT NULL,
  `origin_id` int(10) UNSIGNED DEFAULT NULL,
  `time` datetime NOT NULL,
  `location` varchar(255) DEFAULT NULL,
  `privacy` varchar(32) NOT NULL,
  `text` longtext,
  `likes` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `comments` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `shares` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `school_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `views_count` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Số lượt xem bài viết',
  `in_event` enum('0','1') NOT NULL DEFAULT '0',
  `event_id` int(10) UNSIGNED DEFAULT NULL,
  `feeling_action` varchar(32) DEFAULT NULL,
  `feeling_value` varchar(255) DEFAULT NULL,
  `boosted` enum('0','1') NOT NULL DEFAULT '0',
  `boosted_by` int(10) UNSIGNED DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `posts_articles`
--

CREATE TABLE `posts_articles` (
  `article_id` int(10) UNSIGNED NOT NULL,
  `post_id` int(10) UNSIGNED NOT NULL,
  `cover` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `text` longtext NOT NULL,
  `tags` text NOT NULL,
  `views` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `posts_audios`
--

CREATE TABLE `posts_audios` (
  `audio_id` int(10) UNSIGNED NOT NULL,
  `post_id` int(10) UNSIGNED NOT NULL,
  `source` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `posts_comments`
--

CREATE TABLE `posts_comments` (
  `comment_id` int(10) UNSIGNED NOT NULL,
  `node_id` int(10) UNSIGNED NOT NULL,
  `node_type` enum('post','photo','comment') NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_type` enum('user','page') NOT NULL,
  `text` longtext NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `time` datetime NOT NULL,
  `likes` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `replies` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Số lượng reply của comment'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `posts_comments_likes`
--

CREATE TABLE `posts_comments_likes` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `comment_id` int(10) UNSIGNED NOT NULL,
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `posts_files`
--

CREATE TABLE `posts_files` (
  `file_id` int(10) UNSIGNED NOT NULL,
  `post_id` int(10) UNSIGNED NOT NULL,
  `source` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `posts_hidden`
--

CREATE TABLE `posts_hidden` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `post_id` int(10) UNSIGNED NOT NULL,
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `posts_likes`
--

CREATE TABLE `posts_likes` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `post_id` int(10) UNSIGNED NOT NULL,
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `posts_links`
--

CREATE TABLE `posts_links` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `post_id` int(10) UNSIGNED NOT NULL,
  `source_url` tinytext NOT NULL,
  `source_host` varchar(255) NOT NULL,
  `source_title` varchar(255) NOT NULL,
  `source_text` text NOT NULL,
  `source_thumbnail` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `posts_media`
--

CREATE TABLE `posts_media` (
  `media_id` int(10) UNSIGNED NOT NULL,
  `post_id` int(10) NOT NULL,
  `source_url` text NOT NULL,
  `source_provider` varchar(255) NOT NULL,
  `source_type` varchar(255) NOT NULL,
  `source_title` varchar(255) DEFAULT NULL,
  `source_thumbnail` varchar(255) NOT NULL,
  `source_text` text,
  `source_html` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `posts_photos`
--

CREATE TABLE `posts_photos` (
  `photo_id` int(10) UNSIGNED NOT NULL,
  `post_id` int(10) UNSIGNED NOT NULL,
  `album_id` int(10) UNSIGNED DEFAULT NULL,
  `source` varchar(255) NOT NULL,
  `likes` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `comments` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `posts_photos_albums`
--

CREATE TABLE `posts_photos_albums` (
  `album_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_type` enum('user','page') CHARACTER SET utf8 NOT NULL,
  `in_group` enum('0','1') CHARACTER SET utf8 NOT NULL DEFAULT '0',
  `group_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8 NOT NULL,
  `privacy` enum('me','friends','public','custom') CHARACTER SET utf8 NOT NULL DEFAULT 'public',
  `in_event` enum('0','1') CHARACTER SET utf8 NOT NULL DEFAULT '0',
  `event_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `posts_photos_likes`
--

CREATE TABLE `posts_photos_likes` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `photo_id` int(10) UNSIGNED NOT NULL,
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

-- --------------------------------------------------------

--
-- Table structure for table `posts_polls`
--

CREATE TABLE `posts_polls` (
  `poll_id` int(10) UNSIGNED NOT NULL,
  `post_id` int(10) UNSIGNED NOT NULL,
  `votes` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `posts_polls_options`
--

CREATE TABLE `posts_polls_options` (
  `option_id` int(10) UNSIGNED NOT NULL,
  `poll_id` int(10) UNSIGNED NOT NULL,
  `text` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `posts_polls_options_users`
--

CREATE TABLE `posts_polls_options_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `poll_id` int(10) UNSIGNED NOT NULL,
  `option_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

-- --------------------------------------------------------

--
-- Table structure for table `posts_products`
--

CREATE TABLE `posts_products` (
  `product_id` int(10) UNSIGNED NOT NULL,
  `post_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `price` varchar(32) NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `location` varchar(255) NOT NULL,
  `available` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `posts_saved`
--

CREATE TABLE `posts_saved` (
  `post_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `time` datetime NOT NULL,
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `posts_videos`
--

CREATE TABLE `posts_videos` (
  `video_id` int(10) UNSIGNED NOT NULL,
  `post_id` int(10) UNSIGNED NOT NULL,
  `source` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `reports`
--

CREATE TABLE `reports` (
  `report_id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `node_id` int(10) NOT NULL,
  `node_type` varchar(32) NOT NULL,
  `time` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `static_pages`
--

CREATE TABLE `static_pages` (
  `page_id` int(10) NOT NULL,
  `page_url` varchar(64) NOT NULL,
  `page_title` varchar(255) NOT NULL,
  `page_text` text NOT NULL,
  `page_in_footer` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `stickers`
--

CREATE TABLE `stickers` (
  `sticker_id` int(10) UNSIGNED NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `stories`
--

CREATE TABLE `stories` (
  `story_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `text` longtext,
  `time` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `stories_media`
--

CREATE TABLE `stories_media` (
  `media_id` int(10) UNSIGNED NOT NULL,
  `story_id` int(10) UNSIGNED NOT NULL,
  `source` varchar(255) NOT NULL,
  `is_photo` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `system_countries`
--

CREATE TABLE `system_countries` (
  `country_id` int(10) UNSIGNED NOT NULL,
  `country_code` varchar(2) NOT NULL DEFAULT '',
  `country_name` varchar(64) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `system_languages`
--

CREATE TABLE `system_languages` (
  `language_id` int(10) UNSIGNED NOT NULL,
  `code` varchar(32) NOT NULL,
  `title` varchar(255) NOT NULL,
  `flag_icon` varchar(32) NOT NULL,
  `dir` enum('LTR','RTL') NOT NULL,
  `default` enum('0','1') NOT NULL,
  `enabled` enum('0','1') NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `system_options`
--

CREATE TABLE `system_options` (
  `id` int(10) UNSIGNED NOT NULL,
  `system_public` enum('0','1') NOT NULL DEFAULT '1',
  `system_live` enum('0','1') NOT NULL DEFAULT '1',
  `system_message` text NOT NULL,
  `system_title` varchar(255) NOT NULL DEFAULT 'Coniu',
  `system_description` text NOT NULL,
  `system_keywords` text NOT NULL,
  `system_email` varchar(255) DEFAULT NULL,
  `contact_enabled` enum('0','1') NOT NULL DEFAULT '1',
  `directory_enabled` enum('0','1') NOT NULL DEFAULT '1',
  `pages_enabled` enum('0','1') NOT NULL DEFAULT '1',
  `groups_enabled` enum('0','1') NOT NULL DEFAULT '1',
  `events_enabled` enum('0','1') NOT NULL DEFAULT '1',
  `blogs_enabled` enum('0','1') NOT NULL DEFAULT '1',
  `market_enabled` enum('0','1') NOT NULL DEFAULT '1',
  `games_enabled` enum('0','1') NOT NULL DEFAULT '1',
  `daytime_msg_enabled` enum('0','1') NOT NULL DEFAULT '1',
  `verification_requests` enum('0','1') NOT NULL DEFAULT '1',
  `profile_notification_enabled` enum('0','1') NOT NULL DEFAULT '1',
  `browser_notifications_enabled` enum('0','1') NOT NULL DEFAULT '1',
  `noty_notifications_enabled` enum('0','1') NOT NULL DEFAULT '1',
  `stories_enabled` enum('0','1') NOT NULL DEFAULT '1',
  `wall_posts_enabled` enum('0','1') NOT NULL DEFAULT '1',
  `social_share_enabled` enum('0','1') NOT NULL DEFAULT '1',
  `smart_yt_player` enum('0','1') NOT NULL DEFAULT '1',
  `polls_enabled` enum('0','1') NOT NULL DEFAULT '1',
  `geolocation_enabled` enum('0','1') NOT NULL DEFAULT '0',
  `geolocation_key` varchar(255) DEFAULT NULL,
  `post_translation_enabled` enum('0','1') NOT NULL DEFAULT '0',
  `yandex_key` varchar(255) DEFAULT NULL,
  `default_privacy` enum('public','friends','me') NOT NULL DEFAULT 'friends',
  `registration_enabled` enum('0','1') NOT NULL DEFAULT '1',
  `registration_type` enum('free','paid') NOT NULL DEFAULT 'free',
  `invitation_enabled` enum('0','1') NOT NULL DEFAULT '0',
  `packages_enabled` enum('0','1') NOT NULL DEFAULT '0',
  `activation_enabled` enum('0','1') NOT NULL DEFAULT '1',
  `activation_type` enum('email','sms') NOT NULL DEFAULT 'email',
  `age_restriction` enum('0','1') NOT NULL DEFAULT '0',
  `minimum_age` tinyint(1) UNSIGNED DEFAULT NULL,
  `getting_started` enum('0','1') NOT NULL DEFAULT '1',
  `delete_accounts_enabled` enum('0','1') NOT NULL DEFAULT '1',
  `max_accounts` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `social_login_enabled` enum('0','1') NOT NULL DEFAULT '0',
  `facebook_login_enabled` enum('0','1') NOT NULL DEFAULT '0',
  `facebook_appid` varchar(255) DEFAULT NULL,
  `facebook_secret` varchar(255) DEFAULT NULL,
  `twitter_login_enabled` enum('0','1') NOT NULL DEFAULT '0',
  `twitter_appid` varchar(255) DEFAULT NULL,
  `twitter_secret` varchar(255) DEFAULT NULL,
  `google_login_enabled` enum('0','1') NOT NULL DEFAULT '0',
  `google_appid` varchar(255) DEFAULT NULL,
  `google_secret` varchar(255) DEFAULT NULL,
  `instagram_login_enabled` enum('0','1') NOT NULL DEFAULT '0',
  `instagram_appid` varchar(255) DEFAULT NULL,
  `instagram_secret` varchar(255) DEFAULT NULL,
  `linkedin_login_enabled` enum('0','1') NOT NULL DEFAULT '0',
  `linkedin_appid` varchar(255) DEFAULT NULL,
  `linkedin_secret` varchar(255) DEFAULT NULL,
  `vkontakte_login_enabled` enum('0','1') NOT NULL DEFAULT '0',
  `vkontakte_appid` varchar(255) DEFAULT NULL,
  `vkontakte_secret` varchar(255) DEFAULT NULL,
  `email_smtp_enabled` enum('0','1') NOT NULL DEFAULT '0',
  `email_smtp_authentication` enum('0','1') NOT NULL DEFAULT '1',
  `email_smtp_ssl` enum('0','1') NOT NULL DEFAULT '0',
  `email_smtp_server` varchar(255) DEFAULT NULL,
  `email_smtp_port` varchar(255) DEFAULT NULL,
  `email_smtp_username` varchar(255) DEFAULT NULL,
  `email_smtp_password` varchar(255) DEFAULT NULL,
  `email_smtp_setfrom` varchar(255) DEFAULT NULL,
  `email_notifications` enum('0','1') NOT NULL DEFAULT '1',
  `email_post_likes` enum('0','1') NOT NULL DEFAULT '1',
  `email_post_comments` enum('0','1') NOT NULL DEFAULT '1',
  `email_post_shares` enum('0','1') NOT NULL DEFAULT '1',
  `email_wall_posts` enum('0','1') NOT NULL DEFAULT '1',
  `email_mentions` enum('0','1') NOT NULL DEFAULT '1',
  `email_profile_visits` enum('0','1') NOT NULL DEFAULT '1',
  `email_friend_requests` enum('0','1') NOT NULL DEFAULT '1',
  `twilio_sid` varchar(255) DEFAULT NULL,
  `twilio_token` varchar(255) DEFAULT NULL,
  `twilio_phone` varchar(255) DEFAULT NULL,
  `system_phone` varchar(255) DEFAULT NULL,
  `chat_enabled` enum('0','1') NOT NULL DEFAULT '1',
  `chat_status_enabled` enum('0','1') NOT NULL DEFAULT '1',
  `s3_enabled` enum('0','1') NOT NULL DEFAULT '0',
  `s3_bucket` varchar(255) DEFAULT NULL,
  `s3_region` varchar(255) DEFAULT NULL,
  `s3_key` varchar(255) DEFAULT NULL,
  `s3_secret` varchar(255) DEFAULT NULL,
  `uploads_directory` varchar(255) NOT NULL DEFAULT 'content/uploads',
  `uploads_prefix` varchar(255) DEFAULT 'coniu',
  `max_avatar_size` int(10) UNSIGNED NOT NULL DEFAULT '5120',
  `max_cover_size` int(10) UNSIGNED NOT NULL DEFAULT '5120',
  `photos_enabled` enum('0','1') NOT NULL DEFAULT '1',
  `max_photo_size` int(10) UNSIGNED NOT NULL DEFAULT '5120',
  `uploads_quality` enum('high','medium','low') NOT NULL DEFAULT 'medium',
  `videos_enabled` enum('0','1') NOT NULL DEFAULT '1',
  `max_video_size` int(10) UNSIGNED NOT NULL DEFAULT '5120',
  `video_extensions` text NOT NULL,
  `audio_enabled` enum('0','1') NOT NULL DEFAULT '1',
  `max_audio_size` int(10) UNSIGNED NOT NULL DEFAULT '5120',
  `audio_extensions` text NOT NULL,
  `file_enabled` enum('0','1') NOT NULL DEFAULT '1',
  `max_file_size` int(10) UNSIGNED NOT NULL DEFAULT '5120',
  `file_extensions` text NOT NULL,
  `censored_words_enabled` enum('0','1') NOT NULL DEFAULT '1',
  `censored_words` text NOT NULL,
  `reCAPTCHA_enabled` enum('0','1') NOT NULL DEFAULT '0',
  `reCAPTCHA_site_key` varchar(255) DEFAULT NULL,
  `reCAPTCHA_secret_key` varchar(255) DEFAULT NULL,
  `session_hash` varchar(255) NOT NULL,
  `paypal_enabled` enum('0','1') NOT NULL DEFAULT '0',
  `paypal_mode` enum('live','sandbox') NOT NULL DEFAULT 'sandbox',
  `paypal_id` varchar(255) DEFAULT NULL,
  `paypal_secret` varchar(255) DEFAULT NULL,
  `creditcard_enabled` enum('0','1') NOT NULL DEFAULT '0',
  `alipay_enabled` enum('0','1') NOT NULL DEFAULT '0',
  `stripe_mode` enum('live','test') NOT NULL DEFAULT 'test',
  `stripe_test_secret` varchar(255) DEFAULT NULL,
  `stripe_test_publishable` varchar(255) DEFAULT NULL,
  `stripe_live_secret` varchar(255) DEFAULT NULL,
  `stripe_live_publishable` varchar(255) DEFAULT NULL,
  `system_currency` varchar(64) DEFAULT 'USD',
  `data_heartbeat` int(10) UNSIGNED NOT NULL DEFAULT '5',
  `chat_heartbeat` int(10) UNSIGNED NOT NULL DEFAULT '5',
  `offline_time` int(10) UNSIGNED NOT NULL DEFAULT '10',
  `min_results` int(10) UNSIGNED NOT NULL DEFAULT '5',
  `max_results` int(10) UNSIGNED NOT NULL DEFAULT '10',
  `min_results_even` int(10) UNSIGNED NOT NULL DEFAULT '6',
  `max_results_even` int(10) UNSIGNED NOT NULL DEFAULT '12',
  `analytics_code` text NOT NULL,
  `system_logo` varchar(255) DEFAULT NULL,
  `system_wallpaper_default` enum('0','1') NOT NULL DEFAULT '1',
  `system_wallpaper` varchar(255) DEFAULT NULL,
  `system_random_profiles` enum('0','1') NOT NULL DEFAULT '1',
  `system_favicon_default` enum('0','1') NOT NULL DEFAULT '1',
  `system_favicon` varchar(255) DEFAULT NULL,
  `system_ogimage_default` enum('0','1') NOT NULL DEFAULT '1',
  `system_ogimage` varchar(255) DEFAULT NULL,
  `css_customized` enum('0','1') NOT NULL DEFAULT '0',
  `css_background` varchar(32) DEFAULT NULL,
  `css_link_color` varchar(32) DEFAULT NULL,
  `css_header` varchar(32) DEFAULT NULL,
  `css_header_search` varchar(32) DEFAULT NULL,
  `css_header_search_color` varchar(32) DEFAULT NULL,
  `css_btn_primary` varchar(32) DEFAULT NULL,
  `css_menu_background` varchar(32) DEFAULT NULL,
  `css_custome_css` text NOT NULL,
  `custome_js_header` text NOT NULL,
  `custome_js_footer` text NOT NULL,
  `forums_enabled` enum('0','1') NOT NULL DEFAULT '1',
  `forums_online_enabled` enum('0','1') NOT NULL DEFAULT '1',
  `forums_statistics_enabled` enum('0','1') NOT NULL DEFAULT '1',
  `affiliates_enabled` enum('0','1') NOT NULL DEFAULT '0',
  `affiliate_type` enum('registration','packages') NOT NULL DEFAULT 'registration',
  `affiliate_payment_method` enum('paypal','skrill','both') NOT NULL DEFAULT 'both',
  `affiliates_min_withdrawal` int(10) UNSIGNED NOT NULL DEFAULT '50',
  `affiliates_per_user` varchar(32) NOT NULL DEFAULT '0.1',
  `ads_enabled` enum('0','1') NOT NULL DEFAULT '0',
  `ads_cost_click` varchar(32) NOT NULL DEFAULT '0.05',
  `ads_cost_view` varchar(32) NOT NULL DEFAULT '0.01'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `system_themes`
--

CREATE TABLE `system_themes` (
  `theme_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(64) NOT NULL,
  `default` enum('0','1') NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_group` tinyint(10) UNSIGNED NOT NULL DEFAULT '3',
  `user_name` varchar(64) NOT NULL,
  `user_email` varchar(64) DEFAULT NULL,
  `user_email_activation` varchar(64) DEFAULT NULL,
  `user_password` varchar(64) NOT NULL,
  `user_firstname` varchar(255) NOT NULL,
  `user_lastname` varchar(255) DEFAULT NULL,
  `user_fullname` varchar(255) NOT NULL,
  `user_gender` enum('male','female','other') NOT NULL,
  `user_birthdate` date DEFAULT NULL,
  `user_phone` varchar(64) DEFAULT NULL,
  `user_phone_signin` varchar(64) DEFAULT NULL COMMENT 'Luu sdt dung de dang nhap',
  `user_id_card_number` varchar(64) DEFAULT NULL COMMENT 'Số CMND, CCCD',
  `date_of_issue` date DEFAULT NULL COMMENT 'Ngày cấp',
  `place_of_issue` varchar(64) DEFAULT NULL COMMENT 'Nơi cấp',
  `city_id` int(10) NOT NULL DEFAULT '0',
  `user_picture` varchar(255) DEFAULT NULL,
  `user_picture_id` int(10) UNSIGNED DEFAULT NULL,
  `user_cover` varchar(255) DEFAULT NULL,
  `user_cover_id` int(10) UNSIGNED DEFAULT NULL,
  `user_album_pictures` int(10) UNSIGNED DEFAULT NULL,
  `user_album_covers` int(10) UNSIGNED DEFAULT NULL,
  `user_album_timeline` int(10) UNSIGNED DEFAULT NULL,
  `user_pinned_post` int(10) UNSIGNED DEFAULT NULL,
  `user_work_title` varchar(255) DEFAULT NULL,
  `user_work_place` varchar(255) DEFAULT NULL,
  `user_current_city` varchar(255) DEFAULT NULL,
  `user_hometown` varchar(255) DEFAULT NULL,
  `user_edu_major` varchar(255) DEFAULT NULL,
  `user_edu_school` varchar(255) DEFAULT NULL,
  `user_edu_class` varchar(255) DEFAULT NULL,
  `user_registered` datetime DEFAULT NULL,
  `user_last_login` datetime DEFAULT NULL,
  `user_last_active` datetime DEFAULT NULL COMMENT 'Lưu lần hoạt động cuối cùng',
  `user_privacy_wall` enum('me','friends','public') NOT NULL DEFAULT 'friends',
  `user_privacy_birthdate` enum('me','friends','public') NOT NULL DEFAULT 'me',
  `user_privacy_work` enum('me','friends','public') NOT NULL DEFAULT 'me',
  `user_privacy_location` enum('me','friends','public') NOT NULL DEFAULT 'me',
  `user_privacy_education` enum('me','friends','public') NOT NULL DEFAULT 'me',
  `user_privacy_friends` enum('me','friends','public') NOT NULL DEFAULT 'me',
  `user_privacy_photos` enum('me','friends','public') NOT NULL DEFAULT 'me',
  `user_privacy_pages` enum('me','friends','public') NOT NULL DEFAULT 'me',
  `user_privacy_groups` enum('me','friends','public') NOT NULL DEFAULT 'me',
  `user_activation_key` varchar(64) DEFAULT NULL,
  `user_activated` enum('0','1') NOT NULL DEFAULT '0',
  `user_verified` enum('0','1') NOT NULL DEFAULT '0',
  `user_started` enum('0','1') NOT NULL DEFAULT '0',
  `user_changed_pass` enum('0','1') NOT NULL DEFAULT '0',
  `user_reseted` enum('0','1') NOT NULL DEFAULT '0',
  `user_reset_key` varchar(64) DEFAULT NULL,
  `user_blocked` enum('0','1') NOT NULL DEFAULT '0',
  `user_ip` varchar(64) NOT NULL,
  `user_chat_enabled` enum('0','1') NOT NULL DEFAULT '1',
  `user_live_requests_counter` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `user_live_requests_lastid` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `user_live_messages_counter` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `user_live_messages_lastid` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `user_live_notifications_counter` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `user_live_notifications_lastid` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `facebook_connected` enum('0','1') NOT NULL DEFAULT '0',
  `facebook_id` varchar(255) DEFAULT NULL,
  `twitter_connected` enum('0','1') NOT NULL DEFAULT '0',
  `twitter_id` varchar(255) DEFAULT NULL,
  `google_connected` enum('0','1') NOT NULL DEFAULT '0',
  `google_id` varchar(255) DEFAULT NULL,
  `linkedin_connected` enum('0','1') NOT NULL DEFAULT '0',
  `linkedin_id` varchar(255) DEFAULT NULL,
  `vkontakte_connected` enum('0','1') NOT NULL DEFAULT '0',
  `vkontakte_id` varchar(255) DEFAULT NULL,
  `device_token` varchar(255) DEFAULT NULL COMMENT 'deviceToken (token firebase để push notification về device)',
  `user_subscribed` enum('0','1') NOT NULL DEFAULT '0',
  `user_package` int(10) UNSIGNED DEFAULT NULL,
  `user_subscription_date` datetime DEFAULT NULL,
  `user_boosted_posts` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `user_boosted_pages` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `user_banned` enum('0','1') NOT NULL DEFAULT '0',
  `user_country` int(10) UNSIGNED DEFAULT NULL,
  `user_relationship` varchar(255) DEFAULT NULL,
  `user_biography` text,
  `user_website` varchar(255) DEFAULT NULL,
  `user_work_url` varchar(255) DEFAULT NULL,
  `user_social_facebook` varchar(255) DEFAULT NULL,
  `user_social_twitter` varchar(255) DEFAULT NULL,
  `user_social_google` varchar(255) DEFAULT NULL,
  `user_social_youtube` varchar(255) DEFAULT NULL,
  `user_social_instagram` varchar(255) DEFAULT NULL,
  `user_social_linkedin` varchar(255) DEFAULT NULL,
  `user_social_vkontakte` varchar(255) DEFAULT NULL,
  `user_privacy_relationship` enum('me','friends','public') NOT NULL DEFAULT 'me',
  `user_privacy_basic` enum('me','friends','public') NOT NULL DEFAULT 'me',
  `user_privacy_other` enum('me','friends','public') NOT NULL DEFAULT 'me',
  `user_privacy_events` enum('me','friends','public') NOT NULL DEFAULT 'me',
  `email_post_likes` enum('0','1') NOT NULL DEFAULT '1',
  `email_post_comments` enum('0','1') NOT NULL DEFAULT '1',
  `email_post_shares` enum('0','1') NOT NULL DEFAULT '1',
  `email_wall_posts` enum('0','1') NOT NULL DEFAULT '1',
  `email_mentions` enum('0','1') NOT NULL DEFAULT '1',
  `email_profile_visits` enum('0','1') NOT NULL DEFAULT '1',
  `email_friend_requests` enum('0','1') NOT NULL DEFAULT '1',
  `instagram_connected` enum('0','1') NOT NULL DEFAULT '0',
  `instagram_id` varchar(255) DEFAULT NULL,
  `user_referrer_id` int(10) DEFAULT NULL,
  `user_affiliate_balance` varchar(64) NOT NULL DEFAULT '0',
  `user_wallet_balance` varchar(64) NOT NULL DEFAULT '0',
  `notifications_sound` enum('0','1') NOT NULL DEFAULT '1',
  `created_id` int(10) UNSIGNED DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users_blocks`
--

CREATE TABLE `users_blocks` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `blocked_id` int(10) UNSIGNED NOT NULL,
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

-- --------------------------------------------------------

--
-- Table structure for table `users_online`
--

CREATE TABLE `users_online` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `last_seen` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `users_searches`
--

CREATE TABLE `users_searches` (
  `log_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `node_id` int(10) UNSIGNED NOT NULL,
  `node_type` varchar(32) NOT NULL,
  `time` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `users_sessions`
--

CREATE TABLE `users_sessions` (
  `session_id` int(10) UNSIGNED NOT NULL,
  `session_token` varchar(64) NOT NULL,
  `device_token` varchar(255) DEFAULT NULL,
  `session_date` datetime NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_browser` varchar(64) NOT NULL,
  `user_os` varchar(64) NOT NULL,
  `device_version` varchar(255) NOT NULL,
  `user_ip` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `verification_requests`
--

CREATE TABLE `verification_requests` (
  `request_id` int(10) UNSIGNED NOT NULL,
  `node_id` int(10) UNSIGNED NOT NULL,
  `node_type` varchar(32) NOT NULL,
  `time` datetime NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `widgets`
--

CREATE TABLE `widgets` (
  `widget_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `place` varchar(32) NOT NULL,
  `code` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ads_campaigns`
--
ALTER TABLE `ads_campaigns`
  ADD PRIMARY KEY (`campaign_id`);

--
-- Indexes for table `ads_system`
--
ALTER TABLE `ads_system`
  ADD PRIMARY KEY (`ads_id`);

--
-- Indexes for table `ads_users_wallet_transactions`
--
ALTER TABLE `ads_users_wallet_transactions`
  ADD PRIMARY KEY (`transaction_id`);

--
-- Indexes for table `affiliates_payments`
--
ALTER TABLE `affiliates_payments`
  ADD PRIMARY KEY (`payment_id`);

--
-- Indexes for table `announcements`
--
ALTER TABLE `announcements`
  ADD PRIMARY KEY (`announcement_id`);

--
-- Indexes for table `announcements_users`
--
ALTER TABLE `announcements_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `announcement_id_user_id` (`announcement_id`,`user_id`);

--
-- Indexes for table `banned_ips`
--
ALTER TABLE `banned_ips`
  ADD PRIMARY KEY (`ip_id`);

--
-- Indexes for table `ci_attendance`
--
ALTER TABLE `ci_attendance`
  ADD PRIMARY KEY (`attendance_id`),
  ADD UNIQUE KEY `class_id` (`class_id`,`attendance_date`);

--
-- Indexes for table `ci_attendance_back`
--
ALTER TABLE `ci_attendance_back`
  ADD PRIMARY KEY (`attendance_back_id`);

--
-- Indexes for table `ci_attendance_back_detail`
--
ALTER TABLE `ci_attendance_back_detail`
  ADD PRIMARY KEY (`attendance_back_detail_id`);

--
-- Indexes for table `ci_attendance_detail`
--
ALTER TABLE `ci_attendance_detail`
  ADD PRIMARY KEY (`attendance_detail_id`),
  ADD KEY `att_id_child_id` (`attendance_id`,`child_id`) USING BTREE;

--
-- Indexes for table `ci_background_email`
--
ALTER TABLE `ci_background_email`
  ADD PRIMARY KEY (`email_id`);

--
-- Indexes for table `ci_background_firebase`
--
ALTER TABLE `ci_background_firebase`
  ADD PRIMARY KEY (`firebase_id`);

--
-- Indexes for table `ci_background_notifications`
--
ALTER TABLE `ci_background_notifications`
  ADD PRIMARY KEY (`send_notification_id`);

--
-- Indexes for table `ci_child`
--
ALTER TABLE `ci_child`
  ADD PRIMARY KEY (`child_id`);

--
-- Indexes for table `ci_child_based_on_month`
--
ALTER TABLE `ci_child_based_on_month`
  ADD PRIMARY KEY (`child_based_on_month_id`);

--
-- Indexes for table `ci_child_development`
--
ALTER TABLE `ci_child_development`
  ADD PRIMARY KEY (`child_development_id`);

--
-- Indexes for table `ci_child_development_child`
--
ALTER TABLE `ci_child_development_child`
  ADD PRIMARY KEY (`child_development_child_id`);

--
-- Indexes for table `ci_child_edit_history`
--
ALTER TABLE `ci_child_edit_history`
  ADD PRIMARY KEY (`ci_child_edit_history_id`);

--
-- Indexes for table `ci_child_foetus_growth`
--
ALTER TABLE `ci_child_foetus_growth`
  ADD PRIMARY KEY (`child_foetus_growth_id`);

--
-- Indexes for table `ci_child_growth`
--
ALTER TABLE `ci_child_growth`
  ADD PRIMARY KEY (`child_growth_id`);

--
-- Indexes for table `ci_child_info`
--
ALTER TABLE `ci_child_info`
  ADD PRIMARY KEY (`child_info_id`);

--
-- Indexes for table `ci_child_journal`
--
ALTER TABLE `ci_child_journal`
  ADD PRIMARY KEY (`child_journal_id`);

--
-- Indexes for table `ci_child_journal_album`
--
ALTER TABLE `ci_child_journal_album`
  ADD PRIMARY KEY (`child_journal_album_id`);

--
-- Indexes for table `ci_child_medical`
--
ALTER TABLE `ci_child_medical`
  ADD PRIMARY KEY (`child_medical_id`);

--
-- Indexes for table `ci_child_parent`
--
ALTER TABLE `ci_child_parent`
  ADD PRIMARY KEY (`child_parent_id`);

--
-- Indexes for table `ci_class_child`
--
ALTER TABLE `ci_class_child`
  ADD PRIMARY KEY (`class_id`,`child_id`);

--
-- Indexes for table `ci_class_level`
--
ALTER TABLE `ci_class_level`
  ADD PRIMARY KEY (`class_level_id`);

--
-- Indexes for table `ci_class_level_subject`
--
ALTER TABLE `ci_class_level_subject`
  ADD PRIMARY KEY (`class_level_subject_id`);

--
-- Indexes for table `ci_class_school_year`
--
ALTER TABLE `ci_class_school_year`
  ADD PRIMARY KEY (`class_school_year_id`);

--
-- Indexes for table `ci_class_subject_teacher`
--
ALTER TABLE `ci_class_subject_teacher`
  ADD PRIMARY KEY (`class_subject_teacher_id`);

--
-- Indexes for table `ci_comment_c1`
--
ALTER TABLE `ci_comment_c1`
  ADD PRIMARY KEY (`comment_id`);

--
-- Indexes for table `ci_comment_c2`
--
ALTER TABLE `ci_comment_c2`
  ADD PRIMARY KEY (`comment_id`);

--
-- Indexes for table `ci_conduct`
--
ALTER TABLE `ci_conduct`
  ADD PRIMARY KEY (`conduct_id`);

--
-- Indexes for table `ci_diary`
--
ALTER TABLE `ci_diary`
  ADD PRIMARY KEY (`diary_id`);

--
-- Indexes for table `ci_diary_item`
--
ALTER TABLE `ci_diary_item`
  ADD PRIMARY KEY (`diary_id`);

--
-- Indexes for table `ci_diary_template`
--
ALTER TABLE `ci_diary_template`
  ADD PRIMARY KEY (`diary_template_id`);

--
-- Indexes for table `ci_diary_template_item`
--
ALTER TABLE `ci_diary_template_item`
  ADD PRIMARY KEY (`diary_template_id`,`time_of_day`);

--
-- Indexes for table `ci_event`
--
ALTER TABLE `ci_event`
  ADD PRIMARY KEY (`event_id`);

--
-- Indexes for table `ci_event_participate`
--
ALTER TABLE `ci_event_participate`
  ADD PRIMARY KEY (`event_id`,`pp_type`,`pp_id`);

--
-- Indexes for table `ci_fee`
--
ALTER TABLE `ci_fee`
  ADD PRIMARY KEY (`fee_id`);

--
-- Indexes for table `ci_feedback`
--
ALTER TABLE `ci_feedback`
  ADD PRIMARY KEY (`feedback_id`);

--
-- Indexes for table `ci_fee_child`
--
ALTER TABLE `ci_fee_child`
  ADD PRIMARY KEY (`fee_id`,`child_id`);

--
-- Indexes for table `ci_foetus_based_on_week`
--
ALTER TABLE `ci_foetus_based_on_week`
  ADD PRIMARY KEY (`foetus_based_on_week_id`);

--
-- Indexes for table `ci_foetus_info`
--
ALTER TABLE `ci_foetus_info`
  ADD PRIMARY KEY (`foetus_info_id`);

--
-- Indexes for table `ci_foetus_knowledge`
--
ALTER TABLE `ci_foetus_knowledge`
  ADD PRIMARY KEY (`foetus_knowledge_id`);

--
-- Indexes for table `ci_interactive`
--
ALTER TABLE `ci_interactive`
  ADD PRIMARY KEY (`interactivel_id`);

--
-- Indexes for table `ci_interactive_school`
--
ALTER TABLE `ci_interactive_school`
  ADD PRIMARY KEY (`view_type`,`date`,`school_id`);

--
-- Indexes for table `ci_interactive_topic`
--
ALTER TABLE `ci_interactive_topic`
  ADD PRIMARY KEY (`group_id`,`date`);

--
-- Indexes for table `ci_medicine`
--
ALTER TABLE `ci_medicine`
  ADD PRIMARY KEY (`medicine_id`);

--
-- Indexes for table `ci_medicine_detail`
--
ALTER TABLE `ci_medicine_detail`
  ADD PRIMARY KEY (`medicine_id`,`usage_date`,`time_on_day`);

--
-- Indexes for table `ci_menu`
--
ALTER TABLE `ci_menu`
  ADD PRIMARY KEY (`menu_id`);

--
-- Indexes for table `ci_menu_detail`
--
ALTER TABLE `ci_menu_detail`
  ADD PRIMARY KEY (`menu_detail_id`);

--
-- Indexes for table `ci_noga_manage`
--
ALTER TABLE `ci_noga_manage`
  ADD PRIMARY KEY (`noga_manage_id`);

--
-- Indexes for table `ci_noga_notification`
--
ALTER TABLE `ci_noga_notification`
  ADD PRIMARY KEY (`noga_notification_id`);

--
-- Indexes for table `ci_notification`
--
ALTER TABLE `ci_notification`
  ADD PRIMARY KEY (`notification_id`);

--
-- Indexes for table `ci_notifications_setting`
--
ALTER TABLE `ci_notifications_setting`
  ADD PRIMARY KEY (`user_id`,`school_id`);

--
-- Indexes for table `ci_parent_manage`
--
ALTER TABLE `ci_parent_manage`
  ADD PRIMARY KEY (`user_id`,`child_parent_id`,`child_id`);

--
-- Indexes for table `ci_pickup`
--
ALTER TABLE `ci_pickup`
  ADD PRIMARY KEY (`pickup_id`),
  ADD UNIQUE KEY `school_id_pickup_class_id_pickup_time` (`school_id`,`pickup_class_id`,`pickup_time`);

--
-- Indexes for table `ci_pickup_assign`
--
ALTER TABLE `ci_pickup_assign`
  ADD PRIMARY KEY (`pickup_id`,`user_id`,`assign_time`);

--
-- Indexes for table `ci_pickup_child`
--
ALTER TABLE `ci_pickup_child`
  ADD PRIMARY KEY (`pickup_child_id`);

--
-- Indexes for table `ci_pickup_class`
--
ALTER TABLE `ci_pickup_class`
  ADD PRIMARY KEY (`pickup_class_id`);

--
-- Indexes for table `ci_pickup_price_list`
--
ALTER TABLE `ci_pickup_price_list`
  ADD PRIMARY KEY (`unit_price_id`);

--
-- Indexes for table `ci_pickup_register`
--
ALTER TABLE `ci_pickup_register`
  ADD PRIMARY KEY (`pickup_register_id`);

--
-- Indexes for table `ci_pickup_service`
--
ALTER TABLE `ci_pickup_service`
  ADD PRIMARY KEY (`service_id`);

--
-- Indexes for table `ci_pickup_service_register`
--
ALTER TABLE `ci_pickup_service_register`
  ADD PRIMARY KEY (`pickup_register_id`,`child_id`,`service_id`);

--
-- Indexes for table `ci_pickup_service_usage`
--
ALTER TABLE `ci_pickup_service_usage`
  ADD PRIMARY KEY (`service_usage_id`);

--
-- Indexes for table `ci_pickup_teacher`
--
ALTER TABLE `ci_pickup_teacher`
  ADD PRIMARY KEY (`school_id`,`user_id`);

--
-- Indexes for table `ci_pickup_template`
--
ALTER TABLE `ci_pickup_template`
  ADD PRIMARY KEY (`school_id`);

--
-- Indexes for table `ci_point`
--
ALTER TABLE `ci_point`
  ADD PRIMARY KEY (`point_id`);

--
-- Indexes for table `ci_point_km_c2`
--
ALTER TABLE `ci_point_km_c2`
  ADD PRIMARY KEY (`point_id`);

--
-- Indexes for table `ci_region_manage`
--
ALTER TABLE `ci_region_manage`
  ADD PRIMARY KEY (`region_manage_id`);

--
-- Indexes for table `ci_report`
--
ALTER TABLE `ci_report`
  ADD PRIMARY KEY (`report_id`);

--
-- Indexes for table `ci_report_category`
--
ALTER TABLE `ci_report_category`
  ADD PRIMARY KEY (`report_category_id`);

--
-- Indexes for table `ci_report_template`
--
ALTER TABLE `ci_report_template`
  ADD PRIMARY KEY (`report_template_id`);

--
-- Indexes for table `ci_report_template_category`
--
ALTER TABLE `ci_report_template_category`
  ADD PRIMARY KEY (`report_template_category_id`);

--
-- Indexes for table `ci_report_template_detail`
--
ALTER TABLE `ci_report_template_detail`
  ADD PRIMARY KEY (`report_template_category_id`,`report_template_id`);

--
-- Indexes for table `ci_review`
--
ALTER TABLE `ci_review`
  ADD PRIMARY KEY (`review_id`);

--
-- Indexes for table `ci_role`
--
ALTER TABLE `ci_role`
  ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `ci_role_permission`
--
ALTER TABLE `ci_role_permission`
  ADD PRIMARY KEY (`module`,`role_id`);

--
-- Indexes for table `ci_schedule`
--
ALTER TABLE `ci_schedule`
  ADD PRIMARY KEY (`schedule_id`);

--
-- Indexes for table `ci_schedule_detail`
--
ALTER TABLE `ci_schedule_detail`
  ADD PRIMARY KEY (`schedule_detail_id`);

--
-- Indexes for table `ci_school_child`
--
ALTER TABLE `ci_school_child`
  ADD PRIMARY KEY (`school_id`,`child_id`);

--
-- Indexes for table `ci_school_configuration`
--
ALTER TABLE `ci_school_configuration`
  ADD PRIMARY KEY (`school_id`);

--
-- Indexes for table `ci_school_group`
--
ALTER TABLE `ci_school_group`
  ADD PRIMARY KEY (`school_group_id`);

--
-- Indexes for table `ci_school_photos`
--
ALTER TABLE `ci_school_photos`
  ADD PRIMARY KEY (`photo_id`);

--
-- Indexes for table `ci_service`
--
ALTER TABLE `ci_service`
  ADD PRIMARY KEY (`service_id`);

--
-- Indexes for table `ci_service_child`
--
ALTER TABLE `ci_service_child`
  ADD PRIMARY KEY (`service_child_id`);

--
-- Indexes for table `ci_service_deduction`
--
ALTER TABLE `ci_service_deduction`
  ADD PRIMARY KEY (`service_id`,`child_id`,`deduction_date`);

--
-- Indexes for table `ci_service_usage`
--
ALTER TABLE `ci_service_usage`
  ADD PRIMARY KEY (`service_usage_id`);

--
-- Indexes for table `ci_subject`
--
ALTER TABLE `ci_subject`
  ADD PRIMARY KEY (`subject_id`);

--
-- Indexes for table `ci_teacher`
--
ALTER TABLE `ci_teacher`
  ADD PRIMARY KEY (`user_id`,`school_id`);

--
-- Indexes for table `ci_tuition`
--
ALTER TABLE `ci_tuition`
  ADD PRIMARY KEY (`tuition_id`);

--
-- Indexes for table `ci_tuition_4leave`
--
ALTER TABLE `ci_tuition_4leave`
  ADD PRIMARY KEY (`tuition_4leave_id`);

--
-- Indexes for table `ci_tuition_4leave_detail`
--
ALTER TABLE `ci_tuition_4leave_detail`
  ADD PRIMARY KEY (`tuition_4leave_id`,`fee_id`,`type`,`service_id`);

--
-- Indexes for table `ci_tuition_child`
--
ALTER TABLE `ci_tuition_child`
  ADD PRIMARY KEY (`tuition_child_id`);

--
-- Indexes for table `ci_tuition_detail`
--
ALTER TABLE `ci_tuition_detail`
  ADD PRIMARY KEY (`tuition_child_id`,`fee_id`,`type`,`service_id`);

--
-- Indexes for table `ci_users_online`
--
ALTER TABLE `ci_users_online`
  ADD PRIMARY KEY (`user_id`,`date`);

--
-- Indexes for table `ci_user_birthday`
--
ALTER TABLE `ci_user_birthday`
  ADD PRIMARY KEY (`user_birthday_id`);

--
-- Indexes for table `ci_user_manage`
--
ALTER TABLE `ci_user_manage`
  ADD PRIMARY KEY (`user_manage_id`);

--
-- Indexes for table `ci_user_review`
--
ALTER TABLE `ci_user_review`
  ADD PRIMARY KEY (`user_review_id`);

--
-- Indexes for table `ci_user_role`
--
ALTER TABLE `ci_user_role`
  ADD PRIMARY KEY (`user_id`,`role_id`);

--
-- Indexes for table `ci_views_count`
--
ALTER TABLE `ci_views_count`
  ADD PRIMARY KEY (`school_id`,`view_type`);

--
-- Indexes for table `ci_views_count_detail`
--
ALTER TABLE `ci_views_count_detail`
  ADD PRIMARY KEY (`view_type`,`date`,`object_id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id_contact`);

--
-- Indexes for table `conversations`
--
ALTER TABLE `conversations`
  ADD PRIMARY KEY (`conversation_id`);

--
-- Indexes for table `conversations_messages`
--
ALTER TABLE `conversations_messages`
  ADD PRIMARY KEY (`message_id`);

--
-- Indexes for table `conversations_users`
--
ALTER TABLE `conversations_users`
  ADD UNIQUE KEY `conversation_id_user_id` (`conversation_id`,`user_id`);

--
-- Indexes for table `custom_fields`
--
ALTER TABLE `custom_fields`
  ADD PRIMARY KEY (`field_id`);

--
-- Indexes for table `custom_fields_values`
--
ALTER TABLE `custom_fields_values`
  ADD PRIMARY KEY (`value_id`),
  ADD UNIQUE KEY `field_id_node_id_node_type` (`field_id`,`node_id`,`node_type`);

--
-- Indexes for table `emojis`
--
ALTER TABLE `emojis`
  ADD PRIMARY KEY (`emoji_id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`event_id`);

--
-- Indexes for table `events_categories`
--
ALTER TABLE `events_categories`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `events_members`
--
ALTER TABLE `events_members`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `group_id_user_id` (`event_id`,`user_id`);

--
-- Indexes for table `followings`
--
ALTER TABLE `followings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id_following_id` (`user_id`,`following_id`);

--
-- Indexes for table `forums`
--
ALTER TABLE `forums`
  ADD PRIMARY KEY (`forum_id`);

--
-- Indexes for table `forums_replies`
--
ALTER TABLE `forums_replies`
  ADD PRIMARY KEY (`reply_id`);

--
-- Indexes for table `forums_threads`
--
ALTER TABLE `forums_threads`
  ADD PRIMARY KEY (`thread_id`);

--
-- Indexes for table `friends`
--
ALTER TABLE `friends`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_one_id_user_two_id` (`user_one_id`,`user_two_id`);

--
-- Indexes for table `games`
--
ALTER TABLE `games`
  ADD PRIMARY KEY (`game_id`);

--
-- Indexes for table `game_players`
--
ALTER TABLE `game_players`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `game_id_user_id` (`game_id`,`user_id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`group_id`),
  ADD UNIQUE KEY `username` (`group_name`);

--
-- Indexes for table `groups_admins`
--
ALTER TABLE `groups_admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `group_id_user_id` (`group_id`,`user_id`);

--
-- Indexes for table `groups_categories`
--
ALTER TABLE `groups_categories`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `groups_members`
--
ALTER TABLE `groups_members`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `group_id_user_id` (`group_id`,`user_id`);

--
-- Indexes for table `monitor`
--
ALTER TABLE `monitor`
  ADD PRIMARY KEY (`monitor_token`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`to_user_id`,`notification_id`) USING BTREE;

--
-- Indexes for table `packages`
--
ALTER TABLE `packages`
  ADD PRIMARY KEY (`package_id`);

--
-- Indexes for table `packages_payments`
--
ALTER TABLE `packages_payments`
  ADD PRIMARY KEY (`payment_id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`page_id`),
  ADD UNIQUE KEY `username` (`page_name`);

--
-- Indexes for table `pages_admins`
--
ALTER TABLE `pages_admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `page_id_user_id` (`page_id`,`user_id`);

--
-- Indexes for table `pages_categories`
--
ALTER TABLE `pages_categories`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `pages_invites`
--
ALTER TABLE `pages_invites`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `page_id_user_id_from_user_id` (`page_id`,`user_id`,`from_user_id`);

--
-- Indexes for table `pages_likes`
--
ALTER TABLE `pages_likes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `page_id_user_id` (`page_id`,`user_id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`post_id`);

--
-- Indexes for table `posts_articles`
--
ALTER TABLE `posts_articles`
  ADD PRIMARY KEY (`article_id`);

--
-- Indexes for table `posts_audios`
--
ALTER TABLE `posts_audios`
  ADD PRIMARY KEY (`audio_id`);

--
-- Indexes for table `posts_comments`
--
ALTER TABLE `posts_comments`
  ADD PRIMARY KEY (`comment_id`);

--
-- Indexes for table `posts_comments_likes`
--
ALTER TABLE `posts_comments_likes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `comment_id_user_id` (`comment_id`,`user_id`);

--
-- Indexes for table `posts_files`
--
ALTER TABLE `posts_files`
  ADD PRIMARY KEY (`file_id`);

--
-- Indexes for table `posts_hidden`
--
ALTER TABLE `posts_hidden`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `post_id_user_id` (`post_id`,`user_id`);

--
-- Indexes for table `posts_likes`
--
ALTER TABLE `posts_likes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `post_id_user_id` (`post_id`,`user_id`);

--
-- Indexes for table `posts_links`
--
ALTER TABLE `posts_links`
  ADD PRIMARY KEY (`link_id`);

--
-- Indexes for table `posts_media`
--
ALTER TABLE `posts_media`
  ADD PRIMARY KEY (`media_id`);

--
-- Indexes for table `posts_photos`
--
ALTER TABLE `posts_photos`
  ADD PRIMARY KEY (`photo_id`);

--
-- Indexes for table `posts_photos_albums`
--
ALTER TABLE `posts_photos_albums`
  ADD PRIMARY KEY (`album_id`);

--
-- Indexes for table `posts_photos_likes`
--
ALTER TABLE `posts_photos_likes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id_photo_id` (`user_id`,`photo_id`);

--
-- Indexes for table `posts_polls`
--
ALTER TABLE `posts_polls`
  ADD PRIMARY KEY (`poll_id`);

--
-- Indexes for table `posts_polls_options`
--
ALTER TABLE `posts_polls_options`
  ADD PRIMARY KEY (`option_id`);

--
-- Indexes for table `posts_polls_options_users`
--
ALTER TABLE `posts_polls_options_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id_poll_id` (`user_id`,`poll_id`);

--
-- Indexes for table `posts_products`
--
ALTER TABLE `posts_products`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `posts_saved`
--
ALTER TABLE `posts_saved`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `post_id_user_id` (`post_id`,`user_id`);

--
-- Indexes for table `posts_videos`
--
ALTER TABLE `posts_videos`
  ADD PRIMARY KEY (`video_id`);

--
-- Indexes for table `reports`
--
ALTER TABLE `reports`
  ADD PRIMARY KEY (`report_id`);

--
-- Indexes for table `static_pages`
--
ALTER TABLE `static_pages`
  ADD PRIMARY KEY (`page_id`),
  ADD UNIQUE KEY `page_url` (`page_url`);

--
-- Indexes for table `stickers`
--
ALTER TABLE `stickers`
  ADD PRIMARY KEY (`sticker_id`);

--
-- Indexes for table `stories`
--
ALTER TABLE `stories`
  ADD PRIMARY KEY (`story_id`);

--
-- Indexes for table `stories_media`
--
ALTER TABLE `stories_media`
  ADD PRIMARY KEY (`media_id`);

--
-- Indexes for table `system_countries`
--
ALTER TABLE `system_countries`
  ADD PRIMARY KEY (`country_id`);

--
-- Indexes for table `system_languages`
--
ALTER TABLE `system_languages`
  ADD PRIMARY KEY (`language_id`),
  ADD UNIQUE KEY `code` (`code`);

--
-- Indexes for table `system_options`
--
ALTER TABLE `system_options`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `system_themes`
--
ALTER TABLE `system_themes`
  ADD PRIMARY KEY (`theme_id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `username` (`user_name`),
  ADD UNIQUE KEY `facebook_id` (`facebook_id`),
  ADD UNIQUE KEY `twitter_id` (`twitter_id`),
  ADD UNIQUE KEY `google_id` (`google_id`),
  ADD UNIQUE KEY `linkedin_id` (`linkedin_id`),
  ADD UNIQUE KEY `vkontakte_id` (`vkontakte_id`),
  ADD UNIQUE KEY `user_email` (`user_email`),
  ADD UNIQUE KEY `user_phone_signin` (`user_phone_signin`);

--
-- Indexes for table `users_blocks`
--
ALTER TABLE `users_blocks`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id_blocked_id` (`user_id`,`blocked_id`);

--
-- Indexes for table `users_online`
--
ALTER TABLE `users_online`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UserID` (`user_id`);

--
-- Indexes for table `users_searches`
--
ALTER TABLE `users_searches`
  ADD PRIMARY KEY (`log_id`),
  ADD UNIQUE KEY `node_id_node_type` (`node_id`,`node_type`);

--
-- Indexes for table `users_sessions`
--
ALTER TABLE `users_sessions`
  ADD PRIMARY KEY (`session_id`),
  ADD UNIQUE KEY `session_token` (`session_token`),
  ADD KEY `user_ip` (`user_ip`);

--
-- Indexes for table `verification_requests`
--
ALTER TABLE `verification_requests`
  ADD PRIMARY KEY (`request_id`),
  ADD UNIQUE KEY `node_id_node_type` (`node_id`,`node_type`);

--
-- Indexes for table `widgets`
--
ALTER TABLE `widgets`
  ADD PRIMARY KEY (`widget_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ads_campaigns`
--
ALTER TABLE `ads_campaigns`
  MODIFY `campaign_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ads_system`
--
ALTER TABLE `ads_system`
  MODIFY `ads_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ads_users_wallet_transactions`
--
ALTER TABLE `ads_users_wallet_transactions`
  MODIFY `transaction_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `affiliates_payments`
--
ALTER TABLE `affiliates_payments`
  MODIFY `payment_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `announcements`
--
ALTER TABLE `announcements`
  MODIFY `announcement_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `announcements_users`
--
ALTER TABLE `announcements_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `banned_ips`
--
ALTER TABLE `banned_ips`
  MODIFY `ip_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ci_attendance`
--
ALTER TABLE `ci_attendance`
  MODIFY `attendance_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ci_attendance_back`
--
ALTER TABLE `ci_attendance_back`
  MODIFY `attendance_back_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ci_attendance_back_detail`
--
ALTER TABLE `ci_attendance_back_detail`
  MODIFY `attendance_back_detail_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ci_attendance_detail`
--
ALTER TABLE `ci_attendance_detail`
  MODIFY `attendance_detail_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ci_background_email`
--
ALTER TABLE `ci_background_email`
  MODIFY `email_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ci_background_firebase`
--
ALTER TABLE `ci_background_firebase`
  MODIFY `firebase_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Id auto increment ';

--
-- AUTO_INCREMENT for table `ci_background_notifications`
--
ALTER TABLE `ci_background_notifications`
  MODIFY `send_notification_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ci_child`
--
ALTER TABLE `ci_child`
  MODIFY `child_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ci_child_based_on_month`
--
ALTER TABLE `ci_child_based_on_month`
  MODIFY `child_based_on_month_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ci_child_development`
--
ALTER TABLE `ci_child_development`
  MODIFY `child_development_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ci_child_development_child`
--
ALTER TABLE `ci_child_development_child`
  MODIFY `child_development_child_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ci_child_edit_history`
--
ALTER TABLE `ci_child_edit_history`
  MODIFY `ci_child_edit_history_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ci_child_foetus_growth`
--
ALTER TABLE `ci_child_foetus_growth`
  MODIFY `child_foetus_growth_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ci_child_growth`
--
ALTER TABLE `ci_child_growth`
  MODIFY `child_growth_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ci_child_info`
--
ALTER TABLE `ci_child_info`
  MODIFY `child_info_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ci_child_journal`
--
ALTER TABLE `ci_child_journal`
  MODIFY `child_journal_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ci_child_journal_album`
--
ALTER TABLE `ci_child_journal_album`
  MODIFY `child_journal_album_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ci_child_medical`
--
ALTER TABLE `ci_child_medical`
  MODIFY `child_medical_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ci_child_parent`
--
ALTER TABLE `ci_child_parent`
  MODIFY `child_parent_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ci_class_level`
--
ALTER TABLE `ci_class_level`
  MODIFY `class_level_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ci_class_level_subject`
--
ALTER TABLE `ci_class_level_subject`
  MODIFY `class_level_subject_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ci_class_school_year`
--
ALTER TABLE `ci_class_school_year`
  MODIFY `class_school_year_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ci_class_subject_teacher`
--
ALTER TABLE `ci_class_subject_teacher`
  MODIFY `class_subject_teacher_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ci_comment_c1`
--
ALTER TABLE `ci_comment_c1`
  MODIFY `comment_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ci_comment_c2`
--
ALTER TABLE `ci_comment_c2`
  MODIFY `comment_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ci_conduct`
--
ALTER TABLE `ci_conduct`
  MODIFY `conduct_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ci_event`
--
ALTER TABLE `ci_event`
  MODIFY `event_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ci_fee`
--
ALTER TABLE `ci_fee`
  MODIFY `fee_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ci_feedback`
--
ALTER TABLE `ci_feedback`
  MODIFY `feedback_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ci_foetus_based_on_week`
--
ALTER TABLE `ci_foetus_based_on_week`
  MODIFY `foetus_based_on_week_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ci_foetus_info`
--
ALTER TABLE `ci_foetus_info`
  MODIFY `foetus_info_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ci_foetus_knowledge`
--
ALTER TABLE `ci_foetus_knowledge`
  MODIFY `foetus_knowledge_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ci_interactive`
--
ALTER TABLE `ci_interactive`
  MODIFY `interactivel_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ci_medicine`
--
ALTER TABLE `ci_medicine`
  MODIFY `medicine_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ci_menu`
--
ALTER TABLE `ci_menu`
  MODIFY `menu_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ci_menu_detail`
--
ALTER TABLE `ci_menu_detail`
  MODIFY `menu_detail_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ci_noga_manage`
--
ALTER TABLE `ci_noga_manage`
  MODIFY `noga_manage_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ci_noga_notification`
--
ALTER TABLE `ci_noga_notification`
  MODIFY `noga_notification_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ci_notification`
--
ALTER TABLE `ci_notification`
  MODIFY `notification_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ci_pickup`
--
ALTER TABLE `ci_pickup`
  MODIFY `pickup_id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'ID của lần trông muộn';

--
-- AUTO_INCREMENT for table `ci_pickup_child`
--
ALTER TABLE `ci_pickup_child`
  MODIFY `pickup_child_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ci_pickup_class`
--
ALTER TABLE `ci_pickup_class`
  MODIFY `pickup_class_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ci_pickup_price_list`
--
ALTER TABLE `ci_pickup_price_list`
  MODIFY `unit_price_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ci_pickup_register`
--
ALTER TABLE `ci_pickup_register`
  MODIFY `pickup_register_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID';

--
-- AUTO_INCREMENT for table `ci_pickup_service`
--
ALTER TABLE `ci_pickup_service`
  MODIFY `service_id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'ID dịch vụ';

--
-- AUTO_INCREMENT for table `ci_pickup_service_usage`
--
ALTER TABLE `ci_pickup_service_usage`
  MODIFY `service_usage_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID';

--
-- AUTO_INCREMENT for table `ci_point`
--
ALTER TABLE `ci_point`
  MODIFY `point_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ci_point_km_c2`
--
ALTER TABLE `ci_point_km_c2`
  MODIFY `point_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ci_region_manage`
--
ALTER TABLE `ci_region_manage`
  MODIFY `region_manage_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ci_report`
--
ALTER TABLE `ci_report`
  MODIFY `report_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ci_report_category`
--
ALTER TABLE `ci_report_category`
  MODIFY `report_category_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ci_report_template`
--
ALTER TABLE `ci_report_template`
  MODIFY `report_template_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ci_report_template_category`
--
ALTER TABLE `ci_report_template_category`
  MODIFY `report_template_category_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ci_review`
--
ALTER TABLE `ci_review`
  MODIFY `review_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ci_role`
--
ALTER TABLE `ci_role`
  MODIFY `role_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ci_schedule`
--
ALTER TABLE `ci_schedule`
  MODIFY `schedule_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ci_schedule_detail`
--
ALTER TABLE `ci_schedule_detail`
  MODIFY `schedule_detail_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ci_school_group`
--
ALTER TABLE `ci_school_group`
  MODIFY `school_group_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ci_school_photos`
--
ALTER TABLE `ci_school_photos`
  MODIFY `photo_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ci_service`
--
ALTER TABLE `ci_service`
  MODIFY `service_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ci_service_child`
--
ALTER TABLE `ci_service_child`
  MODIFY `service_child_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ci_service_usage`
--
ALTER TABLE `ci_service_usage`
  MODIFY `service_usage_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ci_subject`
--
ALTER TABLE `ci_subject`
  MODIFY `subject_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ci_tuition`
--
ALTER TABLE `ci_tuition`
  MODIFY `tuition_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ci_tuition_4leave`
--
ALTER TABLE `ci_tuition_4leave`
  MODIFY `tuition_4leave_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ci_tuition_child`
--
ALTER TABLE `ci_tuition_child`
  MODIFY `tuition_child_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ci_user_birthday`
--
ALTER TABLE `ci_user_birthday`
  MODIFY `user_birthday_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ci_user_manage`
--
ALTER TABLE `ci_user_manage`
  MODIFY `user_manage_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ci_user_review`
--
ALTER TABLE `ci_user_review`
  MODIFY `user_review_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id_contact` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `conversations`
--
ALTER TABLE `conversations`
  MODIFY `conversation_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `conversations_messages`
--
ALTER TABLE `conversations_messages`
  MODIFY `message_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `custom_fields`
--
ALTER TABLE `custom_fields`
  MODIFY `field_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `custom_fields_values`
--
ALTER TABLE `custom_fields_values`
  MODIFY `value_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `emojis`
--
ALTER TABLE `emojis`
  MODIFY `emoji_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `event_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `events_categories`
--
ALTER TABLE `events_categories`
  MODIFY `category_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `events_members`
--
ALTER TABLE `events_members`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `followings`
--
ALTER TABLE `followings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `forums`
--
ALTER TABLE `forums`
  MODIFY `forum_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `forums_replies`
--
ALTER TABLE `forums_replies`
  MODIFY `reply_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `forums_threads`
--
ALTER TABLE `forums_threads`
  MODIFY `thread_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `friends`
--
ALTER TABLE `friends`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `games`
--
ALTER TABLE `games`
  MODIFY `game_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `game_players`
--
ALTER TABLE `game_players`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `group_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `groups_admins`
--
ALTER TABLE `groups_admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `groups_categories`
--
ALTER TABLE `groups_categories`
  MODIFY `category_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `groups_members`
--
ALTER TABLE `groups_members`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `notification_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `packages`
--
ALTER TABLE `packages`
  MODIFY `package_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `packages_payments`
--
ALTER TABLE `packages_payments`
  MODIFY `payment_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `page_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pages_admins`
--
ALTER TABLE `pages_admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pages_categories`
--
ALTER TABLE `pages_categories`
  MODIFY `category_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pages_invites`
--
ALTER TABLE `pages_invites`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pages_likes`
--
ALTER TABLE `pages_likes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `post_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `posts_articles`
--
ALTER TABLE `posts_articles`
  MODIFY `article_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `posts_audios`
--
ALTER TABLE `posts_audios`
  MODIFY `audio_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `posts_comments`
--
ALTER TABLE `posts_comments`
  MODIFY `comment_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `posts_comments_likes`
--
ALTER TABLE `posts_comments_likes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `posts_files`
--
ALTER TABLE `posts_files`
  MODIFY `file_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `posts_hidden`
--
ALTER TABLE `posts_hidden`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `posts_likes`
--
ALTER TABLE `posts_likes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `posts_links`
--
ALTER TABLE `posts_links`
  MODIFY `link_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `posts_media`
--
ALTER TABLE `posts_media`
  MODIFY `media_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `posts_photos`
--
ALTER TABLE `posts_photos`
  MODIFY `photo_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `posts_photos_albums`
--
ALTER TABLE `posts_photos_albums`
  MODIFY `album_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `posts_photos_likes`
--
ALTER TABLE `posts_photos_likes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `posts_polls`
--
ALTER TABLE `posts_polls`
  MODIFY `poll_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `posts_polls_options`
--
ALTER TABLE `posts_polls_options`
  MODIFY `option_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `posts_polls_options_users`
--
ALTER TABLE `posts_polls_options_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `posts_products`
--
ALTER TABLE `posts_products`
  MODIFY `product_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `posts_saved`
--
ALTER TABLE `posts_saved`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `posts_videos`
--
ALTER TABLE `posts_videos`
  MODIFY `video_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `reports`
--
ALTER TABLE `reports`
  MODIFY `report_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `static_pages`
--
ALTER TABLE `static_pages`
  MODIFY `page_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `stickers`
--
ALTER TABLE `stickers`
  MODIFY `sticker_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `stories`
--
ALTER TABLE `stories`
  MODIFY `story_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `stories_media`
--
ALTER TABLE `stories_media`
  MODIFY `media_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `system_countries`
--
ALTER TABLE `system_countries`
  MODIFY `country_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `system_languages`
--
ALTER TABLE `system_languages`
  MODIFY `language_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `system_options`
--
ALTER TABLE `system_options`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `system_themes`
--
ALTER TABLE `system_themes`
  MODIFY `theme_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users_blocks`
--
ALTER TABLE `users_blocks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users_online`
--
ALTER TABLE `users_online`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users_searches`
--
ALTER TABLE `users_searches`
  MODIFY `log_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users_sessions`
--
ALTER TABLE `users_sessions`
  MODIFY `session_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `verification_requests`
--
ALTER TABLE `verification_requests`
  MODIFY `request_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `widgets`
--
ALTER TABLE `widgets`
  MODIFY `widget_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
