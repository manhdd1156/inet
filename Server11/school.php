<?php
/**
 * Quản lý các chức năng chính của module ConIu
 *
 * @package ConIu v1
 * @author QuanND
 */

// set override_shutdown
$override_shutdown = true;

$bootstrap_calling = 1; //BOOTSTRAP_CALLING_SCHOOL
// fetch bootstrap
require('bootstrap.php');
$bootstrap_calling = BOOTSTRAP_CALLING_OTHER;

// Kiểm tra user login rồi hay chưa
user_access();

// check username
if (is_empty($_GET['username']) || !valid_username($_GET['username'])) {
    _error(404);
}
$status = isset($_SESSION['menu_status']) ? $_SESSION['menu_status'] : 1;
$smarty->assign('status', $status);

include_once(DAO_PATH . 'dao_school.php');
include_once(DAO_PATH . 'dao_class.php');
include_once(DAO_PATH . 'dao_point.php');
include_once(DAO_PATH . 'dao_class_level.php');
include_once(DAO_PATH . 'dao_teacher.php');
include_once(DAO_PATH . 'dao_child.php');
include_once(DAO_PATH . 'dao_subject.php');
include_once(DAO_PATH . 'dao_event.php');
include_once(DAO_PATH . 'dao_medicine.php');
include_once(DAO_PATH . 'dao_tuition.php');
include_once(DAO_PATH . 'dao_attendance.php');
include_once(DAO_PATH . 'dao_service.php');
include_once(DAO_PATH . 'dao_role.php');
include_once(DAO_PATH . 'dao_report.php');
include_once(DAO_PATH . 'dao_birthday.php');
include_once(DAO_PATH . 'dao_parent.php');
include_once(DAO_PATH . 'dao_feedback.php');
include_once(DAO_PATH . 'dao_schedule.php');
include_once(DAO_PATH . 'dao_pickup.php');
include_once(DAO_PATH . 'dao_menu.php');
include_once(DAO_PATH . 'dao_review.php');
include_once(DAO_PATH . 'dao_statistic.php');
include_once(DAO_PATH . 'dao_journal.php');
include_once(DAO_PATH . 'dao_conduct.php');

$schoolDao = new SchoolDAO();
$classDao = new ClassDAO();
$classLevelDao = new ClassLevelDAO();
$teacherDao = new TeacherDAO();
$pointDao = new PointDAO();
$subjectDao = new SubjectDAO();
$childDao = new ChildDAO();
$eventDao = new EventDAO();
$medicineDao = new MedicineDAO();
$tuitionDao = new TuitionDAO();
$attendanceDao = new AttendanceDAO();
$serviceDao = new ServiceDAO();
$roleDao = new RoleDao();
$reportDao = new ReportDao();
$birthdayDao = new BirthdayDao();
$parentDao = new ParentDAO();
$feedbackDao = new FeedbackDAO();
$scheduleDao = new ScheduleDAO();
$pickupDao = new PickupDAO();
$menuDao = new MenuDAO();
$reviewDao = new ReviewDAO();
$statisticDao = new StatisticDAO();
$journalDao = new JournalDAO();
$conductDao = new ConductDAO();

$view = $_GET['view'];
$subView = $_GET['sub_view'];
$canEdit = false; //Để truyền lên giao diện xử lý các button.

//Kiểm tra quyền của user xem có được thực hiện không
if (($view != "") && ($view != "pickupteacher") && ($view != "fees") && ($view != "useservices") && ($view != "classlevels")
    && ($view != "reviews") && ($view != "statistics")) {
    $canEdit = canEdit($_GET['username'], $view);
    //Đối với những view khác 3 view trên thì view=module.
    if (($subView == "") || ($subView == "detail")) {
        //Trường hợp liệt kê hoặc xem chi tiết, thì kiểm tra quyền XEM
        if (!canView($_GET['username'], $view) && $view != "events" && $view != "settings") {
            _error(403);
        }
    } else if (($subView == "add") || ($subView == "edit")) {
        //Đối với trường hợp sửa, thêm mới thì kiểm tra quyền EDIT
        if (!$canEdit) {
            _error(403);
        }
    }
}

/*
1. Khi user login, kiểm tra có phải là GovOfficer không. Lưu isGovOfficer vào session
2. Nếu isGovOfficer = true, list danh sách shortname các trường mà user quản lý. Lưu danh sách đó vào session.
3. trong hàm getSchoolDataByUsername, lôi isGovOfficer và danh sách trường từ session. Nếu isGovOfficer = true và có quản lý trường này thì lấy thông tin trường ko căn cứ theo user hiện tại để file này chạy bình thường.
4. sửa hàm canView và canEdit

*/

//Những trường hợp khác kiểm tra tại case luôn.

//$school = getSchool($_GET['username']);
//Memcache - Lấy thông tin trường bằng username
$school = getSchoolDataByUsername($_GET['username'], SCHOOL_DATA);
//echo "<pre>"; print_r($school); die;
// UPDATE START MANHDD 03/06/2021
//if ($school['grade'] == 2) {
//    $semesters['0'] = 'Cả năm';
//}
//if ($school['grade'] == 2) {
$semesters['0'] = 'Cả năm';
//}
// UPDATE END MANHDD 03/06/2021
if (is_null($school)) {
    _error(403);
}
// echo "<pre>"; print_r($_SESSION); die('xxx');
// lấy danh sách những thông tin cần đếm
if ($school['school_step'] != SCHOOL_STEP_FINISH) {
    $classlevelsStep = getSchoolData($school['page_id'], SCHOOL_CLASS_LEVELS);
    $smarty->assign('classlevelsStep', $classlevelsStep);

    $classesStep = getSchoolData($school['page_id'], SCHOOL_CLASSES);
    $smarty->assign('classesStep', $classesStep);

    $teachersStep = getSchoolData($school['page_id'], SCHOOL_TEACHERS);
    $teachers = array();
    foreach ($teachersStep as $teacher) {
        if ($teacher['user_id'] != $user->_data['user_id']) {
            $teachers[] = $teacher;
        }
    }
    $smarty->assign('teachersStep', $teachers);

    $feesStep = $tuitionDao->getFees($school['page_id']);
    $smarty->assign('feesStep', $feesStep);

    $servicesStep = getSchoolData($school['page_id'], SCHOOL_SERVICES);
    $smarty->assign('servicesStep', $servicesStep);
}

$pickupConfig = getSchoolData($school['page_id'], SCHOOL_LATE_PICKUP);
$is_assigned = $pickupDao->checkPickupPermission($school['page_id']);
$is_configured = is_null($pickupConfig) ? false : true;
$is_teacher = (isset($pickupConfig['teachers']) && in_array($user->_data['user_id'], $pickupConfig['teachers'])) ? true : false;

$smarty->assign('pickup_is_assigned', $is_assigned);
$smarty->assign('pickup_is_configured', $is_configured);
$smarty->assign('pickup_is_teacher', $is_teacher);

// ConIu - Lấy ra danh sách schools, classes, children mà user quản lý
include_once('includes/ajax/ci/dao/dao_child.php');
$childDao = new ChildDAO();
$objects = getRelatedObjects();
// Lấy những trường đang sử dụng inet
$schoolUsing = array();
foreach ($objects['schools'] as $schoolOb) {
    if ($schoolOb['school_status'] == SCHOOL_USING_CONIU) {
        $schoolUsing[] = $schoolOb;
    }
}

$smarty->assign('schoolList', $schoolUsing);

$classUsing = array();
foreach ($objects['classes'] as $classOb) {
    if ($classOb['school_status'] == SCHOOL_USING_CONIU) {
        $classUsing[] = $classOb;
    }
}
$smarty->assign('classList', $classUsing);

$childrenOb = $childDao->getChildrenOfParent($user->_data['user_id']);

$smarty->assign('childrenList', $childrenOb);

// Cập nhật last_active
updateUserLastActive($user->_data['user_id']);

// Cập nhật users online
$today = date("Y-m-d");
updateUserOnline($user->_data['user_id']);
// ConIu - END
switch ($view) {
    case '':
        // chuyển hướng nếu chưa hoàn thành khởi tạo trường
        if ($school['school_step'] != SCHOOL_STEP_FINISH) {
            if ($school['school_step'] == 0 && $view != '' && $view != 'classlevels') {
                redirect('/school/' . $_GET['username']);
            } else if ($school['school_step'] == 1 && $view != 'classes') {
                if (count($classesStep) > 0) {
                    redirect('/school/' . $_GET['username'] . '/classes');
                } else {
                    redirect('/school/' . $_GET['username'] . '/classes/add');
                }
            } else if ($school['school_step'] == 2 && $view != 'teachers') {
                if (count($teachersStep) > 0) {
                    redirect('/school/' . $_GET['username'] . '/teachers');
                } else {
                    redirect('/school/' . $_GET['username'] . '/teachers/add');
                }
            } else if ($school['school_step'] == 7 && $view != 'children') {
                redirect('/school/' . $_GET['username'] . '/children');
            } else if ($school['school_step'] == 4 && $view != 'fees') {
                if (count($feesStep) > 0) {
                    redirect('/school/' . $_GET['username'] . '/fees');
                } else {
                    redirect('/school/' . $_GET['username'] . '/fees/add');
                }
            } else if ($school['school_step'] == 3 && $view != 'services') {
                if (count($servicesStep) > 0) {
                    redirect('/school/' . $_GET['username'] . '/services');
                } else {
                    redirect('/school/' . $_GET['username'] . '/services/add');
                }
            } else if ($school['school_step'] == 5 && $view != 'pickup' && $subView != 'template') {
                redirect('/school/' . $_GET['username'] . '/pickup/template');
            } else if ($school['school_step'] == 6 && $view != 'pickup' && $subView != 'assign') {
                redirect('/school/' . $_GET['username'] . '/pickup/assign');
            }
        }

        // page header
        page_header(__("School Management") . " &rsaquo; " . __("Dashboard"));

        // Tăng số đếm tương tác của trường
        increaseSchoolInteractive($school['page_id'], 'dashboard');
        // Tăng lượt tương tác - TaiLA
        addInteractive($school['page_id'], 'school_dashboard', 'school_view');

        $insights['child_cnt'] = $school['male_count'] + $school['female_count'];
        $today = date($system['date_format']);
        $insights['today'] = $today;

        if (canView($_GET['username'], "medicines")) {
            $count_no_confirm = 0;
            $medicines = $medicineDao->getMedicineOnDate($school['page_id'], $today, $count_no_confirm);
            $insights['medicines'] = $medicines;
        }

        //$events = $eventDao->getEvents($school['page_id'],  $school['event_count_on_daskboard']);
        //$insights['events'] = $events;

        if (canView($_GET['username'], "attendance")) {
            // Danh sách điểm danh toàn trường
            $attendances = $attendanceDao->getAttendanceOfSchoolADay($school['page_id'], $today);
            $insights['attendances'] = $attendances;

            /* ---------- Trẻ nghỉ nhiều liên tục ---------- */
            //$insights['conabs'] = getConsecutiveAbsentChild($school['page_id']);
            /* ---------- END - Trẻ nghỉ nhiều liên tục ---------- */
        }

        /* ---------- LẤY THỐNG KÊ LƯỢT TƯƠNG TÁC  --------- */
        //if (canView($_GET['username'], "statistic")) {
//            $statistics = getSchoolStatistics($school['page_id']);
        $statistics = getStatisticsInteractive($school['page_id']);
        $insights['statistics'] = $statistics;
        //}
        /* ---------- END - LẤY THỐNG KÊ LƯỢT TƯƠNG TÁC --------- */

        /* ---------- LẤY THÔNG TIN ĐÁNH GIÁ TRƯỜNG, GIÁO VIÊN --------- */
        //if (canView($_GET['username'], "review")) {
        $reviews = $reviewDao->getReviewInfo($school['page_id']);
        $insights['reviews'] = $reviews;
        //}
        /* ---------- END - LẤY THÔNG TIN ĐÁNH GIÁ TRƯỜNG, GIÁO VIÊN --------- */

        /* ---------- LẤY THÔNG TIN DỊCH VỤ ĂN UỐNG - ĐỂ TẠM ĐÂY, TỐI ƯU SAU --------- */
        if (canView($_GET['username'], "services")) {
            $food_services = array();
            $foodServiceList = array();

            //$foodServiceList = $serviceDao->getFoodServiceSchool($school['page_id']);
            //Memcache - Lấy ra tất cả dịch vụ và lọc ra dịch vụ ăn uống
            $serviceList = getSchoolData($school['page_id'], SCHOOL_SERVICES);
            foreach ($serviceList as $service) {
                if ($service['is_food'] == 1 && $service['status'] == 1) {
                    $foodServiceList[] = $service;
                }
            }

            $foodServiceList = sortMultiOrderArray($foodServiceList, [['type', 'ASC'], ['service_id'], ['ASC']]);
            //1. Liệt kê danh sách điểm danh toàn trường của ngày HÔM NAY, sort theo lớp
            $presentChildIds = $attendanceDao->getPresentChildrenOfSchoolADay($school['page_id'], $today);
            $classes = $presentChildIds['classes'];
            if ((count($foodServiceList) > 0) && (count($classes) > 0)) {

                //2. Liệt kê danh sách đăng ký dịch vụ ĂN UỐNG theo tháng và theo điểm danh, sort theo lớp
                $notCountBasedServices = $serviceDao->getFoodNotCountBasedServiceSchoolADay($school['page_id'], $presentChildIds['childIds'], $foodServiceList);
                //3. Liệt kê danh sách đăng ký dịch vụ ĂN UỐNG theo lần
                $countBasedServices = $serviceDao->getFoodCountBasedServiceSchoolADay($school['page_id'], $today, $presentChildIds['childIds'], $foodServiceList);

                $totalServices = array();
                foreach ($foodServiceList as $service) {
                    $totalServices["'" . $service['service_id'] . "'"] = 0;
                }
                //4. Ghép tất mỗi loại dịch vụ vào danh sách lớp để hiện ra màn hình.
                $newClasses = array();
                foreach ($classes as $class) {
                    $newClass = $class;
                    $serviceFood = array();
                    foreach ($notCountBasedServices as $notCountBasedService) {
                        if ($class['class_id'] == $notCountBasedService['class_id']) {
                            $newClass['not_countbased_services'] = $notCountBasedService['not_countbased_services'];
                            //Tính tổng đối với từng dịch vụ
                            foreach ($notCountBasedService['not_countbased_services'] as $service) {
                                $totalServices["'" . $service['service_id'] . "'"] = $totalServices["'" . $service['service_id'] . "'"] + $service['total'];
                            }
                            foreach ($newClass['not_countbased_services'] as $value) {
                                $serviceFood[] = $value;
                            }
                            break;
                        }
                    }
                    foreach ($countBasedServices as $countBasedService) {
                        if ($class['class_id'] == $countBasedService['class_id']) {
                            $newClass['countbased_services'] = $countBasedService['countbased_services'];

                            //Tính tổng đối với từng dịch vụ
                            foreach ($countBasedService['countbased_services'] as $service) {
                                $totalServices["'" . $service['service_id'] . "'"] = $totalServices["'" . $service['service_id'] . "'"] + $service['total'];
                            }
                            foreach ($newClass['countbased_services'] as $value) {
                                $serviceFood[] = $value;
                            }
                            break;
                        }
                    }
                    $newClass['food_services'] = $serviceFood;
                    $newClasses[] = $newClass;
                }

                $food_services['classes'] = $newClasses;
                $food_services['service_list'] = $foodServiceList;
                $food_services['service_total'] = $totalServices;
            }
            $insights['food_services'] = $food_services;
        }
        /* ---------- END - LẤY THÔNG TIN DỊCH VỤ ĂN UỐNG - ĐỂ TẠM ĐÂY, TỐI ƯU SAU --------- */

        // Thông tin sinh nhật
        if (canView($_GET['username'], "birthday")) {
            $beginDate = date($system['date_format'], mktime(0, 0, 0, date("m"), 1, date("Y")));
            $today = date('Y-m-d');
            $begin = date("d/m/Y");

            $week = strtotime(date("Y-m-d", strtotime($today)) . " +1 week");
            $end = strftime("%d/%m/%Y", $week);
            $class_id = 0;
            $birthdays = $birthdayDao->getChildBirthday($school['page_id'], $begin, $end, $class_id);
            $insights['birthdays'] = $birthdays;

            // Lấy thông tin sinh nhật giáo viên
            $teacher_birthday = $teacherDao->getTeachersBirthday($school['page_id'], $begin, $end);
            $insights['birthday_teacher'] = $teacher_birthday;
        }

        // assign variables
        $smarty->assign('insights', $insights);

        // Lấy danh sách hiệu trưởng của trường
        $principals = array();
        if (!is_empty($school['principal'])) {
            $principals = $school['principal'];
            $principals = explode(',', $principals);
        }

        $smarty->assign('principals', $principals);

        // Lấy danh sách lớp của trường
        $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
        $smarty->assign('classes', $classes);

        $name = "guide_dashboard_" . $school['page_name'] . "_" . $user->_data['user_id'];
        $cookie = 1;
        if (isset($_COOKIE[$name])) {
            $cookie = $_COOKIE[$name];
        }

        $smarty->assign('cookie', $cookie);

        break;
    case 'settings':
        // page header
        page_header(__("School Management") . " &rsaquo; " . __("Setting and control"));
        // Lấy cấu hình notification
        //$notifySetting = $schoolDao->getNotificationSetting($user->_data['user_id'], $school['page_id']);
        $notifySettings = getSchoolData($school['page_id'], SCHOOL_NOTIFICATIONS);
        $notifySetting = isset($notifySettings[$user->_data['user_id']]) ? $notifySettings[$user->_data['user_id']] : null;

        // Lấy danh sách hiệu trưởng của trường
        $principals = array();
        if (!is_empty($school['principal'])) {
            $principals = $school['principal'];
            $principals = explode(',', $principals);
        }
        // Khởi tạo biến tạm danh sách module cấu hình
        $modules_temp = array();
        // Nếu chưa cấu hình (Không có bản ghi trong db)
        if ($notifySetting == null) {
            foreach ($notifyModules as $k => $module) {
                if ($module['module'] == "tuitions" || $module['module'] == "feedback") {
                    $module['value'] = NOTIFY_ALL;
                } else {
                    $module['value'] = NOTIFY_NO;
                }
                $modules_temp[] = $module;
            }
        } else {
            // Nếu cấu hình rồi (có bản ghi trong db)
            foreach ($notifyModules as $k => $module) {
                $module['value'] = $notifySetting[$module['module']];
                $modules_temp[] = $module;
            }
        }

        // Lấy danh sách quyền của user, nếu user không có quyền thì set 'show' = 0: Không cho hiển thị trên màn hình
        $permissions = array();
        if (($user->_data['user_id'] != $school['page_admin']) && !in_array($user->_data['user_id'], $principals)) {
            // memcache??
            $permissions = $roleDao->getPermissionOfSchool($school['page_id'], $user->_data['user_id']);
        }

        // Khởi tạo biến modules (Danh sách cấu hình thông báo)
        $modules = array();
        // Lặp danh sách biến tạm
        foreach ($modules_temp as $module) {
            // Nếu là tài khoản admin (không có $permission) thì hiển thị tất
            if (empty($permissions)) {
                $module['show'] = 1;
            } else {
                // Nếu tồn tại
                foreach ($permissions as $permission) {
                    if ($module['module'] == $permission['module']) {
                        if ($permission['value'] == PERMISSION_NONE) {
                            $module['show'] = 0;
                        } else {
                            $module['show'] = 1;
                        }
                    }
                }
            }
            $modules[] = $module;
        }
        $smarty->assign('modules', $modules);

        $canEdit = canEdit($_GET['username'], $view);
        $smarty->assign('canEdit', $canEdit);
        break;
    case 'roles':
        switch ($subView) {
            case '':
                // page header
                page_header(__("School Management") . " &rsaquo; " . __('Role'));
                // memcache??
                $rows = $roleDao->getRoleOfSchool($school['page_id']);
                // assign variables
                $smarty->assign('rows', $rows);

                break;

            case 'add':
                page_header(__("School Management") . " &rsaquo; " . __('Role') . " &rsaquo; " . __("Add New"));
                $smarty->assign('modules', $systemModules);
                break;
            case 'edit':
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }
                // memcache??
                $role = $roleDao->getRoleWithPermissions($_GET['id']);
                if (is_null($role)) {
                    _error(404);
                }

                //Build lại danh sách quyền của vai trò để hiển thị lên màn hình.
                $modules = array();
                foreach ($systemModules as $module) {
                    $found = false;
                    $newMod = $module;
                    foreach ($role['permissions'] as $permission) {
                        if ($module['module'] == $permission['module']) {
                            $found = true;
                            $newMod['value'] = $permission['value'];
                            break;
                        }
                    }
                    if (!$found) {
                        $newMod['value'] = PERM_NONE;
                    }
                    $modules[] = $newMod;
                }
                $smarty->assign('data', $role);
                $smarty->assign('modules', $modules);

                page_header(__("School Management") . " &rsaquo; " . __("Role") . " &rsaquo; " . $role['role_name']);
                break;
            case 'assignrole':
                $canEdit = canEdit($_GET['username'], $view);
                if (!$canEdit) {
                    _error(403);
                }
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }
                // memcache??
                $role = $roleDao->getRoleWithUser($_GET['id']);
                if (is_null($role)) {
                    _error(404);
                }
                $smarty->assign('data', $role);
                page_header(__("School Management") . " &rsaquo; " . __("Assign role") . " &rsaquo; " . $role['role_name']);
                break;
            case 'assignprincipal':
                $canEdit = canEdit($_GET['username'], $view);
                if (!$canEdit) {
                    _error(403);
                }
                // Lấy danh sách hiệu trưởng
                $principals = array();
                if (!is_empty($school['principal'])) {
                    $principals = $school['principal'];
                    $principals = explode(',', $principals);
                }

                // Lấy danh sách nhân viên của trường
                $teachers = getSchoolData($school['page_id'], SCHOOL_TEACHERS);
                $fields = array('user_id', 'user_fullname');

                $teacherPrincipal = array();
                // Lặp danh sách giáo viên, kiểm tra xem giáo viên có phải hiệu trưởng không
                foreach ($teachers as $teacher) {
                    $teacher = getArrayFromKeys($teacher, $fields);
                    if (in_array($teacher['user_id'], $principals)) {
                        $teacher['is_principal'] = 1;
                    } else {
                        $teacher['is_principal'] = 0;
                    }
                    $teacherPrincipal[] = $teacher;
                }
                $smarty->assign('data', $teacherPrincipal);
                page_header(__("School Management") . " &rsaquo; " . __("Assign principal"));
                break;

            default:
                _error(404);
                break;
        }
        break;
    case 'attendance':
        // Tăng số đếm tương tác của trường
        increaseSchoolInteractive($school['page_id'], 'attendance');
        // Tăng lượt tương tác - TaiLA
        addInteractive($school['page_id'], 'attendance', 'school_view');

        switch ($subView) {
            case '':
                page_header(__("School Management") . " &rsaquo; " . __("Attendance"));

                $attendanceDate = date($system['date_format']);
                $smarty->assign('attendanceDate', $attendanceDate);

                //Màn hình mặc định ban đầu chỉ lấy ngày hiện tại
                $results = $attendanceDao->getAttendanceOfSchoolADay($school['page_id'], $attendanceDate);
                $smarty->assign('rows', $results);

                break;
            case 'classatt':
                if (!canView($_GET['username'], $view)) {
                    _error(403);
                }
                page_header(__("School Management") . " &rsaquo; " . __("Attendance"));
                //$classes = $classDao->getClassNamesOfSchool($school['page_id']);
                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);

                $smarty->assign('classes', $classes);

                $toDate = date($system['date_format']);
                $fromDate = date($system['date_format'], mktime(0, 0, 0, date("m"), 1, date("Y")));
                $smarty->assign('toDate', $toDate);
                $smarty->assign('fromDate', $fromDate);

                break;
            case 'rollup': //Quản lý trường điểm danh cho các lớp.
                if (!canEdit($_GET['username'], $view)) {
                    _error(403);
                }
                page_header(__("School Management") . " &rsaquo; " . __("Attendance") . " &rsaquo; " . __("Roll up"));
                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);

                $smarty->assign('classes', $classes);

                $classId = 0;
                if (isset($_GET['id']) && is_numeric($_GET['id'])) {
                    $classId = $_GET['id'];
                    $attendanceDate = date($system['date_format']);
                    $attendance = $attendanceDao->getAttendanceDetail($classId, $attendanceDate);
                    $smarty->assign('data', $attendance);
                }
                $smarty->assign('class_id', $classId);
                break;
            case 'detail': //Chi tiết một lần điểm danh của một lớp/ngày
                if (!canView($_GET['username'], $view)) {
                    _error(403);
                }
                $attendance = $attendanceDao->getAttendance($_GET['id'], $school['page_id']);
                $smarty->assign('data', $attendance);
                page_header(__("School Management") . " &rsaquo; " . __("Attendance") . " &rsaquo; " . $attendance['group_title']);

                break;
            case 'child':
                if (!canView($_GET['username'], $view)) {
                    _error(403);
                }
                $data = array();
                $toDate = date($system['date_format']);
                $fromDate = date($system['date_format'], mktime(0, 0, 0, date("m"), 1, date("Y")));
                $data['fromDate'] = $fromDate;
                $data['toDate'] = $toDate;

                //$classes = $classDao->getClassNamesOfSchool($school['page_id']);
                //Memcache - lấy ra danh sách lớp của trường
                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                $data['classes'] = $classes;

                if (is_numeric($_GET['id']) && $_GET['id'] > 0) {
                    //$child = $childDao->getChild($_GET['id']);
                    //Memcache - lấy thông tin của trẻ
                    $child = getChildData($_GET['id'], CHILD_INFO);
                    $data['child'] = $child;
                    $data['class_child'] = getClassData($child['class_id'], CLASS_CHILDREN);
                    $attendances = $attendanceDao->getAttendanceChild($_GET['id'], $fromDate, $toDate);
                    if (count($attendances) > 0) {
                        $data['attendance'] = $attendances;
                    }
                    page_header(__("School Management") . " &rsaquo; " . __("Attendance") . " &rsaquo; " . $child['child_name']);
                } else {
                    page_header(__("School Management") . " &rsaquo; " . __("Attendance") . " &rsaquo; " . __("Child"));
                }

                $smarty->assign('data', $data);

                break;
            case 'conabs':
                if (!canView($_GET['username'], $view)) {
                    _error(403);
                }
                page_header(__("School Management") . " &rsaquo; " . __("Attendance") . " &rsaquo; " . __("Consecutive absent student"));
                $daynum = 3;
                $children = $attendanceDao->getConsecutiveAbsentChild($school['page_id'], $daynum);
                $tmpArray = array();
                while ((count($children) > 0) && (MAX_CONSECUTIVE_ABSENT_DAY >= $daynum)) {
                    $tmpArray[$daynum] = $children;
                    $daynum++;
                    if (MAX_CONSECUTIVE_ABSENT_DAY >= $daynum) {
                        $children = $attendanceDao->getConsecutiveAbsentChild($school['page_id'], $daynum);
                    }
                }
                $newArrays = array();
                while ($daynum > 3) {
                    $daynum--;
                    foreach ($tmpArray[$daynum] as $childOut) {
                        $found = false;
                        foreach ($newArrays as $childIn) {
                            if ($childOut['child_id'] == $childIn['child_id']) {
                                $found = true;
                                break;
                            }
                        }
                        if (!$found) {
                            $newChild = $childOut;
                            $newChild['number_of_absent_day'] = $daynum;
                            $newArrays[] = $newChild;
                        }
                    }
                }

                // lặp danh sách trẻ nghỉ nhiều liên tục lấy lý do nghỉ
                // Lấy ngày điểm danh cuối cùng của trường
                $dates = $attendanceDao->getLastAttendanceDates($school['page_id'], $daynum);
                $toDate = $dates[0];
                $endDate = toSysDate($toDate);

                // Khai báo mảng mới
                $resultsAb = array();
                foreach ($newArrays as $row) {
                    $beginDate = date('Y-m-d', strtotime('- ' . ($row['number_of_absent_day'] + 2) . ' days', strtotime($toDate)));
                    $beginDate = toSysDate($beginDate);
                    $attendances = $attendanceDao->getAttendanceChild($row['child_id'], $beginDate, $endDate);
                    $reason = '';

                    // LẶp mảng thông tin điểm danh của trẻ laayslys do nghỉ (lấy lần cuối cùng có lý do)
                    foreach ($attendances['attendance'] as $value) {
                        if ($value['reason'] != '') {
                            $reason = $value['reason'];
                            break;
                        }
                    }
                    $row['reason'] = $reason;
                    $resultsAb[] = $row;
                }
                $smarty->assign('children', $resultsAb);
                $smarty->assign('daynum', ($daynum));
                break;
            default:
                _error(404);
                break;
        }
        break;
    case 'classlevels':
        $canEdit = canEdit($_GET['username'], 'classlevels');
        switch ($subView) {
            case '':
                // page header
                page_header(__("School Management") . " &rsaquo; " . __("Class level"));

                //$rows = $classLevelDao->getClassLevels($school['page_id'], true);
                //Memcache - Lấy ra các khối của trường
                $rows = getSchoolData($school['page_id'], SCHOOL_CLASS_LEVELS);
                // Lấy ra số lượng môn học được gán cho 1 khối
                error_log('school.php 722 - rows : ' . json_encode($rows));
                $result = array();
                $curDate = Date('Y') . '-' . (Date('Y') + 1);
                foreach ($rows as $row) {
                    $classLevelSubjects = $subjectDao->getSubjectsByClassLevel($school['page_id'], $row['gov_class_level'], $curDate);
                    $row['cntSubject'] = count($classLevelSubjects);
                    $result[] = $row;
                }
                $smarty->assign('rows', $result);
                break;

            case 'edit':
                // valid inputs
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }
                //$data = $classLevelDao->getClassLevel($_GET['id']);
                //Memcache - Lấy thông tin của 1 khối
                $data = getClassLevelData($_GET['id'], CLASS_LEVEL_INFO);
                if (is_null($data)) {
                    _error(404);
                }

                $smarty->assign('data', $data);
                // page header
                page_header(__("School Management") . " &rsaquo; " . __("Class level") . " &rsaquo; " . $data['class_level_name']);
                break;

            case 'add':
                // page header
                page_header(__("School Management") . " &rsaquo; " . __("Class level") . " &rsaquo; " . __("Add New"));
                break;

            default:
                _error(404);
                break;
        }
        break;
    case 'birthdays':
        // Tăng số đếm tương tác của trường
        increaseSchoolInteractive($school['page_id'], 'birthday');
        // Tăng lượt tương tác - TaiLA
        addInteractive($school['page_id'], 'birthday', 'school_view');

        //check permission
        // get content
        switch ($subView) {
            case '':
                // page header
                page_header(__("School Management") . " &rsaquo; " . __("Birthday"));
//                $rows = $birthdayDao->getChildBirthday($school['page_id'], 31, 0);
//                $smarty->assign('rows', $rows);
                //$classes = $classDao->getClassNamesOfSchool($school['page_id']);
                // Memcache - Lấy ra các lớp của 1 trường
                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                $smarty->assign('classes', $classes);
                break;

            case 'parent':
                if (!canView($_GET['username'], $view)) {
                    _error(403);
                }
                // valid inputs
//                $beginDate = date($system['date_format'], mktime(0, 0, 0, date("m"), 1, date("Y")));
//                $smarty->assign('begin', $beginDate);
//                $rows = $birthdayDao->getParentBirthdayInTheMonth($school['page_id']);
//
//                $smarty->assign('rows', $rows);
                //$classes = $classDao->getClassNamesOfSchool($school['page_id']);
                // Memcache - Lấy ra các lớp của 1 trường
                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                $smarty->assign('classes', $classes);
                // page header
                page_header(__("School Management") . " &rsaquo; " . __("Birthday") . " &rsaquo; " . __("Parent"));
                break;

            case 'teacher':
                if (!canView($_GET['username'], $view)) {
                    _error(403);
                }
//                $beginDate = date($system['date_format'], mktime(0, 0, 0, date("m"), 1, date("Y")));
//                $smarty->assign('begin', $beginDate);
//                // page header
//                $rows = $birthdayDao->getTeacherBirthdayInTheMonth($school['page_id']);
//
//                $smarty->assign('rows', $rows);
                //$classes = $classDao->getClassNamesOfSchool($school['page_id']);
                // Memcache - Lấy ra các lớp của 1 trường
                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                $smarty->assign('classes', $classes);
                page_header(__("School Management") . " &rsaquo; " . __("Birthday") . " &rsaquo; " . __("Teacher"));
                break;
            default:
                _error(404);
                break;
        }
        break;
    case 'classes':
        //check permission
        // get content
        switch ($subView) {
            case '':
                // page header
                page_header(__("School Management") . " &rsaquo; " . __("Class"));

                //$rows = $classDao->getClassesOfSchool($school['page_id'], true);
                $rows = array();
                // Memcache - Lấy ra các lớp (bao gồm gv lớp) của 1 trường
                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                foreach ($classes as $class) {
                    $class['teachers'] = getClassData($class['group_id'], CLASS_TEACHERS);
                    $rows[] = $class;
                }

                // assign variables
                $smarty->assign('rows', $rows);
                break;

            case 'edit':
                // valid inputs
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }

                //$data = $classDao->getClass($_GET['id']);
                // Memcache - Lấy ra thông tin của 1 lớp
                $data = getClassData($_GET['id'], CLASS_INFO);
                if (is_null($data)) {
                    _error(404);
                }
                // assign variables
                $smarty->assign('data', $data);

                //$teachers = $teacherDao->getTeacherOfClass($_GET['id']);
                // Memcache - Lấy ra giáo viên của 1 lớp
                $teachers = getClassData($_GET['id'], CLASS_TEACHERS);
                $smarty->assign('teachers', $teachers);

                //$class_levels = $classLevelDao->getClassLevels($school['page_id']);
                //Memcache - Lấy ra các khối của 1 trường
                $class_levels = getSchoolData($school['page_id'], SCHOOL_CLASS_LEVELS);
                $smarty->assign('class_levels', $class_levels);

                // page header
                page_header(__("School Management") . " &rsaquo; " . __("Class") . " &rsaquo; " . $data['group_title']);
                break;

            case 'add':
                //$class_levels = $classLevelDao->getClassLevels($school['page_id']);
                //Memcache - Lấy ra các khối của 1 trường
                $class_levels = getSchoolData($school['page_id'], SCHOOL_CLASS_LEVELS);
                $smarty->assign('class_levels', $class_levels);
                // page header
                page_header(__("School Management") . " &rsaquo; " . __("Class") . " &rsaquo; " . __("Add New"));
                break;
            case 'classup':
                // page header
                page_header(__("School Management") . " &rsaquo; " . __("Class up"));

                // Memcache - Lấy ra các lớp (bao gồm gv lớp) của 1 trường
                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);

                // assign variables
                $smarty->assign('classes', $classes);
                //Memcache - Lấy ra các khối của 1 trường
                $class_levels = getSchoolData($school['page_id'], SCHOOL_CLASS_LEVELS);
                $smarty->assign('class_levels', $class_levels);
                break;
            case 'graduate':
                // page header
                page_header(__("School Management") . " &rsaquo; " . __("Graduate"));
                // valid inputs
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }

                //$data = $classDao->getClass($_GET['id']);
                // Memcache - Lấy ra thông tin của 1 lớp
                $data = getClassData($_GET['id'], ALL);
                if (is_null($data)) {
                    _error(404);
                }
                // assign variables
                $results = array();
                if (count($data['children']) > 0) {
                    foreach ($data['children'] as $child) {
                        $pads = array();
                        $childPad = $childDao->getChild4Leave($child['child_id']);
                        if (is_null($child)) {
                            _error(404);
                        }
                        $dataPad = $tuitionDao->getTuition4Leave($school['page_id'], $child['child_id'], $childPad['end_at']);
                        $pads['child'] = $childPad;
                        $pads['data'] = $dataPad;
                        $results[] = $pads;
                    }
                }
//                                echo "<pre>";
//                print_r($results);
//                echo "</pre>";
//                die("qqq");
                $smarty->assign('data', $data['info']);
                $smarty->assign('results', $results);
                break;
//            case 'graduates':
//                // page header
//                page_header(__("School Management")." &rsaquo; ".__("Graduate"));
//
//                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
//                $smarty->assign('classes', $classes);
//                break;
            default:
                _error(404);
                break;
        }
        break;
    case 'subjects':
        //check permission
        // get content
        switch ($subView) {
            case '':
                // page header
                page_header(__("School Management") . " &rsaquo; " . __("Subject"));

                //$rows = $classDao->getClassesOfSchool($school['page_id'], true);
                $rows = array();
                // Memcache - Lấy ra các môn của hệ thống
//                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                $subjects = $subjectDao->getAllSubjects();
                foreach ($subjects as $subject) {
                    // lấy danh sách khối đã được gán môn học
                    $curDate = date('Y') . '-' . (date('Y') + 1);
                    $class_levels = $classLevelDao->getClassLevelsBySubjectId($subject['subject_id'], $school['page_id'], $curDate);

                    foreach ($class_levels as $class_level) {
                        $class_level['class_level_id'] = $class_levels['class_level_id'];
                        $class_level['class_level_name'] = $class_levels['class_level_name'];
                    }
                    $subject['class_levels'] = $class_levels;
                    $rows[] = $subject;
                }

                // assign variables
                $smarty->assign('rows', $rows);
                break;

            case 'edit':
                // valid inputs
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }
                // lấy tên môn học
                $subject_detail = $subjectDao->getSubjectById($_GET['id']);
                $curDate = date('Y') . '-' . (date('Y') + 1);
                error_log('school.php - 975 :$_GET[\'id\'] :' . $_GET['id'] . ';page_id :' . $school['page_id']);
                // Lấy ra các khối được gắn môn học này
                $class_levels_assigned = $classLevelDao->getClassLevelsBySubjectId($_GET['id'], $school['page_id'], $curDate);
                error_log('school.php - 978 : $class_levels_assigned :' . json_encode($class_levels_assigned));

                if (is_null($subject_detail)) {
                    _error(404);
                }
                // assign variables
                $smarty->assign('data', $subject_detail);


                $smarty->assign('class_levels_assigned', $class_levels_assigned);

                //Memcache - Lấy ra các khối của 1 trường
                $class_levels = getSchoolData($school['page_id'], SCHOOL_CLASS_LEVELS);
                error_log('school.php - 979 : $class_levels :' . json_encode($class_levels));
                $smarty->assign('class_levels', $class_levels);
                $smarty->assign('school_id', $school['page_id']);

                // page header
                page_header(__("School Management") . " &rsaquo; " . __("Subject") . " &rsaquo; " . $data['group_title']);
                break;

            case 'add':
                //$class_levels = $classLevelDao->getClassLevels($school['page_id']);
                //Memcache - Lấy ra các khối của 1 trường
                $class_levels = getSchoolData($school['page_id'], SCHOOL_CLASS_LEVELS);
                $smarty->assign('class_levels', $class_levels);
                // page header
                page_header(__("School Management") . " &rsaquo; " . __("Class") . " &rsaquo; " . __("Add New"));
                break;
            case 'classup':
                // page header
                page_header(__("School Management") . " &rsaquo; " . __("Class up"));

                // Memcache - Lấy ra các lớp (bao gồm gv lớp) của 1 trường
                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);

                // assign variables
                $smarty->assign('classes', $classes);
                //Memcache - Lấy ra các khối của 1 trường
                $class_levels = getSchoolData($school['page_id'], SCHOOL_CLASS_LEVELS);
                $smarty->assign('class_levels', $class_levels);
                break;
            case 'graduate':
                // page header
                page_header(__("School Management") . " &rsaquo; " . __("Graduate"));
                // valid inputs
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }

                //$data = $classDao->getClass($_GET['id']);
                // Memcache - Lấy ra thông tin của 1 lớp
                $data = getClassData($_GET['id'], ALL);
                if (is_null($data)) {
                    _error(404);
                }
                // assign variables
                $results = array();
                if (count($data['children']) > 0) {
                    foreach ($data['children'] as $child) {
                        $pads = array();
                        $childPad = $childDao->getChild4Leave($child['child_id']);
                        if (is_null($child)) {
                            _error(404);
                        }
                        $dataPad = $tuitionDao->getTuition4Leave($school['page_id'], $child['child_id'], $childPad['end_at']);
                        $pads['child'] = $childPad;
                        $pads['data'] = $dataPad;
                        $results[] = $pads;
                    }
                }
//                                echo "<pre>";
//                print_r($results);
//                echo "</pre>";
//                die("qqq");
                $smarty->assign('data', $data['info']);
                $smarty->assign('results', $results);
                break;
//            case 'graduates':
//                // page header
//                page_header(__("School Management")." &rsaquo; ".__("Graduate"));
//
//                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
//                $smarty->assign('classes', $classes);
//                break;
            default:
                _error(404);
                break;
        }
        break;
    case 'teachers':
        switch ($subView) {
            case '':
                // page header
                page_header(__("School Management") . " &rsaquo; " . __("Teacher"));
                //$rows = $teacherDao->getAllTeachers($school['page_id']);

                $rows = array();
                //Memcache - Lấy ra các giáo viên(bao gồm lớp đc phân công) của 1 trường
                $teachers = getSchoolData($school['page_id'], SCHOOL_TEACHERS);

                foreach ($teachers as $teacher) {
                    $classes = getTeacherData($teacher['user_id'], TEACHER_CLASSES);
                    foreach ($classes as $class) {
                        if ($class['school_id'] == $school['page_id']) {
                            $teacher['classes'][] = $class;
                        }
                    }
                    $rows[] = $teacher;
                }

                // assign variables
                $smarty->assign('rows', $rows);
                break;

            case 'addexisting':
                $canEdit = canEdit($_GET['username'], $view);
                if (!$canEdit) {
                    _error(403);
                }
                page_header(__("School Management") . " &rsaquo; " . __("Teacher") . " &rsaquo; " . __("Add Existing Account"));

                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                $smarty->assign('classes', $classes);

                $classLevels = getSchoolData($school['page_id'], SCHOOL_CLASS_LEVELS);
                $smarty->assign('classLevels', $classLevels);
                break;

            case 'add':
                page_header(__("School Management") . " &rsaquo; " . __("Teacher") . " &rsaquo; " . __("Add New"));
                $roles = $roleDao->getRoleOfSchool($school['page_id']);
                $smarty->assign('roles', $roles);

                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                $smarty->assign('classes', $classes);

                $classLevels = getSchoolData($school['page_id'], SCHOOL_CLASS_LEVELS);
                $smarty->assign('classLevels', $classLevels);
                break;
            case 'edit':

                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }
                //$teacher = $teacherDao->getTeacher($_GET['id']);
                //Memcache - Lấy ra thông tin của 1 giáo viên
                $teacher = getTeacherData($_GET['id'], TEACHER_INFO);

                if (is_null($teacher)) {
                    _error(404);
                }
                $roles = $roleDao->getRoleOfUser($school['page_id'], $_GET['id']);
                $smarty->assign('data', $teacher);
                $smarty->assign('roles', $roles);
                page_header(__("School Management") . " &rsaquo; " . __("Teacher") . " &rsaquo; " . $teacher['user_fullname']);
                break;
            case 'assign':
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }

                $roles = $roleDao->getRoleOfUser($school['page_id'], $_GET['id']);
                $classesTeacher = getTeacherData($_GET['id'], TEACHER_CLASSES);

                $classesId = array_keys($classesTeacher);
                $data = array();
                // Lấy danh sách lớp của trường
                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                foreach ($classes as $class) {
                    if (in_array($class['group_id'], $classesId)) {
                        $class['normal_teacher'] = 1;
                        // Kiểm tra xem có phải giáo viên chủ nhiệm không
                        $homeroom_teacher = $teacherDao->getHomeroomTeacher($class['group_id'], $_GET['id']);
                        if (isset($homeroom_teacher)) {
//                            $class['homeroom_teacher_id'] = $homeroom_teacher['user_manage_id'];
                            $class['homeroom_teacher'] = 1;
                        }
                    } else {
                        $class['normal_teacher'] = 0;
                    }
                    $data[] = $class;
                }
                $teacherId = $_GET['id'];
                $smarty->assign('roles', $roles);
                $smarty->assign('classes', $data);
                $smarty->assign('teacherId', $teacherId);
                page_header(__("School Management") . " &rsaquo; " . __("Teacher") . " &rsaquo; " . $teacher['user_fullname']);
                break;

            default:
                _error(404);
                break;
        }
        break;
    case 'children':
        switch ($subView) {
            case '':
                // page header
                page_header(__("School Management") . " &rsaquo; " . __("Children"));

                //$classes = $classDao->getClassNamesOfSchool($school['page_id']);
                //Memcache - Lấy ra các lớp của 1 trường
                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                $classes = sortArray($classes, "ASC", "class_level_id");
                $smarty->assign('classes', $classes);

                //Lấy điều kiện search trước đó ra khỏi session
                $condition = $_SESSION[SESSION_KEY_SEARCH_CHILDREN];
                if ($school['page_id'] == $condition['school_id']) {
                    $keyword = isset($condition) ? $condition['keyword'] : '';
                    $classId = isset($condition) ? $condition['class_id'] : '';
                    $page = isset($condition) ? $condition['page'] : 1;
                } else {
                    $keyword = '';
                    $classId = '';
                    $page = 1;
                }

                $result = array();
                $totalChildren = $childDao->searchCount($keyword, $school['page_id'], $classId, STATUS_ACTIVE);
                $children = $childDao->searchPaging($keyword, $school['page_id'], $classId, $page, PAGING_LIMIT, STATUS_ACTIVE);
                $result['total'] = $totalChildren;
                $result['page_count'] = ceil(($totalChildren + 0.0) / PAGING_LIMIT);
                $result['children'] = $children;
                $result['page'] = $page;
                $result['class_id'] = $classId;
                $result['keyword'] = $keyword;

                $smarty->assign('result', $result);
                $smarty->assign('ages', $ages);
                break;

            case 'import':
                $canEdit = canEdit($_GET['username'], $view);
                if (!$canEdit) {
                    _error(403);
                }
                page_header(__("School Management") . " &rsaquo; " . __("Import from Excel file"));
                //$classes = $classDao->getClassNamesOfSchool($school['page_id']);
                //Memcache - Lấy ra các lớp của 1 trường
                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                $smarty->assign('classes', $classes);

                break;
            case 'edit':
                // valid inputs
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }

                //$child = $childDao->getChild($_GET['id']);
                //Memcache - Lấy ra thông tin của 1 trẻ
                $child = getChildData($_GET['id'], CHILD_INFO);
                if (is_null($child)) {
                    _error(404);
                }

                // assign variables
                $smarty->assign('child', $child);
                $smarty->assign('child_admin', $child['child_admin']);
                //$classes = $classDao->getClassNamesOfSchool($school['page_id']);
                //Memcache - Lấy ra các lớp của 1 trường
                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                $smarty->assign('classes', $classes);

                //include_once(DAO_PATH.'dao_parent.php');
                //$parentDao = new ParentDAO();
                //$parent = $parentDao->getParent($_GET['id']);
                $parent = getChildData($_GET['id'], CHILD_PARENTS);
                $smarty->assign('parent', $parent);

                page_header(__("School Management") . " &rsaquo; " . __("Children") . " &rsaquo; " . $child['child_name']);
                break;
            case 'confirm':
                // valid inputs
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }

                //$child = $childDao->getChild($_GET['id']);
                //Memcache - Lấy ra thông tin của 1 trẻ
                $child = getChildData($_GET['id'], CHILD_INFO);
                if (is_null($child)) {
                    _error(404);
                }
                // lấy thông tin trẻ edit
                $child_edit = $childDao->getChildEditOfTeacher($_GET['id'], $school['page_id']);

                // assign variables
                $smarty->assign('child', $child);
                $smarty->assign('child_edit', $child_edit);
                //$classes = $classDao->getClassNamesOfSchool($school['page_id']);
                //Memcache - Lấy ra các lớp của 1 trường
                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                $smarty->assign('classes', $classes);

                page_header(__("School Management") . " &rsaquo; " . __("Children") . " &rsaquo; " . $child['child_name']);
                break;
            case 'moveclass':
                $canEdit = canEdit($_GET['username'], $view);
                if (!$canEdit) {
                    _error(403);
                }
                // valid inputs
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }

                //$child = $childDao->getChild($_GET['id']);
                //Memcache - Lấy ra thông tin của 1 trẻ
                $child = getChildData($_GET['id'], CHILD_INFO);
                if (is_null($child)) {
                    _error(404);
                }

                // assign variables
                $smarty->assign('child', $child);
                //$classes = $classDao->getClassNamesOfSchool($school['page_id']);
                //Memcache - Lấy ra các lớp của 1 trường
                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                $smarty->assign('classes', $classes);

                page_header(__("School Management") . " &rsaquo; " . __("Children") . " &rsaquo; " . __("Move class"));
                break;
            case 'movesclass':
                $canEdit = canEdit($_GET['username'], $view);
                if (!$canEdit) {
                    _error(403);
                }
                //$classes = $classDao->getClassNamesOfSchool($school['page_id']);
                //Memcache - Lấy ra các lớp của 1 trường
                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                $smarty->assign('classes', $classes);

                page_header(__("School Management") . " &rsaquo; " . __("Children") . " &rsaquo; " . __("Move class"));
                break;

            case 'leave':
                $canEdit = canEdit($_GET['username'], $view);
                if (!$canEdit) {
                    _error(403);
                }
                // valid inputs
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }

                $child = $childDao->getChild4Leave($_GET['id']);
                if (is_null($child)) {
                    _error(404);
                }
                $data = $tuitionDao->getTuition4Leave($school['page_id'], $_GET['id'], $child['end_at']);
                $smarty->assign('data', $data);
                $smarty->assign('child', $child);
                page_header(__("School Management") . " &rsaquo; " . __("Children") . " &rsaquo; " . __("Leave"));

                break;
            case 'editleave':
                $canEdit = canEdit($_GET['username'], $view);
                if (!$canEdit) {
                    _error(403);
                }
                // valid inputs
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }

                $child = $childDao->getChild4Leave($_GET['id']);
                if (is_null($child)) {
                    _error(404);
                }
                $data = $tuitionDao->getLeaveTuitionDetail($school['page_id'], $_GET['id']);
                $smarty->assign('data', $data);
                $smarty->assign('child', $child);
                page_header(__("School Management") . " &rsaquo; " . __("Children") . " &rsaquo; " . __("Leave") . " &rsaquo; " . __("Edit"));

                break;
            case 'leaveschool':
                //$classes = $classDao->getClassNamesOfSchool($school['page_id']);
                //Memcache - Lấy ra các lớp của 1 trường
                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                $smarty->assign('classes', $classes);

                //Lấy điều kiện search trước đó ra khỏi session
                $condition = $_SESSION[SESSION_KEY_SEARCH_CHILDREN_LEAVE];
                if ($school['page_id'] == $condition['school_id']) {
                    $page = isset($condition) ? $condition['page'] : 1;
                    $keyword = isset($condition) ? $condition['keyword'] : '';
                    $classId = isset($condition) ? $condition['class_id'] : '';
                } else {
                    $page = 1;
                    $keyword = '';
                    $classId = '';
                }

                $result = array();
                $totalChildren = $childDao->searchCount($keyword, $school['page_id'], $classId, STATUS_INACTIVE);
                //$totalChildren = getSchoolData($school['page_id'], SCHOOL_CHILDREN);

                $result['total'] = $totalChildren;
                $result['page_count'] = ceil(($totalChildren + 0.0) / PAGING_LIMIT);
                $children = $childDao->searchPaging($keyword, $school['page_id'], $classId, $page, PAGING_LIMIT, STATUS_INACTIVE);
                // Lặp danh sách trẻ, kiểm tra xem trẻ đã có thông tin thanh toán học phí thôi học hay chưa
                $childs = array(); // Biến trung gian, đặt tạm tên như vậy (sai tiếng anh)
                foreach ($children as $child) {
                    $data = $tuitionDao->getLeaveTuitionDetail($school['page_id'], $child['child_id']);
                    if (empty($data)) {
                        $child['is_leave_fee'] = 0;
                    } else {
                        $child['is_leave_fee'] = 1;
                    }
                    $childs[] = $child;
                }
                $result['children'] = $childs;
                $result['page'] = $page;
                $smarty->assign('result', $result);
                $smarty->assign('condition', $condition);

                page_header(__("School Management") . " &rsaquo; " . __("Children") . " &rsaquo; " . __("Leave"));
                break;

            case 'add':
                page_header(__("School Management") . " &rsaquo; " . __("Children") . " &rsaquo; " . __("Add New"));
                //$classes = $classDao->getClassNamesOfSchool($school['page_id']);
                //Memcache - Lấy ra các lớp của 1 trường
                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                $services = getSchoolData($school['page_id'], SCHOOL_SERVICES);
                $smarty->assign('classes', $classes);
                $smarty->assign('services', $services);

                break;

            case 'addexisting':
                page_header(__("School Management") . " &rsaquo; " . __("Children") . " &rsaquo; " . __("Add child "));
                //$classes = $classDao->getClassNamesOfSchool($school['page_id']);
                //Memcache - Lấy ra các lớp của 1 trường
                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                $smarty->assign('classes', $classes);

                break;
            case 'detail':
                addInteractive($school['page_id'], 'picker', 'school_view');
                // valid inputs
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }

                //$child = $childDao->getChild($_GET['id']);
                //Memcache - Lấy ra thông tin của 1 trẻ
                $child = getChildData($_GET['id'], CHILD_INFO);
                if (is_null($child)) {
                    _error(404);
                }

                // assign variables
                $smarty->assign('child', $child);

                //$parent = $parentDao->getParent($_GET['id']);
                //Memcache
                $parent = getChildData($_GET['id'], CHILD_PARENTS);
                $smarty->assign('parent', $parent);

                //$data = $childDao->getChildInformation($_GET['id']);
                //Memcache - Lấy ra thôn tin người đón của trẻ
                $data = getChildData($_GET['id'], CHILD_PICKER_INFO);
                $smarty->assign('data', $data);

                page_header(__("School Management") . " &rsaquo; " . __("Children") . " &rsaquo; " . $child['child_name']);
                break;
            case 'informations':
                if (!canView($_GET['username'], $view)) {
                    _error(403);
                }
                page_header(__("School Management") . " &rsaquo; " . __("Children") . " &rsaquo; " . __("Student information"));
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }
                //$data = $childDao->getChildInformation($_GET['id']);
                //Memcache - Lấy ra thôn tin người đón của trẻ
                $data = getChildData($_GET['id'], CHILD_PICKER_INFO);
                $smarty->assign('data', $data);

                break;
            case 'listchildedit':
                $canEdit = canEdit($_GET['username'], $view);
                if (!$canEdit) {
                    _error(403);
                }
                page_header(__("School Management") . " &rsaquo; " . __("Children") . " &rsaquo; " . __("The student list was edited by the teacher"));

                //Memcache - Lấy ra các lớp của 1 trường
                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                $smarty->assign('classes', $classes);

                // Lấy toàn bộ danh sách trẻ đã được sửa của trường (tất cả nhưng là trong 1 tháng đấy :)! Hì)
                $results = array();
                $results = $childDao->getChildEditHistory($school['page_id']);

                $smarty->assign('results', $results);
                break;
            case 'listchildnew':
                $canEdit = canEdit($_GET['username'], $view);
                if (!$canEdit) {
                    _error(403);
                }
                page_header(__("School Management") . " &rsaquo; " . __("Children") . " &rsaquo; " . __("New student study begin in month list"));

                //Memcache - Lấy ra các lớp của 1 trường
                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                $smarty->assign('classes', $classes);

                // Lấy toàn bộ danh sách trẻ đã được sửa của trường (tất cả nhưng là trong 1 tháng đấy :)! Hì)
                $today = date('Y-m-d');
                $dateNow = toSysDate($today);

                $dayMonthPre = date('Y-m-d', strtotime('- 1 month', strtotime($today)));
                $dayMonthPre = toSysDate($dayMonthPre);
                $results = array();
                $results = $childDao->getChildAddNewInTheMonth($school['page_id'], $dayMonthPre, $dateNow);

                $smarty->assign('results', $results);
                break;
            case 'addhealthindex':
                // page header
                page_header(__("School Management") . " &rsaquo; " . __("Add health index"));

                //$classes = $classDao->getClassNamesOfSchool($school['page_id']);
                //Memcache - Lấy ra các lớp của 1 trường
                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                $smarty->assign('classes', $classes);
                break;
            case 'health':
                // page header
                page_header(__("School Management") . " &rsaquo; " . __("Children") . " &rsaquo; " . __("Health information"));

                // valid inputs
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }

                //$child = $childDao->getChild($_GET['id']);
                $child = getChildData($_GET['id'], CHILD_INFO);
                if (is_null($child)) {
                    _error(404);
                }

                // Lấy ngày hiện tại
                $dateNow = date('Y-m-d');
                $dateNowSys = toSysDate($dateNow);

                // Lấy ngày sinh nhật của trẻ
                $birthday = toDBDate($child['birthday']);

                $begin = $child['birthday'];
                $end = $dateNowSys;
                // Khởi tạo mảng lưu chỉ số sức khỏe của trẻ
                $childGrowth = array();

                // Lấy dữ liệu từ ngày sinh đến ngày hiện tại
                // Lấy dữ liệu nhập và0 của trẻ
                $childGrowth = $childDao->getChildGrowthSearchForChartBeginEnd($child['child_parent_id'], $begin, $end);

                // Lấy ngày bắt đầu và ngày kết thúc
//                $begin = toDBDate($begin);
//                $end = toDBDate($end);
//
//                $end_day = (strtotime($end) - strtotime($birthday)) / (60*60*24);
//                $end_day = (int)$end_day;
//
//                $begin_day = (strtotime($begin) - strtotime($birthday)) / (60*60*24);
//                $begin_day = (int)$begin_day;

                // Tính xem trẻ được bao nhiêu ngày tuổi
                $day_age_now = (strtotime($dateNow) - strtotime($birthday)) / (60 * 60 * 24);

                // đoạn này xử lý để lấy ngày bắt đầu (đặt tạm a, b, c; bí tên quá :) )
                $a = $day_age_now % 30;
                $b = $day_age_now - $a;
                $c = $b - 30 * 10;
                $dayGet = (int)$c;
                $end_day = $day_age_now + 65;
                if ($day_age_now >= 300) {
                    $begin_day = $dayGet;
                } else {
                    $begin_day = 0;
                }
                if ($begin_day < 0) {
                    throw new Exception(__("You must select day begin after birthday"));
                }

                // Dữ liệu người dùng nhập vào vẽ biểu đồ chiều cao (thư viện charts.js)
                $heightForChartChartJS = getDataJsonForChartChartJS($childGrowth, $begin_day, $end_day, $birthday, 'height');

                // Lấy dữ liệu người dùng nhập vào vẽ biểu đồ cân nặng (charts.js)
                $weightForChartChartJS = getDataJsonForChartChartJS($childGrowth, $begin_day, $end_day, $birthday, 'weight');

                if ($child['gender'] == 'male') {
                    // Dữ liệu chuẩn
                    $hightStandardArray = getHightDataForChart($hightStandardJsonMale);
                    $weightStandardArray = getHightDataForChart($weightStandardJsonMale);
                } else {
                    $hightStandardArray = getHightDataForChart($hightStandardJsonFeMale);
                    $weightStandardArray = getHightDataForChart($weightStandardJsonFeMale);
                }

                // 1. Chiều cao
                // Lấy 2 ngày gần nhất của $begin_day có dữ liệu, nội suy chuẩn của ngày bắt đầu
                if ($begin_day < 1826) {
                    $hightStandardArray[$begin_day]['highest'] = getStandardDataBeginAndEnd($hightStandardArray, $begin_day, 'highest');

                    $hightStandardArray[$begin_day]['lowest'] = getStandardDataBeginAndEnd($hightStandardArray, $begin_day, 'lowest');
                }

                if ($end_day < 1826) {
                    // Lấy 2 ngày gần nhất của $end_day có dữ liệu, nội suy chuẩn của ngày kết thúc
                    $hightStandardArray[$end_day]['highest'] = getStandardDataBeginAndEnd($hightStandardArray, $end_day, 'highest');

                    $hightStandardArray[$end_day]['lowest'] = getStandardDataBeginAndEnd($hightStandardArray, $end_day, 'lowest');
                }

                // dỮ LIỆU đường cao nhất (thư viện charts.js)
                $highestForChartChartsJS = getStandardDataForChartChartJS($hightStandardArray, $begin_day, $end_day, 'highest');

                // Dữ liệu đường thấp nhất (thư viện charts.js)
                $lowestForChartChartsJS = getStandardDataForChartChartJS($hightStandardArray, $begin_day, $end_day, 'lowest');

                // 2. Cân nặng
                // Lấy 2 ngày gần nhất của $begin_day có dữ liệu, nội suy chuẩn của ngày bắt đầu
                if ($begin_day < 1826) {
                    $weightStandardArray[$begin_day]['highest'] = getStandardDataBeginAndEnd($weightStandardArray, $begin_day, 'highest');

                    $weightStandardArray[$begin_day]['lowest'] = getStandardDataBeginAndEnd($weightStandardArray, $begin_day, 'lowest');
                }

                // Lấy 2 ngày gần nhất của $end_day có dữ liệu, nội suy chuẩn của ngày kết thúc
                if ($end_day < 1826) {
                    $weightStandardArray[$end_day]['highest'] = getStandardDataBeginAndEnd($weightStandardArray, $end_day, 'highest');

                    $weightStandardArray[$end_day]['lowest'] = getStandardDataBeginAndEnd($weightStandardArray, $end_day, 'lowest');
                }

                // Dữ liệu đường cao nhất (charts.js)
                $highestWeightForChartChartJS = getStandardDataForChartChartJS($weightStandardArray, $begin_day, $end_day, 'highest');

                // Dữ liệu đường thấp nhất (charts.js)
                $lowestWeightForChartChartJS = getStandardDataForChartChartJS($weightStandardArray, $begin_day, $end_day, 'lowest');

                // 3. BMI (charts.js)
                $bmiForChartChartJS = getDataJsonForChartChartJS($childGrowth, $begin_day, $end_day, $birthday, 'bmi');
//                print_r($end_day);
//                die();
                // Chiều cao for charts.js
                $smarty->assign('highestForChartChartJS', $highestForChartChartsJS);
                $smarty->assign('lowestForChartChartJS', $lowestForChartChartsJS);
                $smarty->assign('heightForChartChartJS', $heightForChartChartJS);

                //Cân nặng cho charts.js
                $smarty->assign('highestWeightForChartChartJS', $highestWeightForChartChartJS);
                $smarty->assign('lowestWeightForChartChartJS', $lowestWeightForChartChartJS);
                $smarty->assign('weightForChartChartJS', $weightForChartChartJS);

                // BMI (charts.js)
                $smarty->assign('bmiForChartChartJS', $bmiForChartChartJS);

                $smarty->assign('child', $child);
                $smarty->assign('growth', array_reverse($childGrowth));

                $label = '';
                for ($i = $begin_day; $i <= $end_day; $i++) {
                    if ($i % 30 == 0) {
                        $month = $i / 30;
                        $label .= ' ' . $month . ',';
                    } else {
                        $label .= ' ,';
                    }
                }
                $smarty->assign('label', $label);
                break;
            case 'addgrowth':
                page_header(__("School Management") . " &rsaquo; " . __("Children") . " &rsaquo; " . __("Add new growth"));

                // valid inputs
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }

                $child = getChildData($_GET['id'], CHILD_INFO);
                if (is_null($child)) {
                    _error(404);
                }

                $smarty->assign('child', $child);
                break;
            case 'editgrowth':
                page_header(__("School Management") . " &rsaquo; " . __("Children") . " &rsaquo; " . __("Edit student growth"));

                // valid inputs
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }
                $is_module = 0;
                if (isset($_GET['p5']) && $_GET['p5'] == 'module') {
                    $is_module = 1;
                }

                $data = $childDao->getChildGrowthById($_GET['id']);
                $child = $childDao->getChildByParent($data['child_parent_id']);

                $smarty->assign('data', $data);
                $smarty->assign('child', $child);
                $smarty->assign('is_module', $is_module);
                break;
            case 'developmentindex':
                page_header(__("School Management") . " &rsaquo; " . __("Children") . " &rsaquo; " . __("Development index"));

                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                $smarty->assign('classes', $classes);
                break;
            case 'developmentclass':
                page_header(__("School Management") . " &rsaquo; " . __("Children") . " &rsaquo; " . __("Development index of child in class"));

                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }
                if (!isset($_GET['p5'])) {
                    _error(404);
                }
                $month = str_replace('-', '/', $_GET['p5']);
                $children = getClassData($_GET['id'], CLASS_CHILDREN);
                $childGrowts = array();
                // Lặp danh sách trẻ, kiểm tra xem trong tháng đó trẻ có tăng cân không
                foreach ($children as $child) {
                    // Lấy lần kiểm tra sức khỏe cuối cùng trong tháng của trẻ (được tạo bởi school hoặc class)
                    $child_parent_id = $childDao->getChildParentIdFromChildId($child['child_id']);
                    $growthLastestMonth = $childDao->getChildGrowthLastestMonthBySchoolOrClass($child_parent_id, $month);

                    if (is_null($growthLastestMonth)) {
                        $child['height'] = null;
                        $child['weight'] = null;
                        $child['weight_status'] = WEIGHT_UNAVAILABLE;

                        // Lấy lần do trước của tháng này
                        $firstDayOfMonth = "01/" . $month;
                        $growthPrevMonth = $childDao->getChildGrowthNearLastestMonthBySchoolOrClass($child_parent_id, $firstDayOfMonth);
                        if (is_null($growthPrevMonth)) {
                            $child['last_time_weight'] = null;
                        } else {
                            $child['last_time_weight'] = $growthPrevMonth['weight'];
                        }
                    } else {
                        $child['height'] = $growthLastestMonth['height'];
                        $child['weight'] = $growthLastestMonth['weight'];
                        // Lấy lần nhập thông tin sức khỏe trước lần cuối cùng trong tháng
                        $growthNearLastestMonth = $childDao->getChildGrowthNearLastestMonthBySchoolOrClass($child_parent_id, $growthLastestMonth['recorded_at']);
                        if (is_null($growthNearLastestMonth)) {
                            $child['last_time_weight'] = null;
                            $child['weight_status'] = WEIGHT_UNAVAILABLE;
                        } else {
                            $child['last_time_weight'] = $growthNearLastestMonth['weight'];
                            if (($growthLastestMonth['weight'] - $growthNearLastestMonth['weight']) > 0.1) {
                                $child['weight_status'] = WEIGHT_GET_WEIGHT;
                            } else if (($growthNearLastestMonth['weight'] - $growthLastestMonth['weight']) > 0.1) {
                                $child['weight_status'] = WEIGHT_LOSE_WEIGHT;
                            } else {
                                $child['weight_status'] = WEIGHT_NO_CHANGED;
                            }
                        }
                    }
                    $childGrowths[] = $child;
                }

                $smarty->assign('childGrowths', $childGrowths);
                break;
            case 'addphoto':
                page_header(__("School Management") . " &rsaquo; " . __("Children") . " &rsaquo; " . __("Add photo to diary"));

                // valid inputs
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }

                $child = getChildData($_GET['id'], CHILD_INFO);
                if (is_null($child)) {
                    _error(404);
                }

                $smarty->assign('child', $child);
                break;
            case 'journal':
                page_header(__("School Management") . " &rsaquo; " . __("Children") . " &rsaquo; " . __("Diary corner"));

                // valid inputs
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }

                $child = getChildData($_GET['id'], CHILD_INFO);
                if (is_null($child)) {
                    _error(404);
                }

                $journals = $journalDao->getAllJournalChildOfSchool($child['child_parent_id']);
                $smarty->assign('results', $journals);

                $smarty->assign('child', $child);
                $smarty->assign('child_parent_id', $child['child_parent_id']);
                $smarty->assign('username', $_GET['username']);
                break;
            case 'adddiary':
                // page header
                page_header(__("School Management") . " &rsaquo; " . __("Add diary"));

                //$classes = $classDao->getClassNamesOfSchool($school['page_id']);
                //Memcache - Lấy ra các lớp của 1 trường
                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                $smarty->assign('classes', $classes);
                break;
            default:
                _error(404);
                break;
        }
        break;
    case 'events':
        // Tăng số đếm tương tác của trường
        increaseSchoolInteractive($school['page_id'], 'event');
        // Tăng số lượt tương tác của trường - TaiLA
        addInteractive($school['page_id'], 'event', 'school_view');

        switch ($subView) {
            case '':
                // page header
                page_header(__("School Management") . " &rsaquo; " . __("Notification - Event"));
                if (canView($_GET['username'], $view)) {
                    $events = $eventDao->getEvents($school['page_id']);
                } else {
                    $events = $eventDao->getEventsForEmployee($school['page_id']);
                }
                $smarty->assign('rows', $events);

                break;
            case 'participants':
                if (!canView($_GET['username'], $view)) {
                    _error(403);
                }
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }
                //Lấy ra thông tin sự kiện
                $event = $eventDao->getEvent($_GET['id']);
                if (is_null($event)) {
                    _error(404);
                }

                $childCount = 0;
                $parentCount = 0;
                if ($event['level'] == SCHOOL_LEVEL) {
                    //Lấy ra danh sách lớp của trường hoặc khối
                    //$classes = $classDao->getClassNamesOfSchool($school['page_id']);
                    //Memcache - Lấy ra các lớp của 1 trường
                    $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                    $smarty->assign('classes', $classes);
                    $eventPars = array();
                    foreach ($classes as $class) {
                        $countChild = 0;
                        $countParent = 0;
                        $participants = $eventDao->getParticipantsOfClass($class['group_id'], $event['event_id'], $event['for_parent'], $countChild, $countParent);
                        $class['countChild'] = $countChild;
                        $class['countParent'] = $countParent;
                        $class['participants'] = $participants;
                        $eventPars[] = $class;
                        $childCount = $childCount + $countChild;
                        $parentCount = $parentCount + $countParent;
                    }
                    $smarty->assign('eventPars', $eventPars);
                    $smarty->assign('childCount', $childCount);
                    $smarty->assign('parentCount', $parentCount);
//                    print_r($childCount);
//                    print_r($parentCount);
//                    echo "<pre>";
//                    print_r($eventPars);
//                    echo "<pre>";
//                    die('qqq');
                } else if ($event['level'] == CLASS_LEVEL_LEVEL) {
                    //$classes = $classDao->getClassesOfLevel($event['class_level_id']);
                    //Memcache - Lấy ra các khối của 1 trường
                    $classes = getClassLevelData($event['class_level_id'], CLASS_LEVEL_CLASSES);
                    $smarty->assign('classes', $classes);
                    $eventPars = array();
                    foreach ($classes as $class) {
                        $countChild = 0;
                        $countParent = 0;
                        $participants = $eventDao->getParticipantsOfClass($class['group_id'], $event['event_id'], $event['for_parent'], $countChild, $countParent);
                        $class['countChild'] = $countChild;
                        $class['countParent'] = $countParent;
                        $class['participants'] = $participants;
                        $eventPars[] = $class;
                        $childCount = $childCount + $countChild;
                        $parentCount = $parentCount + $countParent;
                    }
                    $smarty->assign('eventPars', $eventPars);
                    $smarty->assign('childCount', $childCount);
                    $smarty->assign('parentCount', $parentCount);
                } else {
                    $participants = $eventDao->getParticipantsOfClass($event['class_id'], $event['event_id'], $event['for_parent'], $childCount, $parentCount);
                    $smarty->assign('participants', $participants);
                    $smarty->assign('childCount', $childCount);
                    $smarty->assign('parentCount', $parentCount);
                }

                $smarty->assign('event', $event);

                page_header(__("School Management") . " &rsaquo; " . __("Notification - Event") . " &rsaquo; " . $event['event_name'] . " &rsaquo; " . __("Participants"));
                break;

            case 'participantsforemployee':
                page_header(__("School Management") . " &rsaquo; " . __("Notification - Event") . " &rsaquo; " . $event['event_name'] . " &rsaquo; " . __("Participants"));
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }
                //Lấy ra thông tin sự kiện
                $event = $eventDao->getEvent($_GET['id']);
                if (is_null($event)) {
                    _error(404);
                }
                $participants = $eventDao->getTeacherParticipants($school['page_id'], $event['event_id']);

                $smarty->assign('participants', $participants);
                $smarty->assign('event', $event);
                $smarty->assign('canEdit', $canEdit);
                $smarty->assign('username', $_POST['school_username']);
                break;

            case 'add':
                page_header(__("School Management") . " &rsaquo; " . __("Notification - Event") . " &rsaquo; " . __("Add New"));

                //$class_levels = $classLevelDao->getClassLevels($school['page_id']);
                //Memcache - Lấy ra các khối của 1 trường
                $class_levels = getSchoolData($school['page_id'], SCHOOL_CLASS_LEVELS);
                //Memcache - Lấy ra các lớp của 1 trường
                //$classes = $classDao->getClassesOfSchool($school['page_id']);
                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                // assign variables
                $smarty->assign('class_levels', $class_levels);
                $smarty->assign('classes', $classes);
                break;

            case 'edit':
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }
                $event = $eventDao->getEvent($_GET['id']);
                if (is_null($event)) {
                    _error(404);
                }
                $smarty->assign('data', $event);

                //$class_levels = $classLevelDao->getClassLevels($school['page_id']);
                //Memcache - Lấy ra các khối của 1 trường
                $class_levels = getSchoolData($school['page_id'], SCHOOL_CLASS_LEVELS);
                //$classes = $classDao->getClassesOfSchool($school['page_id']);
                //Memcache - Lấy ra các lớp của 1 trường
                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                // assign variables
                $smarty->assign('class_levels', $class_levels);
                $smarty->assign('classes', $classes);

                page_header(__("School Management") . " &rsaquo; " . __("Notification - Event") . " &rsaquo; " . $event['event_name']);
                break;
            case 'detail':
                // Tăng số lượt tương tác của trường đối với event này
                addInteractive($school['page_id'], 'event', 'school_view', $_GET['id']);
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }
                $event = $eventDao->getEventDetail($_GET['id']);
                if (is_null($event)) {
                    _error(404);
                }
                if (!$event['for_teacher']) {
                    $smarty->assign('data', $event);
                } else {
                    $event_for_teacher = $eventDao->getTeacherEventDetail($event['event_id'], $user->_data['user_id']);
                    $smarty->assign('data', $event_for_teacher);
                }

                page_header(__("School Management") . " &rsaquo; " . __("Notification - Event") . " &rsaquo; " . $event['event_name']);
                break;

            default:
                _error(404);
                break;
        }
        break;
    case 'medicines':
        // Tăng số đếm tương tác của trường
        increaseSchoolInteractive($school['page_id'], 'medicine');
        // Tăng lượt tương tác của trường - TaiLA
        addInteractive($school['page_id'], 'medicine', 'school_view');

        switch ($subView) {
            case '':

                // page header
                page_header(__("School Management") . " &rsaquo; " . __("Medicines"));
                $today = date($system['date_format']);
                $count_no_confirm = 0;
                $medicines = $medicineDao->getMedicineOnDate($school['page_id'], $today, $count_no_confirm);

                $smarty->assign('rows', $medicines);
                $smarty->assign('count_no_confirm', $count_no_confirm);

                break;
            case 'all':
                if (!canView($_GET['username'], $view)) {
                    _error(403);
                }
                // page header
                page_header(__("School Management") . " &rsaquo; " . __("All medicines"));
                $count_no_confirm = 0;
                $medicines = $medicineDao->getAllMedicines($school['page_id'], $count_no_confirm);

                $smarty->assign('rows', $medicines);
                $smarty->assign('count_no_confirm', $count_no_confirm);

                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                $smarty->assign('classes', $classes);
                break;

            case 'add':
                page_header(__("School Management") . " &rsaquo; " . __("Medicines") . " &rsaquo; " . __("Add New"));
                //$classes = $classDao->getClassNamesOfSchool($school['page_id']);
                //Memcache - Lấy ra các lớp của 1 trường
                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                $smarty->assign('classes', $classes);

                break;
            case 'edit':
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }
                $medicine = $medicineDao->getMedicine($_GET['id'], false);
                if (is_null($medicine)) {
                    _error(404);
                }
                $smarty->assign('data', $medicine);

                //$classes = $classDao->getClassNamesOfSchool($school['page_id']);
                //Memcache - Lấy ra các lớp của 1 trường
                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                $smarty->assign('classes', $classes);

                page_header(__("School Management") . " &rsaquo; " . __("Medicines") . " &rsaquo; " . $medicine['medicine_list']);
                break;

            case 'detail':
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }
                include_once(DAO_PATH . 'dao_user.php');
                $userDao = new UserDAO();
                $medicine = $medicineDao->getMedicine4Detail($_GET['id']);
                if (is_null($medicine)) {
                    _error(404);
                }
                $medicine['confirm_user'] = '';
                $medicine['confirm_username'] = '';
                if (!is_null($medicine['confirm_user_id'])) {
                    $userConfirm = $userDao->getUserByUserId($medicine['confirm_user_id']);
                    $medicine['confirm_user'] = $userConfirm['user_fullname'];
                    $medicine['confirm_username'] = $userConfirm['user_name'];
                }
                $smarty->assign('data', $medicine);

                page_header(__("School Management") . " &rsaquo; " . __("Medicines") . " &rsaquo; " . $medicine['medicine_list']);
                break;

            default:
                _error(404);
                break;
        }
        break;
    case 'diarys':
        // Tăng số lượt tương tác của trường - TaiLA
        addInteractive($school['page_id'], 'diary', 'school_view');
        switch ($subView) {
            case '':
                // page header
                page_header(__("School Management") . " &rsaquo; " . __("Diary corner"));
                //Memcache - Lấy ra các lớp của 1 trường
                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                $smarty->assign('classes', $classes);
                //Lấy điều kiện search trước đó ra khỏi session
                $condition = $_SESSION[SESSION_KEY_SEARCH_DIARY];

                if ($school['page_id'] == $condition['school_id']) {
                    $classId = isset($condition) ? $condition['class_id'] : '';
                    $childId = isset($condition) ? $condition['child_id'] : '';
                    $year = isset($condition) ? $condition['year'] : '';
                } else {
                    $classId = '';
                    $childId = '';
                    $year = '';
                }

                $results = array();
                $children = array();
                if ($childId != '') {
                    $child_parent_id = $childDao->getChildParentIdFromChildId($childId);
                    $results = $journalDao->getAllJournalChildForSchoolOfYear($child_parent_id, $year);

                    $children = getClassData($classId, CLASS_CHILDREN);
                }
                $child = getChildData($childId, CHILD_INFO);
                $is_module = 1;
                $smarty->assign('results', $results);
                $smarty->assign('children', $children);
                $smarty->assign('year', $year);
                $smarty->assign('childId', $childId);
                $smarty->assign('classId', $classId);
                $smarty->assign('child', $child);
                $smarty->assign('is_module', $is_module);


                break;

            case 'add':
                page_header(__("School Management") . " &rsaquo; " . __("Diary corner") . " &rsaquo; " . __("Add New"));
                //$classes = $classDao->getClassNamesOfSchool($school['page_id']);
                //Memcache - Lấy ra các lớp của 1 trường
                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                $smarty->assign('classes', $classes);
                break;

            default:
                _error(404);
                break;
        }
        break;
    case 'healths':
        // Tăng số lượt tương tác của trường - TaiLA
        addInteractive($school['page_id'], 'health', 'school_view');
        switch ($subView) {
            case '':
                // page header
                page_header(__("School Management") . " &rsaquo; " . __("Health information"));
                // Memcache - Lấy ra các lớp của 1 trường
                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                $smarty->assign('classes', $classes);
                //Lấy điều kiện search trước đó ra khỏi session
                $condition = $_SESSION[SESSION_KEY_SEARCH_CHILDREN_HEALTH];
                if ($school['page_id'] == $condition['school_id']) {
                    $classId = isset($condition) ? $condition['class_id'] : '';
                    $childId = isset($condition) ? $condition['child_id'] : '';
                    $begin = isset($condition) ? $condition['begin'] : '';
                    $end = isset($condition) ? $condition['end'] : '';
                } else {
                    $classId = '';
                    $childId = '';
                    $begin = '';
                    $end = '';
                }

                // Hai biến dưới để assign ra màn hình
                $beginScreen = $begin;
                $endScreen = $end;

                $children = array();
                if ($childId != '') {
                    $child = getChildData($childId, CHILD_INFO);
                    if (is_null($child)) {
                        _error(404);
                    }
                    // Lấy ngày hiện tại
                    $dateNow = date('Y-m-d');
                    $dateNowSys = toSysDate($dateNow);

                    // Lấy ngày sinh nhật của trẻ
                    $birthday = toDBDate($child['birthday']);

                    // Khởi tạo mảng lưu chỉ số sức khỏe của trẻ
                    $childGrowth = array();

                    if (is_null($begin) || is_null($end)) {
                        // Lấy toàn bộ thông tin sức khỏe của trẻ
                        $childGrowth = $childDao->getChildGrowthForChart($child['child_parent_id']);

                        // Tính xem trẻ được bao nhiêu ngày tuổi
                        $day_age_now = (strtotime($dateNow) - strtotime($birthday)) / (60 * 60 * 24);

                        // đoạn này xử lý để lấy ngày bắt đầu (đặt tạm a, b, c; bí tên quá)
                        $a = $day_age_now % 30;
                        $b = $day_age_now - $a;
                        $c = $b - 30 * 10;
                        $dayGet = (int)$c;
                        $day_age_max = $day_age_now + 65;
                        if ($day_age_now >= 300) {
                            $day_age_min = $dayGet;
                        } else {
                            $day_age_min = 0;
                        }

                        // Lấy dữ liệu người dùng nhập vào vẽ biểu đồ
                        $heightForChartChartJS = getDataJsonForChartChartJS($childGrowth, $day_age_min, $day_age_max, $birthday, 'height');
                        $weightForChartChartJS = getDataJsonForChartChartJS($childGrowth, $day_age_min, $day_age_max, $birthday, 'weight');
                        if ($child['gender'] == 'male') {
                            // Dữ liệu chuẩn
                            $hightStandardArray = getHightDataForChart($hightStandardJsonMale);
                            $weightStandardArray = getHightDataForChart($weightStandardJsonMale);
                        } else {
                            $hightStandardArray = getHightDataForChart($hightStandardJsonFeMale);
                            $weightStandardArray = getHightDataForChart($weightStandardJsonFeMale);

                        }
                        // 1. Chiều cao
                        // Lấy 2 ngày gần nhất của $begin_day có dữ liệu, nội suy chuẩn của ngày bắt đầu
                        $hightStandardArray[$day_age_min]['highest'] = getStandardDataBeginAndEnd($hightStandardArray, $day_age_min, 'highest');

                        $hightStandardArray[$day_age_min]['lowest'] = getStandardDataBeginAndEnd($hightStandardArray, $day_age_min, 'lowest');

                        // Lấy 2 ngày gần nhất của $end_day có dữ liệu, nội suy chuẩn của ngày kết thúc
                        $hightStandardArray[$day_age_max]['highest'] = getStandardDataBeginAndEnd($hightStandardArray, $day_age_max, 'highest');

                        $hightStandardArray[$day_age_max]['lowest'] = getStandardDataBeginAndEnd($hightStandardArray, $day_age_max, 'lowest');

                        // dỮ LIỆU đường cao nhất (thư viện charts.js)
                        $highestForChartChartsJS = getStandardDataForChartChartJS($hightStandardArray, $day_age_min, $day_age_max, 'highest');

                        // Dữ liệu đường thấp nhất (thư viện charts.js)
                        $lowestForChartChartsJS = getStandardDataForChartChartJS($hightStandardArray, $day_age_min, $day_age_max, 'lowest');

                        // 2. Cân nặng
                        // Lấy 2 ngày gần nhất của $begin_day có dữ liệu, nội suy chuẩn của ngày bắt đầu
                        $weightStandardArray[$day_age_min]['highest'] = getStandardDataBeginAndEnd($weightStandardArray, $day_age_min, 'highest');

                        $weightStandardArray[$day_age_min]['lowest'] = getStandardDataBeginAndEnd($weightStandardArray, $day_age_min, 'lowest');

                        // Lấy 2 ngày gần nhất của $end_day có dữ liệu, nội suy chuẩn của ngày kết thúc
                        $weightStandardArray[$day_age_max]['highest'] = getStandardDataBeginAndEnd($weightStandardArray, $day_age_max, 'highest');

                        $weightStandardArray[$day_age_max]['lowest'] = getStandardDataBeginAndEnd($weightStandardArray, $day_age_max, 'lowest');

                        // Dữ liệu đường cao nhất (charts.js)
                        $highestWeightForChartChartJS = getStandardDataForChartChartJS($weightStandardArray, $day_age_min, $day_age_max, 'highest');

                        // Dữ liệu đường thấp nhất (charts.js)
                        $lowestWeightForChartChartJS = getStandardDataForChartChartJS($weightStandardArray, $day_age_min, $day_age_max, 'lowest');

                        // 3. BMI (charts.js)
                        $bmiForChartChartJS = getDataJsonForChartChartJS($childGrowth, $day_age_min, $day_age_max, $birthday, 'bmi');

                        // GEt label
                        $label = '';
                        for ($i = $day_age_min; $i <= $day_age_max; $i++) {
                            if ($i % 30 == 0) {
                                $month = $i / 30;
                                $label .= ' ' . $month . ',';
                            } else {
                                $label .= ' ,';
                            }
                        }
                        $smarty->assign('label', $label);
                    } else {
                        // Lấy dữ liệu nhập và của trẻ
                        $childGrowth = $childDao->getChildGrowthSearchForChartBeginEnd($child['child_parent_id'], $begin, $end);

                        // Lấy ngày bắt đầu và ngày kết thúc
                        $begin = toDBDate($begin);
                        $end = toDBDate($end);

                        $end_day = (strtotime($end) - strtotime($birthday)) / (60 * 60 * 24);
                        $end_day = $end_day / 30;
                        $end_day = (int)$end_day;
                        $end_day = ($end_day + 1) * 30;

                        $begin_day = (strtotime($begin) - strtotime($birthday)) / (60 * 60 * 24);
                        $begin_day = $begin_day / 30;
                        $begin_day = (int)$begin_day;
                        $begin_day = $begin_day * 30;
                        if ($begin_day < 0) {
                            throw new Exception(__("You must select day begin after birthday"));
                        }

                        // Dữ liệu chuẩn người dùng nhập vào vẽ biểu đồ chiều cao (thư viện charts.js)
                        $heightForChartChartJS = getDataJsonForChartChartJS($childGrowth, $begin_day, $end_day, $birthday, 'height');

                        // Lấy dữ liệu người dùng nhập vào vẽ biểu đồ cân nặng (charts.js)
                        $weightForChartChartJS = getDataJsonForChartChartJS($childGrowth, $begin_day, $end_day, $birthday, 'weight');

                        if ($child['gender'] == 'male') {
                            // Dữ liệu chuẩn
                            $hightStandardArray = getHightDataForChart($hightStandardJsonMale);
                            $weightStandardArray = getHightDataForChart($weightStandardJsonMale);
                        } else {
                            $hightStandardArray = getHightDataForChart($hightStandardJsonFeMale);
                            $weightStandardArray = getHightDataForChart($weightStandardJsonFeMale);
                        }

                        // 1. Chiều cao
                        // Lấy 2 ngày gần nhất của $begin_day có dữ liệu, nội suy chuẩn của ngày bắt đầu
                        $hightStandardArray[$begin_day]['highest'] = getStandardDataBeginAndEnd($hightStandardArray, $begin_day, 'highest');

                        $hightStandardArray[$begin_day]['lowest'] = getStandardDataBeginAndEnd($hightStandardArray, $begin_day, 'lowest');

                        // Lấy 2 ngày gần nhất của $end_day có dữ liệu, nội suy chuẩn của ngày kết thúc
                        $hightStandardArray[$end_day]['highest'] = getStandardDataBeginAndEnd($hightStandardArray, $end_day, 'highest');

                        $hightStandardArray[$end_day]['lowest'] = getStandardDataBeginAndEnd($hightStandardArray, $end_day, 'lowest');

                        // dỮ LIỆU đường cao nhất (thư viện charts.js)
                        $highestForChartChartsJS = getStandardDataForChartChartJS($hightStandardArray, $begin_day, $end_day, 'highest');

                        // Dữ liệu đường thấp nhất (thư viện charts.js)
                        $lowestForChartChartsJS = getStandardDataForChartChartJS($hightStandardArray, $begin_day, $end_day, 'lowest');

                        // 2. Cân nặng
                        // Lấy 2 ngày gần nhất của $begin_day có dữ liệu, nội suy chuẩn của ngày bắt đầu
                        $weightStandardArray[$begin_day]['highest'] = getStandardDataBeginAndEnd($weightStandardArray, $begin_day, 'highest');

                        $weightStandardArray[$begin_day]['lowest'] = getStandardDataBeginAndEnd($weightStandardArray, $begin_day, 'lowest');

                        // Lấy 2 ngày gần nhất của $end_day có dữ liệu, nội suy chuẩn của ngày kết thúc
                        $weightStandardArray[$end_day]['highest'] = getStandardDataBeginAndEnd($weightStandardArray, $end_day, 'highest');

                        $weightStandardArray[$end_day]['lowest'] = getStandardDataBeginAndEnd($weightStandardArray, $end_day, 'lowest');

                        // Dữ liệu đường cao nhất (charts.js)
                        $highestWeightForChartChartJS = getStandardDataForChartChartJS($weightStandardArray, $begin_day, $end_day, 'highest');

                        // Dữ liệu đường thấp nhất (charts.js)
                        $lowestWeightForChartChartJS = getStandardDataForChartChartJS($weightStandardArray, $begin_day, $end_day, 'lowest');

                        // 3. BMI (charts.js)
                        $bmiForChartChartJS = getDataJsonForChartChartJS($childGrowth, $begin_day, $end_day, $birthday, 'bmi');

                        // GEt label
                        $label = '';
                        for ($i = $begin_day; $i <= $end_day; $i++) {
                            if ($i % 30 == 0) {
                                $month = $i / 30;
                                $label .= ' ' . $month . ',';
                            } else {
                                $label .= ' ,';
                            }
                        }
                        $smarty->assign('label', $label);
                    }

                    // Chiều cao for charts.js
                    $smarty->assign('highestForChartChartJS', $highestForChartChartsJS);
                    $smarty->assign('lowestForChartChartJS', $lowestForChartChartsJS);
                    $smarty->assign('heightForChartChartJS', $heightForChartChartJS);

                    //Cân nặng cho charts.js
                    $smarty->assign('highestWeightForChartChartJS', $highestWeightForChartChartJS);
                    $smarty->assign('lowestWeightForChartChartJS', $lowestWeightForChartChartJS);
                    $smarty->assign('weightForChartChartJS', $weightForChartChartJS);

                    // BMI (charts.js)
                    $smarty->assign('bmiForChartChartJS', $bmiForChartChartJS);

                    $children = getClassData($classId, CLASS_CHILDREN);
                    $smarty->assign('username', $school['page_name']);
                    $smarty->assign('growth', array_reverse($childGrowth));
                    $smarty->assign('child', $child);
                    $smarty->assign('children', $children);
                    $smarty->assign('childId', $childId);
                    $smarty->assign('classId', $classId);
                }
//                $child = getChildData($childId, CHILD_INFO);
                $is_module = 1;
                $smarty->assign('begin', $beginScreen);
                $smarty->assign('end', $endScreen);
                $smarty->assign('is_module', $is_module);
                break;

            case 'add':
                page_header(__("School Management") . " &rsaquo; " . __("Diary corner") . " &rsaquo; " . __("Add New"));
                //$classes = $classDao->getClassNamesOfSchool($school['page_id']);
                //Memcache - Lấy ra các lớp của 1 trường
                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                $smarty->assign('classes', $classes);
                break;

            default:
                _error(404);
                break;
        }
        break;
    case 'fees':
        // Tăng số đếm tương tác của trường
        increaseSchoolInteractive($school['page_id'], 'tuition');
        // Tăng số lượt tương tác của trường - TaiLA
        addInteractive($school['page_id'], 'tuition', 'school_view');

        $canEdit = canEdit($_GET['username'], 'tuitions');
        switch ($subView) {
            case '':
                if (!canView($_GET['username'], 'tuitions')) {
                    _error(403);
                }
                page_header(__("School Management") . " &rsaquo; " . __("Fee"));
                //$fees = $tuitionDao->getFees($school['page_id']);
                $fees = $tuitionDao->getFeesOfSchool($school['page_id'], true);

                $smarty->assign('rows', $fees);

                break;
            case 'inactive':
                if (!canView($_GET['username'], 'tuitions')) {
                    _error(403);
                }
                page_header(__("School Management") . " &rsaquo; " . __("Inactive fee list"));
                //$fees = $tuitionDao->getFees($school['page_id']);
                $fees = $tuitionDao->getFeesOfSchool($school['page_id'], false);

                $smarty->assign('rows', $fees);

                break;
            case 'add':
                if (!$canEdit) {
                    _error(403);
                }
                unset($_SESSION['child_checked_add']);
                page_header(__("School Management") . " &rsaquo; " . __("Fee") . " &rsaquo; " . __("Add New"));

                //$class_levels = $classLevelDao->getClassLevels($school['page_id']);
                //Memcache - Lấy ra các khối của 1 trường
                $class_levels = getSchoolData($school['page_id'], SCHOOL_CLASS_LEVELS);
                //$classes = $classDao->getClassesOfSchool($school['page_id']);
                //Memcache - Lấy ra các lớp của 1 trường
                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                $fees = $tuitionDao->getOnlyFees($school['page_id']);
                $smarty->assign('class_levels', $class_levels);
                $smarty->assign('classes', $classes);
                $smarty->assign('fees', $fees);

                break;
            case 'edit':
                if (!$canEdit) {
                    _error(403);
                }
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }
                $data = $tuitionDao->getFee($_GET['id']);
                if (is_null($data)) {
                    _error(404);
                }
                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);

                $newFees = array();
                if ($data['level'] == CHILD_LEVEL) {
                    //Trường hợp là phí thay thế, áp dụng cho một số trẻ
                    $classIds = array_keys($classes);
                    $childIds = $data['child_ids'];
                    $classId = $classIds[0];
                    // if (count($childIds) > 0) {
                    //     $classId = getChildData($childIds[0], CHILD_CLASS)['group_id'];
                    // }
                    $data['class_id'] = $classId;

                    //Tạo nên danh sách trẻ của lớp hiện tại.
                    $currentClassChildIds = array();
                    $newChildren = array();
                    $children = getClassData($classId, CLASS_CHILDREN);
                    foreach ($children as $child) {
                        if (in_array($child['child_id'], $childIds)) {
                            $child['checked'] = 1;
                            $currentClassChildIds[] = $child['child_id'];
                        } else {
                            $child['checked'] = 0;
                        }
                        $newChildren[] = $child;
                    }
                    $remandChildIds = array_diff($childIds, $currentClassChildIds);
                    //Tất cả ID của trẻ được áp dụng loại phí này.
                    $smarty->assign('remanding_child_ids', implode(",", $remandChildIds));
                    $smarty->assign('children', $newChildren);
                    $childrenListHtml = $smarty->fetch("ci/school/school.fees.children.tpl");
                    $smarty->assign('children_list_html', $childrenListHtml);

                    //Kiểm tra xem danh sách những fee nào được thay thế.
                    $fees = $tuitionDao->getOnlyFees($school['page_id']);
                    $fee_change_ids = explode(",", $data['fee_change_id']);
                    foreach ($fees as $fee) {
                        if (in_array($fee['fee_id'], $fee_change_ids)) {
                            $fee['checked'] = 1;
                        }
                        $newFees[] = $fee;
                    }
                } else {
                    //Nếu không phải phí cấp TRẺ thì lấy toàn bộ danh sách.
                    $newFees = $tuitionDao->getOnlyFees($school['page_id']);
                }

                $smarty->assign('data', $data);

                //$class_levels = $classLevelDao->getClassLevels($school['page_id']);
                //Memcache - Lấy ra các khối của 1 trường
                $class_levels = getSchoolData($school['page_id'], SCHOOL_CLASS_LEVELS);
                //$classes = $classDao->getClassesOfSchool($school['page_id']);
                //Memcache - Lấy ra các lớp của 1 trường
                $smarty->assign('class_levels', $class_levels);
                $smarty->assign('classes', $classes);
                $smarty->assign('fees', $newFees);

                page_header(__("School Management") . " &rsaquo; " . __("Fee") . " &rsaquo; " . $data['fee_name']);
                break;

            default:
                _error(404);
                break;
        }
        break;
    case 'useservices':
        // Tăng số đếm tương tác của trường
        increaseSchoolInteractive($school['page_id'], 'service');
        // Tăng số lượt tương tác của trường - TaiLA
        addInteractive($school['page_id'], 'service', 'school_view');

        $canEdit = canEdit($_GET['username'], 'services');
        switch ($subView) {
            case 'reg':
                if (!$canEdit) {
                    _error(403);
                }

                //Lấy ra danh sách dịch vụ đang áp dụng của trường
                $serviceList = getSchoolData($school['page_id'], SCHOOL_SERVICES);
                $serviceList = sortArray($serviceList, "DESC", "type");
                $newServices = array();
                foreach ($serviceList as $service) {
                    if ($service['status'] == STATUS_ACTIVE) {
                        $newServices[] = $service;
                    }
                }
                $smarty->assign('services', $newServices);

                //$classes = $classDao->getClassNamesOfSchool($school['page_id']);
                //Memcache - Lấy ra các lớp của 1 trường
                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                $classes = sortArray($classes, "ASC", "class_level_id");
                $smarty->assign('classes', $classes);

                page_header(__("School Management") . " &rsaquo; " . __("Service") . " &rsaquo; " . __("Register service"));
                break;
            case 'deduction':
                if (!$canEdit) {
                    _error(403);
                }

                //Lấy ra danh sách dịch vụ đang áp dụng của trường
                $serviceList = getSchoolData($school['page_id'], SCHOOL_SERVICES);
                $serviceList = sortArray($serviceList, "DESC", "type");
                $smarty->assign('services', $serviceList);

                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                $classes = sortArray($classes, "ASC", "class_level_id");
                $smarty->assign('classes', $classes);

                page_header(__("School Management") . " &rsaquo; " . __("Service") . " &rsaquo; " . __("Deduction note"));
                break;
            case 'history':
                if (!canView($_GET['username'], 'services')) {
                    _error(403);
                }
                page_header(__("School Management") . " &rsaquo; " . __("Usage history"));

                //$classes = $classDao->getClassNamesOfSchool($school['page_id']);
                //Memcache - Lấy ra các lớp của 1 trường
                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                $smarty->assign('classes', $classes);
                //Memcache
                //$services = $serviceDao->getAllSchoolServices($school['page_id']);
                $services = getSchoolData($school['page_id'], SCHOOL_SERVICES);
                $services = sortArray($services, "DESC", "type");
                $smarty->assign('services', $services);

                break;
            case 'class':
                if (!canView($_GET['username'], 'services')) {
                    _error(403);
                }
                page_header(__("School Management") . " &rsaquo; " . __("Count-based service summary"));

                //$classes = $classDao->getClassNamesOfSchool($school['page_id']);
                //Memcache - Lấy ra các lớp của 1 trường
                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                $smarty->assign('classes', $classes);
                //Memcache
                //$services = $serviceDao->getAllSchoolServices($school['page_id']);
                $services = getSchoolData($school['page_id'], SCHOOL_SERVICES);
                $services = sortArray($services, "DESC", "type");
                $smarty->assign('services', $services);
                $smarty->assign('childList', $childList);
                break;
            case 'classdate':
                if (!canView($_GET['username'], 'services')) {
                    _error(403);
                }
                page_header(__("School Management") . " &rsaquo; " . __("Count-based service summary"));
                $str = $_GET['id'];
                $class_id = 0;
                for ($i = 0; $i < strlen($str); $i++) {
                    if ($str[$i] == '_') {
                        $class_id = substr($str, 0, $i + 1);
                        $date_pos = substr($str, $i + 1, (strlen($str) - ($i + 1)));
                        break;
                    }
                }
                $date_pos = str_replace('_', '/', $date_pos);
                $childList = array();
                if ($class_id > 0) {
                    // Lấy danh sách child của lớp
                    $children = $childDao->getChildrenOfClass($class_id, $date_pos);

                    // Lấy ra tất cả dịch vụ theo số lần sử dụng của trường và danh sách trẻ
                    //$servicesCountBase = $serviceDao->getAllServiceCBForAllChildOfClass($date, $childrenIds);
                    // Lặp danh sách trẻ của lớp
                    foreach ($children as $child) {
                        // Lấy danh sách dịch vụ theo số lần sử dụng của mỗi trẻ
                        $services = $serviceDao->getChildService4Record($school['page_id'], $child['child_id'], $date_pos);
                        $child['services'] = $services;
                        $childList[] = $child;
                    }

                }

                //$classes = $classDao->getClassNamesOfSchool($school['page_id']);
                //Memcache - Lấy ra các lớp của 1 trường
                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                $smarty->assign('classes', $classes);
                //Memcache
                //$services = $serviceDao->getAllSchoolServices($school['page_id']);\
                $servicesCB = array();
                $services = getSchoolData($school['page_id'], SCHOOL_SERVICES);
                foreach ($services as $service) {
                    if ($service['type'] == 3 && $service['status'] == 1) {
                        $servicesCB[] = $service;
                    }
                }
                $classTitle = array();
                foreach ($classes as $class) {
                    if ($class_id == $class['group_id']) {
                        $classTitle = $class['group_title'];
                    }
                }
                $services = sortArray($services, "DESC", "type");
                $smarty->assign('services', $services);
                $smarty->assign('servicesCB', $servicesCB);
                $smarty->assign('childList', $childList);
                $smarty->assign('classTitle', $classTitle);
                $smarty->assign('date_pos', $date_pos);
                break;
            case 'foodsvr':
                if (!canView($_GET['username'], 'services')) {
                    _error(403);
                }

                //$foodServiceList = $serviceDao->getFoodServiceSchool($school['page_id']);
                $foodServiceList = array();

                //Memcache - Lấy ra tất cả dịch vụ và lọc ra dịch vụ ăn uống
                $serviceList = getSchoolData($school['page_id'], SCHOOL_SERVICES);
                foreach ($serviceList as $service) {
                    if ($service['is_food'] == 1 && $service['status'] == 1) {
                        $foodServiceList[] = $service;
                    }
                }
                $foodServiceList = sortMultiOrderArray($foodServiceList, [['type', 'ASC'], ['service_id'], ['ASC']]);

                //1. Liệt kê danh sách điểm danh toàn trường của ngày HÔM NAY, sort theo lớp
                $today = date($system['date_format']);
                $presentChildIds = $attendanceDao->getPresentChildrenOfSchoolADay($school['page_id'], $today);
                $classes = $presentChildIds['classes'];

                if ((count($foodServiceList) > 0) && (count($classes) > 0)) {
                    //2. Liệt kê danh sách đăng ký dịch vụ ĂN UỐNG theo tháng và theo điểm danh, sort theo lớp
                    $notCountBasedServices = $serviceDao->getFoodNotCountBasedServiceSchoolADay($school['page_id'], $presentChildIds['childIds'], $foodServiceList);
                    //3. Liệt kê danh sách đăng ký dịch vụ ĂN UỐNG theo lần
                    $countBasedServices = $serviceDao->getFoodCountBasedServiceSchoolADay($school['page_id'], $today, $presentChildIds['childIds'], $foodServiceList);

                    $totalServices = array();
                    foreach ($foodServiceList as $service) {
                        $totalServices["'" . $service['service_id'] . "'"] = 0;
                    }
                    //4. Ghép tất mỗi loại dịch vụ vào danh sách lớp để hiện ra màn hình.
                    $newClasses = array();
                    foreach ($classes as $class) {
                        $newClass = $class;
                        $serviceFood = array();
                        foreach ($notCountBasedServices as $notCountBasedService) {
                            if ($class['class_id'] == $notCountBasedService['class_id']) {
                                $newClass['not_countbased_services'] = $notCountBasedService['not_countbased_services'];
                                //Tính tổng đối với từng dịch vụ
                                foreach ($notCountBasedService['not_countbased_services'] as $service) {
                                    $totalServices["'" . $service['service_id'] . "'"] = $totalServices["'" . $service['service_id'] . "'"] + $service['total'];
                                }
                                foreach ($newClass['not_countbased_services'] as $value) {
                                    $serviceFood[] = $value;
                                }
                                break;
                            }
                        }

                        foreach ($countBasedServices as $countBasedService) {
                            if ($class['class_id'] == $countBasedService['class_id']) {
                                $newClass['countbased_services'] = $countBasedService['countbased_services'];
                                //Tính tổng đối với từng dịch vụ
                                foreach ($countBasedService['countbased_services'] as $service) {
                                    $totalServices["'" . $service['service_id'] . "'"] = $totalServices["'" . $service['service_id'] . "'"] + $service['total'];
                                }
                                foreach ($newClass['countbased_services'] as $value) {
                                    $serviceFood[] = $value;
                                }
                                break;
                            }
                        }
                        $newClass['food_services'] = $serviceFood;
                        $newClasses[] = $newClass;
                    }

                    $smarty->assign('classes', $newClasses);
                    $smarty->assign('service_list', $foodServiceList);
                    $smarty->assign('service_total', $totalServices);
                }

                page_header(__("School Management") . " &rsaquo; " . __("Service") . " &rsaquo; " . __("Daily food service summary"));
                break;

            default:
                _error(404);
                break;
        }
        break;
    case 'services':
        // Tăng số đếm tương tác của trường
        increaseSchoolInteractive($school['page_id'], 'service');
        // Tăng số lượt tương tác của trường - TaiLA
        addInteractive($school['page_id'], 'service', 'school_view');

        switch ($subView) {
            case '':
                page_header(__("School Management") . " &rsaquo; " . __("Service"));
                //Memcache - Lấy ra tất cả dịch vụ của 1 trường
                //$services = $serviceDao->getServices($school['page_id']);
                $services = getSchoolData($school['page_id'], SCHOOL_SERVICES);
                $services = sortArray($services, "ASC", "type");

                $newServices = array();
                foreach ($services as $service) {
                    if ($service['status'] == STATUS_ACTIVE) {
                        $checkUsed = $serviceDao->checkUsedService($service['service_id']);
                        $service['can_delete'] = $checkUsed ? false : true;
                        $newServices[] = $service;
                    }
                }

                $smarty->assign('rows', $newServices);
                break;

            case 'inactive':
                page_header(__("School Management") . " &rsaquo; " . __("Service"));
                $services = $serviceDao->getInActiveSchoolServices($school['page_id']);
                $smarty->assign('rows', $services);
                break;

            case 'add':
                page_header(__("School Management") . " &rsaquo; " . __("Service") . " &rsaquo; " . __("Add New"));
                break;

            case 'edit':
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }
                $data = $serviceDao->getServiceById($_GET['id']);
//                $services = getSchoolData($school['page_id'], SCHOOL_SERVICES);
//                if (!isset($services[$_GET['id']])) {
//                    _error(404);
//                }
//                $data = $services[$_GET['id']];
                $smarty->assign('data', $data);

                page_header(__("School Management") . " &rsaquo; " . __("Service") . " &rsaquo; " . $data['service_name']);
                break;

            default:
                _error(404);
                break;
        }
        break;
    case 'tuitions':
        // Tăng số đếm tương tác của trường
        increaseSchoolInteractive($school['page_id'], 'tuition');
        // Tăng số lượt tương tác của trường - TaiLA
        addInteractive($school['page_id'], 'tuition', 'school_view');

        // get cookie cho học phí
        $name = "guide_tuition_" . $school['page_name'] . "_" . $user->_data['user_id'];
        $cookie = 1;
        if (isset($_COOKIE[$name])) {
            $cookie = $_COOKIE[$name];
        }
        $smarty->assign('cookie', $cookie);
        switch ($subView) {
            case '':
                page_header(__("School Management") . " &rsaquo; " . __("Tuition"));
                $tuitions = $tuitionDao->getSchoolTuitions($school['page_id']);
                $smarty->assign('rows', $tuitions);

                break;
//            case '4leave':
//                if (!$canEdit) {
//                    _error(403);
//                }
//                if(!isset($_GET['id']) || !is_numeric($_GET['id'])) {
//                    _error(404);
//                }
//
//                $data = $tuitionDao->getTuition4Leave($school['page_id'], $_GET['id']);
//                if(is_null($data)) {
//                    _error(404);
//                }
//                $smarty->assign('data', $data);
//
//                $child = getChildData($_GET['id'], CHILD_INFO);
//                if (is_null($child)) {
//                    _error(404);
//                }
//                $smarty->assign('child', $child);
//
//                page_header(__("School Management")." &rsaquo; ".__("Tuition")." &rsaquo; ".__("Account for leave"));
//                break;
            case 'newchild':
                if (!$canEdit) {
                    _error(403);
                }
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }
                // Lấy thông tin tuition
                $data = $tuitionDao->getTuitionOnly($_GET['id']);
                if (is_null($data)) {
                    _error(404);
                }
                $results = $tuitionDao->getTuitionOfNewChildInMonth($data);

                $smarty->assign('results', $results);
                $smarty->assign('attCount', $data['day_of_month']);
                $smarty->assign('data', $data);

                page_header(__("School Management") . " &rsaquo; " . __("Tuition") . " &rsaquo; " . __("Add tuition for new child"));
                break;
            case 'history':
                if (!canView($_GET['username'], $view)) {
                    _error(403);
                }
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }
                $data = $tuitionDao->getTuitionHistory($_GET['id']);
                if (is_null($data)) {
                    _error(404);
                }

                $smarty->assign('data', $data);
                page_header(__("School Management") . " &rsaquo; " . __("Tuition") . " &rsaquo; " . __("History"));
                break;
            case 'add':
                page_header(__("School Management") . " &rsaquo; " . __("Tuition") . " &rsaquo; " . __("Add New"));

                //Memcache
                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);

                $month = toSysMMYYYY($date);
                $attCount = workingDayInMonth($month);
                $smarty->assign('attCount', $attCount);

                $smarty->assign('classes', $classes);

                break;
            case 'edit':
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }
                // Lấy những tuition của tháng hiện tại
                $data = $tuitionDao->getTuition($_GET['id']);
                if (is_null($data)) {
                    _error(404);
                }

                $smarty->assign('data', $data);
                page_header(__("School Management") . " &rsaquo; " . __("Tuition") . " &rsaquo; " . __("Edit"));
                break;
            case 'detail':
                // Tăng số lượt tương tác của trường - TaiLA
                addInteractive($school['page_id'], 'tuition', 'school_view', $_GET['id']);
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }
                // Lấy những tuition của tháng hiện tại
                $data = $tuitionDao->getTuition($_GET['id']);
                if (is_null($data)) {
                    _error(404);
                }

                $smarty->assign('data', $data);
                page_header(__("School Management") . " &rsaquo; " . __("Tuition") . " &rsaquo; " . __("Detail"));
                break;

            case 'export':
                if (!canView($_GET['username'], $view)) {
                    _error(403);
                }
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }
                $data = $tuitionDao->getTuition($_GET['id']);
                if (is_null($data)) {
                    _error(404);
                }
                $smarty->assign('data', $data);
                break;

            case 'export_all':
                if (!canView($_GET['username'], $view)) {
                    _error(403);
                }
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }
                $data = $tuitionDao->getTuition($_GET['id']);
                if (is_null($data)) {
                    _error(404);
                }
                $smarty->assign('data', $data);
                break;
            case 'summarypaid':
                if (!canView($_GET['username'], $view)) {
                    _error(403);
                }
                // Lấy danh sách giáo viên của trường
                $teachers = getSchoolData($school['page_id'], SCHOOL_TEACHERS);
                $managers = array();
                // Lấy danh sách những quản lý trường có thể thanh toán học phí
                $userIds = $roleDao->getUserIdsOfModule($school['page_id'], 'tuitions');
                if ($school['principal'] != '') {
                    $principals = explode(',', $school['principal']);
                    $userIds = array_merge($userIds, $principals);
                }
                $userIds[] = $school['page_admin'];
                $userIds = array_unique($userIds);
                foreach ($teachers as $teacher) {
                    if (in_array($teacher['user_id'], $userIds)) {
                        $managers[] = $teacher;
                    }
                }
                $fromDate = date($system['date_format'], mktime(0, 0, 0, date("m"), 1, date("Y")));

                // Lấy danh sách lớp của trường
                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);

                $smarty->assign('fromDate', $fromDate);
                $smarty->assign('classes', $classes);
                $smarty->assign('managers', $managers);
                page_header(__("School Management") . " &rsaquo; " . __("Tuition") . " &rsaquo; " . __("Summary tuition paid"));
                break;
            default:
                _error(404);
                break;

        }
        break;
    case 'schedules':
        // Tăng số đếm tương tác của trường
        //increaseSchoolInteractive($school['page_id'], 'schedule');

        // Tăng số đếm tương tác lên 1 - Taila - làm lại
        addInteractive($school['page_id'], 'schedule', 'school_view');

        switch ($subView) {
            case '':
                page_header(__("School Management") . " &rsaquo; " . __("Schedule"));
                $rows = $scheduleDao->getScheduleOfSchool($school['page_id']);
                $smarty->assign('rows', $rows);
                //Memcache
                //$class_levels = $classLevelDao->getClassLevels($school['page_id']);
                $class_levels = getSchoolData($school['page_id'], SCHOOL_CLASS_LEVELS);
                //$classes = $classDao->getClassesOfSchool($school['page_id']);
                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                $smarty->assign('class_levels', $class_levels);
                $smarty->assign('classes', $classes);
                break;
            case 'add':
                page_header(__("School Management") . " &rsaquo; " . __("Add schedule"));

                //Memcache
                //$class_levels = $classLevelDao->getClassLevels($school['page_id']);
                $class_levels = getSchoolData($school['page_id'], SCHOOL_CLASS_LEVELS);
                //$classes = $classDao->getClassesOfSchool($school['page_id']);
                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                $smarty->assign('class_levels', $class_levels);
                $smarty->assign('classes', $classes);

                $time = strtotime('monday this week');
                $monday = strftime("%d/%m/%Y", $time);
                $smarty->assign('monday', $monday);
                break;
            case 'edit':
                page_header(__("School Management") . " &rsaquo; " . __("Edit schedule"));
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }
                //Memcache
                //$class_levels = $classLevelDao->getClassLevels($school['page_id']);
                $class_levels = getSchoolData($school['page_id'], SCHOOL_CLASS_LEVELS);
                //$classes = $classDao->getClassesOfSchool($school['page_id']);
                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                $smarty->assign('class_levels', $class_levels);
                $smarty->assign('classes', $classes);
                $data = $scheduleDao->getScheduleDetailById($_GET['id']);
                $smarty->assign('data', $data);
                break;
            case 'copy':
                $canEdit = canEdit($_GET['username'], $view);
                if (!$canEdit) {
                    _error(403);
                }
                page_header(__("School Management") . " &rsaquo; " . __("Add schedule"));

                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }
                //Memcache
                //$class_levels = $classLevelDao->getClassLevels($school['page_id']);
                $class_levels = getSchoolData($school['page_id'], SCHOOL_CLASS_LEVELS);
                //$classes = $classDao->getClassesOfSchool($school['page_id']);
                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                $smarty->assign('class_levels', $class_levels);
                $smarty->assign('classes', $classes);
                $data = $scheduleDao->getScheduleDetailById($_GET['id']);
                $smarty->assign('data', $data);
                break;
            case 'detail':
                // Tăng số lượt tương tác của trường - TaiLA
                addInteractive($school['page_id'], 'schedule', 'school_view', $_GET['id']);
                page_header(__("School Management") . " &rsaquo; " . __("Detail schedule"));
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }
                $data = $scheduleDao->getScheduleDetailById($_GET['id']);
                $smarty->assign('data', $data);

                if ($data['applied_for'] == 2) {
                    //$class_level = $classLevelDao->getClassLevel($data['class_level_id']);
                    $class_level = getClassLevelData($data['class_level_id'], CLASS_LEVEL_INFO);
                    $smarty->assign('class_level', $class_level);
                }

                if ($data['applied_for'] == 3) {
                    //$class = $classDao->getClass($data['class_id']);
                    $class = getClassData($data['class_id'], CLASS_INFO);
                    $smarty->assign('class', $class);
                }
                break;
            case 'import':
                $canEdit = canEdit($_GET['username'], $view);
                if (!$canEdit) {
                    _error(403);
                }
                page_header(__("School Management") . " &rsaquo; " . __("Import from Excel file"));
                //$class_levels = $classLevelDao->getClassLevels($school['page_id']);
                $class_levels = getSchoolData($school['page_id'], SCHOOL_CLASS_LEVELS);
                //$classes = $classDao->getClassesOfSchool($school['page_id']);
                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                $smarty->assign('class_levels', $class_levels);
                $smarty->assign('classes', $classes);

                break;
            default:
                _error(404);
                break;
        }
        break;
    case 'menus':
        // Tăng số đếm tương tác của trường
        increaseSchoolInteractive($school['page_id'], 'menu');
        // Tăng số lượt tương tác của trường - TaiLA
        addInteractive($school['page_id'], 'menu', 'school_view');

        switch ($subView) {
            case '':
                page_header(__("School Management") . " &rsaquo; " . __("Menu"));
                $rows = $menuDao->getMenuOfSchool($school['page_id']);
                $smarty->assign('rows', $rows);
                //$class_levels = $classLevelDao->getClassLevels($school['page_id']);
                $class_levels = getSchoolData($school['page_id'], SCHOOL_CLASS_LEVELS);
                //$classes = $classDao->getClassesOfSchool($school['page_id']);
                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                $smarty->assign('class_levels', $class_levels);
                $smarty->assign('classes', $classes);
                break;
            case 'add':
                page_header(__("School Management") . " &rsaquo; " . __("Add menu"));

                //$class_levels = $classLevelDao->getClassLevels($school['page_id']);
                $class_levels = getSchoolData($school['page_id'], SCHOOL_CLASS_LEVELS);
                //$classes = $classDao->getClassesOfSchool($school['page_id']);
                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                $smarty->assign('class_levels', $class_levels);
                $smarty->assign('classes', $classes);

                $time = strtotime('monday this week');
                $monday = strftime("%d/%m/%Y", $time);
                $smarty->assign('monday', $monday);
                break;
            case 'edit':
                page_header(__("School Management") . " &rsaquo; " . __("Edit menu"));
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }
                //$class_levels = $classLevelDao->getClassLevels($school['page_id']);
                $class_levels = getSchoolData($school['page_id'], SCHOOL_CLASS_LEVELS);
                //$classes = $classDao->getClassesOfSchool($school['page_id']);
                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                $smarty->assign('class_levels', $class_levels);
                $smarty->assign('classes', $classes);
                $data = $menuDao->getMenuDetailById($_GET['id']);
                $smarty->assign('data', $data);
                break;
            case 'copy':
                $canEdit = canEdit($_GET['username'], $view);
                if (!$canEdit) {
                    _error(403);
                }
                page_header(__("School Management") . " &rsaquo; " . __("Add menu"));

                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }
                //$class_levels = $classLevelDao->getClassLevels($school['page_id']);
                $class_levels = getSchoolData($school['page_id'], SCHOOL_CLASS_LEVELS);
                //$classes = $classDao->getClassesOfSchool($school['page_id']);
                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                $smarty->assign('class_levels', $class_levels);
                $smarty->assign('classes', $classes);
                $data = $menuDao->getMenuDetailById($_GET['id']);
                $smarty->assign('data', $data);
                break;
            case 'detail':
                // Tăng số lượt tương tác của trường - TaiLA
                addInteractive($school['page_id'], 'menu', 'school_view', $_GET['id']);
                page_header(__("School Management") . " &rsaquo; " . __("Detail menu"));
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }
                $data = $menuDao->getMenuDetailById($_GET['id']);
                $smarty->assign('data', $data);

                if ($data['applied_for'] == 2) {
                    //$class_level = $classLevelDao->getClassLevel($data['class_level_id']);
                    $class_level = getClassLevelData($data['class_level_id'], CLASS_LEVEL_INFO);
                    $smarty->assign('class_level', $class_level);
                }

                if ($data['applied_for'] == 3) {
                    //$class = $classDao->getClass($data['class_id']);
                    $class = getClassData($data['class_id'], CLASS_INFO);
                    $smarty->assign('class', $class);
                }
                break;
            default:
                _error(404);
                break;
        }
        break;
    case 'reports':
        // Tăng số đếm tương tác của trường
        increaseSchoolInteractive($school['page_id'], 'report');
        // Tăng số lượt tương tác của trường - TaiLA
        addInteractive($school['page_id'], 'report', 'school_view');

        // get cookie cho sổ liên lạc
        $name = "guide_contact_book_" . $school['page_name'] . "_" . $user->_data['user_id'];
        $cookie = 1;
        if (isset($_COOKIE[$name])) {
            $cookie = $_COOKIE[$name];
        }
        $smarty->assign('cookie', $cookie);

        //page_header(__("School Management")." &rsaquo; ".__("Report"));
        switch ($subView) {
            case '':
                page_header(__("School Management") . " &rsaquo; " . __("Contact book"));
                //Lấy ra danh sách lớp
                //$classes = $classDao->getClassesOfSchool($school['page_id']);
                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                $smarty->assign('classes', $classes);
                // Màn hình mặc định ban đầu lấy danh sách của toàn sổ liên lạc toàn trường
                $result = $reportDao->getSchoolReport($school['page_id']);
                $countNotNotify = 0;
                foreach ($result as $row) {
                    if (!$row['is_notified']) {
                        $countNotNotify = $countNotNotify + 1;
                    }
                }

                // Lấy danh sách tiêu chí của trường
                $categories = $reportDao->getAllCategoryOfSchool($school['page_id']);
                $smarty->assign('categories', $categories);

                $smarty->assign('result', $result);
                $smarty->assign('countNotNotify', $countNotNotify);

                break;
            case 'add':
                page_header(__("School Management") . " &rsaquo; " . __("Contact book") . " &rsaquo; " . __("Add New"));

                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);

                // Lấy danh sách mẫu
                $templates = $reportDao->getSchoolReportTemplate($school['page_id']);

                $smarty->assign('classes', $classes);
                $smarty->assign('templates', $templates);
                break;
            case 'edit':
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }
                $data = $reportDao->getReportById($_GET['id']);
                if (is_null($data) || $data['school_id'] != $school['page_id']) {
                    _error(404);
                }
                $smarty->assign('data', $data);
                $templates = $reportDao->getSchoolReportTemplate($school['page_id']);
                $smarty->assign('templates', $templates);
                //$child = $childDao->getChild($data['child_id']);
                $child = getChildData($data['child_id'], CHILD_INFO);
                $smarty->assign('child', $child);

                page_header(__("School Management") . " &rsaquo; " . __("Contact book") . " &rsaquo; " . $data['report_name']);
                break;
            case 'detail':
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }
                $data = $reportDao->getReportById($_GET['id']);
                if (is_null($data) || $data['school_id'] != $school['page_id']) {
                    _error(404);
                }
                $smarty->assign('data', $data);
                //$child = $childDao->getChild($data['child_id']);
                $child = getChildData($data['child_id'], CHILD_INFO);
                $smarty->assign('child', $child);
                //$class = $classDao->getClass($child['class_id']);
                $class = getClassData($child['class_id'], CLASS_INFO);
                $class_name = $class['group_title'];
                $smarty->assign('class_name', $class_name);

                // Start get points list
                //Lấy ra class_id của lớp
                $class_id = $child['class_id'];
                $school_year = date("Y", strtotime($data['created_at'])) . '-' . (date("Y", strtotime($data['created_at'])) + 1);

                // Lấy school_config
                $studentCode = $child['child_code'];
                $schoolConfig = getSchoolData($school['page_id'], SCHOOL_CONFIG);
                // Lấy chi tiết gov_class_level
                $class_level = getClassLevelData($class['class_level_id'], CLASS_LEVEL_INFO);
                // Lấy danh sách điểm của trẻ trong năm học
                $children_point = $pointDao->getChildrenPointsByStudentCode($class_id, $school_year, $studentCode);
                // Lấy child_name

                if (count($children_point) > 0) {
                    $children_point_last = array();
                    // Lưu các điểm đã được tính toán
                    $children_point_avgs = array();
                    // Lưu điểm của các môn thi lại
                    $children_subject_reexams = array();
                    // flag đánh dấu đã đủ điểm các môn hay chưa
                    $flag_enough_point = true;
                    // trạng thái fail/pass hay re-exam hay chưa đủ điểm ( Waiting.. )
                    $child_id = $childDao->getChildIdByCode($child['child_code']);
                    // Lấy số buổi nghỉ có phép và không phép của học sinh
                    $child_absent = $childDao->getAbsentsChild($child_id, $class_id);
                   // điểm trung bình năm
                    $child_avYear_point = 0;
                    $status_result = 'fail';
                    $subject_key = array();
                    $children_point_last = array();
                    // lưu số cột mỗi đầu điểm
                    $countColumnMaxPoint_hk1 = 2;
                    $countColumnMaxPoint_hk2 = 2;
                    $countColumnMaxPoint_gk1 = 1;
                    $countColumnMaxPoint_gk2 = 1;

                    if ($schoolConfig['score_fomula'] == "km") {
                        // điểm các tháng của 2 kỳ
                        $child_m11_point = $child_m12_point = $child_m13_point = $child_m21_point = $child_m22_point = $child_m23_point = 0;
                        // điểm trung bình các tháng của 2 kỳ
                        $child_avM1_point = $child_avM2_point = 0;
                        // điểm trung bình của cuối kỳ
                        $child_avd1_point = 0;
                        $child_avd2_point = 0;
                        $child_final_semester1 = $child_final_semester2 = 0;

                        // Mặc định cột điểm của khmer là 3
                        $countColumnMaxPoint_hk1 = 3;
                        $countColumnMaxPoint_hk2 = 3;
                        foreach ($children_point as $child) {
                            // Nếu bất kì điểm nào thiếu sẽ đánh dấu là chưa đủ điểm
                            if (!isset($child['hs11']) || !isset($child['hs12']) || !isset($child['hs13']) || !isset($child['gk11'])
                                || !isset($child['hs21']) || !isset($child['hs22']) || !isset($child['hs23']) || !isset($child['gk21'])) {
                                $flag_enough_point = false;
                            }
                            // tạm thời tính tổng các đầu điểm
                            // kỳ 1
                            $child_m11_point += (int)$child['hs11'];
                            $child_m12_point += (int)$child['hs12'];
                            $child_m13_point += (int)$child['hs13'];
                            $child_avd1_point += (int)$child['gk11'];
                            // kỳ 2
                            $child_m21_point += (int)$child['hs21'];
                            $child_m22_point += (int)$child['hs22'];
                            $child_m23_point += (int)$child['hs23'];
                            $child_avd2_point += (int)$child['gk21'];

                            // lấy tên môn học
                            $subject_detail = $subjectDao->getSubjectById($child['subject_id']);
                            $child['subject_name'] = $subject_detail['subject_name'];
                            if (isset($subject_detail['re_exam']) && $subject_detail['re_exam']) { // nếu môn đó là môn thi lại thì lưu vào array
                                $subject_reexam['name'] = $subject_detail['subject_name'];
                                $subject_reexam['point'] = $child['re_exam'];
                                $children_subject_reexams[] = $subject_reexam;
                            }

                        $children_point_last[] = $child;
                        }


                        // định nghĩa số bị chia của từng khối
                        if ($class_level['gov_class_level'] == '7' || $class_level['gov_class_level'] == '8') {
                            $dividend = 15;
                        } else if ($class_level['gov_class_level'] == '9') {
                            $dividend = 11.4;
                        } else if ($class_level['gov_class_level'] == '10') {
                            $dividend = 15.26;
                        } else if ($class_level['gov_class_level'] == '11') {
                            $dividend = 16.5;
                        } else if ($class_level['gov_class_level'] == '12') {
                            $dividend = 14.5;
                        }
                        // tính điểm theo công thức từng khối
                        // kỳ 1
                        $child_m11_point /= $dividend;
                        $child_m12_point /= $dividend;
                        $child_m13_point /= $dividend;
                        $child_avd1_point /= $dividend;
                        $child_avM1_point = ($child_m11_point + $child_m12_point + $child_m13_point) / 3;
                        $child_final_semester1 = ($child_avd1_point + $child_avM1_point) / 2;
                        // kỳ 2
                        $child_m21_point /= $dividend;
                        $child_m22_point /= $dividend;
                        $child_m23_point /= $dividend;
                        $child_avd2_point /= $dividend;
                        $child_avM2_point = ($child_m21_point + $child_m22_point + $child_m23_point) / 3;
                        $child_final_semester2 = ($child_avd2_point + $child_avM2_point) / 2;
                        // cả năm
                        $child_avYear_point = ($child_final_semester1 + $child_final_semester2) / 2;
                        // lưu lại vào array
                        $children_point_avgs['a1'] = $child_m11_point;
                        $children_point_avgs['b1'] = $child_m12_point;
                        $children_point_avgs['c1'] = $child_m13_point;
                        $children_point_avgs['d1'] = $child_avd1_point;
                        $children_point_avgs['x1'] = $child_avM1_point;
                        $children_point_avgs['e1'] = $child_final_semester1;
                        $children_point_avgs['a2'] = $child_m21_point;
                        $children_point_avgs['b2'] = $child_m22_point;
                        $children_point_avgs['c2'] = $child_m23_point;
                        $children_point_avgs['d2'] = $child_avd2_point;
                        $children_point_avgs['x2'] = $child_avM2_point;
                        $children_point_avgs['e2'] = $child_final_semester2;
                        $children_point_avgs['y'] = $child_avYear_point;

                        //xét điều kiện để đánh giá pass hay fail
                        if ($child_avYear_point < 25) { // dưới 25 điểm thì fail chặt
                            $status_result = 'Fail';
                        } else { // nếu trên 25 điểm thì xét các điều kiện khác
                            $status_result = 'Pass';
                            if ((isset($child_absent['absent_true']) && (int)$child_absent['absent_true'] >= 55)
                                || (isset($child_absent['absent_false']) && (int)$child_absent['absent_false'] >= 30)
                                || (((int)$child_absent['absent_true'] + (int)$child_absent['absent_false']) >= 55)) {
                                if ($class_level['gov_class_level'] == '7' || $class_level['gov_class_level'] == '8'
                                    || $class_level['gov_class_level'] == '10' || $class_level['gov_class_level'] == '11') {
                                    $status_result = 'Re-exam';
                                } else if ($class_level['gov_class_level'] == '9' || $class_level['gov_class_level'] == '12') {
                                    $status_result = 'Pass';
                                }
                            }
                        }
                        // xét đến đánh giá điểm thi lại
                        if ($status_result == 'Re-exam') {
                            $result_exam = 0;
                            foreach ($children_subject_reexams as $subject_reexam) {
                                if (!isset($subject_reexam['point'])) {
                                    $flag_enough_point = false;
                                } else {
                                    $result_exam += (int)$subject_reexam['point'];
                                }
                            }
                            if ($class_level['gov_class_level'] == '7' || $class_level['gov_class_level'] == '8'
                                || $class_level['gov_class_level'] == '11') {
                                $result_exam /= 4;
                            } else if ($class_level['gov_class_level'] == '10') {
                                $result_exam /= 4;
                            }
                            // nếu điểm thi lại trên 25 điểm thì pass còn lại fail
                            if ($flag_enough_point && $result_exam >= 25) {
                                $status_result = 'Pass';
                            } else if ($flag_enough_point && $result_exam < 25) {
                                $status_result = 'Fail';
                            } else { // chuaw du diem
                                $status_result = 'Re-exam';
                            }
                        }

                        if (count($children_point_last) == 0) {
                            $flag_enough_point = false;
                        }
                        if (!$flag_enough_point && $status_result != 'Re-exam') {
                            $status_result = 'Waiting..';
                        }
                        $subject_key[] = 'hs11';
                        $subject_key[] = 'hs12';
                        $subject_key[] = 'hs13';
                        $subject_key[] = 'gk11';
                        $subject_key[] = 'hs21';
                        $subject_key[] = 'hs22';
                        $subject_key[] = 'hs23';
                        $subject_key[] = 'gk21';
                        $smarty->assign("result_exam", $result_exam);

                    } else if ($schoolConfig['score_fomula'] == "vn") {
                        // điều kiện điểm của môn 1: kém, 2 yếu, 3 trung bình, 4 tiên tiến, 5 giỏi
                        $point_avg1_condition1 = 5;//hk1
                        $point_avg2_condition1 = 5;//hk2
                        $point_avgYearAll_condition1 = 5;// cả năm
                        // điều kiện điểm của môn toán hoặc văn  1: kém, 2 yếu, 3 trung bình, 4 tiên tiến, 5 giỏi
                        $point_avg1_condition2 = 5; //hk1
                        $point_avg2_condition2 = 5; //hk2
                        $point_avgYearAll_condition2 = 5; // cả năm
                        // trạng thái
                        $status_point1 = '';
                        $status_point2 = '';
                        $status_pointAll = '';
                        // flag đánh dấu học sinh đã thi lại hay chưa
                        $flag_reexam = false;
                        // điểm trung bình các môn 2 kỳ
                        $child_TBM1_point = $child_TBM2_point = 0;
                        // điểm trung bình năm
                        $child_avYearAll_point = 0;
                        // TÍnh điểm mỗi môn

                        // lưu số cột mỗi đầu điểm

                            // flag đánh dấu đã đủ điểm các môn hay chưa
                            $flag_enough_point = true;
                            // trạng thái fail/pass hay re-exam hay chưa đủ điểm ( Waiting.. )
                            $status_result = 'fail';

                            // Lấy danh sách điểm tất cả các môn của trẻ trong năm học để tính điêm trung bình năm
                            foreach ($children_point as $each) {
                                if (!isset($each['hs11']) || !isset($each['hs12']) || !isset($each['gk11'])
                                    || !isset($each['hs21']) || !isset($each['hs22']) || !isset($each['gk21'])
                                    || !isset($each['ck1']) || !isset($each['ck2'])) {
                                    $flag_enough_point = false;
                                }
                                // điểm hệ số 2 kỳ
                                $child_hk1_point = $child_hk2_point = 0;
                                $child_TBM1_current_point = $child_TBM2_current_point = 0;
                                // đếm số điểm đã có
                                $count_hk1_point = 0;
                                $count_hk2_point = 0;
                                //Tính điểm trung bình môn

                                for ($i = 1; $i <= 6; $i++) {
                                    // điểm hệ số 1
                                    if (isset($each['hs1' . $i])) {
                                        $child_hk1_point += (int)$each['hs1' . $i];
                                        $count_hk1_point++;
                                        // lưu số cột đã có điểm của hệ số 1 kỳ 1
                                        if ($i > $countColumnMaxPoint_hk1) {
                                            $countColumnMaxPoint_hk1 = $i;
                                        }
                                    }
                                    if (isset($each['hs2' . $i])) {
                                        $child_hk2_point += (int)$each['hs2' . $i];
                                        $count_hk2_point++;
                                        // lưu số cột đã có điểm của hệ số 1 kỳ 2
                                        if ($i > $countColumnMaxPoint_hk2) {
                                            $countColumnMaxPoint_hk2 = $i;
                                        }
                                    }
                                    // điểm hệ số 2
                                    if (isset($each['gk1' . $i])) {
                                        $child_hk1_point += (int)$each['gk1' . $i] * 2;
                                        $count_hk1_point += 2;
                                        // lưu số cột đã có điểm
                                        if ($i > $countColumnMaxPoint_gk1) {
                                            $countColumnMaxPoint_gk1 = $i;
                                        }

                                    }
                                    if (isset($each['gk2' . $i])) {
                                        $child_hk2_point += (int)$each['gk2' . $i] * 2;
                                        $count_hk2_point += 2;
                                        // lưu số cột đã có điểm
                                        if ($i > $countColumnMaxPoint_gk2) {
                                            $countColumnMaxPoint_gk2 = $i;
                                        }
                                    }
                                }
                                //điểm cuối kì

                                if (isset($each['ck1'])) {
                                    $child_hk1_point += (int)$each['ck1'] * 3;
                                    $count_hk1_point += 3;
                                }
                                if (isset($each['ck2'])) {
                                    // Nếu có điểm thi lại thì điểm tổng kết sẽ tính dựa theo điểm thi lại
                                    if(isset($each['re_exam'])) {
                                        $child_hk2_point += (int)$each['re_exam'] * 3;
                                        $flag_reexam = true;
                                    }else {
                                        $child_hk2_point += (int)$each['ck2'] * 3;
                                    }
                                    $count_hk2_point += 3;
                                }
                                if ($count_hk1_point > 0) {
                                    $child_TBM1_current_point = $child_hk1_point / $count_hk1_point;
                                    $each['tb_hk1'] = number_format($child_TBM1_current_point,2);
                                }
                                if ($count_hk2_point > 0) {
                                    $child_TBM2_current_point = $child_hk2_point / $count_hk2_point;
                                    $each['tb_hk2'] = number_format($child_TBM2_current_point,2);
                                }
                                // Điểm tổng kết
                                $child_avYear_point = ($child_TBM1_current_point + $child_TBM2_current_point * 2) / 3;

                                $each['tb_year'] = number_format($child_avYear_point,2);
                                // xét xem điều kiện các môn ở mức nào
                                //học kỳ 1
                                if ($child_TBM1_current_point < 2 && $point_avg1_condition1 > 1) {
                                    $point_avg1_condition1 = 1;
                                } else if ($child_TBM1_current_point < 3.5 && $point_avg1_condition1 > 2) {
                                    $point_avg1_condition1 = 2;
                                } else if ($child_TBM1_current_point < 5 && $point_avg1_condition1 > 3) {
                                    $point_avg1_condition1 = 3;
                                } else if ($child_TBM1_current_point < 6.5 && $point_avg1_condition1 > 4) {
                                    $point_avg1_condition1 = 4;
                                }
                                //học kỳ 2
                                if ($child_TBM2_current_point < 2 && $point_avg2_condition1 > 1) {
                                    $point_avg2_condition1 = 1;
                                } else if ($child_TBM2_current_point < 3.5 && $point_avg2_condition1 > 2) {
                                    $point_avg2_condition1 = 2;
                                } else if ($child_TBM2_current_point < 5 && $point_avg2_condition1 > 3) {
                                    $point_avg2_condition1 = 3;
                                } else if ($child_TBM2_current_point < 6.5 && $point_avg2_condition1 > 4) {
                                    $point_avg2_condition1 = 4;
                                }
                                // cả năm
                                if ($child_avYear_point < 2 && $point_avgYearAll_condition1 > 1) {
                                    $point_avgYearAll_condition1 = 1;
                                } else if ($child_avYear_point < 3.5 && $point_avgYearAll_condition1 > 2) {
                                    $point_avgYearAll_condition1 = 2;
                                } else if ($child_avYear_point < 5 && $point_avgYearAll_condition1 > 3) {
                                    $point_avgYearAll_condition1 = 3;
                                } else if ($child_avYear_point < 6.5 && $point_avgYearAll_condition1 > 4) {
                                    $point_avgYearAll_condition1 = 4;
                                }
                                // xét xem điều kiện của môn toán văn
                                // lấy tên môn học
                                $subject_detail = $subjectDao->getSubjectById($each['subject_id']);
                                $each['subject_name'] = $subject_detail['subject_name'];
                                if (convert_vi_to_en(strtolower(html_entity_decode($subject_detail['subject_name']))) == "toan" || convert_vi_to_en(strtolower(html_entity_decode($subject_detail['subject_name']))) == "van") {
                                    //học kỳ 1
                                    if ($child_TBM1_current_point < 2 && $point_avg1_condition2 > 1) {
                                        $point_avg1_condition2 = 1;
                                    } else if ($child_TBM1_current_point < 3.5 && $point_avg1_condition2 > 2) {
                                        $point_avg1_condition2 = 2;
                                    } else if ($child_TBM1_current_point < 5 && $point_avg1_condition2 > 3) {
                                        $point_avg1_condition2 = 3;
                                    } else if ($child_TBM1_current_point < 6.5 && $point_avg1_condition2 > 4) {
                                        $point_avg1_condition2 = 4;
                                    }
                                    // học kỳ 2
                                    if ($child_TBM2_current_point < 2 && $point_avg2_condition2 > 1) {
                                        $point_avg2_condition2 = 1;
                                    } else if ($child_TBM2_current_point < 3.5 && $point_avg2_condition2 > 2) {
                                        $point_avg2_condition2 = 2;
                                    } else if ($child_TBM2_current_point < 5 && $point_avg2_condition2 > 3) {
                                        $point_avg2_condition2 = 3;
                                    } else if ($child_TBM2_current_point < 6.5 && $point_avg2_condition2 > 4) {
                                        $point_avg2_condition2 = 4;
                                    }
                                    // cả năm
                                    if ($child_avYear_point < 2 && $point_avgYearAll_condition2 > 1) {
                                        $point_avgYearAll_condition2 = 1;
                                    } else if ($child_avYear_point < 3.5 && $point_avgYearAll_condition2 > 2) {
                                        $point_avgYearAll_condition2 = 2;
                                    } else if ($child_avYear_point < 5 && $point_avgYearAll_condition2 > 3) {
                                        $point_avgYearAll_condition2 = 3;
                                    } else if ($child_avYear_point < 6.5 && $point_avgYearAll_condition2 > 4) {
                                        $point_avgYearAll_condition2 = 4;
                                    }
                                }
                                // Cộng vào điểm trung bình các môn
                                $child_TBM1_point += $child_TBM1_current_point;
                                $child_TBM2_point += $child_TBM2_current_point;
                                $child_avYearAll_point += $child_avYear_point;
                                $children_point_last[] = $each;
                            }
                            // điểm trung bình các môn
                            $child_TBM1_point /= count($children_point);
                            $child_TBM2_point /= count($children_point);
                            $child_avYearAll_point /= count($children_point);

                            //xét điều kiện để đánh giá pass hay fail
                            //học kỳ 1
                            if ($child_TBM1_point >= 8 && $point_avg1_condition1 == 5 && $point_avg1_condition2 == 5) { // trên 8 điểm thì đánh giá loại giỏi
                                $status_point1 = 5;
                            } else if ($child_TBM1_point >= 8 && ($point_avg1_condition1 == 3 || $point_avg1_condition2 == 3)) {
                                $status_point1 = 4;
                            } else if ($child_TBM1_point >= 8 && ($point_avg1_condition1 == 2 || $point_avg1_condition2 == 2)) {
                                $status_point1 = 3;
                            } else if ($child_TBM1_point >= 6.5 && $point_avg1_condition1 >= 4 && $point_avg1_condition2 >= 4) {  // trên 6.5 điểm thì đánh giá loại tiên tiến
                                $status_point1 = 4;
                            } else if ($child_TBM1_point >= 6.5 && ($point_avg1_condition1 == 2 || $point_avg1_condition2 == 2)) {  // trên 6.5 điểm thì đánh giá loại tiên tiến
                                $status_point1 = 3;
                            } else if ($child_TBM1_point >= 6.5 && ($point_avg1_condition1 == 1 || $point_avg1_condition2 == 1)) {  // trên 6.5 điểm thì đánh giá loại tiên tiến
                                $status_point1 = 2;
                            } else if ($child_TBM1_point >= 5 && $point_avg1_condition1 >= 3 && $point_avg1_condition2 >= 3) {  // trên 5 điểm thì đánh giá loại trung bình
                                $status_point1 = 3;
                            } else if ($child_TBM1_point >= 3.5 && $point_avg1_condition1 >= 2 && $point_avg1_condition2 >= 2) {  // trên 3.5 điểm thì đánh giá loại yếu
                                $status_point1 = 2;
                            } else if ($child_TBM1_point < 3.5 && $point_avg1_condition1 >= 1 && $point_avg1_condition2 >= 1) {
                                $status_point1 = 1;
                            }
                            // học kỳ 2
                            if ($child_TBM2_point >= 8 && $point_avg2_condition1 == 5 && $point_avg2_condition2 == 5) { // trên 8 điểm thì đánh giá loại giỏi
                                $status_point2 = 5;
                            } else if ($child_TBM2_point >= 8 && ($point_avg2_condition1 == 3 || $point_avg2_condition2 == 3)) {
                                $status_point2 = 4;
                            } else if ($child_TBM2_point >= 8 && ($point_avg2_condition1 == 2 || $point_avg2_condition2 == 2)) {
                                $status_point2 = 3;
                            } else if ($child_TBM2_point >= 6.5 && $point_avg2_condition1 >= 4 && $point_avg2_condition2 >= 4) {  // trên 6.5 điểm thì đánh giá loại tiên tiến
                                $status_point2 = 4;
                            } else if ($child_TBM2_point >= 6.5 && ($point_avg2_condition1 == 2 || $point_avg2_condition2 == 2)) {  // trên 6.5 điểm thì đánh giá loại tiên tiến
                                $status_point2 = 3;
                            } else if ($child_TBM2_point >= 6.5 && ($point_avg2_condition1 == 1 || $point_avg2_condition2 == 1)) {  // trên 6.5 điểm thì đánh giá loại tiên tiến
                                $status_point2 = 2;
                            } else if ($child_TBM2_point >= 5 && $point_avg2_condition1 >= 3 && $point_avg2_condition2 >= 3) {  // trên 5 điểm thì đánh giá loại trung bình
                                $status_point2 = 3;
                            } else if ($child_TBM2_point >= 3.5 && $point_avg2_condition1 >= 2 && $point_avg2_condition2 >= 2) {  // trên 3.5 điểm thì đánh giá loại yếu
                                $status_point2 = 2;
                            } else if ($child_TBM2_point < 3.5 && $point_avg2_condition1 >= 1 && $point_avg2_condition2 >= 1) {
                                $status_point2 = 1;
                            }
                            // cả năm
                            if ($child_avYearAll_point >= 8 && $point_avgYearAll_condition1 == 5 && $point_avgYearAll_condition2 == 5) { // trên 8 điểm thì đánh giá loại giỏi
                                $status_pointAll = 5;
                            } else if ($child_avYearAll_point >= 8 && ($point_avgYearAll_condition1 == 3 || $point_avgYearAll_condition2 == 3)) {
                                $status_pointAll = 4;
                            } else if ($child_avYearAll_point >= 8 && ($point_avgYearAll_condition1 == 2 || $point_avgYearAll_condition2 == 2)) {
                                $status_pointAll = 3;
                            } else if ($child_avYearAll_point >= 6.5 && $point_avgYearAll_condition1 >= 4 && $point_avgYearAll_condition2 >= 4) {  // trên 6.5 điểm thì đánh giá loại tiên tiến
                                $status_pointAll = 4;
                            } else if ($child_avYearAll_point >= 6.5 && ($point_avgYearAll_condition1 == 2 || $point_avgYearAll_condition2 == 2)) {  // trên 6.5 điểm thì đánh giá loại tiên tiến
                                $status_pointAll = 3;
                            } else if ($child_avYearAll_point >= 6.5 && ($point_avgYearAll_condition1 == 1 || $point_avgYearAll_condition2 == 1)) {  // trên 6.5 điểm thì đánh giá loại tiên tiến
                                $status_pointAll = 2;
                            } else if ($child_avYearAll_point >= 5 && $point_avgYearAll_condition1 >= 3 && $point_avgYearAll_condition2 >= 3) {  // trên 5 điểm thì đánh giá loại trung bình
                                $status_pointAll = 3;
                            } else if ($child_avYearAll_point >= 3.5 && $point_avgYearAll_condition1 >= 2 && $point_avgYearAll_condition2 >= 2) {  // trên 3.5 điểm thì đánh giá loại yếu
                                $status_pointAll = 2;
                            } else if ($child_avYearAll_point < 3.5 && $point_avgYearAll_condition1 >= 1 && $point_avgYearAll_condition2 >= 1) {
                                $status_pointAll = 1;
                            }

                            // Lấy hạnh kiểm của học sinh
                            $conduct = $conductDao->getConductOfChildren($class_id, $child_id, $school_year);
                            if (!isset($conduct) || !isset($conduct['ck'])) {
                                $flag_enough_point = false;
                            } else if ($child_absent['absent_true'] + $child_absent['absent_false'] <= 45
                                && $status_pointAll >= 3 && $conduct['ck'] != "Weak") {
                                $status_result = "pass";
                            }

                            if (!$flag_enough_point) {
                                $status_result = "waiting..";
                            }
//                            $children_point_last[] = $child;

                        // setup subject_key
                        for ($i = 1; $i <= $countColumnMaxPoint_hk1; $i++) {
                            $subject_key[] = 'hs1' . $i;
                        }
                        for ($i = 1; $i <= $countColumnMaxPoint_gk1; $i++) {
                            $subject_key[] = 'gk1' . $i;
                        }
                        $subject_key[] = 'ck1';
                        $subject_key[] = 'tb_hk1';
                        for ($i = 1; $i <= $countColumnMaxPoint_hk2; $i++) {
                            $subject_key[] = 'hs2' . $i;
                        }
                        for ($i = 1; $i <= $countColumnMaxPoint_gk2; $i++) {
                            $subject_key[] = 'gk2' . $i;
                        }
                        $subject_key[] = 'ck2';
                        $subject_key[] = 'tb_hk2';
                        $subject_key[] = 're_exam';
                        $smarty->assign("tb_total_hk1", $child_TBM1_point);
                        $smarty->assign("tb_total_hk2", $child_TBM2_point);
                        $smarty->assign("tb_total_year", $child_avYearAll_point);
                    }
                    $smarty->assign("column_hk1", $countColumnMaxPoint_hk1);
                    $smarty->assign("column_hk2", $countColumnMaxPoint_hk2);
                    $smarty->assign("column_gk1", $countColumnMaxPoint_gk1);
                    $smarty->assign("column_gk2", $countColumnMaxPoint_gk2);
                    $smarty->assign("rows", $children_point_last);
                    $smarty->assign("subject_key", $subject_key);
                    $smarty->assign("status", $status_result);
                    $smarty->assign("child_absent", $child_absent);
                    $smarty->assign("children_point_avgs", $children_point_avgs);
                    $smarty->assign("children_subject_reexams", $children_subject_reexams);
                }
                $smarty->assign("point_module", $schoolConfig['points']);
                $smarty->assign("grade", $schoolConfig['grade']);
                $smarty->assign('score_fomula', $schoolConfig['score_fomula']);

                // End points list

                addInteractive($school['page_id'], 'report', 'school_view', $_GET['id']);
                page_header(__("School Management") . " &rsaquo; " . __("Contact book") . " &rsaquo; " . $data['report_name']);
                break;
            case
            'listtemp':
                if (!canView($_GET['username'], $view)) {
                    _error(403);
                }
                page_header(__("School Management") . " &rsaquo; " . __("Contact book") . " &rsaquo; " . __('List template'));
                $results = $reportDao->getSchoolReportTemplate($school['page_id']);
                $smarty->assign('results', $results);
                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                $smarty->assign('classes', $classes);

                $classLevels = getSchoolData($school['page_id'], SCHOOL_CLASS_LEVELS);
                $smarty->assign('class_levels', $classLevels);
                break;
            case 'edittemp':
                $canView = canView($_GET['username'], $view);
                if (!$canView) {
                    _error(403);
                }
                page_header(__("School Management") . " &rsaquo; " . __("Contact book") . " &rsaquo; " . __('Edit template'));

                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }
                $temp = $reportDao->getReportTemplate($_GET['id']);
                if (is_null($temp) || $temp['school_id'] != $school['page_id']) {
                    _error(404);
                }

                $tempCategoryIds = array();
                foreach ($temp['details'] as $detail) {
                    $tempCategoryIds[] = $detail['report_template_category_id'];
                }
                // Lấy danh sách các hạng mục của trường
                $categorys = $reportDao->getAllCategoryOfSchool($school['page_id']);
                $categorysTemp = array();
                // Lấy những tiêu chí được chọn trước đó cho lên trên
                foreach ($categorys as $category) {
                    if (in_array($category['report_template_category_id'], $tempCategoryIds)) {
                        $category['checked'] = 1;
                        // Gắn vị trí tương ứng trong những tiêu chí được chọn
                        foreach ($tempCategoryIds as $k => $tempCategoryId) {
                            if ($category['report_template_category_id'] == $tempCategoryId) {
                                $category['position'] = $k + 1;
                            }
                        }
                        foreach ($temp['details'] as $detail) {
                            if ($detail['report_template_category_id'] == $category['report_template_category_id']) {
                                $category['template_content'] = $detail['template_content'];
                            }
                            //$tempCategoryIds[] = $detail['report_template_category_id'];
                            // Không hiểu câu lệnh trên để làm gì, comment tạm lại đó
                        }
                        $categorysTemp[] = $category;
                    }
                }
                // Gắn key cho mảng và sắp xếp theo key
                $cateTemp = array();
                foreach ($categorysTemp as $item) {
                    $cateTemp[$item['position']] = $item;
                }
                ksort($cateTemp);

                // Lấy những tiêu chí không được chọn trước đây cho xuống dưới
                foreach ($categorys as $category) {
                    if (!in_array($category['report_template_category_id'], $tempCategoryIds)) {
                        $category['checked'] = 0;
                        $cateTemp[] = $category;
                    }
                }
                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                $smarty->assign('classes', $classes);

                $classLevels = getSchoolData($school['page_id'], SCHOOL_CLASS_LEVELS);
                $smarty->assign('classLevels', $classLevels);
                $smarty->assign('data', $temp);
                $smarty->assign('categorysTemp', $cateTemp);
                break;
            case 'addtemp':
                $canEdit = canEdit($_GET['username'], $view);
                if (!$canEdit) {
                    _error(403);
                }
                page_header(__("School Management") . " &rsaquo; " . __("Contact book") . " &rsaquo; " . __('Add new template'));

                // Lấy danh sách các hạng mục của trường
                $categorys = $reportDao->getAllCategoryOfSchool($school['page_id']);

                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                $class_levels = getSchoolData($school['page_id'], SCHOOL_CLASS_LEVELS);
                $smarty->assign('classes', $classes);
                $smarty->assign('class_levels', $class_levels);
                $smarty->assign('categorys', $categorys);
                break;
            case 'listcate':
                if (!canView($_GET['username'], $view)) {
                    _error(403);
                }
                page_header(__("School Management") . " &rsaquo; " . __("Contact book") . " &rsaquo; " . __('List category'));
                $categorys = $reportDao->getAllCategoryOfSchool($school['page_id']);
                $smarty->assign('categorys', $categorys);
                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                $smarty->assign('classes', $classes);
                break;
            case 'addcate':
                if (!canEdit($_GET['username'], $view)) {
                    _error(403);
                }
                page_header(__("School Management") . " &rsaquo; " . __("Contact book") . " &rsaquo; " . __('Add new category'));
                break;
            case 'editcate':
                if (!canView($_GET['username'], $view)) {
                    _error(403);
                }
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }

                if (!isset($_GET['p5']) || !is_numeric($_GET['p5'])) {
                    _error(404);
                }
                page_header(__("School Management") . " &rsaquo; " . __("Contact book") . " &rsaquo; " . __('Category suggests'));
                // Lấy chi tiết hạng mục
                $categoryDetail = $reportDao->getReportTemplateCategoryDetail($_GET['id']);
                $smarty->assign('data', $categoryDetail);

                // Assign biến p5 để xem sau khi lưu nó back về đâu
                // 1. back về danh sách hạng mục
                // 2. Back về template
                $smarty->assign('p5', $_GET['p5']);
                // Assign id của template để back về template đấy
                if (isset($_GET['p6'])) {
                    $smarty->assign('p6', $_GET['p6']);
                }
                break;

            default:
                _error(404);
                break;
        }


        break;
    case 'points':
        switch ($subView) {
            case 'listsbysubject':
            case 'listsbystudent':
                page_header(__("School Management") . " &rsaquo; " . __("Point"));
                // Lấy danh sách khối
                $class_levels = getSchoolData($school['page_id'], SCHOOL_CLASS_LEVELS);

                //Lấy ra danh sách lớp
                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);

                $smarty->assign('classes', $classes);
                $smarty->assign('class_levels', $class_levels);
//                $smarty->assign('default_subjects', $default_subjects);
                $smarty->assign('semesters', $semesters);
//                $smarty->assign('grade', $school['grade']);
                $smarty->assign('default_year', $default_school_year);
                break;

            case 'assign':
                page_header(__("School Management") . " &rsaquo; " . __("Point"));

                // Lấy danh sách khối
                $class_levels = getSchoolData($school['page_id'], SCHOOL_CLASS_LEVELS);

                //Lấy ra danh sách lớp
                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                unset($semesters['0']);
                $smarty->assign('classes', $classes);
                $smarty->assign('class_levels', $class_levels);
//                $smarty->assign('default_subjects', $default_subjects);
                $smarty->assign('semesters', $semesters);
                $smarty->assign('default_year', $default_school_year);
                break;

            case 'comment':
                page_header(__("School Management") . " &rsaquo; " . __("Point"));

                // Lấy danh sách khối
                $class_levels = getSchoolData($school['page_id'], SCHOOL_CLASS_LEVELS);

                //Lấy ra danh sách lớp
                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                $smarty->assign('classes', $classes);
                $smarty->assign('class_levels', $class_levels);
//                $smarty->assign('default_subjects', $default_subjects);
                $smarty->assign('semesters', $semesters);
                $smarty->assign('default_year', $default_school_year);
//                $smarty->assign('grade', $school['grade']);
                break;
            case 'importManual':
                global $default_school_year;
                page_header(__("School Management") . " &rsaquo; " . __("Point"));
                // Lấy danh sách khối
                $class_levels = getSchoolData($school['page_id'], SCHOOL_CLASS_LEVELS);

                //Lấy ra danh sách lớp
                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                // Lấy schoolConfig
                $schoolConfig = getSchoolData($school['page_id'], SCHOOL_CONFIG);
                $smarty->assign('classes', $classes);
                $smarty->assign('class_levels', $class_levels);
//                $smarty->assign('default_subjects', $default_subjects);
                $smarty->assign('score_fomula', $schoolConfig['score_fomula']);
                $smarty->assign('semesters', $semesters);
                $smarty->assign('default_year', $default_school_year);
                break;
            case 'importExcel':
                page_header(__("School Management") . " &rsaquo; " . __("Point"));
                // Lấy danh sách khối
                $class_levels = getSchoolData($school['page_id'], SCHOOL_CLASS_LEVELS);

                //Lấy ra danh sách lớp
                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);

                $smarty->assign('classes', $classes);
                $smarty->assign('class_levels', $class_levels);
//                $smarty->assign('default_subjects', $default_subjects);
//                $smarty->assign('grade', $school['grade']);
                $smarty->assign('semesters', $semesters);
                $smarty->assign('default_year', $default_school_year);
                break;
            default:
                _error(404);
                break;
        }
        break;
    // ADD START MANHDD 05/06/2021
    case 'conducts':
        addInteractive($school['page_id'], 'conduct', 'school_view');
        switch ($_GET['sub_view']) {
            case '':
                page_header(__("School Management") . " &rsaquo; " . __("Conducts"));
                //Lấy ra danh sách lớp
                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                $smarty->assign('classes', $classes);
                $smarty->assign('default_year', $default_school_year);

                break;
            case 'edit':
                global $default_conducts;
                page_header(__("Class Management") . " &rsaquo; " . __("Conducts") . " &rsaquo; " . __('Edit'));
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }

                $data = $conductDao->getConductById($_GET['id']);
                $child = getChildData($data['child_id'], CHILD_INFO);
                if (is_null($data) || !$child['school_id']) {
                    _error(404);
                }
//                $conducts = $default_conducts;

                $smarty->assign('data', $data);
                $smarty->assign('child', $child);
                $smarty->assign('conducts', $default_conducts);
                break;
            default:
                _error(404);
                break;
        }
        break;
        // ADD END MANHDD 05/06/2021
    // ADD START MANHDD 17/06/2021
    case 'courses':
        addInteractive($school['page_id'], 'course', 'school_view');
        switch ($_GET['sub_view']) {
            case '':
                global $default_school_year;
                page_header(__("School Management") . " &rsaquo; " . __("Course"));
//                global $default_school_year;
                // Lấy danh sách khối
                $class_levels = getSchoolData($school['page_id'], SCHOOL_CLASS_LEVELS);

//                //Lấy ra danh sách lớp
//                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);

//                $smarty->assign('classes', $classes);
                $smarty->assign('class_levels', $class_levels);
//                $smarty->assign('default_subjects', $default_subjects);
                $smarty->assign('default_year', $default_school_year);
                break;
            case 'edit':
                global $default_conducts;
                page_header(__("Class Management") . " &rsaquo; " . __("Conducts") . " &rsaquo; " . __('Edit'));
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }

                $data = $conductDao->getConductById($_GET['id']);
                $child = getChildData($data['child_id'], CHILD_INFO);
                if (is_null($data) || !$child['school_id']) {
                    _error(404);
                }
//                $conducts = $default_conducts;

                $smarty->assign('data', $data);
                $smarty->assign('child', $child);
                $smarty->assign('conducts', $default_conducts);
                break;
            default:
                _error(404);
                break;
        }
        break;
    // ADD END MANHDD 17/06/2021
    case 'feedback':
        // Tăng số đếm tương tác của trường
        increaseSchoolInteractive($school['page_id'], 'feedback');
        // Tăng số lượt tương tác của trường - TaiLA
        addInteractive($school['page_id'], 'feedback', 'school_view');

        switch ($subView) {
            case '':
                page_header(__("School Management") . " &rsaquo; " . __("Feedback"));
                //Lấy ra danh sách feedback
                $feedbacks = $feedbackDao->getFeedbackOfSchool($school['page_id']);

                $smarty->assign('rows', $feedbacks);

                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                $smarty->assign('classes', $classes);
                break;

            default:
                _error(404);
                break;
        }


        break;
    case 'pickup':
        // Tăng số đếm tương tác của trường
        increaseSchoolInteractive($school['page_id'], 'pickup');
        // Tăng số lượt tương tác của trường - TaiLA
        addInteractive($school['page_id'], 'pickup', 'school_view');

        // get cookie cho đón muộn
        $name = "guide_pickup_" . $school['page_name'] . "_" . $user->_data['user_id'];
        $cookie = 1;
        if (isset($_COOKIE[$name])) {
            $cookie = $_COOKIE[$name];
        }
        $smarty->assign('cookie', $cookie);
        switch ($subView) {
            case 'template':
                $canEdit = canEdit($_GET['username'], $view);
                if (!$canEdit) {
                    _error(403);
                }
                page_header(__("School Management") . " &rsaquo; " . __("Late pickup") . " &rsaquo; " . __("Setting and create price table"));

                $template = $pickupDao->getTemplate($school['page_id'], true, true, true);
                $teachers = $pickupDao->getPickupTeachers($school['page_id']);
                // assign variables
                $smarty->assign('template', $template);
                $smarty->assign('teachers', $teachers);
                break;

            case 'assign':
                if (!$is_configured) {
                    _error(404);
                }
                $canEdit = canEdit($_GET['username'], $view);
                if (!$canEdit) {
                    _error(403);
                }
                page_header(__("School Management") . " &rsaquo; " . __("Late pickup") . " &rsaquo; " . __("Assign"));

                $template = $pickupDao->getTemplate($school['page_id']);
                $classes = $pickupDao->getPickupClasses($school['page_id']);

                $pickupClassId = !empty($classes) ? $classes[0]['pickup_class_id'] : 0;

                if ($template['assign_type'] == 'week') {
                    $fromDate = date('Y-m-d', strtotime('monday this week'));
                    $toDate = date('Y-m-d', strtotime("+6 day", strtotime($fromDate)));
                } else {
                    $fromDate = date('Y-m-01');
                    $toDate = date('Y-m-t');
                }

                $results = $pickupDao->getAssignHistory($school['page_id'], $pickupClassId, $fromDate, $toDate);

                $fDate = strtotime($fromDate);
                $tDate = strtotime($toDate);

                $dayOfFromDate = date('w', $timestamp);

                $displayDates = array();
                $displayDays = array();
                while ($fDate <= $tDate) {
                    $day = $pickupDao->getDayShortOfDate($fDate);
                    $displayDays[] = $day;

                    $parts = explode("-", date("Y-m-d", $fDate));
                    $displayDates[] = $parts[2];

                    //$dates[] = $fromDate;
                    $fDate = strtotime("+1 day", $fDate);
                }

                $smarty->assign('dates', $displayDates);
                $smarty->assign('days', $displayDays);

                //Mảng 2 chiều lưu bảng phân công toàn trường
                $newResults = array();

                //Băm danh sách kết quả thành mảng 2 chiều theo từng giáo viên
                $lastTeacherId = -1;
                $aRow = array(); //Dòng lưu thông tin phân công của 1 giáo viên (chỉ những ngày đc phân công).

                $assignTime = array();
                foreach ($results as $result) {
                    //Nếu user_id thay đổi thì bắt đầu một dòng mới.
                    if ($lastTeacherId != $result['user_id']) {
                        //Nếu trước đó là một dòng (không phải là dòng đầu tiên), thì xử lý bổ xung dòng đã xong trước đó.
                        if ($lastTeacherId > 0) {
                            $isFirstCell = true; //Check có phải cell đầu dòng hay không
                            $newRow = array(); //Dòng lưu thông tin phân công 1 giáo viên (bao gồm những ngày không phân công).
                            $newCells = array();
                            $lastDate = strtotime($fromDate);
                            $dateRow = $fromDate;

                            foreach ($aRow as $cell) {
                                if (is_empty($cell['assign_time'])) {
                                    $newCells[] = array("is_checked" => 0, 'date_row' => date('Y-m-d', strtotime($dateRow)));
                                    $dateRow = date('Y-m-d', strtotime("+1 day", strtotime($dateRow)));
                                } else {
                                    //Xét ngày đầu hàng là ngày ko được phân công thì thêm thành phần vào
                                    while ($isFirstCell && $lastDate < strtotime($cell['assign_time'])) {
                                        $newCells[] = array("is_checked" => 0, 'date_row' => date('Y-m-d', strtotime($dateRow)));
                                        $dateRow = date('Y-m-d', strtotime("+1 day", strtotime($dateRow)));

                                        $lastDate = strtotime("+1 day", $lastDate);
                                    }

                                    //Xét thành phần giữa hàng, nếu có ngày không được phân công thì chèn thêm vào
                                    while ((!$isFirstCell) && strtotime("+1 day", $lastDate) < strtotime($cell['assign_time'])) {
                                        $newCells[] = array("is_checked" => 0, 'date_row' => date('Y-m-d', strtotime($dateRow)));
                                        $dateRow = date('Y-m-d', strtotime("+1 day", strtotime($dateRow)));

                                        $lastDate = strtotime("+1 day", $lastDate);
                                    }
                                    $lastDate = strtotime($cell['assign_time']);
                                }


                                if ($cell['pickup_id']) {
                                    $newCells[] = array("is_checked" => 1, 'date_row' => date('Y-m-d', strtotime($dateRow)));
                                    $dateRow = date('Y-m-d', strtotime("+1 day", strtotime($dateRow)));
                                }
                                $newRow['teacher_name'] = $cell['user_fullname'];
                                $newRow['user_id'] = $cell['user_id'];
                                $isFirstCell = false;

                            }
                            //Xét cuối hàng, nếu những ngày cuối hàng là ngày nghỉ thì thêm ô trống
                            while ($lastDate < strtotime($toDate)) {
                                $newCells[] = array("is_checked" => 0, 'date_row' => date('Y-m-d', strtotime($dateRow)));
                                $dateRow = date('Y-m-d', strtotime("+1 day", strtotime($dateRow)));

                                $lastDate = strtotime("+1 day", $lastDate);
                            }
                            $newRow['cells'] = $newCells;
                            $newResults[] = $newRow;
                        }
                        $aRow = array();
                    }

                    $assignTime[strtotime($result['assign_time'])][] = $result;
                    $aRow[] = $result;
                    $lastTeacherId = $result['user_id'];
                }

                //Xử lý hàng cuối cùng (giáo viên cuối cùng)
                if ($lastTeacherId > 0) {
                    $isFirstCell = true;
                    $newRow = array(); //Dòng lưu thông phần công của giáo viên (bao gồm những ngày không phân công).
                    $newCells = array();
                    $lastDate = strtotime($fromDate);
                    $dateRow = $fromDate;

                    foreach ($aRow as $cell) {
                        if (is_empty($cell['assign_time'])) {
                            $newCells[] = array("is_checked" => 0, 'date_row' => date('Y-m-d', strtotime($dateRow)));
                            $dateRow = date('Y-m-d', strtotime("+1 day", strtotime($dateRow)));
                        } else {
                            //Xét ngày đầu hàng là ngày ko được phân công thì thêm thành phần vào
                            while ($isFirstCell && $lastDate < strtotime($cell['assign_time'])) {
                                $newCells[] = array("is_checked" => 0, 'date_row' => date('Y-m-d', strtotime($dateRow)));
                                $dateRow = date('Y-m-d', strtotime("+1 day", strtotime($dateRow)));

                                $lastDate = strtotime("+1 day", $lastDate);
                            }

                            //Xét thành phần giữa hàng, nếu có ngày không được phân công thì chèn thêm vào
                            while ((!$isFirstCell) && strtotime("+1 day", $lastDate) < strtotime($cell['assign_time'])) {
                                $newCells[] = array("is_checked" => 0, 'date_row' => date('Y-m-d', strtotime($dateRow)));
                                $dateRow = date('Y-m-d', strtotime("+1 day", strtotime($dateRow)));

                                $lastDate = strtotime("+1 day", $lastDate);
                            }
                            $lastDate = strtotime($cell['assign_time']);
                        }


                        if ($cell['pickup_id']) {
                            $newCells[] = array("is_checked" => 1, 'date_row' => date('Y-m-d', strtotime($dateRow)));
                            $dateRow = date('Y-m-d', strtotime("+1 day", strtotime($dateRow)));
                        }
                        $newRow['teacher_name'] = $cell['user_fullname'];
                        $newRow['user_id'] = $cell['user_id'];
                        $isFirstCell = false;
                    }

                    //Xét cuối hàng, nếu những ngày cuối hàng là ngày nghỉ thì thêm ô trống
                    while ($lastDate < strtotime($toDate)) {
                        $newCells[] = array("is_checked" => 0, 'date_row' => date('Y-m-d', strtotime($dateRow)));
                        $dateRow = date('Y-m-d', strtotime("+1 day", strtotime($dateRow)));

                        $lastDate = strtotime("+1 day", $lastDate);
                    }
                    $newRow['cells'] = $newCells;
                    $newResults[] = $newRow;
                }

                $twoLastRows = array(); //Biến này lưu tổng kết số lượng phân công theo ngày của trường.

                $timeFromDate = strtotime($fromDate);
                $timeToDate = strtotime($toDate);
                for ($i = $timeFromDate; $i <= $timeToDate; $i = strtotime("+1 day", $i)) {
                    if (isset($assignTime[$i])) {
                        $twoLastRows[] = array("is_checked" => 1, 'pickup_count' => count($assignTime[$i]));
                    } else {
                        $twoLastRows[] = array("is_checked" => 0);
                    }
                }

                $monday = date('d/m/Y', strtotime('monday this week'));
                $smarty->assign('monday', $monday);
                $smarty->assign('rows', $newResults);
                $smarty->assign('last_rows', $twoLastRows);

                // assign variables
                $smarty->assign('classes', $classes);
                $smarty->assign('is_repeat', $template['is_repeat']);
                $smarty->assign('assign_type', $template['assign_type']);
                break;

            case 'list':
                if (!$is_configured) {
                    _error(404);
                }
                if (!canView($_GET['username'], $view)) {
                    _error(403);
                }
                page_header(__("School Management") . " &rsaquo; " . __("Late pickup") . " &rsaquo; " . __("Lists"));

                $toDate = date('d/m/Y');
                $fromDate = date('01/m/Y');
                $pickups = $pickupDao->getPickupInMonth($school['page_id'], date('Y-m-01'), date('Y-m-d'));

                /*echo '<pre>';
                print_r($pickups);
                echo '<pre>';die;*/
                $smarty->assign('toDate', $toDate);
                $smarty->assign('fromDate', $fromDate);
                $smarty->assign('pickups', $pickups);
                break;

            case 'child':
                if (!$is_configured) {
                    _error(404);
                }
                if (!canView($_GET['username'], $view)) {
                    _error(403);
                }
                $data = array();
                $results = array();
                $toDate = date($system['date_format']);
                $fromDate = date($system['date_format'], mktime(0, 0, 0, date("m"), 1, date("Y")));
                $data['fromDate'] = $fromDate;
                $data['toDate'] = $toDate;

                //$classes = $classDao->getClassNamesOfSchool($school['page_id']);
                //Memcache - lấy ra danh sách lớp của trường
                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                $data['classes'] = $classes;

                if (is_numeric($_GET['id']) && $_GET['id'] > 0) {
                    //$child = $childDao->getChild($_GET['id']);
                    //Memcache - lấy thông tin của trẻ
                    $child = getChildData($_GET['id'], CHILD_INFO);
                    $data['child'] = $child;

                    $total = 0;
                    $results = $pickupDao->getChildPickupHistory($child['school_id'], $child['child_id'], toDBDate($fromDate), toDBDate($toDate), $total);

                    $smarty->assign('results', $results);
                    $smarty->assign('total', $total);

                    page_header(__("School Management") . " &rsaquo; " . __("Late pickup") . " &rsaquo; " . $child['child_name']);
                } else {
                    page_header(__("School Management") . " &rsaquo; " . __("Late pickup") . " &rsaquo; " . __("Child"));
                }

                $smarty->assign('data', $data);
                break;

            case 'add':
                if (!$is_configured) {
                    _error(404);
                }
                $canEdit = canEdit($_GET['username'], $view);
                if (!$canEdit) {
                    _error(403);
                }
                page_header(__("School Management") . " &rsaquo; " . __("Late pickup") . " &rsaquo; " . __("Add child"));

                //$classes = $classDao->getClassesOfSchool($school['page_id']);
                $classes = array();
                $classOfSchool = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                foreach ($classOfSchool as $class) {
                    $classes[] = $class;
                }
                $smarty->assign('classes', $classes);

                $child_count = 0;
                $pickupId = 0;
                $first_pickup_class = 0;
                $pickupClass = array();
                $childrenRegistered = array();

                if (isset($_GET['id'])) {
                    $pickup = $pickupDao->getPickup($_GET['id']);
                    $date = $pickup['pickup_time'];
                    $pickupId = $pickup['pickup_id'];
                    if ($pickup['pickup_class_id'] > 0) {
                        $pickupClass[] = $pickupDao->getPickupClass($pickup['pickup_class_id']);
                    }
                    $first_pickup_class = isset($pickupClass[0]['pickup_class_id']) ? $pickupClass[0]['pickup_class_id'] : 0;
                } else {
                    $date = isset($_SESSION['pickup_time_edit']) ? $_SESSION['pickup_time_edit'] : date('Y-m-d');
                    $pickupClass = $pickupDao->getPickupClasses($school['page_id']);

                    $first_pickup_class = isset($pickupClass[0]['pickup_class_id']) ? $pickupClass[0]['pickup_class_id'] : 0;
                    $pickupId = $pickupDao->getPickupId($school['page_id'], $date, $first_pickup_class);
                }

                $children = $pickupDao->getChildOfClass($classes[0]['group_id'], $pickupId, $first_pickup_class, $child_count, $date);
                $disableSave = (count($children) == 0) ? true : false;

                $pickupIds = $pickupDao->getPickupIds($school['page_id'], $date);
                if (!is_null($pickupIds) && $first_pickup_class > 0) {
                    $childrenRegistered = $pickupDao->getChildRegistered($classes[0]['group_id'], $pickupIds, $first_pickup_class);
                }

                $smarty->assign('child_count', $child_count);
                $smarty->assign('today', toSysDate($date));
                $smarty->assign('pickup_class', $pickupClass);
                $smarty->assign('class_id', $classes[0]['group_id']);
                $smarty->assign('children', $children);
                $smarty->assign('childrenRegistered', $childrenRegistered);
                $smarty->assign('disableSave', $disableSave);

                break;

            case 'detail':
                if (!$is_configured) {
                    _error(404);
                }
                $canEdit = canEdit($_GET['username'], $view);
                if (!$canEdit) {
                    _error(403);
                }
                page_header(__("School Management") . " &rsaquo; " . __("Late pickup") . " &rsaquo; " . __("Detail"));

                $child_count = 0;
                $is_today = false;
                $pickupId = $_GET['id'];
                $pickup = $pickupDao->getPickup($pickupId, true);
                if ($pickup['pickup_time'] == date('Y-m-d')) {
                    $is_today = true;
                }
                $price_list = $pickupDao->getPriceList($school['page_id']);
                $children = $pickupDao->getChildOfPickup($pickupId, $child_count);

                $smarty->assign('is_today', $is_today);
                // Số trẻ đã trả
                $smarty->assign('child_count', $child_count);
                $smarty->assign('children', $children);
                $smarty->assign('today', toSysDate($pickup['pickup_time']));
                $smarty->assign('pickup', $pickup);
                $smarty->assign('time_option', $price_list[0]['ending_time']);

                break;
            default:
                _error(404);
                break;
        }

        break;

    case 'pickupteacher':
        // Tăng số lượt tương tác của trường - TaiLA
        addInteractive($school['page_id'], 'pickup', 'school_view');
        if ($is_configured && $is_teacher) {
            switch ($_GET['sub_view']) {

                case 'assign':
                    page_header(__("School Management") . " &rsaquo; " . __("Late pickup") . " &rsaquo; " . __("Assign"));

                    $monday = date('d/m/Y', strtotime('monday this week'));
                    $monDbDate = toDBDate($monday);
                    //$today = date("l");

                    $day['begin'] = date('d/m/Y');
                    $day['mon'] = $monday;
                    $day['tue'] = date('d/m/Y', strtotime("+1 day", strtotime($monDbDate)));
                    $day['wed'] = date('d/m/Y', strtotime("+2 day", strtotime($monDbDate)));
                    $day['thu'] = date('d/m/Y', strtotime("+3 day", strtotime($monDbDate)));
                    $day['fri'] = date('d/m/Y', strtotime("+4 day", strtotime($monDbDate)));
                    $day['sat'] = date('d/m/Y', strtotime("+5 day", strtotime($monDbDate)));
                    $day['sun'] = date('d/m/Y', strtotime("+6 day", strtotime($monDbDate)));

                    $classes = $pickupDao->getPickupClasses($school['page_id']);
                    $teachers = $pickupDao->getPickupTeachersGroup($school['page_id']);
                    $assignInfo = $pickupDao->getAssignInWeek($school['page_id'], toDBDate($monday));

                    // assign variables
                    $smarty->assign('classes', $classes);
                    $smarty->assign('teachers', $teachers);
                    $smarty->assign('data', $assignInfo['teacher_list']);
                    $smarty->assign('list', $assignInfo['teacher_id']);
                    $smarty->assign('day', $day);
                    break;

                case 'manage':

                    // Kiểm tra user có quyền trong lớp đón muộn không
                    if (!$is_assigned) {
                        _error(404);
                    }
                    page_header(__("School Management") . " &rsaquo; " . __("Late pickup") . " &rsaquo; " . __("Manage"));

                    $children = array();
                    $child_count = 0;
                    $is_today = true;
                    $pickup_time = date('Y-m-d');
                    $pickup = $pickupDao->getPickupAssign($school['page_id'], $pickup_time, true);
                    if (is_null($pickup)) {
                        _error(404);
                    }
                    $price_list = $pickupDao->getPriceList($school['page_id']);
                    $smarty->assign('time_option', $price_list[0]['ending_time']);
                    $children = $pickupDao->getChildOfPickup($pickup['pickup_id'], $child_count);

                    $smarty->assign('is_today', $is_today);
                    $smarty->assign('child_count', $child_count);
                    $smarty->assign('children', $children);
                    $smarty->assign('today', toSysDate($pickup_time));
                    $smarty->assign('pickup', $pickup);
                    break;

                case 'history':
                    // Kiểm tra user có quyền trong lớp đón muộn không
                    page_header(__("School Management") . " &rsaquo; " . __("Late pickup") . " &rsaquo; " . __("History"));

                    $toDate = date('t/m/Y');
                    $fromDate = date('01/m/Y');
                    $pickups = $pickupDao->getPickupTeacherInMonth($school['page_id'], $user->_data['user_id'], date('Y-m-01'), date('Y-m-t'));

                    $smarty->assign('toDate', $toDate);
                    $smarty->assign('fromDate', $fromDate);
                    $smarty->assign('pickups', $pickups);
                    break;

                case 'detail':
                    page_header(__("School Management") . " &rsaquo; " . __("Late pickup") . " &rsaquo; " . __("Detail"));
                    $child_count = 0;
                    $is_today = false;
                    $pickupId = $_GET['id'];
                    $pickup = $pickupDao->getPickup($pickupId, true);
                    if (is_null($pickup)) {
                        _error(404);
                    }
                    if ($pickup['pickup_time'] == date('Y-m-d')) {
                        $is_today = true;
                    }
                    $allow_teacher_edit_pickup_before = $school['allow_teacher_edit_pickup_before'];
                    $price_list = $pickupDao->getPriceList($school['page_id']);
                    $children = $pickupDao->getChildOfPickup($pickupId, $child_count);

                    $smarty->assign('is_today', $is_today);
                    $smarty->assign('child_count', $child_count);
                    $smarty->assign('children', $children);
                    $smarty->assign('today', toSysDate($pickup['pickup_time']));
                    //$smarty->assign('pickup_date', strtotime($pickup['pickup_time']));
                    $smarty->assign('pickup', $pickup);
                    $smarty->assign('time_option', $price_list[0]['ending_time']);
                    $smarty->assign('allow_teacher_edit_pickup_before', $allow_teacher_edit_pickup_before);
                    break;

                case 'add':
                    page_header(__("School Management") . " &rsaquo; " . __("Late pickup") . " &rsaquo; " . __("Add"));

                    // valid inputs
                    if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                        _error(404);
                    }

                    // Lấy ra danh sách các lớp của trường
                    //$classes = $classDao->getClassesOfSchool($class['school_id']);
                    $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                    $smarty->assign('classes', $classes);

                    $child_count = 0;
                    $pickupClass = array();
                    $childrenRegistered = array();

                    // Lấy ra thông tin 1 lần đón muộn
                    $pickup = $pickupDao->getPickup($_GET['id']);
                    $date = $pickup['pickup_time'];
                    $pickupId = $pickup['pickup_id'];
                    if ($pickup['pickup_class_id'] > 0) {
                        $pickupClass[] = $pickupDao->getPickupClass($pickup['pickup_class_id']);
                    }
                    $first_pickup_class = isset($pickupClass[0]['pickup_class_id']) ? $pickupClass[0]['pickup_class_id'] : 0;
                    $children = $pickupDao->getChildOfClass(array_values($classes)[0]['group_id'], $pickupId, $first_pickup_class, $child_count, $date);

                    /*echo '<pre>';
                    print_r($classes);
                    echo '<pre>'; die;*/

                    $pickupIds = $pickupDao->getPickupIds($school['page_id'], $date);
                    if (!is_null($pickupIds) && $first_pickup_class > 0) {
                        $childrenRegistered = $pickupDao->getChildRegistered(array_values($classes)[0]['group_id'], $pickupIds, $first_pickup_class);
                    }

                    $smarty->assign('child_count', $child_count);
                    $smarty->assign('today', toSysDate($pickup['pickup_time']));
                    $smarty->assign('pickup_class', $pickupClass);
                    $smarty->assign('class_id', array_values($classes)[0]['group_id']);
                    $smarty->assign('children', $children);
                    $smarty->assign('childrenRegistered', $childrenRegistered);
                    $smarty->assign('pickup_id', $pickup['pickup_id']);

                    break;

                default:
                    _error(404);
                    break;
            }
        } else _error(404);

        break;

    case 'reviews':
        switch ($subView) {
            case '':
                page_header(__("School Management") . " &rsaquo; " . __("School Reviews"));
                //Lấy ra danh sách đánh giá trường
                $reviews = $reviewDao->getSchoolReviewForWeb($school['page_id']);
                $smarty->assign('rows', $reviews);
                break;

            case 'teachers':
                page_header(__("School Management") . " &rsaquo; " . __("Teacher Reviews"));
                //Lấy ra danh sách đánh giá giáo viên toàn trường
                $reviews = $reviewDao->getTeacherReviewInSchool($school['page_id']);
                $smarty->assign('rows', $reviews);
                break;

            case 'teacher':
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }
                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                $classIds = array_keys($classes);
                if (!in_array($_GET['id'], $classIds)) {
                    _error(404);
                }

                if (!isset($_GET['p5']) || !is_numeric($_GET['p5'])) {
                    _error(404);
                }
                $result = array();

                $class = getClassData($_GET['id'], CLASS_INFO);
                if (is_null($class)) {
                    _error(404);
                }
                $result['class'] = $class;

                $teacher = getTeacherData($_GET['p5'], TEACHER_INFO);
                if (is_null($class)) {
                    _error(404);
                }
                $result['teacher'] = $teacher;

                //Lấy ra danh sách đánh giá trường
                $reviews = $reviewDao->getTeacherReviewDetail($_GET['p5'], $_GET['id']);
                $result['reviews'] = $reviews;
                $smarty->assign('rows', $result);
                page_header(__("School Management") . " &rsaquo; " . __("Teacher Reviews") . " &rsaquo; " . $teacher['user_fullname']);
                break;

            default:
                _error(404);
                break;
        }


        break;
    case 'loginstatistics':
        switch ($subView) {
            case '':
                page_header(__("School Management") . " &rsaquo; " . __("Login statistics"));
                //$classes = $classDao->getClassNamesOfSchool($school['page_id']);
                //Memcache - Lấy ra các lớp của 1 trường
                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                $smarty->assign('classes', $classes);

                //Lấy điều kiện search trước đó ra khỏi session
                $condition = $_SESSION[SESSION_KEY_SEARCH_CHILDREN_STATISTICS];
                if ($school['page_id'] == $condition['school_id']) {
                    $classId = isset($condition) ? $condition['class_id'] : '';
                    $page = isset($condition) ? $condition['page'] : 1;
                } else {
                    $classId = '';
                    $page = 1;
                }

                $result = array();
                $totalChildren = $childDao->searchCount('', $school['page_id'], $classId, STATUS_ACTIVE);
                $children = $childDao->searchPaging('', $school['page_id'], $classId, $page, PAGING_LIMIT, STATUS_ACTIVE);
                $childrenLast = array();
                foreach ($children as $row) {
                    foreach ($row['parent'] as $k => $value) {
                        if (isset($value['suggest']) && $value['suggest'] == 1) {
                            unset($row['parent'][$k]);
                        }
                    }
                    $childrenLast[] = $row;
                }
                $childrenAll = array();
                if ($classId > 0) {
                    $childrenAll = getClassData($classId, CLASS_CHILDREN);
                } else {
                    $childrenAll = getSchoolData($school['page_id'], SCHOOL_CHILDREN);
                }
                $countParentActive = 0;
                foreach ($childrenAll as $child) {
                    $parents = getChildData($child['child_id'], CHILD_PARENTS);
                    if (count($parents) > 0) {
                        foreach ($parents as $parent) {
                            if ($parent['user_last_active'] != '') {
                                $countParentActive++;
                                break;
                            }
                        }
                    }
                }

                $result['total'] = $totalChildren;
                $result['page_count'] = ceil(($totalChildren + 0.0) / PAGING_LIMIT);
                $result['children'] = $childrenLast;
                $result['class_id'] = $classId;
                $result['page'] = $page;

                $smarty->assign('result', $result);
                $smarty->assign('countParentActive', $countParentActive);
                break;
            default:
                _error(404);
                break;
        }
        break;

    default:
        _error(404);
}

// assign variables
$smarty->assign('school', $school);
$smarty->assign('username', $_GET['username']);
$smarty->assign('canEdit', $canEdit);
$smarty->assign('view', $view);
$smarty->assign('sub_view', $subView);

// page footer
page_footer("school");

?>