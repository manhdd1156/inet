<?php
/**
 * groups
 * 
 * @package Inet
 * @author NOGA Co., Ltd
 */

// fetch bootstrap
require('bootstrap.php');

// user access
user_access();
try {

	// get view content
    if (isset($_GET['category'])) {
        switch ($_GET['category']) {

            /* CI - Thêm các topic: Thai kỳ, Chăm con, Dạy con, Hôn nhân gia đình */
            case 'thaiky':

                // page header
                page_header(__("Topic") . " &ndash; " .__("Preganancy"));

                // get groups
                $groups = $user->get_groups_by_category(CATEGORY_THAIKY);
                /* assign variables */
                $smarty->assign('groups', $groups);
                $smarty->assign('get', "groups");

                break;
            case 'chamcon':

                // page header
                page_header(__("Topic") . " &ndash; " .__("Child care"));

                // get groups
                $groups = $user->get_groups_by_category(CATEGORY_CHAMCON);
                /* assign variables */
                $smarty->assign('groups', $groups);
                $smarty->assign('get', "groups");

                break;
            case 'daycon':

                // page header
                page_header(__("Topic") . " &ndash; " .__("Parenting"));

                // get groups
                $groups = $user->get_groups_by_category(CATEGORY_DAYCON);
                /* assign variables */
                $smarty->assign('groups', $groups);
                $smarty->assign('get', "groups");

                break;
            case 'giadinh':

                // page header
                page_header(__("Topic") . " &ndash; " .__("Marriage and family"));

                // get groups
                $groups = $user->get_groups_by_category(CATEGORY_GAIDINH);
                /* assign variables */
                $smarty->assign('groups', $groups);
                $smarty->assign('get', "groups");

                break;

            default:
                _error(404);
                break;
        }
    } else {
        switch ($_GET['view']) {
            case 'discover':

                // page header
                page_header(__("Discover")." &rsaquo; ".__("Groups"));

                // get new groups
                $groups = $user->get_groups(array('suggested' => true));
                /* assign variables */
                $smarty->assign('groups', $groups);
                $smarty->assign('get', "suggested_groups");

                break;

            case '':

                // page header
                page_header(__("Joined Groups"));

                // get joined groups
                $groups = $user->get_groups( array('user_id' => $user->_data['user_id']) );
                /* assign variables */
                $smarty->assign('groups', $groups);
                $smarty->assign('get', "joined_groups");

                break;

            case 'manage':

                // page header
                page_header(__("Your Groups"));

                // get managed groups
                $groups = $user->get_groups();
                /* assign variables */
                $smarty->assign('groups', $groups);
                $smarty->assign('get', "groups");

                break;

            default:
                _error(404);
                break;
        }
    }

	/* assign variables */
	$smarty->assign('view', $_GET['view']);
	$smarty->assign('category', $_GET['category']);

    // get pages categories
    $categories = $user->get_groups_categories();
    /* assign variables */
    $smarty->assign('categories', $categories);

    // ConIu - Lấy ra danh sách schools, classes, children mà user quản lý
    include_once('includes/ajax/ci/dao/dao_child.php');
    $childDao = new ChildDAO();
    $objects = getRelatedObjects();
    // Lấy những trường đang sử dụng inet
    $schoolUsing = array();
    foreach ($objects['schools'] as $school) {
        if($school['school_status'] == SCHOOL_USING_CONIU) {
            $schoolUsing[] = $school;
        }
    }
    $smarty->assign('schools', $schoolUsing);
    $smarty->assign('classes', $objects['classes']);

    $children = $childDao->getChildrenOfParent($user->_data['user_id']);
    $smarty->assign('children', $children);
    // ConIu - END

} catch (Exception $e) {
	_error(__("Error"), $e->getMessage());
}

// page footer
page_footer("groups");

?>