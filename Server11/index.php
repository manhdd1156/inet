<?php
/**
 * index
 * 
 * @package CINet v1.1
 * @author NOGA Co., Ltd
 */

// fetch bootstrap
include('bootstrap.php');

try {

	// check user logged in
	if(!$user->_logged_in) {

		// page header
		page_header(__("Welcome to").' '.$system['system_title']);

		// get random users
		/* Coniu - Bỏ đoạn code này - Theme mới không cần đến
		$random_users = array();
		$get_random_users = $db->query("SELECT user_name, user_fullname, user_gender, user_picture FROM users ORDER BY RAND() LIMIT 4") or _error(SQL_ERROR_THROWEN);
		if($get_random_users->num_rows > 0) {
			while($random_user = $get_random_users->fetch_assoc()) {
				$random_user['user_picture'] = User::get_picture($random_user['user_picture'], $random_user['user_gender']);
				$random_users[] = $random_user;
			}
		}
		$smarty->assign('random_users', $random_users);
		*/

	} else {

		// user access
		user_access();

        // Cập nhật last_active
        updateUserLastActive($user->_data['user_id']);

        // Cập nhật users online
        $today = date("Y-m-d");
        updateUserOnline($user->_data['user_id']);

		// get view content
		switch ($_GET['view']) {
			case '':
				// page header
				page_header($system['system_title'] . " | ". __("School management and connection software"));

				// get posts (newsfeed)
				$posts = $user->get_posts();
				/* assign variables */
				$smarty->assign('posts', $posts);
				break;

			case 'saved':
				// page header
				page_header(__("Saved Posts") . " | ". __("School management and connection software"));

				// get posts (saved)
				$posts = $user->get_posts( array('get' => 'saved') );
				/* assign variables */
				$smarty->assign('posts', $posts);
				break;
            case 'contact':
                // page header
                page_header(__("Contact") . " | ". __("School management and connection software"));
                break;
            case 'addchild':
                // page header
                page_header(__("Add new student") . " | ". __("School management and connection software"));
                break;

			//ConIu - BEGIN
            case 'create_school':
                // page header
                page_header($system['system_title']." &rsaquo; ".__("Create School"));

                /* assign variables */
                //$smarty->assign('cities', $cities);
                break;
            case 'create_child':
                // page header
                page_header($system['system_title']." &rsaquo; ".__("Create Child"));

                /* assign variables */
                //$smarty->assign('cities', $cities);
                break;
            //ConIu - END
			case 'groups':
				// page header
				page_header(__("Groups") . " | ". __("School management and connection software"));
				break;

			case 'create_page':
				// page header
				page_header($system['system_title']." &rsaquo; ".__("Create Page"));

				// get pages categories
				$categories = $user->get_pages_categories();
				/* assign variables */
				$smarty->assign('categories', $categories);
				break;

			case 'create_group':
				// page header
				page_header($system['system_title']." &rsaquo; ".__("Create Group"));
				break;

			case 'games':
				// games enabled
				if(!$system['games_enabled']) {
					_error(404);
				}

				// page header
				page_header($system['system_title']." &rsaquo; ".__("Games"));

				// get games
				$games = $user->get_games();
				/* assign variables */
				$smarty->assign('games', $games);
				break;

			default:
				_error(404);
				break;
		}
		/* assign variables */
		$smarty->assign('view', $_GET['view']);

		// get pages
		$pages = $user->get_pages();
		/* assign variables */
		$smarty->assign('pages', $pages);

		// get groups
		$groups = $user->get_groups();
		/* assign variables */
		$smarty->assign('groups', $groups);

		// get new people
		$new_people = $user->get_new_people(0, true);
		/* assign variables */
		$smarty->assign('new_people', $new_people);

		// get new pages
		//$new_pages = $user->get_pages( array('suggested' => true));
		$new_pages = $user->get_pages(array('suggested' => true, 'results' => 5));
		/* assign variables */
		$smarty->assign('new_pages', $new_pages);

		// get new groups
		//$new_groups = $user->get_groups( array('suggested' => true));
		$new_groups = $user->get_groups(array('suggested' => true, 'results' => 5));
		/* assign variables */
		$smarty->assign('new_groups', $new_groups);

		// get announcements
		$announcements = $user->announcements();
		/* assign variables */
		$smarty->assign('announcements', $announcements);

		// get ads
		$ads = $user->ads('home');
		/* assign variables */
		$smarty->assign('ads', $ads);

        // get widgets
        $widgets = $user->widgets('home');
        /* assign variables */
        $smarty->assign('widgets', $widgets);

		// ConIu - Lấy ra danh sách schools, classes, children mà user quản lý
		include_once('includes/ajax/ci/dao/dao_child.php');
		$childDao = new ChildDAO();
        $objects = getRelatedObjects();
        // Lấy những trường đang sử dụng inet
        $schoolUsing = array();
        foreach ($objects['schools'] as $school) {
            if($school['school_status'] == SCHOOL_USING_CONIU) {
                $schoolUsing[] = $school;
            }
        }

		$smarty->assign('schools', $schoolUsing);

        $classUsing = array();
        foreach ($objects['classes'] as $class) {
            if($class['school_status'] == SCHOOL_USING_CONIU) {
                $classUsing[] = $class;
            }
        }
		$smarty->assign('classes', $classUsing);

		$children = $childDao->getChildrenOfParent($user->_data['user_id']);

		$smarty->assign('children', $children);
		//UPDATE START - MANHDD
        // get info for user children
        if(count($schoolUsing)==0 && count($classUsing)==0 && count($children)==0 ) {


        $itself = $childDao->getChildrenOfItSelf($user->_data['user_id']);
        if(count($itself)>0) {
        $itself['user_id'] = $user->_data['user_id'];
        }
        $smarty->assign('itself', $itself);
        }
        //UPDATE END - MANHDD
		// ConIu - END

        // cooki tải app
        $name = "download_ap_" . $user->_data['user_id'];
        $cookie = 1;
        if(isset($_COOKIE[$name])) {
            $cookie = $_COOKIE[$name];
        }

        $smarty->assign('cookie_app', $cookie);
	}

} catch (Exception $e) {
	_error(__("Error"), $e->getMessage());
}

// page footer
page_footer("index");


?>