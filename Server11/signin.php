<?php
/**
 * signin
 * 
 * @package CINet v1.1
 * @author NOGA Co., Ltd
 */

// fetch bootstrap
require('bootstrap.php');

// check user logged in
if($user->_logged_in) {
    redirect();
}

// page header
page_header($system['system_title']." &rsaquo; ".__("Login"));

// page footer
page_footer("signin");

?>