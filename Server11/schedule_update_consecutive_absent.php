<?php
/**
 * Hàm push notification nhắc nhở tạo lịch học
 *
 * @package ConIu v1
 * @author TaiLA
 */

//$path = str_replace('includes/ajax/ci/php_script/schedule', '', dirname(__FILE__));
//set_include_path($path);

// fetch bootstrap
require('bootstrap.php');

try {

    include_once(DAO_PATH . 'dao_school.php');
    include_once(DAO_PATH . 'dao_user.php');
    include_once(DAO_PATH . 'dao_attendance.php');
    include_once(ABSPATH . 'includes/ajax/ci/memcache_caching.php');

    $schoolDao = new SchoolDAO();
    $userDao = new UserDAO();
    $attendanceDao = new AttendanceDAO();
    $memcache = new CacheMemcache();

    if ($memcache->bEnabled) {// if Memcache enabled

        // Lấy danh sách trường đang sử dụng coniu
        $schools = $schoolDao->getAllSchoolsUsingInet(1);
        $time = date('H:i d/m/Y');

        $insights = array();
        //Lặp danh sách trường, lấy danh sách lớp của trường
        foreach ($schools as $school) {

            /* ---------- Trẻ nghỉ nhiều liên tục ---------- */
            $daynum = 3;
            $children = $attendanceDao->getConsecutiveAbsentChild($school['page_id'], $daynum);

            $tmpArray = array();
            while ((count($children) > 0) && (MAX_CONSECUTIVE_ABSENT_DAY >= $daynum)) {
                $tmpArray[$daynum] = $children;
                $daynum++;
                if (MAX_CONSECUTIVE_ABSENT_DAY >= $daynum) {
                    $children = $attendanceDao->getConsecutiveAbsentChild($school['page_id'], $daynum);
                }
            }

            $newArrays = array();
            while ($daynum > 3) {
                $daynum--;
                foreach ($tmpArray[$daynum] as $childOut) {
                    $found = false;
                    foreach ($newArrays as $childIn) {
                        if ($childOut['child_id'] == $childIn['child_id']) {
                            $found = true;
                            break;
                        }
                    }
                    if (!$found) {
                        $newChild = $childOut;
                        $newChild['number_of_absent_day'] = $daynum;
                        $newArrays[] = $newChild;
                    }
                }
            }
            // lặp danh sách trẻ nghỉ nhiều liên tục lấy lý do nghỉ
            // Lấy ngày điểm danh cuối cùng của trường
            $dates = $attendanceDao->getLastAttendanceDates($school['page_id'], $daynum);
            $toDate = $dates[0];
            $endDate = toSysDate($toDate);

            // Khai báo mảng mới
            $resultsAb['list'] = array();
            foreach ($newArrays as $row) {
                $beginDate = date('Y-m-d', strtotime('- ' . ($row['number_of_absent_day'] + 2) . ' days', strtotime($toDate)));
                $beginDate = toSysDate($beginDate);
                $attendances = $attendanceDao->getAttendanceChild($row['child_id'], $beginDate, $endDate);
                $reason = '';

                // Lặp mảng thông tin điểm danh của trẻ lấy lý do nghỉ (lấy lần cuối cùng có lý do)
                foreach ($attendances['attendance'] as $value) {
                    if($value['reason'] != '') {
                        $reason = $value['reason'];
                        break;
                    }
                }
                $row['reason'] = $reason;
                $resultsAb['list'][] = $row;
            }
            $resultsAb['updated_at'] = $time;
            $memcache->setData(SCHOOL_CONSECUTIVE_ABSENT . $school['page_id'], $resultsAb);
        }
    }
}
catch (Exception $e) {
    $db->rollback();
    return_json( array('error' => true, 'message' => $e->getMessage()) );
} finally {
    $db->autocommit(true);
}


?>