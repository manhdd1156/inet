<?php
/**
 * update email and phone
 *
 * @package CINet v1.1
 * @author NOGA Co., Ltd
 */

// fetch bootstrap
require('bootstrap.php');

// user access
if(!$user->_logged_in) {
    get_login();
}

// page header
page_header($system['system_title']." &rsaquo; ".__("Update Infomation"));

// page footer
page_footer("updateinfo");

?>