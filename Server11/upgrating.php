<?php
/**
 * terms
 * 
 * @package CINet v1.1
 * @author NOGA Co., Ltd
 */

// fetch bootstrap
require('bootstrap.php');

// page header
page_header($system['system_title']." &rsaquo; ".__("Upgrating"));

// page footer
page_footer("upgrating");

?>