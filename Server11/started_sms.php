<?php
/**
 * started
 * 
 * @package CINet v1.1
 * @author NOGA Co., Ltd
 */

// fetch bootstrap
require('bootstrap.php');

// check user logged in
if($user->_logged_in) {
    header('Location: '.$system['system_url']);
}

// page header
page_header($system['system_title']." &rsaquo; ".__("Getting Started"));

if (!isset($_SESSION['phone']) || (!isset($_SESSION['csrf']) && $_SESSION['csrf'] != 'coniuahihi') ) {
    _error(404);
}

//Coniu - Thêm trạng giá trị step để hiển thị màn hình tương ứng
$step = 1;
if (isset($_GET['step']) && ($_GET['step'] == 2 || $_GET['step'] == 3) ) {
    $step = $_GET['step'];
}

$smarty->assign('step', $step);
$smarty->assign('phone', $phone);

// page footer
page_footer("started_sms");

?>