<?php
/**
 * Quản lý các chức năng chính của module ConIu
 *
 * @package ConIu v1
 * @author QuanND
 */

// set override_shutdown
$override_shutdown = true;

// fetch bootstrap
require('bootstrap.php');

// Kiểm tra user login rồi hay chưa
user_access();

include_once(DAO_PATH . 'dao_school.php');
include_once(DAO_PATH . 'dao_class.php');
include_once(DAO_PATH . 'dao_teacher.php');
include_once(DAO_PATH . 'dao_subject.php');
include_once(DAO_PATH . 'dao_child.php');
include_once(DAO_PATH . 'dao_point.php');
include_once(DAO_PATH . 'dao_event.php');
include_once(DAO_PATH . 'dao_medicine.php');
include_once(DAO_PATH . 'dao_tuition.php');
include_once(DAO_PATH . 'dao_parent.php');
include_once(DAO_PATH . 'dao_attendance.php');
include_once(DAO_PATH . 'dao_service.php');
include_once(DAO_PATH . 'dao_report.php');
include_once(DAO_PATH . 'dao_feedback.php');
include_once(DAO_PATH . 'dao_schedule.php');
include_once(DAO_PATH . 'dao_class_level.php');
include_once(DAO_PATH . 'dao_user.php');
include_once(DAO_PATH . 'dao_pickup.php');
include_once(DAO_PATH . 'dao_menu.php');
include_once(DAO_PATH . 'dao_conduct.php');

$parentDao = new ParentDAO();
$schoolDao = new SchoolDAO();
$subjectDao = new SubjectDAO();
$classDao = new ClassDAO();
$teacherDao = new TeacherDAO();
$childDao = new ChildDAO();
$pointDao = new PointDAO();
$eventDao = new EventDAO();
$medicineDao = new MedicineDAO();
$tuitionDao = new TuitionDAO();
$attendanceDao = new AttendanceDAO();
$serviceDao = new ServiceDAO();
$reportDao = new ReportDAO();
$feedbackDao = new FeedbackDAO();
$scheduleDao = new ScheduleDAO();
$classLevelDao = new ClassLevelDAO();
$userDao = new UserDAO();
$conductDao = new ConductDAO();
$pickupDao = new PickupDAO();
$menuDao = new MenuDAO();

//Lấy danh sách CHILD được quản lý bởi 1 user
$children = getUserManageData($user->_data['user_id'], USER_MANAGE_CHILDREN);
// nếu là học sinh thì lấy theo kiểu này
if (!$children) {
    $children = getUserManageData($user->_data['user_id'], USER_MANAGE_CHILDREN_ITSELF);
}
error_log('child 64 - $children :' . json_encode($children));
if (count($children) == 0) {
    _error(404);
}

$child = getChildDataById($_GET['child_id'], CHILD_INFO);
// nếu là học sinh thì lấy kiểu này
error_log('child 71 - $child :' . json_encode($child));

if (!$child) {
    error_log('child 74 - ');
    $child = getChildDataById($_GET['child_id'], CHILD_INFO, "itself");
}
error_log('child 77 - $child :' . json_encode($child) . ';$child[\'status\'] : ' . $child['status']);
if (is_null($child) || $child['status'] == STATUS_INACTIVE) {
    error_log('vao day');
    _error(404);
}

$class = getChildData($child['child_id'], CHILD_CLASS);

$school = getSchoolData($child['school_id'], SCHOOL_INFO);
$school['config'] = getSchoolData($child['school_id'], SCHOOL_CONFIG);

//memcache???
$pickup_configured = $pickupDao->checkPickupConfiguration($school['page_id']);
// Lấy cấu hình thông báo của những user quản lý trường
$notify_settings = getSchoolData($school['page_id'], SCHOOL_NOTIFICATIONS);

$smarty->assign('children', $children);
$smarty->assign('child', $child);
$smarty->assign('pickup_configured', $pickup_configured);
$smarty->assign('school', $school);

// ConIu - Lấy ra danh sách schools, classes, children mà user quản lý
include_once('includes/ajax/ci/dao/dao_child.php');
$childDao = new ChildDAO();
$objects = getRelatedObjects();
// Lấy những trường đang sử dụng inet
$schoolUsing = array();
foreach ($objects['schools'] as $schoolOb) {
    if ($schoolOb['school_status'] == SCHOOL_USING_CONIU) {
        $schoolUsing[] = $schoolOb;
    }
}

$smarty->assign('schoolList', $schoolUsing);

$classUsing = array();
foreach ($objects['classes'] as $classOb) {
    if ($classOb['school_status'] == SCHOOL_USING_CONIU) {
        $classUsing[] = $classOb;
    }
}
$smarty->assign('classList', $classUsing);

$childrenOb = $childDao->getChildrenOfParent($user->_data['user_id']);

$smarty->assign('childrenList', $childrenOb);
// ConIu - END

// Cập nhật last_active
updateUserLastActive($user->_data['user_id']);

// Cập nhật users online
$today = date("Y-m-d");
updateUserOnline($user->_data['user_id']);

// page content
switch ($_GET['view']) {
    case '':
        // page header
        page_header(__("Child Management") . " &rsaquo; " . __("Dashboard"));

//        $teachers = $teacherDao->getTeacherOfClass($class['group_id']);
//        $insights['teachers'] = $teachers;
        if (!is_null($class)) {
            $teachers = getClassData($class['group_id'], CLASS_TEACHERS);
            $insights['teachers'] = $teachers;

            $insights['class'] = $class;
        }

        $insights['school'] = $school;
        $admin = $schoolDao->getAdmin($school['page_id']);
        $insights['admin'] = $admin;
        //$insights['admin'] = $school['page_admin'];

        $today = date($system['date_format']);
        $medicines = $medicineDao->getChildMedicineOnDate($child['child_id'], $today);
        $insights['medicines'] = $medicines;

        //$children = $childDao->getChildrenOfClass($class['group_id'], true);
        $children = array();
        if (!is_null($class)) {
            $childs = getClassData($class['group_id'], CLASS_CHILDREN);
            $childs = sortArray($childs, "ASC", "name_for_sort");
            foreach ($childs as $child) {
                //$parents = $parentDao->getParent($child['child_id']);
                $child['parent'] = getChildData($child['child_id'], CHILD_PARENTS);
                $children[] = $child;
            }
            $insights['children'] = $children;
        }

        $events = $eventDao->getChildEvents($class['group_id'], $class['class_level_id'], $class['school_id'], $child['child_id'], 2);
        $insights['events'] = $events;

        // assign variables
        $smarty->assign('insights', $insights);
        break;
    case 'contact':
        page_header(__("Child Management") . " &rsaquo; " . __("Student list"));

        //$children = $childDao->getChildrenOfClass($class['group_id'], true);
        $children = array();
        $teachers = array();
        if (!is_null($class)) {
            $childs = getClassData($class['group_id'], CLASS_CHILDREN);
            $childs = sortArray($childs, "ASC", "name_for_sort");

            foreach ($childs as $child) {
                //$parents = $parentDao->getParent($child['child_id']);
                $child['parent'] = getChildData($child['child_id'], CHILD_PARENTS);
                $children[] = $child;
            }
            //$teachers = $teacherDao->getTeacherOfClass($class['group_id']);
            $teachers = getClassData($class['group_id'], CLASS_TEACHERS);
        }

        $smarty->assign('children', $children);
        $smarty->assign('teacher', $teachers);

        $userIds = array();
        $userIds[] = $school['page_admin'];
        $schoolAdmin = array();
        $schoolAdmin = $userDao->getUsers($userIds);
        $smarty->assign('schoolAdmin', $schoolAdmin);

        break;
    case 'informations':
        // Tăng lượt tương tác - TaiLA
        addInteractive($child['school_id'], 'picker', 'parent_view');
        switch ($_GET['sub_view']) {
            case '':
                page_header(__("Child Management") . " &rsaquo; " . __("Student information"));
                $childInfo = $childDao->getChildInformation($child['child_id']);
                $smarty->assign('rows', $childInfo);
                break;
            case 'add':
                page_header(__("Child Management") . " &rsaquo; " . __("Student information") . " " . __("Add New"));
                break;
            case 'edit':
                page_header(__("Child Management") . " &rsaquo; " . __("Student information") . " " . __("Edit"));
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }
                $rows = $childDao->getChildInformationDetail($_GET['id']);
                $smarty->assign('rows', $rows);
                break;
            default:
                _error(404);
                break;
        }
        break;
    case 'events':
        // Tăng lượt tương tác - TaiLA
        addInteractive($child['school_id'], 'event', 'parent_view');
        switch ($_GET['sub_view']) {
            case '':
                page_header(__("Child Management") . " &rsaquo; " . __("Notification - Event"));
                $events = $eventDao->getChildEvents($class['group_id'], $class['class_level_id'], $school['page_id'], $child['child_id']);
                $smarty->assign('rows', $events);
                break;

            case 'detail':
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }
                // Tăng lượt tương tác - TaiLA
                addInteractive($child['school_id'], 'event', 'parent_view', $_GET['id']);

                $event = $eventDao->getChildEventDetail($_GET['id'], $child['child_id']);
                if (is_null($event)) {
                    _error(404);
                }

                /* Coniu - Tương tác trường */
                //1. Cập nhật lượt xem sự kiện của trường
                $eventIds = array();
                $eventIds[] = $event['event_id'];
                increaseCountViews($child['school_id'], 'event', $eventIds);
                /* ConIu - END */

                $smarty->assign('data', $event);

                page_header(__("Child Management") . " &rsaquo; " . __("Notification - Event") . " &rsaquo; " . $event['event_name']);
                break;
            default:
                _error(404);
                break;
        }
        break;
    case 'services':
        // Tăng lượt tương tác - TaiLA
        addInteractive($child['school_id'], 'service', 'parent_view');
        switch ($_GET['sub_view']) {
            case '':
                page_header(__("Child Management") . " &rsaquo; " . __("Service") . " &rsaquo; " . __("Service usage information"));

                $notCountBasedServices = $serviceDao->getChildServiceNotCountBased($class['school_id'], $child['child_id']);
                $smarty->assign('ncb_services', $notCountBasedServices);

                //Lấy ra danh sách dịch vụ tính tiền theo lần sử dụng
                $services = $serviceDao->getCountBasedServices($class['school_id']);
                $servicesDisplay = array();
                foreach ($services as $service) {
                    if ($service['parent_display'] == 1) {
                        $servicesDisplay[] = $service;
                    }
                }
                $smarty->assign('cb_services', $servicesDisplay);

                $begin = date($system['date_format'], mktime(0, 0, 0, date("m"), 1, date("Y")));
                $end = date($system['date_format']);
                $smarty->assign('begin', $begin);
                $smarty->assign('end', $end);

                $cbs_usage = $serviceDao->getCountBasedServiceAChild($class['school_id'], $child['child_id'], $begin, $end);
                $smarty->assign('cbs_usage', $cbs_usage);

                break;
            case 'reg':

                $ncb_services = $serviceDao->getNotCountBasedServiceChild4Register($school['page_id'], $child['child_id']);
                $smarty->assign('ncb_services', $ncb_services);

                /* echo '<pre>';
                 print_r($ncb_services);
                 echo '<pre>';die;*/

                $using_at = date($system['date_format']);
                $smarty->assign('using_at', $using_at);

                //Lấy ra danh sách dịch vụ đang áp dụng của trường
                $cb_services = $serviceDao->getCountBasedServiceAChild4Register($school['page_id'], $child['child_id'], $using_at);
                $smarty->assign('cb_services', $cb_services);
                $total = count($cb_services);
                foreach ($cb_services as $service) {
                    if (isset($service['service_usage_id']) && ($service['service_usage_id'] > 0)) {
                        $total = $total - 1;
                    }
                }
                $smarty->assign('total', $total);
                page_header(__("Child Management") . " &rsaquo; " . __("Service") . " &rsaquo; " . __("Register service"));
                break;
            default:
                _error(404);
                break;
        }

        break;
    case 'attendance':
        // Tăng lượt tương tác - TaiLA
        addInteractive($child['school_id'], 'attendance', 'parent_view');
        switch ($_GET['sub_view']) {
            case '':
                // page header
                page_header(__("Child Management") . " &rsaquo; " . __("Attendance"));

                $toDate = date($system['date_format']);
                $fromDate = date($system['date_format'], mktime(0, 0, 0, date("m"), 1, date("Y")));
                $attendances = $attendanceDao->getAttendanceChild($child['child_id'], $fromDate, $toDate);

                $data = array();
                $data['child'] = $child;
                $data['fromDate'] = $fromDate;
                $data['toDate'] = $toDate;
                $data['attendance'] = $attendances;
                $smarty->assign('data', $data);

                break;
            case 'resign':
                page_header(__("Child Management") . " &rsaquo; " . __("Resign"));
                //$attendanceDate = date($system['date_format']);
                //$smarty->assign('attendance_date', $attendanceDate);

                break;
            default:
                _error(404);
                break;
        }
        break;
    case 'medicines':
        // Tăng lượt tương tác - TaiLA
        addInteractive($child['school_id'], 'medicine', 'parent_view');
        switch ($_GET['sub_view']) {
            case '':
                // page header
                page_header(__("Child Management") . " &rsaquo; " . __("Medicines"));
                $medicines = $medicineDao->getChildAllMedicines($child['child_id']);

                $smarty->assign('rows', $medicines);

                break;
            case 'add':
                page_header(__("Child Management") . " &rsaquo; " . __("Medicines") . " &rsaquo; " . __("Add New"));

                break;
            case 'edit':
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }
                $medicine = $medicineDao->getMedicine($_GET['id'], false);
                if (is_null($medicine)) {
                    _error(404);
                }
                $smarty->assign('data', $medicine);

                page_header(__("Child Management") . " &rsaquo; " . __("Medicines") . " &rsaquo; " . $medicine['medicine_list']);
                break;
            case 'detail':
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }
                include_once(DAO_PATH . 'dao_user.php');
                $userDao = new UserDAO();

                $medicine = $medicineDao->getMedicine4Detail($_GET['id']);
                if (is_null($medicine)) {
                    _error(404);
                }
                $medicine['confirm_user'] = '';
                $medicine['confirm_username'] = '';
                if (!is_null($medicine['confirm_user_id'])) {
                    $userConfirm = $userDao->getUserByUserId($medicine['confirm_user_id']);
                    $medicine['confirm_user'] = $userConfirm['user_fullname'];
                    $medicine['confirm_username'] = $userConfirm['user_name'];
                }
                $smarty->assign('data', $medicine);

                page_header(__("Child Management") . " &rsaquo; " . __("Medicines") . " &rsaquo; " . $medicine['medicine_list']);
                break;
            default:
                _error(404);
                break;
        }
        break;
    case 'schedules':
        // Tăng số đếm tương tác lên 1 - Taila - làm lại
        addInteractive($school['page_id'], 'schedule', 'parent_view');
        switch ($_GET['sub_view']) {
            case '':
                page_header(__("Class Management") . " &rsaquo; " . __("Schedule"));
                //Lấy ra danh sách lịch học áp dụng cho lớp
                $data = $scheduleDao->getScheduleOfSchoolById($class['school_id'], $class['class_level_id'], $class['group_id']);

                $removeArr = array();
                for ($i = 0; $i < count($data); $i++) {
                    for ($j = 0; $j < count($data); $j++) {
                        if ($i != $j) {
                            $beginI = strtotime(toDBDate($data[$i]['begin']));
                            $beginJ = strtotime(toDBDate($data[$j]['begin']));
                            if ($beginI == $beginJ) {
                                if ($data[$i]['applied_for'] > $data[$j]['applied_for']) {
                                    $removeArr[] = $j;
                                }
                            }
                        }
                    }
                }
                $schedules = array();
                for ($k = 0; $k < count($data); $k++) {
                    if (!in_array($k, $removeArr)) {
//                        $schedules[] = $data[$k];
                        // Thêm trường này vào để biết nó bị thay thế hay không
                        $data[$k]['use'] = STATUS_ACTIVE;
                    } else {
                        $data[$k]['use'] = STATUS_INACTIVE;
                    }
                }
//                $smarty->assign('rows', $schedules);
                $smarty->assign('rows', $data);

                //$class_levels = $classLevelDao->getClassLevels($class['school_id']);
                $class_levels = getSchoolData($class['school_id'], SCHOOL_CLASS_LEVELS);
                //$classes = $classDao->getClassesOfSchool($class['school_id']);
                $classes = getSchoolData($class['school_id'], SCHOOL_CLASSES);

                $smarty->assign('class_levels', $class_levels);
                $smarty->assign('classes', $classes);
                break;
            case 'detail':
                // Tăng số lượt tương tác của phụ huynh
                addInteractive($school['page_id'], 'schedule', 'parent_view', $_GET['id']);
                page_header(__("Class Management") . " &rsaquo; " . __("Detail schedule"));
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }
                $data = $scheduleDao->getScheduleDetailById($_GET['id']);
                $smarty->assign('data', $data);

                /* Coniu - Tương tác trường */
                $scheduleIds = array();
                $scheduleIds[] = $_GET['id'];
                increaseCountViews($child['school_id'], 'schedule', $scheduleIds);
                /* Coniu - END */

                if ($data['applied_for'] == 2) {
                    //$class_level = $classLevelDao->getClassLevel($data['class_level_id']);
                    $class_level = getClassLevelData($data['class_level_id'], CLASS_LEVEL_INFO);
                    $smarty->assign('class_level', $class_level);
                }
                if ($data['applied_for'] == 3) {
                    //$classes = $classDao->getClass($data['class_id']);
                    $classes = getClassData($data['class_id'], CLASS_INFO);
                    $smarty->assign('classes', $classes);
                }
                break;

            default:
                _error(404);
                break;
        }
        break;
    case 'menus':
        // Tăng lượt tương tác - TaiLA
        addInteractive($child['school_id'], 'menu', 'parent_view');
        switch ($_GET['sub_view']) {
            case '':
                page_header(__("Class Management") . " &rsaquo; " . __("Menu"));
                //Lấy ra danh sách thực đơn áp dụng cho lớp
                $data = $menuDao->getMenuOfSchoolById($class['school_id'], $class['class_level_id'], $class['group_id']);

                $removeArr = array();
                for ($i = 0; $i < count($data); $i++) {
                    for ($j = 0; $j < count($data); $j++) {
                        if ($i != $j) {
                            $beginI = strtotime(toDBDate($data[$i]['begin']));
                            $beginJ = strtotime(toDBDate($data[$j]['begin']));
                            if ($beginI == $beginJ) {
                                if ($data[$i]['applied_for'] > $data[$j]['applied_for']) {
                                    $removeArr[] = $j;
                                }
                            }
                        }
                    }
                }
                $menus = array();
                for ($k = 0; $k < count($data); $k++) {
                    if (!in_array($k, $removeArr)) {
//                        $menus[] = $data[$k];
                        // Thêm trường này vào để biết nó bị thay thế hay không
                        $data[$k]['use'] = STATUS_ACTIVE;
                    } else {
                        $data[$k]['use'] = STATUS_INACTIVE;
                    }
                }
//                $smarty->assign('rows', $menus);
                $smarty->assign('rows', $data);

                //$class_levels = $classLevelDao->getClassLevels($class['school_id']);
                $class_levels = getSchoolData($class['school_id'], SCHOOL_CLASS_LEVELS);
                //$classes = $classDao->getClassesOfSchool($class['school_id']);
                $classes = getSchoolData($class['school_id'], SCHOOL_CLASSES);

                $smarty->assign('class_levels', $class_levels);
                $smarty->assign('classes', $classes);
                break;
            case 'detail':
                page_header(__("Class Management") . " &rsaquo; " . __("Detail menu"));
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }

                // Tăng lượt tương tác - TaiLA
                addInteractive($child['school_id'], 'menu', 'parent_view', $_GET['id']);

                $data = $menuDao->getMenuDetailById($_GET['id']);
                $smarty->assign('data', $data);

                /* Coniu - Tương tác trường */
                $scheduleIds = array();
                $scheduleIds[] = $_GET['id'];
                increaseCountViews($class['school_id'], 'schedule', $scheduleIds);
                /* Coniu - END */

                if ($data['applied_for'] == 2) {
                    //$class_level = $classLevelDao->getClassLevel($data['class_level_id']);
                    $class_level = getClassLevelData($data['class_level_id'], CLASS_LEVEL_INFO);
                    $smarty->assign('class_level', $class_level);
                }
                if ($data['applied_for'] == 3) {
                    //$classes = $classDao->getClass($data['class_id']);
                    $classes = getClassData($data['class_id'], CLASS_INFO);
                    $smarty->assign('classes', $classes);
                }
                break;

            default:
                _error(404);
                break;
        }
        break;
    case 'tuitions':
        // Tăng lượt tương tác - TaiLA
        addInteractive($child['school_id'], 'tuition', 'parent_view');
        switch ($_GET['sub_view']) {
            case '':
                if (!isset($_GET['child_id']) || !is_numeric($_GET['child_id'])) {
                    _error(404);
                }
                $data = $tuitionDao->getTuitionsByChild($_GET['child_id']);
                if (is_null($data)) {
                    _error(404);
                }

                $smarty->assign('rows', $data);
                page_header(__("Child Management") . " &rsaquo; " . __("Tuition"));
                break;
            case 'detail':
                if (!isset($_GET['child_id']) || !is_numeric($_GET['child_id'])) {
                    _error(404);
                }
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }
                // Tăng lượt tương tác - TaiLA
                addInteractive($child['school_id'], 'tuition', 'parent_view', $_GET['id']);
                $data = $tuitionDao->getTuitionDetailOfChildInMonth($_GET['id'], $_GET['child_id']);
                if (is_null($data)) {
                    _error(404);
                }

                /* Coniu - Tương tác trường */
                $tuitionChildIds = array();
                $tuitionChildIds[] = $data['tuition_child_id'];
                increaseCountViews($child['school_id'], 'tuition', $tuitionChildIds);
                /* Coniu - END */

                $smarty->assign('data', $data);
                page_header(__("Child Management") . " &rsaquo; " . __("Tuition") . " &rsaquo; " . __("Detail"));
                break;
            case 'history':
                if (!isset($_GET['child_id']) || !is_numeric($_GET['child_id'])) {
                    _error(404);
                }
                $data = $tuitionDao->getTuitionChildHistory($_GET['id'], $_GET['child_id']);
                if (is_null($data)) {
                    _error(404);
                }

                $smarty->assign('data', $data);
                page_header(__("Child Management") . " &rsaquo; " . __("Tuition") . " &rsaquo; " . __("History"));
                break;
            default:
                _error(404);
                break;
        }


        break;
    case 'reports':
        // Tăng lượt tương tác - TaiLA
        addInteractive($child['school_id'], 'report', 'parent_view');
        switch ($_GET['sub_view']) {
            case "":
                page_header(__("Child Management") . " &rsaquo; " . __("Contact book"));
                $reports = $reportDao->getChildReport($child['child_id']);

                $smarty->assign('rows', $reports);
                break;
            case "detail":
                page_header(__("Child Management") . " &rsaquo; " . __("Contact book") . " &rsaquo; " . __('Detail'));
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }
                $data = $reportDao->getReportById($_GET['id']);
                if (is_null($data)) {
                    _error(404);
                }
                /* Coniu - Tương tác trường */
                $reportIds = array();
                $reportIds[] = $_GET['id'];
                increaseCountViews($child['school_id'], 'report', $reportIds);
                addInteractive($child['school_id'], 'report', 'parent_view', $_GET['id']);
                /* Coniu - END */
                $smarty->assign('data', $data);

                // Start get points list
                $class = getClassData($child['class_id'], CLASS_INFO);
                //Lấy ra class_id của lớp
                $class_id = $child['class_id'];
                $school_year = date("Y", strtotime($data['created_at'])) . '-' . (date("Y", strtotime($data['created_at'])) + 1);

                // Lấy school_config
                $studentCode = $child['child_code'];
                $schoolConfig = getSchoolData($child['school_id'], SCHOOL_CONFIG);
                // Lấy chi tiết gov_class_level
                $class_level = getClassLevelData($class['class_level_id'], CLASS_LEVEL_INFO);
                // Lấy danh sách điểm của trẻ trong năm học
                $children_point = $pointDao->getChildrenPointsByStudentCode($class_id, $school_year, $studentCode);
                // Lấy child_name

                if (count($children_point) > 0) {
                    $children_point_last = array();
                    // Lưu các điểm đã được tính toán
                    $children_point_avgs = array();
                    // Lưu điểm của các môn thi lại
                    $children_subject_reexams = array();
                    // flag đánh dấu đã đủ điểm các môn hay chưa
                    $flag_enough_point = true;
                    // trạng thái fail/pass hay re-exam hay chưa đủ điểm ( Waiting.. )
                    $child_id = $childDao->getChildIdByCode($child['child_code']);
                    // Lấy số buổi nghỉ có phép và không phép của học sinh
                    $child_absent = $childDao->getAbsentsChild($child_id, $class_id);
                    // điểm trung bình năm
                    $child_avYear_point = 0;
                    $status_result = 'fail';
                    $subject_key = array();
                    $children_point_last = array();
                    // lưu số cột mỗi đầu điểm
                    $countColumnMaxPoint_hk1 = 2;
                    $countColumnMaxPoint_hk2 = 2;
                    $countColumnMaxPoint_gk1 = 1;
                    $countColumnMaxPoint_gk2 = 1;

                    if ($schoolConfig['score_fomula'] == "km") {
                        // điểm các tháng của 2 kỳ
                        $child_m11_point = $child_m12_point = $child_m13_point = $child_m21_point = $child_m22_point = $child_m23_point = 0;
                        // điểm trung bình các tháng của 2 kỳ
                        $child_avM1_point = $child_avM2_point = 0;
                        // điểm trung bình của cuối kỳ
                        $child_avd1_point = 0;
                        $child_avd2_point = 0;
                        $child_final_semester1 = $child_final_semester2 = 0;

                        // Mặc định cột điểm của khmer là 3
                        $countColumnMaxPoint_hk1 = 3;
                        $countColumnMaxPoint_hk2 = 3;
                        foreach ($children_point as $child) {
                            // Nếu bất kì điểm nào thiếu sẽ đánh dấu là chưa đủ điểm
                            if (!isset($child['hs11']) || !isset($child['hs12']) || !isset($child['hs13']) || !isset($child['gk11'])
                                || !isset($child['hs21']) || !isset($child['hs22']) || !isset($child['hs23']) || !isset($child['gk21'])) {
                                $flag_enough_point = false;
                            }
                            // tạm thời tính tổng các đầu điểm
                            // kỳ 1
                            $child_m11_point += (int)$child['hs11'];
                            $child_m12_point += (int)$child['hs12'];
                            $child_m13_point += (int)$child['hs13'];
                            $child_avd1_point += (int)$child['gk11'];
                            // kỳ 2
                            $child_m21_point += (int)$child['hs21'];
                            $child_m22_point += (int)$child['hs22'];
                            $child_m23_point += (int)$child['hs23'];
                            $child_avd2_point += (int)$child['gk21'];

                            // lấy tên môn học
                            $subject_detail = $subjectDao->getSubjectById($child['subject_id']);
                            $child['subject_name'] = $subject_detail['subject_name'];
                            if (isset($subject_detail['re_exam']) && $subject_detail['re_exam']) { // nếu môn đó là môn thi lại thì lưu vào array
                                $subject_reexam['name'] = $subject_detail['subject_name'];
                                $subject_reexam['point'] = $child['re_exam'];
                                $children_subject_reexams[] = $subject_reexam;
                            }

                            $children_point_last[] = $child;
                        }


                        // định nghĩa số bị chia của từng khối
                        if ($class_level['gov_class_level'] == '7' || $class_level['gov_class_level'] == '8') {
                            $dividend = 15;
                        } else if ($class_level['gov_class_level'] == '9') {
                            $dividend = 11.4;
                        } else if ($class_level['gov_class_level'] == '10') {
                            $dividend = 15.26;
                        } else if ($class_level['gov_class_level'] == '11') {
                            $dividend = 16.5;
                        } else if ($class_level['gov_class_level'] == '12') {
                            $dividend = 14.5;
                        }
                        // tính điểm theo công thức từng khối
                        // kỳ 1
                        $child_m11_point /= $dividend;
                        $child_m12_point /= $dividend;
                        $child_m13_point /= $dividend;
                        $child_avd1_point /= $dividend;
                        $child_avM1_point = ($child_m11_point + $child_m12_point + $child_m13_point) / 3;
                        $child_final_semester1 = ($child_avd1_point + $child_avM1_point) / 2;
                        // kỳ 2
                        $child_m21_point /= $dividend;
                        $child_m22_point /= $dividend;
                        $child_m23_point /= $dividend;
                        $child_avd2_point /= $dividend;
                        $child_avM2_point = ($child_m21_point + $child_m22_point + $child_m23_point) / 3;
                        $child_final_semester2 = ($child_avd2_point + $child_avM2_point) / 2;
                        // cả năm
                        $child_avYear_point = ($child_final_semester1 + $child_final_semester2) / 2;
                        // lưu lại vào array
                        $children_point_avgs['a1'] = $child_m11_point;
                        $children_point_avgs['b1'] = $child_m12_point;
                        $children_point_avgs['c1'] = $child_m13_point;
                        $children_point_avgs['d1'] = $child_avd1_point;
                        $children_point_avgs['x1'] = $child_avM1_point;
                        $children_point_avgs['e1'] = $child_final_semester1;
                        $children_point_avgs['a2'] = $child_m21_point;
                        $children_point_avgs['b2'] = $child_m22_point;
                        $children_point_avgs['c2'] = $child_m23_point;
                        $children_point_avgs['d2'] = $child_avd2_point;
                        $children_point_avgs['x2'] = $child_avM2_point;
                        $children_point_avgs['e2'] = $child_final_semester2;
                        $children_point_avgs['y'] = $child_avYear_point;

                        //xét điều kiện để đánh giá pass hay fail
                        if ($child_avYear_point < 25) { // dưới 25 điểm thì fail chặt
                            $status_result = 'Fail';
                        } else { // nếu trên 25 điểm thì xét các điều kiện khác
                            $status_result = 'Pass';
                            if ((isset($child_absent['absent_true']) && (int)$child_absent['absent_true'] >= 55)
                                || (isset($child_absent['absent_false']) && (int)$child_absent['absent_false'] >= 30)
                                || (((int)$child_absent['absent_true'] + (int)$child_absent['absent_false']) >= 55)) {
                                if ($class_level['gov_class_level'] == '7' || $class_level['gov_class_level'] == '8'
                                    || $class_level['gov_class_level'] == '10' || $class_level['gov_class_level'] == '11') {
                                    $status_result = 'Re-exam';
                                } else if ($class_level['gov_class_level'] == '9' || $class_level['gov_class_level'] == '12') {
                                    $status_result = 'Pass';
                                }
                            }
                        }
                        // xét đến đánh giá điểm thi lại
                        if ($status_result == 'Re-exam') {
                            $result_exam = 0;
                            foreach ($children_subject_reexams as $subject_reexam) {
                                if (!isset($subject_reexam['point'])) {
                                    $flag_enough_point = false;
                                } else {
                                    $result_exam += (int)$subject_reexam['point'];
                                }
                            }
                            if ($class_level['gov_class_level'] == '7' || $class_level['gov_class_level'] == '8'
                                || $class_level['gov_class_level'] == '11') {
                                $result_exam /= 4;
                            } else if ($class_level['gov_class_level'] == '10') {
                                $result_exam /= 4;
                            }
                            // nếu điểm thi lại trên 25 điểm thì pass còn lại fail
                            if ($flag_enough_point && $result_exam >= 25) {
                                $status_result = 'Pass';
                            } else if ($flag_enough_point && $result_exam < 25) {
                                $status_result = 'Fail';
                            } else { // chuaw du diem
                                $status_result = 'Re-exam';
                            }
                        }

                        if (count($children_point_last) == 0) {
                            $flag_enough_point = false;
                        }
                        if (!$flag_enough_point && $status_result != 'Re-exam') {
                            $status_result = 'Waiting..';
                        }
                        $subject_key[] = 'hs11';
                        $subject_key[] = 'hs12';
                        $subject_key[] = 'hs13';
                        $subject_key[] = 'gk11';
                        $subject_key[] = 'hs21';
                        $subject_key[] = 'hs22';
                        $subject_key[] = 'hs23';
                        $subject_key[] = 'gk21';
                        $smarty->assign("result_exam", $result_exam);

                    } else if ($schoolConfig['score_fomula'] == "vn") {
                        // điều kiện điểm của môn 1: kém, 2 yếu, 3 trung bình, 4 tiên tiến, 5 giỏi
                        $point_avg1_condition1 = 5;//hk1
                        $point_avg2_condition1 = 5;//hk2
                        $point_avgYearAll_condition1 = 5;// cả năm
                        // điều kiện điểm của môn toán hoặc văn  1: kém, 2 yếu, 3 trung bình, 4 tiên tiến, 5 giỏi
                        $point_avg1_condition2 = 5; //hk1
                        $point_avg2_condition2 = 5; //hk2
                        $point_avgYearAll_condition2 = 5; // cả năm
                        // trạng thái
                        $status_point1 = '';
                        $status_point2 = '';
                        $status_pointAll = '';
                        // flag đánh dấu học sinh đã thi lại hay chưa
                        $flag_reexam = false;
                        // điểm trung bình các môn 2 kỳ
                        $child_TBM1_point = $child_TBM2_point = 0;
                        // điểm trung bình năm
                        $child_avYearAll_point = 0;
                        // TÍnh điểm mỗi môn

                        // lưu số cột mỗi đầu điểm

                        // flag đánh dấu đã đủ điểm các môn hay chưa
                        $flag_enough_point = true;
                        // trạng thái fail/pass hay re-exam hay chưa đủ điểm ( Waiting.. )
                        $status_result = 'fail';

                        // Lấy danh sách điểm tất cả các môn của trẻ trong năm học để tính điêm trung bình năm
                        foreach ($children_point as $each) {
                            if (!isset($each['hs11']) || !isset($each['hs12']) || !isset($each['gk11'])
                                || !isset($each['hs21']) || !isset($each['hs22']) || !isset($each['gk21'])
                                || !isset($each['ck1']) || !isset($each['ck2'])) {
                                $flag_enough_point = false;
                            }
                            // điểm hệ số 2 kỳ
                            $child_hk1_point = $child_hk2_point = 0;
                            $child_TBM1_current_point = $child_TBM2_current_point = 0;
                            // đếm số điểm đã có
                            $count_hk1_point = 0;
                            $count_hk2_point = 0;
                            //Tính điểm trung bình môn

                            for ($i = 1; $i <= 6; $i++) {
                                // điểm hệ số 1
                                if (isset($each['hs1' . $i])) {
                                    $child_hk1_point += (int)$each['hs1' . $i];
                                    $count_hk1_point++;
                                    // lưu số cột đã có điểm của hệ số 1 kỳ 1
                                    if ($i > $countColumnMaxPoint_hk1) {
                                        $countColumnMaxPoint_hk1 = $i;
                                    }
                                }
                                if (isset($each['hs2' . $i])) {
                                    $child_hk2_point += (int)$each['hs2' . $i];
                                    $count_hk2_point++;
                                    // lưu số cột đã có điểm của hệ số 1 kỳ 2
                                    if ($i > $countColumnMaxPoint_hk2) {
                                        $countColumnMaxPoint_hk2 = $i;
                                    }
                                }
                                // điểm hệ số 2
                                if (isset($each['gk1' . $i])) {
                                    $child_hk1_point += (int)$each['gk1' . $i] * 2;
                                    $count_hk1_point += 2;
                                    // lưu số cột đã có điểm
                                    if ($i > $countColumnMaxPoint_gk1) {
                                        $countColumnMaxPoint_gk1 = $i;
                                    }

                                }
                                if (isset($each['gk2' . $i])) {
                                    $child_hk2_point += (int)$each['gk2' . $i] * 2;
                                    $count_hk2_point += 2;
                                    // lưu số cột đã có điểm
                                    if ($i > $countColumnMaxPoint_gk2) {
                                        $countColumnMaxPoint_gk2 = $i;
                                    }
                                }
                            }
                            //điểm cuối kì

                            if (isset($each['ck1'])) {
                                $child_hk1_point += (int)$each['ck1'] * 3;
                                $count_hk1_point += 3;
                            }
                            if (isset($each['ck2'])) {
                                // Nếu có điểm thi lại thì điểm tổng kết sẽ tính dựa theo điểm thi lại
                                if (isset($each['re_exam'])) {
                                    $child_hk2_point += (int)$each['re_exam'] * 3;
                                    $flag_reexam = true;
                                } else {
                                    $child_hk2_point += (int)$each['ck2'] * 3;
                                }
                                $count_hk2_point += 3;
                            }
                            if ($count_hk1_point > 0) {
                                $child_TBM1_current_point = $child_hk1_point / $count_hk1_point;
                                $each['tb_hk1'] = number_format($child_TBM1_current_point, 2);
                            }
                            if ($count_hk2_point > 0) {
                                $child_TBM2_current_point = $child_hk2_point / $count_hk2_point;
                                $each['tb_hk2'] = number_format($child_TBM2_current_point, 2);
                            }
                            // Điểm tổng kết
                            $child_avYear_point = ($child_TBM1_current_point + $child_TBM2_current_point * 2) / 3;

                            $each['tb_year'] = number_format($child_avYear_point, 2);
                            // xét xem điều kiện các môn ở mức nào
                            //học kỳ 1
                            if ($child_TBM1_current_point < 2 && $point_avg1_condition1 > 1) {
                                $point_avg1_condition1 = 1;
                            } else if ($child_TBM1_current_point < 3.5 && $point_avg1_condition1 > 2) {
                                $point_avg1_condition1 = 2;
                            } else if ($child_TBM1_current_point < 5 && $point_avg1_condition1 > 3) {
                                $point_avg1_condition1 = 3;
                            } else if ($child_TBM1_current_point < 6.5 && $point_avg1_condition1 > 4) {
                                $point_avg1_condition1 = 4;
                            }
                            //học kỳ 2
                            if ($child_TBM2_current_point < 2 && $point_avg2_condition1 > 1) {
                                $point_avg2_condition1 = 1;
                            } else if ($child_TBM2_current_point < 3.5 && $point_avg2_condition1 > 2) {
                                $point_avg2_condition1 = 2;
                            } else if ($child_TBM2_current_point < 5 && $point_avg2_condition1 > 3) {
                                $point_avg2_condition1 = 3;
                            } else if ($child_TBM2_current_point < 6.5 && $point_avg2_condition1 > 4) {
                                $point_avg2_condition1 = 4;
                            }
                            // cả năm
                            if ($child_avYear_point < 2 && $point_avgYearAll_condition1 > 1) {
                                $point_avgYearAll_condition1 = 1;
                            } else if ($child_avYear_point < 3.5 && $point_avgYearAll_condition1 > 2) {
                                $point_avgYearAll_condition1 = 2;
                            } else if ($child_avYear_point < 5 && $point_avgYearAll_condition1 > 3) {
                                $point_avgYearAll_condition1 = 3;
                            } else if ($child_avYear_point < 6.5 && $point_avgYearAll_condition1 > 4) {
                                $point_avgYearAll_condition1 = 4;
                            }
                            // xét xem điều kiện của môn toán văn
                            // lấy tên môn học
                            $subject_detail = $subjectDao->getSubjectById($each['subject_id']);
                            $each['subject_name'] = $subject_detail['subject_name'];
                            if (convert_vi_to_en(strtolower(html_entity_decode($subject_detail['subject_name']))) == "toan" || convert_vi_to_en(strtolower(html_entity_decode($subject_detail['subject_name']))) == "van") {
                                //học kỳ 1
                                if ($child_TBM1_current_point < 2 && $point_avg1_condition2 > 1) {
                                    $point_avg1_condition2 = 1;
                                } else if ($child_TBM1_current_point < 3.5 && $point_avg1_condition2 > 2) {
                                    $point_avg1_condition2 = 2;
                                } else if ($child_TBM1_current_point < 5 && $point_avg1_condition2 > 3) {
                                    $point_avg1_condition2 = 3;
                                } else if ($child_TBM1_current_point < 6.5 && $point_avg1_condition2 > 4) {
                                    $point_avg1_condition2 = 4;
                                }
                                // học kỳ 2
                                if ($child_TBM2_current_point < 2 && $point_avg2_condition2 > 1) {
                                    $point_avg2_condition2 = 1;
                                } else if ($child_TBM2_current_point < 3.5 && $point_avg2_condition2 > 2) {
                                    $point_avg2_condition2 = 2;
                                } else if ($child_TBM2_current_point < 5 && $point_avg2_condition2 > 3) {
                                    $point_avg2_condition2 = 3;
                                } else if ($child_TBM2_current_point < 6.5 && $point_avg2_condition2 > 4) {
                                    $point_avg2_condition2 = 4;
                                }
                                // cả năm
                                if ($child_avYear_point < 2 && $point_avgYearAll_condition2 > 1) {
                                    $point_avgYearAll_condition2 = 1;
                                } else if ($child_avYear_point < 3.5 && $point_avgYearAll_condition2 > 2) {
                                    $point_avgYearAll_condition2 = 2;
                                } else if ($child_avYear_point < 5 && $point_avgYearAll_condition2 > 3) {
                                    $point_avgYearAll_condition2 = 3;
                                } else if ($child_avYear_point < 6.5 && $point_avgYearAll_condition2 > 4) {
                                    $point_avgYearAll_condition2 = 4;
                                }
                            }
                            // Cộng vào điểm trung bình các môn
                            $child_TBM1_point += $child_TBM1_current_point;
                            $child_TBM2_point += $child_TBM2_current_point;
                            $child_avYearAll_point += $child_avYear_point;
                            $children_point_last[] = $each;
                        }
                        // điểm trung bình các môn
                        $child_TBM1_point /= count($children_point);
                        $child_TBM2_point /= count($children_point);
                        $child_avYearAll_point /= count($children_point);

                        //xét điều kiện để đánh giá pass hay fail
                        //học kỳ 1
                        if ($child_TBM1_point >= 8 && $point_avg1_condition1 == 5 && $point_avg1_condition2 == 5) { // trên 8 điểm thì đánh giá loại giỏi
                            $status_point1 = 5;
                        } else if ($child_TBM1_point >= 8 && ($point_avg1_condition1 == 3 || $point_avg1_condition2 == 3)) {
                            $status_point1 = 4;
                        } else if ($child_TBM1_point >= 8 && ($point_avg1_condition1 == 2 || $point_avg1_condition2 == 2)) {
                            $status_point1 = 3;
                        } else if ($child_TBM1_point >= 6.5 && $point_avg1_condition1 >= 4 && $point_avg1_condition2 >= 4) {  // trên 6.5 điểm thì đánh giá loại tiên tiến
                            $status_point1 = 4;
                        } else if ($child_TBM1_point >= 6.5 && ($point_avg1_condition1 == 2 || $point_avg1_condition2 == 2)) {  // trên 6.5 điểm thì đánh giá loại tiên tiến
                            $status_point1 = 3;
                        } else if ($child_TBM1_point >= 6.5 && ($point_avg1_condition1 == 1 || $point_avg1_condition2 == 1)) {  // trên 6.5 điểm thì đánh giá loại tiên tiến
                            $status_point1 = 2;
                        } else if ($child_TBM1_point >= 5 && $point_avg1_condition1 >= 3 && $point_avg1_condition2 >= 3) {  // trên 5 điểm thì đánh giá loại trung bình
                            $status_point1 = 3;
                        } else if ($child_TBM1_point >= 3.5 && $point_avg1_condition1 >= 2 && $point_avg1_condition2 >= 2) {  // trên 3.5 điểm thì đánh giá loại yếu
                            $status_point1 = 2;
                        } else if ($child_TBM1_point < 3.5 && $point_avg1_condition1 >= 1 && $point_avg1_condition2 >= 1) {
                            $status_point1 = 1;
                        }
                        // học kỳ 2
                        if ($child_TBM2_point >= 8 && $point_avg2_condition1 == 5 && $point_avg2_condition2 == 5) { // trên 8 điểm thì đánh giá loại giỏi
                            $status_point2 = 5;
                        } else if ($child_TBM2_point >= 8 && ($point_avg2_condition1 == 3 || $point_avg2_condition2 == 3)) {
                            $status_point2 = 4;
                        } else if ($child_TBM2_point >= 8 && ($point_avg2_condition1 == 2 || $point_avg2_condition2 == 2)) {
                            $status_point2 = 3;
                        } else if ($child_TBM2_point >= 6.5 && $point_avg2_condition1 >= 4 && $point_avg2_condition2 >= 4) {  // trên 6.5 điểm thì đánh giá loại tiên tiến
                            $status_point2 = 4;
                        } else if ($child_TBM2_point >= 6.5 && ($point_avg2_condition1 == 2 || $point_avg2_condition2 == 2)) {  // trên 6.5 điểm thì đánh giá loại tiên tiến
                            $status_point2 = 3;
                        } else if ($child_TBM2_point >= 6.5 && ($point_avg2_condition1 == 1 || $point_avg2_condition2 == 1)) {  // trên 6.5 điểm thì đánh giá loại tiên tiến
                            $status_point2 = 2;
                        } else if ($child_TBM2_point >= 5 && $point_avg2_condition1 >= 3 && $point_avg2_condition2 >= 3) {  // trên 5 điểm thì đánh giá loại trung bình
                            $status_point2 = 3;
                        } else if ($child_TBM2_point >= 3.5 && $point_avg2_condition1 >= 2 && $point_avg2_condition2 >= 2) {  // trên 3.5 điểm thì đánh giá loại yếu
                            $status_point2 = 2;
                        } else if ($child_TBM2_point < 3.5 && $point_avg2_condition1 >= 1 && $point_avg2_condition2 >= 1) {
                            $status_point2 = 1;
                        }
                        // cả năm
                        if ($child_avYearAll_point >= 8 && $point_avgYearAll_condition1 == 5 && $point_avgYearAll_condition2 == 5) { // trên 8 điểm thì đánh giá loại giỏi
                            $status_pointAll = 5;
                        } else if ($child_avYearAll_point >= 8 && ($point_avgYearAll_condition1 == 3 || $point_avgYearAll_condition2 == 3)) {
                            $status_pointAll = 4;
                        } else if ($child_avYearAll_point >= 8 && ($point_avgYearAll_condition1 == 2 || $point_avgYearAll_condition2 == 2)) {
                            $status_pointAll = 3;
                        } else if ($child_avYearAll_point >= 6.5 && $point_avgYearAll_condition1 >= 4 && $point_avgYearAll_condition2 >= 4) {  // trên 6.5 điểm thì đánh giá loại tiên tiến
                            $status_pointAll = 4;
                        } else if ($child_avYearAll_point >= 6.5 && ($point_avgYearAll_condition1 == 2 || $point_avgYearAll_condition2 == 2)) {  // trên 6.5 điểm thì đánh giá loại tiên tiến
                            $status_pointAll = 3;
                        } else if ($child_avYearAll_point >= 6.5 && ($point_avgYearAll_condition1 == 1 || $point_avgYearAll_condition2 == 1)) {  // trên 6.5 điểm thì đánh giá loại tiên tiến
                            $status_pointAll = 2;
                        } else if ($child_avYearAll_point >= 5 && $point_avgYearAll_condition1 >= 3 && $point_avgYearAll_condition2 >= 3) {  // trên 5 điểm thì đánh giá loại trung bình
                            $status_pointAll = 3;
                        } else if ($child_avYearAll_point >= 3.5 && $point_avgYearAll_condition1 >= 2 && $point_avgYearAll_condition2 >= 2) {  // trên 3.5 điểm thì đánh giá loại yếu
                            $status_pointAll = 2;
                        } else if ($child_avYearAll_point < 3.5 && $point_avgYearAll_condition1 >= 1 && $point_avgYearAll_condition2 >= 1) {
                            $status_pointAll = 1;
                        }

                        // Lấy hạnh kiểm của học sinh
                        $conduct = $conductDao->getConductOfChildren($class_id, $child_id, $school_year);
                        if (!isset($conduct) || !isset($conduct['ck'])) {
                            $flag_enough_point = false;
                        } else if ($child_absent['absent_true'] + $child_absent['absent_false'] <= 45
                            && $status_pointAll >= 3 && $conduct['ck'] != "Weak") {
                            $status_result = "pass";
                        }

                        if (!$flag_enough_point) {
                            $status_result = "waiting..";
                        }
//                            $children_point_last[] = $child;

                        // setup subject_key
                        for ($i = 1; $i <= $countColumnMaxPoint_hk1; $i++) {
                            $subject_key[] = 'hs1' . $i;
                        }
                        for ($i = 1; $i <= $countColumnMaxPoint_gk1; $i++) {
                            $subject_key[] = 'gk1' . $i;
                        }
                        $subject_key[] = 'ck1';
                        $subject_key[] = 'tb_hk1';
                        for ($i = 1; $i <= $countColumnMaxPoint_hk2; $i++) {
                            $subject_key[] = 'hs2' . $i;
                        }
                        for ($i = 1; $i <= $countColumnMaxPoint_gk2; $i++) {
                            $subject_key[] = 'gk2' . $i;
                        }
                        $subject_key[] = 'ck2';
                        $subject_key[] = 'tb_hk2';
                        $subject_key[] = 're_exam';
                        $smarty->assign("tb_total_hk1", $child_TBM1_point);
                        $smarty->assign("tb_total_hk2", $child_TBM2_point);
                        $smarty->assign("tb_total_year", $child_avYearAll_point);
                    }
                    $smarty->assign("column_hk1", $countColumnMaxPoint_hk1);
                    $smarty->assign("column_hk2", $countColumnMaxPoint_hk2);
                    $smarty->assign("column_gk1", $countColumnMaxPoint_gk1);
                    $smarty->assign("column_gk2", $countColumnMaxPoint_gk2);
                    $smarty->assign("rows", $children_point_last);
                    $smarty->assign("subject_key", $subject_key);
                    $smarty->assign("status", $status_result);
                    $smarty->assign("child_absent", $child_absent);
                    $smarty->assign("children_point_avgs", $children_point_avgs);
                    $smarty->assign("children_subject_reexams", $children_subject_reexams);
                }
                $smarty->assign("point_module", $schoolConfig['points']);
                $smarty->assign("grade", $schoolConfig['grade']);
                $smarty->assign('score_fomula', $schoolConfig['score_fomula']);

                // End points list
                break;
            default:
                _error(404);
                break;
        }
        break;
    case 'points':
        addInteractive($child['school_id'], 'points', 'parent_view');

        page_header(__("Child Management") . " &rsaquo; " . __("Points"));

        switch ($_GET['sub_view']) {
            case 'detail':
                error_log('child points 864');
                page_header(__("Child Management") . " &rsaquo; " . __("Point"));
                $smarty->assign('child', $child);
                $smarty->assign('default_year', $default_school_year);
                error_log('child points 868');
                break;
            case 'comment':
                page_header(__("class Management") . " &rsaquo; " . __("Point") . " &rsaquo; " . __("Comment"));
                $smarty->assign('child', $child);
                $smarty->assign('semesters', $semesters);
                $smarty->assign('username', $class['group_name']);
                $smarty->assign('default_year', $default_school_year);
                break;
            default:
                _error(404);
                break;
        }
        break;
    // ADD START MANHDD 17/06/2021
    case 'courses':
        addInteractive($child['school_id'], 'course', 'parent_view');
        switch ($_GET['sub_view']) {
            case '':
                global $default_school_year;
                page_header(__("Class Management") . " &rsaquo; " . __("Courses"));

                $url = 'https://elearning.mascom.com.vn/api/course/get/list';
                $data = array('secret' => '7402b1e6-933d-42eb-aff0-acceb5ccd13e',
                    'userId' => '',
                    'schoolId' => $class['school_id'],
                    'classLevelId' => $class['class_level_id'],
                    'groupId' => $class['group_id'],
                    'limit' => '100',
                    'offset' => '0');
                $data = http_build_query($data);
//                $data_string = json_encode($data);
                $curl = curl_init();
                curl_setopt($curl, CURLOPT_URL, $url);
                curl_setopt($curl, CURLOPT_POST, 1);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 5.1; rv:5.0) Gecko/20100101 Firefox/5.0 Firefox/5.0');
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($curl, CURLOPT_TIMEOUT, 30);
                curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
                curl_setopt($curl, CURLOPT_MAXREDIRS, 10);
                curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
                $contents = curl_exec($curl);
                $status = curl_getinfo($curl);
                curl_close($curl);
                if ($status['http_code'] == 200) {
                    $contents = json_decode($contents, true);
                    if ($contents['error']) {
                        throw new Exception($contents['error']['message'] . ' Error Code #' . $contents['error']['code']);
                    }
                } else {
                    throw new Exception("Error Processing Request");
                }

                $smarty->assign('result', $contents['course']);
                $return['results'] = $smarty->fetch("ci/child/ajax.courselist.tpl");
                break;
            default:
                _error(404);
                break;
        }
        break;
    // ADD END MANHDD 17/06/2021
    case 'feedback':
        // Tăng lượt tương tác - TaiLA
        addInteractive($child['school_id'], 'feedback', 'parent_view');
        switch ($_GET['sub_view']) {
            case '':
                page_header(__("Child Management") . " &rsaquo; " . __("Feedback"));
                break;

            case 'list':
                page_header(__("Child Management") . " &rsaquo; " . __("Feedback"));
                $feedbacks = $feedbackDao->getFeedbackOfUser($user->_data['user_id']);
                $smarty->assign('rows', $feedbacks);
                break;
        }
        break;
    case 'pickup':
        // Tăng lượt tương tác - TaiLA
        addInteractive($child['school_id'], 'pickup', 'parent_view');
        if ($pickup_configured) {
            switch ($_GET['sub_view']) {
                case '':
                    // page header
                    page_header(__("Child Management") . " &rsaquo; " . __("Late pickup") . " &rsaquo; " . __("Information"));
                    $template = $pickupDao->getTemplate($school['page_id'], false, true, true);
                    $smarty->assign('template', $template);
                    break;

                case 'list':
                    page_header(__("Child Management") . " &rsaquo; " . __("Late pickup") . " &rsaquo; " . __("Lists"));

                    $toDate = date('Y-m-t');
                    $fromDate = date('Y-m-01');

                    $total = 0;
                    $results = $pickupDao->getChildPickupHistory($class['school_id'], $child['child_id'], $fromDate, $toDate, $total);

                    /*echo '<pre>';
                    print_r($results);
                    echo '<pre>'; die;*/

                    $smarty->assign('results', $results);
                    $smarty->assign('fromDate', toSysDate($fromDate));
                    $smarty->assign('toDate', toSysDate($toDate));
                    $smarty->assign('total', $total);
                    break;

                default:
                    _error(404);
                    break;
            }
        } else _error(404);

        break;

    case 'leaveschool':
        page_header(__("Child Management") . " &rsaquo; " . __("Leave school"));

        break;

    default:
        _error(404);
}

// assign variables
$smarty->assign('class_id', $class['group_id']);
$smarty->assign('school', $school);
$smarty->assign('school_id', $school['page_id']);
$smarty->assign('view', $_GET['view']);
error_log('child.php 953 - sub_view :' . $_GET['sub_view']);
$smarty->assign('sub_view', $_GET['sub_view']);

// page footer
page_footer("child");

?>