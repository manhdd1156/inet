<?php
/**
 * signout
 * 
 * @package CINet v1.1
 * @author NOGA Co., Ltd
 */

// fetch bootstrap
require('bootstrap.php');

// check user logged in
if(!$user->_logged_in) {
	redirect();
}

// sign out
$user->sign_out();
redirect();

?>