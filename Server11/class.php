<?php
/**
 * Quản lý các chức năng chính của module Inet
 *
 * @package ConIu v1
 * @author QuanND
 */

// set override_shutdown
$override_shutdown = true;

// fetch bootstrap
require('bootstrap.php');

// Kiểm tra user login rồi hay chưa
user_access();

// check username
if (is_empty($_GET['username']) || !valid_username($_GET['username'])) {
    _error(404);
}

include_once(DAO_PATH . 'dao_school.php');
include_once(DAO_PATH . 'dao_class.php');
include_once(DAO_PATH . 'dao_subject.php');
include_once(DAO_PATH . 'dao_conduct.php');
include_once(DAO_PATH . 'dao_point.php');
include_once(DAO_PATH . 'dao_teacher.php');
include_once(DAO_PATH . 'dao_child.php');
include_once(DAO_PATH . 'dao_event.php');
include_once(DAO_PATH . 'dao_medicine.php');
include_once(DAO_PATH . 'dao_tuition.php');
include_once(DAO_PATH . 'dao_parent.php');
include_once(DAO_PATH . 'dao_attendance.php');
include_once(DAO_PATH . 'dao_service.php');
include_once(DAO_PATH . 'dao_feedback.php');
include_once(DAO_PATH . 'dao_schedule.php');
include_once(DAO_PATH . 'dao_class_level.php');
include_once(DAO_PATH . 'dao_pickup.php');
include_once(DAO_PATH . 'dao_report.php');
include_once(DAO_PATH . 'dao_menu.php');
include_once(DAO_PATH . 'dao_journal.php');

$parentDao = new ParentDAO();
$schoolDao = new SchoolDAO();
$classDao = new ClassDAO();
$subjectDao = new SubjectDAO();
$teacherDao = new TeacherDAO();
$childDao = new ChildDAO();
$eventDao = new EventDAO();
$medicineDao = new MedicineDAO();
$tuitionDao = new TuitionDAO();
$attendanceDao = new AttendanceDAO();
$serviceDao = new ServiceDAO();
$feedbackDao = new FeedbackDAO();
$scheduleDao = new ScheduleDAO();
$classLevelDao = new ClassLevelDAO();
$pickupDao = new PickupDAO();
$reportDao = new ReportDAO();
$pointDao = new PointDAO();
$conductDao = new ConductDAO();
$menuDao = new MenuDAO();
$journalDao = new JournalDAO();

//Lấy ra thông tin lớp, bao gồm cả role_id của user hiện tại
//$class = $classDao->getClassByUsername($_GET['username'], $user->_data['user_id']);
$class = getClassDataByUsername($_GET['username'], CLASS_INFO);
if (is_null($class)) {
    _error(403);
}
$smarty->assign('class', $class);
$semesters['0'] = 'Cả năm';
$schoolConfig = getSchoolData($class['school_id'], SCHOOL_CONFIG);
// Lấy ra thông tin cấu hình thông báo user của trường
$notify_settings = getSchoolData($class['school_id'], SCHOOL_NOTIFICATIONS);

$schoolInfo = getSchoolData($class['school_id'], SCHOOL_INFO);
//ADD START MANHDD 02/06/2021
// Thêm shoolname vào $schoolConfig để truyền sang giao diện ( mục đích : check quyền canview, canedit )
$schoolConfig['school_name'] = $schoolInfo['page_name'];
//ADD END MANHDD 02/06/2021
$pickupConfig = getSchoolData($class['school_id'], SCHOOL_LATE_PICKUP);
$is_assigned = $pickupDao->checkPickupPermission($class['school_id']);
$is_configured = is_null($pickupConfig) ? false : true;
$is_teacher = (isset($pickupConfig['teachers']) && in_array($user->_data['user_id'], $pickupConfig['teachers'])) ? true : false;

$smarty->assign('school_name', $schoolInfo['page_name']);
$smarty->assign('pickup_is_assigned', $is_assigned);
$smarty->assign('pickup_is_configured', $is_configured);
$smarty->assign('pickup_is_teacher', $is_teacher);

// ConIu - Lấy ra danh sách schools, classes, children mà user quản lý
include_once('includes/ajax/ci/dao/dao_child.php');
$childDao = new ChildDAO();
$objects = getRelatedObjects();
// Lấy những trường đang sử dụng inet
$schoolUsing = array();
foreach ($objects['schools'] as $schoolOb) {
    if ($schoolOb['school_status'] == SCHOOL_USING_CONIU) {
        $schoolUsing[] = $schoolOb;
    }
}

$smarty->assign('schoolList', $schoolUsing);

$classUsing = array();
foreach ($objects['classes'] as $classOb) {
    if ($classOb['school_status'] == SCHOOL_USING_CONIU) {
        $classUsing[] = $classOb;
    }
}
$smarty->assign('classList', $classUsing);

$childrenOb = $childDao->getChildrenOfParent($user->_data['user_id']);

$smarty->assign('childrenList', $childrenOb);
// ConIu - END

// Cập nhật last_active
updateUserLastActive($user->_data['user_id']);

// Cập nhật users online
$today = date("Y-m-d");
updateUserOnline($user->_data['user_id']);

// page content
switch ($_GET['view']) {
    case '':
        // page header
        page_header(__("Class Management") . " &rsaquo; " . __("Dashboard"));

        //$teachers = $teacherDao->getTeacherOfClass($class['group_id']);
        $teachers = getClassData($class['group_id'], CLASS_TEACHERS);
        $insights['teachers'] = $teachers;

        //$children = $childDao->getChildrenOfClassInSession($class['group_id']);
        $children = array();
        $childs = getClassData($class['group_id'], CLASS_CHILDREN);
        foreach ($childs as $child) {
            //$parents = $parentDao->getParent($child['child_id']);
            $child['parent'] = getChildData($child['child_id'], CHILD_PARENTS);
            $children[] = $child;
        }
        $insights['children'] = $children;

//        $today = new DateTime();
        $today = date($system['date_format']);
        $medicines = $medicineDao->getClassMedicineOnDate($class['group_id'], $today, false, $countNoConfirm);

        $insights['medicines'] = $medicines;

        $event_count_on_dashboard = 3;
        $events = $eventDao->getClassEvents($class['group_id'], $class['class_level_id'], $class['school_id'], $event_count_on_dashboard);
        $insights['events'] = $events;

        $insights['class'] = $class;
        // assign variables
        $smarty->assign('insights', $insights);
        //ADD START MANHDD 11/06/2021
        $smarty->assign('user_id', $user->_data['user_id']);
        //ADD END MANHDD 11/06/2021
        break;
    case 'settings':
        // page header
        page_header(__("Class Management") . " &rsaquo; " . __("Settings"));
        break;
    case 'attendance':
        // Tăng lượt tương tác - TaiLA
        addInteractive($class['school_id'], 'attendance', 'school_view');
        switch ($_GET['sub_view']) {
            case '':
                page_header(__("Class Management") . " &rsaquo; " . __("Attendance"));

                $end = date($system['date_format']);
                $begin = date($system['date_format'], mktime(0, 0, 0, date("m"), 1, date("Y")));
                //$attendance = $attendanceDao->getAttendanceOfClass($class['group_id'], $fromDate, $toDate);
                //$smarty->assign('rows', $attendance);
                $smarty->assign('toDate', $end);
                $smarty->assign('fromDate', $begin);


                //Lấy ra điểm danh của một lớp
                $results = $attendanceDao->getFullAttendanceOfClass($class['group_id'], $begin, $end);

                //Tạo ra danh sách các ngày để build lên header của bảng.
                $fromDate = strtotime(toDBDate($begin));
                $toDate = strtotime(toDBDate($end));

                $displayDates = array();
                while ($fromDate <= $toDate) {
                    $parts = explode("-", date("Y-m-d", $fromDate));
                    $displayDates[] = $parts[2];
                    $fromDate = strtotime("+1 day", $fromDate);
                }
                $smarty->assign('dates', $displayDates);

                //Mãng 2 chiều lưu bảng điểm danh toàn lớp
                $newResults = array();

                //Băm danh sách kết quả thành mãng 2 chiều theo từng cháu
                $lastChildId = -1;
                $aRow = array(); //Dòng lưu thông tin điểm danh 1 trẻ (chỉ những ngày có điểm danh).
                foreach ($results as $result) {
                    //Nếu child_id thay đổi thì bắt đầu một dòng mới.
                    if ($lastChildId != $result['child_id']) {

                        //Nếu trước đó là một dòng (không phải là dòng đầu tiên), thì xử lý bổ xung dòng đã xong trước đó.
                        if ($lastChildId > 0) {
                            $isFirstCell = true; //Check có phải cell đầu dòng hay không
                            $newRow = array(); //Dòng lưu thông tin điểm danh 1 trẻ (bao gồm những ngày không điểm danh).
                            $newCells = array();
                            $lastDate = strtotime(toDBDate($begin));
                            foreach ($aRow as $cell) {
                                //Xét ngày đầu hàng là ngày nghỉ thì thêm thành phần vào
                                while ($isFirstCell && ($lastDate < strtotime($cell['attendance_date']))) {
                                    $newCells[] = array("is_checked" => 0);
                                    $lastDate = strtotime("+1 day", $lastDate);
                                }

                                //Xét thành phần giữa hàng, nếu có ngày nghỉ thì chèn thêm vào
                                while ((!$isFirstCell) && (strtotime("+1 day", $lastDate) < strtotime($cell['attendance_date']))) {
                                    $newCells[] = array("is_checked" => 0);
                                    $lastDate = strtotime("+1 day", $lastDate);
                                }

                                $newCells[] = $cell;
                                $newRow['child_name'] = $cell['child_name'];
                                $newRow['child_id'] = $cell['child_id'];
                                $newRow['child_status'] = $cell['child_status'];
                                $isFirstCell = false;
                                $lastDate = strtotime($cell['attendance_date']);
                            }
                            //Xét cuối hàng, nếu những ngày cuối hàng là ngày nghỉ thì thêm ô trống
                            while ($lastDate < strtotime(toDBDate($end))) {
                                $newCells[] = array("is_checked" => 0);
                                $lastDate = strtotime("+1 day", $lastDate);
                            }
                            $newRow['cells'] = $newCells;
                            $newResults[] = $newRow;
                        }
                        $aRow = array();
                    }

                    $aRow[] = $result;
                    $lastChildId = $result['child_id'];
                }

                //Xử lý hàng cuối cùng (cháu cuối cùng).
                if ($lastChildId > 0) {
                    $isFirstCell = true;
                    $newRow = array(); //Dòng lưu thông tin điểm danh 1 trẻ (bao gồm những ngày không điểm danh).
                    $newCells = array();
                    $lastDate = strtotime(toDBDate($begin));
                    foreach ($aRow as $cell) {
                        //Xét ngày đầu hàng là ngày nghỉ thì thêm thành phần vào
                        while ($isFirstCell && ($lastDate < strtotime($cell['attendance_date']))) {
                            $newCells[] = array("is_checked" => 0);
                            $lastDate = strtotime("+1 day", $lastDate);
                        }

                        //Xét thành phần giữa hàng, nếu có ngày nghỉ thì chèn thêm vào
                        while ((!$isFirstCell) && (strtotime("+1 day", $lastDate) < strtotime($cell['attendance_date']))) {
                            $newCells[] = array("is_checked" => 0);
                            $lastDate = strtotime("+1 day", $lastDate);
                        }

                        $newCells[] = $cell;
                        $newRow['child_name'] = $cell['child_name'];
                        $newRow['child_id'] = $cell['child_id'];
                        $newRow['child_status'] = $cell['child_status'];

                        $isFirstCell = false;
                        $lastDate = strtotime($cell['attendance_date']);
                    }
                    //Xét cuối hàng, nếu những ngày cuối hàng là ngày nghỉ thì thêm ô trống
                    while ($lastDate < strtotime(toDBDate($end))) {
                        $newCells[] = array("is_checked" => 0);
                        $lastDate = strtotime("+1 day", $lastDate);
                    }
                    $newRow['cells'] = $newCells;
                    $newResults[] = $newRow;

                    ///// Xử lý phần tổng hợp cuối cùng ----------
                    $attendances = $attendanceDao->getAttendanceOfClass4Summary($class['group_id'], $begin, $end);
                    $isFirstCell = true;
                    $summaries = array(); //Lưu thống kê theo ngày của lớp
                    $lastDate = strtotime(toDBDate($begin));
                    foreach ($attendances as $cell) {
                        //Xét ngày đầu hàng là ngày nghỉ thì thêm thành phần vào
                        while ($isFirstCell && ($lastDate < strtotime($cell['attendance_date']))) {
                            $lastDate = strtotime("+1 day", $lastDate);
                            $summaries[] = array("is_checked" => 0);
                        }
                        //Xét thành phần giữa hàng, nếu có ngày nghỉ thì chèn thêm vào
                        while ((!$isFirstCell) && (strtotime("+1 day", $lastDate) < strtotime($cell['attendance_date']))) {
                            $lastDate = strtotime("+1 day", $lastDate);
                            $summaries[] = array("is_checked" => 0);
                        }
                        $isFirstCell = false;
                        $lastDate = strtotime($cell['attendance_date']);

                        $summaries[] = $cell;
                    }
                    //Xét cuối hàng, nếu những ngày cuối hàng là ngày nghỉ thì thêm ô trống
                    while ($lastDate < strtotime(toDBDate($end))) {
                        $lastDate = strtotime("+1 day", $lastDate);
                        $summaries[] = array("is_checked" => 0);
                    }
                }
                //ADD START MANHDD 11/06/2021
                $smarty->assign('class_id', $class['group_id']);
                $smarty->assign('user_id', $user->_data['user_id']);
                //ADD END MANHDD 11/06/2021
                $smarty->assign('rows', $newResults);
                $smarty->assign('last_rows', $summaries);

                break;
            case 'rollup':
                page_header(__("Class Management") . " &rsaquo; " . __("Attendance") . " &rsaquo; " . __("Roll up"));

                $attendance = null;
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    //Khi bấm vào 'điểm danh ngày' ở menu trái.
                    $attendanceDate = date($system['date_format']);
                    $attendance = $attendanceDao->getAttendanceDetail($class['group_id'], $attendanceDate);
                    // Lấy thông tin điểm danh về của lớp
                    $data_cameback = $attendanceDao->getAttendanceCameBackdetail($class['group_id'], $attendanceDate);
                    for ($i = 0; $i < count($data_cameback['detail']); $i++) {
                        $data_cameback['detail'][$i]['status'] = $attendance['detail'][$i]['status'];
                    }
                } else {
                    //Xử lý khi user click vào một lần điểm danh
                    $attendance = $attendanceDao->getAttendance($_GET['id'], $class['school_id'], $class['group_id']);

                    // Lấy thông tin điểm danh về của lớp
                    $data_cameback = $attendanceDao->getAttendanceCameBackdetail($class['group_id'], $attendance['attendance_date']);
                    for ($i = 0; $i < count($data_cameback['detail']); $i++) {
                        $data_cameback['detail'][$i]['status'] = $attendance['detail'][$i]['status'];
                    }
                }
                // Lấy giờ phút hiện tại
                $hour = date('H:i');
//                echo "<pre>";
//                print_r($data);
//                print_r($data_cameback);
//                die;
                $smarty->assign('data', $attendance);
                $smarty->assign('hour', $hour);
                $smarty->assign('data_cameback', $data_cameback);

                break;
            case 'child':
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }
                //Lấy ra thông tin của trẻ
                //$child = $childDao->getChild($_GET['id']);
                $child = getChildData($_GET['id'], CHILD_INFO);
                //$child = getChildData($_GET['id'], CHILD_INFO);

                /*$childsInClass = getClassData($class['group_id'], CLASS_CHILDREN);
                if (!in_array($_GET['id'], array_keys($childsInClass))) {
                    _error(404);
                }*/

                //Kiểm tra xem trẻ có từng thuộc lớp đấy không?
                $belong = $childDao->checkChildInClass($_GET['id'], $class['group_id']);
                if (!$belong) {
                    _error(404);
                }

                $toDate = date($system['date_format']);
                $fromDate = date($system['date_format'], mktime(0, 0, 0, date("m"), 1, date("Y")));
                $attendances = $attendanceDao->getAttendanceChild($_GET['id'], $fromDate, $toDate);

                $data = array();
                $data['child'] = $child;
                $data['fromDate'] = $fromDate;
                $data['toDate'] = $toDate;
                $data['attendance'] = $attendances;
                $smarty->assign('data', $data);

                $smarty->assign('school', $schoolConfig);

                // assign ngày hôm nay để kiểm tra
                $today = date('Y-m-d');
                $smarty->assign('today', $today);

                //ADD START MANHDD 11/06/2021
                $smarty->assign('class_id', $class['group_id']);
                $smarty->assign('user_id', $user->_data['user_id']);
                //ADD END MANHDD 11/06/2021

                page_header(__("Class Management") . " &rsaquo; " . __("Attendance") . " &rsaquo; " . $child['child_name']);

                break;
            default:
                _error(404);
                break;
        }

        break;
    case 'children':
        switch ($_GET['sub_view']) {
            case '':
                // page header
                page_header(__("Class Management") . " &rsaquo; " . __("Children"));

                //$children = $childDao->getChildrenOfClass($class['group_id']);
                $children = array();
                $childs = getClassData($class['group_id'], CLASS_CHILDREN);
                foreach ($childs as $child) {
                    //$parents = $parentDao->getParent($child['child_id']);
                    $child['parent'] = getChildData($child['child_id'], CHILD_PARENTS);
                    $children[] = $child;
                }
                $smarty->assign('rows', $children);
                //ADD START MANHDD 11/06/2021
                $smarty->assign('class_id', $class['group_id']);
                $smarty->assign('user_id', $user->_data['user_id']);
                //ADD END MANHDD 11/06/2021
                break;
            case 'edit':
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }
                //$adminId = $schoolDao->getAdminId($class['school_id']);
                $schoolInfo = getSchoolData($class['school_id'], SCHOOL_INFO);
                $adminId = isset($schoolInfo['page_admin']) ? $schoolInfo['page_admin'] : 0;
                $smarty->assign('adminId', $adminId);

                //$child = $childDao->getChild($_GET['id']);
                $child = getChildData($_GET['id'], CHILD_INFO);
                if (is_null($child)) {
                    _error(404);
                }
                $smarty->assign('child', $child);

                //$parent = $parentDao->getParent($_GET['id']);
                $parent = getChildData($_GET['id'], CHILD_PARENTS);
                $smarty->assign('parent', $parent);

                page_header(__("Class Management") . " &rsaquo; " . __("Children") . " &rsaquo; " . $child['child_name']);
                break;
            case 'add':
                page_header(__("Class Management") . " &rsaquo; " . __("Children") . " &rsaquo; " . __("Add New"));
//                $classe = $classDao->getClass($class['group_id']);
//                $smarty->assign('classes', $classes);
                break;
            case 'import':
                page_header(__("School Management") . " &rsaquo; " . __("Import from Excel file"));
//                $classes = $classDao->getClassNamesOfSchool($school['page_id']);
//                $smarty->assign('classes', $classes);

                break;
            case 'detail':
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }
                //$child = $childDao->getChild($_GET['id']);
                $child = getChildData($_GET['id'], CHILD_INFO);
                if (is_null($child)) {
                    _error(404);
                }
                $smarty->assign('child', $child);

                //$parent = $parentDao->getParent($_GET['id']);
                $parent = getChildData($_GET['id'], CHILD_PARENTS);
                $smarty->assign('parent', $parent);

                //$data = $childDao->getChildInformation($_GET['id']);
                $data = getChildData($_GET['id'], CHILD_PICKER_INFO);
                $smarty->assign('data', $data);

                page_header(__("Class Management") . " &rsaquo; " . __("Children") . " &rsaquo; " . $child['child_name']);
                break;
            case 'reedit':
                // valid inputs
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }

                //$child = $childDao->getChild($_GET['id']);
                $child = getChildData($_GET['id'], CHILD_INFO);
                if (is_null($child)) {
                    _error(404);
                }
                // lấy thông tin trẻ edit
                $child_edit = $childDao->getChildEditOfTeacher($_GET['id'], $class['school_id']);

                // assign variables
                $smarty->assign('child', $child);
                $smarty->assign('child_edit', $child_edit);
                //$classes = $classDao->getClassNamesOfSchool($class['school_id']);
                $classes = getSchoolData($class['school_id'], SCHOOL_CLASSES);
                $smarty->assign('classes', $classes);

                page_header(__("School Management") . " &rsaquo; " . __("Children") . " &rsaquo; " . $child['child_name']);
                break;
            case 'health':
                page_header(__("Class Management") . " &rsaquo; " . __("Children") . " &rsaquo; " . __("Health information"));

                // valid inputs
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }

                //$child = $childDao->getChild($_GET['id']);
                $child = getChildData($_GET['id'], CHILD_INFO);
                if (is_null($child)) {
                    _error(404);
                }

                // Lấy ngày hiện tại
                $dateNow = date('Y-m-d');
                $dateNowSys = toSysDate($dateNow);

                // Lấy ngày sinh nhật của trẻ
                $birthday = toDBDate($child['birthday']);

                $begin = $child['birthday'];
                $end = $dateNowSys;
                // Khởi tạo mảng lưu chỉ số sức khỏe của trẻ
                $childGrowth = array();

                // Lấy dữ liệu từ ngày sinh đến ngày hiện tại
                // Lấy dữ liệu nhập và của trẻ
                $childGrowth = $childDao->getChildGrowthSearchForChartBeginEnd($child['child_parent_id'], $begin, $end);

                // Lấy ngày bắt đầu và ngày kết thúc
//                $begin = toDBDate($begin);
//                $end = toDBDate($end);
//
//                $end_day = (strtotime($end) - strtotime($birthday)) / (60*60*24);
//                $end_day = (int)$end_day;
//
//                $begin_day = (strtotime($begin) - strtotime($birthday)) / (60*60*24);
//                $begin_day = (int)$begin_day;

                // Tính xem trẻ được bao nhiêu ngày tuổi
                $day_age_now = (strtotime($dateNow) - strtotime($birthday)) / (60 * 60 * 24);

                /// đoạn này xử lý để lấy ngày bắt đầu (đặt tạm a, b, c; bí tên quá)
                $a = $day_age_now % 30;
                $b = $day_age_now - $a;
                $c = $b - 30 * 10;
                $dayGet = (int)$c;
                $end_day = $day_age_now + 65;
                if ($day_age_now >= 300) {
                    $begin_day = $dayGet;
                } else {
                    $begin_day = 0;
                }
                if ($begin_day < 0) {
                    throw new Exception(__("You must select day begin after birthday"));
                }

                // Dữ liệu chuẩn người dùng nhập vào vẽ biểu đồ chiều cao (thư viện charts.js)
                $heightForChartChartJS = getDataJsonForChartChartJS($childGrowth, $begin_day, $end_day, $birthday, 'height');

                // Lấy dữ liệu người dùng nhập vào vẽ biểu đồ cân nặng (charts.js)
                $weightForChartChartJS = getDataJsonForChartChartJS($childGrowth, $begin_day, $end_day, $birthday, 'weight');

                if ($child['gender'] == 'male') {
                    // Dữ liệu chuẩn
                    $hightStandardArray = getHightDataForChart($hightStandardJsonMale);
                    $weightStandardArray = getHightDataForChart($weightStandardJsonMale);
                } else {
                    $hightStandardArray = getHightDataForChart($hightStandardJsonFeMale);
                    $weightStandardArray = getHightDataForChart($weightStandardJsonFeMale);
                }

                // 1. Chiều cao
                // Lấy 2 ngày gần nhất của $begin_day có dữ liệu, nội suy chuẩn của ngày bắt đầu
                if ($begin_day < 1826) {
                    $hightStandardArray[$begin_day]['highest'] = getStandardDataBeginAndEnd($hightStandardArray, $begin_day, 'highest');

                    $hightStandardArray[$begin_day]['lowest'] = getStandardDataBeginAndEnd($hightStandardArray, $begin_day, 'lowest');
                }
                if ($end_day < 1826) {
                    // Lấy 2 ngày gần nhất của $end_day có dữ liệu, nội suy chuẩn của ngày kết thúc
                    $hightStandardArray[$end_day]['highest'] = getStandardDataBeginAndEnd($hightStandardArray, $end_day, 'highest');

                    $hightStandardArray[$end_day]['lowest'] = getStandardDataBeginAndEnd($hightStandardArray, $end_day, 'lowest');
                }

                // dỮ LIỆU đường cao nhất (thư viện charts.js)
                $highestForChartChartsJS = getStandardDataForChartChartJS($hightStandardArray, $begin_day, $end_day, 'highest');

                // Dữ liệu đường thấp nhất (thư viện charts.js)
                $lowestForChartChartsJS = getStandardDataForChartChartJS($hightStandardArray, $begin_day, $end_day, 'lowest');

                // 2. Cân nặng
                // Lấy 2 ngày gần nhất của $begin_day có dữ liệu, nội suy chuẩn của ngày bắt đầu
                if ($begin_day < 1826) {
                    $weightStandardArray[$begin_day]['highest'] = getStandardDataBeginAndEnd($weightStandardArray, $begin_day, 'highest');

                    $weightStandardArray[$begin_day]['lowest'] = getStandardDataBeginAndEnd($weightStandardArray, $begin_day, 'lowest');
                }

                // Lấy 2 ngày gần nhất của $end_day có dữ liệu, nội suy chuẩn của ngày kết thúc
                if ($end_day < 1826) {
                    $weightStandardArray[$end_day]['highest'] = getStandardDataBeginAndEnd($weightStandardArray, $end_day, 'highest');

                    $weightStandardArray[$end_day]['lowest'] = getStandardDataBeginAndEnd($weightStandardArray, $end_day, 'lowest');
                }

                // Dữ liệu đường cao nhất (charts.js)
                $highestWeightForChartChartJS = getStandardDataForChartChartJS($weightStandardArray, $begin_day, $end_day, 'highest');

                // Dữ liệu đường thấp nhất (charts.js)
                $lowestWeightForChartChartJS = getStandardDataForChartChartJS($weightStandardArray, $begin_day, $end_day, 'lowest');

                // 3. BMI (charts.js)
                $bmiForChartChartJS = getDataJsonForChartChartJS($childGrowth, $begin_day, $end_day, $birthday, 'bmi');

                // Chiều cao for charts.js
                $smarty->assign('highestForChartChartJS', $highestForChartChartsJS);
                $smarty->assign('lowestForChartChartJS', $lowestForChartChartsJS);
                $smarty->assign('heightForChartChartJS', $heightForChartChartJS);

                //Cân nặng cho charts.js
                $smarty->assign('highestWeightForChartChartJS', $highestWeightForChartChartJS);
                $smarty->assign('lowestWeightForChartChartJS', $lowestWeightForChartChartJS);
                $smarty->assign('weightForChartChartJS', $weightForChartChartJS);

                // BMI (charts.js)
                $smarty->assign('bmiForChartChartJS', $bmiForChartChartJS);

                $smarty->assign('child', $child);
                $smarty->assign('growth', array_reverse($childGrowth));

                $label = '';
                for ($i = $begin_day; $i <= $end_day; $i++) {
                    if ($i % 30 == 0) {
                        $month = $i / 30;
                        $label .= ' ' . $month . ',';
                    } else {
                        $label .= ' ,';
                    }
                }
                $smarty->assign('label', $label);
                break;
            case 'addgrowth':
                page_header(__("Class Management") . " &rsaquo; " . __("Children") . " &rsaquo; " . __("Add new growth"));

                // valid inputs
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }

                $child = getChildData($_GET['id'], CHILD_INFO);
                if (is_null($child)) {
                    _error(404);
                }

                $smarty->assign('child', $child);
                break;
            case 'editgrowth':
                page_header(__("Class Management") . " &rsaquo; " . __("Children") . " &rsaquo; " . __("Edit student growth"));

                // valid inputs
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }

                $is_module = 0;
                if (isset($_GET['p5']) && $_GET['p5'] == 'module') {
                    $is_module = 1;
                }

                $data = $childDao->getChildGrowthById($_GET['id']);
                $child = $childDao->getChildByParent($data['child_parent_id']);

                $smarty->assign('data', $data);
                $smarty->assign('child', $child);
                $smarty->assign('is_module', $is_module);
                break;
            case 'addhealthindex':
                page_header(__("Class Management") . " &rsaquo; " . __("Children") . " &rsaquo; " . __("Add health index"));
                $children = getClassData($class['group_id'], CLASS_CHILDREN);
                $smarty->assign('children', $children);
                break;
            case 'addexisting':
                page_header(__("Class Management") . " &rsaquo; " . __("Children") . " &rsaquo; " . __("Add child "));
                //$classes = $classDao->getClassNamesOfSchool($school['page_id']);
                //Memcache - Lấy ra các lớp của 1 trường
//                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
//                $smarty->assign('classes', $classes);
                $schoolData = getSchoolData($class['school_id'], SCHOOL_INFO);
                $smarty->assign('school', $schoolData);

                break;
            case 'addphoto':
                page_header(__("Class Management") . " &rsaquo; " . __("Children") . " &rsaquo; " . __("Add photo to diary"));

                // valid inputs
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }

                $child = getChildData($_GET['id'], CHILD_INFO);
                if (is_null($child)) {
                    _error(404);
                }

                $smarty->assign('child', $child);
                break;
            case 'journal':
                page_header(__("Class Management") . " &rsaquo; " . __("Children") . " &rsaquo; " . __("Diary corner"));

                // valid inputs
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }

                $child = getChildData($_GET['id'], CHILD_INFO);
                if (is_null($child)) {
                    _error(404);
                }

                $journals = $journalDao->getAllJournalChildOfSchool($child['child_parent_id']);
                $smarty->assign('results', $journals);

                $smarty->assign('child', $child);
                $smarty->assign('child_parent_id', $child['child_parent_id']);
                $smarty->assign('username', $_GET['username']);
                break;
            case 'adddiary':
                page_header(__("Class Management") . " &rsaquo; " . __("Children") . " &rsaquo; " . __("Add diary"));
                //$children = $childDao->getChildrenOfClassInSession($class['group_id']);
                $children = getClassData($class['group_id'], CLASS_CHILDREN);
                $smarty->assign('children', $children);

                break;
            default:
                _error(404);
                break;
        }
        break;
    case 'useservices':
        // Tăng lượt tương tác - TaiLA
        addInteractive($class['school_id'], 'service', 'school_view');
        switch ($_GET['sub_view']) {
            case 'record':
                page_header(__("Class Management") . " &rsaquo; " . __("Register service"));

                //Lấy ra danh sách dịch vụ tính phí theo số lần sử dụng
                //$services = $serviceDao->getCountBasedServices($class['school_id']);
                $schoolService = getSchoolData($class['school_id'], SCHOOL_SERVICES);
                $schoolService = sortArray($schoolService, "ASC", "service_name");
                $services = array();
                foreach ($schoolService as $service) {
                    if ($service['type'] == SERVICE_TYPE_COUNT_BASED && $service['status'] == 1) {
                        $services[] = $service;
                    }
                }
                $smarty->assign('services', $services);

                break;
            case 'history':
                page_header(__("Class Management") . " &rsaquo; " . __("Service usage information"));

                //Lấy ra danh sách dịch vụ tính phí theo số lần sử dụng
                //$services = $serviceDao->getCountBasedServices($class['school_id']);
                $schoolService = getSchoolData($class['school_id'], SCHOOL_SERVICES);
                $schoolService = sortArray($schoolService, "ASC", "service_name");
                $services = array();
                foreach ($schoolService as $service) {
                    if ($service['type'] == SERVICE_TYPE_COUNT_BASED) {
                        $services[] = $service;
                    }
                }
                $smarty->assign('services', $services);

                //Lấy ra danh sách trẻ của lớp
                //??? Có lấy những trẻ đã nghỉ học  hay không?
                $children = $childDao->getChildrenOfClass($class['group_id']);
                $children = sortArray($children, "DESC", "child_status");
                //$children = getClassData($class['group_id'], CLASS_CHILDREN);
                $smarty->assign('children', $children);

                $beginDate = date($system['date_format'], mktime(0, 0, 0, date("m"), 1, date("Y")));
                $smarty->assign('begin', $beginDate);

                //Nếu là trường hợp bấm link từ màn hình 'Sử dụng dịch vụ' thì lấy ra 2 thông tin service_id và child_id
                if (isset($_GET['id']) && ($_GET['id'] != '')) {
                    $ids = explode("_", trim($_GET['id']));
                    $smarty->assign('service_id', $ids[0]);
                    $smarty->assign('child_id', $ids[1]);

                    //Lấy ra thông tin lịch sử sử dụng của trẻ
                    $begin = date($system['date_format'], mktime(0, 0, 0, date("m"), 1, date("Y")));  //Ngày đầu tháng
                    $history = $serviceDao->getServiceHistory($ids[1], $ids[0], $begin, toSysDate($date));
                    $smarty->assign('history', $history);
                }
                //ADD START MANHDD 11/06/2021
                $smarty->assign('class_id', $class['group_id']);
                $smarty->assign('user_id', $user->_data['user_id']);
                //ADD END MANHDD 11/06/2021
                break;
            default:
                _error(404);
                break;
        }
        break;

    case 'events':
        // Tăng lượt tương tác - TaiLA
        addInteractive($class['school_id'], 'event', 'school_view');
        $smarty->assign('class', $class);
        switch ($_GET['sub_view']) {
            case '':
                // page header
                page_header(__("Class Management") . " &rsaquo; " . __("Notification - Event"));
                $events = $eventDao->getClassEvents($class['group_id'], $class['class_level_id'], $class['school_id']);

                $smarty->assign('rows', $events);

                break;
            case 'participants':
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }
                //Lấy ra thông tin sự kiện
                $event = $eventDao->getEvent($_GET['id']);
                if (is_null($event)) {
                    _error(404);
                }

                $childCount = 0;
                $parentCount = 0;

                $participants = $eventDao->getParticipantsOfClass($class['group_id'], $event['event_id'], $event['for_parent'], $childCount, $parentCount);
                $smarty->assign('participants', $participants);
                $smarty->assign('childCount', $childCount);
                $smarty->assign('parentCount', $parentCount);
                $smarty->assign('event', $event);

                page_header(__("Class Management") . " &rsaquo; " . __("Notification - Event") . " &rsaquo; " . $event['event_name'] . " &rsaquo; " . __("Participants"));

                break;
            case 'add':
                page_header(__("Class Management") . " &rsaquo; " . __("Notification - Event") . " &rsaquo; " . __("Add New"));
                break;
            case 'edit':
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }
                $event = $eventDao->getEvent($_GET['id']);
                if (is_null($event)) {
                    _error(404);
                }
                $smarty->assign('data', $event);

                page_header(__("Class Management") . " &rsaquo; " . __("Notification - Event") . " &rsaquo; " . $event['event_name']);
                break;
            case 'detail':
                // Tăng lượt tương tác - TaiLA
                addInteractive($class['school_id'], 'event', 'school_view', $_GET['id']);
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }
                $event = $eventDao->getEventDetail($_GET['id']);
                if (is_null($event)) {
                    _error(404);
                }
                $smarty->assign('data', $event);

                page_header(__("Class Management") . " &rsaquo; " . __("Notification - Event") . " &rsaquo; " . $event['event_name']);
                break;
            default:
                _error(404);
                break;
        }
        break;
    case 'medicines':
        // Tăng lượt tương tác - TaiLA
        addInteractive($class['school_id'], 'medicine', 'school_view');
        switch ($_GET['sub_view']) {
            case '':
                // page header
                page_header(__("Class Management") . " &rsaquo; " . __("Medicines"));
                $today = date($system['date_format']);
                $count_no_confirm = 0;
                $medicines = $medicineDao->getClassMedicineOnDate($class['group_id'], $today, false, $count_no_confirm);

                $smarty->assign('rows', $medicines);
                $smarty->assign('count_no_confirm', $count_no_confirm);

                break;
            case 'all':
                // page header
                page_header(__("Class Management") . " &rsaquo; " . __("All medicines"));
                $count_no_confirm = 0;
                $medicines = $medicineDao->getClassAllMedicines($class['group_id'], $count_no_confirm);

                $smarty->assign('rows', $medicines);
                $smarty->assign('count_no_confirm', $count_no_confirm);

                break;

            case 'add':
                page_header(__("Class Management") . " &rsaquo; " . __("Medicines") . " &rsaquo; " . __("Add New"));
                //$children = $childDao->getChildrenOfClassInSession($class['group_id']);
                $children = getClassData($class['group_id'], CLASS_CHILDREN);
                $smarty->assign('children', $children);

                break;
            case 'edit':
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }

                $medicine = $medicineDao->getMedicine($_GET['id'], false);
                if (is_null($medicine)) {
                    _error(404);
                }
                $smarty->assign('data', $medicine);

                //$children = $childDao->getChildrenOfClassInSession($class['group_id']);
                $children = getClassData($class['group_id'], CLASS_CHILDREN);
                $smarty->assign('children', $children);

                page_header(__("Class Management") . " &rsaquo; " . __("Medicines") . " &rsaquo; " . $data['medicine_list']);
                break;

            case 'detail':
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }
                include_once(DAO_PATH . 'dao_user.php');
                $userDao = new UserDAO();

                $medicine = $medicineDao->getClassMedicine4Detail($class['group_id'], $_GET['id']);
                if (is_null($medicine)) {
                    _error(404);
                }
                $medicine['confirm_user'] = '';
                $medicine['confirm_username'] = '';
                if (!is_null($medicine['confirm_user_id'])) {
                    $userConfirm = $userDao->getUserByUserId($medicine['confirm_user_id']);
                    $medicine['confirm_user'] = $userConfirm['user_fullname'];
                    $medicine['confirm_username'] = $userConfirm['user_name'];
                }

                $smarty->assign('data', $medicine);

                page_header(__("Class Management") . " &rsaquo; " . __("Medicines") . " &rsaquo; " . $medicine['medicine_list']);
                break;
            default:
                _error(404);
                break;
        }
        break;
    case 'schedules':
        // Tăng lượt tương tác - TaiLA
        addInteractive($class['school_id'], 'schedule', 'school_view');
        switch ($_GET['sub_view']) {
            case '':
                page_header(__("Class Management") . " &rsaquo; " . __("Schedule"));
                //Lấy ra danh sách lịch học áp dụng cho lớp
                $data = $scheduleDao->getScheduleOfSchoolById($class['school_id'], $class['class_level_id'], $class['group_id']);
                $removeArr = array();
                for ($i = 0; $i < count($data); $i++) {
                    for ($j = 0; $j < count($data); $j++) {
                        if ($i != $j) {
                            $beginI = strtotime(toDBDate($data[$i]['begin']));
                            $beginJ = strtotime(toDBDate($data[$j]['begin']));
                            if ($beginI == $beginJ) {
                                if ($data[$i]['applied_for'] > $data[$j]['applied_for']) {
                                    $removeArr[] = $j;
                                }
                            }
                        }
                    }
                }
                $schedules = array();
                for ($k = 0; $k < count($data); $k++) {
                    if (!in_array($k, $removeArr)) {
//                        $schedules[] = $data[$k];
                        // Thêm trường này vào để biết nó bị thay thế hay không
                        $data[$k]['use'] = STATUS_ACTIVE;
                    } else {
                        $data[$k]['use'] = STATUS_INACTIVE;
                    }
                }
//                $smarty->assign('rows', $schedules);
                $smarty->assign('rows', $data);

                //$class_levels = $classLevelDao->getClassLevels($class['school_id']);
                $class_levels = getSchoolData($class['school_id'], SCHOOL_CLASS_LEVELS);
                //$classes = $classDao->getClassesOfSchool($class['school_id']);
                $classes = getSchoolData($class['school_id'], SCHOOL_CLASSES);

                $smarty->assign('class_levels', $class_levels);
                $smarty->assign('classes', $classes);
                break;
            case 'detail':
                // Tăng lượt tương tác - TaiLA
                addInteractive($class['school_id'], 'schedule', 'school_view', $_GET['id']);
                page_header(__("Class Management") . " &rsaquo; " . __("Detail schedule"));
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }
                $data = $scheduleDao->getScheduleDetailById($_GET['id']);
                $smarty->assign('data', $data);

                if ($data['applied_for'] == 2) {
                    //$class_level = $classLevelDao->getClassLevel($data['class_level_id']);
                    $class_levels = getSchoolData($class['school_id'], SCHOOL_CLASS_LEVELS);
                    $class_level = isset($class_levels[$data['class_level_id']]) ? $class_levels[$data['class_level_id']] : 0;
                    $smarty->assign('class_level', $class_level);
                }
                if ($data['applied_for'] == 3) {
//                    $classes = $classDao->getClass($data['class_id']);
                    $classes = getClassData($data['class_id'], CLASS_INFO);
                    $smarty->assign('classes', $classes);
                }
                break;

            default:
                _error(404);
                break;
        }
        break;
    case 'menus':
        // Tăng lượt tương tác - TaiLA
        addInteractive($class['school_id'], 'menu', 'school_view');
        switch ($_GET['sub_view']) {
            case '':
                page_header(__("Class Management") . " &rsaquo; " . __("Menu"));
                //Lấy ra danh sách lịch học áp dụng cho lớp
                $data = $menuDao->getMenuOfSchoolById($class['school_id'], $class['class_level_id'], $class['group_id']);
                $removeArr = array();
                for ($i = 0; $i < count($data); $i++) {
                    for ($j = 0; $j < count($data); $j++) {
                        if ($i != $j) {
                            $beginI = strtotime(toDBDate($data[$i]['begin']));
                            $beginJ = strtotime(toDBDate($data[$j]['begin']));
                            if ($beginI == $beginJ) {
                                if ($data[$i]['applied_for'] > $data[$j]['applied_for']) {
                                    $removeArr[] = $j;
                                }
                            }
                        }
                    }
                }
                $menus = array();
                for ($k = 0; $k < count($data); $k++) {
                    if (!in_array($k, $removeArr)) {
//                        $menus[] = $data[$k];
                        // Thêm trường này vào để biết nó bị thay thế hay không
                        $data[$k]['use'] = STATUS_ACTIVE;
                    } else {
                        $data[$k]['use'] = STATUS_INACTIVE;
                    }
                }
//                $smarty->assign('rows', $menus);
                $smarty->assign('rows', $data);

                //$class_levels = $classLevelDao->getClassLevels($class['school_id']);
                $class_levels = getSchoolData($class['school_id'], SCHOOL_CLASS_LEVELS);
                //$classes = $classDao->getClassesOfSchool($class['school_id']);
                $classes = getSchoolData($class['school_id'], SCHOOL_CLASSES);

                $smarty->assign('class_levels', $class_levels);
                $smarty->assign('classes', $classes);
                break;
            case 'detail':
                // Tăng lượt tương tác - TaiLA
                addInteractive($class['school_id'], 'menu', 'school_view', $_GET['id']);
                page_header(__("Class Management") . " &rsaquo; " . __("Detail schedule"));
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }
                $data = $menuDao->getMenuDetailById($_GET['id']);
                $smarty->assign('data', $data);

                if ($data['applied_for'] == 2) {
                    //$class_level = $classLevelDao->getClassLevel($data['class_level_id']);
                    $class_level = getClassLevelData($data['class_level_id'], CLASS_LEVEL_INFO);
                    $smarty->assign('class_level', $class_level);
                }
                if ($data['applied_for'] == 3) {
                    //$classes = $classDao->getClass($data['class_id']);
                    $classes = getClassData($data['class_id'], CLASS_INFO);
                    $smarty->assign('classes', $classes);
                }
                break;

            default:
                _error(404);
                break;
        }
        break;
    case 'tuitions':
        // Tăng lượt tương tác - TaiLA
        addInteractive($class['school_id'], 'tuition', 'school_view');
        if ($schoolConfig['allow_class_see_tuition']) {
            switch ($_GET['sub_view']) {
                case '':
                    page_header(__("Class Management") . " &rsaquo; " . __("Tuition"));
                    $tuitions = $tuitionDao->getClassTuitions($class['group_id']);

                    $smarty->assign('rows', $tuitions);

                    break;
                case 'detail':
                    // Tăng lượt tương tác - TaiLA
                    addInteractive($class['school_id'], 'attendance', 'school_view', $_GET['id']);
                    if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                        _error(404);
                    }
                    // Lấy những tuition của tháng hiện tại
                    $data = $tuitionDao->getTuition($_GET['id']);
                    if (is_null($data)) {
                        _error(404);
                    }

                    $smarty->assign('data', $data);

                    page_header(__("Class Management") . " &rsaquo; " . __("Tuition") . " &rsaquo; " . __("Detail"));
                    break;
                case 'history':
                    if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                        _error(404);
                    }
                    $data = $tuitionDao->getTuitionHistory($_GET['id']);
                    if (is_null($data)) {
                        _error(404);
                    }

                    $smarty->assign('data', $data);
                    page_header(__("Class Management") . " &rsaquo; " . __("Tuition") . " &rsaquo; " . __("History"));

                    break;
                default:
                    _error(404);
                    break;
            }
        } else {
            _error(404);
        }

        break;
    case 'reports':
        // Tăng lượt tương tác - TaiLA
        addInteractive($class['school_id'], 'report', 'school_view');
        switch ($_GET['sub_view']) {
            case '':
                page_header(__("Class Management") . " &rsaquo; " . __("Contact book"));
                $results = $reportDao->getClassReport($class['group_id']);
                $smarty->assign('results', $results);

                $noNotifyReport = $reportDao->getClassReportNoNotify($class['group_id']);
                $smarty->assign('noNotifyReport', $noNotifyReport);

                $children = $childDao->getChildrenOfClass($class['group_id']);
                $children = sortArray($children, "DESC", "child_status");
                //$children = getClassData($class['group_id'], CLASS_CHILDREN);
                $smarty->assign('children', $children);

                $countNotNotify = 0;
                foreach ($results as $row) {
                    if (!$row['is_notified']) {
                        $countNotNotify = $countNotNotify + 1;
                    }
                }
                $smarty->assign('countNotNotify', $countNotNotify);
                break;
            case 'detail':
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }
                $data = $reportDao->getReportById($_GET['id']);
                if (is_null($data) || $data['school_id'] != $class['school_id']) {
                    _error(404);
                }
                $smarty->assign('data', $data);
                //$child = $childDao->getChild($data['child_id']);
                $child = getChildData($data['child_id'], CHILD_INFO);
                $smarty->assign('child', $child);
                //$class = $classDao->getClass($child['class_id']);
                $class = getClassData($child['class_id'], CLASS_INFO);
                $class_name = $class['group_title'];
                $smarty->assign('class_name', $class_name);

                // Start get points list
                //Lấy ra class_id của lớp
                $class_id = $child['class_id'];
                $school_year = date("Y", strtotime($data['created_at'])).'-'.(date("Y", strtotime($data['created_at']))+1);

                // Lấy school_config
                $studentCode = $child['child_code'];
                $schoolConfig = getSchoolData($child['school_id'], SCHOOL_CONFIG);
                // Lấy chi tiết gov_class_level
                $class_level = getClassLevelData($class['class_level_id'], CLASS_LEVEL_INFO);
                // Lấy danh sách điểm của trẻ trong năm học
                $children_point = $pointDao->getChildrenPointsByStudentCode($class_id, $school_year, $studentCode);
                // Lấy child_name

                if (count($children_point) > 0) {
                    $children_point_last = array();
                    // Lưu các điểm đã được tính toán
                    $children_point_avgs = array();
                    // Lưu điểm của các môn thi lại
                    $children_subject_reexams = array();
                    // flag đánh dấu đã đủ điểm các môn hay chưa
                    $flag_enough_point = true;
                    // trạng thái fail/pass hay re-exam hay chưa đủ điểm ( Waiting.. )
                    $child_id = $childDao->getChildIdByCode($child['child_code']);
                    // Lấy số buổi nghỉ có phép và không phép của học sinh
                    $child_absent = $childDao->getAbsentsChild($child_id, $class_id);
                    // điểm trung bình năm
                    $child_avYear_point = 0;
                    $status_result = 'fail';
                    $subject_key = array();
                    $children_point_last = array();
                    // lưu số cột mỗi đầu điểm
                    $countColumnMaxPoint_hk1 = 2;
                    $countColumnMaxPoint_hk2 = 2;
                    $countColumnMaxPoint_gk1 = 1;
                    $countColumnMaxPoint_gk2 = 1;

                    if ($schoolConfig['score_fomula'] == "km") {
                        // điểm các tháng của 2 kỳ
                        $child_m11_point = $child_m12_point = $child_m13_point = $child_m21_point = $child_m22_point = $child_m23_point = 0;
                        // điểm trung bình các tháng của 2 kỳ
                        $child_avM1_point = $child_avM2_point = 0;
                        // điểm trung bình của cuối kỳ
                        $child_avd1_point = 0;
                        $child_avd2_point = 0;
                        $child_final_semester1 = $child_final_semester2 = 0;

                        // Mặc định cột điểm của khmer là 3
                        $countColumnMaxPoint_hk1 = 3;
                        $countColumnMaxPoint_hk2 = 3;
                        foreach ($children_point as $child) {
                            // Nếu bất kì điểm nào thiếu sẽ đánh dấu là chưa đủ điểm
                            if (!isset($child['hs11']) || !isset($child['hs12']) || !isset($child['hs13']) || !isset($child['gk11'])
                                || !isset($child['hs21']) || !isset($child['hs22']) || !isset($child['hs23']) || !isset($child['gk21'])) {
                                $flag_enough_point = false;
                            }
                            // tạm thời tính tổng các đầu điểm
                            // kỳ 1
                            $child_m11_point += (int)$child['hs11'];
                            $child_m12_point += (int)$child['hs12'];
                            $child_m13_point += (int)$child['hs13'];
                            $child_avd1_point += (int)$child['gk11'];
                            // kỳ 2
                            $child_m21_point += (int)$child['hs21'];
                            $child_m22_point += (int)$child['hs22'];
                            $child_m23_point += (int)$child['hs23'];
                            $child_avd2_point += (int)$child['gk21'];

                            // lấy tên môn học
                            $subject_detail = $subjectDao->getSubjectById($child['subject_id']);
                            $child['subject_name'] = $subject_detail['subject_name'];
                            if (isset($subject_detail['re_exam']) && $subject_detail['re_exam']) { // nếu môn đó là môn thi lại thì lưu vào array
                                $subject_reexam['name'] = $subject_detail['subject_name'];
                                $subject_reexam['point'] = $child['re_exam'];
                                $children_subject_reexams[] = $subject_reexam;
                            }

                            $children_point_last[] = $child;
                        }


                        // định nghĩa số bị chia của từng khối
                        if ($class_level['gov_class_level'] == '7' || $class_level['gov_class_level'] == '8') {
                            $dividend = 15;
                        } else if ($class_level['gov_class_level'] == '9') {
                            $dividend = 11.4;
                        } else if ($class_level['gov_class_level'] == '10') {
                            $dividend = 15.26;
                        } else if ($class_level['gov_class_level'] == '11') {
                            $dividend = 16.5;
                        } else if ($class_level['gov_class_level'] == '12') {
                            $dividend = 14.5;
                        }
                        // tính điểm theo công thức từng khối
                        // kỳ 1
                        $child_m11_point /= $dividend;
                        $child_m12_point /= $dividend;
                        $child_m13_point /= $dividend;
                        $child_avd1_point /= $dividend;
                        $child_avM1_point = ($child_m11_point + $child_m12_point + $child_m13_point) / 3;
                        $child_final_semester1 = ($child_avd1_point + $child_avM1_point) / 2;
                        // kỳ 2
                        $child_m21_point /= $dividend;
                        $child_m22_point /= $dividend;
                        $child_m23_point /= $dividend;
                        $child_avd2_point /= $dividend;
                        $child_avM2_point = ($child_m21_point + $child_m22_point + $child_m23_point) / 3;
                        $child_final_semester2 = ($child_avd2_point + $child_avM2_point) / 2;
                        // cả năm
                        $child_avYear_point = ($child_final_semester1 + $child_final_semester2) / 2;
                        // lưu lại vào array
                        $children_point_avgs['a1'] = $child_m11_point;
                        $children_point_avgs['b1'] = $child_m12_point;
                        $children_point_avgs['c1'] = $child_m13_point;
                        $children_point_avgs['d1'] = $child_avd1_point;
                        $children_point_avgs['x1'] = $child_avM1_point;
                        $children_point_avgs['e1'] = $child_final_semester1;
                        $children_point_avgs['a2'] = $child_m21_point;
                        $children_point_avgs['b2'] = $child_m22_point;
                        $children_point_avgs['c2'] = $child_m23_point;
                        $children_point_avgs['d2'] = $child_avd2_point;
                        $children_point_avgs['x2'] = $child_avM2_point;
                        $children_point_avgs['e2'] = $child_final_semester2;
                        $children_point_avgs['y'] = $child_avYear_point;

                        //xét điều kiện để đánh giá pass hay fail
                        if ($child_avYear_point < 25) { // dưới 25 điểm thì fail chặt
                            $status_result = 'Fail';
                        } else { // nếu trên 25 điểm thì xét các điều kiện khác
                            $status_result = 'Pass';
                            if ((isset($child_absent['absent_true']) && (int)$child_absent['absent_true'] >= 55)
                                || (isset($child_absent['absent_false']) && (int)$child_absent['absent_false'] >= 30)
                                || (((int)$child_absent['absent_true'] + (int)$child_absent['absent_false']) >= 55)) {
                                if ($class_level['gov_class_level'] == '7' || $class_level['gov_class_level'] == '8'
                                    || $class_level['gov_class_level'] == '10' || $class_level['gov_class_level'] == '11') {
                                    $status_result = 'Re-exam';
                                } else if ($class_level['gov_class_level'] == '9' || $class_level['gov_class_level'] == '12') {
                                    $status_result = 'Pass';
                                }
                            }
                        }
                        // xét đến đánh giá điểm thi lại
                        if ($status_result == 'Re-exam') {
                            $result_exam = 0;
                            foreach ($children_subject_reexams as $subject_reexam) {
                                if (!isset($subject_reexam['point'])) {
                                    $flag_enough_point = false;
                                } else {
                                    $result_exam += (int)$subject_reexam['point'];
                                }
                            }
                            if ($class_level['gov_class_level'] == '7' || $class_level['gov_class_level'] == '8'
                                || $class_level['gov_class_level'] == '11') {
                                $result_exam /= 4;
                            } else if ($class_level['gov_class_level'] == '10') {
                                $result_exam /= 4;
                            }
                            // nếu điểm thi lại trên 25 điểm thì pass còn lại fail
                            if ($flag_enough_point && $result_exam >= 25) {
                                $status_result = 'Pass';
                            } else if ($flag_enough_point && $result_exam < 25) {
                                $status_result = 'Fail';
                            } else { // chuaw du diem
                                $status_result = 'Re-exam';
                            }
                        }

                        if (count($children_point_last) == 0) {
                            $flag_enough_point = false;
                        }
                        if (!$flag_enough_point && $status_result != 'Re-exam') {
                            $status_result = 'Waiting..';
                        }
                        $subject_key[] = 'hs11';
                        $subject_key[] = 'hs12';
                        $subject_key[] = 'hs13';
                        $subject_key[] = 'gk11';
                        $subject_key[] = 'hs21';
                        $subject_key[] = 'hs22';
                        $subject_key[] = 'hs23';
                        $subject_key[] = 'gk21';
                        $smarty->assign("result_exam", $result_exam);

                    } else if ($schoolConfig['score_fomula'] == "vn") {
                        // điều kiện điểm của môn 1: kém, 2 yếu, 3 trung bình, 4 tiên tiến, 5 giỏi
                        $point_avg1_condition1 = 5;//hk1
                        $point_avg2_condition1 = 5;//hk2
                        $point_avgYearAll_condition1 = 5;// cả năm
                        // điều kiện điểm của môn toán hoặc văn  1: kém, 2 yếu, 3 trung bình, 4 tiên tiến, 5 giỏi
                        $point_avg1_condition2 = 5; //hk1
                        $point_avg2_condition2 = 5; //hk2
                        $point_avgYearAll_condition2 = 5; // cả năm
                        // trạng thái
                        $status_point1 = '';
                        $status_point2 = '';
                        $status_pointAll = '';
                        // flag đánh dấu học sinh đã thi lại hay chưa
                        $flag_reexam = false;
                        // điểm trung bình các môn 2 kỳ
                        $child_TBM1_point = $child_TBM2_point = 0;
                        // điểm trung bình năm
                        $child_avYearAll_point = 0;
                        // TÍnh điểm mỗi môn

                        // lưu số cột mỗi đầu điểm

                        // flag đánh dấu đã đủ điểm các môn hay chưa
                        $flag_enough_point = true;
                        // trạng thái fail/pass hay re-exam hay chưa đủ điểm ( Waiting.. )
                        $status_result = 'fail';

                        // Lấy danh sách điểm tất cả các môn của trẻ trong năm học để tính điêm trung bình năm
                        foreach ($children_point as $each) {
                            if (!isset($each['hs11']) || !isset($each['hs12']) || !isset($each['gk11'])
                                || !isset($each['hs21']) || !isset($each['hs22']) || !isset($each['gk21'])
                                || !isset($each['ck1']) || !isset($each['ck2'])) {
                                $flag_enough_point = false;
                            }
                            // điểm hệ số 2 kỳ
                            $child_hk1_point = $child_hk2_point = 0;
                            $child_TBM1_current_point = $child_TBM2_current_point = 0;
                            // đếm số điểm đã có
                            $count_hk1_point = 0;
                            $count_hk2_point = 0;
                            //Tính điểm trung bình môn

                            for ($i = 1; $i <= 6; $i++) {
                                // điểm hệ số 1
                                if (isset($each['hs1' . $i])) {
                                    $child_hk1_point += (int)$each['hs1' . $i];
                                    $count_hk1_point++;
                                    // lưu số cột đã có điểm của hệ số 1 kỳ 1
                                    if ($i > $countColumnMaxPoint_hk1 && $each['subject_id'] == $child['subject_id']) {
                                        $countColumnMaxPoint_hk1 = $i;
                                    }
                                }
                                if (isset($each['hs2' . $i])) {
                                    $child_hk2_point += (int)$each['hs2' . $i];
                                    $count_hk2_point++;
                                    // lưu số cột đã có điểm của hệ số 1 kỳ 2
                                    if ($i > $countColumnMaxPoint_hk2 && $each['subject_id'] == $child['subject_id']) {
                                        $countColumnMaxPoint_hk2 = $i;
                                    }
                                }
                                // điểm hệ số 2
                                if (isset($each['gk1' . $i])) {
                                    $child_hk1_point += (int)$each['gk1' . $i] * 2;
                                    $count_hk1_point += 2;
                                    // lưu số cột đã có điểm
                                    if ($i > $countColumnMaxPoint_gk1 && $each['subject_id'] == $child['subject_id']) {
                                        $countColumnMaxPoint_gk1 = $i;
                                    }

                                }
                                if (isset($each['gk2' . $i])) {
                                    $child_hk2_point += (int)$each['gk2' . $i] * 2;
                                    $count_hk2_point += 2;
                                    // lưu số cột đã có điểm
                                    if ($i > $countColumnMaxPoint_gk2 && $each['subject_id'] == $child['subject_id']) {
                                        $countColumnMaxPoint_gk2 = $i;
                                    }
                                }
                            }
                            //điểm cuối kì

                            if (isset($each['ck1'])) {
                                $child_hk1_point += (int)$each['ck1'] * 3;
                                $count_hk1_point += 3;
                            }
                            if (isset($each['ck2'])) {
                                // Nếu có điểm thi lại thì điểm tổng kết sẽ tính dựa theo điểm thi lại
                                if(isset($each['re_exam'])) {
                                    $child_hk2_point += (int)$each['re_exam'] * 3;
                                    $flag_reexam = true;
                                }else {
                                    $child_hk2_point += (int)$each['ck2'] * 3;
                                }
                                $count_hk2_point += 3;
                            }
                            if ($count_hk1_point > 0) {
                                $child_TBM1_current_point = $child_hk1_point / $count_hk1_point;
                                $each['tb_hk1'] = number_format($child_TBM1_current_point,2);
                            }
                            if ($count_hk2_point > 0) {
                                $child_TBM2_current_point = $child_hk2_point / $count_hk2_point;
                                $each['tb_hk2'] = number_format($child_TBM2_current_point,2);
                            }
                            // Điểm tổng kết
                            $child_avYear_point = ($child_TBM1_current_point + $child_TBM2_current_point * 2) / 3;

                            $each['tb_year'] = number_format($child_avYear_point,2);
                            // xét xem điều kiện các môn ở mức nào
                            //học kỳ 1
                            if ($child_TBM1_current_point < 2 && $point_avg1_condition1 > 1) {
                                $point_avg1_condition1 = 1;
                            } else if ($child_TBM1_current_point < 3.5 && $point_avg1_condition1 > 2) {
                                $point_avg1_condition1 = 2;
                            } else if ($child_TBM1_current_point < 5 && $point_avg1_condition1 > 3) {
                                $point_avg1_condition1 = 3;
                            } else if ($child_TBM1_current_point < 6.5 && $point_avg1_condition1 > 4) {
                                $point_avg1_condition1 = 4;
                            }
                            //học kỳ 2
                            if ($child_TBM2_current_point < 2 && $point_avg2_condition1 > 1) {
                                $point_avg2_condition1 = 1;
                            } else if ($child_TBM2_current_point < 3.5 && $point_avg2_condition1 > 2) {
                                $point_avg2_condition1 = 2;
                            } else if ($child_TBM2_current_point < 5 && $point_avg2_condition1 > 3) {
                                $point_avg2_condition1 = 3;
                            } else if ($child_TBM2_current_point < 6.5 && $point_avg2_condition1 > 4) {
                                $point_avg2_condition1 = 4;
                            }
                            // cả năm
                            if ($child_avYear_point < 2 && $point_avgYearAll_condition1 > 1) {
                                $point_avgYearAll_condition1 = 1;
                            } else if ($child_avYear_point < 3.5 && $point_avgYearAll_condition1 > 2) {
                                $point_avgYearAll_condition1 = 2;
                            } else if ($child_avYear_point < 5 && $point_avgYearAll_condition1 > 3) {
                                $point_avgYearAll_condition1 = 3;
                            } else if ($child_avYear_point < 6.5 && $point_avgYearAll_condition1 > 4) {
                                $point_avgYearAll_condition1 = 4;
                            }
                            // xét xem điều kiện của môn toán văn
                            // lấy tên môn học
                            $subject_detail = $subjectDao->getSubjectById($each['subject_id']);
                            $each['subject_name'] = $subject_detail['subject_name'];
                            if (convert_vi_to_en(strtolower(html_entity_decode($subject_detail['subject_name']))) == "toan" || convert_vi_to_en(strtolower(html_entity_decode($subject_detail['subject_name']))) == "van") {
                                //học kỳ 1
                                if ($child_TBM1_current_point < 2 && $point_avg1_condition2 > 1) {
                                    $point_avg1_condition2 = 1;
                                } else if ($child_TBM1_current_point < 3.5 && $point_avg1_condition2 > 2) {
                                    $point_avg1_condition2 = 2;
                                } else if ($child_TBM1_current_point < 5 && $point_avg1_condition2 > 3) {
                                    $point_avg1_condition2 = 3;
                                } else if ($child_TBM1_current_point < 6.5 && $point_avg1_condition2 > 4) {
                                    $point_avg1_condition2 = 4;
                                }
                                // học kỳ 2
                                if ($child_TBM2_current_point < 2 && $point_avg2_condition2 > 1) {
                                    $point_avg2_condition2 = 1;
                                } else if ($child_TBM2_current_point < 3.5 && $point_avg2_condition2 > 2) {
                                    $point_avg2_condition2 = 2;
                                } else if ($child_TBM2_current_point < 5 && $point_avg2_condition2 > 3) {
                                    $point_avg2_condition2 = 3;
                                } else if ($child_TBM2_current_point < 6.5 && $point_avg2_condition2 > 4) {
                                    $point_avg2_condition2 = 4;
                                }
                                // cả năm
                                if ($child_avYear_point < 2 && $point_avgYearAll_condition2 > 1) {
                                    $point_avgYearAll_condition2 = 1;
                                } else if ($child_avYear_point < 3.5 && $point_avgYearAll_condition2 > 2) {
                                    $point_avgYearAll_condition2 = 2;
                                } else if ($child_avYear_point < 5 && $point_avgYearAll_condition2 > 3) {
                                    $point_avgYearAll_condition2 = 3;
                                } else if ($child_avYear_point < 6.5 && $point_avgYearAll_condition2 > 4) {
                                    $point_avgYearAll_condition2 = 4;
                                }
                            }
                            // Cộng vào điểm trung bình các môn
                            $child_TBM1_point += $child_TBM1_current_point;
                            $child_TBM2_point += $child_TBM2_current_point;
                            $child_avYearAll_point += $child_avYear_point;
                            $children_point_last[] = $each;
                        }
                        // điểm trung bình các môn
                        $child_TBM1_point /= count($children_point);
                        $child_TBM2_point /= count($children_point);
                        $child_avYearAll_point /= count($children_point);

                        //xét điều kiện để đánh giá pass hay fail
                        //học kỳ 1
                        if ($child_TBM1_point >= 8 && $point_avg1_condition1 == 5 && $point_avg1_condition2 == 5) { // trên 8 điểm thì đánh giá loại giỏi
                            $status_point1 = 5;
                        } else if ($child_TBM1_point >= 8 && ($point_avg1_condition1 == 3 || $point_avg1_condition2 == 3)) {
                            $status_point1 = 4;
                        } else if ($child_TBM1_point >= 8 && ($point_avg1_condition1 == 2 || $point_avg1_condition2 == 2)) {
                            $status_point1 = 3;
                        } else if ($child_TBM1_point >= 6.5 && $point_avg1_condition1 >= 4 && $point_avg1_condition2 >= 4) {  // trên 6.5 điểm thì đánh giá loại tiên tiến
                            $status_point1 = 4;
                        } else if ($child_TBM1_point >= 6.5 && ($point_avg1_condition1 == 2 || $point_avg1_condition2 == 2)) {  // trên 6.5 điểm thì đánh giá loại tiên tiến
                            $status_point1 = 3;
                        } else if ($child_TBM1_point >= 6.5 && ($point_avg1_condition1 == 1 || $point_avg1_condition2 == 1)) {  // trên 6.5 điểm thì đánh giá loại tiên tiến
                            $status_point1 = 2;
                        } else if ($child_TBM1_point >= 5 && $point_avg1_condition1 >= 3 && $point_avg1_condition2 >= 3) {  // trên 5 điểm thì đánh giá loại trung bình
                            $status_point1 = 3;
                        } else if ($child_TBM1_point >= 3.5 && $point_avg1_condition1 >= 2 && $point_avg1_condition2 >= 2) {  // trên 3.5 điểm thì đánh giá loại yếu
                            $status_point1 = 2;
                        } else if ($child_TBM1_point < 3.5 && $point_avg1_condition1 >= 1 && $point_avg1_condition2 >= 1) {
                            $status_point1 = 1;
                        }
                        // học kỳ 2
                        if ($child_TBM2_point >= 8 && $point_avg2_condition1 == 5 && $point_avg2_condition2 == 5) { // trên 8 điểm thì đánh giá loại giỏi
                            $status_point2 = 5;
                        } else if ($child_TBM2_point >= 8 && ($point_avg2_condition1 == 3 || $point_avg2_condition2 == 3)) {
                            $status_point2 = 4;
                        } else if ($child_TBM2_point >= 8 && ($point_avg2_condition1 == 2 || $point_avg2_condition2 == 2)) {
                            $status_point2 = 3;
                        } else if ($child_TBM2_point >= 6.5 && $point_avg2_condition1 >= 4 && $point_avg2_condition2 >= 4) {  // trên 6.5 điểm thì đánh giá loại tiên tiến
                            $status_point2 = 4;
                        } else if ($child_TBM2_point >= 6.5 && ($point_avg2_condition1 == 2 || $point_avg2_condition2 == 2)) {  // trên 6.5 điểm thì đánh giá loại tiên tiến
                            $status_point2 = 3;
                        } else if ($child_TBM2_point >= 6.5 && ($point_avg2_condition1 == 1 || $point_avg2_condition2 == 1)) {  // trên 6.5 điểm thì đánh giá loại tiên tiến
                            $status_point2 = 2;
                        } else if ($child_TBM2_point >= 5 && $point_avg2_condition1 >= 3 && $point_avg2_condition2 >= 3) {  // trên 5 điểm thì đánh giá loại trung bình
                            $status_point2 = 3;
                        } else if ($child_TBM2_point >= 3.5 && $point_avg2_condition1 >= 2 && $point_avg2_condition2 >= 2) {  // trên 3.5 điểm thì đánh giá loại yếu
                            $status_point2 = 2;
                        } else if ($child_TBM2_point < 3.5 && $point_avg2_condition1 >= 1 && $point_avg2_condition2 >= 1) {
                            $status_point2 = 1;
                        }
                        // cả năm
                        if ($child_avYearAll_point >= 8 && $point_avgYearAll_condition1 == 5 && $point_avgYearAll_condition2 == 5) { // trên 8 điểm thì đánh giá loại giỏi
                            $status_pointAll = 5;
                        } else if ($child_avYearAll_point >= 8 && ($point_avgYearAll_condition1 == 3 || $point_avgYearAll_condition2 == 3)) {
                            $status_pointAll = 4;
                        } else if ($child_avYearAll_point >= 8 && ($point_avgYearAll_condition1 == 2 || $point_avgYearAll_condition2 == 2)) {
                            $status_pointAll = 3;
                        } else if ($child_avYearAll_point >= 6.5 && $point_avgYearAll_condition1 >= 4 && $point_avgYearAll_condition2 >= 4) {  // trên 6.5 điểm thì đánh giá loại tiên tiến
                            $status_pointAll = 4;
                        } else if ($child_avYearAll_point >= 6.5 && ($point_avgYearAll_condition1 == 2 || $point_avgYearAll_condition2 == 2)) {  // trên 6.5 điểm thì đánh giá loại tiên tiến
                            $status_pointAll = 3;
                        } else if ($child_avYearAll_point >= 6.5 && ($point_avgYearAll_condition1 == 1 || $point_avgYearAll_condition2 == 1)) {  // trên 6.5 điểm thì đánh giá loại tiên tiến
                            $status_pointAll = 2;
                        } else if ($child_avYearAll_point >= 5 && $point_avgYearAll_condition1 >= 3 && $point_avgYearAll_condition2 >= 3) {  // trên 5 điểm thì đánh giá loại trung bình
                            $status_pointAll = 3;
                        } else if ($child_avYearAll_point >= 3.5 && $point_avgYearAll_condition1 >= 2 && $point_avgYearAll_condition2 >= 2) {  // trên 3.5 điểm thì đánh giá loại yếu
                            $status_pointAll = 2;
                        } else if ($child_avYearAll_point < 3.5 && $point_avgYearAll_condition1 >= 1 && $point_avgYearAll_condition2 >= 1) {
                            $status_pointAll = 1;
                        }

                        // Lấy hạnh kiểm của học sinh
                        $conduct = $conductDao->getConductOfChildren($class_id, $child_id, $school_year);
                        if (!isset($conduct) || !isset($conduct['ck'])) {
                            $flag_enough_point = false;
                        } else if ($child_absent['absent_true'] + $child_absent['absent_false'] <= 45
                            && $status_pointAll >= 3 && $conduct['ck'] != "Weak") {
                            $status_result = "pass";
                        }

                        if (!$flag_enough_point) {
                            $status_result = "waiting..";
                        }
//                            $children_point_last[] = $child;

                        // setup subject_key
                        for ($i = 1; $i <= $countColumnMaxPoint_hk1; $i++) {
                            $subject_key[] = 'hs1' . $i;
                        }
                        for ($i = 1; $i <= $countColumnMaxPoint_gk1; $i++) {
                            $subject_key[] = 'gk1' . $i;
                        }
                        $subject_key[] = 'ck1';
                        $subject_key[] = 'tb_hk1';
                        for ($i = 1; $i <= $countColumnMaxPoint_hk2; $i++) {
                            $subject_key[] = 'hs2' . $i;
                        }
                        for ($i = 1; $i <= $countColumnMaxPoint_gk2; $i++) {
                            $subject_key[] = 'gk2' . $i;
                        }
                        $subject_key[] = 'ck2';
                        $subject_key[] = 'tb_hk2';
                        $subject_key[] = 're_exam';
                        $smarty->assign("tb_total_hk1", $child_TBM1_point);
                        $smarty->assign("tb_total_hk2", $child_TBM2_point);
                        $smarty->assign("tb_total_year", $child_avYearAll_point);
                    }
                    $smarty->assign("column_hk1", $countColumnMaxPoint_hk1);
                    $smarty->assign("column_hk2", $countColumnMaxPoint_hk2);
                    $smarty->assign("column_gk1", $countColumnMaxPoint_gk1);
                    $smarty->assign("column_gk2", $countColumnMaxPoint_gk2);
                    $smarty->assign("rows", $children_point_last);
                    $smarty->assign("subject_key", $subject_key);
                    $smarty->assign("status", $status_result);
                    $smarty->assign("child_absent", $child_absent);
                    $smarty->assign("children_point_avgs", $children_point_avgs);
                    $smarty->assign("children_subject_reexams", $children_subject_reexams);
                }

                $smarty->assign("point_module", $schoolConfig['points']);
                $smarty->assign("grade", $schoolConfig['grade']);
                $smarty->assign('score_fomula', $schoolConfig['score_fomula']);


                page_header(__("School Management") . " &rsaquo; " . __("Contact book") . " &rsaquo; " . $data['report_name']);
                break;
            case 'edit':
                page_header(__("Class Management") . " &rsaquo; " . __("Contact book") . " &rsaquo; " . __('Edit'));
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }
                $data = $reportDao->getReportById($_GET['id']);
                if (is_null($data) || $data['school_id'] != $class['school_id']) {
                    _error(404);
                }
                $smarty->assign('data', $data);
                $templates = $reportDao->getClassReportTemplate($class['group_id'], $class['class_level_id'], $class['school_id']);
                $smarty->assign('templates', $templates);
                //$child = $childDao->getChild($data['child_id']);
                $child = getChildData($data['child_id'], CHILD_INFO);
                $smarty->assign('child', $child);
                break;
            case 'add':
                page_header(__("Class Management") . " &rsaquo; " . __("Contact book") . " &rsaquo; " . __('Add New'));
                $children = $childDao->getChildrenOfClassForReport($class['group_id']);
                $templates = $reportDao->getClassReportTemplate($class['group_id'], $class['class_level_id'], $class['school_id']);
                $smarty->assign('children', $children);
                $smarty->assign('templates', $templates);
                break;
            case 'listtemp':
                page_header(__("Class Management") . " &rsaquo; " . __("Contact book") . " &rsaquo; " . __('List template'));
                $results = $reportDao->getClassReportTemplate($class['group_id'], $class['class_level_id'], $class['school_id']);
                $smarty->assign('results', $results);

                $classes = getSchoolData($class['school_id'], SCHOOL_CLASSES);
                $smarty->assign('classes', $classes);

                $classLevels = getSchoolData($class['school_id'], SCHOOL_CLASS_LEVELS);
                $smarty->assign('class_levels', $classLevels);
                break;
            case 'edittemp':
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }
                $temp = $reportDao->getReportTemplate($_GET['id']);
                if (is_null($temp) || $temp['school_id'] != $class['school_id']) {
                    _error(404);
                }
                $tempCategoryIds = array();
                foreach ($temp['details'] as $detail) {
                    $tempCategoryIds[] = $detail['report_template_category_id'];
                }
                // Lấy danh sách các hạng mục của trường
                $categorys = $reportDao->getAllCategoryOfSchool($class['school_id']);
                $categorysTemp = array();
                foreach ($categorys as $category) {
                    if (in_array($category['report_template_category_id'], $tempCategoryIds)) {
                        $category['checked'] = 1;
                        foreach ($temp['details'] as $detail) {
                            if ($detail['report_template_category_id'] == $category['report_template_category_id']) {
                                $category['template_content'] = $detail['template_content'];
                            }
                            $tempCategoryIds[] = $detail['report_template_category_id'];
                        }
                    } else {
                        $category['checked'] = 0;
                    }
                    $categorysTemp[] = $category;
                }
                $smarty->assign('data', $temp);
                $smarty->assign('categorysTemp', $categorysTemp);
                break;
            case 'addtemp':
                page_header(__("Class Management") . " &rsaquo; " . __("Contact book") . " &rsaquo; " . __('Add new template'));

                // Lấy danh sách các hạng mục của trường
                $categorys = $reportDao->getAllCategoryOfSchool($class['school_id']);
                $smarty->assign('categorys', $categorys);
                break;
            default:
                _error(404);
                break;
        }
        break;
    case 'points':
        addInteractive($class['school_id'], 'report', 'school_view');
        switch ($_GET['sub_view']) {
            case 'listsbysubject':
            case 'importManual':
                page_header(__("Class Management") . " &rsaquo; " . __("Point"));
                global $default_school_year;
                $subjects = $subjectDao->getSubjectByTeacher($user->_data['user_id'],$class['group_id']);

                $smarty->assign('default_subjects', $subjects);
                $smarty->assign('username', $class['group_name']);
                $smarty->assign('semesters', $semesters);
            $smarty->assign('score_fomula', $schoolConfig['score_fomula']);
                $smarty->assign('default_year', $default_school_year);
                break;
            case 'listsbystudent':
                page_header(__("class Management") . " &rsaquo; " . __("Point"));
                $students = getClassData($class['group_id'], CLASS_CHILDREN);
                $smarty->assign('default_childrens', $students);
                $smarty->assign('username', $class['group_name']);
                $smarty->assign('default_year', $default_school_year);
                break;
            case 'comment':
                page_header(__("class Management") . " &rsaquo; " . __("Point"));
                $students = getClassData($class['group_id'], CLASS_CHILDREN);
                $smarty->assign('default_childrens', $students);
                $smarty->assign('semesters', $semesters);
                $smarty->assign('username', $class['group_name']);
                $smarty->assign('default_year', $default_school_year);
                break;
            case 'detail':
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }
                $data = $reportDao->getReportById($_GET['id']);
                if (is_null($data) || $data['school_id'] != $class['school_id']) {
                    _error(404);
                }
                $smarty->assign('data', $data);
                //$child = $childDao->getChild($data['child_id']);
                $child = getChildData($data['child_id'], CHILD_INFO);
                $smarty->assign('child', $child);
                //$class = $classDao->getClass($child['class_id']);
                $class = getClassData($child['class_id'], CLASS_INFO);
                $class_name = $class['group_title'];
                $smarty->assign('class_name', $class_name);
                page_header(__("School Management") . " &rsaquo; " . __("Contact book") . " &rsaquo; " . $data['report_name']);
                break;

            case 'edit':
                page_header(__("Class Management") . " &rsaquo; " . __("Contact book") . " &rsaquo; " . __('Edit'));
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }
                $data = $reportDao->getReportById($_GET['id']);
                if (is_null($data) || $data['school_id'] != $class['school_id']) {
                    _error(404);
                }
                $smarty->assign('data', $data);
                $templates = $reportDao->getClassReportTemplate($class['group_id'], $class['class_level_id'], $class['school_id']);
                $smarty->assign('templates', $templates);
                //$child = $childDao->getChild($data['child_id']);
                $child = getChildData($data['child_id'], CHILD_INFO);
                $smarty->assign('child', $child);
                break;
            case 'add':
                page_header(__("Class Management") . " &rsaquo; " . __("Contact book") . " &rsaquo; " . __('Add New'));
                $children = $childDao->getChildrenOfClassForReport($class['group_id']);
                $templates = $reportDao->getClassReportTemplate($class['group_id'], $class['class_level_id'], $class['school_id']);
                $smarty->assign('children', $children);
                $smarty->assign('templates', $templates);
                break;
            default:
                _error(404);
                break;
        }
        break;
    // ADD START MANHDD 02/06/2021
    case 'conducts':
        addInteractive($class['school_id'], 'conduct', 'school_view');
        switch ($_GET['sub_view']) {
            case '':
                page_header(__("Class Management") . " &rsaquo; " . __("Conducts"));
                // page header

                //$children = $childDao->getChildrenOfClass($class['group_id']);
                $children = array();
                $childs = getClassData($class['group_id'], CLASS_CHILDREN);
                foreach ($childs as $child) {
                    //Lấy danh sách hạnh kiểm của học sinh
                    $conduct = $conductDao->getConductOfChildren($class['group_id'],$child['child_id'],$class['school_year']);
                    if(count($conduct)>0) {
                        $child['conduct_id'] = $conduct['conduct_id'];
                        $child['conduct1'] = $conduct['hk1'];
                        $child['conduct2'] = $conduct['hk2'];
                        $child['conduct3'] = $conduct['ck'];
                        $children[] = $child;
                    }
                }
                $smarty->assign('rows', $children);

                break;
            case 'edit':
                global $default_conducts;
                page_header(__("Class Management") . " &rsaquo; " . __("Conducts") . " &rsaquo; " . __('Edit'));
                if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }
                $data = $conductDao->getConductById($_GET['id']);
                $child = getChildData($data['child_id'], CHILD_INFO);
                if (is_null($data) || $child['school_id'] != $class['school_id']) {
                    _error(404);
                }
//                $conducts = $default_conducts;

                $smarty->assign('data', $data);
                $smarty->assign('child', $child);
                $smarty->assign('conducts', $default_conducts);
                break;
            default:
                _error(404);
                break;
        }
        break;
    // ADD END MANHDD 02/06/2021
    // ADD START MANHDD 17/06/2021
    case 'courses':
        addInteractive($class['school_id'], 'course', 'school_view');
        switch ($_GET['sub_view']) {
            case '':
                global $default_school_year;
                page_header(__("Class Management") . " &rsaquo; " . __("Courses"));

                $url = 'https://elearning.mascom.com.vn/api/course/get/list';
                $data = array( 'secret' => '7402b1e6-933d-42eb-aff0-acceb5ccd13e',
                    'userId' => '',
                    'schoolId' => $class['school_id'],
                    'classLevelId' => $class['class_level_id'],
                    'groupId' => $class['group_id'],
                    'limit' => '100',
                    'offset' => '0');
                $data = http_build_query($data);
//                $data_string = json_encode($data);
                $curl = curl_init();
                curl_setopt($curl, CURLOPT_URL, $url);
                curl_setopt($curl, CURLOPT_POST, 1);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 5.1; rv:5.0) Gecko/20100101 Firefox/5.0 Firefox/5.0');
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($curl, CURLOPT_TIMEOUT, 30);
                curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
                curl_setopt($curl, CURLOPT_MAXREDIRS, 10);
                curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
                $contents = curl_exec($curl);
                $status = curl_getinfo($curl);
                curl_close($curl);
                if ($status['http_code'] == 200) {
                    $contents = json_decode($contents, true);
                    if ($contents['error']) {
                        throw new Exception($contents['error']['message'] . ' Error Code #' . $contents['error']['code']);
                    }
                } else {
                    throw new Exception("Error Processing Request");
                }

                $smarty->assign('username', $_POST['school_username']);
                $smarty->assign('result', $contents['course']);
//                $smarty->assign('canEdit', canEdit($_POST['school_username'], 'courses'));
                $return['results'] = $smarty->fetch("ci/class/ajax.courselist.tpl");
                break;
            default:
                _error(404);
                break;
        }
        break;
    // ADD END MANHDD 17/06/2021
    case 'pickup':
        // Tăng lượt tương tác - TaiLA
        addInteractive($class['school_id'], 'pickup', 'school_view');
        if ($is_configured && $is_teacher) {
            switch ($_GET['sub_view']) {

                case 'assign':
                    page_header(__("School Management") . " &rsaquo; " . __("Late pickup") . " &rsaquo; " . __("Assign"));

                    $monday = date('d/m/Y', strtotime('monday this week'));
                    $monDbDate = toDBDate($monday);
                    //$today = date("l");

                    $day['begin'] = date('d/m/Y');
                    $day['mon'] = $monday;
                    $day['tue'] = date('d/m/Y', strtotime("+1 day", strtotime($monDbDate)));
                    $day['wed'] = date('d/m/Y', strtotime("+2 day", strtotime($monDbDate)));
                    $day['thu'] = date('d/m/Y', strtotime("+3 day", strtotime($monDbDate)));
                    $day['fri'] = date('d/m/Y', strtotime("+4 day", strtotime($monDbDate)));
                    $day['sat'] = date('d/m/Y', strtotime("+5 day", strtotime($monDbDate)));
                    $day['sun'] = date('d/m/Y', strtotime("+6 day", strtotime($monDbDate)));

                    $classes = $pickupDao->getPickupClasses($schoolInfo['page_id']);
                    $teachers = $pickupDao->getPickupTeachersGroup($schoolInfo['page_id']);
                    $assignInfo = $pickupDao->getAssignInWeek($schoolInfo['page_id'], toDBDate($monday));

                    // assign variables
                    $smarty->assign('classes', $classes);
                    $smarty->assign('teachers', $teachers);
                    $smarty->assign('data', $assignInfo['teacher_list']);
                    $smarty->assign('list', $assignInfo['teacher_id']);
                    $smarty->assign('day', $day);
                    break;

                case 'manage':

                    // Kiểm tra user có quyền trong lớp đón muộn không
                    if (!$is_assigned) {
                        _error(404);
                    }
                    page_header(__("School Management") . " &rsaquo; " . __("Late pickup") . " &rsaquo; " . __("Manage"));

                    $children = array();
                    $child_count = 0;
                    $is_today = true;
                    $pickup_time = date('Y-m-d');
                    $pickup = $pickupDao->getPickupAssign($schoolInfo['page_id'], $pickup_time, true);
                    if (is_null($pickup)) {
                        _error(404);
                    }
                    $price_list = $pickupDao->getPriceList($schoolInfo['page_id']);
                    $smarty->assign('time_option', $price_list[0]['ending_time']);
                    $children = $pickupDao->getChildOfPickup($pickup['pickup_id'], $child_count);

                    $smarty->assign('is_today', $is_today);
                    $smarty->assign('child_count', $child_count);
                    $smarty->assign('children', $children);
                    $smarty->assign('today', toSysDate($pickup_time));
                    $smarty->assign('pickup', $pickup);
                    break;

                case 'history':
                    // Kiểm tra user có quyền trong lớp đón muộn không
                    page_header(__("School Management") . " &rsaquo; " . __("Late pickup") . " &rsaquo; " . __("History"));

                    $toDate = date('t/m/Y');
                    $fromDate = date('01/m/Y');
                    $pickups = $pickupDao->getPickupTeacherInMonth($schoolInfo['page_id'], $user->_data['user_id'], date('Y-m-01'), date('Y-m-t'));

                    $smarty->assign('toDate', $toDate);
                    $smarty->assign('fromDate', $fromDate);
                    $smarty->assign('pickups', $pickups);
                    break;

                case 'detail':
                    page_header(__("School Management") . " &rsaquo; " . __("Late pickup") . " &rsaquo; " . __("Detail"));
                    $child_count = 0;
                    $is_today = false;
                    $pickupId = $_GET['id'];
                    $pickup = $pickupDao->getPickup($pickupId, true);
                    if (is_null($pickup)) {
                        _error(404);
                    }
                    if ($pickup['pickup_time'] == date('Y-m-d')) {
                        $is_today = true;
                    }
                    $allow_teacher_edit_pickup_before = $schoolConfig['allow_teacher_edit_pickup_before'];
                    $price_list = $pickupDao->getPriceList($schoolInfo['page_id']);
                    $children = $pickupDao->getChildOfPickup($pickupId, $child_count);

                    $smarty->assign('is_today', $is_today);
                    $smarty->assign('child_count', $child_count);
                    $smarty->assign('children', $children);
                    $smarty->assign('today', toSysDate($pickup['pickup_time']));
                    //$smarty->assign('pickup_date', strtotime($pickup['pickup_time']));
                    $smarty->assign('pickup', $pickup);
                    $smarty->assign('time_option', $price_list[0]['ending_time']);
                    $smarty->assign('allow_teacher_edit_pickup_before', $allow_teacher_edit_pickup_before);
                    break;

                case 'add':
                    page_header(__("School Management") . " &rsaquo; " . __("Late pickup") . " &rsaquo; " . __("Add"));

                    // valid inputs
                    if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                        _error(404);
                    }

                    // Lấy ra danh sách các lớp của trường
                    //$classes = $classDao->getClassesOfSchool($class['school_id']);
                    $classes = getSchoolData($schoolInfo['page_id'], SCHOOL_CLASSES);
                    $smarty->assign('classes', $classes);

                    $child_count = 0;
                    $pickupClass = array();
                    $childrenRegistered = array();

                    // Lấy ra thông tin 1 lần đón muộn
                    $pickup = $pickupDao->getPickup($_GET['id']);
                    $date = $pickup['pickup_time'];
                    $pickupId = $pickup['pickup_id'];
                    if ($pickup['pickup_class_id'] > 0) {
                        $pickupClass[] = $pickupDao->getPickupClass($pickup['pickup_class_id']);
                    }
                    $first_pickup_class = isset($pickupClass[0]['pickup_class_id']) ? $pickupClass[0]['pickup_class_id'] : 0;
                    $children = $pickupDao->getChildOfClass(array_values($classes)[0]['group_id'], $pickupId, $first_pickup_class, $child_count, $date);

                    $pickupIds = $pickupDao->getPickupIds($schoolInfo['page_id'], $date);
                    if (!is_null($pickupIds) && $first_pickup_class > 0) {
                        $childrenRegistered = $pickupDao->getChildRegistered(array_values($classes)[0]['group_id'], $pickupIds, $first_pickup_class);
                    }

                    $smarty->assign('child_count', $child_count);
                    $smarty->assign('today', toSysDate($pickup['pickup_time']));
                    $smarty->assign('pickup_class', $pickupClass);
                    $smarty->assign('classId', array_values($classes)[0]['group_id']);
                    $smarty->assign('children', $children);
                    $smarty->assign('childrenRegistered', $childrenRegistered);
                    $smarty->assign('pickup_id', $pickup['pickup_id']);

                    break;

                default:
                    _error(404);
                    break;
            }
        } else _error(404);

        break;

    case 'feedback':
        switch ($_GET['sub_view']) {
            case '':
                page_header(__("Class Management") . " &rsaquo; " . __("Feedback"));
                //Lấy ra danh sách feedback
                $feedbacks = $feedbackDao->getFeedbackOfClass($class['group_id']);
                $smarty->assign('rows', $feedbacks);
                break;

            default:
                _error(404);
                break;
        }
        break;
    case 'diarys':
        // Tăng lượt tương tác - TaiLA
        addInteractive($class['school_id'], 'diary', 'school_view');
        switch ($_GET['sub_view']) {
            case '':
                // page header
                page_header(__("Class Management") . " &rsaquo; " . __("Diary corner"));

                //Lấy điều kiện search trước đó ra khỏi session
                $condition = $_SESSION[SESSION_KEY_SEARCH_DIARY_CLASS];

                if ($class['group_id'] == $condition['class_id']) {
                    $childId = isset($condition) ? $condition['child_id'] : '';
                    $year = isset($condition) ? $condition['year'] : '';
                } else {
                    $childId = '';
                    $year = '';
                }

                // Lấy danh sách lớp
                $children = getClassData($class['group_id'], CLASS_CHILDREN);

                $results = array();
                if ($childId != '') {
                    $child_parent_id = $childDao->getChildParentIdFromChildId($childId);
                    $results = $journalDao->getAllJournalChildForSchoolOfYear($child_parent_id, $year);
                }
                $child = getChildData($childId, CHILD_INFO);
                $is_module = 1;
                $smarty->assign('results', $results);
                $smarty->assign('children', $children);
                $smarty->assign('year', $year);
                $smarty->assign('childId', $childId);
                $smarty->assign('child', $child);
                $smarty->assign('is_module', $is_module);
                break;

            case 'add':
                page_header(__("Class Management") . " &rsaquo; " . __("Diary corner") . " &rsaquo; " . __("Add New"));
                //Memcache - Lấy ra trẻ của lớp
                $children = getClassData($class['group_id'], CLASS_CHILDREN);
                $smarty->assign('children', $children);
                break;

            default:
                _error(404);
                break;
        }
        break;
    case 'healths':
        // Tăng lượt tương tác - TaiLA
        addInteractive($class['school_id'], 'health', 'school_view');
        switch ($_GET['sub_view']) {
            case '':
                // page header
                page_header(__("School Management") . " &rsaquo; " . __("Health information"));
                //Lấy điều kiện search trước đó ra khỏi session
                $condition = $_SESSION[SESSION_KEY_SEARCH_CHILDREN_HEALTH_CLASS];
                if ($class['group_id'] == $condition['class_id']) {
                    $childId = isset($condition) ? $condition['child_id'] : '';
                    $begin = isset($condition) ? $condition['begin'] : '';
                    $end = isset($condition) ? $condition['end'] : '';
                } else {
                    $childId = '';
                    $begin = '';
                    $end = '';
                }

                // Hai biến dưới để assign ra màn hình
                $beginScreen = $begin;
                $endScreen = $end;

                $children = array();
                if ($childId != '') {
                    $child = getChildData($childId, CHILD_INFO);
                    if (is_null($child)) {
                        _error(404);
                    }
                    // Lấy ngày hiện tại
                    $dateNow = date('Y-m-d');
                    $dateNowSys = toSysDate($dateNow);

                    // Lấy ngày sinh nhật của trẻ
                    $birthday = toDBDate($child['birthday']);

                    // Khởi tạo mảng lưu chỉ số sức khỏe của trẻ
                    $childGrowth = array();

                    if (is_null($begin) || is_null($end)) {
                        // Lấy toàn bộ thông tin sức khỏe của trẻ
                        $childGrowth = $childDao->getChildGrowthForChart($child['child_parent_id']);

                        // Tính xem trẻ được bao nhiêu ngày tuổi
                        $day_age_now = (strtotime($dateNow) - strtotime($birthday)) / (60 * 60 * 24);

                        // đoạn này xử lý để lấy ngày bắt đầu (đặt tạm a, b, c; bí tên quá)
                        $a = $day_age_now % 30;
                        $b = $day_age_now - $a;
                        $c = $b - 30 * 10;
                        $dayGet = (int)$c;
                        $day_age_max = $day_age_now + 65;
                        if ($day_age_now >= 300) {
                            $day_age_min = $dayGet;
                        } else {
                            $day_age_min = 0;
                        }

                        // Lấy dữ liệu người dùng nhập vào vẽ biểu đồ
                        $heightForChartChartJS = getDataJsonForChartChartJS($childGrowth, $day_age_min, $day_age_max, $birthday, 'height');
                        $weightForChartChartJS = getDataJsonForChartChartJS($childGrowth, $day_age_min, $day_age_max, $birthday, 'weight');
                        if ($child['gender'] == 'male') {
                            // Dữ liệu chuẩn
                            $hightStandardArray = getHightDataForChart($hightStandardJsonMale);
                            $weightStandardArray = getHightDataForChart($weightStandardJsonMale);
                        } else {
                            $hightStandardArray = getHightDataForChart($hightStandardJsonFeMale);
                            $weightStandardArray = getHightDataForChart($weightStandardJsonFeMale);

                        }
                        // 1. Chiều cao
                        // Lấy 2 ngày gần nhất của $begin_day có dữ liệu, nội suy chuẩn của ngày bắt đầu
                        $hightStandardArray[$day_age_min]['highest'] = getStandardDataBeginAndEnd($hightStandardArray, $day_age_min, 'highest');

                        $hightStandardArray[$day_age_min]['lowest'] = getStandardDataBeginAndEnd($hightStandardArray, $day_age_min, 'lowest');

                        // Lấy 2 ngày gần nhất của $end_day có dữ liệu, nội suy chuẩn của ngày kết thúc
                        $hightStandardArray[$day_age_max]['highest'] = getStandardDataBeginAndEnd($hightStandardArray, $day_age_max, 'highest');

                        $hightStandardArray[$day_age_max]['lowest'] = getStandardDataBeginAndEnd($hightStandardArray, $day_age_max, 'lowest');

                        // dỮ LIỆU đường cao nhất (thư viện charts.js)
                        $highestForChartChartsJS = getStandardDataForChartChartJS($hightStandardArray, $day_age_min, $day_age_max, 'highest');

                        // Dữ liệu đường thấp nhất (thư viện charts.js)
                        $lowestForChartChartsJS = getStandardDataForChartChartJS($hightStandardArray, $day_age_min, $day_age_max, 'lowest');

                        // 2. Cân nặng
                        // Lấy 2 ngày gần nhất của $begin_day có dữ liệu, nội suy chuẩn của ngày bắt đầu
                        $weightStandardArray[$day_age_min]['highest'] = getStandardDataBeginAndEnd($weightStandardArray, $day_age_min, 'highest');

                        $weightStandardArray[$day_age_min]['lowest'] = getStandardDataBeginAndEnd($weightStandardArray, $day_age_min, 'lowest');

                        // Lấy 2 ngày gần nhất của $end_day có dữ liệu, nội suy chuẩn của ngày kết thúc
                        $weightStandardArray[$day_age_max]['highest'] = getStandardDataBeginAndEnd($weightStandardArray, $day_age_max, 'highest');

                        $weightStandardArray[$day_age_max]['lowest'] = getStandardDataBeginAndEnd($weightStandardArray, $day_age_max, 'lowest');

                        // Dữ liệu đường cao nhất (charts.js)
                        $highestWeightForChartChartJS = getStandardDataForChartChartJS($weightStandardArray, $day_age_min, $day_age_max, 'highest');

                        // Dữ liệu đường thấp nhất (charts.js)
                        $lowestWeightForChartChartJS = getStandardDataForChartChartJS($weightStandardArray, $day_age_min, $day_age_max, 'lowest');

                        // 3. BMI (charts.js)
                        $bmiForChartChartJS = getDataJsonForChartChartJS($childGrowth, $day_age_min, $day_age_max, $birthday, 'bmi');

                        // GEt label
                        $label = '';
                        for ($i = $day_age_min; $i <= $day_age_max; $i++) {
                            if ($i % 30 == 0) {
                                $month = $i / 30;
                                $label .= ' ' . $month . ',';
                            } else {
                                $label .= ' ,';
                            }
                        }
                        $smarty->assign('label', $label);
                    } else {
                        // Lấy dữ liệu nhập và của trẻ
                        $childGrowth = $childDao->getChildGrowthSearchForChartBeginEnd($child['child_parent_id'], $begin, $end);

                        // Lấy ngày bắt đầu và ngày kết thúc
                        $begin = toDBDate($begin);
                        $end = toDBDate($end);

                        $end_day = (strtotime($end) - strtotime($birthday)) / (60 * 60 * 24);
                        $end_day = $end_day / 30;
                        $end_day = (int)$end_day;
                        $end_day = ($end_day + 1) * 30;

                        $begin_day = (strtotime($begin) - strtotime($birthday)) / (60 * 60 * 24);
                        $begin_day = $begin_day / 30;
                        $begin_day = (int)$begin_day;
                        $begin_day = $begin_day * 30;
                        if ($begin_day < 0) {
                            throw new Exception(__("You must select day begin after birthday"));
                        }

                        // Dữ liệu chuẩn người dùng nhập vào vẽ biểu đồ chiều cao (thư viện charts.js)
                        $heightForChartChartJS = getDataJsonForChartChartJS($childGrowth, $begin_day, $end_day, $birthday, 'height');

                        // Lấy dữ liệu người dùng nhập vào vẽ biểu đồ cân nặng (charts.js)
                        $weightForChartChartJS = getDataJsonForChartChartJS($childGrowth, $begin_day, $end_day, $birthday, 'weight');

                        if ($child['gender'] == 'male') {
                            // Dữ liệu chuẩn
                            $hightStandardArray = getHightDataForChart($hightStandardJsonMale);
                            $weightStandardArray = getHightDataForChart($weightStandardJsonMale);
                        } else {
                            $hightStandardArray = getHightDataForChart($hightStandardJsonFeMale);
                            $weightStandardArray = getHightDataForChart($weightStandardJsonFeMale);
                        }

                        // 1. Chiều cao
                        // Lấy 2 ngày gần nhất của $begin_day có dữ liệu, nội suy chuẩn của ngày bắt đầu
                        $hightStandardArray[$begin_day]['highest'] = getStandardDataBeginAndEnd($hightStandardArray, $begin_day, 'highest');

                        $hightStandardArray[$begin_day]['lowest'] = getStandardDataBeginAndEnd($hightStandardArray, $begin_day, 'lowest');

                        // Lấy 2 ngày gần nhất của $end_day có dữ liệu, nội suy chuẩn của ngày kết thúc
                        $hightStandardArray[$end_day]['highest'] = getStandardDataBeginAndEnd($hightStandardArray, $end_day, 'highest');

                        $hightStandardArray[$end_day]['lowest'] = getStandardDataBeginAndEnd($hightStandardArray, $end_day, 'lowest');

                        // dỮ LIỆU đường cao nhất (thư viện charts.js)
                        $highestForChartChartsJS = getStandardDataForChartChartJS($hightStandardArray, $begin_day, $end_day, 'highest');

                        // Dữ liệu đường thấp nhất (thư viện charts.js)
                        $lowestForChartChartsJS = getStandardDataForChartChartJS($hightStandardArray, $begin_day, $end_day, 'lowest');

                        // 2. Cân nặng
                        // Lấy 2 ngày gần nhất của $begin_day có dữ liệu, nội suy chuẩn của ngày bắt đầu
                        $weightStandardArray[$begin_day]['highest'] = getStandardDataBeginAndEnd($weightStandardArray, $begin_day, 'highest');

                        $weightStandardArray[$begin_day]['lowest'] = getStandardDataBeginAndEnd($weightStandardArray, $begin_day, 'lowest');

                        // Lấy 2 ngày gần nhất của $end_day có dữ liệu, nội suy chuẩn của ngày kết thúc
                        $weightStandardArray[$end_day]['highest'] = getStandardDataBeginAndEnd($weightStandardArray, $end_day, 'highest');

                        $weightStandardArray[$end_day]['lowest'] = getStandardDataBeginAndEnd($weightStandardArray, $end_day, 'lowest');

                        // Dữ liệu đường cao nhất (charts.js)
                        $highestWeightForChartChartJS = getStandardDataForChartChartJS($weightStandardArray, $begin_day, $end_day, 'highest');

                        // Dữ liệu đường thấp nhất (charts.js)
                        $lowestWeightForChartChartJS = getStandardDataForChartChartJS($weightStandardArray, $begin_day, $end_day, 'lowest');

                        // 3. BMI (charts.js)
                        $bmiForChartChartJS = getDataJsonForChartChartJS($childGrowth, $begin_day, $end_day, $birthday, 'bmi');

                        // GEt label
                        $label = '';
                        for ($i = $begin_day; $i <= $end_day; $i++) {
                            if ($i % 30 == 0) {
                                $month = $i / 30;
                                $label .= ' ' . $month . ',';
                            } else {
                                $label .= ' ,';
                            }
                        }
                        $smarty->assign('label', $label);
                    }

                    // Chiều cao for charts.js
                    $smarty->assign('highestForChartChartJS', $highestForChartChartsJS);
                    $smarty->assign('lowestForChartChartJS', $lowestForChartChartsJS);
                    $smarty->assign('heightForChartChartJS', $heightForChartChartJS);

                    //Cân nặng cho charts.js
                    $smarty->assign('highestWeightForChartChartJS', $highestWeightForChartChartJS);
                    $smarty->assign('lowestWeightForChartChartJS', $lowestWeightForChartChartJS);
                    $smarty->assign('weightForChartChartJS', $weightForChartChartJS);

                    // BMI (charts.js)
                    $smarty->assign('bmiForChartChartJS', $bmiForChartChartJS);


                    $smarty->assign('username', $class['group_name']);
                    $smarty->assign('growth', array_reverse($childGrowth));
                    $smarty->assign('child', $child);
                    $smarty->assign('childId', $childId);
                }
                $children = getClassData($class['group_id'], CLASS_CHILDREN);
                $smarty->assign('children', $children);
                $smarty->assign('begin', $beginScreen);
                $smarty->assign('end', $endScreen);
                $smarty->assign('is_module', 1);
                break;

            case 'add':
                page_header(__("Class Management") . " &rsaquo; " . __("Children") . " &rsaquo; " . __("Add health index"));
                $children = getClassData($class['group_id'], CLASS_CHILDREN);
                $smarty->assign('children', $children);
                break;

            default:
                _error(404);
                break;
        }
        break;

    case 'dailymessage':
        page_header(__("Class Management") . " &rsaquo; " . __("Daily message"));

        break;

    default:
        _error(404);
}

// assign variables
$smarty->assign('schoolConfig', $schoolConfig);
$smarty->assign('class_id', $class['group_id']);
$smarty->assign('username', $_GET['username']);
$smarty->assign('view', $_GET['view']);
$smarty->assign('sub_view', $_GET['sub_view']);
// page footer
page_footer("class");

?>