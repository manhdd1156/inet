<?php
/**
 * Hàm push notification nhắc nhở viết sổ liên lạc
 *
 * @package ConIu v1
 * @author TaiLA
 */

//$path = str_replace('includes/ajax/ci/php_script/schedule', '', dirname(__FILE__));
//set_include_path($path);

// fetch bootstrap
require('bootstrap.php');

$db->autocommit(false);

try {
    include_once(DAO_PATH . 'dao_report.php');
    $reportDao = new ReportDAO();

    include_once(DAO_PATH . 'dao_attendance.php');
    $attendanceDao = new AttendanceDAO();

    include_once(DAO_PATH . 'dao_school.php');
    $schoolDao = new SchoolDAO();

    include_once(DAO_PATH . 'dao_user.php');
    $userDao = new UserDAO();

    include_once(DAO_PATH . 'dao_role.php');
    $roleDao = new RoleDAO();

    $db->begin_transaction();

    // Lấy danh sách trường đang sử dụng coniu
    $schools = $schoolDao->getAllSchoolsUsingInet(1);

    $today = date('Y-m-d');

    // Lặp danh sách trường, kiểm tra chu kỳ viết sổ liên lạc của trường
    foreach ($schools as $school) {
        $report_interval = $school['report_cycle'];
        if($report_interval == 1) {
            $stringClass = "";
            $countChild = 0;

            // Nếu chu kỳ viết sổ liên lạc hàng ngày
            // Lấy danh sách trẻ của trường
            $children = getSchoolData($school['page_id'], SCHOOL_CHILDREN);
            $childIds = array_keys($children);

            // Lấy danh sách những trẻ được viết sổ liên lạc trong ngày của trường
            $childIdsCreated = $reportDao->getChildIdsCreatedReportOnDay($school['page_id'], $today);

            // Danh sách những trẻ chưa được viết sổ liên lạc trong ngày
            $childIdsNoReport = array_diff($childIds, $childIdsCreated);

            // Lấy danh sách lớp của trường
            $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
            // lặp danh sách lớp
            foreach ($classes as $class) {
                // Lấy danh sách trẻ của lớp
                $childrenClass = getClassData($class['group_id'], CLASS_CHILDREN);
                $childIdsClass = array_keys($childrenClass);
                // Danh sách những trẻ chưa được viết sổ liên lạc trong lớp
                $childIdsClassNoReport = array_diff($childIdsClass, $childIdsCreated);
                $todaySys = toSysDate($today);
                // Lấy danh sách những trẻ nghỉ học của lớp trong ngày
                $childIdsPresent = $attendanceDao->getPresentChildIds($school['page_id'], $todaySys, $class['group_id']);

                // Chỉ tính những trẻ đi học mà không được gửi sổ liên lạc
                $childIdsLast = array();
                foreach ($childIdsPresent as $row) {
                    if(in_array($row, $childIdsClassNoReport)) {
                        $childIdsLast[] = $row;
                    }
                }
                if(count($childIdsLast) > 0) {
                    $stringClass .= ", " . $class['group_title'];
                    $countChild = $countChild + count($childIdsLast);

                    // Gửi thông báo đến giáo viên trong lớp
                    $teachers = getClassData($class['group_id'], CLASS_TEACHERS);
                    $teacherIds = array_keys($teachers);
                    $message = sprintf(__("%s: There are %s student have not contact book on this day"), $class['group_title'], count($childIdsLast));

                    foreach ($teacherIds as $userId) {
                        $userDao->postNotifications($userId, NOTIFICATION_REMIND_REPORT, NOTIFICATION_NODE_TYPE_CLASS,
                            $class['group_id'], $message, convertText4Web($class['group_name']), '', 1);
                    }
                }
            }

            // Gửi thông báo cho quản lý trường
            // Lấy danh sách user có quyền đối với module $view
            $userIdsYesPermission = $roleDao->getUserIdsOfModule($school['page_id'], 'reports');
            $userIdsYesPermission[] = $school['page_admin'];

            // Lấy danh sách hiệu trưởng của trường
            if (!is_empty($school['principal'])) {
                $principals = explode(',', $school['principal']);
                foreach ($principals as $row) {
                    $userIdsYesPermission[] = $row;
                }
            }
            $userIdsYesPermission = array_unique($userIdsYesPermission);
            if($countChild > 0) {
                if($stringClass != '') {
                    $stringClass = trim($stringClass, ", ");
                }
                $message = sprintf(__("%s: There are %s student (%s) have not contact book on this day"), $school['page_title'], $countChild, convertText4Web($stringClass));
                // Gửi thông báo đến quản lý trường
                foreach ($userIdsYesPermission as $userId) {
                    $userDao->postNotifications($userId, NOTIFICATION_REMIND_REPORT, NOTIFICATION_NODE_TYPE_SCHOOL,
                        $school['page_id'], $message, convertText4Web($school['page_name']), '', 1);
                }
            }
        } elseif ($report_interval == 2) {
            // Kiểm tra xem ngày hiện tại có phải là thứ 7 không.
            $weekday = date("l");
            $weekday = strtolower($weekday);
            if($weekday == "saturday") {
                $stringClass = "";
                $countChild = 0;

                // Nếu chu kỳ viết sổ liên lạc hàng tuần
                // Lấy danh sách trẻ của trường
                $children = getSchoolData($school['page_id'], SCHOOL_CHILDREN);
                $childIds = array_keys($children);

                // Lấy ngày thứ 2 đầu tuần
                $time = strtotime('monday this week');
                $monday = strftime("%Y-%m-%d", $time);
                // Lấy danh sách những trẻ được viết sổ liên lạc trong tuần của trường
                $childIdsCreated = $reportDao->getChildIdsCreatedReportOnWeek($school['page_id'], $monday, $today);

                // Danh sách những trẻ chưa được viết sổ liên lạc trong tuần
                $childIdsNoReport = array_diff($childIds, $childIdsCreated);

                // Lấy danh sách lớp của trường
                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                // lặp danh sách lớp
                foreach ($classes as $class) {
                    // Lấy danh sách trẻ của lớp
                    $childrenClass = getClassData($class['group_id'], CLASS_CHILDREN);
                    $childIdsClass = array_keys($childrenClass);
                    // Danh sách những trẻ chưa được viết sổ liên lạc trong lớp
                    $childIdsClassNoReport = array_diff($childIdsClass, $childIdsCreated);
                    if(count($childIdsClassNoReport) > 0) {
                        $stringClass .= ", " . $class['group_title'];
                        $countChild = $countChild + count($childIdsClassNoReport);

                        // Gửi thông báo đến giáo viên trong lớp
                        $teachers = getClassData($class['group_id'], CLASS_TEACHERS);
                        $teacherIds = array_keys($teachers);
                        $message = sprintf(__("%s: There are %s student have not contact book on this week"), $class['group_title'], count($childIdsClassNoReport));
                        // Gửi thông báo đến quản lý trường
                        foreach ($teacherIds as $userId) {
                            $userDao->postNotifications($userId, NOTIFICATION_REMIND_REPORT, NOTIFICATION_NODE_TYPE_CLASS,
                                $class['group_id'], $message, convertText4Web($class['group_name']), '', 1);
                        }
                    }
                }

                // Gửi thông báo cho quản lý trường
                // Lấy danh sách user có quyền đối với module $view
                $userIdsYesPermission = $roleDao->getUserIdsOfModule($school['page_id'], 'reports');
                $userIdsYesPermission[] = $school['page_admin'];

                // Lấy danh sách hiệu trưởng của trường
                if (!is_empty($school['principal'])) {
                    $principals = explode(',', $school['principal']);
                    foreach ($principals as $row) {
                        $userIdsYesPermission[] = $row;
                    }
                }
                $userIdsYesPermission = array_unique($userIdsYesPermission);
                if($countChild > 0) {
                    if($stringClass != '') {
                        $stringClass = trim($stringClass, ", ");
                    }
                    $message = sprintf(__("%s: There are %s student (%s) have not contact book on this week"), $school['page_title'], $countChild, convertText4Web($stringClass));
                    // Gửi thông báo đến quản lý trường
                    foreach ($userIdsYesPermission as $userId) {
                        $userDao->postNotifications($userId, NOTIFICATION_REMIND_REPORT, NOTIFICATION_NODE_TYPE_SCHOOL,
                            $school['page_id'], $message, convertText4Web($school['page_name']), '', 1);
                    }
                }
            }
        }

        elseif ($report_interval == 3) {
            // Kiểm tra xem ngày hiện tại có phải là ngày cuối tháng không.
            $endDay = date("Y-m-t");
            if($endDay == $today) {
                $stringClass = "";
                $countChild = 0;

                // Nếu chu kỳ viết sổ liên lạc hàng tháng
                // Lấy danh sách trẻ của trường
                $children = getSchoolData($school['page_id'], SCHOOL_CHILDREN);
                $childIds = array_keys($children);

                // Lấy ngày đầu tháng
                $beginDay = date("Y-m-01");
                // Lấy danh sách những trẻ được viết sổ liên lạc trong tháng của trường
                $childIdsCreated = $reportDao->getChildIdsCreatedReportOnWeek($school['page_id'], $beginDay, $today);

                // Danh sách những trẻ chưa được viết sổ liên lạc trong tuần
                $childIdsNoReport = array_diff($childIds, $childIdsCreated);

                // Lấy danh sách lớp của trường
                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                // lặp danh sách lớp
                foreach ($classes as $class) {
                    // Lấy danh sách trẻ của lớp
                    $childrenClass = getClassData($class['group_id'], CLASS_CHILDREN);
                    $childIdsClass = array_keys($childrenClass);
                    // Danh sách những trẻ chưa được viết sổ liên lạc trong lớp
                    $childIdsClassNoReport = array_diff($childIdsClass, $childIdsCreated);
                    if(count($childIdsClassNoReport) > 0) {
                        $stringClass .= ", " . $class['group_title'];
                        $countChild = $countChild + count($childIdsClassNoReport);

                        // Gửi thông báo đến giáo viên trong lớp
                        $teachers = getClassData($class['group_id'], CLASS_TEACHERS);
                        $teacherIds = array_keys($teachers);
                        $message = sprintf(__("%s: There are %s student have not contact book on this month"), $class['group_title'], count($childIdsClassNoReport));
                        // Gửi thông báo đến quản lý trường
                        foreach ($teacherIds as $userId) {
                            $userDao->postNotifications($userId, NOTIFICATION_REMIND_REPORT, NOTIFICATION_NODE_TYPE_CLASS,
                                $class['group_id'], $message, convertText4Web($class['group_name']), '', 1);
                        }
                    }
                }

                // Gửi thông báo cho quản lý trường
                // Lấy danh sách user có quyền đối với module $view
                $userIdsYesPermission = $roleDao->getUserIdsOfModule($school['page_id'], 'reports');
                $userIdsYesPermission[] = $school['page_admin'];

                // Lấy danh sách hiệu trưởng của trường
                if (!is_empty($school['principal'])) {
                    $principals = explode(',', $school['principal']);
                    foreach ($principals as $row) {
                        $userIdsYesPermission[] = $row;
                    }
                }
                $userIdsYesPermission = array_unique($userIdsYesPermission);
                if($countChild > 0) {
                    if($stringClass != '') {
                        $stringClass = trim($stringClass, ", ");
                    }
                    $message = sprintf(__("%s: There are %s student (%s) have not contact book on this month"), $school['page_title'], $countChild, convertText4Web($stringClass));
                    // Gửi thông báo đến quản lý trường
                    foreach ($userIdsYesPermission as $userId) {
                        $userDao->postNotifications($userId, NOTIFICATION_REMIND_REPORT, NOTIFICATION_NODE_TYPE_SCHOOL,
                            $school['page_id'], $message, convertText4Web($school['page_name']), '', 1);
                    }
                }
            }
        }
    }

    $db->commit();
}
catch (Exception $e) {
    $db->rollback();
    return_json( array('error' => true, 'message' => $e->getMessage()) );
} finally {
    $db->autocommit(true);
}
?>