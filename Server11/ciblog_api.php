<?php

/**
 * index
 *
 * @package inet
 * @author NOGA Co., Ltd
 */

define('ABSPATH', dirname(__FILE__) . '/');
define("DEFAULT_LOCALE", "vi_VN");
// get functions
require_once(ABSPATH . 'includes/functions.php');

define('MAX_RESULT', 10);
define('SYS_URL', 'https://blog.inet.vn/');
define('SYS_UPLOAD', 'https://blog.inet.vn/wp-content/uploads/');

define('DB_BLOG_NAME', 'bloginet');
/** MySQL database username */
//define('DB_BLOG_USER', 'root');
define('DB_BLOG_USER', 'bloguser');
/** MySQL database password */
//define('DB_BLOG_PASSWORD', '');
define('DB_BLOG_PASSWORD', 'Inet195!');
/** MySQL hostname */
define('DB_BLOG_HOST', 'localhost');
/** Database Charset to use in creating database tables. */
define('DB_BLOG_CHARSET', 'utf8mb4');
/** The Database Collate type. Don't change this if in doubt. */
define('DB_BLOG_COLLATE', '');
error_log('ciblog_api database connect : ');
//$time_start = microtime(true);
// connect to the database
$db = new mysqli(DB_BLOG_HOST, DB_BLOG_USER, DB_BLOG_PASSWORD, DB_BLOG_NAME);
$db->set_charset(DB_BLOG_CHARSET);
if (mysqli_connect_error()) {
    _error(DB_ERROR);
}


switch ($_GET['action']) {
    case 'getPosts':

        if (!isset($_POST['category'])) {
            _api_error(404);
        }

        $validFilter = array('latest', 'featured', 'most_popular', 'popular_in_7days', 'review_score', 'random');
        if (!in_array($_POST['filter_by'], $validFilter)) {
            _api_error(404);
        }

        $offset = isset($_POST['page']) ? $_POST['page'] : 0;
        $offset *= MAX_RESULT;

        $queryLatest = "SELECT p.id, p.post_date, p.post_title, p.post_name, m.meta_value as post_views_count, wm2.meta_value as thumbnail FROM cib_posts p
          INNER JOIN cib_postmeta m ON (p.ID = m.post_id) AND m.meta_key = 'post_views_count'
            LEFT JOIN 
                cib_postmeta wm1
                ON (
                    wm1.post_id = p.id 
                    AND wm1.meta_value IS NOT NULL
                    AND wm1.meta_key = '_thumbnail_id'             
                )
            LEFT JOIN 
                cib_postmeta wm2
                ON (
                    wm1.meta_value = wm2.post_id
                    AND wm2.meta_key = '_wp_attached_file'
                    AND wm2.meta_value IS NOT NULL  
                )
            WHERE p.post_type='post' AND p.post_status = 'publish' AND p.ID IN (
                SELECT r.object_id FROM cib_term_relationships r WHERE r.term_taxonomy_id IN (
              SELECT ta.term_taxonomy_id FROM cib_term_taxonomy ta WHERE ta.taxonomy = 'category' AND ta.term_id IN (
                        SELECT t.term_id FROM cib_terms t WHERE t.slug = %s
                    )
                )
            ) GROUP BY p.ID ORDER BY p.post_date DESC LIMIT %s, %s";

        $queryFeatured = "SELECT p.id, p.post_date, p.post_title, p.guid, p.post_name, m.meta_value as post_views_count, wm2.meta_value as thumbnail FROM cib_posts p
            INNER JOIN cib_postmeta m ON ( p.ID = m.post_id ) AND m.meta_key = 'post_views_count'
            LEFT JOIN 
                cib_postmeta wm1
                ON (
                    wm1.post_id = p.id 
                    AND wm1.meta_value IS NOT NULL
                    AND wm1.meta_key = '_thumbnail_id'             
                )
            LEFT JOIN 
                cib_postmeta wm2
                ON (
                    wm1.meta_value = wm2.post_id
                    AND wm2.meta_key = '_wp_attached_file'
                    AND wm2.meta_value IS NOT NULL  
                )
            WHERE p.post_type='post' AND p.post_status = 'publish' AND p.ID IN ( SELECT p.id FROM cib_posts p, cib_terms t, cib_term_taxonomy tt, cib_term_relationships tr, cib_terms t2, cib_term_taxonomy tt2, cib_term_relationships tr2, cib_terms t3, cib_term_taxonomy tt3, cib_term_relationships tr3 
WHERE p.id = tr.object_id AND t.term_id = tt.term_id AND tr.term_taxonomy_id = tt.term_taxonomy_id AND p.id = tr2.object_id AND t2.term_id = tt2.term_id AND tr2.term_taxonomy_id = tt2.term_taxonomy_id AND p.id = tr3.object_id AND t3.term_id = tt3.term_id AND tr3.term_taxonomy_id = tt3.term_taxonomy_id 
AND (tt.taxonomy = 'category' AND tt.term_id = t.term_id AND t.slug = %s) AND (tt2.taxonomy = 'category' AND tt2.term_id = t2.term_id and t2.slug = 'featured')) GROUP BY p.ID ORDER BY p.post_date DESC LIMIT %s, %s";

        $queryMostPopular = "SELECT p.id, p.post_date, p.post_title, p.post_name, m.meta_value as post_views_count, wm2.meta_value as thumbnail FROM cib_posts p
            INNER JOIN cib_postmeta m ON ( p.ID = m.post_id ) AND m.meta_key = 'post_views_count'
            LEFT JOIN 
                cib_postmeta wm1
                ON (
                    wm1.post_id = p.id 
                    AND wm1.meta_value IS NOT NULL
                    AND wm1.meta_key = '_thumbnail_id'             
                )
            LEFT JOIN 
                cib_postmeta wm2
                ON (
                    wm1.meta_value = wm2.post_id
                    AND wm2.meta_key = '_wp_attached_file'
                    AND wm2.meta_value IS NOT NULL  
                )
            WHERE p.post_type='post' AND p.post_status = 'publish' AND p.ID IN (
                SELECT r.object_id FROM cib_term_relationships r WHERE r.term_taxonomy_id IN (
              SELECT ta.term_taxonomy_id FROM cib_term_taxonomy ta WHERE ta.taxonomy = 'category' AND ta.term_id IN (
                        SELECT t.term_id FROM cib_terms t WHERE t.slug = %s
                    )
                )
            ) GROUP BY p.ID ORDER BY m.meta_value+0 DESC LIMIT %s, %s";

        $queryPopularIn7Days = "SELECT p.id, p.post_date, p.post_title, p.post_name, m.meta_value as post_views_count_7_day_total, wm2.meta_value as thumbnail FROM cib_posts p
            INNER JOIN cib_postmeta m ON ( p.ID = m.post_id ) AND m.meta_key = 'post_views_count_7_day_total'
            LEFT JOIN 
                cib_postmeta wm1
                ON (
                    wm1.post_id = p.id 
                    AND wm1.meta_value IS NOT NULL
                    AND wm1.meta_key = '_thumbnail_id'             
                )
            LEFT JOIN 
                cib_postmeta wm2
                ON (
                    wm1.meta_value = wm2.post_id
                    AND wm2.meta_key = '_wp_attached_file'
                    AND wm2.meta_value IS NOT NULL  
                )
            WHERE p.post_type='post' AND p.post_status = 'publish' AND p.ID IN (
                SELECT r.object_id FROM cib_term_relationships r WHERE r.term_taxonomy_id IN (
              SELECT ta.term_taxonomy_id FROM cib_term_taxonomy ta WHERE ta.taxonomy = 'category' AND ta.term_id IN (
                        SELECT t.term_id FROM cib_terms t WHERE t.slug = %s
                    )
                )
            ) GROUP BY p.ID ORDER BY m.meta_value+0 DESC LIMIT %s, %s";

        $queryReviewScore = "SELECT p.id, p.post_date, p.post_title, p.post_name, m.meta_value as td_review_key, wm2.meta_value as thumbnail FROM cib_posts p
            INNER JOIN cib_postmeta m ON ( p.ID = m.post_id ) AND m.meta_key = 'td_review_key'
            LEFT JOIN 
                cib_postmeta wm1
                ON (
                    wm1.post_id = p.id 
                    AND wm1.meta_value IS NOT NULL
                    AND wm1.meta_key = '_thumbnail_id'             
                )
            LEFT JOIN 
                cib_postmeta wm2
                ON (
                    wm1.meta_value = wm2.post_id
                    AND wm2.meta_key = '_wp_attached_file'
                    AND wm2.meta_value IS NOT NULL  
                )
            WHERE p.post_type='post' AND p.post_status = 'publish' AND p.ID IN (
                SELECT r.object_id FROM cib_term_relationships r WHERE r.term_taxonomy_id IN (
              SELECT ta.term_taxonomy_id FROM cib_term_taxonomy ta WHERE ta.taxonomy = 'category' AND ta.term_id IN (
                        SELECT t.term_id FROM cib_terms t WHERE t.slug = %s
                    )
                )
            ) GROUP BY p.ID ORDER BY m.meta_value+0 DESC LIMIT %s, %s";

        $queryRandom = "SELECT p.id, p.post_date, p.post_title, p.post_name, wm2.meta_value as thumbnail FROM cib_posts p          
            LEFT JOIN 
                cib_postmeta wm1
                ON (
                    wm1.post_id = p.id 
                    AND wm1.meta_value IS NOT NULL
                    AND wm1.meta_key = '_thumbnail_id'             
                )
            LEFT JOIN 
                cib_postmeta wm2
                ON (
                    wm1.meta_value = wm2.post_id
                    AND wm2.meta_key = '_wp_attached_file'
                    AND wm2.meta_value IS NOT NULL  
                )
            WHERE p.post_type='post' AND p.post_status = 'publish' AND p.ID IN (
                SELECT r.object_id FROM cib_term_relationships r WHERE r.term_taxonomy_id IN (
              SELECT ta.term_taxonomy_id FROM cib_term_taxonomy ta WHERE ta.taxonomy = 'category' AND ta.term_id IN (
                        SELECT t.term_id FROM cib_terms t WHERE t.slug = %s
                    )
                )
            ) GROUP BY p.ID ORDER BY RAND() DESC LIMIT %s, %s";


        $slug = $_POST['category'];
        if ($_POST['filter_by'] == 'latest') {
            $strSql = sprintf($queryLatest, secure($slug), secure($offset, 'int', false), secure(MAX_RESULT, 'int', false));
        } elseif ($_POST['filter_by'] == 'featured') {
            $strSql = sprintf($queryFeatured, secure($slug), secure($offset, 'int', false), secure(MAX_RESULT, 'int', false));
        } elseif ($_POST['filter_by'] == 'most_popular') {
            $strSql = sprintf($queryMostPopular, secure($slug), secure($offset, 'int', false), secure(MAX_RESULT, 'int', false));
        } elseif ($_POST['filter_by'] == 'popular_in_7days') {
            $strSql = sprintf($queryPopularIn7Days, secure($slug), secure($offset, 'int', false), secure(MAX_RESULT, 'int', false));
        } elseif ($_POST['filter_by'] == 'review_score') {
            $strSql = sprintf($queryReviewScore, secure($slug), secure($offset, 'int', false), secure(MAX_RESULT, 'int', false));
        } elseif ($_POST['filter_by'] == 'random') {
            $strSql = sprintf($queryRandom, secure($slug), secure($offset, 'int', false), secure(MAX_RESULT, 'int', false));
        }

        $results = array();
        $get_results = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_results->num_rows > 0) {
            while($data = $get_results->fetch_assoc()) {
                $data['thumbnail'] = !is_empty($data['thumbnail']) ? SYS_UPLOAD.$data['thumbnail'] : "" ;
                $data['post_name'] = !is_empty($data['post_name']) ? SYS_URL.$data['post_name'] : "" ;
                $results[] = $data;
            }
        }
        
        return_json(array(
            'code' => 200,
            'message' => 'OK',
            'data' => array(
                'posts' => $results
            )
        ));
        break;

    case 'getCategories':

        $results = null;
        if (isset($_POST['category'])) {
            $category = getCategory($_POST['category']);
            if (!empty($category)) {
                $sub1 = array();
                $children = getChildCategories($category['id']);
                foreach ($children as $child) {

                    $sub2 = getChildCategories($child['id']);
                    if (!empty($sub2)) {
                        $child['sub2'] = $sub2;
                    }

                    $sub1[] = $child;
                }
                if (!empty($sub1)) {
                    $category['sub1'] = $sub1;
                }
                $results = $category;
            }

        } else {
            $parents = getParentCategories();
            foreach ($parents as $parent) {
                $sub1 = array();
                $children = getChildCategories($parent['id']);
                foreach ($children as $child) {

                    $sub2 = getChildCategories($child['id']);
                    if (!empty($sub2)) {
                        $child['sub2'] = $sub2;
                    }
                    $sub1[] = $child;
                }
                if (!empty($sub1)) {
                    $parent['sub1'] = $sub1;
                }
                $results = $parent;
            }

        }

        return_json(array(
            'code' => 200,
            'message' => 'OK',
            'data' => array(
                'categories' => $results
            )
        ));
        break;

    default:
        _api_error(400);

}


function getCategory($slug) {
    global $db;

    $strSql = sprintf("SELECT term_id AS id, name, slug FROM cib_terms 
           WHERE term_id IN (SELECT term_id FROM cib_term_taxonomy WHERE taxonomy = 'category') AND slug = %s", secure($slug));
    $result = array();
    $get_category = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    if($get_category->num_rows > 0) {
        $result = $get_category->fetch_assoc();
    }
    return $result;
}

function getParentCategories() {
    global $db;

    $strSql = "SELECT term_id AS id, name, slug FROM cib_terms 
           WHERE term_id IN (SELECT term_id FROM cib_term_taxonomy WHERE parent = 0 and taxonomy = 'category')  AND slug NOT IN ('coniu', 'featured', 'uncategorized')
           GROUP BY id ORDER BY name ASC";
    $parents = array();
    $get_parents = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    if($get_parents->num_rows > 0) {
        while($parent = $get_parents->fetch_assoc()) {
            $parents[] = $parent;
        }
    }
    return $parents;
}

function getChildCategories($id) {
    global $db;
    $strSql = "SELECT term_id as id, name, slug FROM cib_terms 
      WHERE term_id IN (SELECT term_id FROM cib_term_taxonomy WHERE parent = $id AND taxonomy = 'category') 
      GROUP BY id ORDER BY name ASC";

    $children = array();
    $get_children = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    if($get_children->num_rows > 0) {
        while($child = $get_children->fetch_assoc()) {
            $children[] = $child;
        }
    }
    return $children;
}


?>