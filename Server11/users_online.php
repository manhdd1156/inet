<?php
/**
 * Hàm chạy ngầm lưu user online tương tác vào DB
 *
 * @package ConIu v1
 * @author ConIu v1
 */

// $path = str_replace('includes/ajax/ci/php_script/schedule', '', dirname(__FILE__));
// set_include_path($path);

// fetch bootstrap
require('bootstrap.php');

$db->autocommit(false);

try {
    include_once(ABSPATH . 'includes/ajax/ci/memcache_caching.php');
    include_once(DAO_PATH . 'dao_statistic.php');

    $memcache = new CacheMemcache();
    $statisticDao = new StatisticDAO();

    $curDate = date('Y-m-d');
    if ($memcache->bEnabled) { // if Memcache enabled
        $db->begin_transaction();
        $stData = $memcache->getData(USERS_ONLINE);

        if(is_array($stData)) {
            if(count($stData[$curDate]) > 0){
                $statisticDao->updateUserOnlineForDB($stData[$curDate]);
            }
        }

        $db->commit();
    }

} catch (Exception $e) {
    $db->rollback();
    return_json( array('error' => true, 'message' => $e->getMessage()) );
} finally {
    $db->autocommit(true);
}


/**
 * Lấy tất cả các trường đang sử dụng inet
 *
 * @param $school_status
 * @param int $from
 * @param int $limit
 * @return array
 * @throws Exception
 */
function getSchoolIdsUsingInet() {
    global $db;

    $strSql = sprintf("SELECT P.page_id FROM pages P
              INNER JOIN ci_school_configuration SC ON P.page_id = SC.school_id
              WHERE P.page_category = '2' AND SC.school_status = '1' ORDER BY P.page_id ASC");


    $schoolIds = array();
    $get_schools = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    if($get_schools->num_rows > 0) {
        while($school = $get_schools->fetch_assoc()) {
            $schoolIds[] = $school['page_id'];
        }
    }
    return $schoolIds;
}


?>