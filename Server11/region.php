<?php
/**
 * Quản lý các chức năng chính của nhan vien Quản lý khu vực
 *
 * @package ConIu v1
 * @author TaiLA
 */

// set override_shutdown
$override_shutdown = true;

// fetch bootstrap
require('bootstrap.php');

// Kiểm tra user login rồi hay chưa
user_access();
// check admin logged in


$nogaRole = $user->_data['user_group'];

//if ((!$user->_is_admin) && ($nogaRole != USER_NOGA_MANAGE_ALL) && ($nogaRole != USER_NOGA_MANAGE_CITY)
//    && ($nogaRole != USER_NOGA_MANAGE_SCHOOL) && ($nogaRole != USER_MOD)){
//    _error(__('System Message'), __("You don't have the right permission to access this"));
//}

include_once(DAO_PATH.'dao_school.php');
include_once(DAO_PATH.'dao_class.php');
include_once(DAO_PATH.'dao_teacher.php');
include_once(DAO_PATH.'dao_child.php');
include_once(DAO_PATH.'dao_user.php');
include_once(DAO_PATH.'dao_foetus_development.php');
include_once(DAO_PATH.'dao_foetus_knowledge.php');
include_once(DAO_PATH.'dao_child_development.php');
include_once(DAO_PATH.'dao_noga_notification.php');
include_once(DAO_PATH.'dao_childmonth.php');
include_once(DAO_PATH . 'dao_statistic.php');
include_once(DAO_PATH . 'dao_review.php');
include_once(DAO_PATH . 'dao_region_manage.php');

$schoolDao = new SchoolDAO();
$classDao = new ClassDAO();
$teacherDao = new TeacherDAO();
$childDao = new ChildDAO();
$userDao = new UserDAO();
$foetusDevelopmentDao = new FoetusDevelopmentDAO();
$childDevelopmentDao = new ChildDevelopmentDAO();
$nogaNotificationDao = new NogaNotificationDAO();
$childMonthDao = new ChildMonthDAO();
$statisticDao = new StatisticDAO();
$reviewDao = new ReviewDAO();
$regionManageDao = new RegionManageDAO();

// Check tài khoản quản lý phòng
$region_manages = $regionManageDao->getRegionManageByUserId($user->_data['user_id']);

if(empty($regionManageDao)) {
    // Thông báo lỗi không có quyền vào đây
    _error(__('System Message'), __("You don't have the right permission to access this"));
}
// page content
switch ($_GET['view']) {
    case '':
        // page header
        page_header(__("REGION Management")." &rsaquo; ".__("School list"));
        $schools = null;

        // Lấy toàn bộ danh sách trường mà user đang quản lý thì vùng
        $city_ids = array();
        foreach ($region_manages as $region) {
            $city_ids[] = $region['city_id'];
        }
        $city_ids = array_unique($city_ids);


        if (($nogaRole == USER_NOGA_MANAGE_ALL) || $user->_is_admin) {
            $schools = $schoolDao->getAllSchools();
        } elseif ($nogaRole == USER_NOGA_MANAGE_CITY) {
            $nogaManages = $userDao->getNogaManage($user->_data['user_id']);
            $schools = $schoolDao->getSchoolInCity($nogaManages[0]['city_id']);
        } else {
            $schools = $schoolDao->getSchoolOfNOGAUser($user->_data['user_id']);
        }

        $insights['schools'] = $schools;
        // assign variables
        $smarty->assign('insights', $insights);
        break;
    case 'school':
        switch ($_GET['sub_view']) {
            case '':
                page_header(__("REGION Management") . " &rsaquo; " . __("School list"));
                $schools = array();
                $schoolIds = array();
                // Lấy danh sách tỉnh và huyện user quản lý
                $manages = array();
                foreach ($region_manages as $region) {
                    // Lấy danh sách trường thuộc region truyền vào
                    $school_ids = $schoolDao->getSchoolIdsByRegion($region['city_id'], $region['district_slug'], $region['grade']);

                    $schoolIds = array_merge($schoolIds, $school_ids);
                }

                $schoolIds = array_unique($schoolIds);

                $count = array();
                $count[0] = 0;
                $count[1] = 0;
                $count[2] = 0;
                $count[99] = 0;
                $count[100] = 0; // Trường chưa xác định cấp
                foreach ($schoolIds as $schoolId) {
                    $school['info'] = getSchoolData($schoolId, SCHOOL_INFO);
                    $school['config'] = getSchoolData($schoolId, SCHOOL_CONFIG);
                    // Lấy danh sách học sinh của trường
                    $children = getSchoolData($schoolId, SCHOOL_CHILDREN);
                    $school['children'] = count($children);
                    $schools[] = $school;
                    if(!empty($school['config']) && $school['config']['grade'] == '0') {
                        $count[0]++;
                    } elseif(!empty($school['config']) && $school['config']['grade'] == '1') {
                        $count[1]++;
                    } elseif(!empty($school['config']) && $school['config']['grade'] == '2') {
                        $count[2]++;
                    } elseif(!empty($school['config']) && $school['config']['grade'] == '99') {
                        $count[99]++;
                    } else {
                        $count[100]++;
                    }
                }

                for($i = 0; $i < count($schools); $i++) {
                    for($j = 1; $j <= count($schools); $j++) {
                        if($school[$i]['config']['grade'] > $schools[$j]['config']['grade']) {
                            $temp = $school[$i];
                            $school[$i] = $school[$j];
                            $school[$j] = $temp;
                        }
                    }
                }
                //echo "<pre>"; print_r($schools); die;
                $smarty->assign('rows', $schools);
                $smarty->assign('count', $count);
                break;
            default:
                _error(404);
                break;
        }

        $smarty->assign('configure', $_GET['p4']);
        break;
    case 'role':
        switch ($_GET['sub_view']) {
            case '':
                page_header(__("REGION Management") . " &rsaquo; " . __("Permission"));

                $city_id = $region_manages[0]['city_id'];
                // Lấy danh sách quản lý trong bảng ci_region_manage

                $result = $regionManageDao->getAllUserRegionManageByProvider($city_id, $region_manages[0]['district_slug'], $region_manages[0]['grade'], 0);
//                echo "<pre>";
//                print_r($region_manages); die('xxx');
                //echo "<pre>"; print_r($result); die;
                $smarty->assign('result', $result);
                $smarty->assign('region_manage', $region_manages[0]);
                $smarty->assign('cities', $cities);
                $smarty->assign('grades', $grades);
                $smarty->assign('city_id', $city_id);
                break;
            default:
                _error(404);
                break;
        }
        break;
    default:
        _error(404);
}

// assign variables
$smarty->assign('view', $_GET['view']);
$smarty->assign('sub_view', $_GET['sub_view']);

// page footer
page_footer("region");

?>