<?php
/**
 * Hàm chạy ngầm lưu lượt tương tác vào DB
 *
 * @package ConIu v1
 * @author ConIu v1
 */

// $path = str_replace('includes/ajax/ci/php_script/schedule', '', dirname(__FILE__));
// set_include_path($path);

// fetch bootstrap
require('bootstrap.php');

$db->autocommit(false);

try {
    include_once(ABSPATH . 'includes/ajax/ci/memcache_caching.php');
    include_once(DAO_PATH . 'dao_statistic.php');
    include_once(DAO_PATH . 'dao_school.php');

    $memcache = new CacheMemcache();
    $statisticDao = new StatisticDAO();
    $schoolDao = new SchoolDAO();

    $curDate = date('Y-m-d');
    $schoolIds = getSchoolIdsUsingInet();
    $schoolIds[] = 0;

    if ($memcache->bEnabled) { // if Memcache enabled
        $db->begin_transaction();

        foreach ($schoolIds as $schoolId) {
            $stData = $memcache->getData(STATISTIC_INTERACTIVE . $schoolId);

            if (is_array($stData)) {
                $args = array();
                foreach ($stData as $module => $data) {
                    if(count($data[$curDate]) > 0) {
                        foreach ($data[$curDate] as $object_id => $row) {
                            $args['school_id'] = $schoolId;
                            $args['date'] = $curDate;
                            $args['module'] = $module;
                            $args['object_id'] = $object_id;
                            $args['school_view'] = isset($row['school_view']) ? $row['school_view'] : 0;
                            $args['parent_view'] = isset($row['parent_view']) ? $row['parent_view'] : 0;
                            $args['is_created'] = isset($row['is_created']) ? $row['is_created'] : 0;
                            $statisticDao->insertInteractive($args);
                        }
                        if($module == "schedule") {
                            $statisticDao->updateViewCountSchedule($schoolId);
                        } elseif($module == "menu") {
                            $statisticDao->updateViewCountMenu($schoolId);
                        }
                        elseif($module == "post") {
                            $statisticDao->updateViewCountPost($schoolId);
                        }
                        elseif($module == "tuition") {
                            $statisticDao->updateViewCountTuition($schoolId);
                        }
                        elseif($module == "report") {
                            $statisticDao->updateViewCountReport($schoolId);
                        }
                        elseif($module == "event") {
                            $statisticDao->updateViewCountEvent($schoolId);
                        }
                    }
//                    switch ($module) {
//                        case 'page' :
//                            //1.Tăng số lượt xem trong ngày
//                            if (is_numeric($data[$curDate])) {
//                                $args['view_type'] = 'page';
//                                $args['date'] = $curDate;
//                                $args['school_id'] = $schoolId;
//                                $args['views_count_in_date'] = $data[$curDate];
//                                $statisticDao->insertViewsCountOfPage($args);
//                            }
//                            //Cập nhật số lượt xem trang trường trong 30 ngày gần nhất
//                            $statisticDao->updateViewsCountOfPageInLast30Days($schoolId);
//                            break;
//
//                        case 'post' :
//                            if (is_array($data[$curDate])) {
//                                $args['view_type'] = 'post';
//                                $args['date'] = $curDate;
//                                $args['school_id'] = $schoolId;
//                                $args['data'] = $data[$curDate];
//                                $statisticDao->insertViewsCount($args);
//                            }
//
//                            //Cập nhật số lượt xem các bài viết của trường
//                            $statisticDao->updateViewsCountOfPost($schoolId);
//                            break;
//
//                        case 'event' :
//                            if (is_array($data[$curDate])) {
//                                $args['view_type'] = 'event';
//                                $args['date'] = $curDate;
//                                $args['school_id'] = $schoolId;
//                                $args['data'] = $data[$curDate];
//                                $statisticDao->insertViewsCount($args);
//                            }
//
//                            //Cập nhật số lượt xem các bài viết của trường
//                            $statisticDao->updateViewsCountOfEvent($schoolId);
//                            break;
//
//                        case 'schedule' :
//                            if (is_array($data[$curDate])) {
//                                $args['view_type'] = 'schedule';
//                                $args['date'] = $curDate;
//                                $args['school_id'] = $schoolId;
//                                $args['data'] = $data[$curDate];
//                                $statisticDao->insertViewsCount($args);
//                            }
//                            //Cập nhật số lượt xem các bài viết của trường
//                            $statisticDao->updateViewsCountOfSchedule($schoolId);
//                            break;
//
//                        case 'menu' :
//                            if (is_array($data[$curDate])) {
//                                $args['view_type'] = 'menu';
//                                $args['date'] = $curDate;
//                                $args['school_id'] = $schoolId;
//                                $args['data'] = $data[$curDate];
//                                $statisticDao->insertViewsCount($args);
//                            }
//                            //Cập nhật số lượt xem các bài viết của trường
//                            $statisticDao->updateViewsCountOfMenu($schoolId);
//                            break;
//
//                        case 'report' :
//                            if (is_array($data[$curDate])) {
//                                $args['view_type'] = 'report';
//                                $args['date'] = $curDate;
//                                $args['school_id'] = $schoolId;
//                                $args['data'] = $data[$curDate];
//                                $statisticDao->insertViewsCount($args);
//                            }
//                            //Cập nhật số lượt xem các bài viết của trường
//                            $statisticDao->updateViewsCountOfReport($schoolId);
//                            break;
//
//                        case 'tuition' :
//                            if (is_array($data[$curDate])) {
//                                $args['view_type'] = 'tuition';
//                                $args['date'] = $curDate;
//                                $args['school_id'] = $schoolId;
//                                $args['data'] = $data[$curDate];
//                                $statisticDao->insertViewsCount($args);
//                            }
//                            //Cập nhật số lượt xem các bài viết của trường
//                            $statisticDao->updateViewsCountOfTuition($schoolId);
//                            break;
//
//                        case 'post_created' :
//                            if (is_array($data[$curDate])) {
//                                $args['view_type'] = 'post';
//                                $args['date'] = $curDate;
//                                $args['school_id'] = $schoolId;
//                                $args['data'] = $data[$curDate];
//                                $statisticDao->insertCreated($args);
//                            }
//                            break;
//
//                        case 'event_created' :
//                            if (is_array($data[$curDate])) {
//                                $args['view_type'] = 'event';
//                                $args['date'] = $curDate;
//                                $args['school_id'] = $schoolId;
//                                $args['data'] = $data[$curDate];
//                                $statisticDao->insertCreated($args);
//                            }
//                            break;
//
//                        case 'schedule_created' :
//                            if (is_array($data[$curDate])) {
//                                $args['view_type'] = 'schedule';
//                                $args['date'] = $curDate;
//                                $args['school_id'] = $schoolId;
//                                $args['data'] = $data[$curDate];
//                                $statisticDao->insertCreated($args);
//                            }
//                            break;
//
//                        case 'menu_created' :
//                            if (is_array($data[$curDate])) {
//                                $args['view_type'] = 'menu';
//                                $args['date'] = $curDate;
//                                $args['school_id'] = $schoolId;
//                                $args['data'] = $data[$curDate];
//                                $statisticDao->insertCreated($args);
//                            }
//                            break;
//
//                        case 'report_created' :
//                            if (is_array($data[$curDate])) {
//                                $args['view_type'] = 'report';
//                                $args['date'] = $curDate;
//                                $args['school_id'] = $schoolId;
//                                $args['data'] = $data[$curDate];
//                                $statisticDao->insertCreated($args);
//                            }
//                            break;
//
//                        case 'tuition_created' :
//                            if (is_array($data[$curDate])) {
//                                $args['view_type'] = 'tuition';
//                                $args['date'] = $curDate;
//                                $args['school_id'] = $schoolId;
//                                $args['data'] = $data[$curDate];
//                                $statisticDao->insertCreated($args);
//                            }
//                            break;
//                    }
                }

            } else {
                //3.Cập nhật tương tác toàn trường vào memcached
                updateStatisticInteractive($schoolId);
            }

        }

        //2. Cập nhật tương tác của người dùng với page, group của Coniu
        $topicData = $memcache->getData(NOGA_STATISTIC_TOPIC);

        if (is_array($topicData)) {
            $statisticDao->updateInteractiveTopic($topicData, $curDate);
        }
        $db->commit();
    }

} catch (Exception $e) {
    $db->rollback();
    return_json( array('error' => true, 'message' => $e->getMessage()) );
} finally {
    $db->autocommit(true);
}


/**
 * Lấy tất cả các trường đang sử dụng inet
 *
 * @param $school_status
 * @param int $from
 * @param int $limit
 * @return array
 * @throws Exception
 */
function getSchoolIdsUsingInet() {
    global $db;

    $strSql = sprintf("SELECT P.page_id FROM pages P
              INNER JOIN ci_school_configuration SC ON P.page_id = SC.school_id
              WHERE P.page_category = '2' AND SC.school_status = '1' ORDER BY P.page_id ASC");


    $schoolIds = array();
    $get_schools = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    if($get_schools->num_rows > 0) {
        while($school = $get_schools->fetch_assoc()) {
            $schoolIds[] = $school['page_id'];
        }
    }
    return $schoolIds;
}


?>