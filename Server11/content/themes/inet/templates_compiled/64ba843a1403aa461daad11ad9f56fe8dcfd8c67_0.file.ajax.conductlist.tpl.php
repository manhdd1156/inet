<?php
/* Smarty version 3.1.31, created on 2021-06-07 09:26:49
  from "D:\workplace\Inet-project\source\Server11\content\themes\inet\templates\ci\school\ajax.conductlist.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_60bd83e94a5fa9_72053396',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '64ba843a1403aa461daad11ad9f56fe8dcfd8c67' => 
    array (
      0 => 'D:\\workplace\\Inet-project\\source\\Server11\\content\\themes\\inet\\templates\\ci\\school\\ajax.conductlist.tpl',
      1 => 1622863375,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60bd83e94a5fa9_72053396 (Smarty_Internal_Template $_smarty_tpl) {
?>
<table class="table table-striped table-bordered table-hover js_dataTable">
    <thead>
    <tr><th colspan="7"><?php echo __("Conduct list");?>
&nbsp;(<span class="count_conduct"><?php echo count($_smarty_tpl->tpl_vars['result']->value);?>
</span>) </th></tr>
    <tr>
        <th><?php echo __("#");?>
</th>
        <th><?php echo __("Full name");?>
</th>
        <th><?php echo __("Gender");?>
</th>
        <th><?php echo __("Birthdate");?>
</th>
        <th><?php echo __("Semester");?>
 1</th>
        <th><?php echo __("Semester");?>
 2</th>
        <th><?php echo __("End semester");?>
</th>
        <th><?php echo __("Actions");?>
</th>

    </tr>
    </thead>
    <tbody>
        <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['result']->value, 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
            <tr>
                <td class="align-middle" align="center">
                    <strong><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</strong>
                </td>
                <td class="align-middle" style="word-break: break-all">
                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/children/detail/<?php echo $_smarty_tpl->tpl_vars['row']->value['child_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['row']->value['child_name'];?>
</a>
                </td>
                <td class="align-middle">
                    <?php if ($_smarty_tpl->tpl_vars['row']->value['gender'] == @constant('MALE')) {
echo __("Male");
} else {
echo __("Female");
}?>
                </td>
                <td align="center" class="align-middle"><?php echo $_smarty_tpl->tpl_vars['row']->value['birthday'];?>
</td>
                <td align="center" class="align-middle"><?php echo $_smarty_tpl->tpl_vars['row']->value['conduct1'];?>
</td>
                <td align="center" class="align-middle"><?php echo $_smarty_tpl->tpl_vars['row']->value['conduct2'];?>
</td>
                <td align="center" class="align-middle"><?php echo $_smarty_tpl->tpl_vars['row']->value['conduct3'];?>
</td>
                <td align = "center" class="align-middle">
                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/conducts/edit/<?php echo $_smarty_tpl->tpl_vars['row']->value['conduct_id'];?>
" class="btn btn-xs btn-default"><?php echo __("Edit");?>
</a>
                </td>
            </tr>
            <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

    </tbody>
</table>

<?php echo '<script'; ?>
>
    $(function() {
        // run DataTable
        $('.js_dataTable').DataTable({
            "aoColumnDefs": [ { 'aDataSort': false, 'aTargets': [ -1 ] } ],
            "language": {
                "decimal":        "",
                "emptyTable":     __["No data available in table"],
                "info":           __["Showing _START_ to _END_ of _TOTAL_ results"],
                "infoEmpty":      __["Showing 0 to 0 of 0 results"],
                "infoFiltered":   "(filtered from _MAX_ total entries)",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     __["Show"] + " " + "_MENU_" + " " + __["results"],
                "loadingRecords": __["Loading..."],
                "processing":     __["Processing..."],
                "search":         __["Search"],
                "zeroRecords":    __["No matching records found"],
                "paginate": {
                    "first":      __["First"],
                    "last":       __["Last"],
                    "next":       __["Next"],
                    "previous":   __["Previous"]
                },
                "aria": {
                    "sortAscending":  ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                }
            }
        });
    });
<?php echo '</script'; ?>
>
<?php }
}
