<?php
/* Smarty version 3.1.31, created on 2021-03-31 08:54:47
  from "D:\workplace\Server11\content\themes\inet\templates\ci\noga\noga.foetusdevelopments.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_6063d66711fe38_30626217',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '8f483d671f272e863d027e52ab672486f0fee3cc' => 
    array (
      0 => 'D:\\workplace\\Server11\\content\\themes\\inet\\templates\\ci\\noga\\noga.foetusdevelopments.tpl',
      1 => 1552404703,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:ci/noga/ajax.noga.foetusdevelopment.list.tpl' => 1,
  ),
),false)) {
function content_6063d66711fe38_30626217 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
            <div class="pull-right flip">
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/noga/foetusdevelopments/add" class="btn btn-default">
                    <i class="fa fa-plus"></i> <?php echo __("Add New");?>

                </a>
            </div>
        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
            <div class="pull-right flip">
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/noga/foetusdevelopments" class="btn btn-default">
                    <i class="fa fa-list"></i> <?php echo __("Lists");?>

                </a>
            </div>
        <?php }?>
        <i class="fas fa-calendar-alt fa-fw fa-lg pr10"></i>
        <?php echo __("Foetus development");?>

        <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
            &rsaquo; <?php echo __("Add New");?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "edit") {?>
            &rsaquo; <?php echo __("Update information about your foetus's development");?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "detail") {?>
            &rsaquo; <?php echo __('Detail');?>

        <?php }?>
    </div>
    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
        <div class="panel-body with-table form-horizontal">
            <div class="row" style="padding-left: 11px">
                <div class='col-sm-3'>
                    <div class="form-group">
                        <select name="child_type" id="child_type" class="form-control js_noga-foetus-development-search">
                            <option value="0"><?php echo __("All");?>
</option>
                            <option value="1"><?php echo __("Pregnancy check");?>
</option>
                            <option value="2"><?php echo __("Information");?>
</option>
                        </select>
                    </div>
                </div>
            </div>
            <div id="noga_development_list" class="pt10">
                <?php $_smarty_tpl->_subTemplateRender("file:ci/noga/ajax.noga.foetusdevelopment.list.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

            </div>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "detail") {?>
        <div class="panel-body with-table form-horizontal">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <tbody>
                    <tr>
                        <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Week");?>
</strong></td>
                        <td><?php echo $_smarty_tpl->tpl_vars['data']->value['week'];?>
</td>
                    </tr>
                    <tr>
                        <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Title");?>
</strong></td>
                        <td><?php echo $_smarty_tpl->tpl_vars['data']->value['title'];?>
</td>
                    </tr>
                    <tr>
                        <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Content");?>
</strong></td>
                        <td><?php echo $_smarty_tpl->tpl_vars['data']->value['content'];?>
</td>
                    </tr>
                    <tr>
                        <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Type");?>
</strong></td>
                        <td><?php if ($_smarty_tpl->tpl_vars['data']->value['type'] == 1) {?> <?php echo __("Pregnancy check");?>
 <?php } else { ?> <?php echo __("Information");?>
 <?php }?></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
        <div class="panel-body">
            <form class="form-horizontal" enctype="multipart/form-data" method="post" id="create_foetus_development">
                <input type="hidden" name="do" value="add"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Type");?>
</label>
                    <div class="col-sm-9">
                        <select name = "type" class="form-control">
                            <option value = "1"><?php echo __("Pregnancy check");?>
</option>
                            <option value = "2"><?php echo __("Information");?>
</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Day push notification");?>
 (*)</label>
                    <div class="col-sm-2">
                        <input type = "number" class="form-control" name="day_push" required autofocus max="365" min = "1">
                    </div>
                    <label class="col-sm-2 control-label text-left"><?php echo __("Time");?>
 (*)</label>
                    <div class="col-sm-2">
                        <input type = "text" class="form-control" name="hour" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Notice before");?>
?</label>
                    <div class="col-sm-4">
                        <div class="onoffswitch">
                            <input type="checkbox" name="is_notice_before" class="onoffswitch-checkbox" id="is_notice_before">
                            <label class="onoffswitch-label" for="is_notice_before"></label>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <input type = "number" class="form-control" name="notice_before_days" value="0" min="0" required>
                    </div>
                    <span class="control-label inl-block"><?php echo __("day");?>
</span>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Reminder before");?>
?</label>
                    <div class="col-sm-4">
                        <div class="onoffswitch">
                            <input type="checkbox" name="is_reminder_before" class="onoffswitch-checkbox" id="is_reminder_before">
                            <label class="onoffswitch-label" for="is_reminder_before"></label>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <input type = "number" class="form-control" name="reminder_before_days" value="0" min="0" required>
                    </div>
                    <span class="control-label inl-block"><?php echo __("day");?>
</span>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Title");?>
 (*)</label>
                    <div class="col-sm-9">
                        <input type = "text" class="form-control" name="title" required>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Text or Link?");?>
</label>
                    <div class="col-sm-3">
                        <select name = "content_type" class="form-control" id="content_type_select">
                            <option value = "3"><?php echo __("Text and Link");?>
</option>
                            <option value = "1"><?php echo __("Text");?>
</option>
                            <option value = "2"><?php echo __("Link");?>
</option>
                        </select>
                    </div>
                </div>
                <div class="form-group content_text">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Text");?>
</label>
                    <div class="col-sm-9">
                        <textarea class="form-control js_autosize text" name="content_text" rows="6"></textarea>
                    </div>
                </div>
                <div class="form-group content_link">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Link");?>
</label>
                    <div class="col-sm-9">
                        <textarea class="form-control link" name="link" rows="2"></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Picture");?>
</label>
                    <div class="col-sm-6">
                        <input type="file" name="file" id="file"/>
                    </div>
                    
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Show in too development");?>
</label>
                    <div class="col-sm-9">
                        <select name = "is_development" class="form-control">
                            <option value = "1" <?php if ($_smarty_tpl->tpl_vars['data']->value['is_development'] == 1) {?>selected<?php }?>><?php echo __("Yes");?>
</option>
                            <option value = "0" <?php if ($_smarty_tpl->tpl_vars['data']->value['is_development'] == 0) {?>selected<?php }?>><?php echo __("No");?>
</option>
                        </select>
                    </div>
                </div>
                <div class = "form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30"><?php echo __("Save");?>
</button>
                    </div>
                </div>
            </form>
        </div>


    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "edit") {?>
        <div class="panel-body">
            <form class="form-horizontal" enctype="multipart/form-data" method="post" id="create_foetus_development">
                <input type="hidden" name="do" value="edit"/>
                <input type="hidden" name="foetus_info_id" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['foetus_info_id'];?>
"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Type");?>
</label>
                    <div class="col-sm-9">
                        <select name = "type" class="form-control">
                            <option value = "1" <?php if ($_smarty_tpl->tpl_vars['data']->value['type'] == PREGNANCY_CHECK) {?>selected<?php }?>><?php echo __("Pregnancy check");?>
</option>
                            <option value = "2" <?php if ($_smarty_tpl->tpl_vars['data']->value['type'] == FOETUS_INFORMATION) {?>selected<?php }?>><?php echo __("Information");?>
</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Day push notification");?>
 (*)</label>
                    <div class="col-sm-2">
                        <input type = "number" class="form-control" name="day_push" required autofocus max="365" min = "1" value = "<?php echo $_smarty_tpl->tpl_vars['data']->value['day_push'];?>
">
                    </div>
                    <label class="col-sm-2 control-label text-left"><?php echo __("Time");?>
</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control" name="hour" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['time'];?>
">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Notice before");?>
?</label>
                    <div class="col-sm-4">
                        <div class="onoffswitch">
                            <input type="checkbox" name="is_notice_before" class="onoffswitch-checkbox" id="is_notice_before" <?php if ($_smarty_tpl->tpl_vars['data']->value['is_notice_before']) {?>checked<?php }?>>
                            <label class="onoffswitch-label" for="is_notice_before"></label>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <input type="number" class="form-control" name="notice_before_days" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['notice_before_days'];?>
">
                    </div>
                    <span class="control-label inl-block"><?php echo __("day");?>
</span>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Reminder before");?>
?</label>
                    <div class="col-sm-4">
                        <div class="onoffswitch">
                            <input type="checkbox" name="is_reminder_before" class="onoffswitch-checkbox" id="is_reminder_before" <?php if ($_smarty_tpl->tpl_vars['data']->value['is_reminder_before']) {?>checked<?php }?>>
                            <label class="onoffswitch-label" for="is_reminder_before"></label>
                        </div>
                        
                    </div>

                    <div class="col-sm-2">
                        <input type = "number" class="form-control" name="reminder_before_days" value = "<?php echo $_smarty_tpl->tpl_vars['data']->value['reminder_before_days'];?>
">
                    </div>
                    <span class="control-label inl-block"><?php echo __("day");?>
</span>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Title");?>
 (*)</label>
                    <div class="col-sm-9">
                        <input type = "text" class="form-control" name="title" required value = "<?php echo $_smarty_tpl->tpl_vars['data']->value['title'];?>
">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Text or Link?");?>
</label>
                    <div class="col-sm-3">
                        <select name = "content_type" class="form-control" id="content_type_select">
                            <option value = "3" <?php if ($_smarty_tpl->tpl_vars['data']->value['content_type'] == 3) {?>selected<?php }?>><?php echo __("Text and Link");?>
</option>
                            <option value = "1" <?php if ($_smarty_tpl->tpl_vars['data']->value['content_type'] == 1) {?>selected<?php }?>><?php echo __("Text");?>
</option>
                            <option value = "2" <?php if ($_smarty_tpl->tpl_vars['data']->value['content_type'] == 2) {?>selected<?php }?>><?php echo __("Link");?>
</option>
                        </select>
                    </div>
                </div>
                <div class="form-group content_text <?php if ($_smarty_tpl->tpl_vars['data']->value['content_type'] == 2) {?>x-hidden<?php }?>">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Text");?>
</label>
                    <div class="col-sm-9">
                        <textarea class="form-control js_autosize text" name="content_text" rows="6"><?php echo $_smarty_tpl->tpl_vars['data']->value['content'];?>
</textarea>
                    </div>
                </div>
                <div class="form-group content_link <?php if ($_smarty_tpl->tpl_vars['data']->value['content_type'] == 1) {?>x-hidden<?php }?>">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Link");?>
</label>
                    <div class="col-sm-9">
                        <textarea class="form-control link" name="link" rows="2"><?php echo $_smarty_tpl->tpl_vars['data']->value['link'];?>
</textarea>
                    </div>
                </div>

                <input type = "hidden" id="is_file" name="is_file" value = "1">
                <div class="form-group" id="file_old">
                    <label class="rp-file col-sm-3 control-label text-left" style="padding-top: 0px"><?php echo __("Picture");?>
</label>
                    <div class="col-sm-6">
                        <?php if (!is_empty($_smarty_tpl->tpl_vars['data']->value['image_source'])) {?>
                            <a href="<?php echo $_smarty_tpl->tpl_vars['data']->value['image_source'];?>
" target="_blank"><img src="<?php echo $_smarty_tpl->tpl_vars['data']->value['image_source'];?>
" class = "img-responsive"></a>
                            <input type = "hidden" name="old_src" value = "<?php echo $_smarty_tpl->tpl_vars['data']->value['image'];?>
">
                        <?php } else { ?> <?php echo __('No file attachment');?>

                            <input type = "hidden" name="old_src" value = "">
                        <?php }?>
                    </div>
                </div>
                <div class = "form-group">
                    <label class="control-label col-sm-3">
                        <?php if (is_empty($_smarty_tpl->tpl_vars['data']->value['image_source'])) {?>
                            <?php echo __("Choose file");?>

                        <?php } else { ?>
                            <?php echo __("Choose file replace");?>

                        <?php }?>
                    </label>
                    <div class = "col-sm-6">
                        <input type="file" name="file" id="file"/>
                    </div>
                    <a class = "delete_image btn btn-danger btn-xs text-left"><?php echo __('Delete');?>
</a>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Show in too development");?>
</label>
                    <div class="col-sm-9">
                        <select name = "is_development" class="form-control">
                            <option value = "1" <?php if ($_smarty_tpl->tpl_vars['data']->value['is_development'] == 1) {?>selected<?php }?>><?php echo __("Yes");?>
</option>
                            <option value = "0" <?php if ($_smarty_tpl->tpl_vars['data']->value['is_development'] == 0) {?>selected<?php }?>><?php echo __("No");?>
</option>
                        </select>
                    </div>
                </div>
                <div class = "form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30"><?php echo __("Save");?>
</button>
                    </div>
                </div>
                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->
                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "push") {?>
        <div class="panel-body">
            <a href = "#" class="js_push-notify btn btn-default"><?php echo __("Push notify");?>
</a>
        </div>
    <?php }?>
</div><?php }
}
