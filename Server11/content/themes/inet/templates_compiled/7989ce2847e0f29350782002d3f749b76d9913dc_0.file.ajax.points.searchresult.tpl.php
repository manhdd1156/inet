<?php
/* Smarty version 3.1.31, created on 2021-06-25 15:53:44
  from "D:\workplace\Inet-project\source\Server11\content\themes\inet\templates\ci\class\ajax.points.searchresult.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_60d5999858abd1_30557954',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '7989ce2847e0f29350782002d3f749b76d9913dc' => 
    array (
      0 => 'D:\\workplace\\Inet-project\\source\\Server11\\content\\themes\\inet\\templates\\ci\\class\\ajax.points.searchresult.tpl',
      1 => 1624503113,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60d5999858abd1_30557954 (Smarty_Internal_Template $_smarty_tpl) {
if (count($_smarty_tpl->tpl_vars['rows']->value) > 0) {?>
    
    
    
    
    
    
    
    <div class="table-responsive" id="example">

            <?php if ($_smarty_tpl->tpl_vars['search_with']->value == 'search_with_subject') {?>
                <?php if ($_smarty_tpl->tpl_vars['semester']->value == 0) {?>
                    <table class="table table-striped table-bordered table-pinned" style="z-index: 1">
                        <thead>
                        <tr bgcolor="#fff">
                            <th rowspan="2" class="pinned" style="padding: 8px 6px" id="width_no">#</th>
                            <th rowspan="2" class="pinned"
                                style="padding: 8px 6px"><?php echo __("Student name");?>
</th>
                            <th rowspan="2" nowrap="true"
                                style="padding: 8px 6px"><?php echo __("Student code");?>
</th>
                            <th colspan="<?php if ($_smarty_tpl->tpl_vars['score_fomula']->value == 'vn') {
echo $_smarty_tpl->tpl_vars['column_hk1']->value+$_smarty_tpl->tpl_vars['column_gk1']->value+2;
} else { ?>4<?php }?>"><?php echo __("Semester 1");?>
</th>
                            <th colspan="<?php if ($_smarty_tpl->tpl_vars['score_fomula']->value == 'vn') {
echo $_smarty_tpl->tpl_vars['column_hk2']->value+$_smarty_tpl->tpl_vars['column_gk2']->value+2;
} else { ?>4<?php }?>"><?php echo __("Semester 2");?>
</th>
                            <?php if ($_smarty_tpl->tpl_vars['score_fomula']->value == 'vn') {?>
                                <th rowspan="2"><?php echo __("Avg year");?>
</th>
                                <th rowspan="2"><?php echo __("Re exam");?>
</th>
                            <?php } else { ?>
                                <th colspan="2"><?php echo __("Average semesterly");?>
</th>
                                <th colspan="2"><?php echo __("End semester");?>
</th>
                                <th rowspan="2"><?php echo __("End year");?>
</th>
                                <th rowspan="2"><?php echo __("Re exam");?>
</th>
                                <th rowspan="2"><?php echo __("Without permission");?>
</th>
                                <th rowspan="2"><?php echo __("With permission");?>
</th>
                                <th rowspan="2"><?php echo __("Status");?>
</th>
                            <?php }?>
                        </tr>
                        <tr>
                            <?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable(null, $_smarty_tpl->isRenderingCache);
$_smarty_tpl->tpl_vars['i']->value = 1;
if ($_smarty_tpl->tpl_vars['i']->value <= $_smarty_tpl->tpl_vars['column_hk1']->value) {
for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value <= $_smarty_tpl->tpl_vars['column_hk1']->value; $_smarty_tpl->tpl_vars['i']->value++) {
?>
                                <th><?php if ($_smarty_tpl->tpl_vars['score_fomula']->value == 'vn') {?>tx<?php } else { ?>M<?php echo $_smarty_tpl->tpl_vars['i']->value;
}?></th>
                            <?php }
}
?>

                            <?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable(null, $_smarty_tpl->isRenderingCache);
$_smarty_tpl->tpl_vars['i']->value = 0;
if ($_smarty_tpl->tpl_vars['i']->value < $_smarty_tpl->tpl_vars['column_gk1']->value) {
for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value < $_smarty_tpl->tpl_vars['column_gk1']->value; $_smarty_tpl->tpl_vars['i']->value++) {
?>
                                <th><?php if ($_smarty_tpl->tpl_vars['score_fomula']->value == 'vn') {?>gk<?php } else {
echo __("Last");
}?></th>
                            <?php }
}
?>

                            <?php if ($_smarty_tpl->tpl_vars['score_fomula']->value == 'vn') {?>
                                <th>ck</th>
                                <th><?php echo __("Average");?>
</th>
                            <?php }?>
                            <?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable(null, $_smarty_tpl->isRenderingCache);
$_smarty_tpl->tpl_vars['i']->value = 1;
if ($_smarty_tpl->tpl_vars['i']->value <= $_smarty_tpl->tpl_vars['column_hk2']->value) {
for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value <= $_smarty_tpl->tpl_vars['column_hk2']->value; $_smarty_tpl->tpl_vars['i']->value++) {
?>
                                <th><?php if ($_smarty_tpl->tpl_vars['score_fomula']->value == 'vn') {?>tx<?php } else { ?>M<?php echo $_smarty_tpl->tpl_vars['i']->value;
}?></th>
                            <?php }
}
?>

                            <?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable(null, $_smarty_tpl->isRenderingCache);
$_smarty_tpl->tpl_vars['i']->value = 0;
if ($_smarty_tpl->tpl_vars['i']->value < $_smarty_tpl->tpl_vars['column_gk2']->value) {
for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value < $_smarty_tpl->tpl_vars['column_gk2']->value; $_smarty_tpl->tpl_vars['i']->value++) {
?>
                                <th><?php if ($_smarty_tpl->tpl_vars['score_fomula']->value == 'vn') {?>gk<?php } else {
echo __("Last");
}?></th>
                            <?php }
}
?>

                            <?php if ($_smarty_tpl->tpl_vars['score_fomula']->value == 'vn') {?>
                                <th>ck</th>
                                <th><?php echo __("Average");?>
</th>
                            <?php }?>
                            <?php if ($_smarty_tpl->tpl_vars['score_fomula']->value != 'vn') {?>
                                <th>S1</th>
                                <th>S2</th>
                            <?php }?>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $_smarty_tpl->_assignInScope('rowIdx', 1);
?>
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['rows']->value, 'row', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['row']->value) {
?>
                            <tr>
                                <td align="center" class="pinned"><?php echo $_smarty_tpl->tpl_vars['rowIdx']->value;?>
</td>
                                <td nowrap="true" class="pinned text-bold color-blue">
                                    <strong><?php echo $_smarty_tpl->tpl_vars['row']->value['child_lastname'];?>
 <?php echo $_smarty_tpl->tpl_vars['row']->value['child_firstname'];?>
</strong></td>
                                <td nowrap="true" class=" text-bold color-blue"><a
                                            href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/children/detail/<?php echo $_smarty_tpl->tpl_vars['row']->value['child_id'];?>
"><strong><?php echo $_smarty_tpl->tpl_vars['row']->value['child_code'];?>
</strong></a>
                                </td>
                                <?php $_smarty_tpl->_assignInScope('rowIdx', $_smarty_tpl->tpl_vars['rowIdx']->value+1);
?>
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['subject_key']->value, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value) {
?>
                                    <td><?php echo $_smarty_tpl->tpl_vars['row']->value[$_smarty_tpl->tpl_vars['key']->value];?>
</td>
                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                            </tr>
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </tbody>
                    </table>
                <?php } else { ?>
                    <table class="table table-striped table-bordered table-pinned" style="z-index: 1">
                        <thead>
                        <tr bgcolor="#fff">
                            <th rowspan="2" class="pinned" style="padding: 8px 6px" id="width_no">#</th>
                            <th rowspan="2" nowrap="true" class="pinned"
                                style="padding: 8px 6px"><?php echo __("Student name");?>
</th>
                            <th rowspan="2" nowrap="true" class="pinned"
                                style="padding: 8px 6px"><?php echo __("Student code");?>
</th>
                            
                            <th colspan="<?php if ($_smarty_tpl->tpl_vars['score_fomula']->value == 'vn') {
echo $_smarty_tpl->tpl_vars['column_hk']->value+$_smarty_tpl->tpl_vars['column_gk']->value+2;
} else { ?>4<?php }?>"><?php echo __("Semester");?>
</th>
                            
                            
                            
                            
                            
                            
                        </tr>
                        <tr>
                            <?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable(null, $_smarty_tpl->isRenderingCache);
$_smarty_tpl->tpl_vars['i']->value = 1;
if ($_smarty_tpl->tpl_vars['i']->value <= $_smarty_tpl->tpl_vars['column_hk']->value) {
for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value <= $_smarty_tpl->tpl_vars['column_hk']->value; $_smarty_tpl->tpl_vars['i']->value++) {
?>
                                <th><?php if ($_smarty_tpl->tpl_vars['score_fomula']->value == 'vn') {?>tx<?php } else { ?>M<?php echo $_smarty_tpl->tpl_vars['i']->value;
}?></th>
                            <?php }
}
?>

                            <?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable(null, $_smarty_tpl->isRenderingCache);
$_smarty_tpl->tpl_vars['i']->value = 0;
if ($_smarty_tpl->tpl_vars['i']->value < $_smarty_tpl->tpl_vars['column_gk']->value) {
for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value < $_smarty_tpl->tpl_vars['column_gk']->value; $_smarty_tpl->tpl_vars['i']->value++) {
?>
                                <th><?php if ($_smarty_tpl->tpl_vars['score_fomula']->value == 'vn') {?>gk<?php } else {
echo __("Last");
}?></th>
                            <?php }
}
?>

                            <?php if ($_smarty_tpl->tpl_vars['score_fomula']->value == 'vn') {?>
                                <th>ck</th>
                                <th><?php echo __("Average");?>
</th>
                            <?php }?>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $_smarty_tpl->_assignInScope('rowIdx', 1);
?>
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['rows']->value, 'row', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['row']->value) {
?>
                            <tr>
                                <td align="center" class="pinned"><?php echo $_smarty_tpl->tpl_vars['rowIdx']->value;?>
</td>
                                <td nowrap="true" class="pinned text-bold color-blue">
                                    <strong><?php echo $_smarty_tpl->tpl_vars['row']->value['child_lastname'];?>
 <?php echo $_smarty_tpl->tpl_vars['row']->value['child_firstname'];?>
</strong></td>
                                <td nowrap="true" class="pinned text-bold color-blue"><a
                                            href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/children/detail/<?php echo $_smarty_tpl->tpl_vars['row']->value['child_id'];?>
"><strong><?php echo $_smarty_tpl->tpl_vars['row']->value['child_code'];?>
</strong></a>
                                </td>
                                <?php $_smarty_tpl->_assignInScope('rowIdx', $_smarty_tpl->tpl_vars['rowIdx']->value+1);
?>
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['subject_key']->value, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value) {
?>
                                    <td><?php echo $_smarty_tpl->tpl_vars['row']->value[$_smarty_tpl->tpl_vars['key']->value];?>
</td>
                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                            </tr>
                            
                            <?php if ($_smarty_tpl->tpl_vars['rowIdx']->value%10 == 0) {?>
                                <tr bgcolor="#fff">
                                    <th rowspan="2" class="pinned" style="padding: 8px 6px" id="width_no">#</th>
                                    <th rowspan="2" nowrap="true" class="pinned"
                                        style="padding: 8px 6px"><?php echo __("Student name");?>
</th>
                                    <th rowspan="2" nowrap="true" class="pinned"
                                        style="padding: 8px 6px"><?php echo __("Student code");?>
</th>
                                    <th colspan="<?php if ($_smarty_tpl->tpl_vars['score_fomula']->value == 'vn') {
echo $_smarty_tpl->tpl_vars['column_hk']->value+$_smarty_tpl->tpl_vars['column_gk']->value+2;
} else { ?>4<?php }?>"><?php echo __("Semester");?>
</th>
                                </tr>
                                <tr>
                                    <?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable(null, $_smarty_tpl->isRenderingCache);
$_smarty_tpl->tpl_vars['i']->value = 1;
if ($_smarty_tpl->tpl_vars['i']->value <= $_smarty_tpl->tpl_vars['column_hk']->value) {
for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value <= $_smarty_tpl->tpl_vars['column_hk']->value; $_smarty_tpl->tpl_vars['i']->value++) {
?>
                                        <th><?php if ($_smarty_tpl->tpl_vars['score_fomula']->value == 'vn') {?>tx<?php } else { ?>M<?php echo $_smarty_tpl->tpl_vars['i']->value;
}?></th>
                                    <?php }
}
?>

                                    <?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable(null, $_smarty_tpl->isRenderingCache);
$_smarty_tpl->tpl_vars['i']->value = 0;
if ($_smarty_tpl->tpl_vars['i']->value < $_smarty_tpl->tpl_vars['column_gk']->value) {
for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value < $_smarty_tpl->tpl_vars['column_gk']->value; $_smarty_tpl->tpl_vars['i']->value++) {
?>
                                        <th><?php if ($_smarty_tpl->tpl_vars['score_fomula']->value == 'vn') {?>gk<?php } else {
echo __("Last");
}?></th>
                                    <?php }
}
?>

                                    <?php if ($_smarty_tpl->tpl_vars['score_fomula']->value == 'vn') {?>
                                        <th>ck</th>
                                        <th><?php echo __("Average");?>
</th>
                                    <?php }?>
                                </tr>
                            <?php }?>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </tbody>
                    </table>
                <?php }?>
            <?php } elseif ($_smarty_tpl->tpl_vars['search_with']->value == 'search_with_student') {?>
                
                
                
                
                
                <strong style="float: right"><?php echo __("Status ");?>
 :
                    <?php if ($_smarty_tpl->tpl_vars['status']->value == 'Pass') {?>
                        <strong style="color:lawngreen"><?php ob_start();
echo $_smarty_tpl->tpl_vars['status']->value;
$_prefixVariable1=ob_get_clean();
echo __($_prefixVariable1);?>
</strong>
                    <?php } elseif ($_smarty_tpl->tpl_vars['status']->value == 'Fail') {?>
                        <strong style="color:red"><?php ob_start();
echo $_smarty_tpl->tpl_vars['status']->value;
$_prefixVariable2=ob_get_clean();
echo __($_prefixVariable2);?>
</strong>
                    <?php } elseif ($_smarty_tpl->tpl_vars['status']->value == 'Re-exam') {?>
                        <strong style="color:orange"><?php ob_start();
echo $_smarty_tpl->tpl_vars['status']->value;
$_prefixVariable3=ob_get_clean();
echo __($_prefixVariable3);?>
</strong>
                    <?php } else { ?>
                        <strong><?php ob_start();
echo $_smarty_tpl->tpl_vars['status']->value;
$_prefixVariable4=ob_get_clean();
echo __($_prefixVariable4);?>
</strong>
                    <?php }?>

                </strong>
                <table class="table table-striped table-bordered" style="z-index: 1">
                    <thead>
                    <tr bgcolor="#fff">
                        <th rowspan="2" class="pinned" style="padding: 8px 6px" id="width_no">#</th>
                        <th rowspan="2" nowrap="true" class="pinned"
                            style="padding: 8px 6px"><?php echo __("Subject name");?>
</th>
                        <th colspan="<?php if ($_smarty_tpl->tpl_vars['score_fomula']->value == 'vn') {
echo $_smarty_tpl->tpl_vars['column_hk2']->value+$_smarty_tpl->tpl_vars['column_gk2']->value+2;
} else { ?>4<?php }?>"><?php echo __("Semester 1");?>
</th>
                        <th colspan="<?php if ($_smarty_tpl->tpl_vars['score_fomula']->value == 'vn') {
echo $_smarty_tpl->tpl_vars['column_hk2']->value+$_smarty_tpl->tpl_vars['column_gk2']->value+2;
} else { ?>4<?php }?>"><?php echo __("Semester 2");?>
</th>
                    </tr>
                    <tr>
                        <?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable(null, $_smarty_tpl->isRenderingCache);
$_smarty_tpl->tpl_vars['i']->value = 1;
if ($_smarty_tpl->tpl_vars['i']->value <= $_smarty_tpl->tpl_vars['column_hk1']->value) {
for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value <= $_smarty_tpl->tpl_vars['column_hk1']->value; $_smarty_tpl->tpl_vars['i']->value++) {
?>
                            <th><?php if ($_smarty_tpl->tpl_vars['score_fomula']->value == 'vn') {?>tx<?php } else { ?>M<?php echo $_smarty_tpl->tpl_vars['i']->value;
}?></th>
                        <?php }
}
?>

                        <?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable(null, $_smarty_tpl->isRenderingCache);
$_smarty_tpl->tpl_vars['i']->value = 0;
if ($_smarty_tpl->tpl_vars['i']->value < $_smarty_tpl->tpl_vars['column_gk1']->value) {
for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value < $_smarty_tpl->tpl_vars['column_gk1']->value; $_smarty_tpl->tpl_vars['i']->value++) {
?>
                            <th><?php if ($_smarty_tpl->tpl_vars['score_fomula']->value == 'vn') {?>gk<?php } else { ?>D1<?php }?></th>
                        <?php }
}
?>

                        <?php if ($_smarty_tpl->tpl_vars['score_fomula']->value == 'vn') {?>
                            <th>ck</th>
                            <th><?php echo __("Average");?>
</th>
                        <?php }?>
                        <?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable(null, $_smarty_tpl->isRenderingCache);
$_smarty_tpl->tpl_vars['i']->value = 1;
if ($_smarty_tpl->tpl_vars['i']->value <= $_smarty_tpl->tpl_vars['column_hk2']->value) {
for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value <= $_smarty_tpl->tpl_vars['column_hk2']->value; $_smarty_tpl->tpl_vars['i']->value++) {
?>
                            <th><?php if ($_smarty_tpl->tpl_vars['score_fomula']->value == 'vn') {?>tx<?php } else { ?>M<?php echo $_smarty_tpl->tpl_vars['i']->value;
}?></th>
                        <?php }
}
?>

                        <?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable(null, $_smarty_tpl->isRenderingCache);
$_smarty_tpl->tpl_vars['i']->value = 0;
if ($_smarty_tpl->tpl_vars['i']->value < $_smarty_tpl->tpl_vars['column_gk2']->value) {
for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value < $_smarty_tpl->tpl_vars['column_gk2']->value; $_smarty_tpl->tpl_vars['i']->value++) {
?>
                            <th><?php if ($_smarty_tpl->tpl_vars['score_fomula']->value == 'vn') {?>gk<?php } else { ?>D2<?php }?></th>
                        <?php }
}
?>

                        <?php if ($_smarty_tpl->tpl_vars['score_fomula']->value == 'vn') {?>
                            <th>ck</th>
                            <th><?php echo __("Average");?>
</th>
                            <th>Reexam</th>
                        <?php }?>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $_smarty_tpl->_assignInScope('rowIdx', 1);
?>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['rows']->value, 'row', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['row']->value) {
?>
                        <tr>
                            <td align="center" class="pinned"><?php echo $_smarty_tpl->tpl_vars['rowIdx']->value;?>
</td>

                            <td nowrap="true" class="pinned text-bold color-blue">
                                <strong><?php echo $_smarty_tpl->tpl_vars['row']->value['subject_name'];?>
</strong>
                            </td>

                            <?php $_smarty_tpl->_assignInScope('rowIdx', $_smarty_tpl->tpl_vars['rowIdx']->value+1);
?>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['subject_key']->value, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value) {
?>
                                <td><?php echo $_smarty_tpl->tpl_vars['row']->value[strtolower($_smarty_tpl->tpl_vars['key']->value)];?>
</td>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </tr>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                    
                <?php if ($_smarty_tpl->tpl_vars['score_fomula']->value == 'vn') {?>
                    <tr>
                        <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                            <strong><?php echo __("End Semester");?>
</strong>
                        </td>
                        <td colspan="<?php echo $_smarty_tpl->tpl_vars['column_hk1']->value+$_smarty_tpl->tpl_vars['column_gk1']->value+1;?>
"></td>
                        <td><?php echo number_format($_smarty_tpl->tpl_vars['tb_total_hk1']->value,2);?>
</td>
                        <td colspan="<?php echo $_smarty_tpl->tpl_vars['column_hk2']->value+$_smarty_tpl->tpl_vars['column_gk2']->value+1;?>
"></td>
                        <td><?php echo number_format($_smarty_tpl->tpl_vars['tb_total_hk2']->value,2);?>
</td>
                        <td colspan="1"></td>
                    </tr>
                    
                    <tr>
                        <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                            <strong><?php echo __("End year");?>
</strong>
                        </td>
                        <td colspan="<?php echo $_smarty_tpl->tpl_vars['column_hk1']->value+$_smarty_tpl->tpl_vars['column_gk1']->value+2+$_smarty_tpl->tpl_vars['column_hk2']->value+$_smarty_tpl->tpl_vars['column_gk2']->value+2+1;?>
"
                            style="text-align: center"><?php echo number_format($_smarty_tpl->tpl_vars['tb_total_year']->value,2);?>
</td>
                    </tr>
                    
                    <tr>
                        <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                            <strong><?php echo __("Absent with permission");?>
</strong>
                        </td>
                        <td colspan="<?php echo $_smarty_tpl->tpl_vars['column_hk1']->value+$_smarty_tpl->tpl_vars['column_gk1']->value+2+$_smarty_tpl->tpl_vars['column_hk2']->value+$_smarty_tpl->tpl_vars['column_gk2']->value+2+1;?>
"
                            style="text-align: center"><?php echo $_smarty_tpl->tpl_vars['child_absent']->value['absent_true'];?>
</td>
                    </tr>
                    
                    <tr>
                        <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                            <strong><?php echo __("Absent without permission");?>
</strong>
                        </td>
                        <td colspan="<?php echo $_smarty_tpl->tpl_vars['column_hk1']->value+$_smarty_tpl->tpl_vars['column_gk1']->value+2+$_smarty_tpl->tpl_vars['column_hk2']->value+$_smarty_tpl->tpl_vars['column_gk2']->value+2+1;?>
"
                            style="text-align: center"><?php echo $_smarty_tpl->tpl_vars['child_absent']->value['absent_false'];?>
</td>
                    </tr>
                    </tbody>
                    </table>

                <?php } elseif ($_smarty_tpl->tpl_vars['score_fomula']->value == 'km') {?>
                    <tr>
                        <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                            <strong><?php echo __("Average monthly");?>
</strong>
                        </td>
                        <td><?php echo number_format($_smarty_tpl->tpl_vars['children_point_avgs']->value['a1'],2);?>
</td>
                        <td><?php echo number_format($_smarty_tpl->tpl_vars['children_point_avgs']->value['b1'],2);?>
</td>
                        <td><?php echo number_format($_smarty_tpl->tpl_vars['children_point_avgs']->value['c1'],2);?>
</td>
                        <td><?php echo number_format($_smarty_tpl->tpl_vars['children_point_avgs']->value['d1'],2);?>
</td>
                        <td><?php echo number_format($_smarty_tpl->tpl_vars['children_point_avgs']->value['a2'],2);?>
</td>
                        <td><?php echo number_format($_smarty_tpl->tpl_vars['children_point_avgs']->value['b2'],2);?>
</td>
                        <td><?php echo number_format($_smarty_tpl->tpl_vars['children_point_avgs']->value['c2'],2);?>
</td>
                        <td><?php echo number_format($_smarty_tpl->tpl_vars['children_point_avgs']->value['d2'],2);?>
</td>

                    </tr>
                    <tr>
                        <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                            <strong><?php echo __("Average semesterly");?>
</strong>
                        </td>
                        <td colspan="3" style="text-align: center"><?php echo number_format($_smarty_tpl->tpl_vars['children_point_avgs']->value['x1'],2);?>
</td>
                        <td></td>
                        <td colspan="3" style="text-align: center"><?php echo number_format($_smarty_tpl->tpl_vars['children_point_avgs']->value['x2'],2);?>
</td>
                        <td></td>
                    </tr>
                    
                    <tr>
                        <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                            <strong><?php echo __("End semester");?>
</strong>
                        </td>
                        <td colspan="4" style="text-align: center"><?php echo number_format($_smarty_tpl->tpl_vars['children_point_avgs']->value['e1'],2);?>
</td>
                        <td colspan="4" style="text-align: center"><?php echo number_format($_smarty_tpl->tpl_vars['children_point_avgs']->value['e2'],2);?>
</td>
                    </tr>
                    
                    <tr>
                        <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                            <strong><?php echo __("End year");?>
</strong>
                        </td>
                        <td colspan="8" style="text-align: center"><?php echo number_format($_smarty_tpl->tpl_vars['children_point_avgs']->value['y'],2);?>
</td>
                    </tr>
                    
                    <tr>
                        <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                            <strong><?php echo __("Absent has permission");?>
</strong>
                        </td>
                        <td colspan="8" style="text-align: center"><?php echo $_smarty_tpl->tpl_vars['child_absent']->value['absent_true'];?>
</td>
                    </tr>
                    
                    <tr>
                        <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                            <strong><?php echo __("Absent without permission");?>
</strong>
                        </td>
                        <td colspan="8" style="text-align: center"><?php echo $_smarty_tpl->tpl_vars['child_absent']->value['absent_false'];?>
</td>
                    </tr>
                    </tbody>
                    </table>
                    <strong><?php echo __("Re-Exam");?>
</strong>
                    <table class="table table-striped table-bordered" style="z-index: 1;">
                        <thead>
                        <tr bgcolor="#fff">
                            <th rowspan="2" class="pinned" style="padding: 8px 6px" id="width_no">#</th>
                            <th rowspan="2" nowrap="true" class="pinned"
                                style="padding: 8px 6px"><?php echo __("Subject name");?>

                            </th>
                            <th colspan="1"><?php echo __("Point");?>
</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $_smarty_tpl->_assignInScope('rowIdx', 1);
?>
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['children_subject_reexams']->value, 'row', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['row']->value) {
?>
                            <tr>
                                <td align="center" class="pinned"><?php echo $_smarty_tpl->tpl_vars['rowIdx']->value;?>
</td>
                                <td nowrap="true" class="pinned text-bold color-blue">
                                    <strong><?php echo $_smarty_tpl->tpl_vars['row']->value['name'];?>
</strong>
                                </td>
                                <td style="text-align: center"><?php echo $_smarty_tpl->tpl_vars['row']->value['point'];?>
</td>
                                <?php $_smarty_tpl->_assignInScope('rowIdx', $_smarty_tpl->tpl_vars['rowIdx']->value+1);
?>
                            </tr>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        <tr>
                            <td colspan="2" nowrap="true" class="pinned text-bold color-blue"
                                style="text-align: center">
                                <strong><?php echo __("Result Re-exam");?>
</strong>
                            </td>
                            <td colspan="3" style="text-align: center"><?php echo number_format($_smarty_tpl->tpl_vars['result_exam']->value,2);?>
</td>
                        </tr>
                        </tbody>
                    </table>
                <?php }?>
            <?php } elseif ($_smarty_tpl->tpl_vars['search_with']->value == 'showDataImport') {?>
                <?php if ($_smarty_tpl->tpl_vars['semester']->value == 0) {?>
                    <table class="table table-striped table-bordered table-pinned" style="z-index: 1">
                        <thead>
                        <tr bgcolor="#fff">
                            <th rowspan="2" class="pinned" style="padding: 8px 6px" id="width_no">#</th>
                            <th rowspan="2" nowrap="true" class="pinned"
                                style="padding: 8px 6px"><?php echo __("Student name");?>
</th>
                            <th rowspan="2" nowrap="true"
                                style="padding: 8px 6px"><?php echo __("Student code");?>
</th>

                            
                            
                            <th colspan="<?php if ($_smarty_tpl->tpl_vars['score_fomula']->value == 'vn') {
echo $_smarty_tpl->tpl_vars['column_hk1']->value+$_smarty_tpl->tpl_vars['column_gk1']->value+1;
} else { ?>4<?php }?>"><?php echo __("Semester 1");?>
</th>
                            <th colspan="<?php if ($_smarty_tpl->tpl_vars['score_fomula']->value == 'vn') {
echo $_smarty_tpl->tpl_vars['column_hk2']->value+$_smarty_tpl->tpl_vars['column_gk2']->value+1;
} else { ?>4<?php }?>"><?php echo __("Semester 2");?>
</th>
                            <th rowspan="2"><?php echo __("Re exam");?>
</th>
                        </tr>
                        <tr>
                            <?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable(null, $_smarty_tpl->isRenderingCache);
$_smarty_tpl->tpl_vars['i']->value = 1;
if ($_smarty_tpl->tpl_vars['i']->value <= $_smarty_tpl->tpl_vars['column_hk1']->value) {
for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value <= $_smarty_tpl->tpl_vars['column_hk1']->value; $_smarty_tpl->tpl_vars['i']->value++) {
?>
                                <th><?php if ($_smarty_tpl->tpl_vars['score_fomula']->value == 'vn') {?>tx<?php } else { ?>M<?php echo $_smarty_tpl->tpl_vars['i']->value;
}?></th>
                            <?php }
}
?>

                            <?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable(null, $_smarty_tpl->isRenderingCache);
$_smarty_tpl->tpl_vars['i']->value = 0;
if ($_smarty_tpl->tpl_vars['i']->value < $_smarty_tpl->tpl_vars['column_gk1']->value) {
for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value < $_smarty_tpl->tpl_vars['column_gk1']->value; $_smarty_tpl->tpl_vars['i']->value++) {
?>
                                <th><?php if ($_smarty_tpl->tpl_vars['score_fomula']->value == 'vn') {?>gk<?php } else {
echo __("Last");
}?></th>
                            <?php }
}
?>

                            <?php if ($_smarty_tpl->tpl_vars['score_fomula']->value == 'vn') {?>
                                <th>ck</th>
                            <?php }?>
                            <?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable(null, $_smarty_tpl->isRenderingCache);
$_smarty_tpl->tpl_vars['i']->value = 1;
if ($_smarty_tpl->tpl_vars['i']->value <= $_smarty_tpl->tpl_vars['column_hk2']->value) {
for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value <= $_smarty_tpl->tpl_vars['column_hk2']->value; $_smarty_tpl->tpl_vars['i']->value++) {
?>
                                <th><?php if ($_smarty_tpl->tpl_vars['score_fomula']->value == 'vn') {?>tx<?php } else { ?>M<?php echo $_smarty_tpl->tpl_vars['i']->value;
}?></th>
                            <?php }
}
?>

                            <?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable(null, $_smarty_tpl->isRenderingCache);
$_smarty_tpl->tpl_vars['i']->value = 0;
if ($_smarty_tpl->tpl_vars['i']->value < $_smarty_tpl->tpl_vars['column_gk2']->value) {
for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value < $_smarty_tpl->tpl_vars['column_gk2']->value; $_smarty_tpl->tpl_vars['i']->value++) {
?>
                                <th><?php if ($_smarty_tpl->tpl_vars['score_fomula']->value == 'vn') {?>gk<?php } else { ?>D2<?php }?></th>
                            <?php }
}
?>

                            <?php if ($_smarty_tpl->tpl_vars['score_fomula']->value == 'vn') {?>
                                <th>ck</th>
                            <?php }?>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $_smarty_tpl->_assignInScope('rowIdx', 1);
?>
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['rows']->value, 'row', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['row']->value) {
?>
                            <tr>
                                <td align="center" class="pinned"><?php echo $_smarty_tpl->tpl_vars['rowIdx']->value;?>
</td>
                                <td nowrap="true" class="pinned text-bold color-blue">
                                    <strong><?php echo $_smarty_tpl->tpl_vars['row']->value['child_lastname'];?>
 <?php echo $_smarty_tpl->tpl_vars['row']->value['child_firstname'];?>
</strong></td>
                                <td nowrap="true" class="text-bold color-blue"><a
                                            href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/children/detail/<?php echo $_smarty_tpl->tpl_vars['row']->value['child_id'];?>
"><strong><?php echo $_smarty_tpl->tpl_vars['row']->value['child_code'];?>
</strong></a>
                                </td>

                                
                                
                                <?php $_smarty_tpl->_assignInScope('rowIdx', $_smarty_tpl->tpl_vars['rowIdx']->value+1);
?>
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['subject_key']->value, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value) {
?>
                                    
                                    <?php if ($_smarty_tpl->tpl_vars['key']->value == 're_exam' && !$_smarty_tpl->tpl_vars['row']->value['is_reexam']) {?>
                                        <td><?php echo $_smarty_tpl->tpl_vars['row']->value[$_smarty_tpl->tpl_vars['key']->value];?>
</td>
                                    <?php } else { ?>
                                        <td><input name="point" type="number" min="0" step="0.01" style="max-width: 50px"
                                                   value="<?php echo $_smarty_tpl->tpl_vars['row']->value[$_smarty_tpl->tpl_vars['key']->value];?>
"/></td>
                                    <?php }?>
                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                            </tr>
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </tbody>
                    </table>
                <?php } else { ?>
                    <table class="table table-striped table-bordered table-pinned" style="z-index: 1">
                        <thead>
                        <tr bgcolor="#fff">
                            <th rowspan="2" class="pinned" style="padding: 8px 6px" id="width_no">#</th>
                            <th rowspan="2" nowrap="true" class="pinned"
                                style="padding: 8px 6px"><?php echo __("Student name");?>
</th>
                            <th rowspan="2" nowrap="true" class="pinned"
                                style="padding: 8px 6px"><?php echo __("Student code");?>
</th>
                            
                            <th colspan="<?php if ($_smarty_tpl->tpl_vars['score_fomula']->value == 'vn') {
echo $_smarty_tpl->tpl_vars['column_hk1']->value+$_smarty_tpl->tpl_vars['column_gk1']->value+1;
} else { ?>4<?php }?>"><?php echo __("Semester");?>
</th>
                            
                            
                            
                            
                            
                            
                        </tr>
                        <tr>
                            <?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable(null, $_smarty_tpl->isRenderingCache);
$_smarty_tpl->tpl_vars['i']->value = 1;
if ($_smarty_tpl->tpl_vars['i']->value <= $_smarty_tpl->tpl_vars['column_hk1']->value) {
for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value <= $_smarty_tpl->tpl_vars['column_hk1']->value; $_smarty_tpl->tpl_vars['i']->value++) {
?>
                                <th><?php if ($_smarty_tpl->tpl_vars['score_fomula']->value == 'vn') {?>tx<?php } else { ?>M<?php echo $_smarty_tpl->tpl_vars['i']->value;
}?></th>
                            <?php }
}
?>

                            <?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable(null, $_smarty_tpl->isRenderingCache);
$_smarty_tpl->tpl_vars['i']->value = 0;
if ($_smarty_tpl->tpl_vars['i']->value < $_smarty_tpl->tpl_vars['column_gk1']->value) {
for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value < $_smarty_tpl->tpl_vars['column_gk1']->value; $_smarty_tpl->tpl_vars['i']->value++) {
?>
                                <th><?php if ($_smarty_tpl->tpl_vars['score_fomula']->value == 'vn') {?>gk<?php } else {
echo __("Last");
}?></th>
                            <?php }
}
?>

                            <?php if ($_smarty_tpl->tpl_vars['score_fomula']->value == 'vn') {?>
                                <th>ck</th>
                            <?php }?>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $_smarty_tpl->_assignInScope('rowIdx', 1);
?>
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['rows']->value, 'row', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['row']->value) {
?>
                            <tr>
                                <td align="center" class="pinned"><?php echo $_smarty_tpl->tpl_vars['rowIdx']->value;?>
</td>
                                <td nowrap="true" class="pinned text-bold color-blue">
                                    <strong><?php echo $_smarty_tpl->tpl_vars['row']->value['child_lastname'];?>
 <?php echo $_smarty_tpl->tpl_vars['row']->value['child_firstname'];?>
</strong></td>
                                <td nowrap="true" class="pinned text-bold color-blue"><a
                                            href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/children/detail/<?php echo $_smarty_tpl->tpl_vars['row']->value['child_id'];?>
"><strong><?php echo $_smarty_tpl->tpl_vars['row']->value['child_code'];?>
</strong></a>
                                </td>
                                <?php $_smarty_tpl->_assignInScope('rowIdx', $_smarty_tpl->tpl_vars['rowIdx']->value+1);
?>
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['subject_key']->value, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value) {
?>
                                    
                                    <td><input type="number" style="max-width: 50px" min="0" step="0.01" value="<?php echo $_smarty_tpl->tpl_vars['row']->value[$_smarty_tpl->tpl_vars['key']->value];?>
"/></td>
                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                            </tr>
                            
                            <?php if ($_smarty_tpl->tpl_vars['rowIdx']->value%10 == 0) {?>
                                <tr bgcolor="#fff">
                                    <th rowspan="2" class="pinned" style="padding: 8px 6px" id="width_no">#</th>
                                    <th rowspan="2" nowrap="true" class="pinned"
                                        style="padding: 8px 6px"><?php echo __("Student name");?>
</th>
                                    <th rowspan="2" nowrap="true" class="pinned"
                                        style="padding: 8px 6px"><?php echo __("Student code");?>
</th>
                                    
                                    <th colspan="<?php if ($_smarty_tpl->tpl_vars['score_fomula']->value == 'vn') {
echo $_smarty_tpl->tpl_vars['column_hk1']->value+$_smarty_tpl->tpl_vars['column_gk1']->value+1;
} else { ?>4<?php }?>"><?php echo __("Semester");?>
</th>
                                    
                                    
                                    
                                    
                                    
                                    
                                </tr>
                                <tr>
                                    <?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable(null, $_smarty_tpl->isRenderingCache);
$_smarty_tpl->tpl_vars['i']->value = 1;
if ($_smarty_tpl->tpl_vars['i']->value <= $_smarty_tpl->tpl_vars['column_hk1']->value) {
for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value <= $_smarty_tpl->tpl_vars['column_hk1']->value; $_smarty_tpl->tpl_vars['i']->value++) {
?>
                                        <th><?php if ($_smarty_tpl->tpl_vars['score_fomula']->value == 'vn') {?>tx<?php } else { ?>M<?php echo $_smarty_tpl->tpl_vars['i']->value;
}?></th>
                                    <?php }
}
?>

                                    <?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable(null, $_smarty_tpl->isRenderingCache);
$_smarty_tpl->tpl_vars['i']->value = 0;
if ($_smarty_tpl->tpl_vars['i']->value < $_smarty_tpl->tpl_vars['column_gk1']->value) {
for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value < $_smarty_tpl->tpl_vars['column_gk1']->value; $_smarty_tpl->tpl_vars['i']->value++) {
?>
                                        <th><?php if ($_smarty_tpl->tpl_vars['score_fomula']->value == 'vn') {?>gk<?php } else {
echo __("Last");
}?></th>
                                    <?php }
}
?>

                                    <?php if ($_smarty_tpl->tpl_vars['score_fomula']->value == 'vn') {?>
                                        <th>ck</th>
                                    <?php }?>
                                    
                                    
                                    
                                    
                                </tr>
                            <?php }?>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </tbody>
                    </table>
                <?php }?>


















































        <?php }?>
    </div>
<?php } else { ?>
    <div align="center"><strong style="color: red"><?php echo __("Chưa có thông tin điểm");?>
</strong></div>
<?php }?>

<?php echo '<script'; ?>
 type="text/javascript">

    var $table = $('.table-pinned');
    var $fixedColumn = $table.clone().insertBefore($table).addClass('fixed-column');
    $fixedColumn.find('th').each(function (i, elem) {
        $(this).css("width",$table.find('th:eq(' + i + ')')[0].getBoundingClientRect().width);
    });
    $fixedColumn.find('td').each(function (i, elem) {
        $(this).css("width",$table.find('td:eq(' + i + ')')[0].getBoundingClientRect().width);
    });
    $fixedColumn.find('th,td').not('.pinned').hide();
    $fixedColumn.find('[id]').each(function () {
        $(this).removeAttr('id');
    });

    $fixedColumn.find('tr').each(function (i, elem) {
        $(this).css("height",$table.find('tr:eq(' + i + ')')[0].getBoundingClientRect().height);
    });

    $(window).resize(function () {
        $fixedColumn.find('tr').each(function (i, elem) {
            $(this).css("height",$table.find('tr:eq(' + i + ')')[0].getBoundingClientRect().height);
        });
        $fixedColumn.find('td').each(function (i, elem) {
            $(this).addClass('white-space_nowrap');
            $(this).css("width",$table.find('td:eq(' + i + ')')[0].getBoundingClientRect().width);
        });
    });
    //$fixedColumn.find('td').addClass('white-space_nowrap');
    //    $("#right").on("click", function() {
    //        var leftPos = $('#example').scrollLeft();
    //        console.log(leftPos);
    //        $("#example").animate({
    //            scrollLeft: leftPos - 200
    //        }, 800);
    //    });
    $('.right').click(function (event) {
        var width_col = $('.table-pinned').find('td:eq(' + 4 + ')').width();
        var pos = $('#example').scrollLeft() + width_col + 100;
        return false;
        $('#example').scrollLeft(pos);
    });
    $('.left').click(function (event) {
        var width_col = $('.table-pinned').find('td:eq(' + 4 + ')').width();
        var pos = $('#example').scrollLeft() - width_col - 100;
        $('#example').scrollLeft(pos);
    });

    jQuery(function ($) {
        function fixDiv() {
            var $cache = $('#getFixed');
            var $button = $('#table_button');
            if ($(window).scrollTop() > 100) {
                $cache.css({
                    'position': 'fixed',
                    'top': '50px'
                });
                $cache.width($('#class_list_point').width() - 1);
            } else
                $cache.css({
                    'position': 'relative',
                    'top': 'auto'
                });

            if ($(window).scrollTop() > 100)
                $button.css({
                    'position': 'fixed',
                    'top': '90px'
                });
            else
                $button.css({
                    'position': 'relative',
                    'top': 'auto'
                });
        }

        $(window).scroll(fixDiv);
        fixDiv();
    });
<?php echo '</script'; ?>
><?php }
}
