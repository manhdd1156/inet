<?php
/* Smarty version 3.1.31, created on 2021-06-25 10:39:02
  from "D:\workplace\Inet-project\source\Server11\content\themes\inet\templates\_header_new.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_60d54fd64f7f26_51241955',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '0c52716c7e4ddde4d8d5720a31d3bc1e8e056e9b' => 
    array (
      0 => 'D:\\workplace\\Inet-project\\source\\Server11\\content\\themes\\inet\\templates\\_header_new.tpl',
      1 => 1624346405,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60d54fd64f7f26_51241955 (Smarty_Internal_Template $_smarty_tpl) {
?>
<body class="header-fixed" data-chat-enabled="1">
<!-- DELETE START - MANHDD - 25/05/2021 - chưa có app chat trên facebook -->


















<!-- DELETE END - MANHDD - 25/05/2021 - chưa có app chat trên facebook -->
<div class="wrapper">
    <!--=== Header v6 ===-->
    <div class="header-v6 header-classic-white header-sticky header-fixed-shrink">
        <!-- Navbar -->
        <div class="navbar mega-menu" role="navigation">
            <div class="container menu_container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="menu-container">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Navbar Brand -->
                    <div class="navbar-brand">
                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
">
                            <div class="logo-img">
                                <img class="shrink-logo" style="height: 50px;" src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/content/themes/<?php echo $_smarty_tpl->tpl_vars['system']->value['theme'];?>
/images/logo.png" alt="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_title'];?>
 | <?php echo __("School management and connection software");?>
" title="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_title'];?>
 | <?php echo __("School management and connection software");?>
">

                            </div>

                        </a>
                    </div>
                    <!-- ENd Navbar Brand -->

                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse navbar-responsive-collapse">
                    <div class="menu-container">
                        <ul class="nav navbar-nav">
                            <li class="active"><a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
"><?php echo __("Home");?>
</a></li>

                            <?php if ($_smarty_tpl->tpl_vars['system']->value['language']['code'] == 'vi_VN') {?>
                                <li class=""><a href="https://sll.mobiedu.vn/static/huong_dan_su_dung" target="_blank"><?php echo __("Guide");?>
</a></li>
                            <?php } elseif ($_smarty_tpl->tpl_vars['system']->value['language']['code'] == 'en_us') {?>
                            <li class=""><a href="https://sll.mobiedu.vn/static/guide" target="_blank"><?php echo __("Guide");?>
</a></li>
                            <?php }?>
                            
                            
                            


                        </ul>
                    </div>
                </div><!--/navbar-collapse-->
            </div>
        </div>
    </div>
    <!-- End Navbar -->
    <!--=== End Header v6 ===--><?php }
}
