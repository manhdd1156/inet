<?php
/* Smarty version 3.1.31, created on 2021-06-03 14:36:17
  from "D:\workplace\Inet-project\source\Server11\content\themes\inet\templates\ci\school\ajax.school.tuition.4leave.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_60b88671b9e445_11785658',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '1aa101649c7e631ef572d13dfeb83d741a8cb2a1' => 
    array (
      0 => 'D:\\workplace\\Inet-project\\source\\Server11\\content\\themes\\inet\\templates\\ci\\school\\ajax.school.tuition.4leave.tpl',
      1 => 1621501238,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60b88671b9e445_11785658 (Smarty_Internal_Template $_smarty_tpl) {
?>
<table class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <td colspan="6">
            <strong>I. <?php echo __("Fee usage history");?>
&nbsp;(<?php echo __("Number of charged days from last tuition");?>
:&nbsp;<?php echo $_smarty_tpl->tpl_vars['data']->value['attendance_count'];?>
)</strong>
            <input type="hidden" name="attendance_count" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['attendance_count'];?>
"/>
        </td>
    </tr>
    <tr>
        <th>#</th>
        <th><?php echo __("Fee name");?>
</th>
        <th class="text-center"><?php echo __("Unit");?>
</th>
        <th class="text-center"><?php echo __("Quantity");?>
</th>
        <th class="text-right"><?php echo __("Unit price");?>
&nbsp;(<?php echo @constant('MONEY_UNIT');?>
)</th>
        <th class="text-right"><?php echo __("Money amount");?>
&nbsp;(<?php echo @constant('MONEY_UNIT');?>
)</th>
    </tr>
    </thead>
    <tbody>
    
    <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['data']->value['fees'], 'fee');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['fee']->value) {
?>
        <tr>
            <td class="text-center">
                <?php echo $_smarty_tpl->tpl_vars['idx']->value;?>

                <input type="hidden" class="fee_id" name="fee_id[]" value="<?php echo $_smarty_tpl->tpl_vars['fee']->value['fee_id'];?>
"/>
            </td>
            <td><?php echo $_smarty_tpl->tpl_vars['fee']->value['fee_name'];?>
</td>
            <td class="text-center"><?php if ($_smarty_tpl->tpl_vars['fee']->value['fee_type'] == @constant('FEE_TYPE_MONTHLY')) {
echo __('Monthly');
} else {
echo __('Daily');
}?></td>
            <td>
                <input style="width: 50px;" type="number" class="text-center tuition4leave_quantity" id="quantity_<?php echo $_smarty_tpl->tpl_vars['fee']->value['fee_id'];?>
" fee_id="<?php echo $_smarty_tpl->tpl_vars['fee']->value['fee_id'];?>
"
                       name="quantity[]" min="0" value="<?php echo $_smarty_tpl->tpl_vars['fee']->value['quantity'];?>
" step="1"/>
            </td>
            <td><input style="width: 100px;" type="text" class="text-right tuition4leave_unit_price money_tui" id="unit_price_<?php echo $_smarty_tpl->tpl_vars['fee']->value['fee_id'];?>
" fee_id="<?php echo $_smarty_tpl->tpl_vars['fee']->value['fee_id'];?>
"
                       name="unit_price[]" min="0" value="<?php echo $_smarty_tpl->tpl_vars['fee']->value['unit_price'];?>
" step="1"/></td>
            <td><input style="width: 100px;" type="text" class="text-right money_tui" id="money_<?php echo $_smarty_tpl->tpl_vars['fee']->value['fee_id'];?>
" name="money[]" value="<?php echo $_smarty_tpl->tpl_vars['fee']->value['to_money'];?>
" readonly/></td>
        </tr>
        <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>


    
    <tr><td colspan="6" class="text-left"><strong>II. <?php echo __("Service");?>
</strong></td></tr>
    <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['data']->value['services'], 'service');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['service']->value) {
?>
        <tr>
            <td class="text-center">
                <?php echo $_smarty_tpl->tpl_vars['idx']->value;?>

                <input type="hidden" class="service_id" name="service_id[]" value="<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
"/>
            </td>
            <td><?php echo $_smarty_tpl->tpl_vars['service']->value['service_name'];?>
</td>
            <td class="text-center">
                <input type="hidden" name="service_type[]" value="<?php echo $_smarty_tpl->tpl_vars['service']->value['service_type'];?>
"/>
                <?php if ($_smarty_tpl->tpl_vars['service']->value['service_type'] == @constant('SERVICE_TYPE_MONTHLY')) {
echo __('Monthly');
} else {
echo __('Daily');
}?>
            </td>
            <td>
                <input  style="width: 50px;" type="number" class="text-center tuition4leave_service_quantity" id="service_quantity_<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
" service_id="<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
"
                        name="service_quantity[]" min="0" value="<?php echo $_smarty_tpl->tpl_vars['service']->value['quantity'];?>
" step="1"/>
            </td>
            <td><input style="width: 100px;" type="text" class="text-right tuition4leave_service_fee money_tui" id="service_fee_<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
"
                       service_id="<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
" name="service_fee[]" min="0" value="<?php echo $_smarty_tpl->tpl_vars['service']->value['unit_price'];?>
" step="1"/></td>
            <td><input style="width: 100px;" type="text" class="text-right money_tui" id="service_amount_<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
" name="service_amount[]" value="<?php echo $_smarty_tpl->tpl_vars['service']->value['to_money'];?>
" readonly/></td>
        </tr>
        <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>


    
    <tr>
        <td colspan="6"><strong>III. <?php echo __("Count-based service");?>
</strong></td>
    </tr>
    <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['data']->value['count_based_services'], 'service');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['service']->value) {
?>
        <tr>
            <td class="text-center">
                <?php echo $_smarty_tpl->tpl_vars['idx']->value;?>

                <input type="hidden" class="cbservice_id" name="cbservice_id[]" value="<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
"/>
            </td>
            <?php if ($_smarty_tpl->tpl_vars['service']->value['service_id'] > 0) {?>
                <td><?php echo $_smarty_tpl->tpl_vars['service']->value['service_name'];?>
</td>
                <td class="text-center"><?php echo __('Times');?>
</td>
                <td>
                    <input style="width: 50px;" type="number" class="text-center tuition4leave_service_quantity" id="service_quantity_<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
" service_id="<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
"
                           name="cbservice_quantity[]" min="0" value="<?php echo $_smarty_tpl->tpl_vars['service']->value['quantity'];?>
" step="1"/>
                </td>
                <td><input style="width: 100px;" type="text" class="text-right tuition4leave_service_fee money_tui" id="service_fee_<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
"
                           service_id="<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
" name="cbservice_price[]" min="0" value="<?php echo $_smarty_tpl->tpl_vars['service']->value['unit_price'];?>
" step="1"/></td>
                <td><input style="width: 100px;" type="text" class="text-right money_tui" id="service_amount_<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
" name="service_amount[]" value="<?php echo $_smarty_tpl->tpl_vars['service']->value['to_money'];?>
" readonly/></td>
            <?php } else { ?>
                <td colspan="4">
                    <?php echo $_smarty_tpl->tpl_vars['service']->value['service_name'];?>

                    <input type="hidden" id="service_quantity_<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
" service_id="<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
" name="cbservice_quantity[]" value="<?php echo $_smarty_tpl->tpl_vars['service']->value['quantity'];?>
"/>
                    <input type="hidden" id="service_fee_<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
" service_id="<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
" name="cbservice_price[]" value="<?php echo $_smarty_tpl->tpl_vars['service']->value['unit_price'];?>
"/>
                </td>
                <td><input style="width: 100px;" type="text" class="text-right tuition4leave_pick_up money_tui" id="service_amount_<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
" name="service_amount[]" value="<?php echo $_smarty_tpl->tpl_vars['service']->value['to_money'];?>
"/></td>
            <?php }?>
        </tr>
        <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

    <tr>
        <td colspan="5" class="text-right"><strong>IV. <?php echo __("Debt amount of previous month");?>
&nbsp;(<?php echo @constant('MONEY_UNIT');?>
)</strong></td>
        <td><input style="width: 100px;" type='text' name="debt_amount" id="debt_amount" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['debt_amount'];?>
" class="text-right money_tui" readonly/></td>
    </tr>
    <tr>
        <td colspan="5" class="text-right"><strong>V. <?php echo __("Tuition deduction of previous month");?>
&nbsp;(<?php echo @constant('MONEY_UNIT');?>
)</strong></td>
        <td><input style="width: 100px;" type='text' name="total_deduction" id="total_deduction" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['total_deduction'];?>
" class="text-right money_tui" readonly/></td>
    </tr>
    <tr>
        <td colspan="5" class="text-right"><strong>VI. <?php echo __("Paid tuition amount at begining");?>
&nbsp;(<?php echo @constant('MONEY_UNIT');?>
)</strong></td>
        <td><input style="width: 100px;" type='text' name="paid_amount" id="paid_amount" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['paid_amount'];?>
" class="text-right money_tui" readonly/></td>
    </tr>
    <tr>
        <td colspan="5" class="text-right"><strong><?php echo __("Final total");?>
&nbsp;(I + II + III + IV - V - VI)&nbsp;(<?php echo @constant('MONEY_UNIT');?>
)</strong></td>
        <td><input style="width: 100px;" type='text' name="final_amount" id="final_amount" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['final_amount'];?>
" class="text-right money_tui" readonly/></td>
    </tr>
    </tbody>
</table>
<?php }
}
