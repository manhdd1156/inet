<?php
/* Smarty version 3.1.31, created on 2021-06-22 16:21:38
  from "D:\workplace\Inet-project\source\Server11\content\themes\inet\templates\ci\email_templates\create_report.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_60d1aba2be31a5_17888554',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'cf3a7dc15fb958a49c3257b3432cd8b9675a60f7' => 
    array (
      0 => 'D:\\workplace\\Inet-project\\source\\Server11\\content\\themes\\inet\\templates\\ci\\email_templates\\create_report.tpl',
      1 => 1621501238,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60d1aba2be31a5_17888554 (Smarty_Internal_Template $_smarty_tpl) {
?>
Kính gửi ông/bà, <br/>
Phụ huynh bé <?php echo $_smarty_tpl->tpl_vars['child_name']->value;?>
<br/><br/>

<b><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</b><br/><br/>

<?php echo $_smarty_tpl->tpl_vars['content']->value;?>


Thân ái,<br/>
<?php echo $_smarty_tpl->tpl_vars['class_name']->value;?>
.

<?php }
}
