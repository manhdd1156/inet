<?php
/* Smarty version 3.1.31, created on 2021-06-24 16:26:07
  from "D:\workplace\Inet-project\source\Server11\content\themes\inet\templates\ci\class\class.courses.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_60d44faf4ed231_22793149',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'cfd71e53265c16843fa081bfee9ea938eef29973' => 
    array (
      0 => 'D:\\workplace\\Inet-project\\source\\Server11\\content\\themes\\inet\\templates\\ci\\class\\class.courses.tpl',
      1 => 1624525720,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:ci/class/ajax.courselist.tpl' => 1,
  ),
),false)) {
function content_60d44faf4ed231_22793149 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <i class="fa fa-graduation-cap fa-fw fa-lg pr10"></i>
        <?php echo __("Courses");?>

        <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
            &rsaquo; <?php echo __("Lists");?>

        <?php }?>
    </div>
    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
        <div class="panel-body with-table">
            <form class="form-horizontal" action="#" enctype="multipart/form-data" method="post"
                  id="course_search_form">
                <input type="hidden" name="username" id="username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                <input type="hidden" name="do" value="search_course"/>
                <div class = "table-responsive" id ="course_list">
                    <?php $_smarty_tpl->_subTemplateRender("file:ci/class/ajax.courselist.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                </div>

                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
        </div>
    <?php }?>
</div>
<?php }
}
