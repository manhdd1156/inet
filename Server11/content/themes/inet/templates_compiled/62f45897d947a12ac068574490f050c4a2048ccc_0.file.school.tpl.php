<?php
/* Smarty version 3.1.31, created on 2021-05-08 16:12:03
  from "D:\workplace\mascom-edu-server\Server11\content\themes\inet\templates\school.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_609655e328ea84_60059115',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '62f45897d947a12ac068574490f050c4a2048ccc' => 
    array (
      0 => 'D:\\workplace\\mascom-edu-server\\Server11\\content\\themes\\inet\\templates\\school.tpl',
      1 => 1620465120,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:_head.tpl' => 1,
    'file:_header.tpl' => 1,
    'file:ci/school/school.steps.tpl' => 1,
    'file:ci/school/school.help.tpl' => 1,
    'file:ci/school/school.dashboard.tpl' => 1,
    'file:ci/school/school.".((string)$_smarty_tpl->tpl_vars[\'view\']->value).".tpl' => 1,
    'file:ci/school/school.faq.tpl' => 1,
    'file:ci/school/school.direct.tpl' => 1,
    'file:_footer.tpl' => 1,
  ),
),false)) {
function content_609655e328ea84_60059115 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:_head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php $_smarty_tpl->_subTemplateRender('file:_header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php $_smarty_tpl->_assignInScope('schoolUser', getSchool($_smarty_tpl->tpl_vars['username']->value));
?>
<!-- page content -->
<div class="container mt20 height_min" id="container_width"
     <?php if ((($_smarty_tpl->tpl_vars['view']->value == 'pickup' && $_smarty_tpl->tpl_vars['sub_view']->value == 'assign'))) {?>style="width: 100%!important; padding-left: 50px"<?php }?>>
    <div class="row">
        <?php if ($_smarty_tpl->tpl_vars['school']->value['school_step'] == SCHOOL_STEP_FINISH || $_smarty_tpl->tpl_vars['school']->value['grade'] != 0) {?>
            <div class="menu_fixed <?php if (($_smarty_tpl->tpl_vars['status']->value)) {?>hidden<?php }?>">
                <div class="panel-body with-nav">
                    <ul class="side-nav nav navbar-nav navbar-right">
                        <?php if ($_smarty_tpl->tpl_vars['schoolUser']->value['is_teacher'] != 1) {?>
                            <!-- Dashboard -->
                            <li <?php if ($_smarty_tpl->tpl_vars['view']->value == '') {?>class="active selected"<?php }?>>
                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
">
                                    <i class="fas fa-tachometer-alt fa-fw fa-lg pr10"></i>
                                </a>
                            </li>
                            <!-- Dashboard -->
                        <?php }?>
                        <!-- Attendance -->
                        <?php if ($_smarty_tpl->tpl_vars['school']->value['attendance']) {?>
                            <?php if (canView($_smarty_tpl->tpl_vars['username']->value,"attendance")) {?>
                                <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "attendance") {?>class="active selected"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/attendance">
                                        <i class="fa fa-tasks fa-fw fa-lg pr10"></i>
                                    </a>

                                    <ul>
                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "attendance" && $_smarty_tpl->tpl_vars['sub_view']->value == '') {?>class="active selected"<?php }?>>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/attendance">
                                                <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Whole school");?>

                                            </a>
                                        </li>
                                        <?php if (canEdit($_smarty_tpl->tpl_vars['username']->value,"attendance")) {?>
                                            <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "attendance" && $_smarty_tpl->tpl_vars['sub_view']->value == "rollup") {?>class="active selected"<?php }?>>
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/attendance/rollup">
                                                    <i class="fa fa-caret-right fa-fw pr10"></i> <?php echo __("Roll up");?>

                                                </a>
                                            </li>
                                        <?php }?>
                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "attendance" && $_smarty_tpl->tpl_vars['sub_view']->value == "classatt") {?>class="active selected"<?php }?>>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/attendance/classatt">
                                                <i class="fa fa-caret-right fa-fw pr10"></i> <?php echo __("Class attendance summary");?>

                                            </a>
                                        </li>
                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "attendance" && $_smarty_tpl->tpl_vars['sub_view']->value == "child") {?>class="active selected"<?php }?>>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/attendance/child">
                                                <i class="fa fa-caret-right fa-fw pr10"></i> <?php echo __("Student attendance summary");?>

                                            </a>
                                        </li>
                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "attendance" && $_smarty_tpl->tpl_vars['sub_view']->value == "conabs") {?>class="active selected"<?php }?>>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/attendance/conabs">
                                                <i class="fa fa-caret-right fa-fw pr10"></i> <?php echo __("Consecutive absent student");?>

                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            <?php }?>
                        <?php }?>
                        <!-- Attendance -->

                        <!-- Service -->
                        <?php if ($_smarty_tpl->tpl_vars['school']->value['services']) {?>
                            <?php if (canView($_smarty_tpl->tpl_vars['username']->value,"services")) {?>
                                <li <?php if (($_smarty_tpl->tpl_vars['view']->value == "useservices") || ($_smarty_tpl->tpl_vars['view']->value == "services")) {?>class="active"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/useservices">
                                        <i class="fa fa-life-ring fa-lg fa-fw pr10"></i>
                                    </a>
                                    <ul>
                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "services" && $_smarty_tpl->tpl_vars['sub_view']->value == '') {?>class="active selected"<?php }?>>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/services">
                                                <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Service list");?>

                                            </a>
                                        </li>
                                        <?php if (canEdit($_smarty_tpl->tpl_vars['username']->value,"services")) {?>
                                            <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "useservices" && $_smarty_tpl->tpl_vars['sub_view']->value == "reg") {?>class="active selected"<?php }?>>
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/useservices/reg">
                                                    <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Register service");?>

                                                </a>
                                            </li>
                                        <?php }?>
                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "useservices" && $_smarty_tpl->tpl_vars['sub_view']->value == "history") {?>class="active selected"<?php }?>>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/useservices/history">
                                                <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Service usage information");?>

                                            </a>
                                        </li>
                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "useservices" && $_smarty_tpl->tpl_vars['sub_view']->value == "foodsvr") {?>class="active selected"<?php }?>>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/useservices/foodsvr">
                                                <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Daily food service summary");?>

                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            <?php }?>
                        <?php }?>
                        <!-- Service ->

                        <!-- Late Pickup -->
                        <?php if ($_smarty_tpl->tpl_vars['school']->value['pickup']) {?>
                            <?php if (canView($_smarty_tpl->tpl_vars['username']->value,"pickup")) {?>
                                <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "pickup") {?>class="active"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/pickup/list">
                                        <i class="fa fa-universal-access fa-lg fa-fw pr10"></i>
                                    </a>
                                    <ul>
                                        <?php if (canEdit($_smarty_tpl->tpl_vars['username']->value,"pickup")) {?>
                                            <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "pickup" && $_smarty_tpl->tpl_vars['sub_view']->value == "template") {?>class="active selected"<?php }?>>
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/pickup/template">
                                                    <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Setting and create price table");?>

                                                </a>
                                            </li>
                                            <?php if ($_smarty_tpl->tpl_vars['pickup_is_configured']->value) {?>
                                                <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "pickup" && $_smarty_tpl->tpl_vars['sub_view']->value == "assign") {?>class="active selected"<?php }?>>
                                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/pickup/assign">
                                                        <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Assign teacher");?>

                                                    </a>
                                                </li>
                                            <?php }?>
                                        <?php }?>
                                        <?php if ($_smarty_tpl->tpl_vars['pickup_is_configured']->value) {?>
                                            <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "pickup" && $_smarty_tpl->tpl_vars['sub_view']->value == "history") {?>class="active selected"<?php }?>>
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/pickup/history">
                                                    <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Assignment history");?>

                                                </a>
                                            </li>
                                            <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "pickup" && $_smarty_tpl->tpl_vars['sub_view']->value == "list") {?>class="active selected"<?php }?>>
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/pickup/list">
                                                    <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Lists");?>

                                                </a>
                                            </li>
                                        <?php }?>
                                    </ul>
                                </li>
                            <?php }?>
                        <?php }?>

                        <?php if ($_smarty_tpl->tpl_vars['school']->value['pickupteacher']) {?>
                            <?php if ($_smarty_tpl->tpl_vars['pickup_is_configured']->value && $_smarty_tpl->tpl_vars['pickup_is_teacher']->value) {?>
                                <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "pickupteacher") {?>class="active"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/pickupteacher/history">
                                        <i class="fa fa-universal-access fa-lg fa-fw pr10"></i>
                                    </a>
                                    <ul>
                                        <?php if ($_smarty_tpl->tpl_vars['pickup_is_assigned']->value) {?>
                                            <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "pickup" && $_smarty_tpl->tpl_vars['sub_view']->value == "manage") {?>class="active selected"<?php }?>>
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/pickupteacher/manage">
                                                    <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Add students - Pickup");?>

                                                </a>
                                            </li>
                                        <?php }?>
                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "pickup" && $_smarty_tpl->tpl_vars['sub_view']->value == "history") {?>class="active selected"<?php }?>>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/pickupteacher/history">
                                                <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("History");?>

                                            </a>
                                        </li>
                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "pickup" && $_smarty_tpl->tpl_vars['sub_view']->value == "assign") {?>class="active selected"<?php }?>>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/pickupteacher/assign">
                                                <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Schedule assignment");?>

                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            <?php }?>
                        <?php }?>
                        <!-- END Late Pickup -->

                        <!-- Fee -->
                        <?php if ($_smarty_tpl->tpl_vars['school']->value['tuitions']) {?>
                            <?php if (canView($_smarty_tpl->tpl_vars['username']->value,"tuitions")) {?>
                                <li <?php if (($_smarty_tpl->tpl_vars['view']->value == "tuitions") || ($_smarty_tpl->tpl_vars['view']->value == "fees")) {?>class="active"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/tuitions">
                                        <i class="fas fa-money-bill-alt fa-lg fa-fw pr10"></i>
                                    </a>
                                    <ul>
                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "tuitions" && $_smarty_tpl->tpl_vars['sub_view']->value == '') {?>class="active selected"<?php }?>>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/tuitions">
                                                <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Monthly tuition list");?>

                                            </a>
                                        </li>
                                        <?php if (canEdit($_smarty_tpl->tpl_vars['username']->value,"tuitions")) {?>
                                            <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "tuitions" && $_smarty_tpl->tpl_vars['sub_view']->value == "add") {?> class="active selected" <?php }?>>
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/tuitions/add">
                                                    <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Add monthly tuition");?>

                                                </a>
                                            </li>
                                        <?php }?>
                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "fees" && $_smarty_tpl->tpl_vars['sub_view']->value == '') {?>class="active selected"<?php }?>>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/fees">
                                                <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Fee list");?>

                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            <?php }?>
                        <?php }?>
                        <!-- Fee -->

                        <!-- Event -->
                        <?php if ($_smarty_tpl->tpl_vars['school']->value['events']) {?>
                            
                            <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "events") {?>class="active"<?php }?>>
                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/events">
                                    <i class="fa fa-bell fa-lg fa-fw pr10"></i>
                                </a>
                                <?php if (canEdit($_smarty_tpl->tpl_vars['username']->value,"events")) {?>
                                    <ul>
                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "events" && $_smarty_tpl->tpl_vars['sub_view']->value == '') {?>class="active selected"<?php }?>>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/events">
                                                <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Lists");?>

                                            </a>
                                        </li>
                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "events" && $_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>class="active selected"<?php }?>>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/events/add">
                                                <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Add New");?>

                                            </a>
                                        </li>
                                    </ul>
                                <?php }?>
                            </li>
                            
                        <?php }?>
                        <!-- Event -->

                        <!-- Schedule -->
                        <?php if ($_smarty_tpl->tpl_vars['school']->value['schedules']) {?>
                            <?php if (canView($_smarty_tpl->tpl_vars['username']->value,"schedules")) {?>
                                <li <?php if (($_smarty_tpl->tpl_vars['view']->value == "schedules")) {?>class="active"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/schedules">
                                        <i class="fas fa-calendar-alt fa-lg fa-fw pr10"></i>
                                    </a>
                                    <?php if (canEdit($_smarty_tpl->tpl_vars['username']->value,"schedules")) {?>
                                        <ul>
                                            <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "schedules" && $_smarty_tpl->tpl_vars['sub_view']->value == '') {?>class="active selected"<?php }?>>
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/schedules">
                                                    <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Schedule list");?>

                                                </a>
                                            </li>
                                            <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "schedules" && $_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>class="active selected"<?php }?>>
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/schedules/add">
                                                    <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Add schedule");?>

                                                </a>
                                            </li>
                                            <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "schedules" && $_smarty_tpl->tpl_vars['sub_view']->value == "import") {?>class="active selected"<?php }?>>
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/schedules/import">
                                                    <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Import from Excel file");?>

                                                </a>
                                            </li>
                                        </ul>
                                    <?php }?>
                                </li>
                            <?php }?>
                        <?php }?>
                        <!-- End schedule -->

                        <!-- MENU -->
                        <?php if ($_smarty_tpl->tpl_vars['school']->value['menus']) {?>
                            <?php if (canView($_smarty_tpl->tpl_vars['username']->value,"menus")) {?>
                                <li <?php if (($_smarty_tpl->tpl_vars['view']->value == "menus")) {?>class="active"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/menus">
                                        <i class="fas fa-utensils fa-lg fa-fw pr10"></i>
                                    </a>
                                    <?php if (canEdit($_smarty_tpl->tpl_vars['username']->value,"menus")) {?>
                                        <ul>
                                            <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "menus" && $_smarty_tpl->tpl_vars['sub_view']->value == '') {?>class="active selected"<?php }?>>
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/menus">
                                                    <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Menu list");?>

                                                </a>
                                            </li>
                                            <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "menus" && $_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>class="active selected"<?php }?>>
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/menus/add">
                                                    <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Add menu");?>

                                                </a>
                                            </li>
                                        </ul>
                                    <?php }?>
                                </li>
                            <?php }?>
                        <?php }?>
                        <!-- End menu -->

                        <!-- Contact book -->
                        <?php if ($_smarty_tpl->tpl_vars['school']->value['reports']) {?>
                            <?php if (canView($_smarty_tpl->tpl_vars['username']->value,"reports")) {?>
                                <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "reports") {?>class="active"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/reports">
                                        <i class="fa fa-book fa-lg fa-fw pr10"></i>
                                    </a>
                                    <ul>
                                        <li <?php if (($_smarty_tpl->tpl_vars['view']->value == "reports") && ($_smarty_tpl->tpl_vars['sub_view']->value == '')) {?>class="active selected"<?php }?>>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/reports">
                                                <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Lists");?>

                                            </a>
                                        </li>
                                        <?php if (canEdit($_smarty_tpl->tpl_vars['username']->value,"reports")) {?>
                                            <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "reports" && $_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>class="active selected"<?php }?>>
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/reports/add">
                                                    <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Add New");?>

                                                </a>
                                            </li>
                                        <?php }?>
                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "reports" && $_smarty_tpl->tpl_vars['sub_view']->value == "listtemp") {?>class="active selected"<?php }?>>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/reports/listtemp">
                                                <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("List template");?>

                                            </a>
                                        </li>
                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "reports" && $_smarty_tpl->tpl_vars['sub_view']->value == "listcategory") {?>class="active selected"<?php }?>>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/reports/listcate">
                                                <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("List category");?>

                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            <?php }?>
                        <?php }?>
                        <!-- End Contact book -->
                        <!-- Point -->
                        <?php if ($_smarty_tpl->tpl_vars['school']->value['points']) {?>
                            <?php if (canView($_smarty_tpl->tpl_vars['username']->value,"points")) {?>
                                <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "points") {?>class="active"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/points">
                                        <i class="fa fa-book fa-lg fa-fw pr10"></i>
                                    </a>
                                    <ul>
                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "points" && $_smarty_tpl->tpl_vars['sub_view']->value == "listsbysubject") {?>class="active selected"<?php }?>>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/points/listsbysubject">
                                                <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Lists by subject");?>

                                            </a>
                                        </li>

                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "points" && $_smarty_tpl->tpl_vars['sub_view']->value == "listsbystudent") {?>class="active selected"<?php }?>>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/points/listsbystudent">
                                                <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Lists by student");?>

                                            </a>
                                        </li>
                                        <?php if (canEdit($_smarty_tpl->tpl_vars['username']->value,"points")) {?>
                                            <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "points" && $_smarty_tpl->tpl_vars['sub_view']->value == "importManual") {?>class="active selected"<?php }?>>
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/points/importManual">
                                                    <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Manual Import");?>

                                                </a>
                                            </li>
                                            <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "points" && $_smarty_tpl->tpl_vars['sub_view']->value == "importExcel") {?>class="active selected"<?php }?>>
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/points/importExcel">
                                                    <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Import from Excel file");?>

                                                </a>
                                            </li>
                                            <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "points" && $_smarty_tpl->tpl_vars['sub_view']->value == "assign") {?>class="active selected"<?php }?>>
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/points/assign">
                                                    <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Manual Import");?>

                                                </a>
                                            </li>
                                            <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "points" && $_smarty_tpl->tpl_vars['sub_view']->value == "comment") {?>class="active selected"<?php }?>>
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/points/comment">
                                                    <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("comment");?>

                                                </a>
                                            </li>
                                        <?php }?>
                                    </ul>
                                </li>
                            <?php }?>
                        <?php }?>
                        <!-- End Point -->
                        <!-- Medicine -->
                        <?php if ($_smarty_tpl->tpl_vars['school']->value['medicines']) {?>
                            <?php if (canView($_smarty_tpl->tpl_vars['username']->value,"medicines")) {?>
                                <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "medicines") {?>class="active"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/medicines">
                                        <i class="fa fa-medkit fa-lg fa-fw pr10"></i>
                                    </a>
                                    <ul>
                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "medicines" && $_smarty_tpl->tpl_vars['sub_view']->value == '') {?>class="active selected"<?php }?>>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/medicines">
                                                <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Today lists");?>

                                            </a>
                                        </li>
                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "medicines" && $_smarty_tpl->tpl_vars['sub_view']->value == "all") {?>class="active selected"<?php }?>>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/medicines/all">
                                                <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("List all");?>

                                            </a>
                                        </li>
                                        <?php if (canEdit($_smarty_tpl->tpl_vars['username']->value,"medicines")) {?>
                                            <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "medicines" && $_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>class="active selected"<?php }?>>
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/medicines/add">
                                                    <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Add New");?>

                                                </a>
                                            </li>
                                        <?php }?>
                                    </ul>
                                </li>
                            <?php }?>
                        <?php }?>
                        <!-- Medicine -->

                        <!-- Feedback -->
                        <?php if ($_smarty_tpl->tpl_vars['school']->value['feedback']) {?>
                            <?php if (canView($_smarty_tpl->tpl_vars['username']->value,"feedback")) {?>
                                <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "feedback") {?>class="active selected"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/feedback">
                                        <i class="fa fa-envelope fa-fw fa-lg pr10"></i>
                                    </a>
                                </li>
                            <?php }?>
                        <?php }?>
                        <!-- Feedback -->

                        <!-- Birthday -->
                        <?php if ($_smarty_tpl->tpl_vars['chool']->value['birthdays']) {?>
                            <?php if (canView($_smarty_tpl->tpl_vars['username']->value,"birthdays")) {?>
                                <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "birthdays") {?>class="active"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/birthdays">
                                        <i class="fa fa-birthday-cake fa-fw fa-lg pr10"></i>
                                    </a>
                                </li>
                            <?php }?>
                        <?php }?>
                        <!-- End Birthday -->

                        <!-- Diary -->
                        <?php if ($_smarty_tpl->tpl_vars['school']->value['diarys']) {?>
                            <?php if (canView($_smarty_tpl->tpl_vars['username']->value,"diarys")) {?>
                                <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "diarys") {?>class="active"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/diarys">
                                        <i class="fa fa-medkit fa-lg fa-fw pr10"></i>
                                    </a>
                                    <ul>
                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "diarys" && $_smarty_tpl->tpl_vars['sub_view']->value == '') {?>class="active selected"<?php }?>>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/diarys">
                                                <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Diary corner");?>

                                            </a>
                                        </li>
                                        <?php if (canEdit($_smarty_tpl->tpl_vars['username']->value,"diarys")) {?>
                                            <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "diarys" && $_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>class="active selected"<?php }?>>
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/diarys/add">
                                                    <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Add New");?>

                                                </a>
                                            </li>
                                        <?php }?>
                                    </ul>
                                </li>
                            <?php }?>
                        <?php }?>
                        <!-- End Diary -->

                        <!-- Health -->
                        <?php if ($_smarty_tpl->tpl_vars['school']->value['healths']) {?>
                            <?php if (canView($_smarty_tpl->tpl_vars['username']->value,"healths")) {?>
                                <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "healths") {?>class="active"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/healths">
                                        <i class="fa fa-medkit fa-lg fa-fw pr10"></i>
                                    </a>
                                    <ul>
                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "healths" && $_smarty_tpl->tpl_vars['sub_view']->value == '') {?>class="active selected"<?php }?>>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/healths">
                                                <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Health information");?>

                                            </a>
                                        </li>
                                        <?php if (canEdit($_smarty_tpl->tpl_vars['username']->value,"healths")) {?>
                                            <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "healths" && $_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>class="active selected"<?php }?>>
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/healths/add">
                                                    <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Add New");?>

                                                </a>
                                            </li>
                                        <?php }?>
                                    </ul>
                                </li>
                            <?php }?>
                        <?php }?>
                        <!-- End Healths -->

                        <!-- Classes -->
                        <?php if ($_smarty_tpl->tpl_vars['school']->value['classes'] || $_smarty_tpl->tpl_vars['school']->value['classlevels']) {?>
                            <?php if (canView($_smarty_tpl->tpl_vars['username']->value,"classes") || canView($_smarty_tpl->tpl_vars['username']->value,"classlevels")) {?>
                                <li <?php if (($_smarty_tpl->tpl_vars['view']->value == "classes" || $_smarty_tpl->tpl_vars['view']->value == "classlevels")) {?>class="active"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/classes">
                                        <i class="fa fa-graduation-cap fa-fw fa-lg pr10"></i>
                                    </a>
                                    <?php if (canView($_smarty_tpl->tpl_vars['username']->value,"classes") || canView($_smarty_tpl->tpl_vars['username']->value,"classlevels")) {?>
                                        <ul>
                                            <?php if ($_smarty_tpl->tpl_vars['school']->value['classlevels']) {?>
                                                <?php if (canView($_smarty_tpl->tpl_vars['username']->value,"classlevels")) {?>
                                                    <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "classlevels") {?>class="active selected"<?php }?>>
                                                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/classlevels">
                                                            <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Class level");?>

                                                        </a>
                                                    </li>
                                                <?php }?>
                                            <?php }?>
                                            <?php if ($_smarty_tpl->tpl_vars['school']->value['classes']) {?>
                                                <?php if (canView($_smarty_tpl->tpl_vars['username']->value,"classes")) {?>
                                                    <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "classes" && $_smarty_tpl->tpl_vars['sub_view']->value == '') {?>class="active selected"<?php }?>>
                                                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/classes">
                                                            <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Class lists");?>

                                                        </a>
                                                    </li>
                                                <?php }?>
                                                <?php if (canEdit($_smarty_tpl->tpl_vars['username']->value,"classes")) {?>
                                                    <li <?php if (($_smarty_tpl->tpl_vars['view']->value == "classes") && ($_smarty_tpl->tpl_vars['sub_view']->value == "add")) {?>class="active selected"<?php }?>>
                                                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/classes/add">
                                                            <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Add new class");?>

                                                        </a>
                                                    </li>
                                                <?php }?>
                                            <?php }?>
                                        </ul>
                                    <?php }?>
                                </li>
                            <?php }?>
                        <?php }?>
                        <!-- Classes -->
                        <!-- Subject -->
                        <?php if ($_smarty_tpl->tpl_vars['school']->value['subjects']) {?>
                            <?php if (canView($_smarty_tpl->tpl_vars['username']->value,"subjects")) {?>
                                <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "subjects") {?>class="active"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/subjects">
                                        <i class="fa fa-book fa-lg fa-fw pr10"></i>
                                    </a>
                                    <ul>
                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "subjects" && $_smarty_tpl->tpl_vars['sub_view']->value == '') {?>class="active selected"<?php }?>>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/subjects">
                                                <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Lists");?>

                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            <?php }?>
                        <?php }?>
                        <!-- End Subject -->

                        <!-- Teachers/Employee -->
                        <?php if ($_smarty_tpl->tpl_vars['school']->value['teachers']) {?>
                            <?php if (canView($_smarty_tpl->tpl_vars['username']->value,"teachers")) {?>
                                <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "teachers") {?>class="active"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/teachers">
                                        <i class="fa fa-street-view fa-fw fa-lg pr10"></i>
                                    </a>
                                    <?php if (canEdit($_smarty_tpl->tpl_vars['username']->value,"teachers")) {?>
                                        <ul>
                                            <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "teachers" && $_smarty_tpl->tpl_vars['sub_view']->value == '') {?>class="active selected"<?php }?>>
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/teachers">
                                                    <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Lists");?>

                                                </a>
                                            </li>
                                            <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "teachers" && $_smarty_tpl->tpl_vars['sub_view']->value == "addexisting") {?>class="active selected"<?php }?>>
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/teachers/addexisting">
                                                    <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Add Existing Account");?>

                                                </a>
                                            </li>
                                            <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "teachers" && $_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>class="active selected"<?php }?>>
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/teachers/add">
                                                    <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Add New");?>

                                                </a>
                                            </li>
                                        </ul>
                                    <?php }?>
                                </li>
                            <?php }?>
                        <?php }?>
                        <!-- Teachers -->

                        <!-- BEGIN - Children -->
                        <?php if ($_smarty_tpl->tpl_vars['school']->value['children']) {?>
                            <?php if (canView($_smarty_tpl->tpl_vars['username']->value,"children")) {?>
                                <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "children") {?>class="active"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/children">
                                        <i class="fa fa-child fa-fw fa-lg pr10"></i>
                                    </a>
                                    <?php if (canEdit($_smarty_tpl->tpl_vars['username']->value,"children")) {?>
                                        <ul>
                                            <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "children" && $_smarty_tpl->tpl_vars['sub_view']->value == '') {?>class="active selected"<?php }?>>
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/children">
                                                    <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Lists");?>

                                                </a>
                                            </li>
                                            <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "children" && $_smarty_tpl->tpl_vars['sub_view']->value == '') {?>class="active selected"<?php }?>>
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/children/listchildnew">
                                                    <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("New student study begin in month");?>

                                                </a>
                                            </li>
                                            <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "children" && $_smarty_tpl->tpl_vars['sub_view']->value == "import") {?>class="active selected"<?php }?>>
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/children/import">
                                                    <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Import from Excel file");?>

                                                </a>
                                            </li>
                                            <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "children" && $_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>class="active selected"<?php }?>>
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/children/add">
                                                    <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Add New");?>

                                                </a>
                                            </li>
                                        </ul>
                                    <?php }?>
                                </li>
                            <?php }?>
                        <?php }?>
                        <!-- END - Children -->

                        <!-- Permission -->
                        <?php if ($_smarty_tpl->tpl_vars['school']->value['roles']) {?>
                            <?php if (canView($_smarty_tpl->tpl_vars['username']->value,"roles")) {?>
                                <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "roles") {?>class="active"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/roles">
                                        <i class="fa fa-unlock-alt fa-fw fa-lg pr10"></i>
                                    </a>
                                </li>
                            <?php }?>
                        <?php }?>
                        <!-- End permission -->

                        <!-- Settings -->
                        <?php if ($_smarty_tpl->tpl_vars['school']->value['settings']) {?>
                            <?php if (canView($_smarty_tpl->tpl_vars['username']->value,"settings")) {?>
                                <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "settings") {?>class="active selected"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/settings">
                                        <i class="fa fa-cog fa-fw fa-lg pr10"></i>
                                    </a>
                                </li>
                            <?php }?>
                        <?php }?>
                        <!-- End Setting -->

                        <!-- Ẩn/ hiển thị menu -->
                        <li>
                            <input type="hidden" id="full_screen"
                                   value="<?php if ($_smarty_tpl->tpl_vars['view']->value == 'attendance' && $_smarty_tpl->tpl_vars['sub_view']->value == 'classatt') {?>1<?php } else { ?> 0<?php }?>">
                            <a href="#" class="js_school_hidden" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"> <i
                                        class="fa fa-bars fa-fw pr10 fa-lg"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="<?php if (($_smarty_tpl->tpl_vars['view']->value == "pickup" && $_smarty_tpl->tpl_vars['sub_view']->value == 'assign')) {?>col-md-2 col-sm-2 <?php } else { ?>col-md-3 col-sm-3<?php }?> <?php if (!$_smarty_tpl->tpl_vars['status']->value) {?>hidden<?php }?>"
                 id="menu_left">
                <div class="js_scroller" data-slimscroll-height="100%">
                    <nav class="navbar navbar-default" role="navigation">
                        <div class="panel panel-default">
                            <div class="menu_school">
                                <?php if ($_smarty_tpl->tpl_vars['view']->value == '') {?>
                                    <i class="fas fa-tachometer-alt fa-fw fa-lg pr10"></i>
                                    <?php echo __("Dashboard");?>

                                <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "attendance") {?>
                                    <i class="fa fa-tasks fa-fw fa-lg pr10"></i>
                                    <?php echo __("Attendance");?>

                                    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        <?php echo __("Whole school");?>

                                    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "rollup") {?>
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        <?php echo __("Roll up");?>

                                    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "classatt") {?>
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        <?php echo __("Class attendance summary");?>

                                    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "child") {?>
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        <?php echo __("Student attendance summary");?>

                                    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "conabs") {?>
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        <?php echo __("Consecutive absent student");?>

                                    <?php }?>>
                                <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "medicines") {?>
                                    <i class="fa fa-medkit fa-lg fa-fw pr10"></i>
                                    <?php echo __("Medicines");?>

                                    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        <?php echo __("Today lists");?>

                                    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "all") {?>
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        <?php echo __("List all");?>

                                    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        <?php echo __("Add New");?>

                                    <?php }?>
                                <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "diarys") {?>
                                    <i class="fa fa-medkit fa-lg fa-fw pr10"></i>
                                    <?php echo __("Diary corner");?>

                                    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        <?php echo __("List all");?>

                                    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        <?php echo __("Add New");?>

                                    <?php }?>
                                <?php } elseif (($_smarty_tpl->tpl_vars['view']->value == "useservices") || ($_smarty_tpl->tpl_vars['view']->value == "services")) {?>
                                    <i class="fa fa-life-ring fa-lg fa-fw pr10"></i>
                                    <?php echo __("Service");?>

                                    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        <?php echo __("Service list");?>

                                    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "history") {?>
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        <?php echo __("Service usage information");?>

                                    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "reg") {?>
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        <?php echo __("Register service");?>

                                    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "foodsvr") {?>
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        <?php echo __("Daily food service summary");?>

                                    <?php }?>
                                <?php } elseif (($_smarty_tpl->tpl_vars['view']->value == "tuitions") || ($_smarty_tpl->tpl_vars['view']->value == "fees")) {?>
                                    <i class="fas fa-money-bill-alt fa-lg fa-fw pr10"></i>
                                    <?php echo __("Tuition fee");?>

                                    <?php if ($_smarty_tpl->tpl_vars['view']->value == "tuitions" && $_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        <?php echo __("Monthly tuition list");?>

                                    <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "tuitions" && $_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        <?php echo __("Add monthly tuition");?>

                                    <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "fees" && $_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        <?php echo __("Fee list");?>

                                    <?php }?>
                                <?php } elseif (($_smarty_tpl->tpl_vars['view']->value == "schedules")) {?>
                                    <i class="fas fa-calendar-alt fa-lg fa-fw pr10"></i>
                                    <?php echo __("Schedule");?>

                                    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        <?php echo __("Schedule list");?>

                                    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        <?php echo __("Add schedule");?>

                                    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "import") {?>
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        <?php echo __("Import from Excel file");?>

                                    <?php }?>
                                <?php } elseif (($_smarty_tpl->tpl_vars['view']->value == "menus")) {?>
                                    <i class="fas fa-utensils fa-lg fa-fw pr10"></i>
                                    <?php echo __("Menu");?>

                                    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        <?php echo __("Menu list");?>

                                    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        <?php echo __("Add menu");?>

                                    <?php }?>
                                    <!-- Late pickup-->
                                <?php } elseif (($_smarty_tpl->tpl_vars['view']->value == "pickup")) {?>
                                    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == "template") {?>
                                        <i class="fa fa-universal-access fa-lg fa-fw pr10"></i>
                                        <?php echo __("Late pickup");?>

                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        <?php echo __("Setting and create price table");?>

                                    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "assign") {?>
                                        <i class="fa fa-universal-access fa-lg fa-fw pr10"></i>
                                        <?php echo __("Late pickup");?>

                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        <?php echo __("Assign teacher");?>

                                    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "list") {?>
                                        <i class="fa fa-universal-access fa-lg fa-fw pr10"></i>
                                        <?php echo __("Late pickup");?>

                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        <?php echo __("Lists");?>

                                    <?php }?>

                                <?php } elseif (($_smarty_tpl->tpl_vars['view']->value == "pickupteacher")) {?>
                                    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == "history") {?>
                                        <i class="fa fa-universal-access fa-lg fa-fw pr10"></i>
                                        <?php echo __("Late pickup");?>

                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        <?php echo __("History");?>

                                    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "assign") {?>
                                        <i class="fa fa-universal-access fa-lg fa-fw pr10"></i>
                                        <?php echo __("Late pickup");?>

                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        <?php echo __("Schedule assignment");?>

                                    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "manage") {?>
                                        <i class="fa fa-universal-access fa-lg fa-fw pr10"></i>
                                        <?php echo __("Late pickup");?>

                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        <?php echo __("Add students - Pickup");?>

                                    <?php }?>

                                    <!-- END Late pickup-->
                                <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "events") {?>
                                    <i class="fa fa-bell fa-lg fa-fw pr10"></i>
                                    <?php echo __("Notification - Event");?>

                                    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        <?php echo __("Lists");?>

                                    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        <?php echo __("Add New");?>

                                    <?php }?>
                                <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "children") {?>
                                    <i class="fa fa-child fa-fw fa-lg pr10"></i>
                                    <?php echo __("Student");?>

                                    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        <?php echo __("Lists");?>

                                    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "listchildnew") {?>
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        <?php echo __("New student study begin in month");?>

                                    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "import") {?>
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        <?php echo __("Import from Excel file");?>

                                    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        <?php echo __("Add New");?>

                                    <?php }?>
                                <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "classes") {?>
                                    <i class="fa fa-graduation-cap fa-fw fa-lg pr10"></i>
                                    <?php echo __("Class");?>

                                    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        <?php echo __("Lists");?>

                                    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        <?php echo __("Add New");?>

                                    <?php }?>
                                    <!-- Contact book -->
                                <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "reports") {?>
                                    <?php if ($_smarty_tpl->tpl_vars['view']->value == "reports" && $_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
                                        <i class="fa fa-book fa-lg fa-fw pr10"></i>
                                        <?php echo __("Contact book");?>

                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        <?php echo __("Lists");?>

                                    <?php }?>
                                    <?php if ($_smarty_tpl->tpl_vars['view']->value == "reports" && $_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
                                        <i class="fa fa-book fa-lg fa-fw pr10"></i>
                                        <?php echo __("Contact book");?>

                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        <?php echo __("Add New");?>

                                    <?php }?>
                                    <?php if ($_smarty_tpl->tpl_vars['view']->value == "reports" && $_smarty_tpl->tpl_vars['sub_view']->value == "listtemp") {?>
                                        <i class="fa fa-book fa-lg fa-fw pr10"></i>
                                        <?php echo __("Contact book");?>

                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        <?php echo __("List template");?>

                                    <?php }?>
                                    <?php if ($_smarty_tpl->tpl_vars['view']->value == "reports" && $_smarty_tpl->tpl_vars['sub_view']->value == "listcate") {?>
                                        <i class="fa fa-book fa-lg fa-fw pr10"></i>
                                        <?php echo __("Contact book");?>

                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        <?php echo __("List category");?>

                                    <?php }?>
                                    <!-- end Contact book -->
                                    <!-- Point -->
                                <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "points") {?>
                                    <?php if ($_smarty_tpl->tpl_vars['view']->value == "points" && $_smarty_tpl->tpl_vars['sub_view']->value == "listsbysubject") {?>
                                        <i class="fa fa-book fa-lg fa-fw pr10"></i>
                                        <?php echo __("Points");?>

                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        <?php echo __("Lists by subject");?>

                                    <?php }?>
                                    <?php if ($_smarty_tpl->tpl_vars['view']->value == "points" && $_smarty_tpl->tpl_vars['sub_view']->value == "listsbystudent") {?>
                                        <i class="fa fa-book fa-lg fa-fw pr10"></i>
                                        <?php echo __("Points");?>

                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        <?php echo __("Lists by student");?>

                                    <?php }?>
                                    <?php if ($_smarty_tpl->tpl_vars['view']->value == "points" && $_smarty_tpl->tpl_vars['sub_view']->value == "importManual") {?>
                                        <i class="fa fa-book fa-lg fa-fw pr10"></i>
                                        <?php echo __("Points");?>

                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        <?php echo __("Manual Import");?>

                                    <?php }?>
                                    <?php if ($_smarty_tpl->tpl_vars['view']->value == "points" && $_smarty_tpl->tpl_vars['sub_view']->value == "importExcel") {?>
                                        <i class="fa fa-book fa-lg fa-fw pr10"></i>
                                        <?php echo __("Points");?>

                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        <?php echo __("Import from Excel file");?>

                                    <?php }?>
                                    <?php if ($_smarty_tpl->tpl_vars['view']->value == "points" && $_smarty_tpl->tpl_vars['sub_view']->value == "assign") {?>
                                        <i class="fa fa-book fa-lg fa-fw pr10"></i>
                                        <?php echo __("Points");?>

                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        <?php echo __("Assign teacher");?>

                                    <?php }?>
                                    <?php if ($_smarty_tpl->tpl_vars['view']->value == "points" && $_smarty_tpl->tpl_vars['sub_view']->value == "comment") {?>
                                        <i class="fa fa-book fa-lg fa-fw pr10"></i>
                                        <?php echo __("Points");?>

                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        <?php echo __("Comment");?>

                                    <?php }?>
                                    <!-- end Point -->
                                <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "teachers") {?>
                                    <i class="fa fa-street-view fa-fw fa-lg pr10"></i>
                                    <?php echo __("Teacher");?>
/<?php echo __("Employee");?>

                                    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        <?php echo __("Lists");?>

                                    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "addexisting") {?>
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        <?php echo __("Add Existing Account");?>

                                    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        <?php echo __("Add New");?>

                                    <?php }?>
                                <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "classlevels") {?>
                                    <i class="fa fa-tree fa-fw fa-lg pr10"></i>
                                    <?php echo __("Class level");?>

                                <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "birthdays") {?>
                                    <i class="fa fa-birthday-cake fa-fw fa-lg pr10"></i>
                                    <?php echo __("Birthdate");?>

                                    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        <?php echo __("Child");?>

                                    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "parent") {?>
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        <?php echo __("Parent");?>

                                    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "teacher") {?>
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        <?php echo __("Teacher - Employee");?>

                                    <?php }?>
                                <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "diarys") {?>
                                    <i class="fa fa-image fa-fw fa-lg pr10"></i>
                                    <?php echo __("Journal corner");?>

                                    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        <?php echo __("Lists");?>

                                    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        <?php echo __("Add New");?>

                                    <?php }?>
                                <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "healths") {?>
                                    <i class="fa fa-heart fa-fw fa-lg pr10"></i>
                                    <?php echo __("Health information");?>

                                    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        <?php echo __("Lists");?>

                                    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        <?php echo __("Add New");?>

                                    <?php }?>
                                <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "settings") {?>
                                    <i class="fa fa-cog fa-fw fa-lg pr10"></i>
                                    <?php echo __("Settings");?>

                                <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "roles") {?>
                                    <i class="fa fa-cog fa-fw fa-lg pr10"></i>
                                    <?php echo __('Permission');?>

                                <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "feedback") {?>
                                    <i class="fa fa-envelope fa-fw fa-lg pr10"></i>
                                    <?php echo __("Parent feedback");?>

                                <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "points") {?>
                                    <i class="fa fa-envelope fa-fw fa-lg pr10"></i>
                                    <?php echo __("Point");?>

                                <?php }?>
                                <!-- end Feedback -->

                            </div>
                            <div class="navbar-header">

                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                        data-target="#collapse">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            <?php if (count($_smarty_tpl->tpl_vars['schoolList']->value) > 0 || count($_smarty_tpl->tpl_vars['classList']->value) > 0 || count($_smarty_tpl->tpl_vars['childrenList']->value) > 0) {?>
                                <div class="select_object_box">
                                    <select name="object" id="select_object">
                                        <?php if (count($_smarty_tpl->tpl_vars['schoolList']->value) > 0) {?>
                                            <option disabled>----- <?php echo __("School");?>
 -----</option>
                                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['schoolList']->value, 'school');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['school']->value) {
?>
                                                <option data-type="school" value="<?php echo $_smarty_tpl->tpl_vars['school']->value['page_name'];?>
"
                                                        <?php if ($_smarty_tpl->tpl_vars['school']->value['page_name'] == $_smarty_tpl->tpl_vars['username']->value) {?>selected
                                                        style="background: #3aff77"<?php }?>><?php echo $_smarty_tpl->tpl_vars['school']->value['page_title'];?>
</option>
                                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                        <?php }?>
                                        <?php if (count($_smarty_tpl->tpl_vars['classList']->value) > 0) {?>
                                            <option disabled>-----<?php echo __("Class");?>
-----</option>
                                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['classList']->value, 'class');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class']->value) {
?>
                                                <option data-type="class" value="<?php echo $_smarty_tpl->tpl_vars['class']->value['group_name'];?>
"
                                                        <?php if ($_smarty_tpl->tpl_vars['class']->value['group_name'] == $_smarty_tpl->tpl_vars['username']->value) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['class']->value['group_title'];?>
</option>
                                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>
a
                                        <?php }?>
                                        <?php if (count($_smarty_tpl->tpl_vars['childrenList']->value) > 0) {?>
                                            <option disabled>-----<?php echo __("Child");?>
-----</option>
                                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['childrenList']->value, 'childOb');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['childOb']->value) {
?>
                                                <option <?php if ($_smarty_tpl->tpl_vars['childOb']->value['school_id'] > 0) {?>data-type="child"
                                                        value="<?php echo $_smarty_tpl->tpl_vars['childOb']->value['child_id'];?>
"
                                                        <?php if ($_smarty_tpl->tpl_vars['childOb']->value['child_id'] == $_smarty_tpl->tpl_vars['child']->value['child_id']) {?>selected<?php }?>
                                                        <?php } else { ?>data-type="childinfo"
                                                        value="<?php echo $_smarty_tpl->tpl_vars['childOb']->value['child_parent_id'];?>
" <?php if ($_smarty_tpl->tpl_vars['childOb']->value['child_parent_id'] == $_smarty_tpl->tpl_vars['child']->value['child_parent_id']) {?>selected<?php }
}?>><?php echo $_smarty_tpl->tpl_vars['childOb']->value['child_name'];?>
</option>
                                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                        <?php }?>
                                    </select>
                                </div>
                            <?php }?>
                            <div class="panel-body with-nav collapse navbar-collapse" id="collapse">
                                <ul class="side-nav metismenu js_metisMenu nav navbar-nav navbar-right">
                                    <?php if ($_smarty_tpl->tpl_vars['schoolUser']->value['is_teacher'] != 1) {?>
                                        <!-- Dashboard -->
                                        <li class="<?php if ($_smarty_tpl->tpl_vars['view']->value == '') {?>active selected<?php }?>">
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
">
                                                <i class="fas fa-tachometer-alt fa-fw fa-lg pr10"></i> <?php echo __("Dashboard");?>

                                            </a>
                                        </li>
                                        <!-- Dashboard -->
                                    <?php }?>
                                    
                                    <?php if ($_smarty_tpl->tpl_vars['school']->value['attendance'] || $_smarty_tpl->tpl_vars['school']->value['services'] || $_smarty_tpl->tpl_vars['school']->value['pickup'] || $_smarty_tpl->tpl_vars['school']->value['tuitions']) {?>
                                        <?php if (canView($_smarty_tpl->tpl_vars['username']->value,"attendance") || canView($_smarty_tpl->tpl_vars['username']->value,"services") || canView($_smarty_tpl->tpl_vars['username']->value,"pickup") || canView($_smarty_tpl->tpl_vars['username']->value,"tuitions") || ($_smarty_tpl->tpl_vars['pickup_is_configured']->value && $_smarty_tpl->tpl_vars['pickup_is_teacher']->value)) {?>
                                            <li class="menu_header border_top">
                                                <a><?php echo __("Tuition info");?>
</a>
                                            </li>
                                        <?php }?>
                                    <?php }?>
                                    <!-- Attendance -->
                                    <?php if ($_smarty_tpl->tpl_vars['school']->value['attendance']) {?>
                                        <?php if (canView($_smarty_tpl->tpl_vars['username']->value,"attendance")) {?>
                                            <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "attendance") {?>class="active"<?php }?>>
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/attendance">
                                                    <i class="fa fa-tasks fa-fw fa-lg pr10"></i> <?php echo __("Attendance");?>

                                                    <span class="fa arrow"></span>
                                                </a>
                                                <ul>
                                                    <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "attendance" && $_smarty_tpl->tpl_vars['sub_view']->value == '') {?>class="active selected"<?php }?>>
                                                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/attendance">
                                                            <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Whole school");?>

                                                        </a>
                                                    </li>
                                                    <?php if (canEdit($_smarty_tpl->tpl_vars['username']->value,"attendance")) {?>
                                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "attendance" && $_smarty_tpl->tpl_vars['sub_view']->value == "rollup") {?>class="active selected"<?php }?>>
                                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/attendance/rollup">
                                                                <i class="fa fa-caret-right fa-fw pr10"></i> <?php echo __("Roll up");?>

                                                            </a>
                                                        </li>
                                                    <?php }?>
                                                    <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "attendance" && $_smarty_tpl->tpl_vars['sub_view']->value == "classatt") {?>class="active selected"<?php }?>>
                                                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/attendance/classatt">
                                                            <i class="fa fa-caret-right fa-fw pr10"></i> <?php echo __("Class attendance summary");?>

                                                        </a>
                                                    </li>
                                                    <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "attendance" && $_smarty_tpl->tpl_vars['sub_view']->value == "child") {?>class="active selected"<?php }?>>
                                                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/attendance/child">
                                                            <i class="fa fa-caret-right fa-fw pr10"></i> <?php echo __("Student attendance summary");?>

                                                        </a>
                                                    </li>
                                                    <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "attendance" && $_smarty_tpl->tpl_vars['sub_view']->value == "conabs") {?>class="active selected"<?php }?>>
                                                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/attendance/conabs">
                                                            <i class="fa fa-caret-right fa-fw pr10"></i> <?php echo __("Consecutive absent student");?>

                                                        </a>
                                                    </li>
                                                </ul>
                                            </li>
                                        <?php }?>
                                    <?php }?>
                                    <!-- Attendance -->

                                    <!-- Service -->
                                    <?php if ($_smarty_tpl->tpl_vars['school']->value['services']) {?>
                                        <?php if (canView($_smarty_tpl->tpl_vars['username']->value,"services")) {?>
                                            <li <?php if (($_smarty_tpl->tpl_vars['view']->value == "useservices") || ($_smarty_tpl->tpl_vars['view']->value == "services")) {?>class="active"<?php }?>>
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/services">
                                                    <i class="fa fa-life-ring fa-lg fa-fw pr10"></i> <?php echo __("Service");?>

                                                    <span class="fa arrow"></span>
                                                </a>
                                                <ul>
                                                    <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "services" && $_smarty_tpl->tpl_vars['sub_view']->value == '') {?>class="active selected"<?php }?>>
                                                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/services">
                                                            <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Service list");?>

                                                        </a>
                                                    </li>
                                                    <?php if (canEdit($_smarty_tpl->tpl_vars['username']->value,"services")) {?>
                                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "useservices" && $_smarty_tpl->tpl_vars['sub_view']->value == "reg") {?>class="active selected"<?php }?>>
                                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/useservices/reg">
                                                                <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Register service");?>

                                                            </a>
                                                        </li>
                                                    <?php }?>
                                                    <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "useservices" && $_smarty_tpl->tpl_vars['sub_view']->value == "history") {?>class="active selected"<?php }?>>
                                                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/useservices/history">
                                                            <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Service usage information");?>

                                                        </a>
                                                    </li>
                                                    <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "useservices" && $_smarty_tpl->tpl_vars['sub_view']->value == "foodsvr") {?>class="active selected"<?php }?>>
                                                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/useservices/foodsvr">
                                                            <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Daily food service summary");?>

                                                        </a>
                                                    </li>
                                                </ul>
                                            </li>
                                        <?php }?>
                                    <?php }?>
                                    <!-- Service -->

                                    <!-- Late Pickup -->
                                    <?php if ($_smarty_tpl->tpl_vars['school']->value['pickup']) {?>
                                        <?php if (canView($_smarty_tpl->tpl_vars['username']->value,"pickup")) {?>
                                            <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "pickup") {?>class="active"<?php }?>>
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/pickup/list">
                                                    <i class="fa fa-universal-access fa-lg fa-fw pr10"></i> <?php echo __("Late pickup");?>

                                                    - <?php echo __("Manage");?>

                                                    <span class="fa arrow"></span>
                                                </a>
                                                <ul>
                                                    <?php if (canEdit($_smarty_tpl->tpl_vars['username']->value,"pickup")) {?>
                                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "pickup" && $_smarty_tpl->tpl_vars['sub_view']->value == "template") {?>class="active selected"<?php }?>>
                                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/pickup/template">
                                                                <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Setting and create price table");?>

                                                            </a>
                                                        </li>
                                                        <?php if ($_smarty_tpl->tpl_vars['pickup_is_configured']->value) {?>
                                                            <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "pickup" && $_smarty_tpl->tpl_vars['sub_view']->value == "assign") {?>class="active selected"<?php }?>>
                                                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/pickup/assign">
                                                                    <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Assign teacher");?>

                                                                </a>
                                                            </li>
                                                        <?php }?>
                                                    <?php }?>
                                                    <?php if ($_smarty_tpl->tpl_vars['pickup_is_configured']->value) {?>
                                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "pickup" && $_smarty_tpl->tpl_vars['sub_view']->value == "list") {?>class="active selected"<?php }?>>
                                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/pickup/list">
                                                                <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Lists");?>

                                                            </a>
                                                        </li>
                                                    <?php }?>
                                                </ul>
                                            </li>
                                        <?php }?>
                                    <?php }?>

                                    <?php if ($_smarty_tpl->tpl_vars['school']->value['pickupteacher']) {?>
                                        <?php if ($_smarty_tpl->tpl_vars['pickup_is_configured']->value && $_smarty_tpl->tpl_vars['pickup_is_teacher']->value) {?>
                                            <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "pickupteacher") {?>class="active"<?php }?>>
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/pickupteacher/history">
                                                    <i class="fa fa-universal-access fa-lg fa-fw pr10"></i> <?php echo __("Late pickup");?>

                                                    - <?php echo __("Teacher");?>

                                                    <span class="fa arrow"></span>
                                                </a>
                                                <ul>
                                                    <?php if ($_smarty_tpl->tpl_vars['pickup_is_assigned']->value) {?>
                                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "pickupteacher" && $_smarty_tpl->tpl_vars['sub_view']->value == "manage") {?>class="active selected"<?php }?>>
                                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/pickupteacher/manage">
                                                                <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Add students - Pickup");?>

                                                            </a>
                                                        </li>
                                                    <?php }?>
                                                    <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "pickupteacher" && $_smarty_tpl->tpl_vars['sub_view']->value == "history") {?>class="active selected"<?php }?>>
                                                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/pickupteacher/history">
                                                            <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("History");?>

                                                        </a>
                                                    </li>
                                                    <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "pickupteacher" && $_smarty_tpl->tpl_vars['sub_view']->value == "assign") {?>class="active selected"<?php }?>>
                                                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/pickupteacher/assign">
                                                            <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Schedule assignment");?>

                                                        </a>
                                                    </li>
                                                </ul>
                                            </li>
                                        <?php }?>
                                    <?php }?>
                                    <!-- END Late Pickup -->

                                    <!-- Fee -->
                                    <?php if ($_smarty_tpl->tpl_vars['school']->value['tuitions']) {?>
                                        <?php if (canView($_smarty_tpl->tpl_vars['username']->value,"tuitions")) {?>
                                            <li class="<?php if (($_smarty_tpl->tpl_vars['view']->value == "tuitions") || ($_smarty_tpl->tpl_vars['view']->value == "fees")) {?>active<?php }?>">
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/tuitions">
                                                    <i class="fas fa-money-bill-alt fa-lg fa-fw pr10"></i> <?php echo __("Tuition fee");?>

                                                    <span class="fa arrow"></span>
                                                </a>
                                                <ul>
                                                    <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "tuitions" && $_smarty_tpl->tpl_vars['sub_view']->value == '') {?>class="active selected"<?php }?>>
                                                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/tuitions">
                                                            <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Monthly tuition list");?>

                                                        </a>
                                                    </li>
                                                    <?php if (canEdit($_smarty_tpl->tpl_vars['username']->value,"tuitions")) {?>
                                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "tuitions" && $_smarty_tpl->tpl_vars['sub_view']->value == "add") {?> class="active selected" <?php }?>>
                                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/tuitions/add">
                                                                <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Add monthly tuition");?>

                                                            </a>
                                                        </li>
                                                    <?php }?>
                                                    <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "fees" && $_smarty_tpl->tpl_vars['sub_view']->value == '') {?>class="active selected"<?php }?>>
                                                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/fees">
                                                            <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Fee list");?>

                                                        </a>
                                                    </li>
                                                </ul>
                                            </li>
                                        <?php }?>
                                    <?php }?>
                                    <!-- Fee -->
                                    

                                    
                                    <li class="menu_header border_top">
                                        <a><?php echo __("Information for student");?>
</a>
                                    </li>
                                    <!-- Event -->
                                    <?php if ($_smarty_tpl->tpl_vars['school']->value['events']) {?>
                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "events") {?>class="active"<?php }?>>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/events">
                                                <i class="fa fa-bell fa-lg fa-fw pr10"></i> <?php echo __("Notification - Event");?>

                                                <?php if (canEdit($_smarty_tpl->tpl_vars['username']->value,"events")) {?>
                                                    <span class="fa arrow"></span>
                                                <?php }?>
                                            </a>
                                            <?php if (canEdit($_smarty_tpl->tpl_vars['username']->value,"events")) {?>
                                                <ul>
                                                    <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "events" && $_smarty_tpl->tpl_vars['sub_view']->value == '') {?>class="active selected"<?php }?>>
                                                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/events">
                                                            <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Lists");?>

                                                        </a>
                                                    </li>
                                                    <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "events" && $_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>class="active selected"<?php }?>>
                                                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/events/add">
                                                            <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Add New");?>

                                                        </a>
                                                    </li>
                                                </ul>
                                            <?php }?>
                                        </li>
                                    <?php }?>
                                    <!-- Event -->

                                    <!-- Schedule -->
                                    <?php if ($_smarty_tpl->tpl_vars['school']->value['schedules']) {?>
                                        <?php if (canView($_smarty_tpl->tpl_vars['username']->value,"schedules")) {?>
                                            <li id="li-schedule" <?php if (($_smarty_tpl->tpl_vars['view']->value == "schedules")) {?>class="active"<?php }?>>
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/schedules">
                                                    <i class="fas fa-calendar-alt fa-lg fa-fw pr10"></i> <?php echo __("Schedule");?>

                                                    <?php if (canEdit($_smarty_tpl->tpl_vars['username']->value,"schedules")) {?>
                                                        <span class="fa arrow"></span>
                                                    <?php }?>
                                                </a>
                                                <?php if (canEdit($_smarty_tpl->tpl_vars['username']->value,"schedules")) {?>
                                                    <ul>
                                                        <li id="li-schedule-list"
                                                            <?php if ($_smarty_tpl->tpl_vars['view']->value == "schedules" && $_smarty_tpl->tpl_vars['sub_view']->value == '') {?>class="active selected"<?php }?>>
                                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/schedules">
                                                                <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Schedule list");?>

                                                            </a>
                                                        </li>
                                                        <li id="li-schedule-add"
                                                            <?php if ($_smarty_tpl->tpl_vars['view']->value == "schedules" && $_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>class="active selected"<?php }?>>
                                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/schedules/add">
                                                                <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Add schedule");?>

                                                            </a>
                                                        </li>
                                                    </ul>
                                                <?php }?>
                                            </li>
                                        <?php }?>
                                    <?php }?>
                                    <!-- End schedule -->

                                    <!-- Menu -->
                                    <?php if ($_smarty_tpl->tpl_vars['school']->value['menus']) {?>
                                        <?php if (canView($_smarty_tpl->tpl_vars['username']->value,"menus")) {?>
                                            <li <?php if (($_smarty_tpl->tpl_vars['view']->value == "menus")) {?>class="active"<?php }?>>
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/menus">
                                                    <i class="fas fa-utensils fa-lg fa-fw pr10"></i> <?php echo __("Menu");?>

                                                    <?php if (canEdit($_smarty_tpl->tpl_vars['username']->value,"menus")) {?>
                                                        <span class="fa arrow"></span>
                                                    <?php }?>
                                                </a>
                                                <?php if (canEdit($_smarty_tpl->tpl_vars['username']->value,"menus")) {?>
                                                    <ul>
                                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "menus" && $_smarty_tpl->tpl_vars['sub_view']->value == '') {?>class="active selected"<?php }?>>
                                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/menus">
                                                                <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Menu list");?>

                                                            </a>
                                                        </li>
                                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "menus" && $_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>class="active selected"<?php }?>>
                                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/menus/add">
                                                                <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Add menu");?>

                                                            </a>
                                                        </li>
                                                    </ul>
                                                <?php }?>
                                            </li>
                                        <?php }?>
                                    <?php }?>
                                    <!-- End menu -->

                                    <!-- Contact book -->
                                    <?php if ($_smarty_tpl->tpl_vars['school']->value['reports']) {?>
                                        <?php if (canView($_smarty_tpl->tpl_vars['username']->value,"reports")) {?>
                                            <li id="li-report" class="<?php if ($_smarty_tpl->tpl_vars['view']->value == "reports") {?>active<?php }?>">
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/reports">
                                                    <i class="fa fa-book fa-lg fa-fw pr10"></i> <?php echo __("Contact book");?>

                                                    <span class="fa arrow"></span>
                                                </a>
                                                <ul>
                                                    <li id="li-report-list"
                                                        <?php if (($_smarty_tpl->tpl_vars['view']->value == "reports") && ($_smarty_tpl->tpl_vars['sub_view']->value == '')) {?>class="active selected"<?php }?>>
                                                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/reports">
                                                            <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Lists");?>

                                                        </a>
                                                    </li>
                                                    <?php if (canEdit($_smarty_tpl->tpl_vars['username']->value,"reports")) {?>
                                                        <li id="li-report-add"
                                                            <?php if ($_smarty_tpl->tpl_vars['view']->value == "reports" && $_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>class="active selected"<?php }?>>
                                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/reports/add">
                                                                <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Add New");?>

                                                            </a>
                                                        </li>
                                                    <?php }?>
                                                    <li id="li-report-template"
                                                        <?php if ($_smarty_tpl->tpl_vars['view']->value == "reports" && $_smarty_tpl->tpl_vars['sub_view']->value == "listtemp") {?>class="active selected"<?php }?>>
                                                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/reports/listtemp">
                                                            <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("List template");?>

                                                        </a>
                                                    </li>
                                                    <li id="li-report-category"
                                                        <?php if ($_smarty_tpl->tpl_vars['view']->value == "reports" && $_smarty_tpl->tpl_vars['sub_view']->value == "listcate") {?>class="active selected"<?php }?>>
                                                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/reports/listcate">
                                                            <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("List category");?>

                                                        </a>
                                                    </li>
                                                </ul>
                                            </li>
                                        <?php }?>
                                    <?php }?>
                                    <!-- End Contact book -->

                                    <!-- Points -->
                                    <?php if ($_smarty_tpl->tpl_vars['school']->value['points']) {?>
                                        <?php if (canView($_smarty_tpl->tpl_vars['username']->value,"points")) {?>
                                            <li id="li-point" class="<?php if ($_smarty_tpl->tpl_vars['view']->value == "points") {?>active<?php }?>">
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/points">
                                                    <i class="fa fa-book fa-lg fa-fw pr10"></i> <?php echo __("Point");?>

                                                    <span class="fa arrow"></span>
                                                </a>
                                                <ul>
                                                    <li id="li-point-list"
                                                        <?php if (($_smarty_tpl->tpl_vars['view']->value == "points") && ($_smarty_tpl->tpl_vars['sub_view']->value == "listsbysubject")) {?>class="active selected"<?php }?>>
                                                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/points/listsbysubject">
                                                            <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Lists by subject");?>

                                                        </a>
                                                    </li>
                                                    <li id="li-point-list"
                                                        <?php if (($_smarty_tpl->tpl_vars['view']->value == "points") && ($_smarty_tpl->tpl_vars['sub_view']->value == "listsbystudent")) {?>class="active selected"<?php }?>>
                                                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/points/listsbystudent">
                                                            <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Lists by student");?>

                                                        </a>
                                                    </li>
                                                    <?php if (canEdit($_smarty_tpl->tpl_vars['username']->value,"points")) {?>
                                                        <li id="li-point-assign"
                                                            <?php if ($_smarty_tpl->tpl_vars['view']->value == "points" && $_smarty_tpl->tpl_vars['sub_view']->value == "importManual") {?>class="active selected"<?php }?>>
                                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/points/importManual">
                                                                <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Manual Import");?>

                                                            </a>
                                                        </li>
                                                        <li id="li-point-assign"
                                                            <?php if ($_smarty_tpl->tpl_vars['view']->value == "points" && $_smarty_tpl->tpl_vars['sub_view']->value == "importExcel") {?>class="active selected"<?php }?>>
                                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/points/importExcel">
                                                                <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Import from Excel file");?>

                                                            </a>
                                                        </li>
                                                        <li id="li-point-assign"
                                                            <?php if ($_smarty_tpl->tpl_vars['view']->value == "points" && $_smarty_tpl->tpl_vars['sub_view']->value == "assign") {?>class="active selected"<?php }?>>
                                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/points/assign">
                                                                <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Assign teacher");?>

                                                            </a>
                                                        </li>
                                                        <li id="li-point-comment"
                                                            <?php if ($_smarty_tpl->tpl_vars['view']->value == "points" && $_smarty_tpl->tpl_vars['sub_view']->value == "comment") {?>class="active selected"<?php }?>>
                                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/points/comment">
                                                                <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Comment");?>

                                                            </a>
                                                        </li>
                                                    <?php }?>
                                                </ul>
                                            </li>
                                        <?php }?>
                                    <?php }?>
                                    <!-- End Points -->

                                    

                                    
                                    <?php if ($_smarty_tpl->tpl_vars['school']->value['medicines'] || $_smarty_tpl->tpl_vars['school']->value['feedback'] || $_smarty_tpl->tpl_vars['school']->value['birthdays'] || $_smarty_tpl->tpl_vars['school']->value['diarys']) {?>
                                        <?php if (canView($_smarty_tpl->tpl_vars['username']->value,"medicines") || canView($_smarty_tpl->tpl_vars['username']->value,"feedback") || canView($_smarty_tpl->tpl_vars['username']->value,"birthdays") || canView($_smarty_tpl->tpl_vars['username']->value,"diarys")) {?>
                                            <li class="menu_header border_top">
                                                <a><?php echo __("School utility");?>
</a>
                                            </li>
                                        <?php }?>
                                    <?php }?>
                                    <!-- Medicine -->
                                    <?php if ($_smarty_tpl->tpl_vars['school']->value['medicines']) {?>
                                        <?php if (canView($_smarty_tpl->tpl_vars['username']->value,"medicines")) {?>
                                            <li id="li-medicine" <?php if ($_smarty_tpl->tpl_vars['view']->value == "medicines") {?>class="active"<?php }?>>
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/medicines">
                                                    <i class="fa fa-medkit fa-lg fa-fw pr10"></i> <?php echo __("Medicines");?>

                                                    <span class="fa arrow"></span>
                                                </a>
                                                <ul>
                                                    <li id="li-medicine-today"
                                                        <?php if ($_smarty_tpl->tpl_vars['view']->value == "medicines" && $_smarty_tpl->tpl_vars['sub_view']->value == '') {?>class="active selected"<?php }?>>
                                                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/medicines">
                                                            <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Today lists");?>

                                                        </a>
                                                    </li>
                                                    <li id="li-medicine-all"
                                                        <?php if ($_smarty_tpl->tpl_vars['view']->value == "medicines" && $_smarty_tpl->tpl_vars['sub_view']->value == "all") {?>class="active selected"<?php }?>>
                                                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/medicines/all">
                                                            <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("List all");?>

                                                        </a>
                                                    </li>
                                                    <?php if (canEdit($_smarty_tpl->tpl_vars['username']->value,"medicines")) {?>
                                                        <li id="li-medicine-add"
                                                            <?php if ($_smarty_tpl->tpl_vars['view']->value == "medicines" && $_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>class="active selected"<?php }?>>
                                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/medicines/add">
                                                                <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Add New");?>

                                                            </a>
                                                        </li>
                                                    <?php }?>
                                                </ul>
                                            </li>
                                        <?php }?>
                                    <?php }?>
                                    <!-- Medicine -->

                                    <!-- Feedback -->
                                    <?php if ($_smarty_tpl->tpl_vars['school']->value['feedback']) {?>
                                        <?php if (canView($_smarty_tpl->tpl_vars['username']->value,"feedback")) {?>
                                            <li id="li-feeback" <?php if ($_smarty_tpl->tpl_vars['view']->value == "feedback") {?>class="active selected"<?php }?>>
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/feedback">
                                                    <i class="fa fa-envelope fa-fw fa-lg pr10"></i> <?php echo __("Parent feedback");?>

                                                </a>
                                            </li>
                                        <?php }?>
                                    <?php }?>
                                    <!-- Feedback -->

                                    <!-- Birthday -->
                                    <?php if ($_smarty_tpl->tpl_vars['school']->value['birthdays']) {?>
                                        <?php if (canView($_smarty_tpl->tpl_vars['username']->value,"birthdays")) {?>
                                            <li class="<?php if ($_smarty_tpl->tpl_vars['view']->value == "birthdays") {?>active<?php }?>">
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/birthdays">
                                                    <i class="fa fa-birthday-cake fa-fw fa-lg pr10"></i> <?php echo __("Birthday");?>

                                                </a>
                                            </li>
                                        <?php }?>
                                    <?php }?>
                                    <!-- End Birthday -->

                                    <!-- Diary -->
                                    <?php if ($_smarty_tpl->tpl_vars['school']->value['diarys']) {?>
                                        <?php if (canView($_smarty_tpl->tpl_vars['username']->value,"diarys")) {?>
                                            <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "diarys") {?>class="active"<?php }?>>
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/diarys">
                                                    <i class="fa fa-image fa-lg fa-fw pr10"></i> <?php echo __("Diary corner");?>

                                                    <span class="fa arrow"></span>
                                                </a>
                                                <ul>
                                                    <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "diarys" && $_smarty_tpl->tpl_vars['sub_view']->value == '') {?>class="active selected"<?php }?>>
                                                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/diarys">
                                                            <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Diary lists");?>

                                                        </a>
                                                    </li>
                                                    <?php if (canEdit($_smarty_tpl->tpl_vars['username']->value,"diarys")) {?>
                                                        <li id="li-medicine-add"
                                                            <?php if ($_smarty_tpl->tpl_vars['view']->value == "diarys" && $_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>class="active selected"<?php }?>>
                                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/diarys/add">
                                                                <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Add New");?>

                                                            </a>
                                                        </li>
                                                    <?php }?>
                                                </ul>
                                            </li>
                                        <?php }?>
                                    <?php }?>
                                    <!-- Health -->
                                    <?php if ($_smarty_tpl->tpl_vars['school']->value['healths']) {?>
                                        <?php if (canView($_smarty_tpl->tpl_vars['username']->value,"healths")) {?>
                                            <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "healths") {?>class="active"<?php }?>>
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/healths">
                                                    <i class="fa fa-heart fa-lg fa-fw pr10"></i> <?php echo __("Health information");?>

                                                    <span class="fa arrow"></span>
                                                </a>
                                                <ul>
                                                    <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "healths" && $_smarty_tpl->tpl_vars['sub_view']->value == '') {?>class="active selected"<?php }?>>
                                                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/healths">
                                                            <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Lists");?>

                                                        </a>
                                                    </li>
                                                    <?php if (canEdit($_smarty_tpl->tpl_vars['username']->value,"healths")) {?>
                                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "healths" && $_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>class="active selected"<?php }?>>
                                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/healths/add">
                                                                <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Add New");?>

                                                            </a>
                                                        </li>
                                                    <?php }?>
                                                </ul>
                                            </li>
                                        <?php }?>
                                    <?php }?>
                                    <!-- Health -->
                                    

                                    
                                    <?php if ($_smarty_tpl->tpl_vars['school']->value['classes'] || $_smarty_tpl->tpl_vars['school']->value['classlevels'] || $_smarty_tpl->tpl_vars['school']->value['teachers'] || $_smarty_tpl->tpl_vars['school']->value['children'] || $_smarty_tpl->tpl_vars['school']->value['roles']) {?>
                                        <?php if (canView($_smarty_tpl->tpl_vars['username']->value,"classes") || canView($_smarty_tpl->tpl_vars['username']->value,"classlevels") || canView($_smarty_tpl->tpl_vars['username']->value,"teachers") || canView($_smarty_tpl->tpl_vars['username']->value,"children") || canView($_smarty_tpl->tpl_vars['username']->value,"roles") || canView($_smarty_tpl->tpl_vars['username']->value,"settings")) {?>
                                            <li class="menu_header border_top">
                                                <a><?php echo __("School");?>
</a>
                                            </li>
                                        <?php }?>
                                    <?php }?>
                                    <!-- Classes -->
                                    <?php if ($_smarty_tpl->tpl_vars['school']->value['classes'] || $_smarty_tpl->tpl_vars['school']->value['classlevels']) {?>
                                        <?php if (canView($_smarty_tpl->tpl_vars['username']->value,"classes") || canView($_smarty_tpl->tpl_vars['username']->value,"classlevels")) {?>
                                            <li <?php if (($_smarty_tpl->tpl_vars['view']->value == "classes" || $_smarty_tpl->tpl_vars['view']->value == "classlevels")) {?>class="active"<?php }?>>
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/classes">
                                                    <i class="fa fa-graduation-cap fa-fw fa-lg pr10"></i> <?php echo __("Class level");?>

                                                    - <?php echo __("Class");?>

                                                    <?php if (canEdit($_smarty_tpl->tpl_vars['username']->value,"classes") || canView($_smarty_tpl->tpl_vars['username']->value,"classlevels")) {?>
                                                        <span class="fa arrow"></span>
                                                    <?php }?>
                                                </a>
                                                <?php if (canEdit($_smarty_tpl->tpl_vars['username']->value,"classes") || canView($_smarty_tpl->tpl_vars['username']->value,"classlevels")) {?>
                                                    <ul>
                                                        <?php if ($_smarty_tpl->tpl_vars['school']->value['classlevels']) {?>
                                                            <?php if (canView($_smarty_tpl->tpl_vars['username']->value,"classlevels")) {?>
                                                                <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "classlevels") {?>class="active selected"<?php }?>>
                                                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/classlevels">
                                                                        <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Class level");?>

                                                                    </a>
                                                                </li>
                                                            <?php }?>
                                                        <?php }?>
                                                        <?php if ($_smarty_tpl->tpl_vars['school']->value['classes']) {?>
                                                            <?php if (canView($_smarty_tpl->tpl_vars['username']->value,"classes")) {?>
                                                                <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "classes" && $_smarty_tpl->tpl_vars['sub_view']->value == '') {?>class="active selected"<?php }?>>
                                                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/classes">
                                                                        <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Class lists");?>

                                                                    </a>
                                                                </li>
                                                            <?php }?>
                                                            <?php if (canEdit($_smarty_tpl->tpl_vars['username']->value,"classes")) {?>
                                                                <li <?php if (($_smarty_tpl->tpl_vars['view']->value == "classes") && ($_smarty_tpl->tpl_vars['sub_view']->value == "add")) {?>class="active selected"<?php }?>>
                                                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/classes/add">
                                                                        <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Add new class");?>

                                                                    </a>
                                                                </li>
                                                                <li <?php if (($_smarty_tpl->tpl_vars['view']->value == "classes") && ($_smarty_tpl->tpl_vars['sub_view']->value == "classup")) {?>class="active selected"<?php }?>>
                                                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/classes/classup">
                                                                        <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Class up");?>

                                                                    </a>
                                                                </li>
                                                                <li <?php if (($_smarty_tpl->tpl_vars['view']->value == "classes") && ($_smarty_tpl->tpl_vars['sub_view']->value == "graduates")) {?>class="active selected"<?php }?>>
                                                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/classes">
                                                                        <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Graduate");?>

                                                                    </a>
                                                                </li>
                                                            <?php }?>
                                                        <?php }?>
                                                    </ul>
                                                <?php }?>
                                            </li>
                                        <?php }?>
                                    <?php }?>
                                    <!-- Classes -->
                                    <!-- Subjects -->
                                    <?php if ($_smarty_tpl->tpl_vars['school']->value['subjects']) {?>
                                        <?php if (canView($_smarty_tpl->tpl_vars['username']->value,"subjects")) {?>
                                            <li <?php if (($_smarty_tpl->tpl_vars['view']->value == "subjects")) {?>class="active"<?php }?>>
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/subjects">
                                                    <i class="fa fa-book fa-fw fa-lg pr10"></i> <?php echo __("Subject");?>

                                                    <?php if (canEdit($_smarty_tpl->tpl_vars['username']->value,"subjects")) {?>
                                                        <span class="fa arrow"></span>
                                                    <?php }?>
                                                </a>
                                                <?php if (canEdit($_smarty_tpl->tpl_vars['username']->value,"subjects")) {?>
                                                    <ul>
                                                        <?php if ($_smarty_tpl->tpl_vars['school']->value['subjects']) {?>
                                                            <?php if (canView($_smarty_tpl->tpl_vars['username']->value,"subjects")) {?>
                                                                <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "subjects") {?>class="active selected"<?php }?>>
                                                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/subjects">
                                                                        <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("List");?>

                                                                    </a>
                                                                </li>
                                                            <?php }?>
                                                        <?php }?>
                                                    </ul>
                                                <?php }?>
                                            </li>
                                        <?php }?>
                                    <?php }?>
                                    <!-- Subjects -->
                                    <!-- Teachers/Employee -->
                                    <?php if ($_smarty_tpl->tpl_vars['school']->value['teachers']) {?>
                                        <?php if (canView($_smarty_tpl->tpl_vars['username']->value,"teachers")) {?>
                                            <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "teachers") {?>class="active"<?php }?>>
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/teachers">
                                                    <i class="fa fa-street-view fa-fw fa-lg pr10"></i> <?php echo __("Teacher - Employee");?>

                                                    <?php if (canEdit($_smarty_tpl->tpl_vars['username']->value,"teachers")) {?>
                                                        <span class="fa arrow"></span>
                                                    <?php }?>
                                                </a>
                                                <?php if (canEdit($_smarty_tpl->tpl_vars['username']->value,"teachers")) {?>
                                                    <ul>
                                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "teachers" && $_smarty_tpl->tpl_vars['sub_view']->value == '') {?>class="active selected"<?php }?>>
                                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/teachers">
                                                                <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Lists");?>

                                                            </a>
                                                        </li>
                                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "teachers" && $_smarty_tpl->tpl_vars['sub_view']->value == "addexisting") {?>class="active selected"<?php }?>>
                                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/teachers/addexisting">
                                                                <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Add Existing Account");?>

                                                            </a>
                                                        </li>
                                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "teachers" && $_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>class="active selected"<?php }?>>
                                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/teachers/add">
                                                                <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Add New");?>

                                                            </a>
                                                        </li>
                                                    </ul>
                                                <?php }?>
                                            </li>
                                        <?php }?>
                                    <?php }?>
                                    <!-- Teachers -->

                                    <!-- BEGIN - Children -->
                                    <?php if ($_smarty_tpl->tpl_vars['school']->value['children']) {?>
                                        <?php if (canView($_smarty_tpl->tpl_vars['username']->value,"children")) {?>
                                            <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "children") {?>class="active"<?php }?>>
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/children">
                                                    <i class="fa fa-child fa-fw fa-lg pr10"></i> <?php echo __("Student");?>

                                                    <?php if (canEdit($_smarty_tpl->tpl_vars['username']->value,"children")) {?>
                                                        <span class="fa arrow"></span>
                                                    <?php }?>
                                                </a>
                                                <?php if (canEdit($_smarty_tpl->tpl_vars['username']->value,"children")) {?>
                                                    <ul>
                                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "children" && $_smarty_tpl->tpl_vars['sub_view']->value == '') {?>class="active selected"<?php }?>>
                                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/children">
                                                                <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Lists");?>

                                                            </a>
                                                        </li>
                                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "children" && $_smarty_tpl->tpl_vars['sub_view']->value == "listchildnew") {?>class="active selected"<?php }?>>
                                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/children/listchildnew">
                                                                <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("New student study begin in month");?>

                                                            </a>
                                                        </li>
                                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "children" && $_smarty_tpl->tpl_vars['sub_view']->value == "import") {?>class="active selected"<?php }?>>
                                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/children/import">
                                                                <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Import from Excel file");?>

                                                            </a>
                                                        </li>
                                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "children" && $_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>class="active selected"<?php }?>>
                                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/children/add">
                                                                <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Add New");?>

                                                            </a>
                                                        </li>
                                                    </ul>
                                                <?php }?>
                                            </li>
                                        <?php }?>
                                    <?php }?>
                                    <!-- END - Children -->

                                    <!-- Permission -->
                                    <?php if ($_smarty_tpl->tpl_vars['school']->value['roles']) {?>
                                        <?php if (canView($_smarty_tpl->tpl_vars['username']->value,"roles")) {?>
                                            <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "roles") {?>class="active"<?php }?>>
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/roles">
                                                    <i class="fa fa-unlock-alt fa-fw fa-lg pr10"></i> <?php echo __('Permission');?>

                                                </a>
                                            </li>
                                        <?php }?>
                                    <?php }?>
                                    <!-- End Permission -->
                                    <!-- Thống kê đăng nhập -->
                                    <?php if ($_smarty_tpl->tpl_vars['school']->value['loginstatistics']) {?>
                                        <?php if (canView($_smarty_tpl->tpl_vars['username']->value,"loginstatistics")) {?>
                                            <li class="<?php if ($_smarty_tpl->tpl_vars['view']->value == "loginstatistics") {?>active selected<?php }?>">
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/loginstatistics">
                                                    <i class="fa fa-user fa-fw fa-lg pr10"></i> <?php echo __("Login statistics");?>

                                                </a>
                                            </li>
                                        <?php }?>
                                    <?php }?>
                                    <!-- End Thống kê đăng nhập -->
                                    <!-- Settings -->
                                    <?php if ($_smarty_tpl->tpl_vars['school']->value['settings']) {?>
                                        <?php if (canView($_smarty_tpl->tpl_vars['username']->value,"settings")) {?>
                                            <li class="<?php if ($_smarty_tpl->tpl_vars['view']->value == "settings") {?>active selected<?php }?>">
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/settings">
                                                    <i class="fa fa-cog fa-fw fa-lg pr10"></i> <?php echo __("Setting and control");?>

                                                </a>
                                            </li>
                                        <?php }?>
                                    <?php }?>
                                    <!-- End Setting -->
                                    
                                    <!-- Ẩn hiện menu -->
                                    <li class="shmenu">
                                        <input type="hidden" id="full_screen"
                                               value="<?php if ($_smarty_tpl->tpl_vars['view']->value == 'attendance' && $_smarty_tpl->tpl_vars['sub_view']->value == 'classatt') {?>1<?php } else { ?> 0<?php }?>">
                                        <a href="#" class="js_school_hidden" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"> <i
                                                    class="fa fa-bars fa-fw pr10 fa-lg"></i> <?php echo __("Show/hide menu");?>

                                            <span class="fa fa-angle-left pull-right"></span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
        <?php } else { ?>
            <div class="col-xs-0 col-sm-3" style="height: 500px;"></div>
        <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['school']->value['school_step'] != SCHOOL_STEP_FINISH && $_smarty_tpl->tpl_vars['school']->value['grade'] == 0) {?>
            <?php $_smarty_tpl->_subTemplateRender('file:ci/school/school.steps.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

        <?php }?>
        <?php $_smarty_tpl->_subTemplateRender('file:ci/school/school.help.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

        <div class="<?php if ($_smarty_tpl->tpl_vars['status']->value) {?> <?php if (($_smarty_tpl->tpl_vars['view']->value == 'pickup' && $_smarty_tpl->tpl_vars['sub_view']->value == 'assign' && ($_smarty_tpl->tpl_vars['school']->value['school_step'] == SCHOOL_STEP_FINISH || $_smarty_tpl->tpl_vars['school']->value['grade'] != 0))) {?>col-md-10 col-sm-10 <?php } else { ?> col-md-9 col-sm-9<?php }?> <?php } else { ?> col-md-12 col-sm-12 <?php }?>"
             id="content_right">
            <?php if ($_smarty_tpl->tpl_vars['view']->value == '') {?>
                <?php $_smarty_tpl->_subTemplateRender('file:ci/school/school.dashboard.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

            <?php } else { ?>
                <?php $_smarty_tpl->_subTemplateRender("file:ci/school/school.".((string)$_smarty_tpl->tpl_vars['view']->value).".tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['school']->value['school_step'] == SCHOOL_STEP_FINISH || $_smarty_tpl->tpl_vars['school']->value['grade'] != 0) {?>
                <?php $_smarty_tpl->_subTemplateRender('file:ci/school/school.faq.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['school']->value['school_step'] != SCHOOL_STEP_FINISH && $_smarty_tpl->tpl_vars['school']->value['grade'] == 0) {?>
                <?php $_smarty_tpl->_subTemplateRender('file:ci/school/school.direct.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

            <?php }?>
        </div>
    </div>
</div>
<!-- page content -->

<?php $_smarty_tpl->_subTemplateRender('file:_footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
