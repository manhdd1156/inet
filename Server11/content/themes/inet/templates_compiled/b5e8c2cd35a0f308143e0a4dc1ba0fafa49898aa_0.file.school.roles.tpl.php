<?php
/* Smarty version 3.1.31, created on 2021-03-31 10:41:39
  from "D:\workplace\Server11\content\themes\inet\templates\ci\school\school.roles.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_6063ef73239b31_99129046',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b5e8c2cd35a0f308143e0a4dc1ba0fafa49898aa' => 
    array (
      0 => 'D:\\workplace\\Server11\\content\\themes\\inet\\templates\\ci\\school\\school.roles.tpl',
      1 => 1552404702,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6063ef73239b31_99129046 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <?php if (($_smarty_tpl->tpl_vars['sub_view']->value == '') && $_smarty_tpl->tpl_vars['canEdit']->value) {?>
            <div class = "pull-right flip">
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/roles/assignprincipal" class="btn btn-default">
                    <i class="fa fa-plus"></i> <?php echo __("Assign principal");?>

                </a>
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/roles/add" class="btn btn-default">
                    <i class="fa fa-plus"></i> <?php echo __("Add role");?>

                </a>
            </div>
        <?php }?>
        <div class="pull-right flip" style="margin-right: 5px">
            <a href="https://blog.coniu.vn/huong-dan-phan-mot-quyen-nao-do-cho-nhan-vien-trong-truong/" target="_blank" class="btn btn-info  btn_guide">
                <i class="fa fa-info"></i> <?php echo __("Guide");?>

            </a>
        </div>
        <i class="fa fa-tasks fa-fw fa-lg pr10"></i>
        <?php echo __('Role');?>

        <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == "edit") {?>
            &rsaquo; <?php echo $_smarty_tpl->tpl_vars['data']->value['role_name'];?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
            &rsaquo; <?php echo __('Add New');?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "assignrole") {?>
            &rsaquo; <?php echo __('Assign role');?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "assignprincipal") {?>
            &rsaquo; <?php echo __('Assign principal');?>

        <?php }?>
    </div>
    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
        <div class = "panel-body with-table">
            <div class = "table-responsive">
                <div class = "form-group">
                    <strong><?php echo __('Role list');?>
 (<?php echo count($_smarty_tpl->tpl_vars['rows']->value);?>
)</strong>
                </div>
                <table class = "table table-striped table-bordered table-hover">
                    <thead>
                        <tr><th>#</th>
                            <th><?php echo __('Role');?>
</th>
                            <th><?php echo __('Employee');?>
</th>
                            <th><?php echo __('Description');?>
</th>
                            <th><?php echo __("Actions");?>
</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (count($_smarty_tpl->tpl_vars['rows']->value) > 0) {?>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['rows']->value, 'row', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['row']->value) {
?>
                                <tr>
                                    <td align="center" style="vertical-align:middle"><strong><?php echo $_smarty_tpl->tpl_vars['k']->value+1;?>
</strong></td>
                                    <td style="vertical-align:middle" nowrap="true"><?php echo $_smarty_tpl->tpl_vars['row']->value['role_name'];?>
</td>
                                    <td style="vertical-align:middle" nowrap="true">
                                        <?php if (count($_smarty_tpl->tpl_vars['row']->value['users']) == 0) {?>
                                            <?php echo __("No employee");?>

                                        <?php } else { ?>
                                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['row']->value['users'], 'employee', false, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['employee']->value) {
?>
                                                <?php echo $_smarty_tpl->tpl_vars['key']->value+1;?>
 - <?php echo $_smarty_tpl->tpl_vars['employee']->value['user_fullname'];?>
<br/>
                                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                        <?php }?>
                                    </td>
                                    <td style="vertical-align:middle"><?php echo $_smarty_tpl->tpl_vars['row']->value['description'];?>
</td>
                                    <td align="center" style="vertical-align:middle">
                                        <?php if ($_smarty_tpl->tpl_vars['canEdit']->value) {?>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/roles/assignrole/<?php echo $_smarty_tpl->tpl_vars['row']->value['role_id'];?>
" class="btn btn-xs btn-primary">
                                                <?php echo __("Assign role");?>

                                            </a><br/>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/roles/edit/<?php echo $_smarty_tpl->tpl_vars['row']->value['role_id'];?>
" class="btn btn-xs btn-default">
                                                <?php echo __("Edit");?>

                                            </a>
                                            <?php if (count($_smarty_tpl->tpl_vars['row']->value['users']) == 0) {?>
                                                <a class="btn btn-xs btn-danger js_school-delete" data-handle="role" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['role_id'];?>
">
                                                    <?php echo __("Delete");?>

                                                </a>
                                            <?php }?>
                                        <?php }?>
                                    </td>
                                </tr>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        <?php } else { ?>
                            <tr>
                                <td align="center" colspan="5">
                                    <?php echo __("There is no role in your school. Do you want to create default roles?");?>
<br/>
                                    <a href="#" id="default" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" class="btn btn-primary js_school-default-role"><?php echo __("Create default roles");?>
</a>
                                    <label id="processing" class="btn btn-info x-hidden"><?php echo __("Processing");?>
...</label>
                                </td>
                            </tr>
                        <?php }?>
                    </tbody>
                </table>
            </div>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "edit") {?>
        <div class="panel-body">
            <form class = "js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_role.php">
                <input type="hidden" name="school_username" id="school_username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                <input type="hidden" name="role_id" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['role_id'];?>
"/>
                <input type="hidden" name="do" value="edit"/>

                <div class="form-group">
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left"><?php echo __("Role name");?>
 (*)</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="role_name" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['role_name'];?>
" maxlength="255" required autofocus/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left"><?php echo __("Description");?>
</label>
                        <div class="col-sm-9">
                            <textarea class="form-control" rows="2" name="description" maxlength="300"><?php echo $_smarty_tpl->tpl_vars['data']->value['description'];?>
</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-3">
                            <button type="submit" class="btn btn-primary padrl30"><?php echo __("Save");?>
</button>
                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/roles" class="btn btn-default"><?php echo __("Lists");?>
</a>
                        </div>
                    </div>
                    <!-- success -->
                    <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                    <!-- success -->
                    <!-- error -->
                    <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                    <!-- error -->
                </div>
                <div class = "table-responsive">
                    <table class = "table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th rowspan="2">#</th>
                            <th rowspan="2"><?php echo __('Module');?>
</th>
                            <th><?php echo __('No permission');?>
</th>
                            <th><?php echo __('View only');?>
</th>
                            <th><?php echo __('Can edit');?>
</th>
                        </tr>
                        <tr>
                            <th><input type="checkbox" id="checkAllNo"/><?php echo __('All');?>
</th>
                            <th><input type="checkbox" id="checkAllView"/><?php echo __('All');?>
</th>
                            <th><input type="checkbox" id="checkAllAll"/><?php echo __('All');?>
</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['modules']->value, 'row', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['row']->value) {
?>
                            <tr>
                                <td align="center"><strong><?php echo $_smarty_tpl->tpl_vars['k']->value+1;?>
</strong></td>
                                <td><?php echo __($_smarty_tpl->tpl_vars['row']->value['module_name']);?>
</td>
                                <td align="center">
                                    <input type="radio" name="<?php echo $_smarty_tpl->tpl_vars['row']->value['module'];?>
" value="<?php echo @constant('PERM_NONE');?>
" class="no_permission" <?php if ($_smarty_tpl->tpl_vars['row']->value['value'] == @constant('PERM_NONE')) {?>checked<?php }?>>
                                </td>
                                <td align="center">
                                    <input type="radio" name="<?php echo $_smarty_tpl->tpl_vars['row']->value['module'];?>
" value="<?php echo @constant('PERM_VIEW');?>
" class="view_permission" <?php if ($_smarty_tpl->tpl_vars['row']->value['value'] == @constant('PERM_VIEW')) {?>checked<?php }?>>
                                </td>
                                <td align="center">
                                    <input type="radio" name="<?php echo $_smarty_tpl->tpl_vars['row']->value['module'];?>
" value="<?php echo @constant('PERM_EDIT');?>
" class="all_permission" <?php if ($_smarty_tpl->tpl_vars['row']->value['value'] == @constant('PERM_EDIT')) {?>checked<?php }?>>
                                </td>
                            </tr>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </tbody>
                    </table>
                </div>
            </form>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
        <div class="panel-body ">
            <form class = "js_ajax-forms form-horizontal" data-url = "ci/bo/school/bo_role.php">
                <input type="hidden" name="school_username" id = "school_username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                <input type="hidden" name="do" value="add"/>

                <div class="form-group">
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left"><?php echo __("Role name");?>
 (*)</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="role_name" placeholder="<?php echo __("Role name");?>
" maxlength="255" required autofocus/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left"><?php echo __("Description");?>
</label>
                        <div class="col-sm-9">
                            <textarea class="form-control" rows="2" name="description" maxlength="300"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-3">
                            <button type="submit" class="btn btn-primary padrl30"><?php echo __("Save");?>
</button>
                        </div>
                    </div>
                    <!-- success -->
                    <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                    <!-- success -->
                    <!-- error -->
                    <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                    <!-- error -->
                </div>
                <div class = "table-responsive">
                    <table class = "table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th rowspan="2">#</th>
                                <th rowspan="2"><?php echo __('Module');?>
</th>
                                <th><?php echo __('No permission');?>
</th>
                                <th><?php echo __('View only');?>
</th>
                                <th><?php echo __('Can edit');?>
</th>
                            </tr>
                            <tr>
                                <th><input type="checkbox" id="checkAllNo" checked/><?php echo __('All');?>
</th>
                                <th><input type="checkbox" id="checkAllView"/><?php echo __('All');?>
</th>
                                <th><input type="checkbox" id="checkAllAll"/><?php echo __('All');?>
</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['modules']->value, 'row', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['row']->value) {
?>
                                <tr>
                                    <td align="center"><strong><?php echo $_smarty_tpl->tpl_vars['k']->value+1;?>
</strong></td>
                                    <td><?php echo __($_smarty_tpl->tpl_vars['row']->value['module_name']);?>
</td>
                                    <td align="center"><input type="radio" name="<?php echo $_smarty_tpl->tpl_vars['row']->value['module'];?>
" value="<?php echo @constant('PERM_NONE');?>
" class="no_permission" checked></td>
                                    <td align="center"><input type="radio" name="<?php echo $_smarty_tpl->tpl_vars['row']->value['module'];?>
" value="<?php echo @constant('PERM_VIEW');?>
" class="view_permission"></td>
                                    <td align="center"><input type="radio" name="<?php echo $_smarty_tpl->tpl_vars['row']->value['module'];?>
" value="<?php echo @constant('PERM_EDIT');?>
" class="all_permission"></td>
                                </tr>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </tbody>
                    </table>
                </div>
            </form>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "assignrole") {?>
        <div class="panel-body">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_role.php">
                <input type="hidden" name="school_username" id="school_username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                <input type="hidden" name="role_id" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['role_id'];?>
"/>
                <input type="hidden" name="do" value="assignrole"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">
                        <?php echo __("Role name");?>

                    </label>
                    <div class="col-sm-9">
                        <input class="form-control" name="role_name" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['role_name'];?>
" disabled>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-3" align="right">
                        <label class="control-label text-right"><?php echo __("Employee");?>
</label>
                        <div><?php echo __("Select employees whom you want to assign this role");?>
</div>
                    </div>
                    <div class="col-sm-9">
                        <div class = "table-responsive">
                            <table class = "table table-striped table-bordered table-hover">
                                <tbody>
                                <?php $_smarty_tpl->_assignInScope('newRow', 1);
?>
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['data']->value['users'], 'row', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['row']->value) {
?>
                                    <?php if ($_smarty_tpl->tpl_vars['newRow']->value == 1) {?><tr><?php }?>
                                    <td>
                                        <input type="checkbox" name="userIds[]" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['user_id'];?>
" <?php if ($_smarty_tpl->tpl_vars['row']->value['role_id'] > 0) {?>checked<?php }?>/>&nbsp;
                                        <?php echo $_smarty_tpl->tpl_vars['k']->value+1;?>
-<?php echo $_smarty_tpl->tpl_vars['row']->value['user_fullname'];?>

                                    </td>
                                    <?php if ($_smarty_tpl->tpl_vars['newRow']->value == 0) {?>
                                        <?php $_smarty_tpl->_assignInScope('newRow', 1);
?>
                                        </tr>
                                    <?php } else { ?>
                                        <?php $_smarty_tpl->_assignInScope('newRow', 0);
?>
                                    <?php }?>
                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30"><?php echo __("Save");?>
</button>
                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/roles" class="btn btn-default"><?php echo __("Lists");?>
</a>
                    </div>
                </div>

                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
            </form>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "assignprincipal") {?>
        <div class="panel-body">
            <form class="js_ajax-forms-success-reload form-horizontal" data-url="ci/bo/school/bo_role.php">
                <input type="hidden" name="school_username" id="school_username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                <input type="hidden" name="do" value="assignprincipal"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">

                    </label>
                    <div class="col-sm-9">
                        <?php echo __("Assign principal");?>
<br/>
                        <strong><?php echo __("Note: The person is assigned as manager he will have all permissions");?>
</strong>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-3" align="right">
                        <label class="control-label text-right"><?php echo __("Employee");?>
</label>
                        <div><?php echo __("Select employees whom you want to assign principal");?>
</div>
                    </div>
                    <div class="col-sm-9">
                        <div class = "table-responsive">
                            <table class = "table table-striped table-bordered table-hover">
                                <tbody>
                                <?php $_smarty_tpl->_assignInScope('newRow', 1);
?>
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['data']->value, 'row', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['row']->value) {
?>
                                    <?php if ($_smarty_tpl->tpl_vars['newRow']->value == 1) {?><tr><?php }?>
                                    <td>
                                        <input type="checkbox" name="userIds[]" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['user_id'];?>
" <?php if ($_smarty_tpl->tpl_vars['row']->value['is_principal'] == 1) {?>checked<?php }?>/>&nbsp;
                                        <?php echo $_smarty_tpl->tpl_vars['k']->value+1;?>
-<?php echo $_smarty_tpl->tpl_vars['row']->value['user_fullname'];?>

                                    </td>
                                    <?php if ($_smarty_tpl->tpl_vars['newRow']->value == 0) {?>
                                        <?php $_smarty_tpl->_assignInScope('newRow', 1);
?>
                                        </tr>
                                    <?php } else { ?>
                                        <?php $_smarty_tpl->_assignInScope('newRow', 0);
?>
                                    <?php }?>
                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30"><?php echo __("Save");?>
</button>
                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/roles" class="btn btn-default"><?php echo __("Lists");?>
</a>
                    </div>
                </div>

                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
            </form>
        </div>
    <?php }?>
</div><?php }
}
