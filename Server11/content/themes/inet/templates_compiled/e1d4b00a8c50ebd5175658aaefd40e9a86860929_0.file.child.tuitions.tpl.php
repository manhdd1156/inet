<?php
/* Smarty version 3.1.31, created on 2021-03-31 10:47:00
  from "D:\workplace\Server11\content\themes\inet\templates\ci\child\child.tuitions.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_6063f0b447a364_54969243',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e1d4b00a8c50ebd5175658aaefd40e9a86860929' => 
    array (
      0 => 'D:\\workplace\\Server11\\content\\themes\\inet\\templates\\ci\\child\\child.tuitions.tpl',
      1 => 1552404702,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:ci/child/child.tuitions.detail.tpl' => 1,
    'file:ci/child/child.tuitions.history.tpl' => 1,
  ),
),false)) {
function content_6063f0b447a364_54969243 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <div class="pull-right flip">
            <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == "history") {?>
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/child/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
/tuitions" class="btn btn-success"><?php echo __('Tuition');?>
</a>
            <?php }?>
        </div>
        <i class="fas fa-money-bill-alt fa-lg fa-fw pr10"></i>
        <?php echo __("Tuition");?>

        <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == "history") {?>
            &rsaquo; <?php echo __("History");?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "detail") {?>
            &rsaquo; <?php echo __("Detail");?>

        <?php }?>
    </div>
    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
        <div class="panel-body with-table">
            <div class="col-sm-12">
                <p><b><?php echo __("Transfer credit information");?>
: <?php echo nl2br($_smarty_tpl->tpl_vars['dataCon']->value['bank_account']);?>
</b></p>
            </div>
            <div><strong><?php echo __("Tuition list");?>
&nbsp;(<?php echo count($_smarty_tpl->tpl_vars['rows']->value);?>
)</strong></div>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th><?php echo __("Month");?>
</th>
                        <th><?php echo __("Monthly Fee");?>
</th>
                        
                        <th><?php echo __("Previous debt");?>
</th>
                        <th><?php echo __("Total");?>
</th>
                        <th><?php echo __("Paid");?>
</th>
                        <th><?php echo __("Option");?>
</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['rows']->value, 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
                        <tr>
                            <td class="text-center"><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</td>
                            <td class="text-center"><a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/child/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
/tuitions/detail/<?php echo $_smarty_tpl->tpl_vars['row']->value['tuition_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['row']->value['month'];?>
</a></td>
                            <td class="text-right"><?php echo moneyFormat($_smarty_tpl->tpl_vars['row']->value['total_amount']-$_smarty_tpl->tpl_vars['row']->value['debt_amount']+$_smarty_tpl->tpl_vars['row']->value['total_deduction']);?>
</td>
                            
                            <td class="text-right"><?php echo moneyFormat($_smarty_tpl->tpl_vars['row']->value['debt_amount']);?>
</td>
                            <td class="text-right"><?php echo moneyFormat($_smarty_tpl->tpl_vars['row']->value['total_amount']);?>
</td>
                            <td class="text-right"><?php if ($_smarty_tpl->tpl_vars['row']->value['status'] == @constant('TUITION_CHILD_CONFIRMED')) {
echo moneyFormat($_smarty_tpl->tpl_vars['row']->value['paid_amount']);
} else { ?>0<?php }?></td>
                            <td class="text-center">
                                <?php if ($_smarty_tpl->tpl_vars['row']->value['status'] == @constant('TUITION_CHILD_NOT_PAID')) {?>
                                    <a class="btn btn-xs btn-default js_child-tuition" data-handle="pay" data-child_id="<?php echo $_smarty_tpl->tpl_vars['row']->value['child_id'];?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['tuition_child_id'];?>
"><?php echo __("Pay");?>
</a>
                                <?php } elseif ($_smarty_tpl->tpl_vars['row']->value['status'] == @constant('TUITION_CHILD_PARENT_PAID')) {?>
                                    <strong><?php echo __("Paid and waiting confirmation");?>
</strong>
                                <?php } else { ?>
                                    <strong><?php echo __("Paid");?>
</strong>
                                <?php }?>
                            </td>
                        </tr>
                        <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                    </tbody>
                </table>
            </div>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "detail") {?>
        <?php $_smarty_tpl->_subTemplateRender("file:ci/child/child.tuitions.detail.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "history") {?>
        <?php $_smarty_tpl->_subTemplateRender("file:ci/child/child.tuitions.history.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

    <?php }?>
</div><?php }
}
