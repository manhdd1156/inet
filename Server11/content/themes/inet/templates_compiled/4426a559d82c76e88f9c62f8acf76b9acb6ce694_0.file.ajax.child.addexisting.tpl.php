<?php
/* Smarty version 3.1.31, created on 2021-06-03 14:38:17
  from "D:\workplace\Inet-project\source\Server11\content\themes\inet\templates\ci\school\ajax.child.addexisting.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_60b886e9c5be57_25406803',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '4426a559d82c76e88f9c62f8acf76b9acb6ce694' => 
    array (
      0 => 'D:\\workplace\\Inet-project\\source\\Server11\\content\\themes\\inet\\templates\\ci\\school\\ajax.child.addexisting.tpl',
      1 => 1621501238,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:ci/ajax.parentlist.tpl' => 1,
  ),
),false)) {
function content_60b886e9c5be57_25406803 (Smarty_Internal_Template $_smarty_tpl) {
?>
<form class="js_ajax-add-child-form form-horizontal" data-url="ci/bo/school/bo_child.php">
    <input type="hidden" name="school_username" id="school_username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
    <input type="hidden" name="child_code" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_code'];?>
">
    <input type="hidden" name="child_parent_id" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_parent_id'];?>
">
    <input type="hidden" name="do" value="addexisting"/>

    <div class="form-group">
        <label class="col-sm-3 control-label text-left"><?php echo __("Full name");?>
 (*)</label>
        <div class="col-sm-3">
            <input type="text" class="form-control" name="last_name" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['last_name'];?>
" placeholder="<?php echo __("Last name");?>
" required maxlength="34" autofocus>
        </div>
        <div class="col-sm-2">
            <input type="text" class="form-control" name="first_name" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['first_name'];?>
" placeholder="<?php echo __("First name");?>
" required maxlength="15">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label text-left"><?php echo __("Gender");?>
 (*)</label>
        <div class="col-sm-3">
            <select name="gender" id="gender" class="form-control">
                <option value="<?php echo @constant('MALE');?>
" <?php if ($_smarty_tpl->tpl_vars['child']->value['gender'] == @constant('MALE')) {?>selected<?php }?>><?php echo __("Male");?>
</option>
                <option value="<?php echo @constant('FEMALE');?>
" <?php if ($_smarty_tpl->tpl_vars['child']->value['gender'] == @constant('FEMALE')) {?>selected<?php }?>><?php echo __("Female");?>
</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label text-left"><?php echo __("Birthdate");?>
 (*)</label>
        <div class='col-sm-3'>
            <div class='input-group date' id='birthdate_dp'>
                <input id="birthday" type='text' name="birthday" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['birthday'];?>
" class="form-control" placeholder="<?php echo __("Birthdate");?>
 (*)" required/>
                <label class="input-group-addon btn" for="date">
                    <span class="fas fa-calendar-alt"></span>
                </label>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label text-left"><?php echo __("Parent");?>
</label>
        <div class="col-sm-7">
            <input name="search-parent" id="search-parent" type="text" class="form-control" placeholder="<?php echo __("Enter username, email or fullname to search");?>
" autocomplete="off">
            <div id="search-parent-results" class="dropdown-menu dropdown-widget dropdown-search">
                <div class="dropdown-widget-header">
                    <?php echo __("Search Results");?>

                </div>
                <div class="dropdown-widget-body">
                    <div class="loader loader_small ptb10"></div>
                </div>
            </div>
            <div><?php echo __("Enter at least 4 characters");?>
.</div>
            <br/>
            <div class="col-sm-9" id="parent_list" name="parent_list">
                <?php if (count($_smarty_tpl->tpl_vars['parent']->value) > 0) {?>
                    <?php $_smarty_tpl->_subTemplateRender('file:ci/ajax.parentlist.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('results'=>$_smarty_tpl->tpl_vars['parent']->value), 0, false);
?>

                <?php } else { ?>
                    <?php echo __("No parent");?>

                <?php }?>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label text-left"><?php echo __("Mother's name");?>
</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" id="parent_name" name="parent_name" placeholder="<?php echo __("Mother's name");?>
" maxlength="50">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label text-left"><?php echo __("Telephone");?>
</label>
        <div class="col-sm-3">
            <input type="text" class="form-control" id="parent_phone" name="parent_phone" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['parent_phone'];?>
" placeholder="<?php echo __("Telephone");?>
" maxlength="50">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label text-left"><?php echo __("Job");?>
</label>
        <div class="col-sm-3">
            <input type="text" class="form-control" id="parent_job" name="parent_job" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['parent_job'];?>
" placeholder="<?php echo __("Job");?>
" maxlength="50">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label text-left"><?php echo __("Father's name");?>
</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" id="parent_name_dad" name="parent_name_dad" placeholder="<?php echo __("Father's name");?>
" maxlength="50">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label text-left"><?php echo __("Telephone");?>
</label>
        <div class="col-sm-3">
            <input type="text" class="form-control" id="parent_phone_dad" name="parent_phone_dad" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['parent_phone_dad'];?>
" placeholder="<?php echo __("Telephone");?>
" maxlength="50">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label text-left"><?php echo __("Job");?>
</label>
        <div class="col-sm-3">
            <input type="text" class="form-control" id="parent_job_dad" name="parent_job_dad" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['parent_job_dad'];?>
" placeholder="<?php echo __("Job");?>
" maxlength="50">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label text-left"><?php echo __("Parent email");?>
</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" id="parent_email" name="parent_email" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['parent_email'];?>
" placeholder="<?php echo __("Parent email");?>
" maxlength="100">
            <div><?php echo __("If parent do not have any email, please let it empty");?>
.</div>
        </div>
        <div class="col-sm-4">
            <input type="checkbox" value="1" name="create_parent_account" id="create_parent_account" <?php if (count($_smarty_tpl->tpl_vars['parent']->value) > 0) {?>disabled<?php } else { ?>checked<?php }?>>&nbsp;<?php echo __("Auto-create parent account");?>
?
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label text-left"><?php echo __("Address");?>
</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" name="address" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['address'];?>
" placeholder="<?php echo __("Address");?>
" maxlength="250">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label text-left"><?php echo __("Class");?>
 (*)</label>
        <div class="col-sm-4">
            <select name="class_id" id="class_id" class="form-control">
                <?php if ($_smarty_tpl->tpl_vars['class_id']->value == 0) {?>
                    <?php if ($_smarty_tpl->tpl_vars['school']->value['children_use_no_class']) {?>
                        <option value="0"><?php echo __("No class");?>
</option>
                    <?php }?>

                    <?php $_smarty_tpl->_assignInScope('class_level', -1);
?>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['classes']->value, 'class');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class']->value) {
?>
                        <?php if (($_smarty_tpl->tpl_vars['class_level']->value != $_smarty_tpl->tpl_vars['class']->value['class_level_id'])) {?>
                            <option value="" disabled style="color: blue">-----<?php echo $_smarty_tpl->tpl_vars['class']->value['class_level_name'];?>
-----</option>
                        <?php }?>
                        <option value="<?php echo $_smarty_tpl->tpl_vars['class']->value['group_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['class']->value['group_title'];?>
</option>
                        <?php $_smarty_tpl->_assignInScope('class_level', $_smarty_tpl->tpl_vars['class']->value['class_level_id']);
?>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                <?php } else { ?>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['classes']->value, 'class');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class']->value) {
?>
                        <?php if (($_smarty_tpl->tpl_vars['class_id']->value == $_smarty_tpl->tpl_vars['class']->value['group_id'])) {?>
                            <option value="<?php echo $_smarty_tpl->tpl_vars['class_id']->value;?>
" disabled selected><?php echo $_smarty_tpl->tpl_vars['class']->value['group_title'];?>
</option>
                        <?php }?>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                <?php }?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label text-left"><?php echo __("Study start date");?>
 (*)</label>
        <div class='col-sm-3'>
            <div class='input-group date' id='beginat_dp'>
                <input id="start_date" type='text' name="begin_at" value="" class="form-control" placeholder="<?php echo __("Study start date");?>
" required/>
                <label class="input-group-addon btn" for="date">
                    <span class="fas fa-calendar-alt"></span>
                </label>
            </div>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 control-label text-left"><?php echo __("Description");?>
</label>
        <div class="col-sm-9">
            <textarea class="form-control" name="description" placeholder="<?php echo __("Write about your child...");?>
" maxlength="300"><?php echo $_smarty_tpl->tpl_vars['child']->value['description'];?>
</textarea>
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-3 col-sm-offset-3">
            <button type="submit" class="btn btn-primary padrl30"><?php echo __("Save");?>
</button>
        </div>
        <div class="col-sm-4">
            <a href="#" class="btn btn-default js_add_child_clear"><?php echo __("Clear Data");?>
</a>
        </div>
    </div>

    <!-- success -->
    <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
    <!-- success -->

    <!-- error -->
    <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
    <!-- error -->
</form><?php }
}
