<?php
/* Smarty version 3.1.31, created on 2021-05-20 16:10:13
  from "D:\workplace\Inet-project\source\Server11\content\themes\inet\templates\_header.messages.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_60a627754c02c1_57431335',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '8eab7e2a82246eae5704217a395256514e9db866' => 
    array (
      0 => 'D:\\workplace\\Inet-project\\source\\Server11\\content\\themes\\inet\\templates\\_header.messages.tpl',
      1 => 1621501238,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60a627754c02c1_57431335 (Smarty_Internal_Template $_smarty_tpl) {
?>
<li class="dropdown js_live-messages">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <i class="fa fa-comments fa-lg"></i>
        
        
        <span class="label hidden">

        </span>
    </a>
    <div class="dropdown-menu dropdown-widget with-arrow">
        <div class="dropdown-widget-header">
            <?php echo __("Messages");?>

            <a class="pull-right flip text-link js_chat-start" href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/messages/new"><?php echo __("Send a New Message");?>
</a>
        </div>
        <div class="dropdown-widget-body">
            <div class="js_scroller">
                
                
                <ul id="item_chat_header">
                    
                    
                    
                </ul>
                
                
                    
                
                
            </div>
        </div>
        
    </div>
</li><?php }
}
