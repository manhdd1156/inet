<?php
/* Smarty version 3.1.31, created on 2021-03-31 10:48:28
  from "D:\workplace\Server11\content\themes\inet\templates\ci\child\child.dashboard.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_6063f10c419515_35040375',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '5e603c8c7dd322ef1a23966285b749052dce7e45' => 
    array (
      0 => 'D:\\workplace\\Server11\\content\\themes\\inet\\templates\\ci\\child\\child.dashboard.tpl',
      1 => 1552404702,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6063f10c419515_35040375 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <i class="fa-fw fa-lg pr10"></i>
        <div class="col-sm-6">
            <strong><?php echo __("Child");?>
: <?php echo $_smarty_tpl->tpl_vars['child']->value['child_name'];?>
</strong>
        </div>
        
            
                
            
        
        <div class="pull-right flip">
            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/childinfo/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_parent_id'];?>
/childdetail" class="btn btn-xs btn-primary" style="margin-right: 10px.........">
                <i class="fa fa-info"></i> <?php echo __("Student information");?>

            </a>
            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/child/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
/attendance/resign">
                <i class="fa fa-tasks fa-lg fa-fw pr10"></i> <?php echo __("Resign");?>

            </a>&nbsp;|&nbsp;
            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/child/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
/medicines/add">
                <i class="fa fa-medkit fa-lg fa-fw pr10"></i> <?php echo __("Medicines");?>

            </a>
        </div>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-6">
                <div class="box-primary">
                    <div class="box-header">
                        <strong>
                            <?php echo __("School");?>
:&nbsp;<a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/pages/<?php echo $_smarty_tpl->tpl_vars['insights']->value['school']['page_name'];?>
"><?php echo $_smarty_tpl->tpl_vars['insights']->value['school']['page_title'];?>
</a>
                            &nbsp;|&nbsp;
                            <?php echo __("Class");?>
:&nbsp;<a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/groups/<?php echo $_smarty_tpl->tpl_vars['insights']->value['class']['group_name'];?>
"><?php echo $_smarty_tpl->tpl_vars['insights']->value['class']['group_title'];?>
</a>
                        </strong>
                    </div>
                    <div class="list-group">
                        <div class="list-group-item">
                            <?php echo __("School manager");?>
:
                            <?php if (isset($_smarty_tpl->tpl_vars['insights']->value['admin'])) {?>
                                <span class="name js_user-popover" data-uid="<?php echo $_smarty_tpl->tpl_vars['insights']->value['admin']['user_id'];?>
">
                                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['insights']->value['admin']['user_name'];?>
"><?php echo $_smarty_tpl->tpl_vars['insights']->value['admin']['user_fullname'];?>
</a>
                                    </span>
                                <?php if ($_smarty_tpl->tpl_vars['insights']->value['admin']['user_id'] != $_smarty_tpl->tpl_vars['user']->value->_data['user_id']) {?>
                                    <a href="#" class="far fa-comments fa-lg js_chat-start" data-name="<?php echo $_smarty_tpl->tpl_vars['insights']->value['admin']['user_fullname'];?>
" data-uid="<?php echo $_smarty_tpl->tpl_vars['insights']->value['admin']['user_id'];?>
"></a>
                                <?php }?>
                            <?php }?>
                        </div>
                        <div class="list-group-item">
                            <?php echo __("GV");?>
:
                            <?php if (count($_smarty_tpl->tpl_vars['insights']->value['teachers']) > 0) {?>
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['insights']->value['teachers'], 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
                                    <span class="name js_user-popover" data-uid="<?php echo $_smarty_tpl->tpl_vars['row']->value['user_id'];?>
">
                                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['row']->value['user_name'];?>
"><?php echo $_smarty_tpl->tpl_vars['row']->value['user_fullname'];?>
</a>
                                    </span>
                                    <?php if ($_smarty_tpl->tpl_vars['row']->value['user_id'] != $_smarty_tpl->tpl_vars['user']->value->_data['user_id']) {?>
                                        <a href="#" class="far fa-comments fa-lg js_chat-start" data-name="<?php echo $_smarty_tpl->tpl_vars['row']->value['user_fullname'];?>
" data-uid="<?php echo $_smarty_tpl->tpl_vars['row']->value['user_id'];?>
"></a>
                                    <?php }?>
                                    &nbsp;&nbsp;&nbsp;
                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                            <?php } else { ?>
                                <?php echo __("No teacher");?>

                            <?php }?>
                        </div>
                    </div>
                </div>

                <?php if (count($_smarty_tpl->tpl_vars['insights']->value['medicines']) > 0) {?>
                    <div class="box-primary">
                        <div class="box-header">
                            <i class="fa fa-medkit"></i>
                            <strong><?php echo __("Today medicines");?>
</strong>&nbsp;
                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/child/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
/medicines" title="<?php echo __("See All");?>
" class="pull-right flip"><?php echo __("See All");?>
</a>
                        </div>
                        <div class="list-group" id="medicine_on_dashboard">
                            <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['insights']->value['medicines'], 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
                                <div class="list-group-item">
                                    <strong><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</strong>-<a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/child/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
/medicines/edit/<?php echo $_smarty_tpl->tpl_vars['row']->value['medicine_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['row']->value['medicine_list'];?>
</a>
                                    <?php if ($_smarty_tpl->tpl_vars['row']->value['status'] == @constant('MEDICINE_STATUS_NEW')) {?>
                                        <img src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/content/themes/<?php echo $_smarty_tpl->tpl_vars['system']->value['theme'];?>
/images/new.gif"/>
                                    <?php } elseif ($_smarty_tpl->tpl_vars['row']->value['status'] == @constant('MEDICINE_STATUS_CONFIRMED')) {?>
                                        <i class="fa fa-check"></i>
                                    <?php }?>
                                    <br/><?php echo $_smarty_tpl->tpl_vars['row']->value['time_per_day'];?>
 <?php echo __("times/day");?>
&nbsp;|&nbsp;<?php echo $_smarty_tpl->tpl_vars['row']->value['begin'];?>
&nbsp;-&nbsp;<?php echo $_smarty_tpl->tpl_vars['row']->value['end'];?>
<br/>
                                    <i><?php echo __("Guide");?>
:</i> <strong><?php echo $_smarty_tpl->tpl_vars['row']->value['guide'];?>
</strong>
                                    <?php if (count($_smarty_tpl->tpl_vars['row']->value['detail']) > 0) {?>
                                        <br/><?php echo __("Medicated");?>
:
                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['row']->value['detail'], 'detail');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['detail']->value) {
?>
                                            <br/>&nbsp;&nbsp;&nbsp;<strong><?php echo $_smarty_tpl->tpl_vars['detail']->value['time_on_day'];?>
</strong>&nbsp;-&nbsp;<?php echo $_smarty_tpl->tpl_vars['detail']->value['created_at'];?>
&nbsp;|&nbsp;<?php echo $_smarty_tpl->tpl_vars['detail']->value['user_fullname'];?>

                                            <?php if ($_smarty_tpl->tpl_vars['detail']->value['created_user_id'] != $_smarty_tpl->tpl_vars['user']->value->_data['user_id']) {?>
                                                <a href="#" class="far fa-comments fa-lg js_chat-start" data-name="<?php echo $_smarty_tpl->tpl_vars['detail']->value['user_fullname'];?>
" data-uid="<?php echo $_smarty_tpl->tpl_vars['detail']->value['created_user_id'];?>
"></a>
                                            <?php }?>
                                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                    <?php }?>
                                </div>
                                <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </div>
                    </div>
                <?php }?>
                <?php if ($_smarty_tpl->tpl_vars['school']->value['config']['display_children_list']) {?>
                    <div class="table-responsive">
                        <div><strong><?php echo __("Children list");?>
&nbsp;(<?php echo count($_smarty_tpl->tpl_vars['insights']->value['children']);?>
)</strong></div>
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                            <tr>
                                <th style="width: 40px"><?php echo __("No.");?>
</th>
                                <th <?php if ($_smarty_tpl->tpl_vars['school']->value['config']['display_parent_info_for_others']) {?>width="40%"<?php }?>><?php echo __("Full name");?>
</th>
                                <?php if ($_smarty_tpl->tpl_vars['school']->value['config']['display_parent_info_for_others']) {?>
                                    <th><?php echo __("Parent");?>
</th>
                                <?php }?>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['insights']->value['children'], 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
                                <tr>
                                    <td align="center" style="vertical-align:middle"><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</td>
                                    <td class="align-middle">
                                        <?php echo $_smarty_tpl->tpl_vars['row']->value['child_name'];?>
<br/>
                                        <?php if (!is_empty($_smarty_tpl->tpl_vars['row']->value['birthday'])) {?>
                                            (<?php echo $_smarty_tpl->tpl_vars['row']->value['birthday'];?>
)
                                        <?php }?>
                                    </td>
                                    <?php if ($_smarty_tpl->tpl_vars['school']->value['config']['display_parent_info_for_others']) {?>
                                        <td>
                                            <?php if (count($_smarty_tpl->tpl_vars['row']->value['parent']) == 0) {?>
                                                <?php echo __("No parent");?>

                                            <?php } else { ?>
                                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['row']->value['parent'], '_user');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['_user']->value) {
?>
                                                    <span class="name js_user-popover" data-uid="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_id'];?>
">
                                                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_name'];?>
"><?php echo $_smarty_tpl->tpl_vars['_user']->value['user_fullname'];?>
</a>
                                                    </span>
                                                    <?php if ($_smarty_tpl->tpl_vars['_user']->value['user_id'] != $_smarty_tpl->tpl_vars['user']->value->_data['user_id']) {?>
                                                        <a href="#" class="far fa-comments fa-lg js_chat-start" data-name="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_fullname'];?>
" data-uid="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_id'];?>
"></a>
                                                    <?php }?>
                                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                            <?php }?><br/>
                                            <?php if (!is_empty($_smarty_tpl->tpl_vars['row']->value['parent_phone'])) {?>
                                                (<?php echo $_smarty_tpl->tpl_vars['row']->value['parent_phone'];?>
)
                                            <?php }?>

                                        </td>
                                    <?php }?>
                                </tr>
                                <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                            </tbody>
                        </table>
                    </div>
                <?php }?>
            </div>

            <div class="col-sm-6">
                <div class="box-primary">
                    <div class="box-header">
                        <i class="fa fa-bell"></i>
                        <strong><?php echo __("Notification - Event");?>
</strong>&nbsp;
                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/child/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
/events" title="<?php echo __("See All");?>
" class="pull-right flip"><?php echo __("See All");?>
</a>
                    </div>
                    <div class="list-group" id="event_on_dashboard">
                        <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['insights']->value['events'], 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
                            <div class="list-group-item">
                                <strong><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
 - <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/child/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
/events/detail/<?php echo $_smarty_tpl->tpl_vars['row']->value['event_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['row']->value['event_name'];?>
</a></strong>
                                <br/>
                                <?php echo $_smarty_tpl->tpl_vars['row']->value['description'];?>
<br/>
                            </div>
                            <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div><?php }
}
