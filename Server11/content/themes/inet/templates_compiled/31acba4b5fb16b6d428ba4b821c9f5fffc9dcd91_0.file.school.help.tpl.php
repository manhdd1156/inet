<?php
/* Smarty version 3.1.31, created on 2021-05-19 11:17:15
  from "D:\workplace\inet-project\Server11\content\themes\inet\templates\ci\school\school.help.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_60a4914bdd9f58_96401801',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '31acba4b5fb16b6d428ba4b821c9f5fffc9dcd91' => 
    array (
      0 => 'D:\\workplace\\inet-project\\Server11\\content\\themes\\inet\\templates\\ci\\school\\school.help.tpl',
      1 => 1621397832,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60a4914bdd9f58_96401801 (Smarty_Internal_Template $_smarty_tpl) {
?>

<?php if ($_smarty_tpl->tpl_vars['view']->value == '') {
} elseif ($_smarty_tpl->tpl_vars['view']->value == "attendance") {
} elseif ($_smarty_tpl->tpl_vars['view']->value == "medicines") {?>
    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
        <div class="col-md-9 col-sm-9">
            <div class="panel panel-default">
                <div class="panel-body color_red">
                    <?php echo __("Đây là danh sách trẻ cần được cho uống thuốc của");?>
 <strong>NGÀY HÔM NAY</strong>. Đơn thuốc có thể do nhà trường/giáo viên hoặc phụ huynh tạo ra.
                    <br/>- Những đơn thuốc đã cho trẻ uống ít nhất 1 lần sẽ không thể Sửa/Xóa, chỉ có thể Hủy (vẫn lưu lịch sử).
                </div>
            </div>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "all") {?>
        <div class="col-md-9 col-sm-9">
            <div class="panel panel-default">
                <div class="panel-body color_red">
                    Lịch sử các lần gửi thuốc của tất cả các trẻ trong trường.
                </div>
            </div>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
        <div class="col-md-9 col-sm-9">
            <div class="panel panel-default">
                <div class="panel-body color_red">
                    <strong>Để tạo đơn thuốc cho trẻ, bạn phải thực hiện như sau:</strong>
                    <br/>1. Chọn lớp để lấy ra danh sách trẻ.
                    <br/>2. Chọn trẻ cần cho uống thuốc từ danh sách.
                    <br/>3. Nhập thông tin đơn thuốc (1 đơn thuốc có thể uống trong nhiều ngày).
                </div>
            </div>
        </div>
    <?php }
} elseif ($_smarty_tpl->tpl_vars['view']->value == "services") {?>
    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
        <div class="col-md-9 col-sm-9">
            <div class="panel panel-default">
                <div class="panel-body color_red">
                    <strong><?php echo __('Service');?>
</strong> trên hệ thống Inet được định nghĩa là sản phầm/dịch vụ nhà trường cung cấp THÊM trong việc nuôi dạy trẻ. Trẻ học ở trường có thể sử dụng hoặc không (ví dụ: xe đưa đón, đón muộn...).
                    <?php if ((count($_smarty_tpl->tpl_vars['rows']->value) == 0)) {?>
                        <br/>- Chưa có dịch vụ nào được tạo trên hệ thống. <?php if (canEdit($_smarty_tpl->tpl_vars['username']->value,$_smarty_tpl->tpl_vars['view']->value)) {?><a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/services/add">Tạo dịch vụ</a><?php }?>
                    <?php }?>
                </div>
            </div>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
        <div class="col-md-9 col-sm-9">
            <div class="panel panel-default">
                <div class="panel-body color_red">
                    <?php if ($_smarty_tpl->tpl_vars['school']->value['school_step'] != SCHOOL_STEP_FINISH && $_smarty_tpl->tpl_vars['school']->value['grade'] == 0) {?>
                        <span class="strong">Bước 4.</span> Cài đặt học phí<br/>
                        <span class="strong">Bước 4.1.</span> Thêm dịch vụ mới<br/>
                    <?php }?>
                    <strong><?php echo __('Service');?>
</strong> trên hệ thống Inet được định nghĩa là sản phầm/dịch vụ nhà trường cung cấp THÊM trong việc nuôi dạy trẻ. Trẻ học ở trường có thể sử dụng hoặc không (ví dụ: xe đưa đón, đón muộn...). <br/><br/>
                    <strong>Dịch vụ được phân chia theo hình thức tính phí, bao gồm:</strong>
                    <br/>1. Tính phí theo tháng (ví dụ: xe đưa đón...). Phí dịch vụ là đơn giá theo tháng.
                    <br/>2. Theo ngày trẻ đi học (tính theo điểm danh). Phí dịch vụ là đơn giá cho một lần điểm danh.
                    <br/>3. Theo số lần sử dụng (ví dụ: đón muộn...). Phí dịch vụ là đơn giá cho một lần sử dụng.
                </div>
            </div>
        </div>
    <?php }
} elseif ($_smarty_tpl->tpl_vars['view']->value == "useservices") {?>
    
        
            
                
                
                
                
            
        
    
        
            
                
                    
                    
                        
                    
                
            
        
    
        
            
                
                    
                
            
        
    
<?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "fees") {?>
    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
        <div class="col-md-9 col-sm-9">
            <div class="panel panel-default">
                <div class="panel-body color_red">
                    Các khoản trong học phí là chi phí mà trẻ BẮT BUỘC phải trả khi học ở trường (ví dụ: học phí, tiền ăn, dụng cụ học tập...).
                    <br/>(Phân biệt với phí dịch vụ: là phí có thể sử dụng dịch vụ hoặc không, ví dụ: đón muộn, xe đưa đón).
                </div>
            </div>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
        <div class="col-md-9 col-sm-9">
            <div class="panel panel-default">
                <div class="panel-body color_red">
                    <?php if ($_smarty_tpl->tpl_vars['school']->value['school_step'] != SCHOOL_STEP_FINISH && $_smarty_tpl->tpl_vars['school']->value['grade'] == 0) {?>
                        <span class="strong">Bước 4.</span> Cài đặt học phí<br/>
                        <span class="strong">Bước 4.2.</span> Thêm loại phí mới<br/>
                    <?php }?>
                    - Các khoản trong học phí là chi phí mà trẻ <strong>BẮT BUỘC</strong> phải trả khi học ở trường (ví dụ: học phí, tiền ăn, dụng cụ học tập...).
                    <br/>- Phân biệt với phí dịch vụ: là phí có thể sử dụng dịch vụ hoặc không, ví dụ: đón muộn, xe đưa đón. <br/>
                    <strong>- Lưu ý:</strong> <br/>
                    - Chu kỳ tính phí có 2 loại: <br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;+ Phí hàng tháng: Là các khoản đóng góp cố định theo tháng (ví dụ: học phí). <br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;+ Phí hàng ngày: Là các khoản tính phí theo ngày (ví dụ: tiền ăn), nếu ngày hôm đó trẻ nghỉ hệ thống sẽ tự trừ tiền ăn.

                </div>
            </div>
        </div>
    <?php }
} elseif ($_smarty_tpl->tpl_vars['view']->value == "tuitions") {?>
    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
        
            
                
                
                
            
        
    <?php }
} elseif ($_smarty_tpl->tpl_vars['view']->value == "events") {?>
    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
        <div class="col-md-9 col-sm-9">
            <div class="panel panel-default">
                <div class="panel-body color_red">
                    Danh sách tất cả sự kiện của trường, bao gồm cả cấp khối và cấp lớp.
                </div>
            </div>
        </div>
    <?php } elseif (($_smarty_tpl->tpl_vars['sub_view']->value == "add") || ($_smarty_tpl->tpl_vars['sub_view']->value == "edit")) {?>
        <div class="col-md-9 col-sm-9">
            <div class="panel panel-default">
                <div class="panel-body color_red">
                    - <strong>Phạm vi</strong> của Thông báo - Sự kiện nghĩa là phạm vi có hiệu lực của Thông báo - Sự kiện đó (áp dụng cho toàn trường, toàn khối hay cho một lớp).
                    <br/>- Nếu chọn <strong>Đăng lên tường</strong>, nội dung của Thông báo - Sự kiện sẽ được đăng lên tường của trường (đối với cấp trường), đăng lên tường của tất cả các lớp trong khối (đối với cấp khối) và đăng lên tường của một lớp (đối với cấp lớp).
                    <br/>- Nếu chọn <strong>Thông báo ngay</strong>, khi đó nội dung của Thông báo - Sự kiện sẽ được thông báo tới giáo viên và phụ huynh. Chỉ khi chọn thông báo ngay, thì nội dung <strong>MỚI</strong> đăng lên tường (chưa thông báo, nghĩa là đang soạn thảo nội dung).
                </div>
            </div>
        </div>
    <?php }
} elseif ($_smarty_tpl->tpl_vars['view']->value == "children") {?>
    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
        <div class="col-md-9 col-sm-9">
            <div class="panel panel-default">
                <div class="panel-body color_red">
                    <strong>Mỗi trẻ có thể có nhiều phụ huynh (bố, mẹ, ông, bà...). Phụ huynh được gán vào cho trẻ bằng 02 cách:</strong>
                    <br/>- Cách 1: Điền thông tin vào ô phụ huynh để Tìm & Thêm từ những tài khoản người dùng đã có trong hệ thống.
                    <br/>- Cách 2: Tích chọn ô 'Tạo tài khoản phụ huynh' để hệ thống tự tạo và thông báo cho phụ huynh qua email (trường hợp này, phải nhập chính xác email phụ huynh).
                </div>
            </div>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
        <?php if ($_smarty_tpl->tpl_vars['school']->value['school_step'] != SCHOOL_STEP_FINISH && $_smarty_tpl->tpl_vars['school']->value['grade'] == 0) {?>
            <div class="col-md-9 col-sm-9">
                <div class="panel panel-default">
                    <div class="panel-body color_red">
                        <span class="strong">Bước 6.</span> Thêm trẻ mới. <br/>
                        Bạn có thể thêm trẻ theo hai cách: <br/>
                        <span class="strong">Cách 1:</span> Thêm trẻ mới (thêm từng trẻ một).<br/>
                        <span class="strong">Cách 2:</span> Thêm bằng file excel (thêm nhiều trẻ một lúc).<br/>
                        <strong>- Lưu ý:</strong> Phải tạo lớp trước khi nhập thông tin trẻ vào lớp đó. <?php if (canEdit($_smarty_tpl->tpl_vars['username']->value,"classes")) {?><a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/classes/add">Tạo lớp</a><?php }?>
                    </div>
                </div>
            </div>
        <?php }?>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "import") {?>
        <div class="col-md-9 col-sm-9">
            <div class="panel panel-default">
                <div class="panel-body color_red">
                    <strong>Bạn phải sử dụng biểu mẫu file Excel đúng quy định của hệ thống, biểu mẫu tải về tại <a target="_blank" href="https://drive.google.com/file/d/1Pjo6L0raH1MSwAMjb-fDgkyHyiPLpaWi">ĐÂY</a>.</strong>
                    <br/>- Phải tạo lớp trước khi nhập thông tin trẻ vào lớp đó. <?php if (canEdit($_smarty_tpl->tpl_vars['username']->value,"classes")) {?><a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/classes/add">Tạo lớp</a><?php }?>
                </div>
            </div>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "listchildedit") {?>
        <div class="col-md-9 col-sm-9">
            <div class="panel panel-default">
                <div class="panel-body">
                    <strong>- Danh sách trẻ được cập nhật bởi giáo viên</strong>
                </div>
            </div>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "addhealthindex") {?>
        <div class="col-md-9 col-sm-9">
            <div class="panel panel-default">
                <div class="panel-body color_red">
                    - Thêm chỉ số sức khỏe cho trẻ. <br/>
                    - Kích chuột vào tên từng trẻ để thêm thông tin sức khỏe cho trẻ đó. <br/>
                    - Lưu ý: <strong>Hệ thống chỉ ghi dữ liệu những trẻ được thêm cả chiều cao và cân nặng</strong>
                </div>
            </div>
        </div>
    <?php }
} elseif ($_smarty_tpl->tpl_vars['view']->value == "healths") {?>
    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
        <div class="col-md-9 col-sm-9">
            <div class="panel panel-default">
                <div class="panel-body color_red">
                    - Thêm chỉ số sức khỏe cho trẻ. <br/>
                    - Kích chuột vào tên từng trẻ để thêm thông tin sức khỏe cho trẻ đó. <br/>
                    - Lưu ý: <strong>Hệ thống chỉ ghi dữ liệu những trẻ được thêm cả chiều cao và cân nặng</strong>
                </div>
            </div>
        </div>
    <?php }
} elseif ($_smarty_tpl->tpl_vars['view']->value == "classes") {?>
    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>

            <div class="col-md-9 col-sm-9">
                <div class="panel panel-default">
                    <?php if ($_smarty_tpl->tpl_vars['school']->value['school_step'] == SCHOOL_STEP_FINISH || $_smarty_tpl->tpl_vars['school']->value['grade'] != 0) {?>
                        <div class="panel-body color_red">
                            Để tạo lớp bạn cần tạo giáo viên của lớp trong hệ thống trước bằng cách: <?php if (canEdit($_smarty_tpl->tpl_vars['username']->value,"teachers")) {?><a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/teachers/add"><?php echo __("Add New");?>
</a> | <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/teachers/addexisting"><?php echo __("Add Existing Account");?>
</a>.<?php }?>
                            <br/>- Một lớp có thể có nhiều giáo viên. Nhập thông tin giáo viên để tìm & Thêm vào lớp lần lượt.
                            <?php if (count($_smarty_tpl->tpl_vars['class_levels']->value) == 0) {?>
                                <br/>- Bạn phải tạo khối lớp trước khi tạo lớp. <?php if (canEdit($_smarty_tpl->tpl_vars['username']->value,"classlevels")) {?><a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/classlevels/add"><?php echo __("Add New Class Level");?>
</a><?php }?>
                            <?php }?>
                            <br/>
                            <strong>- Bạn có thể ấn vào "Thêm mới giáo viên" để tạo tài mới tài khoản giáo viên cho trường của bạn</strong>
                        </div>
                    <?php } else { ?>
                        <div class="panel-body">
                            <strong>Bước 2.</strong> Tạo các lớp trong trường.
                        </div>
                    <?php }?>
                </div>
            </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
        <?php if ($_smarty_tpl->tpl_vars['school']->value['school_step'] == SCHOOL_STEP_FINISH || $_smarty_tpl->tpl_vars['school']->value['grade'] != 0) {?>
            <div class="col-md-9 col-sm-9">
                <div class="panel panel-default">
                    <div class="panel-body color_red">
                        Để tốt nghiệp cho cả lớp, vui lòng click vào nút <strong>"Tốt nghiệp"</strong> tại danh sách lớp.
                    </div>
                </div>
            </div>
        <?php }?>
    <?php }
} elseif ($_smarty_tpl->tpl_vars['view']->value == "teachers") {?>
    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
        <div class="col-md-9 col-sm-9">
            <div class="panel panel-default">
                <div class="panel-body">
                    <?php if ($_smarty_tpl->tpl_vars['school']->value['school_step'] != SCHOOL_STEP_FINISH && $_smarty_tpl->tpl_vars['school']->value['grade'] == 0) {?>
                        <strong>Bước 3.</strong> Thêm giáo viên vào trường của bạn. <br/>
                    <?php }?>
                    <span class="color_red"><strong>Lưu ý:</strong></span> <br/>
                    - Nếu giáo viên đã có tài khoản trên hệ thống, bạn có thể ấn vào "Thêm tài khoản đã tồn tại" để thêm giáo viên vào trường của bạn. <br/>
                    - Nếu thêm mới tài khoản giáo viên, bạn phải điền đầy đủ thông tin trên màn hình. Hệ thống sẽ gửi mail thông báo cho giáo viên tới địa chỉ mail được nhập. <br/>
                    - Bạn vui lòng ghi nhớ "Email" và "mật khẩu" để cung cấp cho giáo viên làm tài khoản đăng nhập vào hệ thống.
                </div>
            </div>
        </div>
    <?php }
} elseif ($_smarty_tpl->tpl_vars['view']->value == "classlevels") {?>
    <div class="col-md-9 col-sm-9">
        <div class="panel panel-default">
            <div class="panel-body">
                <?php if ($_smarty_tpl->tpl_vars['school']->value['school_step'] != SCHOOL_STEP_FINISH && $_smarty_tpl->tpl_vars['school']->value['grade'] == 0) {?>
                    <strong>Bước 1.</strong> Tạo các khối của trường để sử dụng Inet (<strong>bắt buộc</strong>).<br/>
                <?php }?>
                Hệ thống tạo mặc định hai khối. Bạn có thể sửa/xoá/thêm khối để phù hợp với trường. <br/>
                ví dụ về rank level : Khối nhà trẻ -> -1. Khối mẫu giáo -> 0. Khối cấp 1 -> 1.. <br/>
                <?php if ($_smarty_tpl->tpl_vars['school']->value['school_step'] != SCHOOL_STEP_FINISH && $_smarty_tpl->tpl_vars['school']->value['grade'] == 0 && count($_smarty_tpl->tpl_vars['rows']->value) == 0) {?>
                    <strong>- Vui lòng tạo một khối để có thể tiếp tục</strong>
                <?php }?>
            </div>
        </div>
    </div>
<?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "schedules") {?>
    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == "import") {?>
        <div class="col-md-9 col-sm-9">
            <div class="panel panel-default">
                <div class="panel-body color_red">
                    <strong>- Bạn phải sử dụng biểu mẫu file Excel đúng quy định của hệ thống, biểu mẫu tải về tại <a target="_blank" href="https://drive.google.com/file/d/1BZOoCkJ8dRJlh_j3IEn1IrT8g7OMav5g">ĐÂY</a>.</strong>
                    <br/>
                    - Bạn phải <strong>cấu hình</strong> lịch học trước khi LƯU
                </div>
            </div>
        </div>
    <?php }
} elseif ($_smarty_tpl->tpl_vars['view']->value == "pickup") {?>
    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == "template") {?>
        <div class="col-md-9 col-sm-9">
            <div class="panel panel-default">
                <div class="panel-body color_red">
                    <?php if ($_smarty_tpl->tpl_vars['school']->value['school_step'] != SCHOOL_STEP_FINISH && $_smarty_tpl->tpl_vars['school']->value['grade'] == 0) {?>
                        <span class="strong">Bước 5.</span> Cài đặt đón muộn <br/>
                        <span class="strong">Bước 5.1.</span> Cài đặt và tạo bảng giá. <br/>
                    <?php }?>
                    - Bạn có thể cài đặt giờ bắt đầu tính đón muộn, bảng giá đón muộn, các dịch vụ đi kèm và tạo các lớp đón muộn tại màn hình này.
                </div>
            </div>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "assign") {?>
        <div class="col-md-9 col-sm-9">
            <div class="panel panel-default">
                <div class="panel-body color_red">
                    <?php if ($_smarty_tpl->tpl_vars['school']->value['school_step'] != SCHOOL_STEP_FINISH && $_smarty_tpl->tpl_vars['school']->value['grade'] == 0) {?>
                        <span class="strong">Bước 5.</span> Cài đặt đón muộn <br/>
                        <span class="strong">Bước 5.2.</span> Phân công giáo viên vào lớp trông muộn. <br/>
                    <?php }?>
                    - Để phân công giáo viên vào lớp đón muộn, bạn có thể làm như sau:<br/>
                    &nbsp;&nbsp;&nbsp;&nbsp; 1. Chọn ngày/thời gian.<br/>
                    &nbsp;&nbsp;&nbsp;&nbsp; 2. Chọn lớp đón muộn.<br/>
                    &nbsp;&nbsp;&nbsp;&nbsp; 3. Chọn giáo viên trông lớp đón muộn đó.<br/>
                    &nbsp;&nbsp;&nbsp;&nbsp; 4. Ấn nút "Lưu".<br/>
                    <strong>Lưu ý:</strong> Bạn có thể bật nút "Lặp lịch" để những tuần tiếp theo lịch trông lớp đón muộn tự động lặp lại.
                </div>
            </div>
        </div>
    <?php }?>

    
        
            
                
            
        
    
<?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "points") {?>
    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == "import") {?>
        <div class="col-md-9 col-sm-9">
            <div class="panel panel-default">
                <div class="panel-body color_red">
                    <?php if ($_smarty_tpl->tpl_vars['school']->value['grade'] == 1) {?>
                        <strong>Bạn phải sử dụng biểu mẫu file Excel đúng quy định của hệ thống, biểu mẫu tải về tại <a target="_blank" href="https://drive.google.com/file/d/1yxcGSMxUOlXgIyqcN1sTAJ-O2qMgkyCv/view?usp=sharing">ĐÂY</a>.</strong>
                    <?php } elseif ($_smarty_tpl->tpl_vars['school']->value['grade'] == 2) {?>
                        <strong>Bạn phải sử dụng biểu mẫu file Excel đúng quy định của hệ thống, biểu mẫu tải về tại <a target="_blank" href="https://drive.google.com/file/d/1eC_0tkbb65g7R6B4DIT3hXk-p5qeUIY8/view?usp=sharing">ĐÂY</a>.</strong>
                    <?php }?>
                </div>
            </div>
        </div>
    <?php }
}
}
}
