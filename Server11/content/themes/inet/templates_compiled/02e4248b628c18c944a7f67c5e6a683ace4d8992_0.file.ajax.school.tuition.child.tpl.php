<?php
/* Smarty version 3.1.31, created on 2021-03-31 10:36:39
  from "D:\workplace\Server11\content\themes\inet\templates\ci\school\ajax.school.tuition.child.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_6063ee4710ab63_34230536',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '02e4248b628c18c944a7f67c5e6a683ace4d8992' => 
    array (
      0 => 'D:\\workplace\\Server11\\content\\themes\\inet\\templates\\ci\\school\\ajax.school.tuition.child.tpl',
      1 => 1616552728,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6063ee4710ab63_34230536 (Smarty_Internal_Template $_smarty_tpl) {
?>

<?php $_smarty_tpl->_assignInScope('childId', $_smarty_tpl->tpl_vars['child']->value['child_id']);
?>
<div style = "color: #9f191f">
    <strong><?php echo $_smarty_tpl->tpl_vars['childIdx']->value;?>
 - <?php echo $_smarty_tpl->tpl_vars['child']->value['child_name'];?>
 (<?php echo $_smarty_tpl->tpl_vars['child']->value['month'];?>
 <?php echo __("Month");?>
)</strong>
    <?php if (!empty($_smarty_tpl->tpl_vars['child']->value['pre_month'])) {?>&nbsp;|&nbsp;<?php echo __("Number of charged days from last tuition");?>
 (<?php echo __("Month");?>
 <?php echo $_smarty_tpl->tpl_vars['child']->value['pre_month'];?>
):&nbsp;<?php echo $_smarty_tpl->tpl_vars['child']->value['attendance_count'];
}?>
    <div class="pull-right flip">
        <a class="btn btn-xs btn-default js_display_fee_2_add" child_id="<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
"><?php echo __("Add");?>
</a>
        <a class="btn btn-xs btn-default js_reload_child_tuition" child_id="<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
" child_idx="<?php echo $_smarty_tpl->tpl_vars['childIdx']->value;?>
"><?php echo __("Reload");?>
</a>
        <a class="btn btn-xs btn-default js_show_hide_tuition_tbody" child_id="<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
"><?php echo __("Show/hide");?>
</a>
        <a class="btn btn-xs btn-danger js_new_tuition_del_child" child_id="<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
"><?php echo __("Delete");?>
</a>
    </div>
</div>

<div class="mb10">
    <?php echo __("The number of school days in a student's month");?>
 <?php echo $_smarty_tpl->tpl_vars['child']->value['child_name'];?>

    <input style="width: 40px" type="number" name="day_of_child_<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
" id="day_of_child_<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
" class="day_of_child" value="<?php echo $_smarty_tpl->tpl_vars['attCount']->value;?>
" child_id="<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
">
</div>

<?php if ($_smarty_tpl->tpl_vars['school']->value['tuition_view_attandance'] && (count($_smarty_tpl->tpl_vars['child']->value['attendances']) > 0)) {?>
    <table class="table table-striped table-bordered table-hover">
        <tr>
            <td style="padding-left: 5px; padding-right: 5px"><a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/attendance/child/<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
" target="_blank"><strong><?php echo __("ATT");?>
</strong></a></td>
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['child']->value['attendances'], 'attendance');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['attendance']->value) {
?>
                <td align="center" style="padding-left: 5px; padding-right: 5px"
                        <?php if ($_smarty_tpl->tpl_vars['attendance']->value['status'] == @constant('ATTENDANCE_COME_LATE')) {?>
                            bgcolor="#6495ed"
                        <?php } elseif ($_smarty_tpl->tpl_vars['attendance']->value['status'] == @constant('ATTENDANCE_EARLY_LEAVE')) {?>
                            bgcolor="#008b8b"
                        <?php } elseif ($_smarty_tpl->tpl_vars['attendance']->value['status'] == @constant('ATTENDANCE_ABSENCE_NO_REASON')) {?>
                            bgcolor="#ff1493"
                        <?php } elseif ($_smarty_tpl->tpl_vars['attendance']->value['status'] == @constant('ATTENDANCE_ABSENCE')) {?>
                            bgcolor="#deb887"
                        <?php }?>
                ><?php echo $_smarty_tpl->tpl_vars['attendance']->value['display_date'];?>
</td>
            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

        </tr>
    </table>
<?php }?>
<input type="hidden" name="child_id[]" value="<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
"/>
<input type="hidden" name="pre_month[]" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['pre_month'];?>
"/>
<table class="table table-striped table-bordered table-hover" id="tbl_fee_<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
">
    <thead>
    <tr>
        <th>#</th>
        <th><?php echo __("Fee name");?>
</th>
        <th style="width: 52px"><?php echo __("Qty");?>
</th>
        <th style="width: 117px"><?php echo __("Unit price");?>
</th>
        <th style="width: 107px"><?php echo __("Deduction");?>
</th>
        <th style="width: 117px"><?php echo __("Money amount");?>
</th>
        <th style="width: 80px"><?php echo __("Option");?>
</th>
    </tr>
    </thead>
    <tbody>
    
    <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['child']->value['fees'], 'fee');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['fee']->value) {
?>
        <tr>
            <td class="text-center">
                <?php echo $_smarty_tpl->tpl_vars['idx']->value;?>

                <input type="hidden" class="fee_id_<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
" name="fee_id_<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
[]" value="<?php echo $_smarty_tpl->tpl_vars['fee']->value['fee_id'];?>
"/>
            </td>
            <td><?php echo $_smarty_tpl->tpl_vars['fee']->value['fee_name'];?>
</td>
            <td>
                <input type="number" id="fee_quantity_<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
_<?php echo $_smarty_tpl->tpl_vars['fee']->value['fee_id'];?>
" child_id="<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
" fee_id="<?php echo $_smarty_tpl->tpl_vars['fee']->value['fee_id'];?>
" name="fee_quantity_<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
[]"
                       <?php if ($_smarty_tpl->tpl_vars['fee']->value['fee_type'] == @constant('FEE_TYPE_MONTHLY')) {?> class="text-center fee_quantity" value="1" readonly style="width: 35px; background-color: #EEEEEE"<?php } else { ?> class="text-center fee_quantity day_of_child_val_<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
" data-type="1" value="<?php echo $_smarty_tpl->tpl_vars['fee']->value['quantity'];?>
" step="1" style="width: 35px;"<?php }?>/>
            </td>
            <td>
                <input readonly style="width: 100px; background-color: #EEEEEE" type="text" class="text-right fee_unit_price money_tui" id="fee_unit_price_<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
_<?php echo $_smarty_tpl->tpl_vars['fee']->value['fee_id'];?>
" child_id="<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
" fee_id="<?php echo $_smarty_tpl->tpl_vars['fee']->value['fee_id'];?>
"
                       name="fee_unit_price_<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
[]" value="<?php echo $_smarty_tpl->tpl_vars['fee']->value['unit_price'];?>
"/>
                <?php if (($_smarty_tpl->tpl_vars['fee']->value['fee_type'] != @constant('FEE_TYPE_MONTHLY')) && ($_smarty_tpl->tpl_vars['fee']->value['unit_price'] != $_smarty_tpl->tpl_vars['fee']->value['unit_price_deduction'])) {?>
                    <br/><?php echo __('Pm');?>
: <?php echo moneyFormat($_smarty_tpl->tpl_vars['fee']->value['unit_price_deduction']);?>

                <?php }?>
            </td>
            <td>
                <?php if ($_smarty_tpl->tpl_vars['fee']->value['fee_type'] == @constant('FEE_TYPE_MONTHLY')) {?>
                    <input type="hidden" id="fee_quantity_deduction_<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
_<?php echo $_smarty_tpl->tpl_vars['fee']->value['fee_id'];?>
" name="fee_quantity_deduction_<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
[]" value="1"/>
                    <input style="width: 90px;" type="text" class="text-right fee_unit_price_deduction money_tui" id="fee_unit_price_deduction_<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
_<?php echo $_smarty_tpl->tpl_vars['fee']->value['fee_id'];?>
" child_id="<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
" fee_id="<?php echo $_smarty_tpl->tpl_vars['fee']->value['fee_id'];?>
"
                           name="fee_unit_price_deduction_<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
[]" value="" step="500" placeholder="(<?php echo @constant('MONEY_UNIT');?>
)"/>
                <?php } else { ?>
                    <input style="width: 35px;" type="number" class="text-center fee_quantity_deduction" id="fee_quantity_deduction_<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
_<?php echo $_smarty_tpl->tpl_vars['fee']->value['fee_id'];?>
" child_id="<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
" fee_id="<?php echo $_smarty_tpl->tpl_vars['fee']->value['fee_id'];?>
"
                           name="fee_quantity_deduction_<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
[]" value="<?php echo $_smarty_tpl->tpl_vars['fee']->value['quantity_deduction'];?>
" step="1"/>(<?php echo __('Daily');?>
)
                    <input type="hidden"  id="fee_unit_price_deduction_<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
_<?php echo $_smarty_tpl->tpl_vars['fee']->value['fee_id'];?>
" name="fee_unit_price_deduction_<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
[]" value="<?php echo $_smarty_tpl->tpl_vars['fee']->value['unit_price_deduction'];?>
"/>
                <?php }?>
            </td>
            <td><input style="width: 100px; background-color: #EEEEEE" type="text" class="text-right money_tui" id="fee_amount_<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
_<?php echo $_smarty_tpl->tpl_vars['fee']->value['fee_id'];?>
" name="fee_amount_<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
[]" value="<?php echo $_smarty_tpl->tpl_vars['fee']->value['amount'];?>
" readonly/></td>
            <td class="text-center"><button class="btn-danger js_del_tuition_item" child_id="<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
"><?php echo __("Delete");?>
</button></td>
        </tr>
        <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

    
    <tr id="fee_checkpoint_<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
"></tr>

    
    
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['child']->value['services'], 'service');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['service']->value) {
?>
        <tr>
            <td class="text-center">
                <?php echo $_smarty_tpl->tpl_vars['idx']->value;?>

                <input type="hidden" class="service_id_<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
" name="service_id_<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
[]" value="<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
"/>
            </td>
            <td>
                <?php echo $_smarty_tpl->tpl_vars['service']->value['service_name'];?>

                <?php if (isset($_smarty_tpl->tpl_vars['service']->value['monthly_daily_deduction']) && ($_smarty_tpl->tpl_vars['service']->value['monthly_daily_deduction'] > 0)) {?>
                    <i>(<?php echo __("Deduction");?>
: <?php echo $_smarty_tpl->tpl_vars['service']->value['monthly_daily_deduction'];?>
)</i>
                <?php }?>
            </td>
            <td>
                <input  type="number" id="service_quantity_<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
_<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
" child_id="<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
" service_id="<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
" name="service_quantity_<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
[]"
                        <?php if ($_smarty_tpl->tpl_vars['service']->value['service_type'] == @constant('SERVICE_TYPE_MONTHLY')) {?>class="text-center service_quantity" value="1" readonly style="width: 35px; background-color:#EEEEEE"<?php } else { ?>class="text-center service_quantity day_of_child_val_<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
" data-type="2" value="<?php echo $_smarty_tpl->tpl_vars['service']->value['quantity'];?>
" step="1" style="width: 35px;"<?php }?>/>
                <input type="hidden" id="service_type_<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
_<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
" name="service_type_<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
[]" value="<?php echo $_smarty_tpl->tpl_vars['service']->value['service_type'];?>
"/>
            </td>
            <td>
                <input style="width: 100px;" type="text" class="text-right service_unit_price money_tui" id="service_unit_price_<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
_<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
" child_id="<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
" service_id="<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
"
                       name="service_unit_price_<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
[]" value="<?php echo $_smarty_tpl->tpl_vars['service']->value['fee'];?>
"/>
                <?php if (($_smarty_tpl->tpl_vars['service']->value['service_type'] != @constant('SERVICE_TYPE_MONTHLY')) && ($_smarty_tpl->tpl_vars['service']->value['fee'] != $_smarty_tpl->tpl_vars['service']->value['unit_price_deduction'])) {?>
                    <br/><?php echo __('Pm');?>
: <?php echo moneyFormat($_smarty_tpl->tpl_vars['service']->value['unit_price_deduction']);?>

                <?php }?>
            </td>
            <td>
                <?php if ($_smarty_tpl->tpl_vars['service']->value['service_type'] == @constant('SERVICE_TYPE_MONTHLY')) {?>
                    <input  style="width: 90px;" type="text" class="text-right service_unit_price_deduction money_tui" id="service_unit_price_deduction_<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
_<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
" child_id="<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
"
                            service_id="<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
" name="service_unit_price_deduction_<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
[]" value="" step="1" placeholder="(<?php echo @constant('MONEY_UNIT');?>
)"/>
                    <input type="hidden" name="service_quantity_deduction_<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
[]" value="1"/>
                <?php } else { ?>
                    <input  style="width: 35px;" type="number" class="text-center service_quantity_deduction" id="service_quantity_deduction_<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
_<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
" child_id="<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
"
                            service_id="<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
" name="service_quantity_deduction_<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
[]" value="<?php echo $_smarty_tpl->tpl_vars['service']->value['quantity_deduction'];?>
" step="1"/>(<?php echo __('Daily');?>
)
                    <input type="hidden" id="service_unit_price_deduction_<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
_<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
" name="service_unit_price_deduction_<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
[]" value="<?php echo $_smarty_tpl->tpl_vars['service']->value['unit_price_deduction'];?>
"/>
                <?php }?>
            </td>
            <td>
                <input style="width: 100px; background-color: #EEEEEE" type="text" class="text-right money_tui" id="service_amount_<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
_<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
" name="service_amount_<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
[]" value="<?php echo $_smarty_tpl->tpl_vars['service']->value['amount'];?>
" readonly/>
            </td>
            <td align="center"><button class="btn-danger js_del_tuition_item" child_id="<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
"><?php echo __("Delete");?>
</button></td>
        </tr>
        <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

    <tr id="service_checkpoint_<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
"></tr>

    
    
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['child']->value['cbservices'], 'service');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['service']->value) {
?>
        <tr>
            <td class="text-center">
                <?php echo $_smarty_tpl->tpl_vars['idx']->value;?>

                <input type="hidden" class="service_id_<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
" name="service_id_<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
[]" value="<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
"/>
            </td>
            <td><?php echo $_smarty_tpl->tpl_vars['service']->value['service_name'];?>
</td>
            <td>
                <input style="width: 35px;" type="number" class="text-center service_quantity" id="service_quantity_<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
_<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
" child_id="<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
" service_id="<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
"
                       name="service_quantity_<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
[]" value="<?php echo $_smarty_tpl->tpl_vars['service']->value['CNT'];?>
" step="1"/>
                <input type="hidden" id="service_type_<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
_<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
" name="service_type_<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
[]" value="<?php echo @constant('SERVICE_TYPE_COUNT_BASED');?>
"/>
            </td>
            <td>
                <input style="width: 100px;" type="text" class="text-right service_unit_price money_tui" id="service_unit_price_<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
_<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
" child_id="<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
" service_id="<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
"
                       name="service_unit_price_<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
[]" value="<?php echo $_smarty_tpl->tpl_vars['service']->value['fee'];?>
"/>
                <input type="hidden" id="service_unit_price_deduction_<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
_<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
" name="service_unit_price_deduction_<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
[]" value="<?php echo $_smarty_tpl->tpl_vars['service']->value['fee'];?>
"/>
            </td>
            <td>
                <input  style="width: 35px;" type="number" class="text-center service_quantity_deduction" id="service_quantity_deduction_<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
_<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
" child_id="<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
"
                        service_id="<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
" name="service_quantity_deduction_<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
[]" value="0"/>(<?php echo __('Times');?>
)
            </td>
            <td>
                <input style="width: 100px; background-color: #EEEEEE" type="text" class="text-right money_tui" id="service_amount_<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
_<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
" name="service_amount_<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
[]" value="<?php echo $_smarty_tpl->tpl_vars['service']->value['amount'];?>
" readonly/>
            </td>
            <td align="center"><button class="btn-danger js_del_tuition_item" child_id="<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
"><?php echo __("Delete");?>
</button></td>
        </tr>
        <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>


    
    <?php if ($_smarty_tpl->tpl_vars['child']->value['pickup'] > 0) {?>
        <tr>
            <td class="text-center">
                <?php echo $_smarty_tpl->tpl_vars['idx']->value;?>

                <input type="hidden" name="pickup_<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
" value="1"/>
                <input type="hidden" class="service_id_<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
" name="service_id_<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
[]" value="0"/>
                <input type="hidden" name="service_type_<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
[]" value="0"/>
                <input type="hidden" id="service_quantity_<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
_0" name="service_quantity_<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
[]" value="1"/>
                <input type="hidden" id="service_quantity_deduction_<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
_0" name="service_quantity_deduction_<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
[]" value="0"/>
                <input type="hidden" id="service_unit_price_<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
_0" name="service_unit_price_<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
[]" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['pickup'];?>
"/>
                <input type="hidden" id="service_unit_price_deduction_<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
_0" name="service_unit_price_deduction_<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
[]" value="0"/>
            </td>
            <td colspan="4"><?php echo __('Late PickUp');?>
</td>
            <td><input style="width: 100px;" type="text" class="text-right tuition_pick_up money_tui" step="1" id="service_amount_<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
_0" name="service_amount_<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
[]" child_id="<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['pickup'];?>
"/></td>
            <td align="center"><button class="btn-danger js_del_tuition_item" child_id="<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
"><?php echo __("Delete");?>
</button></td>
        </tr>
        <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
    <?php }?>
    <tr>
        <td class="text-center"><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</td>
        <td colspan="4"><?php echo __('Debt amount of previous month');?>
&nbsp;<?php echo $_smarty_tpl->tpl_vars['child']->value['pre_month'];?>
</td>
        <td>
            <input style="width: 100px;" type="text" class="text-right tuition_debt_amount money_tui" id="debt_amount_<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
" name="debt_amount[]" child_id="<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['pre_month_debt_amount'];?>
"/>
        </td>
        <td></td>
    </tr>
    <tr>
        <td colspan="6">
            <div class="pull-right flip">
                <strong>
                    <?php echo __("Total");?>
(<?php echo @constant('MONEY_UNIT');?>
)&nbsp;
                    <input style="width: 100px; background-color: #EEEEEE" type="text" class="text-right money_tui" id="total_<?php echo $_smarty_tpl->tpl_vars['childId']->value;?>
" name="child_total[]" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_total_amount'];?>
" readonly/>
                </strong>
            </div>
        </td>
        <td></td>
    </tr>
    <tr>
        <td colspan="2" class="text-right"><strong><?php echo __("Description");?>
</strong></td>
        <td colspan="5" ><input type="text" name="detail_description[]" style="width: 100%" placeholder="<?php echo __("Detail description for this tuition (if necessary)");?>
" value="" maxlength="300"/></td>
    </tr>
    </tbody>
</table>

<?php echo '<script'; ?>
>
    function stateChange() {
        setTimeout(function () {
            $('body .money_tui').each(function () {
                var a = $(this).val();
                a = a.toString().replace(/^-,/, '-');
                $(this).val(a);
            });
        }, 1000);
    }
    stateChange();
<?php echo '</script'; ?>
>
<?php }
}
