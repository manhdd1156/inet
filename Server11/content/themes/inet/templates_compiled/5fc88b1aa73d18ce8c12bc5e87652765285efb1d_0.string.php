<?php
/* Smarty version 3.1.31, created on 2021-06-22 12:36:52
  from "5fc88b1aa73d18ce8c12bc5e87652765285efb1d" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_60d176f49da515_98650844',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60d176f49da515_98650844 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<html lang="en" >
<head>
  <meta charset="UTF-8">
  <title>CodePen - CSS-only numbered lists with &quot;drop&quot; shapes</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Montserrat:700'>
<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Space+Mono'>
<style>
:root {
    --bg: #fdfdfd;
    --highlight1: #ED4264;
    --highlight2: #FFEDBC;
    --color: #1a1e24;
    --font-number: Montserrat, Roboto, Helvetica, Arial, sans-serif;
    --font-head: "Space Mono", Consolas, Menlo, Monaco, "Courier New", monospace;
    --font: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
}

.list {
    list-style: none;
    width: 600px;
    max-width: 90%;
}

.item {
    display: block;
    clear: both;
    counter-increment: list;
    padding-bottom: 4rem;
    font-size: 1.1rem;
    line-height: 1.375;
    position: relative;
}

.item:before {
    font: bold 2.25rem/1 var(--font-number);
    content: counter(list);
    width: 5rem;
    height: 5rem;
    float: left;
    margin: 0 1.5rem 0.75rem 0;
    color: var(--bg);
    background: var(--highlight1) linear-gradient(to bottom right, var(--highlight1) 25%, var(--highlight2));
    text-shadow: 0 0 2px var(--highlight1);
    border-radius: 50%;
    display: inline-flex;
    align-items: center;
    justify-content: center;
    shape-outside: ellipse();
    z-index: 1;
}

.item:after {
    width: 2.5rem;
    height: 2.5rem;
    position: absolute;
    top: 0;
    left: 0;
    content: "";
    background: var(--highlight1);
    z-index: -1;
    border-top-left-radius: 3px;
}


.headline {
    padding: 0rem 0 0 0;
    margin: 0 0 1rem 0;
    font: normal 2rem var(--font-head);
}


/* Demo styles */

body {
    width: 100%;
    height: 100%;
    min-height: 100vh;
    display: flex;
    justify-content: center;
    align-items: center;
    background: var(--bg);
    font-family: var(--font);
    padding: 4rem 0;
}
</style>

</head>
<body>
<!-- partial:index.partial.html -->
<ol class="list">
  <li class="item">
    <h2 class="headline">Hướng dẫn sử dụng đối với nhà trường</h2><span><iframe src="https://docs.google.com/document/d/e/2PACX-1vQSGjoCTp6i-fd4pE1vXIhthggS3OOFPNWs8XDzcSkKMsTMECbM2h9Is4w-ke0ucw/pub?embedded=true"></iframe></span>
  </li>
  <li class="item">
    <h2 class="headline">Number two</h2><span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veritatis, distinctio ad corporis, laboriosam unde provident, architecto tenetur ea odio debitis delectus explicabo eum obcaecati vitae facere iusto laborum consequuntur neque.</span>
  </li>
  <li class="item">
    <h2 class="headline">Number three</h2><span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veritatis, distinctio ad corporis, laboriosam unde provident, architecto tenetur ea odio debitis delectus explicabo eum obcaecati vitae facere iusto laborum consequuntur neque.</span>
  </li>
</ol>
<!-- partial -->
  
</body>
</html>
<?php }
}
