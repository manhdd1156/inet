<?php
/* Smarty version 3.1.31, created on 2021-05-24 17:16:08
  from "D:\workplace\Inet-project\source\Server11\content\themes\inet\templates\ci\school\school.classes.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_60ab7ce8729eb3_44833009',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'd2812898787ca0fc63b6bdf2cb02a87b3b1fc62f' => 
    array (
      0 => 'D:\\workplace\\Inet-project\\source\\Server11\\content\\themes\\inet\\templates\\ci\\school\\school.classes.tpl',
      1 => 1621501239,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:ci/school/ajax.school.addnewteacher.tpl' => 2,
    'file:ci/ajax.teacherlist.tpl' => 1,
    'file:ci/school/ajax.school.graduate.tpl' => 1,
  ),
),false)) {
function content_60ab7ce8729eb3_44833009 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <?php if (($_smarty_tpl->tpl_vars['sub_view']->value == '') && $_smarty_tpl->tpl_vars['canEdit']->value) {?>
            <div class="pull-right flip">
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/classes/add" class="btn btn-default">
                    <i class="fa fa-plus"></i> <?php echo __("Add New Class");?>

                </a>
                <?php if ($_smarty_tpl->tpl_vars['school']->value['school_step'] == SCHOOL_STEP_FINISH) {?>
                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/classes/classup" class="btn btn-default">
                        <i class="fa fa-arrow-circle-up"></i> <?php echo __("Class up");?>

                    </a>
                <?php }?>
            </div>
        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "classup" || $_smarty_tpl->tpl_vars['sub_view']->value == "add" || $_smarty_tpl->tpl_vars['sub_view']->value == "edit") {?>
            <div class="pull-right flip">
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/classes" class="btn btn-default">
                    <i class="fa fa-list"></i> <?php echo __("Lists");?>

                </a>
            </div>
        <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['school']->value['school_step'] == SCHOOL_STEP_FINISH) {?>
            <div class="pull-right flip" style="margin-right: 5px">
                <a href="https://blog.coniu.vn/huong-dan-tao-them-mot-lop-moi/" class="btn btn-info btn_guide" target="_blank">
                    <i class="fa fa-info-circle"></i> <?php echo __("Guide");?>

                </a>
            </div>
        <?php }?>
        <i class="fa fa-graduation-cap fa-fw fa-lg pr10"></i>
        <?php echo __("Class");?>

        <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == "edit") {?>
            &rsaquo; <?php echo $_smarty_tpl->tpl_vars['data']->value['group_title'];?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
            &rsaquo; <?php echo __('Add New');?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
            &rsaquo; <?php echo __('Lists');?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "classup") {?>
            &rsaquo; <?php echo __('Class up');?>

        <?php }?>
    </div>
    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
        <div class="panel-body with-table">
            <div class="mb5"><strong><?php echo __("Class list");?>
&nbsp;(<?php echo count($_smarty_tpl->tpl_vars['rows']->value);?>
 <?php echo __("Class");?>
)</strong></div>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th width="5%">#</th>
                            <th width="20%"><?php echo __("Class");?>
</th>
                            <th width="30%"><?php echo __("Teacher");?>
</th>
                            <th width="10%"><?php echo __("Total");?>
</th>
                            <th width="20%"><?php echo __("Class level");?>
</th>
                            
                            <th width="15%"><?php echo __("Actions");?>
</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['rows']->value, 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
                            <tr>
                                <td class="align-middle" align="center"><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</td>
                                <td class="align-middle"><?php echo $_smarty_tpl->tpl_vars['row']->value['group_title'];?>
</td>
                                <td class="align-middle">
                                    <?php if (count($_smarty_tpl->tpl_vars['row']->value['teachers']) == 0) {?>
                                        <?php echo __("No teacher");?>

                                    <?php } else { ?>
                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['row']->value['teachers'], 'teacher');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['teacher']->value) {
?>
                                            <span class="name js_user-popover" data-uid="<?php echo $_smarty_tpl->tpl_vars['teacher']->value['user_id'];?>
">
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['teacher']->value['user_name'];?>
"><?php echo $_smarty_tpl->tpl_vars['teacher']->value['user_fullname'];?>
</a>
                                            </span>
                                            <?php if ($_smarty_tpl->tpl_vars['teacher']->value['user_id'] != $_smarty_tpl->tpl_vars['user']->value->_data['user_id']) {?>
                                                <a href="#" class="far fa-comments fa-lg js_chat-start" data-name="<?php echo $_smarty_tpl->tpl_vars['teacher']->value['user_fullname'];?>
" data-uid="<?php echo $_smarty_tpl->tpl_vars['teacher']->value['user_id'];?>
"></a>
                                            <?php }?>
                                            &nbsp;&nbsp;&nbsp;
                                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                    <?php }?>
                                </td class="align-middle">
                                <td class="align-middle" align="center"><?php echo $_smarty_tpl->tpl_vars['row']->value['cnt'];?>
 <?php echo __("children");?>
</td>
                                <td class="align-middle"><?php echo $_smarty_tpl->tpl_vars['row']->value['class_level_name'];?>
</td>
                                
                                <td class="align-middle">
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/groups/<?php echo $_smarty_tpl->tpl_vars['row']->value['group_name'];?>
"><?php echo __("Timeline");?>
</a>
                                    <?php if ($_smarty_tpl->tpl_vars['canEdit']->value) {?>
                                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/classes/edit/<?php echo $_smarty_tpl->tpl_vars['row']->value['group_id'];?>
" class="btn btn-xs btn-default"><?php echo __("Edit");?>
</a>
                                        <?php if ($_smarty_tpl->tpl_vars['school']->value['school_step'] == SCHOOL_STEP_FINISH) {?>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/classes/graduate/<?php echo $_smarty_tpl->tpl_vars['row']->value['group_id'];?>
" class="btn btn-xs btn-default"><?php echo __("Graduate");?>
</a>
                                        <?php }?>
                                        <?php if ((!$_smarty_tpl->tpl_vars['row']->value['cnt'])) {?>
                                            <button class="btn btn-xs btn-danger js_school-delete" data-handle="class" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['group_id'];?>
"><?php echo __("Delete");?>
</button>
                                        <?php }?>
                                    <?php }?>
                                </td>
                            </tr>
                            <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>


                        <?php if (count($_smarty_tpl->tpl_vars['rows']->value) == 0) {?>
                            <tr class="odd">
                                <td valign="top" align="center" colspan="6" class="dataTables_empty">
                                    <?php echo __("No data available in table");?>

                                </td>
                            </tr>
                        <?php }?>
                    </tbody>
                </table>
            </div>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "edit") {?>
        <div class="panel-body">
            <div id="open_dialog_teacher_add" class="x-hidden" title="<?php echo mb_strtoupper(__("Add new teacher"), 'UTF-8');?>
">
                <?php $_smarty_tpl->_subTemplateRender("file:ci/school/ajax.school.addnewteacher.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

            </div>
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_class.php">
                <input type="hidden" name="school_username" id="school_username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                <input type="hidden" name="class_id" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['group_id'];?>
"/>
                <input type="hidden" name="do" value="edit"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">
                        <?php echo __("Class name");?>
 (*)
                    </label>
                    <div class="col-sm-9">
                        <input class="form-control" name="title" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['group_title'];?>
" placeholder="<?php echo __("Class name");?>
" required autofocus maxlength="255">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">
                        <?php echo __("Class level");?>
 (*)
                    </label>
                    <div class="col-sm-9">
                        <select class="form-control" name="class_level_id">
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['class_levels']->value, 'class_level');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class_level']->value) {
?>
                                <option <?php if ($_smarty_tpl->tpl_vars['data']->value['class_level_id'] == $_smarty_tpl->tpl_vars['class_level']->value['class_level_id']) {?>selected<?php }?> value="<?php echo $_smarty_tpl->tpl_vars['class_level']->value['class_level_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['class_level']->value['class_level_name'];?>
</option>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">
                        <?php echo __("Teacher");?>

                    </label>
                    <div class="col-sm-9">
                        <input name="search-teacher" id="search-teacher" type="text" class="form-control" placeholder="<?php echo __("Enter username, email or fullname to search");?>
" autocomplete="off">
                        <div id="search-teacher-results" class="dropdown-menu dropdown-widget dropdown-search">
                            <div class="dropdown-widget-header">
                                <?php echo __("Search Results");?>

                            </div>
                            <div class="dropdown-widget-body">
                                <div class="loader loader_small ptb10"></div>
                            </div>
                        </div>
                        <br/>
                        <div class="col-sm-9" id="teacher_list" name="teacher_list">
                            <?php if (count($_smarty_tpl->tpl_vars['teachers']->value) > 0) {?>
                                <?php $_smarty_tpl->_subTemplateRender('file:ci/ajax.teacherlist.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('results'=>$_smarty_tpl->tpl_vars['teachers']->value), 0, false);
?>

                            <?php } else { ?>
                                <?php echo __("No teacher");?>

                            <?php }?>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-3">
                        <a style="margin-left: 10px" class="btn btn-xs btn-default js_school-teacher-add"><i class="fa fa-plus"></i> <?php echo __("Add new teacher");?>
</a>
                    </div>
                </div>
                
                    
                        
                    
                    
                        
                    
                
                
                    
                        
                    
                    
                        
                    
                
                
                    
                        
                    
                    
                        
                    
                
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">
                        <?php echo __("Description");?>

                    </label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="description" id="description" placeholder="<?php echo __("Write about your class...");?>
" rows="3"><?php echo $_smarty_tpl->tpl_vars['data']->value['group_description'];?>
</textarea>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30 js_show-loading"><?php echo __("Save");?>
</button>
                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/classes" class="btn btn-default"><?php echo __("Lists");?>
</a>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->
                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
        <div class="panel-body">
            <div id="open_dialog_teacher_add" class="x-hidden" title="<?php echo mb_strtoupper(__("Add new teacher"), 'UTF-8');?>
">
                <?php $_smarty_tpl->_subTemplateRender("file:ci/school/ajax.school.addnewteacher.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

            </div>
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_class.php">
                <input type="hidden" name="school_username" id="school_username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                <input type="hidden" name="do" value="add"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">
                        <?php echo __("Class name");?>
 (*)
                    </label>
                    <div class="col-sm-9">
                        <input class="form-control" name="title" placeholder="<?php echo __("Class name");?>
" required autofocus maxlength="255">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">
                        <?php echo __("Class level");?>
 (*)
                    </label>
                    <div class="col-sm-9">
                        <select class="form-control" name="class_level_id">
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['class_levels']->value, 'class_level');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class_level']->value) {
?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['class_level']->value['class_level_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['class_level']->value['class_level_name'];?>
</option>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </select>
                    </div>
                </div>
                <?php if ($_smarty_tpl->tpl_vars['school']->value['school_step'] == SCHOOL_STEP_FINISH || $_smarty_tpl->tpl_vars['school']->value['grade'] != 0) {?>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">
                            <?php echo __("Teacher");?>

                        </label>
                        <div class="col-sm-9">
                            <input name="search-teacher" id="search-teacher" type="text" class="form-control" placeholder="<?php echo __("Enter username, email or fullname to search");?>
" autocomplete="off">
                            <div id="search-teacher-results" class="dropdown-menu dropdown-widget dropdown-search">
                                <div class="dropdown-widget-header">
                                    <?php echo __("Search Results");?>

                                </div>
                                <div class="dropdown-widget-body">
                                    <div class="loader loader_small ptb10"></div>
                                </div>
                            </div>
                            <br/>
                            <div class="col-sm-9" id="teacher_list" name="teacher_list"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-3">
                            <a style="margin-left: 10px" class="btn btn-xs btn-default js_school-teacher-add"><i class="fa fa-plus"></i> <?php echo __("Add new teacher");?>
</a>
                        </div>
                    </div>
                <?php }?>
                
                    
                        
                    
                    
                        
                    
                
                
                    
                        
                    
                    
                        
                    
                
                
                    
                        
                    
                    
                        
                    
                
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">
                        <?php echo __("Description");?>

                    </label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="description" id="description" placeholder="<?php echo __("Write about your class...");?>
" rows="3"></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30 js_show-loading"><?php echo __("Save");?>
</button>
                    </div>
                </div>

                
                
                

                
                
                
            </form>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "classup") {?>
        <div class="panel-body">
            <form class="js_ajax-forms-success-reload form-horizontal" data-url="ci/bo/school/bo_class.php">
                <input type="hidden" name="school_username" id="school_username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                <input type="hidden" name="class_id" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['group_id'];?>
"/>
                <input type="hidden" name="do" value="class_up"/>
                <div class="form-group">
                    <div align="center"><strong><?php echo __("Lưu ý: Nếu bạn muốn chuyển lên lớp đã tồn tại trước đó, bạn phải cho lớp đó lên lớp hoặc tốt nghiệp trước.");?>
</strong></div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Select Class");?>
 (*)</label>
                    <div class="col-sm-4">
                        <select name="class_id" id="classup_class_id" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" class="form-control" autofocus>
                            <option value=""><?php echo __("Select class");?>
</option>
                            <?php $_smarty_tpl->_assignInScope('class_level', -1);
?>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['classes']->value, 'class');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class']->value) {
?>
                                <?php if (($_smarty_tpl->tpl_vars['class_level']->value != $_smarty_tpl->tpl_vars['class']->value['class_level_id'])) {?>
                                    <option value="" disabled style="color: blue">-----<?php echo $_smarty_tpl->tpl_vars['class']->value['class_level_name'];?>
-----</option>
                                <?php }?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['class']->value['group_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['class']->value['group_title'];?>
</option>
                                <?php $_smarty_tpl->_assignInScope('class_level', $_smarty_tpl->tpl_vars['class']->value['class_level_id']);
?>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("New class title");?>
 (*)</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="new_title" value="" required maxlength="255">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">
                        <?php echo __("Class level");?>
 (*)
                    </label>
                    <div class="col-sm-4">
                        <select class="form-control" name="class_level_id">
                            <option value=""><?php echo __("Select class level");?>
</option>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['class_levels']->value, 'class_level');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class_level']->value) {
?>
                                <option <?php if ($_smarty_tpl->tpl_vars['data']->value['class_level_id'] == $_smarty_tpl->tpl_vars['class_level']->value['class_level_id']) {?>selected<?php }?> value="<?php echo $_smarty_tpl->tpl_vars['class_level']->value['class_level_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['class_level']->value['class_level_name'];?>
</option>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30 js_show-loading"><?php echo __("Save");?>
</button>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->
                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "graduate") {?>
        <div class="panel-body">
            <form class="js_ajax-forms-confirm form-horizontal" data-url="ci/bo/school/bo_class.php">
                <input type="hidden" id="school_username" name="school_username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                <input type="hidden" id="class_id" name="class_id" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['group_id'];?>
"/>
                <input type="hidden" id="class_level_id" name="class_level_id" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['class_level_id'];?>
"/>
                <input type="hidden" name="do" value="class_leave_school"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Class");?>
</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="group_title" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['group_title'];?>
" disabled>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Leaving date");?>
 (*)</label>
                    <div class='col-sm-3 text-left'>
                        <div class='input-group date' id='class_leave_datepicker'>
                            <input type='text' name="class_end_at" id="class_end_at" class="form-control" placeholder="<?php echo __("Leaving date");?>
"/>
                            <span class="input-group-addon">
                            <span class="fas fa-calendar-alt"></span>
                        </span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30"><?php echo __("Save");?>
</button>
                    </div>
                </div>
                <div class="table-responsive" id="class_children_info">
                    <?php $_smarty_tpl->_subTemplateRender("file:ci/school/ajax.school.graduate.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                </div>
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
            </form>
        </div>
    <?php }?>
</div><?php }
}
