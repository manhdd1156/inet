<?php
/* Smarty version 3.1.31, created on 2021-04-24 10:05:32
  from "D:\workplace\mascom-edu-server\Server11\content\themes\inet\templates\ci\school\ajax.reportlist.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_60838afcdc6401_58566357',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '6ac2b996ca6881bccd22a8708f8f1b9315ac1fc6' => 
    array (
      0 => 'D:\\workplace\\mascom-edu-server\\Server11\\content\\themes\\inet\\templates\\ci\\school\\ajax.reportlist.tpl',
      1 => 1552404702,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60838afcdc6401_58566357 (Smarty_Internal_Template $_smarty_tpl) {
?>
<table class="table table-striped table-bordered table-hover js_dataTable">
    <thead>
    <tr><th colspan="7"><?php echo __("Report list");?>
&nbsp;(<span class="count_report"><?php echo count($_smarty_tpl->tpl_vars['result']->value);?>
</span>) - <?php echo __("Not notified yet");?>
 (<span class="count_not_notify"><?php echo $_smarty_tpl->tpl_vars['countNotNotify']->value;?>
</span>)</th></tr>
    <tr>
        <th><?php echo __("#");?>
</th>
        <th><?php echo __("Title");?>
</th>
        <th><?php echo __("Child");?>
</th>
        
        <th><?php echo __("Class");?>
</th>
        
        <th data-orderable="false" align="center" style="width: 100px">
            <input type="checkbox" id="report_notify_checkall">
            <button class="btn btn-xs btn-default" id="js_school-report-notify-all-select" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-handle="notify_all_select"><?php echo __("Notify");?>
</button>
        </th>
        <?php if ($_smarty_tpl->tpl_vars['canEdit']->value) {?>
            <th align="center" style="width: 100px">
                <input type="checkbox" id="report_checkall">
                <button class="btn btn-xs btn-danger" id="js_school-report-delete-all-select" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-handle="delete_all_select"><?php echo __("Delete");?>
</button>
            </th>
        <?php }?>
    </tr>
    </thead>
    <tbody>
        <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['result']->value, 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
            <tr <?php if ($_smarty_tpl->tpl_vars['row']->value['child_status'] == 0) {?> class="row-disable" <?php }?>>
                <td class="align-middle" align="center">
                    <strong><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</strong>
                </td>
                <td class="align-middle" style="word-break: break-all">
                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/reports/detail/<?php echo $_smarty_tpl->tpl_vars['row']->value['report_id'];?>
">
                        <?php echo $_smarty_tpl->tpl_vars['row']->value['report_name'];?>

                    </a>
                </td>
                <td class="align-middle">
                    <?php echo $_smarty_tpl->tpl_vars['row']->value['child_name'];?>

                </td>
                
                    
                
                <td class="align-middle">
                    <?php echo $_smarty_tpl->tpl_vars['row']->value['group_title'];?>

                </td>
                
                    
                
                <td align="center" class="align-middle">
                    <?php if ($_smarty_tpl->tpl_vars['canEdit']->value) {?>
                        <?php if (!$_smarty_tpl->tpl_vars['row']->value['is_notified']) {?>
                            <input class="report_notify" type="checkbox" name="notify_report_ids" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['report_id'];?>
">
                            <button class="btn btn-xs btn-default js_school-report-notify" data-handle="notify" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['report_id'];?>
"><?php echo __("Notify");?>
</button>
                        <?php }?>
                    <?php }?>
                </td>
                <?php if ($_smarty_tpl->tpl_vars['canEdit']->value) {?>
                    <td align="center" class="align-middle">
                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/reports/edit/<?php echo $_smarty_tpl->tpl_vars['row']->value['report_id'];?>
" class="btn btn-xs btn-default"><?php echo __("Edit");?>
</a>
                        <input class="report_delete" type="checkbox" name="report_ids" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['report_id'];?>
">
                        <button class="btn btn-xs btn-danger js_school-delete-report" data-handle="delete_report" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['report_id'];?>
" <?php if ($_smarty_tpl->tpl_vars['row']->value['is_notified']) {?>data-notify="1"<?php } else { ?>data-notify="0"<?php }?>><?php echo __("Delete");?>
</button>
                    </td>
                <?php }?>
            </tr>
            <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

    </tbody>
</table>

<?php echo '<script'; ?>
>
    $(function() {
        // run DataTable
        $('.js_dataTable').DataTable({
            "aoColumnDefs": [ { 'aDataSort': false, 'aTargets': [ -1 ] } ],
            "language": {
                "decimal":        "",
                "emptyTable":     __["No data available in table"],
                "info":           __["Showing _START_ to _END_ of _TOTAL_ results"],
                "infoEmpty":      __["Showing 0 to 0 of 0 results"],
                "infoFiltered":   "(filtered from _MAX_ total entries)",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     __["Show"] + " " + "_MENU_" + " " + __["results"],
                "loadingRecords": __["Loading..."],
                "processing":     __["Processing..."],
                "search":         __["Search"],
                "zeroRecords":    __["No matching records found"],
                "paginate": {
                    "first":      __["First"],
                    "last":       __["Last"],
                    "next":       __["Next"],
                    "previous":   __["Previous"]
                },
                "aria": {
                    "sortAscending":  ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                }
            }
        });
    });
<?php echo '</script'; ?>
>
<?php }
}
