<?php
/* Smarty version 3.1.31, created on 2021-03-31 14:04:32
  from "D:\workplace\Server11\content\themes\inet\templates\ci\childinfo\childinfo.childdetail.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_60641f0041d988_73207921',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '3d5c531467bdf66cae6a1647b39c5cf5547cbccc' => 
    array (
      0 => 'D:\\workplace\\Server11\\content\\themes\\inet\\templates\\ci\\childinfo\\childinfo.childdetail.tpl',
      1 => 1552404704,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:ci/ajax.parentlist.tpl' => 1,
  ),
),false)) {
function content_60641f0041d988_73207921 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <div class="pull-right flip">
            <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/childinfo/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_parent_id'];?>
/childdetail/edit" class="btn btn-default">
                    <i class="fa fas fa-edit"></i> <?php echo __("Edit");?>

                </a>
                <a href="#" class="btn btn-danger js_child-delete" data-id="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_parent_id'];?>
">
                    <i class="fa fa-eraser"></i> <?php echo __("Delete student");?>

                </a>
            <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "edit") {?>
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/childinfo/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_parent_id'];?>
/childdetail" class="btn btn-default">
                    <i class="fa fa-info"></i> <?php echo __("Detail");?>

                </a>
            <?php }?>
        </div>
        <i class="fa fa-child fa-fw fa-lg pr10"></i>
        <?php echo __("Child");?>
 &rsaquo; <?php echo $_smarty_tpl->tpl_vars['child']->value['child_name'];?>

        <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == "edit") {?>
            &rsaquo; <?php echo __('Edit');?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
            &rsaquo; <?php echo __('Detail');?>

        <?php }?>
    </div>
    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
        <div class="panel-body with-table">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <tbody>
                    <tr>
                        <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Full name");?>
</strong></td>
                        <td><?php echo $_smarty_tpl->tpl_vars['child']->value['child_name'];?>
</td>
                    </tr>
                    <tr>
                        <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Nickname");?>
</strong></td>
                        <td><?php echo $_smarty_tpl->tpl_vars['child']->value['child_nickname'];?>
</td>
                    </tr>
                    <tr>
                        <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Child code");?>
</strong></td>
                        <td><?php echo $_smarty_tpl->tpl_vars['child']->value['child_code'];?>
</td>
                    </tr>
                    <?php if ($_smarty_tpl->tpl_vars['child']->value['is_pregnant'] == 0) {?>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Gender");?>
</strong></td>
                            <td><?php if ($_smarty_tpl->tpl_vars['child']->value['gender'] == @constant('MALE')) {
echo __("Male");
} else {
echo __("Female");
}?></td>
                        </tr>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Birthdate");?>
</strong></td>
                            <td><?php echo $_smarty_tpl->tpl_vars['child']->value['birthday'];?>
</td>
                        </tr>
                    <?php } else { ?>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Pregnant week");?>
</strong></td>
                            <td><?php if ($_smarty_tpl->tpl_vars['child']->value['pregnant_week'] > 0) {
echo $_smarty_tpl->tpl_vars['child']->value['pregnant_week'];
}?></td>
                        </tr>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Due date of childbearing");?>
</strong></td>
                            <td><?php echo $_smarty_tpl->tpl_vars['child']->value['due_date_of_childbearing'];?>
</td>
                        </tr>
                    <?php }?>
                    <tr>
                        <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Parent");?>
</strong></td>
                        <td>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['childInfo']->value['parent'], '_user');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['_user']->value) {
?>
                                <span class="name js_user-popover" data-uid="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_id'];?>
">
                                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_name'];?>
"><?php echo $_smarty_tpl->tpl_vars['_user']->value['user_fullname'];?>
</a>
                                    </span>
                                <?php if ($_smarty_tpl->tpl_vars['_user']->value['user_id'] != $_smarty_tpl->tpl_vars['user']->value->_data['user_id']) {?>
                                    <a href="#" class="far fa-comments fa-lg js_chat-start" data-name="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_fullname'];?>
" data-uid="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_id'];?>
"></a>
                                <?php }?>
                                <br/>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </td>
                    </tr>
                    <tr>
                        <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Mother's name");?>
</strong></td>
                        <td><?php echo $_smarty_tpl->tpl_vars['child']->value['parent_name'];?>
</td>
                    </tr>
                    <tr>
                        <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Telephone");?>
</strong></td>
                        <td><?php echo $_smarty_tpl->tpl_vars['child']->value['parent_phone'];?>
</td>
                    </tr>
                    <tr>
                        <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Job");?>
</strong></td>
                        <td><?php echo $_smarty_tpl->tpl_vars['child']->value['parent_job'];?>
</td>
                    </tr>
                    <tr>
                        <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Father's name");?>
</strong></td>
                        <td><?php echo $_smarty_tpl->tpl_vars['child']->value['parent_name_dad'];?>
</td>
                    </tr>
                    <tr>
                        <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Telephone");?>
</strong></td>
                        <td><?php echo $_smarty_tpl->tpl_vars['child']->value['parent_phone_dad'];?>
</td>
                    </tr>
                    <tr>
                        <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Job");?>
</strong></td>
                        <td><?php echo $_smarty_tpl->tpl_vars['child']->value['parent_job_dad'];?>
</td>
                    </tr>
                    <tr>
                        <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Parent email");?>
</strong></td>
                        <td><?php echo $_smarty_tpl->tpl_vars['child']->value['parent_email'];?>
</td>
                    </tr>
                    <tr>
                        <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Address");?>
</strong></td>
                        <td><?php echo $_smarty_tpl->tpl_vars['child']->value['address'];?>
</td>
                    </tr>
                    <?php if (!$_smarty_tpl->tpl_vars['child']->value['is_pregnant']) {?>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Study start date");?>
</strong></td>
                            <?php if (isset($_smarty_tpl->tpl_vars['childInfo']->value['school'])) {?>
                                <td><?php echo $_smarty_tpl->tpl_vars['child']->value['begin_at'];?>
</td>
                            <?php } else { ?>
                                <td><?php echo __("Student not in school");?>
</td>
                            <?php }?>
                        </tr>
                    <?php }?>
                    <tr>
                        <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Description");?>
</strong></td>
                        <td><?php echo $_smarty_tpl->tpl_vars['child']->value['description'];?>
</td>
                    </tr>
                    <?php if (!$_smarty_tpl->tpl_vars['child']->value['is_pregnant']) {?>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Child picture");?>
</strong></td>
                            <td>
                                <a href="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_picture'];?>
" target="_blank"> <img src="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_picture'];?>
" class="img-responsive">
                                </a>
                            </td>
                        </tr>
                    <?php }?>
                    </tbody>
                </table>
            </div>
            <div class="form-group">
                <div class="col-sm-9 text-left pl0">
                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/childinfo/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_parent_id'];?>
/childdetail/edit" class="btn btn-default"><i class="fas fa-edit"></i> <?php echo __("Edit");?>
</a>
                </div>
            </div>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "edit") {?>
        <div class="panel-body kg-main">
            <form class="form-horizontal" enctype="multipart/form-data" method="post" id="edit_child_parent">
                <input type="hidden" name="school_username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                <input type="hidden" name="child_parent_id" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_parent_id'];?>
"/>
                <input type="hidden" name="child_id" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
"/>
                <input type="hidden" name="do" value="edit"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Child code");?>
</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="child_code" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_code'];?>
" disabled>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Full name");?>
 (*)</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="last_name" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['last_name'];?>
" placeholder="<?php echo __("Last name");?>
" required maxlength="34" <?php if ($_smarty_tpl->tpl_vars['child']->value['school_id'] != 0) {?>readonly<?php }?>>
                    </div>
                    <div class="col-sm-2">
                        <input type="text" class="form-control" name="first_name" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['first_name'];?>
" placeholder="<?php echo __("First name");?>
" required maxlength="15" <?php if ($_smarty_tpl->tpl_vars['child']->value['school_id'] != 0) {?>readonly<?php }?>>
                    </div>
                </div>
                <div class = "form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Nickname");?>
</label>
                    <div class="col-sm-5">
                        <input type = "text" class="form-control" name="nickname" id="nickname" placeholder="<?php echo __("Nickname");?>
" maxlength="50" value = "<?php echo $_smarty_tpl->tpl_vars['child']->value['child_nickname'];?>
">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Pregnant");?>
</label>
                    <div class="col-sm-9">
                        <div class="onoffswitch">
                            <input type="checkbox" name="child_pregnant" class="onoffswitch-checkbox" id="child_pregnant" <?php if ($_smarty_tpl->tpl_vars['child']->value['is_pregnant']) {?>checked<?php }?>>
                            <label class="onoffswitch-label" for="child_pregnant"></label>
                        </div>
                    </div>
                </div>
                <div id = "not_pregnant" <?php if ($_smarty_tpl->tpl_vars['child']->value['is_pregnant'] == 1) {?> class = "x-hidden"<?php }?>>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left"><?php echo __("Gender");?>
 (*)</label>
                        <div class="col-sm-3">
                            <select name="gender" id="gender" class="form-control" <?php if ($_smarty_tpl->tpl_vars['child']->value['school_id'] != 0) {?>disabled<?php }?>>
                                <option value="<?php echo @constant('MALE');?>
" <?php if ($_smarty_tpl->tpl_vars['child']->value['gender'] == @constant('MALE')) {?>selected<?php }?>><?php echo __("Male");?>
</option>
                                <option value="<?php echo @constant('FEMALE');?>
" <?php if ($_smarty_tpl->tpl_vars['child']->value['gender'] == @constant('FEMALE')) {?>selected<?php }?>><?php echo __("Female");?>
</option>
                            </select>
                            <?php if ($_smarty_tpl->tpl_vars['child']->value['school_id'] != 0) {?>
                                <input type="text" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['gender'];?>
" name="gender" class="hidden">
                            <?php }?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left"><?php echo __("Birthdate");?>
 (*)</label>
                        <div class='col-sm-3'>
                            <div class='input-group date' id='birthdate_picker'>
                                <input type='text' name="birthday" value = "<?php echo $_smarty_tpl->tpl_vars['child']->value['birthday'];?>
" id="birthday" class="form-control" placeholder="<?php echo __("Birthdate");?>
 (*)" <?php if ($_smarty_tpl->tpl_vars['child']->value['school_id'] != 0) {?>readonly<?php }?>/>
                                <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                            </div>
                        </div>
                    </div>
                    <input type = "hidden" id = "is_file" name = "is_file" value = "1">
                    <div class="form-group" id = "file_old">
                        <label class="rp-file col-sm-3 control-label text-left" style="padding-top: 0px"><?php echo __("Avatar");?>
</label>
                        <div class="col-sm-6">
                            <?php if (!is_empty($_smarty_tpl->tpl_vars['child']->value['child_picture'])) {?>
                                <a href="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_picture'];?>
" target="_blank"><img src = "<?php echo $_smarty_tpl->tpl_vars['child']->value['child_picture'];?>
" class = "img-responsive"></a>
                            <?php } else { ?> <?php echo __('No Avatar');?>

                            <?php }?>
                        </div>
                    </div>
                    <div class = "form-group">
                        <label class="control-label col-sm-3">
                            <?php if (is_empty($_smarty_tpl->tpl_vars['child']->value['child_picture'])) {?>
                                <?php echo __("Choose Avatar");?>

                            <?php } else { ?>
                                <?php echo __("Change Avatar");?>

                            <?php }?>
                        </label>
                        <div class = "col-sm-6">
                            <input type="file" name="file" id="file"/>
                        </div>
                        <a class = "delete_image btn btn-danger btn-xs text-left"><?php echo __('Delete');?>
</a>
                    </div>
                    
                        
                            
                        
                    
                    
                    
                    
                </div>
                <div <?php if ($_smarty_tpl->tpl_vars['child']->value['is_pregnant'] == 0) {?>class = "x-hidden"<?php }?> id = "is_pregnant">
                    <div class = "form-group">
                        <label class = "col-sm-3 control-label text-left"><?php echo __("Time");?>
</label>
                        <div class="col-sm-2">
                            <input type = "number" name = "pregnant_week" value = "<?php echo $_smarty_tpl->tpl_vars['child']->value['pregnant_week'];?>
" id = "pregnant_week" class = "form-control" placeholder="<?php echo __("Week");?>
">
                        </div>
                        <label class = "col-sm-1 control-label text-left"><?php echo __("Week");?>
(*)</label>
                    </div>
                    <div class = "form-group">
                        <label class="col-sm-3 control-label text-left"><?php echo __("Due date of childbearing");?>
</label>
                        <div class='col-sm-6'>
                            <div class='input-group date' id='due_date_picker'>
                                <input type='text' name="due_date_of_childbearing" id="due_date_of_childbearing" value = "<?php echo $_smarty_tpl->tpl_vars['child']->value['due_date_of_childbearing'];?>
" class="form-control" placeholder="<?php echo __("Due date of childbearing");?>
"/>
                                <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Parent");?>
</label>
                    <div class="col-sm-7">
                        <input name="search-parent" id="search-parent" type="text" class="form-control" placeholder="<?php echo __("Enter username, email or fullname to search");?>
" autocomplete="off">
                        <div id="search-parent-results" class="dropdown-menu dropdown-widget dropdown-search">
                            <div class="dropdown-widget-header">
                                <?php echo __("Search Results");?>

                            </div>
                            <div class="dropdown-widget-body">
                                <div class="loader loader_small ptb10"></div>
                            </div>
                        </div>
                        <div><?php echo __("Enter at least 4 characters");?>
.</div>
                        <br/>
                        <div class="col-sm-9" id="parent_list" name="parent_list">
                            <?php if (count($_smarty_tpl->tpl_vars['childInfo']->value['parent']) > 0) {?>
                                <?php $_smarty_tpl->_subTemplateRender('file:ci/ajax.parentlist.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('results'=>$_smarty_tpl->tpl_vars['childInfo']->value['parent']), 0, false);
?>

                            <?php } else { ?>
                                <?php echo __("No parent");?>

                            <?php }?>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Blood type");?>
</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="blood_type" name="blood_type" placeholder="<?php echo __("Blood type");?>
" value="<?php echo convertText4Web($_smarty_tpl->tpl_vars['child']->value['blood_type']);?>
">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Hobby");?>
</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="hobby" name="hobby" placeholder="<?php echo __("Hobby");?>
" value="<?php echo convertText4Web($_smarty_tpl->tpl_vars['child']->value['hobby']);?>
">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Allergy");?>
</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="allergy" name="allergy" placeholder="<?php echo __("Allergy");?>
" value="<?php echo convertText4Web($_smarty_tpl->tpl_vars['child']->value['allergy']);?>
">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Mother's name");?>
</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="parent_name" name="parent_name" placeholder="<?php echo __("Mother's name");?>
" value="<?php echo convertText4Web($_smarty_tpl->tpl_vars['child']->value['parent_name']);?>
" maxlength="50">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Telephone");?>
</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="parent_phone" name="parent_phone" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['parent_phone'];?>
" placeholder="<?php echo __("Telephone");?>
" maxlength="50">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Job");?>
</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="parent_job" name="parent_job" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['parent_job'];?>
" placeholder="<?php echo __("Job");?>
" maxlength="500">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Father's name");?>
</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="parent_name_dad" name="parent_name_dad" placeholder="<?php echo __("Father's name");?>
" value="<?php echo convertText4Web($_smarty_tpl->tpl_vars['child']->value['parent_name_dad']);?>
" maxlength="50">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Telephone");?>
</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="parent_phone_dad" name="parent_phone_dad" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['parent_phone_dad'];?>
" placeholder="<?php echo __("Telephone");?>
" maxlength="50">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Job");?>
</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="parent_job_dad" name="parent_job_dad" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['parent_job_dad'];?>
" placeholder="<?php echo __("Job");?>
" maxlength="500">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Parent email");?>
</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="parent_email" name="parent_email" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['parent_email'];?>
" placeholder="<?php echo __("Parent email");?>
" maxlength="100">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Address");?>
</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="address" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['address'];?>
" placeholder="<?php echo __("Address");?>
" maxlength="250">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Description");?>
</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="description" placeholder="<?php echo __("Write about your child...");?>
" maxlength="300"><?php echo $_smarty_tpl->tpl_vars['child']->value['description'];?>
</textarea>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30"><?php echo __("Save");?>
</button>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->
                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    <?php }?>
</div>
<?php }
}
