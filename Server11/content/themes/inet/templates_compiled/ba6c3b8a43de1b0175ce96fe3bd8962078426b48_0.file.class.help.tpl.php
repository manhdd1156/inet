<?php
/* Smarty version 3.1.31, created on 2021-03-31 10:26:47
  from "D:\workplace\Server11\content\themes\inet\templates\ci\class\class.help.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_6063ebf7dd6898_24333564',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ba6c3b8a43de1b0175ce96fe3bd8962078426b48' => 
    array (
      0 => 'D:\\workplace\\Server11\\content\\themes\\inet\\templates\\ci\\class\\class.help.tpl',
      1 => 1552404703,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6063ebf7dd6898_24333564 (Smarty_Internal_Template $_smarty_tpl) {
if ($_smarty_tpl->tpl_vars['view']->value == '') {
} elseif ($_smarty_tpl->tpl_vars['view']->value == "children") {?>
    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
        <div class="col-md-9 col-sm-9">
            <div class="panel panel-default">
                <div class="panel-body color_red">
                    <strong>Mỗi trẻ có thể có nhiều phụ huynh (bố, mẹ, ông, bà...). Phụ huynh được gán vào cho trẻ bằng 02 cách:</strong>
                    <br/>- Cách 1: Điền thông tin vào ô phụ huynh để Tìm & Thêm từ những tài khoản người dùng đã có trong hệ thống.
                    <br/>- Cách 2: Tích chọn ô 'Tạo tài khoản phụ huynh' để hệ thống tự tạo và thông báo cho phụ huynh qua email (trường hợp này, phải nhập chính xác email phụ huynh).
                </div>
            </div>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "import") {?>
        <div class="col-md-9 col-sm-9">
            <div class="panel panel-default">
                <div class="panel-body color_red">
                    <strong>Bạn phải sử dụng biểu mẫu file Excel đúng quy định của hệ thống, biểu mẫu tải về tại <a target="_blank" href="https://drive.google.com/file/d/1Pjo6L0raH1MSwAMjb-fDgkyHyiPLpaWi">ĐÂY</a>.</strong>
                    
                    <br/>- Phải tạo lớp trước khi nhập thông tin trẻ vào lớp đó. <?php if (canEdit($_smarty_tpl->tpl_vars['username']->value,"classes")) {?><a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/classes/add">Tạo lớp</a><?php }?>
                </div>
            </div>
        </div>
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == "addhealthindex") {?>
        <div class="col-md-9 col-sm-9">
            <div class="panel panel-default">
                <div class="panel-body color_red">
                    - Thêm chỉ số sức khỏe cho cả lớp. <br/>
                    - Kích chuột vào tên từng trẻ để thêm thông tin sức khỏe cho trẻ đó. <br/>
                    - Lưu ý: <strong>Hệ thống không ghi dữ liệu những trẻ không được thêm chiều cao hoặc cân nặng.</strong>
                </div>
            </div>
        </div>
    <?php }
} elseif ($_smarty_tpl->tpl_vars['view']->value == "healths") {?>
    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
        <div class="col-md-9 col-sm-9">
            <div class="panel panel-default">
                <div class="panel-body color_red">
                    - Thêm chỉ số sức khỏe cho cả lớp. <br/>
                    - Kích chuột vào tên từng trẻ để thêm thông tin sức khỏe cho trẻ đó. <br/>
                    - Lưu ý: <strong>Hệ thống không ghi dữ liệu những trẻ không được thêm chiều cao hoặc cân nặng.</strong>
                </div>
            </div>
        </div>
    <?php }
}
}
}
