<?php
/* Smarty version 3.1.31, created on 2021-04-20 15:31:44
  from "D:\workplace\Server11\content\themes\inet\templates\ci\school\school.reports.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_607e917087ea73_94051008',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '1f21455ca75dcb18b6fbcfc73cd6ae34fd4f7c47' => 
    array (
      0 => 'D:\\workplace\\Server11\\content\\themes\\inet\\templates\\ci\\school\\school.reports.tpl',
      1 => 1618907178,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:ci/school/ajax.reportlist.tpl' => 1,
    'file:ci/ajax.reporttemplatedetail.tpl' => 1,
    'file:ci/school/ajax.school.addcategory.tpl' => 2,
    'file:ci/school/ajax.school.categorydetail.tpl' => 2,
  ),
),false)) {
function content_607e917087ea73_94051008 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="" style="position: relative">
    <?php if ($_smarty_tpl->tpl_vars['school']->value['school_step'] == SCHOOL_STEP_FINISH || $_smarty_tpl->tpl_vars['school']->value['grade'] != 0) {?>
        <div class="dashboard_fixed">
            <button class="btn btn-xs btn-default js_school-contact-book-guide" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['user']->value->_data["user_id"];?>
">
                <i class="fa fa-chevron-down <?php if ($_smarty_tpl->tpl_vars['cookie']->value) {?>hidden<?php }?>" aria-hidden="true" id="guide_hidden"></i>
                <i class="fa fa-chevron-up <?php if (!$_smarty_tpl->tpl_vars['cookie']->value) {?>hidden<?php }?>" aria-hidden="true" id="guide_show"></i>
            </button>
        </div>
        <div class="panel panel-default <?php if ($_smarty_tpl->tpl_vars['cookie']->value == 0) {?>hidden<?php }?>" id="guide_contact">
            <div class="guide_school">
                <div class="init_school text-left pad5 full_width">
                    <div class="full_width pl5">
                        <strong><?php echo __("The execution's order guide");?>
:</strong>
                    </div>
                    <div class="pad5">
                        <strong><?php echo __("Handlings with contact book");?>
:</strong>
                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/reports/addcate" class="pad5 font12">1. <?php echo __("Add new category");?>
</a>
                        <i class="fas fa-long-arrow-alt-right" aria-hidden="true"></i>
                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/reports/addtemp" class="pad5 font12">2. <?php echo __("Add new template");?>
</a>
                        <i class="fas fa-long-arrow-alt-right" aria-hidden="true"></i>
                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/reports/add" class="pad5 font12">3. <?php echo __("Create contact book");?>
</a>
                        <i class="fas fa-long-arrow-alt-right" aria-hidden="true"></i>
                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/reports" class="pad5 font12">4. <?php echo __("List contact book");?>
</a>
                    </div>
                </div>
            </div>
        </div>
    <?php }?>
    <div class="panel panel-default">
    <div class="panel-heading with-icon">
        <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
            <?php if ($_smarty_tpl->tpl_vars['canEdit']->value) {?>
                <div class="pull-right flip">
                    <a href="https://blog.inet.vn/huong-dan-tao-so-lien-lac-tren-website/" target="_blank" class="btn btn-info btn_guide">
                        <i class="fa fa-info-circle"></i> <?php echo __("Guide");?>

                    </a>
                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/reports/add" class="btn btn-default">
                        <i class="fa fa-plus"></i> <?php echo __("Add New");?>

                    </a>
                </div>
            <?php }?>
        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "add" || $_smarty_tpl->tpl_vars['sub_view']->value == "edit") {?>
            <div class="pull-right flip">
                <a href="https://blog.inet.vn/huong-dan-tao-so-lien-lac-tren-website/" target="_blank" class="btn btn-info btn_guide">
                    <i class="fa fa-info-circle"></i> <?php echo __("Guide");?>

                </a>
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/reports" class="btn btn-default">
                    <i class="fa fa-list"></i> <?php echo __("Lists");?>

                </a>
            </div>
        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "detail" || $_smarty_tpl->tpl_vars['sub_view']->value == "add" || $_smarty_tpl->tpl_vars['sub_view']->value == "edit") {?>
            <div class="pull-right flip">
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/reports" class="btn btn-default">
                    <i class="fa fa-list-ul"></i> <?php echo __("Lists");?>

                </a>
            </div>
        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "listtemp") {?>
            <?php if ($_smarty_tpl->tpl_vars['canEdit']->value) {?>
                <div class="pull-right flip">
                    <a href="https://blog.inet.vn/huong-dan-them-moi-mau-so-lien-lac-tren-website/" target="_blank" class="btn btn-info btn_guide">
                        <i class="fa fa-info-circle"></i> <?php echo __("Guide");?>

                    </a>
                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/reports/addtemp" class="btn btn-default">
                        <i class="fa fa-plus"></i> <?php echo __("Add new template");?>

                    </a>
                </div>
            <?php }?>
        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "addtemp" || $_smarty_tpl->tpl_vars['sub_view']->value == "edittemp") {?>
            <div class="pull-right flip">
                <a href="https://blog.inet.vn/huong-dan-them-moi-mau-so-lien-lac-tren-website/" target="_blank" class="btn btn-info btn_guide">
                    <i class="fa fa-info-circle"></i> <?php echo __("Guide");?>

                </a>
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/reports/listtemp" class="btn btn-default">
                    <i class="fa fa-list-ul"></i> <?php echo __("List template");?>

                </a>
            </div>
        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "listcate") {?>
            <?php if ($_smarty_tpl->tpl_vars['canEdit']->value) {?>
                <div class="pull-right flip">
                    <a href="https://blog.inet.vn/huong-dan-them-moi-tieu-chi-so-lien-lac-tren-web/" target="_blank" class="btn btn-info btn_guide">
                        <i class="fa fa-info-circle"></i> <?php echo __("Guide");?>

                    </a>
                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/reports/addcate" class="btn btn-default">
                        <i class="fa fa-plus"></i> <?php echo __("Add new category");?>

                    </a>
                </div>
            <?php }?>
        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "addcate" || $_smarty_tpl->tpl_vars['sub_view']->value == "editcate") {?>
            <div class="pull-right flip">
                <a href="https://blog.inet.vn/huong-dan-them-moi-tieu-chi-so-lien-lac-tren-web/" target="_blank" class="btn btn-info btn_guide">
                    <i class="fa fa-info-circle"></i> <?php echo __("Guide");?>

                </a>
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/reports/listcate" class="btn btn-default">
                    <i class="fa fa-list-ul"></i> <?php echo __("List category");?>

                </a>
            </div>
        <?php }?>
        <i class="fa fa-life-ring fa-fw fa-lg pr10"></i>
        <?php echo __("Contact book");?>

        <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
            &rsaquo; <?php echo __('Add New');?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "listtemp") {?>
            &rsaquo; <?php echo __('List template');?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "addtemp") {?>
            &rsaquo; <?php echo __('Add new template');?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "detail") {?>
            &rsaquo; <?php echo __('Detail');?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "detailtemp") {?>
            &rsaquo; <?php echo __('Detail template');?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "edit") {?>
            &rsaquo; <?php echo __('Edit');?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "edittemp") {?>
            &rsaquo; <?php echo __('Edit template');?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "listcate") {?>
            &rsaquo; <?php echo __('List category');?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "addcate") {?>
            &rsaquo; <?php echo __('Add new category');?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "editcate") {?>
            &rsaquo; <?php echo __('Edit category');?>

        <?php }?>
    </div>
    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
        <div class="panel-body with-table">
            <div class="js_ajax-forms form-horizontal">
                <input type="hidden" name="school_username" id="school_username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                <input type="hidden" name="do" value="search_report"/>
                <div class="form-group">
                    <label class="col-sm-2 control-label text-left"><?php echo __("Scope");?>
</label>
                    <div class="col-sm-2">
                        <select name="level" id="report_level" class="form-control">
                            <option value="<?php echo @constant('SCHOOL_LEVEL');?>
"><?php echo __("School");?>
</option>
                            <option value="<?php echo @constant('CLASS_LEVEL');?>
"><?php echo __("Class");?>
</option>
                            <option value="<?php echo @constant('PARENT_LEVEL');?>
"><?php echo __("Child");?>
</option>
                        </select>
                    </div>
                    <div class="col-sm-3 x-hidden" name="report_class">
                        <select name="class_id" id="re_class_id" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-view="show" class="form-control" autofocus>
                            <option value=""><?php echo __("Select class");?>
</option>
                            <?php $_smarty_tpl->_assignInScope('class_level', -1);
?>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['classes']->value, 'class');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class']->value) {
?>
                                <?php if (($_smarty_tpl->tpl_vars['class_level']->value != $_smarty_tpl->tpl_vars['class']->value['class_level_id'])) {?>
                                    <option value="" disabled style="color: blue">-----<?php echo $_smarty_tpl->tpl_vars['class']->value['class_level_name'];?>
-----</option>
                                <?php }?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['class']->value['group_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['class']->value['group_title'];?>
</option>
                                <?php $_smarty_tpl->_assignInScope('class_level', $_smarty_tpl->tpl_vars['class']->value['class_level_id']);
?>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </select>
                    </div>
                    <div class="col-sm-5 x-hidden" name="report_child">
                        <select name="child_id" id="re_child_id" class="form-control">
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-5 col-sm-offset-2" name="report_category">
                        <select name="category" id="category" class="form-control">
                            <option value="0"><strong><?php echo __("Select category");?>
</strong></option>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['categories']->value, 'category');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['category']->value) {
?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['category']->value['report_template_category_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['category']->value['category_name'];?>
</option>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </select>
                    </div>
                    <div class="col-sm-2">
                        <button type="submit" class="btn btn-default js_report-search"><?php echo __("Search");?>
</button>
                    </div>
                </div>

                <div class="table-responsive" id="report_list" name="report_list">
                    <?php $_smarty_tpl->_subTemplateRender("file:ci/school/ajax.reportlist.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                </div>
                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
            </div>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "edit") {?>
        <div class="panel-body">
            <form class="form-horizontal" enctype="multipart/form-data" method="post" id="create_report">
                <input type="hidden" name="school_username" id="school_username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                <input type="hidden" name="report_id" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['report_id'];?>
"/>
                <input type="hidden" name="do" value="edit"/>
                <div class = "form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Title");?>
 (*)</label>
                    <div class="col-sm-9">
                        <input type="text" name = "title" required class="form-control" autofocus value = "<?php echo $_smarty_tpl->tpl_vars['data']->value['report_name'];?>
">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Use template");?>

                    </label>
                    <div class="col-sm-9">
                        <select name="report_template_id" id="report_template_id" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-view="add" class="form-control">
                            <option value=""><?php echo __("Select template");?>
</option>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['templates']->value, 'temp');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['temp']->value) {
?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['temp']->value['report_template_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['temp']->value['template_name'];?>
</option>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </select>
                    </div>
                </div>
                <div class = "form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Child");?>
</label>
                    <div class = "col-sm-9">
                        <input type="text" class="form-control" value = "<?php echo $_smarty_tpl->tpl_vars['child']->value['child_name'];?>
 - <?php echo $_smarty_tpl->tpl_vars['child']->value['birthday'];?>
" disabled>
                    </div>
                </div>
                <div class = "table-responsive" id = "notemplate">
                    <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['data']->value['details'], 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
                        <table class="table table-striped table-bordered table-hover" id = "addTempTable">
                            <tr>
                                <td><strong><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
 - <?php echo $_smarty_tpl->tpl_vars['row']->value['report_category_name'];?>
</strong></td>
                                <input type="hidden" name="report_category_ids[]" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['report_category_id'];?>
">
                                <input type="hidden" name="report_category_name_<?php echo $_smarty_tpl->tpl_vars['row']->value['report_category_id'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['category_name'];?>
">
                            </tr>
                            <tr>
                                <td>
                                    <textarea placeholder="<?php echo __("Other comment");?>
" class="col-sm-12 mt10 mb10 note" style="width: 100%" name="report_content_<?php echo $_smarty_tpl->tpl_vars['row']->value['report_category_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['row']->value['report_category_content'];?>
</textarea>
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['row']->value['template_multi_content'], 'suggest');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['suggest']->value) {
?>
                                        <div class="col-sm-4">
                                            <input type="checkbox" value="<?php echo $_smarty_tpl->tpl_vars['suggest']->value;?>
" name="report_suggest_<?php echo $_smarty_tpl->tpl_vars['row']->value['report_category_id'];?>
[]" <?php if (in_array($_smarty_tpl->tpl_vars['suggest']->value,$_smarty_tpl->tpl_vars['row']->value['multi_content'])) {?>checked<?php }?>> <?php echo $_smarty_tpl->tpl_vars['suggest']->value;?>

                                        </div>
                                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                </td>
                            </tr>
                        </table>
                        <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                </div>
                <div id = "template_detail">

                </div>
                <div class = "form-group">

                </div>
                <input type = "hidden" id = "is_file" name = "is_file" value = "1">
                <div class="form-group" id = "file_old">
                    <label class="rp-file col-sm-3 control-label text-left" style="padding-top: 0px"><?php echo __("File attachment");?>
</label>
                    <div class="col-sm-6">
                        <?php if (!is_empty($_smarty_tpl->tpl_vars['data']->value['source_file'])) {?>
                            <a href="<?php echo $_smarty_tpl->tpl_vars['data']->value['source_file'];?>
" download="<?php echo $_smarty_tpl->tpl_vars['data']->value['file_name'];?>
"><?php echo $_smarty_tpl->tpl_vars['data']->value['file_name'];?>
</a>
                        <?php } else { ?> <?php echo __('No file attachment');?>

                        <?php }?>
                    </div>
                </div>
                <div class = "form-group">
                    <label class="control-label col-sm-3">
                        <?php if (is_empty($_smarty_tpl->tpl_vars['data']->value['source_file'])) {?>
                            <?php echo __("Choose file");?>

                        <?php } else { ?>
                            <?php echo __("Choose file replace");?>

                        <?php }?>
                    </label>
                    <div class = "col-sm-6">
                        <input type="file" name="file" id="file"/>
                    </div>
                    <a class = "delete_image btn btn-xs btn-danger text-left"><?php echo __('Delete');?>
</a>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Notify immediately");?>
?</label>
                    <div class="col-sm-9">
                        <div class="onoffswitch">
                            <input type="checkbox" name="notify_immediately" class="onoffswitch-checkbox" id="notify_immediately" <?php if ($_smarty_tpl->tpl_vars['data']->value['is_notified'] == 1) {?>checked<?php }?>>
                            <label class="onoffswitch-label" for="notify_immediately"></label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30"><?php echo __("Save");?>
</button>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
        <div class="panel-body">
            <?php if (count($_smarty_tpl->tpl_vars['templates']->value) == 0) {?>
                <div class = "color_red" align="center">
                    <strong><?php echo __("You must create a template before creating a contact book");?>
</strong>
                </div>
            <?php } else { ?>
                <form class="form-horizontal" enctype="multipart/form-data" method="post" id="create_report">
                    <input type="hidden" name="school_username" id="school_username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                    <input type="hidden" name="do" value="add"/>
                    <div class = "form-group">
                        <label class="col-sm-3 control-label text-left"><?php echo __("Title");?>
 (*)</label>
                        <div class="col-sm-9">
                            <input type="text" name = "title" required class="form-control" autofocus>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left"><?php echo __("Notify immediately");?>
?</label>
                        <div class="col-sm-9">
                            <div class="onoffswitch">
                                <input type="checkbox" name="notify_immediately" class="onoffswitch-checkbox" id="notify_immediately" unchecked>
                                <label class="onoffswitch-label" for="notify_immediately"></label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left"><?php echo __("Select Class");?>
 (*)</label>
                        <div class="col-sm-3">
                            <select name="class_id" id="report_class_id" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-view="add" class="form-control" autofocus>
                                <option value=""><?php echo __("Select class");?>
</option>
                                <?php $_smarty_tpl->_assignInScope('class_level', -1);
?>
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['classes']->value, 'class');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class']->value) {
?>
                                    <?php if (($_smarty_tpl->tpl_vars['class_level']->value != $_smarty_tpl->tpl_vars['class']->value['class_level_id'])) {?>
                                        <option value="" disabled style="color: blue">-----<?php echo $_smarty_tpl->tpl_vars['class']->value['class_level_name'];?>
-----</option>
                                    <?php }?>
                                    <option value="<?php echo $_smarty_tpl->tpl_vars['class']->value['group_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['class']->value['group_title'];?>
</option>
                                    <?php $_smarty_tpl->_assignInScope('class_level', $_smarty_tpl->tpl_vars['class']->value['class_level_id']);
?>
                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                            </select>
                        </div>
                    </div>
                    <div class = "form-group">
                        <label class="col-sm-3 control-label text-left">
                            <?php echo __("Send comment to");?>
 (*)
                        </label>
                        <div id="list_child" class = "col-sm-9">

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left"><?php echo __("Use template");?>
</label>
                        <div class="col-sm-9">
                            <select name="report_template_id" id="report_template_id" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-view="add" class="form-control">
                                <option value=""><?php echo __("Select template");?>
</option>
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['templates']->value, 'temp');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['temp']->value) {
?>
                                    <option value="<?php echo $_smarty_tpl->tpl_vars['temp']->value['report_template_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['temp']->value['template_name'];?>
</option>
                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                            </select>
                            <?php if (count($_smarty_tpl->tpl_vars['templates']->value) == 0) {?>
                                <br/>
                                <strong><?php echo __("Bạn chưa có mẫu nào, vui lòng kích vào");?>
 <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/reports/addtemp" target="_blank"><?php echo __("HERE");?>
</a> <?php echo __("để tạo mẫu");?>
</strong>
                            <?php }?>
                        </div>
                    </div>

                    <div id = "template_detail">
                        <?php $_smarty_tpl->_subTemplateRender("file:ci/ajax.reporttemplatedetail.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                    </div>

                    <div class = "form-group"></div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left"><?php echo __("File attachment");?>
</label>
                        <div class="col-sm-6">
                            <input type="file" name="file" id="file"/>
                        </div>
                        <div class="col-sm-3">
                            <a class = "delete_image btn btn-danger btn-xs text-left"><?php echo __('Delete');?>
</a>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-3">
                            <button type="submit" class="btn btn-primary padrl30"><?php echo __("Save");?>
</button>
                            
                        </div>
                    </div>

                    <!-- success -->
                    <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                    <!-- success -->

                    <!-- error -->
                    <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                    <!-- error -->
                </form>
            <?php }?>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "detail") {?>
        <div class="panel-body with-table">
            <table class = "table table-bordered">
                <tbody>
                    <tr>
                        <td class = "col-sm-3 text-right"><?php echo __('Title');?>
</td>
                        <td>
                            <strong><?php echo $_smarty_tpl->tpl_vars['data']->value['report_name'];?>
</strong>
                        </td>
                    </tr>
                    <tr>
                        <td class = "col-sm-3 text-right"><?php echo __('Child');?>
</td>
                        <td>
                            <strong><?php echo $_smarty_tpl->tpl_vars['child']->value['child_name'];?>
 - <?php echo $_smarty_tpl->tpl_vars['child']->value['birthday'];?>
</strong>
                        </td>
                    </tr>
                    <tr>
                        <td class = "col-sm-3 text-right"><?php echo __('Class');?>
</td>
                        <td>
                            <strong><?php echo $_smarty_tpl->tpl_vars['class_name']->value;?>
</strong>
                        </td>
                    </tr>
                    <tr>
                        <td class = "col-sm-3 text-right"><?php echo __('Notification');?>
</td>
                        <td>
                            <?php if ($_smarty_tpl->tpl_vars['data']->value['is_notified']) {?>
                                <?php echo __('Notified');?>

                            <?php } else { ?>
                                <?php echo __('Not notified yet');?>

                            <?php }?>
                        </td>
                    </tr>
                    <tr>
                        <td class = "col-sm-3 text-right"><?php echo __("File attachment");?>
</td>
                        <td>
                            <?php if (!is_empty($_smarty_tpl->tpl_vars['data']->value['source_file'])) {?>
                                <a href = "<?php echo $_smarty_tpl->tpl_vars['data']->value['source_file'];?>
" target="_blank"><strong>
                                        <?php echo __("File attachment");?>

                                    </strong>
                                </a>
                            <?php } else { ?>
                                <?php echo __("No file attachment");?>

                            <?php }?>
                        </td>
                    </tr>
                </tbody>
            </table>

            <div class = "table-responsive">
                <table class = "table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th> # </th>
                        <th> <?php echo __('Title');?>
 </th>
                        <th><?php echo __('Content');?>
 </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['data']->value['details'], 'row', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['row']->value) {
?>
                        <tr>
                            <td align="center">
                                <strong><?php echo $_smarty_tpl->tpl_vars['k']->value+1;?>
</strong>
                            </td>
                            <td>
                                <strong><?php echo $_smarty_tpl->tpl_vars['row']->value['report_category_name'];?>
</strong>
                            </td>
                            <td>
                                <div class="mb10">
                                    <?php echo $_smarty_tpl->tpl_vars['row']->value['report_category_content'];?>

                                </div>
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['row']->value['multi_content'], 'suggest');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['suggest']->value) {
?>
                                    <div class="form-group">
                                        <i class="fa fa-check" aria-hidden="true"></i> <?php echo $_smarty_tpl->tpl_vars['suggest']->value;?>

                                    </div>
                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                            </td>
                        </tr>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                    </tbody>
                </table>
            </div>
            <div style="width:75%;margin-left: auto;margin-right: auto">
                <strong style="float: right"><?php echo __("Status ");?>
 :
                    <?php if ($_smarty_tpl->tpl_vars['status']->value == 'Pass') {?>
                        <strong style="color:lawngreen"><?php ob_start();
echo $_smarty_tpl->tpl_vars['status']->value;
$_prefixVariable1=ob_get_clean();
echo __($_prefixVariable1);?>
</strong>
                    <?php } elseif ($_smarty_tpl->tpl_vars['status']->value == 'Fail') {?>
                        <strong style="color:red"><?php ob_start();
echo $_smarty_tpl->tpl_vars['status']->value;
$_prefixVariable2=ob_get_clean();
echo __($_prefixVariable2);?>
</strong>
                    <?php } elseif ($_smarty_tpl->tpl_vars['status']->value == 'Re-exam') {?>
                        <strong style="color:orange"><?php ob_start();
echo $_smarty_tpl->tpl_vars['status']->value;
$_prefixVariable3=ob_get_clean();
echo __($_prefixVariable3);?>
</strong>
                    <?php } else { ?>
                        <strong><?php ob_start();
echo $_smarty_tpl->tpl_vars['status']->value;
$_prefixVariable4=ob_get_clean();
echo __($_prefixVariable4);?>
</strong>
                    <?php }?>

                </strong>
                <strong><?php echo __("Points list");?>
</strong>
                <table class="table table-striped table-bordered" style="z-index: 1">
                    <thead>
                    <tr bgcolor="#fff">
                        <th rowspan="2" class="pinned" style="padding: 8px 6px" id="width_no">#</th>
                        <th rowspan="2" nowrap="true" class="pinned"
                            style="padding: 8px 6px"><?php echo __("Subject name");?>
</th>
                        <th colspan="4"><?php echo __("Semester 1");?>
</th>
                        <th colspan="4"><?php echo __("Semester 2");?>
</th>
                    </tr>
                    <tr>
                        <th>M1</th>
                        <th>M2</th>
                        <th>M3</th>
                        <th>D1</th>
                        <th>M1</th>
                        <th>M2</th>
                        <th>M3</th>
                        <th>D2</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $_smarty_tpl->_assignInScope('rowIdx', 1);
?>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['rows']->value, 'row', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['row']->value) {
?>
                        <tr>
                            <td align="center" class="pinned"><?php echo $_smarty_tpl->tpl_vars['rowIdx']->value;?>
</td>

                            <td nowrap="true" class="pinned text-bold color-blue">
                                <strong><?php echo $_smarty_tpl->tpl_vars['row']->value['subject_name'];?>
</strong>
                            </td>
                            <?php $_smarty_tpl->_assignInScope('rowIdx', $_smarty_tpl->tpl_vars['rowIdx']->value+1);
?>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['subject_key']->value, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value) {
?>
                                <td><?php echo $_smarty_tpl->tpl_vars['row']->value[strtolower($_smarty_tpl->tpl_vars['key']->value)];?>
</td>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </tr>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                    
                    <tr>
                        <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                            <strong><?php echo __("Average monthly");?>
</strong>
                        </td>
                        <td><?php echo number_format($_smarty_tpl->tpl_vars['children_point_avgs']->value['a1'],2);?>
</td>
                        <td><?php echo number_format($_smarty_tpl->tpl_vars['children_point_avgs']->value['b1'],2);?>
</td>
                        <td><?php echo number_format($_smarty_tpl->tpl_vars['children_point_avgs']->value['c1'],2);?>
</td>
                        <td><?php echo number_format($_smarty_tpl->tpl_vars['children_point_avgs']->value['d1'],2);?>
</td>
                        <td><?php echo number_format($_smarty_tpl->tpl_vars['children_point_avgs']->value['a2'],2);?>
</td>
                        <td><?php echo number_format($_smarty_tpl->tpl_vars['children_point_avgs']->value['b2'],2);?>
</td>
                        <td><?php echo number_format($_smarty_tpl->tpl_vars['children_point_avgs']->value['c2'],2);?>
</td>
                        <td><?php echo number_format($_smarty_tpl->tpl_vars['children_point_avgs']->value['d2'],2);?>
</td>

                    </tr>
                    <tr>
                        <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                            <strong><?php echo __("Average semesterly");?>
</strong>
                        </td>
                        <td colspan="3" style="text-align: center"><?php echo number_format($_smarty_tpl->tpl_vars['children_point_avgs']->value['x1'],2);?>
</td>
                        <td></td>
                        <td colspan="3" style="text-align: center"><?php echo number_format($_smarty_tpl->tpl_vars['children_point_avgs']->value['x2'],2);?>
</td>
                        <td></td>
                    </tr>
                    
                    <tr>
                        <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                            <strong><?php echo __("End semester");?>
</strong>
                        </td>
                        <td colspan="4" style="text-align: center"><?php echo number_format($_smarty_tpl->tpl_vars['children_point_avgs']->value['e1'],2);?>
</td>
                        <td colspan="4" style="text-align: center"><?php echo number_format($_smarty_tpl->tpl_vars['children_point_avgs']->value['e2'],2);?>
</td>
                    </tr>
                    
                    <tr>
                        <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                            <strong><?php echo __("End year");?>
</strong>
                        </td>
                        <td colspan="8" style="text-align: center"><?php echo number_format($_smarty_tpl->tpl_vars['children_point_avgs']->value['y'],2);?>
</td>
                    </tr>
                    
                    <tr>
                        <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                            <strong><?php echo __("Absent has permission");?>
</strong>
                        </td>
                        <td colspan="8" style="text-align: center"><?php echo $_smarty_tpl->tpl_vars['child_absent']->value['absent_true'];?>
</td>
                    </tr>
                    
                    <tr>
                        <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                            <strong><?php echo __("Absent without permission");?>
</strong>
                        </td>
                        <td colspan="8" style="text-align: center"><?php echo $_smarty_tpl->tpl_vars['child_absent']->value['absent_false'];?>
</td>
                    </tr>
                    </tbody>
                </table>

                <strong><?php echo __("Re-Exam");?>
</strong>
                <table class="table table-striped table-bordered" style="z-index: 1;">
                    <thead>
                    <tr bgcolor="#fff">
                        <th rowspan="2" class="pinned" style="padding: 8px 6px" id="width_no">#</th>
                        <th rowspan="2" nowrap="true" class="pinned"
                            style="padding: 8px 6px"><?php echo __("Subject name");?>

                        </th>
                        <th colspan="1"><?php echo __("Point");?>
</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $_smarty_tpl->_assignInScope('rowIdx', 1);
?>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['children_subject_reexams']->value, 'row', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['row']->value) {
?>
                        <tr>
                            <td align="center" class="pinned"><?php echo $_smarty_tpl->tpl_vars['rowIdx']->value;?>
</td>
                            <td nowrap="true" class="pinned text-bold color-blue">
                                <strong><?php echo $_smarty_tpl->tpl_vars['row']->value['name'];?>
</strong>
                            </td>
                            <td style="text-align: center"><?php echo $_smarty_tpl->tpl_vars['row']->value['point'];?>
</td>
                            <?php $_smarty_tpl->_assignInScope('rowIdx', $_smarty_tpl->tpl_vars['rowIdx']->value+1);
?>
                        </tr>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                    <tr>
                        <td colspan="2" nowrap="true" class="pinned text-bold color-blue"
                            style="text-align: center">
                            <strong><?php echo __("Result Re-exam");?>
</strong>
                        </td>
                        <td colspan="3" style="text-align: center"><?php echo number_format($_smarty_tpl->tpl_vars['result_exam']->value,2);?>
</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="form-group pl5">
                <a class="btn btn-default" href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/reports"><?php echo __("Lists");?>
</a>
                <?php if ($_smarty_tpl->tpl_vars['canEdit']->value) {?>
                    <a class="btn btn-default" href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/reports/add"><?php echo __("Add New");?>
</a>
                    <?php if (!$_smarty_tpl->tpl_vars['data']->value['is_notified']) {?>
                        <button class="btn btn-default js_school-report-notify" data-handle="notify" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['data']->value['report_id'];?>
"><?php echo __("Notify");?>
</button>
                    <?php }?>
                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/reports/edit/<?php echo $_smarty_tpl->tpl_vars['data']->value['report_id'];?>
" class="btn btn-default"><?php echo __("Edit");?>
</a>
                    <button class="btn btn-danger js_school-delete-report" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['data']->value['report_id'];?>
" data-handle = "delete_report"><?php echo __("Delete");?>
</button>
                <?php }?>
            </div>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "listtemp") {?>
        <div class="panel-body with-table">
            <div class = "table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr><th colspan="5"><?php echo __("Contact book template list");?>
&nbsp;(<?php echo count($_smarty_tpl->tpl_vars['results']->value);?>
)</th></tr>
                    <tr>
                        <th><?php echo __("#");?>
</th>
                        <th><?php echo __("Title");?>
</th>
                        <th><?php echo __("Scope");?>
</th>
                        <th><?php echo __("Created time");?>
</th>
                        <th><?php echo __("Actions");?>
</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['results']->value, 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
                            <tr>
                                <td class="align-middle">
                                    <center><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</center>
                                </td>
                                <td class="align-middle">
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/reports/edittemp/<?php echo $_smarty_tpl->tpl_vars['row']->value['report_template_id'];?>
">
                                        <?php echo $_smarty_tpl->tpl_vars['row']->value['template_name'];?>

                                    </a>
                                </td>
                                <td class="align_middle" align="center">
                                    <?php if ($_smarty_tpl->tpl_vars['row']->value['level'] == CLASS_LEVEL) {?>
                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['classes']->value, 'class');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class']->value) {
?>
                                            <?php if ($_smarty_tpl->tpl_vars['class']->value['group_id'] == $_smarty_tpl->tpl_vars['row']->value['class_id']) {?>
                                                <?php echo $_smarty_tpl->tpl_vars['class']->value['group_title'];?>

                                            <?php }?>
                                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                    <?php } elseif ($_smarty_tpl->tpl_vars['row']->value['level'] == CLASS_LEVEL_LEVEL) {?>
                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['class_levels']->value, 'level');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['level']->value) {
?>
                                            <?php if ($_smarty_tpl->tpl_vars['level']->value['class_level_id'] == $_smarty_tpl->tpl_vars['row']->value['class_level_id']) {?>
                                                <?php echo $_smarty_tpl->tpl_vars['level']->value['class_level_name'];?>

                                            <?php }?>
                                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                    <?php } else { ?>
                                        <?php echo __("School");?>

                                    <?php }?>
                                </td>
                                <td align="center" class="align_middle">
                                    <?php echo $_smarty_tpl->tpl_vars['row']->value['created_at'];?>

                                </td>
                                <td class="align-middle" align="center">
                                    <?php if ($_smarty_tpl->tpl_vars['canEdit']->value) {?>
                                        <div>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/reports/edittemp/<?php echo $_smarty_tpl->tpl_vars['row']->value['report_template_id'];?>
" class="btn btn-xs btn-default"><?php echo __("Edit");?>
</a>
                                            <button class="btn btn-xs btn-danger js_school-delete-report" data-handle="delete_temp" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['report_template_id'];?>
"><?php echo __("Delete");?>
</button>
                                        </div>
                                    <?php }?>
                                </td>
                            </tr>
                            <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                    </tbody>
                </table>
            </div>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "edittemp") {?>
        <div class="panel-body with-table">
            <div id="open_dialog" class="x-hidden" title="<?php echo mb_strtoupper(__("Add new category"), 'UTF-8');?>
">
                <?php $_smarty_tpl->_subTemplateRender("file:ci/school/ajax.school.addcategory.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

            </div>
            <div id="open_dialog_category_detail" class="x-hidden" title="<?php echo mb_strtoupper(__("Category suggests"), 'UTF-8');?>
">
                <?php $_smarty_tpl->_subTemplateRender("file:ci/school/ajax.school.categorydetail.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

            </div>
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_report.php">
                <input type="hidden" name="school_username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                <input type="hidden" name="report_template_id" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['report_template_id'];?>
"/>
                <input type="hidden" name="do" value="edit_temp"/>
                <div class = "form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Template name");?>
 (*)</label>
                    <div class="col-sm-9">
                        <input class="form-control" name="template_name" required maxlength="300" value = "<?php echo $_smarty_tpl->tpl_vars['data']->value['template_name'];?>
">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Scope");?>
 (*)</label>
                    <div class="col-sm-3">
                        <select name="level" id="cate_level" class="form-control">
                            <option value="<?php echo @constant('SCHOOL_LEVEL');?>
" <?php if ($_smarty_tpl->tpl_vars['data']->value['level'] == @constant('SCHOOL_LEVEL')) {?> selected <?php }?>><?php echo __("School");?>
</option>
                            <option value="<?php echo @constant('CLASS_LEVEL_LEVEL');?>
" <?php if ($_smarty_tpl->tpl_vars['data']->value['level'] == @constant('CLASS_LEVEL_LEVEL')) {?> selected <?php }?>><?php echo __("Class level");?>
</option>
                            <option value="<?php echo @constant('CLASS_LEVEL');?>
" <?php if ($_smarty_tpl->tpl_vars['data']->value['level'] == @constant('CLASS_LEVEL')) {?> selected <?php }?>><?php echo __("Class");?>
</option>
                        </select>
                    </div>
                    <div class="col-sm-4 <?php if ($_smarty_tpl->tpl_vars['data']->value['level'] != @constant('CLASS_LEVEL_LEVEL')) {?>x-hidden<?php }?>" name="cate_class_level">
                        <select name="class_level_id" id="class_level_id" class="form-control">
                            <option value="0"> <?php echo __('Select class level');?>
</option>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['classLevels']->value, 'class_level');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class_level']->value) {
?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['class_level']->value['class_level_id'];?>
" <?php if ($_smarty_tpl->tpl_vars['data']->value['class_level_id'] == $_smarty_tpl->tpl_vars['class_level']->value['class_level_id']) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['class_level']->value['class_level_name'];?>
</option>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </select>
                    </div>
                    <div class="col-sm-4 <?php if ($_smarty_tpl->tpl_vars['data']->value['level'] != (@constant('CLASS_LEVEL'))) {?>x-hidden<?php }?>" name="cate_class">
                        <select name="class_id" id="class_id" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" class="form-control">
                            <option value="0"> <?php echo __('Select class');?>
 </option>
                            <?php $_smarty_tpl->_assignInScope('class_level', -1);
?>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['classes']->value, 'class');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class']->value) {
?>
                                <?php if (($_smarty_tpl->tpl_vars['class_level']->value != $_smarty_tpl->tpl_vars['class']->value['class_level_id'])) {?>
                                    <option value="" disabled style="color: blue">-----<?php echo $_smarty_tpl->tpl_vars['class']->value['class_level_name'];?>
-----</option>
                                <?php }?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['class']->value['group_id'];?>
" <?php if ($_smarty_tpl->tpl_vars['data']->value['class_id'] == $_smarty_tpl->tpl_vars['class']->value['group_id']) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['class']->value['group_title'];?>
</option>
                                <?php $_smarty_tpl->_assignInScope('class_level', $_smarty_tpl->tpl_vars['class']->value['class_level_id']);
?>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </select>
                    </div>
                </div>
                <div class="form-group" align="center">
                    <div class="col-sm-12">
                        <strong><span style="color: red">Lưu ý:</span> Bạn có thể sắp xếp thự tự tiêu chí bằng cách nhấn giữ và kéo thả.</strong>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="sort">
                        <thead>
                        <tr>
                            <th>
                                <?php echo __('#');?>

                            </th>
                            <th align="center">
                                <input type="checkbox" id="select_all" style="float: left"><?php echo __('Category');?>

                            </th>
                            <th>
                                <?php echo __('Content');?>

                            </th>
                            <th>
                                <?php echo __('Category suggest');?>

                            </th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['categorysTemp']->value, 'category');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['category']->value) {
?>
                                <tr>
                                    <td align="center" class = "align_middle index"><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</td>
                                    <input type="hidden" class="positions" value="<?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
">
                                    <td align="left" class="align_middle">
                                        <input type = "checkbox" name = "category_ids[]" value="<?php echo $_smarty_tpl->tpl_vars['category']->value['report_template_category_id'];?>
" <?php if ($_smarty_tpl->tpl_vars['category']->value['checked']) {?>checked<?php }?>>
                                        <?php echo $_smarty_tpl->tpl_vars['category']->value['category_name'];?>

                                    </td>
                                    <td>
                                        <textarea type="text" class="note" name="content_<?php echo $_smarty_tpl->tpl_vars['category']->value['report_template_category_id'];?>
" style="width: 100%"><?php echo $_smarty_tpl->tpl_vars['category']->value['template_content'];?>
</textarea>
                                    </td>
                                    <td align="center" class="align_middle">
                                        <a class="btn btn-xs btn-default js_school-category-detail" data-id="<?php echo $_smarty_tpl->tpl_vars['category']->value['report_template_category_id'];?>
"><?php echo __("Category suggests");?>
</a>
                                    </td>
                                </tr>
                                <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                            <tr id="category_new_pm"></tr>
                        </tbody>
                    </table>
                    <div class = "col-sm-12">
                        <?php if ($_smarty_tpl->tpl_vars['canEdit']->value) {?>
                            <a class="btn btn-default js_school-category-add"><?php echo __("Add new category");?>
</a>
                            <button type="submit" class="btn btn-primary padrl30"><?php echo __("Save");?>
</button>
                        <?php }?>
                    </div>
                </div>
                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "addtemp") {?>
        <div class="panel-body">
            <div id="open_dialog" class="x-hidden" title="<?php echo mb_strtoupper(__("Add new category"), 'UTF-8');?>
">
                <?php $_smarty_tpl->_subTemplateRender("file:ci/school/ajax.school.addcategory.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

            </div>
            <div id="open_dialog_category_detail" class="x-hidden" title="<?php echo mb_strtoupper(__("Category suggests"), 'UTF-8');?>
">
                <?php $_smarty_tpl->_subTemplateRender("file:ci/school/ajax.school.categorydetail.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

            </div>
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_report.php">
                <input type="hidden" name="school_username" id="school_username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                <input type="hidden" name="do" value="add_temp"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Title");?>
 (*)</label>
                    <div class="col-sm-9">
                        <input class="form-control" name="template_name" required autofocus maxlength="100">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Scope");?>
 (*)</label>
                    <div class="col-sm-3">
                        <select name="level" id="schedule_level" class="form-control" data-username = "<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-id = "<?php echo $_smarty_tpl->tpl_vars['school']->value['page_id'];?>
">
                            <option value="<?php echo @constant('SCHOOL_LEVEL');?>
"><?php echo __("School");?>
</option>
                            <option value="<?php echo @constant('CLASS_LEVEL_LEVEL');?>
"><?php echo __("Class level");?>
</option>
                            <option value="<?php echo @constant('CLASS_LEVEL');?>
"><?php echo __("Class");?>
</option>
                        </select>
                    </div>
                    <div class="col-sm-3 x-hidden" name="schedule_class_level">
                        <select name="class_level_id" id="class_level_id" class="form-control">
                            <option value="0"><?php echo __("Select class level");?>
</option>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['class_levels']->value, 'class_level');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class_level']->value) {
?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['class_level']->value['class_level_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['class_level']->value['class_level_name'];?>
</option>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </select>
                    </div>
                    <div class="col-sm-3 x-hidden" name="schedule_class">
                        <select name="class_id" id="class_id" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" class="form-control">
                            <option value="0"><?php echo __("Select class");?>
</option>
                            <?php $_smarty_tpl->_assignInScope('class_level', -1);
?>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['classes']->value, 'class');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class']->value) {
?>
                                <?php if (($_smarty_tpl->tpl_vars['class_level']->value != $_smarty_tpl->tpl_vars['class']->value['class_level_id'])) {?>
                                    <option value="" disabled style="color: blue">-----<?php echo $_smarty_tpl->tpl_vars['class']->value['class_level_name'];?>
-----</option>
                                <?php }?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['class']->value['group_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['class']->value['group_title'];?>
</option>
                                <?php $_smarty_tpl->_assignInScope('class_level', $_smarty_tpl->tpl_vars['class']->value['class_level_id']);
?>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </select>
                    </div>
                </div>
                
                <div class="form-group" align="center">
                    <div class="col-sm-12">
                        <strong><span style="color: red">Lưu ý:</span> Bạn có thể sắp xếp thự tự tiêu chí bằng cách nhấn giữ và kéo thả.</strong>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="sort">
                        <thead>
                            <tr>
                                <th>
                                    <?php echo __('#');?>

                                </th>
                                <th>
                                    <input type="checkbox" id="select_all" style="float: left"><?php echo __('Category');?>

                                </th>
                                <th>
                                    <?php echo __('Content');?>

                                </th>
                                <th>
                                    <?php echo __('Category suggest');?>

                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['categorys']->value, 'category');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['category']->value) {
?>
                                <tr>
                                    <td align="center" class = "align_middle index"><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</td>
                                    <td class="align_middle">
                                        <input type = "checkbox" name = "category_ids[]" value="<?php echo $_smarty_tpl->tpl_vars['category']->value['report_template_category_id'];?>
">
                                        <?php echo $_smarty_tpl->tpl_vars['category']->value['category_name'];?>

                                    </td>
                                    <td>
                                        <textarea type="text" class="note" name="content_<?php echo $_smarty_tpl->tpl_vars['category']->value['report_template_category_id'];?>
" style="width: 100%"></textarea>
                                    </td>
                                    <td align="center" class="align_middle">
                                        
                                        <a class="btn btn-xs btn-default js_school-category-detail" data-id="<?php echo $_smarty_tpl->tpl_vars['category']->value['report_template_category_id'];?>
"><?php echo __("Category suggests");?>
</a>
                                    </td>
                                </tr>
                                <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                            <tr id="category_new_pm"></tr>
                        </tbody>
                    </table>
                    <div class = "col-sm-12">
                        <a class="btn btn-default js_school-category-add"><?php echo __("Add new category");?>
</a>
                        <button type="submit" class="btn btn-primary padrl30"><?php echo __("Save");?>
</button>
                    </div>
                </div>
                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->
                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->

            </form>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "listcate") {?>
        <div class="panel-body with-table">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>
                            <?php echo __('No.');?>

                        </th>
                        <th>
                            <?php echo __('Category name');?>

                        </th>
                        <th>
                            <?php echo __('Actions');?>

                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if (count($_smarty_tpl->tpl_vars['categorys']->value) > 0) {?>
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['categorys']->value, 'row', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['row']->value) {
?>
                            <tr>
                                <td align = "center" class="align_middle">
                                    <?php echo $_smarty_tpl->tpl_vars['k']->value+1;?>

                                </td>
                                <td align="center" class="align_middle">
                                    
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/reports/editcate/<?php echo $_smarty_tpl->tpl_vars['row']->value['report_template_category_id'];?>
/1">
                                        <?php echo $_smarty_tpl->tpl_vars['row']->value['category_name'];?>

                                    </a>
                                </td>
                                <td align = "center" class="align_middle">
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/reports/editcate/<?php echo $_smarty_tpl->tpl_vars['row']->value['report_template_category_id'];?>
/1" class="btn btn-default btn-xs">
                                        <?php echo __("Edit");?>

                                    </a>
                                    <?php if ($_smarty_tpl->tpl_vars['canEdit']->value) {?>
                                        <a class="btn btn-xs btn-danger js_school-delete-report" data-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['report_template_category_id'];?>
" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-handle="delete_cate"><?php echo __("Delete");?>
</a>
                                    <?php }?>
                                </td>
                            </tr>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        <?php } else { ?>
                        <tr>
                           <td colspan="3" align="center" class="align_middle"><?php echo __("No data");?>
</td>
                        </tr>
                    <?php }?>
                    </tbody>
                </table>
            </div>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "addcate") {?>
        <div class="mt10" align="center">
            <strong><?php echo __("Add new category and add suggest for category");?>
</strong>
        </div>
        <div class="panel-body with-table">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_report.php">
                <input type="hidden" name="school_username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                <input type="hidden" name="do" value="add_cate"/>
                <div class = "form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Category name");?>
 (*)</label>
                    <div class="col-sm-9">
                        <input class="form-control" name="category_name" required maxlength="300">
                    </div>
                </div>
                <div class="table-responsive" id="cate_school_add">
                    <table class="table table-striped table-bordered table-hover" id = "addCategoryTableNoPopup">
                        <thead>
                            <tr><th colspan="3"><?php echo __("Suggest content for category");?>
</th></tr>
                            <tr>
                                <th>
                                    <?php echo __('No.');?>

                                </th>
                                <th>
                                    <?php echo __('Title');?>

                                </th>
                                <th>
                                    <?php echo __('Actions');?>

                                </th>
                            </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class = "col_no align_middle"  align = "center">1</td>
                            <td>
                                <input type="text" name = "suggests[]" class = "form-control" placeholder="<?php echo __("Suggest content for category");?>
">
                            </td>
                            <td align="center" class="align_middle"> <a class="btn btn-danger btn-xs js_report_template-delete"> <?php echo __("Delete");?>
 </a></td>
                        </tr>
                        </tbody>
                    </table>
                    <a class="btn btn-default js_report_suggest-add"><?php echo __("Add new suggest");?>
</a>
                    <button type="submit" class="btn btn-primary padrl30"><?php echo __("Save");?>
</button>
                </div>
                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "editcate") {?>
        <div class="mt10" align="center">
            <strong><?php echo __("Edit category");?>
</strong>
        </div>
        <div class="panel-body with-table">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_report.php">
                <input type="hidden" name="school_username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                <input type="hidden" name="do" value="edit_cate"/>
                <input type="hidden" name="category_id" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['report_template_category_id'];?>
"/>
                <input type="hidden" name="p5" value="<?php echo $_smarty_tpl->tpl_vars['p5']->value;?>
"/>
                <?php if (isset($_smarty_tpl->tpl_vars['p6']->value)) {?>
                    <input type="hidden" name="p6" value="<?php echo $_smarty_tpl->tpl_vars['p6']->value;?>
"/>
                <?php }?>
                <div class = "form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Category name");?>
 (*)</label>
                    <div class="col-sm-9">
                        <input class="form-control" name="category_name" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['category_name'];?>
" required maxlength="300">
                    </div>
                </div>
                <div class="table-responsive" id="cate_school_add">
                    <table class="table table-striped table-bordered table-hover" id = "addCategoryTableNoPopup">
                        <thead>
                        <tr><th colspan="3"><?php echo __("Suggest content for category");?>
</th></tr>
                        <tr>
                            <th>
                                <?php echo __('No.');?>

                            </th>
                            <th>
                                <?php echo __('Title');?>

                            </th>
                            <th>
                                <?php echo __('Actions');?>

                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['data']->value['suggests'], 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
                            <tr>
                                <td class = "col_no align_middle"  align = "center"><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</td>
                                <td>
                                    <input type="text" name = "suggests[]" class = "form-control" value="<?php echo $_smarty_tpl->tpl_vars['row']->value;?>
">
                                </td>
                                <td align="center" class="align_middle">
                                    <?php if ($_smarty_tpl->tpl_vars['canEdit']->value) {?>
                                        <a class="btn btn-danger btn-xs js_report_template-delete"> <?php echo __("Delete");?>
</a>
                                    <?php }?>
                                </td>
                            </tr>
                            <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </tbody>
                    </table>
                    <?php if ($_smarty_tpl->tpl_vars['canEdit']->value) {?>
                        <a class="btn btn-default js_report_suggest-add"><?php echo __("Add new suggest");?>
</a>
                        <button type="submit" class="btn btn-primary padrl30"><?php echo __("Save");?>
</button>
                    <?php }?>
                </div>
                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    <?php }?>
</div>
</div><?php }
}
