<?php
/* Smarty version 3.1.31, created on 2021-05-20 17:10:54
  from "D:\workplace\Inet-project\source\Server11\content\themes\inet\templates\ci\class\ajax.pickupchild.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_60a635ae4fe4b3_66096807',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '1062a2e8943a95c8dcdc714b093f15ff97f0978e' => 
    array (
      0 => 'D:\\workplace\\Inet-project\\source\\Server11\\content\\themes\\inet\\templates\\ci\\class\\ajax.pickupchild.tpl',
      1 => 1621501238,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60a635ae4fe4b3_66096807 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="text-left">
    <strong><?php echo __("Total student");?>
: <font color="red"><?php echo count($_smarty_tpl->tpl_vars['children']->value);?>
</font> |
        <?php echo __("Registered");?>
: <font color="red"><?php echo $_smarty_tpl->tpl_vars['child_count']->value;?>
</font>
    </strong>
</div>

<table class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th width="10%">#</th>
        <th width="15%">
            <input type="checkbox" id="childCheckAll" >&nbsp;<?php echo __("All");?>

        </th>
        <th width="40%"><?php echo __("Children");?>
</th>
        <th width="35%"><?php echo __("Added to");?>
</th>
    </tr>
    </thead>
    <tbody>
    <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['children']->value, 'child');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['child']->value) {
?>
        <tr <?php if ($_smarty_tpl->tpl_vars['child']->value['child_status'] == 0) {?> class="row-disable" <?php }?>>
            <td align="center"><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</td>
            <td align="center">
                <input type="checkbox" class="child" name="childIds[]" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
"
                        <?php if (!is_empty($_smarty_tpl->tpl_vars['child']->value['status'])) {?>
                            checked
                        <?php }?>

                        <?php if (array_key_exists($_smarty_tpl->tpl_vars['child']->value['child_id'],$_smarty_tpl->tpl_vars['childrenRegistered']->value)) {?>
                            onclick="return false;" disabled
                        <?php }?>
                />
            </td>
            <td><strong class="ml20"><?php echo $_smarty_tpl->tpl_vars['child']->value['child_name'];?>
</strong></td>
            <td align="center"><strong>
                    <?php if (!is_empty($_smarty_tpl->tpl_vars['child']->value['class_name'])) {?>
                        <?php echo $_smarty_tpl->tpl_vars['child']->value['class_name'];?>

                    <?php } elseif (array_key_exists($_smarty_tpl->tpl_vars['child']->value['child_id'],$_smarty_tpl->tpl_vars['childrenRegistered']->value)) {?>
                        <?php echo $_smarty_tpl->tpl_vars['childrenRegistered']->value[$_smarty_tpl->tpl_vars['child']->value['child_id']];?>

                    <?php } else { ?>
                        &#150;
                    <?php }?>
            </strong></td>
            
        </tr>
        <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>


    <?php if ($_smarty_tpl->tpl_vars['idx']->value == 1) {?>
        <tr class="odd">
            <td valign="top" align="center" colspan="4" class="dataTables_empty">
                <?php echo __("No data available in table");?>

            </td>
        </tr>
    <?php }?>
    </tbody>
</table><?php }
}
