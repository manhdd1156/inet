<?php
/* Smarty version 3.1.31, created on 2021-03-31 10:50:27
  from "D:\workplace\Server11\content\themes\inet\templates\ci\school\school.tuitions.detail.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_6063f18383b779_96018515',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '31124064166242532f2e2f8c86fd4065af3bba53' => 
    array (
      0 => 'D:\\workplace\\Server11\\content\\themes\\inet\\templates\\ci\\school\\school.tuitions.detail.tpl',
      1 => 1573458086,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6063f18383b779_96018515 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="panel-body with-table">
    <table class="table table-bordered">
        <tr>
            <td class="col-sm-3 text-right"><strong><?php echo __("Class");?>
</strong></td>
            <td><?php echo $_smarty_tpl->tpl_vars['data']->value['group_title'];?>
</td>
        </tr>
        <tr>
            <td class="col-sm-3 text-right"><strong><?php echo __("Month");?>
</strong></td>
            <td><?php echo $_smarty_tpl->tpl_vars['data']->value['month'];?>
</td>
        </tr>
        <tr>
            <td class="col-sm-3 text-right"><strong><?php echo __("Paid");?>
</strong></td>
            <td><span class="class_paid_amount"><?php echo moneyFormat($_smarty_tpl->tpl_vars['data']->value['paid_amount']);?>
</span>/<?php echo moneyFormat($_smarty_tpl->tpl_vars['data']->value['total_amount']);?>
&nbsp;<?php echo @constant('MONEY_UNIT');?>

                &nbsp;(<span class="class_paid_count"><?php echo $_smarty_tpl->tpl_vars['data']->value['paid_count'];?>
</span> <?php echo __("children");?>
)
            </td>
        </tr>
        <tr>
            <td class="col-sm-3 text-right"><strong><?php echo __("Notify");?>
</strong></td>
            <td><?php if ($_smarty_tpl->tpl_vars['data']->value['is_notified'] == '1') {
echo __("Notified");
} else {
echo __("Not notified yet");
}?></td>
        </tr>
        <tr>
            <td class="col-sm-3 text-right"><strong><?php echo __("Description");?>
</strong></td>
            <td><?php echo $_smarty_tpl->tpl_vars['data']->value['description'];?>
</td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <?php if ($_smarty_tpl->tpl_vars['canEdit']->value) {?>
                    <?php if (!$_smarty_tpl->tpl_vars['data']->value['is_notified']) {?>
                        <a class="btn btn-default js_school-tuition" data-handle="notify" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"
                           data-id="<?php echo $_smarty_tpl->tpl_vars['data']->value['tuition_id'];?>
">
                            <?php echo __("Notify");?>

                        </a>
                    <?php }?>
                    <a class="btn btn-default"
                       href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/tuitions/newchild/<?php echo $_smarty_tpl->tpl_vars['data']->value['tuition_id'];?>
"><?php echo __("Add child");?>
</a>
                <?php }?>
                <a class="btn btn-default"
                   href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/tuitions/history/<?php echo $_smarty_tpl->tpl_vars['data']->value['tuition_id'];?>
"><?php echo __("Used last month");?>
</a>
                <?php if ($_smarty_tpl->tpl_vars['canEdit']->value) {?>
                    <?php if ($_smarty_tpl->tpl_vars['data']->value['paid_amount'] != $_smarty_tpl->tpl_vars['data']->value['total_amount']) {?>
                        <a class="btn btn-default"
                           href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/tuitions/edit/<?php echo $_smarty_tpl->tpl_vars['data']->value['tuition_id'];?>
"><?php echo __("Edit");?>
</a>
                    <?php }?>
                    <?php if (($_smarty_tpl->tpl_vars['data']->value['paid_amount'] == 0)) {?>
                        <a class="btn btn-danger js_school-tuition" data-handle="remove" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"
                           data-id="<?php echo $_smarty_tpl->tpl_vars['data']->value['tuition_id'];?>
"><?php echo __("Delete");?>
</a>
                    <?php }?>
                <?php }?>
            </td>
        </tr>
    </table>
    
    <?php if ($_smarty_tpl->tpl_vars['school']->value['tuition_view_attandance'] && ($_smarty_tpl->tpl_vars['school']->value['attendance_use_leave_early'] || $_smarty_tpl->tpl_vars['school']->value['attendance_use_come_late'] || $_smarty_tpl->tpl_vars['school']->value['attendance_absence_no_reason'])) {?>
        <table class="table table-striped table-bordered table-hover">
            <tbody>
            <tr>
                <td align="right"><strong><?php echo __("Symbol");?>
:</strong></td>
                <?php if ($_smarty_tpl->tpl_vars['school']->value['attendance_use_come_late']) {?>
                    <td bgcolor="#6495ed" align="center"><i class="fa fa-check" aria-hidden="true"></td>
                    <td align="left"><?php echo __("Come late");?>
</td>
                <?php }?>
                <?php if ($_smarty_tpl->tpl_vars['school']->value['attendance_use_leave_early']) {?>
                    <td bgcolor="#008b8b" align="center"><i class="fa fa-check" aria-hidden="true"></td>
                    <td align="left"><?php echo __("Leave early");?>
</td>
                <?php }?>
                <td align="center"><i class="fa fa-check" aria-hidden="true"></td>
                <td align="left"><?php echo __("Present");?>
</td>

                <?php if ($_smarty_tpl->tpl_vars['school']->value['attendance_absence_no_reason']) {?>
                    <td bgcolor="#ff1493" align="center"><i class="fa fa-times" aria-hidden="true"></td>
                    <td align="left"><?php echo __("Without permission");?>
</td>
                <?php }?>

                <td bgcolor="#deb887" align="center"><i class="fa fa-times" aria-hidden="true"></td>
                <td align="left"><?php echo __("With permission");?>
</td>
            </tr>
            </tbody>
        </table>
    <?php }?>
    <div class="table-responsive" id="children_list">
        <table class="table table-striped table-bordered table-hover">
            <thead>
            <?php if (count($_smarty_tpl->tpl_vars['data']->value['tuition_child']) > 0) {?>
                <tr>
                    <th>
                        <div style="float:right">
                            <a class="btn btn-success js_school-tuition-export" data-handle="export_all"
                               data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"
                               data-id="<?php echo $_smarty_tpl->tpl_vars['data']->value['tuition_id'];?>
"><?php echo __('Export all to Excel');?>
</a>
                            <label class="btn btn-info processing_label x-hidden"><?php echo __("Processing");?>
...</label>
                        </div>
                    </th>
                </tr>
            <?php }?>
            <tr>
                <th>
                    <div class="pull-left flip"><?php echo mb_strtoupper(__("Children list"), 'UTF-8');?>
&nbsp;(<?php echo count($_smarty_tpl->tpl_vars['data']->value['tuition_child']);?>
)</div>
                    <div class="pull-right flip">
                        <?php echo __("Total of class");?>
&nbsp;(<?php echo @constant('MONEY_UNIT');?>
)
                        <input type="text" class="text-right" value="<?php echo moneyFormat($_smarty_tpl->tpl_vars['data']->value['total_amount']);?>
" readonly/>
                    </div>
                </th>
            </tr>
            </thead>
            <tbody>
            <?php $_smarty_tpl->_assignInScope('detailIdx', 1);
?>
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['data']->value['tuition_child'], 'child', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['child']->value) {
?>
                <tr <?php if ($_smarty_tpl->tpl_vars['child']->value['status'] == 0) {?>class="bg_not_paid" <?php } else { ?>class="bg_paid"<?php }?>>
                    <td>
                        <div style=" display: inline;">
                            <strong><?php echo $_smarty_tpl->tpl_vars['detailIdx']->value;?>
 - <?php echo $_smarty_tpl->tpl_vars['child']->value['child_name'];?>
</strong>
                            <?php if (isset($_smarty_tpl->tpl_vars['child']->value['attendance_count']) && ($_smarty_tpl->tpl_vars['child']->value['attendance_count'] > 0)) {?>&nbsp;|&nbsp;
                                <?php echo __("Number of charged days");?>
 <?php echo __("month");?>
 <?php echo $_smarty_tpl->tpl_vars['child']->value['pre_month'];?>
:&nbsp;<?php echo $_smarty_tpl->tpl_vars['child']->value['attendance_count'];?>

                            <?php }?>
                        </div>
                        <div class="pull-right flip btn_no_margin_bot">
                            

                            <?php if ($_smarty_tpl->tpl_vars['canEdit']->value) {?>
                                <div class="tuition_paid notpaid_box notpaid_box_<?php echo $_smarty_tpl->tpl_vars['child']->value['tuition_child_id'];?>
 <?php if ($_smarty_tpl->tpl_vars['child']->value['status'] != @constant('TUITION_CHILD_NOT_PAID')) {?>hidden<?php }?>">
                                    <input style="width: 85px;" type="text" class="text-right money_tui child_total_amount_<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
"
                                           id="pay_amount_<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
" name="pay_amount"
                                           value="<?php echo $_smarty_tpl->tpl_vars['child']->value['total_amount'];?>
"/>
                                    <button style="margin-right: 5px" class="btn btn-xs btn-primary js_school-tuition"
                                            data-handle="confirm" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"
                                            data-amount="<?php echo $_smarty_tpl->tpl_vars['child']->value['total_amount'];?>
"
                                            data-tuition-id="<?php echo $_smarty_tpl->tpl_vars['data']->value['tuition_id'];?>
" data-child-id="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
"
                                            data-id="<?php echo $_smarty_tpl->tpl_vars['child']->value['tuition_child_id'];?>
"><?php echo __("Pay");?>
</button>
                                </div>
                            <?php }?>

                            <?php if ($_smarty_tpl->tpl_vars['canEdit']->value) {?>
                                <div class="tuition_paid parentpaid_box parentpaid_box_<?php echo $_smarty_tpl->tpl_vars['child']->value['tuition_child_id'];?>
 <?php if ($_smarty_tpl->tpl_vars['child']->value['status'] != @constant('TUITION_CHILD_PARENT_PAID')) {?>hidden<?php }?>">
                                    <button class="btn btn-xs btn-default js_school-tuition" data-handle="confirm"
                                            data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"
                                            data-amount="<?php echo $_smarty_tpl->tpl_vars['child']->value['total_amount'];?>
"
                                            data-tuition-id="<?php echo $_smarty_tpl->tpl_vars['data']->value['tuition_id'];?>
"
                                            data-child-id="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
"
                                            data-id="<?php echo $_smarty_tpl->tpl_vars['child']->value['tuition_child_id'];?>
"><?php echo __("Confirm");?>
</button>
                                    <button style="margin-right: 5px" class="btn btn-xs btn-danger js_school-tuition"
                                            data-handle="unconfirm" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"
                                            data-child-id="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
" data-tuition-id="<?php echo $_smarty_tpl->tpl_vars['data']->value['tuition_id'];?>
" data-amount="<?php echo $_smarty_tpl->tpl_vars['child']->value['total_amount'];?>
" data-parent="1"
                                            data-id="<?php echo $_smarty_tpl->tpl_vars['child']->value['tuition_child_id'];?>
"><?php echo __("Unconfirm");?>
</button>
                                    <input style="width: 100px;" type="text" class="text-right money_tui"
                                           id="pay_amount_<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
" name="pay_amount"
                                           value="<?php echo $_smarty_tpl->tpl_vars['child']->value['total_amount'];?>
"/>
                                </div>
                            <?php }?>

                            <div class="tuition_paid paid_box paid_box_<?php echo $_smarty_tpl->tpl_vars['child']->value['tuition_child_id'];?>
 <?php if ($_smarty_tpl->tpl_vars['child']->value['status'] != @constant('TUITION_CHILD_CONFIRMED')) {?>hidden<?php }?>">
                                <strong><?php echo __("Paid");?>
</strong> <?php if ($_smarty_tpl->tpl_vars['child']->value['paid_amount'] != $_smarty_tpl->tpl_vars['child']->value['total_amount']) {?><span
                                    class="paid_amount_<?php echo $_smarty_tpl->tpl_vars['child']->value['tuition_child_id'];?>
">
                                    (<?php echo moneyFormat($_smarty_tpl->tpl_vars['child']->value['paid_amount']);?>
/<?php echo moneyFormat($_smarty_tpl->tpl_vars['child']->value['total_amount']);?>

                                    )</span><?php }?>
                                <?php if ($_smarty_tpl->tpl_vars['canEdit']->value) {?>
                                    <button style="margin-right: 5px" class="btn btn-xs btn-danger js_school-tuition"
                                            data-handle="unconfirm" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"
                                            data-child-id="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
" data-tuition-id="<?php echo $_smarty_tpl->tpl_vars['data']->value['tuition_id'];?>
" data-amount="<?php echo $_smarty_tpl->tpl_vars['child']->value['total_amount'];?>
" data-parent="0"
                                            data-id="<?php echo $_smarty_tpl->tpl_vars['child']->value['tuition_child_id'];?>
"><?php echo __("Unconfirm");?>
</button>
                                    <input type="hidden" id="pay_amount_<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
" name="pay_amount"
                                           value="<?php echo $_smarty_tpl->tpl_vars['child']->value['paid_amount'];?>
" class="amount_<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
"/>
                                <?php }?>
                            </div>

                            <?php if ($_smarty_tpl->tpl_vars['canEdit']->value) {?>
                                <button style="margin-right: 5px"
                                        class="btn btn-xs btn-success js_school-tuition-export"
                                        data-handle="export" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"
                                        data-id="<?php echo $_smarty_tpl->tpl_vars['child']->value['tuition_id'];?>
" data-child-id="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
"
                                        data-amount="<?php echo moneyFormat($_smarty_tpl->tpl_vars['child']->value['total_amount']);?>
"> <?php echo __('Export to Excel');?>
 </button>
                                <a class="btn btn-xs btn-default js_show_hide_tuition_tbody"
                                   child_id="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
"><?php echo __("Show/hide");?>
</a>
                                <button class="btn btn-xs btn-danger js_school-delete-tuition-child"
                                        data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-total="<?php echo $_smarty_tpl->tpl_vars['child']->value['total_amount'];?>
"
                                        data-id="<?php echo $_smarty_tpl->tpl_vars['child']->value['tuition_child_id'];?>
"><?php echo __('Delete');?>
</button>
                            <?php }?>
                        </div>
                        
                        <?php if ($_smarty_tpl->tpl_vars['school']->value['tuition_view_attandance'] && (count($_smarty_tpl->tpl_vars['child']->value['attendances']) > 0)) {?>
                            <table class="table table-striped table-bordered table-hover"
                                   id="tbl_attendance_<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
" style="display: none">
                                <tr>
                                    <td style="width: 90px"><a
                                                href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/attendance/child/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
"
                                                target="_blank"><strong><?php echo __("Attendance");?>
</strong></a></td>
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['child']->value['attendances'], 'attendance');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['attendance']->value) {
?>
                                        <td style="padding: 5px" align="center" class="align_middle"
                                                <?php if ($_smarty_tpl->tpl_vars['attendance']->value['status'] == @constant('ATTENDANCE_COME_LATE')) {?>
                                                    bgcolor="#6495ed"
                                                <?php } elseif ($_smarty_tpl->tpl_vars['attendance']->value['status'] == @constant('ATTENDANCE_EARLY_LEAVE')) {?>
                                                    bgcolor="#008b8b"
                                                <?php } elseif ($_smarty_tpl->tpl_vars['attendance']->value['status'] == @constant('ATTENDANCE_ABSENCE_NO_REASON')) {?>
                                                    bgcolor="#ff1493"
                                                <?php } elseif ($_smarty_tpl->tpl_vars['attendance']->value['status'] == @constant('ATTENDANCE_ABSENCE')) {?>
                                                    bgcolor="#deb887"
                                                <?php }?>
                                        ><?php echo $_smarty_tpl->tpl_vars['attendance']->value['display_date'];?>
</td>
                                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                </tr>
                            </table>
                        <?php }?>
                        <br/>
                        <?php if (count($_smarty_tpl->tpl_vars['child']->value['tuition_detail']) > 0) {?>
                            <table class="table table-striped table-bordered table-hover bg_white"
                                   id="tbl_fee_<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
" style="display: none">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th><?php echo __("Fee name");?>
</th>
                                    <th style="width: 52px"><?php echo __("Qty");?>
</th>
                                    <th style="width: 117px"><?php echo __("Unit price");?>
</th>
                                    <th style="width: 107px"><?php echo __("Deduction");?>
</th>
                                    <th style="width: 117px"><?php echo __("Money amount");?>
</th>
                                </tr>
                                </thead>
                                <tbody>
                                
                                <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['child']->value['tuition_detail'], 'detail');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['detail']->value) {
?>
                                    <tr>
                                        <td align="center"><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</td>
                                        <?php if ($_smarty_tpl->tpl_vars['detail']->value['type'] == @constant('TUITION_DETAIL_FEE')) {?>
                                            <td><?php echo $_smarty_tpl->tpl_vars['detail']->value['fee_name'];?>
</td>
                                            <td class="text-center"><?php echo $_smarty_tpl->tpl_vars['detail']->value['quantity'];?>
</td>
                                            <td class="text-right"><?php echo moneyFormat($_smarty_tpl->tpl_vars['detail']->value['unit_price']);?>
</td>
                                            <?php if ($_smarty_tpl->tpl_vars['detail']->value['fee_type'] != @constant('FEE_TYPE_MONTHLY')) {?>
                                                <td class="text-center"><?php if ($_smarty_tpl->tpl_vars['detail']->value['quantity_deduction'] != 0) {
echo $_smarty_tpl->tpl_vars['detail']->value['quantity_deduction'];?>
 (<?php echo __('Day');?>
)<?php }?></td>
                                            <?php } else { ?>
                                                <td class="text-right"><?php if ($_smarty_tpl->tpl_vars['detail']->value['unit_price_deduction'] != 0 && $_smarty_tpl->tpl_vars['detail']->value['quantity_deduction'] != 0) {
echo moneyFormat($_smarty_tpl->tpl_vars['detail']->value['unit_price_deduction']);?>
(<?php echo @constant('MONEY_UNIT');?>
)<?php }?></td>
                                            <?php }?>
                                        <?php } elseif ($_smarty_tpl->tpl_vars['detail']->value['type'] == @constant('TUITION_DETAIL_SERVICE')) {?>
                                            <td><?php echo $_smarty_tpl->tpl_vars['detail']->value['service_name'];?>
</td>
                                            <td class="text-center"><?php echo $_smarty_tpl->tpl_vars['detail']->value['quantity'];?>
</td>
                                            <td class="text-right"><?php echo moneyFormat($_smarty_tpl->tpl_vars['detail']->value['unit_price']);?>
</td>
                                            <?php if ($_smarty_tpl->tpl_vars['detail']->value['service_type'] == @constant('SERVICE_TYPE_DAILY')) {?>
                                                <td class="text-center"><?php if ($_smarty_tpl->tpl_vars['detail']->value['quantity_deduction'] != 0) {
echo $_smarty_tpl->tpl_vars['detail']->value['quantity_deduction'];?>
 (<?php echo __('Day');?>
)<?php }?></td>
                                            <?php } elseif ($_smarty_tpl->tpl_vars['detail']->value['service_type'] == @constant('SERVICE_TYPE_MONTHLY')) {?>
                                                <td class="text-right"><?php if ($_smarty_tpl->tpl_vars['detail']->value['unit_price_deduction'] != 0 && $_smarty_tpl->tpl_vars['detail']->value['quantity_deduction'] != 0) {
echo moneyFormat($_smarty_tpl->tpl_vars['detail']->value['unit_price_deduction']);?>
(<?php echo @constant('MONEY_UNIT');?>
)<?php }?></td>
                                            <?php } else { ?>
                                                <td class="text-center"><?php if ($_smarty_tpl->tpl_vars['detail']->value['quantity_deduction'] != 0) {
echo $_smarty_tpl->tpl_vars['detail']->value['quantity_deduction'];?>
 (<?php echo __('Times');?>
)<?php }?></td>
                                            <?php }?>
                                        <?php } else { ?>
                                            <td colspan="4"><?php echo __('Late PickUp');?>
</td>
                                        <?php }?>
                                        <td class="text-right">
                                            <?php if ($_smarty_tpl->tpl_vars['detail']->value['type'] != @constant('LATE_PICKUP_FEE')) {?>
                                                <?php echo moneyFormat(($_smarty_tpl->tpl_vars['detail']->value['quantity']*$_smarty_tpl->tpl_vars['detail']->value['unit_price']-$_smarty_tpl->tpl_vars['detail']->value['quantity_deduction']*$_smarty_tpl->tpl_vars['detail']->value['unit_price_deduction']));?>

                                            <?php } else { ?>
                                                <?php echo moneyFormat($_smarty_tpl->tpl_vars['detail']->value['unit_price']);?>

                                            <?php }?>
                                        </td>
                                    </tr>
                                    <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>


                                
                                <?php if ($_smarty_tpl->tpl_vars['child']->value['debt_amount'] != 0) {?>
                                    <tr>
                                        <td colspan="5" class="text-right"><?php echo __("Debt amount of previous month");?>

                                            &nbsp;(<?php echo @constant('MONEY_UNIT');?>
)&nbsp;<?php echo $_smarty_tpl->tpl_vars['child']->value['pre_month'];?>
</td>
                                        <td class="text-right"><?php echo moneyFormat($_smarty_tpl->tpl_vars['child']->value['debt_amount']);?>
</td>
                                    </tr>
                                <?php }?>
                                <tr>
                                    <td colspan="3">

                                    </td>
                                    <td colspan="2" class="text-right"><strong><?php echo __("Total");?>

                                            &nbsp;(<?php echo @constant('MONEY_UNIT');?>
)</strong></td>
                                    <td class="text-right"><strong><?php echo moneyFormat($_smarty_tpl->tpl_vars['child']->value['total_amount']);?>
</strong></td>
                                </tr>
                                

                                    <tr class="tuition_paid_amount_<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
 <?php if (($_smarty_tpl->tpl_vars['child']->value['status'] != @constant('TUITION_CHILD_CONFIRMED'))) {?>x-hidden<?php }?>">
                                        <td colspan="5" class="text-right"><strong><?php echo __("Paid");?>

                                                &nbsp;(<?php echo @constant('MONEY_UNIT');?>
)</strong></td>
                                        <td class="text-right"><strong><span class="paid_amount_td_<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
"><?php echo moneyFormat($_smarty_tpl->tpl_vars['child']->value['paid_amount']);?>
</span></strong>
                                        </td>
                                    </tr>
                                    <tr class="tuition_debt_amount_<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
 <?php if (($_smarty_tpl->tpl_vars['child']->value['status'] != @constant('TUITION_CHILD_CONFIRMED'))) {?>x-hidden<?php }?>">
                                        <td colspan="5" class="text-right"><strong><?php echo __("Debt amount of this month");?>

                                                &nbsp;(<?php echo @constant('MONEY_UNIT');?>
)</strong></td>
                                        <td class="text-right">
                                            <strong><span class="debt_amount_td_<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
"><?php echo moneyFormat($_smarty_tpl->tpl_vars['child']->value['total_amount']-$_smarty_tpl->tpl_vars['child']->value['paid_amount']);?>
</span></strong>
                                        </td>
                                    </tr>

                                <?php ob_start();
echo $_smarty_tpl->tpl_vars['child']->value['description'];
$_prefixVariable1=ob_get_clean();
if ((isset($_smarty_tpl->tpl_vars['child']->value['description']) && ($_prefixVariable1 != ''))) {?>
                                    <tr>
                                        <td colspan="2" class="text-right"><strong><?php echo __("Description");?>
</strong></td>
                                        <td colspan="4"><?php echo $_smarty_tpl->tpl_vars['child']->value['description'];?>
</td>
                                    </tr>
                                <?php }?>
                                </tbody>
                            </table>
                        <?php }?>
                    </td>
                </tr>
                <tr height="10"></tr>
                <?php $_smarty_tpl->_assignInScope('detailIdx', $_smarty_tpl->tpl_vars['detailIdx']->value+1);
?>
            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

            </tbody>
        </table>
        <?php if (count($_smarty_tpl->tpl_vars['data']->value['tuition_child']) > 0) {?>
            <a class="btn btn-success js_school-tuition-export" data-handle="export_all" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"
               data-id="<?php echo $_smarty_tpl->tpl_vars['child']->value['tuition_id'];?>
"><?php echo __('Export all to Excel');?>
</a>
            <label class="btn btn-info processing_label x-hidden"><?php echo __("Processing");?>
...</label>
        <?php }?>
        
        
    </div>
</div>
<?php }
}
