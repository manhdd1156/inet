<?php
/* Smarty version 3.1.31, created on 2021-03-31 08:54:21
  from "D:\workplace\Server11\content\themes\inet\templates\ci\noga\ajax.noga.statistic.useronline.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_6063d64da635d3_78534190',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '1335abc950aed5f210ee66a188b5cf1aadd85e48' => 
    array (
      0 => 'D:\\workplace\\Server11\\content\\themes\\inet\\templates\\ci\\noga\\ajax.noga.statistic.useronline.tpl',
      1 => 1552404703,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6063d64da635d3_78534190 (Smarty_Internal_Template $_smarty_tpl) {
?>



<table class="table table-striped table-bordered table-hover js_dataTable">
    <thead>
    <tr>
        <th>#</th>
        <th><?php echo __("Date");?>
</th>
        <th><?php echo __("User fullname");?>
</th>
        <th><?php echo __("Username");?>
</th>
        <th><?php echo __("User email");?>
</th>
        
    </tr>
    </thead>
    <tbody>
    <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['useronline']->value, 'users', false, 'date');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['date']->value => $_smarty_tpl->tpl_vars['users']->value) {
?>
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['users']->value, 'user');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['user']->value) {
?>
            <tr>
                <td align="center"><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</td>
                <td align="center"><?php echo $_smarty_tpl->tpl_vars['date']->value;?>
</td>
                <td><a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['user']->value['user_name'];?>
" target="_blank"><?php echo $_smarty_tpl->tpl_vars['user']->value['user_fullname'];?>
</a><?php if ($_smarty_tpl->tpl_vars['user']->value['of_school']) {?> (uos)<?php } else { ?> (nos)<?php }?></td>
                <td><?php echo $_smarty_tpl->tpl_vars['user']->value['user_name'];?>
</td>
                <td><?php echo $_smarty_tpl->tpl_vars['user']->value['user_email'];?>
</td>
                
                    
                        
                        
                            
                        
                    
                    
                        
                        
                        
                            
                        
                    
                    
                        
                        
                        
                            
                        
                    
                
            </tr>
            <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

    </tbody>
</table>

<?php echo '<script'; ?>
 type="text/javascript">
    $(function() {
        // run DataTable
        $('.js_dataTable').DataTable({
            "aoColumnDefs": [ { 'aDataSort': false, 'aTargets': [ -1 ] } ],
            "language": {
                "decimal":        "",
                "emptyTable":     __["No data available in table"],
                "info":           __["Showing _START_ to _END_ of _TOTAL_ results"],
                "infoEmpty":      __["Showing 0 to 0 of 0 results"],
                "infoFiltered":   "(filtered from _MAX_ total entries)",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     __["Show"] + " " + "_MENU_" + " " + __["results"],
                "loadingRecords": __["Loading..."],
                "processing":     __["Processing..."],
                "search":         __["Search"],
                "zeroRecords":    __["No matching records found"],
                "paginate": {
                    "first":      __["First"],
                    "last":       __["Last"],
                    "next":       __["Next"],
                    "previous":   __["Previous"]
                },
                "aria": {
                    "sortAscending":  ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                }
            }
        });
    });
<?php echo '</script'; ?>
><?php }
}
