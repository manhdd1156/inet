<?php
/* Smarty version 3.1.31, created on 2021-05-05 10:37:02
  from "D:\workplace\inet-project\Server11\content\themes\inet\templates\ci\child\child.services.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_609212deeee2e4_38373854',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '792327b8ea96c8f6cb555f3e386174e88653c3bc' => 
    array (
      0 => 'D:\\workplace\\mascom-edu-server\\Server11\\content\\themes\\inet\\templates\\ci\\child\\child.services.tpl',
      1 => 1619677394,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:ci/child/ajax.child.servicelist4record.tpl' => 1,
  ),
),false)) {
function content_609212deeee2e4_38373854 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <div class="pull-right flip">
            <a href="https://blog.coniu.vn/huong-dan-phu-huynh-dang-ky-dich-vu-tren-website-inet/" class="btn btn-info btn_guide" target="_blank">
                <i class="fa fa-info-circle"></i> <?php echo __("Guide");?>

            </a>
        </div>
        <i class="fa fa-life-ring fa-lg fa-fw pr10"></i>
        <?php echo __("Service");?>

        <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
            &rsaquo; <?php echo __('Service usage information');?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "reg") {?>
            &rsaquo; <?php echo __('Register service');?>

        <?php }?>
    </div>
    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
        <div class="panel-body with-table">
            <div align="center"><strong><?php echo __("Monthly and daily services");?>
</strong></div>
            <br/>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th><?php echo __("Service");?>
</th>
                        <th><?php echo __("Service fee");?>
&nbsp;(<?php echo @constant('MONEY_UNIT');?>
)</th>
                        <th><?php echo __("Service type");?>
</th>
                        <th><?php echo __("Time");?>
</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['ncb_services']->value, 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
                        <tr>
                            <td align="center"><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</td>
                            <td><?php echo $_smarty_tpl->tpl_vars['row']->value['service_name'];?>
</td>
                            <td align="right"><?php echo moneyFormat($_smarty_tpl->tpl_vars['row']->value['fee']);?>
</td>
                            <td align="center">
                                <?php if ($_smarty_tpl->tpl_vars['row']->value['type'] == @constant('SERVICE_TYPE_MONTHLY')) {?>
                                    <?php echo __("Monthly service");?>

                                <?php } elseif ($_smarty_tpl->tpl_vars['row']->value['type'] == @constant('SERVICE_TYPE_DAILY')) {?>
                                    <?php echo __("Daily service");?>

                                <?php }?>
                            </td>
                            <td align="center"><?php echo $_smarty_tpl->tpl_vars['row']->value['begin'];?>
&nbsp;-&nbsp;<?php echo $_smarty_tpl->tpl_vars['row']->value['end'];?>
</td>
                        </tr>
                        <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>


                    <?php if ($_smarty_tpl->tpl_vars['idx']->value == 1) {?>
                        <tr class="odd">
                            <td valign="top" align="center" colspan="5" class="dataTables_empty">
                                <?php echo __("No data available in table");?>

                            </td>
                        </tr>
                    <?php }?>

                    </tbody>
                </table>
            </div>
        </div>
        <div class="border_bot" style="width: 500px; margin: auto; padding: auto"></div>
        <div class="panel-body with-table">
            <div align="center"><strong><?php echo __("Count-based service");?>
</strong></div>
            <br/>
            <form class="js_ajax-forms form-horizontal" data-url="#">
                <div class="form-group pl5">
                    <div class='col-sm-4'>
                        <select name="service_id" id="service_id" class="form-control">
                            <option value=""><?php echo __("All services");?>
</option>
                            <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['cb_services']->value, 'service');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['service']->value) {
?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
 - <?php echo $_smarty_tpl->tpl_vars['service']->value['service_name'];?>
</option>
                                <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </select>
                    </div>
                    <div class="col-sm-3">
                        <div class='input-group date' id='history_begin_picker'>
                            <input type='text' name="begin" id="begin" value="<?php echo $_smarty_tpl->tpl_vars['begin']->value;?>
" class="form-control" placeholder="<?php echo __("From date");?>
"/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class='input-group date' id='history_end_picker'>
                            <input type='text' name="end" id="end" class="form-control" placeholder="<?php echo __("To date");?>
"/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                    <div class='col-sm-2'>
                        <div class="form-group pl10">
                            <a href="#" id="search" class="btn btn-default js_service-search-history" data-sid="<?php echo $_smarty_tpl->tpl_vars['school_id']->value;?>
" data-cid="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
" <?php if (count($_smarty_tpl->tpl_vars['cb_services']->value) == 0) {?>disabled<?php }?>><?php echo __("Search");?>
</a>
                            <label id="loading" class="btn btn-info x-hidden"><?php echo __("Loading");?>
...</label>
                        </div>
                    </div>
                </div>
                <div class="table-responsive" id="usage_history" name="usage_history">
                    
                </div>
            </form>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "reg") {?>
        <div class="panel-body with-table">
            <form class="js_ajax-forms-success-reload form-horizontal" data-url="ci/bo/child/bochild_all.php">
                <input type="hidden" name="school_username" id="school_username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                <input type="hidden" name="do" id="do" value="reg"/>
                <input type="hidden" id = "school_id" name="school_id" value="<?php echo $_smarty_tpl->tpl_vars['school']->value['page_id'];?>
"/>
                <input type="hidden" id = "child_id" name="child_id" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
"/>

                <div><strong><?php echo __("Monthly and daily services");?>
</strong></div>
                <div class = "table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th><?php echo __("Register");?>
</th>
                            <th><?php echo __("Service");?>
</th>
                            <th><?php echo __("Service fee");?>
&nbsp;(<?php echo @constant('MONEY_UNIT');?>
)</th>
                            <th><?php echo __("Time");?>
</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                        <?php $_smarty_tpl->_assignInScope('cnt', 0);
?>
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['ncb_services']->value, 'service');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['service']->value) {
?>
                            <tr>
                                <td align="center"><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</td>
                                <td align="center">
                                    <?php if (isset($_smarty_tpl->tpl_vars['service']->value['service_child_id']) && ($_smarty_tpl->tpl_vars['service']->value['service_child_id'] > 0)) {?>
                                        <?php echo __("Registered");?>

                                    <?php } else { ?>
                                        <input type="checkbox" class="child" name="ncbServiceIds[]" value="<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
">
                                        <?php $_smarty_tpl->_assignInScope('cnt', $_smarty_tpl->tpl_vars['cnt']->value+1);
?>
                                    <?php }?>
                                </td>
                                <td><?php echo $_smarty_tpl->tpl_vars['service']->value['service_name'];?>
</td>
                                <td align="right"><?php echo moneyFormat($_smarty_tpl->tpl_vars['service']->value['fee']);?>
</td>
                                <td align="center">
                                    <?php if (isset($_smarty_tpl->tpl_vars['service']->value['service_child_id']) && ($_smarty_tpl->tpl_vars['service']->value['service_child_id'] > 0)) {?>
                                        <?php echo $_smarty_tpl->tpl_vars['service']->value['begin'];?>
&nbsp;-&nbsp;<?php echo $_smarty_tpl->tpl_vars['service']->value['end'];?>

                                    <?php }?>
                                </td>
                            </tr>
                            <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </tbody>
                    </table>
                    <input type="hidden" id="ncb_cnt" value="<?php echo $_smarty_tpl->tpl_vars['cnt']->value;?>
"/>
                </div>

                <div><strong><?php echo __("Count-based service");?>
</strong></div>
                <div class="form-group pl5">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Select date");?>
(*):</label>
                    <div class="col-sm-3" id="using_at_div">
                        <div class='input-group date' id='using_time_picker'>
                            <input type="text" name="using_at" id="using_at" value="<?php echo $_smarty_tpl->tpl_vars['using_at']->value;?>
" class="form-control" placeholder="<?php echo __("Using time");?>
 (*)" required/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                </div>

                <div class="table-responsive" id="service_list" name="service_list">
                    <?php $_smarty_tpl->_subTemplateRender("file:ci/child/ajax.child.servicelist4record.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                </div>
                <div class="form-group pl5" id="service_btnSave">
                    <div class="col-sm-9">
                        <button type="submit" class="btn btn-primary padrl30" <?php if ((((isset($_smarty_tpl->tpl_vars['total']->value) && $_smarty_tpl->tpl_vars['total']->value == 0) || count($_smarty_tpl->tpl_vars['cb_services']->value) == 0)) && ($_smarty_tpl->tpl_vars['cnt']->value == 0)) {?>disabled<?php }?>><?php echo __("Save");?>
</button>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    <?php }?>
</div><?php }
}
