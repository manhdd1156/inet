<?php
/* Smarty version 3.1.31, created on 2021-03-31 10:17:43
  from "D:\workplace\Server11\content\themes\inet\templates\ci\school\ajax.school.categorydetail.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_6063e9d7d03544_01634881',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '6dde96d63c8278def78982f14946ee52422896ba' => 
    array (
      0 => 'D:\\workplace\\Server11\\content\\themes\\inet\\templates\\ci\\school\\ajax.school.categorydetail.tpl',
      1 => 1552404702,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6063e9d7d03544_01634881 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="mt10" align="center">
    <strong><?php echo __("Edit category");?>
</strong>
</div>
<div class="panel-body with-table">
    <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_report.php">
        <input type="hidden" name="school_username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
        <input type="hidden" name="do" value="edit_cate_in_temp"/>
        <input type="hidden" name="category_id" value="<?php echo $_smarty_tpl->tpl_vars['results']->value['report_template_category_id'];?>
"/>
        <div class = "form-group">
            <label class="col-sm-12 control-label text-left" style="text-align: center"><?php echo __("Category name");?>
: <?php echo $_smarty_tpl->tpl_vars['results']->value['category_name'];?>
</label>
            <input type="hidden" class="form-control" name="category_name" value="<?php echo $_smarty_tpl->tpl_vars['results']->value['category_name'];?>
">
        </div>
        <div class="table-responsive" id="cate_school_add">
            <table class="table table-striped table-bordered table-hover" id = "editCategoryTable">
                <thead>
                <tr><th colspan="3"><?php echo __("Suggest content for category");?>
</th></tr>
                <tr>
                    <th>
                        <?php echo __('No.');?>

                    </th>
                    <th>
                        <?php echo __('Title');?>

                    </th>
                    <th>
                        <?php echo __('Actions');?>

                    </th>
                </tr>
                </thead>
                <tbody>
                <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['results']->value['suggests'], 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
                    <tr>
                        <td class = "col_no align_middle"  align = "center"><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</td>
                        <td>
                            <input type="text" name = "suggests" class = "form-control" value="<?php echo $_smarty_tpl->tpl_vars['row']->value;?>
">
                        </td>
                        <td align="center" class="align_middle"> <a class="btn btn-danger btn-xs js_report_template-delete"> <?php echo __("Delete");?>
 </a></td>
                    </tr>
                    <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                </tbody>
            </table>
            
                <a class="btn btn-default js_report_suggest-add-temp"><?php echo __("Add new suggest");?>
</a>
                <a class="btn btn-primary padrl30 js_report-category-edit"><?php echo __("Save");?>
</a>
            
        </div>
        <!-- success -->
        <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
        <!-- success -->

        <!-- error -->
        <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
        <!-- error -->
    </form>
</div><?php }
}
