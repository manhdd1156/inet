<?php
/* Smarty version 3.1.31, created on 2021-04-23 11:22:48
  from "D:\workplace\mascom-edu-server\Server11\content\themes\inet\templates\_widget.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_60824b98030927_68652693',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '2468ec4a7f51848a6f29ab51efa072ebc162515b' => 
    array (
      0 => 'D:\\workplace\\mascom-edu-server\\Server11\\content\\themes\\inet\\templates\\_widget.tpl',
      1 => 1552404706,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60824b98030927_68652693 (Smarty_Internal_Template $_smarty_tpl) {
if ($_smarty_tpl->tpl_vars['widgets']->value) {?>
    <!-- Widgets -->
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['widgets']->value, 'widget');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['widget']->value) {
?>
        <div class="panel panel-default panel-widget">
            <div class="panel-heading">
                <strong><?php echo $_smarty_tpl->tpl_vars['widget']->value['title'];?>
</strong>
            </div>
            <div class="panel-body"><?php echo $_smarty_tpl->tpl_vars['widget']->value['code'];?>
</div>
        </div>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

    <!-- Widgets -->
<?php }
}
}
