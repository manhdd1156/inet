<?php
/* Smarty version 3.1.31, created on 2021-04-29 17:21:38
  from "D:\workplace\mascom-edu-server\Server11\content\themes\inet\templates\ci\class\ajax.service.history.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_608a88b2866cf3_31722143',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '11dbd0527c1a007a0acf7ab49b53181d79e0f0d9' => 
    array (
      0 => 'D:\\workplace\\mascom-edu-server\\Server11\\content\\themes\\inet\\templates\\ci\\class\\ajax.service.history.tpl',
      1 => 1619677394,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_608a88b2866cf3_31722143 (Smarty_Internal_Template $_smarty_tpl) {
if ($_smarty_tpl->tpl_vars['serviceId']->value == 0) {?>
    
    <div><strong><?php echo __("Monthly and daily services");?>
</strong></div>
    <table class="table table-striped table-bordered table-hover">
        <thead>
        <tr>
            <th>#</th>
            <th><?php echo __("Service name");?>
</th>
            <th><?php echo __("Time");?>
</th>
        </tr>
        </thead>
        <tbody>
        <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['ncb_services']->value, 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
            <tr>
                <td align="center"><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</td>
                <td align="center"><?php echo $_smarty_tpl->tpl_vars['row']->value['service_name'];?>
</td>
                <td align="center">
                    <?php if ($_smarty_tpl->tpl_vars['row']->value['begin'] != '') {?>
                        <?php echo $_smarty_tpl->tpl_vars['row']->value['begin'];?>
&nbsp;-&nbsp;<?php echo $_smarty_tpl->tpl_vars['row']->value['end'];?>

                    <?php }?>
                </td>
            </tr>
            <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>


        <?php if ($_smarty_tpl->tpl_vars['idx']->value == 1) {?>
            <tr class="odd">
                <td valign="top" align="center" colspan="3" class="dataTables_empty">
                    <?php echo __("No data available in table");?>

                </td>
            </tr>
        <?php }?>

        </tbody>
    </table>
<?php }?>


<div><strong><?php echo __("Count-based service");?>
&nbsp;(<?php echo count($_smarty_tpl->tpl_vars['history']->value);?>
 <?php echo __("times");?>
)</strong></div>
<table class="table table-striped table-bordered table-hover">
    <?php if ($_smarty_tpl->tpl_vars['childId']->value > 0) {?>
        
        <thead>
        <tr>
            <th>#</th>
            <th><?php echo __("Using time");?>
</th>
            <th><?php echo __("Registrar");?>
</th>
            <th><?php echo __("Registered time");?>
</th>
        </tr>
        </thead>
        <tbody>
        <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
        <?php $_smarty_tpl->_assignInScope('serviceTotal', 0);
?>
        <?php $_smarty_tpl->_assignInScope('serviceId', -1);
?>
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['history']->value, 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
            <?php if ($_smarty_tpl->tpl_vars['row']->value['service_id'] != $_smarty_tpl->tpl_vars['serviceId']->value) {?>
                <?php if ($_smarty_tpl->tpl_vars['idx']->value > 1) {?>
                    <tr>
                        <td colspan="2" align="center"><strong><font color="blue"><?php echo __("Total of service");?>
</font></strong></td>
                        <td colspan="2" align="left"><strong><font color="blue"><?php echo $_smarty_tpl->tpl_vars['serviceTotal']->value;?>
 <?php echo __("times");?>
</font></strong></td>
                    </tr>
                <?php }?>
                <tr>
                    <td colspan="4"><strong><?php echo $_smarty_tpl->tpl_vars['row']->value['service_name'];?>
</strong></</td>
                </tr>
                <?php $_smarty_tpl->_assignInScope('serviceTotal', 0);
?>
            <?php }?>
            <tr>
                <td align="center"><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</td>
                <td align="center"><?php echo $_smarty_tpl->tpl_vars['row']->value['using_at'];?>
</td>
                <td align="center"><?php echo $_smarty_tpl->tpl_vars['row']->value['user_fullname'];?>
</td>
                <td align="center"><?php echo $_smarty_tpl->tpl_vars['row']->value['recorded_at'];?>
</td>
            </tr>
            <?php $_smarty_tpl->_assignInScope('serviceId', $_smarty_tpl->tpl_vars['row']->value['service_id']);
?>
            <?php $_smarty_tpl->_assignInScope('serviceTotal', $_smarty_tpl->tpl_vars['serviceTotal']->value+1);
?>
            <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

        <?php if ($_smarty_tpl->tpl_vars['idx']->value > 2) {?>
            <tr>
                <td colspan="2" align="center"><strong><font color="blue"><?php echo __("Total of service");?>
</font></strong></td>
                <td colspan="2" align="left"><strong><font color="blue"><?php echo $_smarty_tpl->tpl_vars['serviceTotal']->value;?>
 <?php echo __("times");?>
</font></strong></td>
            </tr>
            <tr>
                <td colspan="2" align="center"><strong><font color="red"><?php echo mb_strtoupper(__("Total"), 'UTF-8');?>
</font></strong></td>
                <td colspan="2" align="left"><strong><font color="red"><?php echo $_smarty_tpl->tpl_vars['idx']->value-1;?>
 <?php echo __("times");?>
</font></strong></td>
            </tr>
        <?php }?>

        <?php if (count($_smarty_tpl->tpl_vars['history']->value) == 0) {?>
            <tr class="odd">
                <td valign="top" align="center" colspan="4" class="dataTables_empty">
                    <?php echo __("No data available in table");?>

                </td>
            </tr>
        <?php }?>

        </tbody>
    <?php } else { ?>
        
        <thead>
        <tr>
            <th>#</th>
            <th><?php echo __("Full name");?>
</th>
            <th><?php echo __("Registrar");?>
</th>
            <th><?php echo __("Registered time");?>
</th>
        </tr>
        </thead>
        <tbody>
        <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
        <?php $_smarty_tpl->_assignInScope('dayTotal', 0);
?>
        <?php $_smarty_tpl->_assignInScope('usingAt', 'noga');
?>
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['history']->value, 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
            <?php if ($_smarty_tpl->tpl_vars['row']->value['using_at'] != $_smarty_tpl->tpl_vars['usingAt']->value) {?>
                <?php if ($_smarty_tpl->tpl_vars['idx']->value > 1) {?>
                    <tr>
                        <td colspan="2" align="center"><strong><font color="blue"><?php echo __("Total of day");?>
</font></strong></td>
                        <td colspan="2" align="left"><strong><font color="blue"><?php echo $_smarty_tpl->tpl_vars['dayTotal']->value;?>
 <?php echo __("times");?>
</font></strong></td>
                    </tr>
                <?php }?>
                <tr>
                    <td colspan="4"><strong><?php echo $_smarty_tpl->tpl_vars['row']->value['using_at'];?>
</strong></</td>
                </tr>
                <?php $_smarty_tpl->_assignInScope('dayTotal', 0);
?>
            <?php }?>
            <tr <?php if ($_smarty_tpl->tpl_vars['row']->value['child_status'] == 0) {?> class="row-disable" <?php }?>>
                <td align="center"><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</td>
                <td><?php echo $_smarty_tpl->tpl_vars['row']->value['child_name'];?>
</td>
                <td><?php echo $_smarty_tpl->tpl_vars['row']->value['user_fullname'];?>
</td>
                <td><?php echo $_smarty_tpl->tpl_vars['row']->value['recorded_at'];?>
</td>
            </tr>
            <?php $_smarty_tpl->_assignInScope('usingAt', $_smarty_tpl->tpl_vars['row']->value['using_at']);
?>
            <?php $_smarty_tpl->_assignInScope('dayTotal', $_smarty_tpl->tpl_vars['dayTotal']->value+1);
?>
            <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

        <?php if ($_smarty_tpl->tpl_vars['idx']->value > 2) {?>
            <tr>
                <td colspan="2" align="center"><strong><font color="blue"><?php echo __("Total of day");?>
</font></strong></td>
                <td colspan="2" align="left"><strong><font color="blue"><?php echo $_smarty_tpl->tpl_vars['dayTotal']->value;?>
 <?php echo __("times");?>
</font></strong></td>
            </tr>
            <tr>
                <td colspan="2" align="center"><strong><font color="red"><?php echo mb_strtoupper(__("Total"), 'UTF-8');?>
</font></strong></td>
                <td colspan="2" align="left"><strong><font color="red"><?php echo $_smarty_tpl->tpl_vars['idx']->value-1;?>
 <?php echo __("times");?>
</font></strong></td>
            </tr>
        <?php }?>

        <?php if ($_smarty_tpl->tpl_vars['idx']->value == 1) {?>
            <tr class="odd">
                <td valign="top" align="center" colspan="4" class="dataTables_empty">
                    <?php echo __("No data available in table");?>

                </td>
            </tr>
        <?php }?>
        </tbody>
    <?php }?>
</table><?php }
}
