<?php
/* Smarty version 3.1.31, created on 2021-05-20 17:00:17
  from "D:\workplace\Inet-project\source\Server11\content\themes\inet\templates\ci\school\teacher_pickup\ajax.pickuplist.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_60a63331e903d5_33741300',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '9ce27a5c3117016e9d35ee2d4adcd5156a7082a2' => 
    array (
      0 => 'D:\\workplace\\Inet-project\\source\\Server11\\content\\themes\\inet\\templates\\ci\\school\\teacher_pickup\\ajax.pickuplist.tpl',
      1 => 1621501239,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60a63331e903d5_33741300 (Smarty_Internal_Template $_smarty_tpl) {
if (count($_smarty_tpl->tpl_vars['pickups']->value) == 0) {?>
    <div align="center">
        <font color="#ff4500">
            <strong><?php echo __("Not assigned");?>
</strong>
        </font>
    </div>
<?php } else { ?>
    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <th width="7%">#</th>
            <th width="15%"><?php echo __('Time');?>
</th>
            <th width="35%"><?php echo __('Assigned teachers');?>
</th>
            <th width="20%"><?php echo __('Class');?>
</th>
            <th width="10%"><?php echo __('Number of student');?>
</th>
            <th width="13%"><?php echo __("Actions");?>
</th>
        </tr>
        </thead>
        <tbody>
        <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['pickups']->value, 'pickup');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['pickup']->value) {
?>
            <tr>
                <td align="center" style="vertical-align:middle"><strong><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</strong></td>
                <td align="center" style="vertical-align:middle">
                    <strong><?php echo $_smarty_tpl->tpl_vars['pickup']->value['pickup_time'];?>
</strong><br>
                    <?php echo __($_smarty_tpl->tpl_vars['pickup']->value['pickup_day']);?>

                </td>
                <td align="center" style="vertical-align:middle">
                    <?php if (count($_smarty_tpl->tpl_vars['pickup']->value['pickup_assign']) == 0) {?>
                        <strong>&#150;</strong>
                    <?php } else { ?>
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['pickup']->value['pickup_assign'], 'assign');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['assign']->value) {
?>
                            <label class="col-sm-2"></label>
                            <strong>
                                <label class="col-sm-9 text-left" <?php if ($_smarty_tpl->tpl_vars['assign']->value['user_id'] == $_smarty_tpl->tpl_vars['user']->value->_data['user_id']) {?> style="color: royalblue" <?php }?>>&#9679;&#09;<?php echo $_smarty_tpl->tpl_vars['assign']->value['user_fullname'];?>

                            </strong>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                    <?php }?>

                </td>
                <td align="center" style="vertical-align:middle">
                    <?php ob_start();
echo $_smarty_tpl->tpl_vars['pickup']->value['class_name'];
$_prefixVariable2=ob_get_clean();
if (!is_empty($_prefixVariable2)) {?>
                        <strong><?php echo $_smarty_tpl->tpl_vars['pickup']->value['class_name'];?>
</strong>
                    <?php } else { ?>
                        <strong>&#150;</strong>
                    <?php }?>
                </td>
                <td align="center" style="vertical-align:middle">
                    <?php if ($_smarty_tpl->tpl_vars['pickup']->value['total_child'] > 0) {?>
                        <font color="blue"><strong><?php echo $_smarty_tpl->tpl_vars['pickup']->value['total_child'];?>
</strong></font>
                    <?php } else { ?>
                        <font color="#ff4500"><strong><?php echo $_smarty_tpl->tpl_vars['pickup']->value['total_child'];?>
</strong></font>
                    <?php }?>

                </td>

                <td align="center" nowrap="true" style="vertical-align:middle">
                    <?php if ($_smarty_tpl->tpl_vars['pickup']->value['action'] == 0) {?>
                        <i class="fa fa-times" style="color:black" title="<?php echo __("No information");?>
" aria-hidden="true"></i>
                    <?php } elseif ($_smarty_tpl->tpl_vars['pickup']->value['action'] == 1) {?>
                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/pickupteacher/detail/<?php echo $_smarty_tpl->tpl_vars['pickup']->value['pickup_id'];?>
" class="btn btn-xs btn-default">
                            <?php echo __("Edit");?>

                        </a>
                    <?php } elseif ($_smarty_tpl->tpl_vars['pickup']->value['action'] == 2) {?>
                        <a class="btn btn-xs btn-default js_school-pickup_edit"
                           data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-time="<?php echo $_smarty_tpl->tpl_vars['pickup']->value['pickup_time'];?>
">
                            <?php echo __("Add child");?>

                        </a>
                    <?php }?>
                </td>
            </tr>
            <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

        </tbody>
    </table>
<?php }?>

<?php }
}
