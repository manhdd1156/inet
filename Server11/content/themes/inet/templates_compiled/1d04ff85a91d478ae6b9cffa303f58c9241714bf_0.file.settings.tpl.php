<?php
/* Smarty version 3.1.31, created on 2021-05-25 11:22:51
  from "D:\workplace\Inet-project\source\Server11\content\themes\inet\templates\settings.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_60ac7b9b3674b2_96552575',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '1d04ff85a91d478ae6b9cffa303f58c9241714bf' => 
    array (
      0 => 'D:\\workplace\\Inet-project\\source\\Server11\\content\\themes\\inet\\templates\\settings.tpl',
      1 => 1621838814,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:_head.tpl' => 1,
    'file:_header.tpl' => 1,
    'file:__feeds_user.tpl' => 1,
    'file:_footer.tpl' => 1,
  ),
),false)) {
function content_60ac7b9b3674b2_96552575 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:_head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php $_smarty_tpl->_subTemplateRender('file:_header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<!-- page content -->
<div class="container mt20 height_min"> <!-- class height_min để set height tối thiểu của container -->
    <div class="row">

        <!-- left panel -->
        <div class="col-sm-3 offcanvas-sidebar">
            <div class="panel panel-default">
                <div class="panel-body with-nav">
                    <ul class="side-nav">
                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == '') {?>class="active"<?php }?>>
                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/settings"><i class="fa fa-cog fa-fw fa-lg pr10"></i> <?php echo __("Account Settings");?>
</a>
                        </li>
                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "profile") {?>class="active"<?php }?>>
                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/settings/profile"><i class="fa fa-user fa-fw fa-lg pr10"></i> <?php echo __("Edit Profile");?>
</a>
                        </li>
                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "privacy") {?>class="active"<?php }?>>
                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/settings/privacy"><i class="fa fa-lock fa-fw fa-lg pr10"></i> <?php echo __("Privacy Settings");?>
</a>
                        </li>
                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "security") {?>class="active"<?php }?>>
                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/settings/security"><i class="fa fa-shield-alt fa-fw fa-lg pr10"></i> <?php echo __("Security Settings");?>
</a>
                        </li>
                        <?php if ($_smarty_tpl->tpl_vars['system']->value['email_notifications']) {?>
                            <?php if ($_smarty_tpl->tpl_vars['system']->value['email_post_likes'] || $_smarty_tpl->tpl_vars['system']->value['email_post_comments'] || $_smarty_tpl->tpl_vars['system']->value['email_post_shares'] || $_smarty_tpl->tpl_vars['system']->value['email_wall_posts'] || $_smarty_tpl->tpl_vars['system']->value['email_mentions'] || $_smarty_tpl->tpl_vars['system']->value['email_profile_visits'] || $_smarty_tpl->tpl_vars['system']->value['email_friend_requests']) {?>
                                <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "notifications") {?>class="active"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/settings/notifications"><i class="fa fa-envelope-open fa-fw fa-lg pr10"></i> <?php echo __("Email Notifications");?>
</a>
                                </li>
                            <?php }?>
                        <?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['system']->value['social_login_enabled']) {?>
                            <?php if ($_smarty_tpl->tpl_vars['system']->value['facebook_login_enabled'] || $_smarty_tpl->tpl_vars['system']->value['twitter_login_enabled'] || $_smarty_tpl->tpl_vars['system']->value['google_login_enabled'] || $_smarty_tpl->tpl_vars['system']->value['instagram_login_enabled'] || $_smarty_tpl->tpl_vars['system']->value['linkedin_login_enabled'] || $_smarty_tpl->tpl_vars['system']->value['vkontakte_login_enabled']) {?>
                                <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "linked") {?>class="active"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/settings/linked"><i class="fa fa-share-alt fa-fw fa-lg pr10"></i> <?php echo __("Linked Accounts");?>
</a>
                                </li>
                            <?php }?>
                        <?php }?>
                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "blocking") {?>class="active"<?php }?>>
                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/settings/blocking"><i class="fa fa-minus-circle fa-fw fa-lg pr10"></i> <?php echo __("Blocking");?>
</a>
                        </li>
                        <?php if ($_smarty_tpl->tpl_vars['system']->value['packages_enabled']) {?>
                            <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "membership") {?>class="active"<?php }?>>
                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/settings/membership"><i class="fa fa-id-card fa-fw fa-lg pr10"></i> <?php echo __("Membership");?>
</a>
                            </li>
                        <?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['system']->value['affiliates_enabled']) {?>
                            <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "affiliates") {?>class="active"<?php }?>>
                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/settings/affiliates"><i class="fa fa-exchange-alt fa-fw fa-lg pr10"></i> <?php echo __("Affiliates");?>
</a>
                            </li>
                        <?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['system']->value['verification_requests']) {?>
                            <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "verification") {?>class="active"<?php }?>>
                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/settings/verification"><i class="fa fa-check-circle fa-fw fa-lg pr10"></i> <?php echo __("Verification");?>
</a>
                            </li>
                        <?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['system']->value['delete_accounts_enabled']) {?>
                            <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "delete") {?>class="active"<?php }?>>
                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/settings/delete"><i class="fa fa-trash fa-fw fa-lg pr10"></i> <?php echo __("Delete Account");?>
</a>
                            </li>
                        <?php }?>
                    </ul>
                </div>
            </div>
        </div>
        <!-- left panel -->

        <!-- right panel -->
        <div class="col-sm-9 offcanvas-mainbar">
            <div class="panel panel-default">

                <?php if ($_smarty_tpl->tpl_vars['view']->value == '') {?>
                    <div class="panel-heading with-icon with-nav">
                        <!-- panel title -->
                        <div class="mb20">
                            <i class="fa fa-cog pr5 panel-icon"></i>
                            <strong><?php echo __("Account Settings");?>
</strong>
                        </div>
                        <!-- panel title -->

                        <!-- panel nav -->
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#username" data-toggle="tab">
                                    <i class="fa fa-cog fa-fw mr5"></i><strong class="pr5"><?php echo __("Username");?>
</strong>
                                </a>
                            </li>
                            <li>
                                <a href="#email" data-toggle="tab">
                                    <i class="fa fa-envelope fa-fw mr5"></i><strong class="pr5"><?php echo __("Email");?>
</strong>
                                </a>
                            </li>
                            
                                <li>
                                    <a href="#phone" data-toggle="tab">
                                        <i class="fa fa-mobile fa-fw mr5"></i><strong class="pr5"><?php echo __("Phone");?>
</strong>
                                    </a>
                                </li>
                            
                            <li>
                                <a href="#password" data-toggle="tab">
                                    <i class="fa fa-key fa-fw mr5"></i><strong class="pr5"><?php echo __("Password");?>
</strong>
                                </a>
                            </li>
                        </ul>
                        <!-- panel nav -->
                    </div>
                    <div class="panel-body tab-content">
                        <!-- username -->
                        <div class="tab-pane active" id="username">
                            <form class="js_ajax-forms form-horizontal" data-url="users/settings.php?edit=username">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        <?php echo __("Username");?>

                                    </label>
                                    <div class="col-sm-9">
                                        <div class="input-group">
                                            <span class="input-group-addon"><?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/</span>
                                            <input type="text" class="form-control" name="username" value="<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_name'];?>
">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-9 col-sm-offset-3">
                                        <button type="submit" class="btn btn-primary"><?php echo __("Save Changes");?>
</button>
                                    </div>
                                </div>

                                <!-- success -->
                                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                                <!-- success -->

                                <!-- error -->
                                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                                <!-- error -->
                            </form>
                        </div>
                        <!-- username -->

                        <!-- email -->
                        <div class="tab-pane" id="email">
                            <form class="js_ajax-forms form-horizontal" data-url="users/settings.php?edit=email">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        <?php echo __("Email Address");?>

                                    </label>
                                    <div class="col-sm-9">
                                        <input type="email" id="mail" class="form-control" name="email" value="<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_email'];?>
">
                                    </div>
                                </div>


                                <div class="form-group <?php if (!$_smarty_tpl->tpl_vars['user']->value->_data['user_activated'] || is_empty($_smarty_tpl->tpl_vars['user']->value->_data['user_email_activation'])) {?>x-hidden<?php }?>" id="info-email-activation">
                                    <div class="col-sm-9 col-sm-offset-3">
                                        <span class="help-block" style="color: royalblue">
                                            <?php echo __("Email");?>
 <strong> <?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_email_activation'];?>
 </strong> <?php echo __("chưa được xác thực, vui lòng click vào link được gửi về email để hoàn tất quá trình thay đổi");?>

                                        </span>
                                    </div>

                                    <div class="col-sm-9 col-sm-offset-3">
                                        <span class="text-link col-sm-7 pl0" onclick="finishEmailVerify();">
                                            <?php echo __("Đã xác nhận, hoàn tất");?>

                                        </span>

                                        <span class="text-link" data-toggle="modal" data-url="core/activation_email_resend.php">
                                            <?php echo __("Resend Verification Email");?>

                                        </span>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <div class="col-sm-9 col-sm-offset-3">
                                        <button type="submit" class="btn btn-primary"><?php echo __("Save Changes");?>
</button>
                                    </div>
                                </div>

                                <!-- success -->
                                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                                <!-- success -->

                                <!-- error -->
                                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                                <!-- error -->
                            </form>
                        </div>
                        <!-- email -->

                        <!-- phone -->
                        
                            <div class="tab-pane" id="phone">
                                <form class="js_ajax-forms form-horizontal" data-url="users/settings.php?edit=phone">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">
                                            <?php echo __("Mobile number");?>

                                        </label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="phone_number" name="phone" value="<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_phone_signin'];?>
" placeholder='<?php echo __("Mobile number (eg. 098...)");?>
'>
                                            <span class="help-block">
                                                <?php echo __("Hệ thống sẽ gửi tin nhắn SMS để xác minh số của bạn");?>

                                            </span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-9 col-sm-offset-3">
                                            <a onclick="phoneVerification();" class="btn btn-primary"><?php echo __("Xác nhận và lưu thay đổi");?>
</a>
                                        </div>
                                    </div>

                                    <!-- success -->
                                    <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                                    <!-- success -->

                                    <!-- error -->
                                    <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                                    <!-- error -->
                                </form>
                            </div>
                        
                        <!-- phone -->

                        <!-- password -->
                        <div class="tab-pane" id="password">
                            <form class="js_ajax-forms form-horizontal" data-url="users/settings.php?edit=password">

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        <?php echo __("Current");?>

                                    </label>
                                    <div class="col-sm-9">
                                        <input type="password" class="form-control" name="current">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        <?php echo __("New");?>

                                    </label>
                                    <div class="col-sm-9">
                                        <input type="password" class="form-control" name="new">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        <?php echo __("Re-type New");?>

                                    </label>
                                    <div class="col-sm-9">
                                        <input type="password" class="form-control" name="confirm">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-9 col-sm-offset-3">
                                        <button type="submit" class="btn btn-primary"><?php echo __("Save Changes");?>
</button>
                                    </div>
                                </div>

                                <!-- success -->
                                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                                <!-- success -->

                                <!-- error -->
                                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                                <!-- error -->
                            </form>
                        </div>
                        <!-- password -->
                    </div>
                <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "profile") {?>
                    <div class="panel-heading with-icon with-nav">
                        <!-- panel title -->
                        <div class="mb20">
                            <i class="fa fa-user pr5 panel-icon"></i>
                            <strong><?php echo __("Edit Profile");?>
</strong>
                        </div>
                        <!-- panel title -->

                        <!-- panel nav -->
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#basic" data-toggle="tab">
                                    <i class="fa fa-user fa-fw mr5"></i><strong class="pr5"><?php echo __("Basic");?>
</strong>
                                </a>
                            </li>
                            <li>
                                <a href="#work" data-toggle="tab">
                                    <i class="fa fa-briefcase fa-fw mr5"></i><strong class="pr5"><?php echo __("Work");?>
</strong>
                                </a>
                            </li>
                            <li>
                                <a href="#location" data-toggle="tab">
                                    <i class="fa fa-map-marker fa-fw mr5"></i><strong class="pr5"><?php echo __("Location");?>
</strong>
                                </a>
                            </li>
                            <li>
                                <a href="#education" data-toggle="tab">
                                    <i class="fa fa-graduation-cap fa-fw mr5"></i><strong class="pr5"><?php echo __("Education");?>
</strong>
                                </a>
                            </li>
                        </ul>
                        <!-- panel nav -->
                    </div>
                    <div class="panel-body tab-content">
                        <!-- basic tab -->
                        <div class="tab-pane active" id="basic">
                            <form class="js_ajax-forms form-horizontal" data-url="users/settings.php?edit=basic">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        <?php echo __("Last Name");?>

                                    </label>
                                    <div class="col-sm-9">
                                        <input class="form-control" name="lastname" value="<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_lastname'];?>
">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        <?php echo __("First Name");?>

                                    </label>
                                    <div class="col-sm-9">
                                        <input class="form-control" name="firstname" value="<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_firstname'];?>
">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        <?php echo __("I am");?>

                                    </label>
                                    <div class="col-sm-9">
                                        <select name="gender" class="form-control">
                                            <option value="none"><?php echo __("Select Sex");?>
</option>
                                            <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_gender'] == "male") {?>selected<?php }?> value="male"><?php echo __("Male");?>
</option>
                                            <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_gender'] == "female") {?>selected<?php }?> value="female"><?php echo __("Female");?>
</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        <?php echo __("Birthdate");?>

                                    </label>
                                    <div class="col-sm-9">
                                        <div class="row">
                                            <div class="col-xs-4">
                                                <select class="form-control" name="birth_month">
                                                    <option value="none"><?php echo __("Select Month");?>
</option>
                                                    <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_birthdate_parsed']['month'] == '1') {?>selected<?php }?> value="1"><?php echo __("Jan");?>
</option>
                                                    <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_birthdate_parsed']['month'] == '2') {?>selected<?php }?> value="2"><?php echo __("Feb");?>
</option>
                                                    <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_birthdate_parsed']['month'] == '3') {?>selected<?php }?> value="3"><?php echo __("Mar");?>
</option>
                                                    <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_birthdate_parsed']['month'] == '4') {?>selected<?php }?> value="4"><?php echo __("Apr");?>
</option>
                                                    <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_birthdate_parsed']['month'] == '5') {?>selected<?php }?> value="5"><?php echo __("May");?>
</option>
                                                    <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_birthdate_parsed']['month'] == '6') {?>selected<?php }?> value="6"><?php echo __("Jun");?>
</option>
                                                    <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_birthdate_parsed']['month'] == '7') {?>selected<?php }?> value="7"><?php echo __("Jul");?>
</option>
                                                    <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_birthdate_parsed']['month'] == '8') {?>selected<?php }?> value="8"><?php echo __("Aug");?>
</option>
                                                    <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_birthdate_parsed']['month'] == '9') {?>selected<?php }?> value="9"><?php echo __("Sep");?>
</option>
                                                    <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_birthdate_parsed']['month'] == '10') {?>selected<?php }?> value="10"><?php echo __("Oct");?>
</option>
                                                    <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_birthdate_parsed']['month'] == '11') {?>selected<?php }?> value="11"><?php echo __("Nov");?>
</option>
                                                    <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_birthdate_parsed']['month'] == '12') {?>selected<?php }?> value="12"><?php echo __("Dec");?>
</option>
                                                </select>
                                            </div>
                                            <div class="col-xs-4">
                                                <select class="form-control" name="birth_day">
                                                    <option value="none"><?php echo __("Select Day");?>
</option>
                                                    <?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable(null, $_smarty_tpl->isRenderingCache);$_smarty_tpl->tpl_vars['i']->step = 1;$_smarty_tpl->tpl_vars['i']->total = (int) ceil(($_smarty_tpl->tpl_vars['i']->step > 0 ? 31+1 - (1) : 1-(31)+1)/abs($_smarty_tpl->tpl_vars['i']->step));
if ($_smarty_tpl->tpl_vars['i']->total > 0) {
for ($_smarty_tpl->tpl_vars['i']->value = 1, $_smarty_tpl->tpl_vars['i']->iteration = 1;$_smarty_tpl->tpl_vars['i']->iteration <= $_smarty_tpl->tpl_vars['i']->total;$_smarty_tpl->tpl_vars['i']->value += $_smarty_tpl->tpl_vars['i']->step, $_smarty_tpl->tpl_vars['i']->iteration++) {
$_smarty_tpl->tpl_vars['i']->first = $_smarty_tpl->tpl_vars['i']->iteration == 1;$_smarty_tpl->tpl_vars['i']->last = $_smarty_tpl->tpl_vars['i']->iteration == $_smarty_tpl->tpl_vars['i']->total;?>
                                                        <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_birthdate_parsed']['day'] == $_smarty_tpl->tpl_vars['i']->value) {?>selected<?php }?> value="<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['i']->value;?>
</option>
                                                    <?php }
}
?>

                                                </select>
                                            </div>
                                            <div class="col-xs-4">
                                                <select class="form-control" name="birth_year">
                                                    <option value="none"><?php echo __("Select Year");?>
</option>
                                                    <?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable(null, $_smarty_tpl->isRenderingCache);$_smarty_tpl->tpl_vars['i']->step = 1;$_smarty_tpl->tpl_vars['i']->total = (int) ceil(($_smarty_tpl->tpl_vars['i']->step > 0 ? 2015+1 - (1905) : 1905-(2015)+1)/abs($_smarty_tpl->tpl_vars['i']->step));
if ($_smarty_tpl->tpl_vars['i']->total > 0) {
for ($_smarty_tpl->tpl_vars['i']->value = 1905, $_smarty_tpl->tpl_vars['i']->iteration = 1;$_smarty_tpl->tpl_vars['i']->iteration <= $_smarty_tpl->tpl_vars['i']->total;$_smarty_tpl->tpl_vars['i']->value += $_smarty_tpl->tpl_vars['i']->step, $_smarty_tpl->tpl_vars['i']->iteration++) {
$_smarty_tpl->tpl_vars['i']->first = $_smarty_tpl->tpl_vars['i']->iteration == 1;$_smarty_tpl->tpl_vars['i']->last = $_smarty_tpl->tpl_vars['i']->iteration == $_smarty_tpl->tpl_vars['i']->total;?>
                                                        <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_birthdate_parsed']['year'] == $_smarty_tpl->tpl_vars['i']->value) {?>selected<?php }?> value="<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['i']->value;?>
</option>
                                                    <?php }
}
?>

                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- ConIu - Begin - Thêm trường phone, city_id -->
                               
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"><?php echo __("City");?>
</label>
                                    <div class="col-sm-9">
                                        <select name="city_id" class="form-control">
                                            <option value="0"><?php echo __("Select city");?>
</option>
                                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['cities']->value, 'city');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['city']->value) {
?>
                                                <option value="<?php echo $_smarty_tpl->tpl_vars['city']->value['city_id'];?>
" <?php if ($_smarty_tpl->tpl_vars['city']->value['city_id'] == $_smarty_tpl->tpl_vars['user']->value->_data['city_id']) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['city']->value['city_name'];?>
</option>
                                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                        </select>
                                    </div>
                                </div>
                                <!-- ConIu - End - Thêm trường phone, city_id -->

                                <div class="form-group">
                                    <div class="col-sm-9 col-sm-offset-3">
                                        <button type="submit" class="btn btn-primary"><?php echo __("Save Changes");?>
</button>
                                    </div>
                                </div>

                                <!-- success -->
                                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                                <!-- success -->

                                <!-- error -->
                                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                                <!-- error -->
                            </form>
                        </div>
                        <!-- basic tab -->

                        <!-- work tab -->
                        <div class="tab-pane" id="work">
                            <form class="js_ajax-forms form-horizontal" data-url="users/settings.php?edit=work">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        <?php echo __("Work Title");?>

                                    </label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="work_title" value="<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_work_title'];?>
">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        <?php echo __("Work Place");?>

                                    </label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="work_place" value="<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_work_place'];?>
">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        <?php echo __("Work Website");?>

                                    </label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="work_url" value="<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_work_url'];?>
">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-9 col-sm-offset-3">
                                        <button type="submit" class="btn btn-primary"><?php echo __("Save Changes");?>
</button>
                                    </div>
                                </div>

                                <!-- success -->
                                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                                <!-- success -->

                                <!-- error -->
                                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                                <!-- error -->
                            </form>
                        </div>
                        <!-- work tab -->

                        <!-- location tab -->
                        <div class="tab-pane" id="location">
                            <form class="js_ajax-forms form-horizontal" data-url="users/settings.php?edit=location">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        <?php echo __("Current City");?>

                                    </label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control js_geocomplete" name="city" value="<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_current_city'];?>
">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        <?php echo __("Hometown");?>

                                    </label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control js_geocomplete" name="hometown" value="<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_hometown'];?>
">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-9 col-sm-offset-3">
                                        <button type="submit" class="btn btn-primary"><?php echo __("Save Changes");?>
</button>
                                    </div>
                                </div>

                                <!-- success -->
                                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                                <!-- success -->

                                <!-- error -->
                                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                                <!-- error -->
                            </form>
                        </div>
                        <!-- location tab -->

                        <!-- education tab -->
                        <div class="tab-pane" id="education">
                            <form class="js_ajax-forms form-horizontal" data-url="users/settings.php?edit=education">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        <?php echo __("School");?>

                                    </label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="edu_school" value="<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_edu_school'];?>
">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        <?php echo __("Major");?>

                                    </label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="edu_major" value="<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_edu_major'];?>
">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        <?php echo __("Class");?>

                                    </label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="edu_class" value="<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_edu_class'];?>
">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-9 col-sm-offset-3">
                                        <button type="submit" class="btn btn-primary"><?php echo __("Save Changes");?>
</button>
                                    </div>
                                </div>

                                <!-- success -->
                                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                                <!-- success -->

                                <!-- error -->
                                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                                <!-- error -->
                            </form>
                        </div>
                        <!-- education tab -->

                    </div>
                <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "privacy") {?>
                    <div class="panel-heading with-icon">
                        <!-- panel title -->
                        <i class="fa fa-lock pr5 panel-icon"></i>
                        <strong><?php echo __("Privacy Settings");?>
</strong>
                        <!-- panel title -->
                    </div>
                    <div class="panel-body">
                        <form class="js_ajax-forms form-horizontal" data-url="users/settings.php?edit=privacy">
                            <?php if ($_smarty_tpl->tpl_vars['system']->value['chat_enabled']) {?>
                                <div class="form-group">
                                    <label class="col-sm-5 control-label" for="privacy_chat">
                                        <?php echo __("Chat");?>

                                    </label>
                                    <div class="col-sm-3">
                                        <select class="form-control" name="privacy_chat" id="privacy_chat">
                                            <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_chat_enabled'] == 0) {?>selected<?php }?> value="0">
                                                <?php echo __("Offline");?>

                                            </option>
                                            <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_chat_enabled'] == 1) {?>selected<?php }?> value="1">
                                                <?php echo __("Online");?>

                                            </option>
                                        </select>
                                    </div>
                                </div>
                            <?php }?>
                            <div class="form-group">
                                <label class="col-sm-5 control-label" for="privacy_wall">
                                    <?php echo __("Who can post on your wall");?>

                                </label>
                                <div class="col-sm-3">
                                    <select class="form-control" name="privacy_wall" id="privacy_wall">
                                        <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_privacy_wall'] == "public") {?>selected<?php }?> value="public">
                                            <?php echo __("Everyone");?>

                                        </option>
                                        <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_privacy_wall'] == "friends") {?>selected<?php }?> value="friends">
                                            <?php echo __("Friends");?>

                                        </option>
                                        <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_privacy_wall'] == "me") {?>selected<?php }?> value="me">
                                            <?php echo __("Just Me");?>

                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-5 control-label" for="privacy_birthdate">
                                    <?php echo __("Who can see your");?>
 <?php echo __("birthdate");?>

                                </label>
                                <div class="col-sm-3">
                                    <select class="form-control" name="privacy_birthdate" id="privacy_birthdate">
                                        <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_privacy_birthdate'] == "public") {?>selected<?php }?> value="public">
                                            <?php echo __("Everyone");?>

                                        </option>
                                        <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_privacy_birthdate'] == "friends") {?>selected<?php }?> value="friends">
                                            <?php echo __("Friends");?>

                                        </option>
                                        <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_privacy_birthdate'] == "me") {?>selected<?php }?> value="me">
                                            <?php echo __("Just Me");?>

                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-5 control-label" for="privacy_relationship">
                                    <?php echo __("Who can see your");?>
 <?php echo __("relationship");?>

                                </label>
                                <div class="col-sm-3">
                                    <select class="form-control" name="privacy_relationship" id="privacy_relationship">
                                        <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_privacy_relationship'] == "public") {?>selected<?php }?> value="public">
                                            <?php echo __("Everyone");?>

                                        </option>
                                        <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_privacy_relationship'] == "friends") {?>selected<?php }?> value="friends">
                                            <?php echo __("Friends");?>

                                        </option>
                                        <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_privacy_relationship'] == "me") {?>selected<?php }?> value="me">
                                            <?php echo __("Just Me");?>

                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-5 control-label" for="privacy_basic">
                                    <?php echo __("Who can see your");?>
 <?php echo __("basic info");?>

                                </label>
                                <div class="col-sm-3">
                                    <select class="form-control" name="privacy_basic" id="privacy_basic">
                                        <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_privacy_basic'] == "public") {?>selected<?php }?> value="public">
                                            <?php echo __("Everyone");?>

                                        </option>
                                        <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_privacy_basic'] == "friends") {?>selected<?php }?> value="friends">
                                            <?php echo __("Friends");?>

                                        </option>
                                        <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_privacy_basic'] == "me") {?>selected<?php }?> value="me">
                                            <?php echo __("Just Me");?>

                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-5 control-label" for="privacy_work">
                                    <?php echo __("Who can see your");?>
 <?php echo __("work info");?>

                                </label>
                                <div class="col-sm-3">
                                    <select class="form-control" name="privacy_work" id="privacy_work">
                                        <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_privacy_work'] == "public") {?>selected<?php }?> value="public">
                                            <?php echo __("Everyone");?>

                                        </option>
                                        <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_privacy_work'] == "friends") {?>selected<?php }?> value="friends">
                                            <?php echo __("Friends");?>

                                        </option>
                                        <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_privacy_work'] == "me") {?>selected<?php }?> value="me">
                                            <?php echo __("Just Me");?>

                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-5 control-label" for="privacy_location">
                                    <?php echo __("Who can see your");?>
 <?php echo __("location info");?>

                                </label>
                                <div class="col-sm-3">
                                    <select class="form-control" name="privacy_location" id="privacy_location">
                                        <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_privacy_location'] == "public") {?>selected<?php }?> value="public">
                                            <?php echo __("Everyone");?>

                                        </option>
                                        <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_privacy_location'] == "friends") {?>selected<?php }?> value="friends">
                                            <?php echo __("Friends");?>

                                        </option>
                                        <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_privacy_location'] == "me") {?>selected<?php }?> value="me">
                                            <?php echo __("Just Me");?>

                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-5 control-label" for="privacy_education">
                                    <?php echo __("Who can see your");?>
 <?php echo __("education info");?>

                                </label>
                                <div class="col-sm-3">
                                    <select class="form-control" name="privacy_education" id="privacy_education">
                                        <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_privacy_education'] == "public") {?>selected<?php }?> value="public">
                                            <?php echo __("Everyone");?>

                                        </option>
                                        <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_privacy_education'] == "friends") {?>selected<?php }?> value="friends">
                                            <?php echo __("Friends");?>

                                        </option>
                                        <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_privacy_education'] == "me") {?>selected<?php }?> value="me">
                                            <?php echo __("Just Me");?>

                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-5 control-label" for="privacy_other">
                                    <?php echo __("Who can see your");?>
 <?php echo __("other info");?>

                                </label>
                                <div class="col-sm-3">
                                    <select class="form-control" name="privacy_other" id="privacy_other">
                                        <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_privacy_other'] == "public") {?>selected<?php }?> value="public">
                                            <?php echo __("Everyone");?>

                                        </option>
                                        <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_privacy_other'] == "friends") {?>selected<?php }?> value="friends">
                                            <?php echo __("Friends");?>

                                        </option>
                                        <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_privacy_other'] == "me") {?>selected<?php }?> value="me">
                                            <?php echo __("Just Me");?>

                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-5 control-label" for="privacy_friends">
                                    <?php echo __("Who can see your");?>
 <?php echo __("friends");?>

                                </label>
                                <div class="col-sm-3">
                                    <select class="form-control" name="privacy_friends" id="privacy_friends">
                                        <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_privacy_friends'] == "public") {?>selected<?php }?> value="public">
                                            <?php echo __("Everyone");?>

                                        </option>
                                        <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_privacy_friends'] == "friends") {?>selected<?php }?> value="friends">
                                            <?php echo __("Friends");?>

                                        </option>
                                        <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_privacy_friends'] == "me") {?>selected<?php }?> value="me">
                                            <?php echo __("Just Me");?>

                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-5 control-label" for="privacy_photos">
                                    <?php echo __("Who can see your");?>
 <?php echo __("photos");?>

                                </label>
                                <div class="col-sm-3">
                                    <select class="form-control" name="privacy_photos" id="privacy_photos">
                                        <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_privacy_photos'] == "public") {?>selected<?php }?> value="public">
                                            <?php echo __("Everyone");?>

                                        </option>
                                        <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_privacy_photos'] == "friends") {?>selected<?php }?> value="friends">
                                            <?php echo __("Friends");?>

                                        </option>
                                        <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_privacy_photos'] == "me") {?>selected<?php }?> value="me">
                                            <?php echo __("Just Me");?>

                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-5 control-label" for="privacy_pages">
                                    <?php echo __("Who can see your");?>
 <?php echo __("liked pages");?>

                                </label>
                                <div class="col-sm-3">
                                    <select class="form-control" name="privacy_pages" id="privacy_pages">
                                        <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_privacy_pages'] == "public") {?>selected<?php }?> value="public">
                                            <?php echo __("Everyone");?>

                                        </option>
                                        <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_privacy_pages'] == "friends") {?>selected<?php }?> value="friends">
                                            <?php echo __("Friends");?>

                                        </option>
                                        <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_privacy_pages'] == "me") {?>selected<?php }?> value="me">
                                            <?php echo __("Just Me");?>

                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-5 control-label" for="privacy_groups">
                                    <?php echo __("Who can see your");?>
 <?php echo __("joined groups");?>

                                </label>
                                <div class="col-sm-3">
                                    <select class="form-control" name="privacy_groups" id="privacy_groups">
                                        <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_privacy_groups'] == "public") {?>selected<?php }?> value="public">
                                            <?php echo __("Everyone");?>

                                        </option>
                                        <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_privacy_groups'] == "friends") {?>selected<?php }?> value="friends">
                                            <?php echo __("Friends");?>

                                        </option>
                                        <option <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_privacy_groups'] == "me") {?>selected<?php }?> value="me">
                                            <?php echo __("Just Me");?>

                                        </option>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="col-sm-7 col-sm-offset-5">
                                    <button type="submit" class="btn btn-primary"><?php echo __("Save Changes");?>
</button>
                                </div>
                            </div>

                            <!-- success -->
                            <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                            <!-- success -->

                            <!-- error -->
                            <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                            <!-- error -->
                        </form>
                    </div>
                <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "security") {?>
                    <div class="panel-heading with-icon">
                        <!-- panel title -->
                        <i class="fa fa-shield-alt pr5 panel-icon"></i>
                        <strong><?php echo __("Security Settings");?>
</strong>
                        <!-- panel title -->
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th><?php echo __("ID");?>
</th>
                                    <th><?php echo __("Browser");?>
</th>
                                    <th><?php echo __("OS");?>
</th>
                                    <th><?php echo __("Date");?>
</th>
                                    <th><?php echo __("IP");?>
</th>
                                    <th><?php echo __("Actions");?>
</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['sessions']->value, 'session');
$_smarty_tpl->tpl_vars['session']->iteration = 0;
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['session']->value) {
$_smarty_tpl->tpl_vars['session']->iteration++;
$__foreach_session_1_saved = $_smarty_tpl->tpl_vars['session'];
?>
                                    <tr <?php if ($_smarty_tpl->tpl_vars['session']->value['session_token'] == $_smarty_tpl->tpl_vars['user']->value->_data['active_session']) {?>class="success"<?php }?>>
                                        <td><?php echo $_smarty_tpl->tpl_vars['session']->iteration;?>
</td>
                                        <td><?php echo $_smarty_tpl->tpl_vars['session']->value['user_browser'];?>
 <?php if ($_smarty_tpl->tpl_vars['session']->value['session_token'] == $_smarty_tpl->tpl_vars['user']->value->_data['active_session']) {?><span class="label label-info"><?php echo __("Active Session");?>
</span><?php }?></td>
                                        <td><?php echo $_smarty_tpl->tpl_vars['session']->value['user_os'];?>
</td>
                                        <td>
                                            <span class="js_moment" data-time="<?php echo $_smarty_tpl->tpl_vars['session']->value['session_date'];?>
"><?php echo $_smarty_tpl->tpl_vars['session']->value['session_date'];?>
</span>
                                        </td>
                                        <td><?php echo $_smarty_tpl->tpl_vars['session']->value['user_ip'];?>
</td>
                                        <td>
                                            <button data-toggle="tooltip" data-placement="top" title='<?php echo __("End Session");?>
' class="btn btn-xs btn-danger js_session-deleter" data-id="<?php echo $_smarty_tpl->tpl_vars['session']->value['session_id'];?>
">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </td>
                                    </tr>
                                <?php
$_smarty_tpl->tpl_vars['session'] = $__foreach_session_1_saved;
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "linked") {?>
                    <div class="panel-heading with-icon">
                        <!-- panel title -->
                        <i class="fa fa-share-alt pr5 panel-icon"></i>
                        <strong><?php echo __("Linked Accounts");?>
</strong>
                        <!-- panel title -->
                    </div>
                    <div class="panel-body">
                        <ul>
                            <?php if ($_smarty_tpl->tpl_vars['system']->value['facebook_login_enabled']) {?>
                                <li class="feeds-item">
                                    <div class="data-container">
                                        <div class="data-avatar">
                                            <i class="fa fa-facebook-square" style="color: #3B579D"></i>
                                        </div>
                                        <div class="data-content">
                                            <div class="pull-right flip">
                                                <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['facebook_connected']) {?>
                                                    <a class="btn btn-danger" href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/revoke/facebook"><?php echo __("Disconnect");?>
</a>
                                                <?php } else { ?>
                                                    <a class="btn btn-primary" href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/connect/facebook"><?php echo __("Connect");?>
</a>
                                                <?php }?>
                                            </div>
                                            <div>
                                                <div class="name mt5 text-primary">
                                                    <?php echo __("Facebook");?>

                                                </div>
                                                <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['facebook_connected']) {?>
                                                    <?php echo __("Your account is connected to");?>
 <?php echo __("Facebook");?>

                                                <?php } else { ?>
                                                    <?php echo __("Connect your account to");?>
 <?php echo __("Facebook");?>

                                                <?php }?>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            <?php }?>
                            <?php if ($_smarty_tpl->tpl_vars['system']->value['twitter_login_enabled']) {?>
                                <li class="feeds-item">
                                    <div class="data-container">
                                        <div class="data-avatar">
                                            <i class="fa fa-twitter-square" style="color: #55ACEE"></i>
                                        </div>
                                        <div class="data-content">
                                            <div class="pull-right flip">
                                                <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['twitter_connected']) {?>
                                                    <a class="btn btn-danger" href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/revoke/twitter"><?php echo __("Disconnect");?>
</a>
                                                <?php } else { ?>
                                                    <a class="btn btn-primary" href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/connect/twitter"><?php echo __("Connect");?>
</a>
                                                <?php }?>
                                            </div>
                                            <div>
                                                <div class="name mt5 text-primary">
                                                    <?php echo __("Twitter");?>

                                                </div>
                                                <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['twitter_connected']) {?>
                                                    <?php echo __("Your account is connected to");?>
 <?php echo __("Twitter");?>

                                                <?php } else { ?>
                                                    <?php echo __("Connect your account to");?>
 <?php echo __("Twitter");?>

                                                <?php }?>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            <?php }?>
                            <?php if ($_smarty_tpl->tpl_vars['system']->value['google_login_enabled']) {?>
                                <li class="feeds-item">
                                    <div class="data-container">
                                        <div class="data-avatar">
                                            <i class="fa fa-google-plus-square" style="color: #DC4A38"></i>
                                        </div>
                                        <div class="data-content">
                                            <div class="pull-right flip">
                                                <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['google_connected']) {?>
                                                    <a class="btn btn-danger" href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/revoke/google"><?php echo __("Disconnect");?>
</a>
                                                <?php } else { ?>
                                                    <a class="btn btn-primary" href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/connect/google"><?php echo __("Connect");?>
</a>
                                                <?php }?>
                                            </div>
                                            <div>
                                                <div class="name mt5 text-primary">
                                                    <?php echo __("Google");?>

                                                </div>
                                                <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['google_connected']) {?>
                                                    <?php echo __("Your account is connected to");?>
 <?php echo __("Google");?>

                                                <?php } else { ?>
                                                    <?php echo __("Connect your account to");?>
 <?php echo __("Google");?>

                                                <?php }?>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            <?php }?>
                            <?php if ($_smarty_tpl->tpl_vars['system']->value['instagram_login_enabled']) {?>
                                <li class="feeds-item">
                                    <div class="data-container">
                                        <div class="data-avatar">
                                            <i class="fa fa-instagram" style="color: #3f729b"></i>
                                        </div>
                                        <div class="data-content">
                                            <div class="pull-right flip">
                                                <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['instagram_connected']) {?>
                                                    <a class="btn btn-danger" href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/revoke/instagram"><?php echo __("Disconnect");?>
</a>
                                                <?php } else { ?>
                                                    <a class="btn btn-primary" href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/connect/instagram"><?php echo __("Connect");?>
</a>
                                                <?php }?>
                                            </div>
                                            <div>
                                                <div class="name mt5 text-primary">
                                                    <?php echo __("Instagram");?>

                                                </div>
                                                <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['instagram_connected']) {?>
                                                    <?php echo __("Your account is connected to");?>
 <?php echo __("Instagram");?>

                                                <?php } else { ?>
                                                    <?php echo __("Connect your account to");?>
 <?php echo __("Instagram");?>

                                                <?php }?>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            <?php }?>
                            <?php if ($_smarty_tpl->tpl_vars['system']->value['linkedin_login_enabled']) {?>
                                <li class="feeds-item">
                                    <div class="data-container">
                                        <div class="data-avatar">
                                            <i class="fa fa-linkedin" style="color: #1A84BC"></i>
                                        </div>
                                        <div class="data-content">
                                            <div class="pull-right flip">
                                                <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['linkedin_connected']) {?>
                                                    <a class="btn btn-danger" href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/revoke/linkedin"><?php echo __("Disconnect");?>
</a>
                                                <?php } else { ?>
                                                    <a class="btn btn-primary" href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/connect/linkedin"><?php echo __("Connect");?>
</a>
                                                <?php }?>
                                            </div>
                                            <div>
                                                <div class="name mt5 text-primary">
                                                    <?php echo __("Linkedin");?>

                                                </div>
                                                <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['linkedin_connected']) {?>
                                                    <?php echo __("Your account is connected to");?>
 <?php echo __("Linkedin");?>

                                                <?php } else { ?>
                                                    <?php echo __("Connect your account to");?>
 <?php echo __("Linkedin");?>

                                                <?php }?>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            <?php }?>
                            <?php if ($_smarty_tpl->tpl_vars['system']->value['vkontakte_login_enabled']) {?>
                                <li class="feeds-item">
                                    <div class="data-container">
                                        <div class="data-avatar">
                                            <i class="fa fa-vk" style="color: #527498"></i>
                                        </div>
                                        <div class="data-content">
                                            <div class="pull-right flip">
                                                <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['vkontakte_connected']) {?>
                                                    <a class="btn btn-danger" href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/revoke/vkontakte"><?php echo __("Disconnect");?>
</a>
                                                <?php } else { ?>
                                                    <a class="btn btn-primary" href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/connect/vkontakte"><?php echo __("Connect");?>
</a>
                                                <?php }?>
                                            </div>
                                            <div>
                                                <div class="name mt5 text-primary">
                                                    <?php echo __("Vkontakte");?>

                                                </div>
                                                <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['vkontakte_connected']) {?>
                                                    <?php echo __("Your account is connected to");?>
 <?php echo __("Vkontakte");?>

                                                <?php } else { ?>
                                                    <?php echo __("Connect your account to");?>
 <?php echo __("Vkontakte");?>

                                                <?php }?>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            <?php }?>
                        </ul>
                    </div>
                <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "blocking") {?>
                    <div class="panel-heading with-icon">
                        <!-- panel title -->
                        <i class="fa fa-minus-circle pr5 panel-icon"></i>
                        <strong><?php echo __("Manage Blocking");?>
</strong>
                        <!-- panel title -->
                    </div>
                    <div class="panel-body">
                        <div class="alert alert-info">
                            <i class="fa fa-info-circle fa-lg mr10"></i><?php echo __("Once you block someone, that person can no longer see things you post on your timeline");?>
<br>
                        </div>

                        <?php if (count($_smarty_tpl->tpl_vars['blocks']->value) > 0) {?>
                            <ul>
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['blocks']->value, '_user');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['_user']->value) {
?>
                                    <?php $_smarty_tpl->_subTemplateRender('file:__feeds_user.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('_connection'=>"blocked"), 0, true);
?>

                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                            </ul>
                        <?php } else { ?>
                            <p class="text-center text-muted">
                                <?php echo __("No blocked users");?>

                            </p>
                        <?php }?>

                        <?php if (count($_smarty_tpl->tpl_vars['blocks']->value) >= $_smarty_tpl->tpl_vars['system']->value['max_results']) {?>
                            <!-- see-more -->
                            <div class="alert alert-info see-more js_see-more" data-get="blocks">
                                <span><?php echo __("See More");?>
</span>
                                <div class="loader loader_small x-hidden"></div>
                            </div>
                            <!-- see-more -->
                        <?php }?>
                    </div>
                <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "delete") {?>
                    <div class="panel-heading with-icon">
                        <!-- panel title -->
                        <i class="fa fa-trash pr5 panel-icon"></i>
                        <strong><?php echo __("Delete Account");?>
</strong>
                        <!-- panel title -->
                    </div>
                    <div class="panel-body">
                        <div class="alert alert-warning">
                            <?php echo __("Once you delete your account you will no longer can access it again");?>
<br>
                            <?php echo __("Note: All your data will be deleted");?>

                        </div>

                        <div class="text-center">
                            <button class="btn btn-danger js_delete-user"><?php echo __("Delete My Account");?>
</button>
                        </div>

                        <?php if (count($_smarty_tpl->tpl_vars['blocks']->value) >= $_smarty_tpl->tpl_vars['system']->value['max_results']) {?>
                            <!-- see-more -->
                            <div class="alert alert-info see-more js_see-more" data-get="blocks">
                                <span><?php echo __("See More");?>
</span>
                                <div class="loader loader_small x-hidden"></div>
                            </div>
                            <!-- see-more -->
                        <?php }?>
                    </div>
                <?php }?>

            </div>
        </div>

    </div>
</div>
<!-- page content -->

<?php $_smarty_tpl->_subTemplateRender('file:_footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<!-- HTTPS required. HTTP will give a 403 forbidden response -->
<?php echo '<script'; ?>
 src="https://sdk.accountkit.com/vi_VN/sdk.js"><?php echo '</script'; ?>
>


    <?php echo '<script'; ?>
>
        var timeModal = 1500;
        // initialize Account Kit with CSRF protection
        AccountKit_OnInteractive = function(){
            AccountKit.init(
                {
                    appId:1638502962897148,
                    state:"coniuahihi",
                    version:"v1.1",
                    fbAppEventsEnabled:true,
                    redirect:"https://inet.vn"
                }
            );
        };

        var api = [];
        api['core/verify']  = ajax_path+"core/verify_phone.php";
        api['users/check_email_verified']  = ajax_path+"users/check_email_verified.php";

        // login callback
        function loginCallback(response) {
            if (response.status === "PARTIALLY_AUTHENTICATED") {

                $.post(api['core/verify'], {
                    'do': 'edit_phone',
                    'code': response.code,
                    'csrf': response.state
                }, function (response) {

                    if (response.callback) {
                        eval(response.callback);
                    } else if (response.error) {
                        modal('#modal-error', {title: __['Error'], message: response.message});
                    } else if (response.success) {
                        modal('#modal-success', {title: __['Success'], message: response.message});
                        modal_hidden(timeModal);
                    } else {
                        window.location.reload();
                    }

                }, 'json');
            }
            else if (response.status === "NOT_AUTHENTICATED") {
                // handle authentication failure
                console.log("Authentication failure");
            }
            else if (response.status === "BAD_PARAMS") {
                // handle bad parameters
                console.log("Bad parameters");
            }
        }

        // phone form submission handler
        function phoneVerification() {
            //var countryCode = document.getElementById("country_code").value;
            var phoneNumber = document.getElementById("phone_number").value;
            AccountKit.login(
                'PHONE',
                {countryCode: '+84', phoneNumber: phoneNumber}, // will use default values if not specified
                loginCallback
            );
        }

        // phone form submission handler
        function finishEmailVerify() {
            $("#loading_full_screen").removeClass('hidden');
            $.post(api['users/check_email_verified'], {}, function (response) {

                $("#loading_full_screen").addClass('hidden');
                console.log(response.is_verified);
                if (response.is_verified) {
                    $('#info-email-activation').addClass('x-hidden');
                    $('#mail').val(response.email);
                } else if (response.error){
                    modal('#modal-error', {title: __['Error'], message: response.message});
                }

            }, 'json');

        }

    <?php echo '</script'; ?>
>
<?php }
}
