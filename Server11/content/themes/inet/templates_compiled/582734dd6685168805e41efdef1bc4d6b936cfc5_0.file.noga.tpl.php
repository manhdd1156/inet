<?php
/* Smarty version 3.1.31, created on 2021-03-29 11:17:35
  from "D:\Working\Coniu\ServerSvn\trunk\Server11\content\themes\inet\templates\noga.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_606154df0e30c3_27706954',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '582734dd6685168805e41efdef1bc4d6b936cfc5' => 
    array (
      0 => 'D:\\Working\\Coniu\\ServerSvn\\trunk\\Server11\\content\\themes\\inet\\templates\\noga.tpl',
      1 => 1575865658,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:_head.tpl' => 1,
    'file:_header.tpl' => 1,
    'file:ci/noga/noga.dashboard.tpl' => 1,
    'file:ci/noga/noga.".((string)$_smarty_tpl->tpl_vars[\'view\']->value).".tpl' => 1,
    'file:_footer.tpl' => 1,
  ),
),false)) {
function content_606154df0e30c3_27706954 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:_head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php $_smarty_tpl->_subTemplateRender('file:_header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<!-- page content -->
<div class="container mt20">
    <div class="row">

        <div class="col-md-3 col-sm-3">
            <div class="panel panel-default">
                <div class="panel-body with-nav">
                    <ul class="side-nav metismenu js_metisMenu">
                        <!-- Dashboard -->
                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == '') {?>class="active selected"<?php }?>>
                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/noga/">
                                <i class="fas fa-tachometer-alt fa-fw fa-lg pr10"></i> <?php echo __("Dashboard");?>

                            </a>
                        </li>
                        <!-- Dashboard -->
                        <!-- School -->
                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "attendance") {?>class="active selected"<?php }?>>
                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/noga/schools">
                                <i class="fa fa-university fa-fw fa-lg pr10"></i> <?php echo __("School");?>

                                <span class="fa arrow"></span>
                            </a>
                            <ul>
                                <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "attendance" && $_smarty_tpl->tpl_vars['sub_view']->value == '') {?>class="active selected"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/noga/schools">
                                        <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Lists");?>

                                    </a>
                                </li>
                                <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "attendance" && $_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>class="active selected"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/noga/schools/add">
                                        <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Add New");?>

                                    </a>
                                </li>
                                <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "attendance" && $_smarty_tpl->tpl_vars['sub_view']->value == "add_system_school") {?>class="active selected"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/noga/schools/addsystemschool">
                                        <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Add new system school");?>

                                    </a>
                                </li>
                                <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "attendance" && $_smarty_tpl->tpl_vars['sub_view']->value == "listsystemschool") {?>class="active selected"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/noga/schools/listsystemschool">
                                        <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("System school list");?>

                                    </a>
                                </li>
                            </ul>
                        </li>

                        <!-- user -->
                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "users") {?>class="active"<?php }?>>
                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/noga/users">
                                <i class="fa fa-user fa-fw fa-lg pr10"></i><?php echo __("Users");?>

                                <span class="fa arrow"></span>
                            </a>
                            <ul>
                                <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "users" && $_smarty_tpl->tpl_vars['sub_view']->value == '') {?>class="active selected"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/noga/users">
                                        <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Show All Users");?>

                                    </a>
                                </li>
                                <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "users" && $_smarty_tpl->tpl_vars['sub_view']->value == "userregion") {?>class="active selected"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/noga/users/userregion">
                                        <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Regional management account");?>

                                    </a>
                                </li>
                                <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "users" && $_smarty_tpl->tpl_vars['sub_view']->value == "admins") {?>class="active selected"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/noga/users/admins">
                                        <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("List Admins");?>

                                    </a>
                                </li>
                                <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "users" && $_smarty_tpl->tpl_vars['sub_view']->value == "moderators") {?>class="active selected"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/noga/users/moderators">
                                        <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("List Moderators");?>

                                    </a>
                                </li>
                                <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "users" && $_smarty_tpl->tpl_vars['sub_view']->value == "online") {?>class="active selected"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/noga/users/online">
                                        <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("List Online");?>

                                    </a>
                                </li>
                                <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "users" && $_smarty_tpl->tpl_vars['sub_view']->value == "banned") {?>class="active selected"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/noga/users/banned">
                                        <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("List Banned");?>

                                    </a>
                                </li>
                            </ul>
                        </li>
                        <!-- End user -->

                        <!-- Push notifications -->
                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "notifications") {?>class="active"<?php }?>>
                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/noga/notifications">
                                <i class="fa fa-bell fa-fw fa-lg pr10"></i><?php echo __("Notifications");?>

                                <span class="fa arrow"></span>
                            </a>
                            <ul>
                                <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "notifications" && $_smarty_tpl->tpl_vars['sub_view']->value == "immediately") {?>class="active selected"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/noga/notifications/immediately">
                                        <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Notify immediately");?>

                                    </a>
                                </li>
                                <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "notifications" && $_smarty_tpl->tpl_vars['sub_view']->value == "schedule") {?>class="active selected"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/noga/notifications/schedule">
                                        <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Appointment");?>

                                    </a>
                                </li>
                                <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "users" && $_smarty_tpl->tpl_vars['sub_view']->value == "list") {?>class="active selected"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/noga/notifications/list">
                                        <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Lists");?>

                                    </a>
                                </li>
                            </ul>
                        </li>
                        <!-- Push notifications -->

                        <!-- Thống kê tương tác các trường -->
                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "statistics") {?>class="active"<?php }?>>
                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/noga/statistics/schools">
                                <i class="fas fa-chart-pie fa-fw fa-lg pr10"></i><?php echo __("Statistics");?>

                                <span class="fa arrow"></span>
                            </a>
                            <ul>
                                <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "statistics" && $_smarty_tpl->tpl_vars['sub_view']->value == "usersonline") {?>class="active selected"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/noga/statistics/usersonline">
                                        <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Users online");?>

                                    </a>
                                </li>
                                <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "statistics" && $_smarty_tpl->tpl_vars['sub_view']->value == "schools") {?>class="active selected"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/noga/statistics/schools">
                                        <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("School");?>

                                    </a>
                                </li>
                                
                                <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "statistics" && $_smarty_tpl->tpl_vars['sub_view']->value == "topics") {?>class="active selected"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/noga/statistics/topics">
                                        <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Topics");?>

                                    </a>
                                </li>
                            </ul>
                        </li>
                        <!-- Thống kê tương tác các trường -->


                        <!-- development -->
                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "foetusdevelopments") {?>class="active"<?php }?>>
                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/noga/foetusdevelopments">
                                <i class="fas fa-chart-line fa-fw fa-lg pr10"></i><?php echo __("Foetus development");?>

                                <span class="fa arrow"></span>
                            </a>
                            <ul>
                                <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "foetusdevelopments" && $_smarty_tpl->tpl_vars['sub_view']->value == '') {?>class="active selected"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/noga/foetusdevelopments">
                                        <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Lists");?>

                                    </a>
                                </li>
                                <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "foetusdevelopments" && $_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>class="active selected"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/noga/foetusdevelopments/add">
                                        <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Add New");?>

                                    </a>
                                </li>
                                <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "foetusdevelopments" && $_smarty_tpl->tpl_vars['sub_view']->value == "push") {?>class="active selected"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/noga/foetusdevelopments/push">
                                        <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Push");?>

                                    </a>
                                </li>
                            </ul>
                        </li>

                        <!-- child development -->
                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "childdevelopments") {?>class="active"<?php }?>>
                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/noga/childdevelopments">
                                <i class="fas fa-chart-bar fa-fw fa-lg pr10"></i><?php echo __("Child development");?>

                                <span class="fa arrow"></span>
                            </a>
                            <ul>
                                <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "childdevelopments" && $_smarty_tpl->tpl_vars['sub_view']->value == '') {?>class="active selected"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/noga/childdevelopments">
                                        <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Lists");?>

                                    </a>
                                </li>
                                <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "childdevelopments" && $_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>class="active selected"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/noga/childdevelopments/add">
                                        <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Add New");?>

                                    </a>
                                </li>
                                <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "childdevelopments" && $_smarty_tpl->tpl_vars['sub_view']->value == "push") {?>class="active selected"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/noga/childdevelopments/push">
                                        <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Push");?>

                                    </a>
                                </li>
                            </ul>
                        </li>
                        <!-- End child development -->
                        <!-- child based on month -->
                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "childmonths") {?>class="active"<?php }?>>
                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/noga/childmonths">
                                <i class="fas fa-chart-area fa-fw fa-lg pr10"></i><?php echo __("Month age information");?>

                                <span class="fa arrow"></span>
                            </a>
                            <ul>
                                <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "childmonths" && $_smarty_tpl->tpl_vars['sub_view']->value == '') {?>class="active selected"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/noga/childmonths">
                                        <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Lists");?>

                                    </a>
                                </li>
                                <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "childmonths" && $_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>class="active selected"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/noga/childmonths/add">
                                        <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Add New");?>

                                    </a>
                                </li>
                            </ul>
                        </li>
                        <!-- End child based on month -->
                        <!-- foetus based on week -->
                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "pregnancys") {?>class="active"<?php }?>>
                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/noga/pregnancys">
                                <i class="far fa-chart-bar fa-fw fa-lg pr10"></i><?php echo __("Pregnancy information");?>

                                <span class="fa arrow"></span>
                            </a>
                            <ul>
                                <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "pregnancys" && $_smarty_tpl->tpl_vars['sub_view']->value == '') {?>class="active selected"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/noga/pregnancys">
                                        <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Lists");?>

                                    </a>
                                </li>
                                <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "pregnancys" && $_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>class="active selected"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/noga/pregnancys/add">
                                        <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Add New");?>

                                    </a>
                                </li>
                            </ul>
                        </li>
                        <!-- End foetus based on week -->
                        <!-- Danh sách tài khoản mới -->
                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "usernews") {?>class="active"<?php }?>>
                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/noga/usernews">
                                <i class="fa fa-user fa-fw fa-lg pr10"></i><?php echo __("List new user");?>

                            </a>
                        </li>
                        <!-- End danh sách tài khoản mới -->
                        <!-- Danh quản lý và giáo viên -->
                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "manageandteachers") {?>class="active"<?php }?>>
                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/noga/manageandteachers">
                                <i class="fa fa-user fa-fw fa-lg pr10"></i><?php echo __("Manage and teacher");?>

                            </a>
                        </li>
                        <!-- End danh sách quản lý và giáo viên -->
                        <!-- Danh sách trang -->
                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "pages") {?>class="active"<?php }?>>
                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/noga/pages">
                                <i class="fa fa-flag fa-fw fa-lg pr10"></i><?php echo __("Pages");?>

                            </a>
                        </li>
                        <!-- End Danh sách trang -->
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-9 col-sm-9">
            <?php if ($_smarty_tpl->tpl_vars['view']->value == '') {?>
                <?php $_smarty_tpl->_subTemplateRender('file:ci/noga/noga.dashboard.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

            <?php } else { ?>
                <?php $_smarty_tpl->_subTemplateRender("file:ci/noga/noga.".((string)$_smarty_tpl->tpl_vars['view']->value).".tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

            <?php }?>
        </div>
    </div>
</div>
<!-- page content -->


<?php $_smarty_tpl->_subTemplateRender('file:_footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
