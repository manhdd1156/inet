<?php
/* Smarty version 3.1.31, created on 2021-06-11 17:04:03
  from "D:\workplace\Inet-project\source\Server11\content\themes\inet\templates\ci\class\class.useservices.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_60c33513a78cc2_35766125',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'd2cd792b4fa0702419b5083e934ddf57bda1b335' => 
    array (
      0 => 'D:\\workplace\\Inet-project\\source\\Server11\\content\\themes\\inet\\templates\\ci\\class\\class.useservices.tpl',
      1 => 1623405840,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:ci/school/ajax.service.history.tpl' => 1,
  ),
),false)) {
function content_60c33513a78cc2_35766125 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <div class="pull-right flip">
            <a href="https://blog.coniu.vn/huong-dan-giao-vien-dang-ky-dich-vu-tren-website-inet/" class="btn btn-info btn_guide" target="_blank">
                <i class="fa fa-info"></i> <?php echo __("Guide");?>

            </a>
            <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == "history" && canview_class($_smarty_tpl->tpl_vars['user_id']->value,$_smarty_tpl->tpl_vars['class_id']->value)) {?>
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/useservices/record" class="btn btn-default">
                    <i class="fa fa-plus"></i> <?php echo __("Use service");?>

                </a>
            <?php }?>
        </div>
        <i class="fa fa-life-ring fa-fw fa-lg pr10"></i>
        <?php echo __("Service");?>

        <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == "history") {?>
            &rsaquo; <?php echo __('Service usage information');?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "record") {?>
            &rsaquo; <?php echo __('Register service');?>

        <?php }?>
    </div>
    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == "record") {?>
        <div class="panel-body with-table">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/class/boclass_service.php">
                <input type="hidden" name="username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                <input type="hidden" name="do" value="record"/>

                <div class="form-group pl5">
                    <div class='col-sm-4'>
                        <select name="service_id" id="service_id" class="form-control">
                            <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['services']->value, 'service');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['service']->value) {
?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
 - <?php echo $_smarty_tpl->tpl_vars['service']->value['service_name'];?>
</option>
                                <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </select>
                    </div>
                    <div class="col-sm-3">
                        <div class='input-group date' id='using_time_picker'>
                            <input type='text' name="using_at" id="using_at" class="form-control" placeholder="<?php echo __("Using time");?>
 (*)" required/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                    <div class='col-sm-2 pl10'>
                        <div class="form-group">
                            <a id="search" class="btn btn-default js_service-search-child4record" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"><?php echo __("Search");?>
</a>
                            <label id="loading" class="btn btn-info x-hidden"><?php echo __("Loading");?>
...</label>
                        </div>
                    </div>
                </div>
                <div class="table-responsive" id="child_list" name="child_list"></div>
                <div class="form-group pl5">
                    <div class="col-sm-9">
                        <button type="submit" class="btn btn-primary padrl30" disabled><?php echo __("Save");?>
</button>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "history") {?>
        <div class="panel-body with-table">
            <form class="js_ajax-forms form-horizontal" data-url="#">
                <div class="form-group pl5">
                    <div class='col-sm-4'>
                        <select name="service_id" id="service_id" class="form-control" <?php if ($_smarty_tpl->tpl_vars['service_id']->value > 0) {?>disabled<?php }?>>
                            <option value=""><?php echo __("Select service");?>
...</option>
                            <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['services']->value, 'service');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['service']->value) {
?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
" <?php if ($_smarty_tpl->tpl_vars['service_id']->value == $_smarty_tpl->tpl_vars['service']->value['service_id']) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
 - <?php echo $_smarty_tpl->tpl_vars['service']->value['service_name'];?>
</option>
                                <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </select>
                    </div>
                    <div class='col-sm-5'>
                        <select name="child_id" id="service_child_id" class="form-control" <?php if ($_smarty_tpl->tpl_vars['service_id']->value > 0) {?>disabled<?php }?>>
                            <option value=""><?php echo __("Whole class");?>
</option>
                            <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                            <?php $_smarty_tpl->_assignInScope('child_status', -1);
?>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['children']->value, 'child');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['child']->value) {
?>
                                <?php if (($_smarty_tpl->tpl_vars['child_status']->value >= 0) && ($_smarty_tpl->tpl_vars['child_status']->value != $_smarty_tpl->tpl_vars['child']->value['child_status'])) {?>
                                    <option value="" disabled style="color: blue">----------<?php echo __("Trẻ đã nghỉ học");?>
----------</option>
                                <?php }?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
" <?php if ($_smarty_tpl->tpl_vars['child']->value['child_id'] == $_smarty_tpl->tpl_vars['child_id']->value) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
 - <?php echo $_smarty_tpl->tpl_vars['child']->value['child_name'];?>
 (<?php echo $_smarty_tpl->tpl_vars['child']->value['birthday'];?>
)</option>
                                <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                                <?php $_smarty_tpl->_assignInScope('child_status', $_smarty_tpl->tpl_vars['child']->value['child_status']);
?>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </select>
                    </div>
                </div>
                <div class="form-group pl5">
                    <div class="col-sm-3">
                        <div class='input-group date' id='history_begin_picker'>
                            <input type='text' name="begin" id="begin" value="<?php echo $_smarty_tpl->tpl_vars['begin']->value;?>
" class="form-control" placeholder="<?php echo __("From date");?>
"/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class='input-group date' id='history_end_picker'>
                            <input type='text' name="end" id="end" class="form-control" placeholder="<?php echo __("To date");?>
"/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                    <div class='col-sm-2'>
                        <div class="form-group pl10">
                            <a href="#" id="search" class="btn btn-default js_service-search-history" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"><?php echo __("Search");?>
</a>
                            <label id="loading" class="btn btn-info x-hidden"><?php echo __("Loading");?>
...</label>
                        </div>
                    </div>
                </div>
                <br/>
                <div class="table-responsive" id="usage_history" name="usage_history">
                    <?php if ($_smarty_tpl->tpl_vars['service_id']->value > 0) {?>
                        <?php $_smarty_tpl->_subTemplateRender("file:ci/school/ajax.service.history.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                    <?php }?>
                </div>
            </form>
        </div>
    <?php }?>
</div><?php }
}
