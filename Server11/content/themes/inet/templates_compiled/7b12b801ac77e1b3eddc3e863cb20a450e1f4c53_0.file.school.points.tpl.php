<?php
/* Smarty version 3.1.31, created on 2021-06-25 15:20:52
  from "D:\workplace\Inet-project\source\Server11\content\themes\inet\templates\ci\school\school.points.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_60d591e49dd6e8_19615745',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '7b12b801ac77e1b3eddc3e863cb20a450e1f4c53' => 
    array (
      0 => 'D:\\workplace\\Inet-project\\source\\Server11\\content\\themes\\inet\\templates\\ci\\school\\school.points.tpl',
      1 => 1623309629,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60d591e49dd6e8_19615745 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <?php if (($_smarty_tpl->tpl_vars['sub_view']->value == "listsbysubject") || ($_smarty_tpl->tpl_vars['sub_view']->value == "listsbystudent") && $_smarty_tpl->tpl_vars['canEdit']->value) {?>
            <div class="pull-right flip">
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/points/importManual" class="btn btn-default">
                    <i class="fa fa-plus"></i> <?php echo __("Import Manual");?>

                </a>
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/points/importExcel" class="btn btn-default">
                    <i class="fa fa-plus"></i> <?php echo __("Import Excel");?>

                </a>
            </div>
        <?php }?>
        <i class="fa fa-tasks fa-fw fa-lg pr10"></i>
        <?php echo __('Point');?>

        <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == "edit") {?>
            &rsaquo; <?php echo $_smarty_tpl->tpl_vars['data']->value['role_name'];?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "assign") {?>
            &rsaquo; <?php echo __('Assign teacher');?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "listsbysubject") {?>
            &rsaquo; <?php echo __('Lists by subject');?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "listsbystudent") {?>
            &rsaquo; <?php echo __('Lists by student');?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "importManual") {?>
            &rsaquo; <?php echo __('Manual import point');?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "importExcel") {?>
            &rsaquo; <?php echo __('Excel import point');?>

        <?php }?>
    </div>
    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == "listsbysubject") {?>
        <div class="panel-body">
            <form class="form-horizontal" action="#" enctype="multipart/form-data" method="post" id="point_search_form">
                <input type="hidden" name="school_username" id="school_username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                <input type="hidden" name="do" value="search_with_subject"/>
                <div class="form-group">
                    <div class="col-sm-3 col-sm-offset-9">
                        <select name="school_year" id="school_year" class="form-control">
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['default_year']->value, 'year', false, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['year']->value) {
?>
                                
                                <option value="<?php echo $_smarty_tpl->tpl_vars['year']->value;?>
"
                                        <?php if ((substr($_smarty_tpl->tpl_vars['year']->value,0,strlen(date("Y"))) === date("Y"))) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['year']->value;?>
</option>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Select class level");?>
 (*)</label>
                    <div class="col-sm-3">
                        <select name="class_level_id" id="point_class_level_id" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"
                                class="form-control" autofocus>
                            <option value=""><?php echo __("Select class level");?>
</option>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['class_levels']->value, 'level');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['level']->value) {
?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['level']->value['class_level_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['level']->value['class_level_name'];?>
</option>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </select>
                    </div>
                    <div class="col-sm-3">
                        <select name="class_id" id="point_class_id_import" class="form-control"
                                data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" required>
                            <option value=""><?php echo __("Select class");?>
...</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-3 col-sm-offset-3">
                        <select name="semester" class="form-control">
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['semesters']->value, 'name', false, 'semester');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['semester']->value => $_smarty_tpl->tpl_vars['name']->value) {
?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['semester']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['name']->value;?>
</option>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </select>
                    </div>
                    <div class="col-sm-3">
                        <select name="subject_id" id="point_subject_id" class="form-control" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"
                                required>
                            <option value=""><?php echo __("Select subject");?>
...</option>
                        </select>
                    </div>
                </div>
                <?php if ($_smarty_tpl->tpl_vars['grade']->value == 1) {?>
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-3">
                            <select name="is_last_semester" id="is_last_semester" class="form-control">
                                <option value="0"><?php echo __("Mid semester");?>
</option>
                                <option value="1"><?php echo __("Last semester");?>
</option>
                            </select>
                        </div>
                    </div>
                <?php }?>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" id="submit_id" class="btn btn-primary padrl30"><?php echo __("Search");?>
</button>
                    </div>
                </div>
                <div id="school_list_point"></div>
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
            </form>

        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "listsbystudent") {?>
        <div class="panel-body">
            <form class="form-horizontal" action="#" enctype="multipart/form-data" method="post" id="point_search_form">
                <input type="hidden" name="school_username" id="school_username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                <input type="hidden" name="do" value="search_with_student"/>
                <div class="form-group">

                    <div class="col-sm-3 col-sm-offset-9">
                        <select name="school_year" id="school_year" class="form-control">
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['default_year']->value, 'year', false, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['year']->value) {
?>
                                
                                <option value="<?php echo $_smarty_tpl->tpl_vars['year']->value;?>
"
                                        <?php if ((substr($_smarty_tpl->tpl_vars['year']->value,0,strlen(date("Y"))) === date("Y"))) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['year']->value;?>
</option>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Select class level");?>
 (*)</label>
                    <div class="col-sm-3">
                        <select name="class_level_id" id="point_class_level_id" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"
                                class="form-control" autofocus>
                            <option value=""><?php echo __("Select class level");?>
</option>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['class_levels']->value, 'level');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['level']->value) {
?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['level']->value['class_level_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['level']->value['class_level_name'];?>
</option>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </select>
                    </div>
                    <div class="col-sm-3">
                        <select name="class_id" id="point_class_id_import" class="form-control"
                                data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" required>
                            <option value=""><?php echo __("Select class");?>
...</option>
                        </select>
                    </div>
                    <div class="col-sm-3">
                        
                        <select name="student_code" id="point_student_code" class="form-control"
                                data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" required>
                            <option value=""><?php echo __("Select student");?>
...</option>
                        </select>
                    </div>
                </div>
                <?php if ($_smarty_tpl->tpl_vars['grade']->value == 1) {?>
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-3">
                            <select name="is_last_semester" id="is_last_semester" class="form-control">
                                <option value="0"><?php echo __("Mid semester");?>
</option>
                                <option value="1"><?php echo __("Last semester");?>
</option>
                            </select>
                        </div>
                    </div>
                <?php }?>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" id="submit_id" class="btn btn-primary padrl30"><?php echo __("Search");?>
</button>
                    </div>
                </div>
                <div id="school_list_point"></div>
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
            </form>

        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "assign") {?>
        <div class="panel-body">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_point.php">
                <input type="hidden" name="school_username" id="school_username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                
                <input type="hidden" name="do" value="assign"/>
                <div class="form-group">
                    <div class="col-sm-3 col-sm-offset-9">
                        <select name="school_year" id="school_year" class="form-control">
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['default_year']->value, 'year', false, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['year']->value) {
?>
                                
                                <option value="<?php echo $_smarty_tpl->tpl_vars['year']->value;?>
"
                                        <?php if ((substr($_smarty_tpl->tpl_vars['year']->value,0,strlen(date("Y"))) === date("Y"))) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['year']->value;?>
</option>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Select class level");?>
 (*)</label>
                    <div class="col-sm-3">
                        <select name="class_level_id" id="point_class_level_id" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"
                                class="form-control" autofocus>
                            <option value=""><?php echo __("Select class level");?>
</option>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['class_levels']->value, 'level');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['level']->value) {
?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['level']->value['class_level_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['level']->value['class_level_name'];?>
</option>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </select>
                    </div>
                    <div class="col-sm-3">
                        <select name="class_id" id="point_class_id" class="form-control" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"
                                required>
                            <option value=""><?php echo __("Select class");?>
...</option>
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <select name="semester" id="semester" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" class="form-control">
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['semesters']->value, 'name', false, 'semester');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['semester']->value => $_smarty_tpl->tpl_vars['name']->value) {
?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['semester']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['name']->value;?>
</option>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </select>
                    </div>
                </div>

                <div class="table-responsive" id="subject_list"></div>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30"><?php echo __("Save");?>
</button>
                    </div>
                </div>

                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
            </form>

        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "importManual") {?>
        <div class="panel-body">
            <form class="form-horizontal" action="#" enctype="multipart/form-data" method="post"
                  id="point_import_Manual_form">
                <input type="hidden" name="school_username" id="school_username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                <input type="hidden" name="do" value="importManual"/>
                <input type="hidden" name="score_fomula" value="<?php echo $_smarty_tpl->tpl_vars['score_fomula']->value;?>
"/>

                <div class="form-group">
                    <div class="col-sm-3 col-sm-offset-9">
                        <select name="school_year" id="school_year" class="form-control">
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['default_year']->value, 'year', false, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['year']->value) {
?>
                                
                                <option value="<?php echo $_smarty_tpl->tpl_vars['year']->value;?>
"
                                        <?php if ((substr($_smarty_tpl->tpl_vars['year']->value,0,strlen(date("Y"))) === date("Y"))) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['year']->value;?>
</option>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Select class level");?>
 (*)</label>
                </div>
                <div class="form-group">
                    <div class="col-sm-3">
                        <select name="class_level_id" id="point_class_level_id" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"
                                class="form-control" autofocus>
                            <option value=""><?php echo __("Select class level");?>
</option>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['class_levels']->value, 'level');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['level']->value) {
?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['level']->value['class_level_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['level']->value['class_level_name'];?>
</option>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </select>
                    </div>
                    <div class="col-sm-3">
                        <select name="class_id" id="point_class_id_import" class="form-control"
                                data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" required>
                            <option value=""><?php echo __("Select class");?>
...</option>
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <select name="semester" id="semester" class="form-control">
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['semesters']->value, 'name', false, 'semester');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['semester']->value => $_smarty_tpl->tpl_vars['name']->value) {
?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['semester']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['name']->value;?>
</option>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </select>
                    </div>
                    <div class="col-sm-3">
                        <select name="subject_id" id="point_subject_id" class="form-control" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"
                                required>
                            <option value=""><?php echo __("Select subject");?>
...</option>
                        </select>
                    </div>

                </div>
                <?php if ($_smarty_tpl->tpl_vars['grade']->value == 1) {?>
                    <div class="form-group">
                        <div class="col-sm-3">
                            <select name="is_last_semester" id="is_last_semester" class="form-control">
                                <option value="0"><?php echo __("Mid semester");?>
</option>
                                <option value="1"><?php echo __("Last semester");?>
</option>
                            </select>
                        </div>
                    </div>
                <?php }?>
                <div class="form-group action-point" style="display: none">
                    <div class="col-sm-2">
                        <label class="col-sm-2 control-label text-left"><?php echo __("Action");?>
</label>
                    </div>
                    <div class="col-sm-10">
                        <div class="form-group">
                            <div class="col-sm-4 point_tx-add point_tx1-add">
                                <label class="point_tx-add"><?php echo __("Regular score");?>
 :</label>
                                <label class="point_tx1-add"><?php echo __("Regular score 1");?>
 :</label>
                                <button type="button" style="display: none;" class="btn btn-success js_point_tx-add">+
                                </button>
                                <button type="button" style="display: none;" class="btn btn-success js_point_tx1-add">+
                                </button>
                            </div>
                            <div class="col-sm-4 point_gk-add point_gk1-add">
                                <label class="point_gk-add"><?php echo __("Midterm grades");?>
 :</label>
                                <label class="point_gk1-add"><?php echo __("Midterm grades 1");?>
 :</label>
                                <button type="button" style="display: none;" class="btn btn-success js_point_gk-add">+
                                </button>
                                <button type="button" style="display: none;" class="btn btn-success js_point_gk1-add">+
                                </button>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4 point_tx1-add">
                                <label class="point_tx1-add"><?php echo __("Regular score 2");?>
 :</label>
                                <button type="button" style="display: none;" class="btn btn-success js_point_tx2-add">+
                                </button>
                            </div>
                            <div class="col-sm-4 point_gk2-add">
                                <label class="point_gk2-add"><?php echo __("Midterm grades 2");?>
 :</label>
                                <button type="button" style="display: none;" class="btn btn-success js_point_gk2-add">+
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="school_list_point"></div>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-4">
                        <button type="submit" id="submit_id" class="btn btn-primary padrl30"><?php echo __("Save");?>
</button>
                    </div>
                </div>

                <div class="table-responsive" id="result_info" name="result_info"></div>

                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
            </form>

        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "importExcel") {?>
        <div class="panel-body">
            <form class="form-horizontal" action="#" enctype="multipart/form-data" method="post"
                  id="point_import_excel_form">
                <input type="hidden" name="school_username" id="school_username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                <input type="hidden" name="do" value="importExcel"/>

                <div class="form-group">
                    <div class="col-sm-3 col-sm-offset-9">
                        <select name="school_year" id="school_year" class="form-control">
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['default_year']->value, 'year', false, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['year']->value) {
?>
                                
                                <option value="<?php echo $_smarty_tpl->tpl_vars['year']->value;?>
"
                                        <?php if ((substr($_smarty_tpl->tpl_vars['year']->value,0,strlen(date("Y"))) === date("Y"))) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['year']->value;?>
</option>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Select class level");?>
 (*)</label>
                </div>
                <div class="form-group">
                    <div class="col-sm-3">
                        <select name="class_level_id" id="point_class_level_id" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"
                                class="form-control" autofocus>
                            <option value=""><?php echo __("Select class level");?>
</option>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['class_levels']->value, 'level');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['level']->value) {
?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['level']->value['class_level_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['level']->value['class_level_name'];?>
</option>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </select>
                    </div>
                    <div class="col-sm-3">
                        <select name="class_id" id="point_class_id_import" class="form-control"
                                data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" required>
                            <option value=""><?php echo __("Select class");?>
...</option>
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <select name="subject_id" id="point_subject_id" class="form-control" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"
                                required>
                            <option value=""><?php echo __("Select subject");?>
...</option>
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <select name="semester" class="form-control">
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['semesters']->value, 'name', false, 'semester');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['semester']->value => $_smarty_tpl->tpl_vars['name']->value) {
?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['semester']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['name']->value;?>
</option>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </select>
                    </div>
                </div>
                <?php if ($_smarty_tpl->tpl_vars['grade']->value == 1) {?>
                    <div class="form-group">
                        <div class="col-sm-3">
                            <select name="is_last_semester" id="is_last_semester" class="form-control">
                                <option value="0"><?php echo __("Mid semester");?>
</option>
                                <option value="1"><?php echo __("Last semester");?>
</option>
                            </select>
                        </div>
                    </div>
                <?php }?>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Select Excel file");?>
</label>
                    <div class="col-sm-6">
                        <input type="file" name="file" id="file">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" id="submit_id" class="btn btn-primary padrl30"><?php echo __("Save");?>
</button>
                    </div>
                </div>

                <div class="table-responsive" id="result_info" name="result_info"></div>

                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
            </form>

        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "comment") {?>
        <div class="panel-body">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_point.php">
                <input type="hidden" name="school_username" id="school_username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                <input type="hidden" name="do" value="comment"/>
                <div class="form-group">
                    <div class="col-sm-3 col-sm-offset-9">
                        <select name="school_year" id="school_year" class="form-control">
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['default_year']->value, 'year', false, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['year']->value) {
?>
                                
                                <option value="<?php echo $_smarty_tpl->tpl_vars['year']->value;?>
"
                                        <?php if ((substr($_smarty_tpl->tpl_vars['year']->value,0,strlen(date("Y"))) === date("Y"))) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['year']->value;?>
</option>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Select class level");?>
 (*)</label>
                    <div class="col-sm-3">
                        <select name="class_level_id" id="point_class_level_id" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"
                                class="form-control" autofocus>
                            <option value=""><?php echo __("Select class level");?>
</option>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['class_levels']->value, 'level');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['level']->value) {
?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['level']->value['class_level_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['level']->value['class_level_name'];?>
</option>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </select>
                    </div>
                    <div class="col-sm-3">
                        <select name="class_id" id="point_class_id_comment" class="form-control"
                                data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" required>
                            <option value=""><?php echo __("Select class");?>
...</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-3 col-sm-offset-3">
                        <select name="child_id" id="point_child_id_comment" class="form-control"
                                data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" required>
                            <option value=""><?php echo __("Select student...");?>
</option>
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <select id="semester" name="semester" class="form-control">
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['semesters']->value, 'name', false, 'semester');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['semester']->value => $_smarty_tpl->tpl_vars['name']->value) {
?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['semester']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['name']->value;?>
</option>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </select>
                    </div>
                </div>
                <?php if ($_smarty_tpl->tpl_vars['grade']->value == 1) {?>
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-3">
                            <select name="is_last_semester" id="is_last_semester" class="form-control">
                                <option value="0"><?php echo __("Mid semester");?>
</option>
                                <option value="1"><?php echo __("Last semester");?>
</option>
                            </select>
                        </div>
                    </div>
                <?php }?>

                
                
                
                
                

                <div class="table-responsive" id="child_comment"></div>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-4">
                        <button type="submit" class="btn btn-primary padrl30" disabled="true"
                                id="submit_id"><?php echo __("Save");?>
</button>
                    </div>
                </div>
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
            </form>

        </div>
    <?php }?>
</div><?php }
}
