<?php
/* Smarty version 3.1.31, created on 2021-06-25 15:59:28
  from "D:\workplace\Inet-project\source\Server11\content\themes\inet\templates\ci\class\ajax.points.importresult.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_60d59af038fe25_85795252',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '673a9ec8166c3daf750658c766957f01144fab72' => 
    array (
      0 => 'D:\\workplace\\Inet-project\\source\\Server11\\content\\themes\\inet\\templates\\ci\\class\\ajax.points.importresult.tpl',
      1 => 1621501238,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60d59af038fe25_85795252 (Smarty_Internal_Template $_smarty_tpl) {
if ($_smarty_tpl->tpl_vars['sheet']->value['error'] == 1) {?>
    <table class="table table-striped table-bordered table-hover">
        <thead>
        <tr><th><?php echo __("Detail results");?>
</th></tr>
        </thead>
        <tbody>
        <tr><td><div><?php echo $_smarty_tpl->tpl_vars['sheet']->value['message'];?>
</div></td></tr>
        </tbody>
    </table>
<?php } else { ?>

        <?php $_smarty_tpl->_assignInScope('idx', 1);
?>

        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['sheet']->value['child_list'], 'child');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['child']->value) {
?>
            <?php if ($_smarty_tpl->tpl_vars['child']->value['error'] != 0) {?>
                <?php $_smarty_tpl->_assignInScope('error', 1);
?>
            <?php }?>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

        <div style="text-align: center">
        <?php if ($_smarty_tpl->tpl_vars['error']->value == 0) {?>
            <strong style="color: green"><?php echo __("Add point success");?>
</strong>
       <?php } else { ?>
            <strong style="color: red"><?php echo __("Add point not success");?>
</strong>
        <?php }?>
        </div>
            <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
}
}
}
