<?php
/* Smarty version 3.1.31, created on 2021-03-31 14:04:31
  from "D:\workplace\Server11\content\themes\inet\templates\childinfo.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_60641effe24df0_18352251',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'dfbf2441eef2ed31e4dc68d5656beb6d6742dcad' => 
    array (
      0 => 'D:\\workplace\\Server11\\content\\themes\\inet\\templates\\childinfo.tpl',
      1 => 1573458087,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:_head.tpl' => 1,
    'file:_header.tpl' => 1,
    'file:ci/childinfo/childinfo.help.tpl' => 1,
    'file:ci/childinfo/childinfo.dashboard.tpl' => 1,
    'file:ci/childinfo/childinfo.".((string)$_smarty_tpl->tpl_vars[\'view\']->value).".tpl' => 1,
    'file:_footer.tpl' => 1,
  ),
),false)) {
function content_60641effe24df0_18352251 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:_head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php $_smarty_tpl->_subTemplateRender('file:_header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<div class="container mt20 height_min"> <!-- class height_min để set height tối thiểu của container -->
    <div class="row">
        <div class="col-md-3 col-sm-3">
            <nav class="navbar navbar-default" role="navigation">
                <div class="panel panel-default">
                    <div class = "menu_school">
                        <!-- Dashbroad -->
                        <?php if ($_smarty_tpl->tpl_vars['view']->value == '') {?>
                            
                        <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "childdetail") {?>
                            <i class="fa fa-info-circle fa-lg fa-fw pr10"></i> <?php echo __("Student information detail");?>

                        <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "health") {?>
                            <i class="fa fa-medkit fa-fw fa-lg pr10"></i> <?php echo __("Health information");?>

                        <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "medicals") {?>
                            <i class="fa fa-user-md fa-lg fa-fw pr10"></i> <?php echo __("Medical report book");?>

                        <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "journals") {?>
                            <i class="fa fa-file-image fa-lg fa-fw pr10"></i> <?php echo __("Journal corner");?>

                        <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "childdevelopments") {?>
                            <i class="fa fa-file-image fa-lg fa-fw pr10"></i> <?php echo __("Child development");?>

                        <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "foetusdevelopments") {?>
                            <i class="fa fa-file-image fa-lg fa-fw pr10"></i> <?php echo __("Foetus development");?>

                        <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "childmonths") {?>
                            <i class="fa fa-share fa-lg fa-fw pr10"></i> <?php echo __("Foetus knowledge");?>

                        <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "pregnancys") {?>
                            <i class="fa fa-paper-plane fa-lg fa-fw pr10"></i> <?php echo __("Foetus knowledge");?>

                        <?php }?>
                    </div>
                    <div class="navbar-header">

                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <?php if (count($_smarty_tpl->tpl_vars['schoolList']->value) > 0 || count($_smarty_tpl->tpl_vars['classList']->value) > 0 || count($_smarty_tpl->tpl_vars['childrenList']->value) > 0) {?>
                        <div class="select_object_box">
                            <select name="object" id="select_object">
                                <?php if (count($_smarty_tpl->tpl_vars['schoolList']->value) > 0) {?>
                                    <option disabled>----- <?php echo __("School");?>
 -----</option>
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['schoolList']->value, 'school');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['school']->value) {
?>
                                        <option data-type="school" value="<?php echo $_smarty_tpl->tpl_vars['school']->value['page_name'];?>
" <?php if ($_smarty_tpl->tpl_vars['school']->value['page_name'] == $_smarty_tpl->tpl_vars['username']->value) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['school']->value['page_title'];?>
</option>
                                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                <?php }?>
                                <?php if (count($_smarty_tpl->tpl_vars['classList']->value) > 0) {?>
                                    <option disabled>----- <?php echo __("Class");?>
 -----</option>
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['classList']->value, 'class');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class']->value) {
?>
                                        <option data-type="class" value="<?php echo $_smarty_tpl->tpl_vars['class']->value['group_name'];?>
" <?php if ($_smarty_tpl->tpl_vars['class']->value['group_name'] == $_smarty_tpl->tpl_vars['username']->value) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['class']->value['group_title'];?>
</option>
                                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>
a
                                <?php }?>
                                <?php if (count($_smarty_tpl->tpl_vars['childrenList']->value) > 0) {?>
                                    <option disabled>----- <?php echo __("Child");?>
 -----</option>
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['childrenList']->value, 'childOb');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['childOb']->value) {
?>
                                        <option <?php if ($_smarty_tpl->tpl_vars['childOb']->value['school_id'] > 0) {?>data-type="child" value="<?php echo $_smarty_tpl->tpl_vars['childOb']->value['child_id'];?>
" <?php if ($_smarty_tpl->tpl_vars['childOb']->value['child_id'] == $_smarty_tpl->tpl_vars['child']->value['child_id']) {?>selected<?php }?> <?php } else { ?>data-type="childinfo" value="<?php echo $_smarty_tpl->tpl_vars['childOb']->value['child_parent_id'];?>
" <?php if ($_smarty_tpl->tpl_vars['childOb']->value['child_parent_id'] == $_smarty_tpl->tpl_vars['child']->value['child_parent_id']) {?>selected<?php }
}?>><?php echo $_smarty_tpl->tpl_vars['childOb']->value['child_name'];?>
</option>
                                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                <?php }?>
                            </select>
                        </div>
                    <?php }?>
                    <div class="panel-body with-nav collapse navbar-collapse" id="collapse">
                        <ul class="side-nav metismenu js_metisMenu">
                            <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "childdetail") {?>class="active"<?php }?>>
                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/childinfo/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_parent_id'];?>
/childdetail">
                                    <i class="fa fa-info-circle fa-lg fa-fw pr10"></i> <?php if (!$_smarty_tpl->tpl_vars['child']->value['is_pregnant']) {
echo __("Student information detail");
} else {
echo __("Fetus information detail");
}?>
                                </a>
                            </li>
                            <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "health") {?>class="active"<?php }?>>
                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/childinfo/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_parent_id'];?>
/health">
                                    <i class="fa fa-medkit fa-lg fa-fw pr10"></i> <?php echo __("Health information");?>

                                </a>
                                
                                    
                                        
                                        
                                        
                                    
                                    
                                        
                                            
                                                
                                            
                                        
                                    
                                
                            </li>
                            <?php if (!$_smarty_tpl->tpl_vars['child']->value['is_pregnant']) {?>
                                <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "medicals") {?>class="active"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/childinfo/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_parent_id'];?>
/medicals">
                                        <i class="fa fa-user-md fa-lg fa-fw pr10"></i> <?php echo __("Medical report book");?>

                                        <span class="fa arrow"></span>
                                    </a>
                                    <ul>
                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "medicals" && $_smarty_tpl->tpl_vars['sub_view']->value == '') {?> class="active selected"<?php }?>>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/childinfo/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_parent_id'];?>
/medicals">
                                                <i class="fa fa-caret-right fa-fw pr10"></i> <?php echo __("Lists");?>

                                            </a>
                                        </li>
                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "medicals" && $_smarty_tpl->tpl_vars['sub_view']->value == "add") {?> class="active selected" <?php }?>>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/childinfo/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_parent_id'];?>
/medicals/add">
                                                <i class="fa fa-caret-right fa-fw pr10"></i> <?php echo __("Add New");?>

                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "journals") {?>class="active"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/childinfo/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_parent_id'];?>
/journals">
                                        <i class="fa fa-file-image fa-lg fa-fw pr10"></i> <?php echo __("Journal corner");?>

                                        <span class="fa arrow"></span>
                                    </a>
                                    <ul>
                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "journals" && $_smarty_tpl->tpl_vars['sub_view']->value == '') {?> class="active selected"<?php }?>>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/childinfo/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_parent_id'];?>
/journals">
                                                <i class="fa fa-caret-right fa-fw pr10"></i> <?php echo __("Lists");?>

                                            </a>
                                        </li>
                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "journals" && $_smarty_tpl->tpl_vars['sub_view']->value == "add") {?> class="active selected" <?php }?>>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/childinfo/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_parent_id'];?>
/journals/add">
                                                <i class="fa fa-caret-right fa-fw pr10"></i> <?php echo __("Add New");?>

                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "childdevelopments") {?>class="active"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/childinfo/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_parent_id'];?>
/childdevelopments">
                                        <i class="fas fa-chart-line fa-lg fa-fw pr10"></i> <?php echo __("Child development");?>

                                        <span class="fa arrow"></span>
                                    </a>
                                    <ul>
                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "childdevelopments" && $_smarty_tpl->tpl_vars['sub_view']->value == '') {?> class="active selected"<?php }?>>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/childinfo/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_parent_id'];?>
/childdevelopments">
                                                <i class="fa fa-caret-right fa-fw pr10"></i> <?php echo __("Vaccination schedule");?>

                                            </a>
                                        </li>
                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "childdevelopments" && $_smarty_tpl->tpl_vars['sub_view']->value == "referion") {?> class="active selected" <?php }?>>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/childinfo/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_parent_id'];?>
/childdevelopments/referion">
                                                <i class="fa fa-caret-right fa-fw pr10"></i> <?php echo __("Referion information");?>

                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "childmonths") {?>class="active"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/childinfo/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_parent_id'];?>
/childmonths">
                                        <i class="fa fa-share fa-lg fa-fw pr10"></i> <?php echo __("Month age information");?>

                                    </a>
                                </li>
                            <?php }?>
                            <?php if ($_smarty_tpl->tpl_vars['child']->value['is_pregnant']) {?>
                                <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "foetusdevelopments") {?>class="active"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/childinfo/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_parent_id'];?>
/foetusdevelopments">
                                        <i class="fa fa-line-chart fa-lg fa-fw pr10"></i> <?php echo __("Foetus development");?>

                                    </a>
                                    <ul>
                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "foetusdevelopments" && $_smarty_tpl->tpl_vars['sub_view']->value == '') {?> class="active selected"<?php }?>>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/childinfo/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_parent_id'];?>
/foetusdevelopments">
                                                <i class="fa fa-caret-right fa-fw pr10"></i> <?php echo __("Pregnancy check-up");?>

                                            </a>
                                        </li>
                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "childdevelopments" && $_smarty_tpl->tpl_vars['sub_view']->value == "referion") {?> class="active selected" <?php }?>>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/childinfo/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_parent_id'];?>
/foetusdevelopments/referion">
                                                <i class="fa fa-caret-right fa-fw pr10"></i> <?php echo __("Referion information");?>

                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "pregnancys") {?>class="active"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/childinfo/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_parent_id'];?>
/pregnancys">
                                        <i class="fa fa-paper-plane fa-lg fa-fw pr10"></i> <?php echo __("Pregnancy information");?>

                                    </a>
                                </li>
                            <?php }?>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>

        <div class="col-md-9 col-sm-9">
            <?php $_smarty_tpl->_subTemplateRender('file:ci/childinfo/childinfo.help.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

        </div>
        <div class="col-md-9 col-sm-9">
            <?php if ($_smarty_tpl->tpl_vars['view']->value == '') {?>
                <?php $_smarty_tpl->_subTemplateRender('file:ci/childinfo/childinfo.dashboard.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

            <?php } else { ?>
                <?php $_smarty_tpl->_subTemplateRender("file:ci/childinfo/childinfo.".((string)$_smarty_tpl->tpl_vars['view']->value).".tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

            <?php }?>
        </div>
    </div>
</div>
<!-- page content -->


<?php $_smarty_tpl->_subTemplateRender('file:_footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
