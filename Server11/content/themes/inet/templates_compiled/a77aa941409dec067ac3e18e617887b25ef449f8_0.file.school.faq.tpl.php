<?php
/* Smarty version 3.1.31, created on 2021-05-20 16:59:30
  from "D:\workplace\Inet-project\source\Server11\content\themes\inet\templates\ci\school\school.faq.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_60a6330295c317_93898783',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'a77aa941409dec067ac3e18e617887b25ef449f8' => 
    array (
      0 => 'D:\\workplace\\Inet-project\\source\\Server11\\content\\themes\\inet\\templates\\ci\\school\\school.faq.tpl',
      1 => 1621501239,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60a6330295c317_93898783 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="faq_tab">
    <?php if ($_smarty_tpl->tpl_vars['view']->value == "tuitions") {?>
        <div class="faq_header">
            <h4><?php echo __("FAQ");?>
</h4>
        </div>
        <div class="faq_content">
            <div>
                <a href="https://blog.coniu.vn/huong-dan-tao-cac-loai-phi-cho-tre-hoc-phi-hang-thang-tien-an-hang-thang/" target="_blank">1. <?php echo __("Khi cần thêm một loại phí mới thì làm thế nào?");?>
</a>
            </div>
            <div><a href="https://blog.coniu.vn/khi-can-xoa-mot-loai-phi-can-phai-lam-nhu-nao/" target="_blank">2. <?php echo __("Khi cần xóa một loại phí mới thì làm thế nào?");?>
</a></div>
            <div><a href="https://blog.coniu.vn/huong-dan-sua-hoc-phi-da-tao-cho-tre/" target="_blank">3. <?php echo __("Sửa học phí cho một trẻ trong lớp thì làm thế nào?");?>
</a></div>
            <div><a href="https://blog.coniu.vn/tinh-hoc-phi-cho-tre-moi-them-vao-lop-khi-da-tao-hoc-phi-thang/" target="_blank">4. <?php echo __("Tính học phí trẻ được thêm mới vào lớp, trước đó lớp đã tạo học phí thì làm thế nào?");?>
</a></div>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "services") {?>
        <div class="faq_header">
            <h4><?php echo __("FAQ");?>
</h4>
        </div>
        <div class="faq_content">
            <div><a href="https://blog.coniu.vn/them-mot-loai-dich-vu-moi-thi-phai-lam-nhu-the-nao/" target="_blank">1. <?php echo __("Thêm một loại dịch vụ mới thì làm thế nào?");?>
</a></div>
            <div><a href="https://blog.coniu.vn/dang-ky-dich-vu-cho-tre-thi-phai-lam-nhu-the-nao/" target="_blank">2. <?php echo __("Đăng ký dịch vụ cho trẻ thì phải làm thế nào?");?>
</a></div>
            <div><a href="https://blog.coniu.vn/khi-them-moi-mot-loai-dich-vu-can-luu-y-nhung-gi/" target="_blank">3. <?php echo __("Thêm mới một loại dịch vụ cần lưu ý những gì?");?>
</a></div>
            <div><a href="https://blog.coniu.vn/khi-can-xoa-mot-loai-dich-vu-thi-phai-lam-nhu-the-nao/
" target="_blank">4. <?php echo __("Làm thế nào để xóa một dịch vụ?");?>
</a></div>
            <div><a href="https://blog.coniu.vn/huong-dan-huy-dich-vu-da-dang-ky-cho-tre/
" target="_blank">5. <?php echo __("Làm thế nào để hủy một dịch vụ đã đăng ký cho trẻ?");?>
</a></div>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "pickup") {?>
        <div class="faq_header">
            <h4><?php echo __("FAQ");?>
</h4>
        </div>
        <div class="faq_content">
            <div><a href="https://blog.coniu.vn/tien-tra-muon-cua-tre-bi-sai-trong-thang-co-sua-duoc-khong/" target="_blank">1. <?php echo __("Có sửa được tiền đón muộn của trẻ bị sai không?");?>
</a></div>
            <div><a href="https://blog.coniu.vn/them-tre-vao-lop-tra-muon-thi-lam-nhu-the-nao/" target="_blank">2. <?php echo __("Làm thế nào để thêm trẻ vào lớp đón muộn?");?>
</a></div>
            <div><a href="https://blog.coniu.vn/huong-dan-sua-thoi-gian-va-gia-cho-tre-khi-giao-vien-da-thao-tac-tra-muon/" target="_blank">3. <?php echo __("Làm thế nào để sửa thời gian và giá cho trẻ khi giáo viên đã thao tác đón muộn?");?>
</a></div>
            <div><a href="https://blog.coniu.vn/huong-dan-thuc-hien-tra-tre-tren-website/" target="_blank">4. <?php echo __("Làm thế nào để trả trẻ trên website?");?>
</a></div>
        </div>

    <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "attendance") {?>
        <div class="faq_header">
            <h4><?php echo __("FAQ");?>
</h4>
        </div>
        <div class="faq_content">
            <div>
                <a href="https://blog.coniu.vn/huong-dan-diem-danh-lai-cho-mot-lop/" target="_blank">1. <?php echo __("Điểm danh lại cho một lớp như thế nào?");?>
</a>
            </div>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "classes") {?>
        <div class="faq_header">
            <h4><?php echo __("FAQ");?>
</h4>
        </div>
        <div class="faq_content">
            <div><a href="https://blog.coniu.vn/huong-dan-phan-cong-giao-vien-vao-mot-lop/" target="_blank">1. <?php echo __("Làm thế nào để phân công một giáo viên vào lớp?");?>
</a></div>
            <div><a href="https://blog.coniu.vn/lam-the-nao-de-xoa-mot-lop/" target="_blank">2. <?php echo __("Làm thế nào để xóa một lớp?");?>
</a></div>
        </div>
    <?php }?>
</div>
<?php }
}
