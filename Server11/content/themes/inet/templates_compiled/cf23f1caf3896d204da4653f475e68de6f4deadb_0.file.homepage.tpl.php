<?php
/* Smarty version 3.1.31, created on 2021-03-30 13:39:30
  from "D:\workplace\Server11\content\themes\inet\templates\homepage.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_6062c7a2e24ef6_53571740',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'cf23f1caf3896d204da4653f475e68de6f4deadb' => 
    array (
      0 => 'D:\\workplace\\Server11\\content\\themes\\inet\\templates\\homepage.tpl',
      1 => 1573458086,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:_head_new.tpl' => 1,
    'file:_header_new.tpl' => 1,
    'file:_footer_new.tpl' => 1,
  ),
),false)) {
function content_6062c7a2e24ef6_53571740 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:_head_new.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php $_smarty_tpl->_subTemplateRender('file:_header_new.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<div id="top"></div>
<div class="soicial_left_box">
    <div class="soicial_normal soi_face" align="center"><a href="https://facebook.com/inet.vn" target="_blank"><i
                    class="fa fa-facebook-f"></i></a></div>
    <div class="soicial_normal soi_goo" align="center"><a href="https://plus.google.com/u/0/104963440910493823086"
                                                          target="_blank"><i class="fa fa-google"></i></a></div>
    <div class="soicial_normal soi_twi" align="center"><a href="https://twitter.com/ConiuVn" target="_blank"><i
                    class="fa fa-twitter"></i></a></div>
</div>
<div class="box-video">
    <div class="video">
        <a href="#" class="register_close"><i class="fa fa-close"></i></a>
        <iframe class="myVideo" width="100%" height="450"
                src="https://www.youtube.com/embed/xUEI4tcYo7w?rel=0&amp;controls=0&amp;autoplay=0" frameborder="0"
                allowfullscreen=""></iframe>
    </div>
</div>
<div class="box_reg_school">
    <div class="reg_school">
        <a href="#" class="register_school_close"><i class="fa fa-close"></i></a>
        <div class="contact_form">
            <div class align="center">
                <h3><b><?php echo mb_strtoupper(__("FREE register for your school"), 'UTF-8');?>
</b></h3>
            </div>
            <form class="js_ajax-forms" data-url="ci/bo/school/bo_contact.php" action="_email">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="control-group">
                            <div class="control" style="margin-top: 5px">
                                <input type="text" id="name2" name="name2"
                                       placeholder="<?php echo __('School name/Your name');?>
 (*)" class="form-control"
                                       required maxlength="255">
                            </div>
                        </div>
                        <div class="row">
                            <div class="control-group col-sm-6">
                                <div class="control">
                                    <input type="email" class="form-control" name="email2" placeholder="Email (*)"
                                           required maxlength="50">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="control-group col-sm-6">
                                <div class="control">
                                    <input type="text" id="phone_contact" name="phone2"
                                           placeholder="<?php echo __('Telephone');?>
 (*)" class="form-control" required/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="control-group">
                            <textarea rows="4" id="content_contact" name="content2"
                                      placeholder="<?php echo __('Please enter all information, Coniu staff will contact you soon. Note: Only correct school information will be supported.');?>
 (*)"
                                      class="form-control content_class"
                                      required></textarea>
                        </div>
                    </div>
                    <div class="col-xs-12" align="center">
                        <a class="register_term" href="#"><?php echo __("Terms and conditions for FREE registration");?>
</a></span>

                    </div>
                </div>
                <?php if ($_smarty_tpl->tpl_vars['system']->value['reCAPTCHA_enabled']) {?>
                    <div id="recaptcha2"></div>
                <?php }?>

                <div class="control-ci" align="center">
                    <button type="submit" class="btn btn-success"
                            style="width: 50%"><?php echo mb_strtoupper(__("FREE register for your school"), 'UTF-8');?>
</button>
                </div>
                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->
                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    </div>
</div>
<div class="login_overlay"></div>
<div class="home_wrapper">
    <div class="banner_img">
        <img class="img-responsive img_hd"
             src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/content/themes/<?php echo $_smarty_tpl->tpl_vars['system']->value['theme'];?>
/images/banner.jpg"
             alt="Phần mềm quản lý mầm non Coniu">
        <img class="img-responsive img_fullhd"
             src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/content/themes/<?php echo $_smarty_tpl->tpl_vars['system']->value['theme'];?>
/images/banner_full_hd.jpg"
             alt="Phần mềm quản lý mầm non Coniu">
        <div class="banner-content">
            <div class="container">
                <div class="row row_content">
                    <div class="col-sm-8 banner_left_col">
                        <div class="banner_title">
                            <h1><?php echo __("Application of the most comprehensive management and connection of school");?>
</h1>
                        </div>
                        <div class="video_play" align="center">
                            <div class="js_play-video-home">
                                <div class="play_video play_video_button">
                                    <a href="#" class="icon-video"></a>
                                </div>
                            </div>
                            <div class="js_play-video-home_hidden">
                                <div class="play_video play_video_button_link">
                                    <a href="https://www.youtube.com/watch?v=xUEI4tcYo7w" class="icon-video"></a>
                                </div>
                            </div>
                        </div>
                        <div class="banner-trial" align="center">
                            <h4>
                                <b><?php echo mb_strtoupper(__("Free download"), 'UTF-8');?>
</b>
                            </h4>
                        </div>
                        <div class="banner_store row" align="center">
                            <div class="google col-sm-6" align="right">
                                <a id="android_banner" href="<?php echo @constant('GOOGLEPLAY_URL');?>
" target="_blank"> <img
                                            class="img-responsive google_img"
                                            src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/content/themes/<?php echo $_smarty_tpl->tpl_vars['system']->value['theme'];?>
/images/img/google.png"
                                            alt="Phần mềm mầm non Coniu trên CH Play"/> </a>
                                <a id="ios_banner" href="<?php echo @constant('APPSTORE_URL');?>
" target="_blank"><img
                                            class="img-responsive ios_img"
                                            src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/content/themes/<?php echo $_smarty_tpl->tpl_vars['system']->value['theme'];?>
/images/img/ios.png"
                                            alt="Phần mềm mầm non Coniu trên App store"/> </a>
                            </div>
                            <div class="qr_code col-sm-6" align="left">
                                <div class="qr_code_box">
                                    <h5>Ưu điểm vượt trội</h5>
                                    <p><i class="fa fa-check"></i> Không phí cài đặt </p>
                                    <p><i class="fa fa-check"></i> Đa dạng chức năng </p>
                                    <p><i class="fa fa-check"></i> Hỗ trợ 24/7 </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 banner_right_col">
                        <div class="banner_log">
                            <div class="banner_signin panel-body <?php if ($_smarty_tpl->tpl_vars['do']->value == 'up') {?>x-hidden<?php }?>">
                                <h3 class="mb20" align="left"><?php echo __("Sign in");?>
</h3>
                                <div class="login-con">
                                    <div class="signin_mobile">
                                        <form id="login_success" class="js_ajax_not_modal-forms" method="POST" data-url="core/verify_phone.php">
                                            <input id="csrf" type="hidden" name="csrf" />
                                            <input id="code" type="hidden" name="code" />
                                        </form>

                                        <form class="js_ajax_not_modal-forms" data-url="core/signin.php">
                                            <div class="form-group">
                                                <input type="text" id="email" name="username_email"
                                                        
                                                       placeholder='<?php echo __("Email");?>
' class="form-control" required>
                                            </div>
                                            <div class="form-group">
                                                <input name="password" id="password" type="password"
                                                       class="form-control" placeholder='<?php echo __("Password");?>
' required>
                                            </div>

                                            <div class="row">
                                                <div class="signin_text">
                                                    <div class="col-xs-6">
                                                        <div class="check-box">
                                                            <div class="checkbox">
                                                                <label><input style="margin-top: 2px;"
                                                                              type="checkbox" id="remember"
                                                                              name="remember"><?php echo __("Remember me");?>

                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-6" align="right">
                                                        <div class="q_password">
                                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/reset"><?php echo __("Forget your password?");?>
</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div align="center">
                                                        <div class="control-ci">
                                                            <button style="width: 100%" type="submit"
                                                                    class="btn btn-primary but_signin"> <?php echo __("Sign in");?>
 </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- error -->
                                            <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                                            <!-- error -->

                                        </form>
                                        <?php if ($_smarty_tpl->tpl_vars['system']->value['social_login_enabled']) {?>
                                            <div class="hr-heading mt15 mb10">
                                                <div class="hr-heading-text">
                                                    <?php echo __("Or");?>
 <?php echo __("login with");?>

                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <a onclick="smsLogin();" class="btn btn-block btn-social btn-success">
                                                    <i class=" fa fas fa-mobile"></i> <strong><?php echo mb_strtoupper(__("Đăng nhập bằng số điện thoại"), 'UTF-8');?>
</strong>
                                                </a>
                                            </div>
                                            <?php if ($_smarty_tpl->tpl_vars['system']->value['facebook_login_enabled'] || $_smarty_tpl->tpl_vars['system']->value['google_login_enabled']) {?>
                                                <div class="login_soicial">
                                                    <div class="form-group">
                                                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/connect/facebook" class="btn btn-block btn-social btn-facebook">
                                                            <i class="fa fa-facebook-f"></i> <strong><?php echo mb_strtoupper(__("Đăng nhập bằng Facebook"), 'UTF-8');?>
</strong>
                                                        </a>
                                                    </div>
                                                    
                                                        
                                                            
                                                        
                                                    
                                                </div>
                                            <?php }?>
                                        <?php }?>
                                        <?php if ($_smarty_tpl->tpl_vars['system']->value['registration_enabled'] || (!$_smarty_tpl->tpl_vars['system']->value['registration_enabled'] && $_smarty_tpl->tpl_vars['system']->value['invitation_enabled'])) {?>
                                            
                                                
                                            
                                            <div class="login_soicial">
                                                <div class="form-group">
                                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/signup"
                                                       class="js_toggle-panel btn btn-block btn-social btn-google"><i class="fa fa-user" aria-hidden="true"></i> <strong><?php echo mb_strtoupper(__("Register"), 'UTF-8');?>
</strong></a>
                                                </div>
                                            </div>
                                        <?php }?>

                                    </div>

                                </div>
                            </div>
                            <div class="banner_signup panel-body <?php if ($_smarty_tpl->tpl_vars['do']->value != 'up') {?>x-hidden<?php }?>" align="left">
                                <h3><?php echo __("Create a new account");?>
</h3>
                                <div class="signup">
                                    
                                        
                                        
                                    <form class="js_ajax_not_modal-forms" data-url="core/signup.php">

                                        <div class="name_taila">
                                            <div class="row row_dk">
                                                <div class="col-xs-6 col_dk" style="padding-right: 7.5px">
                                                    <div class="control-group">
                                                        <div class="controls">
                                                            <input type="text" id="last_name" name="last_name"
                                                                   placeholder='<?php echo __("Last name");?>
' class="form-control"
                                                                   required/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-6 col_dk" style="padding-left: 7.5px">
                                                    <div class="control-group">
                                                        <div class="controls">
                                                            <input type="text" id="first_name" name="first_name"
                                                                   placeholder='<?php echo __("First name");?>
' class="form-control"
                                                                   required/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="name_taila">
                                            <div class="row row_dk">
                                                <div class="col-xs-8 col_dk" style="padding-right: 7.5px">
                                                    <div class="control-group">
                                                        <div class="controls">
                                                            <input type="text" id="email" name="email"
                                                                    
                                                                   placeholder='<?php echo __("Email");?>
' class="form-control"
                                                                   required/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-4 col_dk" style="padding-left: 7.5px">
                                                    <div class="control-group">
                                                        <div class="controls">
                                                            <select name="gender" id="gender" class="form-control"
                                                                    required>
                                                                <option value="female"><?php echo __("Female");?>
</option>
                                                                <option value="male"><?php echo __("Male");?>
</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="control-group">
                                                    <div class="controls">
                                                        <input type="password" id="password" name="password"
                                                               placeholder='<?php echo __("Password");?>
' class="form-control"
                                                               required/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <?php if ($_smarty_tpl->tpl_vars['system']->value['reCAPTCHA_enabled']) {?>
                                            <div id="recaptcha1"></div>
                                        <?php }?>

                                        <div class="dieukhoan">
                                            <p><?php echo __("Sign Up, you agree to our");?>
 <a
                                                        href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/terms"><?php echo __("Terms");?>
</a></p>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="control-group  mar0">
                                                    <div class="controls-ci">
                                                        <button type="submit" class="btn btn-success"
                                                                style="width: 100%"><?php echo __("Sign Up");?>

                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- error -->
                                        <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                                        <!-- error -->
                                    </form>

                                    <div class="mt20 text-center"><?php echo __("Have an account?");?>

                                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/signin"
                                           class="js_toggle-panel text-link"><?php echo __("Login Now");?>
</a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="home_background">
        <div class="container">
            <div class="home_intro">
                <div class="intro_title" align="center">
                    
                    <h2><?php echo __("Báo chí nói về Coniu");?>
</h2>
                </div>
                <div class="intro_content">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="news_box">
                                <div class="news_img_box">
                                    <img class="img-responsive news_img"
                                         src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/content/themes/<?php echo $_smarty_tpl->tpl_vars['system']->value['theme'];?>
/images/home_new/bao1.png"
                                         alt="Báo chí nói về Phần mềm mầm non Coniu">
                                    <div class="img_mark"></div>
                                    <div class="news_name"><p>Báo Giáo dục</p></div>
                                    <div class="news_name_add"></div>
                                </div>
                                <div class="news_content_box">
                                    
                                    
                                    
                                    <div class="news_title">
                                        <a href="http://giaoduc.net.vn/Giao-duc-24h/Ap-dung-cong-nghe-40-trong-day-hoc-mam-non-post186456.gd"
                                           target="_blank" title="Coniu - Áp dụng công nghệ 4.0 trong dạy học mầm non">Áp
                                            dụng công nghệ 4.0 trong dạy học mầm non</a>
                                    </div>
                                    <div class="news_shortdes">
                                        <p>(GDVN) - Đã có phần mềm mà phụ huynh đăng ký dịch vụ, gửi thuốc, xin nghỉ,
                                            đăng ký ngoại khóa, theo dõi học phí, xem camera lớp con mà không cần cài
                                            đặt gì thêm... <a target="_blank"
                                                              title="Coniu - Áp dụng công nghệ 4.0 trong dạy học mầm non"
                                                              href="http://giaoduc.net.vn/Giao-duc-24h/Ap-dung-cong-nghe-40-trong-day-hoc-mam-non-post186456.gd">Xem
                                                thêm</a></p>
                                    </div>
                                    
                                    
                                    
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="news_box">
                                <div class="news_img_box">
                                    <img class="img-responsive news_img"
                                         src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/content/themes/<?php echo $_smarty_tpl->tpl_vars['system']->value['theme'];?>
/images/home_new/bao2.png"
                                         alt="Báo chí nói về Phần mềm mầm non Coniu">
                                    <div class="img_mark"></div>
                                    <div class="news_name"><p>Báo KH&CN</p></div>
                                    <div class="news_name_add"></div>
                                </div>
                                <div class="news_content_box">
                                    
                                    
                                    
                                    <div class="news_title">
                                        <a href="http://khoahocvacongnghevietnam.com.vn/khcn-trung-uong/19714-quan-ly-va-ket-noi-mam-non-voi-coniu.html"
                                           target="_blank" title="Quản lý và kết nối mầm non với Coniu">Quản lý và kết
                                            nối mầm non với Coniu</a>
                                    </div>
                                    <div class="news_shortdes">
                                        <p>Từ nhu cầu thực tiễn, với sự hỗ trợ của Trung tâm Hỗ trợ khởi nghiệp SCSI,
                                            phần mềm mầm non - Coniu ra đời nhằm hỗ trợ công tác quản lý trường mầm non
                                            và kết nối gia đình - nhà trường… <a target="_blank"
                                                                                 title="Quản lý và kết nối mầm non với Coniu"
                                                                                 href="http://khoahocvacongnghevietnam.com.vn/khcn-trung-uong/19714-quan-ly-va-ket-noi-mam-non-voi-coniu.html">Xem
                                                thêm</a></p>
                                    </div>
                                    
                                    
                                    
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="news_box">
                                <div class="news_img_box">
                                    <iframe width="100%" height="210px"
                                            src="https://www.youtube.com/embed/UeX_NTtV5lU?rel=0&amp;controls=1"
                                            frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                </div>
                                <div class="news_content_box">
                                    
                                    
                                    
                                    <div class="news_title">
                                        <a href="https://youtu.be/UeX_NTtV5lU" target="_blank">Cà phê khởi nghiệp -
                                            VTV1</a>
                                    </div>
                                    <div class="news_shortdes">
                                        <p>Cà phê khởi nghiệp ghi nhận Coniu là ứng dụng đem lại nhiều tiện ích trong
                                            quản lý giáo dục mầm non, kết nối nhà trường - phụ huynh và hỗ trợ phụ huynh
                                            trong việc nuôi dạy trẻ...</p>
                                    </div>
                                    
                                    
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                
                
                <div class="content_trial" align="center" style="margin-top: 30px">
                    <a href="#" class="btn btn-success js_free-trial"><?php echo mb_strtoupper(__("FREE register for your school"), 'UTF-8');?>
</a>
                </div>
            </div>
        </div>
    </div>
    <div class="home_background_none">
        <div class="container">
            <div class="home_intro">
                <div class="intro_title" align="center">
                    <h2><?php echo __("Inet - super connection between parents - school - community");?>
</h2>
                </div>
                <div class="home_around">
                    <img class="img-responsive img_around"
                         src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/content/themes/<?php echo $_smarty_tpl->tpl_vars['system']->value['theme'];?>
/images/home_new/around.png"
                         alt="Phần mềm mầm non Coniu hỗ trợ siêu kết nối">
                    <div class="around_parent" align="right">
                        <div class="parent_title">
                            <h4><?php echo __("Parent");?>
</h4>
                        </div>
                        <div class="parent_content">
                            <?php echo __("Easily track your student at school anytime, anywhere. Connect with the school to better raise student");?>

                            .
                        </div>
                    </div>
                    <div class="around_chef" align="left">
                        <div class="parent_title">
                            <h4><?php echo __("Chef");?>
</h4>
                        </div>
                        <div class="parent_content">
                            <?php echo __("It is easy to see daily school meals on the Inet application");?>
.
                        </div>
                    </div>
                    <div class="around_accountant" align="left">
                        <div class="parent_title">
                            <h4><?php echo __("Accountant");?>
</h4>
                        </div>
                        <div class="parent_content">
                            <?php echo __("The Inet system automatically calculates meal fee, tuition based on attendance information and service registration");?>

                            .
                        </div>
                    </div>
                    <div class="around_principal" align="center">
                        <div class="parent_title">
                            <h4><?php echo __("Administrators");?>
</h4>
                        </div>
                        <div class="parent_content">
                            <?php echo __("Know all the management information and daily activities of the school every time, everywhere on mobile");?>

                            .
                        </div>
                    </div>
                    <div class="around_teacher" align="right">
                        <div class="parent_title">
                            <h4><?php echo __("Teacher");?>
</h4>
                        </div>
                        <div class="parent_content">
                            <?php echo __("Communicate easily and transparently with parents and other departments in the school");?>

                            .
                        </div>
                    </div>
                </div>
                <div class="content_trial" align="center">
                    <a href="#" class="btn btn-success js_free-trial"><?php echo mb_strtoupper(__("FREE register for your school"), 'UTF-8');?>
</a>
                </div>
            </div>
        </div>
    </div>
    <div class="home_background">
        <div class="container">
            <div class="home_intro">
                <div class="intro_title" align="center" style="padding-bottom: 0">
                    <h2><?php echo __("Customers talk about MobiEdu");?>
</h2>
                </div>
                <div class="intro_des" align="center">
                    <h4><?php echo __("Customer satisfaction is our greatest success");?>
!</h4>
                </div>
                <div class="intro_content">
                    <div class="row">
                        <div class="col-sm-6">
                            <div id="myCarousel" class="carousel slide" data-ride="carousel">

                                <!-- Wrapper for slides -->
                                <div class="carousel-inner" role="listbox">

                                    <div class="item active">
                                        <div class="customer">
                                            <div class="customer_intro">
                                                <div class="row">
                                                    <div class="col-xs-4" align="center" style="padding-right: 7.5px">
                                                        <div class="customer_img_box">
                                                            <img class="img-responsive customer_img"
                                                                 src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/content/themes/<?php echo $_smarty_tpl->tpl_vars['system']->value['theme'];?>
/images/home_new/customer.jpg"
                                                                 alt="Khách hàng nói về Phần mềm mầm non Coniu">
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-8" style="padding-left: 7.5px">
                                                        <div class="customer_content_box">
                                                            <p class="customer_name">Nguyễn Thanh Tuấn</p>
                                                            <p class="customer_position">
                                                                Chủ tịch HĐQT
                                                            </p>
                                                            <a class="customer_link" href="#">
                                                                Mầm non Việt Úc
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="customer_text_box">
                                                <p class="customer_text">
                                                    Nhờ Coniu, tôi đã có thể quản lý trường mọi lúc, mọi nơi trên điện
                                                    thoại. Tôi nhận được tất cả phản hồi, góp ý của phụ huynh để có điều
                                                    chỉnh và cải tiến kịp thời. Chúc Coniu ngày càng phát triển!
                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="item">
                                        <div class="customer">
                                            <div class="customer_intro">
                                                <div class="row">
                                                    <div class="col-xs-4" align="center" style="padding-right: 7.5px">
                                                        <div class="customer_img_box">
                                                            <img class="img-responsive customer_img"
                                                                 src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/content/themes/<?php echo $_smarty_tpl->tpl_vars['system']->value['theme'];?>
/images/home_new/customer4.jpg"
                                                                 alt="Khách hàng nói về Phần mềm mầm non Coniu">
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-8" style="padding-left: 7.5px">
                                                        <div class="customer_content_box">
                                                            <p class="customer_name">Vũ Thị Hiền</p>
                                                            <p class="customer_position">
                                                                Hiệu trưởng
                                                            </p>
                                                            <a class="customer_link" href="#">
                                                                Mầm non Kids Garden
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="customer_text_box">
                                                <p class="customer_text">
                                                    Coniu giúp chúng tôi thay đổi phương pháp quản lý nhà trường, tiết
                                                    kiệm và hiệu quả hơn rất nhiều. Các bộ phận đã không phải trao đổi
                                                    thủ công như trước!
                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="item">
                                        <div class="customer">
                                            <div class="customer_intro">
                                                <div class="row">
                                                    <div class="col-xs-4" align="center" style="padding-right: 7.5px">
                                                        <div class="customer_img_box">
                                                            <img class="img-responsive customer_img"
                                                                 src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/content/themes/<?php echo $_smarty_tpl->tpl_vars['system']->value['theme'];?>
/images/home_new/customer3.jpg"
                                                                 alt="Khách hàng nói về Phần mềm mầm non Coniu">
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-8" style="padding-left: 7.5px">
                                                        <div class="customer_content_box">
                                                            <p class="customer_name">Nguyễn Thanh Huyền</p>
                                                            <p class="customer_position">
                                                                Nhân viên Văn phòng
                                                            </p>
                                                            <a class="customer_link" href="#">
                                                                Đống Đa, Hà Nội
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="customer_text_box">
                                                <p class="customer_text">
                                                    Coniu cho tôi cảm giác yên tâm khi đưa con đến trường, lúc nào tôi
                                                    cũng biết được thông tin con ở trường và trao đổi với giáo viên rất
                                                    tiện lợi!
                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <!-- Left and right controls -->
                                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="phone_img_box">
                                <img class="phone_img img-responsive"
                                     src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/content/themes/<?php echo $_smarty_tpl->tpl_vars['system']->value['theme'];?>
/images/home_new/iphonex.png"
                                     alt="Phần mềm mầm non Coniu">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            
            
        </div>
    </div>
    <div class="home_background_none">
        <div class="container">
            <div class="home_intro">
                <div class="intro_title" align="center">
                    <h2><?php echo __("MobiEdu - Proudly the school application that parents across the country believe in");?>
</h2>
                </div>
                <div class="intro_content">
                    <div class="row intro_content_row">
                        <div class="intro_content_in">
                            <div class="col-sm-4 content_col">
                                <div class="content_pad">
                                    <div class="content_icon" align="center">
                                        <div class="content-radius">
                                            <img class="content_img img-responsive"
                                                 src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/content/themes/<?php echo $_smarty_tpl->tpl_vars['system']->value['theme'];?>
/images/home_new/parent.png"
                                                 alt="Lợi ích của phần mềm mầm non Coniu với phụ huynh">
                                        </div>
                                    </div>
                                    <div class="content_header" align="center">
                                        <h3 class="content_header_text"><?php echo __("Benefits to parents");?>
</h3>
                                    </div>
                                    <div class="content_text">
                                        <ul>
                                            <li><?php echo __("Chia sẻ, học hỏi kinh nghiệm nuôi dạy trẻ với cộng đồng các mẹ thông thái");?>

                                                .
                                            </li>
                                            <li><?php echo __("Tham khảo ý kiến, tư vấn chuyên gia về các vấn đề thắc mắc");?>
.</li>
                                            <li><?php echo __("Sử dụng sổ y bạ điện tử: biểu đồ chiều cao, cân nặng; tiểu sử bệnh và lịch sử uống thuốc…");?>
</li>
                                            <li><?php echo __("Tìm trường phù hợp cho con đơn giản thông qua chức năng 'Tìm trường quanh đây'");?>

                                                .
                                            </li>
                                            <li><?php echo __("Tìm kiếm video, truyện cổ tích, truyện ngụ ngôn cho con phù hợp");?>

                                                .
                                            </li>
                                            <li><?php echo __("Cập nhật, trao đổi với giáo viên về hoạt động vui chơi, sinh hoạt, học tập của trẻ");?>

                                                .
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4 content_col">
                                <div class="content_pad">
                                    <div class="content_icon" align="center">
                                        <div class="content-radius">
                                            <img class="content_img img-responsive"
                                                 src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/content/themes/<?php echo $_smarty_tpl->tpl_vars['system']->value['theme'];?>
/images/home_new/teacher.png"
                                                 alt="Lợi ích của phần mềm mầm non Coniu với Giáo viên">
                                        </div>
                                    </div>
                                    <div class="content_header" align="center">
                                        <h3 class="content_header_text"><?php echo __("Benefits to teacher");?>
</h3>
                                    </div>
                                    <div class="content_text">
                                        <ul>
                                            <li><?php echo __("Chia sẻ, học hỏi kinh nghiệm mầm non với cộng đồng giáo viên, chuyên gia");?>

                                                .
                                            </li>
                                            <li><?php echo __("Trao đổi, chia sẻ với phụ huynh nhanh chóng, trực tiếp trên điện thoại");?>

                                                .
                                            </li>
                                            <li><?php echo __("Quản lý lớp học dễ dàng: thông báo, đăng ký sự kiện, điểm danh, đón muộn, nhận xét về trẻ");?>

                                                .
                                            </li>
                                            <li><?php echo __("Thông tin minh bạch, tránh hiểu lầm giữa phụ huynh – giáo viên");?>

                                                .
                                            </li>
                                            <li><?php echo __("Giảm thiểu công việc giấy tờ và trao đổi với bộ phận khác (kế toán, nhà bếp…)");?>

                                                .
                                            </li>
                                            <li><?php echo __("Giảm thiểu tối đa thao tác nhờ các biểu mẫu và tính toán tự động");?>

                                                .
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4 content_col">
                                <div class="content_pad">
                                    <div class="content_icon" align="center">
                                        <div class="content-radius">
                                            <img class="content_img img-responsive"
                                                 src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/content/themes/<?php echo $_smarty_tpl->tpl_vars['system']->value['theme'];?>
/images/home_new/school.png"
                                                 alt="Lợi ích của phần mềm mầm non Coniu với nhà trường">
                                        </div>
                                    </div>
                                    <div class="content_header" align="center">
                                        <h3 class="content_header_text"><?php echo __("Benefits to school");?>
</h3>
                                    </div>
                                    <div class="content_text">
                                        <ul>
                                            <li><?php echo __("Không phải đầu tư ban đầu (chỉ cần đăng ký sử dụng).");?>
</li>
                                            <li><?php echo __("Dễ dàng quảng bá hình ảnh nhà trường tới cộng đồng, hỗ trợ công tác tuyển sinh");?>

                                                .
                                            </li>
                                            <li><?php echo __("Quản lý mọi thông tin hàng ngày của trường trên điện thoại, thống kê nhanh chóng.");?>
</li>
                                            <li><?php echo __("Kết nối liên thông giữa các bộ phận trong trường (giáo viên, kế toán, nhà bếp…).");?>
</li>
                                            <li><?php echo __("Chia sẻ, trao đổi và thông báo tới giáo viên, phụ huynh nhanh chóng");?>

                                                .
                                            </li>
                                            <li><?php echo __("Kịp thời nắm bắt ý kiến phụ huynh và có điều chỉnh phù hợp");?>
.</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content_trial" align="center">
                    <a href="#" data-toggle="tab"
                       class="btn btn-success js_free-trial"><?php echo mb_strtoupper(__("FREE register for your school"), 'UTF-8');?>
</a>
                </div>
            </div>
        </div>
    </div>
    <div class="home_background economi_back"
         style="background: url('<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/content/themes/<?php echo $_smarty_tpl->tpl_vars['system']->value['theme'];?>
/images/home_new/background_economi.jpg')">
        <div class="container">
            <div class="home_intro">
                <div class="intro_title" align="center" style="padding-bottom: 0">
                    <h2><?php echo __("150+ schools have chosen Inet for better management");?>
</h2>
                </div>
                <div class="intro_des" align="center">
                    <h4><?php echo __("Inet is a School social network that connects wise parents. In addition, Inet is an intelligent School management system and supports parent-school connectivity. Register for your school, please contact the information below");?>

                        :</h4>
                </div>
                <div class="intro_content">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="economi_box" align="center">
                                <div class="economi_icon">
                                    <img class="img-responsive"
                                         src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/content/themes/<?php echo $_smarty_tpl->tpl_vars['system']->value['theme'];?>
/images/home_new/support.png"
                                         alt="Liên hệ sử dụng Phần mềm mầm non Coniu">
                                </div>
                                <div class="economi_title">
                                    <p><?php echo __("SUPPORT 24/7");?>
</p>
                                </div>
                                <div class="economi_short">
                                    <p><?php echo __("All days of the week (from Monday to Sunday) | Day and night");?>
.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="economi_box" align="center">
                                <div class="economi_icon">
                                    <img class="img-responsive"
                                         src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/content/themes/<?php echo $_smarty_tpl->tpl_vars['system']->value['theme'];?>
/images/home_new/contact.png"
                                         alt="Liên hệ sử dụng Phần mềm mầm non Coniu">
                                </div>
                                <div class="economi_title">
                                    <p><?php echo __("HOTLINE");?>
: <a href="tel:0966 900 466">0966 900 466</a></p>
                                </div>
                                <div class="economi_short">
                                    <p><?php echo __("Free consultation center for Inet customers");?>
.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="economi_box" align="center">
                                <div class="economi_icon">
                                    <img class="img-responsive"
                                         src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/content/themes/<?php echo $_smarty_tpl->tpl_vars['system']->value['theme'];?>
/images/home_new/email.png"
                                         alt="Liên hệ sử dụng Phần mềm mầm non Coniu">
                                </div>
                                <div class="economi_title">
                                    <p><?php echo __("EMAIL");?>
: <a href="mailto:lienhe@coniu.vn">lienhe@coniu.vn</a></p>
                                </div>
                                <div class="economi_short">
                                    <p><?php echo __("We respond to your email as quickly as possible");?>
.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                
                
            </div>
        </div>
    </div>
    <div class="home_background_black">
        <div class="container">
            <div class="home_intro info_home" style="padding-bottom: 30px; padding-top: 30px">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="footer_box">
                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
">
                                <div class="footer_logo_box">
                                    <div class="footer_logo">
                                        <img class="img-responsive"
                                             src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/content/themes/<?php echo $_smarty_tpl->tpl_vars['system']->value['theme'];?>
/images/logo.png"
                                             alt="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_title'];?>
 | <?php echo __("School management and connection software");?>
" title="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_title'];?>
 | <?php echo __("School management and connection software");?>
">
                                    </div>
                                    <div class="footer_slogan">
                                        <p><?php echo __("Connect love");?>
</p>
                                    </div>
                                </div>
                            </a>
                            <div class="footer_address">
                                <ul>
                                    <li><a href="https://www.noga.vn" target="_blank"
                                           title="<?php echo __("NOGA Joint Stock Company");?>
"><?php echo mb_strtoupper(__("NOGA Joint Stock Company"), 'UTF-8');?>
</a>
                                        <br/>- <?php echo __("Tax Identification Number");?>
: 0107455916, ngày cấp: 01/06/2016, nơi
                                        cấp: Sở KHĐT HN.
                                        <br/>- Website: <a href="https://www.noga.vn" target="_blank"
                                                           title="<?php echo __("NOGA Joint Stock Company");?>
">https://www.noga.vn</a>
                                        <br/>- <?php echo __("Address");?>

                                        : <?php echo __("No. 32-241, Chien Thang Street, Ha Dong District, Hanoi.");?>

                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-5">
                        <div class="footer_box">
                            <div class="row">
                                <div class="col-sm-6" style="padding-right: 5px">
                                    <div class="menu_box" align="left">
                                        <div class="menu_header">
                                            <p><?php echo __("Parent");?>
</p>
                                        </div>
                                        <div class="menu_content">
                                            <ul>
                                                <li><p><a target="_blank"
                                                          href="https://blog.coniu.vn/inet-la-gi/"><?php echo __("What is Inet?");?>
</a>
                                                    </p></li>
                                                <li><p><a target="_blank"
                                                          href="https://blog.coniu.vn/inet-la-gi/#faq_phuhuynh"><?php echo __("Sử dụng Coniu thế nào?");?>
</a>
                                                    </p></li>
                                                <li><p><a target="_blank"
                                                          href="https://blog.coniu.vn/inet-la-gi/#faq_phuhuynh"><?php echo __("Người dùng thắc mắc");?>
</a>
                                                    </p></li>
                                                <li><p><a target="_blank" href="#"><?php echo __("Khi con đi học");?>
</a></p></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6" style="padding-left: 5px">
                                    <div class="menu_box" align="left">
                                        <div class="menu_header">
                                            <p><?php echo __("School");?>
</p>
                                        </div>
                                        <div class="menu_content">
                                            <ul>
                                                <li><p><a target="_blank"
                                                          href="https://blog.coniu.vn/inet-la-gi/"><?php echo __("Inet hỗ trợ trường những gì?");?>
</a>
                                                    </p></li>
                                                <li><p><a target="_blank"
                                                          href="https://blog.coniu.vn/inet-la-gi/"><?php echo __("Đặc điểm nổi bật");?>
</a>
                                                    </p></li>
                                                <li><p><a target="_blank"
                                                          href="https://blog.coniu.vn/inet-la-gi/#faq_nhatruong"><?php echo __("FAQ");?>
</a>
                                                    </p></li>
                                                <li><p><a target="_blank"
                                                          href="https://blog.coniu.vn/category/tin-mam-non/huong-dan-su-dung-ung-dung-inet/tai-khoan-quan-ly/"><?php echo __("Usage guide");?>
</a>
                                                    </p></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="footer_box">
                            <div class="menu_box" align="left">
                                <div class="menu_header">
                                    <p><?php echo __("Cooperation");?>
</p>
                                </div>
                                <div class="menu_content">
                                    <ul>
                                        <li><p><a target="_blank" href="#"><?php echo __("Partner progrmam");?>
</a></p></li>
                                        <li><p><?php echo __("Telephone");?>
: <strong><a href="tel:0966 900 466">0966 900
                                                        466</a></strong></p></li>
                                        <li><p>Email: <a href="mailto:lienhe@coniu.vn"><?php echo $_smarty_tpl->tpl_vars['system']->value['mail_contact'];?>
</a></p>
                                        </li>
                                        <li><p>Facebook: <a href="https://facebook.com/inet.vn" target="_blank">fb.com/inet.vn</a>
                                            </p></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end liên hệ taila -->
<?php $_smarty_tpl->_subTemplateRender('file:_footer_new.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php }
}
