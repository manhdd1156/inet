<?php
/* Smarty version 3.1.31, created on 2021-05-10 17:16:59
  from "D:\workplace\mascom-edu-server\Server11\content\themes\inet\templates\ci\noga\ajax.noga.user.list.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_6099081b052362_54563316',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c212dce2eb412e6a01643be3fa32a01678671072' => 
    array (
      0 => 'D:\\workplace\\mascom-edu-server\\Server11\\content\\themes\\inet\\templates\\ci\\noga\\ajax.noga.user.list.tpl',
      1 => 1619677394,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6099081b052362_54563316 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_modifier_date_format')) require_once 'D:\\workplace\\mascom-edu-server\\Server11\\includes\\libs\\Smarty\\plugins\\modifier.date_format.php';
?>
<strong><?php echo __("Children list");?>
&nbsp;(<?php echo $_smarty_tpl->tpl_vars['result']->value['total'];?>
 <?php echo __("Children");?>
)</strong>
<div class="pull-right flip mb10">
    <label id="export_processing" class="btn btn-info x-hidden"><?php echo __("Loading");?>
...</label>
</div>
<table class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th>#</th>
        <th><?php echo __("ID");?>
</th>
        <th><?php echo __("Picture");?>
</th>
        <th><?php echo __("Username");?>
</th>
        <th><?php echo __("Name");?>
</th>
        <th><?php echo __("IP");?>
</th>
        <th><?php echo __("Joined");?>
</th>
        <th><?php echo __("Activated");?>
</th>
        <th><?php echo __("Actions");?>
</th>
    </tr>
    </thead>
    <tbody>
    <?php $_smarty_tpl->_assignInScope('idx', ($_smarty_tpl->tpl_vars['result']->value['page']-1)*@constant('PAGING_LIMIT')+1);
?>
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['result']->value['users'], 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
        <tr>
            <td class="align-middle" align="center"><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</td>
            <td><a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['row']->value['user_name'];?>
" target="_blank"><?php echo $_smarty_tpl->tpl_vars['row']->value['user_id'];?>
</a></td>
            <td>
                <a target="_blank" class="x-image sm" href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['row']->value['user_name'];?>
" style="background-image:url(<?php echo $_smarty_tpl->tpl_vars['row']->value['user_picture'];?>
);">
                </a>
            </td>
            <td>
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['row']->value['user_name'];?>
" target="_blank">
                    <?php echo $_smarty_tpl->tpl_vars['row']->value['user_name'];?>

                </a>
            </td>
            <td>
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['row']->value['user_name'];?>
" target="_blank">
                    <?php echo $_smarty_tpl->tpl_vars['row']->value['user_fullname'];?>

                </a>
            </td>
            <td><?php echo $_smarty_tpl->tpl_vars['row']->value['user_ip'];?>
</td>
            <td><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['row']->value['user_registered'],"%e %B %Y");?>
</td>
            <td>
                <?php if ($_smarty_tpl->tpl_vars['row']->value['user_activated']) {?>
                    <span class="label label-success"><?php echo __("Yes");?>
</span>
                <?php } else { ?>
                    <span class="label label-danger"><?php echo __("No");?>
</span>
                <?php }?>
            </td>
            <td>
                <button class="btn btn-xs btn-danger js_admin-deleter" data-handle="user" data-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['user_id'];?>
">
                    <i class="fas fa-trash"></i>
                </button>
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/noga/users/edit/<?php echo $_smarty_tpl->tpl_vars['row']->value['user_id'];?>
" class="btn btn-xs btn-primary">
                    <i class="fa fa-pencil-alt"></i>
                </a>
            </td>
        </tr>
        <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>


    <?php if (count($_smarty_tpl->tpl_vars['result']->value['users']) == 0) {?>
        <tr class="odd">
            <td valign="top" align="center" colspan="9" class="dataTables_empty">
                <?php echo __("No data available in table");?>

            </td>
        </tr>
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['result']->value['page_count'] > 1) {?>
        <tr>
            <td colspan="9">
                <div class="pull-right flip">
                    <ul class="pagination">
                        <?php
$_smarty_tpl->tpl_vars['idx'] = new Smarty_Variable(null, $_smarty_tpl->isRenderingCache);$_smarty_tpl->tpl_vars['idx']->step = 1;$_smarty_tpl->tpl_vars['idx']->total = (int) ceil(($_smarty_tpl->tpl_vars['idx']->step > 0 ? $_smarty_tpl->tpl_vars['result']->value['page_count']+1 - (1) : 1-($_smarty_tpl->tpl_vars['result']->value['page_count'])+1)/abs($_smarty_tpl->tpl_vars['idx']->step));
if ($_smarty_tpl->tpl_vars['idx']->total > 0) {
for ($_smarty_tpl->tpl_vars['idx']->value = 1, $_smarty_tpl->tpl_vars['idx']->iteration = 1;$_smarty_tpl->tpl_vars['idx']->iteration <= $_smarty_tpl->tpl_vars['idx']->total;$_smarty_tpl->tpl_vars['idx']->value += $_smarty_tpl->tpl_vars['idx']->step, $_smarty_tpl->tpl_vars['idx']->iteration++) {
$_smarty_tpl->tpl_vars['idx']->first = $_smarty_tpl->tpl_vars['idx']->iteration == 1;$_smarty_tpl->tpl_vars['idx']->last = $_smarty_tpl->tpl_vars['idx']->iteration == $_smarty_tpl->tpl_vars['idx']->total;?>
                            <?php if ($_smarty_tpl->tpl_vars['idx']->value == $_smarty_tpl->tpl_vars['result']->value['page']) {?>
                                <li class="active"><a href="#"><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</a></li>
                            <?php } else { ?>
                                <li>
                                    <a href="#" class="js_user-search" data-page="<?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</a>
                                </li>
                            <?php }?>
                        <?php }
}
?>

                    </ul>
                </div>
            </td>
        </tr>
    <?php }?>
    </tbody>
</table>
<?php }
}
