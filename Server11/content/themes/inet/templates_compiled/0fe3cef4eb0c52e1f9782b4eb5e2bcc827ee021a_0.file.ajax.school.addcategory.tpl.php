<?php
/* Smarty version 3.1.31, created on 2021-03-31 10:17:43
  from "D:\workplace\Server11\content\themes\inet\templates\ci\school\ajax.school.addcategory.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_6063e9d7c90979_74377042',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '0fe3cef4eb0c52e1f9782b4eb5e2bcc827ee021a' => 
    array (
      0 => 'D:\\workplace\\Server11\\content\\themes\\inet\\templates\\ci\\school\\ajax.school.addcategory.tpl',
      1 => 1552404702,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6063e9d7c90979_74377042 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="mt10" align="center">
    <strong><?php echo __("Add new category and add suggest for category");?>
</strong>
</div>
<div class="panel-body with-table">
    <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_report.php">
        <input type="hidden" id="school_username" name="school_username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
        <input type="hidden" name="do" value="add_cate_temp"/>
        <input type="hidden" name="report_template_id" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['report_template_id'];?>
"/>
        <div class = "form-group">
            <label class="col-sm-3 control-label text-left"><?php echo __("Category name");?>
 (*)</label>
            <div class="col-sm-9">
                <input class="form-control" name="category_name_add" required maxlength="300">
            </div>
        </div>
        <div class="table-responsive" id="cate_school_add">
            <table class="table table-striped table-bordered table-hover" id = "addCategoryTable">
                <thead>
                <tr><th colspan="3"><?php echo __("Suggest content for category");?>
</th></tr>
                <tr>
                    <th>
                        <?php echo __('No.');?>

                    </th>
                    <th>
                        <?php echo __('Title');?>

                    </th>
                    <th>
                        <?php echo __('Actions');?>

                    </th>
                </tr>
                </thead>
                <tbody class="table_suggest">
                <tr>
                    <td class = "col_no align_middle"  align = "center">1</td>
                    <td>
                        <input type="text" name = "suggests_add" required class = "form-control suggest" placeholder="<?php echo __("Suggest content for category");?>
">
                    </td>
                    <td align="center" class="align_middle"> <a class="btn btn-danger btn-xs js_report_template-delete"> <?php echo __("Delete");?>
 </a></td>
                </tr>
                </tbody>
            </table>
            <a class="btn btn-default js_report_suggest-add"><?php echo __("Add new row");?>
</a>
            <a class="btn btn-default js_school-category-add-done"><?php echo __("Add");?>
</a>
        </div>
        <!-- success -->
        <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
        <!-- success -->

        <!-- error -->
        <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
        <!-- error -->
    </form>
</div><?php }
}
