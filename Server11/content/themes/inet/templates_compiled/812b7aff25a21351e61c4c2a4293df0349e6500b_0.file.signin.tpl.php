<?php
/* Smarty version 3.1.31, created on 2021-04-27 08:40:07
  from "D:\workplace\mascom-edu-server\Server11\content\themes\inet\templates\signin.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_60876b77c9fe10_64370297',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '812b7aff25a21351e61c4c2a4293df0349e6500b' => 
    array (
      0 => 'D:\\workplace\\mascom-edu-server\\Server11\\content\\themes\\inet\\templates\\signin.tpl',
      1 => 1552404706,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:_head.tpl' => 1,
    'file:_header.tpl' => 1,
    'file:_footer.tpl' => 1,
  ),
),false)) {
function content_60876b77c9fe10_64370297 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:_head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php $_smarty_tpl->_subTemplateRender('file:_header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<!-- page content -->
<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <h3 class="mb20 text-center"><?php echo __("Login");?>
</h3>

            <?php if ($_smarty_tpl->tpl_vars['highlight']->value) {?>
            <div class="alert alert-warning" role="alert"><?php echo $_smarty_tpl->tpl_vars['highlight']->value;?>
</div>
            <?php }?>
            
            <?php if ($_smarty_tpl->tpl_vars['system']->value['social_login_enabled']) {?>
                <?php if ($_smarty_tpl->tpl_vars['system']->value['facebook_login_enabled'] || $_smarty_tpl->tpl_vars['system']->value['twitter_login_enabled'] || $_smarty_tpl->tpl_vars['system']->value['google_login_enabled'] || $_smarty_tpl->tpl_vars['system']->value['linkedin_login_enabled'] || $_smarty_tpl->tpl_vars['system']->value['vkontakte_login_enabled']) {?>
                    <div class="mb20">
                        <?php if ($_smarty_tpl->tpl_vars['system']->value['facebook_login_enabled']) {?>
                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/connect/facebook" class="btn btn-block btn-social btn-facebook">
                            <i class="fab fa-facebook-f"></i> <?php echo __("Login with");?>
 <?php echo __("Facebook");?>

                        </a>
                        <?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['system']->value['twitter_login_enabled']) {?>
                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/connect/twitter" class="btn btn-block btn-social btn-twitter">
                            <i class="fab fa-twitter"></i> <?php echo __("Login with");?>
 <?php echo __("Twitter");?>

                        </a>
                        <?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['system']->value['google_login_enabled']) {?>
                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/connect/google" class="btn btn-block btn-social btn-google">
                            <i class="fab fa-google"></i> <?php echo __("Login with");?>
 <?php echo __("Google");?>

                        </a>
                        <?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['system']->value['linkedin_login_enabled']) {?>
                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/connect/linkedin" class="btn btn-block btn-social btn-linkedin">
                            <i class="fa fa-linkedin"></i> <?php echo __("Login with");?>
 <?php echo __("Linkedin");?>

                        </a>
                        <?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['system']->value['vkontakte_login_enabled']) {?>
                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/connect/vkontakte" class="btn btn-block btn-social btn-vk">
                            <i class="fa fa-vk"></i> <?php echo __("Login with");?>
 <?php echo __("Vkontakte");?>

                        </a>
                        <?php }?>
                    </div>
                    <div class="hr-heading mb10">
                        <div class="hr-heading-text">
                            <?php echo __("or");?>

                        </div>
                    </div>
                <?php }?>
            <?php }?>

            <form class="js_ajax-forms" data-url="core/signin.php">
                <div class="form-group">
                    <label for="username_email"><?php echo __("Email");?>
 <?php echo __("or");?>
 <?php echo __("Username");?>
</label>
                    <input name="username_email" id="username_email" value="<?php echo $_smarty_tpl->tpl_vars['username_email']->value;?>
" type="text" class="form-control" required autofocus>
                </div>
                <div class="form-group">
                    <label for="password"><?php echo __("Password");?>
</label>
                    <input name="password" id="password" type="password" class="form-control" required>
                </div>
                <div class="checkbox clearfix">
                    <label>
                        <input name="remember" type="checkbox"> <?php echo __("Remember me");?>

                    </label>
                </div>
                <button name="submit" type="submit" class="btn btn-lg btn-primary btn-block"><?php echo __("Login");?>
</button>

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
            
            <div class="mt20">
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/reset"><?php echo __("Forget your password?");?>
</a>
            </div>

        </div>
    </div>
</div>
<!-- page content -->

<?php $_smarty_tpl->_subTemplateRender('file:_footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
