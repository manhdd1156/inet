<?php
/* Smarty version 3.1.31, created on 2021-06-02 11:16:02
  from "D:\workplace\Inet-project\source\Server11\content\themes\inet\templates\ci\school\school.services.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_60b706021d63e6_41071747',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '7efa7e88c560c1c1e46b8046ac38f10b48ee9035' => 
    array (
      0 => 'D:\\workplace\\Inet-project\\source\\Server11\\content\\themes\\inet\\templates\\ci\\school\\school.services.tpl',
      1 => 1621843777,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60b706021d63e6_41071747 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <div class="pull-right flip">
            <?php if ($_smarty_tpl->tpl_vars['sub_view']->value != '') {?>
                <?php if ($_smarty_tpl->tpl_vars['school']->value['school_step'] == SCHOOL_STEP_FINISH || $_smarty_tpl->tpl_vars['school']->value['grade'] != 0) {?>
                    <a href="https://blog.coniu.vn/huong-dan-quan-ly-tao-dang-ky-dich-vu-tren-website-inet/" target="_blank" class="btn btn-info  btn_guide">
                        <i class="fa fa-info"></i> <?php echo __("Guide");?>

                    </a>
                <?php }?>
            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
                <?php if ($_smarty_tpl->tpl_vars['canEdit']->value) {?>
                    <?php if ($_smarty_tpl->tpl_vars['school']->value['school_step'] == SCHOOL_STEP_FINISH || $_smarty_tpl->tpl_vars['school']->value['grade'] != 0) {?>
                        <a href="https://blog.coniu.vn/huong-dan-them-moi-mot-loai-dich-vu/" target="_blank" class="btn btn-info btn_guide">
                            <i class="fa fa-info"></i> <?php echo __("Add new a service guide");?>

                        </a>
                    <?php }?>
                <?php }?>
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/services/add" class="btn btn-default">
                    <i class="fa fa-plus"></i> <?php echo __("Add New");?>

                </a>
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/services/inactive" class="btn btn-warning">
                    <i class="fa fa-list"></i> <?php echo __("Inactive service list");?>

                </a>
            <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "inactive") {?>
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/services/" class="btn btn-default">
                    <i class="fa fa-list"></i> <?php echo __("Service list");?>

                </a>
            <?php }?>
        </div>
        <i class="fa fa-life-ring fa-lg fa-fw pr10"></i>
        <?php echo __("Service");?>

        <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == "edit") {?>
            &rsaquo; <?php echo $_smarty_tpl->tpl_vars['data']->value['service_name'];?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
            &rsaquo; <?php echo __('Add New');?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "inactive") {?>
            &rsaquo; <?php echo __('Inactive service list');?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
            &rsaquo; <?php echo __('Service list');?>

        <?php }?>
    </div>
    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
        <div class="panel-body with-table">
            <div class="table-responsive">
                <div><strong><?php echo __("Service list");?>
&nbsp;(<span class="count_service"><?php echo count($_smarty_tpl->tpl_vars['rows']->value);?>
</span>)</strong></div>
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th><?php echo __("Service name");?>
</th>
                            <th><?php echo __("Service type");?>
</th>
                            <th><?php echo __("Service fee");?>
 (<?php echo @constant('MONEY_UNIT');?>
)</th>
                            
                            <th><?php echo __("Is it feed");?>
?</th>
                            <th><?php echo __("Description");?>
</th>
                            <th><?php echo __("Actions");?>
</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['rows']->value, 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
                            <tr class="list_service_<?php echo $_smarty_tpl->tpl_vars['row']->value['service_id'];?>
">
                                <td align="center"><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['row']->value['service_name'];?>
</td>
                                <td>
                                    <?php if ($_smarty_tpl->tpl_vars['row']->value['type'] == @constant('SERVICE_TYPE_MONTHLY')) {?>
                                        <?php echo __("Monthly service");?>

                                    <?php } elseif ($_smarty_tpl->tpl_vars['row']->value['type'] == @constant('SERVICE_TYPE_DAILY')) {?>
                                        <?php echo __("Daily service");?>

                                    <?php } else { ?>
                                        <?php echo __("Count-based service");?>

                                    <?php }?>
                                </td>
                                <td align="right"><?php echo moneyFormat($_smarty_tpl->tpl_vars['row']->value['fee']);?>
</td>
                                
                                    
                                        
                                    
                                        
                                    
                                
                                <td align="center">
                                    <?php if ($_smarty_tpl->tpl_vars['row']->value['is_food']) {?>
                                        <i class="fa fa-check" aria-hidden="true"></i>
                                    <?php } else { ?>
                                        <i class="fa fa-times" aria-hidden="true"></i>
                                    <?php }?>
                                </td>
                                <td><?php echo $_smarty_tpl->tpl_vars['row']->value['description'];?>
</td>
                                <td nowrap="true">
                                    <?php if ($_smarty_tpl->tpl_vars['canEdit']->value) {?>
                                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/services/edit/<?php echo $_smarty_tpl->tpl_vars['row']->value['service_id'];?>
" class="btn btn-xs btn-default"><?php echo __("Edit");?>
</a>
                                        <button class="btn btn-xs btn-warning js_school-service-cancel" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['service_id'];?>
"><?php echo __("Inactive");?>
</button>
                                        <?php if ($_smarty_tpl->tpl_vars['row']->value['can_delete']) {?>
                                            <button class="btn btn-xs btn-danger js_school-service-delete" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['service_id'];?>
"><?php echo __("Delete");?>
</button>
                                        <?php }?>
                                    <?php }?>
                                </td>
                            </tr>
                            <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        <?php if (count($_smarty_tpl->tpl_vars['rows']->value) == 0) {?>
                            <tr class="odd" class="list_service_<?php echo $_smarty_tpl->tpl_vars['row']->value['service_id'];?>
">
                                <td valign="top" align="center" colspan="8" class="dataTables_empty">
                                    <?php echo __("No data available in table");?>

                                </td>
                            </tr>
                        <?php }?>

                    </tbody>
                </table>
            </div>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "inactive") {?>
        <div class="panel-body with-table">
            <div class="table-responsive">
                <div><strong><?php echo __("Inactive service list");?>
&nbsp;(<?php echo count($_smarty_tpl->tpl_vars['rows']->value);?>
)</strong></div>
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th><?php echo __("Service name");?>
</th>
                        <th><?php echo __("Service type");?>
</th>
                        <th><?php echo __("Service fee");?>
 (<?php echo @constant('MONEY_UNIT');?>
)</th>
                        
                        <th><?php echo __("Is it feed");?>
?</th>
                        <th><?php echo __("Description");?>
</th>
                        <th><?php echo __("Actions");?>
</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['rows']->value, 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
                        <tr>
                            <td align="center"><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</td>
                            <td><?php echo $_smarty_tpl->tpl_vars['row']->value['service_name'];?>
</td>
                            <td>
                                <?php if ($_smarty_tpl->tpl_vars['row']->value['type'] == @constant('SERVICE_TYPE_MONTHLY')) {?>
                                    <?php echo __("Monthly service");?>

                                <?php } elseif ($_smarty_tpl->tpl_vars['row']->value['type'] == @constant('SERVICE_TYPE_DAILY')) {?>
                                    <?php echo __("Daily service");?>

                                <?php } else { ?>
                                    <?php echo __("Count-based service");?>

                                <?php }?>
                            </td>
                            <td align="right"><?php echo moneyFormat($_smarty_tpl->tpl_vars['row']->value['fee']);?>
</td>
                            
                            
                            
                            
                            
                            
                            
                            <td align="center">
                                <?php if ($_smarty_tpl->tpl_vars['row']->value['is_food']) {?>
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                <?php } else { ?>
                                    <i class="fa fa-times" aria-hidden="true"></i>
                                <?php }?>
                            </td>
                            <td><?php echo $_smarty_tpl->tpl_vars['row']->value['description'];?>
</td>
                            <td nowrap="true">
                                <?php if ($_smarty_tpl->tpl_vars['canEdit']->value) {?>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/services/edit/<?php echo $_smarty_tpl->tpl_vars['row']->value['service_id'];?>
" class="btn btn-xs btn-default"><?php echo __("Edit");?>
</a>
                                    <?php if ($_smarty_tpl->tpl_vars['row']->value['can_delete']) {?>
                                        <button class="btn btn-xs btn-danger js_school-service-delete" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['service_id'];?>
"><?php echo __("Delete");?>
</button>
                                    <?php }?>
                                <?php }?>
                            </td>
                        </tr>
                        <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                    <?php if (count($_smarty_tpl->tpl_vars['rows']->value) == 0) {?>
                        <tr class="odd">
                            <td valign="top" align="center" colspan="8" class="dataTables_empty">
                                <?php echo __("No data available in table");?>

                            </td>
                        </tr>
                    <?php }?>

                    </tbody>
                </table>
            </div>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "edit") {?>
        <div class="panel-body">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_service.php">
                <input type="hidden" name="school_username" id="school_username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                <input type="hidden" name="service_id" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['service_id'];?>
"/>
                <input type="hidden" name="do" value="edit_service"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Service name");?>
 (*)</label>
                    <div class="col-sm-9">
                        <input class="form-control" name="service_name" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['service_name'];?>
" required autofocus maxlength="254">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Service type");?>
 (*)</label>
                    <div class="col-sm-4">
                        
                            
                            
                            
                        
                        <input type="hidden" name="type" id="type" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['type'];?>
">
                        <input type="text" value="<?php if ($_smarty_tpl->tpl_vars['data']->value['type'] == @constant('SERVICE_TYPE_MONTHLY')) {
echo __("Monthly service");
} elseif ($_smarty_tpl->tpl_vars['data']->value['type'] == @constant('SERVICE_TYPE_DAILY')) {
echo __("Daily service");
} else {
echo __("Count-based service");
}?>" class="form-control" disabled>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Unit price");?>
</label>
                    <div class="col-sm-4 input-group" style="padding-left: 10px">
                        <input type="text" class="form-control money_tui" name="fee" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['fee'];?>
" maxlength="11" required>
                        <span class="input-group-addon"><?php echo @constant('MONEY_UNIT');?>
</span>
                    </div>
                </div>
                
                    
                    
                        
                            
                            
                        
                    
                
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Is it feed");?>
?</label>
                    <div class="col-sm-9">
                        <div class="onoffswitch">
                            <input type="checkbox" id="is_food" name="is_food" class="onoffswitch-checkbox" <?php if ($_smarty_tpl->tpl_vars['data']->value['is_food'] == 1) {?>checked<?php }?>>
                            <label class="onoffswitch-label" for="is_food"></label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Description");?>
</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="description" rows="4"><?php echo $_smarty_tpl->tpl_vars['data']->value['description'];?>
</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Applied");?>
?</label>
                    <div class="col-sm-9">
                        <div class="onoffswitch">
                            <input type="checkbox" id="status" name="status" class="onoffswitch-checkbox" <?php if ($_smarty_tpl->tpl_vars['data']->value['status'] == 1) {?>checked<?php }?>>
                            <label class="onoffswitch-label" for="status"></label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Parent display");?>
?</label>
                    <div class="col-sm-9">
                        <div class="onoffswitch">
                            <input type="checkbox" id="parent_display" name="parent_display" class="onoffswitch-checkbox" <?php if ($_smarty_tpl->tpl_vars['data']->value['parent_display'] == 1) {?>checked<?php }?>>
                            <label class="onoffswitch-label" for="parent_display"></label>
                        </div>
                        <span class="help-block"><?php echo __("Allow parent display");?>
?</span>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30"><?php echo __("Save");?>
</button>
                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/services" class="btn btn-default"><?php echo __("Lists");?>
</a>
                    </div>
                </div>

                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
            </form>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
        <div class="panel-body">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_service.php">
                <input type="hidden" name="school_username" id="school_username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                <input type="hidden" name="do" value="add_service"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Service name");?>
 (*)</label>
                    <div class="col-sm-9">
                        <input class="form-control" name="service_name" required autofocus maxlength="254">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Service type");?>
 (*)</label>
                    <div class="col-sm-5">
                        <select name="type" id="type" class="form-control">
                            <option value="<?php echo @constant('SERVICE_TYPE_MONTHLY');?>
"><?php echo __("Monthly service");?>
</option>
                            <option value="<?php echo @constant('SERVICE_TYPE_DAILY');?>
"><?php echo __("Daily service");?>
</option>
                            <option value="<?php echo @constant('SERVICE_TYPE_COUNT_BASED');?>
"><?php echo __("Count-based service");?>
</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Unit price");?>
</label>
                    <div class="col-sm-9">
                        <div class="input-group col-md-5">
                            <input type="text" class="form-control money_tui" name="fee" value="0" maxlength="11" required>
                            <span class="input-group-addon"><?php echo @constant('MONEY_UNIT');?>
</span>
                        </div>
                    </div>
                </div>
                
                    
                    
                        
                            
                            
                        
                    
                
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Is it feed");?>
?</label>
                    <div class="col-sm-9">
                        <div class="onoffswitch">
                            <input type="checkbox" id="is_food" name="is_food" class="onoffswitch-checkbox">
                            <label class="onoffswitch-label" for="is_food"></label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Description");?>
</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="description" rows="4"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Applied");?>
?</label>
                    <div class="col-sm-9">
                        <div class="onoffswitch">
                            <input type="checkbox" id="status" name="status" class="onoffswitch-checkbox" checked>
                            <label class="onoffswitch-label" for="status"></label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Parent display");?>
?</label>
                    <div class="col-sm-9">
                        <div class="onoffswitch">
                            <input type="checkbox" id="parent_display" name="parent_display" class="onoffswitch-checkbox" checked>
                            <label class="onoffswitch-label" for="parent_display"></label>
                        </div>
                        <span class="help-block"><?php echo __("Allow parent display");?>
?</span>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30"><?php echo __("Save");?>
</button>
                    </div>
                </div>

                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
            </form>
        </div>
    <?php }?>
</div><?php }
}
