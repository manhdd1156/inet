<?php
/* Smarty version 3.1.31, created on 2021-03-30 16:13:05
  from "D:\workplace\Server11\content\themes\inet\templates\__feeds_notification.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_6062eba1ccd0a4_15525844',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ea1cf3a86febd71c4efefd97240e860ac19d2220' => 
    array (
      0 => 'D:\\workplace\\Server11\\content\\themes\\inet\\templates\\__feeds_notification.tpl',
      1 => 1552404706,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6062eba1ccd0a4_15525844 (Smarty_Internal_Template $_smarty_tpl) {
if ($_smarty_tpl->tpl_vars['notification']->value['ci']) {?>
    <li class="feeds-item <?php if (!$_smarty_tpl->tpl_vars['notification']->value['seen']) {?>unread<?php }?>" data-id="<?php echo $_smarty_tpl->tpl_vars['notification']->value['notification_id'];?>
">
        <?php if ($_smarty_tpl->tpl_vars['notification']->value['is_link'] == 1) {?>
            <a class="data-container" target="_blank" href="<?php echo $_smarty_tpl->tpl_vars['notification']->value['url'];?>
">
        <?php } else { ?>
            <a class="data-container js_notification" data-action="<?php echo $_smarty_tpl->tpl_vars['notification']->value['action'];?>
" data-type="<?php echo $_smarty_tpl->tpl_vars['notification']->value['node_type'];?>
"
               data-url="<?php echo $_smarty_tpl->tpl_vars['notification']->value['node_url'];?>
" data-extra1="<?php echo $_smarty_tpl->tpl_vars['notification']->value['extra1'];?>
" data-extra2="<?php echo $_smarty_tpl->tpl_vars['notification']->value['extra2'];?>
"
               data-extra3="<?php echo $_smarty_tpl->tpl_vars['notification']->value['extra3'];?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['notification']->value['notification_id'];?>
" href="#">
        <?php }?>
            <img class="data-avatar" src="<?php echo $_smarty_tpl->tpl_vars['notification']->value['user_picture'];?>
" alt="">
                <div class="data-content">
                    <div><span class="name"><?php echo $_smarty_tpl->tpl_vars['notification']->value['user_fullname'];?>
</span></div>
                    <div><i class="fas <?php echo $_smarty_tpl->tpl_vars['notification']->value['icon'];?>
 pr5"></i> <?php echo $_smarty_tpl->tpl_vars['notification']->value['message'];?>
</div>
                    <div class="time js_moment" data-time="<?php echo $_smarty_tpl->tpl_vars['notification']->value['time'];?>
"><?php echo $_smarty_tpl->tpl_vars['notification']->value['time'];?>
</div>
                </div>
            </a>
    </li>
<?php } else { ?>
    <li class="feeds-item <?php if (!$_smarty_tpl->tpl_vars['notification']->value['seen']) {?>unread<?php }?>" data-id="<?php echo $_smarty_tpl->tpl_vars['notification']->value['notification_id'];?>
">
        <a class="data-container" href="<?php echo $_smarty_tpl->tpl_vars['notification']->value['url'];?>
">
            <img class="data-avatar" src="<?php echo $_smarty_tpl->tpl_vars['notification']->value['user_picture'];?>
" alt="">
            <div class="data-content">
                <div><span class="name"><?php echo $_smarty_tpl->tpl_vars['notification']->value['user_fullname'];?>
</span></div>
                <div><i class="fa <?php echo $_smarty_tpl->tpl_vars['notification']->value['icon'];?>
 pr5"></i> <?php echo $_smarty_tpl->tpl_vars['notification']->value['message'];?>
</div>
                <div class="time js_moment" data-time="<?php echo $_smarty_tpl->tpl_vars['notification']->value['time'];?>
"><?php echo $_smarty_tpl->tpl_vars['notification']->value['time'];?>
</div>
            </div>
        </a>
    </li>
<?php }
}
}
