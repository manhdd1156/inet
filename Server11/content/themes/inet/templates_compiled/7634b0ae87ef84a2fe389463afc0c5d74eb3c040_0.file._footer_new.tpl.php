<?php
/* Smarty version 3.1.31, created on 2019-06-24 10:35:00
  from "D:\Working\Coniu\ServerSvn\trunk\Server11\content\themes\inet\templates\_footer_new.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5d1044e4291459_45937365',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '7634b0ae87ef84a2fe389463afc0c5d74eb3c040' => 
    array (
      0 => 'D:\\Working\\Coniu\\ServerSvn\\trunk\\Server11\\content\\themes\\inet\\templates\\_footer_new.tpl',
      1 => 1552404706,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:_js_files.tpl' => 1,
    'file:_js_templates.tpl' => 1,
  ),
),false)) {
function content_5d1044e4291459_45937365 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="footer-v1">
    <div class="copyright">
        <div class="row">
            <div class="col-md-4">
                <p style="font-size: 12px">
                    <?php echo date('Y');?>
 &copy; <?php echo $_smarty_tpl->tpl_vars['system']->value['system_title'];?>
.
                    
                    <a href = "#" class="text-link" data-toggle="modal" data-url="#translator"><?php echo $_smarty_tpl->tpl_vars['system']->value['language']['title'];?>
</a> |
                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/privacy"><?php echo __("Privacy");?>
</a> | <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/terms"><?php echo __("Terms");?>
</a>
                </p>
            </div>
            <div class="col-md-5" align="center">
                <p style="font-size: 13px">
                    <span class="text-bold"><?php echo __("Social network license");?>
:</span> 576/GP-BTTTT. <?php echo __("Date of issue");?>
: 22/11/2017. <?php echo __("Đơn vị cấp");?>
: Bộ Thông tin và Truyền thông. <?php echo __("Người đại diện");?>
: Ông Ngô Đạo Quân. <?php echo __("Chức vụ");?>
: Giám đốc. Email: quan@noga.vn
                </p>
                <p><?php echo __("The system is supported by the SCSI-Hansiba startup center");?>
</p>
            </div>
            <!-- Social Links -->
            <div class="col-md-3">
                <ul class="footer-socials list-inline">
                    <li>
                        <a href="https://facebook.com/mobiedu.vn" class="tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Facebook">
                            <i class="fa fa-facebook-f"></i>
                        </a>
                    </li>
                    <li>
                        <a href="mobiedu@mobifone.vn" class="tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Google Plus">
                            <i class="fa fa-google-plus"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Twitter">
                            <i class="fa fa-twitter"></i>
                        </a>
                    </li>
                </ul>
            </div>
            <!-- End Social Links -->
        </div>
        <div class="row">
            <div class="col-sm-12" align="center">
            </div>
        </div>
    </div><!--/copyright-->
</div>
<div id="app_android" style="display: none" class="app_pop">
    <div class="android_down app_down_box">
        <a href="<?php echo @constant('GOOGLEPLAY_URL');?>
" target="_blank"> <img
                    class=""
                    src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/content/themes/<?php echo $_smarty_tpl->tpl_vars['system']->value['theme'];?>
/images/favicon.png"
                    alt="Ứng dụng mầm non Coniu CH Play"/> <?php echo __("Download Inet App");?>
</a>
        <div class="down_app_hide_home">
            <i class="fa fa-close"></i>
        </div>
    </div>
</div>
<div id="app_ios" style="display: none" class="app_pop">
    <div class="ios_down app_down_box">
        <a href="<?php echo @constant('APPSTORE_URL');?>
" target="_blank"><img class=""
                                                                    src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/content/themes/<?php echo $_smarty_tpl->tpl_vars['system']->value['theme'];?>
/images/favicon.png"
                                                                    alt="Ứng dụng mầm non Coniu App store"/> <?php echo __("Download Inet App");?>
</a>
        <div class="down_app_hide_home">
            <i class="fa fa-close"></i>
        </div>
    </div>
</div>
<?php echo '<script'; ?>
>
    function getMobileOperatingSystem() {
        var userAgent = navigator.userAgent || navigator.vendor || window.opera;
        // Windows Phone must come first because its UA also contains "Android"
        if (/windows phone/i.test(userAgent)) {
            return "Windows Phone";
        }

        if (/android/i.test(userAgent)) {
            document.getElementById("app_android").style.display = 'block';
            document.getElementById("ios_banner").style.display = 'none';
            document.getElementById("android_banner").style.marginRight = '0';
            return "Android";
        }

        // iOS detection from: http://stackoverflow.com/a/9039885/177710
        if (/iPad|iPhone|iPod/i.test(userAgent)) {
            document.getElementById("app_ios").style.display = 'block';
            document.getElementById("android_banner").style.display = 'none';
            document.getElementById("ios_banner").style.marginRight = '0';
            return "iOS";
        }
        return "Web";
    }
    getMobileOperatingSystem();
    // if(x == Adroi)
    // document.getElementById("osmobile").innerHTML = x;
<?php echo '</script'; ?>
>
</div><!--/wrapper-->
<!-- JS Files -->
<?php $_smarty_tpl->_subTemplateRender('file:_js_files.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<!-- JS Files -->

<!-- JS Templates -->
<?php $_smarty_tpl->_subTemplateRender('file:_js_templates.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<!-- JS Templates -->

<!-- Analytics Code -->

<!-- Global site tag (gtag.js) - Google Analytics -->

    <?php echo '<script'; ?>
 async src="https://www.googletagmanager.com/gtag/js?id=UA-87252737-2"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-87252737-2');
    <?php echo '</script'; ?>
>

<!-- Analytics Code -->


<!-- Facebook Account Kit SDK-->

<!-- HTTPS required. HTTP will give a 403 forbidden response -->
<?php echo '<script'; ?>
 src="https://sdk.accountkit.com/vi_VN/sdk.js"><?php echo '</script'; ?>
>


    <?php echo '<script'; ?>
>
        // initialize Account Kit with CSRF protection
        AccountKit_OnInteractive = function(){
            AccountKit.init(
                {
                    appId:1638502962897148,
                    state:"coniuahihi",
                    version:"v1.1",
                    fbAppEventsEnabled:true,
                    redirect:"https://inet.vn"
                }
            );
        };

        var api = [];
        api['core/verify']  = ajax_path+"core/verify_phone.php";

        // login callback
        function loginCallback(response) {
            if (response.status === "PARTIALLY_AUTHENTICATED") {

                $.post(api['core/verify'], {
                    'do': 'signin',
                    'code': response.code,
                    'csrf': response.state
                }, function (response) {

                    if (response.callback) {
                        eval(response.callback);
                    } else if(response.error) {
                        modal('#modal-error', {title: __['Error'], message: response.message});
                    } else {
                        window.location.reload();
                    }

                }, 'json');
            }
            else if (response.status === "NOT_AUTHENTICATED") {
                // handle authentication failure
                console.log("Authentication failure");
            }
            else if (response.status === "BAD_PARAMS") {
                // handle bad parameters
                console.log("Bad parameters");
            }
        }

        // phone form submission handler
        function smsLogin() {
            //var countryCode = document.getElementById("country_code").value;
            //var phoneNumber = document.getElementById("phone_number").value;
            AccountKit.login(
                'PHONE',
                {}, // will use default values if this is not specified
                //{countryCode: countryCode, phoneNumber: phoneNumber}, // will use default values if not specified
                loginCallback
            );
        }

    <?php echo '</script'; ?>
>


<!-- JS Global Compulsory -->
<!-- JS Implementing Plugins -->
<?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/content/themes/<?php echo $_smarty_tpl->tpl_vars['system']->value['theme'];?>
/assets/plugins/back-to-top.js"><?php echo '</script'; ?>
>
<!-- JS Customization -->

<!-- JS Page Level -->

<!-- taila -->
<?php echo '<script'; ?>
>
    $(document).ready(function(){
        $('a[href="#topcontrol"]').on('click',function (e) {
            e.preventDefault();

            var target = this.hash;
            var $target = $(target);

            $('html, body').stop().animate({
                'scrollTop': $target.offset().top
            }, 900, 'swing', function () {
                window.location.hash = target;
            });
        });
        $('.js_chat-widget-master').hide();
    });
<?php echo '</script'; ?>
>

<!-- end taila -->
<!--[if lt IE 9]>
<?php echo '<script'; ?>
 src="assets/plugins/respond.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="assets/plugins/html5shiv.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="assets/plugins/placeholder-IE-fixes.js"><?php echo '</script'; ?>
>
<![endif]-->
<?php if ($_smarty_tpl->tpl_vars['system']->value['language']['code'] == 'vi_VN') {?>
    <?php echo '<script'; ?>
 src="https://www.google.com/recaptcha/api.js?onload=myCallBack&render=explicit&hl=vi" async defer><?php echo '</script'; ?>
>
<?php } else { ?>
    <?php echo '<script'; ?>
 src="https://www.google.com/recaptcha/api.js?onload=myCallBack&render=explicit&hl=en" async defer><?php echo '</script'; ?>
>
<?php }
echo '<script'; ?>
>
    var recaptcha1;
    var recaptcha2;
    var recaptcha3;
    var myCallBack = function() {
        //Render the recaptcha1 on the element with ID "recaptcha1"
        recaptcha1 = grecaptcha.render('recaptcha1', {
            'sitekey' : '<?php echo $_smarty_tpl->tpl_vars['system']->value['reCAPTCHA_site_key'];?>
', //Replace this with your Site key
            'theme' : 'light'
        });

        //Render the recaptcha2 on the element with ID "recaptcha2"
        recaptcha2 = grecaptcha.render('recaptcha2', {
            'sitekey' : '<?php echo $_smarty_tpl->tpl_vars['system']->value['reCAPTCHA_site_key'];?>
', //Replace this with your Site key
            'theme' : 'light'
        });

        //Render the recaptcha3 on the element with ID "recaptcha3"
        recaptcha3 = grecaptcha.render('recaptcha3', {
            'sitekey' : '<?php echo $_smarty_tpl->tpl_vars['system']->value['reCAPTCHA_site_key'];?>
', //Replace this with your Site key
            'theme' : 'light'
        });
    };
<?php echo '</script'; ?>
>
<!--End of Tawk.to Script-->
</body>
</html>
<?php }
}
