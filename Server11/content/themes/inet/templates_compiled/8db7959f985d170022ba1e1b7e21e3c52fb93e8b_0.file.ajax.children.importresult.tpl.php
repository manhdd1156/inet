<?php
/* Smarty version 3.1.31, created on 2021-03-31 09:47:23
  from "D:\workplace\Server11\content\themes\inet\templates\ci\school\ajax.children.importresult.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_6063e2bbc8d197_67808022',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '8db7959f985d170022ba1e1b7e21e3c52fb93e8b' => 
    array (
      0 => 'D:\\workplace\\Server11\\content\\themes\\inet\\templates\\ci\\school\\ajax.children.importresult.tpl',
      1 => 1552404702,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6063e2bbc8d197_67808022 (Smarty_Internal_Template $_smarty_tpl) {
if ($_smarty_tpl->tpl_vars['sheet']->value['error'] == 1) {?>
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr><th><?php echo __("Detail results");?>
</th></tr>
        </thead>
        <tbody>
            <tr><td><div><?php echo $_smarty_tpl->tpl_vars['sheet']->value['message'];?>
</div></td></tr>
        </tbody>
    </table>
<?php } else { ?>
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr><th><?php echo __("Detail results");?>
&nbsp;(<?php echo count($_smarty_tpl->tpl_vars['sheet']->value['child_list']);?>
)</th></tr>
        </thead>
        <tbody>
            <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['sheet']->value['child_list'], 'child');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['child']->value) {
?>
                <tr>
                    <td>
                        <div>
                            <?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
&nbsp;-&nbsp;<?php echo $_smarty_tpl->tpl_vars['child']->value['child_name'];?>
&nbsp;|&nbsp;<?php echo $_smarty_tpl->tpl_vars['child']->value['birthday'];?>
&nbsp;|&nbsp;<?php echo $_smarty_tpl->tpl_vars['child']->value['parent_email'];?>
&nbsp;|&nbsp;
                            <?php if ($_smarty_tpl->tpl_vars['child']->value['error'] == 0) {?>
                                <?php echo __("Create student successfull");?>
&nbsp;|&nbsp;<?php echo $_smarty_tpl->tpl_vars['child']->value['create_account_result'];?>

                            <?php } else { ?>
                                <?php echo $_smarty_tpl->tpl_vars['child']->value['message'];?>

                            <?php }?>
                        </div>
                    </td>
                </tr>
                <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

        </tbody>
    </table>
<?php }
}
}
