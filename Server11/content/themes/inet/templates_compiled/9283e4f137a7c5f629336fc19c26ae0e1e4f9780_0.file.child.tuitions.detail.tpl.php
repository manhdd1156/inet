<?php
/* Smarty version 3.1.31, created on 2021-03-31 10:47:16
  from "D:\workplace\Server11\content\themes\inet\templates\ci\child\child.tuitions.detail.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_6063f0c41881f1_32433371',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '9283e4f137a7c5f629336fc19c26ae0e1e4f9780' => 
    array (
      0 => 'D:\\workplace\\Server11\\content\\themes\\inet\\templates\\ci\\child\\child.tuitions.detail.tpl',
      1 => 1552404702,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6063f0c41881f1_32433371 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="panel-body with-table">
    <table class="table table-bordered">
        <tr>
            <td class="col-sm-3 text-right"><?php echo __("Month");?>
</td>
            <td><?php echo $_smarty_tpl->tpl_vars['data']->value['month'];?>
</td>
        </tr>
        <tr>
            <td class="col-sm-3 text-right"><?php echo __("Total");?>
</td>
            <td><?php echo moneyFormat($_smarty_tpl->tpl_vars['data']->value['total_amount']);?>
</td>
        </tr>
        <tr>
            <td class="col-sm-3 text-right"><?php echo __("Status");?>
</td>
            <td>
                <?php if ($_smarty_tpl->tpl_vars['data']->value['status'] == @constant('TUITION_CHILD_NOT_PAID')) {?>
                    <strong><?php echo __("Not paid yet");?>
</strong>
                <?php } elseif ($_smarty_tpl->tpl_vars['data']->value['status'] == @constant('TUITION_CHILD_PARENT_PAID')) {?>
                    <strong><?php echo __("Paid and waiting confirmation");?>
</strong>
                <?php } else { ?>
                    <strong><?php echo __("Paid");?>
</strong>
                <?php }?>
            </td>
        </tr>
        <tr>
            <td class="col-sm-3 text-right"><?php echo __("Description");?>
</td>
            <td><?php echo $_smarty_tpl->tpl_vars['data']->value['description'];?>
</td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <a class="btn btn-default" href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/child/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
/tuitions/history/<?php echo $_smarty_tpl->tpl_vars['data']->value['tuition_id'];?>
"><?php echo __("Used last month");?>
</a>
                <?php if ($_smarty_tpl->tpl_vars['data']->value['status'] == @constant('TUITION_CHILD_NOT_PAID')) {?>
                    <a class="btn btn-primary js_child-tuition" data-handle="pay" data-child_id="<?php echo $_smarty_tpl->tpl_vars['data']->value['child_id'];?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['data']->value['tuition_child_id'];?>
"><?php echo __("Pay");?>
</a>
                <?php }?>
            </td>
        </tr>
    </table>
    <div class="table-responsive">
        <?php if (count($_smarty_tpl->tpl_vars['data']->value['tuition_detail']) > 0) {?>
            <table class="table table-striped table-bordered table-hover bg_white">
                <thead>
                <tr>
                    <th>#</th>
                    <th><?php echo __("Fee name");?>
</th>
                    <th><?php echo __("Quantity");?>
</th>
                    <th><?php echo __("Unit price");?>
</th>
                    <th><?php echo __("Deduction");?>
</th>
                    <th><?php echo __("Money amount");?>
</th>
                </tr>
                </thead>
                <tbody>
                
                <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['data']->value['tuition_detail'], 'detail');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['detail']->value) {
?>
                    <tr>
                        <td align="center"><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</td>
                        <?php if ($_smarty_tpl->tpl_vars['detail']->value['type'] == @constant('TUITION_DETAIL_FEE')) {?>
                            <td><?php echo $_smarty_tpl->tpl_vars['detail']->value['fee_name'];?>
</td>
                            <td class="text-center"><?php echo $_smarty_tpl->tpl_vars['detail']->value['quantity'];?>
</td>
                            <td class="text-right"><?php echo moneyFormat($_smarty_tpl->tpl_vars['detail']->value['unit_price']);?>
</td>
                            <?php if ($_smarty_tpl->tpl_vars['detail']->value['fee_type'] != @constant('FEE_TYPE_MONTHLY')) {?>
                                <td class="text-center"><?php if ($_smarty_tpl->tpl_vars['detail']->value['quantity_deduction'] != 0) {
echo $_smarty_tpl->tpl_vars['detail']->value['quantity_deduction'];?>
(<?php echo __('Day');?>
)<?php }?></td>
                            <?php } else { ?>
                                <td class="text-right"><?php if ($_smarty_tpl->tpl_vars['detail']->value['unit_price_deduction'] != 0) {
echo moneyFormat($_smarty_tpl->tpl_vars['detail']->value['unit_price_deduction']);?>
(<?php echo @constant('MONEY_UNIT');?>
)<?php }?></td>
                            <?php }?>
                        <?php } elseif ($_smarty_tpl->tpl_vars['detail']->value['type'] == @constant('TUITION_DETAIL_SERVICE')) {?>
                            <td><?php echo $_smarty_tpl->tpl_vars['detail']->value['service_name'];?>
</td>
                            <td class="text-center"><?php echo $_smarty_tpl->tpl_vars['detail']->value['quantity'];?>
</td>
                            <td class="text-right"><?php echo moneyFormat($_smarty_tpl->tpl_vars['detail']->value['unit_price']);?>
</td>
                            <?php if ($_smarty_tpl->tpl_vars['detail']->value['service_type'] == @constant('SERVICE_TYPE_DAILY')) {?>
                                <td class="text-center"><?php if ($_smarty_tpl->tpl_vars['detail']->value['quantity_deduction'] != 0) {
echo $_smarty_tpl->tpl_vars['detail']->value['quantity_deduction'];?>
 (<?php echo __('Day');?>
)<?php }?></td>
                            <?php } elseif ($_smarty_tpl->tpl_vars['detail']->value['service_type'] == @constant('SERVICE_TYPE_MONTHLY')) {?>
                                <td class="text-right"><?php if ($_smarty_tpl->tpl_vars['detail']->value['unit_price_deduction'] != 0) {
echo moneyFormat($_smarty_tpl->tpl_vars['detail']->value['unit_price_deduction']);
}?></td>
                            <?php } else { ?>
                                <td class="text-center"><?php if ($_smarty_tpl->tpl_vars['detail']->value['quantity_deduction'] != 0) {
echo $_smarty_tpl->tpl_vars['detail']->value['quantity_deduction'];?>
 (<?php echo __('Times');?>
)<?php }?></td>
                            <?php }?>
                        <?php } else { ?>
                            <td colspan="4"><?php echo __('Late PickUp');?>
</td>
                        <?php }?>
                        <td class="text-right">
                            <?php if ($_smarty_tpl->tpl_vars['detail']->value['type'] != @constant('LATE_PICKUP_FEE')) {?>
                                <?php echo moneyFormat(($_smarty_tpl->tpl_vars['detail']->value['quantity']*$_smarty_tpl->tpl_vars['detail']->value['unit_price']-$_smarty_tpl->tpl_vars['detail']->value['quantity_deduction']*$_smarty_tpl->tpl_vars['detail']->value['unit_price_deduction']));?>

                            <?php } else { ?>
                                <?php echo moneyFormat($_smarty_tpl->tpl_vars['detail']->value['unit_price']);?>

                            <?php }?>
                        </td>
                    </tr>
                    <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>


                
                <?php if ($_smarty_tpl->tpl_vars['data']->value['debt_amount'] != 0) {?>
                    <tr>
                        <td colspan="5" class="text-right"><?php echo __("Debt amount of previous month");?>
 <?php echo $_smarty_tpl->tpl_vars['data']->value['pre_month'];?>
</td>
                        <td class="text-right"><?php echo moneyFormat($_smarty_tpl->tpl_vars['data']->value['debt_amount']);?>
</td>
                    </tr>
                <?php }?>
                <tr>
                    <td colspan="3"></td>
                    <td colspan="2" class="text-right"><strong><?php echo __("Total");?>
</strong></td>
                    <td class="text-right"><strong><?php echo moneyFormat($_smarty_tpl->tpl_vars['data']->value['total_amount']);?>
</strong></td>
                </tr>
                
                <?php if (($_smarty_tpl->tpl_vars['data']->value['status'] == @constant('TUITION_CHILD_CONFIRMED')) && (($_smarty_tpl->tpl_vars['data']->value['total_amount']-$_smarty_tpl->tpl_vars['data']->value['paid_amount']) != 0)) {?>
                    <tr>
                        <td colspan="5" class="text-right"><strong><?php echo __("Paid");?>
</strong></td>
                        <td class="text-right"><strong><?php echo moneyFormat($_smarty_tpl->tpl_vars['data']->value['paid_amount']);?>
</strong></td>
                    </tr>
                    <tr>
                        <td colspan="5" class="text-right"><strong><?php echo __("Debt amount of this month");?>
</strong></td>
                        <td class="text-right"><strong><?php echo moneyFormat($_smarty_tpl->tpl_vars['data']->value['total_amount']-$_smarty_tpl->tpl_vars['data']->value['paid_amount']);?>
</strong></td>
                    </tr>
                <?php }?>
                <?php ob_start();
echo $_smarty_tpl->tpl_vars['data']->value['description'];
$_prefixVariable1=ob_get_clean();
if ((isset($_smarty_tpl->tpl_vars['data']->value['description']) && ($_prefixVariable1 != ''))) {?>
                    <tr>
                        <td colspan="2" class="text-right"><strong><?php echo __("Description");?>
</strong></td>
                        <td colspan="4"><?php echo $_smarty_tpl->tpl_vars['data']->value['description'];?>
</td>
                    </tr>
                <?php }?>
                </tbody>
            </table>
        <?php }?>
    </div>
</div>
<?php }
}
