<?php
/* Smarty version 3.1.31, created on 2021-06-25 10:56:45
  from "D:\workplace\Inet-project\source\Server11\content\themes\inet\templates\ci\noga\noga.usernews.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_60d553fd483ad8_53140823',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '727d00c36999691a650ec086a1227ddd55ff2076' => 
    array (
      0 => 'D:\\workplace\\Inet-project\\source\\Server11\\content\\themes\\inet\\templates\\ci\\noga\\noga.usernews.tpl',
      1 => 1621501238,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60d553fd483ad8_53140823 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <div class="pull-right flip">
            <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
            <?php }?>
        </div>
        <i class="fa fa-user fa-fw fa-lg pr10"></i>
        <?php echo __("Users new");?>

        <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
            &rsaquo; <?php echo __("Search");?>

        <?php }?>
    </div>
    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
        <div class="panel-body with-table form-horizontal">
            <div class="row">
                <div class='col-sm-3'>
                    <div class='input-group date' id='fromuserregpicker'>
                        <input type='text' name="fromDate" id="fromDate" class="form-control" placeholder="<?php echo __("From date");?>
"/>
                        <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                    </div>
                </div>
                <div class='col-sm-3'>
                    <div class='input-group date' id='touserregpicker'>
                        <input type='text' name="toDate" id="toDate" class="form-control" placeholder="<?php echo __("To date");?>
"/>
                        <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                    </div>
                </div>
                <div class='col-sm-2'>
                    <div class="form-group">
                        <a href="#" id="search" class="btn btn-default js_user-registed-new"><?php echo __("Search");?>
</a>
                        <label id="loading" class="btn btn-info x-hidden"><?php echo __("Loading");?>
...</label>
                    </div>
                </div>
            </div>
            <div class="table-responsive pt10" id="user_new_list">
                
            </div>
        </div>
    <?php }?>
</div><?php }
}
