<?php
/* Smarty version 3.1.31, created on 2021-05-27 13:29:48
  from "D:\workplace\Inet-project\source\Server11\content\themes\inet\templates\messages.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_60af3c5c75caf1_27596421',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '52502d418dd9f9b0e8d148bed7b3d35ae92c42e9' => 
    array (
      0 => 'D:\\workplace\\Inet-project\\source\\Server11\\content\\themes\\inet\\templates\\messages.tpl',
      1 => 1621501239,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:_head.tpl' => 1,
    'file:_header.tpl' => 1,
    'file:_emoji-menu.tpl' => 1,
    'file:ajax.chat.conversation.tpl' => 1,
    'file:_footer.tpl' => 1,
  ),
),false)) {
function content_60af3c5c75caf1_27596421 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:_head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php $_smarty_tpl->_subTemplateRender('file:_header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<!-- page content -->
<div class="container mt20 height_min"> <!-- class height_min để set height tối thiểu của container -->
    <div class="row">

        <!-- threads -->
        <div class="col-lg-4 col-md-4 col-sm-4">
            <div class="panel panel-default panel-conversations">
                <div class="panel-heading">
                     <?php echo __("Inbox");?>

                </div>
                <div class="panel-body js_live-messages-alt">
                    
                        <div class="js_scroller" data-slimScroll-height="100%">
                            <div class="loader loader_medium pr10"></div>
                            <ul id="item-chat-panel" class="hidden">
                                
                                
                                
                            </ul>

                            
                            
                            
                                
                                
                            
                            
                            
                        </div>
                    
                </div>
            </div>
        </div>
        <!-- threads -->

        <!-- conversation -->
        <div class="col-lg-8 col-md-8 col-sm-8 js_conversation-container">
            <?php if ($_smarty_tpl->tpl_vars['view']->value == "new") {?>
                <div class="panel panel-default panel-messages fresh">
                    <div class="panel-heading clearfix">
                        <div class="mt5">
                            <?php echo __("New Message");?>

                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="chat-conversations js_scroller" data-slimScroll-height="367px"></div>
                        <div class="chat-to clearfix js_autocomplete">
                            <div class="to"><?php echo __("To");?>
:</div>
                            <ul class="tags"></ul>
                            <div class="typeahead">
                                <input type="text" size="1">
                            </div>
                        </div>
                        <div class="chat-attachments attachments clearfix x-hidden">
                            <ul>
                                <li class="loading">
                                    <div class="loader loader_small"></div>
                                </li>
                            </ul>
                        </div>
                        <div class="chat-form-container">
                            <div class="x-form chat-form">
                                <div class="chat-form-message">
                                    <textarea class="js_autosize  js_post-message" placeholder='<?php echo __("Write a message");?>
'></textarea>
                                </div>
                                <div class="x-form-tools">
                                    <div class="x-form-tools-attach">
                                        <i class="fa fa-camera js_x-uploader" data-handle="chat"></i>
                                    </div>
                                    <div class="x-form-tools-emoji js_emoji-menu-toggle">
                                        <i class="fa fa-smile-o fa-lg"></i>
                                    </div>
                                    <?php $_smarty_tpl->_subTemplateRender('file:_emoji-menu.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } else { ?>
                
                    <?php $_smarty_tpl->_subTemplateRender('file:ajax.chat.conversation.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                
                    
                        
                            
                                
                                    
                                    
                                
                            
                            
                        
                        
                            
                            
                        
                    
                
            <?php }?>
                
        </div>
        <!-- conversation -->

    </div>
</div>
<!-- page content -->

<?php $_smarty_tpl->_subTemplateRender('file:_footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
