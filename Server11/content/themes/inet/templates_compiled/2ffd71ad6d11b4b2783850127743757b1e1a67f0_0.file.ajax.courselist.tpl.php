<?php
/* Smarty version 3.1.31, created on 2021-06-24 16:26:07
  from "D:\workplace\Inet-project\source\Server11\content\themes\inet\templates\ci\school\ajax.courselist.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_60d44faf413396_42531142',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '2ffd71ad6d11b4b2783850127743757b1e1a67f0' => 
    array (
      0 => 'D:\\workplace\\Inet-project\\source\\Server11\\content\\themes\\inet\\templates\\ci\\school\\ajax.courselist.tpl',
      1 => 1624523683,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60d44faf413396_42531142 (Smarty_Internal_Template $_smarty_tpl) {
?>
<table class="table table-striped table-bordered table-hover js_dataTable">
    <thead>
    <tr><th colspan="7"><?php echo __("course list");?>
&nbsp;(<span class="count_conduct"><?php echo count($_smarty_tpl->tpl_vars['result']->value);?>
</span>) </th></tr>
    <tr>
        <th><?php echo __("#");?>
</th>
        <th><?php echo __("Course name");?>
</th>
        <th><?php echo __("Short detail");?>
</th>
        <th><?php echo __("Created at");?>
</th>

        <th><?php echo __("Price");?>
</th>
        <th><?php echo __("Number of view");?>
</th>
        <th><?php echo __("Actions");?>
</th>

    </tr>
    </thead>
    <tbody>
        <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['result']->value, 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
            <tr>
                <td class="align-middle" align="center">
                    <strong><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</strong>
                </td>
                <td class="align-middle" style="word-break: break-all"> <?php echo $_smarty_tpl->tpl_vars['row']->value['title'];?>
</td>
                <td class="align-middle"> <?php echo $_smarty_tpl->tpl_vars['row']->value['short_detail'];?>
 </td>
                <td align="center" class="align-middle"><?php echo date_format($_smarty_tpl->tpl_vars['row']->value['created_at'],'H:i:s d-m-Y');?>
</td>
                <td align="center" class="align-middle"><?php echo $_smarty_tpl->tpl_vars['row']->value['price'];?>
</td>
                <td align="center" class="align-middle"><?php echo $_smarty_tpl->tpl_vars['row']->value['view'];?>
</td>
                <td align = "center" class="align-middle">
                    <a href="<?php echo $_smarty_tpl->tpl_vars['row']->value['detailUrl'];?>
" target="_blank" class="btn btn-xs btn-default"><?php echo __("Detail");?>
</a>
                </td>
            </tr>
            <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

    </tbody>
</table>

<?php echo '<script'; ?>
>
    $(function() {
        // run DataTable
        $('.js_dataTable').DataTable({
            "aoColumnDefs": [ { 'aDataSort': false, 'aTargets': [ -1 ] } ],
            "language": {
                "decimal":        "",
                "emptyTable":     __["No data available in table"],
                "info":           __["Showing _START_ to _END_ of _TOTAL_ results"],
                "infoEmpty":      __["Showing 0 to 0 of 0 results"],
                "infoFiltered":   "(filtered from _MAX_ total entries)",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     __["Show"] + " " + "_MENU_" + " " + __["results"],
                "loadingRecords": __["Loading..."],
                "processing":     __["Processing..."],
                "search":         __["Search"],
                "zeroRecords":    __["No matching records found"],
                "paginate": {
                    "first":      __["First"],
                    "last":       __["Last"],
                    "next":       __["Next"],
                    "previous":   __["Previous"]
                },
                "aria": {
                    "sortAscending":  ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                }
            }
        });
    });
<?php echo '</script'; ?>
>
<?php }
}
