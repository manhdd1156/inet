<?php
/* Smarty version 3.1.31, created on 2021-06-22 10:24:53
  from "D:\workplace\Inet-project\source\Server11\content\themes\inet\templates\ci\noga\ajax.noga.page.list.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_60d158051d6520_91650204',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'a8035d59fec39be7de7d71a34ed45733144de8eb' => 
    array (
      0 => 'D:\\workplace\\Inet-project\\source\\Server11\\content\\themes\\inet\\templates\\ci\\noga\\ajax.noga.page.list.tpl',
      1 => 1621501238,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60d158051d6520_91650204 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover js_dataTable">
        <thead>
        <tr>
            <th><?php echo __("ID");?>
</th>
            <th><?php echo __("Picture");?>
</th>
            <th><?php echo __("URL");?>
</th>
            <th><?php echo __("Title");?>
</th>
            <th><?php echo __("Likes");?>
</th>
            <th><?php echo __("Post time");?>
</th>
        </tr>
        </thead>
        <tbody>
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['results']->value, 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
            <tr>
                <td>
                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/pages/<?php echo $_smarty_tpl->tpl_vars['row']->value['page_name'];?>
" target="_blank">
                        <?php echo $_smarty_tpl->tpl_vars['row']->value['page_id'];?>

                    </a>
                </td>
                <td class="post-avatar">
                    <a target="_blank" class="post-avatar-picture" href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/pages/<?php echo $_smarty_tpl->tpl_vars['row']->value['page_name'];?>
" style="background-image:url(<?php echo $_smarty_tpl->tpl_vars['row']->value['page_picture'];?>
);">
                    </a>
                </td>
                <td>
                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/pages/<?php echo $_smarty_tpl->tpl_vars['row']->value['page_name'];?>
" target="_blank">
                        <?php echo $_smarty_tpl->tpl_vars['row']->value['page_name'];?>

                    </a>
                </td>
                <td>
                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/pages/<?php echo $_smarty_tpl->tpl_vars['row']->value['page_name'];?>
" target="_blank">
                        <?php echo $_smarty_tpl->tpl_vars['row']->value['page_title'];?>

                    </a>
                </td>
                <td><?php echo $_smarty_tpl->tpl_vars['row']->value['page_likes'];?>
</td>
                <td>
                    <?php echo $_smarty_tpl->tpl_vars['row']->value['time'];?>

                </td>
            </tr>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

        </tbody>
    </table>
</div><?php }
}
