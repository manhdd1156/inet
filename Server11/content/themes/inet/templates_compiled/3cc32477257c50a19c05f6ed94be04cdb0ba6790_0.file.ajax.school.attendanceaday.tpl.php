<?php
/* Smarty version 3.1.31, created on 2019-06-24 11:00:09
  from "D:\Working\Coniu\ServerSvn\trunk\Server11\content\themes\inet\templates\ci\school\ajax.school.attendanceaday.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5d104ac94d7135_71881935',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '3cc32477257c50a19c05f6ed94be04cdb0ba6790' => 
    array (
      0 => 'D:\\Working\\Coniu\\ServerSvn\\trunk\\Server11\\content\\themes\\inet\\templates\\ci\\school\\ajax.school.attendanceaday.tpl',
      1 => 1552404702,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d104ac94d7135_71881935 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div><strong><?php echo __("Attendance list");?>
&nbsp;(<?php echo count($_smarty_tpl->tpl_vars['rows']->value);?>
&nbsp;<?php echo __("Class");?>
)</strong></div>
<table class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th>#</th>
        <?php if ($_smarty_tpl->tpl_vars['class_id']->value <= 0) {?><th><?php echo __("Class");?>
</th><?php }?>
        <th><?php echo __("Status");?>
</th>
        <th><?php echo __("Number of student");?>
</th>
        <th><?php echo __("Present");?>
</th>
        <th><?php echo __("Absence");?>
</th>
        <th><?php echo __("Time");?>
</th>
        <th><?php echo __("Actions");?>
</th>
    </tr>
    </thead>
    <tbody>
    <?php $_smarty_tpl->_assignInScope('totalPresent', 0);
?>
    <?php $_smarty_tpl->_assignInScope('totalAbsence', 0);
?>
    <?php $_smarty_tpl->_assignInScope('total', 0);
?>
    <?php $_smarty_tpl->_assignInScope('idx', 1);
?>

    <?php $_smarty_tpl->_assignInScope('classLevelId', -1);
?>
    <?php $_smarty_tpl->_assignInScope('classLevelPresent', 0);
?>
    <?php $_smarty_tpl->_assignInScope('classLevelAbsence', 0);
?>
    <?php $_smarty_tpl->_assignInScope('classLevelTotal', 0);
?>

    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['rows']->value, 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
        
        <?php if (($_smarty_tpl->tpl_vars['idx']->value == 1)) {?>
            <tr><td colspan="8" align="left"><strong><?php echo $_smarty_tpl->tpl_vars['row']->value['class_level_name'];?>
</strong></td></tr>
        <?php } elseif (($_smarty_tpl->tpl_vars['idx']->value > 1) && ($_smarty_tpl->tpl_vars['classLevelId']->value != $_smarty_tpl->tpl_vars['row']->value['class_level_id'])) {?>
            <tr>
                <td colspan="3" align="center"><strong><font color="blue"><?php echo __('Total of grade');?>
</font></strong></td>
                <td align="center"><strong><font color="blue"><?php echo $_smarty_tpl->tpl_vars['classLevelTotal']->value;?>
</font></strong></td>
                <td align="center"><strong><font color="blue"><?php ob_start();
echo $_smarty_tpl->tpl_vars['classLevelPresent']->value;
$_prefixVariable1=ob_get_clean();
if ($_prefixVariable1 > 0) {
echo $_smarty_tpl->tpl_vars['classLevelPresent']->value;
}?></font></strong></td>
                <td align="center"><strong><font color="blue"><?php ob_start();
echo $_smarty_tpl->tpl_vars['classLevelPresent']->value;
$_prefixVariable2=ob_get_clean();
if ($_prefixVariable2 > 0) {
echo $_smarty_tpl->tpl_vars['classLevelAbsence']->value;
}?></font></strong></td>
                <td colspan="2"></td>
            </tr>
            <tr><td colspan="8"></td></tr>
            <?php $_smarty_tpl->_assignInScope('classLevelPresent', 0);
?>
            <?php $_smarty_tpl->_assignInScope('classLevelAbsence', 0);
?>
            <?php $_smarty_tpl->_assignInScope('classLevelTotal', 0);
?>
            <tr><td colspan="8" align="left"><strong><?php echo $_smarty_tpl->tpl_vars['row']->value['class_level_name'];?>
</strong></td></tr>
        <?php }?>
        <tr>
            <td align="center"><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</td>
            <?php if ($_smarty_tpl->tpl_vars['class_id']->value <= 0) {?>
                <td>
                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/attendance/rollup/<?php echo $_smarty_tpl->tpl_vars['row']->value['group_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['row']->value['group_title'];?>
</a>
                </td>
            <?php }?>
            <td align="center">
                <?php if ($_smarty_tpl->tpl_vars['row']->value['is_checked'] == 0) {?>
                    <i class="fa fa-times" aria-hidden="true"></i>
                <?php } else { ?>
                    <i class="fa fa-check" aria-hidden="true"></i>
                <?php }?>
            </td>
            <td align="center"><?php echo $_smarty_tpl->tpl_vars['row']->value['total'];?>
</td>
            <td align="center"><?php if ($_smarty_tpl->tpl_vars['row']->value['is_checked']) {
echo $_smarty_tpl->tpl_vars['row']->value['present_count'];
}?></td>
            <td align="center"><?php if ($_smarty_tpl->tpl_vars['row']->value['is_checked']) {
echo $_smarty_tpl->tpl_vars['row']->value['absence_count'];
}?></td>
            <td><?php if ($_smarty_tpl->tpl_vars['row']->value['is_checked'] == 1) {
echo $_smarty_tpl->tpl_vars['row']->value['recorded_at'];
}?></td>
            <td align="center">
                <?php if ($_smarty_tpl->tpl_vars['row']->value['is_checked'] == 1) {?>
                    <a class="btn btn-xs btn-default" href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/attendance/detail/<?php echo $_smarty_tpl->tpl_vars['row']->value['attendance_id'];?>
"><?php echo __("Detail");?>
</a>
                <?php }?>
                <?php if (canEdit($_smarty_tpl->tpl_vars['username']->value,'attendance')) {?>
                    <a class="btn btn-xs btn-default" href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/attendance/rollup/<?php echo $_smarty_tpl->tpl_vars['row']->value['group_id'];?>
"><?php echo __("Roll up");?>
</a>
                <?php }?>
            </td>
            <?php $_smarty_tpl->_assignInScope('totalPresent', $_smarty_tpl->tpl_vars['totalPresent']->value+$_smarty_tpl->tpl_vars['row']->value['present_count']);
?>
            <?php $_smarty_tpl->_assignInScope('totalAbsence', $_smarty_tpl->tpl_vars['totalAbsence']->value+$_smarty_tpl->tpl_vars['row']->value['absence_count']);
?>
            <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
            <?php $_smarty_tpl->_assignInScope('classLevelId', $_smarty_tpl->tpl_vars['row']->value['class_level_id']);
?>
            <?php $_smarty_tpl->_assignInScope('classLevelPresent', $_smarty_tpl->tpl_vars['classLevelPresent']->value+$_smarty_tpl->tpl_vars['row']->value['present_count']);
?>
            <?php $_smarty_tpl->_assignInScope('classLevelAbsence', $_smarty_tpl->tpl_vars['classLevelAbsence']->value+$_smarty_tpl->tpl_vars['row']->value['absence_count']);
?>
            <?php $_smarty_tpl->_assignInScope('classLevelTotal', $_smarty_tpl->tpl_vars['classLevelTotal']->value+$_smarty_tpl->tpl_vars['row']->value['total']);
?>
            <?php $_smarty_tpl->_assignInScope('total', $_smarty_tpl->tpl_vars['total']->value+$_smarty_tpl->tpl_vars['row']->value['total']);
?>
        </tr>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

    <?php if (($_smarty_tpl->tpl_vars['idx']->value > 1)) {?>
        <tr>
            <td colspan="3" align="center"><strong><font color="blue"><?php echo __('Total of grade');?>
</font></strong></td>
            <td align="center"><strong><font color="blue"><?php echo $_smarty_tpl->tpl_vars['classLevelTotal']->value;?>
</font></strong></td>
            <td align="center"><strong><font color="blue"><?php if ($_smarty_tpl->tpl_vars['classLevelPresent']->value > 0) {
echo $_smarty_tpl->tpl_vars['classLevelPresent']->value;
}?></font></strong></td>
            <td align="center"><strong><font color="blue"><?php if ($_smarty_tpl->tpl_vars['classLevelPresent']->value > 0) {
echo $_smarty_tpl->tpl_vars['classLevelAbsence']->value;
}?></font></strong></td>
            <td colspan="2"></td>
        </tr>
    <?php }?>
    <tr>
        <td colspan="3" align="center"><strong><font color="red"><?php echo __('TOTAL OF SCHOOL');?>
</font></strong></td>
        <td align="center"><strong><font color="red"><?php echo $_smarty_tpl->tpl_vars['total']->value;?>
</font></strong></td>
        <td align="center"><strong><font color="red"><?php ob_start();
echo $_smarty_tpl->tpl_vars['totalPresent']->value;
$_prefixVariable3=ob_get_clean();
if ($_prefixVariable3 > 0) {
echo $_smarty_tpl->tpl_vars['totalPresent']->value;
}?></font></strong></td>
        <td align="center"><strong><font color="red"><?php ob_start();
echo $_smarty_tpl->tpl_vars['totalPresent']->value;
$_prefixVariable4=ob_get_clean();
if ($_prefixVariable4 > 0) {
echo $_smarty_tpl->tpl_vars['totalAbsence']->value;
}?></font></strong></td>
        <td colspan="2"></td>
    </tr>
    </tbody>
</table><?php }
}
