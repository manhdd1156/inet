<?php
/* Smarty version 3.1.31, created on 2021-03-31 10:16:22
  from "D:\workplace\Server11\content\themes\inet\templates\ci\school\ajax.school.schedule.preview.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_6063e9862052d9_52712559',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '1aa595983b503c80774271e74b4bcb07ea315b90' => 
    array (
      0 => 'D:\\workplace\\Server11\\content\\themes\\inet\\templates\\ci\\school\\ajax.school.schedule.preview.tpl',
      1 => 1552404702,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6063e9862052d9_52712559 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="panel-body with-table">
    <table class = "table table-bordered">
        <tbody>
        <tr>
            <td class = "col-sm-3 text-right"><?php echo __('Schedule name');?>
</td>
            <td>
                <?php echo $_smarty_tpl->tpl_vars['data']->value['schedule_name'];?>

            </td>
        </tr>
        <tr>
            <td class = "col-sm-3 text-right"><?php echo __('Scope');?>
</td>
            <td>
                <?php if (isset($_smarty_tpl->tpl_vars['class']->value)) {?> <?php echo $_smarty_tpl->tpl_vars['class']->value['group_title'];?>

                <?php } elseif (isset($_smarty_tpl->tpl_vars['class_level']->value)) {
echo $_smarty_tpl->tpl_vars['class_level']->value['class_level_name'];?>

                <?php } elseif (!isset($_smarty_tpl->tpl_vars['class']->value) && !isset($_smarty_tpl->tpl_vars['class_level']->value)) {?> <?php echo __('School');?>

                <?php }?>
            </td>
        </tr>
        <tr>
            <td class = "col-sm-3 text-right"><?php echo __('Begin');?>
</td>
            <td>
                <?php echo $_smarty_tpl->tpl_vars['data']->value['begin'];?>

            </td>
        </tr>
        <tr>
            <td class = "col-sm-3 text-right"><?php echo __('Use category');?>
</td>
            <td>
                <?php if ($_smarty_tpl->tpl_vars['data']->value['is_category']) {?>
                    <?php echo __('Yes');?>

                <?php } else { ?>
                    <?php echo __('No');?>

                <?php }?>
            </td>
        </tr>
        <tr>
            <td class = "col-sm-3 text-right"><?php echo __('Study saturday');?>
</td>
            <td>
                <?php if ($_smarty_tpl->tpl_vars['data']->value['is_saturday']) {?>
                    <?php echo __('Yes');?>

                <?php } else { ?>
                    <?php echo __('No');?>

                <?php }?>
            </td>
        </tr>
        <tr>
            <td class = "col-sm-3 text-right"><?php echo __('Description');?>
</td>
            <td>
                <?php echo nl2br($_smarty_tpl->tpl_vars['data']->value['description']);?>

            </td>
        </tr>
        </tbody>
    </table>
    <div class = "table-responsive">
        <table class = "table table-striped table-bordered table-hover">
            <thead>
            <tr>
                <th><?php echo __('#');?>
</th>
                <th><?php echo __('Time');?>
 </th>
                <th <?php if (!$_smarty_tpl->tpl_vars['data']->value['is_category']) {?>class="hidden"<?php }?>><?php echo __('Activity');?>
</th>
                <th><?php echo __('Monday');?>
 </th>
                <th><?php echo __('Tuesday');?>
</th>
                <th> <?php echo __('Wednesday');?>
</th>
                <th><?php echo __('Thursday');?>
</th>
                <th><?php echo __('Friday');?>
 </th>
                <th <?php if (!$_smarty_tpl->tpl_vars['data']->value['is_saturday']) {?>class="hidden"<?php }?>><?php echo __('Saturday');?>
</th>
            </tr>
            </thead>
            <tbody>
            <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['data']->value['details'], 'row', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['row']->value) {
?>
                <?php $_smarty_tpl->_assignInScope('array', array_values($_smarty_tpl->tpl_vars['row']->value));
?>
                <?php $_smarty_tpl->_assignInScope('temp', array());
?>
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['array']->value, 'value', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['value']->value) {
?>
                    <?php if ($_smarty_tpl->tpl_vars['data']->value['is_saturday']) {?>
                        <?php if (($_smarty_tpl->tpl_vars['k']->value >= 2) && ($_smarty_tpl->tpl_vars['k']->value < (count($_smarty_tpl->tpl_vars['array']->value)))) {?>
                            <?php $_tmp_array = isset($_smarty_tpl->tpl_vars['temp']) ? $_smarty_tpl->tpl_vars['temp']->value : array();
if (!is_array($_tmp_array) || $_tmp_array instanceof ArrayAccess) {
settype($_tmp_array, 'array');
}
$_tmp_array[] = $_smarty_tpl->tpl_vars['value']->value;
$_smarty_tpl->_assignInScope('temp', $_tmp_array);
?>
                        <?php }?>
                    <?php } else { ?>
                        <?php if (($_smarty_tpl->tpl_vars['k']->value >= 2) && ($_smarty_tpl->tpl_vars['k']->value < (count($_smarty_tpl->tpl_vars['array']->value)-1))) {?>
                            <?php $_tmp_array = isset($_smarty_tpl->tpl_vars['temp']) ? $_smarty_tpl->tpl_vars['temp']->value : array();
if (!is_array($_tmp_array) || $_tmp_array instanceof ArrayAccess) {
settype($_tmp_array, 'array');
}
$_tmp_array[] = $_smarty_tpl->tpl_vars['value']->value;
$_smarty_tpl->_assignInScope('temp', $_tmp_array);
?>
                        <?php }?>
                    <?php }?>
                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                <tr>
                    <td style="vertical-align: middle" align="center">
                        <strong><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</strong>
                    </td>
                    <td style="vertical-align: middle" align="center">
                        <strong><?php echo $_smarty_tpl->tpl_vars['row']->value['subject_time'];?>
</strong>
                    </td>
                    <td <?php if (!$_smarty_tpl->tpl_vars['data']->value['is_category']) {?>class="hidden"<?php }?> align="center" style = "vertical-align: middle;">
                        <strong><?php echo $_smarty_tpl->tpl_vars['row']->value['subject_name'];?>
</strong>
                    </td>
                    <?php $_smarty_tpl->_assignInScope('col', 1);
?>
                    <?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable(null, $_smarty_tpl->isRenderingCache);
$_smarty_tpl->tpl_vars['i']->value = 0;
if ($_smarty_tpl->tpl_vars['i']->value < count($_smarty_tpl->tpl_vars['temp']->value)) {
for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value < count($_smarty_tpl->tpl_vars['temp']->value); $_smarty_tpl->tpl_vars['i']->value++) {
?>
                        <?php if ($_smarty_tpl->tpl_vars['temp']->value[$_smarty_tpl->tpl_vars['i']->value] === $_smarty_tpl->tpl_vars['temp']->value[($_smarty_tpl->tpl_vars['i']->value+1)]) {?>
                            <?php $_smarty_tpl->_assignInScope('col', $_smarty_tpl->tpl_vars['col']->value+1);
?>
                        <?php } else { ?>
                            <td colspan = "<?php echo $_smarty_tpl->tpl_vars['col']->value;?>
" align="center">
                                <?php echo nl2br($_smarty_tpl->tpl_vars['temp']->value[$_smarty_tpl->tpl_vars['i']->value]);?>

                            </td>
                            <?php $_smarty_tpl->_assignInScope('col', 1);
?>
                        <?php }?>
                    <?php }
}
?>

                </tr>
                <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

            </tbody>
        </table>
    </div>
</div><?php }
}
