<?php
/* Smarty version 3.1.31, created on 2021-03-30 15:35:31
  from "D:\workplace\Server11\content\themes\inet\templates\_widget.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_6062e2d342a692_99136655',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '10c1a65a4204e5e45f98c0676ed5f00c04626069' => 
    array (
      0 => 'D:\\workplace\\Server11\\content\\themes\\inet\\templates\\_widget.tpl',
      1 => 1552404706,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6062e2d342a692_99136655 (Smarty_Internal_Template $_smarty_tpl) {
if ($_smarty_tpl->tpl_vars['widgets']->value) {?>
    <!-- Widgets -->
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['widgets']->value, 'widget');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['widget']->value) {
?>
        <div class="panel panel-default panel-widget">
            <div class="panel-heading">
                <strong><?php echo $_smarty_tpl->tpl_vars['widget']->value['title'];?>
</strong>
            </div>
            <div class="panel-body"><?php echo $_smarty_tpl->tpl_vars['widget']->value['code'];?>
</div>
        </div>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

    <!-- Widgets -->
<?php }
}
}
