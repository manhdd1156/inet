<?php
/* Smarty version 3.1.31, created on 2021-03-31 10:30:11
  from "D:\workplace\Server11\content\themes\inet\templates\ci\school\school.events.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_6063ecc31566b8_63544223',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '359161661b913acaef9814c6b28aa7df43e81e29' => 
    array (
      0 => 'D:\\workplace\\Server11\\content\\themes\\inet\\templates\\ci\\school\\school.events.tpl',
      1 => 1573458086,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:ci/school/ajax.eventparticipants.tpl' => 1,
    'file:ci/school/ajax.event.teacherparticipants.tpl' => 1,
  ),
),false)) {
function content_6063ecc31566b8_63544223 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <?php if (($_smarty_tpl->tpl_vars['sub_view']->value == '') && $_smarty_tpl->tpl_vars['canEdit']->value) {?>
            <div class="pull-right flip">
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/events/add" class="btn btn-default">
                    <i class="fa fa-plus"></i> <?php echo __("Add New");?>

                </a>
            </div>
        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "add" || $_smarty_tpl->tpl_vars['sub_view']->value == "edit") {?>
            <div class="pull-right flip">
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/events" class="btn btn-default">
                    <i class="fa fa-list"></i> <?php echo __("Lists");?>

                </a>
            </div>
        <?php }?>
        <div class="pull-right flip" style="margin-right: 5px">
            <a href="https://blog.coniu.vn/huong-dan-tao-su-kien-tren-website/" class="btn btn-info btn_guide" target="_blank">
                <i class="fa fa-info-circle"></i> <?php echo __("Guide");?>

            </a>
        </div>
        <i class="fa fa-bell fa-lg fa-fw pr10"></i>
        <?php echo __("Notification - Event");?>

        <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == "edit") {?>
            &rsaquo; <?php echo $_smarty_tpl->tpl_vars['data']->value['event_name'];?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "detail") {?>
            &rsaquo; <?php echo $_smarty_tpl->tpl_vars['data']->value['event_name'];?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
            &rsaquo; <?php echo __('Add New');?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "participants") {?>
            &rsaquo; <?php echo __("Participants");?>
 &rsaquo; <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/events/detail/<?php echo $_smarty_tpl->tpl_vars['event']->value['event_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['event']->value['event_name'];?>
</a>
        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "participantsforemployee") {?>
            &rsaquo; <?php echo __("Participants");?>
 &rsaquo; <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/events/detail/<?php echo $_smarty_tpl->tpl_vars['event']->value['event_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['event']->value['event_name'];?>
</a>
        <?php }?>
    </div>
    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
        <div class="panel-body with-table">
            <div><strong><?php echo __("Notification - Event list");?>
&nbsp;(<span class="count_event"><?php echo count($_smarty_tpl->tpl_vars['rows']->value);?>
</span>)</strong></div>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th><?php echo __("Notification - Event");?>
</th>
                            <th><?php echo __("Event level");?>
</th>
                            <?php if ($_smarty_tpl->tpl_vars['canEdit']->value) {?>
                                
                                <th><?php echo __("Actions");?>
</th>
                            <?php }?>
                        </tr>
                    </thead>
                    <tr>
                        <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['rows']->value, 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
                            <tr>
                                
                                <td align="center" rowspan="2" style="vertical-align:middle"><strong><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</strong></td>
                                <td style="vertical-align:middle">
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/events/detail/<?php echo $_smarty_tpl->tpl_vars['row']->value['event_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['row']->value['event_name'];?>
</a> <a href="#" data-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['event_id'];?>
" style="float: right; color: #5D9D58" class="js_event_toggle"><?php echo __("Show/hide");?>
</a>
                                    <img class="cancel_img_<?php echo $_smarty_tpl->tpl_vars['row']->value['event_id'];?>
 <?php if ($_smarty_tpl->tpl_vars['row']->value['status'] != @constant('EVENT_STATUS_CANCELLED')) {?>x-hidden<?php }?>" src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/content/themes/<?php echo $_smarty_tpl->tpl_vars['system']->value['theme'];?>
/images/cancel.png"/>
                                </td>
                                <td align="center" style="vertical-align:middle">
                                    <?php if ($_smarty_tpl->tpl_vars['row']->value['for_teacher']) {?>
                                        <?php echo __("Teacher - Employee");?>

                                    <?php } else { ?>
                                        <?php if ($_smarty_tpl->tpl_vars['row']->value['level'] == @constant('SCHOOL_LEVEL')) {?>
                                            <?php echo __("School");?>

                                        <?php } elseif ($_smarty_tpl->tpl_vars['row']->value['level'] == @constant('CLASS_LEVEL_LEVEL')) {?>
                                            <?php echo $_smarty_tpl->tpl_vars['row']->value['class_level_name'];?>

                                        <?php } elseif ($_smarty_tpl->tpl_vars['row']->value['level'] == @constant('CLASS_LEVEL')) {?>
                                            <?php echo $_smarty_tpl->tpl_vars['row']->value['group_title'];?>

                                        <?php }?>
                                    <?php }?>
                                </td>
                                <?php if ($_smarty_tpl->tpl_vars['canEdit']->value) {?>
                                    
                                    
                                    
                                    
                                    
                                    <td align="center" style="vertical-align:middle">
                                        <?php if ($_smarty_tpl->tpl_vars['canEdit']->value) {?>
                                            <?php if ((!$_smarty_tpl->tpl_vars['row']->value['is_notified']) && ($_smarty_tpl->tpl_vars['row']->value['status'] != @constant('EVENT_STATUS_CANCELLED'))) {?>
                                                <button class="btn btn-xs btn-default js_school-event-notify cancel_hidden_<?php echo $_smarty_tpl->tpl_vars['row']->value['event_id'];?>
" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['event_id'];?>
">
                                                    <?php echo __("Notify");?>

                                                </button>
                                            <?php }?>
                                        <?php }?>
                                        <?php if ($_smarty_tpl->tpl_vars['row']->value['must_register']) {?>
                                            <?php if (!$_smarty_tpl->tpl_vars['row']->value['for_teacher']) {?>
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/events/participants/<?php echo $_smarty_tpl->tpl_vars['row']->value['event_id'];?>
" class="btn btn-xs btn-default">
                                                    <?php echo __("Participants");?>

                                                </a><br/>
                                            <?php } else { ?>
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/events/participantsforemployee/<?php echo $_smarty_tpl->tpl_vars['row']->value['event_id'];?>
" class="btn btn-xs btn-default">
                                                    <?php echo __("Participants");?>

                                                </a><br/>
                                            <?php }?>
                                        <?php }?>
                                        <?php if ($_smarty_tpl->tpl_vars['canEdit']->value) {?>
                                            <?php if (($_smarty_tpl->tpl_vars['row']->value['status'] != @constant('EVENT_STATUS_CANCELLED'))) {?>
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/events/edit/<?php echo $_smarty_tpl->tpl_vars['row']->value['event_id'];?>
" class="btn btn-xs btn-default cancel_hidden_<?php echo $_smarty_tpl->tpl_vars['row']->value['event_id'];?>
"><?php echo __("Edit");?>
</a>
                                                <?php if ((($_smarty_tpl->tpl_vars['row']->value['parent_participants']+$_smarty_tpl->tpl_vars['row']->value['child_participants']+$_smarty_tpl->tpl_vars['row']->value['teacher_participants']) == 0) && (!$_smarty_tpl->tpl_vars['row']->value['is_notified'])) {?>
                                                    <button class="btn btn-xs btn-danger js_school-delete cancel_hidden_<?php echo $_smarty_tpl->tpl_vars['row']->value['event_id'];?>
" data-handle="event" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['event_id'];?>
"><?php echo __("Delete");?>
</button>
                                                <?php } else { ?>
                                                    <button class="btn btn-xs btn-warning js_school-event-cancel cancel_hidden_<?php echo $_smarty_tpl->tpl_vars['row']->value['event_id'];?>
" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['event_id'];?>
"><?php echo __("Cancel");?>
</button>
                                                <?php }?>
                                            <?php }?>
                                        <?php }?>
                                    </td>
                                <?php }?>
                            </tr>
                            <tr>
                                <td style="display: none" class="event_description_<?php echo $_smarty_tpl->tpl_vars['row']->value['event_id'];?>
 description_style" <?php if ($_smarty_tpl->tpl_vars['canEdit']->value) {?>colspan="6"<?php } else { ?>colspan="3"<?php }?>><?php echo $_smarty_tpl->tpl_vars['row']->value['description'];?>
</td>
                            </tr>
                            <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>


                        <?php if (count($_smarty_tpl->tpl_vars['rows']->value) == 0) {?>
                            <tr class="odd">
                                <td valign="top" align="center" colspan="7" class="dataTables_empty">
                                    <?php echo __("No data available in table");?>

                                </td>
                            </tr>
                        <?php }?>
                    </tbody>
                </table>
            </div>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "detail") {?>
        <div class="panel-body with-table">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_event.php">
                <input type="hidden" name="school_username" id="school_username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                <input type="hidden" name="do" id="do" value="reg_event"/>
                <input type="hidden" id="school_id" name="school_id" value="<?php echo $_smarty_tpl->tpl_vars['school']->value['page_id'];?>
"/>
                <input type="hidden" id="event_id" name="event_id" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['event_id'];?>
"/>
                <input type="hidden" name="event_name" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['event_name'];?>
"/>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <tbody>
                            <tr>
                                <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Notification - Event");?>
</strong></td>
                                <td><?php echo $_smarty_tpl->tpl_vars['data']->value['event_name'];?>
</td>
                            </tr>
                            <?php if ($_smarty_tpl->tpl_vars['data']->value['for_teacher']) {?>
                                <tr>
                                    <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Scope");?>
</strong></td>
                                    <td><?php echo __('Teacher - Employee');?>
</td>
                                </tr>
                            <?php } else { ?>
                                <tr>
                                    <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Event level");?>
</strong></td>
                                    <td>
                                        <?php if ($_smarty_tpl->tpl_vars['data']->value['level'] == @constant('SCHOOL_LEVEL')) {?>
                                            <?php echo __("School");?>

                                        <?php } elseif ($_smarty_tpl->tpl_vars['data']->value['level'] == @constant('CLASS_LEVEL_LEVEL')) {?>
                                            <?php echo $_smarty_tpl->tpl_vars['data']->value['class_level_name'];?>

                                        <?php } elseif ($_smarty_tpl->tpl_vars['data']->value['level'] == @constant('CLASS_LEVEL')) {?>
                                            <?php echo $_smarty_tpl->tpl_vars['data']->value['group_title'];?>

                                        <?php }?>
                                    </td>
                                </tr>
                            <?php }?>
                            <?php if ($_smarty_tpl->tpl_vars['canEdit']->value) {?>
                                <tr>
                                    <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Post on page");?>
?</strong></td>
                                    <td><?php if ($_smarty_tpl->tpl_vars['data']->value['post_on_wall']) {
echo __('Post on page');
} else {
echo __('Unpost on wall');
}?></td>
                                </tr>
                                <tr>
                                    <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Notify immediately");?>
?</strong></td>
                                    <td><?php if ($_smarty_tpl->tpl_vars['data']->value['is_notified']) {
echo __('Notified');
} else {
echo __('Not notified yet');
}?></td>
                                </tr>
                            <?php }?>
                            
                                
                                    
                                    
                                        
                                    
                                
                            
                            <?php if ($_smarty_tpl->tpl_vars['data']->value['must_register'] == '1') {?>
                                
                                    
                                    
                                
                                
                                    
                                    
                                
                                <tr>
                                    <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Must register");?>
?</strong></td>
                                    <td><?php echo __('Yes');?>
</td>
                                </tr>
                                <?php if ($_smarty_tpl->tpl_vars['data']->value['registration_deadline'] != '') {?>
                                    <tr>
                                        <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Registration deadline");?>
</strong></td>
                                        <td><font color="red"><?php echo $_smarty_tpl->tpl_vars['data']->value['registration_deadline'];?>
</font></td>
                                    </tr>
                                <?php }?>
                                <?php if (!$_smarty_tpl->tpl_vars['data']->value['for_teacher']) {?>
                                    <tr>
                                        <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("For child");?>
?</strong></td>
                                        <td><?php if ($_smarty_tpl->tpl_vars['data']->value['for_child'] == '1') {
echo __('Yes');
} else {
echo __('No');
}?></td>
                                    </tr>
                                    <?php if ($_smarty_tpl->tpl_vars['data']->value['for_child'] == '1') {?>
                                        <tr>
                                            <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Children total");?>
</strong></td>
                                            <td><?php echo $_smarty_tpl->tpl_vars['data']->value['child_participants'];?>
</td>
                                        </tr>
                                    <?php }?>
                                    <tr>
                                        <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("For parent");?>
?</strong></td>
                                        <td><?php if ($_smarty_tpl->tpl_vars['data']->value['for_parent'] == '1') {
echo __('Yes');
} else {
echo __('No');
}?></td>
                                    </tr>
                                    <?php if ($_smarty_tpl->tpl_vars['data']->value['for_parent'] == '1') {?>
                                        <tr>
                                            <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Parent total");?>
</strong></td>
                                            <td><?php echo $_smarty_tpl->tpl_vars['data']->value['parent_participants'];?>
</td>
                                        </tr>
                                    <?php }?>
                                <?php } else { ?>
                                    <tr>
                                        <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Teacher total");?>
</strong></td>
                                        <td><?php echo $_smarty_tpl->tpl_vars['data']->value['teacher_participants'];?>
</td>
                                    </tr>
                                <?php }?>
                            <?php }?>
                            <tr>
                                <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Creator");?>
</strong></td>
                                <td><?php echo $_smarty_tpl->tpl_vars['data']->value['user_fullname'];?>
</td>
                            </tr>
                            <tr>
                                <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Created time");?>
</strong></td>
                                <td><?php echo $_smarty_tpl->tpl_vars['data']->value['created_at'];?>
</td>
                            </tr>
                            <?php if ($_smarty_tpl->tpl_vars['data']->value['updated_at'] != '') {?>
                                <tr>
                                    <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Last updated");?>
</strong></td>
                                    <td><?php echo $_smarty_tpl->tpl_vars['data']->value['updated_at'];?>
</td>
                                </tr>
                            <?php }?>
                            <?php if ($_smarty_tpl->tpl_vars['data']->value['for_teacher'] && $_smarty_tpl->tpl_vars['data']->value['must_register'] && ($_smarty_tpl->tpl_vars['school']->value['page_admin'] != $_smarty_tpl->tpl_vars['user']->value->_data['user_id'])) {?>
                                <tr>
                                    <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Register");?>
</strong></td>
                                    <td>
                                        <?php if ($_smarty_tpl->tpl_vars['data']->value['for_teacher']) {?>
                                            <input type="checkbox" name="teacherId" value="<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_id'];?>
"
                                                   <?php if ($_smarty_tpl->tpl_vars['data']->value['participants']['teacher']['is_registered']) {?>checked<?php }?> <?php if ($_smarty_tpl->tpl_vars['data']->value['can_register'] == 0) {?>disabled<?php }?>> <?php echo __("You");?>

                                            <?php if ($_smarty_tpl->tpl_vars['data']->value['participants']['teacher']['is_registered']) {?>
                                                <input type="hidden" name="old_teacher_id" value="<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_id'];?>
"/>
                                            <?php }?>
                                        <?php }?>
                                    </td>
                                </tr>
                            <?php }?>
                            <tr style="background: #fff">
                                
                                <td colspan="2"><?php echo $_smarty_tpl->tpl_vars['data']->value['description'];?>
</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-1">
                        <?php if ($_smarty_tpl->tpl_vars['data']->value['can_register'] > 0 && $_smarty_tpl->tpl_vars['data']->value['must_register'] && ($_smarty_tpl->tpl_vars['school']->value['page_admin'] != $_smarty_tpl->tpl_vars['user']->value->_data['user_id']) && $_smarty_tpl->tpl_vars['data']->value['for_teacher']) {?>
                            <button type="submit" class="btn btn-primary padrl30"><?php echo __("Save");?>
</button>
                        <?php }?>
                    </div>
                </div>
                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->
                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <br/>
            </form>
            <div class="form-group">
                <a class="btn btn-default" href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/events"><?php echo __("Lists");?>
</a>
                <?php if ($_smarty_tpl->tpl_vars['data']->value['must_register']) {?>
                    <?php if (!$_smarty_tpl->tpl_vars['data']->value['for_teacher']) {?>
                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/events/participants/<?php echo $_smarty_tpl->tpl_vars['data']->value['event_id'];?>
" class="btn btn-default"><?php echo __("Participants");?>
</a>
                    <?php } else { ?>
                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/events/participantsforemployee/<?php echo $_smarty_tpl->tpl_vars['data']->value['event_id'];?>
" class="btn btn-default"><?php echo __("Participants");?>
</a>
                    <?php }?>
                <?php }?>
                <?php if ($_smarty_tpl->tpl_vars['canEdit']->value && ($_smarty_tpl->tpl_vars['data']->value['status'] != @constant('EVENT_STATUS_CANCELLED'))) {?>
                    <?php if ((!$_smarty_tpl->tpl_vars['data']->value['is_notified'])) {?>
                        <a class="btn btn-default js_school-event-notify" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['data']->value['event_id'];?>
"><?php echo __("Notify");?>
</a>
                    <?php }?>
                    <a class="btn btn-default" href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/events/edit/<?php echo $_smarty_tpl->tpl_vars['data']->value['event_id'];?>
"><?php echo __("Edit");?>
</a>
                    <?php if ((($_smarty_tpl->tpl_vars['data']->value['parent_participants']+$_smarty_tpl->tpl_vars['data']->value['child_participants']+$_smarty_tpl->tpl_vars['data']->value['teacher_participants']) == 0) && (!$_smarty_tpl->tpl_vars['data']->value['is_notified'])) {?>
                        <a class="btn btn-danger js_school-delete" data-handle="event" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['data']->value['event_id'];?>
"><?php echo __("Delete");?>
</a>
                    <?php } else { ?>
                        <a class="btn btn-warning js_school-event-cancel" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['data']->value['event_id'];?>
"><?php echo __("Cancel");?>
</a>
                    <?php }?>
                <?php }?>
            </div>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "participants") {?>
        <div class="panel-body with-table">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_event.php">
                <input type="hidden" name="school_username" id="school_username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                <input type="hidden" name="event_id" id="event_id" value="<?php echo $_smarty_tpl->tpl_vars['event']->value['event_id'];?>
"/>
                <input type="hidden" name="event_name" id="event_name" value="<?php echo $_smarty_tpl->tpl_vars['event']->value['event_name'];?>
"/>
                <input type="hidden" name="do" value="add_pp"/>

                <?php if (($_smarty_tpl->tpl_vars['event']->value['level'] != @constant('CLASS_LEVEL')) && ($_smarty_tpl->tpl_vars['event']->value['level'] != @constant('TEACHER_LEVEL'))) {?>
                    <div class="">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th><?php echo __("Class");?>
</th>
                                    <th><?php echo __("Number of student involved");?>
</th>
                                    <th><?php echo __("Number of parents involved");?>
</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['eventPars']->value, 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
                                    <tr>
                                        <td align="center"><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</td>
                                        <td width="50%"><?php echo $_smarty_tpl->tpl_vars['row']->value['group_title'];?>
</td>
                                        <td align="center"><?php echo $_smarty_tpl->tpl_vars['row']->value['countChild'];?>
</td>
                                        <td align="center"><?php echo $_smarty_tpl->tpl_vars['row']->value['countParent'];?>
</td>
                                    </tr>
                                    <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                <tr>
                                    <td colspan="2" align="right"><strong><?php echo __("Total");?>
</strong></td>
                                    <td align="center"><strong><?php echo $_smarty_tpl->tpl_vars['childCount']->value;?>
</strong></td>
                                    <td align="center"><strong><?php echo $_smarty_tpl->tpl_vars['parentCount']->value;?>
</strong></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <hr/>
                    <div class="form-group">
                        <label class="col-sm-5 control-label text-left"><?php echo __("Select a class to see participant list");?>
&nbsp;</label>
                        <div class='col-sm-5'>
                            <div class="form-group">
                                <select name="class_id" id="participants_class_id" class="form-control" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-uid="<?php echo $_smarty_tpl->tpl_vars['event']->value['event_id'];?>
">
                                    <option value=""><?php echo __("Select Class");?>
</option>

                                    <?php $_smarty_tpl->_assignInScope('class_level', -1);
?>
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['classes']->value, 'class');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class']->value) {
?>
                                        <?php if (($_smarty_tpl->tpl_vars['class_level']->value != $_smarty_tpl->tpl_vars['class']->value['class_level_id'])) {?>
                                            <option value="" disabled style="color: blue">-----<?php echo $_smarty_tpl->tpl_vars['class']->value['class_level_name'];?>
-----</option>
                                        <?php }?>
                                        <option value="<?php echo $_smarty_tpl->tpl_vars['class']->value['group_id'];?>
" <?php if ($_smarty_tpl->tpl_vars['class']->value['group_id'] == $_smarty_tpl->tpl_vars['classId']->value) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['class']->value['group_title'];?>
</option>
                                        <?php $_smarty_tpl->_assignInScope('class_level', $_smarty_tpl->tpl_vars['class']->value['class_level_id']);
?>
                                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                </select>
                            </div>
                        </div>
                    </div>
                <?php } elseif ($_smarty_tpl->tpl_vars['event']->value['level'] == @constant('CLASS_LEVEL')) {?>
                    <input type="hidden" name="class_id" id="class_id" value="<?php echo $_smarty_tpl->tpl_vars['event']->value['class_id'];?>
"/>
                <?php }?>
                <?php if (($_smarty_tpl->tpl_vars['event']->value['can_register'] == 1) && $_smarty_tpl->tpl_vars['canEdit']->value) {?>
                    <?php if (($_smarty_tpl->tpl_vars['event']->value['level'] != @constant('TEACHER_LEVEL'))) {?>
                        <div id="event_select_panel" <?php if (($_smarty_tpl->tpl_vars['event']->value['level'] != @constant('CLASS_LEVEL'))) {?>class="x-hidden"<?php }?>>
                            <?php if ($_smarty_tpl->tpl_vars['event']->value['for_child'] && $_smarty_tpl->tpl_vars['event']->value['for_parent']) {?>
                                <input type="checkbox" id="eventCheckAll">&nbsp;<?php echo __("All");?>

                            <?php }?>
                            <?php if ($_smarty_tpl->tpl_vars['event']->value['for_child']) {?>
                                &nbsp;&nbsp;<input type="checkbox" id="eventCheckAllChildren">&nbsp;<?php echo __("All children");?>

                            <?php }?>
                            <?php if ($_smarty_tpl->tpl_vars['event']->value['for_parent']) {?>
                                &nbsp;&nbsp;<input type="checkbox" id="eventCheckAllParent">&nbsp;<?php echo __("All parent");?>

                            <?php }?>
                        </div>
                    <?php } else { ?>
                        <input type="checkbox" id="eventCheckAllTeacher">&nbsp;<?php echo __("All teachers");?>

                    <?php }?>
                <?php }?>
                <div class="table-responsive" id="participant_list">
                    <?php if ($_smarty_tpl->tpl_vars['event']->value['level'] == @constant('CLASS_LEVEL')) {?>
                        <?php $_smarty_tpl->_subTemplateRender("file:ci/school/ajax.eventparticipants.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                    <?php } elseif ($_smarty_tpl->tpl_vars['event']->value['level'] == @constant('TEACHER_LEVEL')) {?>
                        <?php $_smarty_tpl->_subTemplateRender("file:ci/school/ajax.event.teacherparticipants.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                    <?php }?>
                </div>

                
                <?php if ($_smarty_tpl->tpl_vars['canEdit']->value) {?>
                    <?php if ((($_smarty_tpl->tpl_vars['event']->value['level'] != @constant('CLASS_LEVEL')) || ($_smarty_tpl->tpl_vars['event']->value['level'] != @constant('TEACHER_LEVEL')) || (count($_smarty_tpl->tpl_vars['participants']->value) > 0)) && ($_smarty_tpl->tpl_vars['event']->value['can_register'] == 1)) {?>
                        <div class="form-group pl5" id="participant_btnSave">
                            <div class="col-sm-9">
                                <button type="submit" class="btn btn-primary padrl30" <?php if (($_smarty_tpl->tpl_vars['event']->value['level'] != @constant('CLASS_LEVEL')) && ($_smarty_tpl->tpl_vars['event']->value['level'] != @constant('TEACHER_LEVEL'))) {?>disabled<?php }?>><?php echo __("Save");?>
</button>
                            </div>
                        </div>
                    <?php } elseif ($_smarty_tpl->tpl_vars['event']->value['can_register'] == 0) {?>
                        <div class="form-group pl5">
                            <div class="col-sm-9">
                                <?php echo __("The registration deadline has passed or the event has been cancelled");?>
.
                            </div>
                        </div>
                    <?php }?>
                <?php }?>
                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "participantsforemployee") {?>
        <div class="panel-body with-table">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_event.php">
                <input type="hidden" name="school_username" id="school_username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                <input type="hidden" name="event_id" id="event_id" value="<?php echo $_smarty_tpl->tpl_vars['event']->value['event_id'];?>
"/>
                <input type="hidden" name="event_name" id="event_name" value="<?php echo $_smarty_tpl->tpl_vars['event']->value['event_name'];?>
"/>
                <input type="hidden" name="do" value="add_pp_for_employee"/>
                <?php if (($_smarty_tpl->tpl_vars['event']->value['can_register'] == 1) && $_smarty_tpl->tpl_vars['canEdit']->value) {?>
                    <input type="checkbox" id="eventCheckAllTeacher">&nbsp;<?php echo __("All teachers");?>

                <?php }?>
                <div class="table-responsive" id="participant_list">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th><?php echo __("Select");?>
</th>
                            <th><?php echo __("Teacher");?>
</th>
                            <th><?php echo __("Registed time");?>
</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['participants']->value, 'teacher');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['teacher']->value) {
?>
                            <tr>
                                <td align="center"><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</td>
                                <td align="center">
                                    <input type="checkbox" class="teacher" name="teacherIds[]" value="<?php echo $_smarty_tpl->tpl_vars['teacher']->value['user_id'];?>
"
                                           <?php if ($_smarty_tpl->tpl_vars['teacher']->value['event_id'] > 0) {?>checked<?php }?> <?php if (!$_smarty_tpl->tpl_vars['canEdit']->value) {?>disabled<?php }?>>
                                    <?php if ($_smarty_tpl->tpl_vars['teacher']->value['event_id'] > 0) {?>
                                        <input type="hidden" name="oldTeacherIds[]" value="<?php echo $_smarty_tpl->tpl_vars['teacher']->value['user_id'];?>
"/>
                                    <?php }?>
                                </td>
                                <td><?php echo $_smarty_tpl->tpl_vars['teacher']->value['user_fullname'];?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['teacher']->value['created_at'];?>
</td>
                            </tr>

                            <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </tbody>
                    </table>
                </div>

                <?php if ($_smarty_tpl->tpl_vars['canEdit']->value) {?>
                    <?php if (($_smarty_tpl->tpl_vars['event']->value['for_teacher'] || (count($_smarty_tpl->tpl_vars['participants']->value) > 0)) && ($_smarty_tpl->tpl_vars['event']->value['can_register'] == 1)) {?>
                        <div class="form-group pl5" id="participant_btnSave">
                            <div class="col-sm-9">
                                <?php if ($_smarty_tpl->tpl_vars['event']->value['for_teacher'] && $_smarty_tpl->tpl_vars['event']->value['must_register'] && ($_smarty_tpl->tpl_vars['event']->value['can_register'] > 0)) {?>
                                    <button type="submit" class="btn btn-primary padrl30"><?php echo __("Save");?>
</button>
                                <?php }?>
                            </div>
                        </div>
                    <?php } elseif ($_smarty_tpl->tpl_vars['event']->value['can_register'] == 0) {?>
                        <div class="form-group pl5">
                            <div class="col-sm-9">
                                <?php echo __("The registration deadline has passed or the event has been cancelled");?>
.
                            </div>
                        </div>
                    <?php }?>
                <?php }?>
                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "edit") {?>
        <div class="panel-body">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_event.php">
                <input type="hidden" name="school_username" id="school_username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                <input type="hidden" name="event_id" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['event_id'];?>
"/>
                <input type="hidden" name="post_ids" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['post_ids'];?>
"/>
                <input type="hidden" name="do" value="edit"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Notification - Event");?>
 (*)</label>
                    <div class="col-sm-9">
                        <input class="form-control" name="event_name" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['event_name'];?>
" required autofocus maxlength="250">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Description");?>
 (*)</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="description" id="description" rows="8" required><?php echo $_smarty_tpl->tpl_vars['data']->value['description'];?>
</textarea>
                        <?php echo '<script'; ?>
>
                            CKEDITOR.replace('description');
                        <?php echo '</script'; ?>
>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("For teachers/employees");?>
?</label>
                    <div class="col-sm-9">
                        <div class="onoffswitch">
                            <input type="checkbox" name="for_teacher" class="onoffswitch-checkbox" id="for_teacher" <?php if ($_smarty_tpl->tpl_vars['data']->value['for_teacher']) {?>checked<?php }?>>
                            <label class="onoffswitch-label" for="for_teacher"></label>
                        </div>
                    </div>
                </div>
                <div class="form-group <?php if ($_smarty_tpl->tpl_vars['data']->value['for_teacher']) {?>x-hidden<?php }?>" id="level">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Event level");?>
 (*)</label>
                    
                        <div class="col-sm-3">
                            <select name="event_level" id="event_level" class="form-control">
                                <option value="<?php echo @constant('SCHOOL_LEVEL');?>
" <?php if ($_smarty_tpl->tpl_vars['data']->value['level'] == @constant('SCHOOL_LEVEL')) {?>selected<?php }?>><?php echo __("School");?>
</option>
                                <option value="<?php echo @constant('CLASS_LEVEL_LEVEL');?>
" <?php if ($_smarty_tpl->tpl_vars['data']->value['level'] == @constant('CLASS_LEVEL_LEVEL')) {?>selected<?php }?>><?php echo __("Class level");?>
</option>
                                <option value="<?php echo @constant('CLASS_LEVEL');?>
" <?php if ($_smarty_tpl->tpl_vars['data']->value['level'] == @constant('CLASS_LEVEL')) {?>selected<?php }?>><?php echo __("Class");?>
</option>
                                
                            </select>
                        </div>
                        <div class="col-sm-3 <?php if ($_smarty_tpl->tpl_vars['data']->value['level'] != @constant('CLASS_LEVEL_LEVEL')) {?>x-hidden<?php }?>" name="event_class_level">
                            <select name="class_level_id" id="class_level_id" class="form-control">
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['class_levels']->value, 'class_level');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class_level']->value) {
?>
                                    <option value="<?php echo $_smarty_tpl->tpl_vars['class_level']->value['class_level_id'];?>
" <?php if ($_smarty_tpl->tpl_vars['data']->value['class_level_id'] == $_smarty_tpl->tpl_vars['class_level']->value['class_level_id']) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['class_level']->value['class_level_name'];?>
</option>
                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                            </select>
                        </div>
                        <div class="col-sm-3 <?php if ($_smarty_tpl->tpl_vars['data']->value['level'] != @constant('CLASS_LEVEL')) {?>x-hidden<?php }?>" name="event_class">
                            <select name="class_id" id="class_id" class="form-control">
                                <?php $_smarty_tpl->_assignInScope('class_level', -1);
?>
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['classes']->value, 'class');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class']->value) {
?>
                                    <?php if (($_smarty_tpl->tpl_vars['class_level']->value != $_smarty_tpl->tpl_vars['class']->value['class_level_id'])) {?>
                                        <option value="" disabled style="color: blue">-----<?php echo $_smarty_tpl->tpl_vars['class']->value['class_level_name'];?>
-----</option>
                                    <?php }?>
                                    <option value="<?php echo $_smarty_tpl->tpl_vars['class']->value['group_id'];?>
" <?php if ($_smarty_tpl->tpl_vars['data']->value['class_id'] == $_smarty_tpl->tpl_vars['class']->value['group_id']) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['class']->value['group_title'];?>
</option>
                                    <?php $_smarty_tpl->_assignInScope('class_level', $_smarty_tpl->tpl_vars['class']->value['class_level_id']);
?>
                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                            </select>
                        </div>
                    
                </div>
                <div class="form-group" name="event_post_on_wall">
                    <label class="col-sm-3 control-label text-left">
                        <?php echo __("Post on page");?>
?
                    </label>
                    <div class="col-sm-9">
                        <div class="onoffswitch">
                            <input type="checkbox" name="post_on_wall" class="onoffswitch-checkbox" id="post_on_wall" <?php if ($_smarty_tpl->tpl_vars['data']->value['post_on_wall'] == '1') {?>checked<?php }?>>
                            <label class="onoffswitch-label" for="post_on_wall"></label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">
                        <?php echo __("Notify immediately");?>
?
                    </label>
                    <div class="col-sm-9">
                        <div class="onoffswitch">
                            <input type="checkbox" name="notify_immediately" class="onoffswitch-checkbox" id="notify_immediately" <?php if ($_smarty_tpl->tpl_vars['data']->value['is_notified'] == '1') {?>checked<?php }?>>
                            <label class="onoffswitch-label" for="notify_immediately"></label>
                        </div>
                    </div>
                </div>
                
                    
                    
                        
                            
                                
                                    
                                        
                                    
                            
                        

                        
                            
                                
                                    
                                        
                                    
                            
                        
                    
                
                <?php if ($_smarty_tpl->tpl_vars['data']->value['must_register'] == '0') {?>
                    <div class="form-group" id="event_more_information_button">
                        <div class="col-sm-12">
                            <a href="#" class="btn btn-default pull-right js_event-more-information"><?php echo __("More information");?>
</a>
                        </div>
                    </div>
                <?php }?>
                <div <?php if ($_smarty_tpl->tpl_vars['data']->value['must_register'] == '0') {?>class="x-hidden"<?php }?> id="event_more_information">
                    
                        
                        
                            
                        
                    
                    
                        
                        
                            
                                
                                
                            
                        
                    

                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left"><?php echo __("Must register");?>
?</label>
                        <div class="col-sm-9">
                            <div class="onoffswitch">
                                <input type="checkbox" name="must_register" class="onoffswitch-checkbox" id="must_register" <?php if ($_smarty_tpl->tpl_vars['data']->value['must_register'] == '1') {?>checked<?php }?>>
                                <label class="onoffswitch-label" for="must_register"></label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group <?php if (!$_smarty_tpl->tpl_vars['data']->value['must_register']) {?>x-hidden<?php }?>" id="event_registration_deadline">
                        <label class="col-sm-3 control-label text-left"><?php echo __("Registration deadline");?>
 (*)</label>
                        <div class='col-sm-4'>
                            <div class='input-group date' id='registration_deadlinepicker'>
                                <input type='text' name="registration_deadline" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['registration_deadline'];?>
" class="form-control registration_deadline" placeholder="<?php echo __("Registration deadline");?>
"/>
                                <span class="input-group-addon">
                                    <span class="fas fa-calendar-alt"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group <?php if (($_smarty_tpl->tpl_vars['data']->value['for_teacher'] || ($_smarty_tpl->tpl_vars['data']->value['must_register'] == 0))) {?>x-hidden<?php }?>" id="event_for_child">
                        <label class="col-sm-3 control-label text-left"><?php echo __("For child");?>
?</label>
                        <div class="col-sm-9">
                            <div class="onoffswitch">
                                <input type="checkbox" name="for_child" class="onoffswitch-checkbox" id="for_child" <?php if ($_smarty_tpl->tpl_vars['data']->value['for_child'] == '1') {?>checked<?php }?>>
                                <label class="onoffswitch-label" for="for_child"></label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group <?php if (($_smarty_tpl->tpl_vars['data']->value['for_teacher'] || ($_smarty_tpl->tpl_vars['data']->value['must_register'] == 0))) {?>x-hidden<?php }?>" id="event_for_parent">
                        <label class="col-sm-3 control-label text-left"><?php echo __("For parent");?>
?</label>
                        <div class="col-sm-9">
                            <div class="onoffswitch">
                                <input type="checkbox" name="for_parent" class="onoffswitch-checkbox" id="for_parent" <?php if ($_smarty_tpl->tpl_vars['data']->value['for_parent'] == '1') {?>checked<?php }?>>
                                <label class="onoffswitch-label" for="for_parent"></label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30"><?php echo __("Save");?>
</button>
                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/events" class="btn btn-default"><?php echo __("Lists");?>
</a>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
        <div class="panel-body">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_event.php">
                <input type="hidden" name="school_username" id="school_username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                <input type="hidden" name="do" value="add"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Notification - Event");?>
 (*)</label>
                    <div class="col-sm-9">
                        <input class="form-control" name="event_name" required autofocus maxlength="250">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Description");?>
 (*)</label>
                    <label class="col-sm-9 control-label" style="text-align: left!important;">
                        <strong style="color: orangered"><?php echo __("Lưu ý: Muốn căn giữa cho ảnh trong mô tả, bạn vui lòng căn giữa trước khi đăng ảnh");?>
</strong>
                    </label>
                </div>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <textarea class="form-control" name="description" id="description" required></textarea>
                        <?php echo '<script'; ?>
>
                            CKEDITOR.replace('description');
                        <?php echo '</script'; ?>
>
                    </div>
                </div>
                
                    
                    
                        
                    
                
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("For teachers/employees");?>
?</label>
                    <div class="col-sm-9">
                        <div class="onoffswitch">
                            <input type="checkbox" name="for_teacher" class="onoffswitch-checkbox" id="for_teacher">
                            <label class="onoffswitch-label" for="for_teacher"></label>
                        </div>
                    </div>
                </div>
                <div class="form-group" id = "level">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Event level");?>
 (*)</label>
                    
                        <div class="col-sm-3">
                            <select name="event_level" id="event_level" class="form-control">
                                <option value="<?php echo @constant('SCHOOL_LEVEL');?>
"><?php echo __("School");?>
</option>
                                <option value="<?php echo @constant('CLASS_LEVEL_LEVEL');?>
"><?php echo __("Class level");?>
</option>
                                <option value="<?php echo @constant('CLASS_LEVEL');?>
"><?php echo __("Class");?>
</option>
                                
                            </select>
                        </div>
                        <div class="col-sm-3 x-hidden" name="event_class_level">
                            <select name="class_level_id" id="class_level_id" class="form-control">
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['class_levels']->value, 'class_level');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class_level']->value) {
?>
                                    <option value="<?php echo $_smarty_tpl->tpl_vars['class_level']->value['class_level_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['class_level']->value['class_level_name'];?>
</option>
                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                            </select>
                        </div>
                        <div class="col-sm-3 x-hidden" name="event_class">
                            <select name="class_id" id="class_id" class="form-control">
                                <?php $_smarty_tpl->_assignInScope('class_level', -1);
?>
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['classes']->value, 'class');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class']->value) {
?>
                                    <?php if (($_smarty_tpl->tpl_vars['class_level']->value != $_smarty_tpl->tpl_vars['class']->value['class_level_id'])) {?>
                                        <option value="" disabled style="color: blue">-----<?php echo $_smarty_tpl->tpl_vars['class']->value['class_level_name'];?>
-----</option>
                                    <?php }?>
                                    <option value="<?php echo $_smarty_tpl->tpl_vars['class']->value['group_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['class']->value['group_title'];?>
</option>
                                    <?php $_smarty_tpl->_assignInScope('class_level', $_smarty_tpl->tpl_vars['class']->value['class_level_id']);
?>
                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                            </select>
                        </div>
                    
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Post on page");?>
?</label>
                    <div class="col-sm-9">
                        <div class="onoffswitch">
                            <input type="checkbox" name="post_on_wall" class="onoffswitch-checkbox" id="post_on_wall" checked>
                            <label class="onoffswitch-label" for="post_on_wall"></label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Notify immediately");?>
?</label>
                    <div class="col-sm-9">
                        <div class="onoffswitch">
                            <input type="checkbox" name="notify_immediately" class="onoffswitch-checkbox" id="notify_immediately" checked>
                            <label class="onoffswitch-label" for="notify_immediately"></label>
                        </div>
                    </div>
                </div>
                
                    
                    
                        
                            
                                
                                    
                                        
                                    
                            
                        

                        
                            
                                
                                    
                                        
                                    
                            
                        
                    
                
                <div class="form-group" id="event_more_information_button">
                    <div class="col-sm-12">
                        <a href="#" class="btn btn-default pull-right js_event-more-information"><?php echo __("More information");?>
</a>
                    </div>
                </div>
                <div class="x-hidden" id="event_more_information">
                    
                        
                        
                            
                        
                    
                    
                        
                        
                            
                                
                                
                            
                        
                    

                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left"><?php echo __("Must register");?>
?</label>
                        <div class="col-sm-9">
                            <div class="onoffswitch">
                                <input type="checkbox" name="must_register" class="onoffswitch-checkbox" id="must_register">
                                <label class="onoffswitch-label" for="must_register"></label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group x-hidden" id="event_registration_deadline">
                        <label class="col-sm-3 control-label text-left"><?php echo __("Registration deadline");?>
 (*)</label>
                        <div class='col-sm-4'>
                            <div class='input-group date' id='registration_deadlinepicker'>
                                <input type='text' name="registration_deadline" class="form-control registration_deadline" placeholder="<?php echo __("Registration deadline");?>
"/>
                                <span class="input-group-addon">
                                    <span class="fas fa-calendar-alt"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group x-hidden" id="event_for_child">
                        <label class="col-sm-3 control-label text-left"><?php echo __("For child");?>
?</label>
                        <div class="col-sm-9">
                            <div class="onoffswitch">
                                <input type="checkbox" name="for_child" class="onoffswitch-checkbox" id="for_child">
                                <label class="onoffswitch-label" for="for_child"></label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group x-hidden" id="event_for_parent">
                        <label class="col-sm-3 control-label text-left"><?php echo __("For parent");?>
?</label>
                        <div class="col-sm-9">
                            <div class="onoffswitch">
                                <input type="checkbox" name="for_parent" class="onoffswitch-checkbox" id="for_parent">
                                <label class="onoffswitch-label" for="for_parent"></label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30"><?php echo __("Save");?>
</button>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    <?php }?>
</div><?php }
}
