<?php
/* Smarty version 3.1.31, created on 2021-06-03 17:08:27
  from "D:\workplace\Inet-project\source\Server11\content\themes\inet\templates\ci\class\class.conducts.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_60b8aa1bf13e91_58320624',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'd2bd8a57655f4a1a375399a5439e3ae2d4195e00' => 
    array (
      0 => 'D:\\workplace\\Inet-project\\source\\Server11\\content\\themes\\inet\\templates\\ci\\class\\class.conducts.tpl',
      1 => 1622714897,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60b8aa1bf13e91_58320624 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <i class="fa fa-gavel fa-fw fa-lg pr10"></i>
        <?php echo __("Conduct");?>

        <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == "edit") {?>
            &rsaquo; <?php echo $_smarty_tpl->tpl_vars['child']->value['child_name'];?>

            <div class="pull-right flip">
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/conducts" class="btn btn-default">
                    <i class="fa fa-list-ul"></i><?php echo __("Lists");?>

                </a>
            </div>
        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
            &rsaquo; <?php echo __("Lists");?>

        <?php }?>
    </div>
    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
        <div class="panel-body with-table">
            <div class="table-responsive">
                <div><strong><?php echo __("Children list");?>
&nbsp;(<?php echo count($_smarty_tpl->tpl_vars['rows']->value);?>
) </strong></div>
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th><?php echo __("#");?>
</th>
                        <th><?php echo __("Full name");?>
</th>
                        <th><?php echo __("Gender");?>
</th>
                        <th><?php echo __("Birthdate");?>
</th>
                        <th><?php echo __("Semester");?>
 1</th>
                        <th><?php echo __("Semester");?>
 2</th>
                        <th><?php echo __("End semester");?>
</th>
                        <th><?php echo __("Actions");?>
</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['rows']->value, 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
                            <tr>
                                <td align="center" class="align-middle"> <?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
 </td>
                                <td class="align-middle"><a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/children/detail/<?php echo $_smarty_tpl->tpl_vars['row']->value['child_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['row']->value['child_name'];?>
</a></td>
                                <td align="center" class="align-middle"><?php if ($_smarty_tpl->tpl_vars['row']->value['gender'] == @constant('MALE')) {
echo __("Male");
} else {
echo __("Female");
}?></td>
                                <td align="center" class="align-middle"><?php echo $_smarty_tpl->tpl_vars['row']->value['birthday'];?>
</td>
                                <td align="center" class="align-middle"><?php echo $_smarty_tpl->tpl_vars['row']->value['conduct1'];?>
</td>
                                <td align="center" class="align-middle"><?php echo $_smarty_tpl->tpl_vars['row']->value['conduct2'];?>
</td>
                                <td align="center" class="align-middle"><?php echo $_smarty_tpl->tpl_vars['row']->value['conduct3'];?>
</td>

                                <td align = "center" class="align-middle">
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/conducts/edit/<?php echo $_smarty_tpl->tpl_vars['row']->value['conduct_id'];?>
" class="btn btn-xs btn-default"><?php echo __("Edit");?>
</a>
                                </td>
                            </tr>
                            <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>


                        <?php if (count($_smarty_tpl->tpl_vars['rows']->value) == 0) {?>
                            <tr class="odd">
                                <td valign="top" align="center" colspan="7" class="dataTables_empty">
                                    <?php echo __("No data available in table");?>

                                </td>
                            </tr>
                        <?php }?>
                    </tbody>
                </table>
            </div>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "edit") {?>
        <div class="panel-body">
            <form class="form-horizontal" enctype="multipart/form-data" method="post" id="class_edit_conduct">
                <input type="hidden" name="username" id="username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                <input type="hidden" name="conduct_id" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['conduct_id'];?>
"/>
                <input type="hidden" name="child_id" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
"/>
                <input type="hidden" name="do" value="edit"/>
                <div class = "form-group">
                    <label class="col-sm-3 control-label text-left">
                        <?php echo __("Student");?>

                    </label>
                    <div class = "col-sm-9">
                        <input type="text" class="form-control" value = "<?php echo $_smarty_tpl->tpl_vars['child']->value['child_name'];?>
 - <?php echo $_smarty_tpl->tpl_vars['child']->value['birthday'];?>
" disabled>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Semester");?>
 1
                    </label>
                    <div class="col-sm-9">
                        <select name="conduct_id1" id="conduct_id1" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-view="edit" class="form-control">
                            <option value=""><?php echo __("Select conduct");?>
</option>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['conducts']->value, 'conduct');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['conduct']->value) {
?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['conduct']->value;?>
" <?php if ($_smarty_tpl->tpl_vars['data']->value['conduct1'] == $_smarty_tpl->tpl_vars['conduct']->value) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['conduct']->value;?>
</option>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>





                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Semester");?>
 2
                    </label>
                    <div class="col-sm-9">
                        <select name="conduct_id2" id="conduct_id2" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-view="edit" class="form-control">
                            <option value=""><?php echo __("Select conduct");?>
</option>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['conducts']->value, 'conduct');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['conduct']->value) {
?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['conduct']->value;?>
" <?php if ($_smarty_tpl->tpl_vars['data']->value['conduct2'] == $_smarty_tpl->tpl_vars['conduct']->value) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['conduct']->value;?>
</option>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("End semester");?>

                    </label>
                    <div class="col-sm-9">
                        <select name="conduct_id3" id="conduct_id3" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-view="edit" class="form-control">
                            <option value=""><?php echo __("Select conduct");?>
</option>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['conducts']->value, 'conduct');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['conduct']->value) {
?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['conduct']->value;?>
" <?php if ($_smarty_tpl->tpl_vars['data']->value['conduct3'] == $_smarty_tpl->tpl_vars['conduct']->value) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['conduct']->value;?>
</option>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30"><?php echo __("Save");?>
</button>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    <?php }?>
</div>
<?php }
}
