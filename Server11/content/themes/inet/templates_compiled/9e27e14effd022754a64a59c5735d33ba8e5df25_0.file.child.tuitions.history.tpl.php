<?php
/* Smarty version 3.1.31, created on 2021-03-31 10:47:20
  from "D:\workplace\Server11\content\themes\inet\templates\ci\child\child.tuitions.history.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_6063f0c83d2be6_49790432',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '9e27e14effd022754a64a59c5735d33ba8e5df25' => 
    array (
      0 => 'D:\\workplace\\Server11\\content\\themes\\inet\\templates\\ci\\child\\child.tuitions.history.tpl',
      1 => 1552404702,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6063f0c83d2be6_49790432 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="panel-body with-table">
    <table class="table table-bordered">
        <tr>
            <td class="col-sm-3 text-right"><?php echo __("Month");?>
</td>
            <td><?php echo $_smarty_tpl->tpl_vars['data']->value['month'];?>
</td>
        </tr>
        <tr>
            <td class="col-sm-3 text-right"><?php echo __("Child usage total");?>
&nbsp;(<?php echo @constant('MONEY_UNIT');?>
)</td>
            <td><?php echo moneyFormat($_smarty_tpl->tpl_vars['data']->value['child_total']);?>
</td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <a class="btn btn-default" href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/child/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
/tuitions/detail/<?php echo $_smarty_tpl->tpl_vars['data']->value['tuition_id'];?>
"><?php echo __("Tuition detail");?>
</a>
            </td>
        </tr>
    </table>
    <div class="table-responsive">
        <?php if ((count($_smarty_tpl->tpl_vars['data']->value['tuition_detail']['fees'])+count($_smarty_tpl->tpl_vars['data']->value['tuition_detail']['services'])) > 0) {?>
            <table class="table table-striped table-bordered table-hover bg_white">
                <thead>
                <tr>
                    <td colspan="6"><strong><?php echo __("Fee usage history of previous month");?>
 <?php echo $_smarty_tpl->tpl_vars['data']->value['pre_month'];?>
</strong>
                    </td>
                </tr>
                <tr>
                    <th>#</th>
                    <th><?php echo __("Fee name");?>
</th>
                    <th class="text-center"><?php echo __("Unit");?>
</th>
                    <th class="text-center"><?php echo __("Quantity");?>
</th>
                    <th class="text-right"><?php echo __("Unit price");?>
&nbsp;(<?php echo @constant('MONEY_UNIT');?>
)</th>
                    <th class="text-right"><?php echo __("Money amount");?>
&nbsp;(<?php echo @constant('MONEY_UNIT');?>
)</th>
                </tr>
                </thead>
                <tbody>
                
                <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['data']->value['tuition_detail']['fees'], 'detail');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['detail']->value) {
?>
                    <?php if ($_smarty_tpl->tpl_vars['detail']->value['to_money'] > 0) {?>
                        <tr>
                            <td align="center"><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</td>
                            <td><?php echo $_smarty_tpl->tpl_vars['detail']->value['fee_name'];?>
</td>
                            <td class="text-center"><?php if ($_smarty_tpl->tpl_vars['detail']->value['fee_type'] == @constant('FEE_TYPE_MONTHLY')) {
echo __('Monthly');
} else {
echo __('Daily');
}?></td>
                            <td class="text-center"><?php echo $_smarty_tpl->tpl_vars['detail']->value['quantity'];?>
</td>
                            <td class="text-right"><?php echo moneyFormat($_smarty_tpl->tpl_vars['detail']->value['unit_price']);?>
</td>
                            <td class="text-right"><?php echo moneyFormat($_smarty_tpl->tpl_vars['detail']->value['to_money']);?>
</td>
                        </tr>
                        <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                    <?php }?>
                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                
                <?php if (count($_smarty_tpl->tpl_vars['data']->value['tuition_detail']['services']) > 0) {?>
                    <tr><td colspan="6"><strong><?php echo __("Service");?>
</strong></td></tr>
                    <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['data']->value['tuition_detail']['services'], 'detail');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['detail']->value) {
?>
                        <?php if ($_smarty_tpl->tpl_vars['detail']->value['to_money'] > 0) {?>
                            <tr>
                                <td align="center"><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['detail']->value['service_name'];?>
</td>
                                <td class="text-center"><?php if ($_smarty_tpl->tpl_vars['detail']->value['service_type'] == @constant('SERVICE_TYPE_MONTHLY')) {
echo __('Monthly');
} else {
echo __('Daily');
}?></td>
                                <td class="text-center"><?php echo $_smarty_tpl->tpl_vars['detail']->value['quantity'];?>
</td>
                                <td class="text-right"><?php echo moneyFormat($_smarty_tpl->tpl_vars['detail']->value['unit_price']);?>
</td>
                                <td class="text-right"><?php echo moneyFormat($_smarty_tpl->tpl_vars['detail']->value['to_money']);?>
</td>
                            </tr>
                            <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                        <?php }?>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                <?php }?>
                
                <?php if (count($_smarty_tpl->tpl_vars['data']->value['tuition_detail']['count_based_services']) > 0) {?>
                    <tr><td colspan="6"><strong><?php echo __("Count-based service of previous month");?>
 <?php echo $_smarty_tpl->tpl_vars['child']->value['pre_month'];?>
</strong></td></tr>
                    <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['data']->value['tuition_detail']['count_based_services'], 'detail');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['detail']->value) {
?>
                        <?php if ($_smarty_tpl->tpl_vars['detail']->value['to_money'] > 0) {?>
                            <tr>
                                <td align="center"><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['detail']->value['service_name'];?>
</td>
                                <td class="text-center"><?php echo __('Times');?>
</td>
                                <td class="text-center"><?php echo $_smarty_tpl->tpl_vars['detail']->value['quantity'];?>
</td>
                                <td class="text-right"><?php echo moneyFormat($_smarty_tpl->tpl_vars['detail']->value['unit_price']);?>
</td>
                                <td class="text-right"><?php echo moneyFormat($_smarty_tpl->tpl_vars['detail']->value['to_money']);?>
</td>
                            </tr>
                            <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                        <?php }?>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                <?php }?>
                </tbody>
            </table>
        <?php }?>
    </div>
</div>
<?php }
}
