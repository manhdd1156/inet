<?php
/* Smarty version 3.1.31, created on 2021-05-20 17:07:41
  from "D:\workplace\Inet-project\source\Server11\content\themes\inet\templates\ci\school\school.fees.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_60a634ed991029_81079922',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'd380fe2e204927c24fdc1751dc4da063f5622259' => 
    array (
      0 => 'D:\\workplace\\Inet-project\\source\\Server11\\content\\themes\\inet\\templates\\ci\\school\\school.fees.tpl',
      1 => 1621501239,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60a634ed991029_81079922 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <div class="pull-right flip">
            <?php if ($_smarty_tpl->tpl_vars['school']->value['school_step'] == SCHOOL_STEP_FINISH || $_smarty_tpl->tpl_vars['school']->value['grade'] != 0) {?>
                <a href="https://blog.coniu.vn/huong-dan-tao-cac-loai-phi-cho-tre/" class="btn btn-info btn_guide" target="_blank">
                    <i class="fa fa-info-circle"></i> <?php echo __("Guide");?>

                </a>
            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
                <?php if ($_smarty_tpl->tpl_vars['canEdit']->value) {?>
                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/fees/add" class="btn btn-default">
                        <i class="fa fa-plus"></i> <?php echo __("Add New Fee");?>

                    </a>
                <?php }?>
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/fees/inactive" class="btn btn-warning">
                    <i class="fa fa-adjust"></i> <?php echo __("Inactive fee list");?>

                </a>
            <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "inactive") {?>
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/fees" class="btn btn-default">
                    <i class="fa fa-adjust"></i> <?php echo __("Active fee list");?>

                </a>
            <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "edit") {?>
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/fees/add" class="btn btn-default">
                    <i class="fa fa-plus"></i> <?php echo __("Add New Fee");?>

                </a>
            <?php }?>
        </div>
        <i class="fas fa-money-bill-alt fa-lg fa-fw pr10"></i>
        <?php echo __("Fee");?>

        <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == "edit") {?>
            &rsaquo; <?php echo $_smarty_tpl->tpl_vars['data']->value['fee_name'];?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
            &rsaquo; <?php echo __('Add New');?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
            &rsaquo; <?php echo __('Active fee list');?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "inactive") {?>
            &rsaquo; <?php echo __('Inactive fee list');?>

        <?php }?>
    </div>
    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
        <div class="panel-body with-table">
            <div class="table-responsive">
                <div><strong><?php echo __("Fee list");?>
&nbsp;(<span class="count_fee"><?php echo count($_smarty_tpl->tpl_vars['rows']->value);?>
</span>)</strong></div>
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th><?php echo __("Fee name");?>
</th>
                            <th><?php echo __("Fee cycle");?>
</th>
                            <th><?php echo __("Unit price");?>
 (<?php echo @constant('MONEY_UNIT');?>
)</th>
                            <th><?php echo __("Fee level");?>
</th>
                            <th><?php echo __("Status");?>
</th>
                            <th><?php echo __("Description");?>
</th>
                            <th><?php echo __("Actions");?>
</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['rows']->value, 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
                            <tr class="list_fee_<?php echo $_smarty_tpl->tpl_vars['row']->value['fee_id'];?>
">
                                <td align="center" style="vertical-align:middle" <?php if ($_smarty_tpl->tpl_vars['row']->value['level'] == @constant('CHILD_LEVEL')) {?>rowspan="2"<?php }?>><strong><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</strong></td>
                                <td style="vertical-align:middle">
                                    <strong><?php echo $_smarty_tpl->tpl_vars['row']->value['fee_name'];?>
</strong>
                                    <?php if ($_smarty_tpl->tpl_vars['row']->value['level'] == @constant('CHILD_LEVEL')) {?>
                                        <br/><?php echo __("Replace for");?>
:
                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['row']->value['fee_changes'], 'fee_change', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['fee_change']->value) {
?>
                                            <br/><?php echo $_smarty_tpl->tpl_vars['k']->value+1;?>
-<?php echo $_smarty_tpl->tpl_vars['fee_change']->value['fee_name'];?>

                                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                    <?php }?>
                                </td>
                                <td align="center" style="vertical-align:middle">
                                    <?php if ($_smarty_tpl->tpl_vars['row']->value['type'] == @constant('FEE_TYPE_MONTHLY')) {?>
                                        <?php echo __("Monthly fee");?>

                                    <?php } elseif ($_smarty_tpl->tpl_vars['row']->value['type'] == @constant('FEE_TYPE_DAILY')) {?>
                                        <?php echo __("Daily fee");?>

                                    <?php }?>
                                </td>
                                <td align="right" style="vertical-align:middle"><?php echo number_format($_smarty_tpl->tpl_vars['row']->value['unit_price'],0,'.',',');?>
</td>
                                <td align="center" style="vertical-align:middle">
                                    <?php if ($_smarty_tpl->tpl_vars['row']->value['level'] == @constant('SCHOOL_LEVEL')) {?>
                                        <?php echo __("School");?>

                                    <?php } elseif ($_smarty_tpl->tpl_vars['row']->value['level'] == @constant('CLASS_LEVEL_LEVEL')) {?>
                                        <?php echo $_smarty_tpl->tpl_vars['row']->value['class_level_name'];?>

                                    <?php } elseif ($_smarty_tpl->tpl_vars['row']->value['level'] == @constant('CLASS_LEVEL')) {?>
                                        <?php echo $_smarty_tpl->tpl_vars['row']->value['group_title'];?>

                                    <?php } elseif ($_smarty_tpl->tpl_vars['row']->value['level'] == @constant('CHILD_LEVEL')) {?>
                                        <?php echo __("Children");?>

                                    <?php }?>
                                </td>
                                <td align="center" style="vertical-align:middle">
                                    <?php if ($_smarty_tpl->tpl_vars['row']->value['status'] == 0) {?>
                                        <i class="fa fa-times" aria-hidden="true"></i>
                                    <?php } else { ?>
                                        <i class="fa fa-check" aria-hidden="true"></i>
                                    <?php }?>
                                </td>
                                <td style="vertical-align:middle"><?php echo $_smarty_tpl->tpl_vars['row']->value['description'];?>
</td>
                                <td nowrap="true" style="vertical-align:middle">
                                    <?php if ($_smarty_tpl->tpl_vars['canEdit']->value) {?>
                                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/fees/edit/<?php echo $_smarty_tpl->tpl_vars['row']->value['fee_id'];?>
" class="btn btn-xs btn-default"><?php echo __("Edit");?>
</a>
                                        <button class="btn btn-xs btn-warning js_school-fee-cancel" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['fee_id'];?>
"><?php echo __("Inactive");?>
</button>
                                        <?php if ($_smarty_tpl->tpl_vars['row']->value['used'] == 0) {?>
                                            <button class="btn btn-xs btn-danger js_school-fee-delete" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['fee_id'];?>
"><?php echo __("Delete");?>
</button>
                                        <?php }?>
                                    <?php }?>
                                </td>
                            </tr>
                            <?php if ($_smarty_tpl->tpl_vars['row']->value['level'] == @constant('CHILD_LEVEL')) {?>
                                <tr class="list_fee_<?php echo $_smarty_tpl->tpl_vars['row']->value['fee_id'];?>
">
                                    <td align="center" style="vertical-align:middle">
                                        <strong><?php echo __("Applied student");?>
</strong>
                                    </td>
                                    <td colspan="6">
                                        <a href="#" class="btn-primary js_sh_children" data-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['fee_id'];?>
"><?php echo __("Show/hide student list");?>
</a>
                                        <div class="children_list_<?php echo $_smarty_tpl->tpl_vars['row']->value['fee_id'];?>
 x-hidden">
                                            <table class="table table-striped table-bordered table-hover">
                                                <tbody>
                                                    <?php $_smarty_tpl->_assignInScope('id', 1);
?>
                                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['row']->value['children'], 'child');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['child']->value) {
?>
                                                        <tr>
                                                            <td align="center"><?php echo $_smarty_tpl->tpl_vars['id']->value;?>
</td>
                                                            <td><?php echo $_smarty_tpl->tpl_vars['child']->value['child_name'];?>
</td>
                                                            <td><?php echo $_smarty_tpl->tpl_vars['child']->value['group_title'];?>
</td>
                                                        </tr>
                                                        <?php $_smarty_tpl->_assignInScope('id', $_smarty_tpl->tpl_vars['id']->value+1);
?>
                                                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                                </tbody>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            <?php }?>
                            <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>


                        <?php if ($_smarty_tpl->tpl_vars['idx']->value == 1) {?>
                            <tr class="odd">
                                <td valign="top" align="center" colspan="8" class="dataTables_empty">
                                    <?php echo __("No data available in table");?>

                                </td>
                            </tr>
                        <?php }?>
                    </tbody>
                </table>
            </div>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "inactive") {?>
        <div class="panel-body with-table">
            <div class="table-responsive">
                <div><strong><?php echo __("Inactive fee list");?>
&nbsp;(<?php echo count($_smarty_tpl->tpl_vars['rows']->value);?>
)</strong></div>
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th><?php echo __("Fee name");?>
</th>
                        <th><?php echo __("Fee cycle");?>
</th>
                        <th><?php echo __("Unit price");?>
 (<?php echo @constant('MONEY_UNIT');?>
)</th>
                        <th><?php echo __("Fee level");?>
</th>
                        <th><?php echo __("Status");?>
</th>
                        <th><?php echo __("Description");?>
</th>
                        <th><?php echo __("Actions");?>
</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['rows']->value, 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
                        <tr>
                            <td align="center" style="vertical-align:middle" <?php if ($_smarty_tpl->tpl_vars['row']->value['level'] == @constant('CHILD_LEVEL')) {?>rowspan="2"<?php }?>><strong><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</strong></td>
                            <td style="vertical-align:middle">
                                <strong><?php echo $_smarty_tpl->tpl_vars['row']->value['fee_name'];?>
</strong>
                                <?php if ($_smarty_tpl->tpl_vars['row']->value['level'] == @constant('CHILD_LEVEL')) {?>
                                    <br/><?php echo __("Replace for");?>
:
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['row']->value['fee_changes'], 'fee_change', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['fee_change']->value) {
?>
                                        <br/><?php echo $_smarty_tpl->tpl_vars['k']->value+1;?>
-<?php echo $_smarty_tpl->tpl_vars['fee_change']->value['fee_name'];?>

                                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                <?php }?>
                            </td>
                            <td align="center" style="vertical-align:middle">
                                <?php if ($_smarty_tpl->tpl_vars['row']->value['type'] == @constant('FEE_TYPE_MONTHLY')) {?>
                                    <?php echo __("Monthly fee");?>

                                <?php } elseif ($_smarty_tpl->tpl_vars['row']->value['type'] == @constant('FEE_TYPE_DAILY')) {?>
                                    <?php echo __("Daily fee");?>

                                <?php }?>
                            </td>
                            <td align="right" style="vertical-align:middle"><?php echo number_format($_smarty_tpl->tpl_vars['row']->value['unit_price'],0,'.',',');?>
</td>
                            <td align="center" style="vertical-align:middle">
                                <?php if ($_smarty_tpl->tpl_vars['row']->value['level'] == @constant('SCHOOL_LEVEL')) {?>
                                    <?php echo __("School");?>

                                <?php } elseif ($_smarty_tpl->tpl_vars['row']->value['level'] == @constant('CLASS_LEVEL_LEVEL')) {?>
                                    <?php echo $_smarty_tpl->tpl_vars['row']->value['class_level_name'];?>

                                <?php } elseif ($_smarty_tpl->tpl_vars['row']->value['level'] == @constant('CLASS_LEVEL')) {?>
                                    <?php echo $_smarty_tpl->tpl_vars['row']->value['group_title'];?>

                                <?php } elseif ($_smarty_tpl->tpl_vars['row']->value['level'] == @constant('CHILD_LEVEL')) {?>
                                    <?php echo __("Children");?>

                                <?php }?>
                            </td>
                            <td align="center" style="vertical-align:middle">
                                <?php if ($_smarty_tpl->tpl_vars['row']->value['status'] == 0) {?>
                                    <i class="fa fa-times" aria-hidden="true"></i>
                                <?php } else { ?>
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                <?php }?>
                            </td>
                            <td style="vertical-align:middle"><?php echo $_smarty_tpl->tpl_vars['row']->value['description'];?>
</td>
                            <td nowrap="true" style="vertical-align:middle">
                                <?php if ($_smarty_tpl->tpl_vars['canEdit']->value) {?>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/fees/edit/<?php echo $_smarty_tpl->tpl_vars['row']->value['fee_id'];?>
" class="btn btn-xs btn-default"><?php echo __("Edit");?>
</a>
                                    <?php if ($_smarty_tpl->tpl_vars['row']->value['used'] == 0) {?>
                                        <button class="btn btn-xs btn-danger js_school-fee-delete" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['fee_id'];?>
"><?php echo __("Delete");?>
</button>
                                    <?php }?>
                                <?php }?>
                            </td>
                        </tr>
                        <?php if ($_smarty_tpl->tpl_vars['row']->value['level'] == @constant('CHILD_LEVEL')) {?>
                            <tr>
                                <td align="center" style="vertical-align:middle">
                                    <strong><?php echo __("Applied student");?>
</strong>
                                </td>
                                <td colspan="6">
                                    <a href="#" class="btn-primary js_sh_children" data-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['fee_id'];?>
"><?php echo __("Show/hide student list");?>
</a>
                                    <div class="children_list_<?php echo $_smarty_tpl->tpl_vars['row']->value['fee_id'];?>
 x-hidden">
                                        <table class="table table-striped table-bordered table-hover">
                                            <tbody>
                                            <?php $_smarty_tpl->_assignInScope('id', 1);
?>
                                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['row']->value['children'], 'child');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['child']->value) {
?>
                                                <tr>
                                                    <td align="center"><?php echo $_smarty_tpl->tpl_vars['id']->value;?>
</td>
                                                    <td><?php echo $_smarty_tpl->tpl_vars['child']->value['child_name'];?>
</td>
                                                    <td><?php echo $_smarty_tpl->tpl_vars['child']->value['group_title'];?>
</td>
                                                </tr>
                                                <?php $_smarty_tpl->_assignInScope('id', $_smarty_tpl->tpl_vars['id']->value+1);
?>
                                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                            </tbody>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        <?php }?>
                        <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>


                    <?php if ($_smarty_tpl->tpl_vars['idx']->value == 1) {?>
                        <tr class="odd">
                            <td valign="top" align="center" colspan="8" class="dataTables_empty">
                                <?php echo __("No data available in table");?>

                            </td>
                        </tr>
                    <?php }?>
                    </tbody>
                </table>
            </div>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "edit") {?>
        <div class="panel-body">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_tuition.php">
                <input type="hidden" name="school_username" id="school_username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                <input type="hidden" name="fee_id" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['fee_id'];?>
"/>
                <input type="hidden" name="do" value="edit_fee"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Fee name");?>
 (*)</label>
                    <div class="col-sm-9">
                        <input class="form-control" name="fee_name" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['fee_name'];?>
" required autofocus maxlength="100">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Fee cycle");?>
 (*)</label>
                    <div class="col-sm-4">
                        <input type="hidden" name="type" id="type" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['type'];?>
">
                        <input type="text" value="<?php if ($_smarty_tpl->tpl_vars['data']->value['type'] == @constant('FEE_TYPE_MONTHLY')) {
echo __("Monthly fee");
} else {
echo __("Daily fee");
}?>" class="form-control" disabled>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Unit price");?>
</label>
                    <div class="col-sm-9">
                        <div class="input-group col-md-5">
                            <input type="text" class="form-control money_tui" name="unit_price" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['unit_price'];?>
" maxlength="11">
                            <span class="input-group-addon"><?php echo @constant('MONEY_UNIT');?>
</span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Fee level");?>
 (*)</label>
                    <div class="col-sm-3">
                        <select name="level" id="fee_level" class="form-control" <?php if ($_smarty_tpl->tpl_vars['data']->value['level'] == @constant('CHILD_LEVEL')) {?>disabled<?php }?>>
                            <option value="<?php echo @constant('SCHOOL_LEVEL');?>
" <?php if ($_smarty_tpl->tpl_vars['data']->value['level'] == @constant('SCHOOL_LEVEL')) {?>selected<?php }?>><?php echo __("School");?>
</option>
                            <option value="<?php echo @constant('CLASS_LEVEL_LEVEL');?>
" <?php if ($_smarty_tpl->tpl_vars['data']->value['level'] == @constant('CLASS_LEVEL_LEVEL')) {?>selected<?php }?>><?php echo __("Class level");?>
</option>
                            <option value="<?php echo @constant('CLASS_LEVEL');?>
" <?php if ($_smarty_tpl->tpl_vars['data']->value['level'] == @constant('CLASS_LEVEL')) {?>selected<?php }?>><?php echo __("Class");?>
</option>
                            <option value="<?php echo @constant('CHILD_LEVEL');?>
" <?php if ($_smarty_tpl->tpl_vars['data']->value['level'] == @constant('CHILD_LEVEL')) {?>selected<?php }?>><?php echo __("Child");?>
</option>
                        </select>
                    </div>
                    <div class="col-sm-3 <?php if ($_smarty_tpl->tpl_vars['data']->value['level'] != (@constant('CLASS_LEVEL_LEVEL'))) {?>x-hidden<?php }?>" name="fee_class_level">
                        <select name="class_level_id" id="class_level_id" class="form-control">
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['class_levels']->value, 'class_level');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class_level']->value) {
?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['class_level']->value['class_level_id'];?>
" <?php if ($_smarty_tpl->tpl_vars['data']->value['class_level_id'] == $_smarty_tpl->tpl_vars['class_level']->value['class_level_id']) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['class_level']->value['class_level_name'];?>
</option>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </select>
                    </div>
                    <div class="col-sm-3 <?php if ((($_smarty_tpl->tpl_vars['data']->value['level'] != @constant('CLASS_LEVEL')) && ($_smarty_tpl->tpl_vars['data']->value['level'] != @constant('CHILD_LEVEL')))) {?>x-hidden<?php }?>" name="fee_class">
                        <select name="class_id" id="fee_class_id" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['data']->value['fee_id'];?>
" class="form-control">
                            <option value=""><?php echo __("Select class");?>
</option>
                            <?php $_smarty_tpl->_assignInScope('class_level', -1);
?>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['classes']->value, 'class');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class']->value) {
?>
                                <?php if (($_smarty_tpl->tpl_vars['class_level']->value != $_smarty_tpl->tpl_vars['class']->value['class_level_id'])) {?>
                                    <option value="" disabled>-----<?php echo $_smarty_tpl->tpl_vars['class']->value['class_level_name'];?>
-----</option>
                                <?php }?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['class']->value['group_id'];?>
" <?php if ($_smarty_tpl->tpl_vars['data']->value['class_id'] == $_smarty_tpl->tpl_vars['class']->value['group_id']) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['class']->value['group_title'];?>
</option>
                                <?php $_smarty_tpl->_assignInScope('class_level', $_smarty_tpl->tpl_vars['class']->value['class_level_id']);
?>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </select>
                    </div>
                </div>

                <div class="form-group" name="fee_child" id="fee_child_id">
                    <?php if ($_smarty_tpl->tpl_vars['data']->value['level'] == @constant('CHILD_LEVEL')) {?>
                        <?php echo $_smarty_tpl->tpl_vars['children_list_html']->value;?>

                    <?php }?>
                </div>

                <div class = "form-group <?php if ($_smarty_tpl->tpl_vars['data']->value['level'] != @constant('CHILD_LEVEL')) {?>x-hidden<?php }?>" name = "fee_change">
                    <label class = "col-sm-3 control-label text-left"><?php echo __("Replace for");?>
</label>
                    <div class = "col-sm-9">
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['fees']->value, 'fee');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['fee']->value) {
?>
                            <div class ="col-sm-6"><input type="checkbox" name="fee_change_id[]" value="<?php echo $_smarty_tpl->tpl_vars['fee']->value['fee_id'];?>
" <?php if ($_smarty_tpl->tpl_vars['fee']->value['checked'] == 1) {?>checked<?php }?>>&nbsp;<?php echo $_smarty_tpl->tpl_vars['fee']->value['fee_name'];?>
</div>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Description");?>
</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="description" rows="4"><?php echo $_smarty_tpl->tpl_vars['data']->value['description'];?>
</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Applied");?>
?</label>
                    <div class="col-sm-9">
                        <div class="onoffswitch">
                            <input type="checkbox" name="status" class="onoffswitch-checkbox" id="status" <?php if ($_smarty_tpl->tpl_vars['data']->value['status']) {?>checked<?php }?>>
                            <label class="onoffswitch-label" for="status"></label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30"><?php echo __("Save");?>
</button>
                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/fees" class="btn btn-default"><?php echo __("Lists");?>
</a>
                    </div>
                </div>

                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
            </form>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
        <div class="panel-body">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_tuition.php">
                <input type="hidden" name="school_username" id="school_username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                <input type="hidden" name="do" value="add_fee"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Fee name");?>
 (*)</label>
                    <div class="col-sm-9">
                        <input class="form-control" name="fee_name" required autofocus maxlength="100">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Fee cycle");?>
 (*)</label>
                    <div class="col-sm-4">
                        <select name="type" id="type" class="form-control">
                            <option value="<?php echo @constant('FEE_TYPE_MONTHLY');?>
"><?php echo __("Monthly fee");?>
</option>
                            <option value="<?php echo @constant('FEE_TYPE_DAILY');?>
"><?php echo __("Daily fee");?>
</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Unit price");?>
</label>
                    <div class="col-sm-9">
                        <div class="input-group col-md-5">
                            <input type="text" min="0" lang="en-150" class="form-control money_tui" name="unit_price" value="0" maxlength="11">
                            <span class="input-group-addon"><?php echo @constant('MONEY_UNIT');?>
</span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Fee level");?>
 (*)</label>
                    <div class="col-sm-3">
                        <select name="level" id="fee_level" class="form-control">
                            <option value="<?php echo @constant('SCHOOL_LEVEL');?>
"><?php echo __("School");?>
</option>
                            <option value="<?php echo @constant('CLASS_LEVEL_LEVEL');?>
"><?php echo __("Class level");?>
</option>
                            <option value="<?php echo @constant('CLASS_LEVEL');?>
"><?php echo __("Class");?>
</option>
                            <option value="<?php echo @constant('CHILD_LEVEL');?>
"><?php echo __("Child");?>
</option>
                        </select>
                    </div>
                    <div class="col-sm-3 x-hidden" name="fee_class_level">
                        <select name="class_level_id" id="class_level_id" class="form-control">
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['class_levels']->value, 'class_level');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class_level']->value) {
?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['class_level']->value['class_level_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['class_level']->value['class_level_name'];?>
</option>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </select>
                    </div>
                    <div class="col-sm-3 x-hidden" name="fee_class">
                        <select name="class_id" id="fee_class_id" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" class="form-control">
                            <option value=""><?php echo __("Select class");?>
...</option>

                            <?php $_smarty_tpl->_assignInScope('class_level', -1);
?>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['classes']->value, 'class');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class']->value) {
?>
                                <?php if (($_smarty_tpl->tpl_vars['class_level']->value != $_smarty_tpl->tpl_vars['class']->value['class_level_id'])) {?>
                                    <option value="" disabled>-----<?php echo $_smarty_tpl->tpl_vars['class']->value['class_level_name'];?>
-----</option>
                                <?php }?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['class']->value['group_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['class']->value['group_title'];?>
</option>
                                <?php $_smarty_tpl->_assignInScope('class_level', $_smarty_tpl->tpl_vars['class']->value['class_level_id']);
?>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </select>
                    </div>
                </div>

                <div class = "form-group x-hidden" name="fee_child" id="fee_child_id"></div>

                <div class = "form-group x-hidden" name = "fee_change">
                    <label class = "col-sm-3 control-label text-left"><?php echo __("Replace for");?>
</label>
                    <div class = "col-sm-9">
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['fees']->value, 'fee');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['fee']->value) {
?>
                            <div class ="col-sm-6"><input type="checkbox" name="fee_change_id[]" value="<?php echo $_smarty_tpl->tpl_vars['fee']->value['fee_id'];?>
">&nbsp;<?php echo $_smarty_tpl->tpl_vars['fee']->value['fee_name'];?>
</div>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Description");?>
</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="description" rows="4"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Applied");?>
?</label>
                    <div class="col-sm-9">
                        <div class="onoffswitch">
                            <input type="checkbox" name="status" class="onoffswitch-checkbox" id="status" checked>
                            <label class="onoffswitch-label" for="status"></label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30"><?php echo __("Save");?>
</button>
                    </div>
                </div>

                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
            </form>
        </div>
    <?php }?>
</div><?php }
}
