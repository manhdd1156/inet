<?php
/* Smarty version 3.1.31, created on 2021-03-31 08:54:47
  from "D:\workplace\Server11\content\themes\inet\templates\ci\noga\ajax.noga.foetusdevelopment.list.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_6063d6672a75c4_35329525',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '75f158ccb69f304155b7da7553932a119e93a5e2' => 
    array (
      0 => 'D:\\workplace\\Server11\\content\\themes\\inet\\templates\\ci\\noga\\ajax.noga.foetusdevelopment.list.tpl',
      1 => 1552404704,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6063d6672a75c4_35329525 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="table-responsive" name = "schedule_all">
    <table class="table table-striped table-bordered table-hover js_dataTable">
        <thead>
        <tr><th colspan="5"><?php echo __("Foetus development");?>
</th></tr>
        <tr>
            <th>
                <?php echo __("No.");?>

            </th>
            <th>
                <?php echo __("Week");?>

            </th>
            <th>
                <?php echo __("Title");?>

            </th>
            <th>
                <?php echo __("Type");?>

            </th>
            <th>
                <?php echo __("Actions");?>

            </th>
        </tr>
        </thead>
        <tbody>
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['results']->value, 'row', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['row']->value) {
?>
            <tr>
                <td class="align-middle" align="center"><?php echo $_smarty_tpl->tpl_vars['k']->value+1;?>
</td>
                <td class="align-middle" align="center"><?php echo $_smarty_tpl->tpl_vars['row']->value['week'];?>
</td>
                <td class="align-middle"><a href = "<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/noga/foetusdevelopments/edit/<?php echo $_smarty_tpl->tpl_vars['row']->value['foetus_info_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['row']->value['title'];?>
</a></td>
                <td class="align-middle" align="center">
                    <?php if ($_smarty_tpl->tpl_vars['row']->value['type'] == 1) {
echo __("Pregnancy check");?>
 <?php } else { ?> <?php echo __("Information");
}?>
                </td>
                <td class="align-middle" align="center">
                    <a class="btn btn-xs btn-default" href = "<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/noga/foetusdevelopments/edit/<?php echo $_smarty_tpl->tpl_vars['row']->value['foetus_info_id'];?>
"><?php echo __("Edit");?>
</a>
                    <a href="#" class = "btn-xs btn btn-danger js_noga-foetus-info-delete" data-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['foetus_info_id'];?>
"><?php echo __("Delete");?>
</a>
                </td>
            </tr>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

        </tbody>
    </table>
</div>

<?php echo '<script'; ?>
 type="text/javascript">
    $(function() {
        // run DataTable
        $('.js_dataTable').DataTable({
            "aoColumnDefs": [ { 'aDataSort': false, 'aTargets': [ -1 ] } ]
        });
    });
<?php echo '</script'; ?>
><?php }
}
