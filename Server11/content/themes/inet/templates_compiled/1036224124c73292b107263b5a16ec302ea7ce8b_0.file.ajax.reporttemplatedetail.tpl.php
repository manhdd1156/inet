<?php
/* Smarty version 3.1.31, created on 2021-06-25 15:11:48
  from "D:\workplace\Inet-project\source\Server11\content\themes\inet\templates\ci\ajax.reporttemplatedetail.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_60d58fc47b6486_62435640',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '1036224124c73292b107263b5a16ec302ea7ce8b' => 
    array (
      0 => 'D:\\workplace\\Inet-project\\source\\Server11\\content\\themes\\inet\\templates\\ci\\ajax.reporttemplatedetail.tpl',
      1 => 1621501238,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60d58fc47b6486_62435640 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_assignInScope('idx', 1);
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['results']->value['details'], 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
    <table class="table table-striped table-bordered table-hover" id = "addTempTable">
        <tr>
            <td><strong><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
 - <?php echo $_smarty_tpl->tpl_vars['row']->value['category_name'];?>
</strong></td>
            <input type="hidden" name="category_ids[]" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['report_template_category_id'];?>
">
            <input type="hidden" name="category_name_<?php echo $_smarty_tpl->tpl_vars['row']->value['report_template_category_id'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['category_name'];?>
">
        </tr>
        <tr>
            <td>
                <textarea placeholder="<?php echo __("Other comment");?>
" class="col-sm-12 mt10 mb10 noteArea" name="content_<?php echo $_smarty_tpl->tpl_vars['row']->value['report_template_category_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['row']->value['template_content'];?>
</textarea>
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['row']->value['suggests'], 'suggest');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['suggest']->value) {
?>
                    <div class="col-sm-4">
                        <input type="checkbox" value="<?php echo $_smarty_tpl->tpl_vars['suggest']->value;?>
" name="suggest_<?php echo $_smarty_tpl->tpl_vars['row']->value['report_template_category_id'];?>
[]"> <?php echo $_smarty_tpl->tpl_vars['suggest']->value;?>

                    </div>
                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

            </td>
        </tr>
    </table>
    <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
    
    <?php echo '<script'; ?>
 type="text/javascript">
        $('.noteArea').each(function () {
            this.setAttribute('style', 'height:' + (this.scrollHeight) + 'px;overflow-y:hidden;');
        }).on('input', function () {
            this.style.height = 'auto';
            this.style.height = (this.scrollHeight) + 'px';
        });
    <?php echo '</script'; ?>
>
<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>


<?php }
}
