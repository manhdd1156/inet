<?php
/* Smarty version 3.1.31, created on 2021-03-31 10:47:57
  from "D:\workplace\Server11\content\themes\inet\templates\ci\child\ajax.child.servicelist4record.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_6063f0edd94830_26674896',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '50c42917d94efcf234a1c9f6e4191c11b822d93c' => 
    array (
      0 => 'D:\\workplace\\Server11\\content\\themes\\inet\\templates\\ci\\child\\ajax.child.servicelist4record.tpl',
      1 => 1552404702,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6063f0edd94830_26674896 (Smarty_Internal_Template $_smarty_tpl) {
?>
<table class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th>#</th>
        <th><?php echo __("Register");?>
</th>
        <th><?php echo __("Service");?>
</th>
        <th><?php echo __("Service fee");?>
&nbsp;(<?php echo @constant('MONEY_UNIT');?>
)</th>
        <th><?php echo __("Registration time");?>
</th>
        <th><?php echo __("Registrar");?>
</th>
    </tr>
    </thead>
    <tbody>
    <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
    <?php $_smarty_tpl->_assignInScope('total', count($_smarty_tpl->tpl_vars['cb_services']->value));
?>
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['cb_services']->value, 'service');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['service']->value) {
?>
        <tr>
            <td align="center"><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</td>
            <td align="center">
                <?php if (isset($_smarty_tpl->tpl_vars['service']->value['service_usage_id']) && ($_smarty_tpl->tpl_vars['service']->value['service_usage_id'] > 0)) {?>
                    <?php echo __("Registered");?>

                    <?php $_smarty_tpl->_assignInScope('total', $_smarty_tpl->tpl_vars['total']->value-1);
?>
                <?php } else { ?>
                    <input type="checkbox" class="child" name="cbServiceIds[]" value="<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
">
                <?php }?>
            </td>
            <td><?php echo $_smarty_tpl->tpl_vars['service']->value['service_name'];?>
</td>
            <td align="right"><?php echo moneyFormat($_smarty_tpl->tpl_vars['service']->value['fee']);?>
</td>
            <td align="center">
                <?php if (isset($_smarty_tpl->tpl_vars['service']->value['service_usage_id']) && ($_smarty_tpl->tpl_vars['service']->value['service_usage_id'] > 0)) {?>
                    <?php echo $_smarty_tpl->tpl_vars['service']->value['recorded_at'];?>

                <?php }?>
            </td>
            <td align="center">
                <?php if (isset($_smarty_tpl->tpl_vars['service']->value['service_usage_id']) && ($_smarty_tpl->tpl_vars['service']->value['service_usage_id'] > 0)) {?>
                    <?php echo $_smarty_tpl->tpl_vars['service']->value['user_fullname'];?>

                <?php }?>
            </td>
        </tr>
        <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

    </tbody>
</table><?php }
}
