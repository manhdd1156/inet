<?php
/* Smarty version 3.1.31, created on 2021-03-30 15:35:31
  from "D:\workplace\Server11\content\themes\inet\templates\_footer.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_6062e2d3448796_38330875',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '4fb3ef68f6055dcfc3d5bdfa088988b85119717b' => 
    array (
      0 => 'D:\\workplace\\Server11\\content\\themes\\inet\\templates\\_footer.tpl',
      1 => 1560814425,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:_ads.tpl' => 1,
    'file:_js_files.tpl' => 1,
    'file:_js_templates.tpl' => 1,
  ),
),false)) {
function content_6062e2d3448796_38330875 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!-- ads -->
<?php $_smarty_tpl->_subTemplateRender('file:_ads.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('_ads'=>$_smarty_tpl->tpl_vars['ads_master']->value['footer'],'_master'=>true), 0, false);
?>

<!-- ads -->

<!-- footer -->
<div class="container">
	<div class="row footer">
		<div class="col-lg-6 col-md-6 col-sm-6">
			&copy; <?php echo date('Y');?>
 <?php echo $_smarty_tpl->tpl_vars['system']->value['system_title'];?>
 · <span class="text-link" data-toggle="modal" data-url="#translator"><?php echo $_smarty_tpl->tpl_vars['system']->value['language']['title'];?>
</span> |
			<a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/privacy"><?php echo __("Privacy");?>
</a> | <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/terms"><?php echo __("Terms");?>
</a>
		</div>

		<div class="col-lg-6 col-md-6 col-sm-6 links">
            <?php if (count($_smarty_tpl->tpl_vars['static_pages']->value) > 0) {?>
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['static_pages']->value, 'static_page', true);
$_smarty_tpl->tpl_vars['static_page']->iteration = 0;
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['static_page']->value) {
$_smarty_tpl->tpl_vars['static_page']->iteration++;
$_smarty_tpl->tpl_vars['static_page']->last = $_smarty_tpl->tpl_vars['static_page']->iteration == $_smarty_tpl->tpl_vars['static_page']->total;
$__foreach_static_page_3_saved = $_smarty_tpl->tpl_vars['static_page'];
?>
				<a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/static/<?php echo $_smarty_tpl->tpl_vars['static_page']->value['page_url'];?>
">
                    <?php echo $_smarty_tpl->tpl_vars['static_page']->value['page_title'];?>

					</a><?php if (!$_smarty_tpl->tpl_vars['static_page']->last) {?> · <?php }?>
                <?php
$_smarty_tpl->tpl_vars['static_page'] = $__foreach_static_page_3_saved;
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

            <?php }?>

            <?php if ($_smarty_tpl->tpl_vars['system']->value['contact_enabled']) {?>
				·
				<a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/contacts">
                    <?php echo __("Contacts");?>

				</a>
            <?php }?>

            
				
				
                    
				
            

            <?php if ($_smarty_tpl->tpl_vars['system']->value['directory_enabled']) {?>
				·
				<a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/directory">
                    <?php echo __("Directory");?>

				</a>
            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['system']->value['market_enabled']) {?>
				·
				<a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/market">
                    <?php echo __("Market");?>

				</a>
            <?php }?>
		</div>
	</div>
</div>
<!-- footer -->

<div id="app_android" style="display: none" class="app_pop">
	<div class="android_down app_down_box" <?php if (!$_smarty_tpl->tpl_vars['cookie_app']->value) {?>style="display: none"<?php }?>>
		<a href="<?php echo @constant('GOOGLEPLAY_URL');?>
" target="_blank"> <img
					class=""
					src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/content/themes/<?php echo $_smarty_tpl->tpl_vars['system']->value['theme'];?>
/images/favicon.png"
					alt="Ứng dụng mầm non Coniu CH Play"/> <?php echo __("Download Inet App");?>
</a>
		<div class="down_app_hide" data-id="<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_id'];?>
">
			<i class="fa fa-close"></i>
		</div>
	</div>
</div>
<div id="app_ios" style="display: none" class="app_pop">
	<div class="ios_down app_down_box" <?php if (!$_smarty_tpl->tpl_vars['cookie_app']->value) {?>style="display: none"<?php }?>>
		<a href="<?php echo @constant('APPSTORE_URL');?>
" target="_blank"><img class=""
																	src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/content/themes/<?php echo $_smarty_tpl->tpl_vars['system']->value['theme'];?>
/images/favicon.png"
																	alt="Ứng dụng mầm non Coniu App store"/> <?php echo __("Download Inet App");?>
</a>
		<div class="down_app_hide" data-id="<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_id'];?>
">
			<i class="fa fa-close"></i>
		</div>
	</div>
</div>
<?php echo '<script'; ?>
>
    function getMobileOperatingSystem() {
        var userAgent = navigator.userAgent || navigator.vendor || window.opera;
        // Windows Phone must come first because its UA also contains "Android"
        if (/windows phone/i.test(userAgent)) {
            return "Windows Phone";
        }

        if (/android/i.test(userAgent)) {
            document.getElementById("app_android").style.display = 'block';
            return "Android";
        }

        // iOS detection from: http://stackoverflow.com/a/9039885/177710
        if (/iPad|iPhone|iPod/i.test(userAgent)) {
            document.getElementById("app_ios").style.display = 'block';
            return "iOS";
        }
        return "Web";
    }
    getMobileOperatingSystem();
    // if(x == Adroi)
    // document.getElementById("osmobile").innerHTML = x;
<?php echo '</script'; ?>
>

</div>
<!-- main wrapper -->

<!-- Dependencies CSS [Twemoji-Awesome|Flag-Icon] -->
<?php if (!$_smarty_tpl->tpl_vars['user']->value->_logged_in) {?>
	<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/includes/assets/css/font-awesome/css/fontawesome-all.min.css">
<?php }?>
<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/includes/assets/css/twemoji-awesome/twemoji-awesome.min.css">
<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/includes/assets/css/flag-icon/css/flag-icon.min.css">
<!-- Dependencies CSS [Twemoji-Awesome|Flag-Icon] -->

<!-- JS Files -->
<?php $_smarty_tpl->_subTemplateRender('file:_js_files.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<!-- JS Files -->

<!-- JS Templates -->
<?php $_smarty_tpl->_subTemplateRender('file:_js_templates.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<!-- JS Templates -->

<!-- Analytics Code -->
<?php if ($_smarty_tpl->tpl_vars['system']->value['analytics_code']) {
echo html_entity_decode($_smarty_tpl->tpl_vars['system']->value['analytics_code'],ENT_QUOTES);
}?>
<!-- Analytics Code -->


<?php if ($_smarty_tpl->tpl_vars['user']->value->_logged_in) {?>
	<!-- Notification -->
	<audio id="notification_sound">
		<source src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/includes/assets/sounds/notification.mp3" type="audio/mpeg">
	</audio>
	<!-- Notification -->
	<!-- Chat -->
	<audio id="chat_sound">
		<source src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/includes/assets/sounds/chat.mp3" type="audio/mpeg">
	</audio>
	<!-- Chat -->
	<!-- Call -->
	<!-- Coniu - tam bo call.mp3 -->



	<!-- Call -->
	<!-- Video -->
	<audio id="video_sound">
		<source src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/includes/assets/sounds/video.mp3" type="audio/mpeg">
	</audio>
	<!-- Video -->
<?php }?>


</body>
</html>

<?php }
}
