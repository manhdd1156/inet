<?php
/* Smarty version 3.1.31, created on 2021-03-31 10:27:26
  from "D:\workplace\Server11\content\themes\inet\templates\ci\class\ajax.class.attendancesearch.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_6063ec1e4ee678_22861273',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '407cfb21a8b0cc1613f78d1df43bd432d5ee920c' => 
    array (
      0 => 'D:\\workplace\\Server11\\content\\themes\\inet\\templates\\ci\\class\\ajax.class.attendancesearch.tpl',
      1 => 1573458086,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6063ec1e4ee678_22861273 (Smarty_Internal_Template $_smarty_tpl) {
if ($_smarty_tpl->tpl_vars['schoolConfig']->value['attendance_use_leave_early'] || $_smarty_tpl->tpl_vars['schoolConfig']->value['attendance_use_come_late'] || $_smarty_tpl->tpl_vars['schoolConfig']->value['attendance_absence_no_reason']) {?>
    <div class="table-responsive" id="getFixed">
        <table class="table table-striped table-bordered" style="z-index: 1">
            <tbody>
            <tr>
                <td align="right"><strong><?php echo __("Symbol");?>
:</strong></td>
                <?php if ($_smarty_tpl->tpl_vars['schoolConfig']->value['attendance_use_come_late']) {?>
                    <td bgcolor="#6495ed" align="center"><i class="fa fa-check" aria-hidden="true"></i></td>
                    <td align="left"><?php echo __("Come late");?>
</td>
                <?php }?>
                <?php if ($_smarty_tpl->tpl_vars['schoolConfig']->value['attendance_use_leave_early']) {?>
                    <td bgcolor="#008b8b" align="center"><i class="fa fa-check" aria-hidden="true"></i></td>
                    <td align="left"><?php echo __("Leave early");?>
</td>
                <?php }?>
                <td align="center"><i class="fa fa-check" aria-hidden="true"></i></td>
                <td align="left"><?php echo __("Present");?>
</td>

                <?php if ($_smarty_tpl->tpl_vars['schoolConfig']->value['attendance_absence_no_reason']) {?>
                    <td bgcolor="#ff1493" align="center"><i class="fa fa-times" aria-hidden="true"></i></td>
                    <td align="left"><?php echo __("Without permission");?>
</td>
                <?php }?>

                <td bgcolor="#deb887" align="center"><i class="fa fa-times" aria-hidden="true"></i></td>
                <td align="left"><?php echo __("With permission");?>
</td>
            </tr>
            </tbody>
        </table>
    </div>
<?php }?>

<div style="z-index: 2">
    <div><strong><?php echo __("The whole class attendance");?>
</strong></div>
    <div id="table_button">
        <button id="left">&larr;</button>
        <button id="right">&rarr;</button>
    </div>
</div>
<div class="table-responsive" id="example">
    <table class="table table-striped table-bordered table-pinned" style="z-index: 1">
        <thead>
        <tr>
            <th class="pinned">#</th>
            <th nowrap="true" class="pinned"><?php echo __("Full name");?>
</th>
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['dates']->value, 'date');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['date']->value) {
?>
                <th align="center"><?php echo $_smarty_tpl->tpl_vars['date']->value;?>
</th>
            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

            <th nowrap="true"><?php echo __("Present");?>
</th>
            <th><?php echo __("Absence");?>
</th>
        </tr>
        </thead>
        <tbody>
        <?php $_smarty_tpl->_assignInScope('rowIdx', 1);
?>
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['rows']->value, 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
            <tr <?php if ($_smarty_tpl->tpl_vars['row']->value['child_status'] == 0) {?> class="row-disable"<?php }?>>
                <td align="center" class="pinned"><?php echo $_smarty_tpl->tpl_vars['rowIdx']->value;?>
</td>
                <td nowrap="true" class="pinned"><a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/attendance/child/<?php echo $_smarty_tpl->tpl_vars['row']->value['child_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['row']->value['child_name'];?>
</a></td>

                <?php $_smarty_tpl->_assignInScope('rowIdx', $_smarty_tpl->tpl_vars['rowIdx']->value+1);
?>
                <?php $_smarty_tpl->_assignInScope('presentCnt', 0);
?>
                <?php $_smarty_tpl->_assignInScope('absenceCnt', 0);
?>
                <?php $_smarty_tpl->_assignInScope('dateNow', date('Y-m-d'));
?>
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['row']->value['cells'], 'cell');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['cell']->value) {
?>
                    <?php if ($_smarty_tpl->tpl_vars['cell']->value['is_checked'] == 1) {?>
                        <?php if (!$_smarty_tpl->tpl_vars['schoolConfig']->value['allow_teacher_rolls_days_before'] && ($_smarty_tpl->tpl_vars['dateNow']->value != $_smarty_tpl->tpl_vars['cell']->value['attendance_date'])) {?>
                            <td align="center"
                                    <?php if ($_smarty_tpl->tpl_vars['cell']->value['status'] == @constant('ATTENDANCE_COME_LATE')) {?>
                                        bgcolor="#6495ed"
                                    <?php } elseif ($_smarty_tpl->tpl_vars['cell']->value['status'] == @constant('ATTENDANCE_EARLY_LEAVE')) {?>
                                        bgcolor="#008b8b"
                                    <?php } elseif ($_smarty_tpl->tpl_vars['cell']->value['status'] == @constant('ATTENDANCE_ABSENCE_NO_REASON')) {?>
                                        bgcolor="#ff1493"
                                    <?php } elseif ($_smarty_tpl->tpl_vars['cell']->value['status'] == @constant('ATTENDANCE_ABSENCE')) {?>
                                        bgcolor="#deb887"
                                    <?php }?>
                            >
                                <?php if (($_smarty_tpl->tpl_vars['cell']->value['status'] == @constant('ATTENDANCE_PRESENT')) || ($_smarty_tpl->tpl_vars['cell']->value['status'] == @constant('ATTENDANCE_COME_LATE')) || ($_smarty_tpl->tpl_vars['cell']->value['status'] == @constant('ATTENDANCE_EARLY_LEAVE'))) {?>
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                    <?php $_smarty_tpl->_assignInScope('presentCnt', $_smarty_tpl->tpl_vars['presentCnt']->value+1);
?>
                                <?php } else { ?>
                                    <i class="fa fa-times" aria-hidden="true"></i>
                                    <?php $_smarty_tpl->_assignInScope('absenceCnt', $_smarty_tpl->tpl_vars['absenceCnt']->value+1);
?>
                                <?php }?>
                            </td>
                        <?php } else { ?>
                            <td align="center" style="vertical-align: middle; padding: 0px; position: relative">
								<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/includes/assets/css/font-awesome/css/font-awesome.css">
                                <select data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['cell']->value['attendance_id'];?>
" data-child="<?php echo $_smarty_tpl->tpl_vars['cell']->value['child_id'];?>
" data-date="<?php echo $_smarty_tpl->tpl_vars['cell']->value['attendance_date'];?>
" data-status="<?php echo $_smarty_tpl->tpl_vars['cell']->value['status'];?>
" name="attendance_status" class="js_class-attendance-status" style="border:none; font-family: 'FontAwesome', 'Second Font name'; position: absolute; top: 0; left: 0; width: 100%; height: 100%; <?php if ($_smarty_tpl->tpl_vars['cell']->value['status'] == @constant('ATTENDANCE_COME_LATE')) {?>
                                        background: #6495ed;
                                <?php } elseif ($_smarty_tpl->tpl_vars['cell']->value['status'] == @constant('ATTENDANCE_PRESENT')) {?>
                                        background: #fff;
                                <?php } elseif ($_smarty_tpl->tpl_vars['cell']->value['status'] == @constant('ATTENDANCE_EARLY_LEAVE')) {?>
                                        background: #008b8b;
                                <?php } elseif ($_smarty_tpl->tpl_vars['cell']->value['status'] == @constant('ATTENDANCE_ABSENCE_NO_REASON')) {?>
                                        background: #ff1493;
                                <?php } elseif ($_smarty_tpl->tpl_vars['cell']->value['status'] == @constant('ATTENDANCE_ABSENCE')) {?>
                                        background: #deb887;
                                <?php }?>;
                                        -webkit-appearance: none;
                                        -moz-appearance: none;
                                        text-indent: 1px;
                                        text-overflow: '';
                                        text-align-last: center;
                                        ">
                                    <option value="<?php echo @constant('ATTENDANCE_PRESENT');?>
" style="background: #fff; padding: 5px 0; font-size: 20px;">&#xf00c</option>

                                    <?php if ($_smarty_tpl->tpl_vars['schoolConfig']->value['attendance_use_come_late'] || $_smarty_tpl->tpl_vars['cell']->value['status'] == @constant('ATTENDANCE_COME_LATE')) {?>
                                        <option value="<?php echo @constant('ATTENDANCE_COME_LATE');?>
" style="background: #6495ed; padding: 5px 0; font-size: 20px;" <?php if ($_smarty_tpl->tpl_vars['cell']->value['status'] == @constant('ATTENDANCE_COME_LATE')) {?> selected <?php }?>>&#xf00c</option>
                                    <?php }?>
                                    <?php if ($_smarty_tpl->tpl_vars['schoolConfig']->value['attendance_use_leave_early'] || $_smarty_tpl->tpl_vars['cell']->value['status'] == @constant('ATTENDANCE_EARLY_LEAVE')) {?>
                                        <option value="<?php echo @constant('ATTENDANCE_EARLY_LEAVE');?>
" style="background: #008b8b; padding: 5px 0; font-size: 20px;" <?php if ($_smarty_tpl->tpl_vars['cell']->value['status'] == @constant('ATTENDANCE_EARLY_LEAVE')) {?> selected <?php }?>>&#xf00c</option>
                                    <?php }?>
                                    <?php if ($_smarty_tpl->tpl_vars['schoolConfig']->value['attendance_absence_no_reason'] || $_smarty_tpl->tpl_vars['cell']->value['status'] == @constant('ATTENDANCE_ABSENCE_NO_REASON')) {?>
                                        <option value="<?php echo @constant('ATTENDANCE_ABSENCE_NO_REASON');?>
" style="background: #ff1493; padding: 5px 0; font-size: 20px;" <?php if ($_smarty_tpl->tpl_vars['cell']->value['status'] == @constant('ATTENDANCE_ABSENCE_NO_REASON')) {?> selected <?php }?>>&#xf00d</option>
                                    <?php }?>
                                    <option value="<?php echo @constant('ATTENDANCE_ABSENCE');?>
" style="background: #deb887; padding: 5px 0; font-size: 20px;" <?php if ($_smarty_tpl->tpl_vars['cell']->value['status'] == @constant('ATTENDANCE_ABSENCE')) {?>selected<?php }?>>&#xf00d</option>
                                </select>
                                <?php if (($_smarty_tpl->tpl_vars['cell']->value['status'] == @constant('ATTENDANCE_PRESENT')) || ($_smarty_tpl->tpl_vars['cell']->value['status'] == @constant('ATTENDANCE_COME_LATE')) || ($_smarty_tpl->tpl_vars['cell']->value['status'] == @constant('ATTENDANCE_EARLY_LEAVE'))) {?>
                                    <?php $_smarty_tpl->_assignInScope('presentCnt', $_smarty_tpl->tpl_vars['presentCnt']->value+1);
?>
                                <?php } else { ?>
                                    <?php $_smarty_tpl->_assignInScope('absenceCnt', $_smarty_tpl->tpl_vars['absenceCnt']->value+1);
?>
                                <?php }?>
                            </td>
                        <?php }?>
                    <?php } else { ?>
                        <td bgcolor="#a9a9a9"></td>
                    <?php }?>
                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                <td align="center" id="present_<?php echo $_smarty_tpl->tpl_vars['row']->value['child_id'];?>
" style="color: blue; font-weight: bold"><?php echo $_smarty_tpl->tpl_vars['presentCnt']->value;?>
</td>
                <td align="center" id="absence_<?php echo $_smarty_tpl->tpl_vars['row']->value['child_id'];?>
" style="color: red; font-weight: bold"><?php echo $_smarty_tpl->tpl_vars['absenceCnt']->value;?>
</td>
            </tr>
            
            <?php if ($_smarty_tpl->tpl_vars['rowIdx']->value%16 == 0) {?>
                <tr>
                    <th class="pinned">#</th>
                    <th nowrap="true" class="pinned"><?php echo __("Full name");?>
</th>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['dates']->value, 'date');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['date']->value) {
?>
                        <th align="center"><?php echo $_smarty_tpl->tpl_vars['date']->value;?>
</th>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                    <th nowrap="true"><?php echo __("Present");?>
</th>
                    <th nowrap="true"><?php echo __("Absence");?>
</th>
                </tr>
            <?php }?>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

        <?php if (($_smarty_tpl->tpl_vars['rowIdx']->value > 2)) {?>
            <tr>
                <td colspan="2" align="center" class="pinned"><strong><?php echo __('Present total of class');?>
</strong></td>
                <?php $_smarty_tpl->_assignInScope('presentTotal', 0);
?>
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['last_rows']->value, 'cell');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['cell']->value) {
?>
                    <td id="present_count_<?php echo $_smarty_tpl->tpl_vars['cell']->value['attendance_id'];?>
" align="center" bgcolor="#ffebcd" style="color: blue; font-weight: bold">
                        <?php if ($_smarty_tpl->tpl_vars['cell']->value['is_checked'] == 1) {?>
                            <?php echo $_smarty_tpl->tpl_vars['cell']->value['present_count'];?>

                            <?php $_smarty_tpl->_assignInScope('presentTotal', $_smarty_tpl->tpl_vars['presentTotal']->value+$_smarty_tpl->tpl_vars['cell']->value['present_count']);
?>
                        <?php }?>
                    </td>
                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                
                <td id="total_present" align="center" bgcolor="#ffebcd" style="color: blue; font-weight: bold">
                    <?php echo $_smarty_tpl->tpl_vars['presentTotal']->value;?>

                </td>
                <td align="center" bgcolor="#ffebcd"></td>
            </tr>
            <tr>
                <td colspan="2" align="center" class="pinned"><strong><?php echo __('Absence total of class');?>
</strong></td>
                <?php $_smarty_tpl->_assignInScope('absentTotal', 0);
?>
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['last_rows']->value, 'cell');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['cell']->value) {
?>
                    <td id="absence_count_<?php echo $_smarty_tpl->tpl_vars['cell']->value['attendance_id'];?>
" align="center" bgcolor="#ffebcd" style="color: red; font-weight: bold">
                        <?php if ($_smarty_tpl->tpl_vars['cell']->value['is_checked'] == 1) {?>
                            <?php echo $_smarty_tpl->tpl_vars['cell']->value['absence_count'];?>

                            <?php $_smarty_tpl->_assignInScope('absentTotal', $_smarty_tpl->tpl_vars['absentTotal']->value+$_smarty_tpl->tpl_vars['cell']->value['absence_count']);
?>
                        <?php }?>
                    </td>
                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                <td align="center" bgcolor="#ffebcd"></td>
                
                <td id="total_absence" align="center" bgcolor="#ffebcd" style="color: red; font-weight: bold">
                    <?php echo $_smarty_tpl->tpl_vars['absentTotal']->value;?>

                </td>
            </tr>
        <?php }?>
        </tbody>
    </table>
</div>


<?php echo '<script'; ?>
 type="text/javascript">

    var $table = $('.table-pinned');
    var $fixedColumn = $table.clone().insertBefore($table).addClass('fixed-column');
    $fixedColumn.find('th').each(function (i, elem) {
        $(this).width($table.find('th:eq(' + i + ')').width());
    });
    $fixedColumn.find('td').each(function (i, elem) {
        $(this).width($table.find('td:eq(' + i + ')').width());
    });
    $fixedColumn.find('th,td').not('.pinned').hide();
    $fixedColumn.find('[id]').each(function () {
        $(this).removeAttr('id');
    });
    $fixedColumn.find('tr').each(function (i, elem) {
        $(this).height($table.find('tr:eq(' + i + ')').height());
    });

    $(window).resize(function () {
        $fixedColumn.find('tr').each(function (i, elem) {
            $(this).height($table.find('tr:eq(' + i + ')').height());
        });
        $fixedColumn.find('td').each(function (i, elem) {
            $(this).addClass('white-space_nowrap');
            $(this).width($table.find('td:eq(' + i + ')').width());
        });
    });

    function fixDiv() {
        var $cache = $('#getFixed');
        var $button = $('#table_button');
        if ($(window).scrollTop() > 100) {
            $cache.css({
                'position': 'fixed',
                'top': '50px'
            });
            $cache.width($('#attendance_list').width() - 1);
        }
        else
            $cache.css({
                'position': 'relative',
                'top': 'auto'
            });

        if ($(window).scrollTop() > 100)
            $button.css({
                'position': 'fixed',
                'top': '90px'
            });
        else
            $button.css({
                'position': 'relative',
                'top': 'auto'
            });
    }
    $(window).scroll(fixDiv);
    fixDiv();
<?php echo '</script'; ?>
><?php }
}
