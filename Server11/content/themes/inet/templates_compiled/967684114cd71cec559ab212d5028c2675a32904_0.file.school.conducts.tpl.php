<?php
/* Smarty version 3.1.31, created on 2021-06-07 10:17:51
  from "D:\workplace\Inet-project\source\Server11\content\themes\inet\templates\ci\school\school.conducts.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_60bd8fdf4c11a0_07516920',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '967684114cd71cec559ab212d5028c2675a32904' => 
    array (
      0 => 'D:\\workplace\\Inet-project\\source\\Server11\\content\\themes\\inet\\templates\\ci\\school\\school.conducts.tpl',
      1 => 1623035814,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:ci/school/ajax.conductlist.tpl' => 1,
  ),
),false)) {
function content_60bd8fdf4c11a0_07516920 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="" style="position: relative">
    <div class="panel panel-default">
        <div class="panel-heading with-icon">
            <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == "edit") {?>
                <div class="pull-right flip">
                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/conducts" class="btn btn-default">
                        <i class="fa fa-list"></i> <?php echo __("Lists");?>

                    </a>
                </div>
            <?php }?>
            <i class="fa fa-gavel fa-fw fa-lg pr10"></i>
            <?php echo __("Conducts");?>

            <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == "edit") {?>
                &rsaquo; <?php echo __('Edit');?>

            <?php }?>
        </div>
        <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
            <div class="panel-body with-table">
                <div class="js_ajax-forms form-horizontal">
                    <input type="hidden" name="school_username" id="school_username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                    <input type="hidden" name="do" value="search_conduct"/>
                    <div class="form-group">
                        <label class="col-sm-2 control-label text-left"><?php echo __("Class");?>
</label>
                        <div class="col-sm-4" name="conduct_class">
                            <select name="class_id" id="co_class_id" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-view="show" class="form-control" autofocus>
                                <option value=""><?php echo __("Select class");?>
</option>

                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['classes']->value, 'class');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class']->value) {
?>



                                    <option value="<?php echo $_smarty_tpl->tpl_vars['class']->value['group_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['class']->value['group_title'];?>
</option>

                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                            </select>
                        </div>
                        <div class="col-sm-4 ">
                            <select name="school_year" id="school_year" class="form-control">
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['default_year']->value, 'year', false, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['year']->value) {
?>
                                    
                                    <option value="<?php echo $_smarty_tpl->tpl_vars['year']->value;?>
" <?php if ((substr($_smarty_tpl->tpl_vars['year']->value,0,strlen(date("Y"))) === date("Y"))) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['year']->value;?>
</option>
                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2"></label>
                        <div class="col-sm-2">
                            <button type="submit" class="btn btn-default js_conduct-search"><?php echo __("Search");?>
</button>
                        </div>
                    </div>

                    <div class="table-responsive" id="conduct_list" name="conduct_list">
                        <?php $_smarty_tpl->_subTemplateRender("file:ci/school/ajax.conductlist.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                    </div>
                    <!-- success -->
                    <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                    <!-- error -->
                    <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                </div>
            </div>
        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "edit") {?>
            <div class="panel-body">
                <form class="form-horizontal" enctype="multipart/form-data" method="post" id="school_edit_conduct">
                    <input type="hidden" name="school_username" id="school_username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                    <input type="hidden" name="conduct_id" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['conduct_id'];?>
"/>
                    <input type="hidden" name="child_id" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
"/>
                    <input type="hidden" name="do" value="edit"/>
                    <div class = "form-group">
                        <label class="col-sm-3 control-label text-left">
                            <?php echo __("Student");?>

                        </label>
                        <div class = "col-sm-9">
                            <input type="text" class="form-control" value = "<?php echo $_smarty_tpl->tpl_vars['child']->value['child_name'];?>
 - <?php echo $_smarty_tpl->tpl_vars['child']->value['birthday'];?>
" disabled>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left"><?php echo __("Semester");?>
 1
                        </label>
                        <div class="col-sm-9">
                            <select name="conduct_id1" id="conduct_id1" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-view="edit" class="form-control">
                                <option value=""><?php echo __("Select conduct");?>
</option>
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['conducts']->value, 'conduct');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['conduct']->value) {
?>
                                    <option value="<?php echo $_smarty_tpl->tpl_vars['conduct']->value;?>
" <?php if ($_smarty_tpl->tpl_vars['data']->value['hk1'] == $_smarty_tpl->tpl_vars['conduct']->value) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['conduct']->value;?>
</option>
                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                
                                
                                
                                
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left"><?php echo __("Semester");?>
 2
                        </label>
                        <div class="col-sm-9">
                            <select name="conduct_id2" id="conduct_id2" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-view="edit" class="form-control">
                                <option value=""><?php echo __("Select conduct");?>
</option>
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['conducts']->value, 'conduct');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['conduct']->value) {
?>
                                    <option value="<?php echo $_smarty_tpl->tpl_vars['conduct']->value;?>
" <?php if ($_smarty_tpl->tpl_vars['data']->value['hk2'] == $_smarty_tpl->tpl_vars['conduct']->value) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['conduct']->value;?>
</option>
                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left"><?php echo __("End semester");?>

                        </label>
                        <div class="col-sm-9">
                            <select name="conduct_id3" id="conduct_id3" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-view="edit" class="form-control">
                                <option value=""><?php echo __("Select conduct");?>
</option>
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['conducts']->value, 'conduct');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['conduct']->value) {
?>
                                    <option value="<?php echo $_smarty_tpl->tpl_vars['conduct']->value;?>
" <?php if ($_smarty_tpl->tpl_vars['data']->value['ck'] == $_smarty_tpl->tpl_vars['conduct']->value) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['conduct']->value;?>
</option>
                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-3">
                            <button type="submit" class="btn btn-primary padrl30"><?php echo __("Save");?>
</button>
                        </div>
                    </div>

                    <!-- success -->
                    <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                    <!-- success -->

                    <!-- error -->
                    <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                    <!-- error -->
                </form>
            </div>
        <?php }?>
    </div>
</div><?php }
}
