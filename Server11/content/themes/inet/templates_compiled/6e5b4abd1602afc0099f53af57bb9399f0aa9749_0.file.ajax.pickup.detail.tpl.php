<?php
/* Smarty version 3.1.31, created on 2021-03-31 10:32:11
  from "D:\workplace\Server11\content\themes\inet\templates\ci\school\teacher_pickup\ajax.pickup.detail.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_6063ed3b97fbb0_36890083',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '6e5b4abd1602afc0099f53af57bb9399f0aa9749' => 
    array (
      0 => 'D:\\workplace\\Server11\\content\\themes\\inet\\templates\\ci\\school\\teacher_pickup\\ajax.pickup.detail.tpl',
      1 => 1552404700,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6063ed3b97fbb0_36890083 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="panel-body with-table">
    <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_teacherpickup.php">
        <input type="hidden" id="school_username" name="school_username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
        <input type="hidden" id="pickup_date" name="pickup_date" value="<?php echo $_smarty_tpl->tpl_vars['today']->value;?>
"/>
        <input type="hidden" id="pickup_id" name="pickup_id" value="<?php echo $_smarty_tpl->tpl_vars['pickup']->value['pickup_id'];?>
"/>
        <input type="hidden" id="callback" name="callback" value="detail"/>
        <input type="hidden" name="do" value="save"/>
        <div id="time_option" class="x-hidden" data-value="<?php echo $_smarty_tpl->tpl_vars['time_option']->value;?>
"></div>

        <div class="text-center">
            <strong><label style="font-size: 15px"><?php echo mb_strtoupper(__("Late pickup on"), 'UTF-8');?>
: <?php echo $_smarty_tpl->tpl_vars['today']->value;?>
</label></strong><br>
            <?php if (!is_empty($_smarty_tpl->tpl_vars['pickup']->value['class_name'])) {?>
                <strong><label style="font-size: 15px"><?php echo __("Class");?>
: <?php echo $_smarty_tpl->tpl_vars['pickup']->value['class_name'];?>
</label></strong><br><br>
            <?php } else { ?>
                <br>
            <?php }?>
        </div>
        <div class="col-sm-12">
            <label class="col-sm-6 text-right" style="font-size: 15px"><?php echo __("Late pickup teachers");?>
:</label>
            <?php if (count($_smarty_tpl->tpl_vars['pickup']->value['assign']) > 0) {?>
                <div class="col-sm-6">
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['pickup']->value['assign'], 'assign');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['assign']->value) {
?>
                        <strong>
                            <label>&#9679;&#09;<?php echo $_smarty_tpl->tpl_vars['assign']->value['user_fullname'];?>
</label>
                        </strong>
                        <br>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                </div>
            <?php } else { ?>
                <div class="col-sm-6 text-left" style="font-size: 15px;color: red"><?php echo __("Not assigned");?>
</div>
            <?php }?>
        </div>

        <div>
            <strong><?php echo __("Total student");?>
: <font color="red"><?php echo count($_smarty_tpl->tpl_vars['children']->value);?>
</font> |
                <?php echo __("Picked up");?>
: <font color="red"><?php echo $_smarty_tpl->tpl_vars['child_count']->value;?>
</font>
            </strong>
        </div>
        <?php if ($_smarty_tpl->tpl_vars['is_today']->value || $_smarty_tpl->tpl_vars['allow_teacher_edit_pickup_before']->value) {?>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th width="5%">#</th>
                        <th width="25%"><?php echo __("Student name");?>
</th>
                        <th width="20%"><?php echo __("Birthdate");?>
</th>
                        <th width="20%"><?php echo __("Class");?>
</th>
                        <th width="15%"><?php echo __("Status");?>
</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['children']->value, 'child');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['child']->value) {
?>
                        <input type="hidden" name="child[]" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
"/>
                        <tr <?php if ($_smarty_tpl->tpl_vars['child']->value['child_status'] == 0) {?> class="row-disable"<?php }?>>
                            <td align="center"><strong><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</strong></td>
                            <td><strong><?php echo $_smarty_tpl->tpl_vars['child']->value['child_name'];?>
</strong></td>
                            <td align="center"><?php echo $_smarty_tpl->tpl_vars['child']->value['birthday'];?>
</td>
                            <td align="center"><?php echo $_smarty_tpl->tpl_vars['child']->value['group_title'];?>
</td>
                            <td rowspan="4" align="center">
                                <?php if (is_empty($_smarty_tpl->tpl_vars['child']->value['pickup_at'])) {?>
                                    <?php if ($_smarty_tpl->tpl_vars['is_today']->value) {?>
                                        <a class="btn btn-xs btn-default js_class-pickup" data-handle="pickup"
                                           data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-child="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
"
                                           data-pickup="<?php echo $_smarty_tpl->tpl_vars['child']->value['pickup_id'];?>
">
                                            <?php echo __("Pickup");?>

                                        </a>
                                    <?php }?>
                                <?php }?>
                                <a class="btn btn-xs btn-danger js_class-pickup" data-handle="cancel"
                                   data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-child="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
"
                                   data-pickup="<?php echo $_smarty_tpl->tpl_vars['child']->value['pickup_id'];?>
">
                                    <?php echo __("Cancel");?>

                                </a>
                            </td>
                        </tr>
                        <tr <?php if ($_smarty_tpl->tpl_vars['child']->value['child_status'] == 0) {?> class="row-disable"<?php }?>>
                            <td colspan="1" rowspan="3"></td>
                            <td colspan="3" style="padding-left: 50px">
                                <div class="col-sm-4 pt3">
                                    <?php echo __("Pickup at");?>

                                    : &nbsp;
                                </div>
                                <?php if (!is_empty($_smarty_tpl->tpl_vars['child']->value['pickup_at'])) {?>
                                    <input type='text' name="pickup_time[]" class="pickup_teacher_time" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['pickup_at'];?>
" style="width: 70px; font-weight: bold; text-align: center"/>
                                <?php } else { ?>
                                    <input type='text' name="pickup_time[]" class="pickup_teacher_time" value="" style="width: 70px; font-weight: bold;color: red; text-align: center"/>
                                <?php }?>
                                <input type="hidden" min="0"
                                       id="total_pickup_<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
"
                                       data-child="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
"
                                       name="pickup_fee[]" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['late_pickup_fee'];?>
"/>
                            </td>
                        </tr>
                        <tr <?php if ($_smarty_tpl->tpl_vars['child']->value['child_status'] == 0) {?> class="row-disable"<?php }?>>
                            <td colspan="3" align="left" style="padding-left: 50px">
                                <?php if (!empty($_smarty_tpl->tpl_vars['child']->value['services'])) {?>
                                    <div class="col-sm-4 pt3">
                                        <?php echo __("Using service");?>
: &nbsp;
                                    </div>
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['child']->value['services'], 'service');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['service']->value) {
?>
                                        <input type="checkbox" class="pickup_service_fee" name="service_<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
[]"
                                               id="pickup_service_fee_<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
_<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
"
                                               data-child="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
"
                                               data-service="<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
"
                                               data-fee="<?php echo $_smarty_tpl->tpl_vars['service']->value['price'];?>
"
                                               value="<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
" <?php if (!is_null($_smarty_tpl->tpl_vars['service']->value['using_at'])) {?> checked <?php }?>/>
                                        <?php echo $_smarty_tpl->tpl_vars['service']->value['service_name'];?>

                                        &nbsp;&nbsp;&nbsp;
                                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                <?php } else { ?>
                                    &nbsp;
                                <?php }?>
                                
                            </td>

                        </tr>

                        <tr <?php if ($_smarty_tpl->tpl_vars['child']->value['child_status'] == 0) {?> class="row-disable"<?php }?>>
                            <td colspan="3" style="padding-left: 50px">
                                <div class="col-sm-4 pt3">
                                    <?php echo __("Note");?>
 : &nbsp;
                                </div>
                                <input type='text' name="pickup_note[]"  value="<?php echo $_smarty_tpl->tpl_vars['child']->value['description'];?>
" style="width: 60%; color: blue"/>
                                <input type="hidden" min="0"
                                       id="total_<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
"
                                       name="total_child[]"
                                       value="<?php echo $_smarty_tpl->tpl_vars['child']->value['total_amount'];?>
"/>
                            </td>
                        </tr>
                        <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                    <?php if (count($_smarty_tpl->tpl_vars['children']->value) == 0) {?>
                        <tr class="odd">
                            <td valign="top" align="center" colspan="5" class="dataTables_empty">
                                <?php echo __("No data available in table");?>

                            </td>
                        </tr>
                    <?php }?>
                    <input type="hidden" id="total" name="total" value="<?php echo $_smarty_tpl->tpl_vars['pickup']->value['total'];?>
"/>
                    </tbody>
                </table>
            </div>

            <div class="form-group">
                <div class="col-sm-9 pl10">
                    <button type="submit"
                            class="btn btn-primary <?php if (count($_smarty_tpl->tpl_vars['children']->value) == 0) {?> x-hidden <?php }?>"><?php echo __("Save");?>
</button>
                </div>
            </div>
            <!-- success -->
            <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
            <!-- success -->

            <!-- error -->
            <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
            <!-- error -->

        <?php } else { ?>

            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th width="10%">#</th>
                        <th width="30%"><?php echo __("Student name");?>
</th>
                        <th width="30%"><?php echo __("Birthdate");?>
</th>
                        <th width="30%"><?php echo __("Class");?>
</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['children']->value, 'child');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['child']->value) {
?>
                        <input type="hidden" name="child[]" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
"/>
                        <tr <?php if ($_smarty_tpl->tpl_vars['child']->value['child_status'] == 0) {?> class="row-disable"<?php }?>>
                            <td align="center"><strong><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</strong></td>
                            <td><strong><?php echo $_smarty_tpl->tpl_vars['child']->value['child_name'];?>
</strong></td>
                            <td align="center"><?php echo $_smarty_tpl->tpl_vars['child']->value['birthday'];?>
</td>
                            <td align="center"><?php echo $_smarty_tpl->tpl_vars['child']->value['group_title'];?>
</td>
                        </tr>
                        <tr <?php if ($_smarty_tpl->tpl_vars['child']->value['child_status'] == 0) {?> class="row-disable"<?php }?>>
                            <td colspan="1" rowspan="3"></td>
                            <td colspan="3" style="padding-left: 50px">
                                <div class="col-sm-4 pt3">
                                    <?php echo __("Pickup at");?>

                                    : &nbsp;
                                </div>
                                <?php if (!is_empty($_smarty_tpl->tpl_vars['child']->value['pickup_at'])) {?>
                                    <input type='text'  value="<?php echo $_smarty_tpl->tpl_vars['child']->value['pickup_at'];?>
" style="width: 70px; font-weight: bold; text-align: center" readonly/>
                                <?php } else { ?>
                                    <input type='text' value="" style="width: 70px" readonly/>
                                <?php }?>
                            </td>
                        </tr>
                        <tr <?php if ($_smarty_tpl->tpl_vars['child']->value['child_status'] == 0) {?> class="row-disable"<?php }?>>
                            <td colspan="3" align="left" style="padding-left: 50px">
                                <?php if (!empty($_smarty_tpl->tpl_vars['child']->value['services'])) {?>
                                    <div class="col-sm-4 pt3">
                                        <?php echo __("Using service");?>
: &nbsp;
                                    </div>
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['child']->value['services'], 'service');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['service']->value) {
?>
                                        <input type="checkbox" <?php if (!is_null($_smarty_tpl->tpl_vars['service']->value['using_at'])) {?> checked <?php }?> onclick="return false;"/>
                                        <?php echo $_smarty_tpl->tpl_vars['service']->value['service_name'];?>

                                        &nbsp;&nbsp;&nbsp;
                                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                <?php } else { ?>
                                    &nbsp;
                                <?php }?>
                            </td>

                        </tr>

                        <tr <?php if ($_smarty_tpl->tpl_vars['child']->value['child_status'] == 0) {?> class="row-disable"<?php }?>>
                            <td colspan="3" style="padding-left: 50px">
                                <div class="col-sm-4 pt3">
                                    <?php echo __("Note");?>
 : &nbsp;
                                </div>
                                <input type='text' name="pickup_note[]"  value="<?php echo $_smarty_tpl->tpl_vars['child']->value['description'];?>
" style="width: 60%; color: blue"/>
                            </td>
                        </tr>
                        <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                    <?php if (count($_smarty_tpl->tpl_vars['children']->value) == 0) {?>
                        <tr class="odd">
                            <td valign="top" align="center" colspan="5" class="dataTables_empty">
                                <?php echo __("No data available in table");?>

                            </td>
                        </tr>
                    <?php }?>
                    </tbody>
                </table>
            </div>

        <?php }?>

    </form>
</div><?php }
}
