<?php
/* Smarty version 3.1.31, created on 2021-06-25 10:39:02
  from "D:\workplace\Inet-project\source\Server11\content\themes\inet\templates\_footer_new.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_60d54fd65ed305_94799800',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '9e77ad7c20531491327ede8d1c8d9063dfe0f14b' => 
    array (
      0 => 'D:\\workplace\\Inet-project\\source\\Server11\\content\\themes\\inet\\templates\\_footer_new.tpl',
      1 => 1623982241,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:_js_files.tpl' => 1,
    'file:_js_templates.tpl' => 1,
  ),
),false)) {
function content_60d54fd65ed305_94799800 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="footer-v1">
    <div class="copyright">
        <div class="row">
            <div class="col-md-4">
                <p style="font-size: 12px">
                    <?php echo date('Y');?>
 &copy; <?php echo $_smarty_tpl->tpl_vars['system']->value['system_title'];?>
.
                    
                    <a href = "#" class="text-link" data-toggle="modal" data-url="#translator"><?php echo $_smarty_tpl->tpl_vars['system']->value['language']['title'];?>
</a> |
                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/privacy"><?php echo __("Privacy");?>
</a> | <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/terms"><?php echo __("Terms");?>
</a>
                </p>
            </div>






            <!-- Social Links -->
            <div class="col-md-3" style="float: right;">
                <ul class="footer-socials list-inline">
                    <li>
                        <a href="https://www.facebook.com/mobiedu.vn" class="tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Facebook">
                            <i class="fa fa-facebook-f"></i>
                        </a>
                    </li>
                    <!-- DELETE START MANHDD 18/06/2021 -->










                    <!-- DELETE END MANHDD 18/06/2021 -->
                </ul>
            </div>
            <!-- End Social Links -->
        </div>
        <div class="row">
            <div class="col-sm-12" align="center">
            </div>
        </div>
    </div><!--/copyright-->
</div>
<div id="app_android" style="display: none" class="app_pop">
    <div class="android_down app_down_box">
        <a href="<?php echo @constant('GOOGLEPLAY_URL');?>
" target="_blank"> <img
                    class=""
                    src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/content/themes/<?php echo $_smarty_tpl->tpl_vars['system']->value['theme'];?>
/images/favicon.png"
                    alt="Ứng dụng mầm non Inet CH Play"/> <?php echo __("Download Inet App");?>
</a>
        <div class="down_app_hide_home">
            <i class="fa fa-close"></i>
        </div>
    </div>
</div>
<div id="app_ios" style="display: none" class="app_pop">
    <div class="ios_down app_down_box">
        <a href="<?php echo @constant('APPSTORE_URL');?>
" target="_blank"><img class=""
                                                                    src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/content/themes/<?php echo $_smarty_tpl->tpl_vars['system']->value['theme'];?>
/images/favicon.png"
                                                                    alt="Ứng dụng mầm non Inet App store"/> <?php echo __("Download Inet App");?>
</a>
        <div class="down_app_hide_home">
            <i class="fa fa-close"></i>
        </div>
    </div>
</div>
<?php echo '<script'; ?>
>
    function getMobileOperatingSystem() {
        var userAgent = navigator.userAgent || navigator.vendor || window.opera;
        // Windows Phone must come first because its UA also contains "Android"
        if (/windows phone/i.test(userAgent)) {
            return "Windows Phone";
        }

        if (/android/i.test(userAgent)) {
            document.getElementById("app_android").style.display = 'block';
            document.getElementById("ios_banner").style.display = 'none';
            document.getElementById("android_banner").style.marginRight = '0';
            return "Android";
        }

        // iOS detection from: http://stackoverflow.com/a/9039885/177710
        if (/iPad|iPhone|iPod/i.test(userAgent)) {
            document.getElementById("app_ios").style.display = 'block';
            document.getElementById("android_banner").style.display = 'none';
            document.getElementById("ios_banner").style.marginRight = '0';
            return "iOS";
        }
        return "Web";
    }
    getMobileOperatingSystem();
    // if(x == Adroi)
    // document.getElementById("osmobile").innerHTML = x;
<?php echo '</script'; ?>
>
</div><!--/wrapper-->
<!-- JS Files -->
<?php $_smarty_tpl->_subTemplateRender('file:_js_files.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<!-- JS Files -->

<!-- JS Templates -->
<?php $_smarty_tpl->_subTemplateRender('file:_js_templates.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<!-- JS Templates -->

<!-- Analytics Code -->

<!-- Global site tag (gtag.js) - Google Analytics -->

    <?php echo '<script'; ?>
 async src="https://www.googletagmanager.com/gtag/js?id=UA-87252737-2"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-87252737-2');
    <?php echo '</script'; ?>
>

<!-- Analytics Code -->

<!-- DELETE START - MANHDD - 25/05/2021-->
<!-- Facebook Account Kit SDK-->

<!-- HTTPS required. HTTP will give a 403 forbidden response -->






























































































































<!-- DELETE END - MANHDD - 25/05/2021-->
<!--End of Tawk.to Script-->
</body>
</html>
<?php }
}
