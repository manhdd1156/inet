<?php
/* Smarty version 3.1.31, created on 2021-03-31 10:36:38
  from "D:\workplace\Server11\content\themes\inet\templates\ci\school\ajax.school.tuition.children.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_6063ee46ec4158_75435990',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'fbf32fa8ebffa07f428baa9db81e3bc4e551a843' => 
    array (
      0 => 'D:\\workplace\\Server11\\content\\themes\\inet\\templates\\ci\\school\\ajax.school.tuition.children.tpl',
      1 => 1573458086,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:ci/school/ajax.school.tuition.child.tpl' => 1,
  ),
),false)) {
function content_6063ee46ec4158_75435990 (Smarty_Internal_Template $_smarty_tpl) {
?>

<?php if ($_smarty_tpl->tpl_vars['school']->value['tuition_view_attandance'] && ($_smarty_tpl->tpl_vars['school']->value['attendance_use_leave_early'] || $_smarty_tpl->tpl_vars['school']->value['attendance_use_come_late'] || $_smarty_tpl->tpl_vars['school']->value['attendance_absence_no_reason'])) {?>
    <table class="table table-striped table-bordered table-hover">
        <tbody>
        <tr>
            <td align="right"><strong><?php echo __("Symbol");?>
:</strong></td>
            <?php if ($_smarty_tpl->tpl_vars['school']->value['attendance_use_come_late']) {?>
                <td bgcolor="#6495ed" align="center"><i class="fa fa-check" aria-hidden="true"></td>
                <td align="left"><?php echo __("Come late");?>
</td>
            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['school']->value['attendance_use_leave_early']) {?>
                <td bgcolor="#008b8b" align="center"><i class="fa fa-check" aria-hidden="true"></td>
                <td align="left"><?php echo __("Leave early");?>
</td>
            <?php }?>
            <td align="center"><i class="fa fa-check" aria-hidden="true"></td>
            <td align="left"><?php echo __("Present");?>
</td>

            <?php if ($_smarty_tpl->tpl_vars['school']->value['attendance_absence_no_reason']) {?>
                <td bgcolor="#ff1493" align="center"><i class="fa fa-times" aria-hidden="true"></td>
                <td align="left"><?php echo __("Without permission");?>
</td>
            <?php }?>

            <td bgcolor="#deb887" align="center"><i class="fa fa-times" aria-hidden="true"></td>
            <td align="left"><?php echo __("With permission");?>
</td>
        </tr>
        </tbody>
    </table>
<?php }?>
<div id="open_dialog" class="x-hidden" title="<?php echo mb_strtoupper(__("Adding fees to calculate tuition"), 'UTF-8');?>
"></div>
<table class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th>
            <div class="pull-left flip"><?php echo mb_strtoupper(__("Children list"), 'UTF-8');?>
&nbsp;(<?php echo count($_smarty_tpl->tpl_vars['results']->value['children']);?>
)</div>
            <div class="pull-right flip">
                <?php echo __("Total of class");?>
&nbsp;(<?php echo @constant('MONEY_UNIT');?>
)
                <input style="width: 100px; background-color: #EEEEEE" type="text" class="text-right money_tui" id="class_total" name="total_amount" value="<?php echo $_smarty_tpl->tpl_vars['results']->value['total_amount'];?>
" readonly/>
            </div>
        </th>
    </tr>
    </thead>
    <tbody>
    <?php $_smarty_tpl->_assignInScope('childIdx', 0);
?>
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['results']->value['children'], 'child');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['child']->value) {
?>
        <?php $_smarty_tpl->_assignInScope('childIdx', $_smarty_tpl->tpl_vars['childIdx']->value+1);
?>
        <tr id="child_row_<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
">
            <td id="child_tuition_<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
">
                <?php $_smarty_tpl->_subTemplateRender("file:ci/school/ajax.school.tuition.child.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

            </td>
        </tr>
        <tr height = "15"></tr>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

    <?php if (count($_smarty_tpl->tpl_vars['results']->value['children']) > 0) {?>
        <tr>
            <td>
                <button type="submit" class="btn btn-primary padrl30"><?php echo __("Save");?>
</button>
                <div class="pull-right flip">
                    <strong>
                        <?php echo __("Total of class");?>
&nbsp;(<?php echo @constant('MONEY_UNIT');?>
)
                        <input style="width: 100px; background-color: #EEEEEE" type="text" class="text-right money_tui" id="class_total_2" name="class_total_2" value="<?php echo $_smarty_tpl->tpl_vars['results']->value['total_amount'];?>
" readonly/>
                    </strong>
                </div>
            </td>
        </tr>
    <?php }?>
    </tbody>
</table>


<?php echo '<script'; ?>
>
    function stateChange() {
        setTimeout(function () {
            $('body .money_tui').each(function () {
                var a = $(this).val();
                a = a.toString().replace(/^-,/, '-');
                $(this).val(a);
            });
        }, 1000);
    }
    stateChange();
<?php echo '</script'; ?>
><?php }
}
