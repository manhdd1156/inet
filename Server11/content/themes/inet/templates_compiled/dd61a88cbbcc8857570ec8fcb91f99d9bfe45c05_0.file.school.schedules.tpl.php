<?php
/* Smarty version 3.1.31, created on 2021-03-31 10:16:16
  from "D:\workplace\Server11\content\themes\inet\templates\ci\school\school.schedules.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_6063e980040c37_31849564',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'dd61a88cbbcc8857570ec8fcb91f99d9bfe45c05' => 
    array (
      0 => 'D:\\workplace\\Server11\\content\\themes\\inet\\templates\\ci\\school\\school.schedules.tpl',
      1 => 1552404702,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:ci/school/ajax.school.schedule.preview.tpl' => 1,
  ),
),false)) {
function content_6063e980040c37_31849564 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <div class="pull-right flip">
            <a href="https://blog.inet.vn/huong-dan-tao-lich-hoc-tren-website/" target="_blank" class="btn btn-info  btn_guide">
                <i class="fa fa-info-circle"></i> <?php echo __("Guide");?>

            </a>
            <?php if (($_smarty_tpl->tpl_vars['sub_view']->value == '') && $_smarty_tpl->tpl_vars['canEdit']->value) {?>
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/schedules/add" class="btn btn-default">
                    <i class="fa fa-plus"></i> <?php echo __("Add New");?>

                </a>
            <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/schedules" class="btn btn-default">
                    <i class="fa fa-list"></i> <?php echo __("Lists");?>

                </a>
                
                    
                
            <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "import") {?>
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/schedules" class="btn btn-default">
                    <i class="fa fa-list"></i> <?php echo __("Lists");?>

                </a>
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/schedules/add" class="btn btn-default">
                    <i class="fa fa-plus"></i> <?php echo __("Add New");?>

                </a>
            <?php }?>
        </div>
        <i class="fas fa-calendar-alt fa-fw fa-lg pr10"></i>
        <?php echo __("Schedule");?>

        <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
            &rsaquo; <?php echo __("Schedule list");?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
            &rsaquo; <?php echo __('Add schedule');?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "edit") {?>
            &rsaquo; <?php echo __('Edit schedule');?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "copy") {?>
            &rsaquo; <?php echo __('Copy schedule');?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "detail") {?>
            &rsaquo; <?php echo __('Detail schedule');?>
 <?php echo $_smarty_tpl->tpl_vars['data']->value['schedule_name'];?>

        <?php }?>
    </div>
<?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
    <div class="panel-body with-table form-horizontal">
        <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_schedule.php">
            <input type="hidden" name="school_username" id="school_username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
            <input type="hidden" name="do" value="search"/>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left"><?php echo __("Scope");?>
</label>
                <div class="col-sm-9">
                    <div class="col-sm-4">
                        <select name="level" id="schedule_level" class="form-control" data-username = "<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-id = "<?php echo $_smarty_tpl->tpl_vars['school']->value['page_id'];?>
">
                            <option value="0"><?php echo __("Select scope");?>
</option>
                            <option value="<?php echo @constant('SCHOOL_LEVEL');?>
"><?php echo __("School");?>
</option>
                            <option value="<?php echo @constant('CLASS_LEVEL_LEVEL');?>
"><?php echo __("Class level");?>
</option>
                            <option value="<?php echo @constant('CLASS_LEVEL');?>
"><?php echo __("Class");?>
</option>
                        </select>
                    </div>
                    <div class="col-sm-4 x-hidden" name="schedule_class_level">
                        <select name="class_level_id" id="class_level_id" class="form-control">
                            <option value="0"><?php echo __("Select class level");?>
</option>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['class_levels']->value, 'class_level');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class_level']->value) {
?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['class_level']->value['class_level_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['class_level']->value['class_level_name'];?>
</option>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </select>
                    </div>
                    <div class="col-sm-4 x-hidden" name="schedule_class">
                        <select name="class_id" id="class_id" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" class="form-control">
                            <option value="0"><?php echo __("Select class");?>
</option>
                            <?php $_smarty_tpl->_assignInScope('class_level', -1);
?>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['classes']->value, 'class');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class']->value) {
?>
                                <?php if (($_smarty_tpl->tpl_vars['class_level']->value != $_smarty_tpl->tpl_vars['class']->value['class_level_id'])) {?>
                                    <option value="" disabled style="color: blue">-----<?php echo $_smarty_tpl->tpl_vars['class']->value['class_level_name'];?>
-----</option>
                                <?php }?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['class']->value['group_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['class']->value['group_title'];?>
</option>
                                <?php $_smarty_tpl->_assignInScope('class_level', $_smarty_tpl->tpl_vars['class']->value['class_level_id']);
?>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </select>
                    </div>
                    <div class="col-sm-3">
                        <a class="btn btn-default js_school_schedule_search" data-username = "<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-handle = "search"><?php echo __("Search");?>
</a>
                    </div>
                </div>
            </div>

        </form>
        <div class="table-responsive" name = "schedule_all">
            <table class="table table-striped table-bordered table-hover js_dataTable">
                <thead>
                <tr><th colspan="7"><?php echo __("Schedule list");?>
&nbsp;(<span class="count_schedule"><?php echo count($_smarty_tpl->tpl_vars['rows']->value);?>
</span>)</th></tr>
                <tr>
                    <th>
                        <?php echo __("#");?>

                    </th>
                    <th>
                        <?php echo __("Schedule name");?>

                    </th>
                    <th>
                        <?php echo __("Begin");?>

                    </th>
                    <th>
                        <?php echo __("Scope");?>

                    </th>
                    <th>
                        <?php echo __("Use category");?>

                    </th>
                    <th>
                        <?php echo __("Study saturday");?>

                    </th>
                    <th>
                        <?php echo __("Actions");?>

                    </th>
                </tr>
                </thead>
                <tbody>
                <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['rows']->value, 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
                    <tr>
                        <td align="center" class="align-middle">
                            <?php echo $_smarty_tpl->tpl_vars['idx']->value;?>

                        </td>
                        <td class="align-middle">
                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/schedules/detail/<?php echo $_smarty_tpl->tpl_vars['row']->value['schedule_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['row']->value['schedule_name'];?>
</a>
                        </td>
                        <td class="align-middle" align="center">
                            <?php echo $_smarty_tpl->tpl_vars['row']->value['begin'];?>

                        </td>
                        <td class="align-middle" align="center">
                            <?php if ($_smarty_tpl->tpl_vars['row']->value['applied_for'] == @constant('SCHOOL_LEVEL')) {?>
                                <?php echo __("School");?>

                            <?php } elseif ($_smarty_tpl->tpl_vars['row']->value['applied_for'] == @constant('CLASS_LEVEL_LEVEL')) {?>
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['class_levels']->value, 'cl');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['cl']->value) {
?>
                                    <?php if ($_smarty_tpl->tpl_vars['cl']->value['class_level_id'] == $_smarty_tpl->tpl_vars['row']->value['class_level_id']) {
echo $_smarty_tpl->tpl_vars['cl']->value['class_level_name'];
}?>
                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                            <?php } elseif ($_smarty_tpl->tpl_vars['row']->value['applied_for'] == @constant('CLASS_LEVEL')) {?>
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['classes']->value, 'value');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['value']->value) {
?>
                                    <?php if ($_smarty_tpl->tpl_vars['value']->value['group_id'] == $_smarty_tpl->tpl_vars['row']->value['class_id']) {
echo $_smarty_tpl->tpl_vars['value']->value['group_title'];
}?>
                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                            <?php }?>
                        </td>
                        <td class="align-middle" align="center">
                            <?php if ($_smarty_tpl->tpl_vars['row']->value['is_category']) {?>
                                <?php echo __('Yes');?>

                            <?php } else { ?>
                                <?php echo __('No');?>

                            <?php }?>
                        </td>
                        <td class="align-middle" align="center">
                            <?php if ($_smarty_tpl->tpl_vars['row']->value['is_saturday']) {?>
                                <?php echo __('Yes');?>

                            <?php } else { ?>
                                <?php echo __('No');?>

                            <?php }?>
                        </td>
                        <td class="align-middle">
                            <?php if ($_smarty_tpl->tpl_vars['canEdit']->value) {?>
                                <div>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/schedules/copy/<?php echo $_smarty_tpl->tpl_vars['row']->value['schedule_id'];?>
" class="btn btn-xs btn-default"><?php echo __("Copy");?>
</a>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/schedules/edit/<?php echo $_smarty_tpl->tpl_vars['row']->value['schedule_id'];?>
" class="btn btn-xs btn-default"><?php echo __("Edit");?>
</a>
                                    <button class="btn btn-xs btn-danger js_school-schedule-delete" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['schedule_id'];?>
" data-handle = "delete_schedule"><?php echo __("Delete");?>
</button>
                                    <?php if (!$_smarty_tpl->tpl_vars['row']->value['is_notified']) {?>
                                            <div class = "form-label"><button class="btn btn-xs btn-default js_school-schedule-notify" data-handle="notify" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['schedule_id'];?>
"><?php echo __("Notify");?>
</button></div>
                                    <?php }?>
                                </div>
                            <?php }?>
                            <button class="btn btn-xs btn-success js_school-schedule-export" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['schedule_id'];?>
" data-handle = "export"><?php echo __("Export to Excel");?>
</button>
                        </td>
                    </tr>
                    <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>


                <?php if (count($_smarty_tpl->tpl_vars['rows']->value) == 0) {?>
                    <tr class="odd">
                        <td valign="top" align="center" colspan="7" class="dataTables_empty">
                            <?php echo __("No data available in table");?>

                        </td>
                    </tr>
                <?php }?>

                </tbody>
            </table>
        </div>
        <div id="cate_list"></div>
    </div>
<?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "detail") {?>
    <div class="panel-body with-table">
        <table class = "table table-bordered">
            <tbody>
                <tr>
                    <td class = "col-sm-3 text-right"><strong><?php echo __('Schedule name');?>
</strong></td>
                    <td>
                        <?php echo $_smarty_tpl->tpl_vars['data']->value['schedule_name'];?>

                    </td>
                </tr>
                <tr>
                    <td class = "col-sm-3 text-right"><strong><?php echo __('Scope');?>
</strong></td>
                    <td>
                        <?php if (isset($_smarty_tpl->tpl_vars['class']->value) && $_smarty_tpl->tpl_vars['data']->value['applied_for'] == 3) {?> <?php echo $_smarty_tpl->tpl_vars['class']->value['group_title'];?>

                        <?php } elseif (isset($_smarty_tpl->tpl_vars['class_level']->value) && $_smarty_tpl->tpl_vars['data']->value['applied_for'] == 2) {
echo $_smarty_tpl->tpl_vars['class_level']->value['class_level_name'];?>

                        <?php } elseif ($_smarty_tpl->tpl_vars['data']->value['applied_for'] == 1) {?> <?php echo __('School');?>

                        <?php }?>
                    </td>
                </tr>
                <tr>
                    <td class = "col-sm-3 text-right"><strong><?php echo __('Begin');?>
</strong></td>
                    <td>
                        <?php echo $_smarty_tpl->tpl_vars['data']->value['begin'];?>

                    </td>
                </tr>
                
                    
                    
                        
                            
                        
                            
                        
                    
                
                
                    
                    
                        
                            
                        
                            
                        
                    
                
                <?php if ($_smarty_tpl->tpl_vars['data']->value['description'] != '') {?>
                    <tr>
                        <td class = "col-sm-3 text-right"><strong><?php echo __('Description');?>
</strong></td>
                        <td>
                            <?php echo nl2br($_smarty_tpl->tpl_vars['data']->value['description']);?>

                        </td>
                    </tr>
                <?php }?>
            </tbody>
        </table>
        <div class = "table-responsive">
            <table class = "table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th><?php echo __('#');?>
</th>
                        <th><?php echo __('Time');?>
 </th>
                        <th <?php if (!$_smarty_tpl->tpl_vars['data']->value['is_category']) {?>class="hidden"<?php }?>><?php echo __('Activity');?>
</th>
                        <th><?php echo __('Monday');?>
 </th>
                        <th><?php echo __('Tuesday');?>
</th>
                        <th> <?php echo __('Wednesday');?>
</th>
                        <th><?php echo __('Thursday');?>
</th>
                        <th><?php echo __('Friday');?>
 </th>
                        <th <?php if (!$_smarty_tpl->tpl_vars['data']->value['is_saturday']) {?>class="hidden"<?php }?>><?php echo __('Saturday');?>
</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['data']->value['details'], 'row', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['row']->value) {
?>
                        <?php $_smarty_tpl->_assignInScope('array', array_values($_smarty_tpl->tpl_vars['row']->value));
?>
                        <?php $_smarty_tpl->_assignInScope('temp', array());
?>
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['array']->value, 'value', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['value']->value) {
?>
                            <?php if ($_smarty_tpl->tpl_vars['data']->value['is_saturday']) {?>
                                <?php if (($_smarty_tpl->tpl_vars['k']->value >= 4) && ($_smarty_tpl->tpl_vars['k']->value < (count($_smarty_tpl->tpl_vars['array']->value)))) {?>
                                    <?php $_tmp_array = isset($_smarty_tpl->tpl_vars['temp']) ? $_smarty_tpl->tpl_vars['temp']->value : array();
if (!is_array($_tmp_array) || $_tmp_array instanceof ArrayAccess) {
settype($_tmp_array, 'array');
}
$_tmp_array[] = $_smarty_tpl->tpl_vars['value']->value;
$_smarty_tpl->_assignInScope('temp', $_tmp_array);
?>
                                <?php }?>
                            <?php } else { ?>
                                <?php if (($_smarty_tpl->tpl_vars['k']->value >= 4) && ($_smarty_tpl->tpl_vars['k']->value < (count($_smarty_tpl->tpl_vars['array']->value)-1))) {?>
                                    <?php $_tmp_array = isset($_smarty_tpl->tpl_vars['temp']) ? $_smarty_tpl->tpl_vars['temp']->value : array();
if (!is_array($_tmp_array) || $_tmp_array instanceof ArrayAccess) {
settype($_tmp_array, 'array');
}
$_tmp_array[] = $_smarty_tpl->tpl_vars['value']->value;
$_smarty_tpl->_assignInScope('temp', $_tmp_array);
?>
                                <?php }?>
                            <?php }?>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        <tr>
                            <td style="vertical-align: middle" align="center">
                                <strong><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</strong>
                            </td>
                            <td style="vertical-align: middle" align="center">
                                <strong><?php echo $_smarty_tpl->tpl_vars['row']->value['subject_time'];?>
</strong>
                            </td>
                            <td <?php if (!$_smarty_tpl->tpl_vars['data']->value['is_category']) {?>class="hidden"<?php }?> align="center" style = "vertical-align: middle;">
                                <strong><?php echo $_smarty_tpl->tpl_vars['row']->value['subject_name'];?>
</strong>
                            </td>
                            <?php $_smarty_tpl->_assignInScope('col', 1);
?>
                            <?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable(null, $_smarty_tpl->isRenderingCache);
$_smarty_tpl->tpl_vars['i']->value = 0;
if ($_smarty_tpl->tpl_vars['i']->value < count($_smarty_tpl->tpl_vars['temp']->value)) {
for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value < count($_smarty_tpl->tpl_vars['temp']->value); $_smarty_tpl->tpl_vars['i']->value++) {
?>
                                <?php if ($_smarty_tpl->tpl_vars['temp']->value[$_smarty_tpl->tpl_vars['i']->value] === $_smarty_tpl->tpl_vars['temp']->value[($_smarty_tpl->tpl_vars['i']->value+1)]) {?>
                                    <?php $_smarty_tpl->_assignInScope('col', $_smarty_tpl->tpl_vars['col']->value+1);
?>
                                <?php } else { ?>
                                    <td colspan = "<?php echo $_smarty_tpl->tpl_vars['col']->value;?>
" align="center">
                                        <?php echo nl2br($_smarty_tpl->tpl_vars['temp']->value[$_smarty_tpl->tpl_vars['i']->value]);?>

                                    </td>
                                    <?php $_smarty_tpl->_assignInScope('col', 1);
?>
                                <?php }?>
                            <?php }
}
?>

                        </tr>
                        <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                </tbody>
            </table>
        </div>
        <div class="form-group pl5">
            <a class="btn btn-default" href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/schedules"><?php echo __("Lists");?>
</a>
            <?php if ($_smarty_tpl->tpl_vars['canEdit']->value) {?>
                <a class="btn btn-default" href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/schedules/add"><?php echo __("Add New");?>
</a>
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/schedules/copy/<?php echo $_smarty_tpl->tpl_vars['data']->value['schedule_id'];?>
" class="btn btn-default"><?php echo __("Copy");?>
</a>
                <?php if (!$_smarty_tpl->tpl_vars['data']->value['is_notified']) {?>
                    <button class="btn btn-default js_school-schedule-notify" data-handle="notify" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['data']->value['schedule_id'];?>
"><?php echo __("Notify");?>
</button>
                <?php }?>
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/schedules/edit/<?php echo $_smarty_tpl->tpl_vars['data']->value['schedule_id'];?>
" class="btn btn-default"><?php echo __("Edit");?>
</a>
                <button class="btn btn-danger js_school-schedule-delete" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['data']->value['schedule_id'];?>
" data-handle = "delete_schedule"><?php echo __("Delete");?>
</button>
            <?php }?>
        </div>
    </div>
<?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
    <div class="panel-body">
        <div id="open_dialog_schedule" class="x-hidden" title="<?php echo mb_strtoupper(__("Schedule preview"), 'UTF-8');?>
">
            <?php $_smarty_tpl->_subTemplateRender("file:ci/school/ajax.school.schedule.preview.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

        </div>
        <form class="form-horizontal" action="#" enctype="multipart/form-data" method="post" id="import_schedule_excel_form">
            <input type="hidden" name="school_username" id="school_username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
            <input type="hidden" name="do" value="add" id="do"/>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left"><?php echo __("Schedule name");?>
 (*)</label>
                <div class="col-sm-9">
                    <input class="form-control" name="schedule_name" required autofocus maxlength="100">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left"><?php echo __("Scope");?>
 (*)</label>
                <div class="col-sm-3">
                    <select name="level" id="schedule_level" class="form-control" data-username = "<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-id = "<?php echo $_smarty_tpl->tpl_vars['school']->value['page_id'];?>
">
                        <option value="<?php echo @constant('SCHOOL_LEVEL');?>
"><?php echo __("School");?>
</option>
                        <option value="<?php echo @constant('CLASS_LEVEL_LEVEL');?>
"><?php echo __("Class level");?>
</option>
                        <option value="<?php echo @constant('CLASS_LEVEL');?>
"><?php echo __("Class");?>
</option>
                    </select>
                </div>
                <div class="col-sm-3 x-hidden" name="schedule_class_level">
                    <select name="class_level_id" id="class_level_id" class="form-control">
                        <option value="0"><?php echo __("Select class level");?>
</option>
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['class_levels']->value, 'class_level');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class_level']->value) {
?>
                            <option value="<?php echo $_smarty_tpl->tpl_vars['class_level']->value['class_level_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['class_level']->value['class_level_name'];?>
</option>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                    </select>
                </div>
                <div class="col-sm-3 x-hidden" name="schedule_class">
                    <select name="class_id" id="class_id" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" class="form-control">
                        <option value="0"><?php echo __("Select class");?>
</option>
                        <?php $_smarty_tpl->_assignInScope('class_level', -1);
?>
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['classes']->value, 'class');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class']->value) {
?>
                            <?php if (($_smarty_tpl->tpl_vars['class_level']->value != $_smarty_tpl->tpl_vars['class']->value['class_level_id'])) {?>
                                <option value="" disabled style="color: blue">-----<?php echo $_smarty_tpl->tpl_vars['class']->value['class_level_name'];?>
-----</option>
                            <?php }?>
                            <option value="<?php echo $_smarty_tpl->tpl_vars['class']->value['group_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['class']->value['group_title'];?>
</option>
                            <?php $_smarty_tpl->_assignInScope('class_level', $_smarty_tpl->tpl_vars['class']->value['class_level_id']);
?>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left"><?php echo __("Begin");?>
 (*)</label>
                <div class='col-sm-3'>
                    <div class='input-group date' id='schedule_beginpicker_new'>
                        <input type='text' name="begin" class="form-control" placeholder="<?php echo __("Begin");?>
 (*)" value="<?php echo $_smarty_tpl->tpl_vars['monday']->value;?>
" required/>
                        <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left"></label>
                <div class="col-sm-9">
                    <strong> <?php echo __('Note: start date must be monday');?>
 </strong>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left"><?php echo __("Description");?>
</label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="description" rows="3"></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left"><?php echo __("Have active column");?>
?</label>
                <div class="col-sm-1">
                    <div class="onoffswitch">
                        <input type="checkbox" name="use_cate" class="onoffswitch-checkbox" id="use_category" checked>
                        <label class="onoffswitch-label" for="use_category"></label>
                    </div>
                </div>
                <label class="col-sm-2 control-label text-left"><?php echo __("Study saturday");?>
?</label>
                <div class="col-sm-2">
                    <div class="onoffswitch">
                        <input type="checkbox" name="study_saturday" class="onoffswitch-checkbox" id="study_saturday" checked>
                        <label class="onoffswitch-label" for="study_saturday"></label>
                    </div>
                </div>
                <label class="col-sm-2 control-label text-left"><?php echo __("Notify immediately");?>
?</label>
                <div class="col-sm-2">
                    <div class="onoffswitch">
                        <input type="checkbox" name="notify_immediately" class="onoffswitch-checkbox" id="notify_immediately">
                        <label class="onoffswitch-label" for="notify_immediately"></label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left"><?php echo __("Import from Excel file");?>
?</label>
                <div class="col-sm-9">
                    <div class="onoffswitch">
                        <input type="checkbox" name="import_excel" class="onoffswitch-checkbox" id="import_excel">
                        <label class="onoffswitch-label" for="import_excel"></label>
                    </div>
                </div>
            </div>
            <div class="table-responsive" id="cate_list">
                <table style="margin-bottom: 0px" class="table table-striped table-bordered table-hover" id = "myTableSchedule">
                    <thead>
                    <tr>
                        <th>
                            <?php echo __('#');?>

                        </th>
                        <th>
                            <?php echo __('Option');?>

                        </th>
                        <th>
                            <?php echo __('Time');?>

                        </th>
                        <th>
                            <?php echo __('Activity');?>

                        </th>
                        <th>
                            <?php echo __('Monday');?>

                        </th>
                        <th>
                            <?php echo __('Tuesday');?>

                        </th>
                        <th>
                            <?php echo __('Wednesday');?>

                        </th>
                        <th>
                            <?php echo __('Thursday');?>

                        </th>
                        <th>
                            <?php echo __('Friday');?>

                        </th>
                        <th>
                            <?php echo __('Saturday');?>

                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td align="center" class = "col_no">1</td>
                        <td align="center">
                            <a class = "btn btn-default js_cate-delete_null"> <?php echo __("Delete");?>
 </a>
                        </td>
                        <td>
                            <input type = "text" name = "start[]" placeholder="12:00" style = "padding-left: 10px; height: 35px; min-width: 200px;">
                        </td>
                        <td>
                            <input type="text" name = "category[]" placeholder="Tập thể dục" style = "padding-left: 10px; height: 35px; min-width: 200px;">
                        </td>
                        <td>
                            <textarea class = "note" type="text" name = "subject_detail_mon[]"> </textarea>
                        </td>
                        <td>
                            <textarea class = "note" type="text" name = "subject_detail_tue[]"> </textarea>
                        </td>
                        <td>
                            <textarea class = "note" type="text" name = "subject_detail_wed[]"> </textarea>
                        </td>
                        <td>
                            <textarea class = "note" type="text" name = "subject_detail_thu[]"> </textarea>
                        </td>
                        <td>
                            <textarea class = "note" type="text" name = "subject_detail_fri[]"> </textarea>
                        </td>
                        <td>
                            <textarea class = "note" type="text" name = "subject_detail_sat[]"> </textarea>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <div class = "form-group">
                    <div class = "col-sm-12">
                        <a class="btn btn-default js_cate-add-schedule">+</a>
                    </div>
                </div>
            </div>
            <div class="hidden" id="select_excel">
                <div class="form-group color_red">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-9">
                        <strong>- <?php echo __("You must use the Excel file format specified by the system, download form at");?>
 <a target="_blank" href="https://drive.google.com/file/d/1XtB3hnhu453FSsg8hkSldDZzvopKiCG0"><?php echo __("HERE");?>
</a>.</strong>
                        <br/>
                        - <?php echo __("You must");?>
 <strong><?php echo __("configure");?>
</strong> <?php echo __("the schedule before SAVING");?>

                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Select Excel file");?>
</label>
                    <div class="col-sm-6">
                        <input type="file" name="file" id="file"/>
                    </div>
                </div>
            </div>
            <div class="form-group mt10">
                <div class="col-sm-9 col-sm-offset-3">
                    <a href="#" id="preview_normal" class="btn btn-default text-left js_school-schedule-preview"><?php echo __("Preview");?>
</a>
                    <button type="submit" id="preview_excel" class="btn btn-default text-left js_school-schedule-preview-excel x-hidden"><?php echo __("Preview");?>
</button>
                    <button type="submit" id="submit" class="btn btn-primary padrl30 text-left schedule_submit"><?php echo __("Save");?>
</button>
                </div>
            </div>
            <br/>
            <div class="table-responsive" id="result_info" name="result_info"></div>
            <!-- success -->
            <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
            <!-- success -->
            <!-- error -->
            <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
            <!-- error -->

        </form>
    </div>

<?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "edit") {?>
    <div class="panel-body">
        <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_schedule.php">
            <input type="hidden" name="school_username" id="school_username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
            <input type="hidden" name="do" value="edit"/>
            <input type="hidden" name="schedule_id" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['schedule_id'];?>
"/>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left"><?php echo __("Schedule name");?>
 (*)</label>
                <div class="col-sm-9">
                    <input class="form-control" name="schedule_name" required autofocus maxlength="100" value = "<?php echo $_smarty_tpl->tpl_vars['data']->value['schedule_name'];?>
">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left"><?php echo __("Scope");?>
 (*)</label>
                <div class="col-sm-3">
                    <select name="level" id="cate_level" class="form-control">
                        <option value="<?php echo @constant('SCHOOL_LEVEL');?>
" <?php if ($_smarty_tpl->tpl_vars['data']->value['applied_for'] == @constant('SCHOOL_LEVEL')) {?> selected <?php }?>><?php echo __("School");?>
</option>
                        <option value="<?php echo @constant('CLASS_LEVEL_LEVEL');?>
" <?php if ($_smarty_tpl->tpl_vars['data']->value['applied_for'] == @constant('CLASS_LEVEL_LEVEL')) {?> selected <?php }?>><?php echo __("Class level");?>
</option>
                        <option value="<?php echo @constant('CLASS_LEVEL');?>
" <?php if ($_smarty_tpl->tpl_vars['data']->value['applied_for'] == @constant('CLASS_LEVEL')) {?> selected <?php }?>><?php echo __("Class");?>
</option>
                    </select>
                </div>
                <div class="col-sm-3 <?php if ($_smarty_tpl->tpl_vars['data']->value['applied_for'] != @constant('CLASS_LEVEL_LEVEL')) {?>x-hidden<?php }?>" name="cate_class_level">
                    <select name="class_level_id" id="class_level_id" class="form-control">
                        <option value="0"> <?php echo __('Select class level');?>
</option>
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['class_levels']->value, 'class_level');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class_level']->value) {
?>
                            <option value="<?php echo $_smarty_tpl->tpl_vars['class_level']->value['class_level_id'];?>
" <?php if ($_smarty_tpl->tpl_vars['data']->value['class_level_id'] == $_smarty_tpl->tpl_vars['class_level']->value['class_level_id']) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['class_level']->value['class_level_name'];?>
</option>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                    </select>
                </div>
                <div class="col-sm-3 <?php if ($_smarty_tpl->tpl_vars['data']->value['applied_for'] != (@constant('CLASS_LEVEL'))) {?>x-hidden<?php }?>" name="cate_class">
                    <select name="class_id" id="class_id" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" class="form-control">
                        <option value="0"> <?php echo __('Select class');?>
 </option>
                        <?php $_smarty_tpl->_assignInScope('class_level', -1);
?>
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['classes']->value, 'class');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class']->value) {
?>
                            <?php if (($_smarty_tpl->tpl_vars['class_level']->value != $_smarty_tpl->tpl_vars['class']->value['class_level_id'])) {?>
                                <option value="" disabled style="color: blue">-----<?php echo $_smarty_tpl->tpl_vars['class']->value['class_level_name'];?>
-----</option>
                            <?php }?>
                            <option value="<?php echo $_smarty_tpl->tpl_vars['class']->value['group_id'];?>
" <?php if ($_smarty_tpl->tpl_vars['data']->value['class_id'] == $_smarty_tpl->tpl_vars['class']->value['group_id']) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['class']->value['group_title'];?>
</option>
                            <?php $_smarty_tpl->_assignInScope('class_level', $_smarty_tpl->tpl_vars['class']->value['class_level_id']);
?>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left"><?php echo __("Begin");?>
 (*)</label>
                <div class='col-sm-3'>
                    <div class='input-group date' id='schedule_beginpicker_new'>
                        <input type='text' name="begin" class="form-control" placeholder="<?php echo __("Begin");?>
 (*)" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['begin'];?>
" required/>
                        <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left"></label>
                <div class="col-sm-9">
                    <strong> <?php echo __('Note: start date must be monday');?>
 </strong>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left"><?php echo __("Description");?>
</label>
                <div class="col-sm-9">
                    <textarea class="form-control js_autosize" name="description" rows="3"><?php echo $_smarty_tpl->tpl_vars['data']->value['description'];?>
</textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left"><?php echo __("Have active column");?>
?</label>
                <div class="col-sm-1">
                    <div class="onoffswitch">
                        <input type="checkbox" name="use_category" class="onoffswitch-checkbox" id="use_category" <?php if ($_smarty_tpl->tpl_vars['data']->value['is_category']) {?>checked<?php }?>>
                        <label class="onoffswitch-label" for="use_category"></label>
                    </div>
                </div>
                <label class="col-sm-2 control-label text-left"><?php echo __("Study saturday");?>
?</label>
                <div class="col-sm-1">
                    <div class="onoffswitch">
                        <input type="checkbox" name="study_saturday" class="onoffswitch-checkbox" id="study_saturday" <?php if ($_smarty_tpl->tpl_vars['data']->value['is_saturday']) {?>checked<?php }?>>
                        <label class="onoffswitch-label" for="study_saturday"></label>
                    </div>
                </div>
                <label class="col-sm-3 control-label text-left"><?php echo __("Notify immediately");?>
?</label>
                <div class="col-sm-1">
                    <div class="onoffswitch">
                        <input type="checkbox" name="notify_immediately" class="onoffswitch-checkbox" id="notify_immediately" <?php if ($_smarty_tpl->tpl_vars['data']->value['is_notified']) {?>checked<?php }?>>
                        <label class="onoffswitch-label" for="notify_immediately"></label>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover" id = "myTableSchedule">
                    <thead>
                    <tr>
                        <th>
                            <?php echo __('#');?>

                        </th>
                        <th>
                            <?php echo __('Option');?>

                        </th>
                        <th>
                            <?php echo __('Time');?>

                        </th>
                        <th>
                            <?php echo __('Activity');?>

                        </th>
                        <th>
                            <?php echo __('Monday');?>

                        </th>
                        <th>
                            <?php echo __('Tuesday');?>

                        </th>
                        <th>
                            <?php echo __('Wednesday');?>

                        </th>
                        <th>
                            <?php echo __('Thursday');?>

                        </th>
                        <th>
                            <?php echo __('Friday');?>

                        </th>
                        <th>
                            <?php echo __('Saturday');?>

                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['data']->value['details'], 'detail');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['detail']->value) {
?>
                        <tr>
                            <td align="center" class = "col_no">
                                <?php echo $_smarty_tpl->tpl_vars['idx']->value;?>

                                <input type="hidden" name="schedule_detail_id[]" value="<?php echo $_smarty_tpl->tpl_vars['detail']->value['schedule_detail_id'];?>
"/>
                            </td>
                            <td align="center">
                                <a class = "btn btn-default js_cate-delete_null"> <?php echo __("Delete");?>
 </a>
                            </td>
                            <td>
                                <input type="text" name="start[]" value="<?php echo $_smarty_tpl->tpl_vars['detail']->value['subject_time'];?>
" class="form-control" style = "min-width: 200px;"/>
                            </td>
                            <td>
                                <input type = "text" name="category[]" value="<?php echo $_smarty_tpl->tpl_vars['detail']->value['subject_name'];?>
" class = "form-control" style = "min-width: 200px;"/>
                            </td>
                            <td>
                                <textarea type = "text" class = "text-left form-control note" name = "subject_detail_mon[]"><?php echo $_smarty_tpl->tpl_vars['detail']->value['monday'];?>
</textarea>
                            </td>
                            <td>
                                <textarea type = "text" class = "text-left form-control note" name = "subject_detail_tue[]"><?php echo $_smarty_tpl->tpl_vars['detail']->value['tuesday'];?>
</textarea>
                            </td>
                            <td>
                                <textarea type = "text" class = "text-left form-control note" name = "subject_detail_wed[]"><?php echo $_smarty_tpl->tpl_vars['detail']->value['wednesday'];?>
</textarea>
                            </td>
                            <td>
                                <textarea type = "text" class = "text-left form-control note" name = "subject_detail_thu[]"><?php echo $_smarty_tpl->tpl_vars['detail']->value['thursday'];?>
</textarea>
                            </td>
                            <td>
                                <textarea type = "text" class = "text-left form-control note" name = "subject_detail_fri[]"><?php echo $_smarty_tpl->tpl_vars['detail']->value['friday'];?>
</textarea>
                            </td>
                            <td>
                                <textarea type = "text" class = "text-left form-control note" name = "subject_detail_sat[]"><?php echo $_smarty_tpl->tpl_vars['detail']->value['saturday'];?>
</textarea>
                            </td>
                        </tr>
                        <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                    </tbody>
                </table>
                <div class = "form-group">
                    <div class = "col-sm-12">
                        <a class="btn btn-default js_cate-add-schedule">+</a>
                    </div>
                </div>
            </div>
            <div class = "form-group">
                <div class="col-sm-9 mt20">
                    
                    <button type="submit" class="btn btn-primary padrl30"><?php echo __("Save");?>
</button>
                </div>
            </div>
            <!-- success -->
            <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
            <!-- success -->
            <!-- error -->
            <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
            <!-- error -->
        </form>
    </div>
<?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "import") {?>
    <div class="panel-body">
        <form class="form-horizontal" action="#" enctype="multipart/form-data" method="post" id="import_schedule_excel_form">
            <input type="hidden" name="school_username" id="school_username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
            <input type="hidden" name="do" value="import"/>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left"><?php echo __("Schedule name");?>
 (*)</label>
                <div class="col-sm-9">
                    <input class="form-control" name="schedule_name" required autofocus maxlength="100">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left"><?php echo __("Scope");?>
 (*)</label>
                <div class="col-sm-3">
                    <select name="level" id="schedule_level" class="form-control" data-username = "<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-id = "<?php echo $_smarty_tpl->tpl_vars['school']->value['page_id'];?>
">
                        <option value="<?php echo @constant('SCHOOL_LEVEL');?>
"><?php echo __("School");?>
</option>
                        <option value="<?php echo @constant('CLASS_LEVEL_LEVEL');?>
"><?php echo __("Class level");?>
</option>
                        <option value="<?php echo @constant('CLASS_LEVEL');?>
"><?php echo __("Class");?>
</option>
                    </select>
                </div>
                <div class="col-sm-3 x-hidden" name="schedule_class_level">
                    <select name="class_level_id" id="class_level_id" class="form-control">
                        <option value="0"><?php echo __("Select class level");?>
</option>
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['class_levels']->value, 'class_level');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class_level']->value) {
?>
                            <option value="<?php echo $_smarty_tpl->tpl_vars['class_level']->value['class_level_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['class_level']->value['class_level_name'];?>
</option>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                    </select>
                </div>
                <div class="col-sm-3 x-hidden" name="schedule_class">
                    <select name="class_id" id="class_id" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" class="form-control">
                        <option value="0"><?php echo __("Select class");?>
</option>
                        <?php $_smarty_tpl->_assignInScope('class_level', -1);
?>
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['classes']->value, 'class');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class']->value) {
?>
                            <?php if (($_smarty_tpl->tpl_vars['class_level']->value != $_smarty_tpl->tpl_vars['class']->value['class_level_id'])) {?>
                                <option value="" disabled style="color: blue">-----<?php echo $_smarty_tpl->tpl_vars['class']->value['class_level_name'];?>
-----</option>
                            <?php }?>
                            <option value="<?php echo $_smarty_tpl->tpl_vars['class']->value['group_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['class']->value['group_title'];?>
</option>
                            <?php $_smarty_tpl->_assignInScope('class_level', $_smarty_tpl->tpl_vars['class']->value['class_level_id']);
?>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left"><?php echo __("Begin");?>
 (*)</label>
                <div class='col-sm-3'>
                    <div class='input-group date' id='schedule_beginpicker_new'>
                        <input type='text' name="begin" class="form-control" placeholder="<?php echo __("Begin");?>
 (*)" required/>
                        <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left"></label>
                <div class="col-sm-9">
                    <strong> <?php echo __('Note: start date must be monday');?>
 </strong>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left"><?php echo __("Use category");?>
?</label>
                <div class="col-sm-9">
                    <div class="onoffswitch">
                        <input type="checkbox" name="use_category" class="onoffswitch-checkbox" id="use_category" checked>
                        <label class="onoffswitch-label" for="use_category"></label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left"><?php echo __("Study saturday");?>
?</label>
                <div class="col-sm-9">
                    <div class="onoffswitch">
                        <input type="checkbox" name="study_saturday" class="onoffswitch-checkbox" id="study_saturday" checked>
                        <label class="onoffswitch-label" for="study_saturday"></label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left"><?php echo __("Notify immediately");?>
?</label>
                <div class="col-sm-9">
                    <div class="onoffswitch">
                        <input type="checkbox" name="notify_immediately" class="onoffswitch-checkbox" id="notify_immediately" checked>
                        <label class="onoffswitch-label" for="notify_immediately"></label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left"><?php echo __("Description");?>
</label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="description" rows="6"></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left"><?php echo __("Select Excel file");?>
</label>
                <div class="col-sm-6">
                    <input type="file" name="file" id="file" <?php if (count($_smarty_tpl->tpl_vars['classes']->value) <= 0) {?>disabled<?php }?>/>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-9 col-sm-offset-3">
                    <button type="submit" id="submit_id" class="btn btn-primary padrl30" <?php if (count($_smarty_tpl->tpl_vars['classes']->value) <= 0) {?>disabled<?php }?>><?php echo __("Save");?>
</button>
                </div>
            </div>
            <br/>
            <div class="table-responsive" id="result_info" name="result_info"></div>
            <!-- success -->
            <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
            <!-- success -->

            <!-- error -->
            <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
            <!-- error -->
        </form>
    </div>
<?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "copy") {?>
    <div class="panel-body">
        <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_schedule.php">
            <input type="hidden" name="school_username" id="school_username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
            <input type="hidden" name="do" value="copy"/>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left"><?php echo __("Schedule name");?>
 (*)</label>
                <div class="col-sm-9">
                    <input class="form-control" name="schedule_name" required autofocus maxlength="100" value = "<?php echo $_smarty_tpl->tpl_vars['data']->value['schedule_name'];?>
">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left"><?php echo __("Scope");?>
 (*)</label>
                <div class="col-sm-3">
                    <select name="level" id="cate_level" class="form-control">
                        <option value="<?php echo @constant('SCHOOL_LEVEL');?>
" <?php if ($_smarty_tpl->tpl_vars['data']->value['applied_for'] == @constant('SCHOOL_LEVEL')) {?> selected <?php }?>><?php echo __("School");?>
</option>
                        <option value="<?php echo @constant('CLASS_LEVEL_LEVEL');?>
" <?php if ($_smarty_tpl->tpl_vars['data']->value['applied_for'] == @constant('CLASS_LEVEL_LEVEL')) {?> selected <?php }?>><?php echo __("Class level");?>
</option>
                        <option value="<?php echo @constant('CLASS_LEVEL');?>
" <?php if ($_smarty_tpl->tpl_vars['data']->value['applied_for'] == @constant('CLASS_LEVEL')) {?> selected <?php }?>><?php echo __("Class");?>
</option>
                    </select>
                </div>
                <div class="col-sm-3 <?php if ($_smarty_tpl->tpl_vars['data']->value['applied_for'] != @constant('CLASS_LEVEL_LEVEL')) {?>x-hidden<?php }?>" name="cate_class_level">
                    <select name="class_level_id" id="class_level_id" class="form-control">
                        <option value="0"> <?php echo __('Select class level');?>
</option>
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['class_levels']->value, 'class_level');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class_level']->value) {
?>
                            <option value="<?php echo $_smarty_tpl->tpl_vars['class_level']->value['class_level_id'];?>
" <?php if ($_smarty_tpl->tpl_vars['data']->value['class_level_id'] == $_smarty_tpl->tpl_vars['class_level']->value['class_level_id']) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['class_level']->value['class_level_name'];?>
</option>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                    </select>
                </div>
                <div class="col-sm-3 <?php if ($_smarty_tpl->tpl_vars['data']->value['applied_for'] != (@constant('CLASS_LEVEL'))) {?>x-hidden<?php }?>" name="cate_class">
                    <select name="class_id" id="class_id" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" class="form-control">
                        <option value="0"> <?php echo __('Select class');?>
 </option>
                        <?php $_smarty_tpl->_assignInScope('class_level', -1);
?>
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['classes']->value, 'class');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class']->value) {
?>
                            <?php if (($_smarty_tpl->tpl_vars['class_level']->value != $_smarty_tpl->tpl_vars['class']->value['class_level_id'])) {?>
                                <option value="" disabled style="color: blue">-----<?php echo $_smarty_tpl->tpl_vars['class']->value['class_level_name'];?>
-----</option>
                            <?php }?>
                            <option value="<?php echo $_smarty_tpl->tpl_vars['class']->value['group_id'];?>
" <?php if ($_smarty_tpl->tpl_vars['data']->value['class_id'] == $_smarty_tpl->tpl_vars['class']->value['group_id']) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['class']->value['group_title'];?>
</option>
                            <?php $_smarty_tpl->_assignInScope('class_level', $_smarty_tpl->tpl_vars['class']->value['class_level_id']);
?>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left"><?php echo __("Begin");?>
 (*)</label>
                <div class='col-sm-3'>
                    <div class='input-group date' id='schedule_beginpicker_new'>
                        <input type='text' name="begin" class="form-control" placeholder="<?php echo __("Begin");?>
 (*)" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['begin'];?>
" required/>
                        <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left"></label>
                <div class="col-sm-9">
                    <strong> <?php echo __('Note: start date must be monday');?>
 </strong>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left"><?php echo __("Use category");?>
?</label>
                <div class="col-sm-9">
                    <div class="onoffswitch">
                        <input type="checkbox" name="use_category" class="onoffswitch-checkbox" id="use_category" <?php if ($_smarty_tpl->tpl_vars['data']->value['is_category']) {?>checked<?php }?>>
                        <label class="onoffswitch-label" for="use_category"></label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left"><?php echo __("Study saturday");?>
?</label>
                <div class="col-sm-9">
                    <div class="onoffswitch">
                        <input type="checkbox" name="study_saturday" class="onoffswitch-checkbox" id="study_saturday" <?php if ($_smarty_tpl->tpl_vars['data']->value['is_saturday']) {?>checked<?php }?>>
                        <label class="onoffswitch-label" for="study_saturday"></label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left"><?php echo __("Notify immediately");?>
?</label>
                <div class="col-sm-9">
                    <div class="onoffswitch">
                        <input type="checkbox" name="notify_immediately" class="onoffswitch-checkbox" id="notify_immediately" checked>
                        <label class="onoffswitch-label" for="notify_immediately"></label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left"><?php echo __("Description");?>
 (*)</label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="description" rows="6"><?php echo $_smarty_tpl->tpl_vars['data']->value['description'];?>
</textarea>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover" id = "myTableSchedule">
                    <thead>
                    <tr>
                        <th>
                            <?php echo __('#');?>

                        </th>
                        <th>
                            <?php echo __('Option');?>

                        </th>
                        <th>
                            <?php echo __('Time');?>

                        </th>
                        <th>
                            <?php echo __('Activity');?>

                        </th>
                        <th>
                            <?php echo __('Monday');?>

                        </th>
                        <th>
                            <?php echo __('Tuesday');?>

                        </th>
                        <th>
                            <?php echo __('Wednesday');?>

                        </th>
                        <th>
                            <?php echo __('Thursday');?>

                        </th>
                        <th>
                            <?php echo __('Friday');?>

                        </th>
                        <th>
                            <?php echo __('Saturday');?>

                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['data']->value['details'], 'detail');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['detail']->value) {
?>
                        <tr>
                            <td align="center" class = "col_no">
                                <?php echo $_smarty_tpl->tpl_vars['detail']->value['index'];?>

                                <input type="hidden" name="schedule_detail_id[]" value="<?php echo $_smarty_tpl->tpl_vars['detail']->value['schedule_detail_id'];?>
"/>
                            </td>
                            <td align="center">
                                <a class = "btn btn-default js_cate-delete_null"> <?php echo __('Delete');?>
 </a>
                            </td>
                            <td>
                                <input type="text" name="start[]" class="form-control" value = "<?php echo $_smarty_tpl->tpl_vars['detail']->value['subject_time'];?>
" style = "min-width: 200px;">
                            </td>
                            <td>
                                <input type = "text" name="category[]" value="<?php echo $_smarty_tpl->tpl_vars['detail']->value['subject_name'];?>
" class = "form-control" style = "min-width: 200px;"/>
                            </td>
                            <td>
                                <textarea type = "text" class = "text-left form-control note" name = "subject_detail_mon[]"><?php echo $_smarty_tpl->tpl_vars['detail']->value['monday'];?>
</textarea>
                            </td>
                            <td>
                                <textarea type = "text" class = "text-left form-control note" name = "subject_detail_tue[]"><?php echo $_smarty_tpl->tpl_vars['detail']->value['tuesday'];?>
</textarea>
                            </td>
                            <td>
                                <textarea type = "text" class = "text-left form-control note" name = "subject_detail_wed[]"><?php echo $_smarty_tpl->tpl_vars['detail']->value['wednesday'];?>
</textarea>
                            </td>
                            <td>
                                <textarea type = "text" class = "text-left form-control note" name = "subject_detail_thu[]"><?php echo $_smarty_tpl->tpl_vars['detail']->value['thursday'];?>
</textarea>
                            </td>
                            <td>
                                <textarea type = "text" class = "text-left form-control note" name = "subject_detail_fri[]"><?php echo $_smarty_tpl->tpl_vars['detail']->value['friday'];?>
</textarea>
                            </td>
                            <td>
                                <textarea type = "text" class = "text-left form-control note" name = "subject_detail_sat[]"><?php echo $_smarty_tpl->tpl_vars['detail']->value['saturday'];?>
</textarea>
                            </td>
                        </tr>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                    </tbody>
                </table>
            </div>
            <div class = "form-group">
                <div class="col-sm-12 mt20">
                    <a class="btn btn-default js_cate-add-schedule"><?php echo __("Add new row");?>
</a>
                    <button type="submit" class="btn btn-primary padrl30"><?php echo __("Save");?>
</button>
                </div>
            </div>
            <!-- success -->
            <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
            <!-- success -->
            <!-- error -->
            <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
            <!-- error -->
        </form>
    </div>
<?php }?>
</div><?php }
}
