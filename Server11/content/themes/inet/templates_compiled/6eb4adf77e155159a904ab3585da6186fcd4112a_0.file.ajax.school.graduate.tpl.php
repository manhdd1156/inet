<?php
/* Smarty version 3.1.31, created on 2021-03-31 14:12:21
  from "D:\workplace\Server11\content\themes\inet\templates\ci\school\ajax.school.graduate.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_606420d5c99a49_76402664',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '6eb4adf77e155159a904ab3585da6186fcd4112a' => 
    array (
      0 => 'D:\\workplace\\Server11\\content\\themes\\inet\\templates\\ci\\school\\ajax.school.graduate.tpl',
      1 => 1552404702,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_606420d5c99a49_76402664 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_assignInScope('idxx', 1);
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['results']->value, 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
    <table class="table table-striped table-bordered table-hover">
        <thead>
        <tr>
            <td colspan="6"><strong><?php echo $_smarty_tpl->tpl_vars['idxx']->value;?>
 - <?php echo $_smarty_tpl->tpl_vars['row']->value['child']['child_name'];?>
</strong></td>
            <input type="hidden" name="child_ids[]" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['child']['child_id'];?>
"/>
        </tr>
        <tr>
            <td colspan="6">
                <strong>I. <?php echo __("Fee usage history");?>
&nbsp;(<?php echo __("Number of charged days from last tuition");?>
:&nbsp;<?php echo $_smarty_tpl->tpl_vars['row']->value['data']['attendance_count'];?>
)</strong>
                <input type="hidden" name="attendance_count_<?php echo $_smarty_tpl->tpl_vars['row']->value['child']['child_id'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['data']['attendance_count'];?>
"/>
            </td>
        </tr>
        <tr>
            <th>#</th>
            <th><?php echo __("Fee name");?>
</th>
            <th class="text-center"><?php echo __("Unit");?>
</th>
            <th class="text-center"><?php echo __("Quantity");?>
</th>
            <th class="text-right"><?php echo __("Unit price");?>
&nbsp;(<?php echo @constant('MONEY_UNIT');?>
)</th>
            <th class="text-right"><?php echo __("Money amount");?>
&nbsp;(<?php echo @constant('MONEY_UNIT');?>
)</th>
        </tr>
        </thead>
        <tbody>
        
        <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['row']->value['data']['fees'], 'fee');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['fee']->value) {
?>
            <tr>
                <td class="text-center">
                    <?php echo $_smarty_tpl->tpl_vars['idx']->value;?>

                    <input type="hidden" class="fee_id" name="fee_id_<?php echo $_smarty_tpl->tpl_vars['row']->value['child']['child_id'];?>
[]" value="<?php echo $_smarty_tpl->tpl_vars['fee']->value['fee_id'];?>
"/>
                </td>
                <td><?php echo $_smarty_tpl->tpl_vars['fee']->value['fee_name'];?>
</td>
                <td class="text-center"><?php if ($_smarty_tpl->tpl_vars['fee']->value['fee_type'] == @constant('FEE_TYPE_MONTHLY')) {
echo __('Monthly');
} else {
echo __('Daily');
}?></td>
                <td>
                    <input style="width: 50px;" type="number" class="text-center class_tuition4leave_quantity" id="quantity_<?php echo $_smarty_tpl->tpl_vars['fee']->value['fee_id'];?>
_<?php echo $_smarty_tpl->tpl_vars['row']->value['child']['child_id'];?>
" fee_id="<?php echo $_smarty_tpl->tpl_vars['fee']->value['fee_id'];?>
" child-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['child']['child_id'];?>
"
                           name="quantity_<?php echo $_smarty_tpl->tpl_vars['row']->value['child']['child_id'];?>
[]" min="0" value="<?php echo $_smarty_tpl->tpl_vars['fee']->value['quantity'];?>
" step="1"/>
                </td>
                <td><input style="width: 100px;" type="text" class="text-right class_tuition4leave_unit_price money_tui" id="unit_price_<?php echo $_smarty_tpl->tpl_vars['fee']->value['fee_id'];?>
_<?php echo $_smarty_tpl->tpl_vars['row']->value['child']['child_id'];?>
" fee_id="<?php echo $_smarty_tpl->tpl_vars['fee']->value['fee_id'];?>
" child-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['child']['child_id'];?>
"
                           name="unit_price_<?php echo $_smarty_tpl->tpl_vars['row']->value['child']['child_id'];?>
[]" min="0" value="<?php echo $_smarty_tpl->tpl_vars['fee']->value['unit_price'];?>
" step="1"/></td>
                <td><input style="width: 100px;" type="text" class="text-right money_tui" id="money_<?php echo $_smarty_tpl->tpl_vars['fee']->value['fee_id'];?>
_<?php echo $_smarty_tpl->tpl_vars['row']->value['child']['child_id'];?>
" name="money_<?php echo $_smarty_tpl->tpl_vars['row']->value['child']['child_id'];?>
_[]" value="<?php echo $_smarty_tpl->tpl_vars['fee']->value['to_money'];?>
" readonly/></td>
            </tr>
            <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>


        
        <tr><td colspan="6" class="text-left"><strong>II. <?php echo __("Service");?>
</strong></td></tr>
        <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['row']->value['data']['services'], 'service');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['service']->value) {
?>
            <tr>
                <td class="text-center">
                    <?php echo $_smarty_tpl->tpl_vars['idx']->value;?>

                    <input type="hidden" class="service_id" name="service_id_<?php echo $_smarty_tpl->tpl_vars['row']->value['child']['child_id'];?>
[]" value="<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
"/>
                </td>
                <td><?php echo $_smarty_tpl->tpl_vars['service']->value['service_name'];?>
</td>
                <td class="text-center">
                    <input type="hidden" name="service_type_<?php echo $_smarty_tpl->tpl_vars['row']->value['child']['child_id'];?>
[]" value="<?php echo $_smarty_tpl->tpl_vars['service']->value['service_type'];?>
"/>
                    <?php if ($_smarty_tpl->tpl_vars['service']->value['service_type'] == @constant('SERVICE_TYPE_MONTHLY')) {
echo __('Monthly');
} else {
echo __('Daily');
}?>
                </td>
                <td>
                    <input  style="width: 50px;" type="number" class="text-center class_tuition4leave_service_quantity" id="service_quantity_<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
_<?php echo $_smarty_tpl->tpl_vars['row']->value['child']['child_id'];?>
" service_id="<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
" child-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['child']['child_id'];?>
"
                            name="service_quantity_<?php echo $_smarty_tpl->tpl_vars['row']->value['child']['child_id'];?>
[]" min="0" value="<?php echo $_smarty_tpl->tpl_vars['service']->value['quantity'];?>
" step="1"/>
                </td>
                <td><input style="width: 100px;" type="text" class="text-right class_tuition4leave_service_fee money_tui" id="service_fee_<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
_<?php echo $_smarty_tpl->tpl_vars['row']->value['child']['child_id'];?>
" child-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['child']['child_id'];?>
"
                           service_id="<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
" name="service_fee_<?php echo $_smarty_tpl->tpl_vars['row']->value['child']['child_id'];?>
[]" min="0" value="<?php echo $_smarty_tpl->tpl_vars['service']->value['unit_price'];?>
" step="1"/></td>
                <td><input style="width: 100px;" type="text" class="text-right money_tui" id="service_amount_<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
_<?php echo $_smarty_tpl->tpl_vars['row']->value['child']['child_id'];?>
" name="service_amount_<?php echo $_smarty_tpl->tpl_vars['row']->value['child']['child_id'];?>
_[]" value="<?php echo $_smarty_tpl->tpl_vars['service']->value['to_money'];?>
" readonly/></td>
            </tr>
            <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>


        
        <tr>
            <td colspan="6"><strong>III. <?php echo __("Count-based service");?>
</strong></td>
        </tr>
        <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['row']->value['data']['count_based_services'], 'service');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['service']->value) {
?>
            <tr>
                <td class="text-center">
                    <?php echo $_smarty_tpl->tpl_vars['idx']->value;?>

                    <input type="hidden" class="cbservice_id" name="cbservice_id_<?php echo $_smarty_tpl->tpl_vars['row']->value['child']['child_id'];?>
[]" value="<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
"/>
                </td>
                <?php if ($_smarty_tpl->tpl_vars['service']->value['service_id'] > 0) {?>
                    <td><?php echo $_smarty_tpl->tpl_vars['service']->value['service_name'];?>
</td>
                    <td class="text-center"><?php echo __('Times');?>
</td>
                    <td>
                        <input style="width: 50px;" type="number" class="text-center class_tuition4leave_service_quantity" id="service_quantity_<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
_<?php echo $_smarty_tpl->tpl_vars['row']->value['child']['child_id'];?>
" service_id="<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
" child-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['child']['child_id'];?>
"
                               name="cbservice_quantity_<?php echo $_smarty_tpl->tpl_vars['row']->value['child']['child_id'];?>
[]" min="0" value="<?php echo $_smarty_tpl->tpl_vars['service']->value['quantity'];?>
" step="1"/>
                    </td>
                    <td><input style="width: 100px;" type="text" class="text-right class_tuition4leave_service_fee money_tui" id="service_fee_<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
_<?php echo $_smarty_tpl->tpl_vars['row']->value['child']['child_id'];?>
" child-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['child']['child_id'];?>
"
                               service_id="<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
" name="cbservice_price_<?php echo $_smarty_tpl->tpl_vars['row']->value['child']['child_id'];?>
[]" min="0" value="<?php echo $_smarty_tpl->tpl_vars['service']->value['unit_price'];?>
" step="1"/></td>
                    <td><input style="width: 100px;" type="text" class="text-right money_tui" id="service_amount_<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
_<?php echo $_smarty_tpl->tpl_vars['row']->value['child']['child_id'];?>
" name="service_amount_<?php echo $_smarty_tpl->tpl_vars['row']->value['child']['child_id'];?>
_[]" value="<?php echo $_smarty_tpl->tpl_vars['service']->value['to_money'];?>
" readonly/></td>
                <?php } else { ?>
                    <td colspan="4">
                        <?php echo $_smarty_tpl->tpl_vars['service']->value['service_name'];?>

                        <input type="hidden" id="service_quantity_<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
_<?php echo $_smarty_tpl->tpl_vars['row']->value['child']['child_id'];?>
" service_id="<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
" name="cbservice_quantity_<?php echo $_smarty_tpl->tpl_vars['row']->value['child']['child_id'];?>
[]" value="<?php echo $_smarty_tpl->tpl_vars['service']->value['quantity'];?>
"/>
                        <input type="hidden" id="service_fee_<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
_<?php echo $_smarty_tpl->tpl_vars['row']->value['child']['child_id'];?>
" service_id="<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
" name="cbservice_price_<?php echo $_smarty_tpl->tpl_vars['row']->value['child']['child_id'];?>
[]" value="<?php echo $_smarty_tpl->tpl_vars['service']->value['unit_price'];?>
"/>
                    </td>
                    <td><input style="width: 100px;" type="text" class="text-right class_tuition4leave_pick_up money_tui" id="service_amount_<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
_<?php echo $_smarty_tpl->tpl_vars['row']->value['child']['child_id'];?>
" child-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['child']['child_id'];?>
" name="service_amount_<?php echo $_smarty_tpl->tpl_vars['row']->value['child']['child_id'];?>
_[]" value="<?php echo $_smarty_tpl->tpl_vars['service']->value['to_money'];?>
"/></td>
                <?php }?>
            </tr>
            <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

        <tr>
            <td colspan="5" class="text-right"><strong>IV. <?php echo __("Debt amount of previous month");?>
&nbsp;(<?php echo @constant('MONEY_UNIT');?>
)</strong></td>
            <td><input style="width: 100px;" type='text' name="debt_amount_<?php echo $_smarty_tpl->tpl_vars['row']->value['child']['child_id'];?>
" id="debt_amount_<?php echo $_smarty_tpl->tpl_vars['row']->value['child']['child_id'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['data']['debt_amount'];?>
" class="text-right money_tui" readonly/></td>
        </tr>
        <tr>
            <td colspan="5" class="text-right"><strong>V. <?php echo __("Tuition deduction of previous month");?>
&nbsp;(<?php echo @constant('MONEY_UNIT');?>
)</strong></td>
            <td><input style="width: 100px;" type='text' name="total_deduction_<?php echo $_smarty_tpl->tpl_vars['row']->value['child']['child_id'];?>
" id="total_deduction_<?php echo $_smarty_tpl->tpl_vars['row']->value['child']['child_id'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['data']['total_deduction'];?>
" class="text-right money_tui" readonly/></td>
        </tr>
        <tr>
            <td colspan="5" class="text-right"><strong>VI. <?php echo __("Paid tuition amount at begining");?>
&nbsp;(<?php echo @constant('MONEY_UNIT');?>
)</strong></td>
            <td><input style="width: 100px;" type='text' name="paid_amount_<?php echo $_smarty_tpl->tpl_vars['row']->value['child']['child_id'];?>
" id="paid_amount_<?php echo $_smarty_tpl->tpl_vars['row']->value['child']['child_id'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['data']['paid_amount'];?>
" class="text-right money_tui" readonly/></td>
        </tr>
        <tr>
            <td colspan="5" class="text-right"><strong><?php echo __("Final total");?>
&nbsp;(I + II + III + IV - V - VI)&nbsp;(<?php echo @constant('MONEY_UNIT');?>
)</strong></td>
            <td><input style="width: 100px;" type='text' name="final_amount_<?php echo $_smarty_tpl->tpl_vars['row']->value['child']['child_id'];?>
" id="final_amount_<?php echo $_smarty_tpl->tpl_vars['row']->value['child']['child_id'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['data']['final_amount'];?>
" class="text-right money_tui" readonly/></td>
        </tr>
        </tbody>
    </table>
    <?php $_smarty_tpl->_assignInScope('idxx', $_smarty_tpl->tpl_vars['idxx']->value+1);
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

<?php }
}
