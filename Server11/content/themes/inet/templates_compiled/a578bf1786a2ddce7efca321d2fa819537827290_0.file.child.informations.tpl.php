<?php
/* Smarty version 3.1.31, created on 2021-03-31 10:53:16
  from "D:\workplace\Server11\content\themes\inet\templates\ci\child\child.informations.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_6063f22c2e7949_83168374',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'a578bf1786a2ddce7efca321d2fa819537827290' => 
    array (
      0 => 'D:\\workplace\\Server11\\content\\themes\\inet\\templates\\ci\\child\\child.informations.tpl',
      1 => 1552404702,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6063f22c2e7949_83168374 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <div class="pull-right flip">
            <a href="https://blog.coniu.vn/huong-dan-phu-huynh-tao-thong-tin-nguoi-don-tre-tren-website-inet/" class="btn btn-info btn_guide" target="_blank">
                <i class="fa fa-info-circle" aria-hidden="true"></i> <?php echo __("Guide");?>

            </a>
            <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == "add" || $_smarty_tpl->tpl_vars['sub_view']->value == "edit") {?>
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/child/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
/informations" class="btn btn-default">
                    <i class="fa fa-list"></i> <?php echo __("Lists");?>

                </a>
            <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
                <?php if (count($_smarty_tpl->tpl_vars['rows']->value) < 4) {?>
                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/child/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
/informations/add" class="btn btn-default">
                        <i class="fa fa-plus"></i> <?php echo __("Add New");?>

                    </a>
                <?php }?>
            <?php }?>
        </div>
        <i class="fa fa-info-circle fa-lg fa-fw pr10"></i>
        <?php echo __("Student information");?>

        <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
            &rsaquo; <?php echo __("Lists");?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
            &rsaquo; <?php echo __('Add New');?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "edit") {?>
            &rsaquo; <?php echo __('Edit');?>

        <?php }?>
    </div>
    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
        <div class="panel-body with-table">
            <div class="panel-body with-table">
                <div class="form-group">
                    <strong><?php echo __("Picker lists");?>
</strong>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <tbody>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['rows']->value, 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
                                <tr>
                                    <td style="width: 60%">
                                        <?php if (!is_empty($_smarty_tpl->tpl_vars['row']->value['picker_source_file'])) {?>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['row']->value['picker_source_file'];?>
" target="_blank"><img src = "<?php echo $_smarty_tpl->tpl_vars['row']->value['picker_source_file'];?>
" style="width: 100%" class = "img-responsive"></a>
                                        <?php } else { ?>
                                            <?php echo __("No information");?>

                                        <?php }?>
                                    </td>
                                    <td style="width: 40%">
                                        <strong><?php echo __("Picker name");?>
:</strong> <?php echo $_smarty_tpl->tpl_vars['row']->value['picker_name'];?>
 <br/>
                                        <strong><?php echo __("Relation with student");?>
:</strong> <?php echo $_smarty_tpl->tpl_vars['row']->value['picker_relation'];?>
 <br/>
                                        <strong><?php echo __("Telephone");?>
:</strong> <?php echo $_smarty_tpl->tpl_vars['row']->value['picker_phone'];?>
 <br/>
                                        <strong><?php echo __("Address");?>
:</strong> <?php echo $_smarty_tpl->tpl_vars['row']->value['picker_address'];?>
 <br/>
                                        <strong><?php echo __("Creator");?>
:</strong> <?php echo $_smarty_tpl->tpl_vars['row']->value['user_fullname'];?>
 <br/><br/>
                                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/child/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
/informations/edit/<?php echo $_smarty_tpl->tpl_vars['row']->value['child_info_id'];?>
" class="btn btn-primary btn-xs"><?php echo __("Edit");?>
</a> <a href="#" class="btn btn-danger btn-xs js_child-picker-delete" data-child="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['child_info_id'];?>
"><?php echo __("Delete");?>
</a>
                                    </td>
                                </tr>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
        <div class="panel-body">
            <form class="form-horizontal" enctype="multipart/form-data" method="post" id="edit_information">
                <input type="hidden" name="child_id" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
"/>
                <input type="hidden" name="do" value="add_info"/>

                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Picker name");?>
 (*)</label>
                    <div class="col-sm-9">
                        <input class="form-control" name="picker_name" maxlength="100">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Relation with student");?>
 (*)</label>
                    <div class="col-sm-9">
                        <input class="form-control" name="picker_relation" required maxlength="100">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Telephone");?>
 (*)</label>
                    <div class="col-sm-3">
                        <input class="form-control" type="text" name="phone" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Address");?>
 (*)</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="address" rows="6" required></textarea>
                    </div>
                </div>


                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("File attachment");?>
</label>
                    <div class="col-sm-6">
                        <input type="file" name="file" id="file"/>
                    </div>
                </div>
                
                
                
                <div class="form-group">
                    <div class="col-sm-3 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30"><?php echo __("Save");?>
</button>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "edit") {?>
        <div class="panel-body">
            <form class="form-horizontal" enctype="multipart/form-data" method="post" id="edit_information">
                <input type="hidden" name="child_info_id" value="<?php echo $_smarty_tpl->tpl_vars['rows']->value['child_info_id'];?>
"/>
                <input type="hidden" name="child_id" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
"/>
                <input type="hidden" name="do" value="edit_info"/>

                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Picker name");?>
 (*)</label>
                    <div class="col-sm-9">
                        <input class="form-control" name="picker_name" maxlength="100" value="<?php echo $_smarty_tpl->tpl_vars['rows']->value['picker_name'];?>
">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Relation with student");?>
 (*)</label>
                    <div class="col-sm-9">
                        <input class="form-control" name="picker_relation" required maxlength="100" value="<?php echo $_smarty_tpl->tpl_vars['rows']->value['picker_relation'];?>
">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Telephone");?>
 (*)</label>
                    <div class="col-sm-3">
                        <input class="form-control" type="text" name="phone" value="<?php echo $_smarty_tpl->tpl_vars['rows']->value['picker_phone'];?>
" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Address");?>
 (*)</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="address" rows="6" required><?php echo $_smarty_tpl->tpl_vars['rows']->value['picker_address'];?>
</textarea>
                    </div>
                </div>


                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("File attachment");?>
</label>
                    <div class="col-sm-6">
                        <?php if (!is_empty($_smarty_tpl->tpl_vars['rows']->value['picker_source_file'])) {?>
                            <a href="<?php echo $_smarty_tpl->tpl_vars['rows']->value['picker_source_file'];?>
" target="_blank"><img src = "<?php echo $_smarty_tpl->tpl_vars['rows']->value['picker_source_file'];?>
" class = "img-responsive"></a>
                            <br>
                            <label class="control-label"><?php echo __("Choose file replace");?>
</label>
                            <br>
                        <?php }?>
                        <input type="file" name="file" id="file"/>
                    </div>
                </div>
                
                
                
                <div class="form-group">
                    <div class="col-sm-3 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30"><?php echo __("Save");?>
</button>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    <?php }?>
</div><?php }
}
