<?php
/* Smarty version 3.1.31, created on 2021-06-24 15:27:32
  from "D:\workplace\Inet-project\source\Server11\content\themes\inet\templates\ci\school\school.courses.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_60d441f4d47891_69281033',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e005e65f676458631a7ebbabc580d837048917fb' => 
    array (
      0 => 'D:\\workplace\\Inet-project\\source\\Server11\\content\\themes\\inet\\templates\\ci\\school\\school.courses.tpl',
      1 => 1624523193,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60d441f4d47891_69281033 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="" style="position: relative">
    <div class="panel panel-default">
        <div class="panel-heading with-icon">
            <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == "edit") {?>
                <div class="pull-right flip">
                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/courses" class="btn btn-default">
                        <i class="fa fa-list"></i> <?php echo __("Lists");?>

                    </a>
                </div>
            <?php }?>
            <i class="fa fa-gavel fa-fw fa-lg pr10"></i>
            <?php echo __("Courses");?>

            <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == "edit") {?>
                &rsaquo; <?php echo __('Edit');?>

            <?php }?>
        </div>
        <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
            <div class="panel-body with-table">
                    <form class="form-horizontal" action="#" enctype="multipart/form-data" method="post" id="course_search_form">
                    <input type="hidden" name="school_username" id="school_username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                    <input type="hidden" name="do" value="search_course"/>
                    <div class="form-group">
                        <div class="col-sm-3 col-sm-offset-9">
                            <select name="school_year" id="school_year" class="form-control">
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['default_year']->value, 'year', false, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['year']->value) {
?>
                                    
                                    <option value="<?php echo $_smarty_tpl->tpl_vars['year']->value;?>
"
                                            <?php if ((substr($_smarty_tpl->tpl_vars['year']->value,0,strlen(date("Y"))) === date("Y"))) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['year']->value;?>
</option>
                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left"><?php echo __("Select class level");?>
 (*)</label>
                        <div class="col-sm-3">
                            <select name="class_level_id" id="course_class_level_id" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"
                                    class="form-control" autofocus>
                                <option value=""><?php echo __("Select class level");?>
</option>
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['class_levels']->value, 'level');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['level']->value) {
?>
                                    <option value="<?php echo $_smarty_tpl->tpl_vars['level']->value['class_level_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['level']->value['class_level_name'];?>
</option>
                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                            </select>
                        </div>
                        <div class="col-sm-3">
                            <select name="class_id" id="course_class_id" class="form-control"
                                    data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" required>
                                <option value=""><?php echo __("Select class");?>
...</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                    </div>
                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-3">
                            <button type="submit" id="submit_id" class="btn btn-primary padrl30"><?php echo __("Search");?>
</button>
                        </div>
                    </div>
                    <div id="course_list_point"></div>
                    <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                    <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                </div>
            </div>
        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "edit") {?>
            <div class="panel-body">
                <form class="form-horizontal" enctype="multipart/form-data" method="post" id="school_edit_conduct">
                    <input type="hidden" name="school_username" id="school_username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                    <input type="hidden" name="conduct_id" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['conduct_id'];?>
"/>
                    <input type="hidden" name="child_id" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
"/>
                    <input type="hidden" name="do" value="edit"/>
                    <div class = "form-group">
                        <label class="col-sm-3 control-label text-left">
                            <?php echo __("Student");?>

                        </label>
                        <div class = "col-sm-9">
                            <input type="text" class="form-control" value = "<?php echo $_smarty_tpl->tpl_vars['child']->value['child_name'];?>
 - <?php echo $_smarty_tpl->tpl_vars['child']->value['birthday'];?>
" disabled>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left"><?php echo __("Semester");?>
 1
                        </label>
                        <div class="col-sm-9">
                            <select name="conduct_id1" id="conduct_id1" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-view="edit" class="form-control">
                                <option value=""><?php echo __("Select conduct");?>
</option>
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['conducts']->value, 'conduct');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['conduct']->value) {
?>
                                    <option value="<?php echo $_smarty_tpl->tpl_vars['conduct']->value;?>
" <?php if ($_smarty_tpl->tpl_vars['data']->value['hk1'] == $_smarty_tpl->tpl_vars['conduct']->value) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['conduct']->value;?>
</option>
                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                
                                
                                
                                
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left"><?php echo __("Semester");?>
 2
                        </label>
                        <div class="col-sm-9">
                            <select name="conduct_id2" id="conduct_id2" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-view="edit" class="form-control">
                                <option value=""><?php echo __("Select conduct");?>
</option>
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['conducts']->value, 'conduct');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['conduct']->value) {
?>
                                    <option value="<?php echo $_smarty_tpl->tpl_vars['conduct']->value;?>
" <?php if ($_smarty_tpl->tpl_vars['data']->value['hk2'] == $_smarty_tpl->tpl_vars['conduct']->value) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['conduct']->value;?>
</option>
                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left"><?php echo __("End semester");?>

                        </label>
                        <div class="col-sm-9">
                            <select name="conduct_id3" id="conduct_id3" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-view="edit" class="form-control">
                                <option value=""><?php echo __("Select conduct");?>
</option>
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['conducts']->value, 'conduct');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['conduct']->value) {
?>
                                    <option value="<?php echo $_smarty_tpl->tpl_vars['conduct']->value;?>
" <?php if ($_smarty_tpl->tpl_vars['data']->value['ck'] == $_smarty_tpl->tpl_vars['conduct']->value) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['conduct']->value;?>
</option>
                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-3">
                            <button type="submit" class="btn btn-primary padrl30"><?php echo __("Save");?>
</button>
                        </div>
                    </div>

                    <!-- success -->
                    <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                    <!-- success -->

                    <!-- error -->
                    <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                    <!-- error -->
                </form>
            </div>
        <?php }?>
    </div>
</div><?php }
}
