<?php
/* Smarty version 3.1.31, created on 2021-06-22 10:24:52
  from "D:\workplace\Inet-project\source\Server11\content\themes\inet\templates\ci\noga\noga.pages.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_60d15804e97fb3_02149551',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '7a79bf2351ac43dc41ca20c659922e30831edc59' => 
    array (
      0 => 'D:\\workplace\\Inet-project\\source\\Server11\\content\\themes\\inet\\templates\\ci\\noga\\noga.pages.tpl',
      1 => 1621501238,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:ci/noga/ajax.noga.page.list.tpl' => 1,
  ),
),false)) {
function content_60d15804e97fb3_02149551 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == "edit") {?>
            <div class="pull-right flip">
                <a target="_blank" href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/pages/<?php echo $_smarty_tpl->tpl_vars['data']->value['page_name'];?>
" class="btn btn-info">
                    <?php echo __("Go to this page");?>

                </a>
            </div>
        <?php }?>
        <i class="fa fa-flag pr5 panel-icon"></i>
        <strong><?php echo __("Pages");?>
</strong>
        <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == "edit") {?> &rsaquo; <strong><?php echo $_smarty_tpl->tpl_vars['data']->value['page_title'];?>
</strong><?php }?>
    </div>
    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value != "edit") {?>
        <div class="panel-body with-table">
            <div class="row">
                <label class="col-sm-3 control-label text-left"><?php echo __("Post time");?>
</label>
                <div class='col-sm-3'>
                    <div class='input-group date' id='fromuserregpicker'>
                        <input type='text' name="fromDate" id="fromDate" class="form-control" placeholder="<?php echo __("From date");?>
"/>
                        <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                    </div>
                </div>
                <div class='col-sm-3'>
                    <div class='input-group date' id='touserregpicker'>
                        <input type='text' name="toDate" id="toDate" class="form-control" placeholder="<?php echo __("To date");?>
"/>
                        <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                    </div>
                </div>
                <div class='col-sm-2'>
                    <div class="form-group">
                        <a href="#" id="search" class="btn btn-default js_noga-page-search"><?php echo __("Search");?>
</a>
                        <label id="loading" class="btn btn-info x-hidden"><?php echo __("Loading");?>
...</label>
                    </div>
                </div>
            </div>
            <div id="page_list">
                <?php $_smarty_tpl->_subTemplateRender("file:ci/noga/ajax.noga.page.list.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

            </div>
        </div>

        <?php echo '<script'; ?>
 type="text/javascript">
            $(function() {
                // run DataTable
                $('.js_dataTable').DataTable({
                    "aoColumnDefs": [ { 'aDataSort': false, 'aTargets': [ -1 ] } ],
                    "language": {
                        "decimal":        "",
                        "emptyTable":     __["No data available in table"],
                        "info":           __["Showing _START_ to _END_ of _TOTAL_ results"],
                        "infoEmpty":      __["Showing 0 to 0 of 0 results"],
                        "infoFiltered":   "(filtered from _MAX_ total entries)",
                        "infoPostFix":    "",
                        "thousands":      ",",
                        "lengthMenu":     __["Show"] + " " + "_MENU_" + " " + __["results"],
                        "loadingRecords": __["Loading..."],
                        "processing":     __["Processing..."],
                        "search":         __["Search"],
                        "zeroRecords":    __["No matching records found"],
                        "paginate": {
                            "first":      __["First"],
                            "last":       __["Last"],
                            "next":       __["Next"],
                            "previous":   __["Previous"]
                        },
                        "aria": {
                            "sortAscending":  ": activate to sort column ascending",
                            "sortDescending": ": activate to sort column descending"
                        }
                    }
                });
            });
        <?php echo '</script'; ?>
>

    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "edit") {?>
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-offset-3 col-xs-6 col-sm-offset-0 col-sm-2 mb10">
                    <img class="img-responsive img-thumbnail" src="<?php echo $_smarty_tpl->tpl_vars['data']->value['page_picture'];?>
">
                </div>
                <div class="col-xs-12 col-sm-10 mb10">
                    <ul class="list-group">
                        <li class="list-group-item">
                            <span class="badge"><?php echo $_smarty_tpl->tpl_vars['data']->value['page_id'];?>
</span>
                            <?php echo __("Page ID");?>

                        </li>
                        <li class="list-group-item">
                            <span class="badge"><?php echo $_smarty_tpl->tpl_vars['data']->value['page_likes'];?>
</span>
                            <?php echo __("Likes");?>

                        </li>
                    </ul>
                </div>
            </div>
            <!-- tabs nav -->
            <ul class="nav nav-tabs mb20">
                <li class="active">
                    <a href="#basic" data-toggle="tab">
                        <strong class="pr5"><?php echo __("Page Info");?>
</strong>
                    </a>
                </li>
            </ul>
            <!-- tabs nav -->

            <!-- tabs content -->
            <div class="tab-content">
                <!-- basic tab -->
                <div class="tab-pane active" id="basic">
                    <form class="js_ajax-forms form-horizontal" data-url="admin/page.php?id=<?php echo $_smarty_tpl->tpl_vars['data']->value['page_id'];?>
">
                        <div class="form-group">
                            <label class="col-sm-3 control-label text-left">
                                <?php echo __("Verified Page");?>

                            </label>
                            <div class="col-sm-9">
                                <div class="onoffswitch">
                                    <input type="checkbox" name="page_verified" class="onoffswitch-checkbox" id="page_verified" <?php if ($_smarty_tpl->tpl_vars['data']->value['page_verified']) {?>checked<?php }?>>
                                    <label class="onoffswitch-label" for="page_verified"></label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label text-left">
                                <?php echo __("Category");?>

                            </label>
                            <div class="col-sm-9">
                                <select class="form-control" name="page_category">
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['data']->value['categories'], 'category');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['category']->value) {
?>
                                        <option <?php if ($_smarty_tpl->tpl_vars['data']->value['page_category'] == $_smarty_tpl->tpl_vars['category']->value['category_id']) {?>selected<?php }?> value="<?php echo $_smarty_tpl->tpl_vars['category']->value['category_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['category']->value['category_name'];?>
</option>
                                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label text-left">
                                <?php echo __("Title");?>

                            </label>
                            <div class="col-sm-9">
                                <input class="form-control" name="page_title" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['page_title'];?>
">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label text-left">
                                <?php echo __("Username");?>

                            </label>
                            <div class="col-sm-9">
                                <input class="form-control" name="page_name" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['page_name'];?>
">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label text-left">
                                <?php echo __("Username");?>

                            </label>
                            <div class="col-sm-9">
                                <textarea class="form-control" name="page_description"><?php echo $_smarty_tpl->tpl_vars['data']->value['page_description'];?>
</textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-9 col-sm-offset-3">
                                <button type="submit" class="btn btn-primary"><?php echo __("Save Changes");?>
</button>
                            </div>
                        </div>

                        <!-- success -->
                        <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                        <!-- success -->

                        <!-- error -->
                        <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                        <!-- error -->
                    </form>
                </div>
            </div>
            <!-- tabs content -->
        </div>
    <?php }?>
</div><?php }
}
