<?php
/* Smarty version 3.1.31, created on 2021-04-29 17:21:07
  from "D:\workplace\mascom-edu-server\Server11\content\themes\inet\templates\ci\class\ajax.service.childlist4record.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_608a88936912c7_76960232',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '4a497923adbf5b70dccfad1e7e3ddbd7d1a2401e' => 
    array (
      0 => 'D:\\workplace\\mascom-edu-server\\Server11\\content\\themes\\inet\\templates\\ci\\class\\ajax.service.childlist4record.tpl',
      1 => 1619677394,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_608a88936912c7_76960232 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div><strong><?php echo __("Children list");?>
&nbsp;(<?php echo __("Children");?>
: <?php echo count($_smarty_tpl->tpl_vars['results']->value['children']);?>
&nbsp;|&nbsp;<?php echo __("Usage");?>
: <?php echo $_smarty_tpl->tpl_vars['results']->value['usage_count'];?>
)</strong></div>
<table class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th>#</th>
            <th><?php echo __("Select");?>
</th>
            <th><?php echo __("Full name");?>
</th>
            <th><?php echo __("Child code");?>
</th>
            <th><?php echo __("Registration time");?>
</th>
            <th><?php echo __("Registrar");?>
</th>
        </tr>
    </thead>
    <tbody>
    <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['results']->value['children'], 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
            <tr <?php if (($_smarty_tpl->tpl_vars['row']->value['child_status'] == 0) || in_array($_smarty_tpl->tpl_vars['row']->value['child_id'],$_smarty_tpl->tpl_vars['absent_child_ids']->value)) {?>class="row-disable"<?php }?>>
                <td align="center"><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</td>
                <td align="center">
                    <input type="checkbox" class="child" name="childIds[]" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['child_id'];?>
" <?php if ($_smarty_tpl->tpl_vars['row']->value['recorded_user_id'] > 0) {?>checked<?php }?> <?php if ((in_array($_smarty_tpl->tpl_vars['row']->value['child_id'],$_smarty_tpl->tpl_vars['absent_child_ids']->value))) {?>disabled<?php }?>>
                    <input type="hidden" name="allChildIds[]" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['child_id'];?>
"/>
                    <?php if ($_smarty_tpl->tpl_vars['row']->value['recorded_user_id'] > 0) {?>
                        <input type="hidden" name="oldChildIds[]" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['child_id'];?>
"/>
                    <?php }?>
                </td>
                <td><?php echo $_smarty_tpl->tpl_vars['row']->value['child_name'];?>
</td>
                <td><?php echo $_smarty_tpl->tpl_vars['row']->value['child_code'];?>
</td>
                <td>
                    <?php if ($_smarty_tpl->tpl_vars['row']->value['recorded_user_id'] > 0) {?>
                        <?php echo $_smarty_tpl->tpl_vars['row']->value['recorded_at'];?>

                    <?php }?>
                </td>
                <td>
                    <?php if ($_smarty_tpl->tpl_vars['row']->value['recorded_user_id'] > 0) {?>
                        <?php echo $_smarty_tpl->tpl_vars['row']->value['user_fullname'];?>

                        <?php if ($_smarty_tpl->tpl_vars['row']->value['recorded_user_id'] != $_smarty_tpl->tpl_vars['user']->value->_data['user_id']) {?>
                            &nbsp;<a href="#" class="far fa-comments fa-lg js_chat-start" data-name="<?php echo $_smarty_tpl->tpl_vars['row']->value['user_fullname'];?>
" data-uid="<?php echo $_smarty_tpl->tpl_vars['row']->value['recorded_user_id'];?>
"></a>
                        <?php }?>
                    <?php }?>
                </td>
            </tr>
            <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

    </tbody>
</table><?php }
}
