<?php
/* Smarty version 3.1.31, created on 2021-05-20 16:10:13
  from "D:\workplace\Inet-project\source\Server11\content\themes\inet\templates\ci\index.left_menu.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_60a62775851ef2_15825236',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '98db856fe45afdd077fecc8048973961d93b2848' => 
    array (
      0 => 'D:\\workplace\\Inet-project\\source\\Server11\\content\\themes\\inet\\templates\\ci\\index.left_menu.tpl',
      1 => 1621501238,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60a62775851ef2_15825236 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div <?php if (count($_smarty_tpl->tpl_vars['schools']->value) > 0 || count($_smarty_tpl->tpl_vars['class']->value) > 0 || count($_smarty_tpl->tpl_vars['children']->value) > 0) {?>class="manage_box"<?php }?>>
    
        
            
        
    
    <!-- Start school menu -->
    <?php if (count($_smarty_tpl->tpl_vars['schools']->value) > 0) {?>
        <li class="ptb5">
            <small class="text-muted"> <?php echo mb_strtoupper(__("schools"), 'UTF-8');?>
</small>
        </li>
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['schools']->value, 'school');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['school']->value) {
?>
            <li id="li-school" style="border: 1px solid #9c9c9cbd; border-radius: 5px; padding: 5px; margin-bottom: 5px">
                <?php if ($_smarty_tpl->tpl_vars['school']->value['is_teacher'] != 1) {?>
                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['school']->value['page_name'];?>
">
                        
                        <img src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/content/themes/<?php echo $_smarty_tpl->tpl_vars['system']->value['theme'];?>
/images/icons/ci/school_1.png">
                        <span><?php echo $_smarty_tpl->tpl_vars['school']->value['page_title'];?>
</span>
                    </a>
                <?php } else { ?>
                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['school']->value['page_name'];?>
/events">
                        
                        <img src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/content/themes/<?php echo $_smarty_tpl->tpl_vars['system']->value['theme'];?>
/images/icons/ci/school_1.png">
                        <span><?php echo $_smarty_tpl->tpl_vars['school']->value['page_title'];?>
</span>
                    </a>
                <?php }?>
            </li>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

    <?php }?>
    <!-- End school menu -->

    <!-- Start class menu -->
    <?php if (count($_smarty_tpl->tpl_vars['classes']->value) > 0) {?>
        <li class="ptb5">
            <small class="text-muted"> <?php echo mb_strtoupper(__("class"), 'UTF-8');?>
</small>
        </li>

        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['classes']->value, 'class');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class']->value) {
?>
            <?php if ($_smarty_tpl->tpl_vars['class']->value['school_status'] == 1) {?>
                <li style="border: 1px solid #9c9c9cbd; border-radius: 5px; padding: 5px; margin-bottom: 5px">
                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['class']->value['group_name'];?>
">
                        
                        <img src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/content/themes/<?php echo $_smarty_tpl->tpl_vars['system']->value['theme'];?>
/images/icons/ci/class_1.png">
                        <span><?php echo $_smarty_tpl->tpl_vars['class']->value['group_title'];?>
</span>
                    </a>
                </li>
            <?php }?>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

    <?php }?>
    <!-- End class menu -->

    <!-- Start child menu -->
    <?php if (count($_smarty_tpl->tpl_vars['children']->value) > 0) {?>
        <li class="ptb5">
            <small class="text-muted"> <?php echo mb_strtoupper(__("student"), 'UTF-8');?>
</small>
        </li>
        <div class="panel-body with-nav collapse navbar-collapse" id="collapse">
            <ul class="side-nav metismenu">
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['children']->value, 'child', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['child']->value) {
?>
                    <li style="border: 1px solid #9c9c9cbd; border-radius: 5px; margin-bottom: 5px">
                        
                        <a href="<?php if ($_smarty_tpl->tpl_vars['child']->value['school_id'] != 0 && $_smarty_tpl->tpl_vars['child']->value['school_status'] == 1) {
echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/child/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];
} else {
echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/childinfo/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_parent_id'];?>
/childdetail<?php }?>" style="padding: 5px; background: none; border-bottom: none;">
                            <img src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/content/themes/<?php echo $_smarty_tpl->tpl_vars['system']->value['theme'];?>
/images/icons/ci/children_1.png">
                            <?php echo $_smarty_tpl->tpl_vars['child']->value['child_name'];?>

                            
                        </a>
                        
                        
                        
                        
                        
                        

                        
                        
                        
                        
                        
                        
                        
                        
                    </li>
                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

            </ul>
        </div>
    <?php }?>
    <!-- End child menu -->

    <!-- ADD NEW -MANHDD -->
    <!-- Start itself child menu -->
    <?php if (count($_smarty_tpl->tpl_vars['itself']->value) > 0) {?>
        <li class="ptb5">
            <small class="text-muted"> <?php echo mb_strtoupper(__("me"), 'UTF-8');?>
</small>
        </li>
        <div class="panel-body with-nav collapse navbar-collapse" id="collapse">
            <ul class="side-nav metismenu">

                    <li style="border: 1px solid #9c9c9cbd; border-radius: 5px; margin-bottom: 5px">
                        
                        <a href="<?php if ($_smarty_tpl->tpl_vars['itself']->value['school_id'] != 0 && $_smarty_tpl->tpl_vars['itself']->value['school_status'] == 1) {
echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/child/<?php echo $_smarty_tpl->tpl_vars['itself']->value['child_id'];
} else {
echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/childinfo/<?php echo $_smarty_tpl->tpl_vars['itself']->value['child_parent_id'];?>
/childdetail<?php }?>" style="padding: 5px; background: none; border-bottom: none;">
                            <img src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/content/themes/<?php echo $_smarty_tpl->tpl_vars['system']->value['theme'];?>
/images/icons/ci/children_1.png">
                            <?php echo $_smarty_tpl->tpl_vars['itself']->value['child_name'];?>

                            
                        </a>
                        
                        
                        
                        
                        
                        

                        
                        
                        
                        
                        
                        
                        
                        
                    </li>
            </ul>
        </div>
    <?php }?>
    <!-- End child menu -->
</div>
<!-- Start add child -->
<!-- UPDATE START - MANHDD -->
<?php if (count($_smarty_tpl->tpl_vars['itself']->value) == 0) {?>
    <li class="ptb5">
        <a href = "<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/addchild">
            <small class="text-muted"><i class="fa fa-plus" aria-hidden="true"></i> <?php echo mb_strtoupper(__("Add new student"), 'UTF-8');?>
</small>
        </a>
    </li>
<?php }?>
<!-- UPDATE END - MANHDD -->
<!-- End add child -->



<?php }
}
