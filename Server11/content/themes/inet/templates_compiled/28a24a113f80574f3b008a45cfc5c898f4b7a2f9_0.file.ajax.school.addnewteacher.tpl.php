<?php
/* Smarty version 3.1.31, created on 2021-05-20 17:05:01
  from "D:\workplace\Inet-project\source\Server11\content\themes\inet\templates\ci\school\ajax.school.addnewteacher.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_60a6344d4b0391_54988416',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '28a24a113f80574f3b008a45cfc5c898f4b7a2f9' => 
    array (
      0 => 'D:\\workplace\\Inet-project\\source\\Server11\\content\\themes\\inet\\templates\\ci\\school\\ajax.school.addnewteacher.tpl',
      1 => 1621501238,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60a6344d4b0391_54988416 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="color_red mb10">
    <strong>Lưu ý:</strong> Tài khoản giáo viên sẽ được thêm vào trường của bạn sau khi bạn nhấn lưu ở bước này.
</div>
<form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_teacher.php">
    <input type="hidden" name="school_username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
    <input type="hidden" name="do" value="add_teacher_new_class"/>
    <div id="userIds"></div>
    <div class="form-group">
        <label class="col-sm-3 control-label text-left"><?php echo __("Full name");?>
 (*)</label>
        <div class="col-sm-9">
            <input name="full_name" id="full_name" type="text" class="form-control" autofocus maxlength="255">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label text-left"><?php echo __("Telephone");?>
 (*)</label>
        <div class="col-sm-9">
            <input name="user_phone" id="user_phone" type="text" class="form-control" maxlength="50">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label text-left"><?php echo __("Email");?>
 (*)</label>
        <div class="col-sm-9">
            <input name="email" id="email_teacher" type="email" class="form-control" maxlength="100">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label text-left"><?php echo __("Password");?>
 (*)</label>
        <div class="col-sm-9">
            <input name="password" id="password" type="password" class="form-control">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label text-left"><?php echo __("Gender");?>
 (*)</label>
        <div class="col-sm-3">
            <select name="gender" id="gender" class="form-control" >
                <option value="<?php echo @constant('FEMALE');?>
"><?php echo __("Female");?>
</option>
                <option value="<?php echo @constant('MALE');?>
"><?php echo __("Male");?>
</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-9 col-sm-offset-3">
            <a class="btn btn-primary padrl30 js_school-teacher-add-done"><?php echo __("Save");?>
</a>
        </div>
    </div>

    <!-- success -->
    <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
    <!-- success -->

    <!-- error -->
    <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
    <!-- error -->
</form><?php }
}
