<?php
/* Smarty version 3.1.31, created on 2021-06-25 15:21:14
  from "D:\workplace\Inet-project\source\Server11\content\themes\inet\templates\ci\school\ajax.school.point.subject.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_60d591faa0a556_01221215',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '7e3f94ba188df843eb1dff0a0a943fe060a9a336' => 
    array (
      0 => 'D:\\workplace\\Inet-project\\source\\Server11\\content\\themes\\inet\\templates\\ci\\school\\ajax.school.point.subject.tpl',
      1 => 1621501238,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60d591faa0a556_01221215 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_assignInScope('idx', 1);
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['results']->value, 'subject');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['subject']->value) {
?>
    <div class="form-group">
        <div class="col-sm-3">



            <strong><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
. <?php echo $_smarty_tpl->tpl_vars['subject']->value['subject_name'];?>
</strong>
            <input type="hidden" name="subject_ids[]" value="<?php echo $_smarty_tpl->tpl_vars['subject']->value['subject_id'];?>
">
        </div>
        <div class="col-sm-3">
            <select name="teacher_ids[]" class="form-control">
                <option value="0"><?php echo __("Select teacher");?>
</option>
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['teachers']->value, 'teacher');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['teacher']->value) {
?>
                    <option value="<?php echo $_smarty_tpl->tpl_vars['teacher']->value['user_id'];?>
" <?php if ($_smarty_tpl->tpl_vars['subject']->value['teacher_id'] == $_smarty_tpl->tpl_vars['teacher']->value['user_id']) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['teacher']->value['user_fullname'];?>
</option>
                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

            </select>
        </div>
    </div>

    <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
}
}
