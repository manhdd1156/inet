<?php
/* Smarty version 3.1.31, created on 2021-05-10 16:22:01
  from "D:\workplace\mascom-edu-server\Server11\content\themes\inet\templates\class.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_6098fb39b7f7f0_60361489',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '3154805d25d279a0006dcf59bdc1a844ed9d7083' => 
    array (
      0 => 'D:\\workplace\\mascom-edu-server\\Server11\\content\\themes\\inet\\templates\\class.tpl',
      1 => 1620465076,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:_head.tpl' => 1,
    'file:_header.tpl' => 1,
    'file:ci/class/class.help.tpl' => 1,
    'file:ci/class/class.dashboard.tpl' => 1,
    'file:ci/class/class.".((string)$_smarty_tpl->tpl_vars[\'view\']->value).".tpl' => 1,
    'file:_footer.tpl' => 1,
  ),
),false)) {
function content_6098fb39b7f7f0_60361489 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:_head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php $_smarty_tpl->_subTemplateRender('file:_header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<!-- page content -->
<div class="container mt20"
     <?php if (($_smarty_tpl->tpl_vars['view']->value == 'attendance' && $_smarty_tpl->tpl_vars['sub_view']->value == '')) {?>style="width: 100%!important; padding-left: 10px; padding-right: 10px;"<?php }?>>
    <div class="row">
        <div class="<?php if ($_smarty_tpl->tpl_vars['view']->value == 'attendance' && $_smarty_tpl->tpl_vars['sub_view']->value == '') {?>col-sm-2 col-md-2<?php } else { ?> col-sm-3 col-md-3<?php }?>"
             id="menu_left">
            <nav class="navbar navbar-default" role="navigation">
                <div class="panel panel-default">
                    <div class="menu_school">
                        <?php if ($_smarty_tpl->tpl_vars['view']->value == '') {?>
                            <i class="fas fa-tachometer-alt fa-fw fa-lg pr10"></i>
                            <?php echo __("Dashboard");?>

                        <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "attendance") {?>
                            <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
                                <i class="fa fa-tasks fa-fw fa-lg pr10"></i>
                                <?php echo __("Attendance");?>

                                <i class="fa fa-caret-right fa-fw pr10"></i>
                                <?php echo __("Lists");?>

                            <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "rollup") {?>
                                <i class="fa fa-tasks fa-fw fa-lg pr10"></i>
                                <?php echo __("Attendance");?>

                                <i class="fa fa-caret-right fa-fw pr10"></i>
                                <?php echo __("Roll up");?>

                            <?php }?>
                        <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "medicines") {?>
                            <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
                                <i class="fa fa-medkit fa-lg fa-fw pr10"></i>
                                <?php echo __("Medicines");?>

                                <i class="fa fa-caret-right fa-fw pr10"></i>
                                <?php echo __("Today lists");?>

                            <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "all") {?>
                                <i class="fa fa-medkit fa-lg fa-fw pr10"></i>
                                <?php echo __("Medicines");?>

                                <i class="fa fa-caret-right fa-fw pr10"></i>
                                <?php echo __("List all");?>

                            <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
                                <i class="fa fa-medkit fa-lg fa-fw pr10"></i>
                                <?php echo __("Medicines");?>

                                <i class="fa fa-caret-right fa-fw pr10"></i>
                                <?php echo __("Add New");?>

                            <?php }?>
                        <?php } elseif (($_smarty_tpl->tpl_vars['view']->value == "schedules")) {?>
                            <i class="fas fa-calendar-alt fa-lg fa-fw pr10"></i>
                            <?php echo __("Schedule");?>

                        <?php } elseif (($_smarty_tpl->tpl_vars['view']->value == "menus")) {?>
                            <i class="fas fa-utensils fa-lg fa-fw pr10"></i>
                            <?php echo __("Menu");?>

                        <?php } elseif (($_smarty_tpl->tpl_vars['view']->value == "useservices") || ($_smarty_tpl->tpl_vars['view']->value == "services")) {?>
                            <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == "record") {?>
                                <i class="fa fa-life-ring fa-lg fa-fw pr10"></i>
                                <?php echo __("Service");?>

                                <i class="fa fa-caret-right fa-fw pr10"></i>
                                <?php echo __("Register service");?>

                            <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "history") {?>
                                <i class="fa fa-life-ring fa-lg fa-fw pr10"></i>
                                <?php echo __("Service");?>

                                <i class="fa fa-caret-right fa-fw pr10"></i>
                                <?php echo __("Service usage information");?>

                            <?php }?>
                        <?php } elseif (($_smarty_tpl->tpl_vars['view']->value == "tuitions") || ($_smarty_tpl->tpl_vars['view']->value == "fees")) {?>
                            <i class="fas fa-money-bill-alt fa-lg fa-fw pr10"></i>
                            <?php echo __("Tuition fee");?>

                        <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "reports") {?>
                            <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
                                <i class="fa fa-book fa-lg fa-fw pr10"></i>
                                <?php echo __("Contact book");?>

                                <i class="fa fa-caret-right fa-fw pr10"></i>
                                <?php echo __("Lists");?>

                            <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
                                <i class="fa fa-book fa-lg fa-fw pr10"></i>
                                <?php echo __("Contact book");?>

                                <i class="fa fa-caret-right fa-fw pr10"></i>
                                <?php echo __("Add New");?>

                            <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "listtemp") {?>
                                <i class="fa fa-book fa-lg fa-fw pr10"></i>
                                <?php echo __("Contact book");?>

                                <i class="fa fa-caret-right fa-fw pr10"></i>
                                <?php echo __("List template");?>

                            <?php }?>
                        <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "points") {?>
                            <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == "lists_by_subject") {?>
                                <i class="fa fa-book fa-lg fa-fw pr10"></i>
                                <?php echo __("Points");?>

                                <i class="fa fa-caret-right fa-fw pr10"></i>
                                <?php echo __("Lists by subject");?>

                            <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "lists_by_student") {?>
                                <i class="fa fa-book fa-lg fa-fw pr10"></i>
                                <?php echo __("Points");?>

                                <i class="fa fa-caret-right fa-fw pr10"></i>
                                <?php echo __("Lists by student");?>

                            <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "importManual") {?>
                                <i class="fa fa-book fa-lg fa-fw pr10"></i>
                                <?php echo __("Points");?>

                                <i class="fa fa-caret-right fa-fw pr10"></i>
                                <?php echo __("Import point");?>

                            <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "comment") {?>
                                <i class="fa fa-book fa-lg fa-fw pr10"></i>
                                <?php echo __("Points");?>

                                <i class="fa fa-caret-right fa-fw pr10"></i>
                                <?php echo __("Comment");?>

                            <?php }?>
                        <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "diarys") {?>
                            <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
                                <i class="fa fa-image fa-lg fa-fw pr10"></i>
                                <?php echo __("Diary corner");?>

                                <i class="fa fa-caret-right fa-fw pr10"></i>
                                <?php echo __("Lists");?>

                            <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
                                <i class="fa fa-image fa-lg fa-fw pr10"></i>
                                <?php echo __("Diary corner");?>

                                <i class="fa fa-caret-right fa-fw pr10"></i>
                                <?php echo __("Add New");?>

                            <?php }?>
                        <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "healths") {?>
                            <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
                                <i class="fa fa-heart fa-lg fa-fw pr10"></i>
                                <?php echo __("Health information");?>

                                <i class="fa fa-caret-right fa-fw pr10"></i>
                                <?php echo __("Lists");?>

                            <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
                                <i class="fa fa-heart fa-lg fa-fw pr10"></i>
                                <?php echo __("Health information");?>

                                <i class="fa fa-caret-right fa-fw pr10"></i>
                                <?php echo __("Add New");?>

                            <?php }?>
                        <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "children") {?>
                            <i class="fa fa-child fa-fw fa-lg pr10"></i>
                            <?php echo __("Student");?>

                            <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
                                <i class="fa fa-caret-right fa-fw pr10"></i>
                                <?php echo __("Lists");?>

                            <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "import") {?>
                                <i class="fa fa-caret-right fa-fw pr10"></i>
                                <?php echo __("Import from Excel file");?>

                            <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
                                <i class="fa fa-caret-right fa-fw pr10"></i>
                                <?php echo __("Add New");?>

                            <?php }?>
                        <?php }?>
                        <!-- end children -->
                        <!-- Event -->
                        <?php if ($_smarty_tpl->tpl_vars['schoolConfig']->value['allow_class_event']) {?>
                            <?php if ($_smarty_tpl->tpl_vars['view']->value == "events" && $_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
                                <i class="fa fa-bell fa-lg fa-fw pr10"></i>
                                <?php echo __("Notification - Event");?>

                                <i class="fa fa-caret-right fa-fw pr10"></i>
                                <?php echo __("Lists");?>

                            <?php }?>
                            
                            
                            
                        <?php } else { ?>
                            <?php if ($_smarty_tpl->tpl_vars['view']->value == "events") {?>
                                <i class="fa fa-bell fa-lg fa-fw pr10"></i>
                                <?php echo __("Notification - Event");?>

                            <?php }?>
                        <?php }?>
                        <!-- end Event -->

                    </div>
                    <div class="navbar-header">

                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <?php if (count($_smarty_tpl->tpl_vars['schoolList']->value) > 0 || count($_smarty_tpl->tpl_vars['classList']->value) > 0 || count($_smarty_tpl->tpl_vars['childrenList']->value) > 0) {?>
                        <div class="select_object_box">
                            <select name="object" id="select_object">
                                <?php if (count($_smarty_tpl->tpl_vars['schoolList']->value) > 0) {?>
                                    <option disabled>----- <?php echo __("School");?>
 -----</option>
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['schoolList']->value, 'school');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['school']->value) {
?>
                                        <option data-type="school" value="<?php echo $_smarty_tpl->tpl_vars['school']->value['page_name'];?>
"
                                                <?php if ($_smarty_tpl->tpl_vars['school']->value['page_name'] == $_smarty_tpl->tpl_vars['username']->value) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['school']->value['page_title'];?>
</option>
                                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                <?php }?>
                                <?php if (count($_smarty_tpl->tpl_vars['classList']->value) > 0) {?>
                                    <option disabled>----- <?php echo __("Class");?>
 -----</option>
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['classList']->value, 'class');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class']->value) {
?>
                                        <option data-type="class" value="<?php echo $_smarty_tpl->tpl_vars['class']->value['group_name'];?>
"
                                                <?php if ($_smarty_tpl->tpl_vars['class']->value['group_name'] == $_smarty_tpl->tpl_vars['username']->value) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['class']->value['group_title'];?>
</option>
                                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>
a
                                <?php }?>
                                <?php if (count($_smarty_tpl->tpl_vars['childrenList']->value) > 0) {?>
                                    <option disabled>----- <?php echo __("Child");?>
 -----</option>
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['childrenList']->value, 'childOb');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['childOb']->value) {
?>
                                        <option <?php if ($_smarty_tpl->tpl_vars['childOb']->value['school_id'] > 0) {?>data-type="child"
                                                value="<?php echo $_smarty_tpl->tpl_vars['childOb']->value['child_id'];?>
"
                                                <?php if ($_smarty_tpl->tpl_vars['childOb']->value['child_id'] == $_smarty_tpl->tpl_vars['child']->value['child_id']) {?>selected<?php }?>
                                                <?php } else { ?>data-type="childinfo"
                                                value="<?php echo $_smarty_tpl->tpl_vars['childOb']->value['child_parent_id'];?>
" <?php if ($_smarty_tpl->tpl_vars['childOb']->value['child_parent_id'] == $_smarty_tpl->tpl_vars['child']->value['child_parent_id']) {?>selected<?php }
}?>><?php echo $_smarty_tpl->tpl_vars['childOb']->value['child_name'];?>
</option>
                                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                <?php }?>
                            </select>
                        </div>
                    <?php }?>
                    <div class="panel-body with-nav collapse navbar-collapse" id="collapse">
                        <ul class="side-nav metismenu js_metisMenu nav navbar-nav navbar-right">
                            <!-- Dashboard -->
                            <li <?php if ($_smarty_tpl->tpl_vars['view']->value == '') {?>class="active selected"<?php }?>>
                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
">
                                    <i class="fas fa-tachometer-alt fa-fw fa-lg pr10"></i> <?php echo __("Dashboard");?>

                                </a>
                            </li>
                            <!-- Dashboard -->
                            
                            <li class="menu_header border_top">
                                <a><?php echo __("Tuition info");?>
</a>
                            </li>
                            <!-- Attendance -->
                            <?php if ($_smarty_tpl->tpl_vars['schoolConfig']->value['attendance']) {?>
                                <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "attendance") {?>class="active"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/attendance">
                                        <i class="fa fa-tasks fa-fw fa-lg pr10"></i> <?php echo __("Attendance");?>

                                        <span class="fa arrow"></span>
                                    </a>
                                    <ul>
                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "attendance" && $_smarty_tpl->tpl_vars['sub_view']->value == '') {?>class="active selected"<?php }?>>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/attendance">
                                                <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Summary");?>

                                            </a>
                                        </li>
                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "attendance" && $_smarty_tpl->tpl_vars['sub_view']->value == "rollup") {?>class="active selected"<?php }?>>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/attendance/rollup">
                                                <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Roll up");?>

                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            <?php }?>
                            <!-- Attendance -->
                            <!-- Service -->
                            <?php if ($_smarty_tpl->tpl_vars['schoolConfig']->value['services']) {?>
                                <li <?php if (($_smarty_tpl->tpl_vars['view']->value == "useservices") || ($_smarty_tpl->tpl_vars['view']->value == "services")) {?>class="active"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/useservices">
                                        <i class="fa fa-life-ring fa-lg fa-fw pr10"></i> <?php echo __("Service");?>

                                        <span class="fa arrow"></span>
                                    </a>
                                    <ul>
                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "useservices" && $_smarty_tpl->tpl_vars['sub_view']->value == "record") {?>class="active selected"<?php }?>>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/useservices/record">
                                                <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Register service");?>

                                            </a>
                                        </li>
                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "useservices" && $_smarty_tpl->tpl_vars['sub_view']->value == "history") {?>class="active selected"<?php }?>>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/useservices/history">
                                                <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Service usage information");?>

                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            <?php }?>
                            <!-- Service -->
                            <!-- Fee -->
                            <?php if ($_smarty_tpl->tpl_vars['schoolConfig']->value['tuitions']) {?>
                                <?php if ($_smarty_tpl->tpl_vars['schoolConfig']->value['allow_class_see_tuition']) {?>
                                    <!-- Fee -->
                                    <li <?php if (($_smarty_tpl->tpl_vars['view']->value == "tuitions") || ($_smarty_tpl->tpl_vars['view']->value == "fees")) {?>class="active"<?php }?>>
                                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/tuitions">
                                            <i class="fas fa-money-bill-alt fa-lg fa-fw pr10"></i> <?php echo __("Tuition fee");?>

                                        </a>
                                    </li>
                                <?php }?>
                            <?php }?>
                            <!-- Fee -->

                            <!-- Late Pickup -->
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            <!-- END Late Pickup -->

                            
                            
                            <li class="menu_header border_top">
                                <a><?php echo __("Information for student");?>
</a>
                            </li>
                            <!-- Schedule -->
                            <?php if ($_smarty_tpl->tpl_vars['schoolConfig']->value['schedules']) {?>
                                <li <?php if (($_smarty_tpl->tpl_vars['view']->value == "schedules")) {?>class="active"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/schedules">
                                        <i class="fas fa-calendar-alt fa-lg fa-fw pr10"></i> <?php echo __("Schedule");?>

                                    </a>
                                </li>
                            <?php }?>
                            <!-- End schedule -->

                            <!-- Menu -->
                            <?php if ($_smarty_tpl->tpl_vars['schoolConfig']->value['menus']) {?>
                                <li <?php if (($_smarty_tpl->tpl_vars['view']->value == "menus")) {?>class="active"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/menus">
                                        <i class="fas fa-utensils fa-lg fa-fw pr10"></i> <?php echo __("Menu");?>

                                    </a>
                                </li>
                            <?php }?>
                            <!-- End Menu -->
                            <!-- Contact book -->
                            <?php if ($_smarty_tpl->tpl_vars['schoolConfig']->value['reports']) {?>
                                <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "reports") {?>class="active"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/reports">
                                        <i class="fa fa-book fa-lg fa-fw pr10"></i> <?php echo __("Contact book");?>

                                        <span class="fa arrow"></span>
                                    </a>
                                    <ul>
                                        <li <?php if (($_smarty_tpl->tpl_vars['view']->value == "reports") && ($_smarty_tpl->tpl_vars['sub_view']->value == '')) {?>class="active selected"<?php }?>>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/reports">
                                                <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Lists");?>

                                            </a>
                                        </li>
                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "reports" && $_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>class="active selected"<?php }?>>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/reports/add">
                                                <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Add New");?>

                                            </a>
                                        </li>
                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "reports" && $_smarty_tpl->tpl_vars['sub_view']->value == "listtemp") {?>class="active selected"<?php }?>>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/reports/listtemp">
                                                <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("List template");?>

                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            <?php }?>
                            <!-- End Contact book -->
                            <!-- Points -->
                            <?php if ($_smarty_tpl->tpl_vars['school']->value['points']) {?>
                                <li id="li-point" <?php if ($_smarty_tpl->tpl_vars['view']->value == "points") {?>class="active<?php }?>">
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/points">
                                        <i class="fa fa-book fa-lg fa-fw pr10"></i> <?php echo __("Point");?>

                                        <span class="fa arrow"></span>
                                    </a>
                                    <ul>
                                        <li id="li-point-list"
                                            <?php if (($_smarty_tpl->tpl_vars['view']->value == "points") && ($_smarty_tpl->tpl_vars['sub_view']->value == "listsbysubject")) {?>class="active selected"<?php }?>>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/points/listsbysubject">
                                                <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Lists by subject");?>

                                            </a>
                                        </li>
                                        <li id="li-point-list"
                                            <?php if (($_smarty_tpl->tpl_vars['view']->value == "points") && ($_smarty_tpl->tpl_vars['sub_view']->value == "listsbystudent")) {?>class="active selected"<?php }?>>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/points/listsbystudent">
                                                <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Lists by student");?>

                                            </a>
                                        </li>
                                        <li id="li-point-assign"
                                            <?php if ($_smarty_tpl->tpl_vars['view']->value == "points" && $_smarty_tpl->tpl_vars['sub_view']->value == "importManual") {?>class="active selected"<?php }?>>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/points/importManual">
                                                <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Import point");?>

                                            </a>
                                        </li>
                                        <li id="li-point-comment"
                                            <?php if ($_smarty_tpl->tpl_vars['view']->value == "points" && $_smarty_tpl->tpl_vars['sub_view']->value == "comment") {?>class="active selected"<?php }?>>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/points/comment">
                                                <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Comment");?>

                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            <?php }?>
                            <!-- End Points -->
                            <!-- Event -->
                            <?php if ($_smarty_tpl->tpl_vars['schoolConfig']->value['events']) {?>
                                <?php if ($_smarty_tpl->tpl_vars['schoolConfig']->value['allow_class_event']) {?>
                                    <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "events") {?>class="active"<?php }?>>
                                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/events">
                                            <i class="fa fa-bell fa-lg fa-fw pr10"></i><?php echo __("Notification - Event");?>

                                        </a>
                                    </li>
                                <?php } else { ?>
                                    <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "events") {?>class="active"<?php }?>>
                                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/events">
                                            <i class="fa fa-bell fa-lg fa-fw pr10"></i><?php echo __("Notification - Event");?>

                                        </a>
                                    </li>
                                <?php }?>
                            <?php }?>
                            <!-- Event -->
                            
                            
                            <li class="menu_header border_top">
                                <a><?php echo __("School utility");?>
</a>
                            </li>
                            
                            <!-- Diary -->
                            <?php if ($_smarty_tpl->tpl_vars['schoolConfig']->value['diarys']) {?>
                                <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "diarys") {?>class="active"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/diarys">
                                        <i class="fa fa-image fa-lg fa-fw pr10"></i> <?php echo __("Diary corner");?>

                                        <span class="fa arrow"></span>
                                    </a>
                                    <ul>
                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "diarys" && $_smarty_tpl->tpl_vars['sub_view']->value == '') {?>class="active selected"<?php }?>>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/diarys">
                                                <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Diary lists");?>

                                            </a>
                                        </li>
                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "diarys" && $_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>class="active selected"<?php }?>>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/diarys/add">
                                                <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Add New");?>

                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            <?php }?>
                            <!-- Diary -->
                            <!-- Health -->
                            <?php if ($_smarty_tpl->tpl_vars['schoolConfig']->value['healths']) {?>
                                <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "healths") {?>class="active"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/healths">
                                        <i class="fa fa-heart fa-lg fa-fw pr10"></i> <?php echo __("Health information");?>

                                        <span class="fa arrow"></span>
                                    </a>
                                    <ul>
                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "healths" && $_smarty_tpl->tpl_vars['sub_view']->value == '') {?>class="active selected"<?php }?>>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/healths">
                                                <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Lists");?>

                                            </a>
                                        </li>
                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "healths" && $_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>class="active selected"<?php }?>>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/healths/add">
                                                <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Add New");?>

                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            <?php }?>

                            <!-- Health -->
                            <!-- Medicine -->
                            <?php if ($_smarty_tpl->tpl_vars['schoolConfig']->value['medicines']) {?>
                                <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "medicines") {?>class="active"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/medicines">
                                        <i class="fa fa-medkit fa-lg fa-fw pr10"></i> <?php echo __("Medicines");?>

                                        <span class="fa arrow"></span>
                                    </a>
                                    <ul>
                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "medicines" && $_smarty_tpl->tpl_vars['sub_view']->value == '') {?>class="active selected"<?php }?>>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/medicines">
                                                <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Today lists");?>

                                            </a>
                                        </li>
                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "medicines" && $_smarty_tpl->tpl_vars['sub_view']->value == "all") {?>class="active selected"<?php }?>>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/medicines/all">
                                                <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("List all");?>

                                            </a>
                                        </li>
                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "medicines" && $_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>class="active selected"<?php }?>>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/medicines/add">
                                                <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Add New");?>

                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            <?php }?>
                            <!-- Medicine -->

                            <!-- BEGIN - Children -->
                            <?php if ($_smarty_tpl->tpl_vars['schoolConfig']->value['children']) {?>
                                <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "children") {?>class="active"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/children">
                                        <i class="fa fa-child fa-fw fa-lg pr10"></i><?php echo __("Student");?>

                                        <span class="fa arrow"></span>
                                    </a>
                                    <ul>
                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "children" && $_smarty_tpl->tpl_vars['sub_view']->value == '') {?>class="active selected"<?php }?>>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/children">
                                                <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Lists");?>

                                            </a>
                                        </li>
                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "children" && $_smarty_tpl->tpl_vars['sub_view']->value == "import") {?>class="active selected"<?php }?>>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/children/import">
                                                <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Import from Excel file");?>

                                            </a>
                                        </li>
                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "children" && $_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>class="active selected"<?php }?>>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/children/add">
                                                <i class="fa fa-caret-right fa-fw pr10"></i><?php echo __("Add New");?>

                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            <?php }?>
                            <!-- END - Children -->
                            <!-- Reports -->
                            <!--
                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "reports") {?>class="active selected"<?php }?>>
                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/reports">
                                <i class="fa fa-pie-chart fa-lg pr10"></i> <?php echo __("Reports");?>

                            </a>
                        </li>
                        -->
                            <!-- END - Reports -->
                            <!-- BEGIN - Feedback -->
                            <!--
                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "feedback") {?>class="active"<?php }?>>
                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/feedback">
                                <i class="fa fa-envelope fa-fw fa-lg pr10"></i> <?php echo __("Feedback");?>

                            </a>
                        </li>
                        -->
                            <!-- END - Feedback -->
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
        <?php $_smarty_tpl->_subTemplateRender('file:ci/class/class.help.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

        <div class="<?php if ($_smarty_tpl->tpl_vars['view']->value == 'attendance' && $_smarty_tpl->tpl_vars['sub_view']->value == '') {?>col-sm-10 col-md-10 <?php } else { ?> col-sm-9 col-md-9<?php }?>">
            <?php if ($_smarty_tpl->tpl_vars['view']->value == '') {?>
                <?php $_smarty_tpl->_subTemplateRender('file:ci/class/class.dashboard.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

            <?php } else { ?>
                <?php $_smarty_tpl->_subTemplateRender("file:ci/class/class.".((string)$_smarty_tpl->tpl_vars['view']->value).".tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

            <?php }?>
        </div>
    </div>
</div>
<!-- page content -->


<?php $_smarty_tpl->_subTemplateRender('file:_footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
