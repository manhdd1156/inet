<?php
/* Smarty version 3.1.31, created on 2021-05-20 17:01:15
  from "D:\workplace\Inet-project\source\Server11\content\themes\inet\templates\ci\noga\noga.dashboard.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_60a6336b8b4c76_97625738',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '03c18692ca3d2c4f0edfe593bf7f11e0e8e00660' => 
    array (
      0 => 'D:\\workplace\\Inet-project\\source\\Server11\\content\\themes\\inet\\templates\\ci\\noga\\noga.dashboard.tpl',
      1 => 1621501238,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60a6336b8b4c76_97625738 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <i class="fas fa-tachometer-alt fa-fw fa-lg pr10"></i>
        <?php echo __("Dashboard");?>

    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-6">
                <div class="box-primary">
                    <div class="box-header">
                        <strong><?php echo __("School list");?>
</strong>
                    </div>
                    <div class="list-group">
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['insights']->value['schools'], 'school');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['school']->value) {
?>
                            <div class="list-group-item">
                                <?php echo $_smarty_tpl->tpl_vars['school']->value['page_title'];?>

                            </div>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                    </div>
                </div>

            </div>
        </div>
    </div>
</div><?php }
}
