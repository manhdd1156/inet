<?php
/* Smarty version 3.1.31, created on 2021-03-31 10:36:24
  from "D:\workplace\Server11\content\themes\inet\templates\ci\school\ajax.service.history.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_6063ee3847d803_17449734',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'd97bd82c34cb2b16daf5923d07bf6450860c55e5' => 
    array (
      0 => 'D:\\workplace\\Server11\\content\\themes\\inet\\templates\\ci\\school\\ajax.service.history.tpl',
      1 => 1552404702,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6063ee3847d803_17449734 (Smarty_Internal_Template $_smarty_tpl) {
if ((($_smarty_tpl->tpl_vars['service_id']->value > 0) || ($_smarty_tpl->tpl_vars['child_id']->value > 0))) {?>
    <?php if ($_smarty_tpl->tpl_vars['service_type']->value == @constant('SERVICE_TYPE_COUNT_BASED')) {?>
        <div><strong><?php echo __("Usage history");?>
&nbsp;(<?php echo count($_smarty_tpl->tpl_vars['history']->value);?>
 <?php echo __("times");?>
)</strong></div>
        <table class="table table-striped table-bordered table-hover">
            <?php if ((isset($_smarty_tpl->tpl_vars['child_id']->value) && ($_smarty_tpl->tpl_vars['child_id']->value > 0))) {?>
                
                <thead>
                    <tr>
                        <th>#</th>
                        <th><?php echo __("Using time");?>
</th>
                        <th><?php echo __("Registrar");?>
</th>
                        <th><?php echo __("Registered time");?>
</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['history']->value, 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
                        <tr>
                            <td align="center"><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</td>
                            <td><?php echo $_smarty_tpl->tpl_vars['row']->value['using_at'];?>
</td>
                            <td><?php echo $_smarty_tpl->tpl_vars['row']->value['user_fullname'];?>
</td>
                            <td><?php echo $_smarty_tpl->tpl_vars['row']->value['recorded_at'];?>
</td>
                        </tr>
                        <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                    <?php if ($_smarty_tpl->tpl_vars['idx']->value > 2) {?>
                        <tr>
                            <td colspan="2" align="center"><strong><font color="red"><?php echo mb_strtoupper(__("Total"), 'UTF-8');?>
</font></strong></td>
                            <td colspan="2" align="left"><strong><font color="red"><?php echo $_smarty_tpl->tpl_vars['idx']->value-1;?>
 <?php echo __("times");?>
</font></strong></td>
                        </tr>
                    <?php }?>
                    <?php if (count($_smarty_tpl->tpl_vars['history']->value) == 0) {?>
                        <tr class="odd">
                            <td valign="top" align="center" colspan="4" class="dataTables_empty">
                                <?php echo __("No data available in table");?>

                            </td>
                        </tr>
                    <?php }?>
                </tbody>
            <?php } elseif ((isset($_smarty_tpl->tpl_vars['class_id']->value) && ($_smarty_tpl->tpl_vars['class_id']->value > 0))) {?>
                
                <thead>
                    <tr>
                        <th>#</th>
                        <th><?php echo __("Full name");?>
</th>
                        <th><?php echo __("Registrar");?>
</th>
                        <th><?php echo __("Registered time");?>
</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                    <?php $_smarty_tpl->_assignInScope('dayTotal', 0);
?>
                    <?php $_smarty_tpl->_assignInScope('usingAt', 'noga');
?>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['history']->value, 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
                        <?php if ($_smarty_tpl->tpl_vars['row']->value['using_at'] != $_smarty_tpl->tpl_vars['usingAt']->value) {?>
                            <?php if ($_smarty_tpl->tpl_vars['idx']->value > 1) {?>
                                <tr>
                                    <td colspan="2" align="center"><strong><font color="blue"><?php echo __("Total of day");?>
</font></strong></td>
                                    <td colspan="2" align="left"><strong><font color="blue"><?php echo $_smarty_tpl->tpl_vars['dayTotal']->value;?>
 <?php echo __("times");?>
</font></strong></td>
                                </tr>
                            <?php }?>
                            <tr>
                                <td colspan="4"><strong><?php echo $_smarty_tpl->tpl_vars['row']->value['using_at'];?>
</strong></</td>
                            </tr>
                            <?php $_smarty_tpl->_assignInScope('dayTotal', 0);
?>
                        <?php }?>
                        <tr>
                            <td align="center"><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</td>
                            <td><?php echo $_smarty_tpl->tpl_vars['row']->value['child_name'];?>
</td>
                            <td><?php echo $_smarty_tpl->tpl_vars['row']->value['user_fullname'];?>
</td>
                            <td><?php echo $_smarty_tpl->tpl_vars['row']->value['recorded_at'];?>
</td>
                        </tr>
                        <?php $_smarty_tpl->_assignInScope('usingAt', $_smarty_tpl->tpl_vars['row']->value['using_at']);
?>
                        <?php $_smarty_tpl->_assignInScope('dayTotal', $_smarty_tpl->tpl_vars['dayTotal']->value+1);
?>
                        <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                    <?php if ($_smarty_tpl->tpl_vars['idx']->value > 2) {?>
                        <tr>
                            <td colspan="2" align="center"><strong><font color="blue"><?php echo __("Total of day");?>
</font></strong></td>
                            <td colspan="2" align="left"><strong><font color="blue"><?php echo $_smarty_tpl->tpl_vars['dayTotal']->value;?>
 <?php echo __("times");?>
</font></strong></td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center"><strong><font color="red"><?php echo mb_strtoupper(__("Total"), 'UTF-8');?>
</font></strong></td>
                            <td colspan="2" align="left"><strong><font color="red"><?php echo $_smarty_tpl->tpl_vars['idx']->value-1;?>
 <?php echo __("times");?>
</font></strong></td>
                        </tr>
                    <?php }?>
                    <?php if (count($_smarty_tpl->tpl_vars['history']->value) == 0) {?>
                        <tr class="odd">
                            <td valign="top" align="center" colspan="4" class="dataTables_empty">
                                <?php echo __("No data available in table");?>

                            </td>
                        </tr>
                    <?php }?>
                </tbody>
            <?php } else { ?>
                
                <thead>
                <tr>
                    <th>#</th>
                    <th><?php echo __("Full name");?>
</th>
                    <th><?php echo __("Using time");?>
</th>
                    <th><?php echo __("Registrar");?>
</th>
                    <th><?php echo __("Registered time");?>
</th>
                </tr>
                </thead>
                <tbody>
                <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                <?php $_smarty_tpl->_assignInScope('classTotal', 0);
?>
                <?php $_smarty_tpl->_assignInScope('classId', -1);
?>
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['history']->value, 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
                    <?php if ($_smarty_tpl->tpl_vars['row']->value['class_id'] != $_smarty_tpl->tpl_vars['classId']->value) {?>
                        <?php if ($_smarty_tpl->tpl_vars['idx']->value > 1) {?>
                            <tr>
                                <td colspan="2" align="center"><strong><font color="blue"><?php echo __("Total of class");?>
</font></strong></td>
                                <td colspan="3" align="left"><strong><font color="blue"><?php echo $_smarty_tpl->tpl_vars['classTotal']->value;?>
 <?php echo __("times");?>
</font></strong></td>
                            </tr>
                        <?php }?>
                        <tr>
                            <td colspan="4"><strong><?php echo $_smarty_tpl->tpl_vars['row']->value['group_title'];?>
</strong></</td>
                        </tr>
                        <?php $_smarty_tpl->_assignInScope('classTotal', 0);
?>
                    <?php }?>
                    <tr>
                        <td align="center"><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</td>
                        <td><?php echo $_smarty_tpl->tpl_vars['row']->value['child_name'];?>
</td>
                        <td><?php echo $_smarty_tpl->tpl_vars['row']->value['using_at'];?>
</td>
                        <td><?php echo $_smarty_tpl->tpl_vars['row']->value['user_fullname'];?>
</td>
                        <td><?php echo $_smarty_tpl->tpl_vars['row']->value['recorded_at'];?>
</td>
                    </tr>
                    <?php $_smarty_tpl->_assignInScope('classId', $_smarty_tpl->tpl_vars['row']->value['class_id']);
?>
                    <?php $_smarty_tpl->_assignInScope('classTotal', $_smarty_tpl->tpl_vars['classTotal']->value+1);
?>
                    <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                <?php if ($_smarty_tpl->tpl_vars['idx']->value > 2) {?>
                    <tr>
                        <td colspan="2" align="center"><strong><font color="blue"><?php echo __("Total of class");?>
</font></strong></td>
                        <td colspan="3" align="left"><strong><font color="blue"><?php echo $_smarty_tpl->tpl_vars['classTotal']->value;?>
 <?php echo __("times");?>
</font></strong></td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center"><strong><font color="red"><?php echo mb_strtoupper(__("Total"), 'UTF-8');?>
</font></strong></td>
                        <td colspan="3" align="left"><strong><font color="red"><?php echo $_smarty_tpl->tpl_vars['idx']->value-1;?>
 <?php echo __("times");?>
</font></strong></td>
                    </tr>
                <?php }?>
                <?php if (count($_smarty_tpl->tpl_vars['history']->value) == 0) {?>
                    <tr class="odd">
                        <td valign="top" align="center" colspan="5" class="dataTables_empty">
                            <?php echo __("No data available in table");?>

                        </td>
                    </tr>
                <?php }?>
                </tbody>
            <?php }?>
        </table>
    <?php } elseif ($_smarty_tpl->tpl_vars['service_type']->value > 0) {?>
        <div><strong><?php echo __("Children list");?>
&nbsp;(<?php echo __("Register");?>
: <?php echo count($_smarty_tpl->tpl_vars['history']->value);?>
)</strong></div>
        <table class="table table-striped table-bordered table-hover">
            <thead>
            <tr>
                <th>#</th>
                <th><?php echo __("Full name");?>
</th>
                <th><?php echo __("Child code");?>
</th>
                <th><?php echo __("Registration time");?>
</th>
            </tr>
            </thead>
            <tbody>
            <?php $_smarty_tpl->_assignInScope('classId', -1);
?>
            <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['history']->value, 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
                <?php if ((($_smarty_tpl->tpl_vars['class_id']->value == 0) && ($_smarty_tpl->tpl_vars['classId']->value != $_smarty_tpl->tpl_vars['row']->value['class_id']))) {?>
                    <tr>
                        <td colspan="4"><strong><?php echo $_smarty_tpl->tpl_vars['row']->value['group_title'];?>
</strong></td>
                    </tr>
                <?php }?>
                <tr>
                    <td align="center"><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</td>
                    <td><?php echo $_smarty_tpl->tpl_vars['row']->value['child_name'];?>
</td>
                    <td><?php echo $_smarty_tpl->tpl_vars['row']->value['child_code'];?>
</td>
                    <td>
                        <?php if ($_smarty_tpl->tpl_vars['row']->value['begin'] != '') {?>
                            <?php echo $_smarty_tpl->tpl_vars['row']->value['begin'];?>
&nbsp;-&nbsp;<?php echo $_smarty_tpl->tpl_vars['row']->value['end'];?>

                        <?php }?>
                    </td>
                </tr>
                <?php $_smarty_tpl->_assignInScope('classId', $_smarty_tpl->tpl_vars['row']->value['class_id']);
?>
                <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

            <?php if (count($_smarty_tpl->tpl_vars['history']->value) == 0) {?>
                <tr class="odd">
                    <td valign="top" align="center" colspan="4" class="dataTables_empty">
                        <?php echo __("No data available in table");?>

                    </td>
                </tr>
            <?php }?>
            </tbody>
        </table>
    <?php } else { ?>
        
        
        <div><strong><?php echo mb_strtoupper(__("Monthly and daily services"), 'UTF-8');?>
</strong></div>
        <table class="table table-striped table-bordered table-hover">
            <thead>
            <tr>
                <th>#</th>
                <th><?php echo __("Service name");?>
</th>
                <th><?php echo __("Time");?>
</th>
            </tr>
            </thead>
            <tbody>
            <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['ncbs_usage']->value, 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
                <tr>
                    <td align="center"><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</td>
                    <td><?php echo $_smarty_tpl->tpl_vars['row']->value['service_name'];?>
</td>
                    <td>
                        <?php if ($_smarty_tpl->tpl_vars['row']->value['begin'] != '') {?>
                            <?php echo $_smarty_tpl->tpl_vars['row']->value['begin'];?>
&nbsp;-&nbsp;<?php echo $_smarty_tpl->tpl_vars['row']->value['end'];?>

                        <?php }?>
                    </td>
                </tr>
                <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

            <?php if (count($_smarty_tpl->tpl_vars['ncbs_usage']->value) == 0) {?>
                <tr class="odd">
                    <td valign="top" align="center" colspan="3" class="dataTables_empty">
                        <?php echo __("No data available in table");?>

                    </td>
                </tr>
            <?php }?>
            </tbody>
        </table>

        
        <div><strong><?php echo mb_strtoupper(__("Count-based service"), 'UTF-8');?>
&nbsp;(<?php echo count($_smarty_tpl->tpl_vars['cbs_usage']->value);?>
 <?php echo __("times");?>
)</strong></div>
        <table class="table table-striped table-bordered table-hover">
            <thead>
            <tr>
                <th>#</th>
                <th><?php echo __("Using time");?>
</th>
                <th><?php echo __("Registrar");?>
</th>
                <th><?php echo __("Registered time");?>
</th>
            </tr>
            </thead>
            <tbody>
            <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
            <?php $_smarty_tpl->_assignInScope('serviceTotal', 0);
?>
            <?php $_smarty_tpl->_assignInScope('serviceId', -1);
?>
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['cbs_usage']->value, 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
                <?php if ($_smarty_tpl->tpl_vars['row']->value['service_id'] != $_smarty_tpl->tpl_vars['serviceId']->value) {?>
                    <?php if ($_smarty_tpl->tpl_vars['idx']->value > 1) {?>
                        <tr>
                            <td colspan="2" align="center"><strong><font color="blue"><?php echo __("Total of service");?>
</font></strong></td>
                            <td colspan="2" align="left"><strong><font color="blue"><?php echo $_smarty_tpl->tpl_vars['serviceTotal']->value;?>
 <?php echo __("times");?>
</font></strong></td>
                        </tr>
                    <?php }?>
                    <tr>
                        <td colspan="4"><strong><?php echo $_smarty_tpl->tpl_vars['row']->value['service_name'];?>
</strong></</td>
                    </tr>
                    <?php $_smarty_tpl->_assignInScope('serviceTotal', 0);
?>
                <?php }?>
                <tr>
                    <td align="center"><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</td>
                    <td><?php echo $_smarty_tpl->tpl_vars['row']->value['using_at'];?>
</td>
                    <td><?php echo $_smarty_tpl->tpl_vars['row']->value['user_fullname'];?>
</td>
                    <td><?php echo $_smarty_tpl->tpl_vars['row']->value['recorded_at'];?>
</td>
                </tr>
                <?php $_smarty_tpl->_assignInScope('serviceId', $_smarty_tpl->tpl_vars['row']->value['service_id']);
?>
                <?php $_smarty_tpl->_assignInScope('serviceTotal', $_smarty_tpl->tpl_vars['serviceTotal']->value+1);
?>
                <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

            <?php if ($_smarty_tpl->tpl_vars['idx']->value > 2) {?>
                <tr>
                    <td colspan="2" align="center"><strong><font color="blue"><?php echo __("Total of service");?>
</font></strong></td>
                    <td colspan="2" align="left"><strong><font color="blue"><?php echo $_smarty_tpl->tpl_vars['serviceTotal']->value;?>
 <?php echo __("times");?>
</font></strong></td>
                </tr>
                <tr>
                    <td colspan="2" align="center"><strong><font color="red"><?php echo mb_strtoupper(__("Total"), 'UTF-8');?>
</font></strong></td>
                    <td colspan="2" align="left"><strong><font color="red"><?php echo $_smarty_tpl->tpl_vars['idx']->value-1;?>
 <?php echo __("times");?>
</font></strong></td>
                </tr>
            <?php }?>
            <?php if (count($_smarty_tpl->tpl_vars['cbs_usage']->value) == 0) {?>
                <tr class="odd">
                    <td valign="top" align="center" colspan="4" class="dataTables_empty">
                        <?php echo __("No data available in table");?>

                    </td>
                </tr>
            <?php }?>
            </tbody>
        </table>
    <?php }
} else { ?>
    <div><strong><?php echo __("Summary of service registration");?>
</strong></div>
    <table class="table table-striped table-bordered table-hover">
        <thead>
        <tr>
            <th>#</th>
            <th><?php echo __("Service");?>
</th>
            <th><?php echo __("Total");?>
</th>
        </tr>
        </thead>
        <tbody>
        <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['history']->value, 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
            <tr>
                <td align="center"><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</td>
                <td><?php echo $_smarty_tpl->tpl_vars['row']->value['service_name'];?>
</td>
                <td align="center"><?php echo $_smarty_tpl->tpl_vars['row']->value['cnt'];?>
</td>
            </tr>
            <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

        <?php if (count($_smarty_tpl->tpl_vars['history']->value) == 0) {?>
            <tr class="odd">
                <td valign="top" align="center" colspan="3" class="dataTables_empty">
                    <?php echo __("No data available in table");?>

                </td>
            </tr>
        <?php }?>
        </tbody>
    </table>
<?php }
}
}
