<?php
/* Smarty version 3.1.31, created on 2021-05-14 14:42:07
  from "D:\workplace\inet-project\Server11\content\themes\inet\templates\homepage.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_609e29cfc2f810_34188504',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'd8b9243c28c9aeb0f143562dd25fbab46ad7ae58' => 
    array (
      0 => 'D:\\workplace\\mascom-edu-server\\Server11\\content\\themes\\inet\\templates\\homepage.tpl',
      1 => 1620977965,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:_head_new.tpl' => 1,
    'file:_header_new.tpl' => 1,
    'file:_footer_new.tpl' => 1,
  ),
),false)) {
function content_609e29cfc2f810_34188504 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:_head_new.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php $_smarty_tpl->_subTemplateRender('file:_header_new.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<div id="top"></div>
<div class="soicial_left_box">
    <div class="soicial_normal soi_face" align="center"><a href="https://facebook.com/mobiedu.vn" target="_blank"><i
                    class="fa fa-facebook-f"></i></a></div>
    <div class="soicial_normal soi_goo" align="center"><a href="mobiedu@mobifone.vn"
                                                          target="_blank"><i class="fa fa-google"></i></a></div>
    <div class="soicial_normal soi_twi" align="center"><a href="#" target="_blank"><i
                    class="fa fa-twitter"></i></a></div>
</div>
<div class="box-video">
    <div class="video">
        <a href="#" class="register_close"><i class="fa fa-close"></i></a>
        <iframe class="myVideo" width="100%" height="450"
                src="https://www.youtube.com/embed/xUEI4tcYo7w?rel=0&amp;controls=0&amp;autoplay=0" frameborder="0"
                allowfullscreen=""></iframe>
    </div>
</div>
<div class="box_reg_school">
    <div class="reg_school">
        <a href="#" class="register_school_close"><i class="fa fa-close"></i></a>
        <div class="contact_form">
            <div class align="center">
                <h3><b><?php echo mb_strtoupper(__("FREE register for your school"), 'UTF-8');?>
</b></h3>
            </div>
            <form class="js_ajax-forms" data-url="ci/bo/school/bo_contact.php" action="_email">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="control-group">
                            <div class="control" style="margin-top: 5px">
                                <input type="text" id="name2" name="name2"
                                       placeholder="<?php echo __('School name/Your name');?>
 (*)" class="form-control"
                                       required maxlength="255">
                            </div>
                        </div>
                        <div class="row">
                            <div class="control-group col-sm-6">
                                <div class="control">
                                    <input type="email" class="form-control" name="email2" placeholder="Email (*)"
                                           required maxlength="50">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="control-group col-sm-6">
                                <div class="control">
                                    <input type="text" id="phone_contact" name="phone2"
                                           placeholder="<?php echo __('Telephone');?>
 (*)" class="form-control" required/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="control-group">
                            <textarea rows="4" id="content_contact" name="content2"
                                      placeholder="<?php echo __('Please enter all information, Inet staff will contact you soon. Note: Only correct school information will be supported.');?>
 (*)"
                                      class="form-control content_class"
                                      required></textarea>
                        </div>
                    </div>
                    <div class="col-xs-12" align="center">
                        <a class="register_term" href="#"><?php echo __("Terms and conditions for FREE registration");?>
</a></span>

                    </div>
                </div>
                <?php if ($_smarty_tpl->tpl_vars['system']->value['reCAPTCHA_enabled']) {?>
                    <div id="recaptcha2"></div>
                <?php }?>

                <div class="control-ci" align="center">
                    <button type="submit" class="btn btn-success"
                            style="width: 50%"><?php echo mb_strtoupper(__("FREE register for your school"), 'UTF-8');?>
</button>
                </div>
                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->
                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    </div>
</div>
<div class="login_overlay"></div>
<div class="home_wrapper">
    <div class="banner_img">
        <img class="img-responsive img_hd"
             src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/content/themes/<?php echo $_smarty_tpl->tpl_vars['system']->value['theme'];?>
/images/banner.jpg"
             alt="Phần mềm quản lý mầm non Inet">
        <img class="img-responsive img_fullhd"
             src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/content/themes/<?php echo $_smarty_tpl->tpl_vars['system']->value['theme'];?>
/images/banner_full_hd.jpg"
             alt="Phần mềm quản lý mầm non Inet">
        <div class="banner-content">
            <div class="container">
                <div class="row row_content">
                    <div class="col-sm-4 banner_right_col" style="float:right">
                        <div class="banner_log">
                            <div class="banner_signin panel-body <?php if ($_smarty_tpl->tpl_vars['do']->value == 'up') {?>x-hidden<?php }?>">
                                <h3 class="mb20" align="left"><?php echo __("Sign in");?>
</h3>
                                <div class="login-con">
                                    <div class="signin_mobile">
                                        <form id="login_success" class="js_ajax_not_modal-forms" method="POST" data-url="core/verify_phone.php">
                                            <input id="csrf" type="hidden" name="csrf" />
                                            <input id="code" type="hidden" name="code" />
                                        </form>

                                        <form class="js_ajax_not_modal-forms" data-url="core/signin.php">
                                            <div class="form-group">
                                                <input type="text" id="email" name="username_email"
                                                        
                                                       placeholder='<?php echo __("Email");?>
' class="form-control" required>
                                            </div>
                                            <div class="form-group">
                                                <input name="password" id="password" type="password"
                                                       class="form-control" placeholder='<?php echo __("Password");?>
' required>
                                            </div>

                                            <div class="row">
                                                <div class="signin_text">
                                                    <div class="col-xs-6">
                                                        <div class="check-box">
                                                            <div class="checkbox">
                                                                <label><input style="margin-top: 2px;"
                                                                              type="checkbox" id="remember"
                                                                              name="remember"><?php echo __("Remember me");?>

                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-6" align="right">
                                                        <div class="q_password">
                                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/reset"><?php echo __("Forget your password?");?>
</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div align="center">
                                                        <div class="control-ci">
                                                            <button style="width: 100%" type="submit"
                                                                    class="btn btn-primary but_signin"> <?php echo __("Sign in");?>
 </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- error -->
                                            <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                                            <!-- error -->

                                        </form>
                                        <?php if ($_smarty_tpl->tpl_vars['system']->value['social_login_enabled']) {?>
                                            <div class="hr-heading mt15 mb10">
                                                <div class="hr-heading-text">
                                                    <?php echo __("Or");?>
 <?php echo __("login with");?>

                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <a onclick="smsLogin();" class="btn btn-block btn-social btn-success">
                                                    <i class=" fa fas fa-mobile"></i> <strong><?php echo mb_strtoupper(__("Sign in with Facebook"), 'UTF-8');?>
</strong>
                                                </a>
                                            </div>
                                            <?php if ($_smarty_tpl->tpl_vars['system']->value['facebook_login_enabled'] || $_smarty_tpl->tpl_vars['system']->value['google_login_enabled']) {?>
                                                <div class="login_soicial">
                                                    <div class="form-group">
                                                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/connect/facebook" class="btn btn-block btn-social btn-facebook">
                                                            <i class="fa fa-facebook-f"></i> <strong><?php echo mb_strtoupper(__("Sign in with Facebook"), 'UTF-8');?>
</strong>
                                                        </a>
                                                    </div>
                                                    
                                                        
                                                            
                                                        
                                                    
                                                </div>
                                            <?php }?>
                                        <?php }?>
                                        <?php if ($_smarty_tpl->tpl_vars['system']->value['registration_enabled'] || (!$_smarty_tpl->tpl_vars['system']->value['registration_enabled'] && $_smarty_tpl->tpl_vars['system']->value['invitation_enabled'])) {?>
                                            
                                                
                                            
                                            <div class="login_soicial">
                                                <div class="form-group">
                                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/signup"
                                                       class="js_toggle-panel btn btn-block btn-social btn-google"><i class="fa fa-user" aria-hidden="true"></i> <strong><?php echo mb_strtoupper(__("Register"), 'UTF-8');?>
</strong></a>
                                                </div>
                                            </div>
                                        <?php }?>

                                    </div>

                                </div>
                            </div>
                            <div class="banner_signup panel-body <?php if ($_smarty_tpl->tpl_vars['do']->value != 'up') {?>x-hidden<?php }?>" align="left">
                                <h3><?php echo __("Create a new account");?>
</h3>
                                <div class="signup">
                                    
                                        
                                        
                                    <form class="js_ajax_not_modal-forms" data-url="core/signup.php">

                                        <div class="name_taila">
                                            <div class="row row_dk">
                                                <div class="col-xs-6 col_dk" style="padding-right: 7.5px">
                                                    <div class="control-group">
                                                        <div class="controls">
                                                            <input type="text" id="last_name" name="last_name"
                                                                   placeholder='<?php echo __("Last name");?>
' class="form-control"
                                                                   required/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-6 col_dk" style="padding-left: 7.5px">
                                                    <div class="control-group">
                                                        <div class="controls">
                                                            <input type="text" id="first_name" name="first_name"
                                                                   placeholder='<?php echo __("First name");?>
' class="form-control"
                                                                   required/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="name_taila">
                                            <div class="row row_dk">
                                                <div class="col-xs-8 col_dk" style="padding-right: 7.5px">
                                                    <div class="control-group">
                                                        <div class="controls">
                                                            <input type="text" id="email" name="email"
                                                                    
                                                                   placeholder='<?php echo __("Email");?>
' class="form-control"
                                                                   required/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-4 col_dk" style="padding-left: 7.5px">
                                                    <div class="control-group">
                                                        <div class="controls">
                                                            <select name="gender" id="gender" class="form-control"
                                                                    required>
                                                                <option value="female"><?php echo __("Female");?>
</option>
                                                                <option value="male"><?php echo __("Male");?>
</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="control-group">
                                                    <div class="controls">
                                                        <input type="password" id="password" name="password"
                                                               placeholder='<?php echo __("Password");?>
' class="form-control"
                                                               required/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <?php if ($_smarty_tpl->tpl_vars['system']->value['reCAPTCHA_enabled']) {?>
                                            <div id="recaptcha1"></div>
                                        <?php }?>

                                        <div class="dieukhoan">
                                            <p><?php echo __("Sign Up, you agree to our");?>
 <a
                                                        href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/terms"><?php echo __("Terms");?>
</a></p>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="control-group  mar0">
                                                    <div class="controls-ci">
                                                        <button type="submit" class="btn btn-success"
                                                                style="width: 100%"><?php echo __("Sign Up");?>

                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- error -->
                                        <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                                        <!-- error -->
                                    </form>

                                    <div class="mt20 text-center"><?php echo __("Have an account?");?>

                                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/signin"
                                           class="js_toggle-panel text-link"><?php echo __("Login Now");?>
</a>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="banner-trial" align="center">
                            <h4>
                                <b><?php echo mb_strtoupper(__("Free download"), 'UTF-8');?>
</b>
                            </h4>
                        </div>
                        <div class="banner_store row" align="center">
                            <div class="google col-sm-6" align="right">

                                <a id="android_banner" href="#" target="_blank"> <img
                                            class="img-responsive google_img"
                                            src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/content/themes/<?php echo $_smarty_tpl->tpl_vars['system']->value['theme'];?>
/images/img/google.png"
                                            alt="Phần mềm mầm non Inet trên CH Play"/> </a>

                                <a id="ios_banner" href="#" target="_blank"><img
                                            class="img-responsive ios_img"
                                            src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/content/themes/<?php echo $_smarty_tpl->tpl_vars['system']->value['theme'];?>
/images/img/ios.png"
                                            alt="Phần mềm mầm non Inet trên App store"/> </a>
                            </div>
                            <div class="qr_code col-sm-6" align="left">
                                <div class="qr_code_box">
                                    
                                    
                                    
                                    
                                    <h5><?php echo __("Outstanding advantages");?>
</h5>
                                    <p><i class="fa fa-check"></i><?php echo __("No setup fee");?>
</p>
                                    <p><i class="fa fa-check"></i><?php echo __("Multifunction");?>
</p>
                                    <p><i class="fa fa-check"></i><?php echo __("24/7 support");?>
</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="home_background">
        <div class="container">
            <div class="home_intro">
                <div class="intro_title" align="center">
                    <h2><?php echo __("Newspapers talk about MobiEdu");?>
</h2>

                </div>
                <div class="intro_content">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="news_box">
                                <div class="news_img_box">
                                    <img class="img-responsive news_img"
                                         src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/content/themes/<?php echo $_smarty_tpl->tpl_vars['system']->value['theme'];?>
/images/home_new/bao1.png"

                                         alt="<?php echo __("Newspapers talk about MobiEdu's software");?>
">
                                    <div class="img_mark"></div>

                                    <div class="news_name"><p><?php echo __("educational newspaper");?>
</p></div>
                                    <div class="news_name_add"></div>
                                </div>
                                <div class="news_content_box">
                                    
                                    
                                    
                                    <div class="news_title">
                                        <a href="https://www.familyminded.com/s/best-high-school-campuses-6a146788b036480d"
                                           target="_blank" title="Most Beautiful High School Campuses in the U.S">Most Beautiful High School Campuses in the U.S</a>
                                    </div>
                                    <div class="news_shortdes">
                                        <p>Every community has a beloved high school, but not every community turns those high schools into works of art.
                                            Yet across the United States, some high schools have become more than a place to get a diploma... <a target="_blank"
                                                              title="Most Beautiful High School Campuses in the U.S"
                                                              href="https://www.familyminded.com/s/best-high-school-campuses-6a146788b036480d"><?php echo __("See more");?>
</a></p>
                                    </div>
                                    
                                    
                                    
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="news_box">
                                <div class="news_img_box">
                                    <img class="img-responsive news_img"
                                         src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/content/themes/<?php echo $_smarty_tpl->tpl_vars['system']->value['theme'];?>
/images/home_new/bao2.png"
                                         alt="Tech in your child’s middle or high school">
                                    <div class="img_mark"></div>
                                    <div class="news_name"><p><?php echo __("Science & technology");?>
</p></div>
                                    <div class="news_name_add"></div>
                                </div>
                                <div class="news_content_box">
                                    
                                    
                                    
                                    <div class="news_title">
                                        <a href="https://www.greatschools.org/gk/articles/how-technology-is-used-in-middle-high-school-classrooms/"
                                           target="_blank" title="Tech in your child’s middle or high school: what to look for, what to ask">Tech in your child’s middle or high school: what to look for, what to ask</a>
                                    </div>
                                    <div class="news_shortdes">
                                        <p>Kids are using everything from iPads and laptops to smartphones and editing programs at school. But how can you tell if all this "ed tech" is actually helping your child learn… <a target="_blank"
                                                                                 title="Tech in your child’s middle or high school: what to look for, what to ask"
                                                                                 href="https://www.greatschools.org/gk/articles/how-technology-is-used-in-middle-high-school-classrooms/"><?php echo __("See more");?>
</a></p>
                                    </div>
                                    
                                    
                                    
                                </div>
                            </div>
                        </div>


























                    </div>
                </div>
                
                
                
                <div class="content_trial" align="center" style="margin-top: 30px">
                    <a href="#" class="btn btn-success js_free-trial"><?php echo mb_strtoupper(__("FREE register for your school"), 'UTF-8');?>
</a>
                </div>
            </div>
        </div>
    </div>
    <div class="home_background_none">
        <div class="container">
            <div class="home_intro">
                <div class="intro_title" align="center">
                    <h2><?php echo __("Inet - super connection between parents - school - community");?>
</h2>
                </div>
                <div class="home_around">
                    <img class="img-responsive img_around"
                         src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/content/themes/<?php echo $_smarty_tpl->tpl_vars['system']->value['theme'];?>
/images/home_new/around.png"
                         alt="Phần mềm mầm non Inet hỗ trợ siêu kết nối">
                    <div class="around_parent" align="right">
                        <div class="parent_title">
                            <h4><?php echo __("Parent");?>
</h4>
                        </div>
                        <div class="parent_content">
                            <?php echo __("Easily track your student at school anytime, anywhere. Connect with the school to better raise student");?>

                            .
                        </div>
                    </div>
                    <div class="around_chef" align="left">
                        <div class="parent_title">
                            <h4><?php echo __("Chef");?>
</h4>
                        </div>
                        <div class="parent_content">
                            <?php echo __("It is easy to see daily school meals on the Inet application");?>
.
                        </div>
                    </div>
                    <div class="around_accountant" align="left">
                        <div class="parent_title">
                            <h4><?php echo __("Accountant");?>
</h4>
                        </div>
                        <div class="parent_content">
                            <?php echo __("The Inet system automatically calculates meal fee, tuition based on attendance information and service registration");?>

                            .
                        </div>
                    </div>
                    <div class="around_principal" align="center">
                        <div class="parent_title">
                            <h4><?php echo __("Administrators");?>
</h4>
                        </div>
                        <div class="parent_content">
                            <?php echo __("Know all the management information and daily activities of the school every time, everywhere on mobile");?>

                            .
                        </div>
                    </div>
                    <div class="around_teacher" align="right">
                        <div class="parent_title">
                            <h4><?php echo __("Teacher");?>
</h4>
                        </div>
                        <div class="parent_content">
                            <?php echo __("Communicate easily and transparently with parents and other departments in the school");?>

                            .
                        </div>
                    </div>
                </div>
                <div class="content_trial" align="center">
                    <a href="#" class="btn btn-success js_free-trial"><?php echo mb_strtoupper(__("FREE register for your school"), 'UTF-8');?>
</a>
                </div>
            </div>
        </div>
    </div>
    <div class="home_background">
        <div class="container">
            <div class="home_intro">
                <div class="intro_title" align="center" style="padding-bottom: 0">
                    <h2><?php echo __("Customers talk about MobiEdu");?>
</h2>
                </div>
                <div class="intro_des" align="center">
                    <h4><?php echo __("Customer satisfaction is our greatest success");?>
!</h4>
                </div>
                <div class="intro_content">
                    <div class="row">
                        <div class="col-sm-6">
                            <div id="myCarousel" class="carousel slide" data-ride="carousel">

                                <!-- Wrapper for slides -->
                                <div class="carousel-inner" role="listbox">

                                    <div class="item active">
                                        <div class="customer">
                                            <div class="customer_intro">
                                                <div class="row">
                                                    <div class="col-xs-4" align="center" style="padding-right: 7.5px">
                                                        <div class="customer_img_box">
                                                            <img class="img-responsive customer_img"
                                                                 src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/content/themes/<?php echo $_smarty_tpl->tpl_vars['system']->value['theme'];?>
/images/home_new/customer.jpg"
                                                                 alt="Customer talks about Software Inet">
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-8" style="padding-left: 7.5px">
                                                        <div class="customer_content_box">
                                                            <p class="customer_name">Nguyen Thanh Tuan</p>
                                                            <p class="customer_position">
                                                                Chairman of the board
                                                            </p>
                                                            <a class="customer_link" href="#">
                                                                Marie Curie School
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="customer_text_box">
                                                <p class="customer_text">
                                                    Thanks to Inet, I was able to manage the school anytime,
                                                    anywhere on the phone. I received all feedback and suggestions
                                                    from parents for timely adjustments and improvements. Wish Inet
                                                    more and more developed!
                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="item">
                                        <div class="customer">
                                            <div class="customer_intro">
                                                <div class="row">
                                                    <div class="col-xs-4" align="center" style="padding-right: 7.5px">
                                                        <div class="customer_img_box">
                                                            <img class="img-responsive customer_img"
                                                                 src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/content/themes/<?php echo $_smarty_tpl->tpl_vars['system']->value['theme'];?>
/images/home_new/customer4.jpg"
                                                                 alt="Customer talks about Software Inet">
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-8" style="padding-left: 7.5px">
                                                        <div class="customer_content_box">
                                                            <p class="customer_name">Vu Thi Hien</p>
                                                            <p class="customer_position">
                                                                Principal
                                                            </p>
                                                            <a class="customer_link" href="#">
                                                                Lomoloxop junior high school
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="customer_text_box">
                                                <p class="customer_text">
                                                    Inet helped us change our school management method, which is much
                                                    more economical and efficient. Parts did not have to be manually exchanged
                                                    as before!
                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="item">
                                        <div class="customer">
                                            <div class="customer_intro">
                                                <div class="row">
                                                    <div class="col-xs-4" align="center" style="padding-right: 7.5px">
                                                        <div class="customer_img_box">
                                                            <img class="img-responsive customer_img"
                                                                 src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/content/themes/<?php echo $_smarty_tpl->tpl_vars['system']->value['theme'];?>
/images/home_new/customer3.jpg"
                                                                 alt="Customer talks about Software Inet">
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-8" style="padding-left: 7.5px">
                                                        <div class="customer_content_box">
                                                            <p class="customer_name">Nguyen Thanh Huyen</p>
                                                            <p class="customer_position">
                                                                Officer
                                                            </p>
                                                            <a class="customer_link" href="#">
                                                                Dong Da, Ha Noi City
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="customer_text_box">
                                                <p class="customer_text">
                                                    Inet gives me a sense of peace of mind when I bring my children to school
                                                    I also get to know my children at school and talk to teachers very well
                                                    convenient!
                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <!-- Left and right controls -->
                                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="phone_img_box">
                                <img class="phone_img img-responsive" style="margin:auto;"
                                     src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/content/themes/<?php echo $_smarty_tpl->tpl_vars['system']->value['theme'];?>
/images/home_new/iphonex.png"
                                     alt="Software Inet">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            
            
        </div>
    </div>
    <div class="home_background_none">
        <div class="container">
            <div class="home_intro">
                <div class="intro_title" align="center">
                    <h2><?php echo __("MobiEdu - Proudly the School application that parents across the country believe in");?>
</h2>
                </div>
                <div class="intro_content">
                    <div class="row intro_content_row">
                        <div class="intro_content_in">
                            <div class="col-sm-4 content_col">
                                <div class="content_pad">
                                    <div class="content_icon" align="center">
                                        <div class="content-radius">
                                            <img class="content_img img-responsive"
                                                 src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/content/themes/<?php echo $_smarty_tpl->tpl_vars['system']->value['theme'];?>
/images/home_new/parent.png"
                                                 alt="Benefits of software Inet with parents">
                                        </div>
                                    </div>
                                    <div class="content_header" align="center">
                                        <h3 class="content_header_text"><?php echo __("Benefits to parents");?>
</h3>
                                    </div>
                                    <div class="content_text">
                                        <ul>
                                            <li><?php echo __("Share and learn experiences in raising Student with the community of wise mothers");?>

                                                .
                                            </li>
                                            <li><?php echo __("Consult, expert advice on questions");?>
.</li>

                                            <li><?php echo __("Find suitable schools for your child simply through the function 'Find a school around here'");?>

                                                .
                                            </li>
                                            <li><?php echo __("Search for suitable videos, reading stories, lessons for your child");?>

                                                .
                                            </li>
                                            <li><?php echo __("Update and exchange with teachers about student activities, activities and learning");?>

                                                .
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4 content_col">
                                <div class="content_pad">
                                    <div class="content_icon" align="center">
                                        <div class="content-radius">
                                            <img class="content_img img-responsive"
                                                 src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/content/themes/<?php echo $_smarty_tpl->tpl_vars['system']->value['theme'];?>
/images/home_new/teacher.png"
                                                 alt="Benefits of software Inet with Teachers">
                                        </div>
                                    </div>
                                    <div class="content_header" align="center">
                                        <h3 class="content_header_text"><?php echo __("Benefits to teacher");?>
</h3>
                                    </div>
                                    <div class="content_text">
                                        <ul>
                                            <li><?php echo __("Share and learn from school experiences with a community of teachers and experts");?>

                                                .
                                            </li>
                                            <li><?php echo __("Exchange and share with parents quickly and directly on the phone");?>

                                                .
                                            </li>
                                            <li><?php echo __("Easy classroom management: announcements, event registration, attendance, late leave, student comments");?>

                                                .
                                            </li>
                                            <li><?php echo __("Transparent information, avoiding misunderstanding between parents - teachers");?>

                                                .
                                            </li>
                                            <li><?php echo __("Minimize paperwork and exchange with other departments (accountants, labor ...)");?>

                                                .
                                            </li>
                                            <li><?php echo __("Minimize manipulation with automated forms and calculations");?>

                                                .
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4 content_col">
                                <div class="content_pad">
                                    <div class="content_icon" align="center">
                                        <div class="content-radius">
                                            <img class="content_img img-responsive"
                                                 src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/content/themes/<?php echo $_smarty_tpl->tpl_vars['system']->value['theme'];?>
/images/home_new/school.png"
                                                 alt="Benefits of software Inet with Schools">
                                        </div>
                                    </div>
                                    <div class="content_header" align="center">
                                        <h3 class="content_header_text"><?php echo __("Benefits to school");?>
</h3>
                                    </div>
                                    <div class="content_text">
                                        <ul>
                                            <li><?php echo __("No initial investment (only need to register to use)");?>
</li>
                                            <li><?php echo __("Easily promote the image of the school to the community, support enrollment");?>

                                                .
                                            </li>
                                            <li><?php echo __("Manage all daily information of the school on the phone, fast statistics.");?>
</li>
                                            <li><?php echo __("Interconnection between school departments (teachers, accountants, employees ...)");?>
</li>
                                            <li><?php echo __("Share, exchange and inform teachers and parents quickly");?>

                                                .
                                            </li>
                                            <li><?php echo __("Promptly grasp parents' ideas and make appropriate adjustments");?>
.</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content_trial" align="center">
                    <a href="#" data-toggle="tab"
                       class="btn btn-success js_free-trial"><?php echo mb_strtoupper(__("FREE register for your school"), 'UTF-8');?>
</a>
                </div>
            </div>
        </div>
    </div>
    <div class="home_background economi_back"
         style="background: url('<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/content/themes/<?php echo $_smarty_tpl->tpl_vars['system']->value['theme'];?>
/images/home_new/background_economi.jpg')">
        <div class="container">
            <div class="home_intro">
                <div class="intro_title" align="center" style="padding-bottom: 0">
                    <h2><?php echo __("150+ schools have chosen Inet for better management");?>
</h2>
                </div>
                <div class="intro_des" align="center">
                    <h4><?php echo __("Inet is a School social network that connects wise parents. In addition, Inet is an intelligent School management system and supports parent-school connectivity. Register for your school, please contact the information below");?>

                        :</h4>
                </div>
                <div class="intro_content">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="economi_box" align="center">
                                <div class="economi_icon">
                                    <img class="img-responsive"
                                         src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/content/themes/<?php echo $_smarty_tpl->tpl_vars['system']->value['theme'];?>
/images/home_new/support.png"
                                         alt="Liên hệ sử dụng Phần mềm mầm non Inet">
                                </div>
                                <div class="economi_title">
                                    <p><?php echo __("SUPPORT 24/7");?>
</p>
                                </div>
                                <div class="economi_short">
                                    <p><?php echo __("All days of the week (from Monday to Sunday) | Day and night");?>
.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="economi_box" align="center">
                                <div class="economi_icon">
                                    <img class="img-responsive"
                                         src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/content/themes/<?php echo $_smarty_tpl->tpl_vars['system']->value['theme'];?>
/images/home_new/contact.png"
                                         alt="Liên hệ sử dụng Phần mềm mầm non Inet">
                                </div>
                                <div class="economi_title">
                                    <p><?php echo __("HOTLINE");?>
: <a href="tel:0777.202020">0777.202020</a></p>
                                </div>
                                <div class="economi_short">
                                    <p><?php echo __("Free consultation center for Inet customers");?>
.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="economi_box" align="center">
                                <div class="economi_icon">
                                    <img class="img-responsive"
                                         src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/content/themes/<?php echo $_smarty_tpl->tpl_vars['system']->value['theme'];?>
/images/home_new/email.png"
                                         alt="Liên hệ sử dụng Phần mềm mầm non Inet">
                                </div>
                                <div class="economi_title">
                                    <p><?php echo __("EMAIL");?>
: <a href="mailto:contact@mobiedu.vn">contact@mobiedu.vn</a></p>
                                </div>
                                <div class="economi_short">
                                    <p><?php echo __("We respond to your email as quickly as possible");?>
.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                
                
            </div>
        </div>
    </div>
    <div class="home_background_black">
        <div class="container">
            <div class="home_intro info_home" style="padding-bottom: 30px; padding-top: 30px">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="footer_box">
                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
">
                                <div class="footer_logo_box">
                                    <div class="footer_logo">
                                        <img class="img-responsive" style="max-width: 50px;margin-left: auto;margin-right: auto;"
                                             src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/content/themes/<?php echo $_smarty_tpl->tpl_vars['system']->value['theme'];?>
/images/logo.png"
                                             alt="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_title'];?>
 | <?php echo __("School management and connection software");?>
" title="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_title'];?>
 | <?php echo __("School management and connection software");?>
">
                                    </div>
                                    <div class="footer_slogan">
                                        <p style="text-align: center;"><?php echo __("Connect love");?>
</p>
                                    </div>
                                </div>
                            </a>












                        </div>
                    </div>
                    <div class="col-sm-5">
                        <div class="footer_box">
                            <div class="row">
                                <div class="col-sm-6" style="padding-right: 5px">
                                    <div class="menu_box" align="left">
                                        <div class="menu_header">
                                            <p><?php echo __("Parent");?>
</p>
                                        </div>
                                        <div class="menu_content">
                                            <ul>
                                                <li><p><a target="_blank"
                                                          href="#"><?php echo __("What is Mascom- Edu?");?>
</a>
                                                    </p></li>
                                                <li><p><a target="_blank"
                                                          href="#"><?php echo __("How to use Inet?");?>
</a>
                                                    </p></li>
                                                <li><p><a target="_blank"
                                                          href="#"><?php echo __("Questioned users");?>
</a>
                                                    </p></li>
                                                <li><p><a target="_blank" href="#"><?php echo __("When your child go to school");?>
</a></p></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6" style="padding-left: 5px">
                                    <div class="menu_box" align="left">
                                        <div class="menu_header">
                                            <p><?php echo __("School");?>
</p>
                                        </div>
                                        <div class="menu_content">
                                            <ul>
                                                <li><p><a target="_blank"
                                                          href="#"><?php echo __("What is Inet support for the school?");?>
</a>
                                                    </p></li>
                                                <li><p><a target="_blank"
                                                          href="#"><?php echo __("Characteristics");?>
</a>
                                                    </p></li>
                                                <li><p><a target="_blank"
                                                          href="#"><?php echo __("FAQ");?>
</a>
                                                    </p></li>
                                                <li><p><a target="_blank"
                                                          href="#"><?php echo __("Usage guide");?>
</a>
                                                    </p></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="footer_box">
                            <div class="menu_box" align="left">
                                <div class="menu_header">
                                    <p><?php echo __("Cooperation");?>
</p>
                                </div>
                                <div class="menu_content">
                                    <ul>
                                        <li><p><a target="_blank" href="#"><?php echo __("Partner progrmam");?>
</a></p></li>
                                        <li><p><?php echo __("Telephone");?>
: <strong><a href="tel:0777.202020">0777.202020</a></strong></p></li>
                                        <li><p>Email: <a href="mailto:contact@mobiedu.vn"><?php echo $_smarty_tpl->tpl_vars['system']->value['mail_contact'];?>
</a></p>
                                        </li>
                                        <li><p>Facebook: <a href="#" target="_blank">fb.com/mascom.edu.vn</a>
                                            </p></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end liên hệ taila -->
<?php $_smarty_tpl->_subTemplateRender('file:_footer_new.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php }
}
