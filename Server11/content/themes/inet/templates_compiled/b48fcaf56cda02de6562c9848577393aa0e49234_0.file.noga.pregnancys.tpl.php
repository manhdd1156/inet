<?php
/* Smarty version 3.1.31, created on 2021-04-05 13:07:34
  from "D:\workplace\Server11\content\themes\inet\templates\ci\noga\noga.pregnancys.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_606aa92692a596_81919766',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b48fcaf56cda02de6562c9848577393aa0e49234' => 
    array (
      0 => 'D:\\workplace\\Server11\\content\\themes\\inet\\templates\\ci\\noga\\noga.pregnancys.tpl',
      1 => 1552404703,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_606aa92692a596_81919766 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
            <div class="pull-right flip">
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/noga/pregnancys/add" class="btn btn-default">
                    <i class="fa fa-plus"></i> <?php echo __("Add New");?>

                </a>
            </div>
        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
            <div class="pull-right flip">
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/noga/pregnancys" class="btn btn-default">
                    <i class="fa fa-list"></i> <?php echo __("Lists");?>

                </a>
            </div>
        <?php }?>
        <i class="fas fa-calendar-alt fa-fw fa-lg pr10"></i>
        <?php echo __("Pregnancy information");?>

        <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
            &rsaquo; <?php echo __("Add New");?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "edit") {?>
            &rsaquo; <?php echo __("Edit");?>

        <?php }?>
    </div>
    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
        <div class="panel-body with-table form-horizontal">
            <div class="table-responsive" name = "schedule_all">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr><th colspan="5"><?php echo __("Pregnancy information");?>
</th></tr>
                    <tr>
                        <th>
                            <?php echo __("No.");?>

                        </th>
                        <th>
                            <?php echo __("Week");?>

                        </th>
                        <th>
                            <?php echo __("Title");?>

                        </th>
                        <th>
                            <?php echo __("Link");?>

                        </th>
                        <th>
                            <?php echo __("Actions");?>

                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['pregnancys']->value, 'row', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['row']->value) {
?>
                        <tr>
                            <td align="center"><?php echo $_smarty_tpl->tpl_vars['k']->value+1;?>
</td>
                            <td align="center"><a href = "<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/noga/pregnancys/edit/<?php echo $_smarty_tpl->tpl_vars['row']->value['foetus_based_on_week_id'];?>
"><?php echo __("Pregnancy information");?>
 <?php echo $_smarty_tpl->tpl_vars['row']->value['week'];?>
</a></td>
                            <td align="center"><a href = "<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/noga/pregnancys/edit/<?php echo $_smarty_tpl->tpl_vars['row']->value['foetus_based_on_week_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['row']->value['title'];?>
</a></td>
                            <td>
                                <a href="<?php echo $_smarty_tpl->tpl_vars['row']->value['link'];?>
" target="_blank"><?php echo $_smarty_tpl->tpl_vars['row']->value['link'];?>
</a>
                            </td>
                            <td>
                                <a class="btn btn-xs btn-default" href = "<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/noga/pregnancys/edit/<?php echo $_smarty_tpl->tpl_vars['row']->value['foetus_based_on_week_id'];?>
"><?php echo __("Edit");?>
</a>
                                <a href="#" class = "btn-xs btn btn-danger js_noga-pregnancy-delete" data-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['foetus_based_on_week_id'];?>
"><?php echo __("Delete");?>
</a>
                            </td>
                        </tr>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                    </tbody>
                </table>
            </div>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
        <div class="panel-body">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/noga/bo_noga_pregnancy.php">
                <input type="hidden" name="do" value="add"/>
                <div class="form-group">
                    <label class="col-sm-4 control-label text-left" style="float: left"><?php echo __("Enter pregnancy information");?>
</label>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id = "myTablePregnancy">
                        <thead>
                        <tr>
                            <th>
                                <?php echo __('No.');?>

                            </th>
                            <th>
                                <?php echo __('Option');?>

                            </th>
                            <th>
                                <?php echo __('Week');?>

                            </th>
                            <th>
                                <?php echo __('Title');?>

                            </th>
                            <th>
                                <?php echo __('Link');?>

                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td align="center" class = "col_no">1</td>
                            <td align="center">
                                <a class = "btn btn-default js_cate-delete_null"> <?php echo __("Delete");?>
 </a>
                            </td>
                            <td>
                                <input type = "number" min="0" step="1" name = "weeks[]" placeholder="" style = "padding-left: 10px; height: 35px; width: 100%; ">
                            </td>
                            <td>
                                <textarea class = "note" type="text" name = "titles[]" style="width:100%"></textarea>
                            </td>
                            <td>
                                <textarea class = "note" type="text" name = "links[]" style="width:100%"></textarea>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <div class = "form-group mt20">
                        <div class = "col-sm-12">
                            <a class="btn btn-default js_add-pregnancy"><?php echo __("Add new row");?>
</a>
                            <button type="submit" class="btn btn-primary padrl30"><?php echo __("Save");?>
</button>
                        </div>
                    </div>
                </div>
                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->
                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->

            </form>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "edit") {?>
        <div class="panel-body">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/noga/bo_noga_pregnancy.php">
                <input type="hidden" name="do" value="edit"/>
                <input type="hidden" name="foetus_based_on_week_id" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['foetus_based_on_week_id'];?>
"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Pregnancy information");?>
 (*)</label>
                    <div class="col-sm-9">
                        <input type = "text" class="form-control" name="week" required value = "<?php echo $_smarty_tpl->tpl_vars['data']->value['week'];?>
">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Title");?>
</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="title" rows="6"><?php echo $_smarty_tpl->tpl_vars['data']->value['title'];?>
</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Link");?>
</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="link" rows="6"><?php echo $_smarty_tpl->tpl_vars['data']->value['link'];?>
</textarea>
                    </div>
                </div>
                <div class = "form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30"><?php echo __("Save");?>
</button>
                    </div>
                </div>
                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->
                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    <?php }?>
</div><?php }
}
