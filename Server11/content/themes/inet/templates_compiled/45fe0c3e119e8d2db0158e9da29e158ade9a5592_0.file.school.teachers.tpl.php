<?php
/* Smarty version 3.1.31, created on 2021-05-20 17:05:20
  from "D:\workplace\Inet-project\source\Server11\content\themes\inet\templates\ci\school\school.teachers.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_60a63460571b27_44609558',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '45fe0c3e119e8d2db0158e9da29e158ade9a5592' => 
    array (
      0 => 'D:\\workplace\\Inet-project\\source\\Server11\\content\\themes\\inet\\templates\\ci\\school\\school.teachers.tpl',
      1 => 1621501239,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60a63460571b27_44609558 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <?php if ($_smarty_tpl->tpl_vars['canEdit']->value) {?>
            <div class="pull-right flip">
                <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/teachers/addexisting" class="btn btn-default">
                        <i class="fa fa-plus"></i> <?php echo __("Add Existing Account");?>

                    </a>
                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/teachers/add" class="btn btn-default">
                        <i class="fa fa-plus"></i> <?php echo __("Add New");?>

                    </a>
                <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/teachers/addexisting" class="btn btn-default">
                        <i class="fa fa-plus"></i> <?php echo __("Add Existing Account");?>

                    </a>
                <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "addexisting") {?>
                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/teachers/add" class="btn btn-default">
                        <i class="fa fa-plus"></i> <?php echo __("Add New");?>

                    </a>
                <?php }?>
            </div>
        <?php }?>
        <div class="pull-right flip" style="margin-right: 5px">
            <?php if ($_smarty_tpl->tpl_vars['school']->value['school_step'] == SCHOOL_STEP_FINISH || $_smarty_tpl->tpl_vars['school']->value['grade'] != 0) {?>
                <?php if ($_smarty_tpl->tpl_vars['sub_view']->value != 'addexisting') {?>
                    <a href="https://blog.coniu.vn/huong-dan-tao-tai-khoan-va-them-moi-giao-vien-vao-truong/" class="btn btn-info  btn_guide" target="_blank">
                        <i class="fa fa-info-circle"></i> <?php echo __("Guide");?>

                    </a>
                <?php } else { ?>
                    <a href="https://blog.coniu.vn/huong-dan-them-tai-khoan-giao-vien-da-dang-ky-tren-he-thong/" class="btn btn-info  btn_guide" target="_blank">
                        <i class="fa fa-info-circle"></i> <?php echo __("Guide");?>

                    </a>
                <?php }?>
            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['sub_view']->value != '') {?>
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/teachers" class="btn btn-default">
                    <i class="fa fa-list"></i> <?php echo __("Lists");?>

                </a>
            <?php }?>
        </div>
        <i class="fa fa-street-view fa-fw fa-lg pr10"></i>
        <?php echo __("Teacher - Employee");?>

        <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == "edit") {?>
            &rsaquo; <?php echo $_smarty_tpl->tpl_vars['data']->value['user_fullname'];?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
            &rsaquo; <?php echo __('Add New');?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
            &rsaquo; <?php echo __('Lists');?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "addexisting") {?>
            &rsaquo; <?php echo __('Add Existing Account');?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "assign") {?>
            &rsaquo; <?php echo __('Assign teacher');?>

        <?php }?>
    </div>
    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
        <div class="panel-body with-table">
            <div class="mb5"><strong><?php echo __("Teacher - Employee list");?>
&nbsp;(<?php echo count($_smarty_tpl->tpl_vars['rows']->value);?>
)</strong></div>

            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th><?php echo __("Full name");?>
</th>
                            <th><?php echo __("Phone");?>
</th>
                            <th><?php echo __("Email");?>
</th>
                            <th><?php echo __("Class");?>
</th>
                            <th><?php echo __("Actions");?>
</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['rows']->value, 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
                            <tr>
                                <td class="align-middle" align="center"><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</td>
                                <td class="align-middle" nowrap="true">
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['row']->value['user_name'];?>
">
                                        <?php echo $_smarty_tpl->tpl_vars['row']->value['user_fullname'];?>

                                    </a>
                                    &nbsp;
                                    <?php if ($_smarty_tpl->tpl_vars['row']->value['user_id'] != $_smarty_tpl->tpl_vars['user']->value->_data['user_id']) {?>
                                        <a href="#" class="far fa-comments fa-lg js_chat-start" data-name="<?php echo $_smarty_tpl->tpl_vars['row']->value['user_fullname'];?>
" data-uid="<?php echo $_smarty_tpl->tpl_vars['row']->value['user_id'];?>
"></a>
                                    <?php }?>
                                </td>
                                <td class="align-middle"><?php echo $_smarty_tpl->tpl_vars['row']->value['user_phone'];?>
</td>
                                <td class="align-middle"><a href="mailto:<?php echo $_smarty_tpl->tpl_vars['row']->value['user_email'];?>
"><?php echo $_smarty_tpl->tpl_vars['row']->value['user_email'];?>
</a></td>
                                <td class="align-middle">
                                    <?php if (count($_smarty_tpl->tpl_vars['row']->value['classes']) == 0) {?>
                                        <?php echo __("No class");?>

                                    <?php } else { ?>
                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['row']->value['classes'], 'class', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['class']->value) {
?>
                                            <?php echo $_smarty_tpl->tpl_vars['class']->value['group_title'];?>
 <?php if ($_smarty_tpl->tpl_vars['k']->value < (count($_smarty_tpl->tpl_vars['row']->value['classes'])-1)) {?> - <?php }?>
                                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                    <?php }?>
                                </td>
                                <td class="align-middle" align="center">
                                    
                                        
                                    
                                    <?php if ($_smarty_tpl->tpl_vars['canEdit']->value) {?>
                                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/teachers/assign/<?php echo $_smarty_tpl->tpl_vars['row']->value['user_id'];?>
" class="btn btn-xs btn-info">
                                            <?php echo __("Assign");?>

                                        </a>
                                        <?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_id'] != $_smarty_tpl->tpl_vars['row']->value['user_id'] && $_smarty_tpl->tpl_vars['row']->value['user_id'] != $_smarty_tpl->tpl_vars['school']->value['page_admin']) {?>
                                            <button class="btn btn-xs btn-danger js_school-unassign" data-handle="teacher" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['user_id'];?>
">
                                                <?php echo __("Delete");?>

                                            </button>
                                        <?php }?>
                                        
                                            
                                                
                                            
                                            
                                                
                                            
                                        
                                    <?php }?>
                                </td>
                            </tr>
                            <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>


                        <?php if (count($_smarty_tpl->tpl_vars['rows']->value) == 0) {?>
                            <tr class="odd">
                                <td valign="top" align="center" colspan="6" class="dataTables_empty">
                                    <?php echo __("No data available in table");?>

                                </td>
                            </tr>
                        <?php }?>
                    </tbody>
                </table>
            </div>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "edit") {?>
        <div class="panel-body">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_teacher.php">
                <input type="hidden" name="school_username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                <input type="hidden" name="do" value="edit"/>
                <input type="hidden" name="user_id" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['user_id'];?>
"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Full name");?>
 (*)</label>
                    <div class="col-sm-9">
                        <input class="form-control" name="full_name" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['user_fullname'];?>
" required autofocus maxlength="255">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Telephone");?>
 (*)</label>
                    <div class="col-sm-9">
                        <input name="user_phone" id="user_phone" type="text" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['user_phone'];?>
" class="form-control" required maxlength="50">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Email");?>
 (*)</label>
                    <div class="col-sm-9">
                        <input name="email" id="email" type="email" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['user_email'];?>
" class="form-control" required maxlength="100">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Password");?>
</label>
                    <div class="col-sm-9">
                        <input name="password" id="password" type="password" class="form-control" placeholder="<?php echo __("Blank password means no change");?>
">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Gender");?>
 (*)</label>
                    <div class="col-sm-3">
                        <select name="gender" id="gender" class="form-control">
                            <option value="<?php echo @constant('FEMALE');?>
" <?php if ($_smarty_tpl->tpl_vars['data']->value['user_gender'] == @constant('FEMALE')) {?>selected<?php }?>><?php echo __("Female");?>
</option>
                            <option value="<?php echo @constant('MALE');?>
" <?php if ($_smarty_tpl->tpl_vars['data']->value['user_gender'] == @constant('MALE')) {?>selected<?php }?>><?php echo __("Male");?>
</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-3" align="right">
                        <label class="control-label text-right"><?php echo __("Role");?>
</label>
                        <div><?php echo __("If this user is only teacher, you do not need select any role");?>
.</div>
                    </div>
                    <div class="col-sm-9">
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['roles']->value, 'role');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['role']->value) {
?>
                            <input type="checkbox" name="role_ids[]" value="<?php echo $_smarty_tpl->tpl_vars['role']->value['role_id'];?>
" <?php if ($_smarty_tpl->tpl_vars['role']->value['user_id'] > 0) {?>checked<?php }?>/><?php echo $_smarty_tpl->tpl_vars['role']->value['role_name'];?>
<br/>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30"><?php echo __("Save");?>
</button>
                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/teachers" class="btn btn-default"><?php echo __("Lists");?>
</a>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "assign") {?>
        <div class="panel-body">
            <div align="center"><strong><?php echo __("Phân công lớp hoặc vai trò trong trường cho giáo viên");?>
</strong></div>
            <br/>
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_teacher.php">
                <input type="hidden" name="school_username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                <input type="hidden" name="do" value="assign"/>
                <input type="hidden" name="user_id" value="<?php echo $_smarty_tpl->tpl_vars['teacherId']->value;?>
"/>
                <div class="form-group">
                    <div class="col-sm-3" align="right">
                        <label class="control-label text-right"><?php echo __("Class");?>
</label>
                        <div><?php echo __("Select the class that teachers assigned");?>
.</div>
                    </div>
                    <div class="col-sm-9">
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['classes']->value, 'class');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class']->value) {
?>
                            <input type="checkbox" name="class_ids[]" value="<?php echo $_smarty_tpl->tpl_vars['class']->value['group_id'];?>
" <?php if ($_smarty_tpl->tpl_vars['class']->value['homeroom'] == 1) {?>checked<?php }?>/> <?php echo $_smarty_tpl->tpl_vars['class']->value['group_title'];?>
<br/>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                    </div>
                </div>
                <hr/>
                <?php if ($_smarty_tpl->tpl_vars['school']->value['school_step'] == SCHOOL_STEP_FINISH || $_smarty_tpl->tpl_vars['school']->value['grade'] != 0) {?>
                    <div class="form-group">
                        <div class="col-sm-3" align="right">
                            <label class="control-label text-right"><?php echo __("Role");?>
</label>
                            <div><?php echo __("If this user is only teacher, you do not need select any role");?>
.</div>
                        </div>
                        <div class="col-sm-9">
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['roles']->value, 'role');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['role']->value) {
?>
                                <input type="checkbox" name="role_ids[]" value="<?php echo $_smarty_tpl->tpl_vars['role']->value['role_id'];?>
" <?php if ($_smarty_tpl->tpl_vars['role']->value['user_id'] > 0) {?>checked<?php }?>/> <?php echo $_smarty_tpl->tpl_vars['role']->value['role_name'];?>
<br/>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </div>
                    </div>
                <?php }?>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30"><?php echo __("Save");?>
</button>
                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/teachers" class="btn btn-default"><?php echo __("Lists");?>
</a>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
        <div class="panel-body">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_teacher.php">
                <input type="hidden" name="school_username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                <input type="hidden" name="do" value="add"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Full name");?>
 (*)</label>
                    <div class="col-sm-5">
                        <input name="last_name" id="last_name" type="text" class="form-control" placeholder="<?php echo __("Last name");?>
" required autofocus maxlength="255">
                    </div>
                    <div class="col-sm-4">
                        <input name="first_name" id="first_name" type="text" class="form-control" placeholder="<?php echo __("First name");?>
" required maxlength="255">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Telephone");?>
 (*)</label>
                    <div class="col-sm-9">
                        <input name="phone" id="phone" type="text" class="form-control" required maxlength="50">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Email");?>
 (*)</label>
                    <div class="col-sm-9">
                        <input name="email" id="email" type="email" class="form-control" required maxlength="100">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Password");?>
 (*)</label>
                    <div class="col-sm-9">
                        <input name="password" id="password" type="password" class="form-control" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Gender");?>
 (*)</label>
                    <div class="col-sm-3">
                        <select name="gender" id="gender" class="form-control" required>
                            <option value="<?php echo @constant('FEMALE');?>
"><?php echo __("Female");?>
</option>
                            <option value="<?php echo @constant('MALE');?>
"><?php echo __("Male");?>
</option>
                        </select>
                    </div>
                </div>
                <?php if ($_smarty_tpl->tpl_vars['school']->value['school_step'] == SCHOOL_STEP_FINISH || $_smarty_tpl->tpl_vars['school']->value['grade'] != 0) {?>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">
                            <?php echo __("Add class");?>
?
                        </label>
                        <div class="col-sm-9">
                            <div class="onoffswitch">
                                <input type="checkbox" name="class_option" class="onoffswitch-checkbox" id="class_option">
                                <label class="onoffswitch-label" for="class_option"></label>
                            </div>
                        </div>
                    </div>
                <?php }?>
                <div id="select_class">
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left"><?php echo __("Class");?>
</label>
                        <div class="col-sm-3">
                            <select name="class_id" class="form-control">
                                <option value="0"><?php echo __("Select class");?>
...</option>
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['classes']->value, 'class');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class']->value) {
?>
                                    <option value="<?php echo $_smarty_tpl->tpl_vars['class']->value['group_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['class']->value['group_title'];?>
</option>
                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                            </select>
                        </div>
                    </div>
                </div>
                <div id="add_class" class="hidden">
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left"><?php echo __("Class name");?>
 (*)</label>
                        <div class="col-sm-9">
                            <input name="group_title" id="class_name" type="text" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left"><?php echo __("Class level name");?>
 (*)</label>
                        <div class="col-sm-3">
                            <select name="class_level_id" class="form-control">
                                <option value="0"><?php echo __("Select class level");?>
...</option>
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['classLevels']->value, 'classLevel');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['classLevel']->value) {
?>
                                    <option value="<?php echo $_smarty_tpl->tpl_vars['classLevel']->value['class_level_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['classLevel']->value['class_level_name'];?>
</option>
                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                            </select>
                        </div>
                    </div>
                </div>
                <?php if ($_smarty_tpl->tpl_vars['school']->value['school_step'] == SCHOOL_STEP_FINISH || $_smarty_tpl->tpl_vars['school']->value['grade'] != 0) {?>
                    <div class="form-group">
                        <div class="col-sm-3" align="right">
                            <label class="control-label text-right"><?php echo __("Role");?>
</label>
                            <div><?php echo __("If this user is only teacher, you do not need select any role");?>
.</div>
                        </div>
                        <div class="col-sm-9">
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['roles']->value, 'role');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['role']->value) {
?>
                                <input type="checkbox" name="role_ids[]" value="<?php echo $_smarty_tpl->tpl_vars['role']->value['role_id'];?>
"/><?php echo $_smarty_tpl->tpl_vars['role']->value['role_name'];?>
<br/>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </div>
                    </div>
                <?php }?>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30"><?php echo __("Save");?>
</button>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "addexisting") {?>
        <div class="panel-body">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_teacher.php">
                <input type="hidden" name="school_username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                <input type="hidden" name="do" value="addexisting"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Teacher");?>
/<?php echo __("Employee");?>
</label>
                    <div class="col-sm-9">
                        <input name="search-username" id="search-username" type="text" class="form-control" placeholder="<?php echo __("Enter username, email or fullname to search");?>
" autofocus autocomplete="off">
                        <div id="search-username-results" class="dropdown-menu dropdown-widget dropdown-search">
                            <div class="dropdown-widget-header">
                                <?php echo __("Search Results");?>

                            </div>
                            <div class="dropdown-widget-body">
                                <div class="loader loader_small ptb10"></div>
                            </div>
                        </div>
                        <div><?php echo __("Enter at least 4 characters");?>
.</div>
                        <br/>
                        <div class="col-sm-9" id="teacher_list" name="teacher_list"></div>
                    </div>
                </div>
                <?php if ($_smarty_tpl->tpl_vars['school']->value['school_step'] == SCHOOL_STEP_FINISH || $_smarty_tpl->tpl_vars['school']->value['grade'] != 0) {?>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">
                            <?php echo __("Add class");?>
?
                        </label>
                        <div class="col-sm-9">
                            <div class="onoffswitch">
                                <input type="checkbox" name="class_option" class="onoffswitch-checkbox" id="class_option">
                                <label class="onoffswitch-label" for="class_option"></label>
                            </div>
                        </div>
                    </div>
                <?php }?>
                <div id="select_class">
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left"><?php echo __("Class");?>
</label>
                        <div class="col-sm-3">
                            <select name="class_id" class="form-control">
                                <option value="0"><?php echo __("Select class");?>
...</option>
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['classes']->value, 'class');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class']->value) {
?>
                                    <option value="<?php echo $_smarty_tpl->tpl_vars['class']->value['group_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['class']->value['group_title'];?>
</option>
                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                            </select>
                        </div>
                    </div>
                </div>
                <div id="add_class" class="hidden">
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left"><?php echo __("Class name");?>
 (*)</label>
                        <div class="col-sm-9">
                            <input name="group_title" id="class_name" type="text" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left"><?php echo __("Class level name");?>
 (*)</label>
                        <div class="col-sm-3">
                            <select name="class_level_id" class="form-control">
                                <option value="0"><?php echo __("Select class level");?>
...</option>
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['classLevels']->value, 'classLevel');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['classLevel']->value) {
?>
                                    <option value="<?php echo $_smarty_tpl->tpl_vars['classLevel']->value['class_level_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['classLevel']->value['class_level_name'];?>
</option>
                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                            </select>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30" disabled><?php echo __("Save");?>
</button>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    <?php }?>
</div>
<?php }
}
