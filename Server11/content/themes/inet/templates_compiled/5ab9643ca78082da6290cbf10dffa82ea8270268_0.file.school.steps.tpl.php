<?php
/* Smarty version 3.1.31, created on 2021-05-19 11:13:25
  from "D:\workplace\inet-project\Server11\content\themes\inet\templates\ci\school\school.steps.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_60a49065784f48_93833537',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '5ab9643ca78082da6290cbf10dffa82ea8270268' => 
    array (
      0 => 'D:\\workplace\\inet-project\\Server11\\content\\themes\\inet\\templates\\ci\\school\\school.steps.tpl',
      1 => 1619677394,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60a49065784f48_93833537 (Smarty_Internal_Template $_smarty_tpl) {
if ($_smarty_tpl->tpl_vars['view']->value != '') {?>
    <div class="col-sm-9">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">
                    <div class="col-xs-2">
                        <div class="top_direct" align="center">
                            <?php if ($_smarty_tpl->tpl_vars['view']->value == '') {?>
                            <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "classlevels") {?>
                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" class="btn btn-default btn-xs"><i class="fas fa-chevron-left"></i> <?php echo __("Back");?>
</a>

                            <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "classes") {?>
                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/classlevels" class="btn btn-default btn-xs"><i class="fas fa-chevron-left"></i> <?php echo __("Back");?>
</a>

                            <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "teachers") {?>
                                <?php if (count($_smarty_tpl->tpl_vars['classesStep']->value) > 0) {?>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/classes" class="btn btn-default btn-xs"><i class="fas fa-chevron-left"></i> <?php echo __("Back");?>
</a>
                                <?php } else { ?>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/classes/add" class="btn btn-default btn-xs"><i class="fas fa-chevron-left"></i> <?php echo __("Back");?>
</a>
                                <?php }?>

                            <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "services") {?>
                                <?php if (count($_smarty_tpl->tpl_vars['teachersStep']->value > 0)) {?>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/teachers" class="btn btn-default btn-xs"><i class="fas fa-chevron-left"></i> <?php echo __("Back");?>
</a>
                                <?php } else { ?>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/teachers/add" class="btn btn-default btn-xs"><i class="fas fa-chevron-left"></i> <?php echo __("Back");?>
</a>
                                <?php }?>

                            <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "fees") {?>
                                <?php if (count($_smarty_tpl->tpl_vars['servicesStep']->value) > 0) {?>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/services" class="btn btn-default btn-xs"><i class="fas fa-chevron-left"></i> <?php echo __("Back");?>
</a>
                                <?php } else { ?>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/services/add" class="btn btn-default btn-xs"><i class="fas fa-chevron-left"></i> <?php echo __("Back");?>
</a>
                                <?php }?>

                            <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "pickup" && $_smarty_tpl->tpl_vars['sub_view']->value == "template") {?>
                                <?php if (count($_smarty_tpl->tpl_vars['feesStep']->value) > 0) {?>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/fees" class="btn btn-default btn-xs"><i class="fas fa-chevron-left"></i> <?php echo __("Back");?>
</a>
                                <?php } else { ?>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/fees/add" class="btn btn-default btn-xs"><i class="fas fa-chevron-left"></i> <?php echo __("Back");?>
</a>
                                <?php }?>

                            <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "pickup" && $_smarty_tpl->tpl_vars['sub_view']->value == "assign") {?>
                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/pickup/template" class="btn btn-default btn-xs"><i class="fas fa-chevron-left"></i> <?php echo __("Back");?>
</a>

                            <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "children") {?>
                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/pickup/assign" class="btn btn-default btn-xs"><i class="fas fa-chevron-left"></i> <?php echo __("Back");?>
</a>
                            <?php }?>
                        </div>
                    </div>
                    <div class="col-xs-8">
                        <div align="center"><h4 style="font-size: 20px"><?php echo __("Trình tự khởi tạo trường");?>
</h4></div>
                    </div>
                    <div class="col-xs-2">
                        <div class="top_direct" align="center">
                            <?php if ($_smarty_tpl->tpl_vars['view']->value == '') {?>
                            <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "classlevels") {?>
                                <?php if (count($_smarty_tpl->tpl_vars['rows']->value) > 0 && count($_smarty_tpl->tpl_vars['classesStep']->value) > 0) {?>
                                    <a href="#" class="btn btn-success btn-xs js_school-step" data-step="1" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-view="classes" data-subview=""><?php echo __("Next");?>
 <i class="fas fa-chevron-right"></i></a>
                                <?php } elseif (count($_smarty_tpl->tpl_vars['rows']->value) > 0 && count($_smarty_tpl->tpl_vars['classesStep']->value) == 0) {?>
                                    <a href="#" class="btn btn-success btn-xs js_school-step" data-step="1" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-view="classes" data-subview="add"><?php echo __("Next");?>
 <i class="fas fa-chevron-right"></i></a>
                                <?php }?>

                            <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "classes") {?>
                                <?php if (count($_smarty_tpl->tpl_vars['teachersStep']->value) > 0) {?>
                                    <a href="#" class="btn btn-success btn-xs js_school-step" data-step="2" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-view="teachers" data-subview=""><?php echo __("Next");?>
 <i class="fas fa-chevron-right"></i></a>
                                <?php } else { ?>
                                    <a href="#" class="btn btn-success btn-xs js_school-step" data-step="2" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-view="teachers" data-subview="add"><?php echo __("Next");?>
 <i class="fas fa-chevron-right"></i></a>
                                <?php }?>

                            <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "teachers") {?>
                                <?php if (count($_smarty_tpl->tpl_vars['servicesStep']->value) > 0) {?>
                                    <a href="#" class="btn btn-success btn-xs js_school-step" data-step="3" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-view="services" data-subview=""><?php echo __("Next");?>
 <i class="fas fa-chevron-right"></i></a>
                                <?php } else { ?>
                                    <a href="#" class="btn btn-success btn-xs js_school-step" data-step="3" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-view="services" data-subview="add"><?php echo __("Next");?>
 <i class="fas fa-chevron-right"></i></a>
                                <?php }?>

                            <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "services") {?>
                                <?php if (count($_smarty_tpl->tpl_vars['feesStep']->value) > 0) {?>
                                    <a href="#" class="btn btn-success btn-xs js_school-step" data-step="4" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-view="fees" data-subview=""><?php echo __("Next");?>
 <i class="fas fa-chevron-right"></i></a>
                                <?php } else { ?>
                                    <a href="#" class="btn btn-success btn-xs js_school-step" data-step="4" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-view="fees" data-subview="add"><?php echo __("Next");?>
 <i class="fas fa-chevron-right"></i></a>
                                <?php }?>

                            <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "fees") {?>
                                <a href="#" class="btn btn-success btn-xs js_school-step" data-step="5" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-view="pickup" data-subview="template"><?php echo __("Next");?>
 <i class="fas fa-chevron-right"></i></a>

                            <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "pickup" && $_smarty_tpl->tpl_vars['sub_view']->value == "template") {?>
                                <a href="#" class="btn btn-success btn-xs js_school-step" data-step="6" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-view="pickup" data-subview="assign"><?php echo __("Next");?>
 <i class="fas fa-chevron-right"></i></a>

                            <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "pickup" && $_smarty_tpl->tpl_vars['sub_view']->value == "assign") {?>
                                <a href="#" class="btn btn-success btn-xs js_school-step" data-step="7" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-view="children" data-subview=""><?php echo __("Next");?>
 <i class="fas fa-chevron-right"></i></a>

                            <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "children") {?>
                                <a href="#" class="btn btn-success btn-xs js_school-step" data-step="100" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-view="" data-subview=""><?php echo __("Finish");?>
 <i class="fas fa-chevron-right"></i></a>
                            <?php }?>
                        </div>
                    </div>
                </div>
                <div class="step_box" align="center">
                    <div class="main_step">
                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/classlevels" <?php if ($_smarty_tpl->tpl_vars['view']->value == "classlevels") {?>class="color_grey btn-success btn btn-xs" <?php } else { ?>class="btn-success btn btn-xs"<?php }?>>1. <?php echo __("Class level");?>
</a>
                        <i class="fas fa-hand-point-right"></i>
                        <?php if (count($_smarty_tpl->tpl_vars['classlevelsStep']->value) > 0) {?>
                            <?php if ($_smarty_tpl->tpl_vars['school']->value['school_step'] >= 1) {?>
                                <?php if (count($_smarty_tpl->tpl_vars['classesStep']->value) > 0) {?>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/classes" <?php if ($_smarty_tpl->tpl_vars['view']->value == "classes") {?>class="color_grey btn-success btn btn-xs" <?php } else { ?>class="btn-success btn btn-xs"<?php }?>>2. <?php echo __("Class");?>
</a>
                                <?php } else { ?>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/classes/add" <?php if ($_smarty_tpl->tpl_vars['view']->value == "classes") {?>class="color_grey btn-success btn btn-xs" <?php } else { ?>class="btn-success btn btn-xs"<?php }?>>2. <?php echo __("Class");?>
</a>
                                <?php }?>
                            <?php } else { ?>
                                <button class="btn btn-default btn-xs">2. <?php echo __("Class");?>
</button>
                            <?php }?>

                            <i class="fas fa-hand-point-right"></i>
                            <?php if ($_smarty_tpl->tpl_vars['school']->value['school_step'] >= 2) {?>
                                <?php if (count($_smarty_tpl->tpl_vars['teachersStep']->value) > 0) {?>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/teachers" <?php if ($_smarty_tpl->tpl_vars['view']->value == "teachers") {?>class="color_grey btn-success btn btn-xs" <?php } else { ?>class="btn-success btn btn-xs"<?php }?>>3. <?php echo __("Teacher");?>
</a>
                                <?php } else { ?>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/teachers/add" <?php if ($_smarty_tpl->tpl_vars['view']->value == "teachers") {?>class="color_grey btn btn-success btn-xs" <?php } else { ?>class="btn-success btn btn-xs"<?php }?>>3. <?php echo __("Teacher");?>
</a>
                                <?php }?>
                            <?php } else { ?>
                                <button class="btn btn-default btn-xs">3. <?php echo __("Teacher");?>
</button>
                            <?php }?>
                            <i class="fas fa-hand-point-right"></i>
                            <?php if ($_smarty_tpl->tpl_vars['school']->value['school_step'] >= 3) {?>
                                <?php if (count($_smarty_tpl->tpl_vars['servicesStep']->value) > 0) {?>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/services" <?php if ($_smarty_tpl->tpl_vars['view']->value == "fees" || $_smarty_tpl->tpl_vars['view']->value == "services") {?>class="color_grey btn btn-success btn-xs" <?php } else { ?>class="btn-success btn btn-xs"<?php }?>>4. <?php echo __("Tuition settings");?>
</a>
                                <?php } else { ?>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/services/add" <?php if ($_smarty_tpl->tpl_vars['view']->value == "services" || $_smarty_tpl->tpl_vars['view']->value == "fees") {?>class="color_grey btn btn-success btn-xs" <?php } else { ?>class="btn-success btn btn-xs"<?php }?>>4. <?php echo __("Tuition settings");?>
</a>
                                <?php }?>
                            <?php } else { ?>
                                <button class="btn btn-default btn-xs">4. <?php echo __("Tuition settings");?>
</button>
                            <?php }?>
                            <i class="fas fa-hand-point-right"></i>
                            <?php if ($_smarty_tpl->tpl_vars['school']->value['school_step'] >= 5) {?>
                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/pickup/template" <?php if ($_smarty_tpl->tpl_vars['view']->value == "pickup" && ($_smarty_tpl->tpl_vars['sub_view']->value == "template" || $_smarty_tpl->tpl_vars['sub_view']->value == "assign")) {?>class="color_grey btn btn-success btn-xs" <?php } else { ?>class="btn-success btn btn-xs"<?php }?>>5. <?php echo __("Pickup settings");?>
</a>
                            <?php } else { ?>
                                <button class="btn btn-default btn-xs">5. <?php echo __("Pickup settings");?>
</button>
                            <?php }?>
                            <i class="fas fa-hand-point-right"></i>
                            <?php if ($_smarty_tpl->tpl_vars['school']->value['school_step'] >= 7) {?>
                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/children" <?php if ($_smarty_tpl->tpl_vars['view']->value == "children") {?>class="color_grey btn btn-success btn-xs" <?php } else { ?>class="btn-success btn btn-xs"<?php }?>>6. <?php echo __("Child");?>
</a>
                            <?php } else { ?>
                                <button class="btn btn-default btn-xs">6. <?php echo __("Child");?>
</button>
                            <?php }?>

                        <?php } else { ?>
                            <button class="btn btn-default btn-xs">2. <?php echo __("Class");?>
</button>
                            <i class="fas fa-hand-point-right"></i>
                            <button class="btn btn-default btn-xs">3. <?php echo __("Teacher");?>
</button>
                            <i class="fas fa-hand-point-right"></i>
                            <button class="btn btn-default btn-xs">4. <?php echo __("Tuition settings");?>
</button>
                            <i class="fas fa-hand-point-right"></i>
                            <button class="btn btn-default btn-xs">5. <?php echo __("Pickup settings");?>
</button>
                            <i class="fas fa-hand-point-right"></i>
                            <button class="btn btn-default btn-xs">6. <?php echo __("Child");?>
</button>
                        <?php }?>
                    </div>
                    <div class="sub_step">
                        <?php if ($_smarty_tpl->tpl_vars['view']->value == "fees" || $_smarty_tpl->tpl_vars['view']->value == "services") {?>
                            <?php if ($_smarty_tpl->tpl_vars['school']->value['school_step'] >= 3) {?>
                                <?php if (count($_smarty_tpl->tpl_vars['servicesStep']->value) > 0) {?>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/services" <?php if ($_smarty_tpl->tpl_vars['view']->value == "services") {?>class="color_sub" <?php } else { ?>class=""<?php }?>>4.1. <?php echo __("Add new service");?>
</a>
                                <?php } else { ?>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/services/add" <?php if ($_smarty_tpl->tpl_vars['view']->value == "services") {?>class="color_sub" <?php } else { ?>class=""<?php }?>>4.1. <?php echo __("Add new service");?>
</a>
                                <?php }?>
                                <i class="fas fa-hand-point-right"></i>
                                <?php if ($_smarty_tpl->tpl_vars['school']->value['school_step'] >= 4) {?>
                                    <?php if (count($_smarty_tpl->tpl_vars['feesStep']->value) > 0) {?>
                                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/fees" <?php if ($_smarty_tpl->tpl_vars['view']->value == "fees") {?>class="color_sub" <?php } else { ?>class=""<?php }?>>4.2. <?php echo __("Add New Fee");?>
</a>
                                    <?php } else { ?>
                                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/fees/add" <?php if ($_smarty_tpl->tpl_vars['view']->value == "fees") {?>class="color_sub" <?php } else { ?>class=""<?php }?>>4.2. <?php echo __("Add New Fee");?>
</a>
                                    <?php }?>
                                <?php } else { ?>
                                    <a href="#" style="color: #5e5e5e">4.2. <?php echo __("Add New Fee");?>
</a>
                                <?php }?>
                            <?php }?>
                        <?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['view']->value == "pickup") {?>
                            <?php if ($_smarty_tpl->tpl_vars['school']->value['school_step'] >= 5) {?>
                                 <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/pickup/template" <?php if ($_smarty_tpl->tpl_vars['view']->value == "pickup" && $_smarty_tpl->tpl_vars['sub_view']->value == "template") {?>class="color_sub " <?php } else { ?>class=""<?php }?>>5.1. <?php echo __("Setting and create price table");?>
</a>
                                <i class="fas fa-hand-point-right"></i>
                                <?php if ($_smarty_tpl->tpl_vars['school']->value['school_step'] >= 6) {?>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/pickup/assign" <?php if ($_smarty_tpl->tpl_vars['view']->value == "pickup" && $_smarty_tpl->tpl_vars['sub_view']->value == "assign") {?>class="color_sub" <?php } else { ?>class=""<?php }?>>5.2. <?php echo __("Assign teacher");?>
</a>
                                <?php } else { ?>
                                    <a href="#" style="color: #5e5e5e">5.2. <?php echo __("Assign teacher");?>
</a>
                                <?php }?>
                            <?php }?>
                        <?php }?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php }
}
}
