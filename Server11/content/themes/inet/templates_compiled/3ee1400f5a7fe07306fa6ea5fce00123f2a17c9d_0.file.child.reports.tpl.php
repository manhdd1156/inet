<?php
/* Smarty version 3.1.31, created on 2021-06-23 11:44:34
  from "D:\workplace\Inet-project\source\Server11\content\themes\inet\templates\ci\child\child.reports.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_60d2bc320453a6_28967941',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '3ee1400f5a7fe07306fa6ea5fce00123f2a17c9d' => 
    array (
      0 => 'D:\\workplace\\Inet-project\\source\\Server11\\content\\themes\\inet\\templates\\ci\\child\\child.reports.tpl',
      1 => 1624423470,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60d2bc320453a6_28967941 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == "detail") {?>
            <div class="pull-right flip">
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/child/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
/reports" class="btn btn-default">
                    <?php echo __("Lists");?>

                </a>
            </div>
        <?php }?>
        <i class="fa fa-book fa-lg fa-fw pr10"></i>
        <?php echo __("Contact book");?>

        <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
            &rsaquo; <?php echo __('Lists');?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "detail") {?>
            &rsaquo; <?php echo __('Detail');?>

        <?php }?>
    </div>
    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
        <div class="panel-body with-table">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover js_dataTable">
                    <thead>
                    <tr><th colspan="3"><?php echo __("Contact book list");?>
&nbsp;(<?php echo count($_smarty_tpl->tpl_vars['rows']->value);?>
)</th></tr>
                    <tr>
                        <th><?php echo __("#");?>
</th>
                        <th><?php echo __("Title");?>
</th>
                        <th><?php echo __("Date");?>
</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['rows']->value, 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
                        <tr>
                            <td>
                                <div>
                                    <center><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</center>
                                </div>
                            </td>
                            <td>
                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/child/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
/reports/detail/<?php echo $_smarty_tpl->tpl_vars['row']->value['report_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['row']->value['report_name'];?>
</a>
                            </td>
                            <td>
                                <center><?php echo $_smarty_tpl->tpl_vars['row']->value['date'];?>
</center>
                            </td>
                        </tr>
                        <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                    </tbody>
                </table>
            </div>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "detail") {?>
        <div class="panel-body with-table">
            <table class = "table table-bordered">
                <tbody>
                <tr>
                    <td class = "col-sm-3 text-right"><?php echo __('Title');?>
</td>
                    <td>
                        <strong><?php echo $_smarty_tpl->tpl_vars['data']->value['report_name'];?>
</strong>
                    </td>
                </tr>
                <tr>
                    <td class = "col-sm-3 text-right"><?php echo __('Child');?>
</td>
                    <td>
                        <strong><?php echo $_smarty_tpl->tpl_vars['child']->value['child_name'];?>
 - <?php echo $_smarty_tpl->tpl_vars['child']->value['birthday'];?>
</strong>
                    </td>
                </tr>
                <tr>
                    <td class = "col-sm-3 text-right"><?php echo __("File attachment");?>
</td>
                    <td>
                        <?php if (!is_empty($_smarty_tpl->tpl_vars['data']->value['source_file'])) {?>
                            <a href = "<?php echo $_smarty_tpl->tpl_vars['data']->value['source_file'];?>
" target="_blank"><strong>
                                    <?php echo __("File attachment");?>

                                </strong>
                            </a>
                        <?php } else { ?>
                            <?php echo __("No file attachment");?>

                        <?php }?>
                    </td>
                </tr>
                </tbody>
            </table>
            <div class = "table-responsive">
                <table class = "table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th> # </th>
                        <th> <?php echo __('Title');?>
 </th>
                        <th><?php echo __('Content');?>
 </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['data']->value['details'], 'row', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['row']->value) {
?>
                        <?php if ($_smarty_tpl->tpl_vars['row']->value['report_category_content'] != '' || !empty($_smarty_tpl->tpl_vars['row']->value['multi_content'])) {?>
                            <tr>
                                <td align="center">
                                    <strong><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</strong>
                                </td>
                                <td>
                                    <strong><?php echo $_smarty_tpl->tpl_vars['row']->value['report_category_name'];?>
</strong>
                                </td>
                                <td>
                                    <div class="mb10">
                                        <?php echo $_smarty_tpl->tpl_vars['row']->value['report_category_content'];?>

                                    </div>
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['row']->value['multi_content'], 'suggest');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['suggest']->value) {
?>
                                        <div class="form-group">
                                            <i class="fa fa-check" aria-hidden="true"></i> <?php echo $_smarty_tpl->tpl_vars['suggest']->value;?>

                                        </div>
                                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                </td>
                            </tr>
                            <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                        <?php }?>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                    </tbody>
                </table>
            </div>
            <?php if ($_smarty_tpl->tpl_vars['point_module']->value == 1) {?>
                <div style="<?php if ($_smarty_tpl->tpl_vars['score_fomula']->value != 'vn') {?>width: 75%<?php }?>;margin-left: auto;margin-right: auto;overflow-x:auto;">
                    <strong style="float: right"><?php echo __("Status ");?>
 :
                        <?php if ($_smarty_tpl->tpl_vars['status']->value == 'Pass') {?>
                            <strong style="color:lawngreen"><?php ob_start();
echo $_smarty_tpl->tpl_vars['status']->value;
$_prefixVariable1=ob_get_clean();
echo __($_prefixVariable1);?>
</strong>
                        <?php } elseif ($_smarty_tpl->tpl_vars['status']->value == 'Fail') {?>
                            <strong style="color:red"><?php ob_start();
echo $_smarty_tpl->tpl_vars['status']->value;
$_prefixVariable2=ob_get_clean();
echo __($_prefixVariable2);?>
</strong>
                        <?php } elseif ($_smarty_tpl->tpl_vars['status']->value == 'Re-exam') {?>
                            <strong style="color:orange"><?php ob_start();
echo $_smarty_tpl->tpl_vars['status']->value;
$_prefixVariable3=ob_get_clean();
echo __($_prefixVariable3);?>
</strong>
                        <?php } else { ?>
                            <strong><?php ob_start();
echo $_smarty_tpl->tpl_vars['status']->value;
$_prefixVariable4=ob_get_clean();
echo __($_prefixVariable4);?>
</strong>
                        <?php }?>

                    </strong>
                    <table class="table table-striped table-bordered" style="z-index: 1;">
                        <thead>
                        <tr bgcolor="#fff">
                            <th rowspan="2" class="pinned" style="padding: 8px 6px" id="width_no">#</th>
                            <th rowspan="2" nowrap="true" class="pinned"><?php echo __("Subject name");?>
</th>
                            <th colspan="<?php if ($_smarty_tpl->tpl_vars['score_fomula']->value == 'vn') {
echo $_smarty_tpl->tpl_vars['column_hk2']->value+$_smarty_tpl->tpl_vars['column_gk2']->value+2;
} else { ?>4<?php }?>"><?php echo __("Semester 1");?>
</th>
                            <th colspan="<?php if ($_smarty_tpl->tpl_vars['score_fomula']->value == 'vn') {
echo $_smarty_tpl->tpl_vars['column_hk2']->value+$_smarty_tpl->tpl_vars['column_gk2']->value+2;
} else { ?>4<?php }?>"><?php echo __("Semester 2");?>
</th>
                        </tr>
                        <tr>
                            <?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable(null, $_smarty_tpl->isRenderingCache);
$_smarty_tpl->tpl_vars['i']->value = 1;
if ($_smarty_tpl->tpl_vars['i']->value <= $_smarty_tpl->tpl_vars['column_hk1']->value) {
for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value <= $_smarty_tpl->tpl_vars['column_hk1']->value; $_smarty_tpl->tpl_vars['i']->value++) {
?>
                                <th><?php if ($_smarty_tpl->tpl_vars['score_fomula']->value == 'vn') {?>tx<?php } else { ?>M<?php echo $_smarty_tpl->tpl_vars['i']->value;
}?></th>
                            <?php }
}
?>

                            <?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable(null, $_smarty_tpl->isRenderingCache);
$_smarty_tpl->tpl_vars['i']->value = 0;
if ($_smarty_tpl->tpl_vars['i']->value < $_smarty_tpl->tpl_vars['column_gk1']->value) {
for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value < $_smarty_tpl->tpl_vars['column_gk1']->value; $_smarty_tpl->tpl_vars['i']->value++) {
?>
                                <th><?php if ($_smarty_tpl->tpl_vars['score_fomula']->value == 'vn') {?>gk<?php } else { ?>D1<?php }?></th>
                            <?php }
}
?>

                            <?php if ($_smarty_tpl->tpl_vars['score_fomula']->value == 'vn') {?>
                                <th>ck</th>
                                <th><?php echo __("Average");?>
</th>
                            <?php }?>
                            <?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable(null, $_smarty_tpl->isRenderingCache);
$_smarty_tpl->tpl_vars['i']->value = 1;
if ($_smarty_tpl->tpl_vars['i']->value <= $_smarty_tpl->tpl_vars['column_hk2']->value) {
for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value <= $_smarty_tpl->tpl_vars['column_hk2']->value; $_smarty_tpl->tpl_vars['i']->value++) {
?>
                                <th><?php if ($_smarty_tpl->tpl_vars['score_fomula']->value == 'vn') {?>tx<?php } else { ?>M<?php echo $_smarty_tpl->tpl_vars['i']->value;
}?></th>
                            <?php }
}
?>

                            <?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable(null, $_smarty_tpl->isRenderingCache);
$_smarty_tpl->tpl_vars['i']->value = 0;
if ($_smarty_tpl->tpl_vars['i']->value < $_smarty_tpl->tpl_vars['column_gk2']->value) {
for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value < $_smarty_tpl->tpl_vars['column_gk2']->value; $_smarty_tpl->tpl_vars['i']->value++) {
?>
                                <th><?php if ($_smarty_tpl->tpl_vars['score_fomula']->value == 'vn') {?>gk<?php } else { ?>D2<?php }?></th>
                            <?php }
}
?>

                            <?php if ($_smarty_tpl->tpl_vars['score_fomula']->value == 'vn') {?>
                                <th>ck</th>
                                <th><?php echo __("Average");?>
</th>
                                <th>Reexam</th>
                            <?php }?>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $_smarty_tpl->_assignInScope('rowIdx', 1);
?>
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['rows']->value, 'row', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['row']->value) {
?>
                            <tr>
                                <td align="center" class="pinned"><?php echo $_smarty_tpl->tpl_vars['rowIdx']->value;?>
</td>

                                <td nowrap="true" class="pinned text-bold color-blue">
                                    <strong><?php echo $_smarty_tpl->tpl_vars['row']->value['subject_name'];?>
</strong>
                                </td>
                                <?php $_smarty_tpl->_assignInScope('rowIdx', $_smarty_tpl->tpl_vars['rowIdx']->value+1);
?>
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['subject_key']->value, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value) {
?>
                                    <td><?php echo $_smarty_tpl->tpl_vars['row']->value[strtolower($_smarty_tpl->tpl_vars['key']->value)];?>
</td>
                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                            </tr>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        
                        <?php if ($_smarty_tpl->tpl_vars['score_fomula']->value == 'vn') {?>
                        <tr>
                            <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                                <strong><?php echo __("End Semester");?>
</strong>
                            </td>
                            <td colspan="<?php echo $_smarty_tpl->tpl_vars['column_hk1']->value+$_smarty_tpl->tpl_vars['column_gk1']->value+1;?>
"></td>
                            <td><?php echo number_format($_smarty_tpl->tpl_vars['tb_total_hk1']->value,2);?>
</td>
                            <td colspan="<?php echo $_smarty_tpl->tpl_vars['column_hk2']->value+$_smarty_tpl->tpl_vars['column_gk2']->value+1;?>
"></td>
                            <td><?php echo number_format($_smarty_tpl->tpl_vars['tb_total_hk2']->value,2);?>
</td>
                            <td colspan="1"></td>
                        </tr>
                        
                        <tr>
                            <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                                <strong><?php echo __("End year");?>
</strong>
                            </td>
                            <td colspan="<?php echo $_smarty_tpl->tpl_vars['column_hk1']->value+$_smarty_tpl->tpl_vars['column_gk1']->value+2+$_smarty_tpl->tpl_vars['column_hk2']->value+$_smarty_tpl->tpl_vars['column_gk2']->value+2+1;?>
"
                                style="text-align: center"><?php echo number_format($_smarty_tpl->tpl_vars['tb_total_year']->value,2);?>
</td>
                        </tr>
                        
                        <tr>
                            <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                                <strong><?php echo __("Absent with permission");?>
</strong>
                            </td>
                            <td colspan="<?php echo $_smarty_tpl->tpl_vars['column_hk1']->value+$_smarty_tpl->tpl_vars['column_gk1']->value+2+$_smarty_tpl->tpl_vars['column_hk2']->value+$_smarty_tpl->tpl_vars['column_gk2']->value+2+1;?>
"
                                style="text-align: center"><?php echo $_smarty_tpl->tpl_vars['child_absent']->value['absent_true'];?>
</td>
                        </tr>
                        
                        <tr>
                            <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                                <strong><?php echo __("Absent without permission");?>
</strong>
                            </td>
                            <td colspan="<?php echo $_smarty_tpl->tpl_vars['column_hk1']->value+$_smarty_tpl->tpl_vars['column_gk1']->value+2+$_smarty_tpl->tpl_vars['column_hk2']->value+$_smarty_tpl->tpl_vars['column_gk2']->value+2+1;?>
"
                                style="text-align: center"><?php echo $_smarty_tpl->tpl_vars['child_absent']->value['absent_false'];?>
</td>
                        </tr>
                        </tbody>
                    </table>

                    <?php } elseif ($_smarty_tpl->tpl_vars['score_fomula']->value == 'km') {?>
                    <tr>
                        <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                            <strong><?php echo __("Average monthly");?>
</strong>
                        </td>
                        <td><?php echo number_format($_smarty_tpl->tpl_vars['children_point_avgs']->value['a1'],2);?>
</td>
                        <td><?php echo number_format($_smarty_tpl->tpl_vars['children_point_avgs']->value['b1'],2);?>
</td>
                        <td><?php echo number_format($_smarty_tpl->tpl_vars['children_point_avgs']->value['c1'],2);?>
</td>
                        <td><?php echo number_format($_smarty_tpl->tpl_vars['children_point_avgs']->value['d1'],2);?>
</td>
                        <td><?php echo number_format($_smarty_tpl->tpl_vars['children_point_avgs']->value['a2'],2);?>
</td>
                        <td><?php echo number_format($_smarty_tpl->tpl_vars['children_point_avgs']->value['b2'],2);?>
</td>
                        <td><?php echo number_format($_smarty_tpl->tpl_vars['children_point_avgs']->value['c2'],2);?>
</td>
                        <td><?php echo number_format($_smarty_tpl->tpl_vars['children_point_avgs']->value['d2'],2);?>
</td>

                    </tr>
                    <tr>
                        <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                            <strong><?php echo __("Average semesterly");?>
</strong>
                        </td>
                        <td colspan="3" style="text-align: center"><?php echo number_format($_smarty_tpl->tpl_vars['children_point_avgs']->value['x1'],2);?>
</td>
                        <td></td>
                        <td colspan="3" style="text-align: center"><?php echo number_format($_smarty_tpl->tpl_vars['children_point_avgs']->value['x2'],2);?>
</td>
                        <td></td>
                    </tr>
                    
                    <tr>
                        <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                            <strong><?php echo __("End semester");?>
</strong>
                        </td>
                        <td colspan="4" style="text-align: center"><?php echo number_format($_smarty_tpl->tpl_vars['children_point_avgs']->value['e1'],2);?>
</td>
                        <td colspan="4" style="text-align: center"><?php echo number_format($_smarty_tpl->tpl_vars['children_point_avgs']->value['e2'],2);?>
</td>
                    </tr>
                    
                    <tr>
                        <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                            <strong><?php echo __("End year");?>
</strong>
                        </td>
                        <td colspan="8" style="text-align: center"><?php echo number_format($_smarty_tpl->tpl_vars['children_point_avgs']->value['y'],2);?>
</td>
                    </tr>
                    
                    <tr>
                        <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                            <strong><?php echo __("Absent has permission");?>
</strong>
                        </td>
                        <td colspan="8" style="text-align: center"><?php echo $_smarty_tpl->tpl_vars['child_absent']->value['absent_true'];?>
</td>
                    </tr>
                    
                    <tr>
                        <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                            <strong><?php echo __("Absent without permission");?>
</strong>
                        </td>
                        <td colspan="8" style="text-align: center"><?php echo $_smarty_tpl->tpl_vars['child_absent']->value['absent_false'];?>
</td>
                    </tr>
                    </tbody>
                    </table>
                    <strong><?php echo __("Re-Exam");?>
</strong>
                    <table class="table table-striped table-bordered" style="z-index: 1;">
                        <thead>
                        <tr bgcolor="#fff">
                            <th rowspan="2" class="pinned" style="padding: 8px 6px" id="width_no">#</th>
                            <th rowspan="2" nowrap="true" class="pinned"
                                style="padding: 8px 6px"><?php echo __("Subject name");?>

                            </th>
                            <th colspan="1"><?php echo __("Point");?>
</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $_smarty_tpl->_assignInScope('rowIdx', 1);
?>
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['children_subject_reexams']->value, 'row', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['row']->value) {
?>
                            <tr>
                                <td align="center" class="pinned"><?php echo $_smarty_tpl->tpl_vars['rowIdx']->value;?>
</td>
                                <td nowrap="true" class="pinned text-bold color-blue">
                                    <strong><?php echo $_smarty_tpl->tpl_vars['row']->value['name'];?>
</strong>
                                </td>
                                <td style="text-align: center"><?php echo $_smarty_tpl->tpl_vars['row']->value['point'];?>
</td>
                                <?php $_smarty_tpl->_assignInScope('rowIdx', $_smarty_tpl->tpl_vars['rowIdx']->value+1);
?>
                            </tr>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        <tr>
                            <td colspan="2" nowrap="true" class="pinned text-bold color-blue"
                                style="text-align: center">
                                <strong><?php echo __("Result Re-exam");?>
</strong>
                            </td>
                            <td colspan="3" style="text-align: center"><?php echo number_format($_smarty_tpl->tpl_vars['result_exam']->value,2);?>
</td>
                        </tr>
                        </tbody>
                    </table>
                    <?php }?>
                </div>
            <?php }?>
        </div>
    <?php }?>
</div><?php }
}
