<?php
/* Smarty version 3.1.31, created on 2021-05-11 15:02:47
  from "D:\workplace\mascom-edu-server\Server11\content\themes\inet\templates\ci\child\child.points.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_609a3a27c8c4c7_98503630',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'a43c28633bcec577459cccecbc7f10ee9dfd328c' => 
    array (
      0 => 'D:\\workplace\\mascom-edu-server\\Server11\\content\\themes\\inet\\templates\\ci\\child\\child.points.tpl',
      1 => 1620640357,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_609a3a27c8c4c7_98503630 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <i class="fa fa-tasks fa-fw fa-lg pr10"></i>
        <?php echo __('Point');?>

        <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == "detail") {?>
            &rsaquo; <?php echo __('Detail');?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "comment") {?>
            &rsaquo; <?php echo __('Comment');?>

        <?php }?>
    </div>
    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == "detail") {?>
        <div class="panel-body">
            <form class="form-horizontal" action="#" enctype="multipart/form-data" method="post" id="point_search_form">
                <input type="hidden" name="schoolId" id="schoolId" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['school_id'];?>
"/>
                <input type="hidden" name="classId" id="classId" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['class_id'];?>
"/>
                <input type="hidden" name="childId" id="childId" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
"/>
                <input type="hidden" name="do" value="detail"/>
                <div class="form-group">

                    <div class="col-sm-3 col-sm-offset-9">
                        <select name="school_year" id="school_year" class="form-control">
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['default_year']->value, 'year', false, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['year']->value) {
?>
                                
                                <option value="<?php echo $_smarty_tpl->tpl_vars['year']->value;?>
"
                                        <?php if ((substr($_smarty_tpl->tpl_vars['year']->value,0,strlen(date("Y"))) === date("Y"))) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['year']->value;?>
</option>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo $_smarty_tpl->tpl_vars['child']->value['child_code'];?>
</label>
                    <label class="col-sm-3 control-label text-left"><?php echo $_smarty_tpl->tpl_vars['child']->value['child_name'];?>
</label>
                    <label class="col-sm-3 control-label text-left"><?php echo $_smarty_tpl->tpl_vars['child']->value['birthday'];?>
</label>
                </div>
                <?php if ($_smarty_tpl->tpl_vars['grade']->value == 1) {?>
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-3">
                            <select name="is_last_semester" id="is_last_semester" class="form-control">
                                <option value="0"><?php echo __("Mid semester");?>
</option>
                                <option value="1"><?php echo __("Last semester");?>
</option>
                            </select>
                        </div>
                    </div>
                <?php }?>
                <div id="child_list_point"></div>
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
            </form>

        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "comment") {?>
        <div class="panel-body">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/child/bochild_point.php">
                <input type="hidden" name="schoolId" id="schoolId" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['school_id'];?>
"/>
                <input type="hidden" name="classId" id="classId" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['class_id'];?>
"/>
                <input type="hidden" name="childId" id="childId" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
"/>
                <input type="hidden" name="do" value="comment"/>
                <div class="form-group">
                    <div class="col-sm-3 col-sm-offset-9">
                        <select name="school_year" id="school_year" class="form-control">
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['default_year']->value, 'year', false, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['year']->value) {
?>
                                
                                <option value="<?php echo $_smarty_tpl->tpl_vars['year']->value;?>
"
                                        <?php if ((substr($_smarty_tpl->tpl_vars['year']->value,0,strlen(date("Y"))) === date("Y"))) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['year']->value;?>
</option>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </select>
                    </div>
                </div>
                <div class="form-group">

                    <label class="col-sm-3 control-label text-left"><?php echo $_smarty_tpl->tpl_vars['child']->value['child_code'];?>
</label>
                    <label class="col-sm-3 control-label text-left"><?php echo $_smarty_tpl->tpl_vars['child']->value['child_name'];?>
</label>
                    <label class="col-sm-3 control-label text-left"><?php echo $_smarty_tpl->tpl_vars['child']->value['birthday'];?>
</label>
                    <div class="col-sm-3">
                        <select id="semester" name="semester" class="form-control">
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['semesters']->value, 'name', false, 'semester');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['semester']->value => $_smarty_tpl->tpl_vars['name']->value) {
?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['semester']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['name']->value;?>
</option>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </select>
                    </div>
                </div>
                <?php if ($_smarty_tpl->tpl_vars['grade']->value == 1) {?>
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-3">
                            <select name="is_last_semester" id="is_last_semester" class="form-control">
                                <option value="0"><?php echo __("Mid semester");?>
</option>
                                <option value="1"><?php echo __("Last semester");?>
</option>
                            </select>
                        </div>
                    </div>
                <?php }?>
                <div class="table-responsive" id="child_comment"></div>







                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
            </form>

        </div>
    <?php }?>
</div><?php }
}
