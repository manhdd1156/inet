<?php
/* Smarty version 3.1.31, created on 2021-05-24 17:16:40
  from "D:\workplace\Inet-project\source\Server11\content\themes\inet\templates\ci\school\ajax.searchteacher.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_60ab7d08864a96_18905157',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '78f32d0d430adebdbc781d057ac9fd878fc0c1fd' => 
    array (
      0 => 'D:\\workplace\\Inet-project\\source\\Server11\\content\\themes\\inet\\templates\\ci\\school\\ajax.searchteacher.tpl',
      1 => 1621501239,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60ab7d08864a96_18905157 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="js_scroller">
    <ul>
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['results']->value, '_user');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['_user']->value) {
?>
            <li class="feeds-item" data-id="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_name'];?>
">
                <div class="data-container <?php if ($_smarty_tpl->tpl_vars['_small']->value) {?>small<?php }?>">
                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_name'];?>
">
                        <img class="data-avatar" src="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_picture'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_fullname'];?>
">
                    </a>
                    <div class="data-content">
                        <div class="pull-right flip">
                            <div class="btn btn-default js_teacher-select" data-uid="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_id'];?>
" data-ufullname="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_fullname'];?>
"><?php echo __("Add");?>
</div>
                        </div>
                        <div>
                            <span class="name js_user-popover" data-uid="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_id'];?>
">
                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_name'];?>
"><?php echo $_smarty_tpl->tpl_vars['_user']->value['user_fullname'];?>
</a>
                            </span>
                        </div>
                    </div>
                </div>
            </li>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

    </ul>
</div><?php }
}
