<?php
/* Smarty version 3.1.31, created on 2021-05-20 17:00:17
  from "D:\workplace\Inet-project\source\Server11\content\themes\inet\templates\ci\school\school.pickupteacher.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_60a63331599e62_95320511',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '9333943e4ed64c930ce5f65bf1e3e8811d643698' => 
    array (
      0 => 'D:\\workplace\\Inet-project\\source\\Server11\\content\\themes\\inet\\templates\\ci\\school\\school.pickupteacher.tpl',
      1 => 1621501239,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:ci/school/teacher_pickup/ajax.pickuplist.tpl' => 1,
    'file:ci/school/teacher_pickup/ajax.pickup.detail.tpl' => 1,
    'file:ci/school/teacher_pickup/ajax.pickupchild.tpl' => 1,
  ),
),false)) {
function content_60a63331599e62_95320511 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == "manage" || ($_smarty_tpl->tpl_vars['sub_view']->value == "detail" && $_smarty_tpl->tpl_vars['allow_teacher_edit_pickup_before']->value)) {?>
            <div class="pull-right flip">
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/pickupteacher/add/<?php echo $_smarty_tpl->tpl_vars['pickup']->value['pickup_id'];?>
" class="btn btn-default">
                    <i class="fa fa-plus"></i> <?php echo __("Add child");?>

                </a>
                <a href="https://blog.coniu.vn/huong-dan-tra-tre-don-muon-cho-giao-vien/" class="btn btn-info btn_guide" target="_blank">
                    <i class="fa fa-info"></i> <?php echo __("Guide");?>

                </a>
            </div>
        <?php }?>

        <i class="fa fa-life-ring fa-fw fa-lg pr10"></i>
        <?php echo __("Late pickup");?>

        <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
            &rsaquo; <?php echo __("Today lists");?>

        <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == "manage") {?>
            &rsaquo; <?php echo __('Add students - Pickup');?>

        <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == "history") {?>
            &rsaquo; <?php echo __('History');?>

        <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == "detail") {?>
            &rsaquo; <?php echo __('Detail');?>

        <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == "assign") {?>
            &rsaquo; <?php echo __('Assign');?>

        <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
            &rsaquo; <?php echo __('Add child');?>

        <?php }?>
    </div>
    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
        <div class="panel-body with-table">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/class/boclass_pickup.php">
                <input type="hidden" id="username" name="username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                <input type="hidden" name="do" value="add"/>

                <div class="text-center">
                    <strong>
                        <label style="font-size: 15px"><?php echo mb_strtoupper(__("Late pickup on"), 'UTF-8');?>
: <?php echo $_smarty_tpl->tpl_vars['today']->value;?>
</label>
                    </strong><br><br>
                </div>

                <div class="table-responsive" id="pickup_class_list">
                    <?php ob_start();
echo count($_smarty_tpl->tpl_vars['children']->value);
$_prefixVariable1=ob_get_clean();
$_smarty_tpl->_assignInScope('children_count', $_prefixVariable1);
?>
                    <div class="text-left">
                        <strong><?php echo __("Total");?>
: <?php echo $_smarty_tpl->tpl_vars['children_count']->value;?>
 |
                            <?php echo __("Registered");?>
: <?php echo $_smarty_tpl->tpl_vars['child_count']->value;?>

                        </strong>
                    </div>
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th width="15%">#</th>
                            <th width="35%"><?php echo __("Children");?>
</th>
                            <th width="20%"><?php echo __("Status");?>
</th>
                            <th width="30%"><?php echo __("Class");?>
</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['children']->value, 'child');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['child']->value) {
?>
                            <tr <?php if ($_smarty_tpl->tpl_vars['child']->value['child_status'] == 0) {?> class="row-disable"<?php }?>>
                                <td align="center"><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</td>
                                <td><strong class="pl20"><?php echo $_smarty_tpl->tpl_vars['child']->value['child_name'];?>
</strong></td>
                                <td align="center">
                                    <?php if (is_empty($_smarty_tpl->tpl_vars['child']->value['status'])) {?>
                                        <i class="fa fa-times" style="color:orangered" aria-hidden="true"></i>
                                    <?php } else { ?>
                                        <i class="fa fa-check" style="color:mediumseagreen" aria-hidden="true"></i>
                                    <?php }?>
                                </td>
                                <td align="center">
                                    <strong>
                                        <?php if (is_empty($_smarty_tpl->tpl_vars['child']->value['class_name'])) {?>
                                            &#150;
                                        <?php } else { ?>
                                            <?php echo $_smarty_tpl->tpl_vars['child']->value['class_name'];?>

                                        <?php }?>
                                    </strong>
                            </tr>
                            <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        <?php if ($_smarty_tpl->tpl_vars['children_count']->value == 0) {?>
                            <tr class="odd">
                                <td valign="top" align="center" colspan="4" class="dataTables_empty">
                                    <?php echo __("No data available in table");?>

                                </td>
                            </tr>
                        <?php }?>
                        </tbody>
                    </table>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "manage") {?>
        <div class="panel-body with-table">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_teacherpickup.php">
                <input type="hidden" id="school_username" name="school_username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                <input type="hidden" id="pickup_date" name="pickup_date" value="<?php echo $_smarty_tpl->tpl_vars['today']->value;?>
"/>
                <input type="hidden" id="pickup_id" name="pickup_id" value="<?php echo $_smarty_tpl->tpl_vars['pickup']->value['pickup_id'];?>
"/>
                <input type="hidden" id="callback" name="callback" value="manage"/>
                <input type="hidden" name="do" value="save"/>
                <div id="time_option" class="x-hidden" data-value="<?php echo $_smarty_tpl->tpl_vars['time_option']->value;?>
"></div>

                <div class="text-center">
                    
                    <strong><label style="font-size: 15px"><?php echo mb_strtoupper(__("Late pickup on"), 'UTF-8');?>
: <?php echo $_smarty_tpl->tpl_vars['today']->value;?>
</label></strong><br>
                    <?php if (!is_empty($_smarty_tpl->tpl_vars['pickup']->value['class_name'])) {?>
                        <strong><label style="font-size: 15px"><?php echo __("Class");?>
: <?php echo $_smarty_tpl->tpl_vars['pickup']->value['class_name'];?>
</label></strong><br><br>
                    <?php } else { ?>
                        <br>
                    <?php }?>
                </div>
                <div class="col-sm-12">
                    <label class="col-sm-6 text-right" style="font-size: 15px"><?php echo __("Assigned teachers");?>
:</label>
                    <?php if (count($_smarty_tpl->tpl_vars['pickup']->value['assign']) > 0) {?>
                        <div class="col-sm-6">
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['pickup']->value['assign'], 'assign');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['assign']->value) {
?>
                                <strong>
                                    <label>&#9679;&#09;<?php echo $_smarty_tpl->tpl_vars['assign']->value['user_fullname'];?>
</label>
                                </strong>
                                <br>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </div>
                    <?php } else { ?>
                        <div class="col-sm-6 text-left" style="font-size: 15px;color: red"><?php echo __("Not assigned");?>
</div>
                    <?php }?>
                </div>

                <div>
                    <strong> - <?php echo __("Note");?>
: <?php echo __('Ấn vào nút "');?>
<font color="red"><?php echo __("Pickup");?>
</font><?php echo __('" sẽ tính thời gian tại thời điểm ấn (không lưu dịch vụ, ghi chú), nếu có dịch vụ đi kèm và ghi chú vui lòng nhập vào thông tin và ấn vào nút');?>
 "<font color="red"><?php echo __("Save");?>
</font>"</strong>
                </div>
                <br/>
                <?php $_smarty_tpl->_assignInScope('children_count', count($_smarty_tpl->tpl_vars['children']->value));
?>
                <div>
                    <strong><?php echo __("Total student");?>
: <font color="red"><?php echo $_smarty_tpl->tpl_vars['children_count']->value;?>
</font> |
                        <?php echo __("Picked up");?>
: <font color="red"><?php echo $_smarty_tpl->tpl_vars['child_count']->value;?>
</font>
                    </strong>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th width="5%">#</th>
                            <th width="25%"><?php echo __("Student name");?>
</th>
                            <th width="20%"><?php echo __("Birthdate");?>
</th>
                            <th width="20%"><?php echo __("Class");?>
</th>
                            <th width="15%"><?php echo __("Status");?>
</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['children']->value, 'child');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['child']->value) {
?>
                            <input type="hidden" name="child[]" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
"/>
                            <tr>
                                <td align="center"><strong><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</strong></td>
                                <td><strong><?php echo $_smarty_tpl->tpl_vars['child']->value['child_name'];?>
</strong></td>
                                <td align="center"><?php echo $_smarty_tpl->tpl_vars['child']->value['birthday'];?>
</td>
                                <td align="center"><?php echo $_smarty_tpl->tpl_vars['child']->value['group_title'];?>
</td>
                                <td rowspan="4" align="center">
                                    <?php if (is_empty($_smarty_tpl->tpl_vars['child']->value['pickup_at'])) {?>
                                        <?php if ($_smarty_tpl->tpl_vars['is_today']->value) {?>
                                        <a class="btn btn-xs btn-default js_class-pickup-one-child" data-handle="pickup"
                                                data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-child="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
"
                                                data-pickup="<?php echo $_smarty_tpl->tpl_vars['child']->value['pickup_id'];?>
">
                                            <?php echo __("Pickup");?>

                                        </a>
                                        <?php }?>
                                    <?php }?>
                                        <a class="btn btn-xs btn-danger js_class-pickup" data-handle="cancel"
                                                data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-child="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
"
                                                data-pickup="<?php echo $_smarty_tpl->tpl_vars['child']->value['pickup_id'];?>
">
                                            <?php echo __("Cancel");?>

                                        </a>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="1" rowspan="3"></td>
                                <td colspan="3" style="padding-left: 50px">
                                    <div class="col-sm-4 pt3">
                                        <?php echo __("Pickup at");?>

                                        : &nbsp;
                                    </div>
                                    <?php if (!is_empty($_smarty_tpl->tpl_vars['child']->value['pickup_at'])) {?>
                                        <input type='text' name="pickup_time[]" class="pickup_teacher_time" id="pickup_time_<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['pickup_at'];?>
" style="width: 70px; color: blue; text-align: center"/>
                                    <?php } else { ?>
                                        <input type='text' name="pickup_time[]" class="pickup_teacher_time" id="pickup_time_<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
" value="" style="width: 70px; font-weight: bold;color: red; text-align: center"/>
                                    <?php }?>
                                    <input type="hidden" min="0"
                                           id="total_pickup_<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
"
                                           data-child="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
"
                                           name="pickup_fee[]" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['late_pickup_fee'];?>
"/>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" align="left" style="padding-left: 50px">
                                    <?php if (!empty($_smarty_tpl->tpl_vars['child']->value['services'])) {?>
                                        <div class="col-sm-4 pt3">
                                            <?php echo __("Using service");?>
: &nbsp;
                                        </div>
                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['child']->value['services'], 'service');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['service']->value) {
?>
                                            <input type="checkbox" class="pickup_service_fee" name="service_<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
[]"
                                                   id="pickup_service_fee_<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
_<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
"
                                                   data-child="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
"
                                                   data-service="<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
"
                                                   data-fee="<?php echo $_smarty_tpl->tpl_vars['service']->value['price'];?>
"
                                                   value="<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
" <?php if (!is_null($_smarty_tpl->tpl_vars['service']->value['using_at'])) {?> checked <?php }?>/>
                                            <strong><?php echo $_smarty_tpl->tpl_vars['service']->value['service_name'];?>
</strong>
                                            &nbsp;&nbsp;&nbsp;
                                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                    <?php } else { ?>
                                        &nbsp;
                                    <?php }?>

                                </td>

                            </tr>

                            <tr>
                                <td colspan="3" style="padding-left: 50px">
                                    <div class="col-sm-4 pt3">
                                        <?php echo __("Note");?>
 : &nbsp;
                                    </div>
                                    <input type='text' name="pickup_note[]" id="pickup_note_<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['description'];?>
" style="width: 60%; color: blue"/>
                                    <input type="hidden" min="0"
                                           id="total_<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
"
                                           name="total_child[]"
                                           value="<?php echo $_smarty_tpl->tpl_vars['child']->value['total_amount'];?>
"/>
                                </td>
                            </tr>
                            <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>


                        <?php if ($_smarty_tpl->tpl_vars['children_count']->value == 0) {?>
                            <tr class="odd">
                                <td valign="top" align="center" colspan="5" class="dataTables_empty">
                                    <?php echo __("No data available in table");?>

                                </td>
                            </tr>
                        <?php }?>
                        <input type="hidden" id="total" name="total" value="<?php echo $_smarty_tpl->tpl_vars['pickup']->value['total'];?>
"/>
                        </tbody>
                    </table>
                </div>

                <div class="form-group">
                    <div class="col-sm-9 pl10">
                        <button type="submit"
                                class="btn btn-primary padrl30 <?php if (count($_smarty_tpl->tpl_vars['children']->value) == 0) {?> x-hidden <?php }?>"><?php echo __("Save");?>
</button>
                    </div>
                </div>
                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->

            </form>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "history") {?>
        <div class="panel-body with-table">
            <div class="row">
                <div class='col-sm-12'>
                    <div class='col-sm-2'></div>
                    <div class='col-sm-3'>
                        <div class='input-group date' id='fromdatepicker'>
                            <input type='text' name="fromDate" id="fromDate" value="<?php echo $_smarty_tpl->tpl_vars['fromDate']->value;?>
" class="form-control" placeholder="<?php echo __("From date");?>
"/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                    <div class='col-sm-3'>
                        <div class='input-group date' id='todatepicker'>
                            <input type='text' name="toDate" id="toDate" value="<?php echo $_smarty_tpl->tpl_vars['toDate']->value;?>
" class="form-control" placeholder="<?php echo __("To date");?>
"/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                    <div class='col-sm-3'>
                        <div class="form-group">
                            <button class="btn btn-default js_pickup_teacher-search" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" ><?php echo __("Search");?>
</button>
                        </div>
                    </div>
                </div>
            </div>
            <br/>

            <div class="table-responsive" id="pickup_list">
                <?php $_smarty_tpl->_subTemplateRender("file:ci/school/teacher_pickup/ajax.pickuplist.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

            </div>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "detail") {?>
        <?php $_smarty_tpl->_subTemplateRender("file:ci/school/teacher_pickup/ajax.pickup.detail.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "assign") {?>
        <div class = "panel-body">
            <form class="js_ajax-forms form-horizontal">
                <input type="hidden" name="username" id="school_username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                <div class="form-group">
                    <label class="col-sm-5 control-label text-left"><?php echo __("Select date");?>
 (*)</label>
                    <div class='col-sm-3'>
                        <div class='input-group date' id='pickup_teacher_begin_assign'>
                            <input type='text' name="begin" id="begin" value="<?php echo $_smarty_tpl->tpl_vars['monday']->value;?>
" class="form-control" required/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                </div>

                <br>
                <div class="table-responsive <?php if ($_smarty_tpl->tpl_vars['classCnt']->value == 1) {?>col-sm-9<?php } else { ?>col-sm-12<?php }?>">
                    <table class="table table-striped table-bordered table-hover" id="tbody-assign">
                        <thead>
                        <tr>
                            <th class="col-sm-2"></th>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['classes']->value, 'class');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class']->value) {
?>
                                <th class="col-sm-3"><center><?php echo $_smarty_tpl->tpl_vars['class']->value['class_name'];?>
</center></th>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <?php $_smarty_tpl->_assignInScope('classCnt', count($_smarty_tpl->tpl_vars['classes']->value));
?>
                            <td class="align-middle" align="center">
                                <strong><?php echo __("Monday");?>
</strong> <br> <?php echo $_smarty_tpl->tpl_vars['day']->value['mon'];?>

                            </td>

                            <?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable(null, $_smarty_tpl->isRenderingCache);
$_smarty_tpl->tpl_vars['i']->value = 0;
if ($_smarty_tpl->tpl_vars['i']->value < $_smarty_tpl->tpl_vars['classCnt']->value) {
for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value < $_smarty_tpl->tpl_vars['classCnt']->value; $_smarty_tpl->tpl_vars['i']->value++) {
?>
                                <td class="align-middle">
                                    <div id="assign_class_2_<?php echo $_smarty_tpl->tpl_vars['classes']->value[$_smarty_tpl->tpl_vars['i']->value]['pickup_class_id'];?>
" class="assign_class pl10">
                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['data']->value[2], 'arr', false, 'class_id');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class_id']->value => $_smarty_tpl->tpl_vars['arr']->value) {
?>
                                            <?php if ($_smarty_tpl->tpl_vars['class_id']->value == $_smarty_tpl->tpl_vars['classes']->value[$_smarty_tpl->tpl_vars['i']->value]['pickup_class_id']) {?>
                                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arr']->value, 'info');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['info']->value) {
?>
                                                    <div id="assign_teacher_2_<?php echo $_smarty_tpl->tpl_vars['class_id']->value;?>
_<?php echo $_smarty_tpl->tpl_vars['info']->value['user_id'];?>
">
                                                        <a class="text-left <?php if ($_smarty_tpl->tpl_vars['info']->value['user_id'] == $_smarty_tpl->tpl_vars['user']->value->_data['user_id']) {?>strong<?php }?>">&#9679;&#09;<?php echo $_smarty_tpl->tpl_vars['info']->value['user_fullname'];?>
</a>
                                                        <input type="hidden" name="assign_day[]" value="2">
                                                        <input type="hidden" name="assign_class[]" value="<?php echo $_smarty_tpl->tpl_vars['class_id']->value;?>
">
                                                        <input type="hidden" name="assign_teacher[]" value="<?php echo $_smarty_tpl->tpl_vars['info']->value['user_id'];?>
">
                                                    </div>
                                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                            <?php }?>
                                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                    </div>
                                </td>
                            <?php }
}
?>


                        </tr>
                        <tr>
                            <td class="align-middle" align="center">
                                <strong><?php echo __("Tuesday");?>
</strong> <br> <?php echo $_smarty_tpl->tpl_vars['day']->value['tue'];?>

                            </td>

                            <?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable(null, $_smarty_tpl->isRenderingCache);
$_smarty_tpl->tpl_vars['i']->value = 0;
if ($_smarty_tpl->tpl_vars['i']->value < $_smarty_tpl->tpl_vars['classCnt']->value) {
for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value < $_smarty_tpl->tpl_vars['classCnt']->value; $_smarty_tpl->tpl_vars['i']->value++) {
?>
                                <td class="align-middle">
                                    <div id="assign_class_3_<?php echo $_smarty_tpl->tpl_vars['classes']->value[$_smarty_tpl->tpl_vars['i']->value]['pickup_class_id'];?>
" class="assign_class pl10">
                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['data']->value[3], 'arr', false, 'class_id');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class_id']->value => $_smarty_tpl->tpl_vars['arr']->value) {
?>
                                            <?php if ($_smarty_tpl->tpl_vars['class_id']->value == $_smarty_tpl->tpl_vars['classes']->value[$_smarty_tpl->tpl_vars['i']->value]['pickup_class_id']) {?>
                                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arr']->value, 'info');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['info']->value) {
?>
                                                    <div id="assign_teacher_3_<?php echo $_smarty_tpl->tpl_vars['class_id']->value;?>
_<?php echo $_smarty_tpl->tpl_vars['info']->value['user_id'];?>
">
                                                        <a class="text-left <?php if ($_smarty_tpl->tpl_vars['info']->value['user_id'] == $_smarty_tpl->tpl_vars['user']->value->_data['user_id']) {?>strong<?php }?>">&#9679;&#09;<?php echo $_smarty_tpl->tpl_vars['info']->value['user_fullname'];?>
</a>
                                                        <input type="hidden" name="assign_day[]" value="3">
                                                        <input type="hidden" name="assign_class[]" value="<?php echo $_smarty_tpl->tpl_vars['class_id']->value;?>
">
                                                        <input type="hidden" name="assign_teacher[]" value="<?php echo $_smarty_tpl->tpl_vars['info']->value['user_id'];?>
">
                                                    </div>
                                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                            <?php }?>
                                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                    </div>
                                </td>
                            <?php }
}
?>


                        </tr>
                        <tr>
                            <td class="align-middle" align="center">
                                <strong><?php echo __("Wednesday");?>
</strong> <br> <?php echo $_smarty_tpl->tpl_vars['day']->value['wed'];?>

                            </td>
                            <?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable(null, $_smarty_tpl->isRenderingCache);
$_smarty_tpl->tpl_vars['i']->value = 0;
if ($_smarty_tpl->tpl_vars['i']->value < $_smarty_tpl->tpl_vars['classCnt']->value) {
for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value < $_smarty_tpl->tpl_vars['classCnt']->value; $_smarty_tpl->tpl_vars['i']->value++) {
?>
                                <td class="align-middle">
                                    <div id="assign_class_4_<?php echo $_smarty_tpl->tpl_vars['classes']->value[$_smarty_tpl->tpl_vars['i']->value]['pickup_class_id'];?>
" class="assign_class pl10">
                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['data']->value[4], 'arr', false, 'class_id');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class_id']->value => $_smarty_tpl->tpl_vars['arr']->value) {
?>
                                            <?php if ($_smarty_tpl->tpl_vars['class_id']->value == $_smarty_tpl->tpl_vars['classes']->value[$_smarty_tpl->tpl_vars['i']->value]['pickup_class_id']) {?>
                                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arr']->value, 'info');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['info']->value) {
?>
                                                    <div id="assign_teacher_4_<?php echo $_smarty_tpl->tpl_vars['class_id']->value;?>
_<?php echo $_smarty_tpl->tpl_vars['info']->value['user_id'];?>
">
                                                        <a class="text-left <?php if ($_smarty_tpl->tpl_vars['info']->value['user_id'] == $_smarty_tpl->tpl_vars['user']->value->_data['user_id']) {?>strong<?php }?>">&#9679;&#09;<?php echo $_smarty_tpl->tpl_vars['info']->value['user_fullname'];?>
</a>
                                                        <input type="hidden" name="assign_day[]" value="4">
                                                        <input type="hidden" name="assign_class[]" value="<?php echo $_smarty_tpl->tpl_vars['class_id']->value;?>
">
                                                        <input type="hidden" name="assign_teacher[]" value="<?php echo $_smarty_tpl->tpl_vars['info']->value['user_id'];?>
">
                                                    </div>
                                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                            <?php }?>
                                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                    </div>
                                </td>
                            <?php }
}
?>

                        </tr>
                        <tr>
                            <td class="align-middle" align="center">
                                <strong><?php echo __("Thursday");?>
</strong> <br> <?php echo $_smarty_tpl->tpl_vars['day']->value['thu'];?>

                            </td>
                            <?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable(null, $_smarty_tpl->isRenderingCache);
$_smarty_tpl->tpl_vars['i']->value = 0;
if ($_smarty_tpl->tpl_vars['i']->value < $_smarty_tpl->tpl_vars['classCnt']->value) {
for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value < $_smarty_tpl->tpl_vars['classCnt']->value; $_smarty_tpl->tpl_vars['i']->value++) {
?>
                                <td class="align-middle">
                                    <div id="assign_class_5_<?php echo $_smarty_tpl->tpl_vars['classes']->value[$_smarty_tpl->tpl_vars['i']->value]['pickup_class_id'];?>
" class="assign_class pl10">
                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['data']->value[5], 'arr', false, 'class_id');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class_id']->value => $_smarty_tpl->tpl_vars['arr']->value) {
?>
                                            <?php if ($_smarty_tpl->tpl_vars['class_id']->value == $_smarty_tpl->tpl_vars['classes']->value[$_smarty_tpl->tpl_vars['i']->value]['pickup_class_id']) {?>
                                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arr']->value, 'info');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['info']->value) {
?>
                                                    <div id="assign_teacher_5_<?php echo $_smarty_tpl->tpl_vars['class_id']->value;?>
_<?php echo $_smarty_tpl->tpl_vars['info']->value['user_id'];?>
">
                                                        <a class="text-left <?php if ($_smarty_tpl->tpl_vars['info']->value['user_id'] == $_smarty_tpl->tpl_vars['user']->value->_data['user_id']) {?>strong<?php }?>">&#9679;&#09;<?php echo $_smarty_tpl->tpl_vars['info']->value['user_fullname'];?>
</a>
                                                        <input type="hidden" name="assign_day[]" value="5">
                                                        <input type="hidden" name="assign_class[]" value="<?php echo $_smarty_tpl->tpl_vars['class_id']->value;?>
">
                                                        <input type="hidden" name="assign_teacher[]" value="<?php echo $_smarty_tpl->tpl_vars['info']->value['user_id'];?>
">
                                                    </div>
                                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                            <?php }?>
                                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                    </div>
                                </td>
                            <?php }
}
?>

                        </tr>
                        <tr>
                            <td class="align-middle" align="center">
                                <strong><?php echo __("Friday");?>
</strong> <br> <?php echo $_smarty_tpl->tpl_vars['day']->value['fri'];?>

                            </td>
                            <?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable(null, $_smarty_tpl->isRenderingCache);
$_smarty_tpl->tpl_vars['i']->value = 0;
if ($_smarty_tpl->tpl_vars['i']->value < $_smarty_tpl->tpl_vars['classCnt']->value) {
for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value < $_smarty_tpl->tpl_vars['classCnt']->value; $_smarty_tpl->tpl_vars['i']->value++) {
?>
                                <td class="align-middle">
                                    <div id="assign_class_6_<?php echo $_smarty_tpl->tpl_vars['classes']->value[$_smarty_tpl->tpl_vars['i']->value]['pickup_class_id'];?>
" class="assign_class pl10">
                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['data']->value[6], 'arr', false, 'class_id');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class_id']->value => $_smarty_tpl->tpl_vars['arr']->value) {
?>
                                            <?php if ($_smarty_tpl->tpl_vars['class_id']->value == $_smarty_tpl->tpl_vars['classes']->value[$_smarty_tpl->tpl_vars['i']->value]['pickup_class_id']) {?>
                                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arr']->value, 'info');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['info']->value) {
?>
                                                    <div id="assign_teacher_6_<?php echo $_smarty_tpl->tpl_vars['class_id']->value;?>
_<?php echo $_smarty_tpl->tpl_vars['info']->value['user_id'];?>
">
                                                        <a class="text-left <?php if ($_smarty_tpl->tpl_vars['info']->value['user_id'] == $_smarty_tpl->tpl_vars['user']->value->_data['user_id']) {?>strong<?php }?>">&#9679;&#09;<?php echo $_smarty_tpl->tpl_vars['info']->value['user_fullname'];?>
</a>
                                                        <input type="hidden" name="assign_day[]" value="6">
                                                        <input type="hidden" name="assign_class[]" value="<?php echo $_smarty_tpl->tpl_vars['class_id']->value;?>
">
                                                        <input type="hidden" name="assign_teacher[]" value="<?php echo $_smarty_tpl->tpl_vars['info']->value['user_id'];?>
">
                                                    </div>
                                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                            <?php }?>
                                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                    </div>
                                </td>
                            <?php }
}
?>

                        </tr>
                        <tr>
                            <td class="align-middle" align="center">
                                <strong><?php echo __("Saturday");?>
</strong> <br> <?php echo $_smarty_tpl->tpl_vars['day']->value['sat'];?>

                            </td>
                            <?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable(null, $_smarty_tpl->isRenderingCache);
$_smarty_tpl->tpl_vars['i']->value = 0;
if ($_smarty_tpl->tpl_vars['i']->value < $_smarty_tpl->tpl_vars['classCnt']->value) {
for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value < $_smarty_tpl->tpl_vars['classCnt']->value; $_smarty_tpl->tpl_vars['i']->value++) {
?>
                                <td class="align-middle">
                                    <div id="assign_class_7_<?php echo $_smarty_tpl->tpl_vars['classes']->value[$_smarty_tpl->tpl_vars['i']->value]['pickup_class_id'];?>
" class="assign_class pl10">
                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['data']->value[7], 'arr', false, 'class_id');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class_id']->value => $_smarty_tpl->tpl_vars['arr']->value) {
?>
                                            <?php if ($_smarty_tpl->tpl_vars['class_id']->value == $_smarty_tpl->tpl_vars['classes']->value[$_smarty_tpl->tpl_vars['i']->value]['pickup_class_id']) {?>
                                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arr']->value, 'info');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['info']->value) {
?>
                                                    <div id="assign_teacher_7_<?php echo $_smarty_tpl->tpl_vars['class_id']->value;?>
_<?php echo $_smarty_tpl->tpl_vars['info']->value['user_id'];?>
">
                                                        <a class="text-left <?php if ($_smarty_tpl->tpl_vars['info']->value['user_id'] == $_smarty_tpl->tpl_vars['user']->value->_data['user_id']) {?>strong<?php }?>">&#9679;&#09;<?php echo $_smarty_tpl->tpl_vars['info']->value['user_fullname'];?>
</a>
                                                        <input type="hidden" name="assign_day[]" value="7">
                                                        <input type="hidden" name="assign_class[]" value="<?php echo $_smarty_tpl->tpl_vars['class_id']->value;?>
">
                                                        <input type="hidden" name="assign_teacher[]" value="<?php echo $_smarty_tpl->tpl_vars['info']->value['user_id'];?>
">
                                                    </div>
                                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                            <?php }?>
                                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                    </div>
                                </td>
                            <?php }
}
?>

                        </tr>
                        <tr>
                            <td class="align-middle" align="center">
                                <strong><?php echo __("Sunday");?>
</strong> <br> <?php echo $_smarty_tpl->tpl_vars['day']->value['sun'];?>

                            </td>
                            <?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable(null, $_smarty_tpl->isRenderingCache);
$_smarty_tpl->tpl_vars['i']->value = 0;
if ($_smarty_tpl->tpl_vars['i']->value < $_smarty_tpl->tpl_vars['classCnt']->value) {
for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value < $_smarty_tpl->tpl_vars['classCnt']->value; $_smarty_tpl->tpl_vars['i']->value++) {
?>
                                <td class="align-middle">
                                    <div id="assign_class_8_<?php echo $_smarty_tpl->tpl_vars['classes']->value[$_smarty_tpl->tpl_vars['i']->value]['pickup_class_id'];?>
" class="assign_class pl10">
                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['data']->value[8], 'arr', false, 'class_id');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class_id']->value => $_smarty_tpl->tpl_vars['arr']->value) {
?>
                                            <?php if ($_smarty_tpl->tpl_vars['class_id']->value == $_smarty_tpl->tpl_vars['classes']->value[$_smarty_tpl->tpl_vars['i']->value]['pickup_class_id']) {?>
                                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['arr']->value, 'info');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['info']->value) {
?>
                                                    <div id="assign_teacher_8_<?php echo $_smarty_tpl->tpl_vars['class_id']->value;?>
_<?php echo $_smarty_tpl->tpl_vars['info']->value['user_id'];?>
">
                                                        <a class="text-left <?php if ($_smarty_tpl->tpl_vars['info']->value['user_id'] == $_smarty_tpl->tpl_vars['user']->value->_data['user_id']) {?>strong<?php }?>">&#9679;&#09;<?php echo $_smarty_tpl->tpl_vars['info']->value['user_fullname'];?>
</a>
                                                        <input type="hidden" name="assign_day[]" value="8">
                                                        <input type="hidden" name="assign_class[]" value="<?php echo $_smarty_tpl->tpl_vars['class_id']->value;?>
">
                                                        <input type="hidden" name="assign_teacher[]" value="<?php echo $_smarty_tpl->tpl_vars['info']->value['user_id'];?>
">
                                                    </div>
                                                    <br>
                                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                            <?php }?>
                                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                    </div>
                                </td>
                            <?php }
}
?>

                        </tr>
                        </tbody>
                    </table>

                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
        <div class="panel-body with-table">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_teacherpickup.php">
                <input type="hidden" name="school_username" id="school_username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                <input type="hidden" name="pickup_id" id="pickup_id" value="<?php echo $_smarty_tpl->tpl_vars['pickup_id']->value;?>
"/>
                <input type="hidden" id="pickup_class_id" name="pickup_class_id" value="<?php echo $_smarty_tpl->tpl_vars['pickup_class']->value[0]['pickup_class_id'];?>
"/>
                <input type="hidden" name="date" id="add_to_date" value="<?php echo $_smarty_tpl->tpl_vars['today']->value;?>
"/>
                <input type="hidden" name="do" value="add_child"/>

                <div class="text-center">
                    <strong><label style="font-size: 15px"><?php echo mb_strtoupper(__("Add child into late pickup class on"), 'UTF-8');?>

                            : <?php echo $_smarty_tpl->tpl_vars['today']->value;?>
</label></strong><br><br>
                </div>

                <?php if (count($_smarty_tpl->tpl_vars['pickup_class']->value) == 1) {?>
                    <div class="form-group">
                        <label class="col-sm-2"></label>
                        <label class="col-sm-3 control-label text-left"><?php echo __("Add to");?>
 (*)</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['pickup_class']->value[0]['class_name'];?>
" disabled/>
                            <input type="hidden" id="pickup_class_id" name="pickup_class_id" value="<?php echo $_smarty_tpl->tpl_vars['pickup_class']->value[0]['pickup_class_id'];?>
"/>
                        </div>
                    </div>
                <?php }?>

                <div class="form-group">
                    <label class="col-sm-2"></label>
                    <label class="col-sm-3 control-label text-left"><?php echo __("Class");?>
 (*)</label>
                    <div class="col-sm-3">
                        <select name="class_id" id="pickup_teacher_class" class="form-control" required>
                            <?php $_smarty_tpl->_assignInScope('class_level', -1);
?>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['classes']->value, 'class');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class']->value) {
?>
                                <?php if (($_smarty_tpl->tpl_vars['class_level']->value != $_smarty_tpl->tpl_vars['class']->value['class_level_id'])) {?>
                                    <option value="" disabled style="color: blue">-----<?php echo $_smarty_tpl->tpl_vars['class']->value['class_level_name'];?>
-----</option>
                                <?php }?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['class']->value['group_id'];?>
" <?php if ($_smarty_tpl->tpl_vars['class_id']->value == $_smarty_tpl->tpl_vars['class']->value['group_id']) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['class']->value['group_title'];?>
</option>
                                <?php $_smarty_tpl->_assignInScope('class_level', $_smarty_tpl->tpl_vars['class']->value['class_level_id']);
?>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </select>
                    </div>
                </div>

                <div class="table-responsive" id="pickup_class_list">
                    <?php $_smarty_tpl->_subTemplateRender("file:ci/school/teacher_pickup/ajax.pickupchild.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                </div>

                <div class="form-group pl5">
                    <div class="col-sm-9">
                        <button type="submit" class="btn btn-primary padrl30"><?php echo __("Save");?>
</button>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    <?php }?>
</div><?php }
}
