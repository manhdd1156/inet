<?php
/* Smarty version 3.1.31, created on 2021-05-21 09:16:34
  from "D:\workplace\Inet-project\source\Server11\content\themes\inet\templates\_header.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_60a7180248cfe7_34478408',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '12a384891186df5c355e40ada9c48e98aafe2a9f' => 
    array (
      0 => 'D:\\workplace\\Inet-project\\source\\Server11\\content\\themes\\inet\\templates\\_header.tpl',
      1 => 1621563357,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:_header.search.tpl' => 1,
    'file:_header.friend_requests.tpl' => 1,
    'file:_header.messages.tpl' => 1,
    'file:_header.notifications.tpl' => 1,
    'file:ci/index.left_menu.tpl' => 1,
  ),
),false)) {
function content_60a7180248cfe7_34478408 (Smarty_Internal_Template $_smarty_tpl) {
if (!$_smarty_tpl->tpl_vars['user']->value->_logged_in) {?>
    <body class="n_chat">
<?php } else { ?>
    <body data-chat-enabled="<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_chat_enabled'];?>
" class="<?php if (!$_smarty_tpl->tpl_vars['system']->value['chat_enabled']) {?>n_chat<?php }
if ($_smarty_tpl->tpl_vars['system']->value['activation_enabled'] && !$_smarty_tpl->tpl_vars['user']->value->_data['user_activated']) {?> n_activated<?php }
if (!$_smarty_tpl->tpl_vars['system']->value['system_live']) {?> n_live<?php }?>" <?php if ($_smarty_tpl->tpl_vars['system']->value['app_request']) {?>style = "padding-top: 0px;"<?php }?>>
    <!-- Load Facebook SDK for JavaScript -->
    <div id="fb-root"></div>
    <?php echo '<script'; ?>
>(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js#xfbml=1&version=v2.12&autoLogAppEvents=1';
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));<?php echo '</script'; ?>
>

    <!-- Your customer chat code -->
    <div class="fb-customerchat"
         attribution="setup_tool"
         page_id="1024684347570874"
         theme_color="#0084ff"
         logged_in_greeting="Xin chào, Coniu có thể giúp gì cho bạn?"
         logged_out_greeting="Xin chào, Coniu có thể giúp gì cho bạn?">
    </div>
<?php }?>
    <a href="#top" class="goTop"><i class="fa fa-chevron-up" aria-hidden="true"></i></a>
    <a href="#bot" class="goBot"><i class="fa fa-chevron-down" aria-hidden="true"></i></a>
    <!-- main wrapper -->
    <div class="main-wrapper">
        <div id="loading_full_screen" class="hidden">
        </div>

        <?php if ($_smarty_tpl->tpl_vars['user']->value->_logged_in && $_smarty_tpl->tpl_vars['system']->value['activation_enabled'] && !$_smarty_tpl->tpl_vars['user']->value->_data['user_activated']) {?>
            <!-- top-bar -->
            <div class="top-bar">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-7 hidden-xs">
                            
                                
                            
                                
                            

                            <?php if (!is_empty($_smarty_tpl->tpl_vars['user']->value->_data['user_email'])) {?>
                                <?php echo __("Please go to");?>
 <span class="text-primary"><?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_email'];?>
</span> <?php echo __("to complete the sign-up process");?>
.
                            <?php } else { ?>
                                <?php echo __("Please enter a email address");?>
 <?php echo __("to complete the sign-up process");?>
.
                            <?php }?>
                        </div>
                        <div class="col-xs-12 col-sm-5">
                            <?php if (is_empty($_smarty_tpl->tpl_vars['user']->value->_data['user_email'])) {?>
                                <span class="text-link" data-toggle="modal" data-url="#activation-email-reset">
                                    <?php echo __("Add Email");?>

                                </span>
                            <?php } else { ?>
                                <span class="text-link" data-toggle="modal" data-url="core/activation_email_resend.php">
                                    <?php echo __("Resend Activation Email");?>

                                </span>
                                -
                                <span class="text-link" data-toggle="modal" data-url="#activation-email-reset">
                                    <?php echo __("Change Email");?>

                                </span>
                            <?php }?>

                            
                        </div>
                    </div>
                </div>
            </div>
            <!-- top-bar -->
        <?php }?>

        <?php if (!$_smarty_tpl->tpl_vars['system']->value['system_live']) {?>
            <!-- top-bar alert-->
            <div class="top-bar alert-bar">
                <div class="container">
                    <i class="fa fa-exclamation-triangle fa-lg pr5"></i>
                    <span class="hidden-xs"><?php echo __("The system has been shuttd down");?>
.</span>
                    <span><?php echo __("Turn it on from");?>
</span> <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/admin/settings"><?php echo __("Admin Panel");?>
</a>
                </div>
            </div>
            <!-- top-bar alert-->
        <?php }?>

        <div class="main-header" <?php if ($_smarty_tpl->tpl_vars['system']->value['app_request']) {?> id = "x-hidden" <?php }?>>
            <div class="container header-container">

                <div class="brand-container <?php if ($_smarty_tpl->tpl_vars['user']->value->_logged_in) {?>hidden-xs<?php }?>">
                    <!-- brand -->
                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
" class="brand">
                        <?php if ($_smarty_tpl->tpl_vars['system']->value['system_logo']) {?>
                            <img width="60" src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_uploads'];?>
/<?php echo $_smarty_tpl->tpl_vars['system']->value['system_logo'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_title'];?>
 | <?php echo __("School management and connection software");?>
" title="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_title'];?>
 | <?php echo __("School management and connection software");?>
">
                        <?php } else { ?>
                            <!-- Coniu -->
                            <img width="70px;" src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/content/themes/<?php echo $_smarty_tpl->tpl_vars['system']->value['theme'];?>
/images/logo2.png" alt="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_title'];?>
 | <?php echo __("School management and connection software");?>
" title="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_title'];?>
 | <?php echo __("School management and connection software");?>
">
                        <?php }?>
                    </a>
                    <!-- brand -->
                </div>

                <!-- navbar-collapse -->
                <div>

                    <?php if ($_smarty_tpl->tpl_vars['user']->value->_logged_in || (!$_smarty_tpl->tpl_vars['user']->value->_logged_in && $_smarty_tpl->tpl_vars['system']->value['system_public'])) {?>
                        <!-- search -->
                        <?php $_smarty_tpl->_subTemplateRender('file:_header.search.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                        <!-- search -->
                    <?php }?>

                    <!-- navbar-container -->
                    <div class="navbar-container">
                        <ul class="nav navbar-nav">
                            <?php if ($_smarty_tpl->tpl_vars['user']->value->_logged_in) {?>
                                <!-- home -->
                                <li>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
">
                                        <i class="fa fa-home fa-lg"></i>
                                    </a>
                                </li>
                                <!-- home -->
                                
                                <!-- friend requests -->
                                <?php $_smarty_tpl->_subTemplateRender('file:_header.friend_requests.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                                <!-- friend requests -->

                                <!-- messages -->
                                <?php $_smarty_tpl->_subTemplateRender('file:_header.messages.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                                <!-- messages -->

                                <!-- notifications -->
                                <?php $_smarty_tpl->_subTemplateRender('file:_header.notifications.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                                <!-- notifications -->

                                <!-- search -->
                                <li class="visible-xs-block">
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/search">
                                        <i class="fa fa-search fa-lg"></i>
                                    </a>
                                </li>
                                <!-- search -->

                                <!-- user-menu -->
                                <li class="dropdown">
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_name'];?>
" class="dropdown-toggle user-menu" data-toggle="dropdown">
                                        <div class="news">
                                            <div class="article">
                                                <div class="thumb" style="background-image: url(<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_picture'];?>
);"></div>
                                                
                                            </div>
                                        </div>
                                        
                                        <span class="hidden-xs">
                                            
                                            <?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_fullname'];?>

                                        </span>
                                        <i class="caret"></i>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_name'];?>
"><?php echo __("Profile");?>
</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/settings/privacy"><?php echo __("Privacy");?>
</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/settings"><?php echo __("Settings");?>
</a>
                                        </li>
                                        <?php if ($_smarty_tpl->tpl_vars['user']->value->_is_admin) {?>
                                            <li>
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/admin"><?php echo __("Admin Panel");?>
</a>
                                            </li>
                                        <?php }?>

                                        <li class="divider"></li>
                                        <li>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/signout"><?php echo __("Log Out");?>
</a>
                                        </li>
                                    </ul>
                                </li>
                                <!-- user-menu -->
								<!-- user-manager -->
                                <li class = "dropdown hidden-smmore">
									<a href="#" class="dropdown-toggle user-menu" data-toggle="dropdown">
                                        <i class="fa fa-bars" aria-hidden="true"></i>
                                    </a>
									<ul class="dropdown-menu nav-pills nav-stacked nav-home">
										<!-- basic -->
										<li>
											<a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_name'];?>
">
												<img src="<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_picture'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_fullname'];?>
">
												<span><?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_fullname'];?>
</span>
											</a>
										</li>
										<li>
											<a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/settings/profile">
												<i class="fa fa-pencil-square fa-fw pr10"></i> 
												<?php echo __("Edit Profile");?>

											</a>
										</li>
										<?php if ($_smarty_tpl->tpl_vars['user']->value->_data['user_group'] == 1) {?>
											<li>
												<a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/admin">
													<i class="fas fa-tachometer-alt fa-fw pr10"></i>
													<?php echo __("Admin Panel");?>

												</a>
											</li>
										<?php }?>
										<!-- Coniu - Begin - Menu noga -->
										<?php if ((($_smarty_tpl->tpl_vars['user']->value->_is_admin) || ($_smarty_tpl->tpl_vars['user']->value->_data['user_group'] == @constant('USER_NOGA_MANAGE_ALL')) || ($_smarty_tpl->tpl_vars['user']->value->_data['user_group'] == @constant('USER_NOGA_MANAGE_CITY')) || ($_smarty_tpl->tpl_vars['user']->value->_data['user_group'] == @constant('USER_NOGA_MANAGE_SCHOOL')))) {?>
											<li>
												<a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/noga/">
													<i class="fas fa-tachometer-alt fa-fw pr10"></i>
													<?php echo __("School Panel");?>

												</a>
											</li>
										<?php }?>
										<!-- Coniu - End- Menu noga -->
										<!-- basic -->

										<!-- favorites -->
										<li class="ptb5">
											<small class="text-muted"><?php echo mb_strtoupper(__("favorites"), 'UTF-8');?>
</small>
										</li>

										<li <?php if ($_smarty_tpl->tpl_vars['view']->value == '') {?>class="active"<?php }?>>
											<a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
"><i class="fa fa-newspaper-o fa-fw pr10"></i> <?php echo __("News Feed");?>
</a>
										</li>
										<li>
											<a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/messages"><i class="far fa-comments fa-fw pr10"></i> <?php echo __("Messages");?>
</a>
										</li>
										<li>
											<a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_name'];?>
/photos"><i class="fa fa-file-image fa-fw pr10"></i> <?php echo __("Photos");?>
</a>
										</li>
										<li>
											<a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_name'];?>
/friends"><i class="fa fa-users fa-fw pr10"></i> <?php echo __("Friends");?>
</a>
										</li>
										<li <?php if ($_smarty_tpl->tpl_vars['view']->value == "saved") {?>class="active"<?php }?>>
											<a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/saved"><i class="fa fa-bookmark fa-fw pr10"></i> <?php echo __("Saved");?>
</a>
										</li>
										<?php if ($_smarty_tpl->tpl_vars['system']->value['games_enabled']) {?>
											<li <?php if ($_smarty_tpl->tpl_vars['view']->value == "games") {?>class="active"<?php }?>>
												<a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/games"><i class="fa fa-gamepad fa-fw pr10"></i> <?php echo __("Games");?>
</a>
											</li>
										<?php }?>
										<!-- favorites -->

										<!-- ConIu - schools -->
										<?php $_smarty_tpl->_subTemplateRender("file:ci/index.left_menu.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

										<!-- schools -->

									</ul>
								</li>	
								<!-- end user-manager -->
                            <?php } else { ?>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <span><?php echo __("Join");?>
</span>
                                        <i class="caret"></i>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/signin"><?php echo __("Login");?>
</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/signup"><?php echo __("Register");?>
</a>
                                        </li>
                                    </ul>
                                </li>
                            <?php }?>
                        </ul>
                    </div>
                    <!-- navbar-container -->
                </div>
                <!-- navbar-collapse -->

            </div>
        </div><?php }
}
