<?php
/* Smarty version 3.1.31, created on 2021-05-04 10:52:11
  from "D:\workplace\inet-project\Server11\content\themes\inet\templates\ci\school\school.attendance.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_6090c4ebca81e3_70991678',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '927431c316c8e7c683cc41f68fa4e1b97ef6d75c' => 
    array (
      0 => 'D:\\workplace\\inet-project\\Server11\\content\\themes\\inet\\templates\\ci\\school\\school.attendance.tpl',
      1 => 1620098719,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:ci/school/ajax.school.attendanceaday.tpl' => 1,
    'file:ci/school/ajax.school.attendance.list4rollup.tpl' => 1,
    'file:ci/school/ajax.school.attendance.child.tpl' => 1,
    'file:ci/school/ajax.school.attendance.conabs.tpl' => 1,
  ),
),false)) {
function content_6090c4ebca81e3_70991678 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <?php if ($_smarty_tpl->tpl_vars['canEdit']->value) {?>
            <div class="pull-right flip">
                <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/attendance/rollup" class="btn btn-danger">
                        <i class="fa fa-plus-circle"></i> <?php echo __("Roll up");?>

                    </a>
                <?php }?>
            </div>
        <?php }?>
        <div class="pull-right flip" style="margin-right: 5px">
            <a href="https://blog.coniu.vn/huong-dan-diem-danh-tren-website-coniu-vn/" class="btn btn-info btn_guide" target="_blank">
                <i class="fa fa-info-circle"></i> <?php echo __("Guide");?>

            </a>
        </div>
        <i class="fa fa-tasks fa-fw fa-lg pr10"></i>
        <?php echo __("Attendance");?>

        <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == "detail") {?>
            &rsaquo; <?php echo __("Attendance detail");?>
 &rsaquo; <?php echo $_smarty_tpl->tpl_vars['data']->value['group_title'];?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "child") {?>
            &rsaquo; <?php echo __("Student attendance summary");?>
 <?php if ($_smarty_tpl->tpl_vars['data']->value['child'] != '') {?>&rsaquo; <?php echo $_smarty_tpl->tpl_vars['data']->value['child']['child_name'];
}?>
        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "classatt") {?>
            &rsaquo; <?php echo __("Class attendance summary");?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "conabs") {?>
            &rsaquo; <?php echo __("Consecutive absent student");?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "rollup") {?>
            &rsaquo; <?php echo __("Roll up");?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
            &rsaquo; <?php echo __("Whole school");?>

        <?php }?>
    </div>

    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
        <div class="panel-body with-table">
            <form class="js_ajax-forms form-horizontal">
                <input type="hidden" id="username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Select attendance date");?>
</label>
                    <div class="col-sm-3">
                        <div class='input-group date' id='att_aday_picker'>
                            <input type='text' name="attendanceDate" id="attendanceDate" value="<?php echo $_smarty_tpl->tpl_vars['attendanceDate']->value;?>
" class="form-control" placeholder="<?php echo __("Attendance date");?>
"/>
                                <span class="input-group-addon">
                                    <span class="fas fa-calendar-alt"></span>
                                </span>
                        </div>
                    </div>
                    <label id="loading" class="btn btn-info x-hidden"><?php echo __("Loading");?>
...</label>
                </div>
            </form>
            <br/>
            <div class="table-responsive" id="attendance_list">
                <?php $_smarty_tpl->_subTemplateRender("file:ci/school/ajax.school.attendanceaday.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

            </div>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "classatt") {?>
        <div class="panel-body with-table" style="position: relative">
            <div class="row">
                <div class='col-sm-12'>
                    <div class='col-sm-4'>
                        <select name="class_id" id="classatt_class_id" class="form-control">
                            <option value=""><?php echo __("Select Class");?>
...</option>
                            <?php $_smarty_tpl->_assignInScope('class_level', -1);
?>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['classes']->value, 'class');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class']->value) {
?>
                                <?php if (($_smarty_tpl->tpl_vars['class_level']->value != $_smarty_tpl->tpl_vars['class']->value['class_level_id'])) {?>
                                    <option value="" disabled>-----<?php echo $_smarty_tpl->tpl_vars['class']->value['class_level_name'];?>
-----</option>
                                <?php }?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['class']->value['group_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['class']->value['group_title'];?>
</option>
                                <?php $_smarty_tpl->_assignInScope('class_level', $_smarty_tpl->tpl_vars['class']->value['class_level_id']);
?>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </select>
                    </div>
                    <div class='col-sm-2'>
                        <div class='input-group date' id='fromdatepicker'>
                            <input type='text' name="fromDate" id="fromDate" value="<?php echo $_smarty_tpl->tpl_vars['fromDate']->value;?>
" class="form-control" placeholder="<?php echo __("From date");?>
"/>
                                <span class="input-group-addon">
                                    <span class="fas fa-calendar-alt"></span>
                                </span>
                        </div>
                    </div>
                    <div class='col-sm-2'>
                        <div class='input-group date' id='todatepicker'>
                            <input type='text' name="toDate" id="toDate" value="<?php echo $_smarty_tpl->tpl_vars['toDate']->value;?>
" class="form-control" placeholder="<?php echo __("To date");?>
"/>
                                <span class="input-group-addon">
                                    <span class="fas fa-calendar-alt"></span>
                                </span>
                        </div>
                    </div>
                    <div class='col-sm-2'>
                        <div class="form-group">
                            <a href="#" id="search" class="btn btn-default js_attendance-search-class" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['school']->value['page_id'];?>
" disabled="true"><?php echo __("Search");?>
</a>
                            <label id="loading" class="btn btn-info x-hidden"><?php echo __("Loading");?>
...</label>
                        </div>
                    </div>
                    <div class='col-sm-2'>
                        <div class="form-group">
                            <a href="#" id="export_excel" class="btn btn-success js_attendance-export-excel" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['school']->value['page_id'];?>
" disabled="true"><?php echo __("Export to Excel");?>
</a>
                        </div>
                    </div>
                </div>
            </div>
            <br/>
            <div id="attendance_list"></div>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "rollup") {?>
        <div class="panel-body">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_attendance.php">
                <input type="hidden" name="school_username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                <input type="hidden" name="do" value="save_rollup" id="attendance_do"/>
                <div class="form-group">
                    <div class='col-sm-4'>
                        <select name="class_id" id="attendance_rollup_class_id" class="form-control">
                            <option value=""><?php echo __("Select Class");?>
...</option>
                            <?php $_smarty_tpl->_assignInScope('class_level', -1);
?>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['classes']->value, 'class');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class']->value) {
?>
                                <?php if (($_smarty_tpl->tpl_vars['class_level']->value != $_smarty_tpl->tpl_vars['class']->value['class_level_id'])) {?>
                                    <option value="" disabled>-----<?php echo $_smarty_tpl->tpl_vars['class']->value['class_level_name'];?>
-----</option>
                                <?php }?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['class']->value['group_id'];?>
" <?php if ($_smarty_tpl->tpl_vars['class_id']->value == $_smarty_tpl->tpl_vars['class']->value['group_id']) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['class']->value['group_title'];?>
</option>
                                <?php $_smarty_tpl->_assignInScope('class_level', $_smarty_tpl->tpl_vars['class']->value['class_level_id']);
?>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </select>
                    </div>
                    <div class='col-sm-3'>
                        <div class='input-group date' id='attendance_picker'>
                            <input type='text' name="attendance_date" id="attendance_date" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['attendance_date'];?>
" class="form-control" placeholder="<?php echo __("Attendance date");?>
"/>
                                <span class="input-group-addon">
                                    <span class="fas fa-calendar-alt"></span>
                                </span>
                        </div>
                    </div>
                    <div class='col-sm-3'>
                        <a href="#" id="search" class="btn btn-default js_attendance-search-4rollup" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['school']->value['page_id'];?>
" <?php if ($_smarty_tpl->tpl_vars['class_id']->value == 0) {?>disabled="true"<?php }?>><?php echo __("Search");?>
</a>
                        <label id="loading" class="btn btn-info x-hidden"><?php echo __("Loading");?>
...</label>
                    </div>
                </div>
                <div class="table-responsive" id="attendance_list">
                    <?php if ($_smarty_tpl->tpl_vars['class_id']->value > 0) {?>
                        <?php $_smarty_tpl->_subTemplateRender("file:ci/school/ajax.school.attendance.list4rollup.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                    <?php }?>
                </div>
                <div class="form-group">
                    <div class="col-sm-9">
                        <button type="submit" class="btn btn-primary padrl30" <?php if (($_smarty_tpl->tpl_vars['class_id']->value == 0) || (count($_smarty_tpl->tpl_vars['data']->value['detail']) == 0)) {?>disabled<?php }?>><?php echo __("Save");?>
</button>
                    </div>
                </div>

                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
            </form>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "child") {?>
        <div class="panel-body">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_attendance.php">
                <input type="hidden" name="school_username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                <input type="hidden" name="do" value="save_child_rollup"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-right"><?php echo __("Select child");?>
 (*)</label>
                    <div class='col-sm-3'>
                        <select name="class_id" id="attendance_class_id" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" class="form-control" autofocus>
                            <option value=""><?php echo __("Select Class");?>
...</option>
                            <?php $_smarty_tpl->_assignInScope('class_level', -1);
?>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['data']->value['classes'], 'class');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class']->value) {
?>
                                <?php if (($_smarty_tpl->tpl_vars['class_level']->value != $_smarty_tpl->tpl_vars['class']->value['class_level_id'])) {?>
                                    <option value="" disabled>-----<?php echo $_smarty_tpl->tpl_vars['class']->value['class_level_name'];?>
-----</option>
                                <?php }?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['class']->value['group_id'];?>
" <?php if ($_smarty_tpl->tpl_vars['data']->value['child']['class_id'] == $_smarty_tpl->tpl_vars['class']->value['group_id']) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['class']->value['group_title'];?>
</option>
                                <?php $_smarty_tpl->_assignInScope('class_level', $_smarty_tpl->tpl_vars['class']->value['class_level_id']);
?>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>


                        </select>
                    </div>
                    <div class="col-sm-4">
                        <select name="child_id" id="attendance_child_id" class="form-control" required>
                            <option value=""><?php echo __("Select child");?>
...</option>
                            <?php if ($_smarty_tpl->tpl_vars['data']->value['child'] != '') {?>
                                <?php if (isset($_smarty_tpl->tpl_vars['data']->value['class_child']) && !empty($_smarty_tpl->tpl_vars['data']->value['class_child'])) {?>
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['data']->value['class_child'], 'child');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['child']->value) {
?>
                                        <option value="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
" <?php if (($_smarty_tpl->tpl_vars['child']->value['child_id'] == $_smarty_tpl->tpl_vars['data']->value['child']['child_id'])) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['child']->value['child_name'];?>
</option>
                                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                <?php } else { ?>
                                    <option value="<?php echo $_smarty_tpl->tpl_vars['data']->value['child']['child_id'];?>
" selected><?php echo $_smarty_tpl->tpl_vars['data']->value['child']['child_name'];?>
</option>
                                <?php }?>
                            <?php }?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-right"><?php echo __("Time");?>
 (*)</label>
                    <div class='col-sm-3'>
                        <div class='input-group date' id='fromdatepicker'>
                            <input type='text' name="fromDate" id="fromDate" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['fromDate'];?>
" class="form-control" placeholder="<?php echo __("From date");?>
"/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>

                    <div class='col-md-3'>
                        <div class='input-group date' id='todatepicker'>
                            <input type='text' name="toDate" id="toDate" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['toDate'];?>
" class="form-control" placeholder="<?php echo __("To date");?>
"/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <a href="#" id="search" class="btn btn-default js_school-attendance_child" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" <?php if (!($_smarty_tpl->tpl_vars['data']->value['child'] != '')) {?>disabled="true"<?php }?>><?php echo __("Search");?>
</a>
                        <label id="loading" class="btn btn-info x-hidden"><?php echo __("Loading");?>
...</label>
                    </div>
                </div>
                <div class="table-responsive" id="attendance_list">
                    <?php if ($_smarty_tpl->tpl_vars['data']->value['child'] != '') {?>
                        <?php $_smarty_tpl->_subTemplateRender("file:ci/school/ajax.school.attendance.child.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                    <?php }?>
                </div>
                <div class="form-group">
                    <div class="col-sm-9">
                        <button type="submit" class="btn btn-primary padrl30" <?php if ((!canEdit($_smarty_tpl->tpl_vars['username']->value,'attendance')) || (count($_smarty_tpl->tpl_vars['data']->value['attendance']['attendance']) == 0)) {?>disabled<?php }?>><?php echo __("Save");?>
</button>
                    </div>
                </div>
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
            </form>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "detail") {?>
        <div class="panel-body with-table">
            <table class="table table-striped table-bordered table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th><?php echo __("Full name");?>
</th>
                    <th><?php echo __("Status");?>
</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td colspan="3" align="">
                        <strong><?php echo __("Date");?>
: &nbsp;<?php echo $_smarty_tpl->tpl_vars['data']->value['attendance_date'];?>
&nbsp;&nbsp;-&nbsp;&nbsp;<?php echo __("Present");?>
:&nbsp;<?php echo $_smarty_tpl->tpl_vars['data']->value['present_count'];?>
&nbsp;&nbsp;-&nbsp;&nbsp;<?php echo __("Absence");?>
:&nbsp;<?php echo $_smarty_tpl->tpl_vars['data']->value['absence_count'];?>
</strong>
                    </td>
                </tr>
                <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['data']->value['detail'], 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
                    <tr <?php if ($_smarty_tpl->tpl_vars['row']->value['child_status'] == 0) {?> class="row-disable" <?php }?>>
                        <td align="center"><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</td>
                        <td>
                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/attendance/child/<?php echo $_smarty_tpl->tpl_vars['row']->value['child_id'];?>
">
                                <?php if ($_smarty_tpl->tpl_vars['row']->value['status'] == 1) {?>
                                    <?php echo $_smarty_tpl->tpl_vars['row']->value['child_name'];?>

                                <?php } else { ?>
                                    <strong><?php echo $_smarty_tpl->tpl_vars['row']->value['child_name'];?>
</strong>
                                <?php }?>
                            </a>
                        </td>
                        <td>
                            <?php if ($_smarty_tpl->tpl_vars['row']->value['status'] == @constant('ATTENDANCE_PRESENT')) {?>
                                <?php echo __("Present");?>

                            <?php } elseif ($_smarty_tpl->tpl_vars['row']->value['status'] == @constant('ATTENDANCE_COME_LATE')) {?>
                                <?php echo __("Come late");?>

                            <?php } elseif ($_smarty_tpl->tpl_vars['row']->value['status'] == @constant('ATTENDANCE_EARLY_LEAVE')) {?>
                                <?php echo __("Leave early");?>

                            <?php } elseif ($_smarty_tpl->tpl_vars['row']->value['status'] == @constant('ATTENDANCE_ABSENCE_NO_REASON')) {?>
                                <?php echo __("Without permission");?>

                            <?php } else { ?>
                                <strong><?php echo __("With permission");?>
</strong>&nbsp;
                                <?php if ((!isset($_smarty_tpl->tpl_vars['row']->value['reason']) || ($_smarty_tpl->tpl_vars['row']->value['reason'] == ''))) {?>
                                    (<?php echo __("No reason");?>
)
                                <?php } else { ?>
                                    (<?php echo $_smarty_tpl->tpl_vars['row']->value['reason'];?>
)
                                <?php }?>
                            <?php }?>
                        </td>
                    </tr>
                    <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                </tbody>
            </table>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "conabs") {?>
        <div class="panel-body with-table">
            <div class="row">
                <div class="form-group">
                    <label class="col-sm-3 control-label text-right pt5"><?php echo __("Min number of days");?>
 (*)</label>
                    <div class='col-sm-2'>
                        <input type='number' name="daynum" id="daynum" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['daynum']->value;?>
" min="1" max="31" placeholder="<?php echo __("Min number of days");?>
"/>
                    </div>
                    <div class='col-sm-2'>
                        <div class="form-group">
                            <a href="#" id="search" class="btn btn-default js_attendance-search-conabs" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['school']->value['page_id'];?>
"><?php echo __("Search");?>
</a>
                            <label id="loading" class="btn btn-info x-hidden"><?php echo __("Loading");?>
...</label>
                        </div>
                    </div>
                </div>
            </div>
            <br/>
            <div class="table-responsive" id="attendance_list">
                <?php $_smarty_tpl->_subTemplateRender("file:ci/school/ajax.school.attendance.conabs.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

            </div>
        </div>
    <?php }?>
</div><?php }
}
