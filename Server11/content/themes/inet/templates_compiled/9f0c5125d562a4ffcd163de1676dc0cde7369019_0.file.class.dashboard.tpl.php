<?php
/* Smarty version 3.1.31, created on 2021-03-31 10:26:52
  from "D:\workplace\Server11\content\themes\inet\templates\ci\class\class.dashboard.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_6063ebfc3e9b88_56441250',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '9f0c5125d562a4ffcd163de1676dc0cde7369019' => 
    array (
      0 => 'D:\\workplace\\Server11\\content\\themes\\inet\\templates\\ci\\class\\class.dashboard.tpl',
      1 => 1552404703,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6063ebfc3e9b88_56441250 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <i class="fas fa-tachometer-alt fa-fw fa-lg pr10"></i>
        <?php echo __("Dashboard");?>
:&nbsp;<a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/groups/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['insights']->value['class']['group_title'];?>
</a>
        <div class="pull-right flip">
            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/attendance/rollup">
                <i class="fa fa-tasks fa-fw pr10"></i><?php echo __("Roll up");?>

            </a>&nbsp;|&nbsp;
            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/pages/<?php echo $_smarty_tpl->tpl_vars['school_name']->value;?>
"><?php echo __("School page");?>
</a>
        </div>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-6">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead><tr><th><?php echo __("Teacher");?>
</th></tr></thead>
                        <tbody>
                            <tr>
                                <td>
                                    <div>
                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['insights']->value['teachers'], 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
                                            <span class="name js_user-popover" data-uid="<?php echo $_smarty_tpl->tpl_vars['row']->value['user_id'];?>
">
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['row']->value['user_name'];?>
"><?php echo $_smarty_tpl->tpl_vars['row']->value['user_fullname'];?>
</a>
                                            </span>
                                            <?php if ($_smarty_tpl->tpl_vars['row']->value['user_id'] != $_smarty_tpl->tpl_vars['user']->value->_data['user_id']) {?>
                                                <a href="#" class="far fa-comments fa-lg js_chat-start" data-name="<?php echo $_smarty_tpl->tpl_vars['row']->value['user_fullname'];?>
" data-uid="<?php echo $_smarty_tpl->tpl_vars['row']->value['user_id'];?>
"></a>
                                            <?php }?>
                                            &nbsp;&nbsp;&nbsp;
                                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div class="table-responsive">
                    <div><strong><?php echo __("Children list");?>
&nbsp;(<?php echo count($_smarty_tpl->tpl_vars['insights']->value['children']);?>
)</strong></div>
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th><?php echo __("#");?>
</th>
                            <th><?php echo __("Full name");?>
</th>
                            <th><?php echo __("Parent");?>
</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['insights']->value['children'], 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
                            <tr>
                                <td align="center" class="align-middle"><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</td>
                                <td>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/children/detail/<?php echo $_smarty_tpl->tpl_vars['row']->value['child_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['row']->value['child_name'];?>
</a>
                                    <br/>(<?php echo $_smarty_tpl->tpl_vars['row']->value['birthday'];?>
)
                                </td>
                                <td>
                                    <?php if (count($_smarty_tpl->tpl_vars['row']->value['parent']) == 0) {?>
                                        <?php echo __("No parent");?>

                                    <?php } else { ?>
                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['row']->value['parent'], '_user');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['_user']->value) {
?>
                                            <span class="name js_user-popover" data-uid="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_id'];?>
">
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_name'];?>
"><?php echo $_smarty_tpl->tpl_vars['_user']->value['user_fullname'];?>
</a>
                                            </span>
                                            <?php if ($_smarty_tpl->tpl_vars['_user']->value['user_id'] != $_smarty_tpl->tpl_vars['user']->value->_data['user_id']) {?>
                                                <a href="#" class="far fa-comments fa-lg js_chat-start" data-name="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_fullname'];?>
" data-uid="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_id'];?>
"></a>
                                            <?php }?>
                                            <?php if ($_smarty_tpl->tpl_vars['_user']->value['suggest'] == 1) {?>
                                                <button class="btn btn-xs btn-danger js_school-approve-parent" id="button_<?php echo $_smarty_tpl->tpl_vars['row']->value['child_id'];?>
_<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_id'];?>
" data-parent="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_id'];?>
" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['child_id'];?>
"><?php echo __("Approve");?>
</button>
                                            <?php }?>
                                            <br/>
                                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                    <?php }?>
                                    <?php ob_start();
echo $_smarty_tpl->tpl_vars['row']->value['parent_phone'];
$_prefixVariable1=ob_get_clean();
if (!is_empty($_prefixVariable1)) {?>
                                        (<?php echo $_smarty_tpl->tpl_vars['row']->value['parent_phone'];?>
)
                                    <?php }?>
                                </td>
                            </tr>
                            <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>


                        <?php if (count($_smarty_tpl->tpl_vars['insights']->value['children']) == 0) {?>
                            <tr class="odd">
                                <td valign="top" align="center" colspan="3" class="dataTables_empty">
                                    <?php echo __("No data available in table");?>

                                </td>
                            </tr>
                        <?php }?>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="col-sm-6">
                <?php if (count($_smarty_tpl->tpl_vars['insights']->value['medicines']) > 0) {?>
                    <div class="box-primary">
                        <div class="box-header">
                            <i class="fa fa-medkit"></i>
                            <strong><?php echo __("Today medicines");?>
&nbsp;(<?php echo count($_smarty_tpl->tpl_vars['insights']->value['medicines']);?>
)</strong>&nbsp;
                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/medicines" title="<?php echo __("See All");?>
" class="pull-right flip"><?php echo __("See All");?>
</a>
                        </div>
                        <div class="list-group" id="medicine_on_dashboard">
                            <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['insights']->value['medicines'], 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
                                <?php if (is_array($_smarty_tpl->tpl_vars['row']->value)) {?>
                                    <div class="list-group-item">
                                        <?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
-<a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/medicines/edit/<?php echo $_smarty_tpl->tpl_vars['row']->value['medicine_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['row']->value['child_name'];?>
</a>
                                        <?php if ($_smarty_tpl->tpl_vars['row']->value['status'] == @constant('MEDICINE_STATUS_NEW')) {?>
                                            <img src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/content/themes/<?php echo $_smarty_tpl->tpl_vars['system']->value['theme'];?>
/images/new.gif"/>
                                        <?php } elseif ($_smarty_tpl->tpl_vars['row']->value['status'] == @constant('MEDICINE_STATUS_CONFIRMED')) {?>
                                            <i class="fa fa-check"></i>
                                        <?php }?>
                                        &nbsp;|&nbsp;<?php echo $_smarty_tpl->tpl_vars['row']->value['time_per_day'];?>
 <?php echo __("times/day");?>
&nbsp;|&nbsp;<?php echo $_smarty_tpl->tpl_vars['row']->value['begin'];?>
&nbsp;-&nbsp;<?php echo $_smarty_tpl->tpl_vars['row']->value['end'];?>
<br/>
                                        <?php echo $_smarty_tpl->tpl_vars['row']->value['medicine_list'];?>
<br/>
                                        <i><?php echo __("Guide");?>
:</i> <strong><?php echo $_smarty_tpl->tpl_vars['row']->value['guide'];?>
</strong>
                                        <?php if (count($_smarty_tpl->tpl_vars['row']->value['detail']) > 0) {?>
                                            <br/><?php echo __("Medicated");?>
:
                                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['row']->value['detail'], 'detail');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['detail']->value) {
?>
                                                <br/>&nbsp;&nbsp;&nbsp;<strong><?php echo $_smarty_tpl->tpl_vars['detail']->value['time_on_day'];?>
</strong>&nbsp;-&nbsp;<?php echo $_smarty_tpl->tpl_vars['detail']->value['created_at'];?>
&nbsp;|&nbsp;<?php echo $_smarty_tpl->tpl_vars['detail']->value['user_fullname'];?>

                                                <?php if ($_smarty_tpl->tpl_vars['detail']->value['created_user_id'] != $_smarty_tpl->tpl_vars['user']->value->_data['user_id']) {?>
                                                    <a href="#" class="far fa-comments fa-lg js_chat-start" data-name="<?php echo $_smarty_tpl->tpl_vars['detail']->value['user_fullname'];?>
" data-uid="<?php echo $_smarty_tpl->tpl_vars['detail']->value['created_user_id'];?>
"></a>
                                                <?php }?>
                                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                        <?php }?>
                                        <br/>
                                        <?php if (($_smarty_tpl->tpl_vars['row']->value['time_per_day'] > count($_smarty_tpl->tpl_vars['row']->value['detail'])) && ($_smarty_tpl->tpl_vars['row']->value['status'] != @constant('MEDICINE_STATUS_CANCEL'))) {?>
                                            <button class="btn btn-xs btn-default js_class-medicine" data-handle="medicate" data-screen="" data-max="<?php echo $_smarty_tpl->tpl_vars['row']->value['time_per_day'];?>
" data-child="<?php echo $_smarty_tpl->tpl_vars['row']->value['child_id'];?>
" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['medicine_id'];?>
">
                                                <!--i class="fa fa-plus-square"></i-->
                                                <?php echo __("Medicate");?>

                                            </button>
                                        <?php }?>
                                        <?php if ($_smarty_tpl->tpl_vars['row']->value['status'] == @constant('MEDICINE_STATUS_NEW')) {?>
                                            <button class="btn btn-xs btn-default js_class-medicine" data-handle="confirm" data-screen="dashboard" data-max="<?php echo $_smarty_tpl->tpl_vars['row']->value['time_per_day'];?>
" data-child="<?php echo $_smarty_tpl->tpl_vars['row']->value['child_id'];?>
" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['medicine_id'];?>
">
                                                <!--i class="fa fa-check"></i-->
                                                <?php echo __("Confirm");?>

                                            </button>
                                        <?php }?>
                                    </div>
                                <?php }?>
                                <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </div>
                    </div>
                    <br/>
                <?php }?>
                <div class="box-primary">
                    <div class="box-header">
                        <i class="fa fa-bell"></i>
                        <strong><?php echo __("Notification - Event");?>
</strong>&nbsp;
                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/events" title="<?php echo __("See All");?>
" class="pull-right flip"><?php echo __("See All");?>
</a>
                    </div>
                    <div class="list-group" id="event_on_dashboard">
                        <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['insights']->value['events'], 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
                            <div class="list-group-item">
                                <?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
-<a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/events/edit/<?php echo $_smarty_tpl->tpl_vars['row']->value['event_id'];?>
">
                                    <?php echo $_smarty_tpl->tpl_vars['row']->value['event_name'];?>

                                </a>&nbsp;|&nbsp;
                                <?php if ($_smarty_tpl->tpl_vars['row']->value['level'] == @constant('SCHOOL_LEVEL')) {?>
                                    <?php echo __("School");?>

                                <?php } elseif ($_smarty_tpl->tpl_vars['row']->value['level'] == @constant('CLASS_LEVEL_LEVEL')) {?>
                                    <?php echo __("Class level");?>

                                <?php } elseif ($_smarty_tpl->tpl_vars['row']->value['level'] == @constant('CLASS_LEVEL')) {?>
                                    <?php echo __("Class");?>

                                <?php }?>
                                &nbsp;|&nbsp;<?php echo $_smarty_tpl->tpl_vars['row']->value['is_notified_text'];?>
&nbsp;|&nbsp;<?php echo $_smarty_tpl->tpl_vars['row']->value['post_on_wall_text'];?>
&nbsp;|&nbsp;<?php echo $_smarty_tpl->tpl_vars['row']->value['location'];?>

                                <?php if ($_smarty_tpl->tpl_vars['row']->value['must_register']) {?>
                                    <br/><?php echo __("Participants");?>
:&nbsp;
                                    <?php if ($_smarty_tpl->tpl_vars['row']->value['for_child']) {?>
                                        <?php echo $_smarty_tpl->tpl_vars['row']->value['child_participants'];?>
 <?php echo __("children");?>
&nbsp;|&nbsp;
                                    <?php }?>
                                    <?php if ($_smarty_tpl->tpl_vars['row']->value['for_parent']) {?>
                                        <?php echo $_smarty_tpl->tpl_vars['row']->value['parent_participants'];?>
 <?php echo __("parent");?>

                                    <?php }?>
                                <?php }?>
                                <br/><br/>
                                <?php echo $_smarty_tpl->tpl_vars['row']->value['description'];?>
<br/>
                                <?php if ((!$_smarty_tpl->tpl_vars['row']->value['is_notified']) && ($_smarty_tpl->tpl_vars['row']->value['happened'] <= 0)) {?>
                                    <button class="btn btn-xs btn-default js_class-event-notify" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['event_id'];?>
">
                                        <?php echo __("Notify");?>

                                    </button>
                                <?php }?>
                                <?php if ($_smarty_tpl->tpl_vars['row']->value['must_register']) {?>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/events/participants/<?php echo $_smarty_tpl->tpl_vars['row']->value['event_id'];?>
" class="btn btn-xs btn-default">
                                        <?php echo __("Participants");?>

                                    </a>
                                <?php }?>
                                <?php if (($_smarty_tpl->tpl_vars['row']->value['happened'] <= 0) && $_smarty_tpl->tpl_vars['row']->value['can_edit']) {?>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/events/edit/<?php echo $_smarty_tpl->tpl_vars['row']->value['event_id'];?>
" class="btn btn-xs btn-default">
                                        <?php echo __("Edit");?>

                                    </a>
                                <?php }?>
                            </div>
                            <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div><?php }
}
