<?php
/* Smarty version 3.1.31, created on 2021-03-31 10:35:05
  from "D:\workplace\Server11\content\themes\inet\templates\ci\school\ajax.service.listchild.4regmdservice.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_6063ede94c6ba6_86133168',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'f21b00153151172bcdb4226fcebe565c56ea1142' => 
    array (
      0 => 'D:\\workplace\\Server11\\content\\themes\\inet\\templates\\ci\\school\\ajax.service.listchild.4regmdservice.tpl',
      1 => 1552404702,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6063ede94c6ba6_86133168 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="form-group pl5" id="service_btnSave">
    <div class="col-sm-9">
        <button type="submit" class="btn btn-primary padrl30" disabled><?php echo __("Save");?>
</button>
    </div>
</div>

<div>
    <strong><?php echo __("Children list");?>
&nbsp;(<?php echo __("Children");?>
: <?php echo count($_smarty_tpl->tpl_vars['results']->value['children']);?>
&nbsp;|&nbsp;<?php echo __("Register");?>
: <?php echo $_smarty_tpl->tpl_vars['results']->value['use_count'];?>
)</strong>
    <div class="pull-right flip">
        <?php echo __("Date format");?>
: DD/MM/YYYY.<br/>
        Không nhập ngày hay nhập sai, hệ thống tự lấy ngày hiện tại.<br/>
        <label>Lựa chọn Xóa: chỉ dành để xóa các đăng ký nhầm.</label>
    </div>
</div>
<table class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th>#</th>
            <th><?php echo __("Register");?>
/<?php echo __("Cancel");?>
</th>
            <th><?php echo __("Full name");?>
</th>
            <th><?php echo __("Birthdate");?>
</th>
            <th><?php echo __("Begin");?>
</th>
            <th><?php echo __("End");?>
</th>
            <th><?php echo __("Delete");?>
</th>
        </tr>
    </thead>
    <tbody>
        <?php $_smarty_tpl->_assignInScope('classId', -1);
?>
        <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['results']->value['children'], 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
            <?php if (((!isset($_smarty_tpl->tpl_vars['class_id']->value) || ($_smarty_tpl->tpl_vars['class_id']->value == 0)) && ($_smarty_tpl->tpl_vars['classId']->value != $_smarty_tpl->tpl_vars['row']->value['class_id']))) {?>
                <tr>
                    <td colspan="5"><strong><?php echo $_smarty_tpl->tpl_vars['row']->value['group_title'];?>
</strong></td>
                </tr>
            <?php }?>
            <tr <?php if (($_smarty_tpl->tpl_vars['row']->value['child_status'] == 0)) {?>class="row-disable"<?php }?>>
                <td class="align-middle" align="center"><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</td>
                <td class="align-middle" align="center">
                    <input type="checkbox" name="childIds[]" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['child_id'];?>
" <?php if ($_smarty_tpl->tpl_vars['row']->value['status'] > 0) {?>checked<?php }?>>
                    <input type="hidden" name="allChildIds[]" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['child_id'];?>
"/>
                    <?php if ($_smarty_tpl->tpl_vars['row']->value['status'] > 0) {?>
                        <input type="hidden" name="oldChildIds[]" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['child_id'];?>
"/>
                    <?php }?>
                </td>
                <td class="align-middle"><a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/children/edit/<?php echo $_smarty_tpl->tpl_vars['row']->value['child_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['row']->value['child_name'];?>
</a></td>
                <td class="align-middle"><?php echo $_smarty_tpl->tpl_vars['row']->value['birthday'];?>
</td>
                <td class="align-middle" align="center">
                    <input style="width: 100px" type='text' name="begin_<?php echo $_smarty_tpl->tpl_vars['row']->value['child_id'];?>
" class="reg_service_begin" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['begin'];?>
" class="form-control"/>
                    <input type='hidden' name="old_begin_<?php echo $_smarty_tpl->tpl_vars['row']->value['child_id'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['begin'];?>
"/>
                </td>
                <td class="align-middle" align="center">
                    <input style="width: 100px" type='text' name="end_<?php echo $_smarty_tpl->tpl_vars['row']->value['child_id'];?>
" class="reg_service_end" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['end'];?>
" class="form-control"/>
                    <input type='hidden' name="old_end_<?php echo $_smarty_tpl->tpl_vars['row']->value['child_id'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['end'];?>
"/>
                </td>
                <td class="align-middle" align="center">
                    <?php if ($_smarty_tpl->tpl_vars['row']->value['status'] > 0) {?>
                        <a href="#" class="btn btn-xs btn-danger js_school_delete-mdservice-registration" data-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['child_id'];?>
"><?php echo __("Delete");?>
</a>
                    <?php }?>
                </td>
            </tr>
            <?php $_smarty_tpl->_assignInScope('classId', $_smarty_tpl->tpl_vars['row']->value['class_id']);
?>
            <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

        <?php if ($_smarty_tpl->tpl_vars['idx']->value == 1) {?>
            <tr class="odd">
                <td valign="top" align="center" colspan="7" class="dataTables_empty">
                    <?php echo __("No data available in table");?>

                </td>
            </tr>
        <?php }?>
    </tbody>
</table><?php }
}
