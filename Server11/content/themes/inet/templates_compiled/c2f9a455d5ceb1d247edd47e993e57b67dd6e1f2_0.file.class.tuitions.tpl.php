<?php
/* Smarty version 3.1.31, created on 2021-03-31 10:28:38
  from "D:\workplace\Server11\content\themes\inet\templates\ci\class\class.tuitions.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_6063ec66f0d7c7_88727041',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c2f9a455d5ceb1d247edd47e993e57b67dd6e1f2' => 
    array (
      0 => 'D:\\workplace\\Server11\\content\\themes\\inet\\templates\\ci\\class\\class.tuitions.tpl',
      1 => 1552404703,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:ci/class/class.tuitions.detail.tpl' => 1,
    'file:ci/class/class.tuitions.history.tpl' => 1,
  ),
),false)) {
function content_6063ec66f0d7c7_88727041 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <div class="pull-right flip">
            
                
            
        </div>
        <i class="fas fa-money-bill-alt fa-lg fa-fw pr10"></i>
        <?php echo __("Tuition fee");?>

        <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == "detail") {?>
            &rsaquo; <?php echo __('Detail');?>

        <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == "history") {?>
            &rsaquo; <?php echo __("Used last month");?>

        <?php }?>
    </div>
    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
        <div class="panel-body with-table">
            <div><strong><?php echo __("Tuition list");?>
&nbsp;(<?php echo count($_smarty_tpl->tpl_vars['rows']->value);?>
)</strong></div>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th nowrap="true"><?php echo __("Month");?>
</th>
                            <th><?php echo __("Paid");?>
<br/>(<?php echo @constant('MONEY_UNIT');?>
)</th>
                            <th><?php echo __("Total");?>
<br/>(<?php echo @constant('MONEY_UNIT');?>
)</th>
                            <th><?php echo __("Paid studentren");?>
</th>
                            <th><?php echo __("Option");?>
</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['rows']->value, 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
                            <tr>
                                <td align="center" style="vertical-align:middle"><strong><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</strong></td>
                                <td align="center" style="vertical-align:middle"><a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/tuitions/detail/<?php echo $_smarty_tpl->tpl_vars['row']->value['tuition_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['row']->value['month'];?>
</a></td>
                                <td class="text-right" style="vertical-align:middle"><?php echo moneyFormat($_smarty_tpl->tpl_vars['row']->value['paid_amount']);?>
</td>
                                <td class="text-right" style="vertical-align:middle"><?php echo moneyFormat($_smarty_tpl->tpl_vars['row']->value['total_amount']);?>
</td>
                                <td align="center" style="vertical-align:middle"><?php echo $_smarty_tpl->tpl_vars['row']->value['paid_count'];?>
</td>
                                <td align="center" style="vertical-align:middle">
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/tuitions/detail/<?php echo $_smarty_tpl->tpl_vars['row']->value['tuition_id'];?>
" class="btn btn-xs btn-default"><?php echo __("Detail");?>
</a>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/tuitions/history/<?php echo $_smarty_tpl->tpl_vars['row']->value['tuition_id'];?>
" class="btn btn-xs btn-default"> <?php echo __("Used last month");?>
 </a>
                                </td>
                            </tr>
                            <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                    </tbody>
                </table>
            </div>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "detail") {?>
        <?php $_smarty_tpl->_subTemplateRender("file:ci/class/class.tuitions.detail.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "history") {?>
        <?php $_smarty_tpl->_subTemplateRender("file:ci/class/class.tuitions.history.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

    <?php }?>
</div><?php }
}
