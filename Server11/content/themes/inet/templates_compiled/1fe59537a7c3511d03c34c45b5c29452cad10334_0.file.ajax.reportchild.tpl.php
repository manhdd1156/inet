<?php
/* Smarty version 3.1.31, created on 2021-04-27 09:07:46
  from "D:\workplace\mascom-edu-server\Server11\content\themes\inet\templates\ci\class\ajax.reportchild.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_608771f2c6ed75_07519153',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '1fe59537a7c3511d03c34c45b5c29452cad10334' => 
    array (
      0 => 'D:\\workplace\\mascom-edu-server\\Server11\\content\\themes\\inet\\templates\\ci\\class\\ajax.reportchild.tpl',
      1 => 1552404703,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_608771f2c6ed75_07519153 (Smarty_Internal_Template $_smarty_tpl) {
?>
<table class="table table-striped table-bordered table-hover js_dataTable">
    <thead>
    <tr><th colspan="7"><?php echo __("Report list");?>
&nbsp;(<?php echo count($_smarty_tpl->tpl_vars['results']->value);?>
) - <?php echo __("Not notified yet");?>
 (<?php echo $_smarty_tpl->tpl_vars['countNotNotify']->value;?>
)</th></tr>
    <tr>
        <th><?php echo __("#");?>
</th>
        <th><?php echo __("Title");?>
</th>
        <th><?php echo __("Child");?>
</th>
        
        <th><?php echo __("Notice date");?>
</th>
        <th data-orderable="false" align="center" style="width: 100px">
            <input type="checkbox" id="report_notify_checkall">
            <button class="btn btn-xs btn-default" id="js_class-report-notify-all-select" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-handle="notify_all_select"><?php echo __("Notify");?>
</button>
        </th>
        <th align="center" style="width: 100px">
            <input type="checkbox" id="report_checkall">
            <button class="btn btn-xs btn-danger" id="js_class-report-delete-all-select" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-handle="delete_all_select"><?php echo __("Delete");?>
</button>
        </th>
    </tr>
    </thead>
    <tbody>
    <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['results']->value, 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
        <tr <?php if ($_smarty_tpl->tpl_vars['row']->value['child_status'] == 0) {?> class="row-disable" <?php }?>>
            <td class="align-middle" align="center">
                <strong><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</strong>
            </td>
            <td class="align-middle">
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/reports/detail/<?php echo $_smarty_tpl->tpl_vars['row']->value['report_id'];?>
">
                    <?php echo $_smarty_tpl->tpl_vars['row']->value['report_name'];?>

                </a>
            </td>
            <td class="align-middle">
                <?php echo $_smarty_tpl->tpl_vars['row']->value['child_name'];?>

            </td>
            
                
            
            <td class="align-middle" align="center">
                <?php if ($_smarty_tpl->tpl_vars['row']->value['is_notified']) {?>
                    <?php echo $_smarty_tpl->tpl_vars['row']->value['date'];?>

                <?php } else { ?>
                    <?php echo __("Not notified yet");?>

                <?php }?>
            </td>
            <td class="align-middle" align="center">
                <?php if (!$_smarty_tpl->tpl_vars['row']->value['is_notified']) {?>
                    <input class="report_notify" type="checkbox" name="notify_report_ids" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['report_id'];?>
">
                    <button class="btn btn-xs btn-default js_class-report-notify" data-handle="notify" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['report_id'];?>
"><?php echo __("Notify");?>
</button>
                <?php }?>
            </td>
            <td class="align-middle" align="center">
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/reports/edit/<?php echo $_smarty_tpl->tpl_vars['row']->value['report_id'];?>
" class="btn btn-xs btn-default"><?php echo __("Edit");?>
</a>
                <input class="report_delete" type="checkbox" name="report_ids" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['report_id'];?>
">
                <button class="btn btn-xs btn-danger js_class-delete" data-handle="delete_report" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['report_id'];?>
"><?php echo __("Delete");?>
</button>
            </td>
        </tr>
        <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

    </tbody>
</table>
<?php echo '<script'; ?>
>
    $(function() {
        // run DataTable
        $('.js_dataTable').DataTable({
            "aoColumnDefs": [ { 'aDataSort': false, 'aTargets': [ -1 ] } ],
            "language": {
                "decimal":        "",
                "emptyTable":     __["No data available in table"],
                "info":           __["Showing _START_ to _END_ of _TOTAL_ results"],
                "infoEmpty":      __["Showing 0 to 0 of 0 results"],
                "infoFiltered":   "(filtered from _MAX_ total entries)",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     __["Show"] + " " + "_MENU_" + " " + __["results"],
                "loadingRecords": __["Loading..."],
                "processing":     __["Processing..."],
                "search":         __["Search"],
                "zeroRecords":    __["No matching records found"],
                "paginate": {
                    "first":      __["First"],
                    "last":       __["Last"],
                    "next":       __["Next"],
                    "previous":   __["Previous"]
                },
                "aria": {
                    "sortAscending":  ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                }
            }
        });
    });
<?php echo '</script'; ?>
><?php }
}
