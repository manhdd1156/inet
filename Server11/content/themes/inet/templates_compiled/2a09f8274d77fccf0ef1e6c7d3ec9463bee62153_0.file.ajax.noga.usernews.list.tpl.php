<?php
/* Smarty version 3.1.31, created on 2021-03-31 08:54:55
  from "D:\workplace\Server11\content\themes\inet\templates\ci\noga\ajax.noga.usernews.list.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_6063d66fba5022_68135246',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '2a09f8274d77fccf0ef1e6c7d3ec9463bee62153' => 
    array (
      0 => 'D:\\workplace\\Server11\\content\\themes\\inet\\templates\\ci\\noga\\ajax.noga.usernews.list.tpl',
      1 => 1552404703,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6063d66fba5022_68135246 (Smarty_Internal_Template $_smarty_tpl) {
?>

<strong><?php echo __("User list");?>
&nbsp;(<?php echo count($_smarty_tpl->tpl_vars['results']->value);?>
)</strong>
<div class="pull-right flip">
    <a href="#" id="export2excel" class="btn btn-primary js_noga-export-user-new" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['school']->value['page_id'];?>
"><?php echo __("Export to Excel");?>
</a>
    <label id="export_processing" class="btn btn-info x-hidden"><?php echo __("Loading");?>
...</label>
</div>
<div class="pt20">
    <table class="table table-striped table-bordered table-hover js_dataTable">
        <thead>
        <tr>
            <th><?php echo __("#");?>
</th>
            <th><?php echo __("Full name");?>
</th>
            <th><?php echo __("Username");?>
</th>
            <th><?php echo __("Email");?>
</th>
            <th><?php echo __("Phone");?>
</th>
            <th><?php echo __("Birthday");?>
</th>
            <th><?php echo __("Gender");?>
</th>
            <th><?php echo __("Registered");?>
</th>
            <th><?php echo __("Last login");?>
</th>
            <th><?php echo __("Last active");?>
</th>
        </tr>
        </thead>
        <tbody>
        <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['results']->value, 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
            <tr>
                <td align="center"><strong><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</strong></td>
                <td><a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['row']->value['user_name'];?>
"><strong><?php echo $_smarty_tpl->tpl_vars['row']->value['user_fullname'];?>
</strong></a></td>
                <td><?php echo $_smarty_tpl->tpl_vars['row']->value['user_name'];?>
</td>
                <td><?php echo $_smarty_tpl->tpl_vars['row']->value['user_email'];?>
</td>
                <td><?php echo $_smarty_tpl->tpl_vars['row']->value['user_phone'];?>
</td>
                <td align="center"><?php echo $_smarty_tpl->tpl_vars['row']->value['user_birthdate'];?>
</td>
                <td>
                    <?php if ($_smarty_tpl->tpl_vars['row']->value['user_gender'] == 'male') {?>
                        <?php echo __("Male");?>

                    <?php } else { ?>
                        <?php echo __("Female");?>

                    <?php }?>
                </td>
                <td align="center"><?php echo $_smarty_tpl->tpl_vars['row']->value['user_registered'];?>
</td>
                <td align="center"><?php echo $_smarty_tpl->tpl_vars['row']->value['user_last_login'];?>
</td>
                <td align="center"><?php echo $_smarty_tpl->tpl_vars['row']->value['user_last_active'];?>
</td>
            </tr>
            <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

        </tbody>
    </table>
</div>
<?php echo '<script'; ?>
 type="text/javascript">
    $(function() {
        // run DataTable
        $('.js_dataTable').DataTable({
            "aoColumnDefs": [ { 'aDataSort': false, 'aTargets': [ -1 ] } ],
            "language": {
                "decimal":        "",
                "emptyTable":     __["No data available in table"],
                "info":           __["Showing _START_ to _END_ of _TOTAL_ results"],
                "infoEmpty":      __["Showing 0 to 0 of 0 results"],
                "infoFiltered":   "(filtered from _MAX_ total entries)",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     __["Show"] + " " + "_MENU_" + " " + __["results"],
                "loadingRecords": __["Loading..."],
                "processing":     __["Processing..."],
                "search":         __["Search"],
                "zeroRecords":    __["No matching records found"],
                "paginate": {
                    "first":      __["First"],
                    "last":       __["Last"],
                    "next":       __["Next"],
                    "previous":   __["Previous"]
                },
                "aria": {
                    "sortAscending":  ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                }
            }
        });
    });
<?php echo '</script'; ?>
><?php }
}
