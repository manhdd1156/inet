{assign "id" uniqid()}

<div class="emoji-menu">
     <div class="tab-content">
          {*<div class="tab-pane active" id="tab-emojis-{$id}">
               <div class="js_scroller" data-slimScroll-height="180">
                   {foreach from=$user->get_emojis() item=emoji}
                        <div class="item">
                             <i data-emoji="{$emoji['pattern']}" class="js_emoji twa twa-2x twa-{$emoji['class']}"></i>
                        </div>
                   {/foreach}
               </div>
          </div>*}
           <div class="tab-pane active" id="tab-emojis-{$id}">
               <div class="js_scroller" data-slimScroll-height="180">
                    <div class="item"> <i data-emoji=":)" class="js_emoji twa twa-2x twa-smile"></i> </div>
                    <div class="item"> <i data-emoji="(<" class="js_emoji twa twa-2x twa-joy"></i> </div>
                    <div class="item"> <i data-emoji=":D" class="js_emoji twa twa-2x twa-smiley"></i> </div>
                    <div class="item"> <i data-emoji=":(" class="js_emoji twa twa-2x twa-worried"></i> </div>
                    <div class="item"> <i data-emoji=":relaxed:" class="js_emoji twa twa-2x twa-relaxed"></i> </div>
                    <div class="item"> <i data-emoji=":P" class="js_emoji twa twa-2x twa-stuck-out-tongue"></i> </div>
                    <div class="item"> <i data-emoji=":O" class="js_emoji twa twa-2x twa-open-mouth"></i> </div>
                    <div class="item"> <i data-emoji=":/" class="js_emoji twa twa-2x twa-confused"></i> </div>
                    <div class="item"> <i data-emoji=";)" class="js_emoji twa twa-2x twa-wink"></i> </div>
                    <div class="item"> <i data-emoji=";(" class="js_emoji twa twa-2x twa-sob"></i> </div>
                    <div class="item"> <i data-emoji="B|" class="js_emoji twa twa-2x twa-sunglasses"></i> </div>
                    <div class="item"> <i data-emoji=":disappointed:" class="js_emoji twa twa-2x twa-disappointed"></i> </div>
                    <div class="item"> <i data-emoji=":yum:" class="js_emoji twa twa-2x twa-yum"></i> </div>
                    <div class="item"> <i data-emoji="^_^" class="js_emoji twa twa-2x twa-grin"></i> </div>
                    <div class="item"> <i data-emoji=":no_mouth:" class="js_emoji twa twa-2x twa-no-mouth"></i> </div>
                    <div class="item"> <i data-emoji="*_*" class="js_emoji twa twa-2x twa-heart-eyes"></i> </div>
                    <div class="item"> <i data-emoji="*)" class="js_emoji twa twa-2x twa-kissing-heart"></i> </div>
                    <div class="item"> <i data-emoji="O:)" class="js_emoji twa twa-2x twa-innocent"></i> </div>
                    <div class="item"> <i data-emoji=":angry:" class="js_emoji twa twa-2x twa-angry"></i> </div>
                    <div class="item"> <i data-emoji=":rage:" class="js_emoji twa twa-2x twa-rage"></i> </div>
                    <div class="item"> <i data-emoji=":smirk:" class="js_emoji twa twa-2x twa-smirk"></i> </div>
                    <div class="item"> <i data-emoji=":flushed:" class="js_emoji twa twa-2x twa-flushed"></i> </div>
                    <div class="item"> <i data-emoji=":satisfied:" class="js_emoji twa twa-2x twa-satisfied"></i> </div>
                    <div class="item"> <i data-emoji=":relieved:" class="js_emoji twa twa-2x twa-relieved"></i> </div>
                    <div class="item"> <i data-emoji=":sleeping:" class="js_emoji twa twa-2x twa-sleeping"></i> </div>
                    <div class="item"> <i data-emoji=":stuck_out_tongue:" class="js_emoji twa twa-2x twa-stuck-out-tongue"></i> </div>
                    <div class="item"> <i data-emoji=":stuck_out_tongue_closed_eyes:" class="js_emoji twa twa-2x twa-stuck-out-tongue-closed-eyes"></i> </div>
                    <div class="item"> <i data-emoji=":frowning:" class="js_emoji twa twa-2x twa-frowning"></i> </div>
                    <div class="item"> <i data-emoji=":anguished:" class="js_emoji twa twa-2x twa-anguished"></i> </div>
                    <div class="item"> <i data-emoji=":open_mouth:" class="js_emoji twa twa-2x twa-open-mouth"></i> </div>
                    <div class="item"> <i data-emoji=":grimacing:" class="js_emoji twa twa-2x twa-grimacing"></i> </div>
                    <div class="item"> <i data-emoji=":hushed:" class="js_emoji twa twa-2x twa-hushed"></i> </div>
                    <div class="item"> <i data-emoji=":expressionless:" class="js_emoji twa twa-2x twa-expressionless"></i> </div>
                    <div class="item"> <i data-emoji=":unamused:" class="js_emoji twa twa-2x twa-unamused"></i> </div>
                    <div class="item"> <i data-emoji=":sweat_smile:" class="js_emoji twa twa-2x twa-sweat-smile"></i> </div>
                    <div class="item"> <i data-emoji=":sweat:" class="js_emoji twa twa-2x twa-sweat"></i> </div>
                    <div class="item"> <i data-emoji=":confounded:" class="js_emoji twa twa-2x twa-confounded"></i> </div>
                    <div class="item"> <i data-emoji=":weary:" class="js_emoji twa twa-2x twa-weary"></i> </div>
                    <div class="item"> <i data-emoji=":pensive:" class="js_emoji twa twa-2x twa-pensive"></i> </div>
                    <div class="item"> <i data-emoji=":fearful:" class="js_emoji twa twa-2x twa-fearful"></i> </div>
                    <div class="item"> <i data-emoji=":cold_sweat:" class="js_emoji twa twa-2x twa-cold-sweat"></i> </div>
                    <div class="item"> <i data-emoji=":persevere:" class="js_emoji twa twa-2x twa-persevere"></i> </div>
                    <div class="item"> <i data-emoji=":cry:" class="js_emoji twa twa-2x twa-cry"></i> </div>
                    <div class="item"> <i data-emoji=":astonished:" class="js_emoji twa twa-2x twa-astonished"></i> </div>
                    <div class="item"> <i data-emoji=":scream:" class="js_emoji twa twa-2x twa-scream"></i> </div>
                    <div class="item"> <i data-emoji=":mask:" class="js_emoji twa twa-2x twa-mask"></i> </div>
                    <div class="item"> <i data-emoji=":tired_face:" class="js_emoji twa twa-2x twa-tired-face"></i> </div>
                    <div class="item"> <i data-emoji=":triumph:" class="js_emoji twa twa-2x twa-triumph"></i> </div>
                    <div class="item"> <i data-emoji=":dizzy_face:" class="js_emoji twa twa-2x twa-dizzy-face"></i> </div>
                    <div class="item"> <i data-emoji=":imp:" class="js_emoji twa twa-2x twa-imp"></i> </div>
                    <div class="item"> <i data-emoji=":smiling_imp:" class="js_emoji twa twa-2x twa-smiling-imp"></i> </div>
                    <div class="item"> <i data-emoji=":neutral_face:" class="js_emoji twa twa-2x twa-neutral-face"></i> </div>
                    <div class="item"> <i data-emoji=":alien:" class="js_emoji twa twa-2x twa-alien"></i> </div>
                    <div class="item"> <i data-emoji=":yellow_heart:" class="js_emoji twa twa-2x twa-yellow-heart"></i> </div>
                    <div class="item"> <i data-emoji=":blue_heart:" class="js_emoji twa twa-2x twa-blue-heart"></i> </div>
                    <div class="item"> <i data-emoji=":blue_heart:" class="js_emoji twa twa-2x twa-blue-heart"></i> </div>
                    <div class="item"> <i data-emoji=":heart:" class="js_emoji twa twa-2x twa-heart"></i> </div>
                    <div class="item"> <i data-emoji=":green_heart:" class="js_emoji twa twa-2x twa-green-heart"></i> </div>
                    <div class="item"> <i data-emoji=":broken_heart:" class="js_emoji twa twa-2x twa-broken-heart"></i> </div>
                    <div class="item"> <i data-emoji=":heartbeat:" class="js_emoji twa twa-2x twa-heartbeat"></i> </div>
                    <div class="item"> <i data-emoji=":heartpulse:" class="js_emoji twa twa-2x twa-heartpulse"></i> </div>
                    <div class="item"> <i data-emoji=":two_hearts:" class="js_emoji twa twa-2x twa-two-hearts"></i> </div>
                    <div class="item"> <i data-emoji=":revolving_hearts:" class="js_emoji twa twa-2x twa-revolving-hearts"></i> </div>
                    <div class="item"> <i data-emoji=":cupid:" class="js_emoji twa twa-2x twa-cupid"></i> </div>
                    <div class="item"> <i data-emoji=":sparkling_heart:" class="js_emoji twa twa-2x twa-sparkling-heart"></i> </div>
                    <div class="item"> <i data-emoji=":sparkles:" class="js_emoji twa twa-2x twa-sparkles"></i> </div>
                    <div class="item"> <i data-emoji=":star:" class="js_emoji twa twa-2x twa-star"></i> </div>
                    <div class="item"> <i data-emoji=":star2:" class="js_emoji twa twa-2x twa-star2"></i> </div>
                    <div class="item"> <i data-emoji=":dizzy:" class="js_emoji twa twa-2x twa-dizzy"></i> </div>
                    <div class="item"> <i data-emoji=":boom:" class="js_emoji twa twa-2x twa-boom"></i> </div>
                    <div class="item"> <i data-emoji=":exclamation:" class="js_emoji twa twa-2x twa-exclamation"></i> </div>
                    <div class="item"> <i data-emoji=":anger:" class="js_emoji twa twa-2x twa-anger"></i> </div>
                    <div class="item"> <i data-emoji=":question:" class="js_emoji twa twa-2x twa-question"></i> </div>
                    <div class="item"> <i data-emoji=":grey_exclamation:" class="js_emoji twa twa-2x twa-grey-exclamation"></i> </div>
                    <div class="item"> <i data-emoji=":grey_question:" class="js_emoji twa twa-2x twa-grey-question"></i> </div>
                    <div class="item"> <i data-emoji=":zzz:" class="js_emoji twa twa-2x twa-zzz"></i> </div>
                    <div class="item"> <i data-emoji=":dash:" class="js_emoji twa twa-2x twa-dash"></i> </div>
                    <div class="item"> <i data-emoji=":sweat_drops:" class="js_emoji twa twa-2x twa-sweat-drops"></i> </div>
                    <div class="item"> <i data-emoji=":notes:" class="js_emoji twa twa-2x twa-notes"></i> </div>
                    <div class="item"> <i data-emoji=":musical_note:" class="js_emoji twa twa-2x twa-musical-note"></i> </div>
                    <div class="item"> <i data-emoji=":fire:" class="js_emoji twa twa-2x twa-fire"></i> </div>
                    <div class="item"> <i data-emoji=":poop:" class="js_emoji twa twa-2x twa-poop"></i> </div>
                    <div class="item"> <i data-emoji=":thumbsup:" class="js_emoji twa twa-2x twa-thumbsup"></i> </div>
                    <div class="item"> <i data-emoji=":thumbsdown:" class="js_emoji twa twa-2x twa-thumbsdown"></i> </div>
                    <div class="item"> <i data-emoji=":ok_hand:" class="js_emoji twa twa-2x twa-ok-hand"></i> </div>
                    <div class="item"> <i data-emoji=":punch:" class="js_emoji twa twa-2x twa-punch"></i> </div>
                    <div class="item"> <i data-emoji=":fist:" class="js_emoji twa twa-2x twa-fist"></i> </div>
                    <div class="item"> <i data-emoji=":v:" class="js_emoji twa twa-2x twa-v"></i> </div>
                    <div class="item"> <i data-emoji=":wave:" class="js_emoji twa twa-2x twa-wave"></i> </div>
                    <div class="item"> <i data-emoji=":hand:" class="js_emoji twa twa-2x twa-hand"></i> </div>
                    <div class="item"> <i data-emoji=":raised_hand:" class="js_emoji twa twa-2x twa-raised-hand"></i> </div>
                    <div class="item"> <i data-emoji=":open_hands:" class="js_emoji twa twa-2x twa-open-hands"></i> </div>
                    <div class="item"> <i data-emoji=":point_up:" class="js_emoji twa twa-2x twa-point-up"></i> </div>
                    <div class="item"> <i data-emoji=":point_down:" class="js_emoji twa twa-2x twa-point-down"></i> </div>
                    <div class="item"> <i data-emoji=":point_left:" class="js_emoji twa twa-2x twa-point-left"></i> </div>
                    <div class="item"> <i data-emoji=":point_right:" class="js_emoji twa twa-2x twa-point-right"></i> </div>
                    <div class="item"> <i data-emoji=":raised_hands:" class="js_emoji twa twa-2x twa-raised-hands"></i> </div>
                    <div class="item"> <i data-emoji=":pray:" class="js_emoji twa twa-2x twa-pray"></i> </div>
                    <div class="item"> <i data-emoji=":clap:" class="js_emoji twa twa-2x twa-clap"></i> </div>
                    <div class="item"> <i data-emoji=":muscle:" class="js_emoji twa twa-2x twa-muscle"></i> </div>
                    <div class="item"> <i data-emoji=":runner:" class="js_emoji twa twa-2x twa-runner"></i> </div>
                    <div class="item"> <i data-emoji=":couple:" class="js_emoji twa twa-2x twa-couple"></i> </div>
                    <div class="item"> <i data-emoji=":family:" class="js_emoji twa twa-2x twa-family"></i> </div>
                    <div class="item"> <i data-emoji=":two_men_holding_hands:" class="js_emoji twa twa-2x twa-two-men-holding-hands"></i> </div>
                    <div class="item"> <i data-emoji=":two_women_holding_hands:" class="js_emoji twa twa-2x twa-two-women-holding-hands"></i> </div>
                    <div class="item"> <i data-emoji=":dancer:" class="js_emoji twa twa-2x twa-dancer"></i> </div>
                    <div class="item"> <i data-emoji=":dancers:" class="js_emoji twa twa-2x twa-dancers"></i> </div>
                    <div class="item"> <i data-emoji=":ok_woman:" class="js_emoji twa twa-2x twa-ok-woman"></i> </div>
                    <div class="item"> <i data-emoji=":no_good:" class="js_emoji twa twa-2x twa-no-good"></i> </div>
                    <div class="item"> <i data-emoji=":information_desk_person:" class="js_emoji twa twa-2x twa-information-desk-person"></i> </div>
                    <div class="item"> <i data-emoji=":bride_with_veil:" class="js_emoji twa twa-2x twa-bride-with-veil"></i> </div>
                    <div class="item"> <i data-emoji=":couplekiss:" class="js_emoji twa twa-2x twa-couplekiss"></i> </div>
                    <div class="item"> <i data-emoji=":couple_with_heart:" class="js_emoji twa twa-2x twa-couple-with-heart"></i> </div>
                    <div class="item"> <i data-emoji=":nail_care:" class="js_emoji twa twa-2x twa-nail-care"></i> </div>
                    <div class="item"> <i data-emoji=":boy:" class="js_emoji twa twa-2x twa-boy"></i> </div>
                    <div class="item"> <i data-emoji=":girl:" class="js_emoji twa twa-2x twa-girl"></i> </div>
                    <div class="item"> <i data-emoji=":woman:" class="js_emoji twa twa-2x twa-woman"></i> </div>
                    <div class="item"> <i data-emoji=":man:" class="js_emoji twa twa-2x twa-man"></i> </div>
                    <div class="item"> <i data-emoji=":baby:" class="js_emoji twa twa-2x twa-baby"></i> </div>
                    <div class="item"> <i data-emoji=":older_woman:" class="js_emoji twa twa-2x twa-older-woman"></i> </div>
                    <div class="item"> <i data-emoji=":older_man:" class="js_emoji twa twa-2x twa-older-man"></i> </div>
                    <div class="item"> <i data-emoji=":cop:" class="js_emoji twa twa-2x twa-cop"></i> </div>
                    <div class="item"> <i data-emoji=":angel:" class="js_emoji twa twa-2x twa-angel"></i> </div>
                    <div class="item"> <i data-emoji=":princess:" class="js_emoji twa twa-2x twa-princess"></i> </div>
                    <div class="item"> <i data-emoji=":smiley_cat:" class="js_emoji twa twa-2x twa-smiley-cat"></i> </div>
                    <div class="item"> <i data-emoji=":smile_cat:" class="js_emoji twa twa-2x twa-smile-cat"></i> </div>
                    <div class="item"> <i data-emoji=":heart_eyes_cat:" class="js_emoji twa twa-2x twa-heart-eyes-cat"></i> </div>
                    <div class="item"> <i data-emoji=":kissing_cat:" class="js_emoji twa twa-2x twa-kissing-cat"></i> </div>
                    <div class="item"> <i data-emoji=":smirk_cat:" class="js_emoji twa twa-2x twa-smirk-cat"></i> </div>
                    <div class="item"> <i data-emoji=":scream_cat:" class="js_emoji twa twa-2x twa-scream-cat"></i> </div>
                    <div class="item"> <i data-emoji=":crying_cat_face:" class="js_emoji twa twa-2x twa-crying-cat-face"></i> </div>
                    <div class="item"> <i data-emoji=":joy_cat:" class="js_emoji twa twa-2x twa-joy-cat"></i> </div>
                    <div class="item"> <i data-emoji=":pouting_cat:" class="js_emoji twa twa-2x twa-pouting-cat"></i> </div>
                    <div class="item"> <i data-emoji=":japanese_ogre:" class="js_emoji twa twa-2x twa-japanese-ogre"></i> </div>
                    <div class="item"> <i data-emoji=":see_no_evil:" class="js_emoji twa twa-2x twa-see-no-evil"></i> </div>
                    <div class="item"> <i data-emoji=":hear_no_evil:" class="js_emoji twa twa-2x twa-hear-no-evil"></i> </div>
                    <div class="item"> <i data-emoji=":speak_no_evil:" class="js_emoji twa twa-2x twa-speak-no-evil"></i> </div>
                    <div class="item"> <i data-emoji=":guardsman:" class="js_emoji twa twa-2x twa-guardsman"></i> </div>
                    <div class="item"> <i data-emoji=":skull:" class="js_emoji twa twa-2x twa-skull"></i> </div>
                    <div class="item"> <i data-emoji=":feet:" class="js_emoji twa twa-2x twa-feet"></i> </div>
                    <div class="item"> <i data-emoji=":lips:" class="js_emoji twa twa-2x twa-lips"></i> </div>
                    <div class="item"> <i data-emoji=":kiss:" class="js_emoji twa twa-2x twa-kiss"></i> </div>
                    <div class="item"> <i data-emoji=":droplet:" class="js_emoji twa twa-2x twa-droplet"></i> </div>
                    <div class="item"> <i data-emoji=":ear:" class="js_emoji twa twa-2x twa-ear"></i> </div>
                    <div class="item"> <i data-emoji=":eyes:" class="js_emoji twa twa-2x twa-eyes"></i> </div>
                    <div class="item"> <i data-emoji=":nose:" class="js_emoji twa twa-2x twa-nose"></i> </div>
                    <div class="item"> <i data-emoji=":tongue:" class="js_emoji twa twa-2x twa-tongue"></i> </div>
                    <div class="item"> <i data-emoji=":love_letter:" class="js_emoji twa twa-2x twa-love-letter"></i> </div>
                    <div class="item"> <i data-emoji=":speech_balloon:" class="js_emoji twa twa-2x twa-speech-balloon"></i> </div>
                    <div class="item"> <i data-emoji=":thought_balloon:" class="js_emoji twa twa-2x twa-thought-balloon"></i> </div>
                    <div class="item"> <i data-emoji=":sunny:" class="js_emoji twa twa-2x twa-sunny"></i> </div>
               </div>
          </div>

          {*<div class="tab-pane" id="tab-stickers-{$id}">
               <div class="js_scroller" data-slimScroll-height="180">
                   {foreach from=$user->get_stickers() item=sticker}
                        <div class="item">
                             <img data-emoji=":STK-{$sticker['sticker_id']}:" src="{$system['system_uploads']}/{$sticker['image']}" class="js_emoji">
                        </div>
                   {/foreach}
               </div>
          </div>*}
     </div>

     <ul class="nav nav-tabs">
          <li class="active">
               <a href="#tab-emojis-{$id}" data-toggle="tab">
                    <i class="fa fa-smile-o fa-fw"></i> {__("Emojis")}
               </a>
          </li>
          {*<li>
               <a href="#tab-stickers-{$id}" data-toggle="tab">
                    <i class="fa fa-hand-peace-o fa-fw"></i> {__("Stickers")}
               </a>
          </li>*}
     </ul>
</div>