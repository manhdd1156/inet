<!-- taila create contact -->
<div class="panel panel-default">
    <div class="panel-heading">
        <div class="mt5">
            <strong> {__('REGISTER SCHOOL - FEEDBACK')} </strong>
        </div>
    </div>
    <div class="panel-body">
        <form class="js_ajax-forms" data-url="ci/bo/school/bo_contact.php">
            <div class="form-group">
                <label for="name"> {__('Full Name')} (*) </label>
                <input type="text" class="form-control" name="name2" placeholder="{__('School name/Your name')}" required autofocus maxlength="255">
            </div>
            <div class="form-group">
                <label for="email"> {__('Email')} (*) </label>
                <input type="email" class="form-control" name="email2" placeholder="Email" required maxlength="50">
            </div>
            <div class="form-group">
                <label for="telephone">{__('Telephone')} (*)</label>
                <input type="text" class="form-control" name="phone2" placeholder="{__("Telephone")}" required maxlength="50">
            </div>
            <div class="form-group">
                <label for="select_school">{__('Purpose')} (*)</label>
                <select class="form-control" name="select_school2" required>
                    <option value="1">{__("School register")}</option>
                    <option value="2">{__("Feedback for Inet")}</option>
                    <option value="3">{__("Report error")}</option>
                </select>
            </div>
            <div class="form-group">
                <label for="content">{__('Content')} (*)</label>
                <textarea class="form-control" name="content2" placeholder="{__('Content')}" rows="4" required></textarea>
            </div>

            <button type="submit" class="btn btn-default">{__('Send a New Message')}</button>

            <!-- success -->
            <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
            <!-- success -->
            <!-- error -->
            <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
            <!-- error -->
        </form>
    </div>
</div>
