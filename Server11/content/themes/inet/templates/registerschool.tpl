{include file = "_head_new.tpl"}
{include file = '_header_new.tpl'}

<!--=== Breadcrumbs ===-->
<div class="breadcrumbs">
    <div class="container">
        <h1 class="pull-left">{__("School register")}</h1>
        <ul class="pull-right breadcrumb">
            <li><a href="{$system['system_url']}">{__("Home")}</a></li>
            <li class="active">{__("School register")}</li>
        </ul>
    </div>
</div><!--/breadcrumbs-->
<!--=== End Breadcrumbs ===-->

<!--=== Content Part ===-->
<div class="container content">
    <div class="box_school" style="padding-bottom: 50px">
        <div class="reg_school">
            <div class="contact_form">
                <div class align="center">
                    <h3><b>{__("FREE register for your school")|upper}</b></h3>
                </div>
                <form class="js_ajax-forms" data-url="ci/bo/school/bo_contact.php" action="_email">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="row">
                                <div class="control-group col-sm-6">
                                    <div class="control">
                                        <input type="email" class="form-control" name="email2" placeholder="Email (*)"
                                               required maxlength="50">
                                    </div>
                                </div>
                                <div class="control-group col-sm-6">
                                    <div class="control">
                                        <input type="text" id="phone_contact" name="phone2"
                                               placeholder="{__('Telephone')} (*)" class="form-control" required/>
                                    </div>
                                </div>
                            </div>
                            {*<div class="control-group">*}
                            {*<div class="controls">*}
                            {*<div class="form-group">*}
                            {*<select name="select_school2" id="select_school2" class="form-control">*}
                            {*<option value="1">{__("School register")}</option>*}
                            {*</select>*}
                            {*</div>*}
                            {*</div>*}
                            {*</div>*}
                            <div class="row">
                                <div class="control-group col-xs-12">
                                    <div class="control" style="margin-top: 5px">
                                        <input type="text" id="name2" name="name2"
                                               placeholder="{__('School name/Your name')} (*)" class="form-control"
                                               required maxlength="255">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="control-group">
                            <textarea rows="8" id="content_contact" name="content2"
                                      placeholder="{__('Content')} (*)" class="form-control content_class"
                                      required></textarea>
                            </div>
                        </div>

                    </div>
                    {if $system.reCAPTCHA_enabled}
                        <div id="recaptcha1"></div>
                    {/if}

                    <div class="control-ci" align="center">
                        <button type="submit" class="btn btn-success" style="width: 50%">{__("FREE register for your school")|upper}</button>
                    </div>
                    <!-- success -->
                    <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                    <!-- success -->
                    <!-- error -->
                    <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                    <!-- error -->
                </form>
            </div>
        </div>
    </div>
</div><!--/container-->
<!--=== End Content Part ===-->

<div class="footer_bot" style="position: fixed; bottom: 0; left: 0; width: 100%">
    {include file = '_footer_new.tpl'}
</div>