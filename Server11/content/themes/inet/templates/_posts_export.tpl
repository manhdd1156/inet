
<!-- posts-loader -->
<div class="post x-hidden js_posts_loader">
    <div class="post-body">
        <div class="panel-effect">
            <div class="fake-effect fe-0"></div>
            <div class="fake-effect fe-1"></div>
            <div class="fake-effect fe-2"></div>
            <div class="fake-effect fe-3"></div>
            <div class="fake-effect fe-4"></div>
            <div class="fake-effect fe-5"></div>
            <div class="fake-effect fe-6"></div>
            <div class="fake-effect fe-7"></div>
            <div class="fake-effect fe-8"></div>
            <div class="fake-effect fe-9"></div>
            <div class="fake-effect fe-10"></div>
            <div class="fake-effect fe-11"></div>
        </div>
    </div>
</div>
<!-- posts-loader -->

{if count($posts) > 0}
    <div class="" data-get="{$_get}" data-filter="all" {if $_id}data-id="{$_id}"{/if}>
        <ul>
            <!-- posts -->
            {foreach $posts as $post}
                {include file='__feeds_post.tpl' _get=$_get}
            {/foreach}
            <!-- posts -->
        </ul>

        <!-- see-more -->
        <div class="alert alert-post see-more mb10 js_see-more {if $user->_logged_in}js_see-more-infinite{/if}" data-get="{$_get}" data-filter="all" {if $_id}data-id="{$_id}"{/if}>
            <span>{__("More Stories")}</span>
            <div class="loader loader_small x-hidden"></div>
        </div>
        <!-- see-more -->
    </div>
{else}
    <ul class="js_posts_stream" data-get="{$_get}" data-filter="all" {if $_id}data-id="{$_id}"{/if}>
        <div class="text-center x-muted">
            <i class="fa fa-newspaper-o fa-4x"></i>
            <p class="mb10"><strong>{__("No posts to show")}</strong></p>
        </div>
    </ul>
{/if}