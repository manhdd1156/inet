<body class="header-fixed">
<div class="wrapper">
    <!--=== Header v6 ===-->
    <div class="header-v6 header-classic-white header-sticky header-fixed-shrink">
        <!-- Navbar -->
        <div class="navbar mega-menu" role="navigation">
            <div class="container menu_container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="menu-container">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Navbar Brand -->
                    <div class="navbar-brand">
                        <a href="{$system['system_url']}">
                            <img class="shrink-logo" src="{$system['system_url']}/content/themes/{$system['theme']}/images/logo.png" alt="{$system['system_title']} | {__("School management and connection software")}" title="{$system['system_title']} | {__("School management and connection software")}">
                        </a>
                    </div>
                    <!-- ENd Navbar Brand -->

                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse navbar-responsive-collapse">
                    <div class="menu-container">
                        <ul class="nav navbar-nav">
                            <li class=""><a href="{$system['system_url']}">{__("Home")}</a></li>
                            <li class="active"><a href="{$system['system_url']}/introduction">{__("Introduction")}</a></li>
                        </ul>
                    </div>
                </div><!--/navbar-collapse-->
            </div>
        </div>
    </div>
    <!-- End Navbar -->
    <!--=== End Header v6 ===-->