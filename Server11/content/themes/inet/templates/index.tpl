{if !$user->_logged_in}
    {include file='homepage.tpl'}
{else}
    {include file='_head.tpl'}
    {include file='_header.tpl'}

    <!-- page content -->
    <div class="container mt20 offcanvas height_min"> <!-- class height_min để set height tối thiểu của container -->
        <div class="row">

            <!-- left panel -->
            <div class="col-sm-4 col-md-2 offcanvas-sidebar js_sticky-sidebar">
                {include file='_sidebar.tpl'}
            </div>
            <!-- left panel -->

            <div class="col-sm-8 col-md-10 offcanvas-mainbar">
                <div class="row">
                    <!-- center panel -->
                    <div class="col-sm-12 col-md-8">

                        <!-- announcments -->
                        {include file='_announcements.tpl'}
                        <!-- announcments -->
                        <!-- DELETE START - MANHDD - 18/05/2020 -->
{*                        <!-- CI - Chủ đề quan tâm  -->*}
{*                        {if !$system['topics_enabled']}*}
{*                            <div class="panel panel-default panel-users">*}
{*                                <div class="panel-heading light no_border">*}
{*                                    <strong class="text-muted">{__("Topics you are interested in?")}</strong>*}
{*                                    *}{*<div class="x-muted text-clickable pull-right flip">*}
{*                                        <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" title='{__("")}'></i>*}
{*                                    </div>*}

{*                                </div>*}
{*                                <div class="panel-body pt5">*}
{*                                    <div class="row">*}

{*                                        *}{*<div class="col-xs-2">*}
{*                                            <div class="user-picture" data-toggle="modal" data-url="posts/story.php?do=create" style="background-image:url({$user->_data['user_picture']});">*}
{*                                                <div class="add">*}
{*                                                    <i class="fa fa-plus-circle"></i>*}
{*                                                </div>*}
{*                                            </div>*}
{*                                        </div>*}

{*                                        <!-- users stories -->*}
{*                                        <div class="col-xs-2">*}
{*                                            <div class="user-picture-wrapper">*}
{*                                                <a href="{$system['system_url']}/groups/?category=thaiky" class="user-picture" style="background-image:url('content/themes/{$system['theme']}/images/small/thai_ky.jpg');" data-toggle="tooltip" data-placement="top" title='{__("Preganancy")}'>*}
{*                                                </a>*}

{*                                            </div>*}
{*                                            <div class="text-center">*}
{*                                                {__("Preganancy")}*}
{*                                            </div>*}
{*                                        </div>*}

{*                                        <div class="col-xs-2">*}
{*                                            <div class="user-picture-wrapper">*}
{*                                                <a href="{$system['system_url']}/groups/?category=chamcon" class="user-picture" style="background-image:url('content/themes/{$system['theme']}/images/small/cham_con.jpg');" data-toggle="tooltip" data-placement="top" title='{__("Child care")}'>*}
{*                                                </a>*}
{*                                            </div>*}
{*                                            <div class="text-center">*}
{*                                                {__("Child care")}*}
{*                                            </div>*}
{*                                        </div>*}

{*                                        <div class="col-xs-2">*}
{*                                            <div class="user-picture-wrapper">*}
{*                                                <a href="{$system['system_url']}/groups/?category=daycon" class="user-picture" style="background-image:url('content/themes/{$system['theme']}/images/small/day_con.jpg');" data-toggle="tooltip" data-placement="top" title='{__("Parenting")}'>*}
{*                                                </a>*}
{*                                            </div>*}
{*                                            <div class="text-center">*}
{*                                                {__("Parenting")}*}
{*                                            </div>*}
{*                                        </div>*}

{*                                        <div class="col-xs-2">*}
{*                                            <div class="user-picture-wrapper">*}
{*                                                <a href="{$system['system_url']}/groups/?category=giadinh" class="user-picture" style="background-image:url('content/themes/{$system['theme']}/images/small/hon_nhan_gia_dinh.jpg');" data-toggle="tooltip" data-placement="top" title=' {__("Marriage and family")}'>*}
{*                                                </a>*}
{*                                            </div>*}
{*                                            <div class="text-center">*}
{*                                                {__("Marriage and family")}*}
{*                                            </div>*}
{*                                        </div>*}
{*                                        <!-- users stories -->*}
{*                                    </div>*}
{*                                </div>*}
{*                            </div>*}
{*                        {/if}*}
{*                        <!-- stories -->*}
                        <!-- DELETE END - MANHDD - 18/05/2020 -->
                        {if $view == ""}
                            <!-- publisher -->
                            <!-- Coniu - Thêm điều kiện IF allow_user_post_on_wall -->
                            {if $system['allow_user_post_on_wall']}
                                {include file='_publisher.tpl' _handle="me" _privacy=true}
                            {/if}
                            <!-- publisher -->

                            <!-- posts stream -->
                            {include file='_posts.tpl' _get="newsfeed"}
                            <!-- posts stream -->

                        {elseif $view == "saved"}
                            <!-- saved posts stream -->
                            {include file='_posts.tpl' _get="saved" _title=__("Saved Posts")}
                            <!-- saved posts stream -->

                        {elseif $view == "contact"}
                            <!-- contact taila -->
                            {include file='_contact.tpl' _get="contact" _title=__("Contact")}
                            <!-- contact taila -->
                        {elseif $view == "addchild"}
                            <!-- add new student -->
                            {include file='_addchild.tpl' _get="addchild" _title=__("Add new student")}
                            <!-- add new student -->

                            <!-- ConIu - School -->
                        {elseif ($view == "create_school") or ($view == "create_child")}
                            {include file="ci/index.$view.tpl"}
                            <!-- ConIu - End -->
                        {/if}
                    </div>
                    <!-- center panel -->

                    <!-- right panel -->
                    <div class="col-sm-12 col-md-4">

                        {include file='_ads.tpl'}
                        {include file='_widget.tpl'}

                        <!-- suggested groups -->
                        {if count($new_groups) > 0}
                            <div class="panel panel-default panel-widget">
                                <div class="panel-heading">
                                    <div class="pull-right flip">
                                        <small><a href="{$system['system_url']}/groups/discover">{__("See All")}</a></small>
                                    </div>
                                    <strong>{__("Suggested Groups")}</strong>
                                </div>
                                <div class="panel-body">
                                    <ul>
                                        {foreach $new_groups as $_group}
                                            {include file='__feeds_group.tpl' _tpl="list"}
                                        {/foreach}
                                    </ul>
                                </div>
                            </div>
                        {/if}
                        <!-- suggested groups -->

                        <!-- suggested pages -->
                        {*{if count($new_pages) > 0}
                            <div class="panel panel-default panel-widget">
                                <div class="panel-heading">
                                    <div class="pull-right flip">
                                        <small><a href="{$system['system_url']}/pages">{__("See All")}</a></small>
                                    </div>
                                    <strong>{__("Suggested Pages")}</strong>
                                </div>
                                <div class="panel-body">
                                    <ul>
                                        {foreach $new_pages as $_page}
                                            {include file='__feeds_page.tpl' _tpl="list"}
                                        {/foreach}
                                    </ul>
                                </div>
                            </div>
                        {/if}*}
                        <!-- suggested pages -->

                        <!-- people you may know -->
                        {*{if count($user->_data['new_people']) > 0}*}
                            {*<div class="panel panel-default panel-widget">*}
                                {*<div class="panel-heading">*}
                                    {*<div class="pull-right flip">*}
                                        {*<small><a href="{$system['system_url']}/people">{__("See All")}</a></small>*}
                                    {*</div>*}
                                    {*<strong>{__("People you may know")}</strong>*}
                                {*</div>*}
                                {*<div class="panel-body">*}
                                    {*<ul>*}
                                        {*{foreach $user->_data['new_people'] as $_user}*}
                                            {*{include file='__feeds_user.tpl' _connection="add" _small=true}*}
                                        {*{/foreach}*}
                                    {*</ul>*}
                                {*</div>*}
                            {*</div>*}
                        {*{/if}*}
                        <!-- people you may know -->

                        <!-- mini footer -->
                        {if count($user->_data['new_people']) > 0 || count($new_pages) > 0 || count($new_groups) > 0 || count($new_events) > 0}
                            <div class="row plr10 hidden-xs">
                                <div class="col-xs-12 mb5">
                                    {if count($static_pages) > 0}
                                        {foreach $static_pages as $static_page}
                                        <a href="{$system['system_url']}/static/{$static_page['page_url']}">
                                            {$static_page['page_title']}
                                            </a>{if !$static_page@last} · {/if}
                                        {/foreach}
                                    {/if}
                                    {if $system['contact_enabled']}
                                        ·
                                        <a href="{$system['system_url']}/contacts">
                                            {__("Contacts")}
                                        </a>
                                    {/if}
                                    {if $system['directory_enabled']}
                                        ·
                                        <a href="{$system['system_url']}/directory">
                                            {__("Directory")}
                                        </a>
                                    {/if}
                                    {if $system['market_enabled']}
                                        ·
                                        <a href="{$system['system_url']}/market">
                                            {__("Market")}
                                        </a>
                                    {/if}
                                </div>
                                <div class="col-xs-12">
                                    &copy; {'Y'|date} {$system['system_title']} · <span class="text-link" data-toggle="modal" data-url="#translator">{$system['language']['title']}</span> |
                                    <a href="{$system['system_url']}/privacy">{__("Privacy")}</a> | <a href="{$system['system_url']}/terms">{__("Terms")}</a>
                                </div>
                            </div>
                        {/if}
                        <!-- mini footer -->

                    </div>
                    <!-- right panel -->
                </div>
            </div>

        </div>
    </div>
    <!-- page content -->
    {include file='_footer.tpl'}
{/if}

