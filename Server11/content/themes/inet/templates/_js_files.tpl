{strip}
<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]><script src="{$system['cdn_url']}/includes/assets/js/plugins/html5shiv/html5shiv.js"></script><![endif]-->

<!-- Initialize -->
<script type="text/javascript">
    /* initialize vars */
    var site_title = "{$system['system_title']}";
    var site_path = "{$system['system_url']}";
    var ajax_path = site_path+'/includes/ajax/';
    var ajax_api_path = site_path+'/api/';
    var uploads_path = "{$system['system_uploads']}";
    var current_page = "{$page}";
    var avatar_default_path = site_path + '/content/themes/inet/images/blank_profile_male.jpg';
    var avatar_group_default_path = site_path + '/content/themes/inet/images/blank_group_picture.png';
    {if isset($user)}
        var data_user_id = "_{$user->_data['user_id']}";
        var data_user_name = "{$user->_data['user_name']}";
        var data_user_fullname = "{$user->_data['user_fullname']}";
        var data_user_picture = "{$user->_data['user_picture']}";
    {/if}
    var secret = '{$secret}';
    var min_data_heartbeat = "{$system['data_heartbeat']*1000}";
    var min_chat_heartbeat = "{$system['chat_heartbeat']*1000}";
    var chat_enabled = {if $system['chat_enabled']}true{else}false{/if};
    var geolocation_enabled = {if $system['geolocation_enabled']}true{else}false{/if};
    {*var daytime_msg_enabled = {if $daytime_msg_enabled}true{else}false{/if};*}
    {*var notifications_sound = {if $user->_data['notifications_sound']}true{else}false{/if};*}
    {*var currency = "{$system['system_currency']}";*}
    {*var stripe_key = {if $system['stripe_mode'] == "live"}"{$system['stripe_live_publishable']}"{else}"{$system['stripe_test_publishable']}"{/if}*}
    /* ConIu - BEGIN */
    var DATE_FORMAT = "DD/MM/YYYY";
    var DATETIME_FORMAT = "HH:mm DD/MM/YYYY";
    var TIME_FORMAT = "HH:mm";
    /* ConIu - END */
</script>
<script type="text/javascript">
    /* i18n for JS */
    var __ = [];
    __["Describe your item (optional)"] = "{__('Describe your item (optional)')}";
    __["Ask something"] = "{__('Ask something')}";
    __["Verification Requset"] = "{__('Verification Requset')}";
    __["Add Friend"] = "{__('Add Friend')}";
    __["Friends"] = "{__('Friends')}";
    __["Friend Request Sent"] = "{__('Friend Request Sent')}";
    __["Following"] = "{__('Following')}";
    __["Follow"] = "{__('Follow')}";
    __["Pending"] = "{__('Pending')}";
    __["Remove"] = "{__('Remove')}";
    __["Error"] = "{__('Error')}";
    __["Success"] = "{__('Success')}";
    __["Loading"] = "{__('Loading')}";
    __["Like"] = "{__('Like')}";
    __["Unlike"] = "{__('Unlike')}";
    __["Joined"] = "{__('Joined')}";
    __["Join"] = "{__('Join')}";
    __["Remove Admin"] = "{__('Remove Admin')}";
    __["Make Admin"] = "{__('Make Admin')}";
    __["Going"] = "{__('Going')}";
    __["Interested"] = "{__('Interested')}";
    __["Delete"] = "{__('Delete')}";
    __["Delete Cover"] = "{__('Delete Cover')}";
    __["Delete Picture"] = "{__('Delete Picture')}";
    __["Delete Post"] = "{__('Delete Post')}";
    __["Delete Comment"] = "{__('Delete Comment')}";
    __["Delete Conversation"] = "{__('Delete Conversation')}";
    __["Share Post"] = "{__('Share Post')}";
    __["Report"] = "{__('Report')}";
    __["Block User"] = "{__('Block User')}";
    __["Unblock User"] = "{__('Unblock User')}";
    __["Mark as Available"] = "{__('Mark as Available')}";
    __["Mark as Sold"] = "{__('Mark as Sold')}";
    __["Save Post"] = "{__('Save Post')}";
    __["Unsave Post"] = "{__('Unsave Post')}";
    __["Boost Post"] = "{__('Boost Post')}";
    __["Unboost Post"] = "{__('Unboost Post')}";
    __["Pin Post"] = "{__('Pin Post')}";
    __["Unpin Post"] = "{__('Unpin Post')}";
    __["Verify"] = "{__('Verify')}";
    __["Decline"] = "{__('Decline')}";
    __["Boost"] = "{__('Boost')}";
    __["Unboost"] = "{__('Unboost')}";
    __["Mark as Paid"] = "{__('Mark as Paid')}";
    __["Read more"] = "{__('Read more')}";
    __["Read less"] = "{__('Read less')}";
    __['Turn On Chat'] = "{__('Turn On Chat')}";
    __['Turn Off Chat'] = "{__('Turn Off Chat')}";
    __["Monthly Average"] = "{__('Monthly Average')}";
    __["Jan"] = "{__('Jan')}";
    __["Feb"] = "{__('Feb')}";
    __["Mar"] = "{__('Mar')}";
    __["Apr"] = "{__('Apr')}";
    __["May"] = "{__('May')}";
    __["Jun"] = "{__('Jun')}";
    __["Jul"] = "{__('Jul')}";
    __["Aug"] = "{__('Aug')}";
    __["Sep"] = "{__('Sep')}";
    __["Oct"] = "{__('Oct')}";
    __["Nov"] = "{__('Nov')}";
    __["Dec"] = "{__('Dec')}";
    __["Users"] = "{__('Users')}";
    __["Pages"] = "{__('Pages')}";
    __["Groups"] = "{__('Groups')}";
    __["Events"] = "{__('Events')}";
    __["Posts"] = "{__('Posts')}";
    __["Translated"] = "{__('Translated')}";
    __["Are you sure you want to delete this?"] = "{__('Are you sure you want to delete this?')}";
    __["Are you sure you want to remove your cover photo?"] = "{__('Are you sure you want to remove your cover photo?')}";
    __["Are you sure you want to remove your profile picture?"] = "{__('Are you sure you want to remove your profile picture?')}";
    __["Are you sure you want to delete this post?"] = "{__('Are you sure you want to delete this post?')}";
    __["Are you sure you want to share this post?"] = "{__('Are you sure you want to share this post?')}";
    __["Are you sure you want to delete this comment?"] = "{__('Are you sure you want to delete this comment?')}";
    __["Are you sure you want to delete this conversation?"] = "{__('Are you sure you want to delete this conversation?')}";
    __["Are you sure you want to report this?"] = "{__('Are you sure you want to report this?')}";
    __["Are you sure you want to block this user?"] = "{__('Are you sure you want to block this user?')}";
    __["Are you sure you want to unblock this user?"] = "{__('Are you sure you want to unblock this user?')}";
    __["Are you sure you want to delete your account?"] = "{__('Are you sure you want to delete your account?')}";
    __["Are you sure you want to verify this request?"] = "{__('Are you sure you want to verify this request?')}";
    __["Are you sure you want to decline this request?"] = "{__('Are you sure you want to decline this request?')}";
    __["Are you sure you want to approve this request?"] = "{__('Are you sure you want to approve this request?')}";
    __["Are you sure you want to do this?"] = "{__('Are you sure you want to do this?')}";
    __["There is something that went wrong!"] = "{__('There is something that went wrong!')}";
    __["There is no more data to show"] = "{__('There is no more data to show')}";
    __["This has been shared to your Timeline"] = "{__('This has been shared to your Timeline')}";
    /* ConIu - Begin */
    __['Close'] = '{__("Close")}';
    __["Cancel"] = '{__("Cancel")}';
    __["Actions"] = '{__("Actions")}';
    __["Are you sure you want to cancel this?"] = '{__("Are you sure you want to cancel this?")}';
    __["Count illness"] = '{__("Count illness")}';
    __["Are you sure you want to do this?"] = '{__("Are you sure you want to do this?")}';
    __["Add new row"] = '{__("Add new row")}';
    __["Content"] = '{__("Content")}';
    __["Directory"] = '{__("Directory")}';
    __["Activity"] = '{__("Activity")}';
    __["No"] = '{__("No")}';
    __["Yes"] = '{__("Yes")}';
    __["Count illness"] = '{__("Count illness")}';
    __["Pregnancy check"] = '{__("Pregnancy check")}';
    __["Vaccination"] = '{__("Vaccination")}';
    __["Information"] = '{__("Information")}';
    __["It is impossible to remove when student is under the management of the school"] = '{__("It is impossible to remove when student is under the management of the school")}';
    __["Height development chart"] = '{__("Height development chart")}';
    __["Weight development chart"] = '{__("Weight development chart")}';
    __["BMI development chart"] = '{__("BMI development chart")}';
    __["Height"] = '{__("Height")}';
    __["Weight"] = '{__("Weight")}';
    __["Above standard"] = '{__("Above standard")}';
    __["Below standard"] = '{__("Below standard")}';
    __["Standard"] = '{__("Standard")}';
    __["It looks like this post has no content yet. Please write something or attach a link or image to post"] = '{__("It looks like this post has no content yet. Please write something or attach a link or image to post")}';
    __["Please enter a valid mobile number"] = '{__("Please enter a valid phone number")}';
    __["Please enter a valid phone number"] = '{__("Please enter a valid phone number")}';
    __["Please select a valid birth month"] = '{__("Please select a valid birth month")}';
    __["Please select a valid birth day"] = '{__("Please select a valid birth day")}';
    __["Please select a valid birth year"] = '{__("Please select a valid birth year")}';
    __["Search"] = '{__("Search")}';
    __["Show"] = '{__("Show")}';
    __["results"] = '{__("results")}';
    __["Loading..."] = '{__("Loading...")}';
    __["Processing..."] = '{__("Processing...")}';
    __["No matching records found"] = '{__("No matching records found")}';
    __["No data available in table"] = '{__("No data available in table")}';
    __["Showing _START_ to _END_ of _TOTAL_ results"] = '{__("Showing _START_ to _END_ of _TOTAL_ results")}';
    __["Showing 0 to 0 of 0 results"] = '{__("Showing 0 to 0 of 0 results")}';
    __["First"] = '{__("First")}';
    __["Last"] = '{__("Last")}';
    __["Next"] = '{__("Next")}';
    __["Previous"] = '{__("Previous")}';

    /* ConIu - END */
</script>
<!-- Initialize -->

<!-- Dependencies Libs [jQuery|Bootstrap|Mustache] -->
<script type="text/javascript" src="{$system['cdn_url']}/includes/assets/js/jquery/jquery-1.12.2.min.js"></script>
{*<script type="text/javascript" src="{$system['cdn_url']}/includes/assets/js/jquery/jquery-3.2.1.min.js"></script>*}
<script type="text/javascript" src="{$system['cdn_url']}/includes/assets/js/bootstrap/bootstrap.min.js"></script>
<script type="text/javascript" src="{$system['cdn_url']}/includes/assets/js/mustache/mustache.min.js"></script>
<!-- Dependencies Libs [jQuery|Bootstrap|Mustache] -->

<!-- Dependencies Plugins [JS] [fastclick|slimscroll|autogrow|moment|form|inview|videojs|mediaelementplayer] -->
<script type="text/javascript" src="{$system['cdn_url']}/includes/assets/js/plugins/fastclick/fastclick.min.js"></script>
<script type="text/javascript" src="{$system['cdn_url']}/includes/assets/js/plugins/slimscroll/slimscroll.js"></script>

    {*<script type="text/javascript" src="{$system['cdn_url']}/includes/assets/js/plugins/scrollbar/scrollbar.js"></script>*}
<script type="text/javascript" src="{$system['cdn_url']}/includes/assets/js/plugins/autosize/autosize.min.js" {if !$user->_logged_in}defer{/if}></script>
<script type="text/javascript" src="{$system['cdn_url']}/includes/assets/js/plugins/readmore/readmore.min.js" {if !$user->_logged_in}defer{/if}></script>

 <script type="text/javascript" src="{$system['cdn_url']}/includes/assets/js/plugins/autogrow/autogrow.min.js"></script>
<script type="text/javascript" src="{$system['cdn_url']}/includes/assets/js/plugins/moment/moment-with-locales.min.js"></script>
<script type="text/javascript" src="{$system['cdn_url']}/includes/assets/js/plugins/inview/inview.min.js"></script>
<script type="text/javascript" src="{$system['cdn_url']}/includes/assets/js/plugins/form/form.min.js"></script>
<script type="text/javascript" src="{$system['cdn_url']}/includes/assets/js/plugins/videojs/video.min.js"></script>
<script type="text/javascript" src="{$system['cdn_url']}/includes/assets/js/plugins/mediaelementplayer/mediaelement-and-player.min.js"></script>
<script type="text/javascript" src="{$system['cdn_url']}/includes/assets/js/plugins/datetimepicker/datetimepicker.min.js"></script>


<!-- Dependencies Plugins [JS] [fastclick|slimscroll|autogrow|moment|form|inview|videojs|mediaelementplayer] -->

<!-- Dependencies Plugins [CSS] [videojs|mediaelementplayer] -->
<link rel="stylesheet" type='text/css' href="{$system['cdn_url']}/includes/assets/js/plugins/videojs/video-js.min.css">
<link rel="stylesheet" type='text/css' href="{$system['cdn_url']}/includes/assets/js/plugins/mediaelementplayer/mediaelementplayer.min.css">
<link rel="stylesheet" type='text/css' href="{$system['cdn_url']}/includes/assets/js/plugins/datetimepicker/datetimepicker.min.css">
<!-- Dependencies Plugins [CSS] [videojs|mediaelementplayer] -->

<!-- Dependencies CSS [Twemoji-Awesome|Flag-Icon] -->
<link rel="stylesheet" href="{$system['cdn_url']}/includes/assets/css/twemoji-awesome/twemoji-awesome.min.css" onload="if(media!='all')media='all'">
<link rel="stylesheet" href="{$system['cdn_url']}/includes/assets/css/flag-icon/css/flag-icon.min.css" onload="if(media!='all')media='all'">
<!-- Dependencies CSS [Twemoji-Awesome|Flag-Icon] -->
    <link rel="stylesheet" type='text/css' href="{$system['cdn_url']}/includes/assets/js/plugins/metisMenu/metisMenu.min.css">
<!-- Core [JS] -->
<script type="text/javascript" src="{$system['cdn_url']}/includes/assets/js/core{$system["js_path"]}/core.js"></script>
<!-- CICommon [JS] -->
    <script type="text/javascript" src="{$system['cdn_url']}/includes/assets/js/inet{$system["js_path"]}/cicommon.js"></script>
{if $user->_logged_in}
    <!-- JS Initialize Firebase -->
    {if $system['sync_firebase']}
        {$system["initialize_firebase"]}

        <script type="text/javascript" src="{$system['cdn_url']}/includes/assets/js/core{$system["js_path"]}/chat.js"></script>
    {/if}
    <!-- JS Initialize Firebase -->

    {if $system['geolocation_enabled']}
        <script src="{$system['system_url']}/includes/assets/js/plugins/jquery.geocomplete/jquery.geocomplete.min.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?libraries=places&key={$system['geolocation_key']}"></script>
    {/if}

    <!-- Bootstrap select -->
    <script src="{$system['cdn_url']}/includes/assets/js/plugins/bootstrap.select/bootstrap-select.min.js"></script>
    <link rel="stylesheet" type='text/css' href="{$system['cdn_url']}/includes/assets/js/plugins/bootstrap.select/bootstrap-select.min.css">
    <!-- Bootstrap select -->

    <script type="text/javascript" src="{$system['cdn_url']}/includes/assets/js/plugins/noty/noty.min.js"></script>
    <link rel="stylesheet" type='text/css' href="{$system['cdn_url']}/includes/assets/js/plugins/noty/noty.css">

    <!-- CI - Trình soạn thảo văn bản (Dùng cho article) -->
    {*<script src="{$system['system_url']}/includes/assets/js/plugins/tinymce/tinymce.min.js"></script>*}

    <script type="text/javascript" src="{$system['cdn_url']}/includes/assets/js/core{$system["js_path"]}/user.js"></script>
    <script type="text/javascript" src="{$system['cdn_url']}/includes/assets/js/core{$system["js_path"]}/post.js"></script>
{/if}
    <script type="text/javascript" src="{$system['cdn_url']}/includes/assets/js/plugins/metisMenu/metisMenu.min.js"></script>
<!-- Core [JS] -->


{if $page == "admin"}
    <!-- Dependencies Plugins [JS] -->
    <script type="text/javascript" src="{$system['cdn_url']}/includes/assets/js/plugins/metisMenu/metisMenu.min.js"></script>
    <script type="text/javascript" src="{$system['cdn_url']}/includes/assets/js/plugins/dataTables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="{$system['cdn_url']}/includes/assets/js/plugins/dataTables/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" src="{$system['cdn_url']}/includes/assets/js/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
    <!-- Dependencies Plugins [JS] -->

    <!-- Dependencies Plugins [CSS] -->
    <link rel="stylesheet" type='text/css' href="{$system['cdn_url']}/includes/assets/js/plugins/metisMenu/metisMenu.min.css">
    <link rel="stylesheet" type='text/css' href="{$system['cdn_url']}/includes/assets/js/plugins/dataTables/dataTables.bootstrap.min.css">
    <link rel="stylesheet" type='text/css' href="{$system['cdn_url']}/includes/assets/js/plugins/colorpicker/bootstrap-colorpicker.min.css">
    <link rel="stylesheet" type='text/css' href="{$system['cdn_url']}/includes/assets/js/plugins/datetimepicker/datetimepicker.min.css">
    <!-- Dependencies Plugins [CSS] -->

    <!-- Core [JS] -->
    <script type="text/javascript" src="{$system['cdn_url']}/includes/assets/js/core{$system["js_path"]}/admin.js"></script>
    <!-- Core [JS] -->

    <!-- Admin Charts -->
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    {if $view == "dashboard"}
        <script>
            $(function () {
                Highcharts.setOptions();
                $('#admin-chart-dashboard').highcharts({
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: __["Monthly Average"]
                    },
                    xAxis: {
                        categories: [
                            __["Jan"],
                            __["Feb"],
                            __["Mar"],
                            __["Apr"],
                            __["May"],
                            __["Jun"],
                            __["Jul"],
                            __["Aug"],
                            __["Sep"],
                            __["Oct"],
                            __["Nov"],
                            __["Dec"]
                        ],
                        crosshair: true
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: "{'Y'|date}"
                        }
                    },
                    tooltip: {
                        headerFormat: '<span style="font-size:10px">{literal}{point.key}{/literal}</span><table>',
                        pointFormat: '<tr><td style="color:{literal}{series.color}{/literal};padding:0">{literal}{series.name}{/literal}: </td>' +
                        '<td style="padding:0"><b>{literal}{point.y}{/literal}</b></td></tr>',
                        footerFormat: '</table>',
                        shared: true,
                        useHTML: true
                    },
                    plotOptions: {
                        column: {
                            pointPadding: 0.2,
                            borderWidth: 0
                        }
                    },
                    series: [{
                        name: __["Users"],
                        data: [{','|implode:$chart['users']}]

                    }, {
                        name: __["Pages"],
                        data: [{','|implode:$chart['pages']}]

                    }, {
                        name: __["Groups"],
                        data: [{','|implode:$chart['groups']}]

                    }, {
                        name: __["Events"],
                        data: [{','|implode:$chart['events']}]

                    }, {
                        name: __["Posts"],
                        data: [{','|implode:$chart['posts']}]

                    }]
                });
            });
        </script>
    {/if}
    {if $view == "packages" && $sub_view == "earnings"}
        <script>
            $(function () {
                Highcharts.setOptions();
                $('#admin-chart-earnings').highcharts({
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: __["Monthly Average"]
                    },
                    xAxis: {
                        categories: [
                            __["Jan"],
                            __["Feb"],
                            __["Mar"],
                            __["Apr"],
                            __["May"],
                            __["Jun"],
                            __["Jul"],
                            __["Aug"],
                            __["Sep"],
                            __["Oct"],
                            __["Nov"],
                            __["Dec"]
                        ],
                        crosshair: true
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: "{'Y'|date}"
                        }
                    },
                    tooltip: {
                        headerFormat: '<span style="font-size:10px">{literal}{point.key}{/literal}</span><table>',
                        pointFormat: '<tr><td style="color:{literal}{series.color}{/literal};padding:0">{literal}{series.name}{/literal}: </td>' +
                        '<td style="padding:0"><b>{literal}{point.y}{/literal}</b></td></tr>',
                        footerFormat: '</table>',
                        shared: true,
                        useHTML: true
                    },
                    plotOptions: {
                        column: {
                            pointPadding: 0.2,
                            borderWidth: 0
                        }
                    },
                    series: [
                        {foreach $rows as $key => $value}
                        {
                            name: "{$key}",
                            data: [{','|implode:$value['months_sales']}]
                        },
                        {/foreach}
                    ]
                });

            });
        </script>
    {/if}
    <!-- Admin Charts -->

    <!-- Admin Code Editor -->
    {if $view == "design"}
        <script>
            $(function () {
                CodeMirror.fromTextArea(document.getElementById('custome_js_header'), {
                    mode: "javascript",
                    lineNumbers: true,
                    readOnly: false
                });

                CodeMirror.fromTextArea(document.getElementById('custome_js_footer'), {
                    mode: "javascript",
                    lineNumbers: true,
                    readOnly: false
                });

                CodeMirror.fromTextArea(document.getElementById('custom-css'), {
                    mode: "css",
                    lineNumbers: true,
                    readOnly: false
                });
            });
        </script>
    {/if}
    <!-- Admin Code Editor -->

{/if}

<!-- ConIu - BEGIN -->
{if ($page == "school") || ($page == "class") || ($page == "child") || ($page == "noga") || $page == "childinfo" || ($page == "") || ($page == "region")}
    <!-- Dependencies Plugins [JS] -->
    <script type="text/javascript" src="{$system['cdn_url']}/includes/assets/js/plugins/metisMenu/metisMenu.min.js"></script>
    <script src="{$system['cdn_url']}/includes/assets/js/plugins/dataTables/jquery.dataTables.min.js"></script>
    <script src="{$system['cdn_url']}/includes/assets/js/plugins/dataTables/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" src="{$system['cdn_url']}/includes/assets/js/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
    <script type="text/javascript" src="{$system['cdn_url']}/includes/assets/js/jquery/jquery-ui-1.12.1.min.js"></script>
    {*{if $view == "useservices"}*}
    <script type="text/javascript" src="{$system['cdn_url']}/includes/assets/js/jquery/jquery.mask.min.js"></script>
    {*{/if}*}
    <!-- Dependencies Plugins [JS] -->

    <!-- Dependencies Plugins [CSS] -->
    <link rel="stylesheet" type='text/css' href="{$system['cdn_url']}/includes/assets/js/jquery/jquery-ui-1.12.1.min.css">
    <link rel="stylesheet" type='text/css' href="{$system['cdn_url']}/includes/assets/js/plugins/metisMenu/metisMenu.min.css">
    <link rel="stylesheet" type='text/css' href="{$system['cdn_url']}/includes/assets/js/plugins/dataTables/dataTables.bootstrap.min.css">
    <link rel="stylesheet" type='text/css' href="{$system['cdn_url']}/includes/assets/js/plugins/colorpicker/bootstrap-colorpicker.min.css">
    <link rel="stylesheet" type='text/css' href="{$system['cdn_url']}/includes/assets/js/plugins/datetimepicker/datetimepicker.min.css">
    <!-- Dependencies Plugins [CSS] -->

    {if $page == "school"}
        {if $smarty.const.DEBUGGING}
            <script type="text/javascript" src="{$system['cdn_url']}/includes/assets/js/inet{$system['js_path']}/school.js"></script>
        {else}
            <script type="text/javascript" src="{$system['cdn_url']}/includes/assets/js/inet{$system['js_path']}/school.min.js"></script>
        {/if}
    {elseif $page =="class"}
        {if $smarty.const.DEBUGGING}
            <script type="text/javascript" src="{$system['cdn_url']}/includes/assets/js/inet{$system['js_path']}/class.js"></script>
        {else}
            <script type="text/javascript" src="{$system['cdn_url']}/includes/assets/js/inet{$system['js_path']}/class.min.js"></script>
        {/if}
    {elseif $page =="noga"}
        {if $smarty.const.DEBUGGING}
            <script type="text/javascript" src="{$system['cdn_url']}/includes/assets/js/inet{$system['js_path']}/noga.js"></script>
        {else}
            <script type="text/javascript" src="{$system['cdn_url']}/includes/assets/js/inet{$system['js_path']}/noga.min.js"></script>
        {/if}
    {elseif $page =="region"}
        {if $smarty.const.DEBUGGING}
            <script type="text/javascript" src="{$system['cdn_url']}/includes/assets/js/inet{$system['js_path']}/region.js"></script>
        {else}
            <script type="text/javascript" src="{$system['cdn_url']}/includes/assets/js/inet{$system['js_path']}/region.min.js"></script>
        {/if}
    {else}
        {if $smarty.const.DEBUGGING}
            <script type="text/javascript" src="{$system['cdn_url']}/includes/assets/js/inet{$system['js_path']}/child.js"></script>
        {else}
            <script type="text/javascript" src="{$system['cdn_url']}/includes/assets/js/inet{$system['js_path']}/child.min.js"></script>
        {/if}
    {/if}

    <!-- BEGIN - Thêm plugin datepicker -->
    <!--
    <script type="text/javascript" src="{$system['cdn_url']}/includes/assets/js/inet/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
    <link rel="stylesheet" type='text/css' href="{$system['cdn_url']}/includes/assets/js/inet/datetimepicker/css/bootstrap-datetimepicker.min.css">
    -->
    <!-- END - Thêm plugin datepicker -->
{/if}

{if $smarty.const.DEBUGGING}
    <script type="text/javascript" src="{$system['cdn_url']}/includes/assets/js/inet{$system['js_path']}/notify.js"></script>
{else}
    <script type="text/javascript" src="{$system['cdn_url']}/includes/assets/js/inet{$system['js_path']}/notify.min.js"></script>
{/if}
<!-- ConIu - END -->
{/strip}