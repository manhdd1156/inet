{if !$user->_logged_in}
    <body class="n_chat">
{else}
    <!-- DELETE START - MANHDD - 25/05/2021 - chưa có app chat trên facebook -->
{*    <body data-chat-enabled="{$user->_data['user_chat_enabled']}" class="{if !$system['chat_enabled']}n_chat{/if}{if $system['activation_enabled'] && !$user->_data['user_activated']} n_activated{/if}{if !$system['system_live']} n_live{/if}" {if $system['app_request']}style = "padding-top: 0px;"{/if}>*}
{*    <!-- Load Facebook SDK for JavaScript -->*}
{*    <div id="fb-root"></div>*}
{*    <script>(function(d, s, id) {*}
{*            var js, fjs = d.getElementsByTagName(s)[0];*}
{*            if (d.getElementById(id)) return;*}
{*            js = d.createElement(s); js.id = id;*}
{*            js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js#xfbml=1&version=v2.12&autoLogAppEvents=1';*}
{*            fjs.parentNode.insertBefore(js, fjs);*}
{*        }(document, 'script', 'facebook-jssdk'));</script>*}

{*    <!-- Your customer chat code -->*}
{*    <div class="fb-customerchat"*}
{*         attribution="setup_tool"*}
{*         page_id="1024684347570874"*}
{*         theme_color="#0084ff"*}
{*         logged_in_greeting="Xin chào, Coniu có thể giúp gì cho bạn?"*}
{*         logged_out_greeting="Xin chào, Coniu có thể giúp gì cho bạn?">*}
{*    </div>*}
<!-- DELETE END - MANHDD - 25/05/2021 - chưa có app chat trên facebook -->
{/if}
    <a href="#top" class="goTop"><i class="fa fa-chevron-up" aria-hidden="true"></i></a>
    <a href="#bot" class="goBot"><i class="fa fa-chevron-down" aria-hidden="true"></i></a>
    <!-- main wrapper -->
    <div class="main-wrapper">
        <div id="loading_full_screen" class="hidden">
        </div>

        {if $user->_logged_in && $system['activation_enabled'] && !$user->_data['user_activated']}
            <!-- top-bar -->
            <div class="top-bar">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-7 hidden-xs">
                            {*{if $system['activation_type'] == "email"}*}
                                {*{__("Please go to")} <span class="text-primary">{$user->_data['user_email']}</span> {__("to complete the sign-up process")}.*}
                            {*{else}*}
                                {*{__("Please check the SMS on your phone")} <strong>{$user->_data['user_phone']}</strong> {__("to complete the sign-up process")}.*}
                            {*{/if}*}

                            {if !is_empty($user->_data['user_email'])}
                                {__("Please go to")} <span class="text-primary">{$user->_data['user_email']}</span> {__("to complete the sign-up process")}.
                            {else}
                                {__("Please enter a email address")} {__("to complete the sign-up process")}.
                            {/if}
                        </div>
                        <div class="col-xs-12 col-sm-5">
                            {if is_empty($user->_data['user_email'])}
                                <span class="text-link" data-toggle="modal" data-url="#activation-email-reset">
                                    {__("Add Email")}
                                </span>
                            {else}
                                <span class="text-link" data-toggle="modal" data-url="core/activation_email_resend.php">
                                    {__("Resend Activation Email")}
                                </span>
                                -
                                <span class="text-link" data-toggle="modal" data-url="#activation-email-reset">
                                    {__("Change Email")}
                                </span>
                            {/if}

                            {*{if $system['activation_type'] == "email"}
                                <span class="text-link" data-toggle="modal" data-url="core/activation_email_resend.php">
                                    {__("Resend Activation Email")}
                                </span>
                                -
                                <span class="text-link" data-toggle="modal" data-url="#activation-email-reset">
                                    {__("Change Email")}
                                </span>
                            {else}
                                <span class="btn btn-info btn-sm mr10" data-toggle="modal" data-url="#activation-phone">{__("Enter Code")}</span>
                                {if $user->_data['user_phone']}
                                    <span class="text-link" data-toggle="modal" data-url="core/activation_phone_resend.php">
                                        {__("Resend SMS")}
                                    </span>
                                    -
                                {/if}
                                <span class="text-link" data-toggle="modal" data-url="#activation-phone-reset">
                                    {__("Change Phone Number")}
                                </span>
                            {/if}*}
                        </div>
                    </div>
                </div>
            </div>
            <!-- top-bar -->
        {/if}

        {if !$system['system_live']}
            <!-- top-bar alert-->
            <div class="top-bar alert-bar">
                <div class="container">
                    <i class="fa fa-exclamation-triangle fa-lg pr5"></i>
                    <span class="hidden-xs">{__("The system has been shuttd down")}.</span>
                    <span>{__("Turn it on from")}</span> <a href="{$system['system_url']}/admin/settings">{__("Admin Panel")}</a>
                </div>
            </div>
            <!-- top-bar alert-->
        {/if}

        <div class="main-header" {if $system['app_request']} id = "x-hidden" {/if}>
            <div class="container header-container">

                <div class="brand-container {if $user->_logged_in}hidden-xs{/if}">
                    <!-- brand -->
                    <a href="{$system['system_url']}" class="brand">
                        {if $system['system_logo']}
                            <img width="60" src="{$system['system_uploads']}/{$system['system_logo']}" alt="{$system['system_title']} | {__("School management and connection software")}" title="{$system['system_title']} | {__("School management and connection software")}">
                        {else}
                            <!-- Inet -->
                            <img width="70px;" src="{$system['system_url']}/content/themes/{$system['theme']}/images/logo2.png" alt="{$system['system_title']} | {__("School management and connection software")}" title="{$system['system_title']} | {__("School management and connection software")}">
                        {/if}
                    </a>
                    <!-- brand -->
                </div>

                <!-- navbar-collapse -->
                <div>

                    {if $user->_logged_in || (!$user->_logged_in && $system['system_public']) }
                        <!-- search -->
                        {include file='_header.search.tpl'}
                        <!-- search -->
                    {/if}

                    <!-- navbar-container -->
                    <div class="navbar-container">
                        <ul class="nav navbar-nav">
                            {if $user->_logged_in}
                                <!-- home -->
                                <li>
                                    <a href="{$system['system_url']}">
                                        <i class="fa fa-home fa-lg"></i>
                                    </a>
                                </li>
                                <!-- home -->
                                
                                <!-- friend requests -->
                                {include file='_header.friend_requests.tpl'}
                                <!-- friend requests -->

                                <!-- messages -->
                                {include file='_header.messages.tpl'}
                                <!-- messages -->

                                <!-- notifications -->
                                {include file='_header.notifications.tpl'}
                                <!-- notifications -->

                                <!-- search -->
                                <li class="visible-xs-block">
                                    <a href="{$system['system_url']}/search">
                                        <i class="fa fa-search fa-lg"></i>
                                    </a>
                                </li>
                                <!-- search -->

                                <!-- user-menu -->
                                <li class="dropdown">
                                    <a href="{$system['system_url']}/{$user->_data['user_name']}" class="dropdown-toggle user-menu" data-toggle="dropdown">
                                        <div class="news">
                                            <div class="article">
                                                <div class="thumb" style="background-image: url({$user->_data['user_picture']});"></div>
                                                {*<div class="title">Tiêu đề bản tin.</div>*}
                                            </div>
                                        </div>
                                        {*<img src="{$user->_data['user_picture']}" alt="">*}
                                        <span class="hidden-xs">
                                            {*{if strlen($user->_data['user_fullname']) >= 40} {substr($user->_data['user_fullname'], 0, 40)}...
                                            {else} {$user->_data['user_fullname']}
                                            {/if}*}
                                            {$user->_data['user_fullname']}
                                        </span>
                                        <i class="caret"></i>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="{$system['system_url']}/{$user->_data['user_name']}">{__("Profile")}</a>
                                        </li>
                                        <li>
                                            <a href="{$system['system_url']}/settings/privacy">{__("Privacy")}</a>
                                        </li>
                                        <li>
                                            <a href="{$system['system_url']}/settings">{__("Settings")}</a>
                                        </li>
                                        {if $user->_is_admin}
                                            <li>
                                                <a href="{$system['system_url']}/admin">{__("Admin Panel")}</a>
                                            </li>
                                        {/if}

                                        <li class="divider"></li>
                                        <li>
                                            <a href="{$system['system_url']}/signout">{__("Log Out")}</a>
                                        </li>
                                    </ul>
                                </li>
                                <!-- user-menu -->
								<!-- user-manager -->
                                <li class = "dropdown hidden-smmore">
									<a href="#" class="dropdown-toggle user-menu" data-toggle="dropdown">
                                        <i class="fa fa-bars" aria-hidden="true"></i>
                                    </a>
									<ul class="dropdown-menu nav-pills nav-stacked nav-home">
										<!-- basic -->
										<li>
											<a href="{$system['system_url']}/{$user->_data['user_name']}">
												<img src="{$user->_data.user_picture}" alt="{$user->_data['user_fullname']}">
												<span>{$user->_data['user_fullname']}</span>
											</a>
										</li>
										<li>
											<a href="{$system['system_url']}/settings/profile">
												<i class="fa fa-pencil-square fa-fw pr10"></i> 
												{__("Edit Profile")}
											</a>
										</li>
										{if $user->_data['user_group'] == 1}
											<li>
												<a href="{$system['system_url']}/admin">
													<i class="fas fa-tachometer-alt fa-fw pr10"></i>
													{__("Admin Panel")}
												</a>
											</li>
										{/if}
										<!-- Coniu - Begin - Menu noga -->
										{if (($user->_is_admin) || ($user->_data['user_group'] == $smarty.const.USER_NOGA_MANAGE_ALL) || ($user->_data['user_group'] == $smarty.const.USER_NOGA_MANAGE_CITY)
												|| ($user->_data['user_group'] == $smarty.const.USER_NOGA_MANAGE_SCHOOL))}
											<li>
												<a href="{$system['system_url']}/noga/">
													<i class="fas fa-tachometer-alt fa-fw pr10"></i>
													{__("School Panel")}
												</a>
											</li>
										{/if}
										<!-- Coniu - End- Menu noga -->
										<!-- basic -->

										<!-- favorites -->
										<li class="ptb5">
											<small class="text-muted">{__("favorites")|upper}</small>
										</li>

										<li {if $view == ""}class="active"{/if}>
											<a href="{$system['system_url']}"><i class="fa fa-newspaper-o fa-fw pr10"></i> {__("News Feed")}</a>
										</li>
										<li>
											<a href="{$system['system_url']}/messages"><i class="far fa-comments fa-fw pr10"></i> {__("Messages")}</a>
										</li>
										<li>
											<a href="{$system['system_url']}/{$user->_data['user_name']}/photos"><i class="fa fa-file-image fa-fw pr10"></i> {__("Photos")}</a>
										</li>
										<li>
											<a href="{$system['system_url']}/{$user->_data['user_name']}/friends"><i class="fa fa-users fa-fw pr10"></i> {__("Friends")}</a>
										</li>
										<li {if $view == "saved"}class="active"{/if}>
											<a href="{$system['system_url']}/saved"><i class="fa fa-bookmark fa-fw pr10"></i> {__("Saved")}</a>
										</li>
										{if $system['games_enabled']}
											<li {if $view == "games"}class="active"{/if}>
												<a href="{$system['system_url']}/games"><i class="fa fa-gamepad fa-fw pr10"></i> {__("Games")}</a>
											</li>
										{/if}
										<!-- favorites -->

										<!-- ConIu - schools -->
										{include file="ci/index.left_menu.tpl"}
										<!-- schools -->

									</ul>
								</li>	
								<!-- end user-manager -->
                            {else}
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <span>{__("Join")}</span>
                                        <i class="caret"></i>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="{$system['system_url']}/signin">{__("Login")}</a>
                                        </li>
                                        <li>
                                            <a href="{$system['system_url']}/signup">{__("Register")}</a>
                                        </li>
                                    </ul>
                                </li>
                            {/if}
                        </ul>
                    </div>
                    <!-- navbar-container -->
                </div>
                <!-- navbar-collapse -->

            </div>
        </div>