<body class="header-fixed" data-chat-enabled="1">
<!-- DELETE START - MANHDD - 25/05/2021 - chưa có app chat trên facebook -->
{*<!-- Load Facebook SDK for JavaScript -->*}
{*<div id="fb-root"></div>*}
{*<script>(function(d, s, id) {*}
{*        var js, fjs = d.getElementsByTagName(s)[0];*}
{*        if (d.getElementById(id)) return;*}
{*        js = d.createElement(s); js.id = id;*}
{*        js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js#xfbml=1&version=v2.12&autoLogAppEvents=1';*}
{*        fjs.parentNode.insertBefore(js, fjs);*}
{*    }(document, 'script', 'facebook-jssdk'));</script>*}

{*<!-- Your customer chat code -->*}
{*<div class="fb-customerchat"*}
{*     attribution="setup_tool"*}
{*     page_id="1024684347570874"*}
{*     theme_color="#0084ff"*}
{*     logged_in_greeting="Xin chào, Coniu có thể giúp gì cho bạn?"*}
{*     logged_out_greeting="Xin chào, Coniu có thể giúp gì cho bạn?">*}
{*</div>*}
<!-- DELETE END - MANHDD - 25/05/2021 - chưa có app chat trên facebook -->
<div class="wrapper">
    <!--=== Header v6 ===-->
    <div class="header-v6 header-classic-white header-sticky header-fixed-shrink">
        <!-- Navbar -->
        <div class="navbar mega-menu" role="navigation">
            <div class="container menu_container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="menu-container">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Navbar Brand -->
                    <div class="navbar-brand">
                        <a href="{$system['system_url']}">
                            <div class="logo-img">
                                <img class="shrink-logo" style="height: 50px;" src="{$system['system_url']}/content/themes/{$system['theme']}/images/logo.png" alt="{$system['system_title']} | {__("School management and connection software")}" title="{$system['system_title']} | {__("School management and connection software")}">
{*                                <div class="logo-slogan" style="margin:auto;">{__("Connect love")}</div>*}
                            </div>

                        </a>
                    </div>
                    <!-- ENd Navbar Brand -->

                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse navbar-responsive-collapse">
                    <div class="menu-container">
                        <ul class="nav navbar-nav">
                            <li class="active"><a href="{$system['system_url']}">{__("Home")}</a></li>
{*                            <li class=""><a href="https://blog.coniu.vn/inet-la-gi/" target="_blank">{__("Introduction")}</a></li>*}
                            {if $system['language']['code'] == 'vi_VN'}
                                <li class=""><a href="https://sll.mobiedu.vn/static/huong_dan_su_dung" target="_blank">{__("Guide")}</a></li>
                            {elseif $system['language']['code'] == 'en_us'}
                            <li class=""><a href="https://sll.mobiedu.vn/static/guide" target="_blank">{__("Guide")}</a></li>
                            {/if}
                            {* CI - Tạm ẩn chờ xin cấp phép *}
                            {*<li class=""><a href="https://blog.coniu.vn/" target="_blank">{__("Blog")}</a></li>
                            <li class=""><a href="https://blog.coniu.vn/category/truyen/" target="_blank">{__("Tale")}</a></li>
                            <li class=""><a href="https://blog.coniu.vn/category/video/" target="_blank">{__("Video")}</a></li>*}
                            {* END - CI *}
{*                            <li class=""><a href="https://blog.coniu.vn/category/tin-mam-non" target="_blank">{__("Pre - school")}</a></li>*}
{*                            <li class=""><a href="https://elearning.mascom.com.vn/" target="_blank">{__("E-Learning")}</a></li>*}
                        </ul>
                    </div>
                </div><!--/navbar-collapse-->
            </div>
        </div>
    </div>
    <!-- End Navbar -->
    <!--=== End Header v6 ===-->