{include file='_head.tpl'}
{include file='_header.tpl'}

<!-- page content -->
<div class="container mt20">
    <div class="row">

        <div class="col-md-3 col-sm-3">
            <div class="panel panel-default">
                <div class="panel-body with-nav">
                    <ul class="side-nav metismenu js_metisMenu">
                        <!-- Dashboard -->
                        <li {if $view == ""}class="active selected"{/if}>
                            <a href="{$system['system_url']}/noga/">
                                <i class="fas fa-tachometer-alt fa-fw fa-lg pr10"></i> {__("Dashboard")}
                            </a>
                        </li>
                        <!-- Dashboard -->
                        <!-- School -->
                        <li {if $view == "attendance"}class="active selected"{/if}>
                            <a href="{$system['system_url']}/noga/schools">
                                <i class="fa fa-university fa-fw fa-lg pr10"></i> {__("School")}
                                <span class="fa arrow"></span>
                            </a>
                            <ul>
                                <li {if $view == "attendance" && $sub_view == ""}class="active selected"{/if}>
                                    <a href="{$system['system_url']}/noga/schools">
                                        <i class="fa fa-caret-right fa-fw pr10"></i>{__("Lists")}
                                    </a>
                                </li>
                                <li {if $view == "attendance" && $sub_view == "add"}class="active selected"{/if}>
                                    <a href="{$system['system_url']}/noga/schools/add">
                                        <i class="fa fa-caret-right fa-fw pr10"></i>{__("Add New")}
                                    </a>
                                </li>
                                <li {if $view == "attendance" && $sub_view == "add_system_school"}class="active selected"{/if}>
                                    <a href="{$system['system_url']}/noga/schools/addsystemschool">
                                        <i class="fa fa-caret-right fa-fw pr10"></i>{__("Add new system school")}
                                    </a>
                                </li>
                                <li {if $view == "attendance" && $sub_view == "listsystemschool"}class="active selected"{/if}>
                                    <a href="{$system['system_url']}/noga/schools/listsystemschool">
                                        <i class="fa fa-caret-right fa-fw pr10"></i>{__("System school list")}
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <!--  ADD START - ManhDD 03/04/2021  -->
                        <!-- Subject -->
                        <li {if $view == "subjects"}class="active selected"{/if}>
                            <a href="{$system['system_url']}/noga/subjects">
                                <i class="fa fa-book fa-fw fa-lg pr10"></i> {__("Subject")}
                                <span class="fa arrow"></span>
                            </a>
                            <ul>
                                <li {if $view == "subjects" && $sub_view == ""}class="active selected"{/if}>
                                    <a href="{$system['system_url']}/noga/subjects">
                                        <i class="fa fa-caret-right fa-fw pr10"></i>{__("Lists")}
                                    </a>
                                </li>
                                <li {if $view == "attendance" && $sub_view == "add"}class="active selected"{/if}>
                                    <a href="{$system['system_url']}/noga/subjects/add">
                                        <i class="fa fa-caret-right fa-fw pr10"></i>{__("Add New")}
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <!--  ADD END - ManhDD 03/04/2021  -->
                        <!-- user -->
                        <li {if $view == "users"}class="active"{/if}>
                            <a href="{$system['system_url']}/noga/users">
                                <i class="fa fa-user fa-fw fa-lg pr10"></i>{__("Users")}
                                <span class="fa arrow"></span>
                            </a>
                            <ul>
                                <li {if $view == "users" && $sub_view == ""}class="active selected"{/if}>
                                    <a href="{$system['system_url']}/noga/users">
                                        <i class="fa fa-caret-right fa-fw pr10"></i>{__("Show All Users")}
                                    </a>
                                </li>
                                <li {if $view == "users" && $sub_view == "userregion"}class="active selected"{/if}>
                                    <a href="{$system['system_url']}/noga/users/userregion">
                                        <i class="fa fa-caret-right fa-fw pr10"></i>{__("Regional management account")}
                                    </a>
                                </li>
                                <li {if $view == "users" && $sub_view == "admins"}class="active selected"{/if}>
                                    <a href="{$system['system_url']}/noga/users/admins">
                                        <i class="fa fa-caret-right fa-fw pr10"></i>{__("List Admins")}
                                    </a>
                                </li>
                                <li {if $view == "users" && $sub_view == "moderators"}class="active selected"{/if}>
                                    <a href="{$system['system_url']}/noga/users/moderators">
                                        <i class="fa fa-caret-right fa-fw pr10"></i>{__("List Moderators")}
                                    </a>
                                </li>
                                <li {if $view == "users" && $sub_view == "online"}class="active selected"{/if}>
                                    <a href="{$system['system_url']}/noga/users/online">
                                        <i class="fa fa-caret-right fa-fw pr10"></i>{__("List Online")}
                                    </a>
                                </li>
                                <li {if $view == "users" && $sub_view == "banned"}class="active selected"{/if}>
                                    <a href="{$system['system_url']}/noga/users/banned">
                                        <i class="fa fa-caret-right fa-fw pr10"></i>{__("List Banned")}
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <!-- End user -->

                        <!-- Push notifications -->
                        <li {if $view == "notifications"}class="active"{/if}>
                            <a href="{$system['system_url']}/noga/notifications">
                                <i class="fa fa-bell fa-fw fa-lg pr10"></i>{__("Notifications")}
                                <span class="fa arrow"></span>
                            </a>
                            <ul>
                                <li {if $view == "notifications" && $sub_view == "immediately"}class="active selected"{/if}>
                                    <a href="{$system['system_url']}/noga/notifications/immediately">
                                        <i class="fa fa-caret-right fa-fw pr10"></i>{__("Notify immediately")}
                                    </a>
                                </li>
                                <li {if $view == "notifications" && $sub_view == "schedule"}class="active selected"{/if}>
                                    <a href="{$system['system_url']}/noga/notifications/schedule">
                                        <i class="fa fa-caret-right fa-fw pr10"></i>{__("Appointment")}
                                    </a>
                                </li>
                                <li {if $view == "users" && $sub_view == "list"}class="active selected"{/if}>
                                    <a href="{$system['system_url']}/noga/notifications/list">
                                        <i class="fa fa-caret-right fa-fw pr10"></i>{__("Lists")}
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <!-- Push notifications -->

                        <!-- Thống kê tương tác các trường -->
                        <li {if $view == "statistics"}class="active"{/if}>
                            <a href="{$system['system_url']}/noga/statistics/schools">
                                <i class="fas fa-chart-pie fa-fw fa-lg pr10"></i>{__("Statistics")}
                                <span class="fa arrow"></span>
                            </a>
                            <ul>
                                <li {if $view == "statistics" && $sub_view == "usersonline"}class="active selected"{/if}>
                                    <a href="{$system['system_url']}/noga/statistics/usersonline">
                                        <i class="fa fa-caret-right fa-fw pr10"></i>{__("Users online")}
                                    </a>
                                </li>
                                <li {if $view == "statistics" && $sub_view == "schools"}class="active selected"{/if}>
                                    <a href="{$system['system_url']}/noga/statistics/schools">
                                        <i class="fa fa-caret-right fa-fw pr10"></i>{__("School")}
                                    </a>
                                </li>
                                {*<li {if $view == "statistics" && $sub_view == "users"}class="active selected"{/if}>
                                    <a href="{$system['system_url']}/noga/statistics/users">
                                        <i class="fa fa-caret-right fa-fw pr10"></i>{__("Users")}
                                    </a>
                                </li>*}
                                <li {if $view == "statistics" && $sub_view == "topics"}class="active selected"{/if}>
                                    <a href="{$system['system_url']}/noga/statistics/topics">
                                        <i class="fa fa-caret-right fa-fw pr10"></i>{__("Topics")}
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <!-- Thống kê tương tác các trường -->


                        <!-- development -->
                        <li {if $view == "foetusdevelopments"}class="active"{/if}>
                            <a href="{$system['system_url']}/noga/foetusdevelopments">
                                <i class="fas fa-chart-line fa-fw fa-lg pr10"></i>{__("Foetus development")}
                                <span class="fa arrow"></span>
                            </a>
                            <ul>
                                <li {if $view == "foetusdevelopments" && $sub_view == ""}class="active selected"{/if}>
                                    <a href="{$system['system_url']}/noga/foetusdevelopments">
                                        <i class="fa fa-caret-right fa-fw pr10"></i>{__("Lists")}
                                    </a>
                                </li>
                                <li {if $view == "foetusdevelopments" && $sub_view == "add"}class="active selected"{/if}>
                                    <a href="{$system['system_url']}/noga/foetusdevelopments/add">
                                        <i class="fa fa-caret-right fa-fw pr10"></i>{__("Add New")}
                                    </a>
                                </li>
                                <li {if $view == "foetusdevelopments" && $sub_view == "push"}class="active selected"{/if}>
                                    <a href="{$system['system_url']}/noga/foetusdevelopments/push">
                                        <i class="fa fa-caret-right fa-fw pr10"></i>{__("Push")}
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <!-- child development -->
                        <li {if $view == "childdevelopments"}class="active"{/if}>
                            <a href="{$system['system_url']}/noga/childdevelopments">
                                <i class="fas fa-chart-bar fa-fw fa-lg pr10"></i>{__("Child development")}
                                <span class="fa arrow"></span>
                            </a>
                            <ul>
                                <li {if $view == "childdevelopments" && $sub_view == ""}class="active selected"{/if}>
                                    <a href="{$system['system_url']}/noga/childdevelopments">
                                        <i class="fa fa-caret-right fa-fw pr10"></i>{__("Lists")}
                                    </a>
                                </li>
                                <li {if $view == "childdevelopments" && $sub_view == "add"}class="active selected"{/if}>
                                    <a href="{$system['system_url']}/noga/childdevelopments/add">
                                        <i class="fa fa-caret-right fa-fw pr10"></i>{__("Add New")}
                                    </a>
                                </li>
                                <li {if $view == "childdevelopments" && $sub_view == "push"}class="active selected"{/if}>
                                    <a href="{$system['system_url']}/noga/childdevelopments/push">
                                        <i class="fa fa-caret-right fa-fw pr10"></i>{__("Push")}
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <!-- End child development -->
                        <!-- child based on month -->
                        <li {if $view == "childmonths"}class="active"{/if}>
                            <a href="{$system['system_url']}/noga/childmonths">
                                <i class="fas fa-chart-area fa-fw fa-lg pr10"></i>{__("Month age information")}
                                <span class="fa arrow"></span>
                            </a>
                            <ul>
                                <li {if $view == "childmonths" && $sub_view == ""}class="active selected"{/if}>
                                    <a href="{$system['system_url']}/noga/childmonths">
                                        <i class="fa fa-caret-right fa-fw pr10"></i>{__("Lists")}
                                    </a>
                                </li>
                                <li {if $view == "childmonths" && $sub_view == "add"}class="active selected"{/if}>
                                    <a href="{$system['system_url']}/noga/childmonths/add">
                                        <i class="fa fa-caret-right fa-fw pr10"></i>{__("Add New")}
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <!-- End child based on month -->
                        <!-- foetus based on week -->
                        <li {if $view == "pregnancys"}class="active"{/if}>
                            <a href="{$system['system_url']}/noga/pregnancys">
                                <i class="far fa-chart-bar fa-fw fa-lg pr10"></i>{__("Pregnancy information")}
                                <span class="fa arrow"></span>
                            </a>
                            <ul>
                                <li {if $view == "pregnancys" && $sub_view == ""}class="active selected"{/if}>
                                    <a href="{$system['system_url']}/noga/pregnancys">
                                        <i class="fa fa-caret-right fa-fw pr10"></i>{__("Lists")}
                                    </a>
                                </li>
                                <li {if $view == "pregnancys" && $sub_view == "add"}class="active selected"{/if}>
                                    <a href="{$system['system_url']}/noga/pregnancys/add">
                                        <i class="fa fa-caret-right fa-fw pr10"></i>{__("Add New")}
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <!-- End foetus based on week -->
                        <!-- Danh sách tài khoản mới -->
                        <li {if $view == "usernews"}class="active"{/if}>
                            <a href="{$system['system_url']}/noga/usernews">
                                <i class="fa fa-user fa-fw fa-lg pr10"></i>{__("List new user")}
                            </a>
                        </li>
                        <!-- End danh sách tài khoản mới -->
                        <!-- Danh quản lý và giáo viên -->
                        <li {if $view == "manageandteachers"}class="active"{/if}>
                            <a href="{$system['system_url']}/noga/manageandteachers">
                                <i class="fa fa-user fa-fw fa-lg pr10"></i>{__("Manage and teacher")}
                            </a>
                        </li>
                        <!-- End danh sách quản lý và giáo viên -->
                        <!-- Danh sách trang -->
                        <li {if $view == "pages"}class="active"{/if}>
                            <a href="{$system['system_url']}/noga/pages">
                                <i class="fa fa-flag fa-fw fa-lg pr10"></i>{__("Pages")}
                            </a>
                        </li>
                        <!-- End Danh sách trang -->
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-9 col-sm-9">
            {if $view == ""}
                {include file='ci/noga/noga.dashboard.tpl'}
            {else}
                {include file="ci/noga/noga.$view.tpl"}
            {/if}
        </div>
    </div>
</div>
<!-- page content -->


{include file='_footer.tpl'}