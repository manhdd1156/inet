
{include file='_head_new.tpl'}
{include file='_header_new.tpl'}

    <div id="top"></div>
    <div class="">
        <div class="banner_home">
            <img class="img-responsive banner_top" src="{$system['system_url']}/content/themes/{$system['theme']}/images/img/top.jpg" alt="" />
            <div class = "store">
                <div class = "google">
                    <a href = "{$smarty.const.GOOGLEPLAY_URL}" target="_blank"> <img class="img-responsive google_img" src="{$system['system_url']}/content/themes/{$system['theme']}/images/img/google.png" alt="" /> </a>
                </div>
                <div class = "ios">
                    <a href = "{$smarty.const.APPSTORE_URL}" target="_blank"><img class="img-responsive ios_img" src="{$system['system_url']}/content/themes/{$system['theme']}/images/img/ios.png" alt="" /> </a>
                </div>
            </div>
        </div>
    </div>

    <div id="introduction"></div>
    <div class="container content-sm">
        <div class="text-center margin-bottom-50">
            <h2 class="title-v2 title-center">Inet LÀ GÌ?</h2>
            <p class="space-lg-hor"><strong>Ứng dụng tương tác Trường học – Inet</strong> là mạng xã hội thu nhỏ, nơi chia sẻ kiến thức, kinh nghiệm trong quá trình dạy con. Inet hỗ trợ ban giám hiệu, giáo viên và phụ huynh tương tác, trao đổi thông tin dễ dàng và nhanh chóng.</p>
        </div>

        <div class="row content-boxes-v4">
            <div class="col-md-4 md-margin-bottom-40">
                <i class="pull-left fa fa-home"></i>
                <div class="content-boxes-in-v4">
                    <h2>Phụ huynh</h2>
                    <p>Nơi chia sẻ, học hỏi kinh nghiệm trong quá trình nuôi dạy con. Theo sát quá trình phát triển của con cả về thể chất và trí tuệ.</p>
                </div>
            </div>
            <div class="col-md-4 md-margin-bottom-40">
                <i class="pull-left fa fa-street-view"></i>
                <div class="content-boxes-in-v4">
                    <h2>Giáo viên</h2>
                    <p>Trao đổi với phụ huynh nhanh chóng, trực tiếp, giúp công tác nuôi dạy dễ dàng, phù hợp với thể chất và tính cách từng trẻ.</p>
                </div>
            </div>
            <div class="col-md-4">
                <i class="pull-left fa fa-university"></i>
                <div class="content-boxes-in-v4">
                    <h2>Nhà trường</h2>
                    <p>Quản lý học sinh và giáo viên dễ dàng, kịp thời nắm bắt phản hồi phụ huynh và điều chỉnh công tác quản lý nhà trường nếu cần.</p>
                </div>
            </div>
        </div>
    </div>

    <div class="bg-grey content-sm">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="responsive-video margin-bottom-30">
                        <iframe src="https://www.youtube.com/embed/Rf2tk8pUn9Y" width="530" height="300" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
                    </div>
                </div>
                <div class="col-md-6">
                    <h2 class="title-v2">KHỞI NGUỒN Inet</h2>
                    <p>Hiểu được rằng “Tương tác tốt hơn giáo dục tốt hơn”, Inet ra đời với sứ mệnh “Kết nối yêu thương” là cầu nối giúp trao đổi thông tin giữa nhà trường, giáo viên và phụ huynh diễn ra nhanh chóng, thuận lợi.</p><br>
                    <a href="#contact" class="btn-u" rel="nofollow">Nhà trường đăng ký</a>
                    <a href="signup" class="btn-u btn-brd btn-brd-hover btn-u-dark">Phụ huynh đăng ký</a>
                </div>
            </div>
        </div>
    </div>
    <div class="container content-sm">
        <div class="row">
            <div class="col-sm-6 content-boxes-v3 content-boxes-v3-right sm-margin-bottom-30">
                <div class="margin-bottom-30">
                    <i class="icon-custom icon-md rounded-x icon-bg-u icon-line icon-social-facebook"></i>
                    <div class="content-boxes-in-v3">
                        <h2 class="heading-sm">Mạng xã hội</h2>
                        <p>Tham gia các trang, hội nhóm để chia sẻ, học hỏi kinh nghiệm nuôi dạy con. Trao đổi trực tiếp với giáo viên và quản lý nhà trường.</p>
                    </div>
                </div>
                <div class="clearfix margin-bottom-30">
                    <i class="icon-custom icon-md rounded-x icon-bg-darker icon-line icon-directions"></i>
                    <div class="content-boxes-in-v3">
                        <h2 class="heading-sm">Trang nhà trường</h2>
                        <p>Là nơi thông báo các thông tin tuyển sinh, các hoạt động ngoại khóa. Và là kênh quảng bá về trường tới cộng đồng hiệu quả.</p>
                    </div>
                </div>
                <div class="clearfix">
                    <i class="icon-custom icon-md rounded-x icon-bg-u icon-line icon-rocket"></i>
                    <div class="content-boxes-in-v3">
                        <h2 class="heading-sm">Chia sẻ khoảnh khắc</h2>
                        <p>Tham gia nhóm phụ huynh lớp con để theo dõi và chia sẻ các khoảnh khoắc của con hàng ngày, hàng giờ tại trường và ở nhà.</p>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 content-boxes-v3">
                <div class="clearfix margin-bottom-30">
                    <i class="icon-custom icon-md rounded-x icon-bg-darker icon-line icon-bar-chart"></i>
                    <div class="content-boxes-in-v3">
                        <h2 class="heading-sm">Thông tin minh bạch</h2>
                        <p>Thông tin quản lý về học sinh minh bạch, chặt chẽ và dễ dàng theo dõi mọi lúc, mọi nơi. Thông tin cập nhật được thông báo ngay lập tức.</p>
                    </div>
                </div>
                <div class="clearfix margin-bottom-30">
                    <i class="icon-custom icon-md rounded-x icon-bg-u icon-line icon-badge"></i>
                    <div class="content-boxes-in-v3">
                        <h2 class="heading-sm">Quản lý dễ dàng</h2>
                        <p>Cung cấp tiện ích quản lý: Danh bạ, Điểm danh/xin nghỉ, Gửi thuốc, Dịch vụ/đón muộn, Học phí, Thông báo, Đăng ký sự kiện...</p>
                    </div>
                </div>
                <div class="clearfix">
                    <i class="icon-custom icon-md rounded-x icon-bg-darker icon-line icon-envelope"></i>
                    <div class="content-boxes-in-v3">
                        <h2 class="heading-sm">Bảo mật</h2>
                        <p>Hệ thống sử dụng giao thức truyền tải thông tin bảo mật (https), chứng chỉ do liên hiệp Google, Facebook, Cisco… cung cấp.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="register"></div>
    <div class="parallax-counter-v1 parallaxBg">
        <div class="container">
            <h2 class="title-v2 title-light title-center">QUY TRÌNH ĐĂNG KÝ TRƯỜNG</h2>
            <p class="space-xlg-hor text-center color-light">Chỉ cần 2 phút để đăng ký sử dụng hệ thống Inet đối với nhà trường, theo quy trình sau <br/>(phụ huynh và người dùng thông thường, <a href="signup">đăng ký tại ĐÂY</a>).</p>

            <div class="margin-bottom-40"></div>

            <div class="row margin-bottom-10">
                <div class="col-sm-4 col-xs-12 md-margin-bottom-10">
                    <div class="counters">
                        <span>1. Điền thông tin</span>
                        <h4><a href="#contact" class="btn-u" rel="nofollow">Bấm vào đây</a></h4>
                    </div>
                </div>
                <div class="col-sm-4 col-xs-12 md-margin-bottom-10">
                    <div class="counters">
                        <span>2. Xác minh</span>
                        <h4>Inet</h4>
                    </div>
                </div>
                <div class="col-sm-4 col-xs-12">
                    <div class="counters">
                        <span>3. Tạo trường</span>
                        <h4>Inet</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container content-sm">
        <div class="text-center margin-bottom-50">
            <h2 class="title-v2 title-center">HÌNH ẢNH ỨNG DỤNG</h2>
        </div>

        <div class="row">
            <div class="col-md-4 marbot10">
                <img class="img-responsive" src="{$system['system_url']}/content/themes/{$system['theme']}/images/img/apps/inet.big1.jpg" alt="">
            </div>
            <div class="col-md-4 marbot10">
                <img class="img-responsive" src="{$system['system_url']}/content/themes/{$system['theme']}/images/img/apps/inet.big2.jpg" alt="">
            </div>
            <div class="col-md-4 marbot10">
                <img class="img-responsive" src="{$system['system_url']}/content/themes/{$system['theme']}/images/img/apps/inet.big3.jpg" alt="">
            </div>
        </div>
    </div>

    <ul class="list-unstyled row portfolio-box-v1">
        <li class="col-md-3 col-sm-6 marbot10">
            <img class="img-responsive" src="{$system['system_url']}/content/themes/{$system['theme']}/images/img/apps/img1.jpg" alt="">
            <div class="portfolio-box-v1-in">
                <h3>Quản lý trường</h3>
            </div>
        </li>
        <li class="col-md-3 col-sm-6 marbot10">
            <img class="img-responsive" src="{$system['system_url']}/content/themes/{$system['theme']}/images/img/apps/img2.jpg" alt="">
            <div class="portfolio-box-v1-in">
                <h3>Theo dõi thông tin</h3>
            </div>
        </li>
        <li class="col-md-3 col-sm-6 marbot10">
            <img class="img-responsive" src="{$system['system_url']}/content/themes/{$system['theme']}/images/img/apps/img3.jpg" alt="">
            <div class="portfolio-box-v1-in">
                <h3>Trao đổi trực tuyến</h3>
            </div>
        </li>
        <li class="col-md-3 col-sm-6 marbot10">
            <img class="img-responsive" src="{$system['system_url']}/content/themes/{$system['theme']}/images/img/apps/img4.jpg" alt="">
            <div class="portfolio-box-v1-in">
                <h3>Chia sẻ khoảnh khắc</h3>
            </div>
        </li>
    </ul>

    <div id="contact"></div>
    <div class="bg-grey content-sm">
        <div class="container text-center">
            <h2 class="title-v2 title-center">ĐĂNG KÝ TRƯỜNG - GÓP Ý</h2>
            <p class="space-lg-hor">Chúng tôi trân trọng và mong muốn nhận được ý kiến đóng góp của bạn để dịch vụ ngày càng hoàn thiện.</p><br>
        </div>
    </div>

    <!-- liên hệ - taila -->
    <div class="container content-sm">
        <!-- contact taila -->
        <div class="row contact_row">
            <div class="contact">
                <div class="col-xs-12 col-sm-7">
                    <div class="contact_form">
                        <form class="js_ajax-forms" data-url= "ci/bo/school/bo_contact.php" action = "_email">
                            <div class="row">
                                <div class="col-xs-12 col-sm-6">
                                    <div class="control-group">
                                        <div class="control">
                                            <input type="text" id="name2" name="name2" placeholder="{__('School name/You name')} (*)" class="form-control" required maxlength="255">
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <div class="control">
                                            <input type="email" class="form-control" name="email2" placeholder="Email (*)" required maxlength="50">
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <div class="control">
                                            <input type="text" id="phone_contact" name="phone2" placeholder="{__('Telephone')}" class="form-control" required/>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <div class="controls">
                                            <div class="form-group">
                                                <select name="select_school2" id="select_school2" class="form-control">
                                                    <option value="1">Đăng ký sử dụng cho trường</option>
                                                    <option value="2">Góp ý cho Coniu</option>
                                                    <option value="3">Thông báo sự cố/lỗi</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <div class="control-group">
                                        <textarea rows="8" id="content_contact" name="content2" placeholder="{__('Content')} (*)" class="form-control content_class" required></textarea>
                                    </div>
                                </div>

                            </div>
                            {if $system.reCAPTCHA_enabled}
                            <div class="form-group">
                                <!-- reCAPTCHA -->
                                <script src='https://www.google.com/recaptcha/api.js?hl=vi'></script>
                                <div class="g-recaptcha" data-sitekey="{$system.reCAPTCHA_site_key}"></div>
                                <!-- reCAPTCHA -->
                            </div>
                            {/if}
                            <div class="control-ci">
                                <button type="submit" class="btn btn-success">Gửi tin nhắn</button>
                            </div>
                            <br/>
                            <!-- success -->
                            <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                            <!-- success -->
                            <!-- error -->
                            <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                            <!-- error -->
                        </form>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-5">
                    <div class="contact_text">
                        <h2 class="title-v2">LIÊN HỆ</h2>
                        <ul>
                            <li>Điện thoại: <a href="tel:0966900466">0966 900 466</a></li>
                            <li>Email: <a href="mailto:contact@mobiedu.vn">contact@mobiedu.vn</a></li>
                            <li>Facebook: <a href="#" target="_blank">https://facebook.com/inet.edu.vn</a></li>
                            <br/>
                            <li>Hệ thống được xây dựng bởi <a href="https://www.noga.vn" target="_blank" title="Công ty Cổ phần NOGA">Công ty Cổ phần NOGA</a>
                                <br/>Mã doanh nghiệp: 0107455916.
                                <br/>Website: <a href="https://www.noga.vn" target="_blank" title="Công ty Cổ phần NOGA">https://www.noga.vn</a>
                                <br/>Địa chỉ: Số 32, Ngõ 241, Đường Chiến Thắng, Hà Đông, Hà Nội.
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end liên hệ taila -->

    {include file='_footer_new.tpl'}
