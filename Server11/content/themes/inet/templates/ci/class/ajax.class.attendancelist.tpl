<ul class="nav nav-tabs mb10" id="myTab" role="tablist">
    <li class="nav-item active">
        <a class="nav-link" id="came-tab" data-toggle="tab" href="#come_attendance" role="tab" aria-controls="came" aria-selected="true">{__("Điểm danh đến")}</a>
    </li>
    {if $data['isRollUp']}
        <li class="nav-item">
            <a class="nav-link" id="go-tab" data-toggle="tab" href="#go_attendance" role="tab" aria-controls="go" aria-selected="false">{__("Điểm danh về")}</a>
        </li>
    {/if}
</ul>
<div class="tab-content" id="myTabContent">
    <div class="tab-pane fade active in" id="come_attendance" role="tabpanel" aria-labelledby="came-tab">
        <div class="form-group">
            <div class="col-sm-9">
                <button type="submit" class="btn btn-primary padrl30" {if (count($data['detail']) == 0)}disabled{/if}>{__("Save")}</button>
            </div>
        </div>

        <div>
            {if ($data['isRollUp'] == 0) || ($data['is_checked'] == 0)}
                <strong>{__("Attendance list")}&nbsp;({$data['detail']|count})</strong>
            {else}
                {$data['user_fullname']}  {__("have rolled up at")}  {$data['recorded_at']}
                <br/>
                <strong>{__("Present")}</strong>:&nbsp;{$data['present_count']}&nbsp;
                <strong>{__("Absence")}</strong>:&nbsp;{$data['absence_count']}
            {/if}
        </div>
        <input type="hidden" name="attendance_id" value="{$data['attendance_id']}"/>
        <table class="table table-striped table-bordered table-hover table-responsive">
            <thead>
            <tr>
                <th rowspan="2">#</th>
                <th rowspan="2" width="60px">{__("Child picture")}</th>
                <th rowspan="2">{__("Full name")}</th>
                <th colspan="3">
                    {__("Status")}
                </th>
                <th rowspan="2">{__("Note")}</th>
            </tr>
            <tr>
                <th width="80px">{__("Present")}</th>
                <th width="80px">{__("With permission")}</th>
                <th width="80px">{__("Without permission")}</th>
            </tr>
            </thead>
            <tbody>
            {$idx = 1}
            {foreach $data['detail'] as $row}
                <tr {if $row['child_status'] == 0} class="row-disable" {/if}>
                    <td align="center" class="align-middle" {if $row['is_parent']}rowspan="2"{/if}>{$idx}</td>
                    <td align="center" class="align-middle" {if $row['is_parent']}rowspan="2"{/if}>
                        <div class="img_picture_box">
                            <img class="img-responsive" src="{$row['child_picture']}">
                        </div>
                    </td>
                    <td class="align-middle" {if $row['is_parent']}rowspan="2"{/if}>
                        <a href="{$system['system_url']}/class/{$username}/attendance/child/{$row['child_id']}">{$row['child_name']}</a>
                        &nbsp;&nbsp;
                        {if ($row['is_parent'] > 0) && ($row['feedback'] == 0)}
                            <a class="btn btn-xs btn-danger js_class-confirm-attendance" id="button_{$row['attendance_detail_id']}" data-username="{$username}"
                               data-id="{$row['child_id']}" data-detail-id="{$row['attendance_detail_id']}">{__("Confirm")}</a>
                        {/if}
                    </td>
                    <td align="center" class="align-middle">
                        <input type="radio" name="rollup_{$row['child_id']}" value="{$smarty.const.ATTENDANCE_PRESENT}" {if ($data['is_checked'] == 1)}
                            {if $row['status'] == $smarty.const.ATTENDANCE_PRESENT}
                                checked
                            {/if}
                                {elseif ($data['is_checked'] == 0 && $data['isRollUp'] > 0)}
                            {if !isset($row['status']) || $row['status'] == $smarty.const.ATTENDANCE_PRESENT}
                                checked
                            {/if}
                                {elseif ($data['isRollUp'] == 0)}
                        checked
                                {/if}>
                    </td>
                    <td align="center" class="align-middle">
                        <input type="radio" name="rollup_{$row['child_id']}" value="{$smarty.const.ATTENDANCE_ABSENCE}" {if (($data['is_checked'] == 1) && ($row['status'] == $smarty.const.ATTENDANCE_ABSENCE)) || ($data['isRollUp'] && isset($row['status']) && $row['status'] == $smarty.const.ATTENDANCE_ABSENCE)}checked{/if}>
                    </td>
                    <td align="center" class="align-middle">
                        <input type="radio" name="rollup_{$row['child_id']}" value="{$smarty.const.ATTENDANCE_ABSENCE_NO_REASON}" {if ($data['is_checked'] == 1 || $data['isRollUp']) && isset($row['status']) && ($row['status'] == $smarty.const.ATTENDANCE_ABSENCE_NO_REASON)}checked{/if}>
                    </td>
                    <td class="align-middle" {if $row['is_parent']}rowspan="2"{/if}>
                        {*<input type="text" name="reason_{$row['child_id']}" id="reason_{$row['child_id']}" maxlength="512" value="{$row['reason']}" placeholder="{__("Absent reason")}">*}
                        <input type="text" name="allReasons[]" maxlength="512" style="width: 100%" value="{$row['reason']}" placeholder="{__("Absent reason")}">
                    </td>
                    <input type="hidden" name="allChildIds[]" value="{$row['child_id']}">
                    <input type="hidden" name="allResignTime[]" value="{$row['resign_time']}">
                    <input type="hidden" name="allIsParent[]" value="{$row['is_parent']}">
                    <input type="hidden" name="allRecordUserId[]" value="{$row['recorded_user_id']}">
                    <input type="hidden" name="allStartDate[]" value="{$row['start_date']}">
                    <input type="hidden" name="allEndDate[]" value="{$row['end_date']}">
                    <input type="hidden" name="feedback_{$row['child_id']}" value="{$row['feedback']}" id="feedback_{$row['child_id']}">
                </tr>
                {if $row['is_parent']}
                    <tr>
                        <td colspan="3" align="center">{__("Resign")}: {$row['resign_time']}</td>
                    </tr>
                {/if}
                {$idx = $idx + 1}
            {/foreach}

            {if $idx == 1}
                <tr class="odd">
                    <td valign="top" align="center" colspan="7" class="dataTables_empty">
                        {__("No data available in table")}
                    </td>
                </tr>
            {/if}

            </tbody>
        </table>
    </div>

    <div class="tab-pane fade" id="go_attendance" role="tabpanel" aria-labelledby="go-tab">
        <div class="form-group">
            <div class="col-sm-9">
                <button type="submit" class="btn btn-primary padrl30" {if (count($data_cameback['detail']) == 0)}disabled{/if}>{__("Save")}</button>
            </div>
        </div>

        <div>
            {if ($data_cameback['isRollUp'] == 0)}
                {*<strong>{__("Attendance list")}&nbsp;({$data_cameback['detail']|count})</strong>*}
            {else}
                {$data_cameback['user_fullname']}  {__("have rolled up at")}  {$data_cameback['recorded_at']}
                <br/>
                <strong>{__("Came back")}</strong>:&nbsp;{$data_cameback['came_back_count']}&nbsp;
                <strong>{__("Not came back")}</strong>:&nbsp;{$data_came_back['not_came_back_count']}
            {/if}
        </div>
        <input type="hidden" name="attendance_id" value="{$data_cameback['attendance_id']}"/>
        <table class="table table-striped table-bordered table-hover table-responsive">
            <thead>
            <tr>
                <th>#</th>
                <th>{__("Child picture")}</th>
                <th>{__("Full name")}</th>
                <th>{__("Out of school")}</th>
                <th>{__("Note")}</th>
                <th>{__("Came back")} <input type="checkbox" id="attendance_came_back_checkall"> </th>
            </tr>
            </thead>
            <tbody>
            {$idx = 1}
            {foreach $data_cameback['detail'] as $row}
                {if $row['child_status'] == 1 && $row['status'] != 0 && $row['status'] != 4}
                    <tr {if $row['child_status'] == 0 || $row['status'] == 0 || $row['status'] == 4} class="row-disable" {/if}>
                        <td align="center" class="align-middle">{$idx}</td>
                        <td align="center" class="align-middle">
                            <div class="img_picture_box">
                                <img class="img-responsive" src="{$row['child_picture']}">
                            </div>
                        </td>
                        <td class="align-middle">
                            <a href="{$system['system_url']}/school/{$username}/attendance/child/{$row['child_id']}">{$row['child_name']}</a>
                        </td>
                        <td align="center" class="align-middle">
                            <input type='text' name="cameback_time[]" class="cameback_time"
                                   id="cameback_time_{$row['child_id']}" value="{if $row['came_back_status']}{$row['came_back_time']}{else}{$hour}{/if}"
                                   style="width: 70px; color:blue; text-align: center"/>
                        </td>
                        <td class="align-middle">
                            <input type='text' name="cameback_note[]" class="cameback_note" value="{$row['came_back_note']}"/>
                        </td>
                        <td class="align-middle" align="center">
                            {*<input type="text" name="reason_{$row['child_id']}" id="reason_{$row['child_id']}" maxlength="512" value="{$row['reason']}" placeholder="{__("Absent reason")}">*}
                            <input type="checkbox" name="status_{$row['child_id']}" value="{$row['came_back_status']}" class="attendance_cameback_check" {if $row['came_back_status']}checked{/if}>
                        </td>
                        <input type="hidden" name="allChildBackIds[]" value="{$row['child_id']}">
                    </tr>
                    {$idx = $idx + 1}
                {/if}
            {/foreach}

            {if $idx == 1}
                <tr class="odd">
                    <td valign="top" align="center" colspan="5" class="dataTables_empty">
                        {__("No data available in table")}
                    </td>
                </tr>
            {/if}

            </tbody>
        </table>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
    $(document).ready(function(){
        $('.cameback_time').datetimepicker({
            format: TIME_FORMAT,
            sideBySide: true
        });
    });
    $('body').on('focus', '.cameback_time', function () {
        var currentTIme = $(this).val();
        if ($(this).val() == '') {
            var time = new Date();
            var curent = time.getHours() + ':' + time.getMinutes();
            $(this).val(curent);
        }
    });
</script>
