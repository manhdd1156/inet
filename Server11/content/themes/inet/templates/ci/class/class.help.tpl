{if $view == ""}
{elseif $view == "children"}
    {if $sub_view == "add"}
        <div class="col-md-9 col-sm-9">
            <div class="panel panel-default">
                <div class="panel-body color_red">
                    <strong>Mỗi trẻ có thể có nhiều phụ huynh (bố, mẹ, ông, bà...). Phụ huynh được gán vào cho trẻ bằng 02 cách:</strong>
                    <br/>- Cách 1: Điền thông tin vào ô phụ huynh để Tìm & Thêm từ những tài khoản người dùng đã có trong hệ thống.
                    <br/>- Cách 2: Tích chọn ô 'Tạo tài khoản phụ huynh' để hệ thống tự tạo và thông báo cho phụ huynh qua email (trường hợp này, phải nhập chính xác email phụ huynh).
                </div>
            </div>
        </div>
    {elseif $sub_view == "import"}
        <div class="col-md-9 col-sm-9">
            <div class="panel panel-default">
                <div class="panel-body color_red">
                    <strong>Bạn phải sử dụng biểu mẫu file Excel đúng quy định của hệ thống, biểu mẫu tải về tại <a target="_blank" href="https://drive.google.com/file/d/13r9KTR1wADeDJg4rL8vI5dd-YTxgjyGG/view?usp=sharing">ĐÂY</a>.</strong>
                    {*<strong>Bạn phải sử dụng biểu mẫu file Excel đúng quy định của hệ thống, biểu mẫu tải về tại <a target="_blank" href="https://drive.google.com/file/d/0B0zF9ER6zDq9ald3S2JUd2xyUTQ">ĐÂY</a>.</strong>*}
                    <br/>- Phải tạo lớp trước khi nhập thông tin trẻ vào lớp đó. {if canEdit($username, "classes")}<a href="{$system['system_url']}/school/{$username}/classes/add">Tạo lớp</a>{/if}
                </div>
            </div>
        </div>
    {/if}
    {if $sub_view == "addhealthindex"}
        <div class="col-md-9 col-sm-9">
            <div class="panel panel-default">
                <div class="panel-body color_red">
                    - Thêm chỉ số sức khỏe cho cả lớp. <br/>
                    - Kích chuột vào tên từng trẻ để thêm thông tin sức khỏe cho trẻ đó. <br/>
                    - Lưu ý: <strong>Hệ thống không ghi dữ liệu những trẻ không được thêm chiều cao hoặc cân nặng.</strong>
                </div>
            </div>
        </div>
    {/if}
{elseif $view == "healths"}
    {if $sub_view == "add"}
        <div class="col-md-9 col-sm-9">
            <div class="panel panel-default">
                <div class="panel-body color_red">
                    - Thêm chỉ số sức khỏe cho cả lớp. <br/>
                    - Kích chuột vào tên từng trẻ để thêm thông tin sức khỏe cho trẻ đó. <br/>
                    - Lưu ý: <strong>Hệ thống không ghi dữ liệu những trẻ không được thêm chiều cao hoặc cân nặng.</strong>
                </div>
            </div>
        </div>
    {/if}
{/if}
