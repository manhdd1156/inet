<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <i class="fa fa-envelope fa-fw fa-lg pr10"></i>
        {__("Parent feedback")}
    </div>
    {if $sub_view == ""}
        <div class="panel-body with-table">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover js_dataTable">
                    <thead>
                    <tr><th>{__("Feedback list")}&nbsp;({$rows|count})</th></tr>
                    </thead>
                    <tbody>
                    {$idx = 1}
                    {foreach $rows as $row}
                        <tr>
                            <td>
                                <div>
                                    <strong>{$idx}-</strong>&nbsp;
                                    {if $row['level']==$smarty.const.SCHOOL_LEVEL}
                                        {__("Góp ý gửi đến trường")}
                                    {elseif $row['level']==$smarty.const.CLASS_LEVEL}
                                        {__("Góp ý gửi đến lớp")}
                                    {/if}
                                    |&nbsp;{$row['created_at']}&nbsp;&nbsp;
                                    {if !$row['is_incognito']}
                                        <br/>
                                        <strong>{__("Thông tin phụ huynh")} </strong>:&nbsp;<br/>
                                        - {__("Tên người gửi")}: {$row['name']} &emsp;
                                        - {__("Phụ huynh trẻ")}: {$row['child_name']} <br/>
                                        - {__("Lớp")}: {$row['group_title']} <br/>
                                        {if !is_empty($row['phone'])}
                                            - {__("SĐT")}: {$row['phone']}  &emsp;
                                        {/if}
                                        {if !is_empty($row['email'])}
                                            - {__("Email")}: {$row['email']}
                                        {/if}
                                    {/if}

                                    <br/><br/>
                                    <strong>{__("Nội dung góp ý")} </strong>:&nbsp;{$row['content']}
                                    <br/><br/>
                                    {if !$row['confirm']}
                                        {if $row['level']==$smarty.const.SCHOOL_LEVEL}
                                            <button class="btn btn-xs btn-default js_school-feedback"
                                                    data-handle="confirm" data-username="{$username}"
                                                    data-id="{$row['feedback_id']}">{__("Confirm")}</button>
                                        {elseif $row['level']==$smarty.const.CLASS_LEVEL}
                                            <button class="btn btn-xs btn-default js_class-feedback"
                                                    data-handle="confirm" data-username="{$username}"
                                                    data-id="{$row['feedback_id']}">{__("Confirm")}</button>
                                        {/if}
                                    {else}
                                        <strong>{__("Đã thông báo nhận được góp ý đến phụ huynh.")}</strong>
                                    {/if}
                                </div>
                            </td>
                        </tr>
                        {$idx = $idx + 1}
                    {/foreach}
                    </tbody>
                </table>
            </div>
        </div>
    {/if}
</div>