<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <i class="fa fa-child fa-fw fa-lg pr10"></i>
        {__("Student")}
        {if $sub_view == "edit"}
            &rsaquo; {$child['child_name']}
        {elseif $sub_view == "detail"}
            &rsaquo; {$child['child_name']}
            <div class="pull-right flip">
{*                <a href="https://blog.coniu.vn/huong-dan-quan-ly-giao-vien-tao-cac-buoc-de-nhap-thong-tin-tre-tren-website-inet/" class="btn btn-info btn_guide" target="_blank">*}
                <a href="#" class="btn btn-info btn_guide" target="_blank">
                    <i class="fa fa-info-circle" aria-hidden="true"></i> {__("Guide")}
                </a>
                {if isset($data['status']) && $data['status'] == 0}
                    <button class="btn btn-default js_paid_child" data-username = "{$username}" data-id = {$child['child_id']}>
                        {__("Paid student")}
                    </button>
                {/if}
                <a href="{$system['system_url']}/class/{$username}/children/health/{$child['child_id']}" class="btn btn-default">
                    <i class="fa fa-heartbeat" aria-hidden="true"></i> {__("Health information")}
                </a>
                <a href="{$system['system_url']}/class/{$username}/children/journal/{$child['child_id']}" class="btn btn-default">
                    <i class="fa fa-image"></i> {__("List of diary")}
                </a>
            </div>
        {elseif $sub_view == ""}
            &rsaquo; {__("Lists")}
            <div class="pull-right flip">
{*                <a href="https://blog.coniu.vn/huong-dan-quan-ly-giao-vien-tao-cac-buoc-de-nhap-thong-tin-tre-tren-website-inet/" class="btn btn-info btn_guide" target="_blank">*}
                <a href="#" class="btn btn-info btn_guide" target="_blank">
                    <i class="fa fa-info-circle" aria-hidden="true"></i> {__("Guide")}
                </a>
                <a href="{$system['system_url']}/class/{$username}/children/addhealthindex" class="btn btn-default">
                    <i class="fa fa-plus"></i> {__("Add health index")}
                </a>
                {if canview_class($user_id,$class_id)}
                <a href="{$system['system_url']}/class/{$username}/children/add" class="btn btn-default">
                    <i class="fa fa-plus"></i> {__("Add New")}
                </a>
                <a href="{$system['system_url']}/class/{$username}/children/addexisting" class="btn btn-default">
                    <i class="fa fa-plus"></i> {__("Add existing child")}
                </a>
                {/if}
                <a href="{$system['system_url']}/class/{$username}/children/adddiary" class="btn btn-default">
                    <i class="fa fa-image"></i> {__("Add diary")}
                </a>
            </div>
        {elseif $sub_view == "add"}
            &rsaquo; {__("Add New")}
            <div class="pull-right flip">
{*                <a href="https://blog.coniu.vn/huong-dan-quan-ly-giao-vien-tao-cac-buoc-de-nhap-thong-tin-tre-tren-website-inet/" class="btn btn-info btn_guide" target="_blank">*}
                <a href="#" class="btn btn-info btn_guide" target="_blank">
                    <i class="fa fa-info-circle" aria-hidden="true"></i> {__("Guide")}
                </a>
                <a href="{$system['system_url']}/class/{$username}/children" class="btn btn-default">
                    {__("Lists")}
                </a>
            </div>
        {elseif $sub_view == "import"}
            &rsaquo; {__("Import from Excel file")}
            <div class="pull-right flip">
{*                <a href="https://blog.coniu.vn/huong-dan-quan-ly-giao-vien-tao-cac-buoc-de-nhap-thong-tin-tre-tren-website-inet/" class="btn btn-info btn_guide" target="_blank">*}
                <a href="#" class="btn btn-info btn_guide" target="_blank">
                    <i class="fa fa-info-circle" aria-hidden="true"></i> {__("Guide")}
                </a>
                <a href="{$system['system_url']}/class/{$username}/children" class="btn btn-default">
                    {__("Lists")}
                </a>
            </div>
        {elseif $sub_view == "health"}
            &rsaquo; {$child['child_name']}
            <div class="pull-right flip">
{*                <a href="https://blog.coniu.vn/huong-dan-giao-vien-them-chi-suc-khoe-tren-website-inet/" class="btn btn-info btn_guide" target="_blank">*}
                <a href="#" class="btn btn-info btn_guide" target="_blank">
                    <i class="fa fa-info-circle" aria-hidden="true"></i> {__("Guide")}
                </a>
                <a href="{$system['system_url']}/class/{$username}/children/addgrowth/{$child['child_id']}" class="btn btn-default">
                    <i class="fa fa-plus" aria-hidden="true"></i> {__("Add New")}
                </a>
                <a href="{$system['system_url']}/class/{$username}/children/detail/{$child['child_id']}" class="btn btn-default">
                    <i class="fa fa-info" aria-hidden="true"></i> {__("Student information detail")}
                </a>
            </div>
        {elseif $sub_view == "addgrowth"}
            &rsaquo; {$child['child_name']}
            <div class="pull-right flip">
{*                <a href="https://blog.coniu.vn/huong-dan-giao-vien-them-chi-suc-khoe-tren-website-inet/" class="btn btn-info btn_guide" target="_blank">*}
                <a href="#" class="btn btn-info btn_guide" target="_blank">
                    <i class="fa fa-info-circle" aria-hidden="true"></i> {__("Guide")}
                </a>
                <a href="{$system['system_url']}/class/{$username}/children/detail/{$child['child_id']}" class="btn btn-default">
                    <i class="fa fa-info" aria-hidden="true"></i> {__("Student information detail")}
                </a>
                <a href="{$system['system_url']}/class/{$username}/children/health/{$child['child_id']}" class="btn btn-default">
                    <i class="fa fa-heartbeat" aria-hidden="true"></i> {__("Health information")}
                </a>
            </div>
        {elseif $sub_view == "editgrowth"}
            &rsaquo; {$child['child_name']}
            <div class="pull-right flip">
{*                <a href="https://blog.coniu.vn/huong-dan-giao-vien-them-chi-suc-khoe-tren-website-inet/" class="btn btn-info btn_guide" target="_blank">*}
                <a href="#" class="btn btn-info btn_guide" target="_blank">
                    <i class="fa fa-info-circle" aria-hidden="true"></i> {__("Guide")}
                </a>
                <a href="{$system['system_url']}/class/{$username}/children/detail/{$child['child_id']}" class="btn btn-default">
                    <i class="fa fa-info" aria-hidden="true"></i> {__("Student information detail")}
                </a>
                <a href="{$system['system_url']}/class/{$username}/children/health/{$child['child_id']}" class="btn btn-default">
                    <i class="fa fa-heartbeat" aria-hidden="true"></i> {__("Health information")}
                </a>
            </div>
        {elseif $sub_view == "addhealthindex"}
            &rsaquo; {__("Add health index")}
            <div class="pull-right flip">
{*                <a href="https://blog.coniu.vn/huong-dan-giao-vien-them-chi-suc-khoe-tren-website-inet/" class="btn btn-info btn_guide" target="_blank">*}
                <a href="#" class="btn btn-info btn_guide" target="_blank">
                    <i class="fa fa-info-circle" aria-hidden="true"></i> {__("Guide")}
                </a>
                <a href="{$system['system_url']}/class/{$username}/children" class="btn btn-default">
                    <i class="fa fa-list" aria-hidden="true"></i> {__("Lists")}
                </a>
            </div>
        {elseif $sub_view == "addexisting"}
            &rsaquo; {__('Add Existing Account')}
            <div class="pull-right flip">
{*                <a href="https://blog.coniu.vn/huong-dan-quan-ly-tao-cac-buoc-de-nhap-thong-tin-tre-tren-website-inet/" class="btn btn-info btn_guide" target="_blank">*}
                <a href="#" class="btn btn-info btn_guide" target="_blank">
                    <i class="fa fa-info-circle" aria-hidden="true"></i> {__("Guide")}
                </a>
                <a href="{$system['system_url']}/class/{$username}/children" class="btn btn-default">
                    {__("Lists")}
                </a>
            </div>
        {elseif $sub_view == "journal"}
            &rsaquo; {__('List of diary')} &rsaquo; {$child['child_name']}
            <div class="pull-right flip">
                <a href="{$system['system_url']}/class/{$username}/children/detail/{$child['child_id']}" class="btn btn-default">
                    <i class="fa fa-info" aria-hidden="true"></i> {__("Student information detail")}
                </a>
                {if $child['status'] == 1}
                    <a href="{$system['system_url']}/class/{$username}/children/addphoto/{$child['child_id']}" class="btn btn-default">
                        <i class="fa fa-image"></i> {__("Add diary")}
                    </a>
                {/if}
            </div>
        {elseif $sub_view == "addphoto"}
            &rsaquo; {__("Add diary")}
            <div class="pull-right flip">
                <a href="{$system['system_url']}/class/{$username}/children/detail/{$child['child_id']}" class="btn btn-default">
                    <i class="fa fa-info" aria-hidden="true"></i> {__("Student information detail")}
                </a>
                {if $child['status'] == 1}
                    <a href="{$system['system_url']}/class/{$username}/children/journal/{$child['child_id']}" class="btn btn-default">
                        <i class="fa fa-image"></i> {__("List of diary")}
                    </a>
                {/if}
            </div>
        {elseif $sub_view == "adddiary"}
            &rsaquo; {__("Add diary")}
            <div class="pull-right flip">
                <a href="{$system['system_url']}/class/{$username}/children" class="btn btn-default">
                    <i class="fa fa-list" aria-hidden="true"></i> {__("Lists")}
                </a>
            </div>
        {/if}
    </div>
    {if $sub_view == ""}
        <div class="panel-body with-table">
            <div class="table-responsive">
                <div><strong>{__("Student list")}&nbsp;({$rows|count})</strong></div>
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>{__("#")}</th>
                        <th>{__("Full name")}</th>
                        <th>{__("Gender")}</th>
                        <th>{__("Birthdate")}</th>
                        <th>{__("Parent phone")}</th>
                        <th>{__("Parent")}</th>
                        <th>{__("Actions")}</th>
                    </tr>
                    </thead>
                    <tbody>
                        {$idx = 1}
                        {foreach $rows as $row}
                            <tr>
                                <td align="center" class="align-middle"> {$idx} </td>
                                <td class="align-middle"><a href="{$system['system_url']}/class/{$username}/children/detail/{$row['child_id']}">{$row['child_name']}</a></td>
                                <td align="center" class="align-middle">{if $row['gender'] == $smarty.const.MALE}{__("Male")}{else}{__("Female")}{/if}</td>
                                <td align="center" class="align-middle">{$row['birthday']}</td>
                                <td align="center" class="align-middle">
                                    {if $row['parent_phone'] != ''}
                                        {$row['parent_phone']}
                                    {else}
                                        {$row['parent_phone_dad']}
                                    {/if}
                                </td>
                                <td>
                                    {if count($row['parent']) == 0}
                                        {__("No parent")}
                                    {else}
                                        {foreach $row['parent'] as $_user}
                                            <span class="name js_user-popover" data-uid="{$_user['user_id']}">
                                                <a href="{$system['system_url']}/{$_user['user_name']}">{$_user['user_fullname']}</a>
                                            </span>
                                            {if $_user['user_id'] != $user->_data['user_id']}
                                                <a href="#" class="far fa-comments fa-lg js_chat-start" data-name="{$_user['user_fullname']}" data-uid="{$_user['user_id']}"></a>
                                            {/if}
                                            {if $_user['suggest'] == 1}
                                                <button class="btn btn-xs btn-danger js_class-approve-parent" id="button_{$row['child_id']}_{$_user['user_id']}" data-parent="{$_user['user_id']}" data-username="{$username}" data-id="{$row['child_id']}">{__("Approve")}</button>
                                            {/if}
                                            <br/>
                                        {/foreach}
                                    {/if}
                                </td>
                                <td align = "center" class="align-middle">
                                    <a href="{$system['system_url']}/class/{$username}/children/edit/{$row['child_id']}" class="btn btn-xs btn-default">{__("Edit")}</a>
                                </td>
                            </tr>
                            {$idx = $idx + 1}
                        {/foreach}

                        {if $rows|count == 0}
                            <tr class="odd">
                                <td valign="top" align="center" colspan="7" class="dataTables_empty">
                                    {__("No data available in table")}
                                </td>
                            </tr>
                        {/if}
                    </tbody>
                </table>
            </div>
        </div>
    {elseif $sub_view == "edit"}
        <div class="panel-body">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/class/boclass_child.php">
                <input type="hidden" name="username" value="{$username}"/>
                <input type="hidden" name="child_id" value="{$child['child_id']}"/>
                <input type="hidden" name="child_parent_id" value="{$child['child_parent_id']}"/>
                <input type="hidden" name="child_code" value="{$child['child_code']}"/>
                <input type="hidden" name="do" value="edit"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Full name")} (*)</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="last_name" value="{$child['last_name']}" placeholder="{__("Last name")}" required maxlength="34">
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="first_name" value="{$child['first_name']}" placeholder="{__("First name")}" required maxlength="15">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Gender")} (*)</label>
                    <div class="col-sm-3">
                        <select name="gender" id="gender" class="form-control">
                            <option value="{$smarty.const.MALE}" {if $child['gender'] == $smarty.const.MALE}selected{/if}>{__("Male")}</option>
                            <option value="{$smarty.const.FEMALE}" {if $child['gender'] == $smarty.const.FEMALE}selected{/if}>{__("Female")}</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Birthdate")} (*)</label>
                    <div class='col-sm-4'>
                        <div class='input-group date' id='birthdate_picker'>
                            <input type='text' name="birthday" value="{$child['birthday']}" class="form-control" placeholder="{__("Birthdate")} (*)" required/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Parent")}</label>
                    <div class="col-sm-9">
                        <input name="search-parent" id="search-parent" type="text" class="form-control" placeholder="{__("Enter username, email or fullname to search")}" autocomplete="off">
                        <div id="search-parent-results" class="dropdown-menu dropdown-widget dropdown-search">
                            <div class="dropdown-widget-header">
                                {__("Search Results")}
                            </div>
                            <div class="dropdown-widget-body">
                                <div class="loader loader_small ptb10"></div>
                            </div>
                        </div>
                        <br/>
                        <div class="col-sm-9" id="parent_list" name="parent_list">
                            {if count($parent) > 0}
                                {include file='ci/ajax.parentlist.tpl' results=$parent}
                            {else}
                                {__("No parent")}
                            {/if}
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Mother's name")}</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="parent_name" name="parent_name" placeholder="{__("Mother's name")}" maxlength="50" value="{convertText4Web($child['parent_name'])}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Telephone")}</label>
                    <div class="col-xs-6">
                        <input type="text" class="form-control" id="parent_phone" name="parent_phone" value="{$child['parent_phone']}" placeholder="{__("Telephone")}" maxlength="50">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Job")}</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="parent_job" name="parent_job" value="{$child['parent_job']}" placeholder="{__("Job")}" maxlength="50">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Father's name")}</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="parent_name_dad" name="parent_name_dad" placeholder="{__("Father's name")}" maxlength="50" value="{convertText4Web($child['parent_name_dad'])}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Telephone")}</label>
                    <div class="col-xs-6">
                        <input type="text" class="form-control" id="parent_phone_dad" name="parent_phone_dad" value="{$child['parent_phone_dad']}" placeholder="{__("Telephone")}" maxlength="50">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Job")}</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="parent_job_dad" name="parent_job_dad" value="{$child['parent_job_dad']}" placeholder="{__("Job")}" maxlength="50">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Parent email")}</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="parent_email" name="parent_email" value="{$child['parent_email']}" placeholder="{__("Parent email")}" maxlength="50">
                        <div>{__("If parent do not have any email, please let it empty")}.</div>
                    </div>
                    <div class="col-sm-4">
                        <input type="checkbox" value="1" name="create_parent_account" id="create_parent_account" {if count($parent) > 0}disabled{else}checked{/if}>&nbsp;{__("Auto-create parent account")}?
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Address")}</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="address" value="{$child['address']}" placeholder="{__("Address")}" maxlength="250">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Study start date")} (*)</label>
                    <div class='col-sm-4'>
                        <div class='input-group date' id='beginat_datepicker'>
                            <input type='text' name="begin_at" value="{$child['begin_at']}" class="form-control" placeholder="{__("Study start date")}"/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Description")}</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="description" placeholder="{__("Write about your child...")}" maxlength="300">{$child['description']}</textarea>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    {elseif $sub_view == "add"}
        <div class="panel-body">
            <form class="js_ajax-add-child-form form-horizontal" data-url="ci/bo/class/boclass_child.php">
                <input type="hidden" name="username" id="username" value="{$username}"/>
                <input type="hidden" name="do" value="add"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Student code")}</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="child_code" id="child_code" placeholder="{__("Student code")}" disabled autofocus maxlength="30">
                    </div>
                    <div class="col-sm-4">
                        <input type="checkbox" name="code_auto" id="code_auto" value="1" checked/> {__("Generate automatically")}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Full name")} (*)</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="last_name" id="last_name" placeholder="{__("Last name")}" required maxlength="34" autofocus>
                    </div>
                    <div class="col-sm-2">
                        <input type="text" class="form-control" name="first_name" id="first_name" placeholder="{__("First name")}" required maxlength="15">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Gender")} (*)</label>
                    <div class="col-sm-3">
                        <select name="gender" id="gender" class="form-control">
                            <option value="{$smarty.const.MALE}">{__("Male")}</option>
                            <option value="{$smarty.const.FEMALE}">{__("Female")}</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Birthdate")} (*)</label>
                    <div class='col-sm-3'>
                        <div class='input-group date' id='birthdate_picker'>
                            <input type='text' name="birthday" id="birthday" class="form-control" placeholder="{__("Birthdate")} (*)" required/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Parent")}</label>
                    <div class="col-sm-7">
                        <input name="search-parent" id="search-parent" type="text" class="form-control" placeholder="{__("Enter username, email or fullname to search")}" autocomplete="off">
                        <div id="search-parent-results" class="dropdown-menu dropdown-widget dropdown-search">
                            <div class="dropdown-widget-header">
                                {__("Search Results")}
                            </div>
                            <div class="dropdown-widget-body">
                                <div class="loader loader_small ptb10"></div>
                            </div>
                        </div>
                        <div>{__("Enter at least 4 characters")}.</div>
                        <br/>
                        <div class="col-sm-9" id="parent_list" name="parent_list"></div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Mother's name")}</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="parent_name" name="parent_name" placeholder="{__("Mother's name")}" maxlength="50">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Telephone")}</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="parent_phone" id="parent_phone" placeholder="{__("Telephone")}" maxlength="50">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Job")}</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="parent_job" id="parent_job" placeholder="{__("Job")}" maxlength="500">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Father's name")}</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="parent_name_dad" name="parent_name_dad" placeholder="{__("Father's name")}" maxlength="50">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Telephone")}</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="parent_phone_dad" id="parent_phone_dad" placeholder="{__("Telephone")}" maxlength="50">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Job")}</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="parent_job_dad" id="parent_job_dad" placeholder="{__("Job")}" maxlength="500">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Parent email")}</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="parent_email" name="parent_email" placeholder="{__("Parent email")}" maxlength="100">
                        <div>{__("If parent do not have any email, please let it empty")}.</div>
                    </div>
                    <div class="col-sm-4">
                        <input type="checkbox" value="1" name="create_parent_account" id="create_parent_account" checked>&nbsp;{__("Auto-create parent account")}?
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Address")}</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="address" id="address" placeholder="{__("Address")}" maxlength="250">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Class")} (*)</label>
                    <div class="col-sm-6">
                        <select name="class_id" id="class_id" class="form-control">
                            <option value="{$class['group_id']}">{$class['group_title']}</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Study start date")} (*)</label>
                    <div class='col-sm-3'>
                        <div class='input-group date' id='beginat_datepicker'>
                            <input type='text' name="begin_at" class="form-control" placeholder="{__("Study start date")}"/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Description")}</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="description" id="description" placeholder="{__("Write about your child...")}" maxlength="300"></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-3 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                    </div>
                    <div class="col-sm-4">
                        <a href="#" class="btn btn-default js_add_child_clear">{__("Clear Data")}</a>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    {elseif $sub_view == "import"}
        <div class="panel-body">
            <form class="form-horizontal" action="#" enctype="multipart/form-data" method="post" id="import_excel_form">
                <input type="hidden" name="username" value="{$username}"/>
                <input type="hidden" name="do" value="import"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Select Class")} (*)</label>
                    <div class="col-sm-5">
                        <select name="class_id" id="class_id" class="form-control" required>
                            <option value="{$class['group_id']}">{$class['group_title']}</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Select Excel file")}</label>
                    <div class="col-sm-5">
                        <input type="file" name="file" id="file"/>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-3 col-sm-offset-3">
                        <button type="submit" id="submit_id" class="btn btn-primary padrl30">{__("Save")}</button>
                    </div>
                </div>
                <br/>
                <div class="table-responsive" id="result_info" name="result_info"></div>
                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    {elseif $sub_view == "detail"}
        <div class="panel-body with-table">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <tbody>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Full name")}</strong></td>
                            <td>{$child['child_name']}</td>
                        </tr>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Student code")}</strong></td>
                            <td>{$child['child_code']}</td>
                        </tr>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Nickname")}</strong></td>
                            <td>{$child['child_nickname']}</td>
                        </tr>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Gender")}</strong></td>
                            <td>{if $child['gender'] == $smarty.const.MALE}{__("Male")}{else}{__("Female")}{/if}</td>
                        </tr>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Birthdate")}</strong></td>
                            <td>{$child['birthday']}</td>
                        </tr>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Parent")}</strong></td>
                            <td>
                                {foreach $parent as $_user}
                                    <span class="name js_user-popover" data-uid="{$_user['user_id']}">
                                        <a href="{$system['system_url']}/{$_user['user_name']}">{$_user['user_fullname']}</a>
                                    </span>
                                    {if $_user['user_id'] != $user->_data['user_id']}
                                        <a href="#" class="far fa-comments fa-lg js_chat-start" data-name="{$_user['user_fullname']}" data-uid="{$_user['user_id']}"></a>
                                    {/if}
                                    <br/>
                                {/foreach}
                            </td>
                        </tr>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Mother's name")}</strong></td>
                            <td>{$child['parent_name']}</td>
                        </tr>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Telephone")}</strong></td>
                            <td>{$child['parent_phone']}</td>
                        </tr>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Job")}</strong></td>
                            <td>{$child['parent_job']}</td>
                        </tr>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Father's name")}</strong></td>
                            <td>{$child['parent_name_dad']}</td>
                        </tr>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Telephone")}</strong></td>
                            <td>{$child['parent_phone_dad']}</td>
                        </tr>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Job")}</strong></td>
                            <td>{$child['parent_job_dad']}</td>
                        </tr>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Parent email")}</strong></td>
                            <td>{$child['parent_email']}</td>
                        </tr>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Address")}</strong></td>
                            <td>{$child['address']}</td>
                        </tr>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Study start date")}</strong></td>
                            <td>{$child['begin_at']}</td>
                        </tr>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Description")}</strong></td>
                            <td>{$child['description']}</td>
                        </tr>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Pickup information")}</strong></td>
                            <td>
                                <table class = "table table-bordered">
                                    <tbody>
                                        {foreach $data as $row}
                                            <tr>
                                                <td style="width: 60%">
                                                    {if !is_empty($row['picker_source_file'])}
                                                        <a href="{$row['picker_source_file']}" target="_blank"><img src = "{$row['picker_source_file']}" style="width: 100%" class = "img-responsive"></a>
                                                    {else}
                                                        {__("No information")}
                                                    {/if}
                                                </td>
                                                <td style="width: 40%">
                                                    <strong>{__("Picker name")}:</strong> {$row['picker_name']} <br/>
                                                    <strong>{__("Relation with student")}:</strong> {$row['picker_relation']} <br/>
                                                    <strong>{__("Telephone")}:</strong> {$row['picker_phone']} <br/>
                                                    <strong>{__("Address")}:</strong> {$row['picker_address']} <br/>
                                                    <strong>{__("Creator")}:</strong> {$row['user_fullname']}
                                                </td>
                                            </tr>
                                        {/foreach}
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    {elseif $sub_view == "listchildedit"}
        <div class="panel-body with-table">
            <div class="table-responsive" id="child_list" name="child_list">
                {include file="ci/class/ajax.childeditlist.tpl"}
            </div>
            <br>
            <!-- success -->
            <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
            <!-- success -->

            <!-- error -->
            <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
            <!-- error -->
        </div>
    {elseif $sub_view == "reedit"}
        <div class="panel-body">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/class/boclass_child.php">
                <input type="hidden" name="username" value="{$username}"/>
                <input type="hidden" name="child_id" value="{$child_edit['child_id']}"/>
                <input type="hidden" name="child_code" value="{$child_edit['child_code']}"/>
                <input type="hidden" name="do" value="edit"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Student code")}</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="child_code" value="{$child_edit['child_code']}" disabled>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Full name")} (*)</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="last_name" value="{$child_edit['last_name']}" placeholder="{__("Last name")}" required maxlength="34" autofocus>
                    </div>
                    <div class="col-sm-2">
                        <input type="text" class="form-control" name="first_name" value="{$child_edit['first_name']}" placeholder="{__("First name")}" required maxlength="15">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Gender")} (*)</label>
                    <div class="col-sm-3">
                        <select name="gender" id="gender" class="form-control">
                            <option value="{$smarty.const.MALE}" {if $child_edit['gender'] == $smarty.const.MALE}selected{/if}>{__("Male")}</option>
                            <option value="{$smarty.const.FEMALE}" {if $child_edit['gender'] == $smarty.const.FEMALE}selected{/if}>{__("Female")}</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Birthdate")} (*)</label>
                    <div class='col-sm-3'>
                        <div class='input-group date' id='birthdate_picker'>
                            <input type='text' name="birthday" value="{$child_edit['birthday']}" class="form-control" placeholder="{__("Birthdate")} (*)" required/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Parent")}</label>
                    <div class="col-sm-7">
                        <input name="search-parent" id="search-parent" type="text" class="form-control" placeholder="{__("Enter username, email or fullname to search")}" autocomplete="off">
                        <div id="search-parent-results" class="dropdown-menu dropdown-widget dropdown-search">
                            <div class="dropdown-widget-header">
                                {__("Search Results")}
                            </div>
                            <div class="dropdown-widget-body">
                                <div class="loader loader_small ptb10"></div>
                            </div>
                        </div>
                        <div>{__("Enter at least 4 characters")}.</div>
                        <br/>
                        <div class="col-sm-9" id="parent_list" name="parent_list">
                            {if count($child_edit['parent']) > 0}
                                {include file='ci/ajax.parentlist.tpl' results=$child_edit['parent']}
                            {else}
                                {__("No parent")}
                            {/if}
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Parent name")}</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="parent_name" name="parent_name" placeholder="{__("Parent name")}" {if count($child_edit['parent']) > 0}disabled {else} value = "{$child_edit['parent_name']}"{/if} maxlength="50">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Parent phone")}</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="parent_phone" name="parent_phone" value="{$child_edit['parent_phone']}" placeholder="{__("Parent phone")}" maxlength="50">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Parent email")}</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="parent_email" name="parent_email" value="{$child_edit['parent_email']}" placeholder="{__("Parent email")}" maxlength="100">
                    </div>
                    <div class="col-sm-4">
                        <input type="checkbox" value="{$child_edit['create_parent_account']}" name="create_parent_account" id="create_parent_account" {if count($child_edit['parent']) > 0}disabled{else}checked{/if}>&nbsp;{__("Auto-create parent account")}?
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Address")}</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="address" value="{$child_edit['address']}" placeholder="{__("Address")}" maxlength="250">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Class")}</label>
                    <div class="col-sm-9">
                        {if $child_edit['class_id'] == '0'}
                            <input type="text" class="form-control" name="class_name" value="{__("No class ")}" disabled>
                        {else}
                            {foreach $classes as $class}
                                {if $child_edit['class_id'] == $class['group_id']}
                                    <input type="text" class="form-control" name="class_name" value="{$class['group_title']}" disabled>
                                    {break}
                                {/if}
                            {/foreach}
                        {/if}
                        <input type="hidden" name="class_id" value="{$child_edit['class_id']}"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Study start date")} (*)</label>
                    <div class='col-sm-3'>
                        <div class='input-group date' id='beginat_datepicker'>
                            <input type='text' name="begin_at" value="{$child_edit['begin_at']}" class="form-control" placeholder="{__("Study start date")}" required/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Description")}</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="description" placeholder="{__("Write about your child...")}" maxlength="300">{$child_edit['description']}</textarea>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-default">{__("Edit")}</button>
                        <a href="{$system['system_url']}/class/{$username}/children/listchildedit" class="btn btn-default">{__("Waiting list student confirm")}</a>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->
                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    {elseif $sub_view == "health"}
        <div class = "panel-body">
            {*Ô search*}
            <div class="js_ajax-forms form-horizontal">
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Time")}</label>
                    <div class='col-sm-3'>
                        <div class='input-group date' id='begin_chart_picker'>
                            <input type='text' name="begin" class="form-control" id="begin"/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                    <div class='col-sm-1'><i class="fas fa-long-arrow-alt-right fa-lg fa-fw pr10 pt10"></i></div>
                    <div class='col-md-3'>
                        <div class='input-group date' id='end_chart_picker'>
                            <input type='text' name="end" class="form-control" id="end"/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-default js_class-chart-search" data-child="{$child['child_id']}" data-username="{$username}">{__("Search")}</button>
                    </div>
                </div>

                <div id="chart_list" name="chart_list">
                    {include file="ci/class/ajax.class.chartsearch.tpl"}
                </div>
                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
            </div>
        </div>
    {elseif $sub_view == "addgrowth"}
        <div class="panel-body form-horizontal">
            <form class="form-horizontal" enctype="multipart/form-data" method="post" id="class_edit_child_health">
                <input type="hidden" name="username" value="{$username}"/>
                <input type="hidden" name="child_id" value="{$child['child_id']}"/>
                <input type="hidden" name="child_parent_id" value="{$child['child_parent_id']}"/>
                <input type="hidden" name="do" value="add_growth"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Date")} (*)</label>
                    <div class='col-sm-4'>
                        <div class='input-group date' id='birthdate_picker'>
                            <input type='text' name="recorded_at" class="form-control"/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Height")} (*)</label>
                    <div class="col-sm-4">
                        <input type="number" min="0" class="form-control" name="height" step="any" required>
                    </div>
                    <label class="col-sm-1 control-label text-left" style = "text-align: left">{__("cm")}</label>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Weight")} (*)</label>
                    <div class="col-sm-4">
                        <input type="number" min="0" class="form-control" name="weight" step="any" required>
                    </div>
                    <label class="col-sm-1 control-label text-left" style = "text-align: left">{__("kg")}</label>
                </div>
                <div class = "form-group">
                    <label class="col-sm-3 control-label">{__("Nutritional status")}</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="nutriture_status">
                    </div>
                </div>
                <div class = "form-group">
                    <label class="col-sm-3 control-label">{__("Heartbeat")}</label>
                    <div class="col-sm-5">
                        <input type="number" class="form-control" name="heart" min="0" step="1">
                    </div>
                    <label class="col-sm-2 control-label">{__("Times/minute")}</label>
                </div>
                <div class = "form-group">
                    <label class="col-sm-3 control-label">{__("Blood pressure")}</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="blood_pressure">
                    </div>
                </div>
                <div class = "form-group">
                    <label class="col-sm-3 control-label">{__("Ear")}</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="ear">
                    </div>
                </div>
                <div class = "form-group">
                    <label class="col-sm-3 control-label">{__("Eye")}</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="eye">
                    </div>
                </div>
                <div class = "form-group">
                    <label class="col-sm-3 control-label">{__("Nose")}</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="nose">
                    </div>
                </div>
                <div class = "form-group">
                    <label class="col-sm-3 control-label">{__("Description")}</label>
                    <div class="col-sm-9">
                        <textarea type="text" class="form-control" name="description"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("File attachment")}</label>
                    <div class="col-sm-6">
                        <input type="file" name="file" id="file"/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    {elseif $sub_view == "editgrowth"}
        <div class = "panel-body">
            <form class="form-horizontal" enctype="multipart/form-data" method="post" id="class_edit_child_health">
                <input type="hidden" name="username" value="{$username}"/>
                <input type="hidden" name="child_growth_id" value="{$data['child_growth_id']}"/>
                <input type="hidden" name="child_id" value="{$child['child_id']}"/>
                <input type="hidden" name="is_module" value="{$is_module}"/>
                <input type="hidden" name="do" value="edit_growth"/>
                <div class="form-group">
                    <label class = "col-sm-3 control-label text-left">{__("Date")} (*)</label>
                    <div class='col-sm-3'>
                        <div class='input-group date' id='birthdate_picker'>
                            <input type='text' name="recorded_at" value = "{$data['recorded_at']}" id="recorded_at" class="form-control"/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                </div>
                <div class = "form-group">
                    <label class="col-sm-3 control-label text-left">{__("Height")}</label>
                    <div class="col-sm-3">
                        <input type="number" min="0" step = "any" class="form-control" name="height" value="{$data['height']}" placeholder="{__("Height")}">
                    </div>
                    <label class="col-sm-2 control-label text-left" style = "text-align: left">{__("cm")}</label>
                </div>
                <div class = "form-group">
                    <label class="col-sm-3 control-label">{__("Weight")}</label>
                    <div class="col-sm-3">
                        <input type="number" min="0" step = "any" class="form-control" name="weight" value="{$data['weight']}" placeholder="{__("Weight")}">
                    </div>
                    <label class="col-sm-2 control-label text-left" style = "text-align: left">{__("kg")}</label>
                </div>
                <div class = "form-group">
                    <label class="col-sm-3 control-label">{__("Nutritional status")}</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="nutriture_status" value="{$data['nutriture_status']}">
                    </div>
                </div>
                <div class = "form-group">
                    <label class="col-sm-3 control-label">{__("Heartbeat")}</label>
                    <div class="col-sm-5">
                        <input type="number" class="form-control" name="heart" min="0" step="1" value="{$data['heart']}">
                    </div>
                    <label class="col-sm-2 control-label">{__("Times/minute")}</label>
                </div>
                <div class = "form-group">
                    <label class="col-sm-3 control-label">{__("Blood pressure")}</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="blood_pressure" value="{$data['blood_pressure']}">
                    </div>
                </div>
                <div class = "form-group">
                    <label class="col-sm-3 control-label">{__("Ear")}</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="ear" value="{$data['ear']}">
                    </div>
                </div>
                <div class = "form-group">
                    <label class="col-sm-3 control-label">{__("Eye")}</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="eye" value="{$data['eye']}">
                    </div>
                </div>
                <div class = "form-group">
                    <label class="col-sm-3 control-label">{__("Nose")}</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="nose" value="{$data['nose']}">
                    </div>
                </div>
                <div class = "form-group">
                    <label class="col-sm-3 control-label">{__("Description")}</label>
                    <div class="col-sm-9">
                        <textarea type="text" class="form-control" name="description">{$data['description']}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("File attachment")}</label>
                    <div class="col-sm-6">
                        {if !is_empty($data['source_file'])}
                            <a href="{$data['source_file']}" target="_blank"><img src = "{$data['source_file']}" class = "img-responsive"></a>
                            <br>
                            <label class="control-label">{__("Choose file replace")}</label>
                            <br>
                        {/if}
                        <input type="file" name="file" id="file"/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->
                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    {elseif $sub_view == "addhealthindex"}
        <div class = "panel-body with-table">
            <form class="form-horizontal" enctype="multipart/form-data" method="post" id="class_add_health_index">
                <input type="hidden" name="username" value="{$username}"/>
                <input type="hidden" name="do" value="add_health_index"/>
                <div class="form-group">
                    <label class = "col-sm-4 control-label text-left">{__("Measure time")} (*)</label>
                    <div class='col-sm-3'>
                        <div class='input-group date' id='birthdate_picker'>
                            <input type='text' name="recorded_at" id="recorded_at" class="form-control"/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-4 health_left">
                        <div class="table-responsive">
                            <div class="mb10 ml10"><strong>{__("Student list")}&nbsp;({$children|count})</strong></div>
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>{__("#")}</th>
                                    <th>{__("Child")}</th>
                                </tr>
                                </thead>
                                <tbody>
                                {$idx = 1}
                                {foreach $children as $child}
                                    <tr>
                                        <td align="center"><strong>{$idx}</strong></td>
                                        <td>
                                            <a href="#" class="js_class-child-health" id="child_{$child['child_id']}" data-id="{$child['child_id']}">{$child['child_name']}</a>
                                        </td>
                                    </tr>
                                    {$idx = $idx + 1}
                                {/foreach}
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-sm-8 health_right">
                        {foreach $children as $child}
                            <div class="hidden hideAll" id="health_{$child['child_id']}">
                                <input type="hidden" name="childIds[]" value="{$child['child_id']}">
                                <div class="mb10"><strong>{__("Add health index")} {__("for")}:&nbsp;{$child['child_name']}</strong></div>
                                <div class="" style="border: 1px solid #b7b7b7; padding: 10px 10px">
                                    <div class = "form-group">
                                        <label class="col-sm-3 control-label text-left">{__("Height")}</label>
                                        <div class="col-sm-5">
                                            <input type="number" min="0" step = "any" class="form-control" name="height_{$child['child_id']}" placeholder="{__("Height")}">
                                        </div>
                                        <label class="col-sm-2 control-label text-left" style = "text-align: left">{__("cm")}</label>
                                    </div>
                                    <div class = "form-group">
                                        <label class="col-sm-3 control-label">{__("Weight")}</label>
                                        <div class="col-sm-5">
                                            <input type="number" min="0" step = "any" class="form-control" name="weight_{$child['child_id']}" placeholder="{__("Weight")}">
                                        </div>
                                        <label class="col-sm-2 control-label text-left" style = "text-align: left">{__("kg")}</label>
                                    </div>
                                    <div class = "form-group">
                                        <label class="col-sm-3 control-label">{__("Nutritional status")}</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="nutriture_status_{$child['child_id']}">
                                        </div>
                                    </div>
                                    <div class = "form-group">
                                        <label class="col-sm-3 control-label">{__("Heartbeat")}</label>
                                        <div class="col-sm-5">
                                            <input type="number" class="form-control" name="heart_{$child['child_id']}" min="0" step="1">
                                        </div>
                                        <label class="col-sm-2 control-label">{__("Times/minute")}</label>
                                    </div>
                                    <div class = "form-group">
                                        <label class="col-sm-3 control-label">{__("Blood pressure")}</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="blood_pressure_{$child['child_id']}">
                                        </div>
                                    </div>
                                    <div class = "form-group">
                                        <label class="col-sm-3 control-label">{__("Ear")}</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="ear_{$child['child_id']}">
                                        </div>
                                    </div>
                                    <div class = "form-group">
                                        <label class="col-sm-3 control-label">{__("Eye")}</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="eye_{$child['child_id']}">
                                        </div>
                                    </div>
                                    <div class = "form-group">
                                        <label class="col-sm-3 control-label">{__("Nose")}</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="nose_{$child['child_id']}">
                                        </div>
                                    </div>
                                    <div class = "form-group">
                                        <label class="col-sm-3 control-label">{__("Description")}</label>
                                        <div class="col-sm-9">
                                            <textarea type="text" class="form-control" name="description_{$child['child_id']}"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label text-left">{__("File attachment")}</label>
                                        <div class="col-sm-6">
                                            {if !is_empty($data['source_file'])}
                                                <a href="{$data['source_file']}" target="_blank"><img src = "{$data['source_file']}" class = "img-responsive"></a>
                                                <br>
                                                <label class="control-label">{__("Choose file replace")}</label>
                                                <br>
                                            {/if}
                                            <input type="file" name="file_{$child['child_id']}" id="file"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        {/foreach}
                    </div>
                </div>
                <div class="form-group hidden submit_hidden" id="">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->
                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    {elseif $sub_view == "addexisting"}
        <div class="panel-body">
            <div class="panel-body with-table">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-right pt5">{__("Student code")} (*)</label>
                        <div class='col-sm-3'>
                            <input type='text' name="childcode" id="childcode" class="form-control" min="1" placeholder="{__("Student code")}"/>
                        </div>
                        <div class='col-sm-2'>
                            <div class="form-group">
                                <a href="#" id="search" class="btn btn-default js_child-addexisting" data-username="{$username}" data-school_username="{$school['page_name']}" data-id="{$school['page_id']}" data-class_id="{$class['group_id']}">{__("Search")}</a>
                                <label id="loading" class="btn btn-info x-hidden">{__("Loading")}...</label>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div class="" id="table_child_addexisting">
                </div>
            </div>
        </div>
    {elseif $sub_view=="addphoto"}
        <div class="panel-body">
            <div class="mb10" align="center">
                <strong>{__("Add photo to diary")} {__("for")} {$child['child_name']}</strong>
            </div>
            <form class="form-horizontal" enctype="multipart/form-data" method="post" id="add_child_journal_class">
                <input type="hidden" name="username" value="{$username}"/>
                <input type="hidden" name="child_id" value="{$child['child_id']}"/>
                <input type="hidden" name="child_parent_id" value="{$child['child_parent_id']}"/>
                <input type="hidden" name="do" value="add_photo"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Caption")}</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="caption" placeholder="{__("Caption")}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Picture")}</label>
                    <div class="col-sm-6">
                        <input name="file[]" type="file" multiple="true">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                    </div>
                </div>
            </form>
        </div>
    {elseif $sub_view=="journal"}
        <div class="panel-body with-table form-horizontal">
            <div class="form-group">
                <label class="col-sm-3 control-label text-left">{__("Year")}</label>
                <div class="col-sm-3">
                    <select name="year" id="year" class="form-control">
                        <option value = "0">{__("Select year ...")}</option>
                        {for $i = $year_begin; $i < $year_end; $i++}
                            <option value="{$i}">{$i}</option>
                        {/for}
                    </select>
                </div>
                <div class="col-sm-3">
                    <a class="btn btn-default js_class-journal-search" data-username="{$username}" data-id="{$child['child_id']}" data-handle = "search">{__("Search")}</a>
                </div>
            </div>
            <div id = "journal_list">
                {include file="ci/ajax.class.journal.list.tpl"}
            </div>
        </div>
    {elseif $sub_view=="adddiary"}
        <div class="panel-body">
            <form class="form-horizontal" enctype="multipart/form-data" method="post" id="add_child_journal_class">
                <input type="hidden" name="username" value="{$username}"/>
                <input type="hidden" name="reload" value="1"/>
                <input type="hidden" name="do" value="add_photo"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Select student")} (*)</label>
                    <div class="col-sm-5">
                        <select name="child_id" class="form-control" autofocus>
                            {$idx = 1}
                            {foreach $children as $child}
                                <option value="{$child['child_id']}">{$idx} - {$child['child_name']} - {$child['birthday']}</option>
                                {$idx = $idx + 1}
                            {/foreach}
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Caption")}</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="caption" placeholder="{__("Caption")}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Picture")}</label>
                    <div class="col-sm-6">
                        <input name="file[]" type="file" multiple="true">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                    </div>
                </div>
            </form>
        </div>
    {/if}
</div>
