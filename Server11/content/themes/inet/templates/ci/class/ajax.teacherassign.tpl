<tr>
    <td><center><label>Thứ 2 - {$day['mon']}</label></center></td>
    <td><center>
            <div id="teacher_mon" class="col-sm-9">
                {if isset($data[2]) }
                    {foreach $data[2] as $dt }
                        <div id="assign_teacher_mon_{$dt['user_id']}">
                            <label class="col-xs-12 col-sm-6 text-left">&#9679;&#09;{$dt['user_fullname']}
                                <input type="text" class="x-hidden" name="assign_2[]" value="{$dt['user_id']}">
                            </label>
                        </div>
                    {/foreach}
                {/if}
            </div>
        </center>
    </td>
</tr>
<tr>
    <td><center><label>Thứ 3 - {$day['tue']}</label></center></td>
    <td><center>
            <div id="teacher_tue" class="col-sm-9">
                {if isset($data[3]) }
                    {foreach $data[3] as $dt }
                        <div id="assign_teacher_tue_{$dt['user_id']}">
                            <label class="col-xs-12 col-sm-6 text-left">&#9679;&#09;{$dt['user_fullname']}
                                <input type="text" class="x-hidden" name="assign_3[]" value="{$dt['user_id']}">
                            </label>
                        </div>
                    {/foreach}
                {/if}
            </div>
        </center>
    </td>
</tr>
<tr>
    <td><center><label>Thứ 4 - {$day['wed']}</label></center></td>
    <td><center>
            <div id="teacher_wed" class="col-sm-9">
                {if isset($data[4]) }
                    {foreach $data[4] as $dt }
                        <div id="assign_teacher_wed_{$dt['user_id']}">
                            <label class="col-xs-12 col-sm-6 text-left">&#9679;&#09;{$dt['user_fullname']}
                                <input type="text" class="x-hidden" name="assign_4[]" value="{$dt['user_id']}">
                            </label>
                        </div>
                    {/foreach}
                {/if}
            </div>
        </center>
    </td>
</tr>
<tr>
    <td><center><label>Thứ 5 - {$day['thu']}</label></center></td>
    <td><center>
            <div id="teacher_thu" class="col-sm-9">
                {if isset($data[5]) }
                    {foreach $data[5] as $dt }
                        <div id="assign_teacher_thu_{$dt['user_id']}">
                            <label class="col-xs-12 col-sm-6 text-left">&#9679;&#09;{$dt['user_fullname']}
                                <input type="text" class="x-hidden" name="assign_5[]" value="{$dt['user_id']}">
                            </label>
                        </div>
                    {/foreach}
                {/if}
            </div>
        </center>
    </td>
</tr>
<tr>
    <td><center><label>Thứ 6 - {$day['fri']}</label></center></td>
    <td><center>
            <div id="teacher_fri" class="col-sm-9">
                {if isset($data[6]) }
                    {foreach $data[6] as $dt }
                        <div id="assign_teacher_fri_{$dt['user_id']}">
                            <label class="col-xs-12 col-sm-6 text-left">&#9679;&#09;{$dt['user_fullname']}
                                <input type="text" class="x-hidden" name="assign_6[]" value="{$dt['user_id']}">
                            </label>
                        </div>
                    {/foreach}
                {/if}
            </div>
        </center>
    </td>
</tr>
<tr>
    <td><center><label>Thứ 7 - {$day['sat']}</label></center></td>
    <td><center>
            <div id="teacher_sat" class="col-sm-9">
                {if isset($data[7]) }
                    {foreach $data[7] as $dt }
                        <div id="assign_teacher_sat_{$dt['user_id']}">
                            <label class="col-xs-12 col-sm-6 text-left">&#9679;&#09;{$dt['user_fullname']}
                                <input type="text" class="x-hidden" name="assign_7[]" value="{$dt['user_id']}">
                            </label>
                        </div>
                    {/foreach}
                {/if}
            </div>
        </center>
    </td>
</tr>
<tr>
    <td><center><label>Chủ nhật - {$day['sun']}</label></center></td>
    <td><center>
            <div id="teacher_sun" class="col-sm-9">
                {if isset($data[8]) }
                    {foreach $data[8] as $dt }
                        <div id="assign_teacher_sun_{$dt['user_id']}">
                            <label class="col-xs-12 col-sm-6 text-left">&#9679;&#09;{$dt['user_fullname']}
                                <input type="text" class="x-hidden" name="assign_8[]" value="{$dt['user_id']}">
                            </label>
                        </div>
                    {/foreach}
                {/if}
            </div>
        </center>
    </td>
</tr>