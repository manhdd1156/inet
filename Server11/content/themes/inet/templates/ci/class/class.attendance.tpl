<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <div class="pull-right flip">
{*            <a href="https://blog.coniu.vn/huong-dan-giao-vien-diem-danh-tren-website-inet/" class="btn btn-info btn_guide" target="_blank">*}
            <a href="#" class="btn btn-info btn_guide" target="_blank">
                <i class="fa fa-info-circle" aria-hidden="true"></i> {__("Guide")}
            </a>
            {if $sub_view == "" && canView_class($user_id,$class_id)}
                <a href="{$system['system_url']}/class/{$username}/attendance/rollup" class="btn btn-default">
                    <i class="fa fa-plus"></i> {__("Roll up")}
                </a>
            {/if}
        </div>
        <i class="fa fa-tasks fa-fw fa-lg pr10"></i>
        {__("Attendance")}
        {if $sub_view == "search"}
            &rsaquo; {__("Search")}
        {elseif $sub_view == "rollup"}
            &rsaquo; {__("Roll up")}
        {elseif $sub_view == "child"}
            &rsaquo; {$data['child']['child_name']}
        {/if}
    </div>
    {if $sub_view == ""}
        <div class="panel-body with-table">
            <div class="row">
                <div class="form-group">
                    <label class="col-sm-2 control-label text-right pt10">{__("Time")}</label>
                    <div class="col-sm-10">
                        <div class='col-sm-4'>
                            <div class='input-group date' id='fromdate_picker'>
                                <input type='text' name="fromDate" id="fromDate" value="{$fromDate}" class="form-control" placeholder="{__("From date")}"/>
                                <span class="input-group-addon">
                                    <span class="fas fa-calendar-alt"></span>
                                </span>
                            </div>
                        </div>

                        <div class='col-md-4'>
                            <div class='input-group date' id='todate_picker'>
                                <input type='text' name="toDate" id="toDate" value="{$toDate}" class="form-control" placeholder="{__("To date")}"/>
                                <span class="input-group-addon">
                                    <span class="fas fa-calendar-alt"></span>
                                </span>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <a href="#" id="search" class="btn btn-default js_class-attendancesearch" data-username="{$username}">{__("Search")}</a>
                            <label id="loading" class="btn btn-info x-hidden">{__("Loading")}...</label>
                        </div>
                    </div>
                </div>
            </div><br/>
            <div class="table-responsive" id="attendance_list">
                {include file="ci/class/ajax.class.attendancesearch.tpl"}
            </div>
        </div>
    {elseif $sub_view == "child"}
        <div class="panel-body with-table">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/class/boclass_attendance.php">
                <input type="hidden" name="username" id="username" value="{$username}"/>
                <input type="hidden" name="do" value="update_child"/>
                <div class="form-group">
                    <label class="col-sm-2 control-label text-right pt10">{__("Time")}</label>
                    <div class="col-sm-10">
                        <div class='col-sm-4'>
                            <div class='input-group date' id='fromdate_picker'>
                                <input type='text' name="fromDate" id="fromDate" value="{$data['fromDate']}" class="form-control" placeholder="{__("From date")}"/>
                                <span class="input-group-addon">
                                    <span class="fas fa-calendar-alt"></span>
                                </span>
                            </div>
                        </div>

                        <div class='col-md-4'>
                            <div class='input-group date' id='todate_picker'>
                                <input type='text' name="toDate" id="toDate" value="{$data['toDate']}" class="form-control" placeholder="{__("To date")}"/>
                                <span class="input-group-addon">
                                    <span class="fas fa-calendar-alt"></span>
                                </span>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <a class="btn btn-default js_class-attendancechild" data-username="{$username}" data-id="{$data['child']['child_id']}">{__("Search")}</a>
                            <label class="btn btn-info processing_label x-hidden">{__("Processing")}...</label>
                        </div>
                    </div>
                </div>
                <br/>
                <div class="table-responsive" id="attendance_list">
                    {include file="ci/class/ajax.class.attendance.child.tpl"}
                </div>
                <div class="form-group">
                    <div class="col-sm-9 col-md-offset-1">
                        <button type="submit" class="btn btn-primary padrl30" {if (count($data['attendance']['attendance']) == 0 || !canView_class($user_id,$class_id))}disabled{/if}>{__("Save")}</button>
                    </div>
                </div>
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
            </form>
        </div>
    {elseif $sub_view == "rollup"}
        <div class="panel-body">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/class/boclass_attendance.php">
                <input type="hidden" name="username" id="username" value="{$username}"/>
                <input type="hidden" name="do" value="rollup" id="attendance_do"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Date")}</label>
                    <div class='col-sm-4'>
                        <div class='input-group date' id='attendance_picker'>
                            <input type='text' name="attendance_date" id="attendance_date" value="{$data['attendance_date']}" class="form-control" placeholder="{__("Date")} (*)" required/>
                                <span class="input-group-addon">
                                    <span class="fas fa-calendar-alt"></span>
                                </span>
                        </div>
                    </div>
                </div>
                <div id="attendance_list">
                    {include file="ci/class/ajax.class.attendancelist.tpl"}
                </div>
                <div class="form-group">
                    <div class="col-sm-9">
                        <button type="submit" class="btn btn-primary padrl30" {if count($data['detail']) == 0}disabled{/if}>{__("Save")}</button>
                    </div>
                </div>
                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    {/if}
</div>