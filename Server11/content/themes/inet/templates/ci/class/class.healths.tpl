<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <div class="pull-right flip">
            {if $sub_view == ""}
                <a href="{$system['system_url']}/class/{$username}/healths/add" class="btn btn-default">
                    <i class="fa fa-plus"></i> {__("Add New")}
                </a>
            {elseif $sub_view == "add"}
                <a href="{$system['system_url']}/class/{$username}/healths" class="btn btn-default">
                    <i class="fa fa-list"></i> {__("Lists")}
                </a>
            {/if}
        </div>
        <div class="pull-right flip" style="margin-right: 5px">
            {*<a href="https://blog.coniu.vn/huong-dan-quan-ly-diem-danh-tren-website-inet/" class="btn btn-info btn_guide" target="_blank">*}
            {*<i class="fa fa-info-circle"></i> {__("Guide")}*}
            {*</a>*}
        </div>
        <i class="fa fa-heart fa-fw fa-lg pr10"></i>
        {__("Health information")}
        {if $sub_view == ""}
            &rsaquo; {__("Lists")}
        {elseif $sub_view == "add"}
            &rsaquo; {__("Add New")}
        {/if}
    </div>

    {if $sub_view == ""}
        <div class = "panel-body">
            {*Ô search*}
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/class/boclass_child.php">
                <input type="hidden" name="username" value="{$username}"/>
                <input type="hidden" name="do" value="search_health_child"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-right">{__("Select student")} (*)</label>
                    <div class="col-sm-4">
                        <select name="child_id" id="attendance_child_id" class="form-control" required>
                            <option value="">{__("Select student")}...</option>
                            {if isset($children) && !empty($children)}
                                {foreach $children as $child}
                                    <option value="{$child['child_id']}" {if ($child['child_id'] == $childId)}selected{/if}>{$child['child_name']}</option>
                                {/foreach}
                            {/if}
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label text-left">{__("Time")}</label>
                    <div class='col-sm-3'>
                        <div class='input-group date' id='begin_chart_picker'>
                            <input type='text' name="begin" class="form-control" id="begin" {if isset($begin) && $begin != ''} value="{$begin}"{/if}/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                    <div class='col-sm-1'><i class="fas fa-long-arrow-alt-right fa-lg fa-fw pr10 pt10"></i></div>
                    <div class='col-md-3'>
                        <div class='input-group date' id='end_chart_picker'>
                            <input type='text' name="end" class="form-control" id="end" {if isset($end) && $end != ''} value="{$end}"{/if}/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <a href="#" id="search" class="btn btn-default js_class-health-search" data-username="{$username}" data-isnew="1">{__("Search")}</a>
                        <label id="loading" class="btn btn-info x-hidden">{__("Loading")}...</label>
                    </div>
                </div>
                <div id="chart_list" name="chart_list">
                    {include file="ci/class/ajax.class.chartsearch.tpl"}
                </div>
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
            </form>
        </div>
    {elseif $sub_view == "add"}
        <div class = "panel-body with-table">
            <form class="form-horizontal" enctype="multipart/form-data" method="post" id="class_add_health_index">
                <input type="hidden" name="username" value="{$username}"/>
                <input type="hidden" name="do" value="add_health_index"/>
                <div class="form-group">
                    <label class = "col-sm-4 control-label text-left">{__("Measure time")} (*)</label>
                    <div class='col-sm-3'>
                        <div class='input-group date' id='birthdate_picker'>
                            <input type='text' name="recorded_at" id="recorded_at" class="form-control"/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-4 health_left">
                        <div class="table-responsive">
                            <div class="mb10 ml10"><strong>{__("Student list")}&nbsp;({$children|count})</strong></div>
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>{__("#")}</th>
                                    <th>{__("Child")}</th>
                                </tr>
                                </thead>
                                <tbody>
                                {$idx = 1}
                                {foreach $children as $child}
                                    <tr>
                                        <td align="center"><strong>{$idx}</strong></td>
                                        <td>
                                            <a href="#" class="js_class-child-health" id="child_{$child['child_id']}" data-id="{$child['child_id']}">{$child['child_name']}</a>
                                        </td>
                                    </tr>
                                    {$idx = $idx + 1}
                                {/foreach}
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-sm-8 health_right">
                        {foreach $children as $child}
                            <div class="hidden hideAll" id="health_{$child['child_id']}">
                                <input type="hidden" name="childIds[]" value="{$child['child_id']}">
                                <div class="mb10"><strong>{__("Add health index")} {__("for")}:&nbsp;{$child['child_name']}</strong></div>
                                <div class="" style="border: 1px solid #b7b7b7; padding: 10px 10px">
                                    <div class = "form-group">
                                        <label class="col-sm-3 control-label text-left">{__("Height")}</label>
                                        <div class="col-sm-5">
                                            <input type="number" min="0" step = "any" class="form-control" name="height_{$child['child_id']}" placeholder="{__("Height")}">
                                        </div>
                                        <label class="col-sm-2 control-label text-left" style = "text-align: left">{__("cm")}</label>
                                    </div>
                                    <div class = "form-group">
                                        <label class="col-sm-3 control-label">{__("Weight")}</label>
                                        <div class="col-sm-5">
                                            <input type="number" min="0" step = "any" class="form-control" name="weight_{$child['child_id']}" placeholder="{__("Weight")}">
                                        </div>
                                        <label class="col-sm-2 control-label text-left" style = "text-align: left">{__("kg")}</label>
                                    </div>
                                    <div class = "form-group">
                                        <label class="col-sm-3 control-label">{__("Nutritional status")}</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="nutriture_status_{$child['child_id']}">
                                        </div>
                                    </div>
                                    <div class = "form-group">
                                        <label class="col-sm-3 control-label">{__("Heartbeat")}</label>
                                        <div class="col-sm-5">
                                            <input type="number" class="form-control" name="heart_{$child['child_id']}" min="0" step="1">
                                        </div>
                                        <label class="col-sm-2 control-label">{__("Times/minute")}</label>
                                    </div>
                                    <div class = "form-group">
                                        <label class="col-sm-3 control-label">{__("Blood pressure")}</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="blood_pressure_{$child['child_id']}">
                                        </div>
                                    </div>
                                    <div class = "form-group">
                                        <label class="col-sm-3 control-label">{__("Ear")}</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="ear_{$child['child_id']}">
                                        </div>
                                    </div>
                                    <div class = "form-group">
                                        <label class="col-sm-3 control-label">{__("Eye")}</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="eye_{$child['child_id']}">
                                        </div>
                                    </div>
                                    <div class = "form-group">
                                        <label class="col-sm-3 control-label">{__("Nose")}</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="nose_{$child['child_id']}">
                                        </div>
                                    </div>
                                    <div class = "form-group">
                                        <label class="col-sm-3 control-label">{__("Description")}</label>
                                        <div class="col-sm-9">
                                            <textarea type="text" class="form-control" name="description_{$child['child_id']}"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label text-left">{__("File attachment")}</label>
                                        <div class="col-sm-6">
                                            {if !is_empty($data['source_file'])}
                                                <a href="{$data['source_file']}" target="_blank"><img src = "{$data['source_file']}" class = "img-responsive"></a>
                                                <br>
                                                <label class="control-label">{__("Choose file replace")}</label>
                                                <br>
                                            {/if}
                                            <input type="file" name="file_{$child['child_id']}" id="file"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        {/foreach}
                        <br/>
                        <div class="form-group hidden submit_hidden" id="">
                            <div class="col-sm-9">
                                <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group hidden submit_hidden" id="">
                    <div class="col-sm-9">
                        <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->
                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    {/if}
</div>