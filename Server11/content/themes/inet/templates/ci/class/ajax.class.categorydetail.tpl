<div class="form-group">
    {$results['category_name']}
</div>
<div class = "table-responsive">
    <table class="table table-striped table-bordered table-hover js_dataTable">
        <thead>
        <tr>
            <th>{__("#")}</th>
            <th>{__("Category suggest")}</th>
        </tr>
        </thead>
        <tbody>
        {$idx = 1}
        {foreach $results['suggests'] as $row}
            <tr>
                <td class="align-middle">
                    <center>{$idx}</center>
                </td>
                <td class="align-middle">
                    {$row}
                </td>
            </tr>
            {$idx = $idx + 1}
        {/foreach}
        </tbody>
    </table>
</div>