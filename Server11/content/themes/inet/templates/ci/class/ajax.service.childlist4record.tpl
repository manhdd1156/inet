<div><strong>{__("Student list")}&nbsp;({__("Children")}: {$results['children']|count}&nbsp;|&nbsp;{__("Usage")}: {$results['usage_count']})</strong></div>
<table class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th>#</th>
            <th>{__("Select")}</th>
            <th>{__("Full name")}</th>
            <th>{__("Student code")}</th>
            <th>{__("Registration time")}</th>
            <th>{__("Registrar")}</th>
        </tr>
    </thead>
    <tbody>
    {$idx = 1}
        {foreach $results['children'] as $row}
            <tr {if ($row['child_status'] == 0) || in_array($row['child_id'], $absent_child_ids)}class="row-disable"{/if}>
                <td align="center">{$idx}</td>
                <td align="center">
                    <input type="checkbox" class="child" name="childIds[]" value="{$row['child_id']}" {if $row['recorded_user_id'] > 0}checked{/if} {if (in_array($row['child_id'], $absent_child_ids))}disabled{/if}>
                    <input type="hidden" name="allChildIds[]" value="{$row['child_id']}"/>
                    {if $row['recorded_user_id'] > 0}
                        <input type="hidden" name="oldChildIds[]" value="{$row['child_id']}"/>
                    {/if}
                </td>
                <td>{$row['child_name']}</td>
                <td>{$row['child_code']}</td>
                <td>
                    {if $row['recorded_user_id'] > 0}
                        {$row['recorded_at']}
                    {/if}
                </td>
                <td>
                    {if $row['recorded_user_id'] > 0}
                        {$row['user_fullname']}
                        {if $row['recorded_user_id'] != $user->_data['user_id']}
                            &nbsp;<a href="#" class="far fa-comments fa-lg js_chat-start" data-name="{$row['user_fullname']}" data-uid="{$row['recorded_user_id']}"></a>
                        {/if}
                    {/if}
                </td>
            </tr>
            {$idx = $idx + 1}
        {/foreach}
    </tbody>
</table>