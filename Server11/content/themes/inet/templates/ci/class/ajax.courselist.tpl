<table class="table table-striped table-bordered table-hover js_dataTable">
    <thead>
    <tr><th colspan="7">{__("course list")}&nbsp;(<span class="count_conduct">{$result|count}</span>) </th></tr>
    <tr>
        <th>{__("#")}</th>
        <th>{__("Course name")}</th>
        <th>{__("Short detail")}</th>
        <th>{__("Created at")}</th>
{*        <th>{__("Number of video")} 1</th>*}
        <th>{__("Price")}</th>
        <th>{__("Number of view")}</th>
        <th>{__("Actions")}</th>

    </tr>
    </thead>
    <tbody>
        {$idx = 1}
        {foreach $result as $row}
            <tr>
                <td class="align-middle" align="center">
                    <strong>{$idx}</strong>
                </td>
                <td class="align-middle" style="word-break: break-all"> {$row['title']}</td>
                <td class="align-middle"> {$row['short_detail']} </td>
                <td align="center" class="align-middle">{date_format($row['created_at'], 'H:i:s d-m-Y')}</td>
                <td align="center" class="align-middle">{$row['price']}</td>
                <td align="center" class="align-middle">{$row['view']}</td>
                <td align = "center" class="align-middle">
                    <a href="{$row['detailUrl']}" target="_blank" class="btn btn-xs btn-default">{__("Detail")}</a>
                </td>
            </tr>
            {$idx = $idx + 1}
        {/foreach}
    </tbody>
</table>

<script>
    $(function() {
        // run DataTable
        $('.js_dataTable').DataTable({
            "aoColumnDefs": [ { 'aDataSort': false, 'aTargets': [ -1 ] } ],
            "language": {
                "decimal":        "",
                "emptyTable":     __["No data available in table"],
                "info":           __["Showing _START_ to _END_ of _TOTAL_ results"],
                "infoEmpty":      __["Showing 0 to 0 of 0 results"],
                "infoFiltered":   "(filtered from _MAX_ total entries)",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     __["Show"] + " " + "_MENU_" + " " + __["results"],
                "loadingRecords": __["Loading..."],
                "processing":     __["Processing..."],
                "search":         __["Search"],
                "zeroRecords":    __["No matching records found"],
                "paginate": {
                    "first":      __["First"],
                    "last":       __["Last"],
                    "next":       __["Next"],
                    "previous":   __["Previous"]
                },
                "aria": {
                    "sortAscending":  ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                }
            }
        });
    });
</script>
