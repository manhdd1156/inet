<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <i class="fa fa-gavel fa-fw fa-lg pr10"></i>
        {__("Conduct")}
        {if $sub_view == "edit"}
            &rsaquo; {$child['child_name']}
            <div class="pull-right flip">
                <a href="{$system['system_url']}/class/{$username}/conducts" class="btn btn-default">
                    <i class="fa fa-list-ul"></i>{__("Lists")}
                </a>
            </div>
        {elseif $sub_view == ""}
            &rsaquo; {__("Lists")}
        {/if}
    </div>
    {if $sub_view == ""}
        <div class="panel-body with-table">
            <div class="table-responsive">
                <div><strong>{__("Student list")}&nbsp;({$rows|count}) </strong></div>
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>{__("#")}</th>
                        <th>{__("Full name")}</th>
                        <th>{__("Gender")}</th>
                        <th>{__("Birthdate")}</th>
                        <th>{__("Semester")} 1</th>
                        <th>{__("Semester")} 2</th>
                        <th>{__("End semester")}</th>
                        <th>{__("Actions")}</th>
                    </tr>
                    </thead>
                    <tbody>
                        {$idx = 1}
                        {foreach $rows as $row}
                            <tr>
                                <td align="center" class="align-middle"> {$idx} </td>
                                <td class="align-middle"><a href="{$system['system_url']}/class/{$username}/children/detail/{$row['child_id']}">{$row['child_name']}</a></td>
                                <td align="center" class="align-middle">{if $row['gender'] == $smarty.const.MALE}{__("Male")}{else}{__("Female")}{/if}</td>
                                <td align="center" class="align-middle">{$row['birthday']}</td>
                                <td align="center" class="align-middle">{$row['conduct1']}</td>
                                <td align="center" class="align-middle">{$row['conduct2']}</td>
                                <td align="center" class="align-middle">{$row['conduct3']}</td>

                                <td align = "center" class="align-middle">
                                    <a href="{$system['system_url']}/class/{$username}/conducts/edit/{$row['conduct_id']}" class="btn btn-xs btn-default">{__("Edit")}</a>
                                </td>
                            </tr>
                            {$idx = $idx + 1}
                        {/foreach}

                        {if $rows|count == 0}
                            <tr class="odd">
                                <td valign="top" align="center" colspan="7" class="dataTables_empty">
                                    {__("No data available in table")}
                                </td>
                            </tr>
                        {/if}
                    </tbody>
                </table>
            </div>
        </div>
    {elseif $sub_view == "edit"}
        <div class="panel-body">
            <form class="form-horizontal" enctype="multipart/form-data" method="post" id="class_edit_conduct">
                <input type="hidden" name="username" id="username" value="{$username}"/>
                <input type="hidden" name="conduct_id" value="{$data['conduct_id']}"/>
                <input type="hidden" name="child_id" value="{$child['child_id']}"/>
                <input type="hidden" name="do" value="edit"/>
                <div class = "form-group">
                    <label class="col-sm-3 control-label text-left">
                        {__("Student")}
                    </label>
                    <div class = "col-sm-9">
                        <input type="text" class="form-control" value = "{$child['child_name']} - {$child['birthday']}" disabled>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Semester")} 1
                    </label>
                    <div class="col-sm-9">
                        <select name="conduct_id1" id="conduct_id1" data-username="{$username}" data-view="edit" class="form-control">
                            <option value="">{__("Select conduct")}</option>
                            {foreach $conducts as $conduct}
                                <option value="{$conduct}" {if $data['conduct1'] == $conduct}selected{/if}>{$conduct}</option>
                            {/foreach}
{*                                <option value="Excellent">{__("Excellent")}</option>*}
{*                                <option value="Good">{__("Good")}</option>*}
{*                                <option value="Average">{__("Average")}</option>*}
{*                                <option value="Weak">{__("Weak")}</option>*}
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Semester")} 2
                    </label>
                    <div class="col-sm-9">
                        <select name="conduct_id2" id="conduct_id2" data-username="{$username}" data-view="edit" class="form-control">
                            <option value="">{__("Select conduct")}</option>
                            {foreach $conducts as $conduct}
                                <option value="{$conduct}" {if $data['conduct2'] == $conduct}selected{/if}>{$conduct}</option>
                            {/foreach}
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("End semester")}
                    </label>
                    <div class="col-sm-9">
                        <select name="conduct_id3" id="conduct_id3" data-username="{$username}" data-view="edit" class="form-control">
                            <option value="">{__("Select conduct")}</option>
                            {foreach $conducts as $conduct}
                                <option value="{$conduct}" {if $data['conduct3'] == $conduct}selected{/if}>{$conduct}</option>
                            {/foreach}
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    {/if}
</div>
