<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <div class="pull-right flip">
{*            <a href="https://blog.coniu.vn/huong-dan-giao-vien-dang-ky-dich-vu-tren-website-inet/" class="btn btn-info btn_guide" target="_blank">*}
            <a href="#" class="btn btn-info btn_guide" target="_blank">
                <i class="fa fa-info"></i> {__("Guide")}
            </a>
            {if $sub_view == "history" && canview_class($user_id,$class_id)}
                <a href="{$system['system_url']}/class/{$username}/useservices/record" class="btn btn-default">
                    <i class="fa fa-plus"></i> {__("Use service")}
                </a>
            {/if}
        </div>
        <i class="fa fa-life-ring fa-fw fa-lg pr10"></i>
        {__("Service")}
        {if $sub_view == "history"}
            &rsaquo; {__('Service usage information')}
        {elseif $sub_view == "record"}
            &rsaquo; {__('Register service')}
        {/if}
    </div>
    {if $sub_view == "record"}
        <div class="panel-body with-table">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/class/boclass_service.php">
                <input type="hidden" name="username" value="{$username}"/>
                <input type="hidden" name="do" value="record"/>

                <div class="form-group pl5">
                    <div class='col-sm-4'>
                        <select name="service_id" id="service_id" class="form-control">
                            {$idx = 1}
                            {foreach $services as $service}
                                <option value="{$service['service_id']}">{$idx} - {$service['service_name']}</option>
                                {$idx = $idx + 1}
                            {/foreach}
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <div class='input-group date' id='using_time_picker'>
                            <input type='text' name="using_at" id="using_at" class="form-control" placeholder="{__("Using time")} (*)" required/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                    <div class='col-sm-2 pl10'>
                        <div class="form-group">
                            <a id="search" class="btn btn-default js_service-search-child4record" data-username="{$username}">{__("Search")}</a>
                            <label id="loading" class="btn btn-info x-hidden">{__("Loading")}...</label>
                        </div>
                    </div>
                </div>
                <div class="table-responsive" id="child_list" name="child_list"></div>
                <div class="form-group pl5">
                    <div class="col-sm-9">
                        <button type="submit" class="btn btn-primary padrl30" disabled>{__("Save")}</button>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    {elseif $sub_view == "history"}
        <div class="panel-body with-table">
            <form class="js_ajax-forms form-horizontal" data-url="#">
                <div class="form-group pl5">
                    <div class='col-sm-4'>
                        <select name="service_id" id="service_id" class="form-control" {if $service_id > 0}disabled{/if}>
                            <option value="">{__("Select service")}...</option>
                            {$idx = 1}
                            {foreach $services as $service}
                                <option value="{$service['service_id']}" {if $service_id == $service['service_id']}selected{/if}>{$idx} - {$service['service_name']}</option>
                                {$idx = $idx + 1}
                            {/foreach}
                        </select>
                    </div>
                    <div class='col-sm-5'>
                        <select name="child_id" id="service_child_id" class="form-control" {if $service_id > 0}disabled{/if}>
                            <option value="">{__("Whole class")}</option>
                            {$idx = 1}
                            {$child_status = -1}
                            {foreach $children as $child}
                                {if ($child_status >= 0) && ($child_status != $child['child_status'])}
                                    <option value="" disabled style="color: blue">----------{__("Trẻ đã nghỉ học")}----------</option>
                                {/if}
                                <option value="{$child['child_id']}" {if $child['child_id'] == $child_id}selected{/if}>{$idx} - {$child['child_name']} ({$child['birthday']})</option>
                                {$idx = $idx + 1}
                                {$child_status = $child['child_status']}
                            {/foreach}
                        </select>
                    </div>
                </div>
                <div class="form-group pl5">
                    <div class="col-sm-3">
                        <div class='input-group date' id='history_begin_picker'>
                            <input type='text' name="begin" id="begin" value="{$begin}" class="form-control" placeholder="{__("From date")}"/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class='input-group date' id='history_end_picker'>
                            <input type='text' name="end" id="end" class="form-control" placeholder="{__("To date")}"/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                    <div class='col-sm-2'>
                        <div class="form-group pl10">
                            <a href="#" id="search" class="btn btn-default js_service-search-history" data-username="{$username}">{__("Search")}</a>
                            <label id="loading" class="btn btn-info x-hidden">{__("Loading")}...</label>
                        </div>
                    </div>
                </div>
                <br/>
                <div class="table-responsive" id="usage_history" name="usage_history">
                    {if $service_id > 0}
                        {include file="ci/school/ajax.service.history.tpl"}
                    {/if}
                </div>
            </form>
        </div>
    {/if}
</div>