<form class="js_ajax-add-child-form form-horizontal" data-url="ci/bo/class/boclass_child.php">
    <input type="hidden" name="username" id="username" value="{$username}"/>
    <input type="hidden" name="child_code" value="{$child['child_code']}">
    <input type="hidden" name="child_parent_id" value="{$child['child_parent_id']}">
    <input type="hidden" name="do" value="addexisting"/>

    <div class="form-group">
        <label class="col-sm-3 control-label text-left">{__("Full name")} (*)</label>
        <div class="col-sm-3">
            <input type="text" class="form-control" name="last_name" value="{$child['last_name']}" placeholder="{__("Last name")}" required maxlength="34" autofocus>
        </div>
        <div class="col-sm-2">
            <input type="text" class="form-control" name="first_name" value="{$child['first_name']}" placeholder="{__("First name")}" required maxlength="15">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label text-left">{__("Gender")} (*)</label>
        <div class="col-sm-3">
            <select name="gender" id="gender" class="form-control">
                <option value="{$smarty.const.MALE}" {if $child['gender'] == $smarty.const.MALE}selected{/if}>{__("Male")}</option>
                <option value="{$smarty.const.FEMALE}" {if $child['gender'] == $smarty.const.FEMALE}selected{/if}>{__("Female")}</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label text-left">{__("Birthdate")} (*)</label>
        <div class='col-sm-3'>
            <div class='input-group date' id='birthdate_dp'>
                <input id="birthday" type='text' name="birthday" value="{$child['birthday']}" class="form-control" placeholder="{__("Birthdate")} (*)" required/>
                <label class="input-group-addon btn" for="date">
                    <span class="fas fa-calendar-alt"></span>
                </label>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label text-left">{__("Parent")}</label>
        <div class="col-sm-7">
            <input name="search-parent" id="search-parent" type="text" class="form-control" placeholder="{__("Enter username, email or fullname to search")}" autocomplete="off">
            <div id="search-parent-results" class="dropdown-menu dropdown-widget dropdown-search">
                <div class="dropdown-widget-header">
                    {__("Search Results")}
                </div>
                <div class="dropdown-widget-body">
                    <div class="loader loader_small ptb10"></div>
                </div>
            </div>
            <div>{__("Enter at least 4 characters")}.</div>
            <br/>
            <div class="col-sm-9" id="parent_list" name="parent_list">
                {if count($parent) > 0}
                    {include file='ci/ajax.parentlist.tpl' results=$parent}
                {else}
                    {__("No parent")}
                {/if}
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label text-left">{__("Mother's name")}</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" id="parent_name" name="parent_name" placeholder="{__("Mother's name")}" maxlength="50">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label text-left">{__("Telephone")}</label>
        <div class="col-sm-3">
            <input type="text" class="form-control" id="parent_phone" name="parent_phone" value="{$child['parent_phone']}" placeholder="{__("Telephone")}" maxlength="50">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label text-left">{__("Job")}</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" id="parent_job" name="parent_job" value="{$child['parent_job']}" placeholder="{__("Job")}" maxlength="500">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label text-left">{__("Father's name")}</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" id="parent_name_dad" name="parent_name_dad" placeholder="{__("Father's name")}" maxlength="50">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label text-left">{__("Telephone")}</label>
        <div class="col-sm-3">
            <input type="text" class="form-control" id="parent_phone_dad" name="parent_phone_dad" value="{$child['parent_phone_dad']}" placeholder="{__("Telephone")}" maxlength="50">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label text-left">{__("Job")}</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" id="parent_job_dad" name="parent_job_dad" value="{$child['parent_job_dad']}" placeholder="{__("Job")}" maxlength="500">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label text-left">{__("Parent email")}</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" id="parent_email" name="parent_email" value="{$child['parent_email']}" placeholder="{__("Parent email")}" maxlength="100">
            <div>{__("If parent do not have any email, please let it empty")}.</div>
        </div>
        <div class="col-sm-4">
            <input type="checkbox" value="1" name="create_parent_account" id="create_parent_account" {if count($parent) > 0}disabled{else}checked{/if}>&nbsp;{__("Auto-create parent account")}?
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label text-left">{__("Address")}</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" name="address" value="{$child['address']}" placeholder="{__("Address")}" maxlength="250">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label text-left">{__("Class")} (*)</label>
        <div class="col-sm-4">
            <select name="class_id" id="class_id" class="form-control">
                {if $class_id == 0}
                    {if $school['children_use_no_class']}
                        <option value="0">{__("No class")}</option>
                    {/if}

                    {$class_level = -1}
                    {foreach $classes as $class}
                        {if ($class_level != $class['class_level_id'])}
                            <option value="" disabled style="color: blue">-----{$class['class_level_name']}-----</option>
                        {/if}
                        <option value="{$class['group_id']}">{$class['group_title']}</option>
                        {$class_level = $class['class_level_id']}
                    {/foreach}
                {else}
                    {foreach $classes as $class}
                        {if ($class_id == $class['group_id'])}
                            <option value="{$class_id}" disabled selected>{$class['group_title']}</option>
                        {/if}
                    {/foreach}
                {/if}
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label text-left">{__("Study start date")} (*)</label>
        <div class='col-sm-3'>
            <div class='input-group date' id='beginat_dp'>
                <input id="start_date" type='text' name="begin_at" value="" class="form-control" placeholder="{__("Study start date")}" required/>
                <label class="input-group-addon btn" for="date">
                    <span class="fas fa-calendar-alt"></span>
                </label>
            </div>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 control-label text-left">{__("Description")}</label>
        <div class="col-sm-9">
            <textarea class="form-control" name="description" placeholder="{__("Write about your child...")}" maxlength="300">{$child['description']}</textarea>
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-3 col-sm-offset-3">
            <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
        </div>
        <div class="col-sm-4">
            <a href="#" class="btn btn-default js_add_child_clear">{__("Clear Data")}</a>
        </div>
    </div>

    <!-- success -->
    <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
    <!-- success -->

    <!-- error -->
    <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
    <!-- error -->
</form>