<div class="panel panel-default">
    <div class="panel-heading with-icon">
        {if ($sub_view == "") && $canEdit}
            <div class = "pull-right flip">
                <a href="{$system['system_url']}/class/{$username}/points/importManual" class="btn btn-default">
                    <i class="fa fa-plus"></i> {__("Import point")}
                </a>
            </div>
        {/if}
        <i class="fa fa-tasks fa-fw fa-lg pr10"></i>
        {__('Point')}
       {if $sub_view == "listsbysubject"}
            &rsaquo; {__('Lists by subject')}
        {elseif $sub_view == "listsbystudent"}
            &rsaquo; {__('Lists by student')}
        {elseif $sub_view == "importManual"}
            &rsaquo; {__('Import point')}
        {/if}
    </div>
    {if $sub_view == "listsbysubject"}
        <div class="panel-body">
            <form class="form-horizontal" action="#" enctype="multipart/form-data" method="post" id="point_search_form">
                <input type="hidden" name="username" id="username" value="{$username}"/>
                <input type="hidden" name="do" value="search_with_subject"/>
                <div class="form-group">
                    <div class="col-sm-3 col-sm-offset-9">
                        <select name="school_year" id="school_year" class="form-control">
                            {foreach $default_year as $key => $year}
{*                                <option value="{$year}" {if $key == count($default_year) - 1}selected{/if}>{$year}</option>*}
                                <option value="{$year}" {if (substr($year, 0, strlen(date("Y"))) === date("Y"))}selected{/if}>{$year}</option>
                            {/foreach}
                        </select>
                    </div>
                </div>
{*                <div class="form-group">*}
{*                    <label class="col-sm-3 control-label text-left">{__("Select Subject")} (*)</label>*}
{*                    <div class="col-sm-3">*}
{*                        <select name="class_level_id" id="point_class_level_id" data-username="{$username}" class="form-control" autofocus>*}
{*                            <option value="">{__("Select class level")}</option>*}
{*                            {foreach $class_levels as $level}*}
{*                                <option value="{$level['class_level_id']}">{$level['class_level_name']}</option>*}
{*                            {/foreach}*}
{*                        </select>*}
{*                    </div>*}
{*                    <div class="col-sm-3">*}
{*                        <select name="class_id" id="point_class_id_import" class="form-control" data-username="{$username}" required>*}
{*                            <option value="">{__("Select class...")}</option>*}
{*                        </select>*}
{*                    </div>*}
{*                </div>*}
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Select Subject")} (*)</label>
                    <div class="col-sm-3">
                        <select id="semester" name="semester" data-username="{$username}" class="form-control">
                            {foreach $semesters as $semester => $name}
                                <option value="{$semester}">{$name}</option>
                            {/foreach}
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <select name="subject_id" id="point_subject_id" class="form-control" data-username="{$username}" required>
                            <option value="">{__("Select subject")}...</option>
{*                            {foreach $default_subjects as $level}*}
{*                                <option value="{$level['subject_id']}">{$level['subject_name']}</option>*}
{*                            {/foreach}*}
                        </select>
                    </div>
                </div>
                {if $grade == 1}
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-3">
                            <select name="is_last_semester" id="is_last_semester" class="form-control">
                                <option value="0">{__("Mid semester")}</option>
                                <option value="1">{__("Last semester")}</option>
                            </select>
                        </div>
                    </div>
                {/if}
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" id="submit_id" class="btn btn-primary padrl30">{__("Search")}</button>
                    </div>
                </div>
                <div id="class_list_point"></div>
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
            </form>

        </div>
    {elseif $sub_view == "listsbystudent"}
        <div class="panel-body">
            <form class="form-horizontal" action="#" enctype="multipart/form-data" method="post" id="point_search_form">
                <input type="hidden" name="username" id="username" value="{$username}"/>
                <input type="hidden" name="do" value="search_with_student"/>
                <div class="form-group">

                    <div class="col-sm-3 col-sm-offset-9">
                        <select name="school_year" id="school_year" class="form-control">
                            {foreach $default_year as $key => $year}
{*                                <option value="{$year}" {if $key == count($default_year) - 1}selected{/if}>{$year}</option>*}
                                <option value="{$year}" {if (substr($year, 0, strlen(date("Y"))) === date("Y"))}selected{/if}>{$year}</option>
                            {/foreach}
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Select student")} (*)</label>
{*                    <div class="col-sm-3">*}
{*                        <select name="class_level_id" id="point_class_level_id" data-username="{$username}" class="form-control" autofocus>*}
{*                            <option value="">{__("Select class level")}</option>*}
{*                            {foreach $class_levels as $level}*}
{*                                <option value="{$level['class_level_id']}">{$level['class_level_name']}</option>*}
{*                            {/foreach}*}
{*                        </select>*}
{*                    </div>*}
{*                    <div class="col-sm-3">*}
{*                        <select name="class_id" id="point_class_id_import" class="form-control" data-username="{$username}" required>*}
{*                            <option value="">{__("Select class...")}</option>*}
{*                        </select>*}
{*                    </div>*}
                    <div class="col-sm-3">
                        {*                        <select name="subject_id" id="point_subject_id" class="form-control" data-username="{$username}" required></select>*}
                        <select name="student_id" id="point_student_id" class="form-control" data-username="{$username}" required>
                            <option value="">{__("Select student")}...</option>
                            {foreach $default_childrens as $children}
                              <option value="{$children['child_id']}">{$children['child_name']}</option>
                            {/foreach}
                        </select>
                    </div>
                </div>
                {if $grade == 1}
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-3">
                            <select name="is_last_semester" id="is_last_semester" class="form-control">
                                <option value="0">{__("Mid semester")}</option>
                                <option value="1">{__("Last semester")}</option>
                            </select>
                        </div>
                    </div>
                {/if}
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" id="submit_id" class="btn btn-primary padrl30">{__("Search")}</button>
                    </div>
                </div>
                <div id="class_list_point"></div>
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
            </form>

        </div>
    {elseif $sub_view == "importManual"}
        <div class="panel-body">
            <form class="form-horizontal" action="#" enctype="multipart/form-data" method="post" id="point_import_Manual_form">
                <input type="hidden" name="username" id="username" value="{$username}"/>
                <input type="hidden" name="do" value="importManual"/>
                <input type="hidden" name="score_fomula" value="{$score_fomula}"/>
                <div class="form-group">
                    <div class="col-sm-3 col-sm-offset-9">
                        <select name="school_year" id="school_year" class="form-control">
                            {foreach $default_year as $key => $year}
{*                                <option value="{$year}" {if $key == count($default_year) - 1}selected{/if}>{$year}</option>*}
                                <option value="{$year}" {if (substr($year, 0, strlen(date("Y"))) === date("Y"))}selected{/if}>{$year}</option>
                            {/foreach}
                        </select>
                    </div>
                </div>

{*                <div class="form-group">*}
{*                    *}
{*                </div>*}
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Select Subject")} (*)</label>
                    <div class="col-sm-3">
                        <select id="semester" name="semester" data-username="{$username}" class="form-control">
                            {foreach $semesters as $semester => $name}
                                <option value="{$semester}">{$name}</option>
                            {/foreach}
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <select name="subject_id" id="point_subject_id" class="form-control" data-username="{$username}" required>
                            <option value="">{__("Select subject")}...</option>
{*                            {foreach $default_subjects as $level}*}
{*                                <option value="{$level['subject_id']}">{$level['subject_name']}</option>*}
{*                            {/foreach}*}
                        </select>
                    </div>

                </div>
                {if $grade == 1}
                    <div class="form-group">
                        <div class="col-sm-3">
                            <select name="is_last_semester" id="is_last_semester" class="form-control">
                                <option value="0">{__("Mid semester")}</option>
                                <option value="1">{__("Last semester")}</option>
                            </select>
                        </div>
                    </div>
                {/if}
                <div class="form-group action-point" style="display: none">
                    <div class="col-sm-2">
                        <label class="col-sm-2 control-label text-left">{__("Action")}</label>
                    </div>
                    <div class="col-sm-10">
                        <div class="form-group">
                            <div class="col-sm-4 point_tx-add point_tx1-add">
                                <label class="point_tx-add">{__("Regular score")} :</label>
                                <label class="point_tx1-add">{__("Regular score 1")} :</label>
                                <button type="button" style="display: none;" class="btn btn-success js_point_tx-add">+
                                </button>
                                <button type="button" style="display: none;" class="btn btn-success js_point_tx1-add">+
                                </button>
                            </div>
                            <div class="col-sm-4 point_gk-add point_gk1-add">
                                <label class="point_gk-add">{__("Midterm grades")} :</label>
                                <label class="point_gk1-add">{__("Midterm grades 1")} :</label>
                                <button type="button" style="display: none;" class="btn btn-success js_point_gk-add">+
                                </button>
                                <button type="button" style="display: none;" class="btn btn-success js_point_gk1-add">+
                                </button>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4 point_tx1-add">
                                <label class="point_tx1-add">{__("Regular score 2")} :</label>
                                <button type="button" style="display: none;" class="btn btn-success js_point_tx2-add">+
                                </button>
                            </div>
                            <div class="col-sm-4 point_gk2-add">
                                <label class="point_gk2-add">{__("Midterm grades 2")} :</label>
                                <button type="button" style="display: none;" class="btn btn-success js_point_gk2-add">+
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="class_list_point"></div>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-4">
                        <button type="submit" id="submit_id" class="btn btn-primary padrl30">{__("Save")}</button>
                    </div>
                </div>

                <div class="table-responsive" id="result_info" name="result_info"></div>

                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
            </form>

        </div>
    {elseif $sub_view == "comment"}
        <div class="panel-body">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/class/boclass_point.php">
                <input type="hidden" name="username" id="username" value="{$username}"/>
                <input type="hidden" name="do" value="comment"/>
                <div class="form-group">
                    <div class="col-sm-3 col-sm-offset-9">
                        <select name="school_year" id="school_year" class="form-control">
                            {foreach $default_year as $key => $year}
{*                                <option value="{$year}" {if $key == count($default_year) - 1}selected{/if}>{$year}</option>*}
                                <option value="{$year}" {if (substr($year, 0, strlen(date("Y"))) === date("Y"))}selected{/if}>{$year}</option>
                            {/foreach}
                        </select>
                    </div>
                </div>
{*                <div class="form-group">*}
{*                    <label class="col-sm-3 control-label text-left">{__("Select student")} (*)</label>*}
{*                    <div class="col-sm-3">*}
{*                        <select name="class_level_id" id="point_class_level_id" data-username="{$username}" class="form-control" autofocus>*}
{*                            <option value="">{__("Select class level")}</option>*}
{*                            {foreach $class_levels as $level}*}
{*                                <option value="{$level['class_level_id']}">{$level['class_level_name']}</option>*}
{*                            {/foreach}*}
{*                        </select>*}
{*                    </div>*}
{*                    <div class="col-sm-3">*}
{*                        <select name="class_id" id="point_class_id_comment" class="form-control" data-username="{$username}" required></select>*}
{*                    </div>*}
{*                </div>*}
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Select student")} (*)</label>
                    <div class="col-sm-3">
                        <select id="semester" name="semester" class="form-control">
                            {foreach $semesters as $semester => $name}
                                <option value="{$semester}">{$name}</option>
                            {/foreach}
                        </select>
                    </div>
                    <div class="col-sm-3">
{*                        <select name="child_id" id="point_child_id_comment" class="form-control" data-username="{$username}" required></select>*}
                        <select name="student_id" id="point_child_id_comment" class="form-control" data-username="{$username}" required>
                            <option value="">{__("Select student")}...</option>
                            {foreach $default_childrens as $children}
                                <option value="{$children['child_id']}">{$children['child_name']}</option>
                            {/foreach}
                        </select>
                    </div>
                </div>
                {if $grade == 1}
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-3">
                            <select name="is_last_semester" id="is_last_semester" class="form-control">
                                <option value="0">{__("Mid semester")}</option>
                                <option value="1">{__("Last semester")}</option>
                            </select>
                        </div>
                    </div>
                {/if}

{*                <div class="form-group">*}
{*                    <div class="col-sm-9 col-sm-offset-3">*}
{*                        <button type="button" class="btn btn-primary padrl30 search_child_comment"  data-username="{$username}">{__("Search")}</button>*}
{*                    </div>*}
{*                </div>*}

                <div class="table-responsive" id="child_comment"></div>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-4">
                        <button type="submit" class="btn btn-primary padrl30" disabled="true" id="submit_id">{__("Save")}</button>
                    </div>
                </div>
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
            </form>

        </div>
    {/if}
</div>