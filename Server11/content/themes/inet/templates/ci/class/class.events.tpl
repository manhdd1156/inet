<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <div class="pull-right flip">
{*            <a href="https://blog.coniu.vn/huong-dan-giao-vien-dang-ky-su-kien-tren-website-inet/" class="btn btn-info btn_guide" target="_blank">*}
            <a href="#" class="btn btn-info btn_guide" target="_blank">
                <i class="fa fa-info-circle"></i> {__("Guide")}
            </a>
        </div>
        {*{if $sub_view == "" && $schoolConfig['allow_class_event']}*}
            {*<div class="pull-right flip">*}
                {*<a href="{$system['system_url']}/class/{$username}/events/add" class="btn btn-default">*}
                    {*<i class="fa fa-plus"></i> {__("Add New")}*}
                {*</a>*}
            {*</div>*}
        {*{/if}*}
        <i class="fa fa-bell fa-lg fa-fw pr10"></i>
        {__("Notification - Event")}
        {*{if $sub_view == "edit"}*}
            {*&rsaquo; {$data['event_name']}*}
        {if $sub_view == "detail"}
            &rsaquo; {$data['event_name']}
        {*{elseif $sub_view == "add"}*}
            {*{if $schoolConfig['allow_class_event']}*}
                {*&rsaquo; {__('Add New')}*}
            {*{/if}*}
        {elseif $sub_view == "participants"}
            &rsaquo; {__("Participants")} &rsaquo; <a href="{$system['system_url']}/class/{$username}/events/detail/{$event['event_id']}">{$event['event_name']}</a>
        {/if}
    </div>
    {if $sub_view == ""}
        <div class="panel-body with-table">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover js_dataTable">
                    <thead>
                    <tr><th colspan="4">{__("Notification - Event list")}&nbsp;({$rows|count})</th></tr>
                    <tr>
                        <th>#</th>
                        <th>{__("Notification - Event")}</th>
                        <th>{__("Event level")}</th>
                        {*<th>{__("Post on wall")}?</th>*}
                        {*<th>{__("Notify immediately")}?</th>*}
                        {*<th>{__("Time")}</th>*}
                        <th>{__("Actions")}</th>
                    </tr>
                    </thead>
                    <tbody>
                    {$idx = 1}
                    {foreach $rows as $row}
                        <tr>
                            {if $idx > 1}<tr><td colspan="7"></td></tr>{/if}
                            <td align="center" rowspan="2" style="vertical-align:middle"><strong>{$idx}</strong></td>
                            <td style="vertical-align:middle">
                                <a href="{$system['system_url']}/class/{$username}/events/detail/{$row['event_id']}">{$row['event_name']}</a> <a href="#" data-id="{$row['event_id']}" style="float: right; color: #5D9D58" class="js_event_toggle">{__("Show/hide")}</a>
                                {if $row['status']== $smarty.const.EVENT_STATUS_CANCELLED}
                                    <img src="{$system['system_url']}/content/themes/{$system['theme']}/images/cancel.png"/>
                                {/if}
                            </td>
                            <td align="center" style="vertical-align:middle">
                                {if $row['level']==$smarty.const.SCHOOL_LEVEL}
                                    {__("School")}
                                {elseif $row['level']==$smarty.const.CLASS_LEVEL_LEVEL}
                                    {__("Class level")}
                                {elseif $row['level']==$smarty.const.CLASS_LEVEL}
                                    {__("Class")}
                                {elseif $row['level']==$smarty.const.TEACHER_LEVEL}
                                    {__("Teacher")}
                                {/if}
                            </td>
                            {*<td align="center" style="vertical-align:middle">*}
                                {*{if $row['post_on_wall'] == 0}*}
                                    {*<i class="fa fa-times" aria-hidden="true"></i>*}
                                {*{else}*}
                                    {*<i class="fa fa-check" aria-hidden="true"></i>*}
                                {*{/if}*}
                            {*</td>*}
                            {*<td align="center" style="vertical-align:middle">*}
                                {*{if $row['is_notified'] == 0}*}
                                    {*<i class="fa fa-times" aria-hidden="true"></i>*}
                                {*{else}*}
                                    {*<i class="fa fa-check" aria-hidden="true"></i>*}
                                {*{/if}*}
                            {*</td>*}
                            {*<td align="center" style="vertical-align:middle" nowrap="true">*}
                                {*{if (isset($row['begin']) && ($row['begin'] != ''))}*}
                                    {*<font color="blue">{$row['begin']}</font><br/>{$row['end']}*}
                                {*{/if}*}
                            {*</td>*}
                            <td align="center" style="vertical-align:middle">
                                {*{if (!$row['is_notified']) && ($row['status'] != $smarty.const.EVENT_STATUS_CANCELLED)}*}
                                    {*<button class="btn btn-xs btn-default js_class-event-notify" data-username="{$username}" data-id="{$row['event_id']}">*}
                                        {*{__("Notify")}*}
                                    {*</button>*}
                                {*{/if}*}
                                {if $row['must_register']}
                                    <a href="{$system['system_url']}/class/{$username}/events/participants/{$row['event_id']}" class="btn btn-xs btn-default">
                                        {__("Participants")}
                                    </a>
                                {/if}

                                {*{if ($row['status'] != $smarty.const.EVENT_STATUS_CANCELLED)}*}
                                    {*<a href="{$system['system_url']}/class/{$username}/events/edit/{$row['event_id']}" class="btn btn-xs btn-default">{__("Edit")}</a>*}
                                    {*{if (($row['parent_participants'] + $row['child_participants']) == 0) && (!$row['is_notified'])}*}
                                        {*<button class="btn btn-xs btn-danger js_class-event-delete" data-username="{$username}" data-id="{$row['event_id']}">{__("Delete")}</button>*}
                                    {*{else}*}
                                        {*<button class="btn btn-xs btn-warning js_class-event-cancel" data-username="{$username}" data-id="{$row['event_id']}">{__("Cancel")}</button>*}
                                    {*{/if}*}
                                {*{/if}*}
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" style="display: none" class="event_description_{$row['event_id']}">{$row['description']}</td>
                        </tr>
                        {$idx = $idx + 1}
                    {/foreach}
                    </tbody>
                </table>
            </div>
        </div>
    {elseif $sub_view == "detail"}
        <div class="panel-body with-table">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <tbody>
                    <tr>
                        <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Notification - Event")}</strong></td>
                        <td>
                            {$data['event_name']}
                            {if $data['status']== $smarty.const.EVENT_STATUS_CANCELLED}
                                <img src="{$system['system_url']}/content/themes/{$system['theme']}/images/cancel.png"/>
                            {/if}
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Event level")}</strong></td>
                        <td>
                            {if $data['level']==$smarty.const.SCHOOL_LEVEL}
                                {__("School")}
                            {elseif $data['level']==$smarty.const.CLASS_LEVEL_LEVEL}
                                {$data['class_level_name']}
                            {elseif $data['level']==$smarty.const.CLASS_LEVEL}
                                {$data['group_title']}
                            {elseif $data['level']==$smarty.const.TEACHER_LEVEL}
                                {__("Teacher")}
                            {/if}
                        </td>
                    </tr>
                    {*<tr>*}
                        {*<td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Post on wall")}?</strong></td>*}
                        {*<td>{if $data['post_on_wall']}{__('Post on wall')}{else}{__('Unpost on wall')}{/if}</td>*}
                    {*</tr>*}
                    {*<tr>*}
                        {*<td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Notify immediately")}?</strong></td>*}
                        {*<td>{if $data['is_notified']}{__('Notified')}{else}{__('Not notified yet')}{/if}</td>*}
                    {*</tr>*}
                    {*{if (isset($data['begin']) && ($data['begin'] != ''))}*}
                        {*<tr>*}
                            {*<td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Time")}</strong></td>*}
                            {*<td>*}
                                {*<font color="blue">{$data['begin']}</font> - {$data['end']}*}
                            {*</td>*}
                        {*</tr>*}
                    {*{/if}*}
                    {if $data['must_register'] == '1'}
                        {*<tr>*}
                            {*<td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Event location")}</strong></td>*}
                            {*<td>{$data['location']}</td>*}
                        {*</tr>*}
                        {*<tr>*}
                            {*<td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Participation cost")}</strong></td>*}
                            {*<td>{$data['price']} {$smarty.const.MONEY_UNIT}</td>*}
                        {*</tr>*}
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Must register")}?</strong></td>
                            <td>{__('Yes')}</td>
                        </tr>
                        {if $data['registration_deadline'] != ''}
                            <tr>
                                <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Registration deadline")}</strong></td>
                                <td><font color="red">{$data['registration_deadline']}</font></td>
                            </tr>
                        {/if}
                        {*<tr>*}
                            {*<td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("For child")}?</strong></td>*}
                            {*<td>{if $data['for_child'] == '1'}{__('Yes')}{else}{__('No')}{/if}</td>*}
                        {*</tr>*}
                        {*{if $data['for_child'] == '1'}*}
                            {*<tr>*}
                                {*<td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Children total")}</strong></td>*}
                                {*<td>{$data['child_participants']}</td>*}
                            {*</tr>*}
                        {*{/if}*}
                        {*<tr>*}
                            {*<td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("For parent")}?</strong></td>*}
                            {*<td>{if $data['for_parent'] == '1'}{__('Yes')}{else}{__('No')}{/if}</td>*}
                        {*</tr>*}
                        {*{if $data['for_parent'] == '1'}*}
                            {*<tr>*}
                                {*<td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Parent total")}</strong></td>*}
                                {*<td>{$data['parent_participants']}</td>*}
                            {*</tr>*}
                        {*{/if}*}
                    {/if}
                    <tr style="background: #fff">
                        {*<td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Description")}</strong></td>*}
                        <td colspan="2">{$data['description']}</td>
                    </tr>
                    {*<tr>*}
                        {*<td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Creator")}</strong></td>*}
                        {*<td>{$data['user_fullname']}</td>*}
                    {*</tr>*}
                    {*<tr>*}
                        {*<td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Created time")}</strong></td>*}
                        {*<td>{$data['created_at']}</td>*}
                    {*</tr>*}
                    {*{if $data['updated_at'] != ''}*}
                        {*<tr>*}
                            {*<td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Last updated")}</strong></td>*}
                            {*<td>{$data['updated_at']}</td>*}
                        {*</tr>*}
                    {*{/if}*}
                    </tbody>
                </table>
            </div>
            <div class="form-group pl5">
                <div class="col-sm-12">
                    <a class="btn btn-default" href="{$system['system_url']}/class/{$username}/events">{__("Lists")}</a>
                    {if $data['must_register']}
                        <a href="{$system['system_url']}/class/{$username}/events/participants/{$data['event_id']}" class="btn btn-default">{__("Participants")}</a>
                    {/if}
                    {*{if $user->_data['user_id'] == $data['created_user_id']}*}
                        {*{if $data['happened'] == 0}*}
                            {*<a class="btn btn-default" href="{$system['system_url']}/class/{$username}/events/edit/{$data['event_id']}">{__("Edit")}</a>*}
                            {*{if !$data['is_notified']}*}
                                {*<a class="btn btn-default js_class-event-notify" data-username="{$username}" data-id="{$data['event_id']}">{__("Notify")}</a>*}
                            {*{/if}*}
                        {*{/if}*}
                        {*{if (($data['parent_participants'] + $data['child_participants']) == 0) && (!$data['is_notified'])}*}
                            {*<a class="btn btn-danger js_class-event-delete" data-username="{$username}" data-id="{$data['event_id']}">{__("Delete")}</a>*}
                        {*{else}*}
                            {*<a class="btn btn-warning js_class-event-cancel" data-username="{$username}" data-id="{$data['event_id']}">{__("Cancel")}</a>*}
                        {*{/if}*}
                    {*{/if}*}
                </div>
            </div>
        </div>
    {elseif $sub_view == "participants"}
        <div class="panel-body with-table">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/class/boclass_event.php">
                <input type="hidden" name="username" id="username" value="{$username}"/>
                <input type="hidden" name="event_id" id="event_id" value="{$event['event_id']}"/>
                <input type="hidden" name="event_name" id="event_name" value="{$event['event_name']}"/>
                <input type="hidden" name="do" value="add_pp"/>

                {if $event['can_register'] == 1}
                    <div>
                        {if $event['for_child'] && $event['for_parent']}
                            <input type="checkbox" id="eventCheckAll">&nbsp;{__("All")}
                        {/if}
                        {if $event['for_child']}
                            &nbsp;&nbsp;<input type="checkbox" id="eventCheckAllChildren">&nbsp;{__("All children")}
                        {/if}
                        {if $event['for_parent']}
                            &nbsp;&nbsp;<input type="checkbox" id="eventCheckAllParent">&nbsp;{__("All parent")}
                        {/if}
                    </div>
                {/if}
                <div class="table-responsive">
                    <div>
                        <strong>
                            {__("Participants")} ({__("Registered")}:
                            {if $event['for_child']}{$childCount} {__("children")}{/if}
                            {if $event['for_child'] && $event['for_parent']}&nbsp;|&nbsp;{/if}
                            {if $event['for_parent']}{$parentCount} {__("parent")}{/if})
                        </strong>
                    </div>
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>{__("Select")}</th>
                            <th>{__("Child")}</th>
                            <th>{__("Parent phone")}</th>
                            <th>{__("Parent")}</th>
                        </tr>
                        </thead>
                        <tbody>
                        {$idx = 1}
                        {foreach $participants as $child}
                            <tr {if $child['child_status'] == 0} class="row-disable" {/if}>
                                <td align="center">{$idx}</td>
                                <td align="center">
                                    {if $event['for_child']}
                                        <input type="checkbox" class="child" name="childIds[]" value="{$child['child_id']}"
                                               {if $child['event_id'] > 0}checked {if $child['created_user_id']!=$user->_data['user_id']}disabled{/if}{/if}
                                                {if $event['can_register'] == 0}disabled{/if}>

                                        {if $event['can_register'] > 0}
                                            {*Lưu tên trẻ để post ngược về server*}
                                            <input type="hidden" name="child_name_{$child['child_id']}" value="{$child['child_name']}"/>
                                        {/if}

                                        {if $child['event_id'] > 0  && ($child['created_user_id'] == $user->_data['user_id'])}
                                            <input type="hidden" name="oldChildIds[]" value="{$child['child_id']}"/>
                                        {/if}
                                    {/if}
                                </td>
                                <td>{$child['child_name']} ({$child['birthday']})
                                    {if ($child['event_id'] > 0) && ($child['created_user_id'] != $user->_data['user_id'])}
                                        <br/>
                                        {if $event['can_register'] > 0}
                                            <a href="#" class="btn btn-xs btn-warning js_class-event-remove-participant" data-type="{$smarty.const.PARTICIPANT_TYPE_CHILD}" data-uid="{$child['child_id']}" data-name="{$child['child_name']}">
                                                {__("Cancel")}
                                            </a>
                                        {else}
                                            {__("Registrar")}:
                                        {/if} <b>{$child['created_fullname']}</b>
                                    {/if}
                                </td>
                                <td>{$child['parent_phone']}</td>
                                <td>
                                    {if $event['for_parent']}
                                        {foreach $child['parent'] as $parent}
                                            <input type="checkbox" class="parent" name="parentIds[]" value="{$parent['user_id']}"
                                                   {if $parent['event_id'] > 0}checked {if $parent['created_user_id']!=$user->_data['user_id']}disabled{/if}{/if}
                                                    {if $event['can_register'] == 0}disabled{/if}>
                                            {if $event['can_register'] > 0}
                                                {*Lưu vào trường thông tin để post ngược về server*}
                                                <input type="hidden" name="child_id_of_parent_{$parent['user_id']}" value="{$child['child_id']}"/>
                                                <input type="hidden" name="parent_name_{$parent['user_id']}" value="{$parent['user_fullname']}"/>
                                            {/if}
                                            <span class="name js_user-popover" data-uid="{$parent['user_id']}">
                                                <a href="{$system['system_url']}/{$parent['user_name']}">{$parent['user_fullname']}</a>
                                            </span>
                                            {if $parent['user_id'] != $user->_data['user_id']}
                                                <a href="#" class="far fa-comments fa-lg js_chat-start" data-name="{$parent['user_fullname']}" data-uid="{$parent['user_id']}"></a>
                                            {/if}
                                            {if ($parent['event_id'] > 0) && ($parent['created_user_id'] != $user->_data['user_id'])}
                                                <br/>
                                                {if $event['can_register'] > 0}
                                                    <a href="#" class="btn btn-xs btn-warning js_class-event-remove-participant" data-type="{$smarty.const.PARTICIPANT_TYPE_PARENT}" data-uid="{$parent['user_id']}" data-name="{$parent['user_fullname']}" data-child="{$child['child_id']}">
                                                        {__("Cancel")}
                                                    </a>
                                                {else}
                                                    {__("Registrar")}:
                                                {/if} <b>{$parent['created_fullname']}</b>
                                            {/if}
                                            {if $parent['event_id'] > 0 && ($parent['created_user_id'] == $user->_data['user_id'])}
                                                <input type="hidden" name="oldParentIds[]" value="{$parent['user_id']}"/>
                                            {/if}
                                            <br/>
                                        {/foreach}
                                    {/if}
                                </td>
                            </tr>
                            {$idx = $idx + 1}
                        {/foreach}
                        </tbody>
                    </table>
                </div>
                {if (count($participants) > 0) && ($event['can_register'] == 1)}
                    <div class="form-group pl5">
                        <div class="col-sm-9">
                            <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                        </div>
                    </div>
                {elseif $event['can_register'] == 0}
                    <div class="form-group pl5">
                        <div class="col-sm-9">
                            {__("The registration deadline has passed or the event has been cancelled")}.
                        </div>
                    </div>
                {/if}

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    {elseif $sub_view == "edit"}
        <div class="panel-body">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/class/boclass_event.php">
                <input type="hidden" name="username" value="{$username}"/>
                <input type="hidden" name="event_id" value="{$data['event_id']}"/>
                <input type="hidden" name="post_ids" value="{$data['post_ids']}"/>
                <input type="hidden" name="do" value="edit"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Notification - Event")} (*)</label>
                    <div class="col-sm-9">
                        <input class="form-control" name="event_name" value="{$data['event_name']}" required autofocus maxlength="250">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Description")} (*)</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="description" rows="5" required>{$data['description']}</textarea>
                    </div>
                </div>
                <div class="form-group" name="event_post_on_wall">
                    <label class="col-sm-3 control-label text-left">
                        {__("Post on page")}?
                    </label>
                    <div class="col-sm-9">
                        <div class="onoffswitch">
                            <input type="checkbox" name="post_on_wall" class="onoffswitch-checkbox" id="post_on_wall" {if $data['post_on_wall']=='1'}checked{/if}>
                            <label class="onoffswitch-label" for="post_on_wall"></label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">
                        {__("Notify immediately")}?
                    </label>
                    <div class="col-sm-9">
                        <div class="onoffswitch">
                            <input type="checkbox" name="notify_immediately" class="onoffswitch-checkbox" id="notify_immediately" {if $data['is_notified']=='1'}checked{/if}>
                            <label class="onoffswitch-label" for="notify_immediately"></label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Time")}</label>
                    <div class="col-sm-9">
                        <div class='col-sm-5'>
                            <div class='input-group date' id='beginpicker'>
                                <input type='text' name="begin" value="{$data['begin']}" class="form-control" placeholder="{__("Begin")}"/>
                                <span class="input-group-addon">
                                    <span class="fas fa-calendar-alt"></span>
                                </span>
                            </div>
                        </div>

                        <div class='col-md-5'>
                            <div class='input-group date' id='endpicker'>
                                <input type='text' name="end" value="{$data['end']}" class="form-control" placeholder="{__("End")}"/>
                                <span class="input-group-addon">
                                    <span class="fas fa-calendar-alt"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                {if $data['must_register'] == '0'}
                    <div class="form-group" id="event_more_information_button">
                        <div class="col-sm-12">
                            <a href="#" class="btn btn-default pull-right js_event-more-information">{__("More information")}</a>
                        </div>
                    </div>
                {/if}
                <div {if $data['must_register'] == '0'}class="x-hidden"{/if} id="event_more_information">
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Event location")}</label>
                        <div class="col-sm-9">
                            <input class="form-control" name="location" value="{$data['location']}" maxlength="250">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Participation cost")}</label>
                        <div class="col-sm-9">
                            <div class="input-group col-md-5">
                                <input type="number" min="0" step="1000" class="form-control" name="price" value="{$data['price']}" maxlength="10">
                                <span class="input-group-addon">{$smarty.const.MONEY_UNIT}</span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Must register")}?</label>
                        <div class="col-sm-9">
                            <div class="onoffswitch">
                                <input type="checkbox" name="must_register" class="onoffswitch-checkbox" id="must_register" {if $data['must_register']=='1'}checked{/if}>
                                <label class="onoffswitch-label" for="must_register"></label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group" id="event_registration_deadline">
                        <label class="col-sm-3 control-label text-left">{__("Registration deadline")}</label>
                        <div class='col-sm-4'>
                            <div class='input-group date' id='registration_deadlinepicker'>
                                <input type='text' name="registration_deadline" value="{$data['registration_deadline']}" class="form-control" placeholder="{__("Registration deadline")}"/>
                                <span class="input-group-addon">
                                    <span class="fas fa-calendar-alt"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group" id="event_for_child">
                        <label class="col-sm-3 control-label text-left">{__("For child")}?</label>
                        <div class="col-sm-9">
                            <div class="onoffswitch">
                                <input type="checkbox" name="for_child" class="onoffswitch-checkbox" id="for_child" {if $data['for_child']=='1'}checked{/if}>
                                <label class="onoffswitch-label" for="for_child"></label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group" id="event_for_parent">
                        <label class="col-sm-3 control-label text-left">{__("For parent")}?</label>
                        <div class="col-sm-9">
                            <div class="onoffswitch">
                                <input type="checkbox" name="for_parent" class="onoffswitch-checkbox" id="for_parent" {if $data['for_parent']=='1'}checked{/if}>
                                <label class="onoffswitch-label" for="for_parent"></label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    {elseif $sub_view == "add" && $schoolConfig['allow_class_event']}
        <div class="panel-body">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/class/boclass_event.php">
                <input type="hidden" name="username" value="{$username}"/>
                <input type="hidden" name="do" value="add"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Notification - Event")} (*)</label>
                    <div class="col-sm-9">
                        <input class="form-control" name="event_name" required autofocus maxlength="250">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Description")} (*)</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="description" rows="5" required></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Post on page")}?</label>
                    <div class="col-sm-9">
                        <div class="onoffswitch">
                            <input type="checkbox" name="post_on_wall" class="onoffswitch-checkbox" id="post_on_wall" checked>
                            <label class="onoffswitch-label" for="post_on_wall"></label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Notify immediately")}?</label>
                    <div class="col-sm-9">
                        <div class="onoffswitch">
                            <input type="checkbox" name="notify_immediately" class="onoffswitch-checkbox" id="notify_immediately">
                            <label class="onoffswitch-label" for="notify_immediately"></label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Time")}</label>
                    <div class="col-sm-9">
                        <div class='col-sm-5'>
                            <div class='input-group date' id='beginpicker'>
                                <input type='text' name="begin" class="form-control" placeholder="{__("Begin")}"/>
                                    <span class="input-group-addon">
                                        <span class="fas fa-calendar-alt"></span>
                                    </span>
                            </div>
                        </div>

                        <div class='col-md-5'>
                            <div class='input-group date' id='endpicker'>
                                <input type='text' name="end" class="form-control" placeholder="{__("End")}"/>
                                    <span class="input-group-addon">
                                        <span class="fas fa-calendar-alt"></span>
                                    </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group" id="event_more_information_button">
                    <div class="col-sm-12">
                        <a href="#" class="btn btn-default pull-right js_event-more-information">{__("More information")}</a>
                    </div>
                </div>
                <div class="x-hidden" id="event_more_information">
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Event location")}</label>
                        <div class="col-sm-9">
                            <input class="form-control" name="location" value="{__("At class")}" maxlength="250">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Participation cost")}</label>
                        <div class="col-sm-9">
                            <div class="input-group col-md-5">
                                <input type="number" min="0" step="1000" class="form-control" name="price" value="0" maxlength="10">
                                <span class="input-group-addon">{$smarty.const.MONEY_UNIT}</span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Must register")}?</label>
                        <div class="col-sm-9">
                            <div class="onoffswitch">
                                <input type="checkbox" name="must_register" class="onoffswitch-checkbox" id="must_register">
                                <label class="onoffswitch-label" for="must_register"></label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group x-hidden" id="event_registration_deadline">
                        <label class="col-sm-3 control-label text-left">{__("Registration deadline")}</label>
                        <div class='col-sm-4'>
                            <div class='input-group date' id='registration_deadlinepicker'>
                                <input type='text' name="registration_deadline" class="form-control" placeholder="{__("Registration deadline")}"/>
                                <span class="input-group-addon">
                                    <span class="fas fa-calendar-alt"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group x-hidden" id="event_for_child">
                        <label class="col-sm-3 control-label text-left">{__("For child")}?</label>
                        <div class="col-sm-9">
                            <div class="onoffswitch">
                                <input type="checkbox" name="for_child" class="onoffswitch-checkbox" id="for_child">
                                <label class="onoffswitch-label" for="for_child"></label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group x-hidden" id="event_for_parent">
                        <label class="col-sm-3 control-label text-left">{__("For parent")}?</label>
                        <div class="col-sm-9">
                            <div class="onoffswitch">
                                <input type="checkbox" name="for_parent" class="onoffswitch-checkbox" id="for_parent">
                                <label class="onoffswitch-label" for="for_parent"></label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    {/if}
</div>