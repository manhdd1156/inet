<table class="table table-striped table-bordered table-hover js_dataTable">
    <thead>
    <tr><th colspan="7">{__("Report list")}&nbsp;({$results|count}) - {__("Not notified yet")} ({$countNotNotify})</th></tr>
    <tr>
        <th>{__("#")}</th>
        <th>{__("Title")}</th>
        <th>{__("Child")}</th>
        {*<th>{__("Birthdate")}</th>*}
        <th>{__("Notice date")}</th>
        <th data-orderable="false" align="center" style="width: 100px">
            <input type="checkbox" id="report_notify_checkall">
            <button class="btn btn-xs btn-default" id="js_class-report-notify-all-select" data-username="{$username}" data-handle="notify_all_select">{__("Notify")}</button>
        </th>
        <th align="center" style="width: 100px">
            <input type="checkbox" id="report_checkall">
            <button class="btn btn-xs btn-danger" id="js_class-report-delete-all-select" data-username="{$username}" data-handle="delete_all_select">{__("Delete")}</button>
        </th>
    </tr>
    </thead>
    <tbody>
    {$idx = 1}
    {foreach $results as $row}
        <tr {if $row['child_status'] == 0} class="row-disable" {/if}>
            <td class="align-middle" align="center">
                <strong>{$idx}</strong>
            </td>
            <td class="align-middle">
                <a href="{$system['system_url']}/class/{$username}/reports/detail/{$row['report_id']}">
                    {$row['report_name']}
                </a>
            </td>
            <td class="align-middle">
                {$row['child_name']}
            </td>
            {*<td class="align-middle" align="center">*}
                {*{$row['birthday']}*}
            {*</td>*}
            <td class="align-middle" align="center">
                {if $row['is_notified']}
                    {$row['date']}
                {else}
                    {__("Not notified yet")}
                {/if}
            </td>
            <td class="align-middle" align="center">
                {if !$row['is_notified']}
                    <input class="report_notify" type="checkbox" name="notify_report_ids" value="{$row['report_id']}">
                    <button class="btn btn-xs btn-default js_class-report-notify" data-handle="notify" data-username="{$username}" data-id="{$row['report_id']}">{__("Notify")}</button>
                {/if}
            </td>
            <td class="align-middle" align="center">
                <a href="{$system['system_url']}/class/{$username}/reports/edit/{$row['report_id']}" class="btn btn-xs btn-default">{__("Edit")}</a>
                <input class="report_delete" type="checkbox" name="report_ids" value="{$row['report_id']}">
                <button class="btn btn-xs btn-danger js_class-delete" data-handle="delete_report" data-username="{$username}" data-id="{$row['report_id']}">{__("Delete")}</button>
            </td>
        </tr>
        {$idx = $idx + 1}
    {/foreach}
    </tbody>
</table>
<script>
    $(function() {
        // run DataTable
        $('.js_dataTable').DataTable({
            "aoColumnDefs": [ { 'aDataSort': false, 'aTargets': [ -1 ] } ],
            "language": {
                "decimal":        "",
                "emptyTable":     __["No data available in table"],
                "info":           __["Showing _START_ to _END_ of _TOTAL_ results"],
                "infoEmpty":      __["Showing 0 to 0 of 0 results"],
                "infoFiltered":   "(filtered from _MAX_ total entries)",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     __["Show"] + " " + "_MENU_" + " " + __["results"],
                "loadingRecords": __["Loading..."],
                "processing":     __["Processing..."],
                "search":         __["Search"],
                "zeroRecords":    __["No matching records found"],
                "paginate": {
                    "first":      __["First"],
                    "last":       __["Last"],
                    "next":       __["Next"],
                    "previous":   __["Previous"]
                },
                "aria": {
                    "sortAscending":  ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                }
            }
        });
    });
</script>