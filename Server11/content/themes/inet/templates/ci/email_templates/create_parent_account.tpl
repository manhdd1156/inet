Kính gửi ông/bà {$full_name},
Phụ huynh bé {$child_name}

Nhằm tạo điều kiện thuận lợi cho phụ huynh theo dõi thông tin của bé, nhà trường đã đưa vào sử dụng "Ứng dụng tương tác mầm non - Coniu" làm công cụ quản lý và là kênh tương tác/trao đổi giữa nhà trường, giáo viên và phụ huynh.
Hiện tại, nhà trường đã tạo sẵn tài khoản cho ông/bà là:
    - Tài khoản: {$username}
    - Mật khẩu: {$password}
Ông/bà có thể thay đổi thông tin tài khoản sau khi truy cập.

Để sử dụng Coniu với tài khoản được cấp, ông/bà có thể áp dụng 01 trong 02 cách sau:
    1 - Truy cập địa chỉ website Coniu tại địa chỉ: https://inet.vn hoặc;
    2 - Tải ứng dụng 'Coniu' về điện thoại (chỉ áp dụng với điện thoại iPhone và điện thoại sử dụng hệ điều hành Android).

Trân trọng cám ơn sự hợp tác của ông/bà!
Thân ái,
Ban Giám hiệu nhà trường.

