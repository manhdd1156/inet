<!-- create child -->
<div class="panel panel-default">
    <div class="panel-heading">
        <div class="mt5">
            <strong>{__("Create Child")}</strong>
        </div>
    </div>
    <div class="panel-body">
        <form class="js_ajax-forms" data-url="ci/bo/school/bo_child.php?do=create">
            <div class="form-group">
                <label for="title">{__("Full name")} (*)</label>
                <input type="text" class="form-control" name="child_name" id="child_name" placeholder="{__("Full name")}" required autofocus maxlength="45">
            </div>
            <div class="form-group">
                <label for="gender">{__("Gender")} (*)</label>
                <select name="gender" id="gender" class="form-control">
                    <option value="{$smarty.const.MALE}">{__("Male")}</option>
                    <option value="{$smarty.const.FEMALE}">{__("Female")}</option>
                </select>
            </div>
            <div class="form-group">
                <label>{__("Birthdate")} (*)</label>
                <div class="row">
                    <div class="col-xs-4">
                        <select class="form-control" name="birth_day">
                            <option value="">{__("Select Day")}</option>
                            {for $i=1 to 31}
                                <option value="{$i}">{$i}</option>
                            {/for}
                        </select>
                    </div>
                    <div class="col-xs-4">
                        <select class="form-control" name="birth_month">
                            <option value="">{__("Select Month")}</option>
                            <option value="1">{__("Jan")}</option>
                            <option value="2">{__("Feb")}</option>
                            <option value="3">{__("Mar")}</option>
                            <option value="4">{__("Apr")}</option>
                            <option value="5">{__("May")}</option>
                            <option value="6">{__("Jun")}</option>
                            <option value="7">{__("Jul")}</option>
                            <option value="8">{__("Aug")}</option>
                            <option value="9">{__("Sep")}</option>
                            <option value="10">{__("Oct")}</option>
                            <option value="11">{__("Nov")}</option>
                            <option value="12">{__("Dec")}</option>
                        </select>
                    </div>
                    <div class="col-xs-4">
                        <select class="form-control" name="birth_year">
                            <option value="">{__("Select Year")}</option>
                            {for $i=2000 to {'Y'|date}}
                                <option value="{$i}">{$i}</option>
                            {/for}
                        </select>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label for="username">{__("Parent phone")}</label>
                <input type="text" class="form-control" name="parent_phone" id="parent_phone" placeholder="{__("Parent phone")}" maxlength="50">
            </div>
            <div class="form-group">
                <label for="username">{__("Address")}</label>
                <input type="text" class="form-control" name="address" id="address" placeholder="{__("Address")}" maxlength="250">
            </div>
            <div class="form-group">
                <label for="description">{__("Description")}</label>
                <textarea class="form-control" name="description" name="description" placeholder="{__("Write about your child...")}" maxlength="300"></textarea>
            </div>

            <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>

            <!-- error -->
            <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
            <!-- error -->
        </form>
    </div>
</div>
