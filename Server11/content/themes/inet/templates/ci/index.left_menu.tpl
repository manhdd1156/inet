<div {if count($schools) > 0 || count($class) > 0 || count($children) > 0}class="manage_box"{/if}>
    {*{if count($schools) > 0 || count($class) > 0 || count($children) > 0}*}
        {*<div align="center">*}
            {*<b style="color: #17730b">{__("Click on name to enter management section")}</b>*}
        {*</div>*}
    {*{/if}*}
    <!-- Start school menu -->
    {if count($schools) > 0}
        <li class="ptb5">
            <small class="text-muted"> {__("schools")|upper}</small>
        </li>
        {foreach $schools as $school}
            <li id="li-school" style="border: 1px solid #9c9c9cbd; border-radius: 5px; padding: 5px; margin-bottom: 5px">
                {if $school['is_teacher'] != 1}
                    <a href="{$system['system_url']}/school/{$school['page_name']}">
                        {*<img src="{$school['page_picture']}" alt="">*}
                        <img src="{$system['system_url']}/content/themes/{$system['theme']}/images/icons/ci/school_1.png">
                        <span>{$school['page_title']}</span>
                    </a>
                {else}
                    <a href="{$system['system_url']}/school/{$school['page_name']}/events">
                        {*<img src="{$school['page_picture']}" alt="">*}
                        <img src="{$system['system_url']}/content/themes/{$system['theme']}/images/icons/ci/school_1.png">
                        <span>{$school['page_title']}</span>
                    </a>
                {/if}
            </li>
        {/foreach}
    {/if}
    <!-- End school menu -->

    <!-- Start class menu -->
    {if count($classes) > 0}
        <li class="ptb5">
            <small class="text-muted"> {__("class")|upper}</small>
        </li>

        {foreach $classes as $class}
            {if $class['school_status'] == 1}
                <li style="border: 1px solid #9c9c9cbd; border-radius: 5px; padding: 5px; margin-bottom: 5px">
                    <a href="{$system['system_url']}/class/{$class['group_name']}">
                        {*<img src="{$class['group_picture']}" alt="">*}
                        <img src="{$system['system_url']}/content/themes/{$system['theme']}/images/icons/ci/class_1.png">
                        <span>{$class['group_title']}</span>
                    </a>
                </li>
            {/if}
        {/foreach}
    {/if}
    <!-- End class menu -->

    <!-- Start child menu -->
    {if count($children) > 0}
        <li class="ptb5">
            <small class="text-muted"> {__("student")|upper}</small>
        </li>
        <div class="panel-body with-nav collapse navbar-collapse" id="collapse">
            <ul class="side-nav metismenu">
                {foreach $children as $k => $child}
                    <li style="border: 1px solid #9c9c9cbd; border-radius: 5px; margin-bottom: 5px">
                        {*<a href="#" style="padding: 5px; background: none; border-bottom: none;">*}
                        <a href="{if $child['school_id'] != 0 && $child['school_status'] == 1}{$system['system_url']}/child/{$child['child_id']}{else}{$system['system_url']}/childinfo/{$child['child_parent_id']}/childdetail{/if}" style="padding: 5px; background: none; border-bottom: none;">
                            <img src="{$system['system_url']}/content/themes/{$system['theme']}/images/icons/ci/children_1.png">
                            {$child['child_name']}
                            {*<span class="fa arrow"></span>*}
                        </a>
                        {*<ul>*}
                        {*{if $child['school_id'] != 0}*}
                        {*<li>*}
                        {*<a href="{$system['system_url']}/child/{$child['child_id']}" style = "padding: 5px 15px; background: #E9EAEE; border-bottom: none; color: #235291;">*}
                        {*<i class="fa fa-caret-right fa-fw pr10"></i> {__("Manage")}*}
                        {*</a>*}

                        {*</li>*}
                        {*{/if}*}
                        {*<li>*}
                        {*<a href="{$system['system_url']}/childinfo/{$child['child_parent_id']}/childdetail" style = "padding: 5px 15px; background: #E9EAEE; color: #235291;">*}
                        {*<i class="fa fa-caret-right fa-fw pr10"></i> {__("Student information")}*}
                        {*</a>*}
                        {*</li>*}
                        {*</ul>*}
                    </li>
                {/foreach}
            </ul>
        </div>
    {/if}
    <!-- End child menu -->

    <!-- ADD NEW -MANHDD -->
    <!-- Start itself child menu -->
    {if count($itself) > 0}
        <li class="ptb5">
            <small class="text-muted"> {__("me")|upper}</small>
        </li>
        <div class="panel-body with-nav collapse navbar-collapse" id="collapse">
            <ul class="side-nav metismenu">

                    <li style="border: 1px solid #9c9c9cbd; border-radius: 5px; margin-bottom: 5px">
                        {*<a href="#" style="padding: 5px; background: none; border-bottom: none;">*}
                        <a href="{if $itself['school_id'] != 0 && $itself['school_status'] == 1}{$system['system_url']}/child/{$itself['child_id']}{else}{$system['system_url']}/childinfo/{$itself['child_parent_id']}/childdetail{/if}" style="padding: 5px; background: none; border-bottom: none;">
                            <img src="{$system['system_url']}/content/themes/{$system['theme']}/images/icons/ci/children_1.png">
                            {$itself['child_name']}
                            {*<span class="fa arrow"></span>*}
                        </a>
                        {*<ul>*}
                        {*{if $child['school_id'] != 0}*}
                        {*<li>*}
                        {*<a href="{$system['system_url']}/child/{$child['child_id']}" style = "padding: 5px 15px; background: #E9EAEE; border-bottom: none; color: #235291;">*}
                        {*<i class="fa fa-caret-right fa-fw pr10"></i> {__("Manage")}*}
                        {*</a>*}

                        {*</li>*}
                        {*{/if}*}
                        {*<li>*}
                        {*<a href="{$system['system_url']}/childinfo/{$child['child_parent_id']}/childdetail" style = "padding: 5px 15px; background: #E9EAEE; color: #235291;">*}
                        {*<i class="fa fa-caret-right fa-fw pr10"></i> {__("Student information")}*}
                        {*</a>*}
                        {*</li>*}
                        {*</ul>*}
                    </li>
            </ul>
        </div>
    {/if}
    <!-- End child menu -->
</div>
<!-- Start add child -->
<!-- UPDATE START - MANHDD -->
{if count($itself)==0 }
    <li class="ptb5">
        <a href = "{$system['system_url']}/addchild">
            <small class="text-muted"><i class="fa fa-plus" aria-hidden="true"></i> {__("Add new student")|upper}</small>
        </a>
    </li>
{/if}
<!-- UPDATE END - MANHDD -->
<!-- End add child -->



