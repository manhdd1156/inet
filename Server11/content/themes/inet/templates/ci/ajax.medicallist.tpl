<script type="text/javascript" src="{$system['cdn_url']}/includes/assets/js/chart/Chart.bundle.min.js"></script>
<script type="text/javascript" src="{$system['cdn_url']}/includes/assets/js/chart/Chart.bundle.js"></script>
<script type="text/javascript" src="{$system['cdn_url']}/includes/assets/js/chart/utils.js"></script>

<div id="medical_chart">
    <div class="content">
        <div class="wrapper">
            <canvas id="chart-medical"></canvas>
        </div>
    </div>
</div>
<script>
    function stateChange() {
        setTimeout(function () {
            var presets = window.chartColors;
            var utils = Samples.utils;
            var data_medical = {
                labels: [{$label}],
                datasets: [{
                    backgroundColor: presets.blue,
                    borderColor: presets.blue,
                    // borderWidth: 1,
                    data: [{$medicalsForChart}],
                    hidden: false,
                    label: __["Count illness"],
                    fill: false
                }]
            };

            var options = {
                maintainAspectRatio: false,
                spanGaps: true,
                elements: {
                    line: {
                        tension: 0.000001
                    },
                    point:{
                        radius: 0
                    }
                },
                scales: {
                    yAxes: [{
                        stacked: false,
                        ticks: {
                            min: 0, // it is for ignoring negative step.
                            beginAtZero: true,
                            stepSize: 1
                        }
                    }]
                },
                plugins: {
                    filler: {
                        propagate: false
                    },
                    samples_filler_analyser: {
                        target: 'chart-analyser'
                    }
                }
            };

            var chart_medical = new Chart('chart-medical', {
                type: 'bar',
                data: data_medical,
                options: options
            });
        }, 1000);
    }
    stateChange();
</script>
<br/>
<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover js_dataTable">
        <thead>
        <tr>
            <th colspan="6">{__("Medical report book information")}&nbsp;({$results|count})</th>
        </tr>
        <tr>
            <th>{__('No.')}</th>
            <th>{__("Diseased day")}</th>
            <th>{__('Diseased')}</th>
            <th>{__('Medicine list')}</th>
            <th>{__("Actions")}</th>
        </tr>
        </thead>
        <tbody>
        {foreach $results as $k => $row}
            <tr>
                <td align="center">{$k + 1}</td>
                <td align="center"><a href = "{$system['system_url']}/childinfo/{$child['child_parent_id']}/medicals/edit/{$row['child_medical_id']}">{$row['recorded_at']}</a></td>
                <td>{$row['diseased_name']}</td>
                <td>{$row['medicine_list']}</td>
                <td align="center">
                    <a href ="{$system['system_url']}/childinfo/{$child['child_parent_id']}/medicals/edit/{$row['child_medical_id']}" class = "btn btn-xs btn-default">
                        {__("Edit")}
                    </a>
                    <a class="btn btn-xs btn-danger js_child-medical-delete" data-child = "{$child['child_parent_id']}" data-id="{$row['child_medical_id']}">
                        {__("Delete")}
                    </a>
                </td>
            </tr>
        {/foreach}
        </tbody>
    </table>
</div>