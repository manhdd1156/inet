<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <div class="pull-right flip">
            {if $sub_view == "detail"}
                <a href="{$system['system_url']}/child/{$child['child_id']}/schedules" class="btn btn-default">
                    <i class="fa fa-list"></i> {__("Lists")}
                </a>
            {/if}
        </div>
        <i class="fas fa-calendar-alt fa-fw fa-lg pr10"></i>
        {__("Schedule")}
        {if $sub_view == ""}
            &rsaquo; {__("Schedule list")}
        {elseif $sub_view == "detail"}
            &rsaquo; {__('Detail schedule')} {$data['schedule_name']}
        {/if}
    </div>
    {if $sub_view == ""}
        <div class="panel-body with-table">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover js_dataTable">
                    <thead>
                    <tr><th colspan="4">{__("Schedule list")}&nbsp;({$rows|count})</th></tr>
                    <tr>
                        <th>
                            {__("#")}
                        </th>
                        <th>
                            {__("Schedule name")}
                        </th>
                        <th>
                            {__("Begin")}
                        </th>
                        <th>
                            {__("Scope")}
                        </th>
                        {*<th>*}
                            {*{__("Use category")}*}
                        {*</th>*}
                        {*<th>*}
                            {*{__("Study saturday")}*}
                        {*</th>*}
                    </tr>
                    </thead>
                    <tbody>
                    {$idx = 1}
                    {foreach $rows as $row}
                        <tr>
                            <td>
                                {$idx}
                            </td>
                            <td>
                                <a href="{$system['system_url']}/child/{$child['child_id']}/schedules/detail/{$row['schedule_id']}">{$row['schedule_name']}</a> {if !$row['use']}({__("Not use now")}){/if}
                            </td>
                            <td>
                                {$row['begin']}
                            </td>
                            <td>
                                {if $row['applied_for']==$smarty.const.SCHOOL_LEVEL}
                                    {__("School")}
                                {elseif $row['applied_for']==$smarty.const.CLASS_LEVEL_LEVEL}
                                    {foreach $class_levels as $cl}
                                        {if $cl['class_level_id'] == $row['class_level_id']}{$cl['class_level_name']}{/if}
                                    {/foreach}
                                {elseif $row['applied_for']==$smarty.const.CLASS_LEVEL}
                                    {foreach $classes as $value}
                                        {if $value['group_id'] == $row['class_id']}{$value['group_title']}{/if}
                                    {/foreach}
                                {/if}
                            </td>
                            {*<td>*}
                                {*{if $row['is_category']}*}
                                    {*{__('Yes')}*}
                                {*{else}*}
                                    {*{__('No')}*}
                                {*{/if}*}
                            {*</td>*}
                            {*<td>*}
                                {*{if $row['is_saturday']}*}
                                    {*{__('Yes')}*}
                                {*{else}*}
                                    {*{__('No')}*}
                                {*{/if}*}
                            {*</td>*}
                        </tr>
                        {$idx = $idx + 1}
                    {/foreach}
                    </tbody>
                </table>
            </div>
        </div>
    {elseif $sub_view == "detail"}
        <div class="panel-body with-table">
            <table class = "table table-bordered">
                <tbody>
                <tr>
                    <td class = "col-sm-3 text-right"><strong>{__('Schedule name')}</strong></td>
                    <td>
                        {$data['schedule_name']}
                    </td>
                </tr>
                {*<tr>*}
                    {*<td class = "col-sm-3 text-right">{__('Scope')}</td>*}
                    {*<td>*}
                        {*{if isset($classes)} {$classes['group_title']}*}
                        {*{elseif isset($class_level)}{$class_level['class_level_name']}*}
                        {*{elseif !isset($classes) && !isset($class_level)} {__('School')}*}
                        {*{/if}*}
                    {*</td>*}
                {*</tr>*}
                <tr>
                    <td class = "col-sm-3 text-right"><strong>{__('Begin')}</strong></td>
                    <td>
                        {$data['begin']}
                    </td>
                </tr>
                {*<tr>*}
                    {*<td class = "col-sm-3 text-right">{__('Use category')}</td>*}
                    {*<td>*}
                        {*{if $data['is_category']}*}
                            {*{__('Yes')}*}
                        {*{else}*}
                            {*{__('No')}*}
                        {*{/if}*}
                    {*</td>*}
                {*</tr>*}
                {*<tr>*}
                    {*<td class = "col-sm-3 text-right">{__('Study saturday')}</td>*}
                    {*<td>*}
                        {*{if $data['is_saturday']}*}
                            {*{__('Yes')}*}
                        {*{else}*}
                            {*{__('No')}*}
                        {*{/if}*}
                    {*</td>*}
                {*</tr>*}
                {if $data['description'] != ''}
                    <tr>
                        <td class = "col-sm-3 text-right"><strong>{__('Description')}</strong></td>
                        <td>
                            {$data['description']}
                        </td>
                    </tr>
                {/if}
                </tbody>
            </table>
            <div class = "table-responsive">
                <table class = "table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th> <center>{__('#')}</center> </th>
                        <th> <center>{__('Time')}</center> </th>
                        <th {if !$data['is_category']}class="hidden"{/if}> <center>{__('Activity')}</center> </th>
                        <th> <center>{__('Monday')}</center> </th>
                        <th> <center>{__('Tuesday')}</center> </th>
                        <th> <center>{__('Wednesday')}</center> </th>
                        <th> <center>{__('Thursday')}</center> </th>
                        <th> <center>{__('Friday')}</center> </th>
                        <th {if !$data['is_saturday']}class="hidden"{/if}> <center>{__('Saturday')}</center> </th>
                    </tr>
                    </thead>
                    <tbody>
                    {$idx = 1}
                    {foreach $data['details'] as $k => $row}
                        {$array = array_values($row)}
                        {$temp = array()}
                        {foreach $array as $k => $value}
                            {if $data['is_saturday']}
                                {if ($k >= 4) && ($k < (count($array)))}
                                    {$temp[] = $value}
                                {/if}
                            {else}
                                {if ($k >= 4) && ($k < (count($array) - 1))}
                                    {$temp[] = $value}
                                {/if}
                            {/if}
                        {/foreach}
                        <tr>
                            <td style="vertical-align: middle" align="center">
                                <strong>{$idx}</strong>
                            </td>
                            <td style="vertical-align: middle" align="center">
                                <strong>{$row['subject_time']}</strong>
                            </td>
                            <td {if !$data['is_category']}class="hidden"{/if} align="center">
                                <strong>{$row['subject_name']}</strong>
                            </td>
                            {$col = 1}
                            {for $i = 0; $i < count($temp); $i++}
                                {if $temp[$i] === $temp[($i+1)]}
                                    {$col = $col + 1}
                                {else}
                                    <td colspan = "{$col}">
                                        <center>{nl2br($temp[$i])}</center>
                                    </td>
                                    {$col = 1}
                                {/if}
                            {/for}
                        </tr>
                        {$idx = $idx + 1}
                    {/foreach}
                    </tbody>
                </table>
            </div>
        </div>
    {/if}
</div>