<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <div class="pull-right flip">
{*            <a href="https://blog.coniu.vn/huong-dan-phu-huynh-tao-thong-tin-nguoi-don-tre-tren-website-inet/" class="btn btn-info btn_guide" target="_blank">*}
            <a href="#" class="btn btn-info btn_guide" target="_blank">
                <i class="fa fa-info-circle" aria-hidden="true"></i> {__("Guide")}
            </a>
            {if $sub_view == "add" || $sub_view == "edit"}
                <a href="{$system['system_url']}/child/{$child['child_id']}/informations" class="btn btn-default">
                    <i class="fa fa-list"></i> {__("Lists")}
                </a>
            {elseif $sub_view == ""}
                {if count($rows) < 4}
                    <a href="{$system['system_url']}/child/{$child['child_id']}/informations/add" class="btn btn-default">
                        <i class="fa fa-plus"></i> {__("Add New")}
                    </a>
                {/if}
            {/if}
        </div>
        <i class="fa fa-info-circle fa-lg fa-fw pr10"></i>
        {__("Student information")}
        {if $sub_view == ""}
            &rsaquo; {__("Lists")}
        {elseif $sub_view == "add"}
            &rsaquo; {__('Add New')}
        {elseif $sub_view == "edit"}
            &rsaquo; {__('Edit')}
        {/if}
    </div>
    {if $sub_view == ""}
        <div class="panel-body with-table">
            <div class="panel-body with-table">
                <div class="form-group">
                    <strong>{__("Picker lists")}</strong>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <tbody>
                            {foreach $rows as $row}
                                <tr>
                                    <td style="width: 60%">
                                        {if !is_empty($row['picker_source_file'])}
                                            <a href="{$row['picker_source_file']}" target="_blank"><img src = "{$row['picker_source_file']}" style="width: 100%" class = "img-responsive"></a>
                                        {else}
                                            {__("No information")}
                                        {/if}
                                    </td>
                                    <td style="width: 40%">
                                        <strong>{__("Picker name")}:</strong> {$row['picker_name']} <br/>
                                        <strong>{__("Relation with student")}:</strong> {$row['picker_relation']} <br/>
                                        <strong>{__("Telephone")}:</strong> {$row['picker_phone']} <br/>
                                        <strong>{__("Address")}:</strong> {$row['picker_address']} <br/>
                                        <strong>{__("Creator")}:</strong> {$row['user_fullname']} <br/><br/>
                                        <a href="{$system['system_url']}/child/{$child['child_id']}/informations/edit/{$row['child_info_id']}" class="btn btn-primary btn-xs">{__("Edit")}</a> <a href="#" class="btn btn-danger btn-xs js_child-picker-delete" data-child="{$child['child_id']}" data-id="{$row['child_info_id']}">{__("Delete")}</a>
                                    </td>
                                </tr>
                            {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    {elseif $sub_view == "add"}
        <div class="panel-body">
            <form class="form-horizontal" enctype="multipart/form-data" method="post" id="edit_information">
                <input type="hidden" name="child_id" value="{$child['child_id']}"/>
                <input type="hidden" name="do" value="add_info"/>

                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Picker name")} (*)</label>
                    <div class="col-sm-9">
                        <input class="form-control" name="picker_name" maxlength="100">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Relation with student")} (*)</label>
                    <div class="col-sm-9">
                        <input class="form-control" name="picker_relation" required maxlength="100">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Telephone")} (*)</label>
                    <div class="col-sm-3">
                        <input class="form-control" type="text" name="phone" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Address")} (*)</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="address" rows="6" required></textarea>
                    </div>
                </div>


                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("File attachment")}</label>
                    <div class="col-sm-6">
                        <input type="file" name="file" id="file"/>
                    </div>
                </div>
                {*<div class="medicine-image">*}
                {*<i class="fa fa-camera js_x-uploader" data-handle="medicine-attach"></i>*}
                {*</div>*}
                <div class="form-group">
                    <div class="col-sm-3 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    {elseif $sub_view == "edit"}
        <div class="panel-body">
            <form class="form-horizontal" enctype="multipart/form-data" method="post" id="edit_information">
                <input type="hidden" name="child_info_id" value="{$rows['child_info_id']}"/>
                <input type="hidden" name="child_id" value="{$child['child_id']}"/>
                <input type="hidden" name="do" value="edit_info"/>

                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Picker name")} (*)</label>
                    <div class="col-sm-9">
                        <input class="form-control" name="picker_name" maxlength="100" value="{$rows['picker_name']}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Relation with student")} (*)</label>
                    <div class="col-sm-9">
                        <input class="form-control" name="picker_relation" required maxlength="100" value="{$rows['picker_relation']}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Telephone")} (*)</label>
                    <div class="col-sm-3">
                        <input class="form-control" type="text" name="phone" value="{$rows['picker_phone']}" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Address")} (*)</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="address" rows="6" required>{$rows['picker_address']}</textarea>
                    </div>
                </div>


                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("File attachment")}</label>
                    <div class="col-sm-6">
                        {if !is_empty($rows['picker_source_file'])}
                            <a href="{$rows['picker_source_file']}" target="_blank"><img src = "{$rows['picker_source_file']}" class = "img-responsive"></a>
                            <br>
                            <label class="control-label">{__("Choose file replace")}</label>
                            <br>
                        {/if}
                        <input type="file" name="file" id="file"/>
                    </div>
                </div>
                {*<div class="medicine-image">*}
                {*<i class="fa fa-camera js_x-uploader" data-handle="medicine-attach"></i>*}
                {*</div>*}
                <div class="form-group">
                    <div class="col-sm-3 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    {/if}
</div>