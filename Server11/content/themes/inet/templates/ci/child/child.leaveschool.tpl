<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <div class="pull-right flip">
{*            <a href="https://blog.coniu.vn/huong-dan-phu-huynh-tu-thuc-hien-thoi-hoc-cho-tre-tren-website-inet/" class="btn btn-info btn_guide" target="_blank">*}
            <a href="#" class="btn btn-info btn_guide" target="_blank">
                <i class="fa fa-info-circle" aria-hidden="true"></i> {__("Guide")}
            </a>
        </div>
        <i class="fa fa-minus-circle fa-lg fa-fw pr10"></i>
        {__("Leave school")}
    </div>
    <div class = "panel-body with-table">
        <div class="panel-body">
            <div class="alert alert-warning">
                {__("After leaving school , you can not access to your student's management module.")}<br>
                {__("Note: All the data of your student will be deleted")}
            </div>
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/child/bochild_all.php">
                <input type="hidden" name="do" value="leave_school"/>
                <input type="hidden" name="child_id" value="{$child['child_id']}"/>
                <div class="form-group">
                    <label class="col-sm-4 control-label text-left">
                        {__("Password")}
                    </label>
                    <div class="col-sm-5">
                        <input type="password" class="form-control" name="user_password" required>
                    </div>
                </div>

                <div class="text-center">
                    <button type="submit" class="btn btn-danger">{__("Leave school")}</button>
                </div>
            </form>

        </div>
    </div>
</div>