<div class="panel panel-default">
    <div class="panel-heading with-icon">
        {if $sub_view == "resign"}
            <div class="pull-right flip">
{*                <a href="https://blog.coniu.vn/huong-dan-phu-huynh-xin-nghi-hoc-cho-tre-tren-website-inet/" class="btn btn-info btn_guide" target="_blank">*}
                <a href="#" class="btn btn-info btn_guide" target="_blank">
                    <i class="fa fa-info-circle" aria-hidden="true"></i> {__("Guide")}
                </a>
                <a href="{$system['system_url']}/child/{$child['child_id']}/attendance" class="btn btn-default">
                    {__("Attendance information")}
                </a>
            </div>
        {/if}
        {if $sub_view == ""}
            <div class="pull-right flip">
{*                <a href="https://blog.coniu.vn/huong-dan-xin-nghi-hoc-cho-tre-tai-khoan-phu-huynh-tren-ung-dung-mam-non-inet/" class="btn btn-info btn_guide" target="_blank">*}
                <a href="#" class="btn btn-info btn_guide" target="_blank">
                    <i class="fa fa-info-circle" aria-hidden="true"></i> {__("Guide")}
                </a>
                <a href="{$system['system_url']}/child/{$child['child_id']}/attendance/resign" class="btn btn-default">
                    <i class="fa fa-plus"></i> {__("Resign")}
                </a>
            </div>
        {/if}

        <i class="fa fa-tasks fa-fw fa-lg pr10"></i>
        {__("Resign")}
        {if $sub_view == "search"}
            &rsaquo; {__("Search")}
        {elseif $sub_view == ""}
            &rsaquo; {__("Attendance information")}
        {/if}
    </div>
    {if $sub_view == ""}
        <div class="panel-body with-table">
            <div class="row form-horizontal">
                <div class="form-group">
                    <label class="col-sm-2 control-label text-left">{__("Time")}</label>
                    <div class="col-sm-10">
                        <div class='col-sm-4'>
                            <div class='input-group date' id='fromdate_picker'>
                                <input type='text' name="fromDate" id="fromDate" value="{$data['fromDate']}" class="form-control" placeholder="{__("From date")}"/>
                                <span class="input-group-addon">
                                    <span class="fas fa-calendar-alt"></span>
                                </span>
                            </div>
                        </div>
                        <div class='col-sm-1 text-center'><i class="fas fa-long-arrow-alt-right fa-lg fa-fw pr10 pt10"></i></div>
                        <div class='col-sm-4'>
                            <div class='input-group date' id='todate_picker'>
                                <input type='text' name="toDate" id="toDate" value="{$data['toDate']}" class="form-control" placeholder="{__("To date")}"/>
                                <span class="input-group-addon">
                                    <span class="fas fa-calendar-alt"></span>
                                </span>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <a class="btn btn-default js_child-attendance" data-id="{$child['child_id']}">{__("Search")}</a>
                        </div>
                    </div>
                </div>
            </div><br/>
            <div class="table-responsive" id="attendance_list">
                {include file="ci/child/ajax.child.attendance.tpl"}
            </div>
        </div>
    {elseif $sub_view == "resign"}
        <div class="panel-body">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/child/bochild_all.php">
                <input type="hidden" name="child_id" value="{$child['child_id']}"/>
                <input type="hidden" name="do" value="resign"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Date")}</label>
                    <div class='col-sm-3'>
                        <div class='input-group date' id='medicine_beginpicker_new'>
                            <input type='text' name="start_date" class="form-control" placeholder="{__("Begin")} (*)" required/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                    <div class='col-sm-1 text-center'><i class="fas fa-long-arrow-alt-right fa-lg fa-fw pr10 pt10"></i></div>
                    <div class='col-sm-3'>
                        <div class='input-group date' id='medicine_endpicker_new'>
                            <input type='text' name="end_date" class="form-control" placeholder="{__("End")} (*)" required/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>

                    {*<div class='col-sm-4'>
                        <div class='input-group date' id='attendance_pk'>
                            <input type='text' name="attendance_date" id="attendance_date" class="form-control" placeholder="{__("Date")} (*)" required/>
                                <span class="input-group-addon">
                                    <span class="fa fa-calendar"></span>
                                </span>
                        </div>
                    </div>*}
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Absent reason")}</label>
                    <div class='col-sm-8'>
                        <input type="text" class="form-control" name="reason" maxlength="512" placeholder="{__("Absent reason")}">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-3">

                    </div>
                    <div class="col-sm-9">
                        <button type="submit" class="btn btn-default">{__("Resign")}</button>
                    </div>
                </div>
                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    {/if}
</div>