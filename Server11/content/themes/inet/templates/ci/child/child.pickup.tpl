<div class="panel panel-default">
    <div class="panel-heading with-icon">
        {if $sub_view == ""}
            <div class="pull-right flip">
                <a href="{$system['system_url']}/child/{$child['child_id']}/pickup/list" class="btn btn-default">
                    <i class="fa fa-list"></i>  {__("Lists")}
                </a>
            </div>
        {/if}
        {*{if $sub_view == "list"}
            <div class="pull-right flip">
                <a href="{$system['system_url']}/child/{$child['child_id']}/pickup" class="btn btn-default">
                    <i class="fa fa-check-square"></i>  {__("Register")}
                </a>
            </div>
        {/if}*}

        <i class="fa fa-universal-access fa-fw fa-lg pr10"></i>
        {__("Late pickup")}
        {if $sub_view == ""}
            &rsaquo; {__("Information")}
        {elseif $sub_view == "list"}
            &rsaquo; {__("Lists")}
        {/if}
    </div>
    {if $sub_view == ""}
        <div class="panel-body">
            <div class="form-horizontal" style="font-size: 14px">

                <div class="form-group mb0">
                    <div class="text-center">
                        <label style="font-size: 15px">{__("THÔNG TIN DỊCH VỤ ĐÓN MUỘN")}<br><br>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">
                        {__("Pickup beginning time")}
                    </label>
                    <div class="col-sm-2">
                        <input class="form-control text-center" style="background-color: #FFFFFF"
                               value="{if !empty($template['price_list'][0]['beginning_time'])} {$template['price_list'][0]['beginning_time']} {else} - {/if}" readonly/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">
                        {__("Bảng giá")}
                    </label>
                    <div class="col-sm-9">

                        <table class="table table-bordered table-hover mb20" id="pickup_price_list">
                            <thead>
                            <tr>
                                <th class="col-sm-3"><center>{__("Pickup before")}</center></th>
                                <th class="col-sm-3"><center>{__("Unit price")}</center></th>
                            </tr>
                            </thead>
                            <tbody>

                            {if isset($template['price_list']) && count($template['price_list'] > 0)}
                                {foreach $template['price_list'] as $value}
                                    <tr>
                                        <td align="center">{$value['ending_time']}</td>
                                        <td align="center">{number_format($value['unit_price'],0,'.',',')}</td>
                                    </tr>
                                {/foreach}
                            {/if}
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">
                        {__("Services")}
                    </label>
                    <div class="col-sm-9">

                        <table class="table table-bordered table-hover mb0" id="pickup_service">
                            <thead>
                            <tr>
                                <th class="col-sm-2"><center>{__("Service name")}</center></th>
                                <th class="col-sm-2"><center>{__("Unit price")}</center></th>
                                <th class="col-sm-2"><center>{__("Description")}</center></th>
                            </tr>
                            </thead>
                            <tbody>
                            {if isset($template['services']) && count($template['services'] > 0)}
                                {foreach $template['services'] as $service}
                                    <tr>
                                        <td align="center">{$service['service_name']}</td>
                                        <td align="center">{number_format($service['fee'],0,'.',',')}</td>
                                        <td align="center">{$service['description']}</td>
                                    </tr>
                                {/foreach}
                            {/if}
                            </tbody>
                        </table>

                    </div>
                </div>

            </div>
        </div>
    {elseif $sub_view == "list"}
        <div class="panel-body with-table">
            <div class="row">
                <div class='col-sm-12'>
                    <div class='col-sm-2'></div>
                    <div class='col-sm-3'>
                        <div class='input-group date' id='fromDate_pickup'>
                            <input type='text' name="fromDate" id="fromDate" value="{$fromDate}" class="form-control" placeholder="{__("From date")}"/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                    <div class='col-sm-3'>
                        <div class='input-group date' id='toDate_pickup'>
                            <input type='text' name="toDate" id="toDate" value="{$toDate}" class="form-control" placeholder="{__("To date")}"/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                    <div class='col-sm-3'>
                        <div class="form-group">
                            <button class="btn btn-default js_pickup-search" data-child="{$child['child_id']}" >{__("Search")}</button>
                        </div>
                    </div>
                </div>
            </div>
            <br/>

            <div class="table-responsive" id="pickup_list_child">
                {include file="ci/child/ajax.child.pickup.tpl"}
            </div>
        </div>
    {/if}
</div>