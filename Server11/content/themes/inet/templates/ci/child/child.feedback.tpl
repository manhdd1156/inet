<div class="panel panel-default">

    <div class="panel-heading with-icon">
        <div class="pull-right flip">
{*            <a href="https://blog.coniu.vn/huong-dan-phu-huynh-thao-tac-gop-y-nha-truong-tren-webiste-inet/" class="btn btn-info btn_guide">*}
            <a href="#" class="btn btn-info btn_guide">
                <i class="fa fa-info"></i> {__("Guide")}
            </a>
            {if $sub_view == ""}
                <a href="{$system['system_url']}/child/{$child['child_id']}/feedback/list" class="btn btn-default">
                    <i class="fa fa-list"></i> {__("Feedback list")}
                </a>
            {/if}
        </div>
        <i class="fa fa-envelope fa-lg fa-fw pr10"></i>
        {__("Feedback for the school")}
        {if $sub_view == ""}
            &rsaquo; {__('Add New')}
        {elseif $sub_view == "list"}
            &rsaquo; {__('Lists')}
        {/if}
    </div>

    {if $sub_view == ""}
        <div class="panel-body">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/child/bochild_all.php">
                <input type="hidden" name="child_id" value="{$child['child_id']}"/>
                <input type="hidden" name="feedback_level" value="{$smarty.const.SCHOOL_LEVEL}"/>
                <input type="hidden" name="do" value="feedback"/>

                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Send to")}: </label>
                    <div class="col-sm-9">
                        <label class="control-label text-left">{$school['page_title']}</label>
                        {*<div class="col-xs-4">
                            <select name="feedback_level" id="feedback_level" class="form-control">
                                <option value="{$smarty.const.SCHOOL_LEVEL}">{__("School")}</option>
                                <option value="{$smarty.const.CLASS_LEVEL}">{__("Class")}</option>
                            </select>
                        </div>*}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Hiển thị thông tin người gửi")}?</label>
                    <div class="col-sm-9">
                        <div class="onoffswitch">
                            <input type="checkbox" name="incognito" class="onoffswitch-checkbox"
                                   id="incognito" checked>
                            <label class="onoffswitch-label" for="incognito"></label>
                        </div>
                        <span class="help-block">
                        {__("Hiển thị thông tin người gửi? (Tắt là ẩn danh)")}
                    </span>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Content")} (*)</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="content" rows="6" required></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    {elseif $sub_view == "list"}
        <div class="panel-body with-table">
            <div><strong>{__("Feedback list")}&nbsp;({$rows|count})</strong></div>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>{__("Feedback for")}</th>
                        <th>{__("Time")}</th>
                        <th>{__("Content")}</th>
                    </tr>
                    </thead>
                    <tbody>
                        {$idx = 1}
                        {foreach $rows as $row}
                            <tr>
                                <td align="center" style="vertical-align:middle">{$idx}</td>
                                <td align="center">
                                    {if $row['level']==$smarty.const.SCHOOL_LEVEL}
                                        {__("School")}
                                    {elseif $row['level']==$smarty.const.CLASS_LEVEL}
                                        {__("Class")}
                                    {/if}
                                    {if $row['confirm']}
                                        <i class="fa fa-check" aria-hidden="true"></i>
                                    {else}
                                        <i class="fa fa-times" aria-hidden="true"></i>
                                    {/if}
                                </td>
                                <td align="center">{$row['created_at']}</td>
                                <td>{$row['content']}</td>
                            </tr>

                            {$idx = $idx + 1}
                        {/foreach}

                        {if $idx == 1}
                            <tr class="odd">
                                <td valign="top" align="center" colspan="4" class="dataTables_empty">
                                    {__("No data available in table")}
                                </td>
                            </tr>
                        {/if}
                    </tbody>
                </table>
            </div>
        </div>
    {/if}

</div>