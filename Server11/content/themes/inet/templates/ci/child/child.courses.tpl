<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <i class="fa fa-graduation-cap fa-fw fa-lg pr10"></i>
        {__("Courses")}
        {if $sub_view == ""}
            &rsaquo; {__("Lists")}
        {/if}
    </div>
    {if $sub_view == ""}
        <div class="panel-body with-table">
            <form class="form-horizontal" action="#" enctype="multipart/form-data" method="post"
                  id="course_search_form">
                <input type="hidden" name="do" value="search_course"/>
                <div class = "table-responsive" id ="course_list">
                    {include file="ci/child/ajax.courselist.tpl"}
                </div>

                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
        </div>
    {/if}
</div>
