<div><strong>{__("Attendance")}&nbsp;({__("Total")}: {$data['attendance']['attendance']|count}&nbsp;|&nbsp;{__("Present")}: {$data['attendance']['present_count']}&nbsp;|&nbsp;{__("Absence")}: {$data['attendance']['absence_count']})</strong></div>
<table class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th>#</th>
        <th>{__("Attendance date")}</th>
        <th>{__("Status")}</th>
        <th>{__("Teacher")}</th>
    </tr>
    </thead>
    <tbody>
    {$idx = 1}
    {foreach $data['attendance']['attendance'] as $row}
        <tr>
            <td align="center">{$idx}</td>
            <td align="center">{$row['attendance_date']}</td>
            <td align="center">
                {if $row['status'] == $smarty.const.ATTENDANCE_PRESENT}
                    {__("Present")}
                {elseif $row['status'] == $smarty.const.ATTENDANCE_COME_LATE}
                    {__("Come late")}
                {elseif $row['status'] == $smarty.const.ATTENDANCE_EARLY_LEAVE}
                    {__("Leave early")}
                {elseif $row['status'] == $smarty.const.ATTENDANCE_ABSENCE_NO_REASON}
                    {__("Without permission")}
                {else}
                    <strong>{__("With permission")}</strong>&nbsp;(
                    {if (!isset($row['reason']) || ($row['reason'] == ""))}
                        {__("No reason")}
                    {else}
                        {$row['reason']}
                    {/if})
                {/if}
            </td>
            <td>{$row['user_fullname']}</td>
        </tr>
        {$idx = $idx + 1}
    {/foreach}
    </tbody>
</table>