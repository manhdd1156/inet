<div class="panel-body with-table">
    <table class="table table-bordered">
        <tr>
            <td class="col-sm-3 text-right">{__("Month")}</td>
            <td>{$data['month']}</td>
        </tr>
        <tr>
            <td class="col-sm-3 text-right">{__("Child usage total")}&nbsp;({$smarty.const.MONEY_UNIT})</td>
            <td>{moneyFormat($data['child_total'])}</td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <a class="btn btn-default" href="{$system['system_url']}/child/{$child['child_id']}/tuitions/detail/{$data['tuition_id']}">{__("Tuition detail")}</a>
            </td>
        </tr>
    </table>
    <div class="table-responsive">
        {if (count($data['tuition_detail']['fees']) + count($data['tuition_detail']['services'])) > 0}
            <table class="table table-striped table-bordered table-hover bg_white">
                <thead>
                <tr>
                    <td colspan="6"><strong>{__("Fee usage history of previous month")} {$data['pre_month']}</strong>
                    </td>
                </tr>
                <tr>
                    <th>#</th>
                    <th>{__("Fee name")}</th>
                    <th class="text-center">{__("Unit")}</th>
                    <th class="text-center">{__("Quantity")}</th>
                    <th class="text-right">{__("Unit price")}&nbsp;({$smarty.const.MONEY_UNIT})</th>
                    <th class="text-right">{__("Money amount")}&nbsp;({$smarty.const.MONEY_UNIT})</th>
                </tr>
                </thead>
                <tbody>
                {*Hiển thị danh sách phí của trẻ*}
                {$idx = 1}
                {foreach $data['tuition_detail']['fees'] as $detail}
                    {if $detail['to_money'] > 0}
                        <tr>
                            <td align="center">{$idx}</td>
                            <td>{$detail['fee_name']}</td>
                            <td class="text-center">{if $detail['fee_type'] == $smarty.const.FEE_TYPE_MONTHLY}{__('Monthly')}{else}{__('Daily')}{/if}</td>
                            <td class="text-center">{$detail['quantity']}</td>
                            <td class="text-right">{moneyFormat($detail['unit_price'])}</td>
                            <td class="text-right">{moneyFormat($detail['to_money'])}</td>
                        </tr>
                        {$idx = $idx + 1}
                    {/if}
                {/foreach}
                {*Hiển thị danh sách dịch vụ theo THÁNG và ĐIỂM DANH*}
                {if count($data['tuition_detail']['services']) > 0}
                    <tr><td colspan="6"><strong>{__("Service")}</strong></td></tr>
                    {$idx = 1}
                    {foreach $data['tuition_detail']['services'] as $detail}
                        {if $detail['to_money'] > 0}
                            <tr>
                                <td align="center">{$idx}</td>
                                <td>{$detail['service_name']}</td>
                                <td class="text-center">{if $detail['service_type'] == $smarty.const.SERVICE_TYPE_MONTHLY}{__('Monthly')}{else}{__('Daily')}{/if}</td>
                                <td class="text-center">{$detail['quantity']}</td>
                                <td class="text-right">{moneyFormat($detail['unit_price'])}</td>
                                <td class="text-right">{moneyFormat($detail['to_money'])}</td>
                            </tr>
                            {$idx = $idx + 1}
                        {/if}
                    {/foreach}
                {/if}
                {*Hiển thị danh sách dịch vụ theo SỐ LẦN*}
                {if count($data['tuition_detail']['count_based_services']) > 0}
                    <tr><td colspan="6"><strong>{__("Count-based service of previous month")} {$child['pre_month']}</strong></td></tr>
                    {$idx = 1}
                    {foreach $data['tuition_detail']['count_based_services'] as $detail}
                        {if $detail['to_money'] > 0}
                            <tr>
                                <td align="center">{$idx}</td>
                                <td>{$detail['service_name']}</td>
                                <td class="text-center">{__('Times')}</td>
                                <td class="text-center">{$detail['quantity']}</td>
                                <td class="text-right">{moneyFormat($detail['unit_price'])}</td>
                                <td class="text-right">{moneyFormat($detail['to_money'])}</td>
                            </tr>
                            {$idx = $idx + 1}
                        {/if}
                    {/foreach}
                {/if}
                </tbody>
            </table>
        {/if}
    </div>
</div>
