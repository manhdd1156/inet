<div class="panel panel-default">
    <div class="panel-heading with-icon">
        {if $sub_view == "detail"}
            <div class="pull-right flip">
                <a href="{$system['system_url']}/child/{$child['child_id']}/reports" class="btn btn-default">
                    {__("Lists")}
                </a>
            </div>
        {/if}
        <i class="fa fa-book fa-lg fa-fw pr10"></i>
        {__("Contact book")}
        {if $sub_view == ""}
            &rsaquo; {__('Lists')}
        {elseif $sub_view == "detail"}
            &rsaquo; {__('Detail')}
        {/if}
    </div>
    {if $sub_view == ""}
        <div class="panel-body with-table">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover js_dataTable">
                    <thead>
                    <tr><th colspan="3">{__("Contact book list")}&nbsp;({$rows|count})</th></tr>
                    <tr>
                        <th>{__("#")}</th>
                        <th>{__("Title")}</th>
                        <th>{__("Date")}</th>
                    </tr>
                    </thead>
                    <tbody>
                    {$idx = 1}
                    {foreach $rows as $row}
                        <tr>
                            <td>
                                <div>
                                    <center>{$idx}</center>
                                </div>
                            </td>
                            <td>
                                <a href="{$system['system_url']}/child/{$child['child_id']}/reports/detail/{$row['report_id']}">{$row['report_name']}</a>
                            </td>
                            <td>
                                <center>{$row['date']}</center>
                            </td>
                        </tr>
                        {$idx = $idx + 1}
                    {/foreach}
                    </tbody>
                </table>
            </div>
        </div>
    {elseif $sub_view == "detail"}
        <div class="panel-body with-table">
            <table class = "table table-bordered">
                <tbody>
                <tr>
                    <td class = "col-sm-3 text-right">{__('Title')}</td>
                    <td>
                        <strong>{$data['report_name']}</strong>
                    </td>
                </tr>
                <tr>
                    <td class = "col-sm-3 text-right">{__('Child')}</td>
                    <td>
                        <strong>{$child['child_name']} - {$child['birthday']}</strong>
                    </td>
                </tr>
                <tr>
                    <td class = "col-sm-3 text-right">{__("File attachment")}</td>
                    <td>
                        {if !is_empty($data['source_file'])}
                            <a href = "{$data['source_file']}" target="_blank"><strong>
                                    {__("File attachment")}
                                </strong>
                            </a>
                        {else}
                            {__("No file attachment")}
                        {/if}
                    </td>
                </tr>
                </tbody>
            </table>
            <div class = "table-responsive">
                <table class = "table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th> # </th>
                        <th> {__('Title')} </th>
                        <th>{__('Content')} </th>
                    </tr>
                    </thead>
                    <tbody>
                    {$idx = 1}
                    {foreach $data['details'] as $k => $row}
                        {if $row['report_category_content'] != "" || !empty($row['multi_content'])}
                            <tr>
                                <td align="center">
                                    <strong>{$idx}</strong>
                                </td>
                                <td>
                                    <strong>{$row['report_category_name']}</strong>
                                </td>
                                <td>
                                    <div class="mb10">
                                        {$row['report_category_content']}
                                    </div>
                                    {foreach $row['multi_content'] as $suggest}
                                        <div class="form-group">
                                            <i class="fa fa-check" aria-hidden="true"></i> {$suggest}
                                        </div>
                                    {/foreach}
                                </td>
                            </tr>
                            {$idx = $idx + 1}
                        {/if}
                    {/foreach}
                    </tbody>
                </table>
            </div>
            {if $point_module ==1}
                <div style="{if $score_fomula!='vn'}width: 75%{/if};margin-left: auto;margin-right: auto;overflow-x:auto;">
                    <strong style="float: right">{__("Status ")} :
                        {if $status == 'Pass' }
                            <strong style="color:lawngreen">{__({$status})}</strong>
                        {elseif $status == 'Fail'}
                            <strong style="color:red">{__({$status})}</strong>
                        {elseif $status == 'Re-exam'}
                            <strong style="color:orange">{__({$status})}</strong>
                        {else}
                            <strong>{__({$status})}</strong>
                        {/if}

                    </strong>
                    <table class="table table-striped table-bordered" style="z-index: 1;">
                        <thead>
                        <tr bgcolor="#fff">
                            <th rowspan="2" class="pinned" style="padding: 8px 6px" id="width_no">#</th>
                            <th rowspan="2" nowrap="true" class="pinned">{__("Subject name")}</th>
                            <th colspan="{if $score_fomula=='vn'}{$column_hk2+$column_gk2+2}{else}4{/if}">{__("Semester 1")}</th>
                            <th colspan="{if $score_fomula=='vn'}{$column_hk2+$column_gk2+2}{else}4{/if}">{__("Semester 2")}</th>
                        </tr>
                        <tr>
                            {for $i=1;$i<=$column_hk1;$i++}
                                <th>{if $score_fomula=='vn'}tx{else}M{$i}{/if}</th>
                            {/for}
                            {for $i=0;$i<$column_gk1;$i++}
                                <th>{if $score_fomula=='vn'}gk{else}D1{/if}</th>
                            {/for}
                            {if $score_fomula=='vn'}
                                <th>ck</th>
                                <th>{__("Average")}</th>
                            {/if}
                            {for $i=1;$i<=$column_hk2;$i++}
                                <th>{if $score_fomula=='vn'}tx{else}M{$i}{/if}</th>
                            {/for}
                            {for $i=0;$i<$column_gk2;$i++}
                                <th>{if $score_fomula=='vn'}gk{else}D2{/if}</th>
                            {/for}
                            {if $score_fomula=='vn'}
                                <th>ck</th>
                                <th>{__("Average")}</th>
                                <th>Reexam</th>
                            {/if}
                        </tr>
                        </thead>
                        <tbody>
                        {$rowIdx = 1}
                        {foreach $rows as $k => $row}
                            <tr>
                                <td align="center" class="pinned">{$rowIdx}</td>

                                <td nowrap="true" class="pinned text-bold color-blue">
                                    <strong>{$row['subject_name']}</strong>
                                </td>
                                {$rowIdx = $rowIdx + 1}
                                {foreach $subject_key as $key}
                                    <td>{$row[strtolower($key)]}</td>
                                {/foreach}
                            </tr>
                        {/foreach}
                        {*                    Các điểm trung bình*}
                        {if $score_fomula == 'vn'}
                        <tr>
                            <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                                <strong>{__("End Semester")}</strong>
                            </td>
                            <td colspan="{$column_hk1+$column_gk1+1}"></td>
                            <td>{number_format($tb_total_hk1 ,2)}</td>
                            <td colspan="{$column_hk2+$column_gk2+1}"></td>
                            <td>{number_format($tb_total_hk2 ,2)}</td>
                            <td colspan="1"></td>
                        </tr>
                        {*                    kết thúc năm *}
                        <tr>
                            <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                                <strong>{__("End year")}</strong>
                            </td>
                            <td colspan="{$column_hk1 + $column_gk1 + 2 + $column_hk2 + $column_gk2 + 2 + 1}"
                                style="text-align: center">{number_format($tb_total_year ,2)}</td>
                        </tr>
                        {*                    Nghỉ có phép*}
                        <tr>
                            <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                                <strong>{__("Absent with permission")}</strong>
                            </td>
                            <td colspan="{$column_hk1 + $column_gk1 + 2 + $column_hk2 + $column_gk2 + 2 + 1}"
                                style="text-align: center">{$child_absent['absent_true']}</td>
                        </tr>
                        {*                    nghỉ không phép*}
                        <tr>
                            <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                                <strong>{__("Absent without permission")}</strong>
                            </td>
                            <td colspan="{$column_hk1 + $column_gk1 + 2 + $column_hk2 + $column_gk2 + 2 + 1}"
                                style="text-align: center">{$child_absent['absent_false']}</td>
                        </tr>
                        </tbody>
                    </table>

                    {elseif $score_fomula=='km'}
                    <tr>
                        <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                            <strong>{__("Average monthly")}</strong>
                        </td>
                        <td>{number_format($children_point_avgs['a1'] ,2)}</td>
                        <td>{number_format($children_point_avgs['b1'] ,2)}</td>
                        <td>{number_format($children_point_avgs['c1'] ,2)}</td>
                        <td>{number_format($children_point_avgs['d1'] ,2)}</td>
                        <td>{number_format($children_point_avgs['a2'] ,2)}</td>
                        <td>{number_format($children_point_avgs['b2'] ,2)}</td>
                        <td>{number_format($children_point_avgs['c2'] ,2)}</td>
                        <td>{number_format($children_point_avgs['d2'] ,2)}</td>

                    </tr>
                    <tr>
                        <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                            <strong>{__("Average semesterly")}</strong>
                        </td>
                        <td colspan="3" style="text-align: center">{number_format($children_point_avgs['x1'] ,2)}</td>
                        <td></td>
                        <td colspan="3" style="text-align: center">{number_format($children_point_avgs['x2'] ,2)}</td>
                        <td></td>
                    </tr>
                    {*                    kết thúc kỳ*}
                    <tr>
                        <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                            <strong>{__("End semester")}</strong>
                        </td>
                        <td colspan="4" style="text-align: center">{number_format($children_point_avgs['e1'] ,2)}</td>
                        <td colspan="4" style="text-align: center">{number_format($children_point_avgs['e2'] ,2)}</td>
                    </tr>
                    {*                    kết thúc năm *}
                    <tr>
                        <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                            <strong>{__("End year")}</strong>
                        </td>
                        <td colspan="8" style="text-align: center">{number_format($children_point_avgs['y'] ,2)}</td>
                    </tr>
                    {*                    Nghỉ có phép*}
                    <tr>
                        <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                            <strong>{__("Absent has permission")}</strong>
                        </td>
                        <td colspan="8" style="text-align: center">{$child_absent['absent_true']}</td>
                    </tr>
                    {*                    nghỉ không phép*}
                    <tr>
                        <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                            <strong>{__("Absent without permission")}</strong>
                        </td>
                        <td colspan="8" style="text-align: center">{$child_absent['absent_false']}</td>
                    </tr>
                    </tbody>
                    </table>
                    <strong>{__("Re-Exam")}</strong>
                    <table class="table table-striped table-bordered" style="z-index: 1;">
                        <thead>
                        <tr bgcolor="#fff">
                            <th rowspan="2" class="pinned" style="padding: 8px 6px" id="width_no">#</th>
                            <th rowspan="2" nowrap="true" class="pinned"
                                style="padding: 8px 6px">{__("Subject name")}
                            </th>
                            <th colspan="1">{__("Point")}</th>
                        </tr>
                        </thead>
                        <tbody>
                        {$rowIdx = 1}
                        {foreach $children_subject_reexams as $k => $row}
                            <tr>
                                <td align="center" class="pinned">{$rowIdx}</td>
                                <td nowrap="true" class="pinned text-bold color-blue">
                                    <strong>{$row['name']}</strong>
                                </td>
                                <td style="text-align: center">{$row['point']}</td>
                                {$rowIdx = $rowIdx + 1}
                            </tr>
                        {/foreach}
                        <tr>
                            <td colspan="2" nowrap="true" class="pinned text-bold color-blue"
                                style="text-align: center">
                                <strong>{__("Result Re-exam")}</strong>
                            </td>
                            <td colspan="3" style="text-align: center">{number_format($result_exam ,2)}</td>
                        </tr>
                        </tbody>
                    </table>
                    {/if}
                </div>
            {/if}
        </div>
    {/if}
</div>