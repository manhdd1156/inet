{if $rows|count > 0}
    <div class="table-responsive" id="example">
        <strong style="float: right">{__("Status ")} :
            {if $status == 'Pass' }
                <strong style="color:lawngreen">{__({$status})}</strong>
            {elseif $status == 'Fail'}
                <strong style="color:red">{__({$status})}</strong>
            {elseif $status == 'Re-exam'}
                <strong style="color:orange">{__({$status})}</strong>
            {else}
                <strong>{__({$status})}</strong>
            {/if}

        </strong>
        <table class="table table-striped table-bordered" style="z-index: 1">
            <thead>
            <tr bgcolor="#fff">
                <th rowspan="2" class="pinned" style="padding: 8px 6px" id="width_no">#</th>
                <th rowspan="2" nowrap="true" class="pinned"
                    style="padding: 8px 6px">{__("Subject name")}</th>
                <th colspan="{if $score_fomula=='vn'}{$column_hk2+$column_gk2+2}{else}4{/if}">{__("Semester 1")}</th>
                <th colspan="{if $score_fomula=='vn'}{$column_hk2+$column_gk2+2}{else}4{/if}">{__("Semester 2")}</th>
            </tr>
            <tr>
                {for $i=1;$i<=$column_hk1;$i++}
                    <th>{if $score_fomula=='vn'}tx{else}M{$i}{/if}</th>
                {/for}
                {for $i=0;$i<$column_gk1;$i++}
                    <th>{if $score_fomula=='vn'}gk{else}D1{/if}</th>
                {/for}
                {if $score_fomula=='vn'}
                    <th>ck</th>
                    <th>{__("Average")}</th>
                {/if}
                {for $i=1;$i<=$column_hk2;$i++}
                    <th>{if $score_fomula=='vn'}tx{else}M{$i}{/if}</th>
                {/for}
                {for $i=0;$i<$column_gk2;$i++}
                    <th>{if $score_fomula=='vn'}gk{else}D2{/if}</th>
                {/for}
                {if $score_fomula=='vn'}
                    <th>ck</th>
                    <th>Total</th>
                    <th>{__("Average")}</th>
                {/if}
            </tr>
            </thead>
            <tbody>
            {$rowIdx = 1}
            {foreach $rows as $k => $row}
                <tr>
                    <td align="center" class="pinned">{$rowIdx}</td>

                    <td nowrap="true" class="pinned text-bold color-blue">
                        <strong>{$row['subject_name']}</strong>
                    </td>
                    {$rowIdx = $rowIdx + 1}
                    {foreach $subject_key as $key}
                        <td>{$row[strtolower($key)]}</td>
                    {/foreach}
                </tr>
            {/foreach}
            {*                    Các điểm trung bình*}
            {if $score_fomula == 'vn'}
            <tr>
                <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                    <strong>{__("End Semester")}</strong>
                </td>
                <td colspan="{$column_hk1+$column_gk1+1}"></td>
                <td>{number_format($tb_total_hk1 ,2)}</td>
                <td colspan="{$column_hk2+$column_gk2+1}"></td>
                <td>{number_format($tb_total_hk2 ,2)}</td>
                <td colspan="1"></td>
            </tr>
            {*                    kết thúc năm *}
            <tr>
                <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                    <strong>{__("End year")}</strong>
                </td>
                <td colspan="{$column_hk1 + $column_gk1 + 2 + $column_hk2 + $column_gk2 + 2 + 1}"
                    style="text-align: center">{number_format($tb_total_year ,2)}</td>
            </tr>
            {*                    Nghỉ có phép*}
            <tr>
                <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                    <strong>{__("Absent with permission")}</strong>
                </td>
                <td colspan="{$column_hk1 + $column_gk1 + 2 + $column_hk2 + $column_gk2 + 2 + 1}"
                    style="text-align: center">{$child_absent['absent_true']}</td>
            </tr>
            {*                    nghỉ không phép*}
            <tr>
                <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                    <strong>{__("Absent without permission")}</strong>
                </td>
                <td colspan="{$column_hk1 + $column_gk1 + 2 + $column_hk2 + $column_gk2 + 2 + 1}"
                    style="text-align: center">{$child_absent['absent_false']}</td>
            </tr>
            </tbody>
        </table>

        {elseif $score_fomula=='km'}
        <tr>
            <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                <strong>{__("Average monthly")}</strong>
            </td>
            <td>{number_format($children_point_avgs['a1'] ,2)}</td>
            <td>{number_format($children_point_avgs['b1'] ,2)}</td>
            <td>{number_format($children_point_avgs['c1'] ,2)}</td>
            <td>{number_format($children_point_avgs['d1'] ,2)}</td>
            <td>{number_format($children_point_avgs['a2'] ,2)}</td>
            <td>{number_format($children_point_avgs['b2'] ,2)}</td>
            <td>{number_format($children_point_avgs['c2'] ,2)}</td>
            <td>{number_format($children_point_avgs['d2'] ,2)}</td>

        </tr>
        <tr>
            <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                <strong>{__("Average semesterly")}</strong>
            </td>
            <td colspan="3" style="text-align: center">{number_format($children_point_avgs['x1'] ,2)}</td>
            <td></td>
            <td colspan="3" style="text-align: center">{number_format($children_point_avgs['x2'] ,2)}</td>
            <td></td>
        </tr>
        {*                    kết thúc kỳ*}
        <tr>
            <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                <strong>{__("End semester")}</strong>
            </td>
            <td colspan="4" style="text-align: center">{number_format($children_point_avgs['e1'] ,2)}</td>
            <td colspan="4" style="text-align: center">{number_format($children_point_avgs['e2'] ,2)}</td>
        </tr>
        {*                    kết thúc năm *}
        <tr>
            <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                <strong>{__("End year")}</strong>
            </td>
            <td colspan="8" style="text-align: center">{number_format($children_point_avgs['y'] ,2)}</td>
        </tr>
        {*                    Nghỉ có phép*}
        <tr>
            <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                <strong>{__("Absent has permission")}</strong>
            </td>
            <td colspan="8" style="text-align: center">{$child_absent['absent_true']}</td>
        </tr>
        {*                    nghỉ không phép*}
        <tr>
            <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                <strong>{__("Absent without permission")}</strong>
            </td>
            <td colspan="8" style="text-align: center">{$child_absent['absent_false']}</td>
        </tr>
        </tbody>
        </table>
        <strong>{__("Re-Exam")}</strong>
        <table class="table table-striped table-bordered" style="z-index: 1;">
            <thead>
            <tr bgcolor="#fff">
                <th rowspan="2" class="pinned" style="padding: 8px 6px" id="width_no">#</th>
                <th rowspan="2" nowrap="true" class="pinned"
                    style="padding: 8px 6px">{__("Subject name")}
                </th>
                <th colspan="1">{__("Point")}</th>
            </tr>
            </thead>
            <tbody>
            {$rowIdx = 1}
            {foreach $children_subject_reexams as $k => $row}
                <tr>
                    <td align="center" class="pinned">{$rowIdx}</td>
                    <td nowrap="true" class="pinned text-bold color-blue">
                        <strong>{$row['name']}</strong>
                    </td>
                    <td style="text-align: center">{$row['point']}</td>
                    {$rowIdx = $rowIdx + 1}
                </tr>
            {/foreach}
            <tr>
                <td colspan="2" nowrap="true" class="pinned text-bold color-blue"
                    style="text-align: center">
                    <strong>{__("Result Re-exam")}</strong>
                </td>
                <td colspan="3" style="text-align: center">{number_format($result_exam ,2)}</td>
            </tr>
            </tbody>
        </table>
        {/if}
    </div>
{else}
    <div align="center"><strong style="color: red">{__("Chưa có thông tin điểm")}</strong></div>
{/if}
{*Jquery Cố định cột số thứ tự và họ tên*}
<script type="text/javascript">

    var $table = $('.table-pinned');
    var $fixedColumn = $table.clone().insertBefore($table).addClass('fixed-column');
    $fixedColumn.find('th').each(function (i, elem) {
        $(this).width($table.find('th:eq(' + i + ')').width());
    });
    $fixedColumn.find('td').each(function (i, elem) {
        $(this).width($table.find('td:eq(' + i + ')').width());
    });
    $fixedColumn.find('th,td').not('.pinned').hide();
    $fixedColumn.find('[id]').each(function () {
        $(this).removeAttr('id');
    });

    $fixedColumn.find('tr').each(function (i, elem) {
        $(this).height($table.find('tr:eq(' + i + ')').height());
    });

    $(window).resize(function () {
        $fixedColumn.find('tr').each(function (i, elem) {
            $(this).height($table.find('tr:eq(' + i + ')').height());
        });
        $fixedColumn.find('td').each(function (i, elem) {
            $(this).addClass('white-space_nowrap');
            $(this).width($table.find('td:eq(' + i + ')').width());
        });
    });
    //$fixedColumn.find('td').addClass('white-space_nowrap');
    //    $("#right").on("click", function() {
    //        var leftPos = $('#example').scrollLeft();
    //        console.log(leftPos);
    //        $("#example").animate({
    //            scrollLeft: leftPos - 200
    //        }, 800);
    //    });
    $('.right').click(function (event) {
        var width_col = $('.table-pinned').find('td:eq(' + 4 + ')').width();
        var pos = $('#example').scrollLeft() + width_col + 100;
        return false;
        $('#example').scrollLeft(pos);
    });
    $('.left').click(function (event) {
        var width_col = $('.table-pinned').find('td:eq(' + 4 + ')').width();
        var pos = $('#example').scrollLeft() - width_col - 100;
        $('#example').scrollLeft(pos);
    });

    jQuery(function ($) {
        function fixDiv() {
            var $cache = $('#getFixed');
            var $button = $('#table_button');
            if ($(window).scrollTop() > 100) {
                $cache.css({
                    'position': 'fixed',
                    'top': '50px'
                });
                $cache.width($('#class_list_point').width() - 1);
            } else
                $cache.css({
                    'position': 'relative',
                    'top': 'auto'
                });

            if ($(window).scrollTop() > 100)
                $button.css({
                    'position': 'fixed',
                    'top': '90px'
                });
            else
                $button.css({
                    'position': 'relative',
                    'top': 'auto'
                });
        }

        $(window).scroll(fixDiv);
        fixDiv();
    });
</script>