<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <i class="fa fa-tasks fa-fw fa-lg pr10"></i>
        {__('Point')}
        {if $sub_view == "detail"}
            &rsaquo; {__('Detail')}
        {elseif $sub_view == "comment"}
            &rsaquo; {__('Comment')}
        {/if}
    </div>
    {if $sub_view == "detail"}
        <div class="panel-body">
            <form class="form-horizontal" action="#" enctype="multipart/form-data" method="post" id="point_search_form">
                <input type="hidden" name="schoolId" id="schoolId" value="{$child['school_id']}"/>
                <input type="hidden" name="classId" id="classId" value="{$child['class_id']}"/>
                <input type="hidden" name="childId" id="childId" value="{$child['child_id']}"/>
                <input type="hidden" name="do" value="detail"/>
                <div class="form-group">

                    <div class="col-sm-3 col-sm-offset-9">
                        <select name="school_year" id="school_year" class="form-control">
                            {foreach $default_year as $key => $year}
                                {*                                <option value="{$year}">{substr($key, 0, strlen(date("Y")))}</option>*}
                                <option value="{$year}"
                                        {if (substr($year, 0, strlen(date("Y"))) === date("Y"))}selected{/if}>{$year}</option>
                            {/foreach}
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{$child['child_code']}</label>
                    <label class="col-sm-3 control-label text-left">{$child['child_name']}</label>
                    <label class="col-sm-3 control-label text-left">{$child['birthday']}</label>
                </div>
                {if $grade == 1}
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-3">
                            <select name="is_last_semester" id="is_last_semester" class="form-control">
                                <option value="0">{__("Mid semester")}</option>
                                <option value="1">{__("Last semester")}</option>
                            </select>
                        </div>
                    </div>
                {/if}
                <div id="child_list_point"></div>
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
            </form>

        </div>
    {elseif $sub_view == "comment"}
        <div class="panel-body">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/child/bochild_point.php">
                <input type="hidden" name="schoolId" id="schoolId" value="{$child['school_id']}"/>
                <input type="hidden" name="classId" id="classId" value="{$child['class_id']}"/>
                <input type="hidden" name="childId" id="childId" value="{$child['child_id']}"/>
                <input type="hidden" name="do" value="comment"/>
                <div class="form-group">
                    <div class="col-sm-3 col-sm-offset-9">
                        <select name="school_year" id="school_year" class="form-control">
                            {foreach $default_year as $key => $year}
                                {*                                <option value="{$year}" {if $key == count($default_year) - 1}selected{/if}>{$year}</option>*}
                                <option value="{$year}"
                                        {if (substr($year, 0, strlen(date("Y"))) === date("Y"))}selected{/if}>{$year}</option>
                            {/foreach}
                        </select>
                    </div>
                </div>
                <div class="form-group">
{*                    <label class="col-sm-3 control-label text-left">{__("Select student")} (*)</label>*}
                    <label class="col-sm-3 control-label text-left">{$child['child_code']}</label>
                    <label class="col-sm-3 control-label text-left">{$child['child_name']}</label>
                    <label class="col-sm-3 control-label text-left">{$child['birthday']}</label>
                    <div class="col-sm-3">
                        <select id="semester" name="semester" class="form-control">
                            {foreach $semesters as $semester => $name}
                                <option value="{$semester}">{$name}</option>
                            {/foreach}
                        </select>
                    </div>
                </div>
                {if $grade == 1}
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-3">
                            <select name="is_last_semester" id="is_last_semester" class="form-control">
                                <option value="0">{__("Mid semester")}</option>
                                <option value="1">{__("Last semester")}</option>
                            </select>
                        </div>
                    </div>
                {/if}
                <div class="table-responsive" id="child_comment"></div>

{*                <div class="form-group">*}
{*                    <div class="col-sm-9 col-sm-offset-4">*}
{*                        <button type="submit" class="btn btn-primary padrl30" disabled="true"*}
{*                                id="submit_id">{__("Save")}</button>*}
{*                    </div>*}
{*                </div>*}
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
            </form>

        </div>
    {/if}
</div>