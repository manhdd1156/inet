<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <i class="fa fa-list fa-lg fa-fw pr10"></i>
        {__("Contact list")}
    </div>
    <div class = "panel-body with-table">
        <table class = "table-responsive table table-bordered" style = "margin-bottom: 0px">
            <tr>
                <td>
                    <strong>{__('School Management')}: </strong>
                    {foreach $schoolAdmin as $row}
                        <span class="name js_user-popover" data-uid="{$row['user_id']}">
                            <a href="{$system['system_url']}/{$row['user_name']}">{$row['user_fullname']}</a>
                        </span>
                        {if $row['user_id'] != $row->_data['user_id']}
                            <a href="#" class="far fa-comments fa-lg js_chat-start" data-name="{$row['user_fullname']}" data-uid="{$row['user_id']}"></a>
                        {/if}
                    {/foreach}
                </td>
            </tr>
            <tr>
                <td>
                    <strong>{__('Teacher')}:</strong>
                    {foreach $teacher as $row}
                        <span class="name js_user-popover" data-uid="{$row['user_id']}">
                            <a href="{$system['system_url']}/{$row['user_name']}">{$row['user_fullname']}</a>
                        </span>
                        {if $row['user_id'] != $row->_data['user_id']}
                            <a href="#" class="far fa-comments fa-lg js_chat-start" data-name="{$row['user_fullname']}" data-uid="{$row['user_id']}"></a>
                        {/if}
                    {/foreach}
                </td>
            </tr>
        </table>

    </div>
    <div class="panel-body with-table">
        <div><strong>{__("Student list")}&nbsp;({$children|count})</strong></div>
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>{__('Child')}</th>
                    <th>{__('Birthdate')}</th>
                    <th>{__('Parent phone')}</th>
                    {if $school['config']['display_parent_info_for_others']}
                        <th>{__('Parent')}</th>
                    {/if}
                </tr>
                </thead>
                <tbody>
                {$idx = 1}
                {foreach $children as $row}
                    <tr>
                        <td align="center">{$idx}</td>
                        <td>{$row['child_name']}</td>
                        <td>{$row['birthday']}</td>
                        <td>
                            {if $row['parent_phone'] != ''}
                                {$row['parent_phone']}
                            {else}
                                {__('No information')}
                            {/if}
                        </td>

                        {if $school['config']['display_parent_info_for_others']}
                            <td>
                                {if count($row['parent']) == 0}
                                    {__("No parent")}
                                {else}
                                    {foreach $row['parent'] as $_user}
                                        <span class="name js_user-popover" data-uid="{$_user['user_id']}">
                                            <a href="{$system['system_url']}/{$_user['user_name']}">{$_user['user_fullname']}</a>
                                        </span>
                                        {if $_user['user_id'] != $user->_data['user_id']}
                                            <a href="#" class="far fa-comments fa-lg js_chat-start" data-name="{$_user['user_fullname']}" data-uid="{$_user['user_id']}"></a>
                                        {/if}
                                        <br/>
                                    {/foreach}
                                {/if}
                            </td>
                        {/if}
                    </tr>
                    {$idx = $idx + 1}
                {/foreach}
                </tbody>
            </table>
        </div>
    </div>
</div>