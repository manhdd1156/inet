<div class="table-responsive" name = "schedule_all">
    <table class="table table-striped table-bordered table-hover js_dataTable">
        <thead>
        <tr><th colspan="6">{__("Child development")}</th></tr>
        <tr>
            <th width="5%">
                {__("No.")}
            </th>
            <th width="15%">
                {__("Month")}/{__("Day")}
            </th>
            <th width="35%">
                {__("Title")}
            </th>
            <th width="15%">
                {__("Type")}
            </th>
            <th width="15%">
                {__("Exist too development")}
            </th>
            <th width="15%">
                {__("Actions")}
            </th>
        </tr>
        </thead>
        <tbody>
        {foreach $results as $k => $row}
            <tr>
                <td align="center">{$k + 1}</td>
                <td align="center">{$row['month_push']}/{$row['day_push']}</td>
                <td><a href = "{$system['system_url']}/noga/childdevelopments/edit/{$row['child_development_id']}">{$row['title']}</a></td>
                <td align="center">
                    {if $row['type'] == 1}{__("Vaccination")} {else} {__("Information")}{/if}
                </td>
                <td align="center">
                    {if $row['is_development'] == 1}{__("Yes")} {else} {__("No")}{/if}
                </td>
                <td align="center">
                    <a class="btn btn-xs btn-default" href = "{$system['system_url']}/noga/childdevelopments/edit/{$row['child_development_id']}">{__("Edit")}</a>
                    <a href="#" class = "btn-xs btn btn-danger js_noga-child-development-delete" data-id="{$row['child_development_id']}">{__("Delete")}</a>
                </td>
            </tr>
        {/foreach}
        </tbody>
    </table>
</div>

<script type="text/javascript">
    $(function() {
        // run DataTable
        $('.js_dataTable').DataTable({
            "aoColumnDefs": [ { 'aDataSort': false, 'aTargets': [ -1 ] } ],
            "language": {
                "decimal":        "",
                "emptyTable":     __["No data available in table"],
                "info":           __["Showing _START_ to _END_ of _TOTAL_ results"],
                "infoEmpty":      __["Showing 0 to 0 of 0 results"],
                "infoFiltered":   "(filtered from _MAX_ total entries)",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     __["Show"] + " " + "_MENU_" + " " + __["results"],
                "loadingRecords": __["Loading..."],
                "processing":     __["Processing..."],
                "search":         __["Search"],
                "zeroRecords":    __["No matching records found"],
                "paginate": {
                    "first":      __["First"],
                    "last":       __["Last"],
                    "next":       __["Next"],
                    "previous":   __["Previous"]
                },
                "aria": {
                    "sortAscending":  ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                }
            }
        });
    });
</script>