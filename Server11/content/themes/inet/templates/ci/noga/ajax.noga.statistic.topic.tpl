<table class="table table-striped table-bordered table-hover" id="statistic_topic_detail">
    <thead>
    <tr>
        <th>{__("Groups")}</th>
        <th>{__("Lượt tương tác")}</th>

    </tr>
    </thead>
    <tbody>

    <tr>
        <td><strong>{__("Coniu+")}</strong></td>
        <td align="center">{$rows[$smarty.const.GROUP_NAME_CONIU]}</td>
    </tr>
    <tr>
        <td><strong>{__("Ăn dặm")}</strong></td>
        <td align="center">{$rows[$smarty.const.GROUP_NAME_ANDAM]}</td>
    </tr>
    <tr>
        <td><strong>{__("Dinh dưỡng thai kỳ")}</strong></td>
        <td align="center">{$rows[$smarty.const.GROUP_NAME_DINHDUONG]}</td>
    </tr>
    <tr>
        <td><strong>{__("Dạy con đúng cách")}</strong></td>
        <td align="center">{$rows[$smarty.const.GROUP_NAME_DAYCON]}</td>
    </tr>
    <tr>
        <td><strong>{__("Thai giáo")}</strong></td>
        <td align="center">{$rows[$smarty.const.GROUP_NAME_THAIGIAO]}</td>
    </tr>
    <tr>
        <td><strong>{__("Hiếm muộn")}</strong></td>
        <td align="center">{$rows[$smarty.const.GROUP_NAME_HIEMMUON]}</td>
    </tr>
    <tr>
        <td><strong>{__("Hôn nhân gia đình")}</strong></td>
        <td align="center">{$rows[$smarty.const.GROUP_NAME_HONNHAN]}</td>
    </tr>
    <tr>
        <td><strong>{__("Thực phẩm Organic")}</strong></td>
        <td align="center">{$rows[$smarty.const.GROUP_NAME_THUCPHAMORGANIC]}</td>
    </tr>

    </tbody>
</table>