<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <div class="pull-right flip">
            {if $sub_view == ""}
            {/if}
        </div>
        <i class="fa fa-user fa-fw fa-lg pr10"></i>
        {__("Users new")}
        {if $sub_view == ""}
            &rsaquo; {__("Search")}
        {/if}
    </div>
    {if $sub_view == ""}
        <div class="panel-body with-table form-horizontal">
            <div class="row">
                <div class='col-sm-3'>
                    <div class='input-group date' id='fromuserregpicker'>
                        <input type='text' name="fromDate" id="fromDate" class="form-control" placeholder="{__("From date")}"/>
                        <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                    </div>
                </div>
                <div class='col-sm-3'>
                    <div class='input-group date' id='touserregpicker'>
                        <input type='text' name="toDate" id="toDate" class="form-control" placeholder="{__("To date")}"/>
                        <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                    </div>
                </div>
                <div class='col-sm-2'>
                    <div class="form-group">
                        <a href="#" id="search" class="btn btn-default js_user-registed-new">{__("Search")}</a>
                        <label id="loading" class="btn btn-info x-hidden">{__("Loading")}...</label>
                    </div>
                </div>
            </div>
            <div class="table-responsive pt10" id="user_new_list">
                {*{include file="ci/school/ajax.child.birthday.tpl"}*}
            </div>
        </div>
    {/if}
</div>