<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <div class="pull-right flip">
            {if $sub_view == ""}
            {/if}
        </div>
        <i class="fa fa-user fa-fw fa-lg pr10"></i>
        {__("Manage and teacher")}
        {if $sub_view == ""}
            &rsaquo; {__("Search")}
        {/if}
    </div>
    {if $sub_view == ""}
        <div class="panel-body with-table form-horizontal">
            <div class="row">
                <div class='col-sm-3'>
                    <select name="school_role" id="school_role" class="form-control">
                        <option value="1">{__("Manage")}</option>
                        <option value="2">{__("Teacher")}</option>
                    </select>
                </div>
                <div class='col-sm-6'>
                    <div class="form-group">
                        <select name="school_status" id="school_status_no_js" class="form-control">
                            <option value="0">{__("Select satuts")}...</option>
                            <option value="1">{__("Using Inet")}</option>
                            <option value="2">{__("Having Inet's page")}</option>
                            <option value="3">{__("Waiting confirmation")}</option>
                        </select>
                    </div>
                </div>
                <div class='col-sm-3'>
                    <div class="form-group">
                        <a href="#" id="search" class="btn btn-default js_noga-role-search">{__("Search")}</a>
                        <label id="loading" class="btn btn-info x-hidden">{__("Loading")}...</label>
                    </div>
                </div>
            </div>
            <div class="table-responsive pt10" id="role_list">
                {*{include file="ci/school/ajax.child.birthday.tpl"}*}
            </div>
        </div>
    {/if}
</div>