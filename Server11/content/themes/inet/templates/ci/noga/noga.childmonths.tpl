<div class="panel panel-default">
    <div class="panel-heading with-icon">
        {if $sub_view == ""}
            <div class="pull-right flip">
                <a href="{$system['system_url']}/noga/childmonths/add" class="btn btn-default">
                    <i class="fa fa-plus"></i> {__("Add New")}
                </a>
            </div>
        {elseif $sub_view == "add"}
            <div class="pull-right flip">
                <a href="{$system['system_url']}/noga/childmonths" class="btn btn-default">
                    <i class="fa fa-list"></i> {__("Lists")}
                </a>
            </div>
        {/if}
        <i class="fas fa-calendar-alt fa-fw fa-lg pr10"></i>
        {__("Month age information")}
        {if $sub_view == ""}
        {elseif $sub_view == "add"}
            &rsaquo; {__("Add New")}
        {elseif $sub_view == "edit"}
            &rsaquo; {__("Edit")}
        {/if}
    </div>
    {if $sub_view == ""}
        <div class="panel-body with-table form-horizontal">
            <div class="table-responsive" name = "schedule_all">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr><th colspan="5">{__("Month age information")}</th></tr>
                    <tr>
                        <th>
                            {__("No.")}
                        </th>
                        <th>
                            {__("Month age")}
                        </th>
                        <th>
                            {__("Title")}
                        </th>
                        <th>
                            {__("Link")}
                        </th>
                        <th>
                            {__("Actions")}
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    {foreach $childMonths as $k => $row}
                        <tr>
                            <td align="center">{$k + 1}</td>
                            <td align="center"><a href = "{$system['system_url']}/noga/childmonths/edit/{$row['child_based_on_month_id']}">{__("Month information")} {$row['month']}</a></td>
                            <td><a href = "{$system['system_url']}/noga/childmonths/edit/{$row['child_based_on_month_id']}">{$row['title']}</a></td>
                            <td>
                                <a href="{$row['link']}" target="_blank">{$row['link']}</a>
                            </td>
                            <td>
                                <a class="btn btn-xs btn-default" href = "{$system['system_url']}/noga/childmonths/edit/{$row['child_based_on_month_id']}">{__("Edit")}</a>
                                <a href="#" class = "btn-xs btn btn-danger js_noga-child-month-delete" data-id="{$row['child_based_on_month_id']}">{__("Delete")}</a>
                            </td>
                        </tr>
                    {/foreach}
                    </tbody>
                </table>
            </div>
        </div>
    {elseif $sub_view == "add"}
        <div class="panel-body">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/noga/bo_noga_childmonth.php">
                <input type="hidden" name="do" value="add"/>
                <div class="form-group">
                    <label class="col-sm-4 control-label text-left" style="float: left">{__("Enter child month information")}</label>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id = "myTableChildMonth">
                        <thead>
                        <tr>
                            <th>
                                {__('No.')}
                            </th>
                            <th>
                                {__('Option')}
                            </th>
                            <th>
                                {__('Month age')}
                            </th>
                            <th>
                                {__('Title')}
                            </th>
                            <th>
                                {__('Link')}
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td align="center" class = "col_no">1</td>
                            <td align="center">
                                <a class = "btn btn-default js_cate-delete_null"> {__("Delete")} </a>
                            </td>
                            <td>
                                <input type = "number" min="0" step="1" name = "months[]" placeholder="" style = "padding-left: 10px; height: 35px; width: 100%; ">
                            </td>
                            <td>
                                <textarea class = "note js_autosize" type="text" name = "titles[]" style="width:100%"></textarea>
                            </td>
                            <td>
                                <textarea class = "note js_autosize" type="text" name = "links[]" style="width:100%"></textarea>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <div class = "form-group mt20">
                        <div class = "col-sm-12">
                            <a class="btn btn-default js_add-child-month">{__("Add new row")}</a>
                            <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                        </div>
                    </div>
                </div>
                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->
                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->

            </form>
        </div>
    {elseif $sub_view == "edit"}
        <div class="panel-body">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/noga/bo_noga_childmonth.php">
                <input type="hidden" name="do" value="edit"/>
                <input type="hidden" name="child_based_on_month_id" value="{$data['child_based_on_month_id']}"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Month information")} (*)</label>
                    <div class="col-sm-9">
                        <input type = "text" class="form-control" name="month" required value = "{$data['month']}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Title")}</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="title" rows="6">{$data['title']}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Link")}</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="link" rows="6">{$data['link']}</textarea>
                    </div>
                </div>
                <div class = "form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-default">{__("Save")}</button>
                    </div>
                </div>
                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->
                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    {/if}
</div>