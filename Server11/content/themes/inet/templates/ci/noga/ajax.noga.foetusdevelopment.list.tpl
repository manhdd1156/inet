<div class="table-responsive" name = "schedule_all">
    <table class="table table-striped table-bordered table-hover js_dataTable">
        <thead>
        <tr><th colspan="5">{__("Foetus development")}</th></tr>
        <tr>
            <th>
                {__("No.")}
            </th>
            <th>
                {__("Week")}
            </th>
            <th>
                {__("Title")}
            </th>
            <th>
                {__("Type")}
            </th>
            <th>
                {__("Actions")}
            </th>
        </tr>
        </thead>
        <tbody>
        {foreach $results as $k => $row}
            <tr>
                <td class="align-middle" align="center">{$k + 1}</td>
                <td class="align-middle" align="center">{$row['week']}</td>
                <td class="align-middle"><a href = "{$system['system_url']}/noga/foetusdevelopments/edit/{$row['foetus_info_id']}">{$row['title']}</a></td>
                <td class="align-middle" align="center">
                    {if $row['type'] == 1}{__("Pregnancy check")} {else} {__("Information")}{/if}
                </td>
                <td class="align-middle" align="center">
                    <a class="btn btn-xs btn-default" href = "{$system['system_url']}/noga/foetusdevelopments/edit/{$row['foetus_info_id']}">{__("Edit")}</a>
                    <a href="#" class = "btn-xs btn btn-danger js_noga-foetus-info-delete" data-id="{$row['foetus_info_id']}">{__("Delete")}</a>
                </td>
            </tr>
        {/foreach}
        </tbody>
    </table>
</div>

<script type="text/javascript">
    $(function() {
        // run DataTable
        $('.js_dataTable').DataTable({
            "aoColumnDefs": [ { 'aDataSort': false, 'aTargets': [ -1 ] } ]
        });
    });
</script>