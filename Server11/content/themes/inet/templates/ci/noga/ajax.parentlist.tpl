<table class="table table-striped table-bordered table-hover js_dataTable">
    <thead>
    <tr><th colspan="7">{__("Parent list")}:&nbsp;{$countLogin}/{$countNoLogin} ({$results|count}) </th></tr>
    <tr>
        <th>{__("No.")}</th>
        <th>{__("Full name")}</th>
        <th>{__("Username")}</th>
        <th>{__("Email")}</th>
        <th>{__("Telephone")}</th>
        <th>{__("Child")}</th>
        <th>{__("Last Login")}</th>
    </tr>
    </thead>
    <tbody>
    {$idx = 1}
    {foreach $results as $row}
        <tr>
            <td>
                <center>{$idx}</center>
            </td>
            <td>
                <a href="{$system['system_url']}/{$row['user_name']}">
                    {$row['user_fullname']}
                </a>
            </td>
            <td>
                {$row['user_name']}
            </td>
            <td>
                {$row['user_email']}
            </td>
            <td>
                {$row['user_phone']}
            </td>
            <td>
                {foreach $row['children'] as $child}
                    {$child['child_name']}
                    <br/>
                {/foreach}
            </td>
            <td>
                {toSysDate($row['user_last_login'])}
            </td>
        </tr>
        {$idx = $idx + 1}
    {/foreach}
    </tbody>
</table>
