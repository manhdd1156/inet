<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <i class="fa fa-pie-chart fa-fw fa-lg pr10"></i>
        {__("Statistic")}
        {if $sub_view == "schools"}
            &rsaquo; {__('School')}
        {elseif $sub_view == "users"}
            &rsaquo; {__('Users')}
        {elseif $sub_view == "usersonline"}
            &rsaquo; {__('Users online')}
        {elseif $sub_view == "topics"}
            &rsaquo; {__('Topic')}
        {elseif $sub_view == "schooldetail"}
            &rsaquo; {__('School')} &rsaquo; {$school['page_title']}
        {elseif $sub_view == "reviewschool"}
            &rsaquo; {__('School')} &rsaquo; {$school['page_title']}
        {elseif $sub_view == "reviewteachers"}
            &rsaquo; {$school['page_title']} &rsaquo;  {__('Teacher Reviews')}
        {elseif $sub_view == "reviewteacher"}
            &rsaquo; {$school['page_title']}
            &rsaquo; {__('Teacher Reviews')}
            &rsaquo; {$rows['user_fullname']}
        {/if}
    </div>
    {if $sub_view == "schools"}
        {*<pre>*}
            {*{print_r($statistic)}*}
        {*</pre>*}
        <div class="panel-body with-table">
            <div class="row">
                <div class="form-group">
                    <div class="col-sm-2"></div>
                    <div class='col-sm-3'>
                        <div class='input-group date' id='fromDate'>
                            <input type='text' name="begin" class="form-control" id="begin" value="{$begin}"/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                    <div class='col-sm-1' align="center"><i class="fas fa-long-arrow-alt-right fa-lg fa-fw pr10 pt10"></i></div>
                    <div class='col-md-3'>
                        <div class='input-group date' id='toDate'>
                            <input type='text' name="end" class="form-control" id="end"/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>

                    <div class='col-sm-3'>
                        <div class="form-group">
                            <button class="btn btn-default js_statistic-search">{__("Search")}</button>
                        </div>
                    </div>
                </div>
            </div>

            <br>
            <div align="center"><strong>{__("TỔNG HỢP TƯƠNG TÁC CỦA TRƯỜNG THEO NGÀY")}</strong></div>
            <br>
            <div class="table-responsive">
                {include file="ci/noga/ajax.noga.statistic.school.tpl"}
            </div>
            <br/>
            <div align="center"><strong>{__("TỔNG HỢP TƯƠNG TÁC CỦA PHỤ HUYNH VỚI TRƯỜNG THEO NGÀY")}</strong></div>
            <br>
            {*Tổng hợp lượt tương tác của trường*}
            <div class="table-responsive">
                {include file="ci/noga/ajax.noga.statistic.parent.tpl"}
            </div>
            <br/>
            <div id="school_list">

                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover js_dataTable">
                        <thead>
                        <tr><th>{__("School list")}&nbsp;({$rows|count})</th></tr>
                        </thead>
                        <tbody>
                        {$idx = 1}
                        {foreach $rows as $row}
                            <tr>
                                <td>
                                    <div>
                                        {$idx} - {$row['page_title']}
                                        &nbsp;|&nbsp;<a href="{$system['system_url']}/pages/{$row['page_name']}">{__("Timeline")}</a>
                                        &nbsp;|&nbsp;{$row['page_name']}
                                        {if ($row['telephone'] != '')} &nbsp;|&nbsp;{$row['telephone']} {/if}
                                        {if ($row['website'] != '')} &nbsp;|&nbsp;<a href="{$row['website']}">{$row['website']}</a>{/if}
                                        {if ($row['email'] != '')} &nbsp;|&nbsp;<a href="mailto:{$row['email']}">{$row['email']}</a> {/if}
                                    </div>
                                    <br>
                                    <div class="pull-left flip">
                                        - {__("Lượt tương tác")} : <font color="red">{$row['total_interactive']}</font>
                                    </div>
                                    <div class="pull-right flip">
                                        <a href="{$system['system_url']}/noga/statistics/schooldetail/{$row['page_id']}" class="btn btn-xs btn-default">{__("Detail")}</a>
                                    </div>
                                </td>
                            </tr>
                            {$idx = $idx + 1}
                        {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>

        </div>

    {elseif $sub_view == "schooldetail"}
        <div class="panel-body with-table">
            <div class="form-group">
                <div class="col-sm-2"></div>
                <div class='col-sm-3'>
                    <div class='input-group date' id='fromDate'>
                        <input type='text' name="begin" class="form-control" id="begin" value="{$begin}"/>
                        <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                    </div>
                </div>
                <div class='col-sm-1' align="center"><i class="fas fa-long-arrow-alt-right fa-lg fa-fw pr10 pt10"></i></div>
                <div class='col-md-3'>
                    <div class='input-group date' id='toDate'>
                        <input type='text' name="end" class="form-control" id="end"/>
                        <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                    </div>
                </div>

                <div class='col-sm-3'>
                    <div class="form-group">
                        <button class="btn btn-default js_statistic_school-search" data-id="{$school['page_id']}">{__("Search")}</button>
                    </div>
                </div>
            </div>
            <br>
            <div align="center"><strong>{__("TỔNG HỢP TƯƠNG TÁC CỦA TRƯỜNG THEO NGÀY")}</strong></div>
            <br>
            <div class="table-responsive">
                {include file="ci/noga/ajax.noga.statistic.school.tpl"}
            </div>
        </div>

        <div class="panel-body with-table">
            <div align="center"><strong>{__("TỔNG HỢP TƯƠNG TÁC CỦA PHỤ HUYNH VỚI TRƯỜNG THEO NGÀY")}</strong></div>
            <br>
            {*Tổng hợp lượt tương tác của trường*}
            <div class="table-responsive">
                {include file="ci/noga/ajax.noga.statistic.parent.tpl"}
            </div>
            <br>
        </div>

        <div class="panel-body with-table">
            <div align="center"><strong>{__("Parent review your child's school and teachers")|upper}</strong></div>
            <br>

            <div class="form-group">

                <div class="box-primary">
                    <div class="list-group">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>{__("Overall review")}</th>
                                <th>{__("Average review")}</th>
                                <th>{__("New review")}</th>
                                <th>{__("Actions")}</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td class="align_middle">{__("School")}</td>
                                <td class="align_middle" align="center">{$reviews['school']['total_review']}</td>
                                <td class="align_middle" align="center">{$reviews['school']['average_review']}</td>
                                <td class="align_middle" align="center">{$reviews['school']['cnt_new_review']}</td>
                                <td align="center" class="align_middle">
                                    <a class="btn btn-xs btn-default" href="{$system['system_url']}/noga/statistics/reviewschool/{$school['page_id']}">{__("Detail")}</a>
                                </td>
                            </tr>

                            <tr>
                                <td class="align_middle">{__("Teacher")}</td>
                                <td class="align_middle" align="center">{$reviews['teacher']['total_review']}</td>
                                <td class="align_middle" align="center">{$reviews['teacher']['average_review']}</td>
                                <td class="align_middle" align="center">{$reviews['teacher']['cnt_new_review']}</td>
                                <td align="center" class="align_middle">
                                    <a class="btn btn-xs btn-default" href="{$system['system_url']}/noga/statistics/reviewteachers/{$school['page_id']}">{__("Detail")}</a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
            <br>
            <br>

        </div>
    {elseif $sub_view == "usersonline"}
        <div class="panel-body with-table">
            <div class="form-group">
                <div class="col-sm-2"></div>
                <div class='col-sm-3'>
                    <div class='input-group date' id='fromDate'>
                        <input type='text' name="begin" class="form-control" id="begin" value="{$begin}"/>
                        <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                    </div>
                </div>
                <div class='col-sm-1' align="center"><i class="fas fa-long-arrow-alt-right fa-lg fa-fw pr10 pt10"></i></div>
                <div class='col-md-3'>
                    <div class='input-group date' id='toDate'>
                        <input type='text' name="end" class="form-control" id="end"/>
                        <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                    </div>
                </div>

                <div class='col-sm-3'>
                    <div class="form-group">
                        <button class="btn btn-default js_statistic_useronline-search">{__("Search")}</button>
                    </div>
                </div>
            </div>
            <br>
            <div align="center"><strong>{__("TỔNG HỢP USER ONLINE THEO NGÀY")}</strong></div>
            <br>
            <div class="table-responsive" id="statistic_useronline">
                {include file="ci/noga/ajax.noga.statistic.useronline.tpl"}
            </div>
        </div>
    {elseif $sub_view == "topics"}

        <div class="panel-body with-table">
            <div align="center"><strong>{__("TỔNG HỢP TƯƠNG TÁC THEO NGÀY")}</strong></div>
            <br>

            <div class="form-group">
                <div class="col-sm-2"></div>
                <div class='col-sm-3'>
                    <div class='input-group date' id='fromDate'>
                        <input type='text' name="begin" class="form-control" id="begin" value="{$begin}"/>
                        <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                    </div>
                </div>
                <div class='col-sm-1' align="center"><i class="fas fa-long-arrow-alt-right fa-lg fa-fw pr10 pt10"></i></div>
                <div class='col-md-3'>
                    <div class='input-group date' id='toDate'>
                        <input type='text' name="end" class="form-control" id="end"/>
                        <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                    </div>
                </div>

                <div class='col-sm-3'>
                    <div class="form-group">
                        <button class="btn btn-default js_statistic_topic-search" >{__("Search")}</button>
                    </div>
                </div>
            </div>
            <br>
            <br>

            {include file="ci/noga/ajax.noga.statistic.topic.tpl"}

        </div>
    {elseif $sub_view == "reviewschool"}

        <div class="panel-body with-table">
            {if isset($rows['review_info'])}
            <div class="pl10">
                <strong>{__("Overall review")}&#58;&nbsp;<font color="red">{$rows['review_info']['total_review']}</font></strong>&emsp;
                <strong>{__("Average review")}&#58;&nbsp;<font color="red">{$rows['review_info']['average_review']}</font></strong>&emsp;
                <strong>{__("New review")}&#58;&nbsp;<font color="red">{$rows['review_info']['cntNewReview']}</font></strong>&emsp;
            </div>
            {else}
            <div class="pl10">
                <strong>{__("Overall review")}&#58;&nbsp;<font color="red">0</font></strong>&emsp;
                <strong>{__("Average review")}&#58;&nbsp;<font color="red">0</font></strong>&emsp;
                <strong>{__("New review")}&#58;&nbsp;<font color="red">0</font></strong>&emsp;
            </div>
            {/if}<br>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover js_dataTable">
                    <thead>
                    <tr>
                        <th width="10%">#</th>
                        <th width="20%">{__('Full name')}</th>
                        <th width="15%">{__('Rating')}</th>
                        <th width="40%">{__("Review content")}</th>
                        <th width="15%">{__("Time")}</th>
                    </tr>
                    </thead>
                    <tbody>
                    {$idx = 1}
                    {foreach $rows['review_detail'] as $row}
                        <tr>
                            <td align="center" class="align_middle">{$idx}</td>
                            <td class="align_middle">
                                    <span class="name js_user-popover" data-uid="{$row['user_id']}">
                                        <a href="{$system['system_url']}/{$row['user_name']}">{$row['user_fullname']}</a>
                                    </span>
                            </td>
                            <td align="center" class="align_middle">{$row['rating']}</td>
                            <td class="align_middle">{$row['comment']}</td>
                            <td align="center" class="align_middle">{toSysDate($row['created_at'])}</td>
                        </tr>
                        {$idx = $idx + 1}
                    {/foreach}

                    </tbody>
                </table>
            </div>
        </div>
    {elseif $sub_view == "reviewteachers"}

        <div class="panel-body with-table">
            {if isset($rows['review_info'])}
            <div class="pl10">
                <strong>{__("Overall review")}&#58;&nbsp;<font color="red">{$rows['review_info']['total_review']}</font></strong>&emsp;
                <strong>{__("Average review")}&#58;&nbsp;<font color="red">{$rows['review_info']['average_review']}</font></strong>&emsp;
                <strong>{__("New review")}&#58;&nbsp;<font color="red">{$rows['review_info']['cntNewReview']}</font></strong>&emsp;
            </div>
            {else}
            <div class="pl10">
                <strong>{__("Overall review")}&#58;&nbsp;<font color="red">0</font></strong>&emsp;
                <strong>{__("Average review")}&#58;&nbsp;<font color="red">0</font></strong>&emsp;
                <strong>{__("New review")}&#58;&nbsp;<font color="red">0</font></strong>&emsp;
            </div>
            {/if}<br>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover js_dataTable">
                    <thead>
                    <tr>
                        <th width="7%">#</th>
                        <th width="25%">{__('Teacher')}</th>
                        <th width="20%">{__('Class')}</th>
                        <th width="20%">{__("Overall review")}</th>
                        <th width="15%">{__("Average")}</th>
                        <th width="13%">{__("Actions")}</th>
                    </tr>
                    </thead>
                    <tbody>
                    {$idx = 1}
                    {foreach $rows['review_detail'] as $row}
                        <tr>
                            <td align="center" class="align_middle">{$idx}</td>
                            <td class="align_middle">
                                    <span class="name js_user-popover" data-uid="{$row['user_id']}">
                                        <a href="{$system['system_url']}/{$row['user_name']}">{$row['user_fullname']}</a>
                                    </span>
                            </td>
                            <td class="align_middle">{$row['group_title']}</td>
                            <td align="center" class="align_middle">{$row['total_review']}</td>
                            <td  align="center" class="align_middle">{$row['average_review']}</td>
                            <td align="center" class="align_middle">
                                <a class="btn btn-xs btn-default" href="{$system['system_url']}/noga/statistics/reviewteacher/{$row['group_id']}/{$row['user_id']}">{__("Detail")}</a>
                            </td>
                        </tr>
                        {$idx = $idx + 1}
                    {/foreach}

                    </tbody>
                </table>
            </div>
        </div>

    {elseif $sub_view == "reviewteacher"}

        <div class="panel-body with-table">

            <div class="pl10">
                <strong>{__("Teacher")} &#58;&nbsp; </strong> {$rows['teacher']['user_fullname']} <br>
                <strong>{__("Class")}&#58;&nbsp;</strong> {$rows['class']['group_title']} <br>
                <strong>{__("Overall review")}&#58;&nbsp;</strong> {$rows['reviews']|count}
            </div>
            <br>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover js_dataTable">
                    <thead>
                    <tr>
                        <th width="7%">#</th>
                        <th width="25%">{__('Reviewer')}</th>
                        <th width="15%">{__('Rating')}</th>
                        <th width="40%">{__("Review content")}</th>
                        <th width="13%">{__("Time")}</th>
                    </tr>
                    </thead>
                    <tbody>
                    {$idx = 1}
                    {foreach $rows['reviews'] as $row}
                        <tr>
                            <td align="center" class="align_middle">{$idx}</td>
                            <td class="align_middle">
                                <span class="name js_user-popover" data-uid="{$row['user_id']}">
                                    <a href="{$system['system_url']}/{$row['user_name']}">{$row['user_fullname']}</a>
                                </span>
                            </td>
                            <td align="center" class="align_middle">{$row['rating']}</td>
                            <td class="align_middle">
                                {$row['comment']}
                            </td>
                            <td align="center" class="align_middle">{$row['time']}</td>
                        </tr>
                        {$idx = $idx + 1}
                    {/foreach}

                    </tbody>
                </table>
            </div>

        </div>

    {/if}
</div>