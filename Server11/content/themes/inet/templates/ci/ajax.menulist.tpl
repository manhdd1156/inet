<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover js_dataTable">
        <thead>
        <tr><th colspan="7">{__("Menu list")}&nbsp;(<span class="count_menu">{$results['menu']|count}</span>)</th></tr>
            <tr>
                <th>
                    {__("#")}
                </th>
                <th>
                    {__("Menu name")}
                </th>
                <th>
                    {__("Begin")}
                </th>
                <th>
                    {__("Scope")}
                </th>
                <th>
                    {__("Use meal")}
                </th>
                <th>
                    {__("Study saturday")}
                </th>
                <th>
                    {__("Actions")}
                </th>
            </tr>
        </thead>
        <tbody>
        {$idx = 1}
        {foreach $results['menu'] as $row}
            <tr>
                <td class="align-middle" align="center">
                    {$idx}
                </td>
                <td class="align-middle">
                    <a href="{$system['system_url']}/school/{$results['username']}/menus/detail/{$row['menu_id']}">{$row['menu_name']}</a>
                </td>
                <td class="align-middle" align="center">
                    {$row['begin']}
                </td>
                <td class="align-middle" align="center">
                    {if $row['applied_for']==$smarty.const.SCHOOL_LEVEL}
                        {__("School")}
                    {elseif $row['applied_for']==$smarty.const.CLASS_LEVEL_LEVEL}
                        {foreach $class_levels as $cl}
                            {if $cl['class_level_id'] == $row['class_level_id']}{$cl['class_level_name']}{/if}
                        {/foreach}
                    {elseif $row['applied_for']==$smarty.const.CLASS_LEVEL}
                        {foreach $classes as $value}
                            {if $value['group_id'] == $row['class_id']}{$value['group_title']}{/if}
                        {/foreach}
                    {/if}
                </td>
                <td class="align-middle" align="center">
                    {if $row['is_meal']}
                        {__('Yes')}
                    {else}
                        {__('No')}
                    {/if}
                </td>
                <td class="align-middle" align="center">
                    {if $row['is_saturday']}
                        {__('Yes')}
                    {else}
                        {__('No')}
                    {/if}
                </td>
                <td class="align-middle">
                    {if $canEdit}
                        <a href="{$system['system_url']}/school/{$results['username']}/menus/copy/{$row['menu_id']}" class="btn btn-xs btn-default">{__("Copy")}</a>
                        <a href="{$system['system_url']}/school/{$results['username']}/menus/edit/{$row['menu_id']}" class="btn btn-xs btn-default">{__("Edit")}</a>
                        <button class="btn btn-xs btn-danger js_school-menu-delete" data-username="{$results['username']}" data-id="{$row['menu_id']}" data-handle = "delete_menu">{__("Delete")}</button>
                        {if !$row['is_notified']}
                            <div class = "form-label"><button class="btn btn-xs btn-default js_school-menu-notify" data-handle="notify" data-username="{$results['username']}" data-id="{$row['menu_id']}">{__("Notify")}</button></div>
                        {/if}
                    {/if}
                    <button class="btn btn-xs btn-success js_school-menu-export" data-username="{$results['username']}" data-id="{$row['menu_id']}" data-handle = "export">{__("Export to Excel")}</button>
                </td>
            </tr>
            {$idx = $idx + 1}
        {/foreach}

        {if $results['menu']|count == 0}
            <tr class="odd">
                <td valign="top" align="center" colspan="7" class="dataTables_empty">
                    {__("No data available in table")}
                </td>
            </tr>
        {/if}

        </tbody>
    </table>
</div>
<script>
    $(function() {
        // run DataTable
        $('.js_dataTable').DataTable({
            "aoColumnDefs": [ { 'aDataSort': false, 'aTargets': [ -1 ] } ],
            "language": {
                "decimal":        "",
                "emptyTable":     __["No data available in table"],
                "info":           __["Showing _START_ to _END_ of _TOTAL_ results"],
                "infoEmpty":      __["Showing 0 to 0 of 0 results"],
                "infoFiltered":   "(filtered from _MAX_ total entries)",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     __["Show"] + " " + "_MENU_" + " " + __["results"],
                "loadingRecords": __["Loading..."],
                "processing":     __["Processing..."],
                "search":         __["Search"],
                "zeroRecords":    __["No matching records found"],
                "paginate": {
                    "first":      __["First"],
                    "last":       __["Last"],
                    "next":       __["Next"],
                    "previous":   __["Previous"]
                },
                "aria": {
                    "sortAscending":  ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                }
            }
        });
    });
</script>