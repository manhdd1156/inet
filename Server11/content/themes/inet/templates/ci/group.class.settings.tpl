<form class="js_ajax-forms" data-url="ci/bo/school/bo_class.php">
    <input type="hidden" name="do" value="setting"/>
    <input type="hidden" name="school_username" id="school_username" value="{$school['page_name']}"/>
    <input type="hidden" name="class_id" value="{$group['group_id']}"/>
    <div class="form-group">
        <label for="title">{__("Class name")} (*):</label>
        <input type="text" class="form-control" name="title" id="title" placeholder="{__("Class name")}" value="{$group['group_title']}">
    </div>
    <div class="form-group">
        <label for="username">{__("Username")} (*):</label>
        <input type="text" class="form-control" name="username" id="username" placeholder="{__("Username, e.g. lopmam3")}" value="{$group['group_name']}">
    </div>
    <div class="form-group">
        <label for="class_level_id">{__("Class level")} (*):</label>
        <select class="form-control" name="class_level_id">
            {foreach $class_levels as $class_level}
                <option {if $group['class_level_id']==$class_level['class_level_id']}selected{/if} value="{$class_level['class_level_id']}">{$class_level['class_level_name']}</option>
            {/foreach}
        </select>
    </div>
    <div class="form-group">
        <label for="telephone">{__("Telephone")}:</label>
        <input type="text" class="form-control" name="telephone" id="telephone" placeholder="{__("Telephone")}" value="{$group['telephone']}">
    </div>
    <div class="form-group">
        <label for="email">{__("Email")}:</label>
        <input type="text" class="form-control" name="email" id="email" placeholder="{__("Email")}" value="{$group['email']}">
    </div>
    <div class="form-group">
        <label for="description">{__("Description")}:</label>
        <textarea class="form-control" name="description" id="description" placeholder="{__("Write about your class...")}" rows="8">{$group['group_description']}</textarea>
    </div>

    <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>

    <!-- success -->
    <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
    <!-- success -->

    <!-- error -->
    <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
    <!-- error -->
</form>
