<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <i class="fa fa-university fa-fw fa-lg pr10"></i>
        {__("School")}
        {*{if $sub_view == "edit"}*}
            {*&rsaquo; {$data['page_title']}*}
            {*{if $configure == 'basic'} &rsaquo; {__("Basic")}*}
            {*{elseif $configure == 'services'} &rsaquo; {__("Service")}*}
            {*{elseif $configure == 'addmission'} &rsaquo; {__("Admission")}*}
            {*{elseif $configure == 'info'} &rsaquo; {__("More information")}*}
            {*{/if}*}
        {*{/if}*}
    </div>
    {if $sub_view == ""}
        <div class="panel-body with-table">
            <div id="school_list">
                {include file="ci/region/ajax.region.schoollist.tpl"}
            </div>
        </div>
    {/if}
</div>