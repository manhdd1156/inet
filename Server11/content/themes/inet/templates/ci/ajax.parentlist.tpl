<ul>
    {foreach $results as $_user}
        <li class="feeds-item" data-id="{$_user['user_name']}">
            <input type="hidden" name="user_id[]" value="{$_user['user_id']}"/>
            <div class="data-container small">
                <a href="{$system['system_url']}/{$_user['user_name']}">
                    <img class="data-avatar" src="{$_user['user_picture']}" alt="{$_user['user_fullname']}">
                </a>
                <div class="data-content">
                    <div class="pull-right flip">
                        <div class="btn btn-default js_parent-set-admin {if $_user['user_id'] == $child_admin}x-hidden{/if}" data-uid="{$_user['user_id']}">{__("Set admin")}</div> <div class="btn btn-default js_parent-remove {if $_user['user_id'] == $child_admin}x-hidden{/if}" data-uid="{$_user['user_id']}">{__("Remove")}</div>
                    </div>
                    <div>
                        <span class="name js_user-popover" data-uid="{$_user['user_id']}">
                            <a href="{$system['system_url']}/{$_user['user_name']}">{$_user['user_fullname']}</a>
                        </span><br/>
                        {$_user['user_phone']}&nbsp;&nbsp;&nbsp;{$_user['user_email']}
                    </div>
                </div>
            </div>
        </li>
    {/foreach}
</ul>