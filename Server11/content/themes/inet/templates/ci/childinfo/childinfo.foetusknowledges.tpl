<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <div class="pull-right flip">
            {if $sub_view == "detail"}
                <a href="{$system['system_url']}/childinfo/{$child['child_parent_id']}/foetusknowledges" class="btn btn-default">
                    <i class="fa fa-plus"></i> {__("Lists")}
                </a>
            {/if}
        </div>
        <i class="fa fa-file-text-o fa-fw fa-lg pr10"></i>
        {__('Foetus knowledge')}
        {if $sub_view == "detail"}
            &rsaquo; {__('Detail')}
        {/if}
    </div>
    {if $sub_view == ""}
        <div class="panel-body with-table form-horizontal">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover js_dataTable">
                    <thead>
                    <tr>
                        <th colspan="4">{__("Foetus knowledge")}</th>
                    </tr>
                    <tr>
                        <th>{__('No.')}</th>
                        <th>{__('Title')}</th>
                        <th>{__('Link')}</th>
                    </tr>
                    </thead>
                    <tbody>
                    {foreach $foetus_knowledge as $k => $row}
                        <tr>
                            <td align="center">{$k + 1}</td>
                            <td align="center">{$row['title']}</td>
                            <td>
                                <a href="{$row['link']}" target="_blank">{$row['link']}</a>
                            </td>
                        </tr>
                    {/foreach}
                    </tbody>
                </table>
            </div>
        </div>
    {/if}
</div>