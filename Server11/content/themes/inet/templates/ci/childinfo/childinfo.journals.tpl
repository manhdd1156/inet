<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <div class="pull-right flip">
            {if $sub_view == ""}
                <a href="{$system['system_url']}/childinfo/{$child['child_parent_id']}/journals/add" class="btn btn-default">
                    <i class="fa fa-plus"></i> {__("Add New")}
                </a>
            {elseif $sub_view == "add" || $sub_view == "edit"}
                <a href="{$system['system_url']}/childinfo/{$child['child_parent_id']}/journals" class="btn btn-default">
                    <i class="fa fa-list"></i> {__("Lists")}
                </a>
            {/if}
        </div>
        <i class="fa fa-child fa-fw fa-lg pr10"></i>
        {__('Diary corner')} &rsaquo; {$child['child_name']}
        {if $sub_view == "edit"}
            &rsaquo; {__('Edit')}
        {elseif $sub_view == ""}
            &rsaquo; {__('Lists')}
        {elseif $sub_view == "add"}
            &rsaquo; {__('Add New')}
        {/if}
    </div>
    {if $sub_view == ""}
        <div class="panel-body with-table form-horizontal">
            <div class="form-group">
                <label class="col-sm-3 control-label text-left">{__("Year")}</label>
                <div class="col-sm-3">
                    <select name="year" id="year" class="form-control">
                        <option value = "0">{__("Select year ...")}</option>
                        {for $i = $year_begin; $i < $year_end; $i++}
                            <option value="{$i}">{$i}</option>
                        {/for}
                    </select>
                </div>
                <div class="col-sm-3">
                    <a class="btn btn-default js_child-journal-search" data-id="{$child['child_parent_id']}" data-handle = "search">{__("Search")}</a>
                </div>
            </div>

            <div id = "journal_list">
                {include file="ci/ajax.journallist.tpl"}
            </div>
        </div>
    {elseif $sub_view == "edit"}
        <div class="panel-body">
            <form class="form-horizontal" enctype="multipart/form-data" method="post" id="add_child_journal">
                <input type="hidden" name="child_parent_id" value="{$child['child_parent_id']}"/>
                <input type="hidden" name="child_journal_id" value="{$data['child_journal_id']}"/>
                <input type="hidden" name="do" value="edit"/>
                <div class = "form-group">
                    <label class="col-sm-3 control-label text-left">{__("Caption")}</label>
                    <div class="col-sm-9">
                        <input type = "text" class="form-control" name="caption" id="caption" placeholder="{__("Caption")}" value = "{$data['caption']}">
                    </div>
                </div>
                <input type = "hidden" id = "is_file" name = "is_file" value = "1">
                <div class="form-group" id = "file_old">
                    <label class="rp-file col-sm-3 control-label text-left" style="padding-top: 0px">{__("Picture")}</label>
                    <div class="col-sm-6">
                        {if !is_empty($data['source_file'])}
                            <a href="{$data['source_file']}" target="_blank"><img src = "{$data['source_file']}" class = "img-responsive"></a>
                        {else} {__('No picture')}
                        {/if}
                    </div>
                </div>
                <div class = "form-group">
                    <label class="control-label col-sm-3">
                        {if is_empty($data['source_file'])}
                            {__("Choose picture")}
                        {else}
                            {__("Change picture")}
                        {/if}
                    </label>
                    <div class = "col-sm-6">
                        <input type="file" name="file" id="file"/>
                    </div>
                    <a class = "delete_image btn btn-default text-left">{__('Delete')}</a>
                </div>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->
                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    {elseif $sub_view == "add"}
        <div class="panel-body">
            <form class="form-horizontal" enctype="multipart/form-data" method="post" id="add_child_journal">
                <input type="hidden" name="child_parent_id" value="{$child['child_parent_id']}"/>
                <input type="hidden" name="do" value="add"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Caption")}</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="caption" placeholder="{__("Caption")}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Picture")}</label>
                    <div class="col-sm-6">
                        <input name="file[]" type="file" multiple="true">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->
                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    {/if}
</div>