<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <div class="pull-right flip">
            {if $sub_view == "detail"}
                <a href="{$system['system_url']}/childinfo/{$child['child_parent_id']}/childdevelopments" class="btn btn-default">
                    <i class="fa fa-list"></i> {__("Lists")}
                </a>
            {/if}
        </div>
        <i class="fa fa-child fa-fw fa-lg pr10"></i>
        {__('Child development')}
        {if $sub_view == "detail"}
            &rsaquo; {__('Detail')}
        {/if}
    </div>
    {if $sub_view == ""}
        <div class="panel-body with-table form-horizontal">
            <div class="row" align="center">
                <button class="js_general-show btn btn-default selected_chart">{__("General")}</button>
                <button class="js_missed-show btn btn-default">{__("Missed")}</button>
                <button class="js_vaccinating-show btn btn-default">{__("Vaccinating")}</button>
            </div>
            <br/>
            <div id="general">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover js_dataTable">
                        <thead>
                        <tr>
                            <th colspan="5">{__("General")} ({count($child_development)})</th>
                        </tr>
                        <tr>
                            <th>{__('No.')}</th>
                            <th>{__("Time")}</th>
                            <th>{__('Title')}</th>
                            <th>{__('Status')}</th>
                            <th>{__('Actions')}</th>
                        </tr>
                        </thead>
                        <tbody>
                        {foreach $child_development as $k => $row}
                            <tr>
                                <td align="center">{$k + 1}</td>
                                <td align="center">{$row['time']}</td>
                                <td><a href = "{$system['system_url']}/childinfo/{$child['child_parent_id']}/childdevelopments/detail/{$row['child_development_id']}">{$row['title']}</a></td>
                                <td align="center">
                                    {if $row['type'] == 1}
                                        {if $row['vaccinationed']}
                                            <div><strong>{__('Vaccinationed')}</strong></div>
                                        {else}
                                            <div><strong>{__('Not vaccinating')}</strong></div>
                                        {/if}
                                    {/if}
                                </td>
                                <td align="center">
                                    {if $row['type'] == 1}
                                        {if !$row['vaccinationed']}
                                            <a class = "btn btn-success btn-xs js_child-vaccinationed" href="#" data-id="{$row['child_development_id']}" data-child="{$child['child_parent_id']}">{__("Vaccination")}</a>
                                        {else}
                                            <a class = "btn btn-danger btn-xs js_child-cancel-vaccination" href="#" data-id="{$row['child_development_id']}" data-child="{$child['child_parent_id']}">{__("Cancel vaccination")}</a>
                                        {/if}
                                    {/if}
                                </td>
                            </tr>
                        {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
            <div id="missed">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover js_dataTable">
                        <thead>
                        <tr>
                            <th colspan="5">{__("Missed")} ({count($child_vaccination_history)})</th>
                        </tr>
                        <tr>
                            <th>{__('No.')}</th>
                            <th>{__("Time")}</th>
                            <th>{__('Title')}</th>
                            <th>{__('Status')}</th>
                            <th>{__('Actions')}</th>
                        </tr>
                        </thead>
                        <tbody>
                        {foreach $child_vaccination_history as $k => $row}
                            <tr>
                                <td align="center">{$k + 1}</td>
                                <td align="center">{$row['time']}</td>
                                <td><a href = "{$system['system_url']}/childinfo/{$child['child_parent_id']}/childdevelopments/detail/{$row['child_development_id']}">{$row['title']}</a></td>
                                <td align="center">
                                    {if $row['type'] == 1}
                                        {if $row['vaccinationed']}
                                            <div><strong>{__('Vaccinationed')}</strong></div>
                                        {else}
                                            <div><strong>{__('Not vaccinating')}</strong></div>
                                        {/if}
                                    {/if}
                                </td>
                                <td align="center">
                                    {if $row['type'] == 1}
                                        {if !$row['vaccinationed']}
                                            <a class = "btn btn-success btn-xs js_child-vaccinationed" href="#" data-id="{$row['child_development_id']}" data-child="{$child['child_parent_id']}">{__("Vaccination")}</a>
                                        {else}
                                            <a class = "btn btn-danger btn-xs js_child-cancel-vaccination" href="#" data-id="{$row['child_development_id']}" data-child="{$child['child_parent_id']}">{__("Cancel vaccination")}</a>
                                        {/if}
                                    {/if}
                                </td>
                            </tr>
                        {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
            <div id="vaccinating">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover js_dataTable">
                        <thead>
                        <tr>
                            <th colspan="5">{__("Vaccinating")} ({count($child_vaccinating)})</th>
                        </tr>
                        <tr>
                            <th>{__('No.')}</th>
                            <th>{__("Time")}</th>
                            <th>{__('Title')}</th>
                            <th>{__('Status')}</th>
                            <th>{__('Actions')}</th>
                        </tr>
                        </thead>
                        <tbody>
                        {foreach $child_vaccinating as $k => $row}
                            <tr>
                                <td align="center">{$k + 1}</td>
                                <td align="center">{$row['time']}</td>
                                <td><a href = "{$system['system_url']}/childinfo/{$child['child_parent_id']}/childdevelopments/detail/{$row['child_development_id']}">{$row['title']}</a></td>
                                <td align="center">
                                    {if $row['type'] == 1}
                                        {if $row['vaccinationed']}
                                            <div><strong>{__('Vaccinationed')}</strong></div>
                                        {else}
                                            <div><strong>{__('Not vaccinating')}</strong></div>
                                        {/if}
                                    {/if}
                                </td>
                                <td align="center">
                                    {if $row['type'] == 1}
                                        {if !$row['vaccinationed']}
                                            <a class = "btn btn-success btn-xs js_child-vaccinationed" href="#" data-id="{$row['child_development_id']}" data-child="{$child['child_parent_id']}">{__("Vaccination")}</a>
                                        {else}
                                            <a class = "btn btn-danger btn-xs js_child-cancel-vaccination" href="#" data-id="{$row['child_development_id']}" data-child="{$child['child_parent_id']}">{__("Cancel vaccination")}</a>
                                        {/if}
                                    {/if}
                                </td>
                            </tr>
                        {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <script>
            function stateChange() {
                setTimeout(function () {
                    $("#missed").hide();
                    $("#vaccinating").hide();
                }, 500);
            }
            stateChange();
        </script>
    {elseif $sub_view == "detail"}
        <div class="panel-body with-table form-horizontal">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <tbody>
                    <tr>
                        <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Month")}/{__("Day")}</strong></td>
                        <td>{$data['month_push']}/{$data['day_push']}</td>
                    </tr>
                    <tr>
                        <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Title")}</strong></td>
                        <td>{$data['title']}</td>
                    </tr>
                    {if $data['content_type'] != 2}
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Content")}</strong></td>
                            <td>{$data['content']}</td>
                        </tr>
                    {/if}
                    {if $data['content_type'] != 1}
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Link")}</strong></td>
                            <td><a href = "{$data['link']}" target="_blank"><strong>{__("View detail")}</strong></a></td>
                        </tr>
                    {/if}
                    <tr>
                        <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Type")}</strong></td>
                        <td>{if $data['type'] == 1} {__("Vaccination")} {else} {__("Information")} {/if}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    {elseif $sub_view == "referion"}
        <div class="panel-body with-table form-horizontal">
            <div class="row" align="center">
                <button class="js_general-show btn btn-default selected_chart">{__("General")}</button>
                <button class="js_missed-show btn btn-default">{__("Missed knowledge")}</button>
                <button class="js_vaccinating-show btn btn-default">{__("Comming knowledge")}</button>
            </div>
            <br/>
            <div id="general">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover js_dataTable">
                        <thead>
                        <tr>
                            <th colspan="3">{__("General")} ({count($child_development)})</th>
                        </tr>
                        <tr>
                            <th>{__('No.')}</th>
                            <th>{__("Time")}</th>
                            <th>{__('Title')}</th>
                        </tr>
                        </thead>
                        <tbody>
                        {foreach $child_development as $k => $row}
                            <tr>
                                <td align="center">{$k + 1}</td>
                                <td align="center">{$row['time']}</td>
                                <td align="center"><a href = "{$system['system_url']}/childinfo/{$child['child_parent_id']}/childdevelopments/detail/{$row['child_development_id']}">{$row['title']}</a></td>
                            </tr>
                        {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
            <div id="missed">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover js_dataTable">
                        <thead>
                        <tr>
                            <th colspan="3">{__("Missed knowledge")} ({count($child_info_history)})</th>
                        </tr>
                        <tr>
                            <th>{__('No.')}</th>
                            <th>{__("Time")}</th>
                            <th>{__('Title')}</th>
                        </tr>
                        </thead>
                        <tbody>
                        {foreach $child_info_history as $k => $row}
                            <tr>
                                <td align="center">{$k + 1}</td>
                                <td align="center">{$row['time']}</td>
                                <td align="center"><a href = "{$system['system_url']}/childinfo/{$child['child_parent_id']}/childdevelopments/detail/{$row['child_development_id']}">{$row['title']}</a></td>
                            </tr>
                        {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
            <div id="vaccinating">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover js_dataTable">
                        <thead>
                        <tr>
                            <th colspan="3">{__("Comming knowledge")} ({count($info_future)})</th>
                        </tr>
                        <tr>
                            <th>{__('No.')}</th>
                            <th>{__("Time")}</th>
                            <th>{__('Title')}</th>
                        </tr>
                        </thead>
                        <tbody>
                        {foreach $info_future as $k => $row}
                            <tr>
                                <td align="center">{$k + 1}</td>
                                <td align="center">{$row['time']}</td>
                                <td align="center"><a href = "{$system['system_url']}/childinfo/{$child['child_parent_id']}/childdevelopments/detail/{$row['child_development_id']}">{$row['title']}</a></td>
                            </tr>
                        {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <script>
            function stateChange() {
                setTimeout(function () {
                    $("#missed").hide();
                    $("#vaccinating").hide();
                }, 500);
            }
            stateChange();
        </script>
    {/if}
</div>