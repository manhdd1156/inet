<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <div class="pull-right flip">
            {if $sub_view == ""}
                {if $child['is_pregnant']}
                    <a href="{$system['system_url']}/childinfo/{$child['child_parent_id']}/health/addfoetus" class="btn btn-default">
                        <i class="fa fa-pencil-square-o"></i> {__("Add foetus information")}
                    </a>
                {else}
                    <a href="{$system['system_url']}/childinfo/{$child['child_parent_id']}/health/addgrowth" class="btn btn-default">
                        <i class="fa fa-plus"></i> {__("Add new growth")}
                    </a>
                {/if}
            {elseif ($sub_view == "addfoetus" || $sub_view == "editfoetus" || $sub_view == "addgrowth")}
                <a href="{$system['system_url']}/childinfo/{$child['child_parent_id']}/health" class="btn btn-default">
                    <i class="fa fa-list"></i> {__("Lists")}
                </a>
            {/if}
        </div>
        <i class="fa fa-child fa-fw fa-lg pr10"></i>
        {__("Health information")}
        {if $sub_view == "edit"}
            &rsaquo; {__('Detail')} &rsaquo; {$child['child_name']}
        {elseif $sub_view == ""}
            &rsaquo; {$child['child_name']}
        {elseif $sub_view == ""}
            &rsaquo; {__('Add new growth')}&rsaquo;{$child['child_name']}
        {/if}
    </div>
    {if $sub_view == ""}
        <div class = "panel-body">
            {if !$child['is_pregnant']}
                {*Ô search*}
                <div class="js_ajax-forms form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Time")}</label>
                        <div class='col-sm-3'>
                            <div class='input-group date' id='begin_chart_picker'>
                                <input type='text' name="begin" class="form-control" id="begin"/>
                                <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                            </div>
                        </div>
                        <div class='col-sm-1'><i class="fas fa-long-arrow-alt-right fa-lg fa-fw pr10 pt10"></i></div>
                        <div class='col-md-3'>
                            <div class='input-group date' id='end_chart_picker'>
                                <input type='text' name="end" class="form-control" id="end"/>
                                <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-3">
                            <button type="submit" class="btn btn-default js_child-chart-search" data-child="{$child['child_parent_id']}">{__("Search")}</button>
                        </div>
                    </div>

                    <div id="chart_list" name="chart_list">
                        {include file="ci/childinfo/ajax.child.chartsearch.tpl"}
                    </div>
                    <!-- success -->
                    <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                    <!-- error -->
                    <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                </div>
            {else}
                <div>
                    <div class = "panel-body">
                        <div class="row">
                            <button class="js_hight-chart-show btn btn-default selected_chart">{__("Height")}</button>
                            <button class="js_weight-chart-show btn btn-default">{__("Weight")}</button>
                        </div>
                        <br/>
                        <script type="text/javascript" src="{$system['cdn_url']}/includes/assets/js/chart/Chart.bundle.min.js"></script>
                        <script type="text/javascript" src="{$system['cdn_url']}/includes/assets/js/chart/Chart.bundle.js"></script>
                        <script type="text/javascript" src="{$system['cdn_url']}/includes/assets/js/chart/utils.js"></script>

                        <div id="hight_chart">
                            <div class="content">
                                <div class="wrapper">
                                    <canvas id="chart-height"></canvas>
                                </div>
                            </div>
                        </div>
                        <div id="weight_chart">
                            <div class="content">
                                <div class="wrapper">
                                    <canvas id="chart-weight"></canvas>
                                </div>
                            </div>
                        </div>
                        <script>
                            function stateChange() {
                                setTimeout(function () {
                                    var presets = window.chartColors;
                                    var utils = Samples.utils;
                                    var data_height = {
                                        labels: [{$label}],
                                        datasets: [{
                                            backgroundColor: presets.blue,
                                            borderColor: presets.blue,
                                            data: [{$heightForChart}],
                                            hidden: false,
                                            label: __["Height"],
                                            fill: false,
                                            pointRadius: 3
                                        }, {
                                            backgroundColor: utils.transparentize(presets.white),
                                            borderColor: presets.orange,
                                            data: [{$heightStandardForChart}],
                                            hidden: false,
                                            label: __["Standard"]
                                            // fill: 'end'
                                        }]
                                    };

                                    var options = {
                                        maintainAspectRatio: false,
                                        spanGaps: true,
                                        elements: {
                                            line: {
                                                tension: 0.000001
                                            },
                                            point:{
                                                radius: 0
                                            }
                                        },
                                        scales: {
                                            yAxes: [{
                                                stacked: false
                                            }]
                                        },
                                        plugins: {
                                            filler: {
                                                propagate: false
                                            },
                                            samples_filler_analyser: {
                                                target: 'chart-analyser'
                                            }
                                        }
                                    };

                                    var data_weight = {
                                        labels: [{$label}],
                                        datasets: [{
                                            backgroundColor: presets.blue,
                                            borderColor: presets.blue,
                                            data: [{$weightForChart}],
                                            hidden: false,
                                            label: __["Weight"],
                                            fill: false,
                                            pointRadius: 3
                                        }, {
                                            backgroundColor: utils.transparentize(presets.white),
                                            borderColor: presets.orange,
                                            data: [{$weightStandardForChart}],
                                            hidden: false,
                                            label: __["Standard"]
                                            // fill: 'end'
                                        }]
                                    };

                                    var chart_height = new Chart('chart-height', {
                                        type: 'line',
                                        data: data_height,
                                        options: options
                                    });

                                    var chart_weight = new Chart('chart-weight', {
                                        type: 'line',
                                        data: data_weight,
                                        options: options
                                    });
                                }, 1000);
                            }
                            stateChange();
                        </script>

                        <script>
                            function stateChange() {
                                setTimeout(function () {
                                    $("#weight_chart").hide();
                                    $("#bmi_chart").hide();
                                }, 1000);
                            }
                            stateChange();
                        </script>

                        <div class="" align="center">
                            <strong>{__("Note: The chart only shows information from week 8")}</strong>
                        </div>
                        <br/>
                        <table class="table table-striped table-bordered table-hover js_dataTable">
                            <thead>
                            <tr>
                                <th colspan="6">{__("Information foetus health")}&nbsp;({$childFoetusGrowth|count})</th>
                            </tr>
                            <tr>
                                <th>{__('No.')}</th>
                                <th>{__("Date")}</th>
                                <th>{__('Gestation age')} ({__("Week")})</th>
                                <th>{__('Height')} (mm)</th>
                                <th>{__("Weight")} (g)</th>
                                <th>{__("Actions")}</th>
                            </tr>
                            </thead>
                            <tbody>
                            {foreach $childFoetusGrowth as $k => $row}
                                <tr>
                                    <td align="center">{$k + 1}</td>
                                    <td align="center"><a href = "{$system['system_url']}/childinfo/{$child['child_parent_id']}/health/editfoetus/{$row['child_foetus_growth_id']}">{$row['recorded_at']}</a></td>
                                    <td align="center">{$row['pregnant_week']}</td>
                                    <td align="center">{$row['from_head_to_hips_length']}</td>
                                    <td align="center">{$row['weight']}</td>
                                    <td align="center">
                                        <a href ="{$system['system_url']}/childinfo/{$child['child_parent_id']}/health/editfoetus/{$row['child_foetus_growth_id']}" class = "btn btn-xs btn-default">
                                            {__("Edit")}
                                        </a>
                                        <a href ="#" class="btn btn-danger btn-xs js_child-health-delete" data-id="{$row['child_foetus_growth_id']}" data-handle="delete_foetus" data-child="{$child['child_parent_id']}">
                                            {__("Delete")}
                                        </a>
                                    </td>
                                </tr>
                            {/foreach}
                            </tbody>
                        </table>
                    </div>
                </div>
            {/if}
        </div>
    {elseif $sub_view == "editgrowth"}
        <div class = "panel-body">
            <form class="form-horizontal" enctype="multipart/form-data" method="post" id="edit_child_growth">
                <input type="hidden" name="child_growth_id" value="{$data['child_growth_id']}"/>
                <input type="hidden" name="child_parent_id" value="{$child['child_parent_id']}"/>
                <input type="hidden" name="do" value="edit_growth"/>
                <div class="form-group">
                    <label class = "col-sm-3 control-label text-left">{__("Date")} (*)</label>
                    <div class='col-sm-3'>
                        <div class='input-group date' id='birthdate_picker'>
                            <input type='text' name="recorded_at" value = "{$data['recorded_at']}" id="recorded_at" class="form-control"/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                </div>
                <div class = "form-group">
                    <label class="col-sm-3 control-label text-left">{__("Height")}</label>
                    <div class="col-sm-3">
                        <input type="number" min="0" step = "any" class="form-control" name="height" value="{$data['height']}" placeholder="{__("Height")}">
                    </div>
                    <label class="col-sm-2 control-label text-left" style = "text-align: left">{__("cm")}</label>
                </div>
                <div class = "form-group">
                    <label class="col-sm-3 control-label">{__("Weight")}</label>
                    <div class="col-sm-3">
                        <input type="number" min="0" step = "any" class="form-control" name="weight" value="{$data['weight']}" placeholder="{__("Weight")}">
                    </div>
                    <label class="col-sm-2 control-label text-left" style = "text-align: left">{__("kg")}</label>
                </div>
                <div class = "form-group">
                    <label class="col-sm-3 control-label">{__("Nutritional status")}</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="nutriture_status" value="{$data['nutriture_status']}">
                    </div>
                </div>
                <div class = "form-group">
                    <label class="col-sm-3 control-label">{__("Heartbeat")}</label>
                    <div class="col-sm-5">
                        <input type="number" class="form-control" name="heart" min="0" step="1" value="{$data['heart']}">
                    </div>
                    <label class="col-sm-2 control-label">{__("Times/minute")}</label>
                </div>
                <div class = "form-group">
                    <label class="col-sm-3 control-label">{__("Blood pressure")}</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="blood_pressure" value="{$data['blood_pressure']}">
                    </div>
                </div>
                <div class = "form-group">
                    <label class="col-sm-3 control-label">{__("Ear")}</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="ear" value="{$data['ear']}">
                    </div>
                </div>
                <div class = "form-group">
                    <label class="col-sm-3 control-label">{__("Eye")}</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="eye" value="{$data['eye']}">
                    </div>
                </div>
                <div class = "form-group">
                    <label class="col-sm-3 control-label">{__("Nose")}</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="nose" value="{$data['nose']}">
                    </div>
                </div>
                <div class = "form-group">
                    <label class="col-sm-3 control-label">{__("Description")}</label>
                    <div class="col-sm-9">
                        <textarea type="text" class="form-control" name="description">{$data['description']}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("File attachment")}</label>
                    <div class="col-sm-6">
                        {if !is_empty($data['source_file'])}
                            <a href="{$data['source_file']}" target="_blank"><img src = "{$data['source_file']}" class = "img-responsive"></a>
                            <br>
                            <label class="control-label">{__("Choose file replace")}</label>
                            <br>
                        {/if}
                        <input type="file" name="file" id="file"/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-default">{__("Save")}</button>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->
                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    {elseif $sub_view == "addgrowth"}
        <div class="panel-body">
            <form class="form-horizontal" enctype="multipart/form-data" method="post" id="edit_child_growth">
                <input type="hidden" name="child_parent_id" value="{$child['child_parent_id']}"/>
                <input type="hidden" name="do" value="add_growth"/>
                <div class="form-group">
                    <label class = "col-sm-3 control-label text-left">{__("Date")} (*)</label>
                    <div class='col-sm-3'>
                        <div class='input-group date' id='birthdate_picker'>
                            <input type='text' name="recorded_at" id="recorded_at" class="form-control"/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Height")}</label>
                    <div class="col-sm-3">
                        <input type="number" min="0" step = "any" class="form-control" name="height">
                    </div>
                    <label class="col-sm-1 control-label text-left" style = "text-align: left">{__("cm")}</label>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">{__("Weight")}</label>
                    <div class="col-sm-3">
                        <input type="number" min="0" step = "any" class="form-control" name="weight">
                    </div>
                    <label class="col-sm-1 control-label text-left" style = "text-align: left">{__("kg")}</label>
                </div>
                <div class = "form-group">
                    <label class="col-sm-3 control-label">{__("Nutritional status")}</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="nutriture_status">
                    </div>
                </div>
                <div class = "form-group">
                    <label class="col-sm-3 control-label">{__("Heartbeat")}</label>
                    <div class="col-sm-5">
                        <input type="number" class="form-control" name="heart" min="0" step="1">
                    </div>
                    <label class="col-sm-2 control-label">{__("Times/minute")}</label>
                </div>
                <div class = "form-group">
                    <label class="col-sm-3 control-label">{__("Blood pressure")}</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="blood_pressure">
                    </div>
                </div>
                <div class = "form-group">
                    <label class="col-sm-3 control-label">{__("Ear")}</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="ear">
                    </div>
                </div>
                <div class = "form-group">
                    <label class="col-sm-3 control-label">{__("Eye")}</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="eye">
                    </div>
                </div>
                <div class = "form-group">
                    <label class="col-sm-3 control-label">{__("Nose")}</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="nose">
                    </div>
                </div>
                <div class = "form-group">
                    <label class="col-sm-3 control-label">{__("Description")}</label>
                    <div class="col-sm-9">
                        <textarea type="text" class="form-control" name="description"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("File attachment")}</label>
                    <div class="col-sm-6">
                        <input type="file" name="file" id="file"/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-default">{__("Save")}</button>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->
                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    {elseif $sub_view == "addfoetus"}
        <div class="panel-body">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/child/bochild_health.php">
                <input type="hidden" name="school_username" value="{$username}"/>
                <input type="hidden" name="child_parent_id" value="{$child['child_parent_id']}"/>
                <input type="hidden" name="do" value="add_foetus"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Date")} (*)</label>
                    <div class='col-sm-3'>
                        <div class='input-group date' id='birthdate_picker'>
                            <input type='text' name="recorded_at" id="recorded_at" class="form-control" required/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Gestation age")}</label>
                    <div class="col-sm-3">
                        <input type="number" min="0" class="form-control" name="pregnant_week" min="0" step="1">
                    </div>
                    <label class = "col-sm-2 control-label" style = "text-align: left"> {__("Week")} </label>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Due date of childbearing")}</label>
                    <div class='col-sm-3'>
                        <div class='input-group date' id='due_date_of_childbearing_picker'>
                            <input type='text' name="due_date_of_childbearing" id="due_date_of_childbearing" value="{$child['due_date_of_childbearing']}" class="form-control"/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                </div>
                <div class = "form-group">
                    <label class="col-sm-3 control-label text-left">{__("Femus length")}</label>
                    <div class="col-sm-2">
                        <input type = "number" min="0" step="any" class="form-control" name="femus_length" id="femus_length">
                    </div>
                    <label class="col-sm-1 control-label" style = "text-align: left">mm</label>
                </div>
                <div class = "form-group">
                    <label class="col-sm-3 control-label text-left">{__("From head to hips length")}</label>
                    <div class="col-sm-2">
                        <input type = "number" min="0" step="any" class="form-control" name="from_head_to_hips_length" id="from_head_to_hips_length">
                    </div>
                    <label class="col-sm-1 control-label" style = "text-align: left">mm</label>
                </div>
                <div class = "form-group">
                    <label class="col-sm-3 control-label text-left">{__("Weight")}</label>
                    <div class="col-sm-2">
                        <input type = "number" min="0" step="any" class="form-control" name="weight" id="weight">
                    </div>
                    <label class="col-sm-1 control-label" style = "text-align: left">g</label>
                </div>
                <div class = "form-group">
                    <label class="col-sm-3 control-label text-left">{__("Fetal heart")}</label>
                    <div class="col-sm-2">
                        <input type = "number" min="0" step="1" class="form-control" name="fetal_heart" id="fetal_heart">
                    </div>
                    <label class="col-sm-3 control-label" style = "text-align: left">{__('Times/minute')}</label>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Doctor contact information")}</label>
                </div>
                <div class = "form-group">
                    <label class="col-sm-3 control-label text-left">{__("Hospital")}</label>
                    <div class="col-sm-9">
                        <input type = "text" class="form-control" name="hospital" id="hospital">
                    </div>
                </div>
                <div class = "form-group">
                    <label class="col-sm-3 control-label text-left">{__("Hospital address")}</label>
                    <div class="col-sm-9">
                        <input type = "text" class="form-control" name="hospital_address" id="hospital_address">
                    </div>
                </div>
                <div class = "form-group">
                    <label class="col-sm-3 control-label text-left">{__("Doctor name")}</label>
                    <div class="col-sm-9">
                        <input type = "text" class="form-control" name="doctor_name" id="doctor_name">
                    </div>
                </div>
                <div class = "form-group">
                    <label class="col-sm-3 control-label text-left">{__("Doctor phone")}</label>
                    <div class="col-sm-9">
                        <input type = "text" class="form-control" name="doctor_phone" id="doctor_phone">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Description")}</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="description" placeholder="{__("Write about your child...")}" maxlength="300"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-default">{__("Save")}</button>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->
                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    {elseif $sub_view == "editfoetus"}
        <div class="panel-body">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/child/bochild_health.php">
                <input type="hidden" name="school_username" value="{$username}"/>
                <input type="hidden" name="child_parent_id" value="{$child['child_parent_id']}"/>
                <input type="hidden" name="child_foetus_growth_id" value="{$data['child_foetus_growth_id']}"/>
                <input type="hidden" name="do" value="edit_foetus"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Date")} (*)</label>
                    <div class='col-sm-3'>
                        <div class='input-group date' id='birthdate_picker'>
                            <input type='text' name="recorded_at" id="recorded_at" class="form-control" required value="{$data['recorded_at']}"/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Gestation age")}</label>
                    <div class="col-sm-3">
                        <input type="number" class="form-control" name="pregnant_week" value="{$data['pregnant_week']}" min="0" step="1">
                    </div>
                    <label class = "col-sm-2 control-label" style = "text-align: left"> {__("Week")} </label>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Due date of childbearing")}</label>
                    <div class='col-sm-3'>
                        <div class='input-group date' id='due_date_of_childbearing_picker'>
                            <input type='text' name="due_date_of_childbearing" id="due_date_of_childbearing" class="form-control" value="{$data['due_date_of_childbearing']}"/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                </div>
                <div class = "form-group">
                    <label class="col-sm-3 control-label text-left">{__("Femus length")}</label>
                    <div class="col-sm-2">
                        <input type = "number" min="0" step="any" class="form-control" name="femus_length" id="femus_length" value = "{$data['femur_length']}">
                    </div>
                    <label class="col-sm-1 control-label" style = "text-align: left">mm</label>
                </div>
                <div class = "form-group">
                    <label class="col-sm-3 control-label text-left">{__("From head to hips length")}</label>
                    <div class="col-sm-2">
                        <input type = "number" step="any" class="form-control" name="from_head_to_hips_length" id="from_head_to_hips_length" value = "{$data['from_head_to_hips_length']}">
                    </div>
                    <label class="col-sm-1 control-label" style = "text-align: left">mm</label>
                </div>
                <div class = "form-group">
                    <label class="col-sm-3 control-label text-left">{__("Weight")}</label>
                    <div class="col-sm-2">
                        <input type = "number" step="any" min="0" class="form-control" name="weight" id="weight" value = "{$data['weight']}">
                    </div>
                    <label class="col-sm-1 control-label" style = "text-align: left">g</label>
                </div>
                <div class = "form-group">
                    <label class="col-sm-3 control-label text-left">{__("Fetal heart")}</label>
                    <div class="col-sm-2">
                        <input type = "number" min="0" step="1" class="form-control" name="fetal_heart" id="fetal_heart" value = "{$data['fetal_heart']}">
                    </div>
                    <label class="col-sm-3 control-label" style = "text-align: left">{__('Times/minute')}</label>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Doctor contact information")}</label>
                </div>
                <div class = "form-group">
                    <label class="col-sm-3 control-label text-left">{__("Hospital")}</label>
                    <div class="col-sm-9">
                        <input type = "text" class="form-control" name="hospital" id="hospital" value = "{$data['hospital']}">
                    </div>
                </div>
                <div class = "form-group">
                    <label class="col-sm-3 control-label text-left">{__("Hospital address")}</label>
                    <div class="col-sm-9">
                        <input type = "text" class="form-control" name="hospital_address" id="hospital_address" value = "{$data['hospital_address']}">
                    </div>
                </div>
                <div class = "form-group">
                    <label class="col-sm-3 control-label text-left">{__("Doctor name")}</label>
                    <div class="col-sm-9">
                        <input type = "text" class="form-control" name="doctor_name" id="doctor_name" value = "{$data['doctor_name']}">
                    </div>
                </div>
                <div class = "form-group">
                    <label class="col-sm-3 control-label text-left">{__("Doctor phone")}</label>
                    <div class="col-sm-9">
                        <input type = "text" class="form-control" name="doctor_phone" id="doctor_phone" value = "{$data['doctor_phone']}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Description")}</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="description" placeholder="{__("Write about your child...")}" maxlength="300">{$data['description']}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-default">{__("Save")}</button>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->
                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    {/if}
</div>