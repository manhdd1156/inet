<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <div class="pull-right flip">
            {if $sub_view == ""}
                <a href="{$system['system_url']}/childinfo/{$child['child_parent_id']}/medicals/add" class="btn btn-default">
                    <i class="fa fa-plus"></i> {__("Add New")}
                </a>
            {elseif $sub_view == "add" || $sub_view == "edit"}
                <a href="{$system['system_url']}/childinfo/{$child['child_parent_id']}/medicals" class="btn btn-default">
                    <i class="fa fa-list"></i> {__("Lists")}
                </a>
            {/if}
        </div>
        <i class="fa fa-child fa-fw fa-lg pr10"></i>
        {__('Medical report book information')} &rsaquo; {$child['child_name']}
        {if $sub_view == "edit"}
            &rsaquo; {__('Edit')}
        {elseif $sub_view == ""}
            &rsaquo; {__('Lists')}
        {elseif $sub_view == "add"}
            &rsaquo; {__('Add New')}
        {/if}
    </div>
    {if $sub_view == ""}
        <div class="panel-body with-table form-horizontal">
            <div class="form-group">
                <label class="col-sm-3 control-label text-left">{__("Year")}</label>
                <div class="col-sm-3">
                    <select name="year" id="year" class="form-control">
                        <option value = "0">{__("Select year ...")}</option>
                        {for $i = $year_begin; $i < $year_end; $i++}
                            <option value="{$i}" {if $yearNow == $i}selected{/if}>{$i}</option>
                        {/for}
                    </select>
                </div>
                <div class="col-sm-3">
                    <a class="btn btn-default js_child-medical-search" data-id="{$child['child_parent_id']}" data-handle = "search">{__("Search")}</a>
                </div>
            </div>

            <div id = "medical_list">
                {include file="ci/ajax.medicallist.tpl"}
            </div>
        </div>
    {elseif $sub_view == "edit"}
        <div class="panel-body">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/child/bochild_medical.php">
                <input type="hidden" name="child_parent_id" value="{$child['child_parent_id']}"/>
                <input type="hidden" name="child_medical_id" value="{$data['child_medical_id']}"/>
                <input type="hidden" name="do" value="edit"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Diseased day")}</label>
                    <div class='col-sm-3'>
                        <div class='input-group date' id='birthdate_picker'>
                            <input type='text' name="recorded_at" id="recorded_at" class="form-control" value="{$data['recorded_at']}"/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Diseased")} (*)</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="diseased_name" placeholder="{__("Diseased")}" required value="{$data['diseased_name']}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Symptom")} (*)</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="Symptom" value="{$data['symptom']}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Medicine list")} (*)</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="medicine_list" value="{$data['medicine_list']}" required>
                    </div>
                </div>
                <div class = "form-group">
                    <label class="col-sm-3 control-label text-left">{__("Usage guide")} (*)</label>
                    <div class="col-sm-9">
                        <textarea rows="4" class="form-control" name="usage_guide" placeholder="{__("Usage guide")}" required>{$data['usage_guide']}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Day use")}</label>
                    <div class="col-sm-2">
                        <input type="number" min="0" step="1" class="form-control" name="day_use" value="{$data['day_use']}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Hospital")}</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="hospital" value="{$data['hospital']}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Hospital address")}</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="hospital_address" value="{$data['hospital_address']}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Doctor name")}</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="doctor_name" value="{$data['doctor_name']}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Doctor phone")}</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="doctor_phone" value="{$data['doctor_phone']}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Description")}</label>
                    <div class="col-sm-9">
                        <textarea rows="4" class="form-control" name="description"  maxlength="300">{$data['description']}</textarea>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->
                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    {elseif $sub_view == "add"}
        <div class="panel-body">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/child/bochild_medical.php">
                <input type="hidden" name="child_parent_id" value="{$child['child_parent_id']}"/>
                <input type="hidden" name="do" value="add"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Diseased day")}</label>
                    <div class='col-sm-3'>
                        <div class='input-group date' id='birthdate_picker'>
                            <input type='text' name="recorded_at" id="recorded_at" class="form-control"/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Diseased")} (*)</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="diseased_name" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Symptom")} (*)</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="Symptom">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Medicine list")} (*)</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="medicine_list" required>
                    </div>
                </div>
                <div class = "form-group">
                    <label class="col-sm-3 control-label text-left">{__("Usage guide")} (*)</label>
                    <div class="col-sm-9">
                        <textarea rows="4" class="form-control" name="usage_guide" required></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Day use")}</label>
                    <div class="col-sm-2">
                        <input type="number" min="0" step="1" class="form-control" name="day_use">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Hospital")}</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="hospital">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Hospital address")}</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="hospital_address">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Doctor name")}</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="doctor_name">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Doctor phone")}</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="doctor_phone">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Description")}</label>
                    <div class="col-sm-9">
                        <textarea rows="4" class="form-control" name="description"  maxlength="300"></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->
                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    {/if}
</div>