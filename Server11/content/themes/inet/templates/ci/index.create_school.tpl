<!-- create school -->
<div class="panel panel-default">
    <div class="panel-heading">
        <div class="mt5">
            <strong>{__("Create School")}</strong>
        </div>
    </div>
    <div class="panel-body">
        <form class="js_ajax-forms" data-url="ci/bo/school/bo_school.php?do=create">
            <div class="form-group">
                <label for="school_name">{__("School name")}*</label>
                <input type="text" class="form-control" name="title" placeholder="{__("School name")}" required autofocus maxlength="255">
            </div>
            <div class="form-group">
                <label for="username">{__("Username")}*</label>
                <input type="text" class="form-control" name="username" placeholder="{__("Username, e.g. mamnonthienthan")}" required maxlength="50">
            </div>
            <div class="form-group">
                <label for="telephone">{__("Telephone")}</label>
                <input type="text" class="form-control" name="telephone" placeholder="{__("Telephone")}" maxlength="50">
            </div>
            <div class="form-group">
                <label for="website">{__("Website")}</label>
                <input type="text" class="form-control" name="website" placeholder="{__("Website")}" maxlength="50">
            </div>
            <div class="form-group">
                <label for="email">{__("Email")}</label>
                <input type="text" class="form-control" name="email" placeholder="{__("Email")}" maxlength="50">
            </div>
            <div class="form-group">
                <label for="address">{__("Address")}</label>
                <input type="text" class="form-control" name="address" placeholder="{__("Address")}" maxlength="400">
            </div>
            <div class="form-group">
                <label for="city">{__("City")}</label>
                <select class="form-control" name="city_id">
                    <option value="0">{__("Select city")}</option>
                    {foreach $cities as $city}
                        <option value="{$city['city_id']}">{$city['city_name']}</option>
                    {/foreach}
                </select>
            </div>
            <div class="form-group">
                <label for="description">{__("Description")}</label>
                <textarea class="form-control" name="description" placeholder="{__("Write about your school...")}" rows="4"></textarea>
            </div>

            <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>

            <!-- error -->
            <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
            <!-- error -->
        </form>
    </div>
</div>
