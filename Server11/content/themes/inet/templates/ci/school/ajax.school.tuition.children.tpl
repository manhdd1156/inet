{*Hiển thị danh sách học phí của cả lớp. Áp dụng khi tính học phí mới.*}
{if $school['tuition_view_attandance'] && ($school['attendance_use_leave_early'] || $school['attendance_use_come_late'] || $school['attendance_absence_no_reason'])}
    <table class="table table-striped table-bordered table-hover">
        <tbody>
        <tr>
            <td align="right"><strong>{__("Symbol")}:</strong></td>
            {if $school['attendance_use_come_late']}
                <td bgcolor="#6495ed" align="center"><i class="fa fa-check" aria-hidden="true"></td>
                <td align="left">{__("Come late")}</td>
            {/if}
            {if $school['attendance_use_leave_early']}
                <td bgcolor="#008b8b" align="center"><i class="fa fa-check" aria-hidden="true"></td>
                <td align="left">{__("Leave early")}</td>
            {/if}
            <td align="center"><i class="fa fa-check" aria-hidden="true"></td>
            <td align="left">{__("Present")}</td>

            {if $school['attendance_absence_no_reason']}
                <td bgcolor="#ff1493" align="center"><i class="fa fa-times" aria-hidden="true"></td>
                <td align="left">{__("Without permission")}</td>
            {/if}

            <td bgcolor="#deb887" align="center"><i class="fa fa-times" aria-hidden="true"></td>
            <td align="left">{__("With permission")}</td>
        </tr>
        </tbody>
    </table>
{/if}
<div id="open_dialog" class="x-hidden" title="{__("Adding fees to calculate tuition")|upper}"></div>
<table class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th>
            <div class="pull-left flip">{__("Student list")|upper}&nbsp;({$results['children']|count})</div>
            <div class="pull-right flip">
                {__("Total of class")}&nbsp;({$smarty.const.MONEY_UNIT})
                <input style="width: 100px; background-color: #EEEEEE" type="text" class="text-right money_tui" id="class_total" name="total_amount" value="{$results['total_amount']}" readonly/>
            </div>
        </th>
    </tr>
    </thead>
    <tbody>
    {$childIdx = 0}
    {foreach $results['children'] as $child}
        {$childIdx = $childIdx + 1}
        <tr id="child_row_{$child['child_id']}">
            <td id="child_tuition_{$child['child_id']}">
                {include file="ci/school/ajax.school.tuition.child.tpl"}
            </td>
        </tr>
        <tr height = "15"></tr>
    {/foreach}
    {if count($results['children']) > 0}
        <tr>
            <td>
                <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                <div class="pull-right flip">
                    <strong>
                        {__("Total of class")}&nbsp;({$smarty.const.MONEY_UNIT})
                        <input style="width: 100px; background-color: #EEEEEE" type="text" class="text-right money_tui" id="class_total_2" name="class_total_2" value="{$results['total_amount']}" readonly/>
                    </strong>
                </div>
            </td>
        </tr>
    {/if}
    </tbody>
</table>

{*Hàm remove dấu , sau dấu -*}
<script>
    function stateChange() {
        setTimeout(function () {
            $('body .money_tui').each(function () {
                var a = $(this).val();
                a = a.toString().replace(/^-,/, '-');
                $(this).val(a);
            });
        }, 1000);
    }
    stateChange();
</script>