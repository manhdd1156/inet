{if $sheet['error'] == 1}
    <table class="table table-striped table-bordered table-hover">
        <thead>
        <tr><th>{__("Detail results")}</th></tr>
        </thead>
        <tbody>
        <tr><td><div>{$sheet['message']}</div></td></tr>
        </tbody>
    </table>
{else}

        {$idx = 1}
{*        <span><script>console.log('<'{var_dump($sheet)}'>')</script></span>*}
        {foreach $sheet['child_list'] as $child}
            {if $child['error'] != 0 }
                {$error = 1}
            {/if}
        {/foreach}
        <div style="text-align: center">
        {if $error == 0}
            <strong style="color: green">{__("Add point success")}</strong>
       {else}
            <strong style="color: red">{__("Add point not success")}</strong>
        {/if}
        </div>
            {$idx = $idx + 1}
{/if}