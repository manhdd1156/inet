<div class="panel panel-default">
    <div class="panel-heading with-icon">
        {if $sub_view == "manage" || ($sub_view == "detail" && $allow_teacher_edit_pickup_before)}
            <div class="pull-right flip">
                <a href="{$system['system_url']}/school/{$username}/pickupteacher/add/{$pickup['pickup_id']}" class="btn btn-default">
                    <i class="fa fa-plus"></i> {__("Add child")}
                </a>
{*                <a href="https://blog.coniu.vn/huong-dan-tra-tre-don-muon-cho-giao-vien/" class="btn btn-info btn_guide" target="_blank">*}
                <a href="#" class="btn btn-info btn_guide" target="_blank">
                    <i class="fa fa-info"></i> {__("Guide")}
                </a>
            </div>
        {/if}

        <i class="fa fa-life-ring fa-fw fa-lg pr10"></i>
        {__("Late pickup")}
        {if $sub_view == ""}
            &rsaquo; {__("Today lists")}
        {/if}
        {if $sub_view == "manage"}
            &rsaquo; {__('Add students - Pickup')}
        {/if}
        {if $sub_view == "history"}
            &rsaquo; {__('History')}
        {/if}
        {if $sub_view == "detail"}
            &rsaquo; {__('Detail')}
        {/if}
        {if $sub_view == "assign"}
            &rsaquo; {__('Assign')}
        {/if}
        {if $sub_view == "add"}
            &rsaquo; {__('Add child')}
        {/if}
    </div>
    {if $sub_view == ""}
        <div class="panel-body with-table">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/class/boclass_pickup.php">
                <input type="hidden" id="username" name="username" value="{$username}"/>
                <input type="hidden" name="do" value="add"/>

                <div class="text-center">
                    <strong>
                        <label style="font-size: 15px">{__("Late pickup on")|upper}: {$today}</label>
                    </strong><br><br>
                </div>

                <div class="table-responsive" id="pickup_class_list">
                    {$children_count = {$children|count}}
                    <div class="text-left">
                        <strong>{__("Total")}: {$children_count} |
                            {__("Registered")}: {$child_count}
                        </strong>
                    </div>
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th width="15%">#</th>
                            <th width="35%">{__("Children")}</th>
                            <th width="20%">{__("Status")}</th>
                            <th width="30%">{__("Class")}</th>
                        </tr>
                        </thead>
                        <tbody>
                        {$idx = 1}
                        {foreach $children as $child}
                            <tr {if $child['child_status'] == 0} class="row-disable"{/if}>
                                <td align="center">{$idx}</td>
                                <td><strong class="pl20">{$child['child_name']}</strong></td>
                                <td align="center">
                                    {if is_empty($child['status'])}
                                        <i class="fa fa-times" style="color:orangered" aria-hidden="true"></i>
                                    {else}
                                        <i class="fa fa-check" style="color:mediumseagreen" aria-hidden="true"></i>
                                    {/if}
                                </td>
                                <td align="center">
                                    <strong>
                                        {if is_empty($child['class_name']) }
                                            &#150;
                                        {else}
                                            {$child['class_name']}
                                        {/if}
                                    </strong>
                            </tr>
                            {$idx = $idx + 1}
                        {/foreach}
                        {if $children_count == 0}
                            <tr class="odd">
                                <td valign="top" align="center" colspan="4" class="dataTables_empty">
                                    {__("No data available in table")}
                                </td>
                            </tr>
                        {/if}
                        </tbody>
                    </table>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    {elseif $sub_view == "manage"}
        <div class="panel-body with-table">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_teacherpickup.php">
                <input type="hidden" id="school_username" name="school_username" value="{$username}"/>
                <input type="hidden" id="pickup_date" name="pickup_date" value="{$today}"/>
                <input type="hidden" id="pickup_id" name="pickup_id" value="{$pickup['pickup_id']}"/>
                <input type="hidden" id="callback" name="callback" value="manage"/>
                <input type="hidden" name="do" value="save"/>
                <div id="time_option" class="x-hidden" data-value="{$time_option}"></div>

                <div class="text-center">
                    {*<strong><label style="font-size: 15px">{__("LỚP ĐÓN MUỘN NGÀY")}: {$today}</label></strong><br><br>*}
                    <strong><label style="font-size: 15px">{__("Late pickup on")|upper}: {$today}</label></strong><br>
                    {if !is_empty($pickup['class_name'])}
                        <strong><label style="font-size: 15px">{__("Class")}: {$pickup['class_name']}</label></strong><br><br>
                    {else}
                        <br>
                    {/if}
                </div>
                <div class="col-sm-12">
                    <label class="col-sm-6 text-right" style="font-size: 15px">{__("Assigned teachers")}:</label>
                    {if $pickup['assign']|count > 0}
                        <div class="col-sm-6">
                            {foreach $pickup['assign'] as $assign}
                                <strong>
                                    <label>&#9679;&#09;{$assign['user_fullname']}</label>
                                </strong>
                                <br>
                            {/foreach}
                        </div>
                    {else}
                        <div class="col-sm-6 text-left" style="font-size: 15px;color: red">{__("Not assigned")}</div>
                    {/if}
                </div>

                <div>
                    <strong> - {__("Note")}: {__('Ấn vào nút "')}<font color="red">{__("Pickup")}</font>{__('" sẽ tính thời gian tại thời điểm ấn (không lưu dịch vụ, ghi chú), nếu có dịch vụ đi kèm và ghi chú vui lòng nhập vào thông tin và ấn vào nút')} "<font color="red">{__("Save")}</font>"</strong>
                </div>
                <br/>
                {$children_count = $children|count}
                <div>
                    <strong>{__("Total student")}: <font color="red">{$children_count}</font> |
                        {__("Picked up")}: <font color="red">{$child_count}</font>
                    </strong>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th width="5%">#</th>
                            <th width="25%">{__("Student name")}</th>
                            <th width="20%">{__("Birthdate")}</th>
                            <th width="20%">{__("Class")}</th>
                            <th width="15%">{__("Status")}</th>
                        </tr>
                        </thead>
                        <tbody>
                        {$idx = 1}
                        {foreach $children as $child}
                            <input type="hidden" name="child[]" value="{$child['child_id']}"/>
                            <tr>
                                <td align="center"><strong>{$idx}</strong></td>
                                <td><strong>{$child['child_name']}</strong></td>
                                <td align="center">{$child['birthday']}</td>
                                <td align="center">{$child['group_title']}</td>
                                <td rowspan="4" align="center">
                                    {if is_empty($child['pickup_at']) }
                                        {if $is_today}
                                        <a class="btn btn-xs btn-default js_class-pickup-one-child" data-handle="pickup"
                                                data-username="{$username}" data-child="{$child['child_id']}"
                                                data-pickup="{$child['pickup_id']}">
                                            {__("Pickup")}
                                        </a>
                                        {/if}
                                    {/if}
                                        <a class="btn btn-xs btn-danger js_class-pickup" data-handle="cancel"
                                                data-username="{$username}" data-child="{$child['child_id']}"
                                                data-pickup="{$child['pickup_id']}">
                                            {__("Cancel")}
                                        </a>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="1" rowspan="3"></td>
                                <td colspan="3" style="padding-left: 50px">
                                    <div class="col-sm-4 pt3">
                                        {__("Pickup at")}
                                        : &nbsp;
                                    </div>
                                    {if !is_empty($child['pickup_at']) }
                                        <input type='text' name="pickup_time[]" class="pickup_teacher_time" id="pickup_time_{$child['child_id']}" value="{$child['pickup_at']}" style="width: 70px; color: blue; text-align: center"/>
                                    {else}
                                        <input type='text' name="pickup_time[]" class="pickup_teacher_time" id="pickup_time_{$child['child_id']}" value="" style="width: 70px; font-weight: bold;color: red; text-align: center"/>
                                    {/if}
                                    <input type="hidden" min="0"
                                           id="total_pickup_{$child['child_id']}"
                                           data-child="{$child['child_id']}"
                                           name="pickup_fee[]" value="{$child['late_pickup_fee']}"/>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" align="left" style="padding-left: 50px">
                                    {if !empty($child['services'])}
                                        <div class="col-sm-4 pt3">
                                            {__("Using service")}: &nbsp;
                                        </div>
                                        {foreach $child['services'] as $service}
                                            <input type="checkbox" class="pickup_service_fee" name="service_{$child['child_id']}[]"
                                                   id="pickup_service_fee_{$child['child_id']}_{$service['service_id']}"
                                                   data-child="{$child['child_id']}"
                                                   data-service="{$service['service_id']}"
                                                   data-fee="{$service['price']}"
                                                   value="{$service['service_id']}" {if !is_null($service['using_at']) } checked {/if}/>
                                            <strong>{$service['service_name']}</strong>
                                            &nbsp;&nbsp;&nbsp;
                                        {/foreach}
                                    {else}
                                        &nbsp;
                                    {/if}

                                </td>

                            </tr>

                            <tr>
                                <td colspan="3" style="padding-left: 50px">
                                    <div class="col-sm-4 pt3">
                                        {__("Note")} : &nbsp;
                                    </div>
                                    <input type='text' name="pickup_note[]" id="pickup_note_{$child['child_id']}" value="{$child['description']}" style="width: 60%; color: blue"/>
                                    <input type="hidden" min="0"
                                           id="total_{$child['child_id']}"
                                           name="total_child[]"
                                           value="{$child['total_amount']}"/>
                                </td>
                            </tr>
                            {$idx = $idx + 1}
                        {/foreach}

                        {if $children_count == 0}
                            <tr class="odd">
                                <td valign="top" align="center" colspan="5" class="dataTables_empty">
                                    {__("No data available in table")}
                                </td>
                            </tr>
                        {/if}
                        <input type="hidden" id="total" name="total" value="{$pickup['total']}"/>
                        </tbody>
                    </table>
                </div>

                <div class="form-group">
                    <div class="col-sm-9 pl10">
                        <button type="submit"
                                class="btn btn-primary padrl30 {if $children|count == 0} x-hidden {/if}">{__("Save")}</button>
                    </div>
                </div>
                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->

            </form>
        </div>
    {elseif $sub_view == "history"}
        <div class="panel-body with-table">
            <div class="row">
                <div class='col-sm-12'>
                    <div class='col-sm-2'></div>
                    <div class='col-sm-3'>
                        <div class='input-group date' id='fromdatepicker'>
                            <input type='text' name="fromDate" id="fromDate" value="{$fromDate}" class="form-control" placeholder="{__("From date")}"/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                    <div class='col-sm-3'>
                        <div class='input-group date' id='todatepicker'>
                            <input type='text' name="toDate" id="toDate" value="{$toDate}" class="form-control" placeholder="{__("To date")}"/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                    <div class='col-sm-3'>
                        <div class="form-group">
                            <button class="btn btn-default js_pickup_teacher-search" data-username="{$username}" >{__("Search")}</button>
                        </div>
                    </div>
                </div>
            </div>
            <br/>

            <div class="table-responsive" id="pickup_list">
                {include file="ci/school/teacher_pickup/ajax.pickuplist.tpl"}
            </div>
        </div>
    {elseif $sub_view == "detail"}
        {include file="ci/school/teacher_pickup/ajax.pickup.detail.tpl"}
    {elseif $sub_view == "assign"}
        <div class = "panel-body">
            <form class="js_ajax-forms form-horizontal">
                <input type="hidden" name="username" id="school_username" value="{$username}"/>
                <div class="form-group">
                    <label class="col-sm-5 control-label text-left">{__("Select date")} (*)</label>
                    <div class='col-sm-3'>
                        <div class='input-group date' id='pickup_teacher_begin_assign'>
                            <input type='text' name="begin" id="begin" value="{$monday}" class="form-control" required/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                </div>

                <br>
                <div class="table-responsive {if $classCnt == 1}col-sm-9{else}col-sm-12{/if}">
                    <table class="table table-striped table-bordered table-hover" id="tbody-assign">
                        <thead>
                        <tr>
                            <th class="col-sm-2"></th>
                            {foreach $classes as $class}
                                <th class="col-sm-3"><center>{$class['class_name']}</center></th>
                            {/foreach}
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            {$classCnt = $classes|count}
                            <td class="align-middle" align="center">
                                <strong>{__("Monday")}</strong> <br> {$day['mon']}
                            </td>

                            {for $i = 0; $i < $classCnt; $i++}
                                <td class="align-middle">
                                    <div id="assign_class_2_{$classes[$i]['pickup_class_id']}" class="assign_class pl10">
                                        {foreach $data[2] as $class_id => $arr }
                                            {if $class_id == $classes[$i]['pickup_class_id']}
                                                {foreach $arr as $info}
                                                    <div id="assign_teacher_2_{$class_id}_{$info['user_id']}">
                                                        <a class="text-left {if $info['user_id'] == $user->_data['user_id']}strong{/if}">&#9679;&#09;{$info['user_fullname']}</a>
                                                        <input type="hidden" name="assign_day[]" value="2">
                                                        <input type="hidden" name="assign_class[]" value="{$class_id}">
                                                        <input type="hidden" name="assign_teacher[]" value="{$info['user_id']}">
                                                    </div>
                                                {/foreach}
                                            {/if}
                                        {/foreach}
                                    </div>
                                </td>
                            {/for}

                        </tr>
                        <tr>
                            <td class="align-middle" align="center">
                                <strong>{__("Tuesday")}</strong> <br> {$day['tue']}
                            </td>

                            {for $i = 0; $i < $classCnt; $i++}
                                <td class="align-middle">
                                    <div id="assign_class_3_{$classes[$i]['pickup_class_id']}" class="assign_class pl10">
                                        {foreach $data[3] as $class_id => $arr }
                                            {if $class_id == $classes[$i]['pickup_class_id']}
                                                {foreach $arr as $info}
                                                    <div id="assign_teacher_3_{$class_id}_{$info['user_id']}">
                                                        <a class="text-left {if $info['user_id'] == $user->_data['user_id']}strong{/if}">&#9679;&#09;{$info['user_fullname']}</a>
                                                        <input type="hidden" name="assign_day[]" value="3">
                                                        <input type="hidden" name="assign_class[]" value="{$class_id}">
                                                        <input type="hidden" name="assign_teacher[]" value="{$info['user_id']}">
                                                    </div>
                                                {/foreach}
                                            {/if}
                                        {/foreach}
                                    </div>
                                </td>
                            {/for}

                        </tr>
                        <tr>
                            <td class="align-middle" align="center">
                                <strong>{__("Wednesday")}</strong> <br> {$day['wed']}
                            </td>
                            {for $i = 0; $i < $classCnt; $i++}
                                <td class="align-middle">
                                    <div id="assign_class_4_{$classes[$i]['pickup_class_id']}" class="assign_class pl10">
                                        {foreach $data[4] as $class_id => $arr }
                                            {if $class_id == $classes[$i]['pickup_class_id']}
                                                {foreach $arr as $info}
                                                    <div id="assign_teacher_4_{$class_id}_{$info['user_id']}">
                                                        <a class="text-left {if $info['user_id'] == $user->_data['user_id']}strong{/if}">&#9679;&#09;{$info['user_fullname']}</a>
                                                        <input type="hidden" name="assign_day[]" value="4">
                                                        <input type="hidden" name="assign_class[]" value="{$class_id}">
                                                        <input type="hidden" name="assign_teacher[]" value="{$info['user_id']}">
                                                    </div>
                                                {/foreach}
                                            {/if}
                                        {/foreach}
                                    </div>
                                </td>
                            {/for}
                        </tr>
                        <tr>
                            <td class="align-middle" align="center">
                                <strong>{__("Thursday")}</strong> <br> {$day['thu']}
                            </td>
                            {for $i = 0; $i < $classCnt; $i++}
                                <td class="align-middle">
                                    <div id="assign_class_5_{$classes[$i]['pickup_class_id']}" class="assign_class pl10">
                                        {foreach $data[5] as $class_id => $arr }
                                            {if $class_id == $classes[$i]['pickup_class_id']}
                                                {foreach $arr as $info}
                                                    <div id="assign_teacher_5_{$class_id}_{$info['user_id']}">
                                                        <a class="text-left {if $info['user_id'] == $user->_data['user_id']}strong{/if}">&#9679;&#09;{$info['user_fullname']}</a>
                                                        <input type="hidden" name="assign_day[]" value="5">
                                                        <input type="hidden" name="assign_class[]" value="{$class_id}">
                                                        <input type="hidden" name="assign_teacher[]" value="{$info['user_id']}">
                                                    </div>
                                                {/foreach}
                                            {/if}
                                        {/foreach}
                                    </div>
                                </td>
                            {/for}
                        </tr>
                        <tr>
                            <td class="align-middle" align="center">
                                <strong>{__("Friday")}</strong> <br> {$day['fri']}
                            </td>
                            {for $i = 0; $i < $classCnt; $i++}
                                <td class="align-middle">
                                    <div id="assign_class_6_{$classes[$i]['pickup_class_id']}" class="assign_class pl10">
                                        {foreach $data[6] as $class_id => $arr }
                                            {if $class_id == $classes[$i]['pickup_class_id']}
                                                {foreach $arr as $info}
                                                    <div id="assign_teacher_6_{$class_id}_{$info['user_id']}">
                                                        <a class="text-left {if $info['user_id'] == $user->_data['user_id']}strong{/if}">&#9679;&#09;{$info['user_fullname']}</a>
                                                        <input type="hidden" name="assign_day[]" value="6">
                                                        <input type="hidden" name="assign_class[]" value="{$class_id}">
                                                        <input type="hidden" name="assign_teacher[]" value="{$info['user_id']}">
                                                    </div>
                                                {/foreach}
                                            {/if}
                                        {/foreach}
                                    </div>
                                </td>
                            {/for}
                        </tr>
                        <tr>
                            <td class="align-middle" align="center">
                                <strong>{__("Saturday")}</strong> <br> {$day['sat']}
                            </td>
                            {for $i = 0; $i < $classCnt; $i++}
                                <td class="align-middle">
                                    <div id="assign_class_7_{$classes[$i]['pickup_class_id']}" class="assign_class pl10">
                                        {foreach $data[7] as $class_id => $arr }
                                            {if $class_id == $classes[$i]['pickup_class_id']}
                                                {foreach $arr as $info}
                                                    <div id="assign_teacher_7_{$class_id}_{$info['user_id']}">
                                                        <a class="text-left {if $info['user_id'] == $user->_data['user_id']}strong{/if}">&#9679;&#09;{$info['user_fullname']}</a>
                                                        <input type="hidden" name="assign_day[]" value="7">
                                                        <input type="hidden" name="assign_class[]" value="{$class_id}">
                                                        <input type="hidden" name="assign_teacher[]" value="{$info['user_id']}">
                                                    </div>
                                                {/foreach}
                                            {/if}
                                        {/foreach}
                                    </div>
                                </td>
                            {/for}
                        </tr>
                        <tr>
                            <td class="align-middle" align="center">
                                <strong>{__("Sunday")}</strong> <br> {$day['sun']}
                            </td>
                            {for $i = 0; $i < $classCnt; $i++}
                                <td class="align-middle">
                                    <div id="assign_class_8_{$classes[$i]['pickup_class_id']}" class="assign_class pl10">
                                        {foreach $data[8] as $class_id => $arr }
                                            {if $class_id == $classes[$i]['pickup_class_id']}
                                                {foreach $arr as $info}
                                                    <div id="assign_teacher_8_{$class_id}_{$info['user_id']}">
                                                        <a class="text-left {if $info['user_id'] == $user->_data['user_id']}strong{/if}">&#9679;&#09;{$info['user_fullname']}</a>
                                                        <input type="hidden" name="assign_day[]" value="8">
                                                        <input type="hidden" name="assign_class[]" value="{$class_id}">
                                                        <input type="hidden" name="assign_teacher[]" value="{$info['user_id']}">
                                                    </div>
                                                    <br>
                                                {/foreach}
                                            {/if}
                                        {/foreach}
                                    </div>
                                </td>
                            {/for}
                        </tr>
                        </tbody>
                    </table>

                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    {elseif $sub_view == "add"}
        <div class="panel-body with-table">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_teacherpickup.php">
                <input type="hidden" name="school_username" id="school_username" value="{$username}"/>
                <input type="hidden" name="pickup_id" id="pickup_id" value="{$pickup_id}"/>
                <input type="hidden" id="pickup_class_id" name="pickup_class_id" value="{$pickup_class[0]['pickup_class_id']}"/>
                <input type="hidden" name="date" id="add_to_date" value="{$today}"/>
                <input type="hidden" name="do" value="add_child"/>

                <div class="text-center">
                    <strong><label style="font-size: 15px">{__("Add child into late pickup class on")|upper}
                            : {$today}</label></strong><br><br>
                </div>

                {if $pickup_class|count == 1}
                    <div class="form-group">
                        <label class="col-sm-2"></label>
                        <label class="col-sm-3 control-label text-left">{__("Add to")} (*)</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" value="{$pickup_class[0]['class_name']}" disabled/>
                            <input type="hidden" id="pickup_class_id" name="pickup_class_id" value="{$pickup_class[0]['pickup_class_id']}"/>
                        </div>
                    </div>
                {/if}

                <div class="form-group">
                    <label class="col-sm-2"></label>
                    <label class="col-sm-3 control-label text-left">{__("Class")} (*)</label>
                    <div class="col-sm-3">
                        <select name="class_id" id="pickup_teacher_class" class="form-control" required>
                            {$class_level = -1}
                            {foreach $classes as $class}
                                {if ($class_level != $class['class_level_id'])}
                                    <option value="" disabled style="color: blue">-----{$class['class_level_name']}-----</option>
                                {/if}
                                <option value="{$class['group_id']}" {if $class_id==$class['group_id']}selected{/if}>{$class['group_title']}</option>
                                {$class_level = $class['class_level_id']}
                            {/foreach}
                        </select>
                    </div>
                </div>

                <div class="table-responsive" id="pickup_class_list">
                    {include file="ci/school/teacher_pickup/ajax.pickupchild.tpl"}
                </div>

                <div class="form-group pl5">
                    <div class="col-sm-9">
                        <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    {/if}
</div>