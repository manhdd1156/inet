<div class="faq_tab">
    {if $view == "tuitions"}
        <div class="faq_header">
            <h4>{__("FAQ")}</h4>
        </div>
        <div class="faq_content">
            <div>
                <a href="https://blog.coniu.vn/huong-dan-tao-cac-loai-phi-cho-tre-hoc-phi-hang-thang-tien-an-hang-thang/" target="_blank">1. {__("Khi cần thêm một loại phí mới thì làm thế nào?")}</a>
            </div>
            <div><a href="https://blog.coniu.vn/khi-can-xoa-mot-loai-phi-can-phai-lam-nhu-nao/" target="_blank">2. {__("Khi cần xóa một loại phí mới thì làm thế nào?")}</a></div>
            <div><a href="https://blog.coniu.vn/huong-dan-sua-hoc-phi-da-tao-cho-tre/" target="_blank">3. {__("Sửa học phí cho một trẻ trong lớp thì làm thế nào?")}</a></div>
            <div><a href="https://blog.coniu.vn/tinh-hoc-phi-cho-tre-moi-them-vao-lop-khi-da-tao-hoc-phi-thang/" target="_blank">4. {__("Tính học phí trẻ được thêm mới vào lớp, trước đó lớp đã tạo học phí thì làm thế nào?")}</a></div>
        </div>
    {elseif $view == "services"}
        <div class="faq_header">
            <h4>{__("FAQ")}</h4>
        </div>
        <div class="faq_content">
            <div><a href="https://blog.coniu.vn/them-mot-loai-dich-vu-moi-thi-phai-lam-nhu-the-nao/" target="_blank">1. {__("Thêm một loại dịch vụ mới thì làm thế nào?")}</a></div>
            <div><a href="https://blog.coniu.vn/dang-ky-dich-vu-cho-tre-thi-phai-lam-nhu-the-nao/" target="_blank">2. {__("Đăng ký dịch vụ cho trẻ thì phải làm thế nào?")}</a></div>
            <div><a href="https://blog.coniu.vn/khi-them-moi-mot-loai-dich-vu-can-luu-y-nhung-gi/" target="_blank">3. {__("Thêm mới một loại dịch vụ cần lưu ý những gì?")}</a></div>
            <div><a href="https://blog.coniu.vn/khi-can-xoa-mot-loai-dich-vu-thi-phai-lam-nhu-the-nao/
" target="_blank">4. {__("Làm thế nào để xóa một dịch vụ?")}</a></div>
            <div><a href="https://blog.coniu.vn/huong-dan-huy-dich-vu-da-dang-ky-cho-tre/
" target="_blank">5. {__("Làm thế nào để hủy một dịch vụ đã đăng ký cho trẻ?")}</a></div>
        </div>
    {elseif $view == "pickup"}
        <div class="faq_header">
            <h4>{__("FAQ")}</h4>
        </div>
        <div class="faq_content">
            <div><a href="https://blog.coniu.vn/tien-tra-muon-cua-tre-bi-sai-trong-thang-co-sua-duoc-khong/" target="_blank">1. {__("Có sửa được tiền đón muộn của trẻ bị sai không?")}</a></div>
            <div><a href="https://blog.coniu.vn/them-tre-vao-lop-tra-muon-thi-lam-nhu-the-nao/" target="_blank">2. {__("Làm thế nào để thêm trẻ vào lớp đón muộn?")}</a></div>
            <div><a href="https://blog.coniu.vn/huong-dan-sua-thoi-gian-va-gia-cho-tre-khi-giao-vien-da-thao-tac-tra-muon/" target="_blank">3. {__("Làm thế nào để sửa thời gian và giá cho trẻ khi giáo viên đã thao tác đón muộn?")}</a></div>
            <div><a href="https://blog.coniu.vn/huong-dan-thuc-hien-tra-tre-tren-website/" target="_blank">4. {__("Làm thế nào để trả trẻ trên website?")}</a></div>
        </div>

    {elseif $view == "attendance"}
        <div class="faq_header">
            <h4>{__("FAQ")}</h4>
        </div>
        <div class="faq_content">
            <div>
                <a href="https://blog.coniu.vn/huong-dan-diem-danh-lai-cho-mot-lop/" target="_blank">1. {__("Điểm danh lại cho một lớp như thế nào?")}</a>
            </div>
        </div>
    {elseif $view == "classes"}
        <div class="faq_header">
            <h4>{__("FAQ")}</h4>
        </div>
        <div class="faq_content">
            <div><a href="https://blog.coniu.vn/huong-dan-phan-cong-giao-vien-vao-mot-lop/" target="_blank">1. {__("Làm thế nào để phân công một giáo viên vào lớp?")}</a></div>
            <div><a href="https://blog.coniu.vn/lam-the-nao-de-xoa-mot-lop/" target="_blank">2. {__("Làm thế nào để xóa một lớp?")}</a></div>
        </div>
    {/if}
</div>
