{$idxx = 1}
{foreach $results as $row}
    <table class="table table-striped table-bordered table-hover">
        <thead>
        <tr>
            <td colspan="6"><strong>{$idxx} - {$row['child']['child_name']}</strong></td>
            <input type="hidden" name="child_ids[]" value="{$row['child']['child_id']}"/>
        </tr>
        <tr>
            <td colspan="6">
                <strong>I. {__("Fee usage history")}&nbsp;({__("Number of charged days from last tuition")}:&nbsp;{$row['data']['attendance_count']})</strong>
                <input type="hidden" name="attendance_count_{$row['child']['child_id']}" value="{$row['data']['attendance_count']}"/>
            </td>
        </tr>
        <tr>
            <th>#</th>
            <th>{__("Fee name")}</th>
            <th class="text-center">{__("Unit")}</th>
            <th class="text-center">{__("Quantity")}</th>
            <th class="text-right">{__("Unit price")}&nbsp;({$smarty.const.MONEY_UNIT})</th>
            <th class="text-right">{__("Money amount")}&nbsp;({$smarty.const.MONEY_UNIT})</th>
        </tr>
        </thead>
        <tbody>
        {*Hiển thị các loại phí phải trả tháng hiện tại*}
        {$idx = 1}
        {foreach $row['data']['fees'] as $fee}
            <tr>
                <td class="text-center">
                    {$idx}
                    <input type="hidden" class="fee_id" name="fee_id_{$row['child']['child_id']}[]" value="{$fee['fee_id']}"/>
                </td>
                <td>{$fee['fee_name']}</td>
                <td class="text-center">{if $fee['fee_type'] == $smarty.const.FEE_TYPE_MONTHLY}{__('Monthly')}{else}{__('Daily')}{/if}</td>
                <td>
                    <input style="width: 50px;" type="number" class="text-center class_tuition4leave_quantity" id="quantity_{$fee['fee_id']}_{$row['child']['child_id']}" fee_id="{$fee['fee_id']}" child-id="{$row['child']['child_id']}"
                           name="quantity_{$row['child']['child_id']}[]" min="0" value="{$fee['quantity']}" step="1"/>
                </td>
                <td><input style="width: 100px;" type="text" class="text-right class_tuition4leave_unit_price money_tui" id="unit_price_{$fee['fee_id']}_{$row['child']['child_id']}" fee_id="{$fee['fee_id']}" child-id="{$row['child']['child_id']}"
                           name="unit_price_{$row['child']['child_id']}[]" min="0" value="{$fee['unit_price']}" step="1"/></td>
                <td><input style="width: 100px;" type="text" class="text-right money_tui" id="money_{$fee['fee_id']}_{$row['child']['child_id']}" name="money_{$row['child']['child_id']}_[]" value="{$fee['to_money']}" readonly/></td>
            </tr>
            {$idx = $idx + 1}
        {/foreach}

        {*Hiển thị các loại dịch vụ (CHỈ DỊCH VỤ THEO THÁNG VÀ THEO ĐIỂM DANH) *}
        <tr><td colspan="6" class="text-left"><strong>II. {__("Service")}</strong></td></tr>
        {$idx = 1}
        {foreach $row['data']['services'] as $service}
            <tr>
                <td class="text-center">
                    {$idx}
                    <input type="hidden" class="service_id" name="service_id_{$row['child']['child_id']}[]" value="{$service['service_id']}"/>
                </td>
                <td>{$service['service_name']}</td>
                <td class="text-center">
                    <input type="hidden" name="service_type_{$row['child']['child_id']}[]" value="{$service['service_type']}"/>
                    {if $service['service_type'] == $smarty.const.SERVICE_TYPE_MONTHLY}{__('Monthly')}{else}{__('Daily')}{/if}
                </td>
                <td>
                    <input  style="width: 50px;" type="number" class="text-center class_tuition4leave_service_quantity" id="service_quantity_{$service['service_id']}_{$row['child']['child_id']}" service_id="{$service['service_id']}" child-id="{$row['child']['child_id']}"
                            name="service_quantity_{$row['child']['child_id']}[]" min="0" value="{$service['quantity']}" step="1"/>
                </td>
                <td><input style="width: 100px;" type="text" class="text-right class_tuition4leave_service_fee money_tui" id="service_fee_{$service['service_id']}_{$row['child']['child_id']}" child-id="{$row['child']['child_id']}"
                           service_id="{$service['service_id']}" name="service_fee_{$row['child']['child_id']}[]" min="0" value="{$service['unit_price']}" step="1"/></td>
                <td><input style="width: 100px;" type="text" class="text-right money_tui" id="service_amount_{$service['service_id']}_{$row['child']['child_id']}" name="service_amount_{$row['child']['child_id']}_[]" value="{$service['to_money']}" readonly/></td>
            </tr>
            {$idx = $idx + 1}
        {/foreach}

        {* Các khoản phí sử dụng dịch vụ theo LẦN SỬ DỤNG ở tháng TRƯỚC *}
        <tr>
            <td colspan="6"><strong>III. {__("Count-based service")}</strong></td>
        </tr>
        {$idx = 1}
        {foreach $row['data']['count_based_services'] as $service}
            <tr>
                <td class="text-center">
                    {$idx}
                    <input type="hidden" class="cbservice_id" name="cbservice_id_{$row['child']['child_id']}[]" value="{$service['service_id']}"/>
                </td>
                {if $service['service_id'] > 0}
                    <td>{$service['service_name']}</td>
                    <td class="text-center">{__('Times')}</td>
                    <td>
                        <input style="width: 50px;" type="number" class="text-center class_tuition4leave_service_quantity" id="service_quantity_{$service['service_id']}_{$row['child']['child_id']}" service_id="{$service['service_id']}" child-id="{$row['child']['child_id']}"
                               name="cbservice_quantity_{$row['child']['child_id']}[]" min="0" value="{$service['quantity']}" step="1"/>
                    </td>
                    <td><input style="width: 100px;" type="text" class="text-right class_tuition4leave_service_fee money_tui" id="service_fee_{$service['service_id']}_{$row['child']['child_id']}" child-id="{$row['child']['child_id']}"
                               service_id="{$service['service_id']}" name="cbservice_price_{$row['child']['child_id']}[]" min="0" value="{$service['unit_price']}" step="1"/></td>
                    <td><input style="width: 100px;" type="text" class="text-right money_tui" id="service_amount_{$service['service_id']}_{$row['child']['child_id']}" name="service_amount_{$row['child']['child_id']}_[]" value="{$service['to_money']}" readonly/></td>
                {else}
                    <td colspan="4">
                        {$service['service_name']}
                        <input type="hidden" id="service_quantity_{$service['service_id']}_{$row['child']['child_id']}" service_id="{$service['service_id']}" name="cbservice_quantity_{$row['child']['child_id']}[]" value="{$service['quantity']}"/>
                        <input type="hidden" id="service_fee_{$service['service_id']}_{$row['child']['child_id']}" service_id="{$service['service_id']}" name="cbservice_price_{$row['child']['child_id']}[]" value="{$service['unit_price']}"/>
                    </td>
                    <td><input style="width: 100px;" type="text" class="text-right class_tuition4leave_pick_up money_tui" id="service_amount_{$service['service_id']}_{$row['child']['child_id']}" child-id="{$row['child']['child_id']}" name="service_amount_{$row['child']['child_id']}_[]" value="{$service['to_money']}"/></td>
                {/if}
            </tr>
            {$idx = $idx + 1}
        {/foreach}
        <tr>
            <td colspan="5" class="text-right"><strong>IV. {__("Debt amount of previous month")}&nbsp;({$smarty.const.MONEY_UNIT})</strong></td>
            <td><input style="width: 100px;" type='text' name="debt_amount_{$row['child']['child_id']}" id="debt_amount_{$row['child']['child_id']}" value="{$row['data']['debt_amount']}" class="text-right money_tui" readonly/></td>
        </tr>
        <tr>
            <td colspan="5" class="text-right"><strong>V. {__("Tuition deduction of previous month")}&nbsp;({$smarty.const.MONEY_UNIT})</strong></td>
            <td><input style="width: 100px;" type='text' name="total_deduction_{$row['child']['child_id']}" id="total_deduction_{$row['child']['child_id']}" value="{$row['data']['total_deduction']}" class="text-right money_tui" readonly/></td>
        </tr>
        <tr>
            <td colspan="5" class="text-right"><strong>VI. {__("Paid tuition amount at begining")}&nbsp;({$smarty.const.MONEY_UNIT})</strong></td>
            <td><input style="width: 100px;" type='text' name="paid_amount_{$row['child']['child_id']}" id="paid_amount_{$row['child']['child_id']}" value="{$row['data']['paid_amount']}" class="text-right money_tui" readonly/></td>
        </tr>
        <tr>
            <td colspan="5" class="text-right"><strong>{__("Final total")}&nbsp;(I + II + III + IV - V - VI)&nbsp;({$smarty.const.MONEY_UNIT})</strong></td>
            <td><input style="width: 100px;" type='text' name="final_amount_{$row['child']['child_id']}" id="final_amount_{$row['child']['child_id']}" value="{$row['data']['final_amount']}" class="text-right money_tui" readonly/></td>
        </tr>
        </tbody>
    </table>
    {$idxx = $idxx + 1}
{/foreach}
