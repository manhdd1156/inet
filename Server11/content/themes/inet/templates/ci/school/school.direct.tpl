<div class="school_direct" align="center" style="margin-top: 20px">
    {if $view == ""}
    {elseif $view == "classlevels"}
        <a href="{$system['system_url']}/school/{$username}" class="btn btn-default"><i class="fas fa-chevron-left"></i> {__("Back")}</a>
        {if count($rows) > 0 && count($classesStep) > 0}
            <a href="#" class="btn btn-success js_school-step" data-step="1" data-username="{$username}" data-view="classes" data-subview="">{__("Next")} <i class="fas fa-chevron-right"></i></a>
        {elseif count($rows) > 0 && count($classesStep) == 0}
            <a href="#" class="btn btn-success js_school-step" data-step="1" data-username="{$username}" data-view="classes" data-subview="add">{__("Next")} <i class="fas fa-chevron-right"></i></a>
        {/if}

    {elseif $view == "classes"}
        <a href="{$system['system_url']}/school/{$username}/classlevels" class="btn btn-default"><i class="fas fa-chevron-left"></i> {__("Back")}</a>
        {if count($teachersStep) > 0}
            <a href="#" class="btn btn-success js_school-step" data-step="2" data-username="{$username}" data-view="teachers" data-subview="">{__("Next")} <i class="fas fa-chevron-right"></i></a>
        {else}
            <a href="#" class="btn btn-success js_school-step" data-step="2" data-username="{$username}" data-view="teachers" data-subview="add">{__("Next")} <i class="fas fa-chevron-right"></i></a>
        {/if}

    {elseif $view == "teachers"}
        {if count($classesStep) > 0}
            <a href="{$system['system_url']}/school/{$username}/classes" class="btn btn-default"><i class="fas fa-chevron-left"></i> {__("Back")}</a>
        {else}
            <a href="{$system['system_url']}/school/{$username}/classes/add" class="btn btn-default"><i class="fas fa-chevron-left"></i> {__("Back")}</a>
        {/if}
        {if count($servicesStep) > 0}
            <a href="#" class="btn btn-success js_school-step" data-step="3" data-username="{$username}" data-view="services" data-subview="">{__("Next")} <i class="fas fa-chevron-right"></i></a>
        {else}
            <a href="#" class="btn btn-success js_school-step" data-step="3" data-username="{$username}" data-view="services" data-subview="add">{__("Next")} <i class="fas fa-chevron-right"></i></a>
        {/if}

    {elseif $view == "services"}
        {if count($teachersStep > 0)}
            <a href="{$system['system_url']}/school/{$username}/teachers" class="btn btn-default"><i class="fas fa-chevron-left"></i> {__("Back")}</a>
        {else}
            <a href="{$system['system_url']}/school/{$username}/teachers/add" class="btn btn-default"><i class="fas fa-chevron-left"></i> {__("Back")}</a>
        {/if}
        {if count($feesStep) > 0}
            <a href="#" class="btn btn-success js_school-step" data-step="4" data-username="{$username}" data-view="fees" data-subview="">{__("Next")} <i class="fas fa-chevron-right"></i></a>
        {else}
            <a href="#" class="btn btn-success js_school-step" data-step="4" data-username="{$username}" data-view="fees" data-subview="add">{__("Next")} <i class="fas fa-chevron-right"></i></a>
        {/if}

    {elseif $view == "fees"}
        {if count($servicesStep) > 0}
            <a href="{$system['system_url']}/school/{$username}/services" class="btn btn-default"><i class="fas fa-chevron-left"></i> {__("Back")}</a>
        {else}
            <a href="{$system['system_url']}/school/{$username}/services/add" class="btn btn-default"><i class="fas fa-chevron-left"></i> {__("Back")}</a>
        {/if}
        <a href="#" class="btn btn-success js_school-step" data-step="5" data-username="{$username}" data-view="pickup" data-subview="template">{__("Next")} <i class="fas fa-chevron-right"></i></a>

    {elseif $view == "pickup" && $sub_view == "template"}
        {if count($feesStep) > 0}
            <a href="{$system['system_url']}/school/{$username}/fees" class="btn btn-default"><i class="fas fa-chevron-left"></i> {__("Back")}</a>
        {else}
            <a href="{$system['system_url']}/school/{$username}/fees/add" class="btn btn-default"><i class="fas fa-chevron-left"></i> {__("Back")}</a>
        {/if}
        <a href="#" class="btn btn-success js_school-step" data-step="6" data-username="{$username}" data-view="pickup" data-subview="assign">{__("Next")} <i class="fas fa-chevron-right"></i></a>

    {elseif $view == "pickup" && $sub_view == "assign"}
        <a href="{$system['system_url']}/school/{$username}/pickup/template" class="btn btn-default"><i class="fas fa-chevron-left"></i> {__("Back")}</a>
        <a href="#" class="btn btn-success js_school-step" data-step="7" data-username="{$username}" data-view="children" data-subview="">{__("Next")} <i class="fas fa-chevron-right"></i></a>

    {elseif $view == "children"}
        <a href="{$system['system_url']}/school/{$username}/pickup/assign" class="btn btn-default"><i class="fas fa-chevron-left"></i> {__("Back")}</a>
        <a href="#" class="btn btn-success js_school-step" data-step="100" data-username="{$username}" data-view="" data-subview="">{__("Finish")} <i class="fas fa-chevron-right"></i></a>
    {/if}
</div>