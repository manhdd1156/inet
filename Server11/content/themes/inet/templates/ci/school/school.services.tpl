<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <div class="pull-right flip">
{*            {if $sub_view != ""}*}
{*                {if $school['school_step'] == SCHOOL_STEP_FINISH  || $school['grade'] != 0}*}
{*                    <a href="https://blog.coniu.vn/huong-dan-quan-ly-tao-dang-ky-dich-vu-tren-website-inet/" target="_blank" class="btn btn-info  btn_guide">*}
{*                        <i class="fa fa-info"></i> {__("Guide")}*}
{*                    </a>*}
{*                {/if}*}
{*            {/if}*}
            {if $sub_view == ""}
{*                {if $canEdit}*}
{*                    {if $school['school_step'] == SCHOOL_STEP_FINISH  || $school['grade'] != 0}*}
{*                        <a href="https://blog.coniu.vn/huong-dan-them-moi-mot-loai-dich-vu/" target="_blank" class="btn btn-info btn_guide">*}
{*                        <a href="#" target="_blank" class="btn btn-info btn_guide">*}
{*                            <i class="fa fa-info"></i> {__("Add new a service guide")}*}
{*                        </a>*}
{*                    {/if}*}
{*                {/if}*}
                <a href="{$system['system_url']}/school/{$username}/services/add" class="btn btn-default">
                    <i class="fa fa-plus"></i> {__("Add New")}
                </a>
                <a href="{$system['system_url']}/school/{$username}/services/inactive" class="btn btn-warning">
                    <i class="fa fa-list"></i> {__("Inactive service list")}
                </a>
            {elseif $sub_view == "inactive"}
                <a href="{$system['system_url']}/school/{$username}/services/" class="btn btn-default">
                    <i class="fa fa-list"></i> {__("Service list")}
                </a>
            {/if}
        </div>
        <i class="fa fa-life-ring fa-lg fa-fw pr10"></i>
        {__("Service")}
        {if $sub_view == "edit"}
            &rsaquo; {$data['service_name']}
        {elseif $sub_view == "add"}
            &rsaquo; {__('Add New')}
        {elseif $sub_view == "inactive"}
            &rsaquo; {__('Inactive service list')}
        {elseif $sub_view == ""}
            &rsaquo; {__('Service list')}
        {/if}
    </div>
    {if $sub_view == ""}
        <div class="panel-body with-table">
            <div class="table-responsive">
                <div><strong>{__("Service list")}&nbsp;(<span class="count_service">{$rows|count}</span>)</strong></div>
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>{__("Service name")}</th>
                            <th>{__("Service type")}</th>
                            <th>{__("Service fee")} ({$smarty.const.MONEY_UNIT})</th>
                            {*<th>{__("Require registration")}?</th>*}
                            <th>{__("Is it feed")}?</th>
                            <th>{__("Description")}</th>
                            <th>{__("Actions")}</th>
                        </tr>
                    </thead>
                    <tbody>
                        {$idx = 1}
                        {foreach $rows as $row}
                            <tr class="list_service_{$row['service_id']}">
                                <td align="center">{$idx}</td>
                                <td>{$row['service_name']}</td>
                                <td>
                                    {if $row['type']==$smarty.const.SERVICE_TYPE_MONTHLY}
                                        {__("Monthly service")}
                                    {elseif $row['type']==$smarty.const.SERVICE_TYPE_DAILY}
                                        {__("Daily service")}
                                    {else}
                                        {__("Count-based service")}
                                    {/if}
                                </td>
                                <td align="right">{moneyFormat($row['fee'])}</td>
                                {*<td align="center">*}
                                    {*{if $row['must_register']}*}
                                        {*<i class="fa fa-check" aria-hidden="true"></i>*}
                                    {*{else}*}
                                        {*<i class="fa fa-times" aria-hidden="true"></i>*}
                                    {*{/if}*}
                                {*</td>*}
                                <td align="center">
                                    {if $row['is_food']}
                                        <i class="fa fa-check" aria-hidden="true"></i>
                                    {else}
                                        <i class="fa fa-times" aria-hidden="true"></i>
                                    {/if}
                                </td>
                                <td>{$row['description']}</td>
                                <td nowrap="true">
                                    {if $canEdit}
                                        <a href="{$system['system_url']}/school/{$username}/services/edit/{$row['service_id']}" class="btn btn-xs btn-default">{__("Edit")}</a>
                                        <button class="btn btn-xs btn-warning js_school-service-cancel" data-username="{$username}" data-id="{$row['service_id']}">{__("Inactive")}</button>
                                        {if $row['can_delete']}
                                            <button class="btn btn-xs btn-danger js_school-service-delete" data-username="{$username}" data-id="{$row['service_id']}">{__("Delete")}</button>
                                        {/if}
                                    {/if}
                                </td>
                            </tr>
                            {$idx = $idx + 1}
                        {/foreach}
                        {if $rows|count == 0}
                            <tr class="odd" class="list_service_{$row['service_id']}">
                                <td valign="top" align="center" colspan="8" class="dataTables_empty">
                                    {__("No data available in table")}
                                </td>
                            </tr>
                        {/if}

                    </tbody>
                </table>
            </div>
        </div>
    {elseif $sub_view == "inactive"}
        <div class="panel-body with-table">
            <div class="table-responsive">
                <div><strong>{__("Inactive service list")}&nbsp;({$rows|count})</strong></div>
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>{__("Service name")}</th>
                        <th>{__("Service type")}</th>
                        <th>{__("Service fee")} ({$smarty.const.MONEY_UNIT})</th>
                        {*<th>{__("Require registration")}?</th>*}
                        <th>{__("Is it feed")}?</th>
                        <th>{__("Description")}</th>
                        <th>{__("Actions")}</th>
                    </tr>
                    </thead>
                    <tbody>
                    {$idx = 1}
                    {foreach $rows as $row}
                        <tr>
                            <td align="center">{$idx}</td>
                            <td>{$row['service_name']}</td>
                            <td>
                                {if $row['type']==$smarty.const.SERVICE_TYPE_MONTHLY}
                                    {__("Monthly service")}
                                {elseif $row['type']==$smarty.const.SERVICE_TYPE_DAILY}
                                    {__("Daily service")}
                                {else}
                                    {__("Count-based service")}
                                {/if}
                            </td>
                            <td align="right">{moneyFormat($row['fee'])}</td>
                            {*<td align="center">*}
                            {*{if $row['must_register']}*}
                            {*<i class="fa fa-check" aria-hidden="true"></i>*}
                            {*{else}*}
                            {*<i class="fa fa-times" aria-hidden="true"></i>*}
                            {*{/if}*}
                            {*</td>*}
                            <td align="center">
                                {if $row['is_food']}
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                {else}
                                    <i class="fa fa-times" aria-hidden="true"></i>
                                {/if}
                            </td>
                            <td>{$row['description']}</td>
                            <td nowrap="true">
                                {if $canEdit}
                                    <a href="{$system['system_url']}/school/{$username}/services/edit/{$row['service_id']}" class="btn btn-xs btn-default">{__("Edit")}</a>
                                    {if $row['can_delete']}
                                        <button class="btn btn-xs btn-danger js_school-service-delete" data-username="{$username}" data-id="{$row['service_id']}">{__("Delete")}</button>
                                    {/if}
                                {/if}
                            </td>
                        </tr>
                        {$idx = $idx + 1}
                    {/foreach}
                    {if $rows|count == 0}
                        <tr class="odd">
                            <td valign="top" align="center" colspan="8" class="dataTables_empty">
                                {__("No data available in table")}
                            </td>
                        </tr>
                    {/if}

                    </tbody>
                </table>
            </div>
        </div>
    {elseif $sub_view == "edit"}
        <div class="panel-body">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_service.php">
                <input type="hidden" name="school_username" id="school_username" value="{$username}"/>
                <input type="hidden" name="service_id" value="{$data['service_id']}"/>
                <input type="hidden" name="do" value="edit_service"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Service name")} (*)</label>
                    <div class="col-sm-9">
                        <input class="form-control" name="service_name" value="{$data['service_name']}" required autofocus maxlength="254">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Service type")} (*)</label>
                    <div class="col-sm-4">
                        {*<select name="type" id="type" class="form-control">*}
                            {*<option value="{$smarty.const.SERVICE_TYPE_MONTHLY}" {if $data['type']==$smarty.const.SERVICE_TYPE_MONTHLY}selected{/if}>{__("Monthly service")}</option>*}
                            {*<option value="{$smarty.const.SERVICE_TYPE_DAILY}" {if $data['type']==$smarty.const.SERVICE_TYPE_DAILY}selected{/if}>{__("Daily service")}</option>*}
                            {*<option value="{$smarty.const.SERVICE_TYPE_COUNT_BASED}" {if $data['type']==$smarty.const.SERVICE_TYPE_COUNT_BASED}selected{/if}>{__("Count-based service")}</option>*}
                        {*</select>*}
                        <input type="hidden" name="type" id="type" value="{$data['type']}">
                        <input type="text" value="{if $data['type']==$smarty.const.SERVICE_TYPE_MONTHLY}{__("Monthly service")}{elseif $data['type']==$smarty.const.SERVICE_TYPE_DAILY}{__("Daily service")}{else}{__("Count-based service")}{/if}" class="form-control" disabled>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Unit price")}</label>
                    <div class="col-sm-4 input-group" style="padding-left: 10px">
                        <input type="text" class="form-control money_tui" name="fee" value="{$data['fee']}" maxlength="11" required>
                        <span class="input-group-addon">{$smarty.const.MONEY_UNIT}</span>
                    </div>
                </div>
                {*<div class="form-group">*}
                    {*<label class="col-sm-3 control-label text-left">{__("Require registration")}?</label>*}
                    {*<div class="col-sm-9">*}
                        {*<div class="onoffswitch">*}
                            {*<input type="checkbox" id="must_register" name="must_register" class="onoffswitch-checkbox" value="1" {if $data['must_register']==1}checked{/if}>*}
                            {*<label class="onoffswitch-label" for="must_register"></label>*}
                        {*</div>*}
                    {*</div>*}
                {*</div>*}
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Is it feed")}?</label>
                    <div class="col-sm-9">
                        <div class="onoffswitch">
                            <input type="checkbox" id="is_food" name="is_food" class="onoffswitch-checkbox" {if $data['is_food']==1}checked{/if}>
                            <label class="onoffswitch-label" for="is_food"></label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Description")}</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="description" rows="4">{$data['description']}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Applied")}?</label>
                    <div class="col-sm-9">
                        <div class="onoffswitch">
                            <input type="checkbox" id="status" name="status" class="onoffswitch-checkbox" {if $data['status']==1}checked{/if}>
                            <label class="onoffswitch-label" for="status"></label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Parent display")}?</label>
                    <div class="col-sm-9">
                        <div class="onoffswitch">
                            <input type="checkbox" id="parent_display" name="parent_display" class="onoffswitch-checkbox" {if $data['parent_display']==1}checked{/if}>
                            <label class="onoffswitch-label" for="parent_display"></label>
                        </div>
                        <span class="help-block">{__("Allow parent display")}?</span>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                        <a href="{$system['system_url']}/school/{$username}/services" class="btn btn-default">{__("Lists")}</a>
                    </div>
                </div>

                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
            </form>
        </div>
    {elseif $sub_view == "add"}
        <div class="panel-body">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_service.php">
                <input type="hidden" name="school_username" id="school_username" value="{$username}"/>
                <input type="hidden" name="do" value="add_service"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Service name")} (*)</label>
                    <div class="col-sm-9">
                        <input class="form-control" name="service_name" required autofocus maxlength="254">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Service type")} (*)</label>
                    <div class="col-sm-5">
                        <select name="type" id="type" class="form-control">
                            <option value="{$smarty.const.SERVICE_TYPE_MONTHLY}">{__("Monthly service")}</option>
                            <option value="{$smarty.const.SERVICE_TYPE_DAILY}">{__("Daily service")}</option>
                            <option value="{$smarty.const.SERVICE_TYPE_COUNT_BASED}">{__("Count-based service")}</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Unit price")}</label>
                    <div class="col-sm-9">
                        <div class="input-group col-md-5">
                            <input type="text" class="form-control money_tui" name="fee" value="0" maxlength="11" required>
                            <span class="input-group-addon">{$smarty.const.MONEY_UNIT}</span>
                        </div>
                    </div>
                </div>
                {*<div class="form-group">*}
                    {*<label class="col-sm-3 control-label text-left">{__("Require registration")}?</label>*}
                    {*<div class="col-sm-9">*}
                        {*<div class="onoffswitch">*}
                            {*<input type="checkbox" id="must_register" name="must_register" class="onoffswitch-checkbox" value="1" checked>*}
                            {*<label class="onoffswitch-label" for="must_register"></label>*}
                        {*</div>*}
                    {*</div>*}
                {*</div>*}
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Is it feed")}?</label>
                    <div class="col-sm-9">
                        <div class="onoffswitch">
                            <input type="checkbox" id="is_food" name="is_food" class="onoffswitch-checkbox">
                            <label class="onoffswitch-label" for="is_food"></label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Description")}</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="description" rows="4"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Applied")}?</label>
                    <div class="col-sm-9">
                        <div class="onoffswitch">
                            <input type="checkbox" id="status" name="status" class="onoffswitch-checkbox" checked>
                            <label class="onoffswitch-label" for="status"></label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Parent display")}?</label>
                    <div class="col-sm-9">
                        <div class="onoffswitch">
                            <input type="checkbox" id="parent_display" name="parent_display" class="onoffswitch-checkbox" checked>
                            <label class="onoffswitch-label" for="parent_display"></label>
                        </div>
                        <span class="help-block">{__("Allow parent display")}?</span>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                    </div>
                </div>

                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
            </form>
        </div>
    {/if}
</div>