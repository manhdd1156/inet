<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <div class="pull-right flip">
{*            {if $sub_view != 'add'}*}
{*                <a href="https://blog.coniu.vn/huong-dan-quan-ly-tao-don-thuoc-tren-website-inet/" target="_blank" class="btn btn-info btn_guide">*}
{*                <a href="#" target="_blank" class="btn btn-info btn_guide">*}
{*                    <i class="fa fa-info"></i> {__("Guide")}*}
{*                </a>*}
{*            {else}*}
{*                <a href="https://blog.coniu.vn/cap-nhat-mot-don-thuoc-moi-cho-tre/" target="_blank" class="btn btn-info btn_guide">*}
{*                <a href="#" target="_blank" class="btn btn-info btn_guide">*}
{*                    <i class="fa fa-info"></i> {__("Guide")}*}
{*                </a>*}
{*            {/if}*}
            {if $canEdit}
                {if $sub_view == "" || $sub_view == "all"}
                    <a href="{$system['system_url']}/school/{$username}/medicines/add" class="btn btn-default">
                        <i class="fa fa-plus"></i> {__("Add New Medicine")}
                    </a>
                    {if $count_no_confirm > 0}
                        <a href="#" class = "btn btn-default js_school-medicine" {if $sub_view == "all"} data-handle = "confirm_all" {elseif $sub_view == ""} data-handle = "confirm_all_today" {/if} data-username = "{$username}" id="medicine_confirm">{__("Confirm all")}</a>
                    {/if}
                {/if}
            {/if}
        </div>
        <i class="fa fa-medkit fa-lg fa-fw pr10"></i>
        {__("Medicines")}
        {if $sub_view == "edit"}
            &rsaquo; {$data['medicine_list']}
        {elseif $sub_view == "add"}
            &rsaquo; {__('Add New')}
        {elseif $sub_view == "all"}
            &rsaquo; {__('List all')}
        {elseif $sub_view == "detail"}
            &rsaquo; {$data['child_name']} &rsaquo; {$data['medicine_list']}
        {elseif $sub_view == ""}
            &rsaquo; {__('Today lists')}
        {/if}
    </div>
    {if $sub_view == ""}
        <div class="panel-body with-table">
            <div><strong>{__("Medicine list")}&nbsp;{if count($rows) > 0}(<span class="count_medicine">{$rows|count}</span>) {else} (0) {/if}</th></strong></div>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>{__('Child')}</th>
                            <th>{__('Class')}</th>
                            <th>{__("Medicine list")}</th>
                            <th>{__("Times/day")}</th>
                            <th>{__("Time")}</th>
                            <th>{__("Actions")}</th>
                        </tr>
                    </thead>
                    <tbody>
                        {$idx = 1}
                        {foreach $rows as $row}
                            {if $idx > 1}
                                <tr><td colspan="7"></td></tr>
                            {/if}
                            <tr class="list_medicine_{$row['medicine_id']} {if $row['child_status'] == 0}row-disable{/if}">
                                {$rowspan = 1}
                                {if $row['guide'] != ''}{$rowspan = $rowspan + 1}{/if}
                                {if count($row['detail']) > 0}{$rowspan = $rowspan + 1}{/if}
                                <td align="center" rowspan="{$rowspan}" style="vertical-align:middle;"><strong>{$idx}</strong></td>
                                <td style="vertical-align:middle">
                                    <a href="{$system['system_url']}/school/{$username}/medicines/detail/{$row['medicine_id']}">{$row['child_name']}</a>
                                    {if $row['status']== $smarty.const.MEDICINE_STATUS_NEW}
                                        <img src="{$system['system_url']}/content/themes/{$system['theme']}/images/new.gif"/>
                                    {elseif $row['status']== $smarty.const.MEDICINE_STATUS_CONFIRMED}
                                        <i class="fa fa-check"></i>
                                    {/if}
                                    <br/>({$row['birthday']})
                                </td>
                                <td align="center" style="vertical-align:middle">{$row['group_title']}</td>
                                <td style="vertical-align:middle">
                                    {nl2br($row['medicine_list'])}
                                    {if $row['source_file'] != null}<br/><br/><a href = "{$row['source_file']}" target="_blank">{__("Doctor prescription")|upper}</a>{/if}
                                </td>
                                <td align="center" style="vertical-align:middle">
                                    {if $school['school_allow_medicate']}{count($row['detail'])}/{/if}{$row['time_per_day']}
                                </td>
                                <td align="center" style="vertical-align:middle">
                                    {$row['begin']}<br/>
                                    <font color="#dc143c">{$row['end']}</font>
                                </td>
                                <td align="center" nowrap="true">
                                    {if $canEdit}
                                        {if ($row['time_per_day'] > count($row['detail'])) && ($row['status'] != $smarty.const.MEDICINE_STATUS_CANCEL) && $school['school_allow_medicate']}
                                            <button class="btn btn-xs btn-primary js_school-medicine" data-handle="medicate" data-max="{$row['time_per_day']}" data-child="{$row['child_id']}" data-username="{$username}" data-id="{$row['medicine_id']}">
                                                {__("Medicate")}
                                            </button>
                                            <br/><br/>
                                        {/if}
                                        {if $row['status'] == $smarty.const.MEDICINE_STATUS_NEW}
                                            <button class="btn btn-xs btn-default js_school-medicine" data-handle="confirm" data-child="{$row['child_id']}" data-username="{$username}" data-id="{$row['medicine_id']}">
                                                {__("Confirm")}
                                            </button>
                                        {/if}
                                        {if ($row['status'] != $smarty.const.MEDICINE_STATUS_CANCEL) && $row['can_delete']}
                                            <a href="{$system['system_url']}/school/{$username}/medicines/edit/{$row['medicine_id']}" class="btn btn-xs btn-default">
                                                {__("Edit")}
                                            </a>
                                        {/if}
                                        {if $row['can_delete']}
                                            <button class="btn btn-xs btn-danger js_school-delete" data-handle="medicine" data-username="{$username}" data-id="{$row['medicine_id']}">
                                                {__("Delete")}
                                            </button>
                                        {else}
                                            {if $row['status'] != $smarty.const.MEDICINE_STATUS_CANCEL}
                                                <button class="btn btn-xs btn-warning js_school-medicine-cancel" data-screen="" data-handle="cancel" data-child="{$row['child_id']}" data-username="{$username}" data-id="{$row['medicine_id']}">
                                                    {__("Cancel")}
                                                </button>
                                            {/if}
                                        {/if}
                                    {/if}
                                </td>
                            </tr>
                            {if $row['guide'] != ''}
                                <tr class="list_medicine_{$row['medicine_id']}">
                                    <td colspan="2" align="center" style="vertical-align:middle"><strong>{__("Guide")}</strong></td>
                                    <td colspan="4">{nl2br($row['guide'])}</td>
                                </tr>
                            {/if}
                            {if count($row['detail']) > 0 && $school['school_allow_medicate']}
                                <tr>
                                    <td colspan="2" align="center" style="vertical-align:middle"><strong>{__("Medicated")}</strong></td>
                                    <td colspan="4">
                                        {foreach $row['detail'] as $detail}
                                            <strong>{$detail['time_on_day']}</strong>&nbsp;-&nbsp;{$detail['created_at']}&nbsp;|&nbsp;{$detail['user_fullname']}
                                            {if $detail['created_user_id'] != $user->_data['user_id']}
                                                <a href="#" class="far fa-comments fa-lg js_chat-start" data-name="{$detail['user_fullname']}" data-uid="{$detail['created_user_id']}"></a>
                                            {/if}<br/>
                                        {/foreach}
                                    </td>
                                </tr>
                            {/if}
                            {$idx = $idx + 1}
                        {/foreach}

                        {if $rows|count == 0}
                            <tr class="odd">
                                <td valign="top" align="center" colspan="7" class="dataTables_empty">
                                    {__("No data available in table")}
                                </td>
                            </tr>
                        {/if}

                    </tbody>
                </table>
            </div>
        </div>
    {elseif $sub_view == "all"}
        <div class="panel-body with-table">
            <div class="js_ajax-forms form-horizontal">
                <input type="hidden" name="school_username" id="school_username" value="{$username}"/>
                <input type="hidden" name="do" value="search_medicine"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Scope")}</label>
                    <div class="col-sm-2">
                        <select name="level" id="medicine_level" class="form-control">
                            <option value="{$smarty.const.CLASS_LEVEL}">{__("Class")}</option>
                            <option value="{$smarty.const.PARENT_LEVEL}">{__("Child")}</option>
                        </select>
                    </div>
                    <div class="col-sm-3" name="medicine_class">
                        <select name="class_id" id="me_class_id" data-username="{$username}" class="form-control" autofocus>
                            <option value="">{__("Select class")}</option>
                            {$class_level = -1}
                            {foreach $classes as $class}
                                {if ($class_level != $class['class_level_id'])}
                                    <option value="" disabled style="color: blue">-----{$class['class_level_name']}-----</option>
                                {/if}
                                <option value="{$class['group_id']}">{$class['group_title']}</option>
                                {$class_level = $class['class_level_id']}
                            {/foreach}
                        </select>
                    </div>
                    <div class="col-sm-4 x-hidden" name="medicine_child">
                        <select name="child_id" id="me_child_id" class="form-control">
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Time")}</label>
                    <div class='col-sm-3'>
                        <div class='input-group date' id='medicine_beginpicker_new'>
                            <input type='text' name="begin" class="form-control" id="begin"/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                    <div class='col-sm-1'><i class="fas fa-long-arrow-alt-right fa-lg fa-fw pr10 pt10"></i></div>
                    <div class='col-md-3'>
                        <div class='input-group date' id='medicine_endpicker_new'>
                            <input type='text' name="end" class="form-control" id="end"/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-default js_medicine-search">{__("Search")}</button>
                    </div>
                </div>

                <div class="table-responsive" id="medicine_list" name="medicine_list">
                    {include file="ci/school/ajax.school.medicinelist.tpl"}
                </div>
                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
            </div>
        </div>
    {elseif $sub_view == "edit"}
        <div class="panel-body">
            <form class="form-horizontal" enctype="multipart/form-data" method="post" id="create_medicine">
                <input type="hidden" name="school_username" id="school_username" value="{$username}"/>
                <input type="hidden" name="medicine_id" value="{$data['medicine_id']}"/>
                <input type="hidden" name="do" value="edit"/>

                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Select student")} (*)</label>
                    <div class="col-sm-4">
                        <select name="class_id" id="medicine_class_id" data-username="{$username}" class="form-control" autofocus>
                            <option value="">{__("Select class")}</option>

                            {$class_level = -1}
                            {foreach $classes as $class}
                                {if ($class_level != $class['class_level_id'])}
                                    <option value="" disabled style="color: blue">-----{$class['class_level_name']}-----</option>
                                {/if}
                                <option value="{$class['group_id']}" {if $data['class_id']==$class['group_id']}selected{/if}>{$class['group_title']}</option>
                                {$class_level = $class['class_level_id']}
                            {/foreach}
                        </select>
                    </div>
                    <div class="col-sm-5">
                        <select name="child_id" id="medicine_child_id" class="form-control" required>
                            <option value="{$data['child_id']}" selected>{$data['child_name']} - {$data['birthday']}</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Medicine list")} (*)</label>
                    <div class="col-sm-9">
                        <textarea class="form-control js_autosize" name="medicine_list" required maxlength="400">{$data['medicine_list']}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Usage guide")} (*)</label>
                    <div class="col-sm-9">
                        <textarea class="form-control js_autosize" name="guide" rows="6" required>{$data['guide']}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Time per day")} (*)</label>
                    <div class="col-sm-3">
                        <input class="form-control" type="number" name="time_per_day" min="1" maxlength="2" value="{$data['time_per_day']}" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Time")} (*)</label>
                    <div class='col-sm-3'>
                        <div class='input-group date' id='medicine_beginpicker_new'>
                            <input type='text' name="begin" value="{$data['begin']}" class="form-control" placeholder="{__("Begin")} (*)" required/>
                            <span class="input-group-addon" id="register_deadline_input">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                    <div class='col-sm-1'><i class="fas fa-long-arrow-alt-right fa-lg fa-fw pr10 pt10"></i></div>
                    <div class='col-md-3'>
                        <div class='input-group date' id='medicine_endpicker_new'>
                            <input type='text' name="end" value="{$data['end']}" class="form-control" placeholder="{__("End")} (*)" required/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                </div>
                <input type = "hidden" id = "is_file" name = "is_file" value = "1">
                <div class="form-group" id = "file_old">
                    <label class="rp-file col-sm-3 control-label text-left" style="padding-top: 0px">{__("Doctor prescription")}</label>
                    <div class="col-sm-6">
                        {if !is_empty($data['source_file'])}
                            <a href="{$data['source_file']}" target="_blank"><img src = "{$data['source_file']}" class = "img-responsive"></a>
                        {else} {__('No file attachment')}
                        {/if}
                    </div>
                </div>
                <div class = "form-group">
                    <label class="control-label col-sm-3">
                        {if is_empty($data['source_file'])}
                            {__("Choose file")}
                        {else}
                            {__("Choose file replace")}
                        {/if}
                    </label>
                    <div class = "col-sm-6">
                        <input type="file" name="file" id="file"/>
                    </div>
                    <a class = "delete_image btn btn-danger btn-xs text-left">{__('Delete')}</a>
                </div>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                        <a href="{$system['system_url']}/school/{$username}/medicines" class="btn btn-default">{__("Lists")}</a>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    {elseif $sub_view == "detail"}
        <div class="panel-body with-table">
            <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                    <tbody>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Child")}</strong></td>
                            <td><a href="{$system['system_url']}/school/{$username}/children/detail/{$data['child_id']}" target="_blank">{$data['child_name']}</a> ({$data['birthday']})</td>
                        </tr>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Class")}</strong></td>
                            <td>{$data['group_title']}</td>
                        </tr>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Medicine list")}</strong></td>
                            <td>{nl2br($data['medicine_list'])}</td>
                        </tr>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Usage guide")}</strong></td>
                            <td>{nl2br($data['guide'])}</td>
                        </tr>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Time per day")}</strong></td>
                            <td>{$data['time_per_day']}</td>
                        </tr>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Time")}</strong></td>
                            <td>{$data['begin']} - {$data['end']}</td>
                        </tr>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Doctor prescription")|upper}</strong></td>
                            <td>
                                {if !is_empty($data['source_file'])}
                                    <a href = "{$data['source_file']}" target="_blank"><strong>
                                            {__("File attachment")}
                                        </strong>
                                    </a>
                                {else}
                                    {__("No file attachment")}
                                {/if}
                            </td>
                        </tr>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Creator")}</strong></td>
                            <td><a href="{$system['system_url']}/{$data['user_name']}" target="_blank">{$data['user_fullname']}</a></td>
                        </tr>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("User confirmed")}</strong></td>
                            <td><a href="{$system['system_url']}/{$data['confirm_username']}" target="_blank">{$data['confirm_user']}</a></td>
                        </tr>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Created time")}</strong></td>
                            <td>{$data['created_at']}</td>
                        </tr>
                        {if $data['updated_at'] != ''}
                            <tr>
                                <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Last updated")}</strong></td>
                                <td>{$data['updated_at']}</td>
                            </tr>
                        {/if}
                        {if count($data['detail']) > 0}
                            <tr>
                                <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Medicated")}</strong></td>
                                <td>
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>{__('Usage date')}</th>
                                            <th>{__('Creator')}</th>
                                            <th>{__('Created time')}</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {foreach $data['detail'] as $detail}
                                            <tr>
                                                <td align="center">{$detail['time_on_day']}</td>
                                                <td>{$detail['usage_date']}</td>
                                                <td>
                                                    {$detail['user_fullname']}
                                                    {if $detail['created_user_id'] != $user->_data['user_id']}
                                                        <a href="#" class="far fa-comments fa-lg js_chat-start" data-name="{$detail['user_fullname']}" data-uid="{$detail['created_user_id']}"></a>
                                                    {/if}
                                                </td>
                                                <td>{$detail['created_at']}</td>
                                            </tr>
                                        {/foreach}
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        {/if}
                    </tbody>
                </table>
            </div>
            <div class="form-group pl5">
                <div class="col-sm-12">
                    <a class="btn btn-default" href="{$system['system_url']}/school/{$username}/medicines">{__("Today lists")}</a>
                    <a class="btn btn-default" href="{$system['system_url']}/school/{$username}/medicines/all">{__("List all")}</a>
                    {if $canEdit}
                        {if $data['status'] == $smarty.const.MEDICINE_STATUS_NEW}
                            <button class="btn btn-default js_school-medicine" data-handle="confirm" data-child="{$data['child_id']}" data-username="{$username}" data-id="{$data['medicine_id']}">
                                {__("Confirm")}
                            </button>
                        {/if}
                        {if ($data['status'] != $smarty.const.MEDICINE_STATUS_CANCEL) && $data['can_delete']}
                            <a href="{$system['system_url']}/school/{$username}/medicines/edit/{$data['medicine_id']}" class="btn btn-default">
                                {__("Edit")}
                            </a>
                        {/if}
                        {if $data['can_delete']}
                            <button class="btn btn-danger js_school-delete" data-handle="medicine" data-username="{$username}" data-id="{$data['medicine_id']}">
                                {__("Delete")}
                            </button>
                        {else}
                            {if $data['status'] != $smarty.const.MEDICINE_STATUS_CANCEL}
                                <button class="btn btn-warning js_school-medicine-cancel" data-screen="" data-handle="cancel" data-child="{$data['child_id']}" data-username="{$username}" data-id="{$data['medicine_id']}">
                                    {__("Cancel")}
                                </button>
                            {/if}
                        {/if}
                    {/if}
                </div>
            </div>
        </div>
    {elseif $sub_view == "add"}
        <div class="panel-body">
            <form class="form-horizontal" enctype="multipart/form-data" method="post" id="create_medicine">
                <input type="hidden" name="school_username" id="school_username" value="{$username}"/>
                <input type="hidden" name="do" value="add"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Select student")} (*)</label>
                    <div class="col-sm-4">
                        <select name="class_id" id="medicine_class_id" data-username="{$username}" class="form-control" autofocus>
                            <option value="">{__("Select class")}</option>
                            {$class_level = -1}
                            {foreach $classes as $class}
                                {if ($class_level != $class['class_level_id'])}
                                    <option value="" disabled style="color: blue">-----{$class['class_level_name']}-----</option>
                                {/if}
                                <option value="{$class['group_id']}">{$class['group_title']}</option>
                                {$class_level = $class['class_level_id']}
                            {/foreach}
                        </select>
                    </div>
                    <div class="col-sm-5">
                        <select name="child_id" id="medicine_child_id" class="form-control" required></select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Medicine list")} (*)</label>
                    <div class="col-sm-9">
                        <textarea class="form-control js_autosize" name="medicine_list" id="medicine_list_clear" required maxlength="400"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Usage guide")} (*)</label>
                    <div class="col-sm-9">
                        <textarea class="form-control js_autosize" name="guide" id="guide_clear" rows="6" required></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Time per day")} (*)</label>
                    <div class="col-sm-3">
                        <input class="form-control" type="number" name="time_per_day" min="1" maxlength="2" value="2" required>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Time")} (*)</label>
                    <div class='col-sm-3'>
                        <div class='input-group date' id='medicine_beginpicker_new'>
                            <input type='text' name="begin" class="form-control" placeholder="{__("Begin")} (*)" required/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                    <div class='col-sm-1'><i class="fas fa-long-arrow-alt-right fa-lg fa-fw pr10 pt10"></i></div>
                    <div class='col-sm-3'>
                        <div class='input-group date' id='medicine_endpicker_new'>
                            <input type='text' name="end" class="form-control" placeholder="{__("End")} (*)" required/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Doctor prescription")}</label>
                    <div class="col-sm-6">
                        <input type="file" name="file" id="file"/>
                    </div>
                    <div class="col-sm-3">
                        <a class = "delete_image btn btn-danger btn-xs text-left">{__('Delete')}</a>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30" disabled="true">{__("Save")}</button>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    {/if}
</div>