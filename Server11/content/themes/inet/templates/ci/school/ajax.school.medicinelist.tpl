<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover js_dataTable">
        <thead>
        <tr>
            <th>#</th>
            <th>{__('Child')}</th>
            <th>{__('Class')}</th>
            <th>{__("Medicine list")}</th>
            <th>{__("Times/day")}</th>
            <th>{__("Time")}</th>
            <th>{__("Actions")}</th>
        </tr>
        </thead>
        <tbody>
        {$idx = 1}
        {foreach $rows as $row}
            <tr {if $row['child_status'] == 0} class="row-disable" {/if}>
                <td align="center" style="vertical-align:middle"><strong>{$idx}</strong></td>
                <td style="vertical-align:middle">
                    <a href="{$system['system_url']}/school/{$username}/medicines/detail/{$row['medicine_id']}">{$row['child_name']}</a>
                    {if $row['status']== $smarty.const.MEDICINE_STATUS_NEW}
                        <img src="{$system['system_url']}/content/themes/{$system['theme']}/images/new.gif"/>
                    {elseif $row['status']== $smarty.const.MEDICINE_STATUS_CONFIRMED}
                        <i class="fa fa-check"></i>
                    {else}
                        <i class="fa fa-trash-alt"></i>
                    {/if}
                    <br/>({$row['birthday']})
                </td>
                <td align="center" style="vertical-align:middle">{$row['group_title']}</td>
                <td style="vertical-align:middle">
                    {nl2br($row['medicine_list'])}
                    {if $row['source_file'] != null}<br/><br/><a href = "{$row['source_file']}" target="_blank">{__("Doctor prescription")|upper}</a>{/if}
                </td>
                <td align="center" style="vertical-align:middle">{$row['time_per_day']}</td>
                <td align="center" style="vertical-align:middle">
                    {$row['begin']}<br/>
                    <font color="#dc143c">{$row['end']}</font>
                </td>
                <td align="center" nowrap="true">
                    {if $canEdit}
                        {if $row['status'] == $smarty.const.MEDICINE_STATUS_NEW}
                            <button id="button-confirm_{$row['medicine_id']}" class="btn btn-xs btn-default js_school-medicine" data-handle="confirm" data-child="{$row['child_id']}" data-username="{$username}" data-id="{$row['medicine_id']}">
                                {__("Confirm")}
                            </button>
                        {/if}
                        {if ($row['status'] != $smarty.const.MEDICINE_STATUS_CANCEL) && ($row['editable'] == 1)}
                            <a href="{$system['system_url']}/school/{$username}/medicines/edit/{$row['medicine_id']}" class="btn btn-xs btn-default">
                                {__("Edit")}
                            </a>
                        {/if}
                        {if $row['can_delete']}
                            <button class="btn btn-xs btn-danger js_school-delete" data-handle="medicine" data-username="{$username}" data-id="{$row['medicine_id']}">
                                {__("Delete")}
                            </button>
                        {else}
                            {if $row['status'] != $smarty.const.MEDICINE_STATUS_CANCEL}
                                <button class="btn btn-xs btn-warning js_school-medicine-cancel" data-screen="all" data-handle="cancel" data-child="{$row['child_id']}" data-username="{$username}" data-id="{$row['medicine_id']}">
                                    {__("Cancel")}
                                </button>
                            {/if}
                        {/if}
                    {/if}
                </td>
            </tr>
            {$idx = $idx + 1}
        {/foreach}
        </tbody>
    </table>
</div>
<script>
    // run DataTable
    $('.js_dataTable').DataTable({
        "aoColumnDefs": [ { 'aDataSort': false, 'aTargets': [ -1 ] } ],
        "language": {
            "decimal":        "",
            "emptyTable":     __["No data available in table"],
            "info":           __["Showing _START_ to _END_ of _TOTAL_ results"],
            "infoEmpty":      __["Showing 0 to 0 of 0 results"],
            "infoFiltered":   "(filtered from _MAX_ total entries)",
            "infoPostFix":    "",
            "thousands":      ",",
            "lengthMenu":     __["Show"] + " " + "_MENU_" + " " + __["results"],
            "loadingRecords": __["Loading..."],
            "processing":     __["Processing..."],
            "search":         __["Search"],
            "zeroRecords":    __["No matching records found"],
            "paginate": {
                "first":      __["First"],
                "last":       __["Last"],
                "next":       __["Next"],
                "previous":   __["Previous"]
            },
            "aria": {
                "sortAscending":  ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            }
        }
    });
</script>
