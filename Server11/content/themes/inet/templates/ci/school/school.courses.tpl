<div class="" style="position: relative">
    <div class="panel panel-default">
        <div class="panel-heading with-icon">
            <i class="fa fa-graduation-cap fa-fw fa-lg pr10"></i>
            {__("Courses")}
            {if $sub_view == "edit"}
                &rsaquo; {__('Edit')}
            {/if}
        </div>
        {if $sub_view == ""}
            <div class="panel-body with-table">
                    <form class="form-horizontal" action="#" enctype="multipart/form-data" method="post" id="course_search_form">
                    <input type="hidden" name="school_username" id="school_username" value="{$username}"/>
                    <input type="hidden" name="do" value="search_course"/>
                    <div class="form-group">
                        <div class="col-sm-3 col-sm-offset-9">
                            <select name="school_year" id="school_year" class="form-control">
                                {foreach $default_year as $key => $year}
                                    {*                                <option value="{$year}" {if $key == count($default_year) - 1}selected{/if}>{$year}</option>*}
                                    <option value="{$year}"
                                            {if (substr($year, 0, strlen(date("Y"))) === date("Y"))}selected{/if}>{$year}</option>
                                {/foreach}
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Select class level")} (*)</label>
                        <div class="col-sm-3">
                            <select name="class_level_id" id="course_class_level_id" data-username="{$username}"
                                    class="form-control" autofocus>
                                <option value="">{__("Select class level")}</option>
                                {foreach $class_levels as $level}
                                    <option value="{$level['class_level_id']}">{$level['class_level_name']}</option>
                                {/foreach}
                            </select>
                        </div>
                        <div class="col-sm-3">
                            <select name="class_id" id="course_class_id" class="form-control"
                                    data-username="{$username}" required>
                                <option value="">{__("Select class")}...</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                    </div>
                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-3">
                            <button type="submit" id="submit_id" class="btn btn-primary padrl30">{__("Search")}</button>
                        </div>
                    </div>
                    <div id="course_list_point"></div>
                    <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                    <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                </div>
            </div>
        {/if}
    </div>
</div>