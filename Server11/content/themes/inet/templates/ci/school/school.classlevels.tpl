<div class="panel panel-default">
    <div class="panel-heading with-icon">
        {if ($sub_view == "") && $canEdit}
            <div class="pull-right flip">
                <a href="#" class="btn btn-default js_school-class-level-add">
                    <i class="fa fa-plus"></i> {__("Add New Class Level")}
                </a>
            </div>
        {/if}
        {if $school['school_step'] == SCHOOL_STEP_FINISH  || $school['grade'] != 0}
            <div class="pull-right flip" style="margin-right: 5px">
{*                <a href="https://blog.inet.vn/huong-dan-tao-thong-tin-khoi/" class="btn btn-info btn_guide" target="_blank">*}
                <a href="#" class="btn btn-info btn_guide" target="_blank">
                    <i class="fa fa-info-circle"></i> {__("Guide")}
                </a>
            </div>
        {/if}
        <i class="fa fa-tree fa-fw fa-lg pr10"></i>
        {__("Class level")}
        {if $sub_view == "edit"}
            &rsaquo; {$data['class_level_name']}
        {elseif $sub_view == "add"}
            &rsaquo; {__('Add New')}
        {/if}
    </div>
    {if $sub_view == ""}
        <div class="panel-body with-table">
            <div class="x-hidden add_class_level_box" style="position: relative">
                <div class="" style="position: absolute; top: 0; right: 0;"><a href="#" class="close_box_add"><i style="font-size: 20px" class="fas fa-window-close"></i></a></div>
                <div class="" style="border: 1px solid #b1b1b1; padding: 5px; margin-bottom: 20px">
                    <div><strong>{__("Add New Class Level")}:</strong></div>
                    <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_class_level.php">
                        <input type="hidden" name="school_username" id="school_username" value="{$username}"/>
                        <input type="hidden" name="do" value="add"/>
                        <div class="form-group">
                            <label class="col-sm-3 control-label text-left">
                                {__("Class level name")}
                            </label>
                            <div class="col-sm-9">
                                <input class="form-control" name="class_level_name" required maxlength="45">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label text-left">
                                {__("Rank level")}
                            </label>
                            <div class="col-sm-9">
                                <input class="form-control" type="number" name="gov_class_level" required maxlength="45">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label text-left">
                                {__("Description")}
                            </label>
                            <div class="col-sm-9">
                                <textarea class="form-control" name="description" rows="3" maxlength="300"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-9 col-sm-offset-3">
                                {if $canEdit}
                                    <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                                {/if}
                            </div>
                        </div>

                        <!-- success -->
                        <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                        <!-- success -->

                        <!-- error -->
                        <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                        <!-- error -->
                    </form>
                </div>
            </div>
            <div class="edit_box x-hidden" style="position: relative">
                <div class="" style="position: absolute; top: 0; right: 0;"><a href="#" class="close_box_edit"><i style="font-size: 20px" class="fas fa-window-close"></i></a></div>
                <div class="" style="border: 1px solid #b1b1b1; padding: 5px; margin-bottom: 20px">
                    <div><strong>{__("Edit class level")}: <span class="span_class_level_name"></span></strong></div>
                    <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_class_level.php">
                        <input type="hidden" name="school_username" id="school_username" value="{$username}"/>
                        <input type="hidden" name="class_level_id" value="{$data['class_level_id']}"/>
                        <input type="hidden" name="do" value="edit"/>
                        <div class="form-group">
                            <label class="col-sm-3 control-label text-left">
                                {__("Class level name")} (*)
                            </label>
                            <div class="col-sm-9">
                                <input class="form-control" name="class_level_name" value="{$data['class_level_name']}" required maxlength="45">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label text-left">
                                {__("Description")}
                            </label>
                            <div class="col-sm-9">
                                <textarea class="form-control" name="description" rows="3" maxlength="300">{$data['description']}</textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-5 col-sm-offset-3">
                                {if $canEdit}
                                    <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                                {/if}
                            </div>
                        </div>

                        <!-- success -->
                        <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                        <!-- success -->

                        <!-- error -->
                        <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                        <!-- error -->
                    </form>
                </div>
            </div>
            <div><strong>{__("Class level list")}&nbsp;({$rows|count})</strong></div>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>{__("Class level name")}</th>
                            <th>{__("Total Class")}</th>
                            <th>{__("Total Subject")}</th>
                            <th>{__("Description")}</th>
                            <th>{__("Actions")}</th>
                        </tr>
                    </thead>
                    <tbody>
                        {foreach $rows as $row}
                            <tr>
                                <td class="align-middle">{$row['class_level_name']}</td>
                                <td  class="align-middle" align="center">{$row['cnt']} {__("Class")}</td>
                                <td  class="align-middle" align="center">{$row['cntSubject']} {__("Subject")}</td>
                                <td class="align-middle">{$row['description']}</td>
                                <td  class="align-middle" align="center" style="vertical-align: middle">
                                    {if $canEdit}
                                        <a href="#" class="btn btn-xs btn-default js_class-level-edit" data-id="{$row['class_level_id']}" data-name="{$row['class_level_name']}" data-description="{$row['description']}">{__("Edit")}</a>
                                        {if !$row['cnt']}
                                            <button class="btn btn-xs btn-danger js_school-delete" data-handle="class_level" data-username="{$username}" data-id="{$row['class_level_id']}">{__("Delete")}</button>
                                        {/if}
                                    {/if}
                                </td>
                            </tr>
                        {/foreach}
                    </tbody>
                </table>
            </div>
        </div>
    {elseif $sub_view == "edit"}
        <div class="panel-body">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_class_level.php">
                <input type="hidden" name="school_username" id="school_username" value="{$username}"/>
                <input type="hidden" name="class_level_id" value="{$data['class_level_id']}"/>
                <input type="hidden" name="do" value="edit"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">
                        {__("Class level name")} (*)
                    </label>
                    <div class="col-sm-9">
                        <input class="form-control" name="class_level_name" value="{$data['class_level_name']}" required autofocus maxlength="45">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">
                        {__("Description")}
                    </label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="description" rows="3" maxlength="300">{$data['description']}</textarea>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-5 col-sm-offset-3">
                        {if $canEdit}
                            <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                        {/if}
                        <a href="{$system['system_url']}/school/{$username}/classlevels" class="btn btn-default">{__("Lists")}</a>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    {elseif $sub_view == "add"}
        <div class="panel-body">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_class_level.php">
                <input type="hidden" name="school_username" id="school_username" value="{$username}"/>
                <input type="hidden" name="do" value="add"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">
                        {__("Class level name")}
                    </label>
                    <div class="col-sm-9">
                        <input class="form-control" name="class_level_name" required autofocus maxlength="45">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">
                        {__("Description")}
                    </label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="description" rows="3" maxlength="300"></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        {if $canEdit}
                            <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                        {/if}
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    {/if}
</div>