<table class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th>#</th>
        <th>{__("Select")}</th>
        <th>{__("Teacher")}</th>
        <th>{__("Created time")}</th>
    </tr>
    </thead>
    <tbody>
    {$idx = 1}
    {foreach $participants as $teacher}
        <tr>
            <td align="center">{$idx}</td>
            <td align="center">
                <input type="checkbox" class="teacher" name="teacherIds[]" value="{$teacher['user_id']}"
                       {if $teacher['event_id'] > 0}checked {if $teacher['created_user_id']!=$user->_data['user_id']}disabled{/if}{/if}>
                {if $teacher['event_id'] > 0  && ($teacher['created_user_id'] == $user->_data['user_id'])}
                    <input type="hidden" name="oldTeacherIds[]" value="{$teacher['user_id']}"/>
                {/if}
            </td>
            <td>{$teacher['user_fullname']}</td>
            <td>{$teacher['created_at']}</td>
        </tr>

        {$idx = $idx + 1}
    {/foreach}

    {if $idx == 1}
        <tr class="odd">
            <td valign="top" align="center" colspan="7" class="dataTables_empty">
                {__("No data available in table")}
            </td>
        </tr>
    {/if}
    </tbody>
</table>