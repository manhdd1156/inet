<div class="color_red mb10">
    <strong>Lưu ý:</strong> Tài khoản giáo viên sẽ được thêm vào trường của bạn sau khi bạn nhấn lưu ở bước này.
</div>
<form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_teacher.php">
    <input type="hidden" name="school_username" value="{$username}"/>
    <input type="hidden" name="do" value="add_teacher_new_class"/>
    <div id="userIds"></div>
    <div class="form-group">
        <label class="col-sm-3 control-label text-left">{__("Full name")} (*)</label>
        <div class="col-sm-9">
            <input name="full_name" id="full_name" type="text" class="form-control" autofocus maxlength="255">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label text-left">{__("Telephone")} (*)</label>
        <div class="col-sm-9">
            <input name="user_phone" id="user_phone" type="text" class="form-control" maxlength="50">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label text-left">{__("Email")} (*)</label>
        <div class="col-sm-9">
            <input name="email" id="email_teacher" type="email" class="form-control" maxlength="100">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label text-left">{__("Password")} (*)</label>
        <div class="col-sm-9">
            <input name="password" id="password" type="password" class="form-control">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label text-left">{__("Gender")} (*)</label>
        <div class="col-sm-3">
            <select name="gender" id="gender" class="form-control" >
                <option value="{$smarty.const.FEMALE}">{__("Female")}</option>
                <option value="{$smarty.const.MALE}">{__("Male")}</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-9 col-sm-offset-3">
            <a class="btn btn-primary padrl30 js_school-teacher-add-done">{__("Save")}</a>
        </div>
    </div>

    <!-- success -->
    <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
    <!-- success -->

    <!-- error -->
    <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
    <!-- error -->
</form>