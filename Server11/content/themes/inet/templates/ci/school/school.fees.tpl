<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <div class="pull-right flip">
            {if $school['school_step'] == SCHOOL_STEP_FINISH  || $school['grade'] != 0}
{*                <a href="https://blog.coniu.vn/huong-dan-tao-cac-loai-phi-cho-tre/" class="btn btn-info btn_guide" target="_blank">*}
                <a href="#" class="btn btn-info btn_guide" target="_blank">
                    <i class="fa fa-info-circle"></i> {__("Guide")}
                </a>
            {/if}
            {if $sub_view == ""}
                {if $canEdit}
                    <a href="{$system['system_url']}/school/{$username}/fees/add" class="btn btn-default">
                        <i class="fa fa-plus"></i> {__("Add New Fee")}
                    </a>
                {/if}
                <a href="{$system['system_url']}/school/{$username}/fees/inactive" class="btn btn-warning">
                    <i class="fa fa-adjust"></i> {__("Inactive fee list")}
                </a>
            {elseif $sub_view == "inactive"}
                <a href="{$system['system_url']}/school/{$username}/fees" class="btn btn-default">
                    <i class="fa fa-adjust"></i> {__("Active fee list")}
                </a>
            {elseif $sub_view == "edit"}
                <a href="{$system['system_url']}/school/{$username}/fees/add" class="btn btn-default">
                    <i class="fa fa-plus"></i> {__("Add New Fee")}
                </a>
            {/if}
        </div>
        <i class="fas fa-money-bill-alt fa-lg fa-fw pr10"></i>
        {__("Fee")}
        {if $sub_view == "edit"}
            &rsaquo; {$data['fee_name']}
        {elseif $sub_view == "add"}
            &rsaquo; {__('Add New')}
        {elseif $sub_view == ""}
            &rsaquo; {__('Active fee list')}
        {elseif $sub_view == "inactive"}
            &rsaquo; {__('Inactive fee list')}
        {/if}
    </div>
    {if $sub_view == ""}
        <div class="panel-body with-table">
            <div class="table-responsive">
                <div><strong>{__("Fee list")}&nbsp;(<span class="count_fee">{$rows|count}</span>)</strong></div>
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>{__("Fee name")}</th>
                            <th>{__("Fee cycle")}</th>
                            <th>{__("Unit price")} ({$smarty.const.MONEY_UNIT})</th>
                            <th>{__("Fee level")}</th>
                            <th>{__("Status")}</th>
                            <th>{__("Description")}</th>
                            <th>{__("Actions")}</th>
                        </tr>
                    </thead>
                    <tbody>
                        {$idx = 1}
                        {foreach $rows as $row}
                            <tr class="list_fee_{$row['fee_id']}">
                                <td align="center" style="vertical-align:middle" {if $row['level']==$smarty.const.CHILD_LEVEL}rowspan="2"{/if}><strong>{$idx}</strong></td>
                                <td style="vertical-align:middle">
                                    <strong>{$row['fee_name']}</strong>
                                    {if $row['level']==$smarty.const.CHILD_LEVEL}
                                        <br/>{__("Replace for")}:
                                        {foreach $row['fee_changes'] as $k => $fee_change}
                                            <br/>{$k+1}-{$fee_change['fee_name']}
                                        {/foreach}
                                    {/if}
                                </td>
                                <td align="center" style="vertical-align:middle">
                                    {if $row['type']==$smarty.const.FEE_TYPE_MONTHLY}
                                        {__("Monthly fee")}
                                    {elseif $row['type']==$smarty.const.FEE_TYPE_DAILY}
                                        {__("Daily fee")}
                                    {/if}
                                </td>
                                <td align="right" style="vertical-align:middle">{number_format($row['unit_price'],0,'.',',')}</td>
                                <td align="center" style="vertical-align:middle">
                                    {if $row['level']==$smarty.const.SCHOOL_LEVEL}
                                        {__("School")}
                                    {elseif $row['level']==$smarty.const.CLASS_LEVEL_LEVEL}
                                        {$row['class_level_name']}
                                    {elseif $row['level']==$smarty.const.CLASS_LEVEL}
                                        {$row['group_title']}
                                    {elseif $row['level']==$smarty.const.CHILD_LEVEL}
                                        {__("Children")}
                                    {/if}
                                </td>
                                <td align="center" style="vertical-align:middle">
                                    {if $row['status'] == 0}
                                        <i class="fa fa-times" aria-hidden="true"></i>
                                    {else}
                                        <i class="fa fa-check" aria-hidden="true"></i>
                                    {/if}
                                </td>
                                <td style="vertical-align:middle">{$row['description']}</td>
                                <td nowrap="true" style="vertical-align:middle">
                                    {if $canEdit}
                                        <a href="{$system['system_url']}/school/{$username}/fees/edit/{$row['fee_id']}" class="btn btn-xs btn-default">{__("Edit")}</a>
                                        <button class="btn btn-xs btn-warning js_school-fee-cancel" data-username="{$username}" data-id="{$row['fee_id']}">{__("Inactive")}</button>
                                        {if $row['used'] == 0}
                                            <button class="btn btn-xs btn-danger js_school-fee-delete" data-username="{$username}" data-id="{$row['fee_id']}">{__("Delete")}</button>
                                        {/if}
                                    {/if}
                                </td>
                            </tr>
                            {if $row['level']==$smarty.const.CHILD_LEVEL}
                                <tr class="list_fee_{$row['fee_id']}">
                                    <td align="center" style="vertical-align:middle">
                                        <strong>{__("Applied student")}</strong>
                                    </td>
                                    <td colspan="6">
                                        <a href="#" class="btn-primary js_sh_children" data-id="{$row['fee_id']}">{__("Show/hide student list")}</a>
                                        <div class="children_list_{$row['fee_id']} x-hidden">
                                            <table class="table table-striped table-bordered table-hover">
                                                <tbody>
                                                    {$id = 1}
                                                    {foreach $row['children'] as $child}
                                                        <tr>
                                                            <td align="center">{$id}</td>
                                                            <td>{$child['child_name']}</td>
                                                            <td>{$child['group_title']}</td>
                                                        </tr>
                                                        {$id = $id + 1}
                                                    {/foreach}
                                                </tbody>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            {/if}
                            {$idx = $idx + 1}
                        {/foreach}

                        {if $idx == 1}
                            <tr class="odd">
                                <td valign="top" align="center" colspan="8" class="dataTables_empty">
                                    {__("No data available in table")}
                                </td>
                            </tr>
                        {/if}
                    </tbody>
                </table>
            </div>
        </div>
    {elseif $sub_view == "inactive"}
        <div class="panel-body with-table">
            <div class="table-responsive">
                <div><strong>{__("Inactive fee list")}&nbsp;({$rows|count})</strong></div>
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>{__("Fee name")}</th>
                        <th>{__("Fee cycle")}</th>
                        <th>{__("Unit price")} ({$smarty.const.MONEY_UNIT})</th>
                        <th>{__("Fee level")}</th>
                        <th>{__("Status")}</th>
                        <th>{__("Description")}</th>
                        <th>{__("Actions")}</th>
                    </tr>
                    </thead>
                    <tbody>
                    {$idx = 1}
                    {foreach $rows as $row}
                        <tr>
                            <td align="center" style="vertical-align:middle" {if $row['level']==$smarty.const.CHILD_LEVEL}rowspan="2"{/if}><strong>{$idx}</strong></td>
                            <td style="vertical-align:middle">
                                <strong>{$row['fee_name']}</strong>
                                {if $row['level']==$smarty.const.CHILD_LEVEL}
                                    <br/>{__("Replace for")}:
                                    {foreach $row['fee_changes'] as $k => $fee_change}
                                        <br/>{$k+1}-{$fee_change['fee_name']}
                                    {/foreach}
                                {/if}
                            </td>
                            <td align="center" style="vertical-align:middle">
                                {if $row['type']==$smarty.const.FEE_TYPE_MONTHLY}
                                    {__("Monthly fee")}
                                {elseif $row['type']==$smarty.const.FEE_TYPE_DAILY}
                                    {__("Daily fee")}
                                {/if}
                            </td>
                            <td align="right" style="vertical-align:middle">{number_format($row['unit_price'],0,'.',',')}</td>
                            <td align="center" style="vertical-align:middle">
                                {if $row['level']==$smarty.const.SCHOOL_LEVEL}
                                    {__("School")}
                                {elseif $row['level']==$smarty.const.CLASS_LEVEL_LEVEL}
                                    {$row['class_level_name']}
                                {elseif $row['level']==$smarty.const.CLASS_LEVEL}
                                    {$row['group_title']}
                                {elseif $row['level']==$smarty.const.CHILD_LEVEL}
                                    {__("Children")}
                                {/if}
                            </td>
                            <td align="center" style="vertical-align:middle">
                                {if $row['status'] == 0}
                                    <i class="fa fa-times" aria-hidden="true"></i>
                                {else}
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                {/if}
                            </td>
                            <td style="vertical-align:middle">{$row['description']}</td>
                            <td nowrap="true" style="vertical-align:middle">
                                {if $canEdit}
                                    <a href="{$system['system_url']}/school/{$username}/fees/edit/{$row['fee_id']}" class="btn btn-xs btn-default">{__("Edit")}</a>
                                    {if $row['used'] == 0}
                                        <button class="btn btn-xs btn-danger js_school-fee-delete" data-username="{$username}" data-id="{$row['fee_id']}">{__("Delete")}</button>
                                    {/if}
                                {/if}
                            </td>
                        </tr>
                        {if $row['level']==$smarty.const.CHILD_LEVEL}
                            <tr>
                                <td align="center" style="vertical-align:middle">
                                    <strong>{__("Applied student")}</strong>
                                </td>
                                <td colspan="6">
                                    <a href="#" class="btn-primary js_sh_children" data-id="{$row['fee_id']}">{__("Show/hide student list")}</a>
                                    <div class="children_list_{$row['fee_id']} x-hidden">
                                        <table class="table table-striped table-bordered table-hover">
                                            <tbody>
                                            {$id = 1}
                                            {foreach $row['children'] as $child}
                                                <tr>
                                                    <td align="center">{$id}</td>
                                                    <td>{$child['child_name']}</td>
                                                    <td>{$child['group_title']}</td>
                                                </tr>
                                                {$id = $id + 1}
                                            {/foreach}
                                            </tbody>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        {/if}
                        {$idx = $idx + 1}
                    {/foreach}

                    {if $idx == 1}
                        <tr class="odd">
                            <td valign="top" align="center" colspan="8" class="dataTables_empty">
                                {__("No data available in table")}
                            </td>
                        </tr>
                    {/if}
                    </tbody>
                </table>
            </div>
        </div>
    {elseif $sub_view == "edit"}
        <div class="panel-body">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_tuition.php">
                <input type="hidden" name="school_username" id="school_username" value="{$username}"/>
                <input type="hidden" name="fee_id" value="{$data['fee_id']}"/>
                <input type="hidden" name="do" value="edit_fee"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Fee name")} (*)</label>
                    <div class="col-sm-9">
                        <input class="form-control" name="fee_name" value="{$data['fee_name']}" required autofocus maxlength="100">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Fee cycle")} (*)</label>
                    <div class="col-sm-4">
                        <input type="hidden" name="type" id="type" value="{$data['type']}">
                        <input type="text" value="{if $data['type']==$smarty.const.FEE_TYPE_MONTHLY}{__("Monthly fee")}{else}{__("Daily fee")}{/if}" class="form-control" disabled>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Unit price")}</label>
                    <div class="col-sm-9">
                        <div class="input-group col-md-5">
                            <input type="text" class="form-control money_tui" name="unit_price" value="{$data['unit_price']}" maxlength="11">
                            <span class="input-group-addon">{$smarty.const.MONEY_UNIT}</span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Fee level")} (*)</label>
                    <div class="col-sm-3">
                        <select name="level" id="fee_level" class="form-control" {if $data['level']==$smarty.const.CHILD_LEVEL}disabled{/if}>
                            <option value="{$smarty.const.SCHOOL_LEVEL}" {if $data['level']==$smarty.const.SCHOOL_LEVEL}selected{/if}>{__("School")}</option>
                            <option value="{$smarty.const.CLASS_LEVEL_LEVEL}" {if $data['level']==$smarty.const.CLASS_LEVEL_LEVEL}selected{/if}>{__("Class level")}</option>
                            <option value="{$smarty.const.CLASS_LEVEL}" {if $data['level']==$smarty.const.CLASS_LEVEL}selected{/if}>{__("Class")}</option>
                            <option value="{$smarty.const.CHILD_LEVEL}" {if $data['level']==$smarty.const.CHILD_LEVEL}selected{/if}>{__("Child")}</option>
                        </select>
                    </div>
                    <div class="col-sm-3 {if $data['level'] != ($smarty.const.CLASS_LEVEL_LEVEL)}x-hidden{/if}" name="fee_class_level">
                        <select name="class_level_id" id="class_level_id" class="form-control">
                            {foreach $class_levels as $class_level}
                                <option value="{$class_level['class_level_id']}" {if $data['class_level_id']==$class_level['class_level_id']}selected{/if}>{$class_level['class_level_name']}</option>
                            {/foreach}
                        </select>
                    </div>
                    <div class="col-sm-3 {if (($data['level'] != $smarty.const.CLASS_LEVEL) && ($data['level'] != $smarty.const.CHILD_LEVEL))}x-hidden{/if}" name="fee_class">
                        <select name="class_id" id="fee_class_id" data-username="{$username}" data-id="{$data['fee_id']}" class="form-control">
                            <option value="">{__("Select class")}</option>
                            {$class_level = -1}
                            {foreach $classes as $class}
                                {if ($class_level != $class['class_level_id'])}
                                    <option value="" disabled>-----{$class['class_level_name']}-----</option>
                                {/if}
                                <option value="{$class['group_id']}" {if $data['class_id']==$class['group_id']}selected{/if}>{$class['group_title']}</option>
                                {$class_level = $class['class_level_id']}
                            {/foreach}
                        </select>
                    </div>
                </div>

                <div class="form-group" name="fee_child" id="fee_child_id">
                    {if $data['level'] == $smarty.const.CHILD_LEVEL}
                        {$children_list_html}
                    {/if}
                </div>

                <div class = "form-group {if $data['level'] != $smarty.const.CHILD_LEVEL }x-hidden{/if}" name = "fee_change">
                    <label class = "col-sm-3 control-label text-left">{__("Replace for")}</label>
                    <div class = "col-sm-9">
                        {foreach $fees as $fee}
                            <div class ="col-sm-6"><input type="checkbox" name="fee_change_id[]" value="{$fee['fee_id']}" {if $fee['checked']==1}checked{/if}>&nbsp;{$fee['fee_name']}</div>
                        {/foreach}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Description")}</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="description" rows="4">{$data['description']}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Applied")}?</label>
                    <div class="col-sm-9">
                        <div class="onoffswitch">
                            <input type="checkbox" name="status" class="onoffswitch-checkbox" id="status" {if $data['status']}checked{/if}>
                            <label class="onoffswitch-label" for="status"></label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                        <a href="{$system['system_url']}/school/{$username}/fees" class="btn btn-default">{__("Lists")}</a>
                    </div>
                </div>

                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
            </form>
        </div>
    {elseif $sub_view == "add"}
        <div class="panel-body">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_tuition.php">
                <input type="hidden" name="school_username" id="school_username" value="{$username}"/>
                <input type="hidden" name="do" value="add_fee"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Fee name")} (*)</label>
                    <div class="col-sm-9">
                        <input class="form-control" name="fee_name" required autofocus maxlength="100">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Fee cycle")} (*)</label>
                    <div class="col-sm-4">
                        <select name="type" id="type" class="form-control">
                            <option value="{$smarty.const.FEE_TYPE_MONTHLY}">{__("Monthly fee")}</option>
                            <option value="{$smarty.const.FEE_TYPE_DAILY}">{__("Daily fee")}</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Unit price")}</label>
                    <div class="col-sm-9">
                        <div class="input-group col-md-5">
                            <input type="text" min="0" lang="en-150" class="form-control money_tui" name="unit_price" value="0" maxlength="11">
                            <span class="input-group-addon">{$smarty.const.MONEY_UNIT}</span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Fee level")} (*)</label>
                    <div class="col-sm-3">
                        <select name="level" id="fee_level" class="form-control">
                            <option value="{$smarty.const.SCHOOL_LEVEL}">{__("School")}</option>
                            <option value="{$smarty.const.CLASS_LEVEL_LEVEL}">{__("Class level")}</option>
                            <option value="{$smarty.const.CLASS_LEVEL}">{__("Class")}</option>
                            <option value="{$smarty.const.CHILD_LEVEL}">{__("Child")}</option>
                        </select>
                    </div>
                    <div class="col-sm-3 x-hidden" name="fee_class_level">
                        <select name="class_level_id" id="class_level_id" class="form-control">
                            {foreach $class_levels as $class_level}
                                <option value="{$class_level['class_level_id']}">{$class_level['class_level_name']}</option>
                            {/foreach}
                        </select>
                    </div>
                    <div class="col-sm-3 x-hidden" name="fee_class">
                        <select name="class_id" id="fee_class_id" data-username="{$username}" class="form-control">
                            <option value="">{__("Select class")}...</option>

                            {$class_level = -1}
                            {foreach $classes as $class}
                                {if ($class_level != $class['class_level_id'])}
                                    <option value="" disabled>-----{$class['class_level_name']}-----</option>
                                {/if}
                                <option value="{$class['group_id']}">{$class['group_title']}</option>
                                {$class_level = $class['class_level_id']}
                            {/foreach}
                        </select>
                    </div>
                </div>

                <div class = "form-group x-hidden" name="fee_child" id="fee_child_id"></div>

                <div class = "form-group x-hidden" name = "fee_change">
                    <label class = "col-sm-3 control-label text-left">{__("Replace for")}</label>
                    <div class = "col-sm-9">
                        {foreach $fees as $fee}
                            <div class ="col-sm-6"><input type="checkbox" name="fee_change_id[]" value="{$fee['fee_id']}">&nbsp;{$fee['fee_name']}</div>
                        {/foreach}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Description")}</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="description" rows="4"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Applied")}?</label>
                    <div class="col-sm-9">
                        <div class="onoffswitch">
                            <input type="checkbox" name="status" class="onoffswitch-checkbox" id="status" checked>
                            <label class="onoffswitch-label" for="status"></label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                    </div>
                </div>

                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
            </form>
        </div>
    {/if}
</div>