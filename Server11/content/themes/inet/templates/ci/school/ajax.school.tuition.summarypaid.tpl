<div class="panel-body with-table">
    <div><strong>{__("Student list")}&nbsp;({$children|count})</strong></div>
    <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover js_dataTable">
            <thead>
                <tr>
                    <th>#</th>
                    <th nowrap="true">{__("Child")}</th>
                    <th>{__("Total amount payable")}</th>
                    <th>{__("Paid")}</th>
                    <th>{__("Debt amount of this month")}</th>
                    <th>{__("Monthly Fee")}</th>
                    <th>{__("Cashier")}</th>
                    <th>{__("Time")}</th>
                </tr>
            </thead>
            <tbody>
            {$total = 0}
            {$totalPaid = 0}
            {$totalDebt = 0}
            {$classId = -1}
            {$idx = 1}
            {foreach $children as $row}
                {if $classId != $row['class_id']}
                    <tr>
                        <td colspan="7">
                            {if $row['class_id'] > 0}
                                {__("Class")}:&nbsp;{$row['group_title']}
                            {else}
                                {__("No class")}
                            {/if}
                        </td>
                    </tr>
                {/if}
                <tr>
                    <td class="align-middle" align="center">{$idx}</td>
                    <td class="align-middle"><a
                                href="{$system['system_url']}/school/{$username}/children/detail/{$row['child_id']}">{$row['child_name']}</a>
                    </td>
                    <td class="align-middle" align="right">{moneyFormat($row['total_amount'])}</td>
                    <td class="align-middle" align="right">{moneyFormat($row['paid_amount'])}</td>
                    <td class="align-middle" align="right">{moneyFormat($row['total_amount'] - $row['paid_amount'])}</td>
                    <td class="align-middle" align="center">{$row['month']}</td>
                    <td class="align-middle">{$row['user_fullname']}</td>
                    <td class="align-middle">{$row['paid_at']}</td>
                </tr>
                {$debt = $row['total_amount'] - $row['paid_amount']}
                {$total = $total + $row['total_amount']}
                {$totalPaid = $totalPaid + $row['paid_amount']}
                {$totalDebt = $totalDebt + $debt}
                {$classId = $row['class_id']}
                {$idx = $idx + 1}
            {/foreach}
            <tr>
                <td colspan="2" align="right"><strong>{__("Total")}</strong></td>
                <td align="right"><strong>{moneyFormat($total)}</strong></td>
                <td align="right"><strong>{moneyFormat($totalPaid)}</strong></td>
                <td align="right"><strong>{moneyFormat($totalDebt)}</strong></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            </tbody>
        </table>
    </div>
</div>