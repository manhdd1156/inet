<div class="form-group pl5" id="service_btnSave">
    <div class="col-sm-9">
        <button type="submit" class="btn btn-primary padrl30" disabled>{__("Save")}</button>
    </div>
</div>

<div>
    <strong>{__("Student list")}&nbsp;({__("Children")}: {$results['children']|count}&nbsp;|&nbsp;{__("Register")}: {$results['use_count']})</strong>
    <div class="pull-right flip">
        {__("Date format")}: DD/MM/YYYY.<br/>
        Không nhập ngày hay nhập sai, hệ thống tự lấy ngày hiện tại.<br/>
        <label>Lựa chọn Xóa: chỉ dành để xóa các đăng ký nhầm.</label>
    </div>
</div>
<table class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th>#</th>
            <th>{__("Register")}/{__("Cancel")}</th>
            <th>{__("Full name")}</th>
            <th>{__("Birthdate")}</th>
            <th>{__("Begin")}</th>
            <th>{__("End")}</th>
            <th>{__("Delete")}</th>
        </tr>
    </thead>
    <tbody>
        {$classId = -1}
        {$idx = 1}
        {foreach $results['children'] as $row}
            {if ((!isset($class_id) || ($class_id == 0)) && ($classId != $row['class_id']))}
                <tr>
                    <td colspan="5"><strong>{$row['group_title']}</strong></td>
                </tr>
            {/if}
            <tr {if ($row['child_status'] == 0)}class="row-disable"{/if}>
                <td class="align-middle" align="center">{$idx}</td>
                <td class="align-middle" align="center">
                    <input type="checkbox" name="childIds[]" value="{$row['child_id']}" {if $row['status'] > 0}checked{/if}>
                    <input type="hidden" name="allChildIds[]" value="{$row['child_id']}"/>
                    {if $row['status'] > 0}
                        <input type="hidden" name="oldChildIds[]" value="{$row['child_id']}"/>
                    {/if}
                </td>
                <td class="align-middle"><a href="{$system['system_url']}/school/{$username}/children/edit/{$row['child_id']}">{$row['child_name']}</a></td>
                <td class="align-middle">{$row['birthday']}</td>
                <td class="align-middle" align="center">
                    <input style="width: 100px" type='text' name="begin_{$row['child_id']}" class="reg_service_begin" value="{$row['begin']}" class="form-control"/>
                    <input type='hidden' name="old_begin_{$row['child_id']}" value="{$row['begin']}"/>
                </td>
                <td class="align-middle" align="center">
                    <input style="width: 100px" type='text' name="end_{$row['child_id']}" class="reg_service_end" value="{$row['end']}" class="form-control"/>
                    <input type='hidden' name="old_end_{$row['child_id']}" value="{$row['end']}"/>
                </td>
                <td class="align-middle" align="center">
                    {if $row['status'] > 0}
                        <a href="#" class="btn btn-xs btn-danger js_school_delete-mdservice-registration" data-id="{$row['child_id']}">{__("Delete")}</a>
                    {/if}
                </td>
            </tr>
            {$classId = $row['class_id']}
            {$idx = $idx + 1}
        {/foreach}
        {if $idx == 1}
            <tr class="odd">
                <td valign="top" align="center" colspan="7" class="dataTables_empty">
                    {__("No data available in table")}
                </td>
            </tr>
        {/if}
    </tbody>
</table>