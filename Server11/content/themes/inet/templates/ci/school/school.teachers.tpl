<div class="panel panel-default">
    <div class="panel-heading with-icon">
        {if $canEdit}
            <div class="pull-right flip">
                {if $sub_view == ""}
                    <a href="{$system['system_url']}/school/{$username}/teachers/addexisting" class="btn btn-default">
                        <i class="fa fa-plus"></i> {__("Add Existing Account")}
                    </a>
                    <a href="{$system['system_url']}/school/{$username}/teachers/add" class="btn btn-default">
                        <i class="fa fa-plus"></i> {__("Add New")}
                    </a>
                {elseif $sub_view == "add"}
                    <a href="{$system['system_url']}/school/{$username}/teachers/addexisting" class="btn btn-default">
                        <i class="fa fa-plus"></i> {__("Add Existing Account")}
                    </a>
                {elseif $sub_view == "addexisting"}
                    <a href="{$system['system_url']}/school/{$username}/teachers/add" class="btn btn-default">
                        <i class="fa fa-plus"></i> {__("Add New")}
                    </a>
                {/if}
            </div>
        {/if}
        <div class="pull-right flip" style="margin-right: 5px">
            {if $school['school_step'] == SCHOOL_STEP_FINISH  || $school['grade'] != 0}
                {if $sub_view != 'addexisting'}
                    <a href="https://blog.coniu.vn/huong-dan-tao-tai-khoan-va-them-moi-giao-vien-vao-truong/" class="btn btn-info  btn_guide" target="_blank">
                        <i class="fa fa-info-circle"></i> {__("Guide")}
                    </a>
                {else}
                    <a href="https://blog.coniu.vn/huong-dan-them-tai-khoan-giao-vien-da-dang-ky-tren-he-thong/" class="btn btn-info  btn_guide" target="_blank">
                        <i class="fa fa-info-circle"></i> {__("Guide")}
                    </a>
                {/if}
            {/if}
            {if $sub_view != ''}
                <a href="{$system['system_url']}/school/{$username}/teachers" class="btn btn-default">
                    <i class="fa fa-list"></i> {__("Lists")}
                </a>
            {/if}
        </div>
        <i class="fa fa-street-view fa-fw fa-lg pr10"></i>
        {__("Teacher - Employee")}
        {if $sub_view == "edit"}
            &rsaquo; {$data['user_fullname']}
        {elseif $sub_view == "add"}
            &rsaquo; {__('Add New')}
        {elseif $sub_view == ""}
            &rsaquo; {__('Lists')}
        {elseif $sub_view == "addexisting"}
            &rsaquo; {__('Add Existing Account')}
        {elseif $sub_view == "assign"}
            &rsaquo; {__('Assign teacher')}
        {/if}
    </div>
    {if $sub_view == ""}
        <div class="panel-body with-table">
            <div class="mb5"><strong>{__("Teacher - Employee list")}&nbsp;({$rows|count})</strong></div>

            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>{__("Full name")}</th>
                            <th>{__("Phone")}</th>
                            <th>{__("Email")}</th>
                            <th>{__("Class")}</th>
                            <th>{__("Actions")}</th>
                        </tr>
                    </thead>
                    <tbody>
                        {$idx = 1}
                        {foreach $rows as $row}
                            <tr>
                                <td class="align-middle" align="center">{$idx}</td>
                                <td class="align-middle" nowrap="true">
                                    <a href="{$system['system_url']}/{$row['user_name']}">
                                        {$row['user_fullname']}
                                    </a>
                                    &nbsp;
                                    {if $row['user_id'] != $user->_data['user_id']}
                                        <a href="#" class="far fa-comments fa-lg js_chat-start" data-name="{$row['user_fullname']}" data-uid="{$row['user_id']}"></a>
                                    {/if}
                                </td>
                                <td class="align-middle">{$row['user_phone']}</td>
                                <td class="align-middle"><a href="mailto:{$row['user_email']}">{$row['user_email']}</a></td>
                                <td class="align-middle">
                                    {if count($row['classes']) == 0}
                                        {__("No class")}
                                    {else}
                                        {foreach $row['classes'] as $k => $class}
                                            {$class['group_title']} {if $k < (count($row['classes']) - 1)} - {/if}
                                        {/foreach}
                                    {/if}
                                </td>
                                <td class="align-middle" align="center">
                                    {*<a href="{$system['system_url']}/{$row['user_name']}" class="btn btn-xs btn-default">*}
                                        {*{__("Timeline")}*}
                                    {*</a>*}
                                    {if $canEdit}
                                        <a href="{$system['system_url']}/school/{$username}/teachers/assign/{$row['user_id']}" class="btn btn-xs btn-info">
                                            {__("Assign")}
                                        </a>
                                        {if $user->_data['user_id'] != $row['user_id'] && $row['user_id'] != $school['page_admin']}
                                            <button class="btn btn-xs btn-danger js_school-unassign" data-handle="teacher" data-username="{$username}" data-id="{$row['user_id']}">
                                                {__("Delete")}
                                            </button>
                                        {/if}
                                        {*{if $row['created_id'] == $user->_data['user_id']}*}
                                            {*<a href="{$system['system_url']}/school/{$username}/teachers/edit/{$row['user_id']}" class="btn btn-xs btn-default">*}
                                                {*{__("Edit")}*}
                                            {*</a>*}
                                            {*{if count($row['classes']) == 0}*}
                                                {*<button class="btn btn-xs btn-danger js_school-delete" data-handle="teacher" data-username="{$username}" data-id="{$row['user_id']}">{__("Delete")}</button>*}
                                            {*{/if}*}
                                        {*{/if}*}
                                    {/if}
                                </td>
                            </tr>
                            {$idx = $idx + 1}
                        {/foreach}

                        {if $rows|count == 0}
                            <tr class="odd">
                                <td valign="top" align="center" colspan="6" class="dataTables_empty">
                                    {__("No data available in table")}
                                </td>
                            </tr>
                        {/if}
                    </tbody>
                </table>
            </div>
        </div>
    {elseif $sub_view == "edit"}
        <div class="panel-body">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_teacher.php">
                <input type="hidden" name="school_username" value="{$username}"/>
                <input type="hidden" name="do" value="edit"/>
                <input type="hidden" name="user_id" value="{$data['user_id']}"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Full name")} (*)</label>
                    <div class="col-sm-9">
                        <input class="form-control" name="full_name" value="{$data['user_fullname']}" required autofocus maxlength="255">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Telephone")} (*)</label>
                    <div class="col-sm-9">
                        <input name="user_phone" id="user_phone" type="text" value="{$data['user_phone']}" class="form-control" required maxlength="50">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Email")} (*)</label>
                    <div class="col-sm-9">
                        <input name="email" id="email" type="email" value="{$data['user_email']}" class="form-control" required maxlength="100">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Password")}</label>
                    <div class="col-sm-9">
                        <input name="password" id="password" type="password" class="form-control" placeholder="{__("Blank password means no change")}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Gender")} (*)</label>
                    <div class="col-sm-3">
                        <select name="gender" id="gender" class="form-control">
                            <option value="{$smarty.const.FEMALE}" {if $data['user_gender'] == $smarty.const.FEMALE}selected{/if}>{__("Female")}</option>
                            <option value="{$smarty.const.MALE}" {if $data['user_gender'] == $smarty.const.MALE}selected{/if}>{__("Male")}</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-3" align="right">
                        <label class="control-label text-right">{__("Role")}</label>
                        <div>{__("If this user is only teacher, you do not need select any role")}.</div>
                    </div>
                    <div class="col-sm-9">
                        {foreach $roles as $role}
                            <input type="checkbox" name="role_ids[]" value="{$role['role_id']}" {if $role['user_id'] > 0}checked{/if}/>{$role['role_name']}<br/>
                        {/foreach}
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                        <a href="{$system['system_url']}/school/{$username}/teachers" class="btn btn-default">{__("Lists")}</a>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    {elseif $sub_view == "assign"}
        <div class="panel-body">
            <div align="center"><strong>{__("Phân công lớp hoặc vai trò trong trường cho giáo viên")}</strong></div>
            <br/>
            <form class="js_ajax-forms form-horizontal" id="js_assign_teacher" data-url="ci/bo/school/bo_teacher.php">
                <input type="hidden" name="school_username" value="{$username}"/>
                <input type="hidden" name="do" value="assign"/>
                <input type="hidden" name="user_id" value="{$teacherId}"/>
                <div class="form-group">
                    <div class="col-sm-3" align="right">
                        <label class="control-label text-right">{__("Class")}</label>
                        <div>{__("Select the class that teachers assigned")}.</div>
                    </div>
                    <div class="col-sm-9">
                        {foreach $classes as $class}
                            <input type="checkbox" name="class_ids[]" value="{$class['group_id']}" {if $class['normal_teacher'] == 1}checked{/if}/> {$class['group_title']}&nbsp;
                            <div id="homeroom_{$class['group_id']}" style="{if $class['normal_teacher'] == 0}display:none;{else}display:inline;{/if}"><input type="checkbox" name="homeroom_{$class['group_id']}" {if $class['homeroom_teacher'] == 1}checked{/if}/> {__('Homeroom teacher')}</div><br/>
                        {/foreach}
                    </div>
                </div>
                <hr/>
                {if $school['school_step'] == SCHOOL_STEP_FINISH  || $school['grade'] != 0}
                    <div class="form-group">
                        <div class="col-sm-3" align="right">
                            <label class="control-label text-right">{__("Role")}</label>
                            <div>{__("If this user is only teacher, you do not need select any role")}.</div>
                        </div>
                        <div class="col-sm-9">
                            {foreach $roles as $role}
                                <input type="checkbox" name="role_ids[]" value="{$role['role_id']}" {if $role['user_id'] > 0}checked{/if}/> {$role['role_name']}<br/>
                            {/foreach}
                        </div>
                    </div>
                {/if}
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                        <a href="{$system['system_url']}/school/{$username}/teachers" class="btn btn-default">{__("Lists")}</a>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    {elseif $sub_view == "add"}
        <div class="panel-body">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_teacher.php">
                <input type="hidden" name="school_username" value="{$username}"/>
                <input type="hidden" name="do" value="add"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Full name")} (*)</label>
                    <div class="col-sm-5">
                        <input name="last_name" id="last_name" type="text" class="form-control" placeholder="{__("Last name")}" required autofocus maxlength="255">
                    </div>
                    <div class="col-sm-4">
                        <input name="first_name" id="first_name" type="text" class="form-control" placeholder="{__("First name")}" required maxlength="255">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Telephone")} (*)</label>
                    <div class="col-sm-9">
                        <input name="phone" id="phone" type="text" class="form-control" required maxlength="50">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Email")} (*)</label>
                    <div class="col-sm-9">
                        <input name="email" id="email" type="email" class="form-control" required maxlength="100">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Password")} (*)</label>
                    <div class="col-sm-9">
                        <input name="password" id="password" type="password" class="form-control" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Gender")} (*)</label>
                    <div class="col-sm-3">
                        <select name="gender" id="gender" class="form-control" required>
                            <option value="{$smarty.const.FEMALE}">{__("Female")}</option>
                            <option value="{$smarty.const.MALE}">{__("Male")}</option>
                        </select>
                    </div>
                </div>
                {if $school['school_step'] == SCHOOL_STEP_FINISH  || $school['grade'] != 0}
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">
                            {__("Add class")}?
                        </label>
                        <div class="col-sm-9">
                            <div class="onoffswitch">
                                <input type="checkbox" name="class_option" class="onoffswitch-checkbox" id="class_option">
                                <label class="onoffswitch-label" for="class_option"></label>
                            </div>
                        </div>
                    </div>
                {/if}
                <div id="select_class">
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Class")}</label>
                        <div class="col-sm-3">
                            <select name="class_id" class="form-control">
                                <option value="0">{__("Select class")}...</option>
                                {foreach $classes as $class}
                                    <option value="{$class['group_id']}">{$class['group_title']}</option>
                                {/foreach}
                            </select>
                        </div>
                    </div>
                </div>
                <div id="add_class" class="hidden">
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Class name")} (*)</label>
                        <div class="col-sm-9">
                            <input name="group_title" id="class_name" type="text" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Class level name")} (*)</label>
                        <div class="col-sm-3">
                            <select name="class_level_id" class="form-control">
                                <option value="0">{__("Select class level")}...</option>
                                {foreach $classLevels as $classLevel}
                                    <option value="{$classLevel['class_level_id']}">{$classLevel['class_level_name']}</option>
                                {/foreach}
                            </select>
                        </div>
                    </div>
                </div>
                {if $school['school_step'] == SCHOOL_STEP_FINISH  || $school['grade'] != 0}
                    <div class="form-group">
                        <div class="col-sm-3" align="right">
                            <label class="control-label text-right">{__("Role")}</label>
                            <div>{__("If this user is only teacher, you do not need select any role")}.</div>
                        </div>
                        <div class="col-sm-9">
                            {foreach $roles as $role}
                                <input type="checkbox" name="role_ids[]" value="{$role['role_id']}"/>{$role['role_name']}<br/>
                            {/foreach}
                        </div>
                    </div>
                {/if}
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    {elseif $sub_view == "addexisting"}
        <div class="panel-body">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_teacher.php">
                <input type="hidden" name="school_username" value="{$username}"/>
                <input type="hidden" name="do" value="addexisting"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Teacher")}/{__("Employee")}</label>
                    <div class="col-sm-9">
                        <input name="search-username" id="search-username" type="text" class="form-control" placeholder="{__("Enter username, email or fullname to search")}" autofocus autocomplete="off">
                        <div id="search-username-results" class="dropdown-menu dropdown-widget dropdown-search">
                            <div class="dropdown-widget-header">
                                {__("Search Results")}
                            </div>
                            <div class="dropdown-widget-body">
                                <div class="loader loader_small ptb10"></div>
                            </div>
                        </div>
                        <div>{__("Enter at least 4 characters")}.</div>
                        <br/>
                        <div class="col-sm-9" id="teacher_list" name="teacher_list"></div>
                    </div>
                </div>
                {if $school['school_step'] == SCHOOL_STEP_FINISH  || $school['grade'] != 0}
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">
                            {__("Add class")}?
                        </label>
                        <div class="col-sm-9">
                            <div class="onoffswitch">
                                <input type="checkbox" name="class_option" class="onoffswitch-checkbox" id="class_option">
                                <label class="onoffswitch-label" for="class_option"></label>
                            </div>
                        </div>
                    </div>
                {/if}
                <div id="select_class">
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Class")}</label>
                        <div class="col-sm-3">
                            <select name="class_id" class="form-control">
                                <option value="0">{__("Select class")}...</option>
                                {foreach $classes as $class}
                                    <option value="{$class['group_id']}">{$class['group_title']}</option>
                                {/foreach}
                            </select>
                        </div>
                    </div>
                </div>
                <div id="add_class" class="hidden">
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Class name")} (*)</label>
                        <div class="col-sm-9">
                            <input name="group_title" id="class_name" type="text" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Class level name")} (*)</label>
                        <div class="col-sm-3">
                            <select name="class_level_id" class="form-control">
                                <option value="0">{__("Select class level")}...</option>
                                {foreach $classLevels as $classLevel}
                                    <option value="{$classLevel['class_level_id']}">{$classLevel['class_level_name']}</option>
                                {/foreach}
                            </select>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30" disabled>{__("Save")}</button>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    {/if}
</div>
