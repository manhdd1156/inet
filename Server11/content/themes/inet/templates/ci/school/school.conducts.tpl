<div class="" style="position: relative">
    <div class="panel panel-default">
        <div class="panel-heading with-icon">
            {if $sub_view == "edit"}
                <div class="pull-right flip">
                    <a href="{$system['system_url']}/school/{$username}/conducts" class="btn btn-default">
                        <i class="fa fa-list"></i> {__("Lists")}
                    </a>
                </div>
            {/if}
            <i class="fa fa-gavel fa-fw fa-lg pr10"></i>
            {__("Conducts")}
            {if $sub_view == "edit"}
                &rsaquo; {__('Edit')}
            {/if}
        </div>
        {if $sub_view == ""}
            <div class="panel-body with-table">
                <div class="js_ajax-forms form-horizontal">
                    <input type="hidden" name="school_username" id="school_username" value="{$username}"/>
                    <input type="hidden" name="do" value="search_conduct"/>
                    <div class="form-group">
                        <label class="col-sm-2 control-label text-left">{__("Class")}</label>
                        <div class="col-sm-4" name="conduct_class">
                            <select name="class_id" id="co_class_id" data-username="{$username}" data-view="show" class="form-control" autofocus>
                                <option value="">{__("Select class")}</option>
{*                                {$class_level = -1}*}
                                {foreach $classes as $class}
{*                                    {if ($class_level != $class['class_level_id'])}*}
{*                                        <option value="" disabled style="color: blue">-----{$class['class_level_name']}-----</option>*}
{*                                    {/if}*}
                                    <option value="{$class['group_id']}">{$class['group_title']}</option>
{*                                    {$class_level = $class['class_level_id']}*}
                                {/foreach}
                            </select>
                        </div>
                        <div class="col-sm-4 ">
                            <select name="school_year" id="school_year" class="form-control">
                                {foreach $default_year as $key => $year}
                                    {*                                <option value="{$year}" {if $key == count($default_year) - 1}selected{/if}>{$year}</option>*}
                                    <option value="{$year}" {if (substr($year, 0, strlen(date("Y"))) === date("Y"))}selected{/if}>{$year}</option>
                                {/foreach}
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2"></label>
                        <div class="col-sm-2">
                            <button type="submit" class="btn btn-default js_conduct-search">{__("Search")}</button>
                        </div>
                    </div>

                    <div class="table-responsive" id="conduct_list" name="conduct_list">
                        {include file="ci/school/ajax.conductlist.tpl"}
                    </div>
                    <!-- success -->
                    <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                    <!-- error -->
                    <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                </div>
            </div>
        {elseif $sub_view == "edit"}
            <div class="panel-body">
                <form class="form-horizontal" enctype="multipart/form-data" method="post" id="school_edit_conduct">
                    <input type="hidden" name="school_username" id="school_username" value="{$username}"/>
                    <input type="hidden" name="conduct_id" value="{$data['conduct_id']}"/>
                    <input type="hidden" name="child_id" value="{$child['child_id']}"/>
                    <input type="hidden" name="do" value="edit"/>
                    <div class = "form-group">
                        <label class="col-sm-3 control-label text-left">
                            {__("Student")}
                        </label>
                        <div class = "col-sm-9">
                            <input type="text" class="form-control" value = "{$child['child_name']} - {$child['birthday']}" disabled>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Semester")} 1
                        </label>
                        <div class="col-sm-9">
                            <select name="conduct_id1" id="conduct_id1" data-username="{$username}" data-view="edit" class="form-control">
                                <option value="">{__("Select conduct")}</option>
                                {foreach $conducts as $conduct}
                                    <option value="{$conduct}" {if $data['hk1'] == $conduct}selected{/if}>{$conduct}</option>
                                {/foreach}
                                {*                                <option value="Excellent">{__("Excellent")}</option>*}
                                {*                                <option value="Good">{__("Good")}</option>*}
                                {*                                <option value="Average">{__("Average")}</option>*}
                                {*                                <option value="Weak">{__("Weak")}</option>*}
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Semester")} 2
                        </label>
                        <div class="col-sm-9">
                            <select name="conduct_id2" id="conduct_id2" data-username="{$username}" data-view="edit" class="form-control">
                                <option value="">{__("Select conduct")}</option>
                                {foreach $conducts as $conduct}
                                    <option value="{$conduct}" {if $data['hk2'] == $conduct}selected{/if}>{$conduct}</option>
                                {/foreach}
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("End semester")}
                        </label>
                        <div class="col-sm-9">
                            <select name="conduct_id3" id="conduct_id3" data-username="{$username}" data-view="edit" class="form-control">
                                <option value="">{__("Select conduct")}</option>
                                {foreach $conducts as $conduct}
                                    <option value="{$conduct}" {if $data['ck'] == $conduct}selected{/if}>{$conduct}</option>
                                {/foreach}
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-3">
                            <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                        </div>
                    </div>

                    <!-- success -->
                    <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                    <!-- success -->

                    <!-- error -->
                    <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                    <!-- error -->
                </form>
            </div>
        {/if}
    </div>
</div>