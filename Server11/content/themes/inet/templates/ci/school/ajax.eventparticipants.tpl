<div class="pb5">
    <strong>
        {__("Participants")} ({__("Registered")}:
        {if $event['for_child']}{$childCount} {__("children")}{/if}
        {if $event['for_child'] && $event['for_parent']}&nbsp;|&nbsp;{/if}
        {if $event['for_parent']}{$parentCount} {__("parent")}{/if})
    </strong>
</div>
<table class="table table-striped table-bordered">
    <thead>
    <tr>
        <th>#</th>
        <th>{__("Select")}</th>
        <th>{__("Child")}</th>
        <th>{__("Parent phone")}</th>
        <th>{__("Parent")}</th>
    </tr>
    </thead>
    <tbody>
    {$idx = 1}
    {foreach $participants as $child}
        <tr {if $child['child_status']==0} class="row-disable" {/if}>
            <td align="center">{$idx}</td>
            <td align="center">
                {if $event['for_child']}
                    <input type="checkbox" class="child" name="childIds[]" value="{$child['child_id']}"
                           {if $child['event_id'] > 0}checked {if $child['created_user_id']!=$user->_data['user_id']}disabled{/if}{/if}
                            {if ($event['can_register'] == 0) || (!$canEdit)}disabled{/if}>
                    {if $event['can_register'] > 0}
                        {*Lưu tên trẻ để post ngược về server*}
                        <input type="hidden" name="child_name_{$child['child_id']}" value="{$child['child_name']}"/>
                    {/if}
                    {if $child['event_id'] > 0  && ($child['created_user_id'] == $user->_data['user_id'])}
                        <input type="hidden" name="oldChildIds[]" value="{$child['child_id']}"/>
                    {/if}
                {/if}
            </td>
            <td>{$child['child_name']} ({$child['birthday']})
                {if ($child['event_id'] > 0) && ($child['created_user_id'] != $user->_data['user_id'])}
                    <br/>
                    {if ($event['can_register'] > 0) && $canEdit}
                        <a href="#" class="btn btn-xs btn-warning js_school-event-remove-participant" data-type="{$smarty.const.PARTICIPANT_TYPE_CHILD}" data-uid="{$child['child_id']}" data-name="{$child['child_name']}">
                            {__("Cancel")}
                        </a>
                    {else}
                        {__("Registrar")}:
                    {/if} <b>{$child['created_fullname']}</b>
                {/if}
            </td>
            <td>{$child['parent_phone']}</td>
            <td>
                {if $event['for_parent']}
                    {foreach $child['parent'] as $parent}
                        <input type="checkbox" class="parent" name="parentIds[]" value="{$parent['user_id']}"
                            {if $parent['event_id'] > 0}checked {if $parent['created_user_id']!=$user->_data['user_id']}disabled{/if}{/if}
                            {if ($event['can_register'] == 0) || (!$canEdit)}disabled{/if}>
                        <span class="name js_user-popover" data-uid="{$parent['user_id']}">
                            <a href="{$system['system_url']}/{$parent['user_name']}">{$parent['user_fullname']}</a>
                        </span>
                        {if $event['can_register'] > 0}
                            {*Lưu vào trường thông tin để post ngược về server*}
                            <input type="hidden" name="child_id_of_parent_{$parent['user_id']}" value="{$child['child_id']}"/>
                            <input type="hidden" name="parent_name_{$parent['user_id']}" value="{$parent['user_fullname']}"/>
                        {/if}
                        {if $parent['user_id'] != $user->_data['user_id']}
                            <a href="#" class="far fa-comments fa-lg js_chat-start" data-name="{$parent['user_fullname']}" data-uid="{$parent['user_id']}"></a>
                        {/if}
                        {if ($parent['event_id'] > 0) && ($parent['created_user_id'] != $user->_data['user_id'])}
                            <br/>
                            {if ($event['can_register'] > 0) && $canEdit}
                                <a href="#" class="btn btn-xs btn-warning js_school-event-remove-participant" data-type="{$smarty.const.PARTICIPANT_TYPE_PARENT}" data-uid="{$parent['user_id']}" data-name="{$parent['user_fullname']}" data-child="{$child['child_id']}">
                                    {__("Cancel")}
                                </a>
                            {else}
                                {__("Registrar")}:
                            {/if} <b>{$parent['created_fullname']}</b>
                        {/if}
                        {if $parent['event_id'] > 0 && ($parent['created_user_id'] == $user->_data['user_id'])}
                            <input type="hidden" name="oldParentIds[]" value="{$parent['user_id']}"/>
                        {/if}
                        <br/>
                    {/foreach}
                {/if}
            </td>
        </tr>
        {$idx = $idx + 1}
    {/foreach}

    {if $idx == 1}
        <tr class="odd">
            <td valign="top" align="center" colspan="7" class="dataTables_empty">
                {__("No data available in table")}
            </td>
        </tr>
    {/if}

    </tbody>
</table>