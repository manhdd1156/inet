<div class="table-responsive">
    <table class="table table-striped table-bordered">
        <thead>
        <tr bgcolor="#ffebcd">
            <th class="pinned" id="width_no">#</th>
            <th nowrap="true" class="pinned">{__("Full name")}</th>

            {$dayIdx = 0}
            {$sunDayIdxs = array()}
            {foreach $dates as $date}
                {if $days[$dayIdx] == 'CN'}
                    {$sunDayIdxs[] = $dayIdx + 1}
                    <th align="center" bgcolor="#a9a9a9">
                {else}
                    <th align="center">
                {/if}
                    {$date}<br>
                    <label style="color: blue">{$days[$dayIdx]}</label>
                </th>
                {$dayIdx = $dayIdx + 1}
            {/foreach}
            {*<th style="width: 200px">{__("Assigned")}</th>*}
        </tr>
        </thead>
        <tbody>
        {$date_count = $dates|count}
        {$rowIdx = 1}
        {foreach $rows as $row}

            <input type="hidden" name="teacher[]" value="{$row['user_id']}" />
            <tr>
                <td align="center">{$rowIdx}</td>
                <td nowrap="true"><a>{$row['teacher_name']}</a></td>

                {$presentCnt = 0}
                {$totalCnt = 0}
                {$idx = 1}

                {foreach $row['cells'] as $cell}

                    <td align="center" style="padding: 0; {if in_array($idx, $sunDayIdxs)}background-color: #a9a9a9{/if} ">
                        <label class="container_cb">
                        <input type="checkbox" name="teacher_{$row['user_id']}[]" value="{$cell['date_row']}"
                               {if $cell['is_checked'] == 1}checked{/if}/>
                            <span class="checkmark"></span>
                        </label>
                    </td>
                    {$totalCnt = $totalCnt + 1}
                    {$idx = $idx + 1}
                {/foreach}
                {*<td align="center">*}
                {*<strong>*}
                {*<font color="blue">{$presentCnt}</font>/<font color="red">{$totalCnt}</font>*}
                {*</strong></td>*}

                {$rowIdx = $rowIdx + 1}
            </tr>
            {*Sau 15 trẻ thì thêm header cho dễ nhìn*}
            {if $rowIdx % 16 == 0}
                <tr bgcolor="#ffebcd">
                    <th class="pinned" id="width_no">#</th>
                    <th nowrap="true" class="pinned">{__("Full name")}</th>
                    {$dayIdx = 0}
                    {$sunDayIdxs = array()}
                    {foreach $dates as $date}
                        {if $days[$dayIdx] == 'CN'}
                            {$sunDayIdxs[] = $dayIdx + 1}
                            <th align="center" bgcolor="#a9a9a9">
                                {else}
                            <th align="center">
                        {/if}
                        {$date}<br>
                        <label style="color: blue">{$days[$dayIdx]}</label>
                        </th>
                        {$dayIdx = $dayIdx + 1}
                    {/foreach}
                    {*<th style="width: 200px">{__("Assigned")}</th>*}
                </tr>
            {/if}

        {/foreach}
        {* {if ($rowIdx >= 2)}
             <tr><td colspan="{$totalCnt+2}"></td></tr>
             <tr>
                 <td colspan="2" align="center"><strong>{__('Number of teachers')}</strong></td>
                 {foreach $last_rows as $cell}
                     <td align="center" bgcolor="#ffebcd">{if $cell['is_checked'] == 1}<strong><font color="blue">{$cell['pickup_count']}</font></strong>{/if}</td>
                 {/foreach}
             </tr>
         {else}
             <tr class="odd">
                 <td valign="top" align="center" colspan="{$date_count+3}" class="dataTables_empty">
                     {__("No data available in table")}
                 </td>
             </tr>
         {/if}*}

        </tbody>
    </table>
</div>


{*
<style>
    /* The container */
    .container_cb {
        display: block;
        position: relative;
        /*padding-left: 35px;*/
        /*margin-bottom: 12px;*/
        cursor: pointer;
        font-size: 22px;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }

    /* Hide the browser's default checkbox */
    .container_cb input {
        position: absolute;
        opacity: 0;
        cursor: pointer;
        height: 0;
        width: 0;
    }

    /* Create a custom checkbox */
    .checkmark {
        position: absolute;
        top: 0;
        left: 0;
        height: 34px;
        width: 35px;
        background-color: #ffffff;
    }

    /* On mouse-over, add a grey background color */
    .container_cb:hover input ~ .checkmark {
        background-color: #ccc;
    }

    /* When the checkbox is checked, add a blue background */
    .container_cb input:checked ~ .checkmark {
        background-color: #2196F3;
    }

    /* Create the checkmark/indicator (hidden when not checked) */
    .checkmark:after {
        content: "";
        position: absolute;
        display: none;
    }

    /* Show the checkmark when checked */
    .container_cb input:checked ~ .checkmark:after {
        display: block;
    }

    /* Style the checkmark/indicator */
    .container_cb .checkmark:after {
        left: 10px;
        top: 5px;
        width: 10px;
        height: 15px;
        border: solid white;
        border-width: 0 3px 3px 0;
        -webkit-transform: rotate(45deg);
        -ms-transform: rotate(45deg);
        transform: rotate(45deg);
    }
</style>*}
