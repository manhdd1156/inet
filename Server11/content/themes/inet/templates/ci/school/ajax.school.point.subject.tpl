{$idx = 1}
{foreach $results as $subject}
    <div class="form-group">
        <div class="col-sm-3">
{*            {foreach $default_subjects[$grade][$subject['gov_class_level']] as $id => $subject_name}*}
{*                {if $id == $subject['subject_id']}<strong>{$idx}. {$subject_name}</strong>{/if}*}
{*            {/foreach}*}
            <strong>{$idx}. {$subject['subject_name']}</strong>
            <input type="hidden" name="subject_ids[]" value="{$subject['subject_id']}">
        </div>
        <div class="col-sm-3">
            <select name="teacher_ids[]" class="form-control">
                <option value="0">{__("Select teacher")}</option>
                {foreach $teachers as $teacher}
                    <option value="{$teacher['user_id']}" {if $subject['teacher_id'] == $teacher['user_id']}selected{/if}>{$teacher['user_fullname']}</option>
                {/foreach}
            </select>
        </div>
    </div>

    {$idx = $idx + 1}
{/foreach}