<tr>
    <td class="text-center">
        +
        <input type="hidden" class="fee_id_{$child_id}" name="fee_id_{$child_id}[]" value="{$fee['fee_id']}"/>
    </td>
    <td>{$fee['fee_name']}</td>
    <td>
        <input type="number" class="text-center fee_quantity" id="fee_quantity_{$child_id}_{$fee['fee_id']}" child_id="{$child_id}" fee_id="{$fee['fee_id']}" name="fee_quantity_{$child_id}[]"
               {if $fee['type']==$smarty.const.FEE_TYPE_MONTHLY}value="1" readonly style="width: 40px; background-color: #EEEEEE"{else}min="0" value="0" step="1" style="width: 40px;"{/if}/>
    </td>
    <td>
        <input style="width: 100px;" type="text" class="text-right fee_unit_price money_tui" id="fee_unit_price_{$child_id}_{$fee['fee_id']}" child_id="{$child_id}" fee_id="{$fee['fee_id']}"
               name="fee_unit_price_{$child_id}[]" value="{$fee['unit_price']}"/>
    </td>
    <td>
        {if $fee['type'] == $smarty.const.FEE_TYPE_MONTHLY}
            <input type="hidden" id="fee_quantity_deduction_{$child_id}_{$fee['fee_id']}" name="fee_quantity_deduction_{$child_id}[]" value="1"/>
            <input style="width: 90px;" type="text" class="text-right fee_unit_price_deduction money_tui" id="fee_unit_price_deduction_{$child_id}_{$fee['fee_id']}" child_id="{$child_id}" fee_id="{$fee['fee_id']}"
                   name="fee_unit_price_deduction_{$child_id}[]" value="" step="1" placeholder="({$smarty.const.MONEY_UNIT})"/>
        {else}
            <input style="width: 40px;" type="number" class="text-center fee_quantity_deduction" id="fee_quantity_deduction_{$child_id}_{$fee['fee_id']}" child_id="{$child_id}" fee_id="{$fee['fee_id']}"
                   name="fee_quantity_deduction_{$child_id}[]" value="0" step="1"/>({__('Daily')})
            <input type="hidden" id="fee_unit_price_deduction_{$child_id}_{$fee['fee_id']}" name="fee_unit_price_deduction_{$child_id}[]" value="{$fee['unit_price']}"/>
        {/if}
    </td>
    <td><input style="width: 100px; background-color: #EEEEEE" type="text" class="text-right money_tui" id="fee_amount_{$child_id}_{$fee['fee_id']}" name="fee_amount_{$child_id}[]"
               value="{if $fee['type']==$smarty.const.FEE_TYPE_MONTHLY}{$fee['unit_price']}{else}0{/if}" readonly/></td>
    <td class="text-center"><button class="btn-danger js_del_tuition_item" child_id="{$child_id}">{__("Delete")}</button></td>
</tr>