<table class="table table-striped table-bordered table-hover js_dataTable">
    <thead>
    <tr><th colspan="7">{__("Conduct list")}&nbsp;(<span class="count_conduct">{$result|count}</span>) </th></tr>
    <tr>
        <th>{__("#")}</th>
        <th>{__("Full name")}</th>
        <th>{__("Gender")}</th>
        <th>{__("Birthdate")}</th>
        <th>{__("Semester")} 1</th>
        <th>{__("Semester")} 2</th>
        <th>{__("End semester")}</th>
        <th>{__("Actions")}</th>

    </tr>
    </thead>
    <tbody>
        {$idx = 1}
        {foreach $result as $row}
            <tr>
                <td class="align-middle" align="center">
                    <strong>{$idx}</strong>
                </td>
                <td class="align-middle" style="word-break: break-all">
                    <a href="{$system['system_url']}/school/{$username}/children/detail/{$row['child_id']}">{$row['child_name']}</a>
                </td>
                <td class="align-middle">
                    {if $row['gender'] == $smarty.const.MALE}{__("Male")}{else}{__("Female")}{/if}
                </td>
                <td align="center" class="align-middle">{$row['birthday']}</td>
                <td align="center" class="align-middle">{$row['conduct1']}</td>
                <td align="center" class="align-middle">{$row['conduct2']}</td>
                <td align="center" class="align-middle">{$row['conduct3']}</td>
                <td align = "center" class="align-middle">
                    <a href="{$system['system_url']}/school/{$username}/conducts/edit/{$row['conduct_id']}" class="btn btn-xs btn-default">{__("Edit")}</a>
                </td>
            </tr>
            {$idx = $idx + 1}
        {/foreach}
    </tbody>
</table>

<script>
    $(function() {
        // run DataTable
        $('.js_dataTable').DataTable({
            "aoColumnDefs": [ { 'aDataSort': false, 'aTargets': [ -1 ] } ],
            "language": {
                "decimal":        "",
                "emptyTable":     __["No data available in table"],
                "info":           __["Showing _START_ to _END_ of _TOTAL_ results"],
                "infoEmpty":      __["Showing 0 to 0 of 0 results"],
                "infoFiltered":   "(filtered from _MAX_ total entries)",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     __["Show"] + " " + "_MENU_" + " " + __["results"],
                "loadingRecords": __["Loading..."],
                "processing":     __["Processing..."],
                "search":         __["Search"],
                "zeroRecords":    __["No matching records found"],
                "paginate": {
                    "first":      __["First"],
                    "last":       __["Last"],
                    "next":       __["Next"],
                    "previous":   __["Previous"]
                },
                "aria": {
                    "sortAscending":  ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                }
            }
        });
    });
</script>
