<div class="" style="position: relative">
    {if $school['school_step'] == SCHOOL_STEP_FINISH  || $school['grade'] != 0}
        <div class="dashboard_fixed">
            <button class="btn btn-xs btn-default js_school-tuition-guide" data-username="{$username}" data-id="{$user->_data["user_id"]}">
                <i class="fa fa-chevron-down {if $cookie}hidden{/if}" aria-hidden="true" id="guide_hidden"></i>
                <i class="fa fa-chevron-up {if !$cookie}hidden{/if}" aria-hidden="true" id="guide_show"></i>
            </button>
        </div>
        <div class="panel panel-default {if $cookie == 0}hidden{/if}" id="guide_tuition">
            <div class="guide_school row" style="margin-left: 0px; margin-right: 0px">
                <div class="col-sm-4" id="left_height">
                    <div class="init_school text-left pad5 full_width">
                        <div class="full_width pl5">
                            <strong>{__("The execution's order guide")}:</strong>
                        </div>
                        <div style="border: 1px solid #c9c9c9" class="pad5">
                            <div class="full_width">
                                <a href="{$system['system_url']}/school/{$username}/fees/add" class="pad5 font12 full_width">1. {__("Create fees")}</a>
                            </div>
                            <div class="full_width">
                                <a href="{$system['system_url']}/school/{$username}/attendance" class="pad5 font12 full_width">2. {__("Attendance")}</a>
                            </div>
                            <div class="full_width">
                                <a href="{$system['system_url']}/school/{$username}/useservices/reg" class=" pad5 font12 full_width">3. {__("Register service")}</a>
                            </div>
                            <div class="full_width">
                                <a href="{$system['system_url']}/school/{$username}/pickup/list" class="pad5 font12 full_width">4. {__("Late pickup")}</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div style="width: 15px; float: left; vertical-align: middle; position: relative" align="center" class="height_auto mobile_hide">
                    <div class="" style="position: absolute; top: 45%">
                        <i class="fas fa-long-arrow-alt-right" aria-hidden="true"></i>
                    </div>
                </div>
                <div class="col-sm-7 height_auto" style="position: relative">
                    <div class="init_school text-left pad5 full_width" style="position: absolute; top: 33%">
                        <div class="pad5">
                            <a href="{$system['system_url']}/school/{$username}/tuitions/add" class="pad5 font12">5. {__("Add monthly tuition")}</a>
                            <i class="fas fa-long-arrow-alt-right mobile_hide" aria-hidden="true"></i>
                            <a href="{$system['system_url']}/school/{$username}/tuitions" class="pad5 font12">6. {__("Tuition list")}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    {/if}
    <div class="panel panel-default">
        <div class="panel-heading with-icon">
            {if $canEdit}
                {if $sub_view == ""}
                    <div class="pull-right flip">
                        <a href="{$system['system_url']}/school/{$username}/tuitions/add" class="btn btn-default">
                            <i class="fa fa-plus"></i> {__("Add monthly tuition")}
                        </a>
                        <a href="{$system['system_url']}/school/{$username}/tuitions/summarypaid" class="btn btn-default">
                            <i class="fas fa-money-bill-wave"></i> {__("Summary tuition paid")}
                        </a>
                    </div>
                {elseif $sub_view == "add"}
                    <div class="pull-right flip">
                        <a href="{$system['system_url']}/school/{$username}/tuitions/" class="btn btn-default">
                            <i class="fa fa-list"></i> {__("Monthly tuition list")}
                        </a>
                        <a href="{$system['system_url']}/school/{$username}/fees/add" class="btn btn-default">
                            <i class="fa fa-plus"></i> {__("Add New Fee")}
                        </a>
                    </div>
                {elseif $sub_view == "detail"}
                    <div class="pull-right flip">
                        <a href="{$system['system_url']}/school/{$username}/tuitions/" class="btn btn-default">
                            <i class="fa fa-list"></i> {__("Monthly tuition list")}
                        </a>
                        <a href="{$system['system_url']}/school/{$username}/tuitions/add" class="btn btn-default">
                            <i class="fa fa-plus"></i> {__("Add monthly tuition")}
                        </a>
                    </div>
                {/if}
            {/if}
            <div class="pull-right flip" style="margin-right: 5px">
                <a href="https://blog.coniu.vn/huong-dan-tao-hoc-phi-thang-cho-lop-va-tre/" class="btn btn-info btn_guide" target="_blank">
                    <i class="fa fa-info"></i> {__("Guide")}
                </a>
            </div>
            <i class="fas fa-money-bill-alt fa-lg fa-fw pr10"></i>
            {__("Tuition")}
            {if $sub_view == "edit"}
                &rsaquo; {__("Edit")}
            {elseif $sub_view == "history"}
                &rsaquo; {__("History")}
            {elseif $sub_view == "add"}
                &rsaquo; {__('Add monthly tuition')}
            {elseif $sub_view == "newchild"}
                &rsaquo; {__('Add tuition for new child')}
            {elseif $sub_view == "detail"}
                &rsaquo; {__('Detail')}
            {elseif $sub_view == "4leave"}
                &rsaquo; {__('Account for leave')}
            {elseif $sub_view == "summarypaid"}
                &rsaquo; {__('Summary tuition paid')}
            {/if}
        </div>
        {if $sub_view == ""}
            <div class="panel-body with-table">
                <div><strong>{__("Tuition list")}&nbsp;({$rows|count})</strong></div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th nowrap="true">{__("Class")}</th>
                            <th>{__("Paid")}<br/>({$smarty.const.MONEY_UNIT})</th>
                            <th>{__("Total")}<br/>({$smarty.const.MONEY_UNIT})</th>
                            <th>{__("Paid studentren")}</th>
                            <th>{__("Option")}</th>
                        </tr>
                        </thead>
                        <tbody>
                        {$month = "-1"}
                        {$idx = 1}
                        {foreach $rows as $row}
                            {if ($month != $row['month'])}
                                {if $idx > 1}
                                    {*Tổng kết tháng trước*}
                                    <tr>
                                        <td colspan="2" align="center" style="vertical-align:middle"><strong>{__("Month total")}</strong></td>
                                        <td class="text-right" style="vertical-align:middle"><strong><span class="tuition_paid_month_{str_replace('/', '_', $month)}">{moneyFormat($totalPaid)}</span></strong></td>
                                        <td class="text-right" style="vertical-align:middle"><strong><span class="tuition_total_month_{str_replace('/', '_', $month)}">{moneyFormat($totalTution)}</span></strong></td>
                                        <td align="center" style="vertical-align:middle"><strong><span class="tuition_count_month_{str_replace('/', '_', $month)}">{$totalPaidCount}</span></strong></td>
                                        <td></td>
                                    </tr>
                                {/if}

                                <tr><td colspan="6"><strong>{__("Month")}:&nbsp;{$row['month']}</strong></td></tr>
                                {$totalPaid = 0}
                                {$totalTution = 0}
                                {$totalPaidCount = 0}
                                {$idx = 1}
                            {/if}
                            <tr>
                                <td align="center" style="vertical-align:middle"><strong>{$idx}</strong></td>
                                <td nowrap="true" style="vertical-align:middle"><a href="{$system['system_url']}/school/{$username}/tuitions/detail/{$row['tuition_id']}">{$row['group_title']}</a></td>
                                <td class="text-right tuition_paid_list_{$row['tuition_id']}" style="vertical-align:middle">{moneyFormat($row['paid_amount'])}</td>
                                <td class="text-right tuition_total_list_{$row['tuition_id']}" style="vertical-align:middle">{moneyFormat($row['total_amount'])}</td>
                                <td align="center" class="tuition_count_list_{$row['tuition_id']}" style="vertical-align:middle">{$row['paid_count']}</td>
                                <td align="left" style="vertical-align:middle">
                                    <a class="btn btn-success btn-xs js_school-tuition-export" data-handle="export_all" data-username="{$username}" data-id="{$row['tuition_id']}">{__('Export all to Excel')}</a>
                                    <a class="btn btn-default btn-xs" href="{$system['system_url']}/school/{$username}/tuitions/detail/{$row['tuition_id']}">{__('Detail')}</a>
                                    <label class="btn btn-info btn-xs processing_label x-hidden">{__("Processing")}...</label>
                                    {if $canEdit}
                                        {if !$row['is_notified']}
                                            <button class="btn btn-xs btn-default js_school-tuition" data-handle="notify" data-username="{$username}" data-id="{$row['tuition_id']}">
                                                {__("Notify")}
                                            </button>
                                        {/if}
                                        <a href="{$system['system_url']}/school/{$username}/tuitions/newchild/{$row['tuition_id']}" class="btn btn-xs btn-default">{__("Add child")}</a>
                                    {/if}
                                    {*<a href="{$system['system_url']}/school/{$username}/tuitions/history/{$row['tuition_id']}" class="btn btn-xs btn-default">{__("Used last month")}</a>*}
                                    {if $canEdit}
                                        {if $row['paid_amount'] != $row['total_amount']}
                                            <a href="{$system['system_url']}/school/{$username}/tuitions/edit/{$row['tuition_id']}"
                                               class="btn btn-xs btn-default">{__("Edit")}</a>
                                        {/if}
                                        <button class="btn btn-xs btn-danger js_school-tuition" data-handle="remove" data-username="{$username}" data-month="{$row['month']}"
                                                    data-id="{$row['tuition_id']}">{__("Delete")}</button>
                                    {/if}
                                </td>
                            </tr>

                            {$idx = $idx + 1}
                            {$totalPaid = $totalPaid + $row['paid_amount']}
                            {$totalPaidCount = $totalPaidCount + $row['paid_count']}
                            {$totalTution = $totalTution  + $row['total_amount']}
                            {$month = $row['month']}
                        {/foreach}
                        {*Tổng kết cho tháng cuối cùng trong danh sách*}
                        <tr>
                            <td colspan="2" align="center" style="vertical-align:middle"><strong>{__("Month total")}</strong></td>
                            <td class="text-right" style="vertical-align:middle"><strong><span class="tuition_total_month_{str_replace('/', '_', $month)}">{moneyFormat($totalPaid)}</span></strong></td>
                            <td class="text-right" style="vertical-align:middle"><strong><span class="tuition_total_month_{str_replace('/', '_', $month)}">{moneyFormat($totalTution)}</span></strong></td>
                            <td align="center" style="vertical-align:middle"><strong><span class="tuition_count_month_{str_replace('/', '_', $month)}">{$totalPaidCount}</span></strong></td>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        {elseif $sub_view == "newchild"}
            {include file="ci/school/school.tuitions.newchild.tpl"}
        {elseif $sub_view == "history"}
            {include file="ci/school/school.tuitions.history.tpl"}
        {elseif $sub_view == "detail"}
            {include file="ci/school/school.tuitions.detail.tpl"}
        {elseif $sub_view == "edit"}
            {include file="ci/school/school.tuitions.edit.tpl"}
        {elseif $sub_view == "add"}
            <div class="panel-body">
                <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_tuition.php">
                    <input type="hidden" name="school_username" id="school_username" value="{$username}"/>
                    <input type="hidden" name="do" value="add_tuition"/>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Class")} (*)</label>
                        <div class="col-sm-9">
                            <div class="col-xs-5">
                                <select name="class_id" id="tuition_class_id" class="form-control" data-username="{$username}" required autofocus>
                                    <option value="">{__("Select class")}...</option>
                                    {$class_level = -1}
                                    {foreach $classes as $class}
                                        {if ($class_level != $class['class_level_id'])}
                                            <option value="" disabled style="color: blue">-----{$class['class_level_name']}-----</option>
                                        {/if}
                                        <option value="{$class['group_id']}">{$class['group_title']}</option>
                                        {$class_level = $class['class_level_id']}
                                    {/foreach}
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Month")} (*)</label>
                        <div class="col-sm-9">
                            <div class='col-sm-5'>
                                <div class='input-group date' id='month_picker'>
                                    <input type='text' name="month" id="month" class="form-control" placeholder="{__("Month")} (*)" required/>
                                    <span class="input-group-addon"><span class="fas fa-calendar-alt"></span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Number of school days")} (*)</label>
                        <div class="col-sm-9">
                            <div class="col-sm-2">
                                <input type='number' min="1" max="31" name="day_of_month" id="day_of_month" value="{$attCount}" class="form-control"/>
                            </div>
                            <span class="help-block">{__("The number of working days in the selected month (except Saturday and Sunday). You can edit after selecting month")}.</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Notify immediately")}?</label>
                        <div class="col-sm-9">
                            <div class="onoffswitch">
                                <input type="checkbox" name="notify_immediately" class="onoffswitch-checkbox" id="notify_immediately">
                                <label class="onoffswitch-label" for="notify_immediately"></label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Description")}</label>
                        <div class="col-sm-9">
                            <textarea class="form-control" name="description" rows="2"></textarea>
                        </div>
                    </div>
                    <div class="form-group" id="getFixed">
                        <div class="col-sm-9 col-sm-offset-3">
                            <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                            <div id="service_usage_open_dialog" class="x-hidden" title="{__("Service usage in previous month")|upper}"></div>
                            <a href="#" class="btn btn-default js_tuition_service_usage x-hidden">{__("Check service usage")}</a>
                            <label id="loading" class="btn btn-info x-hidden">{__("Loading")}...</label>
                        </div>
                    </div>
                    <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                    <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                    <div class="table-responsive" id="children_list"></div>
                    <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                    <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                </form>
            </div>
        {elseif $sub_view == "summarypaid"}
            <div class="panel-body with-table">
                <div class="row">
                    <div class='col-sm-12'>
                        <div class='col-sm-3'>
                            <div class="form-group">
                                <select name="status" id="status" class="form-control js_change_status_summary" data-username="{$username}">
                                    <option value="">{__("Select status")}...</option>
                                    <option value="2">{__("Paid")}</option>
                                    <option value="1">{__("Waiting confirmation")}</option>
                                    <option value="0">{__("Not paid yet")}</option>
                                </select>
                            </div>
                        </div>
                        <div class='col-sm-3'>
                            <div class="form-group">
                                {$class_level = -1}
                                <select name="class_id" id="class_id" class="form-control">
                                    <option value="">{__("Whole school")|upper}</option>
                                    {foreach $classes as $class}
                                        {if ($class_level != $class['class_level_id'])}
                                            <option value="" disabled>-----{$class['class_level_name']}-----</option>
                                        {/if}
                                        <option value="{$class['group_id']}">{$class['group_title']}</option>
                                        {$class_level = $class['class_level_id']}
                                    {/foreach}
                                </select>
                            </div>
                        </div>
                        <div class='col-sm-3 x-hidden' id="fromDateClass">
                            <div class='input-group date' id='fromdatepicker'>
                                <input type='text' name="fromDate" id="fromDate" value="{$fromDate}" class="form-control" placeholder="{__("From date")}"/>
                                <span class="input-group-addon">
                                    <span class="fas fa-calendar-alt"></span>
                                </span>
                            </div>
                        </div>
                        <div class='col-sm-3 x-hidden' id="toDateClass">
                            <div class="form-group">
                                <div class='input-group date' id='todatepicker'>
                                    <input type='text' name="toDate" id="toDate" value="{$toDate}" class="form-control" placeholder="{__("To date")}"/>
                                    <span class="input-group-addon">
                                    <span class="fas fa-calendar-alt"></span>
                                </span>
                                </div>
                            </div>
                        </div>
                        <div class='col-sm-3 x-hidden' id="cashier">
                            <div class="form-group">
                                <select name="cashier_id" id="cashier_id" class="form-control" data-username="{$username}">
                                    <option value="">{__("Everyone")}...</option>
                                    {$idx = 1}
                                    {foreach $managers as $manager}
                                        <option value="{$manager['user_id']}">{$idx}. {$manager['user_fullname']}</option>
                                        {$idx = $idx + 1}
                                    {/foreach}
                                </select>
                            </div>
                        </div>
                        <div class='col-sm-3'>
                            <div class="form-group">
                                <a href="#" id="search" class="btn btn-default js_school-tuition-paid-search" data-username="{$username}" data-id="{$school['page_id']}" disabled="true">{__("Search")}</a>
                                <label id="loading" class="btn btn-info x-hidden">{__("Loading")}...</label>
                            </div>
                        </div>
                        <div class='col-sm-3'>
                            <div class="form-group">
                                <a href="#" id="export_excel" class="btn btn-success js_school-tuition-summary-export-excel" data-username="{$username}" data-id="{$school['page_id']}" disabled="true">{__("Export to Excel")}</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="summary_paid_list"></div>
            </div>
        {/if}
    </div>
</div>

