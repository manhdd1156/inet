<div><strong>{__("Attendance list")}&nbsp;({$rows|count})</strong></div>
<table class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th>#</th>
        {if $class_id <= 0}<th>{__("Class")}</th>{/if}
        <th>{__("Attendance date")}</th>
        <th>{__("Number of student")}</th>
        <th>{__("Present")}</th>
        <th>{__("Absence")}</th>
        <th>{__("Time")}</th>
        <th>{__("Actions")}</th>
    </tr>
    </thead>
    <tbody>
    {$totalPresent = 0}
    {$totalAbsence = 0}
    {$idx = 1}
    {foreach $rows as $row}
        <tr>
            <td align="center">{$idx}</td>
            {if $class_id <= 0}<td>{$row['group_title']}</td>{/if}
            <td>{$row['attendance_date']}</td>
            <td align="center">{$row['present_count'] + $row['absence_count']}</td>
            <td align="center">{$row['present_count']}</td>
            <td align="center">{$row['absence_count']}</td>
            <td>{$row['recorded_at']}</td>
            <td align="center"><a href="{$system['system_url']}/school/{$username}/attendance/rollup/{$row['attendance_id']}">{__("Detail")}</a></td>
            {$totalPresent = $totalPresent + $row['present_count']}
            {$totalAbsence = $totalAbsence + $row['absence_count']}
            {$idx = $idx + 1}
        </tr>
    {/foreach}
    <tr>
        <td colspan="{if $class_id <= 0}3{else}2{/if}" align="right"><strong>{__('Total')}</strong></td>
        <td align="center"><strong>{$totalPresent + $totalAbsence}</strong></td>
        <td align="center"><strong>{$totalPresent}</strong></td>
        <td align="center"><strong>{$totalAbsence}</strong></td>
        <td colspan="2"></td>
    </tr>
    </tbody>
</table>