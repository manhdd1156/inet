{if $sheet['error'] == 1}
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr><th>{__("Detail results")}</th></tr>
        </thead>
        <tbody>
            <tr><td><div>{$sheet['message']}</div></td></tr>
        </tbody>
    </table>
{else}
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr><th>{__("Detail results")}&nbsp;({$sheet['child_list']|count})</th></tr>
        </thead>
        <tbody>
            {$idx = 1}
            {foreach $sheet['child_list'] as $child}
                <tr>
                    <td>
                        <div>
                            {$idx}&nbsp;-&nbsp;{$child['child_name']}&nbsp;|&nbsp;{$child['birthday']}&nbsp;|&nbsp;{$child['parent_email']}&nbsp;|&nbsp;
                            {if $child['error'] == 0}
                                {__("Create student successfull")}&nbsp;|&nbsp;{$child['create_account_result']}
                            {else}
                                {$child['message']}
                            {/if}
                        </div>
                    </td>
                </tr>
                {$idx = $idx + 1}
            {/foreach}
        </tbody>
    </table>
{/if}