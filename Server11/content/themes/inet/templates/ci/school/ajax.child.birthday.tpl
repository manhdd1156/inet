<table class="table table-striped table-bordered table-hover" width="100%">
    <thead>
    <tr>
        <th colspan="7">{__("Student list")}&nbsp;({$rows|count})</th>
    </tr>
    <tr>
        <th width="7%">{__('#')}</th>
        <th width="20%">{__('Children')}</th>
        <th width="15%">{__('Birthdate')}</th>
        <th width="20%">{__('Parent')}</th>
        <th width="10%">{__('Parent phone')}</th>
        <th width="14%">{__('Email')}</th>
        <th width="14%">{__('Class')}</th>
    </tr>
    </thead>
    <tbody>
    {foreach $rows as $k => $row}
        <tr>
            <td class="align-middle" align="center">{$k + 1 }</td>
            <td class="align-middle">{convertText4Web($row['child_name'])}</td>
            <td class="align-middle" align="center">{toSysDate($row['birthday'])}</td>
            <td class="align-middle">{convertText4Web($row['parent_name'])}</td>
            <td class="align-middle" align="center">{$row['parent_phone']}</td>
            <td class="align-middle"><a href="mailto:{$row['parent_email']}">{$row['parent_email']}</a></td>
            <td class="align-middle" nowrap="true">
                {if $row['group_title'] == null}
                    {__("No class")}
                {else}
                    {convertText4Web($row['group_title'])}
                {/if}
            </td>
        </tr>
    {/foreach}

    {if $rows|count == 0}
        <tr class="odd">
            <td valign="top" align="center" colspan="7" class="dataTables_empty">
                {__("There is no student having birthday in this duration")}
            </td>
        </tr>
    {/if}
    </tbody>
</table>