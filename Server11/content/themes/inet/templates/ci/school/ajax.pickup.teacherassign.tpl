<div id="tbody-content">
    <div class="form-group">
        <label class="col-sm-2 control-label">
            {__("Teacher")} (*)
        </label>

        <div class="table-responsive col-sm-9">
            <a href="#" class="js_display_teacher_assign" style="float: right">{__("Chỉnh sửa")}</a>
            <table class="table table-bordered" id="tbody-teacher">
                <tbody>
                <tr>
                    <th>
                        {foreach $classes as $class }
                            <div id="teacher_2_{$class['pickup_class_id']}" class="teacher_assign {if $day['mon'] != $day['begin'] || $classes[0]['pickup_class_id'] != $class['pickup_class_id']}x-hidden{/if}">
                                {foreach $teachers as $teacher }
                                    <label class="col-xs-12 col-sm-6 text-left">
                                        <input type="checkbox" class="teachers" value="{$teacher['user_id']}" data-class="{$class['pickup_class_id']}" data-name="{$teacher['user_fullname']}"
                                                {if isset($list[2][{$class['pickup_class_id']}]) && in_array($teacher['user_id'], $list[2][{$class['pickup_class_id']}])} checked{/if}> {$teacher['user_fullname']}
                                    </label>
                                {/foreach}
                            </div>

                            <div id="teacher_3_{$class['pickup_class_id']}" class="teacher_assign {if $day['tue'] != $day['begin'] || $classes[0]['pickup_class_id'] != $class['pickup_class_id']}x-hidden{/if}">
                                {foreach $teachers as $teacher }
                                    <label class="col-xs-12 col-sm-6 text-left">
                                        <input type="checkbox" class="teachers" value="{$teacher['user_id']}" data-name="{$teacher['user_fullname']}"
                                                {if isset($list[3][{$class['pickup_class_id']}]) && in_array($teacher['user_id'], $list[3][{$class['pickup_class_id']}])} checked{/if}> {$teacher['user_fullname']}
                                    </label>
                                {/foreach}
                            </div>

                            <div id="teacher_4_{$class['pickup_class_id']}" class="teacher_assign {if $day['wed'] != $day['begin'] || $classes[0]['pickup_class_id'] != $class['pickup_class_id']}x-hidden{/if}">
                                {foreach $teachers as $teacher }
                                    <label class="col-xs-12 col-sm-6 text-left">
                                        <input type="checkbox" class="teachers" value="{$teacher['user_id']}" data-name="{$teacher['user_fullname']}"
                                                {if isset($list[4][{$class['pickup_class_id']}]) && in_array($teacher['user_id'], $list[4][{$class['pickup_class_id']}])} checked{/if}> {$teacher['user_fullname']}
                                    </label>
                                {/foreach}
                            </div>

                            <div id="teacher_5_{$class['pickup_class_id']}" class="teacher_assign {if $day['thu'] != $day['begin'] || $classes[0]['pickup_class_id'] != $class['pickup_class_id']}x-hidden{/if}">
                                {foreach $teachers as $teacher }
                                    <label class="col-xs-12 col-sm-6 text-left">
                                        <input type="checkbox" class="teachers" value="{$teacher['user_id']}" data-name="{$teacher['user_fullname']}"
                                                {if isset($list[5][{$class['pickup_class_id']}]) && in_array($teacher['user_id'], $list[5][{$class['pickup_class_id']}])} checked{/if}> {$teacher['user_fullname']}
                                    </label>
                                {/foreach}
                            </div>

                            <div id="teacher_6_{$class['pickup_class_id']}" class="teacher_assign {if $day['fri'] != $day['begin'] || $classes[0]['pickup_class_id'] != $class['pickup_class_id']}x-hidden{/if}">
                                {foreach $teachers as $teacher }
                                    <label class="col-xs-12 col-sm-6 text-left">
                                        <input type="checkbox" class="teachers" value="{$teacher['user_id']}" data-name="{$teacher['user_fullname']}"
                                                {if isset($list[6][{$class['pickup_class_id']}]) && in_array($teacher['user_id'], $list[6][{$class['pickup_class_id']}])} checked{/if}> {$teacher['user_fullname']}
                                    </label>
                                {/foreach}
                            </div>

                            <div id="teacher_7_{$class['pickup_class_id']}" class="teacher_assign {if $day['sat'] != $day['begin'] || $classes[0]['pickup_class_id'] != $class['pickup_class_id']}x-hidden{/if}">
                                {foreach $teachers as $teacher }
                                    <label class="col-xs-12 col-sm-6 text-left">
                                        <input type="checkbox" class="teachers" value="{$teacher['user_id']}" data-name="{$teacher['user_fullname']}"
                                                {if isset($list[7][{$class['pickup_class_id']}]) && in_array($teacher['user_id'], $list[7][{$class['pickup_class_id']}])} checked{/if}> {$teacher['user_fullname']}
                                    </label>
                                {/foreach}
                            </div>

                            <div id="teacher_8_{$class['pickup_class_id']}" class="teacher_assign {if $day['sun'] != $day['begin'] || $classes[0]['pickup_class_id'] != $class['pickup_class_id']}x-hidden{/if}">
                                {foreach $teachers as $teacher }
                                    <label class="col-xs-12 col-sm-6 text-left">
                                        <input type="checkbox" class="teachers" value="{$teacher['user_id']}" data-name="{$teacher['user_fullname']}"
                                                {if isset($list[8][{$class['pickup_class_id']}]) && in_array($teacher['user_id'], $list[8][{$class['pickup_class_id']}])} checked{/if}> {$teacher['user_fullname']}
                                    </label>
                                {/foreach}
                            </div>

                        {/foreach}

                    </th>
                </tr>
                </tbody>
            </table>

        </div>
    </div>

    {$classCnt = $classes|count}
    {if $classCnt > 1}
        <div class="form-group mb10">
            <label class="col-sm-12 text-center">
                {__("Assignment list")}
            </label>
        </div>
    {/if}


    <div class="form-group">
        {if $classCnt == 1}
            <label class="col-sm-2 control-label">
                {__("Assignment list")}
            </label>
        {/if}
        <div class="table-responsive {if $classCnt == 1}col-sm-9{else}col-sm-12{/if}">
            <table class="table table-striped table-bordered table-hover" id="tbody-assign">
                <thead>
                <tr>
                    <th class="col-sm-2"></th>
                    {foreach $classes as $class}
                        <th class="col-sm-3"><center>{$class['class_name']}</center></th>
                    {/foreach}
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td class="align-middle" align="center">
                        <strong>{__("Monday")}</strong> <br> {$day['mon']}
                    </td>

                    {for $i = 0; $i < $classCnt; $i++}
                        <td class="align-middle">
                            <div id="assign_class_2_{$classes[$i]['pickup_class_id']}" class="assign_class pl10">
                                {foreach $data[2] as $class_id => $arr }
                                    {if $class_id == $classes[$i]['pickup_class_id']}
                                        {foreach $arr as $info}
                                            <div id="assign_teacher_2_{$class_id}_{$info['user_id']}">
                                                <a class="text-left">&#9679;&#09;{$info['user_fullname']}</a>
                                                <input type="hidden" name="assign_day[]" value="2">
                                                <input type="hidden" name="assign_class[]" value="{$class_id}">
                                                <input type="hidden" name="assign_teacher[]" value="{$info['user_id']}">
                                            </div>
                                        {/foreach}
                                    {/if}
                                {/foreach}
                            </div>
                        </td>
                    {/for}

                </tr>
                <tr>
                    <td class="align-middle" align="center">
                        <strong>{__("Tuesday")}</strong> <br> {$day['tue']}
                    </td>

                    {for $i = 0; $i < $classCnt; $i++}
                        <td class="align-middle">
                            <div id="assign_class_3_{$classes[$i]['pickup_class_id']}" class="assign_class pl10">
                                {foreach $data[3] as $class_id => $arr }
                                    {if $class_id == $classes[$i]['pickup_class_id']}
                                        {foreach $arr as $info}
                                            <div id="assign_teacher_3_{$class_id}_{$info['user_id']}">
                                                <a class="text-left">&#9679;&#09;{$info['user_fullname']}</a>
                                                <input type="hidden" name="assign_day[]" value="3">
                                                <input type="hidden" name="assign_class[]" value="{$class_id}">
                                                <input type="hidden" name="assign_teacher[]" value="{$info['user_id']}">
                                            </div>
                                        {/foreach}
                                    {/if}
                                {/foreach}
                            </div>
                        </td>
                    {/for}

                </tr>
                <tr>
                    <td class="align-middle" align="center">
                        <strong>{__("Wednesday")}</strong> <br> {$day['wed']}
                    </td>
                    {for $i = 0; $i < $classCnt; $i++}
                        <td class="align-middle">
                            <div id="assign_class_4_{$classes[$i]['pickup_class_id']}" class="assign_class pl10">
                                {foreach $data[4] as $class_id => $arr }
                                    {if $class_id == $classes[$i]['pickup_class_id']}
                                        {foreach $arr as $info}
                                            <div id="assign_teacher_4_{$class_id}_{$info['user_id']}">
                                                <a class="text-left">&#9679;&#09;{$info['user_fullname']}</a>
                                                <input type="hidden" name="assign_day[]" value="4">
                                                <input type="hidden" name="assign_class[]" value="{$class_id}">
                                                <input type="hidden" name="assign_teacher[]" value="{$info['user_id']}">
                                            </div>
                                        {/foreach}
                                    {/if}
                                {/foreach}
                            </div>
                        </td>
                    {/for}
                </tr>
                <tr>
                    <td class="align-middle" align="center">
                        <strong>{__("Thursday")}</strong> <br> {$day['thu']}
                    </td>
                    {for $i = 0; $i < $classCnt; $i++}
                        <td class="align-middle">
                            <div id="assign_class_5_{$classes[$i]['pickup_class_id']}" class="assign_class pl10">
                                {foreach $data[5] as $class_id => $arr }
                                    {if $class_id == $classes[$i]['pickup_class_id']}
                                        {foreach $arr as $info}
                                            <div id="assign_teacher_5_{$class_id}_{$info['user_id']}">
                                                <a class="text-left">&#9679;&#09;{$info['user_fullname']}</a>
                                                <input type="hidden" name="assign_day[]" value="5">
                                                <input type="hidden" name="assign_class[]" value="{$class_id}">
                                                <input type="hidden" name="assign_teacher[]" value="{$info['user_id']}">
                                            </div>
                                        {/foreach}
                                    {/if}
                                {/foreach}
                            </div>
                        </td>
                    {/for}
                </tr>
                <tr>
                    <td class="align-middle" align="center">
                        <strong>{__("Friday")}</strong> <br> {$day['fri']}
                    </td>
                    {for $i = 0; $i < $classCnt; $i++}
                        <td class="align-middle">
                            <div id="assign_class_6_{$classes[$i]['pickup_class_id']}" class="assign_class pl10">
                                {foreach $data[6] as $class_id => $arr }
                                    {if $class_id == $classes[$i]['pickup_class_id']}
                                        {foreach $arr as $info}
                                            <div id="assign_teacher_6_{$class_id}_{$info['user_id']}">
                                                <a class="text-left">&#9679;&#09;{$info['user_fullname']}</a>
                                                <input type="hidden" name="assign_day[]" value="6">
                                                <input type="hidden" name="assign_class[]" value="{$class_id}">
                                                <input type="hidden" name="assign_teacher[]" value="{$info['user_id']}">
                                            </div>
                                        {/foreach}
                                    {/if}
                                {/foreach}
                            </div>
                        </td>
                    {/for}
                </tr>
                <tr>
                    <td class="align-middle" align="center">
                        <strong>{__("Saturday")}</strong> <br> {$day['sat']}
                    </td>
                    {for $i = 0; $i < $classCnt; $i++}
                        <td class="align-middle">
                            <div id="assign_class_7_{$classes[$i]['pickup_class_id']}" class="assign_class pl10">
                                {foreach $data[7] as $class_id => $arr }
                                    {if $class_id == $classes[$i]['pickup_class_id']}
                                        {foreach $arr as $info}
                                            <div id="assign_teacher_7_{$class_id}_{$info['user_id']}">
                                                <a class="text-left">&#9679;&#09;{$info['user_fullname']}</a>
                                                <input type="hidden" name="assign_day[]" value="7">
                                                <input type="hidden" name="assign_class[]" value="{$class_id}">
                                                <input type="hidden" name="assign_teacher[]" value="{$info['user_id']}">
                                            </div>
                                        {/foreach}
                                    {/if}
                                {/foreach}
                            </div>
                        </td>
                    {/for}
                </tr>
                <tr>
                    <td class="align-middle" align="center">
                        <strong>{__("Sunday")}</strong> <br> {$day['sun']}
                    </td>
                    {for $i = 0; $i < $classCnt; $i++}
                        <td class="align-middle">
                            <div id="assign_class_8_{$classes[$i]['pickup_class_id']}" class="assign_class pl10">
                                {foreach $data[8] as $class_id => $arr }
                                    {if $class_id == $classes[$i]['pickup_class_id']}
                                        {foreach $arr as $info}
                                            <div id="assign_teacher_8_{$class_id}_{$info['user_id']}">
                                                <a class="text-left">&#9679;&#09;{$info['user_fullname']}</a>
                                                <input type="hidden" name="assign_day[]" value="8">
                                                <input type="hidden" name="assign_class[]" value="{$class_id}">
                                                <input type="hidden" name="assign_teacher[]" value="{$info['user_id']}">
                                            </div>
                                            <br>
                                        {/foreach}
                                    {/if}
                                {/foreach}
                            </div>
                        </td>
                    {/for}
                </tr>
                </tbody>
            </table>

        </div>
    </div>
</div>