<div class="" style="position: relative">
    {if $school['school_step'] == SCHOOL_STEP_FINISH  || $school['grade'] != 0}
        <div class="dashboard_fixed">
            <button class="btn btn-xs btn-default js_school-contact-book-guide" data-username="{$username}"
                    data-id="{$user->_data["user_id"]}">
                <i class="fa fa-chevron-down {if $cookie}hidden{/if}" aria-hidden="true" id="guide_hidden"></i>
                <i class="fa fa-chevron-up {if !$cookie}hidden{/if}" aria-hidden="true" id="guide_show"></i>
            </button>
        </div>
        <div class="panel panel-default {if $cookie == 0}hidden{/if}" id="guide_contact">
            <div class="guide_school">
                <div class="init_school text-left pad5 full_width">
                    <div class="full_width pl5">
                        <strong>{__("The execution's order guide")}:</strong>
                    </div>
                    <div class="pad5">
                        <strong>{__("Handlings with contact book")}:</strong>
                        <a href="{$system['system_url']}/school/{$username}/reports/addcate"
                           class="pad5 font12">1. {__("Add new category")}</a>
                        <i class="fas fa-long-arrow-alt-right" aria-hidden="true"></i>
                        <a href="{$system['system_url']}/school/{$username}/reports/addtemp"
                           class="pad5 font12">2. {__("Add new template")}</a>
                        <i class="fas fa-long-arrow-alt-right" aria-hidden="true"></i>
                        <a href="{$system['system_url']}/school/{$username}/reports/add"
                           class="pad5 font12">3. {__("Create contact book")}</a>
                        <i class="fas fa-long-arrow-alt-right" aria-hidden="true"></i>
                        <a href="{$system['system_url']}/school/{$username}/reports"
                           class="pad5 font12">4. {__("List contact book")}</a>
                    </div>
                </div>
            </div>
        </div>
    {/if}
    <div class="panel panel-default">
        <div class="panel-heading with-icon">
            {if $sub_view == ""}
                {if $canEdit}
                    <div class="pull-right flip">
{*                        <a href="https://blog.coniu.vn/huong-dan-tao-so-lien-lac-tren-website/" target="_blank"*}
{*                        <a href="#" target="_blank"*}
{*                           class="btn btn-info btn_guide">*}
{*                            <i class="fa fa-info-circle"></i> {__("Guide")}*}
{*                        </a>*}
                        <a href="{$system['system_url']}/school/{$username}/reports/add" class="btn btn-default">
                            <i class="fa fa-plus"></i> {__("Add New")}
                        </a>
                    </div>
                {/if}
            {elseif $sub_view == "add" || $sub_view == "edit"}
                <div class="pull-right flip">
{*                    <a href="https://blog.coniu.vn/huong-dan-tao-so-lien-lac-tren-website/" target="_blank"*}
{*                    <a href="#" target="_blank"*}
{*                       class="btn btn-info btn_guide">*}
{*                        <i class="fa fa-info-circle"></i> {__("Guide")}*}
{*                    </a>*}
                    <a href="{$system['system_url']}/school/{$username}/reports" class="btn btn-default">
                        <i class="fa fa-list"></i> {__("Lists")}
                    </a>
                </div>
            {elseif $sub_view == "detail" || $sub_view == "add" || $sub_view == "edit"}
                <div class="pull-right flip">
                    <a href="{$system['system_url']}/school/{$username}/reports" class="btn btn-default">
                        <i class="fa fa-list-ul"></i> {__("Lists")}
                    </a>
                </div>
            {elseif $sub_view == "listtemp"}
                {if $canEdit}
                    <div class="pull-right flip">
{*                        <a href="https://blog.coniu.vn/huong-dan-them-moi-mau-so-lien-lac-tren-website/" target="_blank"*}
{*                        <a href="#" target="_blank"*}
{*                           class="btn btn-info btn_guide">*}
{*                            <i class="fa fa-info-circle"></i> {__("Guide")}*}
{*                        </a>*}
                        <a href="{$system['system_url']}/school/{$username}/reports/addtemp" class="btn btn-default">
                            <i class="fa fa-plus"></i> {__("Add new template")}
                        </a>
                    </div>
                {/if}
            {elseif $sub_view == "addtemp" || $sub_view == "edittemp"}
                <div class="pull-right flip">
{*                    <a href="https://blog.coniu.vn/huong-dan-them-moi-mau-so-lien-lac-tren-website/" target="_blank"*}
{*                    <a href="#" target="_blank"*}
{*                       class="btn btn-info btn_guide">*}
{*                        <i class="fa fa-info-circle"></i> {__("Guide")}*}
{*                    </a>*}
                    <a href="{$system['system_url']}/school/{$username}/reports/listtemp" class="btn btn-default">
                        <i class="fa fa-list-ul"></i> {__("List template")}
                    </a>
                </div>
            {elseif $sub_view == "listcate"}
                {if $canEdit}
                    <div class="pull-right flip">
{*                        <a href="https://blog.coniu.vn/huong-dan-them-moi-tieu-chi-so-lien-lac-tren-web/"*}
{*                        <a href="#"*}
{*                           target="_blank" class="btn btn-info btn_guide">*}
{*                            <i class="fa fa-info-circle"></i> {__("Guide")}*}
{*                        </a>*}
                        <a href="{$system['system_url']}/school/{$username}/reports/addcate" class="btn btn-default">
                            <i class="fa fa-plus"></i> {__("Add new category")}
                        </a>
                    </div>
                {/if}
            {elseif $sub_view == "addcate" || $sub_view == "editcate"}
                <div class="pull-right flip">
{*                    <a href="https://blog.coniu.vn/huong-dan-them-moi-tieu-chi-so-lien-lac-tren-web/" target="_blank"*}
{*                    <a href="#" target="_blank"*}
{*                       class="btn btn-info btn_guide">*}
{*                        <i class="fa fa-info-circle"></i> {__("Guide")}*}
{*                    </a>*}
                    <a href="{$system['system_url']}/school/{$username}/reports/listcate" class="btn btn-default">
                        <i class="fa fa-list-ul"></i> {__("List category")}
                    </a>
                </div>
            {/if}
            <i class="fa fa-life-ring fa-fw fa-lg pr10"></i>
            {__("Contact book")}
            {if $sub_view == "add"}
                &rsaquo; {__('Add New')}
            {elseif $sub_view == "listtemp"}
                &rsaquo; {__('List template')}
            {elseif $sub_view == "addtemp"}
                &rsaquo; {__('Add new template')}
            {elseif $sub_view == "detail"}
                &rsaquo; {__('Detail')}
            {elseif $sub_view == "detailtemp"}
                &rsaquo; {__('Detail template')}
            {elseif $sub_view == "edit"}
                &rsaquo; {__('Edit')}
            {elseif $sub_view == "edittemp"}
                &rsaquo; {__('Edit template')}
            {elseif $sub_view == "listcate"}
                &rsaquo; {__('List category')}
            {elseif $sub_view == "addcate"}
                &rsaquo; {__('Add new category')}
            {elseif $sub_view == "editcate"}
                &rsaquo; {__('Edit category')}
            {/if}
        </div>
        {if $sub_view == ""}
            <div class="panel-body with-table">
                <div class="js_ajax-forms form-horizontal">
                    <input type="hidden" name="school_username" id="school_username" value="{$username}"/>
                    <input type="hidden" name="do" value="search_report"/>
                    <div class="form-group">
                        <label class="col-sm-2 control-label text-left">{__("Scope")}</label>
                        <div class="col-sm-2">
                            <select name="level" id="report_level" class="form-control">
                                <option value="{$smarty.const.SCHOOL_LEVEL}">{__("School")}</option>
                                <option value="{$smarty.const.CLASS_LEVEL}">{__("Class")}</option>
                                <option value="{$smarty.const.PARENT_LEVEL}">{__("Child")}</option>
                            </select>
                        </div>
                        <div class="col-sm-3 x-hidden" name="report_class">
                            <select name="class_id" id="re_class_id" data-username="{$username}" data-view="show"
                                    class="form-control" autofocus>
                                <option value="">{__("Select class")}</option>
                                {$class_level = -1}
                                {foreach $classes as $class}
                                    {if ($class_level != $class['class_level_id'])}
                                        <option value="" disabled style="color: blue">-----{$class['class_level_name']}
                                            -----
                                        </option>
                                    {/if}
                                    <option value="{$class['group_id']}">{$class['group_title']}</option>
                                    {$class_level = $class['class_level_id']}
                                {/foreach}
                            </select>
                        </div>
                        <div class="col-sm-5 x-hidden" name="report_child">
                            <select name="child_id" id="re_child_id" class="form-control">
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-5 col-sm-offset-2" name="report_category">
                            <select name="category" id="category" class="form-control">
                                <option value="0"><strong>{__("Select category")}</strong></option>
                                {foreach $categories as $category}
                                    <option value="{$category['report_template_category_id']}">{$category['category_name']}</option>
                                {/foreach}
                            </select>
                        </div>
                        <div class="col-sm-2">
                            <button type="submit" class="btn btn-default js_report-search">{__("Search")}</button>
                        </div>
                    </div>

                    <div class="table-responsive" id="report_list" name="report_list">
                        {include file="ci/school/ajax.reportlist.tpl"}
                    </div>
                    <!-- success -->
                    <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                    <!-- error -->
                    <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                </div>
            </div>
        {elseif $sub_view == "edit"}
            <div class="panel-body">
                <form class="form-horizontal" enctype="multipart/form-data" method="post" id="create_report">
                    <input type="hidden" name="school_username" id="school_username" value="{$username}"/>
                    <input type="hidden" name="report_id" value="{$data['report_id']}"/>
                    <input type="hidden" name="do" value="edit"/>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Title")} (*)</label>
                        <div class="col-sm-9">
                            <input type="text" name="title" required class="form-control" autofocus
                                   value="{$data['report_name']}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Use template")}
                        </label>
                        <div class="col-sm-9">
                            <select name="report_template_id" id="report_template_id" data-username="{$username}"
                                    data-view="add" class="form-control">
                                <option value="">{__("Select template")}</option>
                                {foreach $templates as $temp}
                                    <option value="{$temp['report_template_id']}">{$temp['template_name']}</option>
                                {/foreach}
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Child")}</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control"
                                   value="{$child['child_name']} - {$child['birthday']}" disabled>
                        </div>
                    </div>
                    <div class="table-responsive" id="notemplate">
                        {$idx = 1}
                        {foreach $data['details'] as $row}
                            <table class="table table-striped table-bordered table-hover" id="addTempTable">
                                <tr>
                                    <td><strong>{$idx} - {$row['report_category_name']}</strong></td>
                                    <input type="hidden" name="report_category_ids[]"
                                           value="{$row['report_category_id']}">
                                    <input type="hidden" name="report_category_name_{$row['report_category_id']}"
                                           value="{$row['category_name']}">
                                </tr>
                                <tr>
                                    <td>
                                        <textarea placeholder="{__("Other comment")}" class="col-sm-12 mt10 mb10 note"
                                                  style="width: 100%"
                                                  name="report_content_{$row['report_category_id']}">{$row['report_category_content']}</textarea>
                                        {foreach $row['template_multi_content'] as $suggest}
                                            <div class="col-sm-4">
                                                <input type="checkbox" value="{$suggest}"
                                                       name="report_suggest_{$row['report_category_id']}[]"
                                                       {if in_array($suggest, $row['multi_content'])}checked{/if}> {$suggest}
                                            </div>
                                        {/foreach}
                                    </td>
                                </tr>
                            </table>
                            {$idx = $idx + 1}
                        {/foreach}
                    </div>
                    <div id="template_detail">

                    </div>
                    <div class="form-group">

                    </div>
                    <input type="hidden" id="is_file" name="is_file" value="1">
                    <div class="form-group" id="file_old">
                        <label class="rp-file col-sm-3 control-label text-left"
                               style="padding-top: 0px">{__("File attachment")}</label>
                        <div class="col-sm-6">
                            {if !is_empty($data['source_file'])}
                                <a href="{$data['source_file']}"
                                   download="{$data['file_name']}">{$data['file_name']}</a>
                            {else} {__('No file attachment')}
                            {/if}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">
                            {if is_empty($data['source_file'])}
                                {__("Choose file")}
                            {else}
                                {__("Choose file replace")}
                            {/if}
                        </label>
                        <div class="col-sm-6">
                            <input type="file" name="file" id="file"/>
                        </div>
                        <a class="delete_image btn btn-xs btn-danger text-left">{__('Delete')}</a>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Notify immediately")}?</label>
                        <div class="col-sm-9">
                            <div class="onoffswitch">
                                <input type="checkbox" name="notify_immediately" class="onoffswitch-checkbox"
                                       id="notify_immediately" {if $data['is_notified'] == 1}checked{/if}>
                                <label class="onoffswitch-label" for="notify_immediately"></label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-3">
                            <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                        </div>
                    </div>

                    <!-- success -->
                    <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                    <!-- success -->

                    <!-- error -->
                    <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                    <!-- error -->
                </form>
            </div>
        {elseif $sub_view == "add"}
            <div class="panel-body">
                {if count($templates) == 0}
                    <div class="color_red" align="center">
                        <strong>{__("You must create a template before creating a contact book")}</strong>
                    </div>
                {else}
                    <form class="form-horizontal" enctype="multipart/form-data" method="post" id="create_report">
                        <input type="hidden" name="school_username" id="school_username" value="{$username}"/>
                        <input type="hidden" name="do" value="add"/>
                        <div class="form-group">
                            <label class="col-sm-3 control-label text-left">{__("Title")} (*)</label>
                            <div class="col-sm-9">
                                <input type="text" name="title" required class="form-control" autofocus>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label text-left">{__("Notify immediately")}?</label>
                            <div class="col-sm-9">
                                <div class="onoffswitch">
                                    <input type="checkbox" name="notify_immediately" class="onoffswitch-checkbox"
                                           id="notify_immediately" unchecked>
                                    <label class="onoffswitch-label" for="notify_immediately"></label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label text-left">{__("Select Class")} (*)</label>
                            <div class="col-sm-3">
                                <select name="class_id" id="report_class_id" data-username="{$username}" data-view="add"
                                        class="form-control" autofocus>
                                    <option value="">{__("Select class")}</option>
                                    {$class_level = -1}
                                    {foreach $classes as $class}
                                        {if ($class_level != $class['class_level_id'])}
                                            <option value="" disabled style="color: blue">
                                                -----{$class['class_level_name']}-----
                                            </option>
                                        {/if}
                                        <option value="{$class['group_id']}">{$class['group_title']}</option>
                                        {$class_level = $class['class_level_id']}
                                    {/foreach}
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label text-left">
                                {__("Send comment to")} (*)
                            </label>
                            <div id="list_child" class="col-sm-9">

                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label text-left">{__("Use template")}</label>
                            <div class="col-sm-9">
                                <select name="report_template_id" id="report_template_id" data-username="{$username}"
                                        data-view="add" class="form-control">
                                    <option value="">{__("Select template")}</option>
                                    {foreach $templates as $temp}
                                        <option value="{$temp['report_template_id']}">{$temp['template_name']}</option>
                                    {/foreach}
                                </select>
                                {if count($templates) == 0}
                                    <br/>
                                    <strong>{__("Bạn chưa có mẫu nào, vui lòng kích vào")} <a
                                                href="{$system['system_url']}/school/{$username}/reports/addtemp"
                                                target="_blank">{__("HERE")}</a> {__("để tạo mẫu")}</strong>
                                {/if}
                            </div>
                        </div>

                        <div id="template_detail">
                            {include file="ci/ajax.reporttemplatedetail.tpl"}
                        </div>

                        <div class="form-group"></div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label text-left">{__("File attachment")}</label>
                            <div class="col-sm-6">
                                <input type="file" name="file" id="file"/>
                            </div>
                            <div class="col-sm-3">
                                <a class="delete_image btn btn-danger btn-xs text-left">{__('Delete')}</a>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-9 col-sm-offset-3">
                                <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                                {*<a href="{$system['system_url']}/school/{$username}/reports" class="btn btn-default">{__("Finish")}</a>*}
                            </div>
                        </div>

                        <!-- success -->
                        <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                        <!-- success -->

                        <!-- error -->
                        <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                        <!-- error -->
                    </form>
                {/if}
            </div>
        {elseif $sub_view == "detail"}
            <div class="panel-body with-table">
                <table class="table table-bordered">
                    <tbody>
                    <tr>
                        <td class="col-sm-3 text-right">{__('Title')}</td>
                        <td>
                            <strong>{$data['report_name']}</strong>
                        </td>
                    </tr>
                    <tr>
                        <td class="col-sm-3 text-right">{__('Child')}</td>
                        <td>
                            <strong>{$child['child_name']} - {$child['birthday']}</strong>
                        </td>
                    </tr>
                    <tr>
                        <td class="col-sm-3 text-right">{__('Class')}</td>
                        <td>
                            <strong>{$class_name}</strong>
                        </td>
                    </tr>
                    <tr>
                        <td class="col-sm-3 text-right">{__('Notification')}</td>
                        <td>
                            {if $data['is_notified']}
                                {__('Notified')}
                            {else}
                                {__('Not notified yet')}
                            {/if}
                        </td>
                    </tr>
                    <tr>
                        <td class="col-sm-3 text-right">{__("File attachment")}</td>
                        <td>
                            {if !is_empty($data['source_file'])}
                                <a href="{$data['source_file']}" target="_blank"><strong>
                                        {__("File attachment")}
                                    </strong>
                                </a>
                            {else}
                                {__("No file attachment")}
                            {/if}
                        </td>
                    </tr>
                    </tbody>
                </table>

                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th> #</th>
                            <th> {__('Title')} </th>
                            <th>{__('Content')} </th>
                        </tr>
                        </thead>
                        <tbody>
                        {foreach $data['details'] as $k => $row}
                            <tr>
                                <td align="center">
                                    <strong>{$k + 1}</strong>
                                </td>
                                <td>
                                    <strong>{$row['report_category_name']}</strong>
                                </td>
                                <td>
                                    {if $row['report_category_content']}
                                        <div class="mb10">
                                            {$row['report_category_content']}
                                        </div>
                                    {/if}
                                    {foreach $row['multi_content'] as $suggest}
                                        <div class="form-group">
                                            <i class="fa fa-check" aria-hidden="true"></i> {$suggest}
                                        </div>
                                    {/foreach}
                                </td>
                            </tr>
                        {/foreach}
                        </tbody>
                    </table>
                </div>
                {if $point_module ==1}
                <div style="{if $score_fomula!='vn'}width: 75%{/if};margin-left: auto;margin-right: auto;overflow-x:auto;">
                    <strong style="float: right">{__("Status ")} :
                        {if $status == 'Pass' }
                            <strong style="color:lawngreen">{__({$status})}</strong>
                        {elseif $status == 'Fail'}
                            <strong style="color:red">{__({$status})}</strong>
                        {elseif $status == 'Re-exam'}
                            <strong style="color:orange">{__({$status})}</strong>
                        {else}
                            <strong>{__({$status})}</strong>
                        {/if}

                    </strong>
                    <table class="table table-striped table-bordered" style="z-index: 1;">
                        <thead>
                        <tr bgcolor="#fff">
                            <th rowspan="2" class="pinned" style="padding: 8px 6px" id="width_no">#</th>
                            <th rowspan="2" nowrap="true" class="pinned">{__("Subject name")}</th>
                            <th colspan="{if $score_fomula=='vn'}{$column_hk2+$column_gk2+2}{else}4{/if}">{__("Semester 1")}</th>
                            <th colspan="{if $score_fomula=='vn'}{$column_hk2+$column_gk2+2}{else}4{/if}">{__("Semester 2")}</th>
                        </tr>
                        <tr>
                            {for $i=1;$i<=$column_hk1;$i++}
                                <th>{if $score_fomula=='vn'}tx{else}M{$i}{/if}</th>
                            {/for}
                            {for $i=0;$i<$column_gk1;$i++}
                                <th>{if $score_fomula=='vn'}gk{else}D1{/if}</th>
                            {/for}
                            {if $score_fomula=='vn'}
                                <th>ck</th>
                                <th>{__("Average")}</th>
                            {/if}
                            {for $i=1;$i<=$column_hk2;$i++}
                                <th>{if $score_fomula=='vn'}tx{else}M{$i}{/if}</th>
                            {/for}
                            {for $i=0;$i<$column_gk2;$i++}
                                <th>{if $score_fomula=='vn'}gk{else}D2{/if}</th>
                            {/for}
                            {if $score_fomula=='vn'}
                                <th>ck</th>
                                <th>{__("Average")}</th>
                                <th>Reexam</th>
                            {/if}
                        </tr>
                        </thead>
                        <tbody>
                        {$rowIdx = 1}
                        {foreach $rows as $k => $row}
                            <tr>
                                <td align="center" class="pinned">{$rowIdx}</td>

                                <td nowrap="true" class="pinned text-bold color-blue">
                                    <strong>{$row['subject_name']}</strong>
                                </td>
                                {$rowIdx = $rowIdx + 1}
                                {foreach $subject_key as $key}
                                    <td>{$row[strtolower($key)]}</td>
                                {/foreach}
                            </tr>
                        {/foreach}
                        {*                    Các điểm trung bình*}
                        {if $score_fomula == 'vn'}
                        <tr>
                            <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                                <strong>{__("End Semester")}</strong>
                            </td>
                            <td colspan="{$column_hk1+$column_gk1+1}"></td>
                            <td>{number_format($tb_total_hk1 ,2)}</td>
                            <td colspan="{$column_hk2+$column_gk2+1}"></td>
                            <td>{number_format($tb_total_hk2 ,2)}</td>
                            <td colspan="1"></td>
                        </tr>
                        {*                    kết thúc năm *}
                        <tr>
                            <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                                <strong>{__("End year")}</strong>
                            </td>
                            <td colspan="{$column_hk1 + $column_gk1 + 2 + $column_hk2 + $column_gk2 + 2 + 1}"
                                style="text-align: center">{number_format($tb_total_year ,2)}</td>
                        </tr>
                        {*                    Nghỉ có phép*}
                        <tr>
                            <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                                <strong>{__("Absent with permission")}</strong>
                            </td>
                            <td colspan="{$column_hk1 + $column_gk1 + 2 + $column_hk2 + $column_gk2 + 2 + 1}"
                                style="text-align: center">{$child_absent['absent_true']}</td>
                        </tr>
                        {*                    nghỉ không phép*}
                        <tr>
                            <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                                <strong>{__("Absent without permission")}</strong>
                            </td>
                            <td colspan="{$column_hk1 + $column_gk1 + 2 + $column_hk2 + $column_gk2 + 2 + 1}"
                                style="text-align: center">{$child_absent['absent_false']}</td>
                        </tr>
                        </tbody>
                    </table>

                    {elseif $score_fomula=='km'}
                    <tr>
                        <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                            <strong>{__("Average monthly")}</strong>
                        </td>
                        <td>{number_format($children_point_avgs['a1'] ,2)}</td>
                        <td>{number_format($children_point_avgs['b1'] ,2)}</td>
                        <td>{number_format($children_point_avgs['c1'] ,2)}</td>
                        <td>{number_format($children_point_avgs['d1'] ,2)}</td>
                        <td>{number_format($children_point_avgs['a2'] ,2)}</td>
                        <td>{number_format($children_point_avgs['b2'] ,2)}</td>
                        <td>{number_format($children_point_avgs['c2'] ,2)}</td>
                        <td>{number_format($children_point_avgs['d2'] ,2)}</td>

                    </tr>
                    <tr>
                        <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                            <strong>{__("Average semesterly")}</strong>
                        </td>
                        <td colspan="3" style="text-align: center">{number_format($children_point_avgs['x1'] ,2)}</td>
                        <td></td>
                        <td colspan="3" style="text-align: center">{number_format($children_point_avgs['x2'] ,2)}</td>
                        <td></td>
                    </tr>
                    {*                    kết thúc kỳ*}
                    <tr>
                        <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                            <strong>{__("End semester")}</strong>
                        </td>
                        <td colspan="4" style="text-align: center">{number_format($children_point_avgs['e1'] ,2)}</td>
                        <td colspan="4" style="text-align: center">{number_format($children_point_avgs['e2'] ,2)}</td>
                    </tr>
                    {*                    kết thúc năm *}
                    <tr>
                        <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                            <strong>{__("End year")}</strong>
                        </td>
                        <td colspan="8" style="text-align: center">{number_format($children_point_avgs['y'] ,2)}</td>
                    </tr>
                    {*                    Nghỉ có phép*}
                    <tr>
                        <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                            <strong>{__("Absent has permission")}</strong>
                        </td>
                        <td colspan="8" style="text-align: center">{$child_absent['absent_true']}</td>
                    </tr>
                    {*                    nghỉ không phép*}
                    <tr>
                        <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                            <strong>{__("Absent without permission")}</strong>
                        </td>
                        <td colspan="8" style="text-align: center">{$child_absent['absent_false']}</td>
                    </tr>
                    </tbody>
                    </table>
                    <strong>{__("Re-Exam")}</strong>
                    <table class="table table-striped table-bordered" style="z-index: 1;">
                        <thead>
                        <tr bgcolor="#fff">
                            <th rowspan="2" class="pinned" style="padding: 8px 6px" id="width_no">#</th>
                            <th rowspan="2" nowrap="true" class="pinned"
                                style="padding: 8px 6px">{__("Subject name")}
                            </th>
                            <th colspan="1">{__("Point")}</th>
                        </tr>
                        </thead>
                        <tbody>
                        {$rowIdx = 1}
                        {foreach $children_subject_reexams as $k => $row}
                            <tr>
                                <td align="center" class="pinned">{$rowIdx}</td>
                                <td nowrap="true" class="pinned text-bold color-blue">
                                    <strong>{$row['name']}</strong>
                                </td>
                                <td style="text-align: center">{$row['point']}</td>
                                {$rowIdx = $rowIdx + 1}
                            </tr>
                        {/foreach}
                        <tr>
                            <td colspan="2" nowrap="true" class="pinned text-bold color-blue"
                                style="text-align: center">
                                <strong>{__("Result Re-exam")}</strong>
                            </td>
                            <td colspan="3" style="text-align: center">{number_format($result_exam ,2)}</td>
                        </tr>
                        </tbody>
                    </table>
                    {/if}
                </div>
                {/if}
                <div class="form-group pl5">
                    <a class="btn btn-default"
                       href="{$system['system_url']}/school/{$username}/reports">{__("Lists")}</a>
                    {if $canEdit}
                        <a class="btn btn-default"
                           href="{$system['system_url']}/school/{$username}/reports/add">{__("Add New")}</a>
                        {if !$data['is_notified']}
                            <button class="btn btn-default js_school-report-notify" data-handle="notify"
                                    data-username="{$username}" data-id="{$data['report_id']}">{__("Notify")}</button>
                        {/if}
                        <a href="{$system['system_url']}/school/{$username}/reports/edit/{$data['report_id']}"
                           class="btn btn-default">{__("Edit")}</a>
                        <button class="btn btn-danger js_school-delete-report" data-username="{$username}"
                                data-id="{$data['report_id']}" data-handle="delete_report">{__("Delete")}</button>
                    {/if}
                </div>
            </div>
        {elseif $sub_view == "listtemp"}
            <div class="panel-body with-table">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th colspan="5">{__("Contact book template list")}&nbsp;({$results|count})</th>
                        </tr>
                        <tr>
                            <th>{__("#")}</th>
                            <th>{__("Title")}</th>
                            <th>{__("Scope")}</th>
                            <th>{__("Created time")}</th>
                            <th>{__("Actions")}</th>
                        </tr>
                        </thead>
                        <tbody>
                        {$idx = 1}
                        {foreach $results as $row}
                            <tr>
                                <td class="align-middle">
                                    <center>{$idx}</center>
                                </td>
                                <td class="align-middle">
                                    <a href="{$system['system_url']}/school/{$username}/reports/edittemp/{$row['report_template_id']}">
                                        {$row['template_name']}
                                    </a>
                                </td>
                                <td class="align_middle" align="center">
                                    {if $row['level'] == CLASS_LEVEL}
                                        {foreach $classes as $class}
                                            {if $class['group_id'] == $row['class_id']}
                                                {$class['group_title']}
                                            {/if}
                                        {/foreach}
                                    {elseif $row['level'] == CLASS_LEVEL_LEVEL}
                                        {foreach $class_levels as $level}
                                            {if $level['class_level_id'] == $row['class_level_id']}
                                                {$level['class_level_name']}
                                            {/if}
                                        {/foreach}
                                    {else}
                                        {__("School")}
                                    {/if}
                                </td>
                                <td align="center" class="align_middle">
                                    {$row['created_at']}
                                </td>
                                <td class="align-middle" align="center">
                                    {if $canEdit}
                                        <div>
                                            <a href="{$system['system_url']}/school/{$username}/reports/edittemp/{$row['report_template_id']}"
                                               class="btn btn-xs btn-default">{__("Edit")}</a>
                                            <button class="btn btn-xs btn-danger js_school-delete-report"
                                                    data-handle="delete_temp" data-username="{$username}"
                                                    data-id="{$row['report_template_id']}">{__("Delete")}</button>
                                        </div>
                                    {/if}
                                </td>
                            </tr>
                            {$idx = $idx + 1}
                        {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
        {elseif $sub_view == "edittemp"}
            <div class="panel-body with-table">
                <div id="open_dialog" class="x-hidden" title="{__("Add new category")|upper}">
                    {include file="ci/school/ajax.school.addcategory.tpl"}
                </div>
                <div id="open_dialog_category_detail" class="x-hidden" title="{__("Category suggests")|upper}">
                    {include file="ci/school/ajax.school.categorydetail.tpl"}
                </div>
                <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_report.php">
                    <input type="hidden" name="school_username" value="{$username}"/>
                    <input type="hidden" name="report_template_id" value="{$data['report_template_id']}"/>
                    <input type="hidden" name="do" value="edit_temp"/>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Template name")} (*)</label>
                        <div class="col-sm-9">
                            <input class="form-control" name="template_name" required maxlength="300"
                                   value="{$data['template_name']}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Scope")} (*)</label>
                        <div class="col-sm-3">
                            <select name="level" id="cate_level" class="form-control">
                                <option value="{$smarty.const.SCHOOL_LEVEL}" {if $data['level'] == $smarty.const.SCHOOL_LEVEL} selected {/if}>{__("School")}</option>
                                <option value="{$smarty.const.CLASS_LEVEL_LEVEL}" {if $data['level'] == $smarty.const.CLASS_LEVEL_LEVEL} selected {/if}>{__("Class level")}</option>
                                <option value="{$smarty.const.CLASS_LEVEL}" {if $data['level'] == $smarty.const.CLASS_LEVEL} selected {/if}>{__("Class")}</option>
                            </select>
                        </div>
                        <div class="col-sm-4 {if $data['level'] != $smarty.const.CLASS_LEVEL_LEVEL}x-hidden{/if}"
                             name="cate_class_level">
                            <select name="class_level_id" id="class_level_id" class="form-control">
                                <option value="0"> {__('Select class level')}</option>
                                {foreach $classLevels as $class_level}
                                    <option value="{$class_level['class_level_id']}"
                                            {if $data['class_level_id']==$class_level['class_level_id']}selected{/if}>{$class_level['class_level_name']}</option>
                                {/foreach}
                            </select>
                        </div>
                        <div class="col-sm-4 {if $data['level'] != ($smarty.const.CLASS_LEVEL)}x-hidden{/if}"
                             name="cate_class">
                            <select name="class_id" id="class_id" data-username="{$username}" class="form-control">
                                <option value="0"> {__('Select class')} </option>
                                {$class_level = -1}
                                {foreach $classes as $class}
                                    {if ($class_level != $class['class_level_id'])}
                                        <option value="" disabled style="color: blue">-----{$class['class_level_name']}
                                            -----
                                        </option>
                                    {/if}
                                    <option value="{$class['group_id']}"
                                            {if $data['class_id']==$class['group_id']}selected{/if}>{$class['group_title']}</option>
                                    {$class_level = $class['class_level_id']}
                                {/foreach}
                            </select>
                        </div>
                    </div>
                    <div class="form-group" align="center">
                        <div class="col-sm-12">
                            <strong><span style="color: red">Lưu ý:</span> Bạn có thể sắp xếp thự tự tiêu chí bằng cách
                                nhấn giữ và kéo thả.</strong>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="sort">
                            <thead>
                            <tr>
                                <th>
                                    {__('#')}
                                </th>
                                <th align="center">
                                    <input type="checkbox" id="select_all" style="float: left">{__('Category')}
                                </th>
                                <th>
                                    {__('Content')}
                                </th>
                                <th>
                                    {__('Category suggest')}
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            {$idx = 1}
                            {foreach $categorysTemp as $category}
                                <tr>
                                    <td align="center" class="align_middle index">{$idx}</td>
                                    <input type="hidden" class="positions" value="{$idx}">
                                    <td align="left" class="align_middle">
                                        <input type="checkbox" name="category_ids[]"
                                               value="{$category['report_template_category_id']}"
                                               {if $category['checked']}checked{/if}>
                                        {$category['category_name']}
                                    </td>
                                    <td>
                                        <textarea type="text" class="note"
                                                  name="content_{$category['report_template_category_id']}"
                                                  style="width: 100%">{$category['template_content']}</textarea>
                                    </td>
                                    <td align="center" class="align_middle">
                                        <a class="btn btn-xs btn-default js_school-category-detail"
                                           data-id="{$category['report_template_category_id']}">{__("Category suggests")}</a>
                                    </td>
                                </tr>
                                {$idx = $idx + 1}
                            {/foreach}
                            <tr id="category_new_pm"></tr>
                            </tbody>
                        </table>
                        <div class="col-sm-12">
                            {if $canEdit}
                                <a class="btn btn-default js_school-category-add">{__("Add new category")}</a>
                                <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                            {/if}
                        </div>
                    </div>
                    <!-- success -->
                    <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                    <!-- success -->

                    <!-- error -->
                    <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                    <!-- error -->
                </form>
            </div>
        {elseif $sub_view == "addtemp"}
            <div class="panel-body">
                <div id="open_dialog" class="x-hidden" title="{__("Add new category")|upper}">
                    {include file="ci/school/ajax.school.addcategory.tpl"}
                </div>
                <div id="open_dialog_category_detail" class="x-hidden" title="{__("Category suggests")|upper}">
                    {include file="ci/school/ajax.school.categorydetail.tpl"}
                </div>
                <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_report.php">
                    <input type="hidden" name="school_username" id="school_username" value="{$username}"/>
                    <input type="hidden" name="do" value="add_temp"/>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Title")} (*)</label>
                        <div class="col-sm-9">
                            <input class="form-control" name="template_name" required autofocus maxlength="100">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Scope")} (*)</label>
                        <div class="col-sm-3">
                            <select name="level" id="schedule_level" class="form-control" data-username="{$username}"
                                    data-id="{$school['page_id']}">
                                <option value="{$smarty.const.SCHOOL_LEVEL}">{__("School")}</option>
                                <option value="{$smarty.const.CLASS_LEVEL_LEVEL}">{__("Class level")}</option>
                                <option value="{$smarty.const.CLASS_LEVEL}">{__("Class")}</option>
                            </select>
                        </div>
                        <div class="col-sm-3 x-hidden" name="schedule_class_level">
                            <select name="class_level_id" id="class_level_id" class="form-control">
                                <option value="0">{__("Select class level")}</option>
                                {foreach $class_levels as $class_level}
                                    <option value="{$class_level['class_level_id']}">{$class_level['class_level_name']}</option>
                                {/foreach}
                            </select>
                        </div>
                        <div class="col-sm-3 x-hidden" name="schedule_class">
                            <select name="class_id" id="class_id" data-username="{$username}" class="form-control">
                                <option value="0">{__("Select class")}</option>
                                {$class_level = -1}
                                {foreach $classes as $class}
                                    {if ($class_level != $class['class_level_id'])}
                                        <option value="" disabled style="color: blue">-----{$class['class_level_name']}
                                            -----
                                        </option>
                                    {/if}
                                    <option value="{$class['group_id']}">{$class['group_title']}</option>
                                    {$class_level = $class['class_level_id']}
                                {/foreach}
                            </select>
                        </div>
                    </div>
                    {*Danh sách hạng mục của trường*}
                    <div class="form-group" align="center">
                        <div class="col-sm-12">
                            <strong><span style="color: red">Lưu ý:</span> Bạn có thể sắp xếp thự tự tiêu chí bằng cách
                                nhấn giữ và kéo thả.</strong>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="sort">
                            <thead>
                            <tr>
                                <th>
                                    {__('#')}
                                </th>
                                <th>
                                    <input type="checkbox" id="select_all" style="float: left">{__('Category')}
                                </th>
                                <th>
                                    {__('Content')}
                                </th>
                                <th>
                                    {__('Category suggest')}
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            {$idx = 1}
                            {foreach $categorys as $category}
                                <tr>
                                    <td align="center" class="align_middle index">{$idx}</td>
                                    <td class="align_middle">
                                        <input type="checkbox" name="category_ids[]"
                                               value="{$category['report_template_category_id']}">
                                        {$category['category_name']}
                                    </td>
                                    <td>
                                        <textarea type="text" class="note"
                                                  name="content_{$category['report_template_category_id']}"
                                                  style="width: 100%"></textarea>
                                    </td>
                                    <td align="center" class="align_middle">
                                        {*<a href="{$system['system_url']}/school/{$username}/reports/editcate/{$category['report_template_category_id']}/2" class="btn btn-xs btn-default">{__("Category suggests)}</a>*}
                                        <a class="btn btn-xs btn-default js_school-category-detail"
                                           data-id="{$category['report_template_category_id']}">{__("Category suggests")}</a>
                                    </td>
                                </tr>
                                {$idx = $idx + 1}
                            {/foreach}
                            <tr id="category_new_pm"></tr>
                            </tbody>
                        </table>
                        <div class="col-sm-12">
                            <a class="btn btn-default js_school-category-add">{__("Add new category")}</a>
                            <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                        </div>
                    </div>
                    <!-- success -->
                    <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                    <!-- success -->
                    <!-- error -->
                    <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                    <!-- error -->

                </form>
            </div>
        {elseif $sub_view == "listcate"}
            <div class="panel-body with-table">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>
                                {__('No.')}
                            </th>
                            <th>
                                {__('Category name')}
                            </th>
                            <th>
                                {__('Actions')}
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        {if count($categorys) > 0}
                            {foreach $categorys as $k => $row}
                                <tr>
                                    <td align="center" class="align_middle">
                                        {$k + 1}
                                    </td>
                                    <td align="center" class="align_middle">
                                        {*@param $id = 1 - Edit hạng mục từ màn hình danh sách hạng mục*}
                                        <a href="{$system['system_url']}/school/{$username}/reports/editcate/{$row['report_template_category_id']}/1">
                                            {$row['category_name']}
                                        </a>
                                    </td>
                                    <td align="center" class="align_middle">
                                        <a href="{$system['system_url']}/school/{$username}/reports/editcate/{$row['report_template_category_id']}/1"
                                           class="btn btn-default btn-xs">
                                            {__("Edit")}
                                        </a>
                                        {if $canEdit}
                                            <a class="btn btn-xs btn-danger js_school-delete-report"
                                               data-id="{$row['report_template_category_id']}"
                                               data-username="{$username}" data-handle="delete_cate">{__("Delete")}</a>
                                        {/if}
                                    </td>
                                </tr>
                            {/foreach}
                        {else}
                            <tr>
                                <td colspan="3" align="center" class="align_middle">{__("No data")}</td>
                            </tr>
                        {/if}
                        </tbody>
                    </table>
                </div>
            </div>
        {elseif $sub_view == "addcate"}
            <div class="mt10" align="center">
                <strong>{__("Add new category and add suggest for category")}</strong>
            </div>
            <div class="panel-body with-table">
                <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_report.php">
                    <input type="hidden" name="school_username" value="{$username}"/>
                    <input type="hidden" name="do" value="add_cate"/>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Category name")} (*)</label>
                        <div class="col-sm-9">
                            <input class="form-control" name="category_name" required maxlength="300">
                        </div>
                    </div>
                    <div class="table-responsive" id="cate_school_add">
                        <table class="table table-striped table-bordered table-hover" id="addCategoryTableNoPopup">
                            <thead>
                            <tr>
                                <th colspan="3">{__("Suggest content for category")}</th>
                            </tr>
                            <tr>
                                <th>
                                    {__('No.')}
                                </th>
                                <th>
                                    {__('Title')}
                                </th>
                                <th>
                                    {__('Actions')}
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td class="col_no align_middle" align="center">1</td>
                                <td>
                                    <input type="text" name="suggests[]" class="form-control"
                                           placeholder="{__("Suggest content for category")}">
                                </td>
                                <td align="center" class="align_middle"><a
                                            class="btn btn-danger btn-xs js_report_template-delete"> {__("Delete")} </a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <a class="btn btn-default js_report_suggest-add">{__("Add new suggest")}</a>
                        <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                    </div>
                    <!-- success -->
                    <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                    <!-- success -->

                    <!-- error -->
                    <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                    <!-- error -->
                </form>
            </div>
        {elseif $sub_view == "editcate"}
            <div class="mt10" align="center">
                <strong>{__("Edit category")}</strong>
            </div>
            <div class="panel-body with-table">
                <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_report.php">
                    <input type="hidden" name="school_username" value="{$username}"/>
                    <input type="hidden" name="do" value="edit_cate"/>
                    <input type="hidden" name="category_id" value="{$data['report_template_category_id']}"/>
                    <input type="hidden" name="p5" value="{$p5}"/>
                    {if isset($p6)}
                        <input type="hidden" name="p6" value="{$p6}"/>
                    {/if}
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Category name")} (*)</label>
                        <div class="col-sm-9">
                            <input class="form-control" name="category_name" value="{$data['category_name']}" required
                                   maxlength="300">
                        </div>
                    </div>
                    <div class="table-responsive" id="cate_school_add">
                        <table class="table table-striped table-bordered table-hover" id="addCategoryTableNoPopup">
                            <thead>
                            <tr>
                                <th colspan="3">{__("Suggest content for category")}</th>
                            </tr>
                            <tr>
                                <th>
                                    {__('No.')}
                                </th>
                                <th>
                                    {__('Title')}
                                </th>
                                <th>
                                    {__('Actions')}
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            {$idx = 1}
                            {foreach $data['suggests'] as $row}
                                <tr>
                                    <td class="col_no align_middle" align="center">{$idx}</td>
                                    <td>
                                        <input type="text" name="suggests[]" class="form-control" value="{$row}">
                                    </td>
                                    <td align="center" class="align_middle">
                                        {if $canEdit}
                                            <a class="btn btn-danger btn-xs js_report_template-delete"> {__("Delete")}</a>
                                        {/if}
                                    </td>
                                </tr>
                                {$idx = $idx + 1}
                            {/foreach}
                            </tbody>
                        </table>
                        {if $canEdit}
                            <a class="btn btn-default js_report_suggest-add">{__("Add new suggest")}</a>
                            <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                        {/if}
                    </div>
                    <!-- success -->
                    <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                    <!-- success -->

                    <!-- error -->
                    <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                    <!-- error -->
                </form>
            </div>
        {/if}
    </div>
</div>