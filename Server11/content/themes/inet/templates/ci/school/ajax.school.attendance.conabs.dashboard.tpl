<div id="conabs_list">
    <div class="box-primary">
        <div class="box-header">
            <i class="fa fa-bomb"></i>
            <strong>{__("Consecutive absent student")}</strong>
            <br/>{__("Last update")}: {$conabs['updated_at']}

            {*<i class="fas fa-sync-alt pull-right flip"></i>*}
            <a class="btn btn-xs js_school_conabs_dashboard pull-right flip" data-username = "{$username}" title="{__("Refresh")}"><strong><i class="fas fa-redo-alt"></i> {__("Refresh")}</strong></a>
        </div>

        <div class="list-group">
            <table class="table table-striped table-bordered table-hover" id="hide_absent">
                <thead>
                <tr>
                    <th>#</th>
                    <th>{__("Full name")}</th>
                    <th>{__("Class")}</th>
                    <th>{__("Absent day")}</th>
                    <th>{__("Reason")}</th>
                </tr>
                </thead>
                <tbody>
                {$idx = 1}
                {foreach $conabs['list'] as $k => $child}
                    {if $k < 10}
                        <tr>
                            <td align="center">{$idx}</td>
                            <td><a href="{$system['system_url']}/school/{$username}/attendance/child/{$child['child_id']}">{$child['child_name']}</a></td>
                            <td align="center">{$child['group_title']}</td>
                            <td align="center">
                                {if isset($child['number_of_absent_day']) && ($child['number_of_absent_day'] > 0)}
                                    {if {$child['number_of_absent_day']} > 9}{__("More than")}&nbsp;{/if}{$child['number_of_absent_day']}&nbsp;{__("day(s)")}
                                {else}
                                    {__("More than")}&nbsp;10&nbsp;{__("day(s)")}
                                {/if}
                            </td>
                            <td>{$child['reason']}</td>
                        </tr>
                        {$idx = $idx + 1}
                    {/if}
                {/foreach}
                {if count($conabs['list']) > 10}
                    <tr>
                        <td colspan="5" align="center" style="padding: 5px">
                            <button class="full_absent_but btn btn-xs btn-info mb0">{__("See More")}</button>
                        </td>
                    </tr>
                {/if}
                {if $conabs['list']|count == 0}
                    <tr class="odd">
                        <td valign="top" align="center" colspan="5" class="dataTables_empty">
                            {__("No data available in table")}
                        </td>
                    </tr>
                {/if}
                </tbody>
            </table>
            <table class="table table-striped table-bordered table-hover x-hidden" id="full_absent">
                <thead>
                <tr>
                    <th>#</th>
                    <th>{__("Full name")}</th>
                    <th>{__("Class")}</th>
                    <th>{__("Absent day")}</th>
                    <th>{__("Reason")}</th>
                </tr>
                </thead>
                <tbody>
                {$idx = 1}
                {foreach $conabs['list'] as $k => $child}
                    <tr>
                        <td align="center">{$idx}</td>
                        <td><a href="{$system['system_url']}/school/{$username}/attendance/child/{$child['child_id']}">{$child['child_name']}</a></td>
                        <td align="center">{$child['group_title']}</td>
                        <td align="center">
                            {if isset($child['number_of_absent_day']) && ($child['number_of_absent_day'] > 0)}
                                {if {$child['number_of_absent_day']} > 9}{__("More than")}&nbsp;{/if}{$child['number_of_absent_day']}&nbsp;{__("day(s)")}
                            {else}
                                {__("More than")}&nbsp;10&nbsp;{__("day(s)")}
                            {/if}
                        </td>
                        <td>{$child['reason']}</td>
                    </tr>
                    {$idx = $idx + 1}
                {/foreach}
                {if count($conabs['list']) > 10}
                    <tr>
                        <td colspan="5" align="center" style="padding: 5px">
                            <button class="hide_absent_but btn btn-xs btn-info mb0">{__("Read less")}</button>
                        </td>
                    </tr>
                {/if}
                {if $conabs['list']|count == 0}
                    <tr class="odd">
                        <td valign="top" align="center" colspan="5" class="dataTables_empty">
                            {__("No data available in table")}
                        </td>
                    </tr>
                {/if}
                </tbody>
            </table>
        </div>
    </div>
</div>