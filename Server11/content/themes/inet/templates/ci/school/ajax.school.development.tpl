<div class="mb10"><strong>{__("Summarize the growth index of the child in the class")}</strong></div>
<table class="table table-striped table-bordered table-hover js_dataTable">
    <thead>
    <tr>
        <th><strong>{__("#")}</strong></th>
        <th>{__("Time")}</th>
        <th>{__("Get weight")} %</th>
        <th>{__("Lose weight")} %</th>
        <th>{__("Not changed")} %</th>
        <th>{__("Unavailable")} %</th>
        <th>{__("Actions")}</th>
    </tr>
    </thead>
    <tbody>
    {$idx = 1}
    {foreach $totalMonthSummary as $month => $row}
        <tr>
            <td class="align-middle" align="center"><strong>{$idx}</strong></td>
            <td class="align-middle" align="center">{$month}</td>
            <td class="align-middle" align="center">{$row['month_up']} %</td>
            <td class="align-middle" align="center">{$row['month_down']} %</td>
            <td class="align-middle" align="center">{$row['month_equal']} %</td>
            <td class="align-middle" align="center">{$row['month_no']} %</td>
            <td class="align-middle" align="center"> <a href="{$system['system_url']}/school/{$username}/children/developmentclass/{$class_id}/{str_replace('/', '-', $month)}" class="btn btn-xs btn-default">{__("Detail")}</a></td>
        </tr>
        {$idx = $idx + 1}
    {/foreach}
    </tbody>
</table>