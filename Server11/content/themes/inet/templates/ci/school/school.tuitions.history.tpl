<div class="panel-body with-table">
    <table class="table table-bordered">
        <tr>
            <td colspan="4"><strong>{__("Fees and services usage history related to the tuition calculation")}</strong></td>
        </tr>
        <tr>
            <td class="col-sm-3 text-right"><strong>{__("Class")}</strong></td>
            <td>{$data['group_title']}</td>
            <td class="col-sm-3 text-right"><strong>{__("Month")}</strong></td>
            <td>{$data['pre_month']}</td>
        </tr>
    </table>
    <a class="btn btn-default" href="{$system['system_url']}/school/{$username}/tuitions/detail/{$data['tuition_id']}">{__("Tuition detail")}</a>
    <br/><br/>
    {* Hiển thị bảng ký hiệu giải thích trạng thái điểm danh *}
    {if $school['tuition_view_attandance'] && ($school['attendance_use_leave_early'] || $school['attendance_use_come_late'] || $school['attendance_absence_no_reason'])}
        <table class="table table-striped table-bordered table-hover">
            <tbody>
            <tr>
                <td align="right"><strong>{__("Symbol")}:</strong></td>
                {if $school['attendance_use_come_late']}
                    <td bgcolor="#6495ed" align="center"><i class="fa fa-check" aria-hidden="true"></td>
                    <td align="left">{__("Come late")}</td>
                {/if}
                {if $school['attendance_use_leave_early']}
                    <td bgcolor="#008b8b" align="center"><i class="fa fa-check" aria-hidden="true"></td>
                    <td align="left">{__("Leave early")}</td>
                {/if}
                <td align="center"><i class="fa fa-check" aria-hidden="true"></td>
                <td align="left">{__("Present")}</td>

                {if $school['attendance_absence_no_reason']}
                    <td bgcolor="#ff1493" align="center"><i class="fa fa-times" aria-hidden="true"></td>
                    <td align="left">{__("Without permission")}</td>
                {/if}

                <td bgcolor="#deb887" align="center"><i class="fa fa-times" aria-hidden="true"></td>
                <td align="left">{__("With permission")}</td>
            </tr>
            </tbody>
        </table>
    {/if}
    <div class="table-responsive" id="children_list">
        <table class="table table-striped table-bordered table-hover">
            <thead>
            <tr>
                <th>
                    <div class="pull-left flip">{__("Fees and services usage history of each child")}&nbsp;({$data['tuition_child']|count})</div>
                    <div class="pull-right flip">
                        {__("Usage total of class")}&nbsp;({$smarty.const.MONEY_UNIT})
                        <input type="text" class="text-right" value="{moneyFormat($data['class_total'])}" readonly/>
                    </div>
                </th>
            </tr>
            </thead>
            <tbody>
            {$detailIdx = 1}
            {foreach $data['tuition_child'] as $child}
                <tr>
                    <td>
                        <div style="color: #597BA5; display: inline;">
                            <strong>{$detailIdx} - {$child['child_name']}</strong>&nbsp;|&nbsp;{$child['birthday']}
                            {if isset($child['attendance_count'])}&nbsp;|&nbsp;
                                {__("Number of charged days last month")}:&nbsp;{$child['attendance_count']}
                            {/if}
                        </div>
                        <br/>
                        {if $school['tuition_view_attandance'] && (count($child['attendances']) > 0)}
                            <table class="table table-striped table-bordered table-hover">
                                <tr>
                                    <td style="width: 90px"><a href="{$system['system_url']}/school/{$username}/attendance/child/{$child['child_id']}" target="_blank"><strong>{__("Attendance")}</strong></a></td>
                                    {foreach $child['attendances'] as $attendance}
                                        <td style="padding: 5px" align="center"
                                                {if $attendance['status'] == $smarty.const.ATTENDANCE_COME_LATE}
                                                    bgcolor="#6495ed"
                                                {elseif $attendance['status'] == $smarty.const.ATTENDANCE_EARLY_LEAVE}
                                                    bgcolor="#008b8b"
                                                {elseif $attendance['status'] == $smarty.const.ATTENDANCE_ABSENCE_NO_REASON}
                                                    bgcolor="#ff1493"
                                                {elseif $attendance['status'] == $smarty.const.ATTENDANCE_ABSENCE}
                                                    bgcolor="#deb887"
                                                {/if}
                                        >{$attendance['display_date']}</td>
                                    {/foreach}
                                </tr>
                            </table>
                        {/if}
                        {if (count($child['tuition_detail']['fees']) + count($child['tuition_detail']['services'])) > 0}
                            <table class="table table-striped table-bordered table-hover bg_white">
                                <thead>
                                <tr>
                                    <td colspan="6"><strong>{__("Fee usage history of previous month")} {$data['pre_month']}</strong></td>
                                </tr>
                                <tr>
                                    <th>#</th>
                                    <th>{__("Fee name")}</th>
                                    <th class="text-center">{__("Unit")}</th>
                                    <th class="text-center">{__("Quantity")}</th>
                                    <th class="text-right">{__("Unit price")}&nbsp;({$smarty.const.MONEY_UNIT})</th>
                                    <th class="text-right">{__("Money amount")}&nbsp;({$smarty.const.MONEY_UNIT})</th>
                                </tr>
                                </thead>
                                <tbody>
                                {*Hiển thị danh sách phí của trẻ*}
                                {$idx = 1}
                                {foreach $child['tuition_detail']['fees'] as $detail}
                                    {if $detail['to_money'] > 0}
                                        <tr>
                                            <td align="center">{$idx}</td>
                                            <td>{$detail['fee_name']}</td>
                                            <td class="text-center">{if $detail['fee_type'] == $smarty.const.FEE_TYPE_MONTHLY}{__('Monthly')}{else}{__('Daily')}{/if}</td>
                                            <td class="text-center">{$detail['quantity']}</td>
                                            <td class="text-right">{moneyFormat($detail['unit_price'])}</td>
                                            <td class="text-right">{moneyFormat($detail['to_money'])}</td>
                                        </tr>
                                        {$idx = $idx + 1}
                                    {/if}
                                {/foreach}
                                {*Hiển thị danh sách dịch vụ theo THÁNG và ĐIỂM DANH*}
                                {if count($child['tuition_detail']['services']) > 0}
                                    <tr><td colspan="6"><strong>{__("Service")}</strong></td></tr>
                                    {$idx = 1}
                                    {foreach $child['tuition_detail']['services'] as $detail}
                                        {if $detail['to_money'] > 0}
                                            <tr>
                                                <td align="center">{$idx}</td>
                                                <td>{$detail['service_name']}</td>
                                                <td class="text-center">{if $detail['service_type'] == $smarty.const.SERVICE_TYPE_MONTHLY}{__('Monthly')}{else}{__('Daily')}{/if}</td>
                                                <td class="text-center">{$detail['quantity']}</td>
                                                <td class="text-right">{moneyFormat($detail['unit_price'])}</td>
                                                <td class="text-right">{moneyFormat($detail['to_money'])}</td>
                                            </tr>
                                            {$idx = $idx + 1}
                                        {/if}
                                    {/foreach}
                                {/if}
                                {*Hiển thị danh sách dịch vụ theo SỐ LẦN*}
                                {if count($child['tuition_detail']['count_based_services']) > 0}
                                    <tr><td colspan="6"><strong>{__("Count-based service of previous month")} {$child['pre_month']}</strong></td></tr>
                                    {$idx = 1}
                                    {foreach $child['tuition_detail']['count_based_services'] as $detail}
                                        {if $detail['to_money'] > 0}
                                            <tr>
                                                <td align="center">{$idx}</td>
                                                <td>{$detail['service_name']}</td>
                                                <td class="text-center">{__('Times')}</td>
                                                <td class="text-center">{$detail['quantity']}</td>
                                                <td class="text-right">{moneyFormat($detail['unit_price'])}</td>
                                                <td class="text-right">{moneyFormat($detail['to_money'])}</td>
                                            </tr>
                                            {$idx = $idx + 1}
                                        {/if}
                                    {/foreach}
                                {/if}

                                <tr>
                                    <td colspan="4"></td>
                                    <td class="text-right"><strong>{__("Child usage total")}&nbsp;({$smarty.const.MONEY_UNIT})</strong></td>
                                    <td class="text-right">{moneyFormat($child['child_total'])}</td>
                                </tr>
                                </tbody>
                            </table>
                        {/if}
                    </td>
                </tr>
                <tr height = "10"></tr>
                {$detailIdx = $detailIdx + 1}
            {/foreach}
            </tbody>
        </table>
        <a class="btn btn-default" href="{$system['system_url']}/school/{$username}/tuitions/detail/{$data['tuition_id']}">{__("Tuition detail")}</a>
    </div>
</div>
