{* Nội dung màn hình popup hiển thị danh sách các loại phí và dịch vụ *}
<input type="hidden" name="fee_id_list" id="fee_id_list" value="{$fee_id_list}"/>
<input type="hidden" name="service_id_list" id="service_id_list" value="{$service_id_list}"/>

<div><strong>{__("Fee list")}</strong></div>
<table class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th>#</th>
        <th>{__("Select")}</th>
        <th>{__("Fee name")}</th>
        <th>{__("Unit price")} ({$smarty.const.MONEY_UNIT})</th>
        <th>{__("Fee cycle")}</th>
        <th>{__("Fee level")}</th>
    </tr>
    </thead>
    <tbody>
        {foreach $fee_list as $idx=>$fee}
            <tr>
                <td align="center"><strong>{$idx + 1}</strong></td>
                <td align="center"><input type="checkbox" class="fee_id" value="{$fee['fee_id']}"/></td>
                <td>{$fee['fee_name']}</td>
                <td align="right" style="vertical-align:middle">{moneyFormat($fee['unit_price'])}</td>
                <td align="center" style="vertical-align:middle">
                    {if $fee['type']==$smarty.const.FEE_TYPE_MONTHLY}
                        {__("Monthly fee")}
                    {elseif $fee['type']==$smarty.const.FEE_TYPE_DAILY}
                        {__("Daily fee")}
                    {/if}
                </td>
                <td align="center" style="vertical-align:middle">
                    {if $fee['level']==$smarty.const.SCHOOL_LEVEL}
                        {__("School")}
                    {elseif $fee['level']==$smarty.const.CLASS_LEVEL_LEVEL}
                        {$fee['class_level_name']}
                    {elseif $fee['level']==$smarty.const.CLASS_LEVEL}
                        {$fee['group_title']}
                    {elseif $fee['level']==$smarty.const.CHILD_LEVEL}
                        {__("Children")}
                    {/if}
                </td>
            </tr>
        {/foreach}
    </tbody>
</table>

<div><strong>{__("Service list")}</strong></div>
<table class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th>#</th>
            <th>{__("Actions")}</th>
            <th>{__("Service name")}</th>
            <th>{__("Service fee")} ({$smarty.const.MONEY_UNIT})</th>
            <th>{__("Service type")}</th>
        </tr>
    </thead>
    <tbody>
    {foreach $service_list as $idx=>$service}
        <tr>
            <td align="center"><strong>{$idx + 1}</strong></td>
            <td align="center"><input type="checkbox" class="service_id" value="{$service['service_id']}"/></td>
            <td>{$service['service_name']}</td>
            <td align="right">{moneyFormat($service['fee'])}</td>
            <td>
                {if $service['type']==$smarty.const.SERVICE_TYPE_MONTHLY}
                    {__("Monthly service")}
                {elseif $service['type']==$smarty.const.SERVICE_TYPE_DAILY}
                    {__("Daily service")}
                {else}
                    {__("Count-based service")}
                {/if}
            </td>
        </tr>
    {/foreach}
    </tbody>
</table>
<div class="pull-right flip">
    {__("Late PickUp")}?&nbsp;<input type="checkbox" value="1" id="pickup" name="pickup" {if $has_pickup}disabled{/if}/>&nbsp;&nbsp;
    <a class="btn btn-default js_tuition_popup_close">{__("Cancel")}</a>
    <a class="btn btn-primary js_add_fee_to_tuition" child_id="{$child_id}" data-username="{$username}">{__("Add")}</a>
</div>
