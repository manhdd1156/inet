{if $pickups|count == 0}
    <div align="center">
        <font color="#ff4500">
            <strong>{__("Not assigned")}</strong>
        </font>
    </div>
{else}
    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <th width="7%">#</th>
            <th width="15%">{__('Time')}</th>
            <th width="35%">{__('Teacher')}</th>
            <th width="20%">{__('Class')}</th>
            <th width="10%">{__('Number of student')}</th>
            <th width="13%">{__("Actions")}</th>
        </tr>
        </thead>
        <tbody>
        {$idx = 1}
        {foreach $pickups as $pickup}
            <tr>
                <td align="center" style="vertical-align:middle"><strong>{$idx}</strong></td>
                <td align="center" style="vertical-align:middle">
                    <strong>{$pickup['pickup_time']}</strong><br>
                    {__($pickup['pickup_day'])}
                </td>
                <td align="center" style="vertical-align:middle">
                    {if $pickup['pickup_assign']|count == 0 }
                        <strong>&#150;</strong>
                    {else}
                        {foreach $pickup['pickup_assign'] as $assign}
                            <label class="col-sm-2"></label>
                            <strong>
                                <label class="col-sm-9 text-left" {if $assign['user_id'] == $user->_data['user_id']} style="color: royalblue" {/if}>&#9679;&#09;{$assign['user_fullname']}
                            </strong>
                        {/foreach}
                    {/if}

                </td>
                <td align="center" style="vertical-align:middle">
                    {if !is_empty({$pickup['class_name']})}
                        <strong>{$pickup['class_name']}</strong>
                    {else}
                        <strong>&#150;</strong>
                    {/if}
                </td>
                <td align="center" style="vertical-align:middle">
                    {if $pickup['total_child'] > 0}
                        <font color="blue"><strong>{$pickup['total_child']}</strong></font>
                    {else}
                        <font color="#ff4500"><strong>{$pickup['total_child']}</strong></font>
                    {/if}

                </td>

                <td align="center" nowrap="true" style="vertical-align:middle">
                    {if $pickup['action'] == 0}
                        <i class="fa fa-times" style="color:black" title="Chưa có thông tin" aria-hidden="true"></i>
                    {elseif $pickup['action'] == 1}
                        <a href="{$system['system_url']}/class/{$username}/pickup/detail/{$pickup['pickup_id']}" class="btn btn-xs btn-default">
                            {__("Edit")}
                        </a>
                    {elseif $pickup['action'] == 2}
                        <a class="btn btn-xs btn-default js_school-pickup_edit"
                           data-username="{$username}" data-time="{$pickup['pickup_time']}">
                            {__("Add child")}
                        </a>
                    {/if}
                </td>
            </tr>
            {$idx = $idx + 1}
        {/foreach}
        </tbody>
    </table>
{/if}

