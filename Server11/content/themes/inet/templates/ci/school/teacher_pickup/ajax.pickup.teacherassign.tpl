{$classCnt = $classes|count}
<thead>
<tr>
    <th class="col-sm-2"></th>
    {foreach $classes as $class}
        <th class="col-sm-3"><center>{$class['class_name']}</center></th>
    {/foreach}
</tr>
</thead>
<tbody>
<tr>
    {$classCnt = $classes|count}
    <td class="align-middle" align="center">
        <strong>{__("Monday")}</strong> <br> {$day['mon']}
    </td>

    {for $i = 0; $i < $classCnt; $i++}
        <td class="align-middle">
            <div id="assign_class_2_{$classes[$i]['pickup_class_id']}" class="assign_class pl10">
                {foreach $data[2] as $class_id => $arr }
                    {if $class_id == $classes[$i]['pickup_class_id']}
                        {foreach $arr as $info}
                            <div id="assign_teacher_2_{$class_id}_{$info['user_id']}">
                                <a class="text-left {if $info['user_id'] == $user->_data['user_id']}strong{/if}">&#9679;&#09;{$info['user_fullname']}</a>
                                <input type="hidden" name="assign_day[]" value="2">
                                <input type="hidden" name="assign_class[]" value="{$class_id}">
                                <input type="hidden" name="assign_teacher[]" value="{$info['user_id']}">
                            </div>
                        {/foreach}
                    {/if}
                {/foreach}
            </div>
        </td>
    {/for}

</tr>
<tr>
    <td class="align-middle" align="center">
        <strong>{__("Tuesday")}</strong> <br> {$day['tue']}
    </td>

    {for $i = 0; $i < $classCnt; $i++}
        <td class="align-middle">
            <div id="assign_class_3_{$classes[$i]['pickup_class_id']}" class="assign_class pl10">
                {foreach $data[3] as $class_id => $arr }
                    {if $class_id == $classes[$i]['pickup_class_id']}
                        {foreach $arr as $info}
                            <div id="assign_teacher_3_{$class_id}_{$info['user_id']}">
                                <a class="text-left {if $info['user_id'] == $user->_data['user_id']}strong{/if}">&#9679;&#09;{$info['user_fullname']}</a>
                                <input type="hidden" name="assign_day[]" value="3">
                                <input type="hidden" name="assign_class[]" value="{$class_id}">
                                <input type="hidden" name="assign_teacher[]" value="{$info['user_id']}">
                            </div>
                        {/foreach}
                    {/if}
                {/foreach}
            </div>
        </td>
    {/for}

</tr>
<tr>
    <td class="align-middle" align="center">
        <strong>{__("Wednesday")}</strong> <br> {$day['wed']}
    </td>
    {for $i = 0; $i < $classCnt; $i++}
        <td class="align-middle">
            <div id="assign_class_4_{$classes[$i]['pickup_class_id']}" class="assign_class pl10">
                {foreach $data[4] as $class_id => $arr }
                    {if $class_id == $classes[$i]['pickup_class_id']}
                        {foreach $arr as $info}
                            <div id="assign_teacher_4_{$class_id}_{$info['user_id']}">
                                <a class="text-left {if $info['user_id'] == $user->_data['user_id']}strong{/if}">&#9679;&#09;{$info['user_fullname']}</a>
                                <input type="hidden" name="assign_day[]" value="4">
                                <input type="hidden" name="assign_class[]" value="{$class_id}">
                                <input type="hidden" name="assign_teacher[]" value="{$info['user_id']}">
                            </div>
                        {/foreach}
                    {/if}
                {/foreach}
            </div>
        </td>
    {/for}
</tr>
<tr>
    <td class="align-middle" align="center">
        <strong>{__("Thursday")}</strong> <br> {$day['thu']}
    </td>
    {for $i = 0; $i < $classCnt; $i++}
        <td class="align-middle">
            <div id="assign_class_5_{$classes[$i]['pickup_class_id']}" class="assign_class pl10">
                {foreach $data[5] as $class_id => $arr }
                    {if $class_id == $classes[$i]['pickup_class_id']}
                        {foreach $arr as $info}
                            <div id="assign_teacher_5_{$class_id}_{$info['user_id']}">
                                <a class="text-left {if $info['user_id'] == $user->_data['user_id']}strong{/if}">&#9679;&#09;{$info['user_fullname']}</a>
                                <input type="hidden" name="assign_day[]" value="5">
                                <input type="hidden" name="assign_class[]" value="{$class_id}">
                                <input type="hidden" name="assign_teacher[]" value="{$info['user_id']}">
                            </div>
                        {/foreach}
                    {/if}
                {/foreach}
            </div>
        </td>
    {/for}
</tr>
<tr>
    <td class="align-middle" align="center">
        <strong>{__("Friday")}</strong> <br> {$day['fri']}
    </td>
    {for $i = 0; $i < $classCnt; $i++}
        <td class="align-middle">
            <div id="assign_class_6_{$classes[$i]['pickup_class_id']}" class="assign_class pl10">
                {foreach $data[6] as $class_id => $arr }
                    {if $class_id == $classes[$i]['pickup_class_id']}
                        {foreach $arr as $info}
                            <div id="assign_teacher_6_{$class_id}_{$info['user_id']}">
                                <a class="text-left {if $info['user_id'] == $user->_data['user_id']}strong{/if}">&#9679;&#09;{$info['user_fullname']}</a>
                                <input type="hidden" name="assign_day[]" value="6">
                                <input type="hidden" name="assign_class[]" value="{$class_id}">
                                <input type="hidden" name="assign_teacher[]" value="{$info['user_id']}">
                            </div>
                        {/foreach}
                    {/if}
                {/foreach}
            </div>
        </td>
    {/for}
</tr>
<tr>
    <td class="align-middle" align="center">
        <strong>{__("Saturday")}</strong> <br> {$day['sat']}
    </td>
    {for $i = 0; $i < $classCnt; $i++}
        <td class="align-middle">
            <div id="assign_class_7_{$classes[$i]['pickup_class_id']}" class="assign_class pl10">
                {foreach $data[7] as $class_id => $arr }
                    {if $class_id == $classes[$i]['pickup_class_id']}
                        {foreach $arr as $info}
                            <div id="assign_teacher_7_{$class_id}_{$info['user_id']}">
                                <a class="text-left {if $info['user_id'] == $user->_data['user_id']}strong{/if}">&#9679;&#09;{$info['user_fullname']}</a>
                                <input type="hidden" name="assign_day[]" value="7">
                                <input type="hidden" name="assign_class[]" value="{$class_id}">
                                <input type="hidden" name="assign_teacher[]" value="{$info['user_id']}">
                            </div>
                        {/foreach}
                    {/if}
                {/foreach}
            </div>
        </td>
    {/for}
</tr>
<tr>
    <td class="align-middle" align="center">
        <strong>{__("Sunday")}</strong> <br> {$day['sun']}
    </td>
    {for $i = 0; $i < $classCnt; $i++}
        <td class="align-middle">
            <div id="assign_class_8_{$classes[$i]['pickup_class_id']}" class="assign_class pl10">
                {foreach $data[8] as $class_id => $arr }
                    {if $class_id == $classes[$i]['pickup_class_id']}
                        {foreach $arr as $info}
                            <div id="assign_teacher_8_{$class_id}_{$info['user_id']}">
                                <a class="text-left {if $info['user_id'] == $user->_data['user_id']}strong{/if}">&#9679;&#09;{$info['user_fullname']}</a>
                                <input type="hidden" name="assign_day[]" value="8">
                                <input type="hidden" name="assign_class[]" value="{$class_id}">
                                <input type="hidden" name="assign_teacher[]" value="{$info['user_id']}">
                            </div>
                            <br>
                        {/foreach}
                    {/if}
                {/foreach}
            </div>
        </td>
    {/for}
</tr>
</tbody>