<strong>{__("Student list")}&nbsp;({$result['total']} {__("Children")}) - {__("Parent active")} ({$countParentActive})</strong>
<div class="pull-right flip">
    <label id="export_processing" class="btn btn-info x-hidden">{__("Loading")}...</label>
</div>
<table class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th>{__("#")}</th>
        <th>{__("Full name")}</th>
        <th>{__("Parent phone")}</th>
        <th>{__("Parent")}</th>
        <th>{__("Last active")}</th>
    </tr>
    </thead>
    <tbody>
    {$classId = -1}
    {$idx = ($result['page'] - 1) * $smarty.const.PAGING_LIMIT + 1}
    {foreach $result['children'] as $row}
        {if (!isset($result['class_id']) || ($result['class_id']=="")) && ($classId != $row['class_id'])}
            <tr>
                <td colspan="5">
                    {if $row['class_id'] > 0}
                        {__("Class")}:&nbsp;{$row['group_title']}
                    {else}
                        {__("No class")}
                    {/if}
                </td>
            </tr>
        {/if}
        {if count($row['parent']) <= 1}
            <tr>
                <td class="align-middle" align="center">{$idx}</td>
                <td class="align-middle"><a href="{$system['system_url']}/school/{$username}/children/detail/{$row['child_id']}">{$row['child_name']}</a></td>
                <td class="align-middle">{$row['parent_phone']}</td>

                {if count($row['parent']) == 0}
                    <td class="align-middle">
                        {__("No parent")}
                    </td>
                    <td></td>
                {else}
                    {foreach $row['parent'] as $_user}
                        <td>
                            <span class="name js_user-popover" data-uid="{$_user['user_id']}">
                                <a href="{$system['system_url']}/{$_user['user_name']}">{$_user['user_fullname']}</a>
                            </span>
                            {if $_user['user_id'] != $user->_data['user_id']}
                                <a href="#" class="far fa-comments fa-lg js_chat-start" data-name="{$_user['user_fullname']}" data-uid="{$_user['user_id']}"></a>
                            {/if}
                        </td>
                        <td>{$_user['user_last_active']}</td>
                    {/foreach}
                {/if}
            </tr>
        {else}
            {foreach $row['parent'] as $k => $_user}
                <tr>
                    {if $k == 0}
                        <td rowspan="{count($row['parent'])}" class="align-middle">
                            {$idx}
                        </td>
                        <td rowspan="{count($row['parent'])}" class="align-middle">
                            <a href="{$system['system_url']}/school/{$username}/children/detail/{$row['child_id']}">{$row['child_name']}</a>
                        </td>
                        <td rowspan="{count($row['parent'])}" class="align-middle">
                            {$row['parent_phone']}
                        </td>
                    {/if}
                    <td>
                        <span class="name js_user-popover" data-uid="{$_user['user_id']}">
                            <a href="{$system['system_url']}/{$_user['user_name']}">{$_user['user_fullname']}</a>
                        </span>
                        {if $_user['user_id'] != $user->_data['user_id']}
                            <a href="#" class="far fa-comments fa-lg js_chat-start" data-name="{$_user['user_fullname']}" data-uid="{$_user['user_id']}"></a>
                        {/if}
                    </td>
                    <td>
                        {$_user['user_last_active']}
                    </td>
                </tr>
            {/foreach}
        {/if}
        {$classId = $row['class_id']}
        {$idx = $idx + 1}
    {/foreach}

    {if $result['children']|count == 0}
        <tr class="odd">
            <td valign="top" align="center" colspan="7" class="dataTables_empty">
                {__("No data available in table")}
            </td>
        </tr>
    {/if}
    {if $result['page_count'] > 1}
        <tr>
            <td colspan="7">
                <div class="pull-right flip">
                    <ul class="pagination">
                        {for $idx = 1 to $result['page_count']}
                            {if $idx == $result['page']}
                                <li class="active"><a href="#">{$idx}</a></li>
                            {else}
                                <li>
                                    <a href="#" class="js_child-statistics-search" data-username="{$username}" data-id="{$school['page_id']}" data-isnew="0" data-page="{$idx}">{$idx}</a>
                                </li>
                            {/if}
                        {/for}
                    </ul>
                </div>
            </td>
        </tr>
    {/if}
    </tbody>
</table>