{if (($service_id > 0) || ($child_id > 0))}
    {if $service_type == $smarty.const.SERVICE_TYPE_COUNT_BASED}
        <div><strong>{__("Usage history")}&nbsp;({$history|count} {__("times")})</strong></div>
        <table class="table table-striped table-bordered table-hover">
            {if (isset($child_id) && ($child_id > 0))}
                {*Đối với trường hợp tìm một trẻ*}
                <thead>
                    <tr>
                        <th>#</th>
                        <th>{__("Using time")}</th>
                        <th>{__("Registrar")}</th>
                        <th>{__("Registered time")}</th>
                    </tr>
                </thead>
                <tbody>
                    {$idx = 1}
                    {foreach $history as $row}
                        <tr>
                            <td align="center">{$idx}</td>
                            <td>{$row['using_at']}</td>
                            <td>{$row['user_fullname']}</td>
                            <td>{$row['recorded_at']}</td>
                        </tr>
                        {$idx = $idx + 1}
                    {/foreach}
                    {if $idx > 2}
                        <tr>
                            <td colspan="2" align="center"><strong><font color="red">{__("Total")|upper}</font></strong></td>
                            <td colspan="2" align="left"><strong><font color="red">{$idx - 1} {__("times")}</font></strong></td>
                        </tr>
                    {/if}
                    {if $history|count == 0}
                        <tr class="odd">
                            <td valign="top" align="center" colspan="4" class="dataTables_empty">
                                {__("No data available in table")}
                            </td>
                        </tr>
                    {/if}
                </tbody>
            {elseif (isset($class_id) && ($class_id > 0))}
                {*Đối với trường hợp tìm một lớp*}
                <thead>
                    <tr>
                        <th>#</th>
                        <th>{__("Full name")}</th>
                        <th>{__("Registrar")}</th>
                        <th>{__("Registered time")}</th>
                    </tr>
                </thead>
                <tbody>
                    {$idx = 1}
                    {$dayTotal = 0}
                    {$usingAt = 'noga'}
                    {foreach $history as $row}
                        {if $row['using_at'] != $usingAt}
                            {if $idx > 1}
                                <tr>
                                    <td colspan="2" align="center"><strong><font color="blue">{__("Total of day")}</font></strong></td>
                                    <td colspan="2" align="left"><strong><font color="blue">{$dayTotal} {__("times")}</font></strong></td>
                                </tr>
                            {/if}
                            <tr>
                                <td colspan="4"><strong>{$row['using_at']}</strong></</td>
                            </tr>
                            {$dayTotal = 0}
                        {/if}
                        <tr>
                            <td align="center">{$idx}</td>
                            <td>{$row['child_name']}</td>
                            <td>{$row['user_fullname']}</td>
                            <td>{$row['recorded_at']}</td>
                        </tr>
                        {$usingAt = $row['using_at']}
                        {$dayTotal = $dayTotal + 1}
                        {$idx = $idx + 1}
                    {/foreach}
                    {if $idx > 2}
                        <tr>
                            <td colspan="2" align="center"><strong><font color="blue">{__("Total of day")}</font></strong></td>
                            <td colspan="2" align="left"><strong><font color="blue">{$dayTotal} {__("times")}</font></strong></td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center"><strong><font color="red">{__("Total")|upper}</font></strong></td>
                            <td colspan="2" align="left"><strong><font color="red">{$idx - 1} {__("times")}</font></strong></td>
                        </tr>
                    {/if}
                    {if $history|count == 0}
                        <tr class="odd">
                            <td valign="top" align="center" colspan="4" class="dataTables_empty">
                                {__("No data available in table")}
                            </td>
                        </tr>
                    {/if}
                </tbody>
            {else}
                {*Đối với trường hợp tìm cả trường*}
                <thead>
                <tr>
                    <th>#</th>
                    <th>{__("Full name")}</th>
                    <th>{__("Using time")}</th>
                    <th>{__("Registrar")}</th>
                    <th>{__("Registered time")}</th>
                </tr>
                </thead>
                <tbody>
                {$idx = 1}
                {$classTotal = 0}
                {$classId = -1}
                {foreach $history as $row}
                    {if $row['class_id'] != $classId}
                        {if $idx > 1}
                            <tr>
                                <td colspan="2" align="center"><strong><font color="blue">{__("Total of class")}</font></strong></td>
                                <td colspan="3" align="left"><strong><font color="blue">{$classTotal} {__("times")}</font></strong></td>
                            </tr>
                        {/if}
                        <tr>
                            <td colspan="4"><strong>{$row['group_title']}</strong></</td>
                        </tr>
                        {$classTotal = 0}
                    {/if}
                    <tr>
                        <td align="center">{$idx}</td>
                        <td>{$row['child_name']}</td>
                        <td>{$row['using_at']}</td>
                        <td>{$row['user_fullname']}</td>
                        <td>{$row['recorded_at']}</td>
                    </tr>
                    {$classId = $row['class_id']}
                    {$classTotal = $classTotal + 1}
                    {$idx = $idx + 1}
                {/foreach}
                {if $idx > 2}
                    <tr>
                        <td colspan="2" align="center"><strong><font color="blue">{__("Total of class")}</font></strong></td>
                        <td colspan="3" align="left"><strong><font color="blue">{$classTotal} {__("times")}</font></strong></td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center"><strong><font color="red">{__("Total")|upper}</font></strong></td>
                        <td colspan="3" align="left"><strong><font color="red">{$idx - 1} {__("times")}</font></strong></td>
                    </tr>
                {/if}
                {if $history|count == 0}
                    <tr class="odd">
                        <td valign="top" align="center" colspan="5" class="dataTables_empty">
                            {__("No data available in table")}
                        </td>
                    </tr>
                {/if}
                </tbody>
            {/if}
        </table>
    {elseif $service_type > 0}
        <div><strong>{__("Student list")}&nbsp;({__("Register")}: {$history|count})</strong></div>
        <table class="table table-striped table-bordered table-hover">
            <thead>
            <tr>
                <th>#</th>
                <th>{__("Full name")}</th>
                <th>{__("Student code")}</th>
                <th>{__("Registration time")}</th>
            </tr>
            </thead>
            <tbody>
            {$classId = -1}
            {$idx = 1}
            {foreach $history as $row}
                {if (($class_id == 0) && ($classId != $row['class_id']))}
                    <tr>
                        <td colspan="4"><strong>{$row['group_title']}</strong></td>
                    </tr>
                {/if}
                <tr>
                    <td align="center">{$idx}</td>
                    <td>{$row['child_name']}</td>
                    <td>{$row['child_code']}</td>
                    <td>
                        {if $row['begin'] != ''}
                            {$row['begin']}&nbsp;-&nbsp;{$row['end']}
                        {/if}
                    </td>
                </tr>
                {$classId = $row['class_id']}
                {$idx = $idx + 1}
            {/foreach}
            {if $history|count == 0}
                <tr class="odd">
                    <td valign="top" align="center" colspan="4" class="dataTables_empty">
                        {__("No data available in table")}
                    </td>
                </tr>
            {/if}
            </tbody>
        </table>
    {else}
        {*Đối với trường hợp tìm thông tin sử dụng TẤT CẢ dịch vụ của một trẻ*}
        {*Danh sách dịch vụ sử dụng theo THÁNG và theo ĐIỂM DANH*}
        <div><strong>{__("Monthly and daily services")|upper}</strong></div>
        <table class="table table-striped table-bordered table-hover">
            <thead>
            <tr>
                <th>#</th>
                <th>{__("Service name")}</th>
                <th>{__("Time")}</th>
            </tr>
            </thead>
            <tbody>
            {$idx = 1}
            {foreach $ncbs_usage as $row}
                <tr>
                    <td align="center">{$idx}</td>
                    <td>{$row['service_name']}</td>
                    <td>
                        {if $row['begin'] != ''}
                            {$row['begin']}&nbsp;-&nbsp;{$row['end']}
                        {/if}
                    </td>
                </tr>
                {$idx = $idx + 1}
            {/foreach}
            {if $ncbs_usage|count == 0}
                <tr class="odd">
                    <td valign="top" align="center" colspan="3" class="dataTables_empty">
                        {__("No data available in table")}
                    </td>
                </tr>
            {/if}
            </tbody>
        </table>

        {*Bảng dịch vụ theo SỐ LẦN*}
        <div><strong>{__("Count-based service")|upper}&nbsp;({$cbs_usage|count} {__("times")})</strong></div>
        <table class="table table-striped table-bordered table-hover">
            <thead>
            <tr>
                <th>#</th>
                <th>{__("Using time")}</th>
                <th>{__("Registrar")}</th>
                <th>{__("Registered time")}</th>
            </tr>
            </thead>
            <tbody>
            {$idx = 1}
            {$serviceTotal = 0}
            {$serviceId = -1}
            {foreach $cbs_usage as $row}
                {if $row['service_id'] != $serviceId}
                    {if $idx > 1}
                        <tr>
                            <td colspan="2" align="center"><strong><font color="blue">{__("Total of service")}</font></strong></td>
                            <td colspan="2" align="left"><strong><font color="blue">{$serviceTotal} {__("times")}</font></strong></td>
                        </tr>
                    {/if}
                    <tr>
                        <td colspan="4"><strong>{$row['service_name']}</strong></</td>
                    </tr>
                    {$serviceTotal = 0}
                {/if}
                <tr>
                    <td align="center">{$idx}</td>
                    <td>{$row['using_at']}</td>
                    <td>{$row['user_fullname']}</td>
                    <td>{$row['recorded_at']}</td>
                </tr>
                {$serviceId = $row['service_id']}
                {$serviceTotal = $serviceTotal + 1}
                {$idx = $idx + 1}
            {/foreach}
            {if $idx > 2}
                <tr>
                    <td colspan="2" align="center"><strong><font color="blue">{__("Total of service")}</font></strong></td>
                    <td colspan="2" align="left"><strong><font color="blue">{$serviceTotal} {__("times")}</font></strong></td>
                </tr>
                <tr>
                    <td colspan="2" align="center"><strong><font color="red">{__("Total")|upper}</font></strong></td>
                    <td colspan="2" align="left"><strong><font color="red">{$idx - 1} {__("times")}</font></strong></td>
                </tr>
            {/if}
            {if $cbs_usage|count == 0}
                <tr class="odd">
                    <td valign="top" align="center" colspan="4" class="dataTables_empty">
                        {__("No data available in table")}
                    </td>
                </tr>
            {/if}
            </tbody>
        </table>
    {/if}
{else}
    <div><strong>{__("Summary of service registration")}</strong></div>
    <table class="table table-striped table-bordered table-hover">
        <thead>
        <tr>
            <th>#</th>
            <th>{__("Service")}</th>
            <th>{__("Total")}</th>
        </tr>
        </thead>
        <tbody>
        {$idx = 1}
        {foreach $history as $row}
            <tr>
                <td align="center">{$idx}</td>
                <td>{$row['service_name']}</td>
                <td align="center">{$row['cnt']}</td>
            </tr>
            {$idx = $idx + 1}
        {/foreach}
        {if $history|count == 0}
            <tr class="odd">
                <td valign="top" align="center" colspan="3" class="dataTables_empty">
                    {__("No data available in table")}
                </td>
            </tr>
        {/if}
        </tbody>
    </table>
{/if}