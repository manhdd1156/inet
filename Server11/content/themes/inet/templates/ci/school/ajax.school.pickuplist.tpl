<table class="table table-striped table-bordered">
    <thead>
    <tr>
        <th width="7%">#</th>
        <th width="15%">{__('Time')}</th>
        <th width="30%">{__('Assigned teachers')}</th>
        <th width="15%">{__('Class')}</th>
        <th width="10%">{__('Number of student')}</th>
        <th width="10%">{__('Total amount')}</th>
        <th width="13%">{__("Actions")}</th>
    </tr>
    </thead>
    <tbody>
    {$idx = 1}
    {foreach $pickups as $pickup_class}
        {$classCnt = $pickup_class|count}
        {$firstData = array_values($pickup_class)}

        <tr>
            <td align="center" style="vertical-align:middle" rowspan="{$classCnt}"><strong>{$idx}</strong></td>
            <td align="center" style="vertical-align:middle" rowspan="{$classCnt}">
                <strong>{$firstData[0]['pickup_time']}</strong><br>
                {__($firstData[0]['pickup_day'])}
            </td>
                {foreach $pickup_class as $pickup}

                    <td align="center" style="vertical-align:middle"  rowspan="1">
                        {if $pickup['pickup_assign']|count == 0 }
                            <strong>&#150;</strong>
                        {else}
                            {foreach $pickup['pickup_assign'] as $assign}
                                <label class="col-sm-2"></label>
                                <strong>
                                    <label class="col-sm-9 text-left">&#9679;&#09;{$assign['user_fullname']}</label>
                                </strong>
                            {/foreach}
                        {/if}

                    </td>

                    <td align="center" style="vertical-align:middle"  rowspan="1">
                        {if !is_empty({$pickup['class_name']})}
                            <strong>{$pickup['class_name']}</strong>
                        {else}
                            <strong>&#150;</strong>
                        {/if}
                    </td>

                    <td align="center" style="vertical-align:middle" rowspan="1">
                        {if $pickup['total_child'] > 0}
                            <font color="blue"><strong>{$pickup['total_child']}</strong></font>
                        {else}
                            <font color="#ff4500"><strong>{$pickup['total_child']}</strong></font>
                        {/if}

                    </td>
                    <td align="center" style="vertical-align:middle" rowspan="1">
                        {if $pickup['total_child'] > 0}
                            <font color="blue"><strong>{moneyFormat($pickup['total'])}</strong></font>
                        {else}
                            <font color="#ff4500"><strong>{moneyFormat($pickup['total'])}</strong></font>
                        {/if}
                    </td>
                    <td align="center" nowrap="true" style="vertical-align:middle" rowspan="1">
                        {if $pickup['action'] == 0}
                            <i class="fa fa-times" style="color:black" title="{__("No information")}" aria-hidden="true"></i>
                        {elseif $pickup['action'] == 1}
                            <a href="{$system['system_url']}/school/{$username}/pickup/detail/{$pickup['pickup_id']}"
                               class="btn btn-xs btn-default">
                                {__("Edit")}
                            </a>
                        {elseif $canEdit && ($pickup['action'] == 2)}
                            <a class="btn btn-xs btn-default js_school-pickup_edit"
                               data-username="{$username}" data-time="{$pickup['pickup_time']}" data-class="{$pickup['pickup_class_id']}">
                                {__("Add child")}
                            </a>
                        {/if}
                    </td>
                    </tr>
                {/foreach}
        {$idx = $idx + 1}
    {/foreach}
    </tbody>
</table>
