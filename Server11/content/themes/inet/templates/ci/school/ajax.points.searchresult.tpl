{if $rows|count > 0}
    {*<div style="z-index: 2">*}
    {*<div><strong>{__("The whole class attendance")}</strong></div>*}
    {*<div id="table_button">*}
    {*<button class="left">&larr;</button>*}
    {*<button class="right">&rarr;</button>*}
    {*</div>*}
    {*</div>*}
    <div class="table-responsive" id="example" style="overflow-x: scroll;">
        {*        {if $grade == 2}*}
        {if $search_with == 'search_with_subject' }
            {if $semester == 0}
                <table class="table table-striped table-bordered table-pinned" style="z-index: 1;">
                    <thead>
                    <tr bgcolor="#fff">
                        <th rowspan="2" class="pinned" style="padding: 8px 6px" id="width_no">#</th>
                        <th rowspan="2" nowrap="true" class="pinned"
                            style="padding: 8px 6px">{__("Student name")}</th>
                        <th rowspan="2" nowrap="true"
                            style="padding: 8px 6px">{__("Student code")}</th>
                        <th colspan="{if $score_fomula=='vn'}{$column_hk1+$column_gk1+2}{else}4{/if}">{__("Semester 1")}</th>
                        <th colspan="{if $score_fomula=='vn'}{$column_hk2+$column_gk2+2}{else}4{/if}">{__("Semester 2")}</th>
                        {if $score_fomula=='vn'}
                            <th rowspan="2">{__("Avg year")}</th>
                            <th rowspan="2">{__("Re exam")}</th>
                        {else}
                        <th colspan="2">{__("Average semesterly")}</th>
                            <th colspan="2">{__("End semester")}</th>
                        <th rowspan="2">{__("End year")}</th>
                        <th rowspan="2">{__("Re exam")}</th>
                        <th rowspan="2">{__("Without permission")}</th>
                        <th rowspan="2">{__("With permission")}</th>
                        <th rowspan="2">{__("Status")}</th>
                        {/if}
                    </tr>
                    <tr>
                        {for $i=1;$i<=$column_hk1;$i++}
                            <th>{if $score_fomula=='vn'}tx{else}M{$i}{/if}</th>
                        {/for}
                        {for $i=0;$i<$column_gk1;$i++}
                            <th>{if $score_fomula=='vn'}gk{else}{__("Last")}{/if}</th>
                        {/for}
                        {if $score_fomula=='vn'}
                            <th>ck</th>
                            <th>{__("Average")}</th>
                        {/if}
                        {for $i=1;$i<=$column_hk2;$i++}
                            <th>{if $score_fomula=='vn'}tx{else}M{$i}{/if}</th>
                        {/for}
                        {for $i=0;$i<$column_gk2;$i++}
                            <th>{if $score_fomula=='vn'}gk{else}{__("Last")}{/if}</th>
                        {/for}
                        {if $score_fomula=='vn'}
                            <th>ck</th>
                            <th>{__("Average")}</th>
                        {/if}
                        {if $score_fomula!='vn'}
                            <th>S1</th>
                            <th>S2</th>
                        {/if}
                    </tr>
                    </thead>
                    <tbody>
                    {$rowIdx = 1}
                    {foreach $rows as $k => $row}
                        <tr>
                            <td align="center" class="pinned">{$rowIdx}</td>
                            <td nowrap="true" class="pinned text-bold color-blue">
                                <strong>{$row['child_lastname']} {$row['child_firstname']}</strong></td>
                            <td nowrap="true" class="text-bold color-blue"><a
                                        href="{$system['system_url']}/school/{$username}/children/detail/{$row['child_id']}"><strong>{$row['child_code']}</strong></a>
                            </td>
                            {$rowIdx = $rowIdx + 1}
                            {foreach $subject_key as $key}
                                <td>{$row[$key]}</td>
                            {/foreach}
                        </tr>
                        {*                            *}{*Sau 15 trẻ thì thêm header cho dễ nhìn*}
                        {*                            {if $rowIdx % 10 == 0}*}
                        {*                                <tr bgcolor="#fff">*}
                        {*                                    <th class="pinned" style="padding: 8px 6px" id="width_no">#</th>*}
                        {*                                    <th nowrap="true" class="pinned" style="padding: 8px 6px">{__("Child code")}</th>*}
                        {*                                    <th nowrap="true" class="pinned" style="padding: 8px 6px">{__("Student name")}</th>*}
                        {*                                    <th nowrap="true" class="pinned" style="padding: 8px 6px">{__("Student name")}</th>*}
                        {*                                    <th>{__("TBHK1")}</th>*}
                        {*                                    <th>{__("TBHK2")}</th>*}
                        {*                                    <th>{__("TB Cả năm")}</th>*}
                        {*                                    <th>{__("Điểm thi lại")}</th>*}
                        {*                                </tr>*}
                        {*                            {/if}*}
                    {/foreach}
                    </tbody>
                </table>
            {else}
                <table class="table table-striped table-bordered table-pinned" style="z-index: 1">
                    <thead>
                    <tr bgcolor="#fff">
                        <th rowspan="2" class="pinned" style="padding: 8px 6px" id="width_no">#</th>

                        <th rowspan="2" nowrap="true" class="pinned"
                            style="padding: 8px 6px">{__("Student name")}</th>
                        <th rowspan="2" nowrap="true" class="pinned"
                            style="padding: 8px 6px">{__("Student code")}</th>
                        {*                        /* UPDATE START - ManhDD 09/06/2021 */*}
                        <th colspan="{if $score_fomula=='vn'}{$column_hk+$column_gk+2}{else}4{/if}">{__("Semester")}</th>
                        {*                        <th colspan="3">{__("Mouth")}</th>*}
                        {*                        <th colspan="3">{__("15 Minutes")}</th>*}
                        {*                        <th colspan="8">{__("1 Tiết")}</th>*}
                        {*                            <th rowspan="2">{__("Học kỳ")}</th>*}
                        {*                        <th rowspan="2">{__("Điểm TBHK")}</th>*}
                        {*                        /* UPDATE START - ManhDD 09/06/2021 */*}
                    </tr>
                    <tr>
                        {for $i=1;$i<=$column_hk;$i++}
                            <th>{if $score_fomula=='vn'}tx{else}M{$i}{/if}</th>
                        {/for}
                        {for $i=0;$i<$column_gk;$i++}
                            <th>{if $score_fomula=='vn'}gk{else}{__("Last")}{/if}</th>
                        {/for}
                        {if $score_fomula=='vn'}
                            <th>ck</th>
                            <th>{__("Average")}</th>
                        {/if}

                    </tr>
                    </thead>
                    <tbody>
                    {$rowIdx = 1}
                    {foreach $rows as $k => $row}
                        <tr>
                            <td align="center" class="pinned">{$rowIdx}</td>
                            <td nowrap="true" class="pinned text-bold color-blue">
                                <strong>{$row['child_lastname']} {$row['child_firstname']}</strong></td>
                            <td nowrap="true" class="pinned text-bold color-blue"><a
                                        href="{$system['system_url']}/school/{$username}/children/detail/{$row['child_id']}"><strong>{$row['child_code']}</strong></a>
                            </td>
                            {$rowIdx = $rowIdx + 1}
                            {foreach $subject_key as $key}
                                <td>{$row[$key]}</td>
                            {/foreach}
                        </tr>
                        {*Sau 15 trẻ thì thêm header cho dễ nhìn*}
                        {if $rowIdx % 10 == 0}
                            <tr bgcolor="#fff">
                                <th rowspan="2" class="pinned" style="padding: 8px 6px" id="width_no">#</th>
                                <th rowspan="2" nowrap="true" class="pinned"
                                    style="padding: 8px 6px">{__("Student name")}</th>
                                <th rowspan="2" nowrap="true" class="pinned"
                                    style="padding: 8px 6px">{__("Student code")}</th>
                                <th colspan="{if $score_fomula=='vn'}{$column_hk+$column_gk+2}{else}4{/if}">{__("Semester")}</th>
                            </tr>
                            <tr>
                                {for $i=1;$i<=$column_hk;$i++}
                                    <th>{if $score_fomula=='vn'}tx{else}M{$i}{/if}</th>
                                {/for}
                                {for $i=0;$i<$column_gk;$i++}
                                    <th>{if $score_fomula=='vn'}gk{else}{__("Last")}{/if}</th>
                                {/for}
                                {if $score_fomula=='vn'}
                                    <th>ck</th>
                                    <th>{__("Average")}</th>
                                {/if}
                            </tr>
                        {/if}
                    {/foreach}
                    </tbody>
                </table>
            {/if}
        {elseif $search_with == 'search_with_student' }
            <strong style="float: right">{__("Status ")} :
                {if $status == 'Pass' }
                    <strong style="color:lawngreen">{__({$status})}</strong>
                {elseif $status == 'Fail'}
                    <strong style="color:red">{__({$status})}</strong>
                {elseif $status == 'Re-exam'}
                    <strong style="color:orange">{__({$status})}</strong>
                {else}
                    <strong>{__({$status})}</strong>
                {/if}

            </strong>
            <table class="table table-striped table-bordered" style="z-index: 1">
            <thead>
            <tr bgcolor="#fff">
                <th rowspan="2" class="pinned" style="padding: 8px 6px" id="width_no">#</th>
                <th rowspan="2" nowrap="true" class="pinned"
                    style="padding: 8px 6px">{__("Subject name")}</th>
                <th colspan="{if $score_fomula=='vn'}{$column_hk1+$column_gk1+2}{else}4{/if}">{__("Semester 1")}</th>
                <th colspan="{if $score_fomula=='vn'}{$column_hk2+$column_gk2+2}{else}4{/if}">{__("Semester 2")}</th>
            </tr>
            <tr>
                {for $i=1;$i<=$column_hk1;$i++}
                    <th>{if $score_fomula=='vn'}tx{else}M{$i}{/if}</th>
                {/for}
                {for $i=0;$i<$column_gk1;$i++}
                    <th>{if $score_fomula=='vn'}gk{else}D1{/if}</th>
                {/for}
                {if $score_fomula=='vn'}
                    <th>ck</th>
                    <th>{__("Average")}</th>
                {/if}
                {for $i=1;$i<=$column_hk2;$i++}
                    <th>{if $score_fomula=='vn'}tx{else}M{$i}{/if}</th>
                {/for}
                {for $i=0;$i<$column_gk2;$i++}
                    <th>{if $score_fomula=='vn'}gk{else}D2{/if}</th>
                {/for}
                {if $score_fomula=='vn'}
                    <th>ck</th>
                    <th>{__("Average")}</th>
                    <th>{__("Re exam")}</th>
                {/if}
            </tr>
            </thead>
            <tbody>
            {$rowIdx = 1}
            {foreach $rows as $k => $row}
                <tr>
                    <td align="center" class="pinned">{$rowIdx}</td>

                    <td nowrap="true" class="pinned text-bold color-blue">
                        <strong>{$row['subject_name']}</strong>
                    </td>
                    {$rowIdx = $rowIdx + 1}
                    {foreach $subject_key as $key}
                        <td>{$row[strtolower($key)]}</td>
                    {/foreach}
                </tr>
            {/foreach}
            {*                    Các điểm trung bình*}
            {if $score_fomula == 'vn'}
                <tr>
                    <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                        <strong>{__("End Semester")}</strong>
                    </td>
                    <td colspan="{$column_hk1+$column_gk1+1}"></td>
                    <td>{number_format($tb_total_hk1 ,2)}</td>
                    <td colspan="{$column_hk2+$column_gk2+1}"></td>
                    <td>{number_format($tb_total_hk2 ,2)}</td>
                    <td colspan="1"></td>
                </tr>
                {*                    kết thúc năm *}
                <tr>
                    <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                        <strong>{__("End year")}</strong>
                    </td>
                    <td colspan="{$column_hk1 + $column_gk1 + 2 + $column_hk2 + $column_gk2 + 2 + 1}"
                        style="text-align: center">{number_format($tb_total_year ,2)}</td>
                </tr>
                {*                    Nghỉ có phép*}
                <tr>
                    <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                        <strong>{__("Absent with permission")}</strong>
                    </td>
                    <td colspan="{$column_hk1 + $column_gk1 + 2 + $column_hk2 + $column_gk2 + 2 + 1}"
                        style="text-align: center">{$child_absent['absent_true']}</td>
                </tr>
                {*                    nghỉ không phép*}
                <tr>
                    <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                        <strong>{__("Absent without permission")}</strong>
                    </td>
                    <td colspan="{$column_hk1 + $column_gk1 + 2 + $column_hk2 + $column_gk2 + 2 + 1}"
                        style="text-align: center">{$child_absent['absent_false']}</td>
                </tr>
                </tbody>
                </table>

            {elseif $score_fomula=='km'}
                <tr>
                    <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                        <strong>{__("Average monthly")}</strong>
                    </td>
                    <td>{number_format($children_point_avgs['a1'] ,2)}</td>
                    <td>{number_format($children_point_avgs['b1'] ,2)}</td>
                    <td>{number_format($children_point_avgs['c1'] ,2)}</td>
                    <td>{number_format($children_point_avgs['d1'] ,2)}</td>
                    <td>{number_format($children_point_avgs['a2'] ,2)}</td>
                    <td>{number_format($children_point_avgs['b2'] ,2)}</td>
                    <td>{number_format($children_point_avgs['c2'] ,2)}</td>
                    <td>{number_format($children_point_avgs['d2'] ,2)}</td>

                </tr>
                <tr>
                    <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                        <strong>{__("Average semesterly")}</strong>
                    </td>
                    <td colspan="3" style="text-align: center">{number_format($children_point_avgs['x1'] ,2)}</td>
                    <td></td>
                    <td colspan="3" style="text-align: center">{number_format($children_point_avgs['x2'] ,2)}</td>
                    <td></td>
                </tr>
                {*                    kết thúc kỳ*}
                <tr>
                    <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                        <strong>{__("End semester")}</strong>
                    </td>
                    <td colspan="4" style="text-align: center">{number_format($children_point_avgs['e1'] ,2)}</td>
                    <td colspan="4" style="text-align: center">{number_format($children_point_avgs['e2'] ,2)}</td>
                </tr>
                {*                    kết thúc năm *}
                <tr>
                    <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                        <strong>{__("End year")}</strong>
                    </td>
                    <td colspan="8" style="text-align: center">{number_format($children_point_avgs['y'] ,2)}</td>
                </tr>
                {*                    Nghỉ có phép*}
                <tr>
                    <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                        <strong>{__("Absent has permission")}</strong>
                    </td>
                    <td colspan="8" style="text-align: center">{$child_absent['absent_true']}</td>
                </tr>
                {*                    nghỉ không phép*}
                <tr>
                    <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                        <strong>{__("Absent without permission")}</strong>
                    </td>
                    <td colspan="8" style="text-align: center">{$child_absent['absent_false']}</td>
                </tr>
                </tbody>
                </table>
                <strong>{__("Re-Exam")}</strong>
                <table class="table table-striped table-bordered" style="z-index: 1;">
                    <thead>
                    <tr bgcolor="#fff">
                        <th rowspan="2" class="pinned" style="padding: 8px 6px" id="width_no">#</th>
                        <th rowspan="2" nowrap="true" class="pinned"
                            style="padding: 8px 6px">{__("Subject name")}
                        </th>
                        <th colspan="1">{__("Point")}</th>
                    </tr>
                    </thead>
                    <tbody>
                    {$rowIdx = 1}
                    {foreach $children_subject_reexams as $k => $row}
                        <tr>
                            <td align="center" class="pinned">{$rowIdx}</td>
                            <td nowrap="true" class="pinned text-bold color-blue">
                                <strong>{$row['name']}</strong>
                            </td>
                            <td style="text-align: center">{$row['point']}</td>
                            {$rowIdx = $rowIdx + 1}
                        </tr>
                    {/foreach}
                    <tr>
                        <td colspan="2" nowrap="true" class="pinned text-bold color-blue"
                            style="text-align: center">
                            <strong>{__("Result Re-exam")}</strong>
                        </td>
                        <td colspan="3" style="text-align: center">{number_format($result_exam ,2)}</td>
                    </tr>
                    </tbody>
                </table>
            {/if}
        {elseif $search_with == 'showDataImport' }
            {if $semester == 0}
                <table class="table table-striped table-bordered table-pinned" style="z-index: 1">
                    <thead>
                    <tr bgcolor="#fff">
                        <th rowspan="2" class="pinned" style="padding: 8px 6px" id="width_no">#</th>
                        <th rowspan="2" nowrap="true" class="pinned"
                            style="padding: 8px 6px">{__("Student name")}</th>
                        <th rowspan="2" nowrap="true"
                            style="padding: 8px 6px">{__("Student code")}</th>

                        {*                            <th rowspan="2" nowrap="true" class="pinned"*}
                        {*                                style="padding: 8px 6px">{__("Student name")}</th>*}
                        <th colspan="{if $score_fomula=='vn'}{$column_hk1+$column_gk1+1}{else}4{/if}">{__("Semester 1")}</th>
                        <th colspan="{if $score_fomula=='vn'}{$column_hk2+$column_gk2+1}{else}4{/if}">{__("Semester 2")}</th>
                        <th rowspan="2">{__("Re exam")}</th>
                    </tr>
                    <tr>
                        {for $i=1;$i<=$column_hk1;$i++}
                            <th>{if $score_fomula=='vn'}tx{else}M{$i}{/if}</th>
                        {/for}
                        {for $i=0;$i<$column_gk1;$i++}
                            <th>{if $score_fomula=='vn'}gk{else}{__("Last")}{/if}</th>
                        {/for}
                        {if $score_fomula=='vn'}
                            <th>ck</th>
                        {/if}
                        {for $i=1;$i<=$column_hk2;$i++}
                            <th>{if $score_fomula=='vn'}tx{else}M{$i}{/if}</th>
                        {/for}
                        {for $i=0;$i<$column_gk2;$i++}
                            <th>{if $score_fomula=='vn'}gk{else}D2{/if}</th>
                        {/for}
                        {if $score_fomula=='vn'}
                            <th>ck</th>
                        {/if}
                    </tr>
                    </thead>
                    <tbody>
                    {$rowIdx = 1}
                    {foreach $rows as $k => $row}
                        <tr>
                            <td align="center" class="pinned">{$rowIdx}</td>
                            <td nowrap="true" class="pinned text-bold color-blue">
                                <strong>{$row['child_lastname']} {$row['child_firstname']}</strong></td>
                            <td nowrap="true" class="text-bold color-blue"><a
                                        href="{$system['system_url']}/school/{$username}/children/detail/{$row['child_id']}"><strong>{$row['child_code']}</strong></a>
                            </td>

                            {*                                <td nowrap="true" class="pinned text-bold color-blue">*}
                            {*                                    <strong>{$row['child_firstname']}</strong></td>*}
                            {$rowIdx = $rowIdx + 1}
                            {foreach $subject_key as $key}
                                {*                                    <td>{$row[$key]}</td>*}
                                {if $key=='re_exam' && !$row['is_reexam'] }
                                    <td>{$row[$key]}</td>
                                {else}
                                    <td><input name="point" type="number" min="0" step="0.01" style="max-width: 50px"
                                               value="{$row[$key]}"/></td>
                                {/if}
                            {/foreach}
                        </tr>
                        {*                            *}{*Sau 15 trẻ thì thêm header cho dễ nhìn*}
                        {*                            {if $rowIdx % 10 == 0}*}
                        {*                                <tr bgcolor="#fff">*}
                        {*                                    <th class="pinned" style="padding: 8px 6px" id="width_no">#</th>*}
                        {*                                    <th nowrap="true" class="pinned" style="padding: 8px 6px">{__("Child code")}</th>*}
                        {*                                    <th nowrap="true" class="pinned" style="padding: 8px 6px">{__("Student name")}</th>*}
                        {*                                    <th nowrap="true" class="pinned" style="padding: 8px 6px">{__("Student name")}</th>*}
                        {*                                    <th>{__("TBHK1")}</th>*}
                        {*                                    <th>{__("TBHK2")}</th>*}
                        {*                                    <th>{__("TB Cả năm")}</th>*}
                        {*                                    <th>{__("Điểm thi lại")}</th>*}
                        {*                                </tr>*}
                        {*                            {/if}*}
                    {/foreach}
                    </tbody>
                </table>
            {else}
                <table class="table table-striped table-bordered table-pinned" style="z-index: 1">
                    <thead>
                    <tr bgcolor="#fff">
                        <th rowspan="2" class="pinned" style="padding: 8px 6px" id="width_no">#</th>
                        <th rowspan="2" nowrap="true" class="pinned"
                            style="padding: 8px 6px">{__("Student name")}</th>
                        <th rowspan="2" nowrap="true" class="pinned"
                            style="padding: 8px 6px">{__("Student code")}</th>
                        {*                        /* UPDATE START - ManhDD 06/04/2021 */*}
                        <th colspan="{if $score_fomula=='vn'}{$column_hk1+$column_gk1+1}{else}4{/if}">{__("Semester")}</th>
                        {*                        <th colspan="3">{__("Mouth")}</th>*}
                        {*                        <th colspan="3">{__("15 Minutes")}</th>*}
                        {*                        <th colspan="8">{__("1 Tiết")}</th>*}
                        {*                            <th rowspan="2">{__("Học kỳ")}</th>*}
                        {*                        <th rowspan="2">{__("Điểm TBHK")}</th>*}
                        {*                        /* UPDATE START - ManhDD 06/04/2021 */*}
                    </tr>
                    <tr>
                        {for $i=1;$i<=$column_hk1;$i++}
                            <th>{if $score_fomula=='vn'}tx{else}M{$i}{/if}</th>
                        {/for}
                        {for $i=0;$i<$column_gk1;$i++}
                            <th>{if $score_fomula=='vn'}gk{else}{__("Last")}{/if}</th>
                        {/for}
                        {if $score_fomula=='vn'}
                            <th>ck</th>
                        {/if}
                        {*                            <th>M1</th>*}
                        {*                            <th>M2</th>*}
                        {*                            <th>M3</th>*}
                        {*                            <th>{__("Last")}</th>*}
                    </tr>
                    </thead>
                    <tbody>
                    {$rowIdx = 1}
                    {foreach $rows as $k => $row}
                        <tr>
                            <td align="center" class="pinned">{$rowIdx}</td>
                            <td nowrap="true" class="pinned text-bold color-blue">
                                <strong>{$row['child_lastname']} {$row['child_firstname']}</strong></td>
                            <td nowrap="true" class="pinned text-bold color-blue"><a
                                        href="{$system['system_url']}/school/{$username}/children/detail/{$row['child_id']}"><strong>{$row['child_code']}</strong></a>
                            </td>
                            {$rowIdx = $rowIdx + 1}
                            {foreach $subject_key as $key}
                                {*                                    <td>{$row[$key]}</td>*}
                                <td><input type="number" style="max-width: 50px" min="0" step="0.01"
                                           value="{$row[$key]}"/></td>
                            {/foreach}
                        </tr>
                        {*Sau 15 trẻ thì thêm header cho dễ nhìn*}
                        {if $rowIdx % 10 == 0}
                            <tr bgcolor="#fff">
                                <th rowspan="2" class="pinned" style="padding: 8px 6px" id="width_no">#</th>
                                <th rowspan="2" nowrap="true" class="pinned"
                                    style="padding: 8px 6px">{__("Student name")}</th>
                                <th rowspan="2" nowrap="true" class="pinned"
                                    style="padding: 8px 6px">{__("Student code")}</th>
                                {*                        /* UPDATE START - ManhDD 06/04/2021 */*}
                                <th colspan="{if $score_fomula=='vn'}{$column_hk1+$column_gk1+1}{else}4{/if}">{__("Semester")}</th>
                                {*                        <th colspan="3">{__("Mouth")}</th>*}
                                {*                        <th colspan="3">{__("15 Minutes")}</th>*}
                                {*                        <th colspan="8">{__("1 Tiết")}</th>*}
                                {*                            <th rowspan="2">{__("Học kỳ")}</th>*}
                                {*                        <th rowspan="2">{__("Điểm TBHK")}</th>*}
                                {*                        /* UPDATE START - ManhDD 06/04/2021 */*}
                            </tr>
                            <tr>
                                {for $i=1;$i<=$column_hk1;$i++}
                                    <th>{if $score_fomula=='vn'}tx{else}M{$i}{/if}</th>
                                {/for}
                                {for $i=0;$i<$column_gk1;$i++}
                                    <th>{if $score_fomula=='vn'}gk{else}{__("Last")}{/if}</th>
                                {/for}
                                {if $score_fomula=='vn'}
                                    <th>ck</th>
                                {/if}
                                {*                            <th>M1</th>*}
                                {*                            <th>M2</th>*}
                                {*                            <th>M3</th>*}
                                {*                            <th>{__("Last")}</th>*}
                            </tr>
                        {/if}
                    {/foreach}
                    </tbody>
                </table>
            {/if}
        {/if}
        {*        {elseif $grade == 1}*}
        {*            <table class="table table-striped table-bordered table-pinned" style="z-index: 1">*}
        {*                <thead>*}
        {*                <tr bgcolor="#fff">*}
        {*                    <th class="pinned" style="padding: 8px 6px" id="width_no">#</th>*}
        {*                    <th nowrap="true" class="pinned" style="padding: 8px 6px">{__("Student code")}</th>*}
        {*                    <th nowrap="true" class="pinned" style="padding: 8px 6px">{__("Last name")}</th>*}
        {*                    <th nowrap="true" class="pinned" style="padding: 8px 6px">{__("First name")}</th>*}
        {*                    <th>{__("Nhận xét")}</th>*}
        {*                    <th>{__("Năng lực")}</th>*}
        {*                    <th>{__("Phẩm chất")}</th>*}
        {*                    <th>{__("KTCK")}</th>*}
        {*                    <th>{__("XLCK")}</th>*}
        {*                </tr>*}
        {*                </thead>*}
        {*                <tbody>*}
        {*                {$rowIdx = 1}*}
        {*                {foreach $rows as $k => $row}*}
        {*                    <tr>*}
        {*                        <td align="center" class="pinned">{$rowIdx}</td>*}
        {*                        <td nowrap="true" class="pinned text-bold color-blue"><a*}
        {*                                    href="{$system['system_url']}/school/{$username}/children/detail/{$row['child_id']}"><strong>{$row['child_code']}</strong></a>*}
        {*                        </td>*}
        {*                        <td nowrap="true" class="pinned text-bold color-blue"><strong>{$row['child_lastname']}</strong>*}
        {*                        </td>*}
        {*                        <td nowrap="true" class="pinned text-bold color-blue"><strong>{$row['child_firstname']}</strong>*}
        {*                        </td>*}
        {*                        {$rowIdx = $rowIdx + 1}*}
        {*                        {foreach $subject_key as $key}*}
        {*                            <td>{$row[$key]}</td>*}
        {*                        {/foreach}*}
        {*                    </tr>*}
        {*                    *}{*Sau 10 trẻ thì thêm header cho dễ nhìn*}
        {*                    {if $rowIdx % 10 == 0}*}
        {*                        <tr bgcolor="#fff">*}
        {*                            <th class="pinned" style="padding: 8px 6px" id="width_no">#</th>*}
        {*                            <th nowrap="true" class="pinned" style="padding: 8px 6px">{__("Student code")}</th>*}
        {*                            <th nowrap="true" class="pinned" style="padding: 8px 6px">{__("Last name")}</th>*}
        {*                            <th nowrap="true" class="pinned" style="padding: 8px 6px">{__("First name")}</th>*}
        {*                            <th>{__("Nhận xét")}</th>*}
        {*                            <th>{__("Năng lực")}</th>*}
        {*                            <th>{__("Phẩm chất")}</th>*}
        {*                            <th>{__("KTCK")}</th>*}
        {*                            <th>{__("XLCK")}</th>*}
        {*                        </tr>*}
        {*                    {/if}*}
        {*                {/foreach}*}
        {*                </tbody>*}
        {*            </table>*}
        {*        {/if}*}
    </div>
{else}
    <div align="center"><strong style="color: red">{__("Chưa có thông tin điểm")}</strong></div>
{/if}
{*Jquery Cố định cột số thứ tự và họ tên*}
<script type="text/javascript">

    var $table = $('.table-pinned');
    var $fixedColumn = $table.clone().insertBefore($table).addClass('fixed-column');
    $fixedColumn.find('th').each(function (i, elem) {
        $(this).css("width",$table.find('th:eq(' + i + ')')[0].getBoundingClientRect().width);
    });
    $fixedColumn.find('td').each(function (i, elem) {
        $(this).css("width",$table.find('td:eq(' + i + ')')[0].getBoundingClientRect().width);
    });
    $fixedColumn.find('th,td').not('.pinned').hide();
    $fixedColumn.find('[id]').each(function () {
        $(this).removeAttr('id');
    });

    $fixedColumn.find('tr').each(function (i, elem) {
        $(this).css("height",$table.find('tr:eq(' + i + ')')[0].getBoundingClientRect().height);
    });

    $(window).resize(function () {
        $fixedColumn.find('tr').each(function (i, elem) {
            $(this).css("height",$table.find('tr:eq(' + i + ')')[0].getBoundingClientRect().height);
        });
        $fixedColumn.find('td').each(function (i, elem) {
            $(this).addClass('white-space_nowrap');
            $(this).css("width",$table.find('td:eq(' + i + ')')[0].getBoundingClientRect().width);
        });
    });
    //$fixedColumn.find('td').addClass('white-space_nowrap');
    //    $("#right").on("click", function() {
    //        var leftPos = $('#example').scrollLeft();
    //        console.log(leftPos);
    //        $("#example").animate({
    //            scrollLeft: leftPos - 200
    //        }, 800);
    //    });
    $(document).on('click', '.js_point_gk-add', function (e) {
        e.preventDefault();
        var semester = $('#semester').val();
        var score_fomula = $("input[name='score_fomula']").val();
        var flag_added = false;
        var count_gk = 0;
        var index_insert_td = 0;
        var table = $("table tbody").eq(1); // màn hình bị chia thành 2 tbody, tbody 1 chứa những cột pined, tbody2 là cả table mình cần
        if (semester != '0') {
            table.find('tr').each(function (index, el) {
                var $tds = $(this).find('td');
                var th_length = $('tr:nth-child(2) th').length; // lấy số lượng cột điểm ( bị duplicate nên sẽ chia 2 để lấy tên cột cho chính xác )
                if (score_fomula == 'vn') {
                    for (i = Math.floor(th_length/2); i <= th_length; i++) {
                        if ($('tr:nth-child(2) th').eq(i).text() == 'gk') {
                            count_gk++;
                        } else if ($('tr:nth-child(2) th').eq(i).text() == 'ck') {
                            if (!flag_added) {
                                // vì table đang là 2 bảng chồng lên nhau nên phải thêm vào cả 2 để tăng length th_length cho đều
                                // thêm vào mặt trước
                                $('tr:nth-child(2) th').eq(i - 1).after('<th>gk</th>');
                                //thêm vào mặt sau
                                $('tr:nth-child(2) th').eq((i - 1)-Math.floor(th_length/2)).after('<th style=\"display:none;\">gk</th>');
                                // tăng độ dài cột semester
                                var cols =  $('tr:nth-child(1) th').eq(7).attr("colspan");
                                $('tr:nth-child(1) th').eq(7).attr("colspan",parseInt(cols) + 1);
                                flag_added = true;
                                index_insert_td = i - Math.floor(th_length/2)- 1 + 3;
                            }
                            if(count_gk == 2) {
                                count_gk =0;
                                $('.js_point_gk-add').prop('disabled', true);
                            }
                            $tds.eq(index_insert_td).after('<td><input name=\"point\" type=\"number\" min=\"0\" step=\"0.01\" style=\"max-width: 50px\"\n value=\"\"/></td>');
                            count_gk =0;
                            break;
                        }
                    }
                }
            });
            //ADD START MANHDD 19/06/2021 => fix lỗi khi add thêm col trong phần nhập điểm table (không pined ) thay đổi width mà table (pined) vẫn lấy giá trị width cũ
            resize_table();
            //ADD END MANHDD 19/06/2021
        }
    });
    $('.right').click(function (event) {
        var width_col = $('.table-pinned').find('td:eq(' + 4 + ')').width();
        var pos = $('#example').scrollLeft() + width_col + 100;
        return false;
        $('#example').scrollLeft(pos);
    });
    $('.left').click(function (event) {
        var width_col = $('.table-pinned').find('td:eq(' + 4 + ')').width();
        var pos = $('#example').scrollLeft() - width_col - 100;
        $('#example').scrollLeft(pos);
    });

    jQuery(function ($) {
        function fixDiv() {
            var $cache = $('#getFixed');
            var $button = $('#table_button');
            if ($(window).scrollTop() > 100) {
                $cache.css({
                    'position': 'fixed',
                    'top': '50px'
                });
                $cache.width($('#school_list_point').width() - 1);
            } else
                $cache.css({
                    'position': 'relative',
                    'top': 'auto'
                });

            if ($(window).scrollTop() > 100)
                $button.css({
                    'position': 'fixed',
                    'top': '90px'
                });
            else
                $button.css({
                    'position': 'relative',
                    'top': 'auto'
                });
        }

        $(window).scroll(fixDiv);
        fixDiv();
    });
</script>