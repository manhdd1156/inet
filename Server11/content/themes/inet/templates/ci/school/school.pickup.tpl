<div class="" style="position: relative">
    {if $school['school_step'] == SCHOOL_STEP_FINISH || $school['grade'] != 0}
        <div class="dashboard_fixed">
            <button class="btn btn-xs btn-default js_school-pickup-guide" data-username="{$username}"
                    data-id="{$user->_data["user_id"]}">
                <i class="fa fa-chevron-down {if $cookie}hidden{/if}" aria-hidden="true" id="guide_hidden"></i>
                <i class="fa fa-chevron-up {if !$cookie}hidden{/if}" aria-hidden="true" id="guide_show"></i>
            </button>
        </div>
        <div class="panel panel-default {if $cookie == 0}hidden{/if}" id="guide_pickup">
            <div class="guide_school">
                <div class="init_school text-left pad5 full_width">
                    <div class="full_width pl5">
                        <strong>{__("The execution's order guide")}:</strong>
                    </div>
                    <div class="pad5">
                        <strong>{__("Handlings with late pickup")}:</strong>
                        <a href="{$system['system_url']}/school/{$username}/pickup/template"
                           class="pad5 font12">1. {__("Setting and create price table")}</a>
                        <i class="fas fa-long-arrow-alt-right" aria-hidden="true"></i>
                        <a href="{$system['system_url']}/school/{$username}/pickup/assign"
                           class="pad5 font12">2. {__("Assign teacher")}</a>
                        <i class="fas fa-long-arrow-alt-right" aria-hidden="true"></i>
                        <a href="{$system['system_url']}/school/{$username}/pickup/list"
                           class="pad5 font12">3. {__("Lists")}</a>
                        <i class="fas fa-long-arrow-alt-right" aria-hidden="true"></i>
                        <a href="{$system['system_url']}/school/{$username}/pickup/child"
                           class="pad5 font12">4. {__("Detail one student")}</a>
                    </div>
                </div>
            </div>
        </div>
    {/if}
    <div class="panel panel-default">
        <div class="panel-heading with-icon">
            {if ($sub_view == "detail") && $canEdit}
                <div class="pull-right flip">
                    <a href="{$system['system_url']}/school/{$username}/pickup/add/{$pickup['pickup_id']}"
                       class="btn btn-default">
                        <i class="fa fa-plus"></i> {__("Add child")}
                    </a>
                </div>
            {/if}
            {if ($sub_view == "list") && $canEdit}
                <div class="pull-right flip">

                    <a href="{$system['system_url']}/school/{$username}/pickup/child" class="btn btn-default">
                        <i class="fa fa-history"></i> {__("Using late Pickup information")}
                    </a>

                    {*<a class="btn btn-default js_school-pickup_edit"
                       data-username="{$username}" data-time="{$toDate}">
                        <i class="fa fa-plus"></i> {__("Add child")}
                    </a>*}
                </div>
            {/if}
            {if $school['school_step'] == SCHOOL_STEP_FINISH  || $school['grade'] != 0}
                <div class="pull-right flip" style="margin-right: 5px">
                    {if $sub_view != "template" && $sub_view != "assign"}
                        <a href="https://blog.inet.vn/huong-dan-quan-ly-cau-hinh-tra-muon-tren-website-inet/"
                           class="btn btn-info  btn_guide" target="_blank">
                            <i class="fa fa-info-circle"></i> {__("Guide")}
                        </a>
                    {elseif $sub_view == "template"}
                        <a href="https://blog.inet.vn/huong-dan-thiet-lap-chuc-nang-tra-tre-muon/"
                           class="btn btn-info  btn_guide" target="_blank">
                            <i class="fa fa-info-circle"></i> {__("Late pickup configuration guide")}
                        </a>
                    {elseif $sub_view == "assign"}
                        <a href="https://blog.inet.vn/huong-dan-phan-cong-giao-vien-vao-lop-trong-muon/"
                           class="btn btn-info  btn_guide" target="_blank">
                            <i class="fa fa-info-circle"></i> {__("Assign teacher guide")}
                        </a>
                    {/if}
                </div>
            {/if}
            <i class="fa fa-universal-access fa-fw fa-lg pr10"></i>
            {__("Late PickUp")}
            {if $sub_view == "template"}
                &rsaquo; {__("Setting and create price table")}
            {elseif $sub_view == "assign"}
                &rsaquo; {__("Assign teacher")}
            {elseif $sub_view == "list"}
                &rsaquo; {__("Lists")}
            {elseif $sub_view == "child"}
                &rsaquo; {__("Using late Pickup information")}
            {elseif $sub_view == "detail"}
                &rsaquo; {__("Detail")}
            {elseif $sub_view == "add"}
                &rsaquo; {__("Add child")}
            {/if}
        </div>

        {if $sub_view == "template"}
            <div class="panel-body">
                <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_pickup.php">
                    <input type="hidden" name="school_username" id="school_username" value="{$username}"/>
                    <input type="hidden" name="do" value="add_template"/>

                    <div class="form-group">
                        <div class="text-center">
                            {if is_null($template)}
                                <strong>
                                    <font color="#ff4500"><label>{__("The school has not establish the late pickup configuration")}</label></font>
                                </strong>
                            {else}
                                <strong>
                                    <font color="#228b22"><label>{__("The school has established the late pickup configuration")}</label></font>
                                </strong>
                            {/if}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Pickup beginning time")} (*)</label>
                        <div class='col-sm-3'>
                            <div class='input-group date' id='pickup_beginpicker'>
                                <input type='text' name="begin_pickup_time" class="form-control text-center"
                                       value="{if !empty($template['price_list'][0]['beginning_time'])} {$template['price_list'][0]['beginning_time']} {else}17:00{/if}"
                                       required/>
                                <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">
                            {__("Price table detail")} (*)
                        </label>
                        <div class="col-sm-9">

                            <table class="table table-bordered table-hover mb0" id="pickup_price_list">
                                <thead>
                                <tr>
                                    <th class="col-sm-3" align="center">{__("Pickup before")}</th>
                                    <th class="col-sm-3" align="center">{__("Unit price")}</th>
                                    <th class="col-sm-2" align="center">{__("Actions")}</th>
                                </tr>
                                </thead>
                                <tbody>

                                {if isset($template['price_list']) && count($template['price_list'] > 0)}
                                    {$i = 1}
                                    {foreach $template['price_list'] as $value}
                                        <tr>
                                            {*<td><input type="text" name="beginning_time[]" class="form-control beginning_time text-center" value="{$value['beginning_time']}" required></td>*}
                                            <td><input type="text" name="ending_time[]"
                                                       class="form-control ending_time text-center"
                                                       value="{$value['ending_time']}" required></td>
                                            <td><input type="text" name="unit_price[]"
                                                       class="form-control text-center money_tui"
                                                       value="{$value['unit_price']}" required></td>
                                            {if $i == 1}
                                                <td></td>
                                            {else}
                                                <td class="align-middle" align="center">
                                                    <a class="btn btn-xs btn-danger mt5 js_pickup_price-delete"
                                                       data-handle="delete">
                                                        {__("Delete")}
                                                    </a>
                                                </td>
                                            {/if}
                                        </tr>
                                        {$i = $i + 1}
                                    {/foreach}
                                {else}
                                    <tr>
                                        {*<td><input type="text" name="beginning_time[]" class="form-control beginning_time text-center" required></td>*}
                                        <td><input type="text" name="ending_time[]"
                                                   class="form-control ending_time text-center" required></td>
                                        <td><input type="text" name="unit_price[]"
                                                   class="form-control text-center money_tui" required></td>
                                        <td></td>
                                    </tr>
                                {/if}

                                </tbody>
                            </table>
                            {*<a class="btn btn-default js_pickup_price-add">+</a>*}
                            <button type="button" class="btn btn-success js_pickup_price-add">+</button>

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">
                            {__("Pickup services")}
                        </label>
                        <div class="col-sm-9">
                            {__("Tạo các dịch vụ đi kèm với đón muộn (ví dụ: uống sữa muộn, ...).")}
                            <table class="table table-bordered table-hover mb0" id="pickup_service">
                                <thead>
                                <tr>
                                    <th class="col-sm-2" align="center">{__("Service name")}</th>
                                    <th class="col-sm-2" align="center">{__("Unit price")}</th>
                                    <th class="col-sm-2" align="center">{__("Description")}</th>
                                    <th class="col-sm-2" align="center">{__("Actions")}</th>
                                </tr>
                                </thead>
                                <tbody>
                                {if isset($template['services']) && count($template['services'] > 0)}
                                    {foreach $template['services'] as $service}
                                        <tr>
                                            <td class="x-hidden"><input type="hidden" name="service_id[]"
                                                                        value="{$service['service_id']}" required></td>
                                            <td><input type="text" name="service_name[]" class="form-control"
                                                       value="{$service['service_name']}" required></td>
                                            <td><input type="text" name="service_price[]"
                                                       class="form-control text-center money_tui"
                                                       value="{$service['fee']}" required></td>
                                            <td><input type="text" name="service_description[]" class="form-control"
                                                       value="{$service['description']}"></td>
                                            <td class="align-middle" align="center">
                                                <a class="btn btn-xs btn-danger mt5 js_pickup_service-delete"
                                                   data-handle="delete" data-id="{$service['service_id']}">
                                                    {__("Delete")}
                                                </a>
                                            </td>
                                        </tr>
                                    {/foreach}
                                {/if}
                                </tbody>
                            </table>
                            {*<a class="btn btn-default js_pickup_service-add">+</a>*}
                            <button type="button" class="btn btn-success js_pickup_service-add">+</button>

                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">
                            {__("Late pickup classes")} (*)
                        </label>
                        <div class="col-sm-9">

                            <table class="table table-bordered table-hover mb0" id="pickup_classes">
                                <thead>
                                <tr>
                                    <th class="col-sm-3">
                                        <center>{__("Class name")}</center>
                                    </th>
                                    <th class="col-sm-3">
                                        <center>{__("Description")}
                                            <center>
                                    </th>
                                    <th class="col-sm-2">
                                        <center>{__("Actions")}</center>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                {if !empty($template['classes'])}
                                    {$i = 1}
                                    {foreach $template['classes'] as $class}
                                        <tr>
                                            <td class="x-hidden"><input type="hidden" name="class_id[]"
                                                                        value="{$class['pickup_class_id']}" required>
                                            </td>
                                            <td><input type="text" name="class_name[]" class="form-control"
                                                       value="{$class['class_name']}" required></td>
                                            <td><input type="text" name="class_description[]" class="form-control"
                                                       value="{$class['description']}"></td>
                                            {if $i == 1}
                                                <td></td>
                                            {else}
                                                <td class="align-middle" align="center">
                                                    <a class="btn btn-xs btn-danger mt5 js_pickup_class-delete"
                                                       data-handle="delete" data-id="{$class['pickup_class_id']}">
                                                        {__("Delete")}
                                                    </a>
                                                </td>
                                            {/if}
                                        </tr>
                                        {$i = $i + 1}
                                    {/foreach}
                                {else}
                                    <tr>
                                        <td><input type="text" name="class_name[]" class="form-control"
                                                   value="Lớp đón muộn" required></td>
                                        <td><input type="text" name="class_description[]" class="form-control"></td>
                                        <td></td>
                                    </tr>
                                {/if}
                                </tbody>
                            </table>
                            <button type="button" class="btn btn-success js_pickup_class-add">+</button>

                        </div>
                    </div>

                    {*<div class="form-group">*}
                    {*<label class="col-sm-3 control-label">*}
                    {*{__("Late pickup teachers")}*}
                    {*</label>*}
                    {*<div class="col-sm-9">*}
                    {*<table class="table table-bordered mb10" id="tbody-teacher">*}
                    {*<tbody>*}
                    {*<tr>*}
                    {*<th>*}
                    {*{foreach $teachers as $teacher }*}
                    {*<label class="col-xs-12 col-sm-6 text-left">*}
                    {*<input type="checkbox" class="teachers" name="teacher[]" value="{$teacher['user_id']}" {if $teacher['selected']} checked{/if}> {$teacher['user_fullname']}*}
                    {*<input type="hidden" name="teacher_fullname_{$teacher['user_id']}" value="{$teacher['user_fullname']}">*}
                    {*<input type="hidden" name="teacher_name_{$teacher['user_id']}" value="{$teacher['user_name']}">*}
                    {*</label>*}
                    {*{/foreach}*}
                    {*</th>*}
                    {*</tr>*}
                    {*</tbody>*}
                    {*</table>*}
                    {*<span class="help-block">*}
                    {*{__("Chọn giáo viên có thể phân công đón muộn")} <br>*}
                    {*</span>*}
                    {*</div>*}
                    {*</div>*}

                    <div class="form-group">
                        <div class="col-sm-12 text-center">
                            <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                        </div>
                    </div>

                    <!-- success -->
                    <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                    <!-- success -->

                    <!-- error -->
                    <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                    <!-- error -->
                </form>
            </div>

        {elseif $sub_view == "assign"}
            <div class="panel-body with-table" style="position: relative">
                <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_pickup.php">
                    <input type="hidden" name="school_username" id="school_username" value="{$username}"/>
                    <input type="hidden" name="do" value="assign"/>

                    <div class="row">
                        <div class='col-sm-12'>

                            <div class="form-group">
                                <label class="col-sm-5 control-label">
                                    {__("Phân công theo")}
                                </label>
                                <div class="col-sm-7">
                                    <div class="radio radio-primary radio-inline">
                                        <input class="assign_radio" type="radio" name="assign_type" value="week" {if $assign_type == 'week'}checked{/if}>
                                        <label for="assign_week">{__("Week")}</label>
                                    </div>
                                    <div class="radio radio-primary radio-inline">
                                        <input class="assign_radio" type="radio" name="assign_type" value="month" {if $assign_type == 'month'}checked{/if}>
                                        <label for="assign_month">{__("Month")}</label>
                                    </div>
                                </div>
                            </div>
                            <br/>

                            <div class="form-group">
                                <div class='col-sm-1'></div>
                                <div class='col-sm-4'>
                                    <select name="pickup_class_assign" id="pickup_class_assign" class="form-control">
                                        <option value="" disabled>{__("Select Class")}...</option>
                                        {$i = 1}
                                        {foreach $classes as $class }
                                            <option value="{$class['pickup_class_id']}" {if $i == 1} selected {/if}>{$class['class_name']}</option>
                                            {$i = -1}
                                        {/foreach}
                                    </select>
                                </div>
                                <div class='col-sm-3 {if $assign_type == 'week'}x-hidden{/if}' id="div_assign_month">
                                    <div class='input-group date' id='pickup_month_picker'>
                                        <input type='text' name="month" id="month" class="form-control"
                                               placeholder="{__("Month")} (*)" required/>
                                        <span class="input-group-addon"><span class="fas fa-calendar-alt"></span></span>
                                    </div>
                                </div>

                                <div class='col-sm-3  {if $assign_type == 'month'}x-hidden{/if}' id="div_assign_date">
                                    <div class='input-group date' id='pickup_date_picker'>
                                        <input type='text' name="date" id="date" class="form-control" value="{$monday}"
                                               placeholder="{__("Date")} (*)" required/>
                                        <span class="input-group-addon"><span class="fas fa-calendar-alt"></span></span>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <br/>


                    <div style="z-index: 2">
                        <div><strong>{__("School assign all table")}</strong></div>
                    </div>


                    <div id="table_assign">
                        {include file="ci/school/ajax.school.pickup.assign.tpl"}
                    </div>

                    <div class="form-group mt10 {if $assign_type == 'month'}x-hidden{/if}" id="div_repeat">
                        <div class="col-sm-9">
                            <div class="onoffswitch">
                                <input type="checkbox" name="is_repeat" class="onoffswitch-checkbox" id="is_repeat"
                                       {if $is_repeat == 1}checked{/if}>
                                <label class="onoffswitch-label" for="is_repeat"></label>
                            </div>
                            <span class="help-block">
                            {__("Repeating schedule assignment by week?")}
                        </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-9">
                            <button type="submit"
                                    class="btn btn-primary padrl30">{__("Save")}</button>
                        </div>
                    </div>

                </form>

            </div>
        {elseif $sub_view == "list"}
            <div class="panel-body with-table">
                <div class="row">
                    <div class='col-sm-12'>
                        <div class='col-sm-2'></div>
                        <div class='col-sm-3'>
                            <div class='input-group date' id='fromdatepicker'>
                                <input type='text' name="fromDate" id="fromDate" value="{$fromDate}"
                                       class="form-control" placeholder="{__("From date")}"/>
                                <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                            </div>
                        </div>
                        <div class='col-sm-3'>
                            <div class='input-group date' id='todatepicker'>
                                <input type='text' name="toDate" id="toDate" value="{$toDate}" class="form-control"
                                       placeholder="{__("To date")}"/>
                                <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                            </div>
                        </div>
                        <div class='col-sm-3'>
                            <div class="form-group">
                                <button class="btn btn-default js_pickup-search" data-username="{$username}"
                                        data-id="{$school['page_id']}">{__("Search")}</button>
                            </div>
                        </div>
                    </div>
                </div>
                <br/>

                <div class="table-responsive" id="pickup_list">
                    {include file="ci/school/ajax.school.pickuplist.tpl"}
                </div>
            </div>
        {elseif $sub_view == "child"}
            <div class="panel-body">
                <div class="js_ajax-forms form-horizontal">
                    <input type="hidden" name="school_username" value="{$username}"/>
                    <input type="hidden" name="do" value="save_child_rollup"/>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-right">{__("Select student")} (*)</label>
                        <div class='col-sm-3'>
                            <select name="class_id" id="attendance_class_id" data-username="{$username}"
                                    class="form-control" autofocus>
                                <option value="">{__("Select class")}...</option>
                                {$class_level = -1}
                                {foreach $data['classes'] as $class}
                                    {if ($class_level != $class['class_level_id'])}
                                        <option value="" disabled>-----{$class['class_level_name']}-----</option>
                                    {/if}
                                    <option value="{$class['group_id']}"
                                            {if $data['child']['class_id'] == $class['group_id']}selected{/if}>{$class['group_title']}</option>
                                    {$class_level = $class['class_level_id']}
                                {/foreach}

                            </select>
                        </div>
                        <div class="col-sm-4">
                            <select name="child_id" id="attendance_child_id" class="form-control" required>
                                <option value="">{__("Select student")}...</option>
                                {if $data['child'] != ''}
                                    <option value="{$data['child']['child_id']}"
                                            selected>{$data['child']['child_name']}</option>
                                {/if}
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-right">{__("Time")} (*)</label>
                        <div class='col-sm-3'>
                            <div class='input-group date' id='fromdatepicker'>
                                <input type='text' name="fromDate" id="fromDate" value="{$data['fromDate']}"
                                       class="form-control" placeholder="{__("From date")}"/>
                                <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                            </div>
                        </div>

                        <div class='col-md-3'>
                            <div class='input-group date' id='todatepicker'>
                                <input type='text' name="toDate" id="toDate" value="{$data['toDate']}"
                                       class="form-control" placeholder="{__("To date")}"/>
                                <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <a href="#" id="search" class="btn btn-default js_school-pickup_child"
                               data-username="{$username}"
                               {if !($data['child'] != '')}disabled="true"{/if}>{__("Search")}</a>
                            <label id="loading" class="btn btn-info x-hidden">{__("Loading")}...</label>
                        </div>
                    </div>
                    {*<div class="table-responsive" id="attendance_list">
                        {if $data['child'] != ''}
                            {include file="ci/school/ajax.school.attendance.child.tpl"}
                        {/if}
                    </div>*}

                    <div class="table-responsive" id="pickup_list_child">
                        {include file="ci/school/ajax.child.pickup.tpl"}
                    </div>

                </div>
            </div>
        {elseif $sub_view == "add"}
            <div class="panel-body with-table">
                <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_pickup.php">
                    <input type="hidden" name="school_username" id="school_username" value="{$username}"/>
                    {*<input type="hidden" name="date" id="add_to_date" value="{$today}"/>*}
                    <input type="hidden" name="do" value="add_child"/>

                    <div class="form-group">
                        <label class="col-sm-2"></label>
                        <label class="col-sm-4 control-label text-left">{__("Add child into late pickup class on")|upper}
                            (*)</label>
                        <div class="col-sm-3">
                            <div class='input-group date' id='pickup_datepicker'>
                                <input type='text' name="date" id="add_to_date" value="{$today}" class="form-control"
                                       placeholder="{__("Time")} (*)" required/>
                                <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                            </div>
                        </div>
                    </div>

                    {$count = $pickup_class|count}
                    {if $count == 1}
                        <div class="form-group">
                            <label class="col-sm-2"></label>
                            <label class="col-sm-3 control-label text-left">{__("Add to")} (*)</label>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" value="{$pickup_class[0]['class_name']}"
                                       disabled/>
                                <input type="radio" class="pickup_class_id x-hidden" name="pickup_class_id"
                                       value="{$pickup_class[0]['pickup_class_id']}" checked/>
                                {*<input type="hidden" class="pickup_class_id" name="pickup_class_id" value="{$pickup_class[0]['pickup_class_id']}"/>*}
                            </div>
                        </div>
                    {elseif $count > 1}
                        <div class="form-group">
                            <label class="col-sm-2"></label>
                            <label class="col-sm-3 control-label text-left">{__("Add to")} (*)</label>
                            <div class="col-sm-6">
                                {$i = 1}
                                {foreach $pickup_class as $class }
                                    <input type="radio" class="pickup_class_id" name="pickup_class_id"
                                           value="{$class['pickup_class_id']}"
                                           data-name="{$class['class_name']}" {if $i == 1} checked{/if}/>
                                    {$class['class_name']}
                                    <br>
                                    {$i = -1}
                                {/foreach}
                            </div>
                        </div>
                    {else}
                        <input type="radio" class="pickup_class_id x-hidden" name="pickup_class_id" value="0" checked/>
                        {*<input type="hidden" class="pickup_class_id" name="pickup_class_id" value="0"/>*}
                    {/if}

                    <div class="form-group">
                        <label class="col-sm-2"></label>
                        <label class="col-sm-3 control-label text-left">{__("Class")} (*)</label>
                        <div class="col-sm-3">
                            <select name="class_id" id="pickup_class" class="form-control" required>
                                {$class_level = -1}
                                {foreach $classes as $class}
                                    {if ($class_level != $class['class_level_id'])}
                                        <option value="" disabled style="color: blue">-----{$class['class_level_name']}
                                            -----
                                        </option>
                                    {/if}
                                    <option value="{$class['group_id']}"
                                            {if $class_id==$class['group_id']}selected{/if}>{$class['group_title']}</option>
                                    {$class_level = $class['class_level_id']}
                                {/foreach}
                            </select>
                        </div>
                    </div>


                    <div class="table-responsive" id="pickup_school_list">
                        {include file="ci/class/ajax.pickupchild.tpl"}
                    </div>

                    <div class="form-group pl5">
                        <div class="col-sm-9">
                            <button type="submit"
                                    class="btn btn-primary padrl30" {if $disableSave} disabled{/if}>{__("Save")}</button>
                        </div>
                    </div>

                    <!-- success -->
                    <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                    <!-- success -->

                    <!-- error -->
                    <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                    <!-- error -->
                </form>
            </div>
        {elseif $sub_view == "detail"}
            <div class="panel-body with-table">
                <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_pickup.php">
                    <input type="hidden" name="school_username" id="school_username" value="{$username}"/>
                    <input type="hidden" name="pickup_date" id="pickup_date" value="{$today}"/>
                    <input type="hidden" id="pickup_id" name="pickup_id" value="{$pickup['pickup_id']}"/>
                    <input type="hidden" name="do" value="save"/>
                    <div id="time_option" class="x-hidden" data-value="{$time_option}"></div>

                    <div class="text-center">
                        <strong><label style="font-size: 15px">{__("Late pickup on")}: {$today}</label></strong><br>
                        {if !is_empty($pickup['class_name'])}
                            <strong><label style="font-size: 15px">{__("Class")}
                                    : {$pickup['class_name']}</label></strong>
                            <br>
                            <br>
                        {else}
                            <br>
                        {/if}
                    </div>
                    <div class="col-sm-12">
                        <label class="col-sm-6 text-right" style="font-size: 15px">{__("Late pickup teachers")}:</label>
                        {if $pickup['assign']|count > 0}
                            <div class="col-sm-6">
                                {foreach $pickup['assign'] as $assign}
                                    <strong>
                                        <label>&#9679;&#09;{$assign['user_fullname']}</label>
                                    </strong>
                                    <br>
                                {/foreach}
                            </div>
                        {else}
                            <div class="col-sm-6 text-left"
                                 style="font-size: 15px;color: red">{__("Not assigned")}</div>
                        {/if}
                    </div>


                    <div>
                        <strong>{__("Total student")}: <font color="red">{$children|count}</font> |
                            {__("Picked up")}: <font color="red">{$child_count}</font>
                        </strong>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th width="5%">#</th>
                                <th width="25%">{__("Student name")}</th>
                                <th width="20%">{__("Birthdate")}</th>
                                <th width="20%">{__("Class")}</th>
                                <th width="15%">{__("Status")}</th>
                                <th width="15%">{__("Total amount")}</th>
                            </tr>
                            </thead>
                            <tbody>
                            {$idx = 1}
                            {foreach $children as $child}
                                <input type="hidden" name="child[]" value="{$child['child_id']}"/>
                                <tr {if $child['child_status'] == 0} class="row-disable" {/if}>
                                    <td align="center"><strong>{$idx}</strong></td>
                                    <td><strong>{$child['child_name']}</strong></td>
                                    <td align="center">{$child['birthday']}</td>
                                    <td align="center">{$child['group_title']}</td>
                                    <td rowspan="4" align="center">
                                        {if $canEdit}
                                            {if is_empty($child['pickup_at']) }
                                                {if $is_today}
                                                    <a class="btn btn-xs btn-default js_school-pickup-one-child"
                                                       data-handle="pickup"
                                                       data-username="{$username}" data-child="{$child['child_id']}"
                                                       data-pickup="{$child['pickup_id']}">
                                                        {__("Pickup")}
                                                    </a>
                                                {/if}
                                            {/if}
                                            <a class="btn btn-xs btn-danger js_school-pickup" data-handle="cancel"
                                               data-username="{$username}" data-child="{$child['child_id']}"
                                               data-pickup="{$child['pickup_id']}">{__("Cancel")}
                                            </a>
                                        {/if}
                                    </td>
                                    <td rowspan="1"></td>
                                </tr>
                                <tr {if $child['child_status'] == 0} class="row-disable" {/if}>
                                    <td rowspan="3"></td>
                                    <td class="align-middle text-left" colspan="3" style="padding-left: 35px">
                                        <div class="col-sm-4 pt3">
                                            {__("Pickup at")}
                                            : &nbsp;
                                        </div>

                                        {if !is_empty($child['pickup_at']) }
                                            <input type='text' name="pickup_time[]" class="pickup_time"
                                                   id="pickup_time_{$child['child_id']}" value="{$child['pickup_at']}"
                                                   style="width: 70px; color:blue; text-align: center"/>
                                            {*{if !is_empty($child['giveback_person'])}
                                                &nbsp;-&nbsp;
                                                <strong>
                                                    <font color="#228b22">{$child['giveback_person']}</font>
                                                </strong>
                                            {/if}*}
                                        {else}
                                            <input type='text' name="pickup_time[]" class="pickup_time"
                                                   id="pickup_time_{$child['child_id']}" value=""
                                                   style="width: 70px; color: red; text-align: center"/>
                                            &nbsp;{*-&nbsp;
                                        <strong>
                                            <font color="#ff4500">{__("Chưa có thông tin")}</font>
                                        </strong>*}
                                        {/if}
                                    </td>
                                    <td colspan="1" align="right">
                                        <input type="text" class="text-right money_tui"
                                               id="total_pickup_{$child['child_id']}"
                                               data-child="{$child['child_id']}"
                                               name="pickup_fee[]" value="{$child['late_pickup_fee']}"
                                               maxlength="10" style="width: 100px; font-weight: bold"/>
                                        {*<input type="hidden" name="old_pickup_fee[]" value="{$child['late_pickup_fee']}"/>*}
                                    </td>
                                </tr>
                                <tr {if $child['child_status'] == 0} class="row-disable" {/if}>
                                    <td class="align-middle text-left" colspan="3" style="padding-left: 35px">
                                        {if !empty($child['services'])}
                                            <div class="col-sm-4 pt3">
                                                {__("Using service")}: &nbsp;
                                            </div>
                                            {foreach $child['services'] as $service}
                                                <input type="checkbox" class="pickup_service_fee"
                                                       name="service_{$child['child_id']}[]"
                                                       id="pickup_service_fee_{$child['child_id']}_{$service['service_id']}"
                                                       data-child="{$child['child_id']}"
                                                       data-service="{$service['service_id']}"
                                                       data-fee="{$service['price']}"
                                                       value="{$service['service_id']}" {if !is_null($service['using_at']) } checked {/if}
                                                />
                                                {$service['service_name']}
                                                {*{if !is_null($service['using_at'])}
                                                    <input type="hidden" name="service_used_{$child['child_id']}[]"
                                                           value="{$service['service_id']}"/>
                                                {/if}*}
                                                &nbsp;&nbsp;&nbsp;
                                            {/foreach}
                                        {else}
                                            &nbsp;
                                        {/if}
                                    </td>
                                    <td colspan="1" align="right">
                                        <input type="text" class="text-right money_tui"
                                               name="service_fee[]"
                                               id="total_service_{$child['child_id']}"
                                               data-child="{$child['child_id']}"
                                               value="{$child['using_service_fee']}"
                                               maxlength="10" style="width: 100px; font-weight: bold"/>
                                        {*<input type="hidden" name="old_service_fee[]" value="{$child['using_service_fee']}"/>*}
                                    </td>
                                </tr>
                                <tr {if $child['child_status'] == 0} class="row-disable" {/if}>
                                    <td class="align-middle text-left" colspan="3" style="padding-left: 35px">
                                        {*<strong>{__("Người đăng ký")}</strong>: &nbsp;
                                        <strong>
                                            {if $child['type'] == 2}
                                                <font color="#ff4500">{__("Phụ huynh")} -
                                            {else}
                                                <font color="#228b22">
                                            {/if}
                                                {$child['subscriber']}</font>
                                        </strong>

                                        {if !is_null($child['register'])}
                                            <br/>
                                            <strong>{__("Dịch vụ đăng ký")}</strong> : &nbsp;
                                            {if $child['register']['service']|count > 0 }
                                                <strong style="color: blue">
                                                    {foreach $child['register']['service'] as $srv}
                                                        &#9679;&#09;{$srv['service_name']}
                                                    {/foreach}
                                                </strong>
                                            {else}
                                                <strong style="color: #ff4500">{__("Không đăng ký")}</strong>
                                            {/if}
                                        {/if}

                                        <br/>*}
                                        <div class="col-sm-4 pt3">
                                            {__("Note")} : &nbsp;
                                        </div>
                                        <input type='text' name="pickup_note[]" id="pickup_note_{$child['child_id']}" value="{$child['description']}"
                                               style="width: 60%; color: blue"/>

                                    </td>
                                    <td colspan="1" style="vertical-align: middle">
                                        <input type="text" class="text-right money_tui"
                                               id="total_{$child['child_id']}"
                                               name="total_child[]"
                                               value="{$child['total_amount']}"
                                               maxlength="10"
                                               style="width: 100px; font-weight: bold; color: blue; border: none; padding-right: 2px"
                                               readonly/>
                                        {*<input type="hidden" name="old_total_child[]" value="{$child['total_amount']}" />*}
                                    </td>
                                </tr>
                                {$idx = $idx + 1}
                            {/foreach}
                            <tr>
                                <td colspan="6"></td>
                            </tr>
                            <tr>
                                <td colspan="5" align="right"><strong>{__("Total")}</strong></td>
                                <td colspan="1" align="right">
                                    {*<input type="number" class="text-right" id="total" name="total" value="{moneyFormat($pickup['total'])}" maxlength="10" style="width: 100px; font-weight: bold; color: red" readonly/>*}
                                    <input type="text" class="text-right money_tui" id="total" name="total"
                                           value="{$pickup['total']}"
                                           style="width: 100px; font-weight: bold; color: red; border: none; padding-right: 2px"
                                           readonly/>
                                    {*<input type="hidden" name="old_total" value="{$pickup['total']}" />*}
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    {if $canEdit}
                        <div class="form-group">
                            <div class="col-sm-9 pl10">
                                <button type="submit"
                                        class="btn btn-primary padrl30 {if $children|count == 0} x-hidden {/if}">{__("Save")}</button>
                            </div>
                        </div>
                        <!-- success -->
                        <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                        <!-- success -->
                        <!-- error -->
                        <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                        <!-- error -->
                    {/if}
                </form>
            </div>
        {/if}
    </div>
</div>