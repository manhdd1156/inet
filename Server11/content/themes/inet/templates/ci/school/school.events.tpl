<div class="panel panel-default">
    <div class="panel-heading with-icon">
        {if ($sub_view == "") && $canEdit}
            <div class="pull-right flip">
                <a href="{$system['system_url']}/school/{$username}/events/add" class="btn btn-default">
                    <i class="fa fa-plus"></i> {__("Add New")}
                </a>
            </div>
        {elseif $sub_view == "add" || $sub_view == "edit"}
            <div class="pull-right flip">
                <a href="{$system['system_url']}/school/{$username}/events" class="btn btn-default">
                    <i class="fa fa-list"></i> {__("Lists")}
                </a>
            </div>
        {/if}
        <div class="pull-right flip" style="margin-right: 5px">
{*            <a href="https://blog.inet.vn/huong-dan-tao-su-kien-tren-website/" class="btn btn-info btn_guide" target="_blank">*}
            <a href="#" class="btn btn-info btn_guide" target="_blank">
                <i class="fa fa-info-circle"></i> {__("Guide")}
            </a>
        </div>
        <i class="fa fa-bell fa-lg fa-fw pr10"></i>
        {__("Notification - Event")}
        {if $sub_view == "edit"}
            &rsaquo; {$data['event_name']}
        {elseif $sub_view == "detail"}
            &rsaquo; {$data['event_name']}
        {elseif $sub_view == "add"}
            &rsaquo; {__('Add New')}
        {elseif $sub_view == "participants"}
            &rsaquo; {__("Participants")} &rsaquo; <a href="{$system['system_url']}/school/{$username}/events/detail/{$event['event_id']}">{$event['event_name']}</a>
        {elseif $sub_view == "participantsforemployee"}
            &rsaquo; {__("Participants")} &rsaquo; <a href="{$system['system_url']}/school/{$username}/events/detail/{$event['event_id']}">{$event['event_name']}</a>
        {/if}
    </div>
    {if $sub_view == ""}
        <div class="panel-body with-table">
            <div><strong>{__("Notification - Event list")}&nbsp;(<span class="count_event">{$rows|count}</span>)</strong></div>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>{__("Notification - Event")}</th>
                            <th>{__("Event level")}</th>
                            {if $canEdit}
                                {*<th>{__("Time")}</th>*}
                                <th>{__("Actions")}</th>
                            {/if}
                        </tr>
                    </thead>
                    <tr>
                        {$idx = 1}
                        {foreach $rows as $row}
                            <tr>
                                {*{if $idx > 1}<tr><td colspan="7"></td></tr>{/if}*}
                                <td align="center" rowspan="2" style="vertical-align:middle"><strong>{$idx}</strong></td>
                                <td style="vertical-align:middle">
                                    <a href="{$system['system_url']}/school/{$username}/events/detail/{$row['event_id']}">{$row['event_name']}</a> <a href="#" data-id="{$row['event_id']}" style="float: right; color: #5D9D58" class="js_event_toggle">{__("Show/hide")}</a>
                                    <img class="cancel_img_{$row['event_id']} {if $row['status']!= $smarty.const.EVENT_STATUS_CANCELLED}x-hidden{/if}" src="{$system['system_url']}/content/themes/{$system['theme']}/images/cancel.png"/>
                                </td>
                                <td align="center" style="vertical-align:middle">
                                    {if $row['for_teacher']}
                                        {__("Teacher - Employee")}
                                    {else}
                                        {if $row['level']==$smarty.const.SCHOOL_LEVEL}
                                            {__("School")}
                                        {elseif $row['level']==$smarty.const.CLASS_LEVEL_LEVEL}
                                            {$row['class_level_name']}
                                        {elseif $row['level']==$smarty.const.CLASS_LEVEL}
                                            {$row['group_title']}
                                        {/if}
                                    {/if}
                                </td>
                                {if $canEdit}
                                    {*<td align="center" style="vertical-align:middle" nowrap="true">*}
                                    {*{if (isset($row['begin']) && ($row['begin'] != ''))}*}
                                    {*<font color="blue">{$row['begin']}</font><br/>{$row['end']}*}
                                    {*{/if}*}
                                    {*</td>*}
                                    <td align="center" style="vertical-align:middle">
                                        {if $canEdit}
                                            {if (!$row['is_notified']) && ($row['status'] != $smarty.const.EVENT_STATUS_CANCELLED)}
                                                <button class="btn btn-xs btn-default js_school-event-notify cancel_hidden_{$row['event_id']}" data-username="{$username}" data-id="{$row['event_id']}">
                                                    {__("Notify")}
                                                </button>
                                            {/if}
                                        {/if}
                                        {if $row['must_register']}
                                            {if !$row['for_teacher']}
                                                <a href="{$system['system_url']}/school/{$username}/events/participants/{$row['event_id']}" class="btn btn-xs btn-default">
                                                    {__("Participants")}
                                                </a><br/>
                                            {else}
                                                <a href="{$system['system_url']}/school/{$username}/events/participantsforemployee/{$row['event_id']}" class="btn btn-xs btn-default">
                                                    {__("Participants")}
                                                </a><br/>
                                            {/if}
                                        {/if}
                                        {if $canEdit}
                                            {if ($row['status'] != $smarty.const.EVENT_STATUS_CANCELLED)}
                                                <a href="{$system['system_url']}/school/{$username}/events/edit/{$row['event_id']}" class="btn btn-xs btn-default cancel_hidden_{$row['event_id']}">{__("Edit")}</a>
                                                {if (($row['parent_participants'] + $row['child_participants'] + $row['teacher_participants']) == 0) && (!$row['is_notified'])}
                                                    <button class="btn btn-xs btn-danger js_school-delete cancel_hidden_{$row['event_id']}" data-handle="event" data-username="{$username}" data-id="{$row['event_id']}">{__("Delete")}</button>
                                                {else}
                                                    <button class="btn btn-xs btn-warning js_school-event-cancel cancel_hidden_{$row['event_id']}" data-username="{$username}" data-id="{$row['event_id']}">{__("Cancel")}</button>
                                                {/if}
                                            {/if}
                                        {/if}
                                    </td>
                                {/if}
                            </tr>
                            <tr>
                                <td style="display: none" class="event_description_{$row['event_id']} description_style" {if $canEdit}colspan="6"{else}colspan="3"{/if}>{$row['description']}</td>
                            </tr>
                            {$idx = $idx + 1}
                        {/foreach}

                        {if $rows|count == 0}
                            <tr class="odd">
                                <td valign="top" align="center" colspan="7" class="dataTables_empty">
                                    {__("No data available in table")}
                                </td>
                            </tr>
                        {/if}
                    </tbody>
                </table>
            </div>
        </div>
    {elseif $sub_view == "detail"}
        <div class="panel-body with-table">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_event.php">
                <input type="hidden" name="school_username" id="school_username" value="{$username}"/>
                <input type="hidden" name="do" id="do" value="reg_event"/>
                <input type="hidden" id="school_id" name="school_id" value="{$school['page_id']}"/>
                <input type="hidden" id="event_id" name="event_id" value="{$data['event_id']}"/>
                <input type="hidden" name="event_name" value="{$data['event_name']}"/>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <tbody>
                            <tr>
                                <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Notification - Event")}</strong></td>
                                <td>{$data['event_name']}</td>
                            </tr>
                            {if $data['for_teacher']}
                                <tr>
                                    <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Scope")}</strong></td>
                                    <td>{__('Teacher - Employee')}</td>
                                </tr>
                            {else}
                                <tr>
                                    <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Event level")}</strong></td>
                                    <td>
                                        {if $data['level']==$smarty.const.SCHOOL_LEVEL}
                                            {__("School")}
                                        {elseif $data['level']==$smarty.const.CLASS_LEVEL_LEVEL}
                                            {$data['class_level_name']}
                                        {elseif $data['level']==$smarty.const.CLASS_LEVEL}
                                            {$data['group_title']}
                                        {/if}
                                    </td>
                                </tr>
                            {/if}
                            {if $canEdit}
                                <tr>
                                    <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Post on page")}?</strong></td>
                                    <td>{if $data['post_on_wall']}{__('Post on page')}{else}{__('Unpost on wall')}{/if}</td>
                                </tr>
                                <tr>
                                    <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Notify immediately")}?</strong></td>
                                    <td>{if $data['is_notified']}{__('Notified')}{else}{__('Not notified yet')}{/if}</td>
                                </tr>
                            {/if}
                            {*{if (isset($data['begin']) && ($data['begin'] != ''))}*}
                                {*<tr>*}
                                    {*<td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Time")}</strong></td>*}
                                    {*<td>*}
                                        {*<font color="blue">{$data['begin']}</font> - {$data['end']}*}
                                    {*</td>*}
                                {*</tr>*}
                            {*{/if}*}
                            {if $data['must_register'] == '1'}
                                {*<tr>*}
                                    {*<td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Event location")}</strong></td>*}
                                    {*<td>{$data['location']}</td>*}
                                {*</tr>*}
                                {*<tr>*}
                                    {*<td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Participation cost")}</strong></td>*}
                                    {*<td>{$data['price']} {$smarty.const.MONEY_UNIT}</td>*}
                                {*</tr>*}
                                <tr>
                                    <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Must register")}?</strong></td>
                                    <td>{__('Yes')}</td>
                                </tr>
                                {if $data['registration_deadline'] != ''}
                                    <tr>
                                        <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Registration deadline")}</strong></td>
                                        <td><font color="red">{$data['registration_deadline']}</font></td>
                                    </tr>
                                {/if}
                                {if !$data['for_teacher']}
                                    <tr>
                                        <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("For child")}?</strong></td>
                                        <td>{if $data['for_child'] == '1'}{__('Yes')}{else}{__('No')}{/if}</td>
                                    </tr>
                                    {if $data['for_child'] == '1'}
                                        <tr>
                                            <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Children total")}</strong></td>
                                            <td>{$data['child_participants']}</td>
                                        </tr>
                                    {/if}
                                    <tr>
                                        <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("For parent")}?</strong></td>
                                        <td>{if $data['for_parent'] == '1'}{__('Yes')}{else}{__('No')}{/if}</td>
                                    </tr>
                                    {if $data['for_parent'] == '1'}
                                        <tr>
                                            <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Parent total")}</strong></td>
                                            <td>{$data['parent_participants']}</td>
                                        </tr>
                                    {/if}
                                {else}
                                    <tr>
                                        <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Teacher total")}</strong></td>
                                        <td>{$data['teacher_participants']}</td>
                                    </tr>
                                {/if}
                            {/if}
                            <tr>
                                <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Creator")}</strong></td>
                                <td>{$data['user_fullname']}</td>
                            </tr>
                            <tr>
                                <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Created time")}</strong></td>
                                <td>{$data['created_at']}</td>
                            </tr>
                            {if $data['updated_at'] != ''}
                                <tr>
                                    <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Last updated")}</strong></td>
                                    <td>{$data['updated_at']}</td>
                                </tr>
                            {/if}
                            {if $data['for_teacher'] && $data['must_register'] && ($school['page_admin'] != $user->_data['user_id'])}
                                <tr>
                                    <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Register")}</strong></td>
                                    <td>
                                        {if $data['for_teacher']}
                                            <input type="checkbox" name="teacherId" value="{$user->_data['user_id']}"
                                                   {if $data['participants']['teacher']['is_registered']}checked{/if} {if $data['can_register'] == 0}disabled{/if}> {__("You")}
                                            {if $data['participants']['teacher']['is_registered']}
                                                <input type="hidden" name="old_teacher_id" value="{$user->_data['user_id']}"/>
                                            {/if}
                                        {/if}
                                    </td>
                                </tr>
                            {/if}
                            <tr style="background: #fff">
                                {*<td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Description")}</strong></td>*}
                                <td colspan="2">{$data['description']}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-1">
                        {if $data['can_register'] > 0  && $data['must_register'] && ($school['page_admin'] != $user->_data['user_id']) && $data['for_teacher']}
                            <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                        {/if}
                    </div>
                </div>
                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->
                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <br/>
            </form>
            <div class="form-group">
                <a class="btn btn-default" href="{$system['system_url']}/school/{$username}/events">{__("Lists")}</a>
                {if $data['must_register']}
                    {if !$data['for_teacher']}
                        <a href="{$system['system_url']}/school/{$username}/events/participants/{$data['event_id']}" class="btn btn-default">{__("Participants")}</a>
                    {else}
                        <a href="{$system['system_url']}/school/{$username}/events/participantsforemployee/{$data['event_id']}" class="btn btn-default">{__("Participants")}</a>
                    {/if}
                {/if}
                {if $canEdit && ($data['status'] != $smarty.const.EVENT_STATUS_CANCELLED)}
                    {if (!$data['is_notified'])}
                        <a class="btn btn-default js_school-event-notify" data-username="{$username}" data-id="{$data['event_id']}">{__("Notify")}</a>
                    {/if}
                    <a class="btn btn-default" href="{$system['system_url']}/school/{$username}/events/edit/{$data['event_id']}">{__("Edit")}</a>
                    {if (($data['parent_participants'] + $data['child_participants'] + $data['teacher_participants']) == 0) && (!$data['is_notified'])}
                        <a class="btn btn-danger js_school-delete" data-handle="event" data-username="{$username}" data-id="{$data['event_id']}">{__("Delete")}</a>
                    {else}
                        <a class="btn btn-warning js_school-event-cancel" data-username="{$username}" data-id="{$data['event_id']}">{__("Cancel")}</a>
                    {/if}
                {/if}
            </div>
        </div>
    {elseif $sub_view == "participants"}
        <div class="panel-body with-table">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_event.php">
                <input type="hidden" name="school_username" id="school_username" value="{$username}"/>
                <input type="hidden" name="event_id" id="event_id" value="{$event['event_id']}"/>
                <input type="hidden" name="event_name" id="event_name" value="{$event['event_name']}"/>
                <input type="hidden" name="do" value="add_pp"/>

                {if ($event['level'] != $smarty.const.CLASS_LEVEL) && ($event['level'] != $smarty.const.TEACHER_LEVEL)}
                    <div class="">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>{__("Class")}</th>
                                    <th>{__("Number of student involved")}</th>
                                    <th>{__("Number of parents involved")}</th>
                                </tr>
                                </thead>
                                <tbody>
                                {$idx = 1}
                                {foreach $eventPars as $row}
                                    <tr>
                                        <td align="center">{$idx}</td>
                                        <td width="50%">{$row['group_title']}</td>
                                        <td align="center">{$row['countChild']}</td>
                                        <td align="center">{$row['countParent']}</td>
                                    </tr>
                                    {$idx = $idx + 1}
                                {/foreach}
                                <tr>
                                    <td colspan="2" align="right"><strong>{__("Total")}</strong></td>
                                    <td align="center"><strong>{$childCount}</strong></td>
                                    <td align="center"><strong>{$parentCount}</strong></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <hr/>
                    <div class="form-group">
                        <label class="col-sm-5 control-label text-left">{__("Select a class to see participant list")}&nbsp;</label>
                        <div class='col-sm-5'>
                            <div class="form-group">
                                <select name="class_id" id="participants_class_id" class="form-control" data-username="{$username}" data-uid="{$event['event_id']}">
                                    <option value="">{__("Select Class")}</option>

                                    {$class_level = -1}
                                    {foreach $classes as $class}
                                        {if ($class_level != $class['class_level_id'])}
                                            <option value="" disabled style="color: blue">-----{$class['class_level_name']}-----</option>
                                        {/if}
                                        <option value="{$class['group_id']}" {if $class['group_id']==$classId}selected{/if}>{$class['group_title']}</option>
                                        {$class_level = $class['class_level_id']}
                                    {/foreach}
                                </select>
                            </div>
                        </div>
                    </div>
                {elseif $event['level'] == $smarty.const.CLASS_LEVEL}
                    <input type="hidden" name="class_id" id="class_id" value="{$event['class_id']}"/>
                {/if}
                {if ($event['can_register'] == 1) && $canEdit}
                    {if ($event['level'] != $smarty.const.TEACHER_LEVEL)}
                        <div id="event_select_panel" {if ($event['level'] != $smarty.const.CLASS_LEVEL)}class="x-hidden"{/if}>
                            {if $event['for_child'] && $event['for_parent']}
                                <input type="checkbox" id="eventCheckAll">&nbsp;{__("All")}
                            {/if}
                            {if $event['for_child']}
                                &nbsp;&nbsp;<input type="checkbox" id="eventCheckAllChildren">&nbsp;{__("All children")}
                            {/if}
                            {if $event['for_parent']}
                                &nbsp;&nbsp;<input type="checkbox" id="eventCheckAllParent">&nbsp;{__("All parent")}
                            {/if}
                        </div>
                    {else}
                        <input type="checkbox" id="eventCheckAllTeacher">&nbsp;{__("All teachers")}
                    {/if}
                {/if}
                <div class="table-responsive" id="participant_list">
                    {if $event['level'] == $smarty.const.CLASS_LEVEL}
                        {include file="ci/school/ajax.eventparticipants.tpl"}
                    {elseif $event['level'] == $smarty.const.TEACHER_LEVEL}
                        {include file="ci/school/ajax.event.teacherparticipants.tpl"}
                    {/if}
                </div>

                {*{if $canEdit && $participants|count > 0}*}
                {if $canEdit}
                    {if (($event['level'] != $smarty.const.CLASS_LEVEL) || ($event['level'] != $smarty.const.TEACHER_LEVEL) || (count($participants) > 0)) && ($event['can_register'] == 1)}
                        <div class="form-group pl5" id="participant_btnSave">
                            <div class="col-sm-9">
                                <button type="submit" class="btn btn-primary padrl30" {if ($event['level'] != $smarty.const.CLASS_LEVEL) && ($event['level'] != $smarty.const.TEACHER_LEVEL)}disabled{/if}>{__("Save")}</button>
                            </div>
                        </div>
                    {elseif $event['can_register'] == 0}
                        <div class="form-group pl5">
                            <div class="col-sm-9">
                                {__("The registration deadline has passed or the event has been cancelled")}.
                            </div>
                        </div>
                    {/if}
                {/if}
                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    {elseif $sub_view == "participantsforemployee"}
        <div class="panel-body with-table">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_event.php">
                <input type="hidden" name="school_username" id="school_username" value="{$username}"/>
                <input type="hidden" name="event_id" id="event_id" value="{$event['event_id']}"/>
                <input type="hidden" name="event_name" id="event_name" value="{$event['event_name']}"/>
                <input type="hidden" name="do" value="add_pp_for_employee"/>
                {if ($event['can_register'] == 1) && $canEdit}
                    <input type="checkbox" id="eventCheckAllTeacher">&nbsp;{__("All teachers")}
                {/if}
                <div class="table-responsive" id="participant_list">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>{__("Select")}</th>
                            <th>{__("Teacher")}</th>
                            <th>{__("Registed time")}</th>
                        </tr>
                        </thead>
                        <tbody>
                        {$idx = 1}
                        {foreach $participants as $teacher}
                            <tr>
                                <td align="center">{$idx}</td>
                                <td align="center">
                                    <input type="checkbox" class="teacher" name="teacherIds[]" value="{$teacher['user_id']}"
                                           {if $teacher['event_id'] > 0}checked{/if} {if !$canEdit}disabled{/if}>
                                    {if $teacher['event_id'] > 0}
                                        <input type="hidden" name="oldTeacherIds[]" value="{$teacher['user_id']}"/>
                                    {/if}
                                </td>
                                <td>{$teacher['user_fullname']}</td>
                                <td>{$teacher['created_at']}</td>
                            </tr>

                            {$idx = $idx + 1}
                        {/foreach}
                        </tbody>
                    </table>
                </div>

                {if $canEdit}
                    {if ($event['for_teacher'] || (count($participants) > 0)) && ($event['can_register'] == 1)}
                        <div class="form-group pl5" id="participant_btnSave">
                            <div class="col-sm-9">
                                {if $event['for_teacher'] && $event['must_register'] && ($event['can_register'] > 0)}
                                    <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                                {/if}
                            </div>
                        </div>
                    {elseif $event['can_register'] == 0}
                        <div class="form-group pl5">
                            <div class="col-sm-9">
                                {__("The registration deadline has passed or the event has been cancelled")}.
                            </div>
                        </div>
                    {/if}
                {/if}
                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    {elseif $sub_view == "edit"}
        <div class="panel-body">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_event.php">
                <input type="hidden" name="school_username" id="school_username" value="{$username}"/>
                <input type="hidden" name="event_id" value="{$data['event_id']}"/>
                <input type="hidden" name="post_ids" value="{$data['post_ids']}"/>
                <input type="hidden" name="do" value="edit"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Notification - Event")} (*)</label>
                    <div class="col-sm-9">
                        <input class="form-control" name="event_name" value="{$data['event_name']}" required autofocus maxlength="250">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Description")} (*)</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="description" id="description" rows="8" required>{$data['description']}</textarea>
                        <script>
                            CKEDITOR.replace('description');
                        </script>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("For teachers/employees")}?</label>
                    <div class="col-sm-9">
                        <div class="onoffswitch">
                            <input type="checkbox" name="for_teacher" class="onoffswitch-checkbox" id="for_teacher" {if $data['for_teacher']}checked{/if}>
                            <label class="onoffswitch-label" for="for_teacher"></label>
                        </div>
                    </div>
                </div>
                <div class="form-group {if $data['for_teacher']}x-hidden{/if}" id="level">
                    <label class="col-sm-3 control-label text-left">{__("Event level")} (*)</label>
                    {*<div class="col-sm-9">*}
                        <div class="col-sm-3">
                            <select name="event_level" id="event_level" class="form-control">
                                <option value="{$smarty.const.SCHOOL_LEVEL}" {if $data['level']==$smarty.const.SCHOOL_LEVEL}selected{/if}>{__("School")}</option>
                                <option value="{$smarty.const.CLASS_LEVEL_LEVEL}" {if $data['level']==$smarty.const.CLASS_LEVEL_LEVEL}selected{/if}>{__("Class level")}</option>
                                <option value="{$smarty.const.CLASS_LEVEL}" {if $data['level']==$smarty.const.CLASS_LEVEL}selected{/if}>{__("Class")}</option>
                                {*<option value="{$smarty.const.TEACHER_LEVEL}" {if $data['level']==$smarty.const.TEACHER_LEVEL}selected{/if}>{__("Teacher")}</option>*}
                            </select>
                        </div>
                        <div class="col-sm-3 {if $data['level']!=$smarty.const.CLASS_LEVEL_LEVEL}x-hidden{/if}" name="event_class_level">
                            <select name="class_level_id" id="class_level_id" class="form-control">
                                {foreach $class_levels as $class_level}
                                    <option value="{$class_level['class_level_id']}" {if $data['class_level_id']==$class_level['class_level_id']}selected{/if}>{$class_level['class_level_name']}</option>
                                {/foreach}
                            </select>
                        </div>
                        <div class="col-sm-3 {if $data['level']!=$smarty.const.CLASS_LEVEL}x-hidden{/if}" name="event_class">
                            <select name="class_id" id="class_id" class="form-control">
                                {$class_level = -1}
                                {foreach $classes as $class}
                                    {if ($class_level != $class['class_level_id'])}
                                        <option value="" disabled style="color: blue">-----{$class['class_level_name']}-----</option>
                                    {/if}
                                    <option value="{$class['group_id']}" {if $data['class_id']==$class['group_id']}selected{/if}>{$class['group_title']}</option>
                                    {$class_level = $class['class_level_id']}
                                {/foreach}
                            </select>
                        </div>
                    {*</div>*}
                </div>
                <div class="form-group" name="event_post_on_wall">
                    <label class="col-sm-3 control-label text-left">
                        {__("Post on page")}?
                    </label>
                    <div class="col-sm-9">
                        <div class="onoffswitch">
                            <input type="checkbox" name="post_on_wall" class="onoffswitch-checkbox" id="post_on_wall" {if $data['post_on_wall']=='1'}checked{/if}>
                            <label class="onoffswitch-label" for="post_on_wall"></label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">
                        {__("Notify immediately")}?
                    </label>
                    <div class="col-sm-9">
                        <div class="onoffswitch">
                            <input type="checkbox" name="notify_immediately" class="onoffswitch-checkbox" id="notify_immediately" {if $data['is_notified']=='1'}checked{/if}>
                            <label class="onoffswitch-label" for="notify_immediately"></label>
                        </div>
                    </div>
                </div>
                {*<div class="form-group">*}
                    {*<label class="col-sm-3 control-label text-left">{__("Time")}</label>*}
                    {*<div class="col-sm-9">*}
                        {*<div class='col-sm-5'>*}
                            {*<div class='input-group date' id='beginpicker'>*}
                                {*<input type='text' name="begin" value="{$data['begin']}" class="form-control" placeholder="{__("Begin")}"/>*}
                                    {*<span class="input-group-addon">*}
                                        {*<span class="fa fa-calendar"></span>*}
                                    {*</span>*}
                            {*</div>*}
                        {*</div>*}

                        {*<div class='col-md-5'>*}
                            {*<div class='input-group date' id='endpicker'>*}
                                {*<input type='text' name="end" value="{$data['end']}" class="form-control" placeholder="{__("End")}"/>*}
                                    {*<span class="input-group-addon">*}
                                        {*<span class="fa fa-calendar"></span>*}
                                    {*</span>*}
                            {*</div>*}
                        {*</div>*}
                    {*</div>*}
                {*</div>*}
                {if $data['must_register'] == '0'}
                    <div class="form-group" id="event_more_information_button">
                        <div class="col-sm-12">
                            <a href="#" class="btn btn-default pull-right js_event-more-information">{__("More information")}</a>
                        </div>
                    </div>
                {/if}
                <div {if $data['must_register'] == '0'}class="x-hidden"{/if} id="event_more_information">
                    {*<div class="form-group">*}
                        {*<label class="col-sm-3 control-label text-left">{__("Event location")}</label>*}
                        {*<div class="col-sm-9">*}
                            {*<input class="form-control" name="location" value="{$data['location']}" maxlength="250">*}
                        {*</div>*}
                    {*</div>*}
                    {*<div class="form-group">*}
                        {*<label class="col-sm-3 control-label text-left">{__("Participation cost")}</label>*}
                        {*<div class="col-sm-9">*}
                            {*<div class="input-group col-md-5">*}
                                {*<input type="number" min="0" step="1000" class="form-control" name="price" value="{$data['price']}" maxlength="10">*}
                                {*<span class="input-group-addon">{$smarty.const.MONEY_UNIT}</span>*}
                            {*</div>*}
                        {*</div>*}
                    {*</div>*}

                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Must register")}?</label>
                        <div class="col-sm-9">
                            <div class="onoffswitch">
                                <input type="checkbox" name="must_register" class="onoffswitch-checkbox" id="must_register" {if $data['must_register']=='1'}checked{/if}>
                                <label class="onoffswitch-label" for="must_register"></label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group {if !$data['must_register']}x-hidden{/if}" id="event_registration_deadline">
                        <label class="col-sm-3 control-label text-left">{__("Registration deadline")} (*)</label>
                        <div class='col-sm-4'>
                            <div class='input-group date' id='registration_deadlinepicker'>
                                <input type='text' name="registration_deadline" value="{$data['registration_deadline']}" class="form-control registration_deadline" placeholder="{__("Registration deadline")}"/>
                                <span class="input-group-addon">
                                    <span class="fas fa-calendar-alt"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group {if ($data['for_teacher'] || ($data['must_register'] == 0))}x-hidden{/if}" id="event_for_child">
                        <label class="col-sm-3 control-label text-left">{__("For child")}?</label>
                        <div class="col-sm-9">
                            <div class="onoffswitch">
                                <input type="checkbox" name="for_child" class="onoffswitch-checkbox" id="for_child" {if $data['for_child']=='1'}checked{/if}>
                                <label class="onoffswitch-label" for="for_child"></label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group {if ($data['for_teacher'] || ($data['must_register'] == 0))}x-hidden{/if}" id="event_for_parent">
                        <label class="col-sm-3 control-label text-left">{__("For parent")}?</label>
                        <div class="col-sm-9">
                            <div class="onoffswitch">
                                <input type="checkbox" name="for_parent" class="onoffswitch-checkbox" id="for_parent" {if $data['for_parent']=='1'}checked{/if}>
                                <label class="onoffswitch-label" for="for_parent"></label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                        <a href="{$system['system_url']}/school/{$username}/events" class="btn btn-default">{__("Lists")}</a>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    {elseif $sub_view == "add"}
        <div class="panel-body">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_event.php">
                <input type="hidden" name="school_username" id="school_username" value="{$username}"/>
                <input type="hidden" name="do" value="add"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Notification - Event")} (*)</label>
                    <div class="col-sm-9">
                        <input class="form-control" name="event_name" required autofocus maxlength="250">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Description")} (*)</label>
                    <label class="col-sm-9 control-label" style="text-align: left!important;">
                        <strong style="color: orangered">{__("Lưu ý: Muốn căn giữa cho ảnh trong mô tả, bạn vui lòng căn giữa trước khi đăng ảnh")}</strong>
                    </label>
                </div>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <textarea class="form-control" name="description" id="description" required></textarea>
                        <script>
                            CKEDITOR.replace('description');
                        </script>
                    </div>
                </div>
                {*<div class="form-group">*}
                    {*<label class="col-sm-3 control-label text-left">{__("Description")} (*)</label>*}
                    {*<div class="col-sm-9">*}
                        {*<textarea class="form-control" name="description" rows="8" required></textarea>*}
                    {*</div>*}
                {*</div>*}
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("For teachers/employees")}?</label>
                    <div class="col-sm-9">
                        <div class="onoffswitch">
                            <input type="checkbox" name="for_teacher" class="onoffswitch-checkbox" id="for_teacher">
                            <label class="onoffswitch-label" for="for_teacher"></label>
                        </div>
                    </div>
                </div>
                <div class="form-group" id = "level">
                    <label class="col-sm-3 control-label text-left">{__("Event level")} (*)</label>
                    {*<div class="col-sm-9">*}
                        <div class="col-sm-3">
                            <select name="event_level" id="event_level" class="form-control">
                                <option value="{$smarty.const.SCHOOL_LEVEL}">{__("School")}</option>
                                <option value="{$smarty.const.CLASS_LEVEL_LEVEL}">{__("Class level")}</option>
                                <option value="{$smarty.const.CLASS_LEVEL}">{__("Class")}</option>
                                {*<option value="{$smarty.const.TEACHER_LEVEL}">{__("Teacher")}</option>*}
                            </select>
                        </div>
                        <div class="col-sm-3 x-hidden" name="event_class_level">
                            <select name="class_level_id" id="class_level_id" class="form-control">
                                {foreach $class_levels as $class_level}
                                    <option value="{$class_level['class_level_id']}">{$class_level['class_level_name']}</option>
                                {/foreach}
                            </select>
                        </div>
                        <div class="col-sm-3 x-hidden" name="event_class">
                            <select name="class_id" id="class_id" class="form-control">
                                {$class_level = -1}
                                {foreach $classes as $class}
                                    {if ($class_level != $class['class_level_id'])}
                                        <option value="" disabled style="color: blue">-----{$class['class_level_name']}-----</option>
                                    {/if}
                                    <option value="{$class['group_id']}">{$class['group_title']}</option>
                                    {$class_level = $class['class_level_id']}
                                {/foreach}
                            </select>
                        </div>
                    {*</div>*}
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Post on page")}?</label>
                    <div class="col-sm-9">
                        <div class="onoffswitch">
                            <input type="checkbox" name="post_on_wall" class="onoffswitch-checkbox" id="post_on_wall" checked>
                            <label class="onoffswitch-label" for="post_on_wall"></label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Notify immediately")}?</label>
                    <div class="col-sm-9">
                        <div class="onoffswitch">
                            <input type="checkbox" name="notify_immediately" class="onoffswitch-checkbox" id="notify_immediately" checked>
                            <label class="onoffswitch-label" for="notify_immediately"></label>
                        </div>
                    </div>
                </div>
                {*<div class="form-group">*}
                    {*<label class="col-sm-3 control-label text-left">{__("Time")}</label>*}
                    {*<div class="col-sm-9">*}
                        {*<div class='col-sm-5'>*}
                            {*<div class='input-group date' id='beginpicker'>*}
                                {*<input type='text' name="begin" class="form-control" placeholder="{__("Begin")}"/>*}
                                    {*<span class="input-group-addon">*}
                                        {*<span class="fa fa-calendar"></span>*}
                                    {*</span>*}
                            {*</div>*}
                        {*</div>*}

                        {*<div class='col-md-5'>*}
                            {*<div class='input-group date' id='endpicker'>*}
                                {*<input type='text' name="end" class="form-control" placeholder="{__("End")}"/>*}
                                    {*<span class="input-group-addon">*}
                                        {*<span class="fa fa-calendar"></span>*}
                                    {*</span>*}
                            {*</div>*}
                        {*</div>*}
                    {*</div>*}
                {*</div>*}
                <div class="form-group" id="event_more_information_button">
                    <div class="col-sm-12">
                        <a href="#" class="btn btn-default pull-right js_event-more-information">{__("More information")}</a>
                    </div>
                </div>
                <div class="x-hidden" id="event_more_information">
                    {*<div class="form-group">*}
                        {*<label class="col-sm-3 control-label text-left">{__("Event location")}</label>*}
                        {*<div class="col-sm-9">*}
                            {*<input class="form-control" name="location" value="{$school['address']}" maxlength="250">*}
                        {*</div>*}
                    {*</div>*}
                    {*<div class="form-group">*}
                        {*<label class="col-sm-3 control-label text-left">{__("Participation cost")}</label>*}
                        {*<div class="col-sm-9">*}
                            {*<div class="input-group col-md-5">*}
                                {*<input type="number" min="0" step="1000" class="form-control" name="price" value="0" maxlength="10">*}
                                {*<span class="input-group-addon">{$smarty.const.MONEY_UNIT}</span>*}
                            {*</div>*}
                        {*</div>*}
                    {*</div>*}

                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Must register")}?</label>
                        <div class="col-sm-9">
                            <div class="onoffswitch">
                                <input type="checkbox" name="must_register" class="onoffswitch-checkbox" id="must_register">
                                <label class="onoffswitch-label" for="must_register"></label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group x-hidden" id="event_registration_deadline">
                        <label class="col-sm-3 control-label text-left">{__("Registration deadline")} (*)</label>
                        <div class='col-sm-4'>
                            <div class='input-group date' id='registration_deadlinepicker'>
                                <input type='text' name="registration_deadline" class="form-control registration_deadline" placeholder="{__("Registration deadline")}"/>
                                <span class="input-group-addon">
                                    <span class="fas fa-calendar-alt"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group x-hidden" id="event_for_child">
                        <label class="col-sm-3 control-label text-left">{__("For child")}?</label>
                        <div class="col-sm-9">
                            <div class="onoffswitch">
                                <input type="checkbox" name="for_child" class="onoffswitch-checkbox" id="for_child">
                                <label class="onoffswitch-label" for="for_child"></label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group x-hidden" id="event_for_parent">
                        <label class="col-sm-3 control-label text-left">{__("For parent")}?</label>
                        <div class="col-sm-9">
                            <div class="onoffswitch">
                                <input type="checkbox" name="for_parent" class="onoffswitch-checkbox" id="for_parent">
                                <label class="onoffswitch-label" for="for_parent"></label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    {/if}
</div>