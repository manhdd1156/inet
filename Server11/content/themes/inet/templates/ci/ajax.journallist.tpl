<div class="panel panel-default panel-photos">
    <div class="panel-body">
        {if count($results) > 0}
            {foreach $results as $row}
                <div class="row" style="margin-bottom: 20px">
                    <div class="journal_album">
                        <div class="col-sm-12 mb5">
                            <div class="diary_date"><i class="fas fa-calendar-check" aria-hidden="true"></i> <strong>{$row['created_at']} ({count($row['pictures'])} {__("photo")})</strong> - <span class="caption_show">{$row['caption']}</span></div>
                            <div class="caption_edit  x-hidden">
                                <textarea type="text" class="caption_{$row['child_journal_album_id']} js_child-journal-edit-caption_change" name="caption_{$row['child_journal_album_id']}" data-id="{$row['child_journal_album_id']}" data-child="{$child_parent_id}" data-handle="edit_caption">{$row['caption']}</textarea>
                                <a href="#" class="js_child-journal-edit-caption" data-id="{$row['child_journal_album_id']}" data-child="{$child_parent_id}" data-handle="edit_caption"><i class="fa fa-save" aria-hidden="true"></i> </a>
                            </div>
                            <div class="pull-right flip">
                                <button type="button" class="btn btn-default btn-xs" data-toggle="modal" data-url="ci/bo/modal.php?do=add_photos&id={$row['child_journal_album_id']}">
                                    <i class="fa fa-plus-circle"></i> {__("Add Photos")}
                                </button>
                                <button class="btn btn-xs btn-default edit_caption">{__("Edit caption")}</button>
                                <button class="btn btn-xs btn-default show_or_hidden_diary" data-id="{$row['child_journal_album_id']}">{__("Show/hide")}</button>
                                <button class="btn btn-xs btn-danger js_child-journal-delete-album" data-id="{$row['child_journal_album_id']}" data-child="{$child_parent_id}" data-handle="delete_album">{__("Delete Album")}</button>
                            </div>
                        </div>
                        <div class="album_images_{$row['child_journal_album_id']}">
                            {foreach $row['pictures'] as $photo}
                                {include file='__feeds_photo_child.tpl' _context="photos" _small=true}
                                {$day = $photo['created_at']}
                            {/foreach}
                        </div>
                    </div>
                </div>
            {/foreach}
        {else}
            <div class="" style = "font-weight: bold; text-align: center">
                {__("No data")}
            </div>
        {/if}
    </div>
</div>
