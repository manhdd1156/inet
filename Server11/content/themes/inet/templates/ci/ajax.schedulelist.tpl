<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover js_dataTable">
        <thead>
        <tr><th colspan="7">{__("Schedule list")}&nbsp;(<span class="count_schedule">{$results['schedule']|count}</span>)</th></tr>
            <tr>
                <th>
                    {__("#")}
                </th>
                <th>
                    {__("Schedule name")}
                </th>
                <th>
                    {__("Begin")}
                </th>
                <th>
                    {__("Scope")}
                </th>
                <th>
                    {__("Use category")}
                </th>
                <th>
                    {__("Study saturday")}
                </th>
                <th>
                    {__("Actions")}
                </th>
            </tr>
        </thead>
        <tbody>
        {$idx = 1}
        {foreach $results['schedule'] as $row}
            <tr>
                <td align="center">
                    {$idx}
                </td>
                <td>
                    <a href="{$system['system_url']}/school/{$results['username']}/schedules/detail/{$row['schedule_id']}">{$row['schedule_name']}</a>
                </td>
                <td align="center">
                    {$row['begin']}
                </td>
                <td align="center">
                    {if $row['applied_for']==$smarty.const.SCHOOL_LEVEL}
                        {__("School")}
                    {elseif $row['applied_for']==$smarty.const.CLASS_LEVEL_LEVEL}
                        {foreach $class_levels as $cl}
                            {if $cl['class_level_id'] == $row['class_level_id']}{$cl['class_level_name']}{/if}
                        {/foreach}
                    {elseif $row['applied_for']==$smarty.const.CLASS_LEVEL}
                        {foreach $classes as $value}
                            {if $value['group_id'] == $row['class_id']}{$value['group_title']}{/if}
                        {/foreach}
                    {/if}
                </td>
                <td align="center">
                    {if $row['is_category']}
                        {__('Yes')}
                    {else}
                        {__('No')}
                    {/if}
                </td>
                <td align="center">
                    {if $row['is_saturday']}
                        {__('Yes')}
                    {else}
                        {__('No')}
                    {/if}
                </td>
                <td>
                    {if $canEdit}
                        <a href="{$system['system_url']}/school/{$results['username']}/schedules/copy/{$row['schedule_id']}" class="btn btn-xs btn-default">{__("Copy")}</a>
                        <a href="{$system['system_url']}/school/{$results['username']}/schedules/edit/{$row['schedule_id']}" class="btn btn-xs btn-default">{__("Edit")}</a>
                        <button class="btn btn-xs btn-danger js_school-schedule-delete" data-username="{$results['username']}" data-id="{$row['schedule_id']}" data-handle = "delete_schedule">{__("Delete")}</button>
                        {if !$row['is_notified']}
                            <div class = "form-label"><button class="btn btn-xs btn-default js_school-schedule-notify" data-handle="notify" data-username="{$results['username']}" data-id="{$row['schedule_id']}">{__("Notify")}</button></div>
                        {/if}
                    {/if}
                    <button class="btn btn-xs btn-success js_school-schedule-export" data-username="{$results['username']}" data-id="{$row['schedule_id']}" data-handle = "export">{__("Export to Excel")}</button>
                </td>
            </tr>
            {$idx = $idx + 1}
        {/foreach}

        {if $results['schedule']|count == 0}
            <tr class="odd">
                <td valign="top" align="center" colspan="7" class="dataTables_empty">
                    {__("No data available in table")}
                </td>
            </tr>
        {/if}

        </tbody>
    </table>
</div>
<script>
    $(function() {
        // run DataTable
        $('.js_dataTable').DataTable({
            "aoColumnDefs": [ { 'aDataSort': false, 'aTargets': [ -1 ] } ],
            "language": {
                "decimal":        "",
                "emptyTable":     __["No data available in table"],
                "info":           __["Showing _START_ to _END_ of _TOTAL_ results"],
                "infoEmpty":      __["Showing 0 to 0 of 0 results"],
                "infoFiltered":   "(filtered from _MAX_ total entries)",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     __["Show"] + " " + "_MENU_" + " " + __["results"],
                "loadingRecords": __["Loading..."],
                "processing":     __["Processing..."],
                "search":         __["Search"],
                "zeroRecords":    __["No matching records found"],
                "paginate": {
                    "first":      __["First"],
                    "last":       __["Last"],
                    "next":       __["Next"],
                    "previous":   __["Previous"]
                },
                "aria": {
                    "sortAscending":  ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                }
            }
        });
    });
</script>