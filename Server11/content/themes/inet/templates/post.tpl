{include file='_head.tpl'}
{include file='_header.tpl'}

<!-- page content -->
<div class="container mt20 height_min"> <!-- class height_min để set height tối thiểu của container -->
	<div class="row">

		<!-- left panel -->
		<div class="col-sm-2">
            {*{include file='__feeds_post.tpl' standalone=true}*}
		</div>
		<!-- left panel -->
		<!-- center panel -->
		<div class="col-sm-6">
		{include file='__feeds_post.tpl' standalone=true}
		</div>
		<!-- center panel-->


		<!-- right panel -->
		<div class="col-sm-4">
		{include file='_ads.tpl'}
		{include file='_widget.tpl'}
		</div>
		<!-- right panel -->

	</div>
</div>
<!-- page content -->

{include file='_footer.tpl'}