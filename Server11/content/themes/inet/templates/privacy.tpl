{include file = "_head_new.tpl"}
{include file = '_header_new.tpl'}

<!--=== Breadcrumbs ===-->
<div class="breadcrumbs">
	<div class="container">
		<h1 class="pull-left">{__("Privacy")}</h1>
		<ul class="pull-right breadcrumb">
			<li><a href="{$system['system_url']}">{__("Home")}</a></li>
			<li class="active">{__("Privacy")}</li>
		</ul>
	</div>
</div><!--/breadcrumbs-->
<!--=== End Breadcrumbs ===-->

<!--=== Content Part ===-->
<div class="container content">
	<div class="row-fluid privacy">
		<p class="lead"><em>1. THÔNG TIN CHÚNG TÔI THU THẬP</em></p>
		<p>Chúng tôi thu thập các loại thông tin sau: </p>
		<p style = "font-weight: bold;"> Thông tin bạn cung cấp trực tiếp cho chúng tôi: </p>
		<ul>
			<li>
				Tên người dùng, mật khẩu và địa chỉ email của bạn khi bạn đăng ký tài khoản Inet.
			</li>
			<li>
				Thông tin tiểu sử bạn cung cấp cho hồ sơ người dùng (ví dụ: họ và tên, ảnh, số điện thoại). Thông tin này cho phép chúng tôi giúp bạn hoặc những người khác "được tìm thấy" trên Inet.
			</li>
			<li>
				Nội dung của người dùng (ví dụ: ảnh, bình luận và các tài liệu khác) bạn đăng lên Dịch vụ.
			</li>
			<li>
				Liên lạc tài khoản của bạn. Ví dụ: chúng tôi có thể gửi cho bạn email liên quan đến Dịch vụ (ví dụ: xác minh tài khoản, thay đổi/cập nhật các tính năng của Dịch vụ, thông báo kỹ thuật và bảo mật). Lưu ý rằng bạn không thể bỏ nhận email liên quan đến Dịch vụ.
			</li>
		</ul>
		<p style = "font-weight: bold;"> Thông tin phân tích: </p>
		<ul>
			<li>
				Chúng tôi sử dụng các công cụ phân tích của bên thứ ba để giúp chúng tôi đánh giá xu hướng sử dụng và lưu lượng truy cập Dịch vụ. Những công cụ này sẽ thu thập thông tin được thiết bị của bạn hoặc Dịch vụ của chúng tôi gửi, bao gồm trang web bạn truy cập, tiện ích mở rộng và những thông tin khác hỗ trợ chúng tôi cải thiện Dịch vụ. Chúng tôi sẽ thu thập và sử dụng thông tin phân tích này cùng thông tin phân tích từ những Người dùng khác để không thể sử dụng một cách hợp lý nhằm xác định bất kỳ Người dùng riêng cụ thể nào.
			</li>
		</ul>
		<p style = "font-weight: bold;"> Cookie và các công nghệ tương tự: </p>
		<ul>
			<li>
				Khi bạn truy cập Dịch vụ, chúng tôi có thể sử dụng cookie và các công nghệ tương tự như pixel, điểm ảnh trang web và bộ nhớ cục bộ để thu thập thông tin về cách bạn sử dụng các tính năng trên Inet cũng như cung cấp cho bạn các tính năng.
			</li>
			<li>
				Chúng tôi có thể yêu cầu các nhà quảng cáo hoặc đối tác khác phân phối quảng cáo hoặc dịch vụ đến các thiết bị của bạn. Họ có thể sử dụng cookie hoặc các công nghệ tương tự được chúng tôi hoặc bên thứ ba đưa vào.
			</li>
		</ul>
		<p style = "font-weight: bold;"> Thông tin tệp nhật ký: </p>
		<ul>
			<li>
				Thông tin tệp nhật ký được trình duyệt của bạn tự động báo cáo mỗi khi bạn đưa ra yêu cầu truy cập (ví dụ: truy cập) trang web hoặc ứng dụng. Thông tin này cũng có thể được cung cấp khi nội dung của trang web hoặc ứng dụng được tải xuống trình duyệt hoặc thiết bị của bạn.
			</li>
			<li>
				Khi bạn sử dụng Dịch vụ của chúng tôi, các máy chủ của chúng tôi sẽ tự động ghi lại thông tin tệp nhật ký, bao gồm yêu cầu web, địa chỉ Giao thức Internet ("IP"), loại trình duyệt, URL và trang thoát/giới thiệu, số lần nhấp và cách bạn tương tác với liên kết trên Dịch vụ, tên miền, trang đích, trang đã xem và thông tin tương tự khác. Chúng tôi cũng có thể thu thập thông tin tương tự từ email được gửi đến những Người dùng của mình giúp chúng tôi theo dõi email nào đã được mở cũng như liên kết nào đã được người nhận nhấp vào. Thông tin này cho phép báo cáo chính xác hơn và cải thiện Dịch vụ.
			</li>
		</ul>
		<p style = "font-weight: bold;"> Mã nhận dạng thiết bị: </p>
		<ul>
			<li>
				Khi bạn sử dụng thiết bị di động như máy tính bảng hoặc điện thoại để truy cập Dịch vụ của chúng tôi, chúng tôi có thể truy cập, thu thập, giám sát, lưu trữ trên thiết bị của bạn và/hoặc lưu trữ từ xa một hoặc nhiều "mã nhận dạng thiết bị". Mã nhận dạng thiết bị là những tệp dữ liệu nhỏ hoặc cấu trúc dữ liệu tương tự trên hoặc được liên kết với thiết bị di động của bạn. Mã nhận dạng thiết bị nhận dạng duy nhất thiết bị di động của bạn. Mã nhận dạng thiết bị có thể là dữ liệu được lưu trữ liên quan đến phần cứng thiết bị, dữ liệu được lưu trữ liên quan đến hệ điều hành hoặc phần mềm khác của thiết bị hoặc dữ liệu được Instagram gửi đến thiết bị của bạn.
			</li>
			<li>
				Mã nhận dạng thiết bị có thể cung cấp thông tin cho chúng tôi hoặc cho đối tác bên thứ ba về cách bạn duyệt web và sử dụng Dịch vụ cũng như có thể giúp chúng tôi hoặc những người khác cung cấp báo cáo hoặc nội dung và quảng cáo được cá nhân hóa. Một số tính năng của Dịch vụ có thể không hoạt động đúng nếu việc sử dụng hoặc tính khả dụng của mã nhận dạng thiết bị bị hỏng hoặc bị vô hiệu hóa.
			</li>
		</ul>
		<p style = "font-weight: bold;"> Siêu dữ liệu: </p>
		<ul>
			<li>
				Siêu dữ liệu thường là dữ liệu kỹ thuật được liên kết với Nội dung của người dùng. Ví dụ: Siêu dữ liệu có thể mô tả cách thức, thời điểm và người đã thu thập đoạn Nội dung của người dùng cũng như cách thức nội dung đó được định dạng.
			</li>
			<li>
				Người dùng có thể thêm hoặc có thể đã được thêm Siêu dữ liệu vào Nội dung của người dùng của họ bao gồm hashtag (ví dụ: để đánh dấu từ khóa khi bạn đăng ảnh), thẻ địa lý (ví dụ: để đánh dấu vị trí của bạn vào ảnh), bình luận hoặc dữ liệu khác. Điều này sẽ giúp Nội dung của người dùng của bạn dễ tìm kiếm hơn với người khác cũng như mang tính tương tác hơn. Nếu bạn gắn thẻ địa lý ảnh hoặc gắn thẻ ảnh bằng API của người khác thì kinh độ và vĩ độ sẽ được lưu trữ với ảnh và có thể tìm kiếm được (ví dụ: qua tính năng vị trí hoặc bản đồ) nếu ảnh của bạn được bạn đặt công khai theo cài đặt quyền riêng tư của bạn.
			</li>
		</ul>
		<p class="lead"><em>2. CÁCH CHÚNG TÔI SỬ DỤNG THÔNG TIN CỦA BẠN</em></p>
		<p>
			Ngoài một số việc sử dụng cụ thể đối với thông tin chúng tôi mô tả trong Chính sách quyền riêng tư này, chúng tôi có thể sử dụng thông tin mà mình nhận được để: </p>
		<ul>
			<li>
				Giúp bạn truy cập một cách hiệu quả vào thông tin của mình sau khi đăng nhập;
			</li>
			<li>
				Nhớ thông tin để bạn không phải nhập lại trong quá trình truy cập hoặc lần tới bạn truy cập Dịch vụ;
			</li>
			<li>
				Cung cấp thông tin và nội dung được cá nhân hóa cho bạn và người khác. Thông tin và nội dung này có thể bao gồm quảng cáo trực tuyến hoặc các hình thức tiếp thị khác;
			</li>
			<li>
				Cung cấp, cải thiện, thử nghiệm và giám sát tính hiệu quả của Dịch vụ của chúng tôi;
			</li>
			<li>
				Phát triển và thử nghiệm các sản phẩm cũng như tính năng mới;
			</li>
			<li>
				Giám sát số liệu như tổng số khách truy cập, lưu lượng truy cập và mẫu nhân khẩu học;
			</li>
			<li>
				Chẩn đoán hoặc khắc phục các sự cố kỹ thuật.
			</li>
		</ul>
		<p class="lead"><em>3. CHIA SẺ THÔNG TIN CỦA BẠN</em></p>
		<p>Chúng tôi sẽ KHÔNG cho thuê hoặc bán thông tin của bạn cho bên thứ ba.</p>
		<p style = "font-weight: bold">Các bên mà chúng tôi có thể chia sẻ thông tin của bạn:</p>
		<ul>
			<li>
				Chúng tôi có thể chia sẻ Nội dung của người dùng và thông tin của bạn (bao gồm nhưng không giới hạn ở thông tin từ cookie, tệp nhật ký, mã nhận dạng thiết bị, dữ liệu vị trí và dữ liệu sử dụng) với những doanh nghiệp là một bộ phận hợp pháp của một công ty. Bộ phận có thể sử dụng thông tin này để giúp cung cấp, hiểu và cải thiện Dịch vụ (bao gồm bằng cách cung cấp thông tin phân tích) và các dịch vụ của chính Bộ phận (bao gồm bằng cách cung cấp cho bạn trải nghiệm tốt hơn và phù hợp hơn). Nhưng những Bộ phận này sẽ tôn trọng lựa chọn của bạn về người có thể xem ảnh của bạn.
			</li>
			<li>
				Chúng tôi cũng có thể chia sẻ thông tin của bạn cũng như thông tin từ các công cụ như cookie, tệp nhật ký, mã nhận dạng thiết bị và dữ liệu vị trí với các tổ chức bên thứ ba giúp chúng tôi cung cấp Dịch vụ cho bạn ("Nhà cung cấp dịch vụ"). Nhà cung cấp dịch vụ của chúng tôi sẽ được cấp quyền truy cập vào thông tin của bạn ở mức độ phù hợp cần thiết để cung cấp Dịch vụ theo các điều khoản bí mật hợp lý.
			</li>
			<li>
				Chúng tôi cũng có thể chia sẻ thông tin nhất định như dữ liệu cookie với đối tác quảng cáo bên thứ ba. Thông tin này sẽ cho phép các mạng quảng cáo của bên thứ ba thực hiện nhiều việc, trong đó có phân phối quảng cáo được nhắm mục tiêu mà họ tin rằng bạn sẽ quan tâm nhất.
			</li>
			<li>
				Chúng tôi có thể xóa các phần dữ liệu có thể nhận dạng bạn và chia sẻ dữ liệu được ẩn danh với các bên khác. Chúng tôi cũng có thể kết hợp thông tin của bạn những thông tin khác theo cách không còn được liên kết với bạn và chia sẻ thông tin được tổng hợp đó.
			</li>
		</ul>
		<p style = "font-weight: bold">Các bên mà bạn có thể chọn chia sẻ Nội dung của người dùng của mình:</p>
		<ul>
			<li>
				Bất cứ thông tin hoặc nội dung nào bạn tự nguyện tiết lộ để đăng lên Dịch vụ, chẳng hạn như Nội dung của người dùng, được hiển thị cho mọi người, được kiểm soát theo bất kỳ cài đặt quyền riêng tư có thể áp dụng nào mà bạn đã đặt. Để thay đổi cài đặt quyền riêng tư trên Dịch vụ, vui lòng thay đổi cài đặt trang cá nhân của bạn. Sau khi bạn đã chia sẻ Nội dung của người dùng hoặc đặt nội dung đó thành công khai, người khác có thể chia sẻ lại Nội dung của người dùng đó.
			</li>
		</ul>
		<p style = "font-weight: bold">Các bên mà bạn có thể chọn chia sẻ Nội dung của người dùng của mình:</p>
		<ul>
			<li>
				Chúng tôi có thể truy cập, bảo vệ và chia sẻ thông tin của bạn khi phản hồi các yêu cầu pháp lý (như lệnh khám, lệnh tòa hoặc trát đòi hầu tòa) nếu chúng tôi tin rằng luật yêu cầu chúng tôi làm như vậy. Chúng tôi cũng có thể truy cập, bảo vệ và chia sẻ thông tin khi chúng tôi tin rằng điều đó là cần thiết để: phát hiện, ngăn chặn và giải quyết gian lận và hoạt động phi pháp khác; để tự bảo vệ chúng tôi, bạn và người khác, bao gồm như một phần của việc điều tra; và để ngăn ngừa tử vong hoặc thương tích sắp xảy ra với cơ thể. Thông tin chúng tôi nhận được về bạn có thể được truy cập, xử lý và lưu giữ trong một khoảng thời gian dài khi đó là đối tượng của yêu cầu, nghĩa vụ pháp lý, điều tra của chính phủ hoặc điều tra liên quan đến khả năng vi phạm các điều khoản hoặc chính sách của chúng tôi hoặc nhằm ngăn ngừa thiệt hại.
			</li>
		</ul>
		<p class="lead"><em>4. CÁCH CHÚNG TÔI LƯU TRỮ THÔNG TIN CỦA BẠN</em></p>
		<p style = "font-weight: bold">Lưu trữ và xử lý:</p>
		<ul>
			<li>
				Thông tin của bạn được thu thập qua Dịch vụ có thể được lưu trữ và xử lý tại Việt Nam.
			</li>
			<li>
				Chúng tôi sử dụng các biện pháp bảo vệ hợp lý về mặt thương mại giúp giữ bảo mật thông tin được thu thập qua Dịch vụ cũng như thực hiện các bước hợp lý (chẳng hạn như yêu cầu mật khẩu duy nhất) để xác minh danh tính của bạn trước khi cấp cho bạn quyền truy cập vào tài khoản của mình.
			</li>
			<li>
				Hãy góp phần trợ giúp chúng tôi. Bạn có trách nhiệm duy trì tính bí mật của thông tin tài khoản và mật khẩu duy nhất của mình cũng như có trách nhiệm kiểm soát quyền truy cập tại mọi thời điểm.
			</li>
		</ul>
		<p class="lead"><em>5. LỰA CHỌN THÔNG TIN CỦA BẠN</em></p>
		<p style = "font-weight: bold">Thông tin tài khoản và cài đặt trang cá nhân/quyền riêng tư của bạn:</p>
		<ul>
			<li>
				Cập nhật tài khoản bất kỳ lúc nào bằng cách đăng nhập và thay đổi cài đặt trang cá nhân của bạn.
			</li>
			<li>
				Hủy đăng ký liên lạc qua email từ chúng tôi bằng cách nhấp vào "liên kết hủy đăng ký" có trong liên lạc đó. Như được nêu ở trên, bạn không thể bỏ nhận liên lạc liên quan đến Dịch vụ (ví dụ: xác minh tài khoản, xác nhận mua hàng và thanh toán cũng như lời nhắc, thay đổi/cập nhật các tính năng của Dịch vụ, thông báo kỹ thuật và bảo mật).
			</li>
		</ul>
		<p class="lead"><em>6. DỊCH VỤ VÀ TRANG WEB KHÁC</em></p>
		<p>
			Chúng tôi không chịu trách nhiệm đối với những thực tiễn được triển khai bởi bất kỳ trang web hoặc dịch vụ nào được liên kết đến hoặc từ Dịch vụ của chúng tôi, bao gồm thông tin hoặc nội dung có trong đó. Xin lưu ý rằng khi bạn sử dụng liên kết để đi từ Dịch vụ của chúng tôi đến trang web hoặc dịch vụ khác, Chính sách quyền riêng tư của chúng tôi không áp dụng đối với những trang web hoặc dịch vụ bên thứ ba đó. Việc duyệt web và tương tác của bạn trên bất kỳ trang web hoặc dịch vụ bên thứ ba nào, bao gồm trang web hoặc dịch vụ có liên kết trên trang web của chúng tôi, phải tuân theo các quy tắc và chính sách riêng của bên thứ ba đó. Ngoài ra, bạn đồng ý rằng chúng tôi không có trách nhiệm và không có quyền kiểm soát đối với bất kỳ bên thứ ba nào mà bạn cho phép truy cập vào Nội dung của người dùng của mình. Nếu bạn đang sử dụng trang web hoặc dịch vụ bên thứ ba và bạn cho phép trang web hoặc dịch vụ đó truy cập vào Nội dung của người dùng của mình, bạn phải tự chịu rủi ro khi thực hiện việc đó.
		</p>
		<p class="lead"><em>7. CÁCH LIÊN HỆ VỚI CHÚNG TÔI</em></p>
		<p>
			Nếu bạn có bất kỳ câu hỏi nào về Chính sách quyền riêng tư này hoặc về Dịch vụ, vui lòng tìm kênh hỗ trợ thích hợp trong Trung tâm trợ giúp để liên hệ với chúng tôi.
		</p>
	</div><!--/row-fluid-->
</div><!--/container-->
<!--=== End Content Part ===-->

{include file = '_footer_new.tpl'}