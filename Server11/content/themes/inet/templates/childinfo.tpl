{include file='_head.tpl'}
{include file='_header.tpl'}
<div class="container mt20 height_min"> <!-- class height_min để set height tối thiểu của container -->
    <div class="row">
        <div class="col-md-3 col-sm-3">
            <nav class="navbar navbar-default" role="navigation">
                <div class="panel panel-default">
                    <div class = "menu_school">
                        <!-- Dashbroad -->
                        {if $view == ""}
                            {*<i class="fa fa-tachometer fa-fw fa-lg pr10"></i> {__("Dashboard")} [{$child['child_name']}]*}
                        {elseif $view == "childdetail"}
                            <i class="fa fa-info-circle fa-lg fa-fw pr10"></i> {__("Student information detail")}
                        {elseif $view == "health"}
                            <i class="fa fa-medkit fa-fw fa-lg pr10"></i> {__("Health information")}
                        {elseif $view == "medicals"}
                            <i class="fa fa-user-md fa-lg fa-fw pr10"></i> {__("Medical report book")}
                        {elseif $view == "journals"}
                            <i class="fa fa-file-image fa-lg fa-fw pr10"></i> {__("Journal corner")}
                        {elseif $view == "childdevelopments"}
                            <i class="fa fa-file-image fa-lg fa-fw pr10"></i> {__("Child development")}
                        {elseif $view == "foetusdevelopments"}
                            <i class="fa fa-file-image fa-lg fa-fw pr10"></i> {__("Foetus development")}
                        {elseif $view == "childmonths"}
                            <i class="fa fa-share fa-lg fa-fw pr10"></i> {__("Foetus knowledge")}
                        {elseif $view == "pregnancys"}
                            <i class="fa fa-paper-plane fa-lg fa-fw pr10"></i> {__("Foetus knowledge")}
                        {/if}
                    </div>
                    <div class="navbar-header">

                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    {if count($schoolList) > 0 || count($classList) > 0 || count($childrenList) > 0}
                        <div class="select_object_box">
                            <select name="object" id="select_object">
                                {if count($schoolList) > 0}
                                    <option disabled>----- {__("School")} -----</option>
                                    {foreach $schoolList as $school}
                                        <option data-type="school" value="{$school['page_name']}" {if $school['page_name']==$username}selected{/if}>{$school['page_title']}</option>
                                    {/foreach}
                                {/if}
                                {if count($classList) > 0}
                                    <option disabled>----- {__("Class")} -----</option>
                                    {foreach $classList as $class}
                                        <option data-type="class" value="{$class['group_name']}" {if $class['group_name']==$username}selected{/if}>{$class['group_title']}</option>
                                    {/foreach}a
                                {/if}
                                {if count($childrenList) > 0}
                                    <option disabled>----- {__("Child")} -----</option>
                                    {foreach $childrenList as $childOb}
                                        <option {if $childOb['school_id'] > 0}data-type="child" value="{$childOb['child_id']}" {if $childOb['child_id'] == $child['child_id']}selected{/if} {else}data-type="childinfo" value="{$childOb['child_parent_id']}" {if $childOb['child_parent_id'] == $child['child_parent_id']}selected{/if}{/if}>{$childOb['child_name']}</option>
                                    {/foreach}
                                {/if}
                            </select>
                        </div>
                    {/if}
                    <div class="panel-body with-nav collapse navbar-collapse" id="collapse">
                        <ul class="side-nav metismenu js_metisMenu">
                            <li {if $view == "childdetail"}class="active"{/if}>
                                <a href="{$system['system_url']}/childinfo/{$child['child_parent_id']}/childdetail">
                                    <i class="fa fa-info-circle fa-lg fa-fw pr10"></i> {if !$child['is_pregnant']}{__("Student information detail")}{else}{__("Fetus information detail")}{/if}
                                </a>
                            </li>
                            <li {if $view == "health"}class="active"{/if}>
                                <a href="{$system['system_url']}/childinfo/{$child['child_parent_id']}/health">
                                    <i class="fa fa-medkit fa-lg fa-fw pr10"></i> {__("Health information")}
                                </a>
                                {*<ul>*}
                                    {*<li {if $view == "health" && $sub_view == ""} class="active selected"{/if}>*}
                                        {*<a href="{$system['system_url']}/childinfo/{$child['child_parent_id']}/health">*}
                                        {*<i class="fa fa-caret-right fa-fw pr10"></i> {__("Lists")}*}
                                        {*</a>*}
                                    {*</li>*}
                                    {*{if !$child['is_pregnant']}*}
                                        {*<li {if $view == "health" && $sub_view == "edit"} class="active selected" {/if}>*}
                                            {*<a href="{$system['system_url']}/childinfo/{$child['child_parent_id']}/health/edit">*}
                                                {*<i class="fa fa-caret-right fa-fw pr10"></i> {__("Edit information health")}*}
                                            {*</a>*}
                                        {*</li>*}
                                    {*{/if}*}
                                {*</ul>*}
                            </li>
                            {if !$child['is_pregnant']}
                                <li {if $view == "medicals"}class="active"{/if}>
                                    <a href="{$system['system_url']}/childinfo/{$child['child_parent_id']}/medicals">
                                        <i class="fa fa-user-md fa-lg fa-fw pr10"></i> {__("Medical report book")}
                                        <span class="fa arrow"></span>
                                    </a>
                                    <ul>
                                        <li {if $view == "medicals" && $sub_view == ""} class="active selected"{/if}>
                                            <a href="{$system['system_url']}/childinfo/{$child['child_parent_id']}/medicals">
                                                <i class="fa fa-caret-right fa-fw pr10"></i> {__("Lists")}
                                            </a>
                                        </li>
                                        <li {if $view == "medicals" && $sub_view == "add"} class="active selected" {/if}>
                                            <a href="{$system['system_url']}/childinfo/{$child['child_parent_id']}/medicals/add">
                                                <i class="fa fa-caret-right fa-fw pr10"></i> {__("Add New")}
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li {if $view == "journals"}class="active"{/if}>
                                    <a href="{$system['system_url']}/childinfo/{$child['child_parent_id']}/journals">
                                        <i class="fa fa-file-image fa-lg fa-fw pr10"></i> {__("Journal corner")}
                                        <span class="fa arrow"></span>
                                    </a>
                                    <ul>
                                        <li {if $view == "journals" && $sub_view == ""} class="active selected"{/if}>
                                            <a href="{$system['system_url']}/childinfo/{$child['child_parent_id']}/journals">
                                                <i class="fa fa-caret-right fa-fw pr10"></i> {__("Lists")}
                                            </a>
                                        </li>
                                        <li {if $view == "journals" && $sub_view == "add"} class="active selected" {/if}>
                                            <a href="{$system['system_url']}/childinfo/{$child['child_parent_id']}/journals/add">
                                                <i class="fa fa-caret-right fa-fw pr10"></i> {__("Add New")}
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li {if $view == "childdevelopments"}class="active"{/if}>
                                    <a href="{$system['system_url']}/childinfo/{$child['child_parent_id']}/childdevelopments">
                                        <i class="fas fa-chart-line fa-lg fa-fw pr10"></i> {__("Child development")}
                                        <span class="fa arrow"></span>
                                    </a>
                                    <ul>
                                        <li {if $view == "childdevelopments" && $sub_view == ""} class="active selected"{/if}>
                                            <a href="{$system['system_url']}/childinfo/{$child['child_parent_id']}/childdevelopments">
                                                <i class="fa fa-caret-right fa-fw pr10"></i> {__("Vaccination schedule")}
                                            </a>
                                        </li>
                                        <li {if $view == "childdevelopments" && $sub_view == "referion"} class="active selected" {/if}>
                                            <a href="{$system['system_url']}/childinfo/{$child['child_parent_id']}/childdevelopments/referion">
                                                <i class="fa fa-caret-right fa-fw pr10"></i> {__("Referion information")}
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li {if $view == "childmonths"}class="active"{/if}>
                                    <a href="{$system['system_url']}/childinfo/{$child['child_parent_id']}/childmonths">
                                        <i class="fa fa-share fa-lg fa-fw pr10"></i> {__("Month age information")}
                                    </a>
                                </li>
                            {/if}
                            {if $child['is_pregnant']}
                                <li {if $view == "foetusdevelopments"}class="active"{/if}>
                                    <a href="{$system['system_url']}/childinfo/{$child['child_parent_id']}/foetusdevelopments">
                                        <i class="fa fa-line-chart fa-lg fa-fw pr10"></i> {__("Foetus development")}
                                    </a>
                                    <ul>
                                        <li {if $view == "foetusdevelopments" && $sub_view == ""} class="active selected"{/if}>
                                            <a href="{$system['system_url']}/childinfo/{$child['child_parent_id']}/foetusdevelopments">
                                                <i class="fa fa-caret-right fa-fw pr10"></i> {__("Pregnancy check-up")}
                                            </a>
                                        </li>
                                        <li {if $view == "childdevelopments" && $sub_view == "referion"} class="active selected" {/if}>
                                            <a href="{$system['system_url']}/childinfo/{$child['child_parent_id']}/foetusdevelopments/referion">
                                                <i class="fa fa-caret-right fa-fw pr10"></i> {__("Referion information")}
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li {if $view == "pregnancys"}class="active"{/if}>
                                    <a href="{$system['system_url']}/childinfo/{$child['child_parent_id']}/pregnancys">
                                        <i class="fa fa-paper-plane fa-lg fa-fw pr10"></i> {__("Pregnancy information")}
                                    </a>
                                </li>
                            {/if}
                        </ul>
                    </div>
                </div>
            </nav>
        </div>

        <div class="col-md-9 col-sm-9">
            {include file='ci/childinfo/childinfo.help.tpl'}
        </div>
        <div class="col-md-9 col-sm-9">
            {if $view == ""}
                {include file='ci/childinfo/childinfo.dashboard.tpl'}
            {else}
                {include file="ci/childinfo/childinfo.$view.tpl"}
            {/if}
        </div>
    </div>
</div>
<!-- page content -->


{include file='_footer.tpl'}