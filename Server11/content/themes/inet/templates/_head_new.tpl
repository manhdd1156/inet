<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if gt IE 9]><!--><html class="gt-ie8 gt-ie9 not-ie" lang="{$system['language']['code']}" dir="{$system['language']['dir']}"><!--<![endif]-->
<head>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="generator" content="{$system['system_title']} | {__('School management and connection software')}">
    <meta name="version" content="{$system['system_version']}">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title -->
    <title>{$page_title|truncate:70}</title>
    <!-- Title -->

    <!-- Meta -->
    <meta name="description" content="{$page_description|truncate:300}">
    <meta name="keywords" content="{$system['system_keywords']}">
    <!-- Meta -->

    <!-- OG-Meta -->
    <meta property="og:title" content="{$page_title|truncate:70}"/>
    <meta property="og:description" content="{$page_description|truncate:300}"/>
    <meta property="og:site_name" content="{$system['system_title']}"/>
    <!-- OG-Meta -->

    <!-- OG-Image -->
    <meta property="og:image" content="{$page_image}"/>
    <!-- OG-Image -->

    <!-- Favicon -->
    <link rel="icon" href="{$system['system_url']}/content/themes/{$system['theme']}/images/favicon.png" sizes="64x75"/>

    <!-- CSS Global Compulsory -->
    {*<link rel="stylesheet" href="{$system['system_url']}/content/themes/{$system['theme']}/assets/css/style_home.css">*}

    <!-- CSS Header and Footer -->
    <link rel="stylesheet" href="{$system['system_url']}/content/themes/{$system['theme']}/assets/css/headers/header-v6.css">
    <!-- CSS Implementing Plugins -->
    <link rel="stylesheet" href="{$system['system_url']}/includes/assets/css/font-awesome/css/font-awesome.css">

    <!-- CSS Theme -->
    <!-- taila css -->
    <link rel="stylesheet" href="{$system['system_url']}/includes/assets/css/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="{$system['system_url']}/content/themes/{$system['theme']}/css/bootstrap-social.css">
        <link rel="stylesheet" href="{$system['system_url']}/content/themes/{$system['theme']}/assets/plugins/css{$system['css_path']}/web_static_css.css">
    <link rel="stylesheet" href="{$system['system_url']}/content/themes/{$system['theme']}/assets/plugins/css{$system['css_path']}/viewport.css">
    <!--end taila css -->
</head>