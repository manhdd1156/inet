{if $notification['ci']}
    <li class="feeds-item {if !$notification['seen']}unread{/if}" data-id="{$notification['notification_id']}">
        {if $notification['is_link'] == 1}
            <a class="data-container" target="_blank" href="{$notification['url']}">
        {else}
            <a class="data-container js_notification" data-action="{$notification['action']}" data-type="{$notification['node_type']}"
               data-url="{$notification['node_url']}" data-extra1="{$notification['extra1']}" data-extra2="{$notification['extra2']}"
               data-extra3="{$notification['extra3']}" data-id="{$notification['notification_id']}" href="#">
        {/if}
            <img class="data-avatar" src="{$notification['user_picture']}" alt="">
                <div class="data-content">
                    <div><span class="name">{$notification['user_fullname']}</span></div>
                    <div><i class="fas {$notification['icon']} pr5"></i> {$notification['message']}</div>
                    <div class="time js_moment" data-time="{$notification['time']}">{$notification['time']}</div>
                </div>
            </a>
    </li>
{else}
    <li class="feeds-item {if !$notification['seen']}unread{/if}" data-id="{$notification['notification_id']}">
        <a class="data-container" href="{$notification['url']}">
            <img class="data-avatar" src="{$notification['user_picture']}" alt="">
            <div class="data-content">
                <div><span class="name">{$notification['user_fullname']}</span></div>
                <div><i class="fa {$notification['icon']} pr5"></i> {$notification['message']}</div>
                <div class="time js_moment" data-time="{$notification['time']}">{$notification['time']}</div>
            </div>
        </a>
    </li>
{/if}