{include file='_head.tpl'}
{include file='_header.tpl'}

<!-- page content -->
<div class="container mt20 height_min"> <!-- class height_min để set height tối thiểu của container -->
	<div class="row">
		<div class="col-md-10 col-md-offset-1">

			<div class="wizard-header">
				<h3>
                    {*{__("BUILD")} {__("YOUR PROFILE")}<br>*}
                    {__("Register user account")|upper}<br>
                    {*<small>{__("This information will let us know more about you")}.</small>*}
				</h3>
			</div>

			<ul class="nav nav-pills nav-justified thumbnail js_wizard-steps">
				<li {if $step == 1}class="active"{/if}>
					<a href="#step-1">
						<h4 class="list-group-item-heading">{__("Step 1")}</h4>
						<p class="list-group-item-text">{__("Upload your photo")}</p>
					</a>
				</li>
				<li class="{if $step == 2}active{/if}">
					<a href="#step-2">
						<h4 class="list-group-item-heading">{__("Step 2")}</h4>
						<p class="list-group-item-text">{__("Update your info")}</p>
					</a>
				</li>
			</ul>

			<div class="well js_wizard-content" id="step-1">
				<div class="text-center">
					<h3 class="mb5">{__("Chào mừng bạn đến với Coniu")}</h3>
					<p class="mb20">{__("Let's start with your photo")}</p>
				</div>

				<!-- profile-avatar -->
				<div class="relative" style="height: 160px;">
					<div class="profile-avatar-wrapper static">
                        {*<img src="{$user->_data['user_picture']}" alt="{$user->_data['user_fullname']}">*}
						<div class="new_profile">
							<div class="article_profile">
								<div class="thumb_profile">
									<div style="height:150px; cursor: pointer"></div>
								</div>
							</div>
						</div>
						<div class="profile-avatar-change">
							<i class="fa fa-camera js_x-uploader" data-handle="picture-user"></i>
						</div>
						<div class="profile-avatar-delete">
							<i class="fa fa-trash js_delete-picture" data-handle="picture-user" title='{__("Delete Picture")}'></i>
						</div>
						<div class="profile-avatar-change-loader">
							<div class="loader loader_medium"></div>
						</div>
					</div>
				</div>
				<!-- profile-avatar -->

				<!-- buttons -->
				<div class="clearfix mt20">
					<button id="activate-step-2" class="btn btn-primary pull-right flip">{__("Next Step")}</button>
				</div>
				<!-- buttons -->
			</div>

			<div class="well js_wizard-content" id="step-2">

				<div class="text-center">
					<p class="mb20">{__("Cập nhật thông tin và tài khoản để hoàn tất quá trình đăng ký")}</p>
				</div>

				<div class="text-center">
					<p class="mb20" style="color: red">{__("Người dùng phải cung cấp đầy đủ thông tin về số CMND/Thẻ căn cước công dân theo quy định tại Thông tư số 09/2014/TT-BTTTT")}</p>
				</div>

				<form class="js_ajax_not_modal-forms" data-url="users/started_sms.php?do=register">
                    <input type="hidden" id="avatar" name="avatar" value="">
					<div class="form-group">
						<div class="row">
							<div class="col-xs-6">
								<label>{__("Last Name")} (*)</label>
								<input name="last_name" type="text" class="form-control" placeholder='{__("Last name")}' required>
							</div>
							<div class="col-xs-6">
								<label>{__("First name")} (*)</label>
								<input name="first_name" type="text" class="form-control" placeholder='{__("First name")}' required>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label for="user_identification">{__("Identification card number")} (*)</label>
						<input type="text" class="form-control" name="identification_card_number" placeholder='{__("Identification card number")}' maxlength="64" required>
					</div>

					<div class="form-group">
						<div class="row">
							<div class="col-xs-6">
								<label>{__("Date of issue")} (*)</label>

								<div class='input-group date' id='date_of_issue_picker'>
									<input type='text' class="form-control" name="date_of_issue" value="" maxlength="64" required/>
									<span class="input-group-addon">
                                		<span class="fas fa-calendar-alt"></span>
                            		</span>
								</div>
							</div>
							<div class="col-xs-6">
								<label>{__("Place of issue")} (*)</label>
								<input type="text" class="form-control" name="place_of_issue" placeholder='{__("Place of issue")}' maxlength="64" required>
							</div>
						</div>
					</div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-6 col-sm-4">
                                <label>{__("Birthdate")} (*)</label>

                                <div class='input-group date' id='birth_date_picker'>
                                    <input type='text' class="form-control" name="birth_date" value="" maxlength="64" required/>
                                    <span class="input-group-addon">
                                		<span class="fas fa-calendar-alt"></span>
                            		</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="password">{__("Password")} (*)</label>
                        <input type="password" class="form-control" name="password" required>

                        <span class="help-block">
								{*{__("Đổi mật khẩu khi lần đầu đăng nhập bằng số điện thoại")}  ({__("Bắt buộc")})*}
								{__("Tạo mật khẩu khi lần đầu đăng nhập bằng số điện thoại")}
							</span>
                    </div>

					<!-- success -->
					<div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
					<!-- success -->

					<!-- error -->
					<div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
					<!-- error -->

					<!-- buttons -->
					<div class="clearfix mt20 text-center">
						<button type="submit" class="btn btn-success pull-right flip">{__("Finish")}</button>
					</div>
					<!-- buttons -->
				</form>
			</div>

		</div>
	</div>
</div>
<!-- page content -->

{include file='_footer.tpl'}

<!-- x-uploader -->
{strip}
<script id="x-uploader" type="text/template">
	<form class="x-uploader" action="{literal}{{url}}{/literal}" method="post" enctype="multipart/form-data">
        {literal}{{#multiple}}{/literal}
		<input name="file[]" type="file" multiple="multiple">
        {literal}{{/multiple}}{/literal}
        {literal}{{^multiple}}{/literal}
		<input name="file" type="file">
        {literal}{{/multiple}}{/literal}
		<input type="hidden" name="secret" value="{literal}{{secret}}{/literal}">
	</form>
</script>
{/strip}
<!-- x-uploader -->

{literal}
	<script>
		$('#date_of_issue_picker').datetimepicker({
			format: DATE_FORMAT,
			defaultDate: new Date()
		});

		$('#birth_date_picker').datetimepicker({
			format: DATE_FORMAT,
			defaultDate: new Date()
		});

		$(function() {
			var wizard_steps = $('.js_wizard-steps li a');
			var wizard_content = $('.js_wizard-content');

			wizard_content.hide();

			wizard_steps.click(function(e) {
				e.preventDefault();
				var $target = $($(this).attr('href'));
				var $item = $(this).closest('li');
				if(!$item.hasClass('disabled')) {
					wizard_steps.closest('li').removeClass('active');
					$item.addClass('active');
					wizard_content.hide();
					$target.show();
				}
			});

			$('.js_wizard-steps li.active a').trigger('click');

			$('#activate-step-2').on('click', function(e) {
				$('.js_wizard-steps li:eq(1)').removeClass('disabled');
				$('.js_wizard-steps li a[href="#step-2"]').trigger('click');
			});

			$('#date_of_issue_picker').datetimepicker({
				format: DATE_FORMAT,
				defaultDate: new Date()
			});
			$('#birth_date_picker').datetimepicker({
				format: DATE_FORMAT,
				defaultDate: new Date()
			});

			// run ajax-forms not show modal
			$('body').on('submit', '.js_ajax_started-forms', function(e) {
				e.preventDefault();
				var _this = $(this);
				var url = _this.attr('data-url');
				var submit = _this.find('button[type="submit"]');
				var error = _this.find('.alert.alert-danger');
				var success = _this.find('.alert.alert-success');
				/* show any collapsed section if any */
				if(_this.find('.js_hidden-section').length > 0 && !_this.find('.js_hidden-section').is(':visible')) {
					_this.find('.js_hidden-section').slideDown();
					return false;
				}
				/* show loading */
				submit.data('text', submit.html());
				submit.prop('disabled', true);
				submit.html(__['Loading']);
				/* get ajax response */
				$.post(ajax_path+url, $(this).serialize(), function(response) {
					var disableSave = true;
					if (!response.disableSave) disableSave = false;

					submit.html(submit.data('text'));
					/* handle response */
					if(response.error) {
						/* hide loading */
						submit.prop('disabled', false); //QuanND
						if(success.is(":visible")) success.hide(); // hide previous alert
						error.html(response.message).slideDown();

						$('.js_wizard-steps li:eq(2)').addClass('disabled');

					} else if(response.success) {
						if (success.is(":visible")) success.hide();
						if (error.is(":visible")) error.hide();
						//success.html(response.message).slideDown();

						$('.js_wizard-steps li:eq(2)').removeClass('disabled');
						$('.js_wizard-steps li a[href="#step-3"]').trigger('click');
					} else {
						eval(response.callback);
					}
					submit.prop('disabled', response.disableSave);
				}, "json")
					.fail(function() {
						/* hide loading */
						submit.prop('disabled', false);
						submit.html(submit.data('text'));
						/* handle error */
						if(success.is(":visible")) success.hide(); // hide previous alert
						error.html(__['There is something that went wrong!']).slideDown();
					});
			});

            api['data/upload']  = ajax_path+"data/upload_avatar.php";

            // initialize uploader
            function initialize_uploader() {
                $('.js_x-uploader').each(function(index) {
					/* return if the plugin already running  */
                    if($(this).parents('form.x-uploader').length > 0) {
                        return;
                    }
                    var multiple = ($(this).data('multiple') !== undefined)? true : false;
                    $(this).before(render_template('#x-uploader', {'url': api['data/upload'], 'secret': secret, 'multiple': multiple}));
                    $(this).prev().append($(this));
                });
            }

			// run x-uploader
			/* initialize the uplodaer */
            initialize_uploader();
            $(document).ajaxComplete(function() {
                initialize_uploader();
            });
			/* stop propagation */
            $('body').on('click', '.x-uploader', function (e) {
				/* get type */
                var type = $(this).find('.js_x-uploader').data('type') || "photos";
                if(type == "photos") {
                    e.stopPropagation();
                }
            });
			/* initialize uploading */
            $('body').on('change', '.x-uploader input[type="file"]', function() {
                $(this).parent('.x-uploader').submit();
            });
			/* uploading */
            $('body').on('submit', '.x-uploader', function(e) {
                e.preventDefault;
				/* initialize AJAX options */
                var options = {
                    dataType: "json",
                    uploadProgress: _handle_progress,
                    success: _handle_success,
                    error: _handle_error,
                    resetForm: true
                };
                options['data'] = {};
				/* get uploader input */
                var uploader = $(this).find('input[type="file"]');
				/* get type */
                var type = $(this).find('.js_x-uploader').data('type') || "photos";
                options['data']['type'] = type;
				/* get handle */
                var handle = $(this).find('.js_x-uploader').data('handle');
                if(handle === undefined) {
                    return false;
                }
                options['data']['handle'] = handle;
				/* get multiple */
                var multiple = ($(this).find('.js_x-uploader').data('multiple') !== undefined)? true : false;
                options['data']['multiple'] = multiple;
				/* get id */
                var id = $(this).find('.js_x-uploader').data('id');
                if(id !== undefined) {
                    options['data']['id'] = id;
                }
				/* check type */
                if(type == "photos") {
					/* check handle */
                    if(handle == "cover-user" || handle == "cover-page" || handle == "cover-group" || handle == "cover-event") {
                        var loader = $('.profile-cover-change-loader');
                        loader.show();

                    } else if(handle == "picture-user" || handle == "picture-page" || handle == "picture-group") {
                        var loader = $('.profile-avatar-change-loader');
                        loader.show();

                    } else if(handle == "publisher" || handle == "publisher-mini") {
                        var publisher = (handle == "publisher")? $('.publisher') : $('.publisher.mini');
                        var files_num = uploader.get(0).files.length;
						/* check if there is current (scrabing|video|audio|file) process */
                        if(publisher.data('scrabing') || publisher.data('video') || publisher.data('audio') || publisher.data('file')) {
                            return false;
                        }
						/* check if there is already uploading process */
                        if(!publisher.data('photos')) {
                            publisher.data('photos', {});
                        }
                        var attachments = publisher.find('.publisher-attachments');
                        var loader = $('<ul></ul>').appendTo(attachments);
                        attachments.show();
                        for (var i = 0; i < files_num; ++i) {
                            $('<li class="loading"><div class="loader loader_small"></div></li>').appendTo(loader).show();
                        }

                    } else if(handle == "comment") {
                        var comment = $(this).parents('.comment');
						/* check if there is already uploading process */
                        if(comment.data('photos')) {
                            return false;
                        }
                        var attachments = comment.find('.comment-attachments');
                        var loader = attachments.find('li.loading');
                        attachments.show();
                        loader.show();

                    } else if(handle == "chat") {
                        var chat_widget = $(this).parents('.chat-widget, .panel-messages');
						/* check if there is already uploading process */
                        if(chat_widget.data('photo')) {
                            return false;
                        }
                        var attachments = chat_widget.find('.chat-attachments');
                        var loader = attachments.find('li.loading');
                        attachments.show();
                        loader.show();

                    } else if(handle == "x-image") {
                        var parent = $(this).parents('.x-image');
                        var loader = parent.find('.loader');
                        loader.show();
                    }
                } else if (type == "video" || type == "audio" || type == "file") {
					/* check handle */
                    if(handle == "publisher") {
						/* show upload loader */
                        var publisher = $('.publisher');
						/* check if there is current (uploading|scrabing|video|audio) process */
                        if(publisher.data('photos') || publisher.data('scrabing') || publisher.data('video')  || publisher.data('audio') || publisher.data('file')) {
                            return false;
                        }
                        publisher.data(type, {});
                        var attachments = $('.publisher-attachments');
                        var loader = $('<ul></ul>').appendTo(attachments);
                        attachments.show();
                        $('<li class="loading"><div class="loader loader_small"></div></li>').appendTo(loader).show();
                    }
                }

				/* handle progress */
                function _handle_progress(e) {
					/* disable uploader input during uploading */
                    uploader.prop('disabled', true);
                }
				/* handle success */
                function _handle_success(response) {
					/* enable uploader input */
                    uploader.prop('disabled', false);
					/* hide upload loader */
                    if(loader) loader.hide();
					/* handle the response */
                    if(response.callback) {
                        if(handle == "publisher" || handle == "publisher-mini") {
							/* hide the attachment from publisher */
                            if( (type == "photos" && jQuery.isEmptyObject(publisher.data('photos'))) || type != "photos" ) {
                                attachments.hide();
								/* remove the type object from publisher data */
                                publisher.removeData(type);
                            }
							/* remove upload loader */
                            if(loader) loader.remove();
                        }
                        eval(response.callback);
                    } else {
						/* check type */
                        if(type == "photos") {
							/* check the handle */
                            if(handle == "cover-user" || handle == "cover-page" || handle == "cover-group" || handle == "cover-event") {
								/* update (user|page|group) cover */
                                var image_path = uploads_path+'/'+response.file;
                                $('.profile-cover-wrapper').css("background-image", 'url('+image_path+')').removeClass('no-cover');
								/* show delete btn  */
                                $('.profile-cover-wrapper').find('.profile-cover-delete').show();
								/* remove lightbox */
                                $('.profile-cover-wrapper').removeClass('js_lightbox').removeAttr('data-id').removeAttr('data-image').removeAttr('data-context');

                            } else if(handle == "picture-user" || handle == "picture-page" || handle == "picture-group") {
								/* update (user|page|group) picture */
                                var image_path = uploads_path+'/'+response.file;
                                //$('.profile-avatar-wrapper img').attr("src", image_path);
                                $('.profile-avatar-wrapper .new_profile .article_profile .thumb_profile').css("background-image", "url(" + image_path + ")");
                                $('.profile-avatar-wrapper .new_profile .article_profile .thumb_profile .js_lightbox').attr("data-image", image_path);
                                $("#avatar").val(response.file);
                            } else if(handle == "publisher" || handle == "publisher-mini") {
								/* remove upload loader */
                                if(loader) loader.remove();
								/* add the attachment to publisher data */
                                var files = publisher.data('photos');
                                for(var i in response.files) {
                                    files[response.files[i]] = response.files[i];
									/* add publisher-attachments */
                                    var image_path = uploads_path+'/'+response.files[i];
                                    attachments.find('ul').append(render_template("#publisher-attachments-item", {'src':response.files[i], 'image_path':image_path}));
                                }
                                publisher.data('photos', files);

                            } else if(handle == "comment") {
								/* add the attachment to comment data */
                                comment.data('photos', response.file);
								/* hide comment x-form-tools */
                                comment.find('.x-form-tools-attach').hide();
								/* add comment-attachments */
                                var image_path = uploads_path+'/'+response.file;
                                attachments.find('ul').append(render_template("#comment-attachments-item", {'src':response.file, 'image_path':image_path}));

                            } else if(handle == "chat") {
								/* add the attachment to chat widget data */
                                chat_widget.data('photo', response.file);
								/* hide chat widget x-form-tools */
                                chat_widget.find('.x-form-tools-attach').hide();
								/* add chat-attachments */
                                var image_path = uploads_path+'/'+response.file;
                                attachments.find('ul').append(render_template("#chat-attachments-item", {'src':response.file, 'image_path':image_path}));

                            } else if(handle == "x-image") {
								/* update x-image picture */
                                var image_path = uploads_path+'/'+response.file;
                                parent.css("background-image", 'url('+image_path+')');
								/* add the image to input */
                                parent.find('.js_x-image-input').val(response.file);
								/* show the remover */
                                parent.find('button').show();
                            }
                        } else if (type == "video" || type == "audio" || type == "file") {
							/* hide the attachment from publisher data */
                            attachments.hide();
							/* remove upload loader */
                            if(loader) loader.remove();
							/* show publisher meta */
                            $('.publisher-meta[data-meta="'+type+'"]').show();
							/* add the attachment to publisher data */
                            var object = publisher.data(type);
                            object['source'] = response.file;
							/* add publisher-attachments */
                            publisher.data(type, object);
                        }
                    }
                }
				/* handle error */
                function _handle_error() {
					/* enable uploader input */
                    uploader.prop('disabled', false);
					/* hide upload loader */
                    if(loader) loader.hide();
					/* check the handle */
                    if(handle == "publisher") {
						/* hide the attachment from publisher */
                        if( (type == "photos" && jQuery.isEmptyObject(publisher.data('photos'))) || type != "photos" ) {
                            attachments.hide();
                        }
						/* remove upload loader */
                        if(loader) loader.remove();
                    }
                    modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
                }
				/* submit the form */
                $(this).ajaxSubmit(options);
                return false;
            });

			/* handle profile (cover|picture) remover */
            $('body').on('click', '.js_delete-cover, .js_delete-picture', function (e) {
                e.stopPropagation();
                var id = $(this).data('id');
                var handle = $(this).data('handle');
                var remove = ($(this).hasClass('js_delete-cover'))? 'cover' : 'picture';

                var wrapper = $('.profile-avatar-wrapper');
                var _title = __['Delete Picture'];
                var _message = __['Are you sure you want to remove your profile picture?'];

                confirm(_title, _message, function() {
                    /* hide delete btn  */
                    //wrapper.find('.profile-avatar-delete').hide();
                    /* remove lightbox */
                    $('.profile-avatar-wrapper .new_profile .article_profile .thumb_profile .js_lightbox').removeClass('js_lightbox').removeAttr('data-id').removeAttr('data-image').removeAttr('data-context');
                    /* update (user|page|group) picture with default picture */
                    $('.profile-avatar-wrapper .new_profile .article_profile .thumb_profile').css("background-image", "url('')");
                    $("#avatar").val("");

                    $('#modal').modal('hide');
                });
            });

		});
	</script>
{/literal}

{*<script type="text/javascript" src="{$system['cdn_url']}/includes/assets/js/core{$system["js_path"]}/user.js"></script>*}

