<div class="modal-body plr0 ptb0">
    <div class="panel-body">
        <div class="mb10" align="center"><strong>{__("Add photo to Album")}</strong></div>
        <form class="form-horizontal" enctype="multipart/form-data" method="post" id="add_child_journal">
            <input type="hidden" name="child_parent_id" value="{$journal['child_parent_id']}"/>
            <input type="hidden" name="child_journal_album_id" value="{$journal['child_journal_album_id']}"/>
            <input type="hidden" name="do" value="add_photo"/>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left">{__("Picture")}</label>
                <div class="col-sm-6">
                    <input name="file[]" type="file" multiple="true">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-9 col-sm-offset-3">
                    <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                </div>
            </div>
        </form>
    </div>
</div>
