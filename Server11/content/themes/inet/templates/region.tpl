{include file='_head.tpl'}
{include file='_header.tpl'}

<!-- page content -->
<div class="container mt20">
    <div class="row">

        <div class="col-md-3 col-sm-3">
            <div class="panel panel-default">
                <div class="panel-body with-nav">
                    <ul class="side-nav metismenu js_metisMenu">
                        <!-- School -->
                        <li {if $view == ""}class="active selected"{/if}>
                            <a href="{$system['system_url']}/region/">
                                <i class="fa fa-university fa-fw fa-lg pr10"></i> {__("Dashboard")}
                            </a>
                        </li>
                        <!-- School -->
                        <li {if $view == "school"}class="active selected"{/if}>
                            <a href="{$system['system_url']}/region/school">
                                <i class="fa fa-university fa-fw fa-lg pr10"></i> {__("School list")}
                            </a>
                        </li>

                        {*<!-- Notification -->*}
                        {*<li {if $view == "notify"}class="active selected"{/if}>*}
                            {*<a href="{$system['system_url']}/region/notify">*}
                                {*<i class="fa fa-university fa-fw fa-lg pr10"></i> {__("Notify")}*}
                            {*</a>*}
                        {*</li>*}

                        <!-- Permission -->
                        <li {if $view == "role"}class="active selected"{/if}>
                            <a href="{$system['system_url']}/region/role">
                                <i class="fa fa-university fa-fw fa-lg pr10"></i> {__("Permission")}
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-9 col-sm-9">
            {if $view == ""}
                {include file='ci/region/region.dashboard.tpl'}
            {else}
                {include file="ci/region/region.$view.tpl"}
            {/if}
        </div>
    </div>
</div>
<!-- page content -->


{include file='_footer.tpl'}