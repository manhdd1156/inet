{include file='_head.tpl'}
{include file='_header.tpl'}

<!-- page content -->
<div class="container mt20 height_min"> <!-- class height_min để set height tối thiểu của container -->
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="well js_wizard-content" id="step-2">
                <div class="text-center">
                    <h3 class="mb5">{__("Update your info")}</h3>
                    <p class="mb20">{__("We could not identify your email or phone number, please enter your correct email and phone number.")}</p>
                </div>

                <form class="js_ajax-forms" data-url="users/updateinfo.php">
                    <!-- ConIu - Thêm trường phone và city -->
                    <div class="form-group">
                        <label for="user_email">{__("Email")}</label>
                        <input type="text" class="form-control" name="user_email" id="user_email" value="{$user->_data['user_email']}" maxlength="50">
                    </div>

                    <div class="form-group">
                        <label for="user_phone">{__("Phone")}</label>
                        <input type="text" class="form-control" name="user_phone" id="user_phone" value="{$user->_data['user_phone']}" maxlength="50">
                    </div>

                    <!-- success -->
                    <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                    <!-- success -->

                    <!-- error -->
                    <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                    <!-- error -->

                    <!-- buttons -->
                    <div class="clearfix mt20">
                        <div class="pull-right flip">
                            <a href="{$system['system_url']}" class="btn btn-danger">{__("Skip")}</a>
                            <button type="submit" class="btn btn-success">{__("Finish")}</button>
                            {*<button type="button" class="btn btn-primary" id="activate-step-3">{__("Next Step")}</button>*}
                        </div>
                    </div>
                    <!-- buttons -->
                </form>
            </div>
        </div>
    </div>
</div>
<!-- page content -->

{include file='_footer.tpl'}