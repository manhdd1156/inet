{if count($posts) > 0}
    <div style="background: #E9EAEE; margin-top: 30px" class="js_posts_stream" data-get="{$_get}" data-filter="{if $_filter}{$_filter}{else}all{/if}" {if $_id}data-id="{$_id}"{/if}>
        <ul>
            <!-- posts -->
            {foreach $posts as $k => $post}
                {include file='__feeds_post_right.tpl' _get=$_get}
            {/foreach}
            <!-- posts -->
        </ul>
        <!-- see-more -->
        <div class="alert alert-post see-more mb10 js_see-more {if $user->_logged_in}js_see-more-infinite{/if}" data-get="{$_get}" data-filter="{if $_filter}{$_filter}{else}all{/if}" {if $_id}data-id="{$_id}"{/if} data-pos="right">
            <span>{__("More Stories")}</span>
            <div class="loader loader_small x-hidden"></div>
        </div>
        <!-- see-more -->
    </div>
{else}
    <div class="js_posts_stream" data-get="{$_get}" data-filter="{if $_filter}{$_filter}{else}all{/if}" {if $_id}data-id="{$_id}"{/if}>
        <ul>
            <!-- no posts -->
            <div class="text-center x-muted">
                <i class="fa fa-newspaper-o fa-4x"></i>
                <p class="mb10"><strong>{__("No posts to show")}</strong></p>
            </div>
            <!-- no posts -->
        </ul>
    </div>
{/if}