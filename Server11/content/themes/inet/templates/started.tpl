{include file='_head.tpl'}
{include file='_header.tpl'}

<!-- page content -->
<div class="container mt20 height_min"> <!-- class height_min để set height tối thiểu của container -->
    <div class="row">
        <div class="col-md-10 col-md-offset-1">

            <div class="wizard-header">
                <h3>
                    {*{__("BUILD")} {__("YOUR PROFILE")}<br>*}
                    {__("Update your info and account")|upper}<br>
                    {*<small>{__("This information will let us know more about you")}.</small>*}
                </h3>
            </div>

            <ul class="nav nav-pills nav-justified thumbnail js_wizard-steps">
                <li {if $step == 1}class="active"{/if}>
                    <a href="#step-1">
                        <h4 class="list-group-item-heading">{__("Step 1")}</h4>
                        <p class="list-group-item-text">{__("Upload your photo")}</p>
                    </a>
                </li>
                <li class="{if $step == 2 }active{elseif $step == 1}disabled{/if}">
                    <a href="#step-2">
                        <h4 class="list-group-item-heading">{__("Step 2")}</h4>
                        <p class="list-group-item-text">{__("Update your info")}</p>
                    </a>
                </li>
                {*<li class="disabled">*}
                {*<a href="#step-3">*}
                {*<h4 class="list-group-item-heading">{__("Step 3")}</h4>*}
                {*<p class="list-group-item-text">{__("Add Friends")}</p>*}
                {*</a>*}
                {*</li>*}

                {*{if $has_three_step}
                    <li class="{if $step == 3}active{else}disabled{/if}">
                        <a href="#step-3">
                            <h4 class="list-group-item-heading">{__("Step 3")}</h4>
                            <p class="list-group-item-text">{__("Update your account")}</p>
                        </a>
                    </li>
                {/if}*}
            </ul>

            <div class="well js_wizard-content" id="step-1">
                <div class="text-center">
                    <h3 class="mb5">{__("Welcome")} {$user->_data['user_fullname']}</h3>
                    <p class="mb20">{__("Let's start with your photo")}</p>
                </div>

                <!-- profile-avatar -->
                <div class="relative" style="height: 160px;">
                    <div class="profile-avatar-wrapper static">
                        {*<img src="{$user->_data['user_picture']}" alt="{$user->_data['user_fullname']}">*}
                        <div class="new_profile">
                            <div class="article_profile">
                                <div class="thumb_profile"
                                     style="background-image: url({$user->_data['user_picture']});">
                                    <div style="height:150px; cursor: pointer"></div>
                                </div>
                            </div>
                        </div>
                        <div class="profile-avatar-change">
                            <i class="fa fa-camera js_x-uploader" data-handle="picture-user"></i>
                        </div>
                        <div class="profile-avatar-delete">
                            <i class="fa fa-trash js_delete-picture" data-handle="picture-user"
                               title='{__("Delete Picture")}'></i>
                        </div>
                        <div class="profile-avatar-change-loader">
                            <div class="loader loader_medium"></div>
                        </div>
                    </div>
                </div>
                <!-- profile-avatar -->

                <!-- buttons -->
                <div class="clearfix mt20">
                    <button id="activate-step-2" class="btn btn-primary pull-right flip">{__("Next Step")}</button>
                </div>
                <!-- buttons -->
            </div>

            <div class="well js_wizard-content" id="step-2">
                <div class="text-center">
                    <h3 class="mb5">{__("Update your info")}</h3>
                    <p class="mb20">{__("Please update more information for coniu serve you best")}</p>
                    <p class="mb20"
                       style="color: red">{__("Người dùng phải cung cấp đầy đủ thông tin về số CMND/Thẻ căn cước công dân theo quy định tại Thông tư số 09/2014/TT-BTTTT")}</p>
                </div>

                <form class="js_ajax_started-forms" data-url="users/started.php?edit=profile">

                    <div class="form-group">
                        <label for="user_identification">{__("Identification card number")} (*)</label>
                        <input type="text" class="form-control" name="identification_card_number"
                               value="{$user->_data['user_id_card_number']}" maxlength="64" required>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-6">
                                <label>{__("Date of issue")} (*)</label>

                                <div class='input-group date' id='date_of_issue_picker'>
                                    <input type='text' class="form-control" name="date_of_issue"
                                           value="{$user->_data['date_of_issue']}" maxlength="64" required/>
                                    <span class="input-group-addon">
                                		<span class="fas fa-calendar-alt"></span>
                            		</span>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <label>{__("Place of issue")} (*)</label>
                                <input type="text" class="form-control" name="place_of_issue"
                                       value="{$user->_data['place_of_issue']}" maxlength="64" required>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-6 col-sm-4">
                                <label>{__("Birthdate")} (*)</label>

                                <div class='input-group date' id='birth_date_picker'>
                                    <input type='text' class="form-control" name="birth_date"
                                           value="{$user->_data['user_birthdate']}" maxlength="64" required/>
                                    <span class="input-group-addon">
                                		<span class="fas fa-calendar-alt"></span>
                            		</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    {if ( ($user->_data['facebook_connected'] || $user->_data['google_connected'] || !is_empty($user->_data['user_phone_signin']) )
                    && !$user->_data['user_changed_pass'] )}
                        <div class="form-group">
                            <label for="password">{__("Password")} (*)</label>
                            <input type="password" class="form-control" name="password" required>

                            <span class="help-block">
								{*{__("Đổi mật khẩu khi lần đầu đăng nhập bằng số điện thoại")}  ({__("Bắt buộc")})*}
                                {__("Đổi mật khẩu khi lần đầu đăng nhập bằng số điện thoại")}
							</span>
                        </div>
                    {/if}

                    <!-- success -->
                    <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                    <!-- success -->

                    <!-- error -->
                    <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                    <!-- error -->

                    <!-- buttons -->
                    <div class="clearfix mt20">
                        <div class="pull-right flip">
                            <button type="submit" class="btn btn-success">{__("Finish")}</button>
                        </div>
                    </div>
                    <!-- buttons -->

                </form>
            </div>

        </div>
    </div>
</div>
<!-- page content -->

{include file='_footer.tpl'}

<script type="text/javascript">
    $(function () {
        var wizard_steps = $('.js_wizard-steps li a');
        var wizard_content = $('.js_wizard-content');

        wizard_content.hide();

        wizard_steps.click(function (e) {
            e.preventDefault();
            var $target = $($(this).attr('href'));
            var $item = $(this).closest('li');
            if (!$item.hasClass('disabled')) {
                wizard_steps.closest('li').removeClass('active');
                $item.addClass('active');
                wizard_content.hide();
                $target.show();
            }
        });

        $('.js_wizard-steps li.active a').trigger('click');

        $('#activate-step-2').on('click', function (e) {
            $('.js_wizard-steps li:eq(1)').removeClass('disabled');
            $('.js_wizard-steps li a[href="#step-2"]').trigger('click');
        });

        $('#date_of_issue_picker').datetimepicker({
            format: DATE_FORMAT,
            defaultDate: new Date()
        });
        $('#birth_date_picker').datetimepicker({
            format: DATE_FORMAT,
            defaultDate: new Date()
        });

        // run ajax-forms not show modal
        $('body').on('submit', '.js_ajax_started-forms', function (e) {
            e.preventDefault();
            var _this = $(this);
            var url = _this.attr('data-url');
            var submit = _this.find('button[type="submit"]');
            var error = _this.find('.alert.alert-danger');
            var success = _this.find('.alert.alert-success');
            /* show any collapsed section if any */
            if (_this.find('.js_hidden-section').length > 0 && !_this.find('.js_hidden-section').is(':visible')) {
                _this.find('.js_hidden-section').slideDown();
                return false;
            }
            /* show loading */
            submit.data('text', submit.html());
            submit.prop('disabled', true);
            submit.html(__['Loading']);
            /* get ajax response */
            $.post(ajax_path + url, $(this).serialize(), function (response) {
                var disableSave = true;
                if (!response.disableSave) disableSave = false;

                submit.html(submit.data('text'));
                /* handle response */
                if (response.error) {
                    /* hide loading */
                    submit.prop('disabled', false); //QuanND
                    if (success.is(":visible")) success.hide(); // hide previous alert
                    error.html(response.message).slideDown();

                    $('.js_wizard-steps li:eq(2)').addClass('disabled');

                } else if (response.success) {
                    if (success.is(":visible")) success.hide();
                    if (error.is(":visible")) error.hide();
                    //success.html(response.message).slideDown();

                    $('.js_wizard-steps li:eq(2)').removeClass('disabled');
                    $('.js_wizard-steps li a[href="#step-3"]').trigger('click');
                } else {
                    eval(response.callback);
                }
                submit.prop('disabled', response.disableSave);
            }, "json")
                .fail(function () {
                    /* hide loading */
                    submit.prop('disabled', false);
                    submit.html(submit.data('text'));
                    /* handle error */
                    if (success.is(":visible")) success.hide(); // hide previous alert
                    error.html(__['There is something that went wrong!']).slideDown();
                });
        });

    });
</script>