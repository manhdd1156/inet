<div class="footer-v1">
    <div class="copyright">
        <div class="row">
            <div class="col-md-4">
                <p style="font-size: 12px">
                    {'Y'|date} &copy; {$system['system_title']}.
                    {*<a href="{$system['system_url']}/faq">{__("FAQ")}</a> | *}
                    <a href = "#" class="text-link" data-toggle="modal" data-url="#translator">{$system['language']['title']}</a> |
                    <a href="{$system['system_url']}/privacy">{__("Privacy")}</a> | <a href="{$system['system_url']}/terms">{__("Terms")}</a>
                </p>
            </div>
{*            <div class="col-md-5" align="center">*}
{*                <p style="font-size: 13px">*}
{*                    <span class="text-bold">{__("Social network license")}:</span> 576/GP-BTTTT. {__("Issued")}: 22/11/2017. {__("Issued by")}: Bộ Thông tin và Truyền thông. {__("representation")}: Ông Ngô Đạo Quân. {__("position")}: Giám đốc. Email: quan@noga.vn*}
{*                </p>*}
{*                <p>{__("The system is supported by the SCSI-Hansiba startup center")}</p>*}
{*            </div>*}
            <!-- Social Links -->
            <div class="col-md-3" style="float: right;">
                <ul class="footer-socials list-inline">
                    <li>
                        <a href="https://www.facebook.com/mobiedu.vn" class="tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Facebook">
                            <i class="fa fa-facebook-f"></i>
                        </a>
                    </li>
                    <!-- DELETE START MANHDD 18/06/2021 -->
{*                    <li>*}
{*                        <a href="mobiedu@mobifone.vn" class="tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Google Plus">*}
{*                            <i class="fa fa-google-plus"></i>*}
{*                        </a>*}
{*                    </li>*}
{*                    <li>*}
{*                        <a href="#" class="tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Twitter">*}
{*                            <i class="fa fa-twitter"></i>*}
{*                        </a>*}
{*                    </li>*}
                    <!-- DELETE END MANHDD 18/06/2021 -->
                </ul>
            </div>
            <!-- End Social Links -->
        </div>
        <div class="row">
            <div class="col-sm-12" align="center">
            </div>
        </div>
    </div><!--/copyright-->
</div>
<div id="app_android" style="display: none" class="app_pop">
    <div class="android_down app_down_box">
        <a href="{$smarty.const.GOOGLEPLAY_URL}" target="_blank"> <img
                    class=""
                    src="{$system['system_url']}/content/themes/{$system['theme']}/images/favicon.png"
                    alt="Ứng dụng mầm non Inet CH Play"/> {__("Download Inet App")}</a>
        <div class="down_app_hide_home">
            <i class="fa fa-close"></i>
        </div>
    </div>
</div>
<div id="app_ios" style="display: none" class="app_pop">
    <div class="ios_down app_down_box">
        <a href="{$smarty.const.APPSTORE_URL}" target="_blank"><img class=""
                                                                    src="{$system['system_url']}/content/themes/{$system['theme']}/images/favicon.png"
                                                                    alt="Ứng dụng mầm non Inet App store"/> {__("Download Inet App")}</a>
        <div class="down_app_hide_home">
            <i class="fa fa-close"></i>
        </div>
    </div>
</div>
<script>
    function getMobileOperatingSystem() {
        var userAgent = navigator.userAgent || navigator.vendor || window.opera;
        // Windows Phone must come first because its UA also contains "Android"
        if (/windows phone/i.test(userAgent)) {
            return "Windows Phone";
        }

        if (/android/i.test(userAgent)) {
            document.getElementById("app_android").style.display = 'block';
            document.getElementById("ios_banner").style.display = 'none';
            document.getElementById("android_banner").style.marginRight = '0';
            return "Android";
        }

        // iOS detection from: http://stackoverflow.com/a/9039885/177710
        if (/iPad|iPhone|iPod/i.test(userAgent)) {
            document.getElementById("app_ios").style.display = 'block';
            document.getElementById("android_banner").style.display = 'none';
            document.getElementById("ios_banner").style.marginRight = '0';
            return "iOS";
        }
        return "Web";
    }
    getMobileOperatingSystem();
    // if(x == Adroi)
    // document.getElementById("osmobile").innerHTML = x;
</script>
</div><!--/wrapper-->
<!-- JS Files -->
{include file='_js_files.tpl'}
<!-- JS Files -->

<!-- JS Templates -->
{include file='_js_templates.tpl'}
<!-- JS Templates -->

<!-- Analytics Code -->

<!-- Global site tag (gtag.js) - Google Analytics -->
{literal}
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-87252737-2"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-87252737-2');
    </script>
{/literal}
<!-- Analytics Code -->

<!-- DELETE START - MANHDD - 25/05/2021-->
<!-- Facebook Account Kit SDK-->

<!-- HTTPS required. HTTP will give a 403 forbidden response -->
{*<script src="https://sdk.accountkit.com/vi_VN/sdk.js"></script>*}

{*{literal}*}
{*    <script>*}
{*        // initialize Account Kit with CSRF protection*}
{*        AccountKit_OnInteractive = function(){*}
{*            AccountKit.init(*}
{*                {*}
{*                    appId:1638502962897148,*}
{*                    state:"coniuahihi",*}
{*                    version:"v1.1",*}
{*                    fbAppEventsEnabled:true,*}
{*                    redirect:"https://inet.vn"*}
{*                }*}
{*            );*}
{*        };*}

{*        var api = [];*}
{*        api['core/verify']  = ajax_path+"core/verify_phone.php";*}

{*        // login callback*}
{*        function loginCallback(response) {*}
{*            if (response.status === "PARTIALLY_AUTHENTICATED") {*}

{*                $.post(api['core/verify'], {*}
{*                    'do': 'signin',*}
{*                    'code': response.code,*}
{*                    'csrf': response.state*}
{*                }, function (response) {*}

{*                    if (response.callback) {*}
{*                        eval(response.callback);*}
{*                    } else if(response.error) {*}
{*                        modal('#modal-error', {title: __['Error'], message: response.message});*}
{*                    } else {*}
{*                        window.location.reload();*}
{*                    }*}

{*                }, 'json');*}
{*            }*}
{*            else if (response.status === "NOT_AUTHENTICATED") {*}
{*                // handle authentication failure*}
{*                console.log("Authentication failure");*}
{*            }*}
{*            else if (response.status === "BAD_PARAMS") {*}
{*                // handle bad parameters*}
{*                console.log("Bad parameters");*}
{*            }*}
{*        }*}

{*        // phone form submission handler*}
{*        function smsLogin() {*}
{*            //var countryCode = document.getElementById("country_code").value;*}
{*            //var phoneNumber = document.getElementById("phone_number").value;*}
{*            AccountKit.login(*}
{*                'PHONE',*}
{*                {}, // will use default values if this is not specified*}
{*                //{countryCode: countryCode, phoneNumber: phoneNumber}, // will use default values if not specified*}
{*                loginCallback*}
{*            );*}
{*        }*}

{*    </script>*}
{*{/literal}*}

{*<!-- JS Global Compulsory -->*}
{*<!-- JS Implementing Plugins -->*}
{*<script type="text/javascript" src="{$system['system_url']}/content/themes/{$system['theme']}/assets/plugins/back-to-top.js"></script>*}
{*<!-- JS Customization -->*}
{*<script type="text/javascript" src="{$system['system_url']}/content/themes/{$system['theme']}/assets/js/custom.js"></script>*}
{*<!-- JS Page Level -->*}
{*<script type="text/javascript" src="{$system['system_url']}/content/themes/{$system['theme']}/assets/js/app.js"></script>*}
{*<!-- taila -->*}
{*<script>*}
{*    $(document).ready(function(){*}
{*        $('a[href="#topcontrol"]').on('click',function (e) {*}
{*            e.preventDefault();*}

{*            var target = this.hash;*}
{*            var $target = $(target);*}

{*            $('html, body').stop().animate({*}
{*                'scrollTop': $target.offset().top*}
{*            }, 900, 'swing', function () {*}
{*                window.location.hash = target;*}
{*            });*}
{*        });*}
{*        $('.js_chat-widget-master').hide();*}
{*    });*}
{*</script>*}

{*<!-- end taila -->*}
{*<!--[if lt IE 9]>*}
{*<script src="assets/plugins/respond.js"></script>*}
{*<script src="assets/plugins/html5shiv.js"></script>*}
{*<script src="assets/plugins/placeholder-IE-fixes.js"></script>*}
{*<![endif]-->*}
{*{if $system['language']['code'] == 'vi_VN'}*}
{*    <script src="https://www.google.com/recaptcha/api.js?onload=myCallBack&render=explicit&hl=vi" async defer></script>*}
{*{else}*}
{*    <script src="https://www.google.com/recaptcha/api.js?onload=myCallBack&render=explicit&hl=en" async defer></script>*}
{*{/if}*}
{*<script>*}
{*    var recaptcha1;*}
{*    var recaptcha2;*}
{*    var recaptcha3;*}
{*    var myCallBack = function() {*}
{*        //Render the recaptcha1 on the element with ID "recaptcha1"*}
{*        recaptcha1 = grecaptcha.render('recaptcha1', {*}
{*            'sitekey' : '{$system.reCAPTCHA_site_key}', //Replace this with your Site key*}
{*            'theme' : 'light'*}
{*        });*}

{*        //Render the recaptcha2 on the element with ID "recaptcha2"*}
{*        recaptcha2 = grecaptcha.render('recaptcha2', {*}
{*            'sitekey' : '{$system.reCAPTCHA_site_key}', //Replace this with your Site key*}
{*            'theme' : 'light'*}
{*        });*}

{*        //Render the recaptcha3 on the element with ID "recaptcha3"*}
{*        recaptcha3 = grecaptcha.render('recaptcha3', {*}
{*            'sitekey' : '{$system.reCAPTCHA_site_key}', //Replace this with your Site key*}
{*            'theme' : 'light'*}
{*        });*}
{*    };*}
{*</script>*}
<!-- DELETE END - MANHDD - 25/05/2021-->
<!--End of Tawk.to Script-->
</body>
</html>
