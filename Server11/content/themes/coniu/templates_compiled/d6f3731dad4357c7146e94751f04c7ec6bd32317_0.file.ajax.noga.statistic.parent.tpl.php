<?php
/* Smarty version 3.1.31, created on 2021-03-31 08:54:36
  from "D:\workplace\Server11\content\themes\coniu\templates\ci\noga\ajax.noga.statistic.parent.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_6063d65cb2c9c7_24725984',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'd6f3731dad4357c7146e94751f04c7ec6bd32317' => 
    array (
      0 => 'D:\\workplace\\Server11\\content\\themes\\coniu\\templates\\ci\\noga\\ajax.noga.statistic.parent.tpl',
      1 => 1552404703,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6063d65cb2c9c7_24725984 (Smarty_Internal_Template $_smarty_tpl) {
?>
<table class="table table-striped table-bordered table-hover" id="statistic_parent_detail">
    <thead>
    <tr>
        <th>#</th>
        <th><?php echo __("Page");?>
</th>
        <th><?php echo __("Post");?>
</th>
        <th><?php echo __("Dashboard");?>
</th>
        <th><?php echo __("Resign");?>
</th>
        <th><?php echo __("Service");?>
</th>
        <th><?php echo __("Late pickup");?>
</th>
        <th><?php echo __("Tuition");?>
</th>
        <th><?php echo __("Notification - Event");?>
</th>

        <th><?php echo __("Schedule");?>
</th>
        <th><?php echo __("Menu");?>
</th>
        <th><?php echo __("Contact book");?>
</th>
        <th><?php echo __("Medicines");?>
</th>
        <th><?php echo __("Feedback");?>
</th>
        <th><?php echo __("Diary");?>
</th>
        <th><?php echo __("Health");?>
</th>
        <th><?php echo __("Birthday");?>
</th>
        <th><?php echo __("Picker");?>
</th>
    </tr>
    </thead>
    <tbody>

    <tr>
        <td align="center"><?php echo __("Interactive");?>
</td>
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['keys']->value, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value) {
?>
            <td align="center"><?php echo $_smarty_tpl->tpl_vars['statistic']->value[$_smarty_tpl->tpl_vars['key']->value]['parent_views'];?>
</td>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
    </tr>
    <tr>
        <td align="center"><?php echo __("Add New");?>
</td>
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['keys']->value, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value) {
?>
            <td align="center"><?php echo $_smarty_tpl->tpl_vars['statistic']->value[$_smarty_tpl->tpl_vars['key']->value]['parent_createds'];?>
</td>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

    </tr>

    </tbody>
</table><?php }
}
