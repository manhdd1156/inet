<?php
/* Smarty version 3.1.31, created on 2021-04-16 17:03:07
  from "D:\workplace\Server11\content\themes\coniu\templates\child.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_607960db02fa72_53770429',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '929ec120bd9e1b2fe4f5ea7de693bd4959af1c7e' => 
    array (
      0 => 'D:\\workplace\\Server11\\content\\themes\\coniu\\templates\\child.tpl',
      1 => 1618567307,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:_head.tpl' => 1,
    'file:_header.tpl' => 1,
    'file:ci/child/child.help.tpl' => 1,
    'file:ci/child/child.dashboard.tpl' => 1,
    'file:ci/child/child.".((string)$_smarty_tpl->tpl_vars[\'view\']->value).".tpl' => 1,
    'file:_footer.tpl' => 1,
  ),
),false)) {
function content_607960db02fa72_53770429 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:_head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php $_smarty_tpl->_subTemplateRender('file:_header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<div class="container mt20 height_min"> <!-- class height_min để set height tối thiểu của container -->
    <div class="row">
        <div class="col-md-3 col-sm-3" id = "menu_left">
            <nav class="navbar navbar-default" role="navigation">
                <div class="panel panel-default">
                    <div class = "menu_school">
                        <!-- Dashbroad -->
                        <?php if ($_smarty_tpl->tpl_vars['view']->value == '') {?>
                            <i class="fas fa-tachometer-alt fa-fw fa-lg pr10"></i> <?php echo __("Dashboard");?>
 [<?php echo $_smarty_tpl->tpl_vars['child']->value['child_name'];?>
]
                        <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "medicines") {?>
                            <i class="fa fa-medkit fa-lg fa-fw pr10"></i> <?php echo __("Medicines");?>

                        <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "attendance") {?>
                            <i class="fa fa-tasks fa-fw fa-lg pr10"></i> <?php echo __("Resign");?>

                        <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "contact") {?>
                            <i class="fa fa-list fa-lg fa-fw pr10"></i> <?php echo __("Contact list");?>

                        <?php } elseif (($_smarty_tpl->tpl_vars['view']->value == "services")) {?>
                            <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
                                <i class="fa fa-life-ring fa-lg fa-fw pr10"></i> <?php echo __('Service usage information');?>

                            <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "reg") {?>
                                <i class="fa fa-life-ring fa-lg fa-fw pr10"></i> <?php echo __("Register service");?>

                            <?php }?>
                        <?php } elseif (($_smarty_tpl->tpl_vars['view']->value == "pickup")) {?>
                            <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
                                <i class="fa fa-universal-access fa-lg fa-fw pr10"></i> <?php echo __("Register");?>

                            <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "list") {?>
                                <i class="fa fa-universal-access fa-lg fa-fw pr10"></i> <?php echo __("Lists");?>

                            <?php }?>
                        <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "informations") {?>
                            <i class="fa fa-info-circle fa-lg fa-fw pr10"></i> <?php echo __("Pickup information");?>

                        <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "schedules") {?>
                            <i class="fas fa-calendar-alt fa-lg fa-fw pr10"></i> <?php echo __("Schedule");?>

                        <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "menus") {?>
                            <i class="fas fa-utensils fa-lg fa-fw pr10"></i> <?php echo __("Menu");?>

                        <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "tuitions") {?>
                            <?php if ($_smarty_tpl->tpl_vars['view']->value == "tuitions" && $_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
                                <i class="fa fa-caret-right fa-lg fa-fw pr10"></i> <?php echo __("Tuition fee");?>
 <i class="fa fa-fw pr10"></i> <?php echo __('Monthly Fee');?>

                            <?php }?>
                        <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "events") {?>
                            <i class="fa fa-bell fa-lg fa-fw pr10"></i> <?php echo __("Notification - Event");?>

                        <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "reports") {?>
                            <i class="fa fa-envelope fa-lg fa-fw pr10"></i> <?php echo __("Contact book");?>

                        <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "points") {?>
                            <i class="fa fa-envelope fa-lg fa-fw pr10"></i> <?php echo __("Points");?>

                        <?php }?>
                    </div>
                    <div class="navbar-header">

                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <?php if (count($_smarty_tpl->tpl_vars['schoolList']->value) > 0 || count($_smarty_tpl->tpl_vars['classList']->value) > 0 || count($_smarty_tpl->tpl_vars['childrenList']->value) > 0) {?>
                        <div class="select_object_box">
                            <select name="object" id="select_object">
                                <?php if (count($_smarty_tpl->tpl_vars['schoolList']->value) > 0) {?>
                                    <option disabled>----- <?php echo __("School");?>
 -----</option>
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['schoolList']->value, 'school');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['school']->value) {
?>
                                        <option data-type="school" value="<?php echo $_smarty_tpl->tpl_vars['school']->value['page_name'];?>
" <?php if ($_smarty_tpl->tpl_vars['school']->value['page_name'] == $_smarty_tpl->tpl_vars['username']->value) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['school']->value['page_title'];?>
</option>
                                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                <?php }?>
                                <?php if (count($_smarty_tpl->tpl_vars['classList']->value) > 0) {?>
                                    <option disabled>-----<?php echo __("Class");?>
-----</option>
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['classList']->value, 'class');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class']->value) {
?>
                                        <option data-type="class" value="<?php echo $_smarty_tpl->tpl_vars['class']->value['group_name'];?>
" <?php if ($_smarty_tpl->tpl_vars['class']->value['group_name'] == $_smarty_tpl->tpl_vars['username']->value) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['class']->value['group_title'];?>
</option>
                                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>
a
                                <?php }?>
                                <?php if (count($_smarty_tpl->tpl_vars['childrenList']->value) > 0) {?>
                                    <option disabled>-----<?php echo __("Child");?>
-----</option>
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['childrenList']->value, 'childOb');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['childOb']->value) {
?>
                                        <option <?php if ($_smarty_tpl->tpl_vars['childOb']->value['school_id'] > 0) {?>data-type="child" value="<?php echo $_smarty_tpl->tpl_vars['childOb']->value['child_id'];?>
" <?php if ($_smarty_tpl->tpl_vars['childOb']->value['child_id'] == $_smarty_tpl->tpl_vars['child']->value['child_id']) {?>selected<?php }?> <?php } else { ?>data-type="childinfo" value="<?php echo $_smarty_tpl->tpl_vars['childOb']->value['child_parent_id'];?>
" <?php if ($_smarty_tpl->tpl_vars['childOb']->value['child_parent_id'] == $_smarty_tpl->tpl_vars['child']->value['child_parent_id']) {?>selected<?php }
}?>><?php echo $_smarty_tpl->tpl_vars['childOb']->value['child_name'];?>
</option>
                                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                <?php }?>
                            </select>
                        </div>
                    <?php }?>
                    <div class="panel-body with-nav collapse navbar-collapse" id="collapse">
                        <ul class="side-nav metismenu js_metisMenu nav navbar-nav navbar-right">
                            <li <?php if ($_smarty_tpl->tpl_vars['view']->value == '') {?>class="active selected"<?php }?>>
                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/child/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
">
                                    <i class="fas fa-tachometer-alt fa-fw fa-lg pr10"></i> <?php echo __("Dashboard");?>

                                    [<?php echo $_smarty_tpl->tpl_vars['child']->value['child_name'];?>
]
                                </a>
                            </li>
                            
                            <li class="menu_header border_top">
                                <a><?php echo __("Tuition info");?>
</a>
                            </li>
                            <?php if ($_smarty_tpl->tpl_vars['school']->value['config']['attendance']) {?>
                                <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "attendance") {?>class="active"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/child/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
/attendance/resign">
                                        <i class="fa fa-tasks fa-lg fa-fw pr10"></i> <?php echo __("Resign");?>

                                    </a>
                                </li>
                            <?php }?>
                            <?php if ($_smarty_tpl->tpl_vars['school']->value['config']['services']) {?>
                                <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "services") {?>class="active"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/child/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
/services">
                                        <i class="fa fa-life-ring fa-lg fa-fw pr10"></i> <?php echo __("Service");?>

                                        <span class="fa arrow"></span>
                                    </a>
                                    <ul>
                                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "services" && $_smarty_tpl->tpl_vars['sub_view']->value == '') {?> class="active selected"<?php }?>>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/child/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
/services">
                                                <i class="fa fa-caret-right fa-fw pr10"></i> <?php echo __("Service usage information");?>

                                            </a>
                                        </li>
                                        <?php if ($_smarty_tpl->tpl_vars['school']->value['config']['allow_parent_register_service'] == 1) {?>
                                            <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "services" && $_smarty_tpl->tpl_vars['sub_view']->value == "reg") {?> class="active selected" <?php }?>>
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/child/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
/services/reg">
                                                    <i class="fa fa-caret-right fa-fw pr10"></i> <?php echo __("Register service");?>

                                                </a>
                                            </li>
                                        <?php }?>
                                    </ul>
                                </li>
                            <?php }?>
                            <?php if ($_smarty_tpl->tpl_vars['school']->value['config']['pickup']) {?>
                                <?php if ($_smarty_tpl->tpl_vars['pickup_configured']->value) {?>
                                    <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "pickup") {?>class="active"<?php }?>>
                                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/child/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
/pickup">
                                            <i class="fa fa-universal-access fa-lg fa-fw pr10"></i> <?php echo __("Đón muộn");?>

                                            <span class="fa arrow"></span>
                                        </a>
                                        <ul>
                                            <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "pickup" && $_smarty_tpl->tpl_vars['sub_view']->value == '') {?> class="active selected"<?php }?>>
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/child/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
/pickup">
                                                    <i class="fa fa-caret-right fa-fw pr10"></i> <?php echo __("Information");?>

                                                </a>
                                            </li>
                                            <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "pickup" && $_smarty_tpl->tpl_vars['sub_view']->value == "list") {?> class="active selected" <?php }?>>
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/child/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
/pickup/list">
                                                    <i class="fa fa-caret-right fa-fw pr10"></i> <?php echo __("Lists");?>

                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                <?php }?>
                            <?php }?>
                            <?php if ($_smarty_tpl->tpl_vars['school']->value['config']['tuitions']) {?>
                                <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "tuitions") {?>class="active"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/child/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
/tuitions">
                                        <i class="fas fa-money-bill-alt fa-lg fa-fw pr10"></i> <?php echo __("Tuition");?>

                                    </a>
                                </li>
                            <?php }?>
                            
                            
                            <li class="menu_header border_top">
                                <a><?php echo __("Information for student");?>
</a>
                            </li>
                            <?php if ($_smarty_tpl->tpl_vars['school']->value['config']['schedules']) {?>
                                <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "schedules") {?>class="active"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/child/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
/schedules">
                                        <i class="fas fa-calendar-alt fa-lg fa-fw pr10"></i> <?php echo __("Schedule");?>

                                    </a>
                                </li>
                            <?php }?>
                            <?php if ($_smarty_tpl->tpl_vars['school']->value['config']['menus']) {?>
                                <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "menus") {?>class="active"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/child/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
/menus">
                                        <i class="fas fa-utensils fa-lg fa-fw pr10"></i> <?php echo __("Menu");?>

                                    </a>
                                </li>
                            <?php }?>
                            <?php if ($_smarty_tpl->tpl_vars['school']->value['config']['events']) {?>
                                <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "events") {?>class="active"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/child/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
/events">
                                        <i class="fa fa-bell fa-lg fa-fw pr10"></i> <?php echo __("Notification - Event");?>

                                    </a>
                                </li>
                            <?php }?>

                            <?php if ($_smarty_tpl->tpl_vars['school']->value['config']['reports']) {?>
                                <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "reports") {?>class="active"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/child/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
/reports">
                                        <i class="fa fa-book fa-lg fa-fw pr10"></i> <?php echo __("Contact book");?>

                                    </a>
                                </li>
                            <?php }?>
                            <?php if ($_smarty_tpl->tpl_vars['school']->value['config']['points']) {?>
                                <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "points") {?>class="active"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/child/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
/points">
                                        <i class="fa fa-book fa-lg fa-fw pr10"></i> <?php echo __("Points");?>

                                    </a>
                                </li>
                            <?php }?>
                            
                            
                            <li class="menu_header border_top">
                                <a><?php echo __("School utility");?>
</a>
                            </li>
                            <?php if ($_smarty_tpl->tpl_vars['school']->value['config']['medicines']) {?>
                                <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "medicines") {?>class="active"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/child/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
/medicines">
                                        <i class="fa fa-medkit fa-lg fa-fw pr10"></i> <?php echo __("Medicines");?>

                                    </a>
                                </li>
                            <?php }?>
                            <?php if ($_smarty_tpl->tpl_vars['school']->value['config']['display_children_list'] == 1) {?>
                                <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "contact") {?>class="active"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/child/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
/contact">
                                        <i class="fa fa-list fa-lg fa-fw pr10"></i> <?php echo __("Contact list");?>

                                    </a>
                                </li>
                            <?php }?>
                            <?php if ($_smarty_tpl->tpl_vars['school']->value['config']['children']) {?>
                                <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "informations") {?>class="active"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/child/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
/informations">
                                        <i class="fa fa-info-circle fa-lg fa-fw pr10"></i> <?php echo __("Pickup information");?>

                                    </a>
                                </li>
                            <?php }?>
                            <?php if ($_smarty_tpl->tpl_vars['school']->value['config']['feedback']) {?>
                                <li id="li-child-feedback" <?php if ($_smarty_tpl->tpl_vars['view']->value == "feedback") {?>class="active"<?php }?>>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/child/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
/feedback">
                                        <i class="fa fa-envelope fa-lg fa-fw pr10"></i> <?php echo __("Feedback for the school");?>

                                    </a>
                                </li>
                            <?php }?>
                            
                            <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "leaveschool") {?>class="active"<?php }?>>
                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/child/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
/leaveschool">
                                    <i class="fa fa-minus-circle fa-lg fa-fw pr10"></i> <?php echo __("Leave school");?>

                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>

        <div class="col-md-9 col-sm-9">
            <?php $_smarty_tpl->_subTemplateRender('file:ci/child/child.help.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

        </div>
        <div class="col-md-9 col-sm-9">
            <?php if ($_smarty_tpl->tpl_vars['view']->value == '') {?>
                <?php $_smarty_tpl->_subTemplateRender('file:ci/child/child.dashboard.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

            <?php } else { ?>
                <?php $_smarty_tpl->_subTemplateRender("file:ci/child/child.".((string)$_smarty_tpl->tpl_vars['view']->value).".tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

            <?php }?>
        </div>
    </div>
</div>
<!-- page content -->


<?php $_smarty_tpl->_subTemplateRender('file:_footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
