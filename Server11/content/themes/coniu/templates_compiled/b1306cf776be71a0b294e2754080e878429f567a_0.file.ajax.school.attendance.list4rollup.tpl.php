<?php
/* Smarty version 3.1.31, created on 2021-03-31 09:55:06
  from "D:\workplace\Server11\content\themes\coniu\templates\ci\school\ajax.school.attendance.list4rollup.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_6063e48acdf5a4_76438574',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b1306cf776be71a0b294e2754080e878429f567a' => 
    array (
      0 => 'D:\\workplace\\Server11\\content\\themes\\coniu\\templates\\ci\\school\\ajax.school.attendance.list4rollup.tpl',
      1 => 1575865657,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6063e48acdf5a4_76438574 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!-- <pre>
    <?php echo print_r($_smarty_tpl->tpl_vars['data']->value);?>

</pre> -->
<ul class="nav nav-tabs mb10" id="myTab" role="tablist">
    <li class="nav-item active">
        <a class="nav-link" id="came-tab" data-toggle="tab" href="#come_attendance" role="tab" aria-controls="came" aria-selected="true"><?php echo __("Điểm danh đến");?>
</a>
    </li>
    <?php if ($_smarty_tpl->tpl_vars['data']->value['isRollUp']) {?>
        <li class="nav-item">
            <a class="nav-link" id="go-tab" data-toggle="tab" href="#go_attendance" role="tab" aria-controls="go" aria-selected="false"><?php echo __("Điểm danh về");?>
</a>
        </li>
    <?php }?>
</ul>
<div class="tab-content" id="myTabContent">
    <div class="tab-pane fade active in" id="come_attendance" role="tabpanel" aria-labelledby="came-tab">
        <div class="form-group">
            <div class="col-sm-9">
                <button type="submit" class="btn btn-primary padrl30" <?php if ((count($_smarty_tpl->tpl_vars['data']->value['detail']) == 0)) {?>disabled<?php }?>><?php echo __("Save");?>
</button>
            </div>
        </div>

        <div>
            <?php if (($_smarty_tpl->tpl_vars['data']->value['isRollUp'] == 0) || ($_smarty_tpl->tpl_vars['data']->value['is_checked'] == 0)) {?>
                <strong><?php echo __("Attendance list");?>
&nbsp;(<?php echo count($_smarty_tpl->tpl_vars['data']->value['detail']);?>
)</strong>
            <?php } else { ?>
                <?php echo $_smarty_tpl->tpl_vars['data']->value['user_fullname'];?>
  <?php echo __("have rolled up at");?>
  <?php echo $_smarty_tpl->tpl_vars['data']->value['recorded_at'];?>

                <br/>
                <strong><?php echo __("Present");?>
</strong>:&nbsp;<?php echo $_smarty_tpl->tpl_vars['data']->value['present_count'];?>
&nbsp;
                <strong><?php echo __("Absence");?>
</strong>:&nbsp;<?php echo $_smarty_tpl->tpl_vars['data']->value['absence_count'];?>

            <?php }?>
        </div>
        <input type="hidden" name="attendance_id" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['attendance_id'];?>
"/>
        <table class="table table-striped table-bordered table-hover">
            <thead>
            <tr>
                <th rowspan="2">#</th>
                <th rowspan="2" width="60px"><?php echo __("Child picture");?>
</th>
                <th rowspan="2"><?php echo __("Full name");?>
</th>
                <th colspan="3">
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    <?php echo __("Status");?>

                </th>
                <th rowspan="2"><?php echo __("Note");?>
</th>
            </tr>
            <tr>
                <th width="80px"><?php echo __("Present");?>
</th>
                <th width="80px"><?php echo __("With permission");?>
</th>
                <th width="80px"><?php echo __("Without permission");?>
</th>
            </tr>
            </thead>
            <tbody>
            <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['data']->value['detail'], 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
                <tr <?php if ($_smarty_tpl->tpl_vars['row']->value['child_status'] == 0) {?> class="row-disable" <?php }?>>
                    <td align="center" class="align-middle" <?php if ($_smarty_tpl->tpl_vars['row']->value['is_parent']) {?>rowspan="2"<?php }?>><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</td>
                    <td align="center" class="align-middle" <?php if ($_smarty_tpl->tpl_vars['row']->value['is_parent']) {?>rowspan="2"<?php }?>>
                        <div class="img_picture_box">
                            <img class="img-responsive" src="<?php echo $_smarty_tpl->tpl_vars['row']->value['child_picture'];?>
">
                        </div>
                    </td>
                    <td class="align-middle" <?php if ($_smarty_tpl->tpl_vars['row']->value['is_parent']) {?>rowspan="2"<?php }?>>
                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/attendance/child/<?php echo $_smarty_tpl->tpl_vars['row']->value['child_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['row']->value['child_name'];?>
</a>
                        &nbsp;&nbsp;
                        <?php if (($_smarty_tpl->tpl_vars['row']->value['is_parent'] > 0) && ($_smarty_tpl->tpl_vars['row']->value['feedback'] == 0)) {?>
                            <a class="btn btn-xs btn-danger js_class-confirm-attendance" id="button_<?php echo $_smarty_tpl->tpl_vars['row']->value['attendance_detail_id'];?>
" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"
                               data-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['child_id'];?>
" data-detail-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['attendance_detail_id'];?>
"><?php echo __("Confirm");?>
</a>
                        <?php }?>
                    </td>
                    <td align="center" class="align-middle">
                        <input type="radio" name="rollup_<?php echo $_smarty_tpl->tpl_vars['row']->value['child_id'];?>
" value="<?php echo @constant('ATTENDANCE_PRESENT');?>
" <?php if (($_smarty_tpl->tpl_vars['data']->value['is_checked'] == 1)) {?>
                            <?php if ($_smarty_tpl->tpl_vars['row']->value['status'] == @constant('ATTENDANCE_PRESENT')) {?>
                                checked
                            <?php }?>
                                <?php } elseif (($_smarty_tpl->tpl_vars['data']->value['is_checked'] == 0 && $_smarty_tpl->tpl_vars['data']->value['isRollUp'] > 0)) {?>
                            <?php if (!isset($_smarty_tpl->tpl_vars['row']->value['status']) || $_smarty_tpl->tpl_vars['row']->value['status'] == @constant('ATTENDANCE_PRESENT')) {?>
                                checked
                            <?php }?>
                                <?php } elseif (($_smarty_tpl->tpl_vars['data']->value['isRollUp'] == 0)) {?>
                        checked
                                <?php }?>>
                    </td>
                    <td align="center" class="align-middle">
                        <input type="radio" name="rollup_<?php echo $_smarty_tpl->tpl_vars['row']->value['child_id'];?>
" value="<?php echo @constant('ATTENDANCE_ABSENCE');?>
" <?php if ((($_smarty_tpl->tpl_vars['data']->value['is_checked'] == 1) && ($_smarty_tpl->tpl_vars['row']->value['status'] == @constant('ATTENDANCE_ABSENCE'))) || ($_smarty_tpl->tpl_vars['data']->value['isRollUp'] && isset($_smarty_tpl->tpl_vars['row']->value['status']) && $_smarty_tpl->tpl_vars['row']->value['status'] == @constant('ATTENDANCE_ABSENCE'))) {?>checked<?php }?>>
                    </td>
                    <td align="center" class="align-middle">
                        <input type="radio" name="rollup_<?php echo $_smarty_tpl->tpl_vars['row']->value['child_id'];?>
" value="<?php echo @constant('ATTENDANCE_ABSENCE_NO_REASON');?>
" <?php if (($_smarty_tpl->tpl_vars['data']->value['is_checked'] == 1 || $_smarty_tpl->tpl_vars['data']->value['isRollUp']) && (isset($_smarty_tpl->tpl_vars['row']->value['status']) && $_smarty_tpl->tpl_vars['row']->value['status'] == @constant('ATTENDANCE_ABSENCE_NO_REASON'))) {?>checked<?php }?>>
                    </td>
                    <td class="align-middle" <?php if ($_smarty_tpl->tpl_vars['row']->value['is_parent']) {?>rowspan="2"<?php }?>>
                        
                        <input type="text" name="allReasons[]" maxlength="512" style="width: 100%" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['reason'];?>
" placeholder="<?php echo __("Absent reason");?>
">
                    </td>
                    <input type="hidden" name="allChildIds[]" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['child_id'];?>
">
                    <input type="hidden" name="allResignTime[]" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['resign_time'];?>
">
                    <input type="hidden" name="allIsParent[]" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['is_parent'];?>
">
                    <input type="hidden" name="allRecordUserId[]" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['recorded_user_id'];?>
">
                    <input type="hidden" name="allStartDate[]" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['start_date'];?>
">
                    <input type="hidden" name="allEndDate[]" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['end_date'];?>
">
                    <input type="hidden" name="feedback_<?php echo $_smarty_tpl->tpl_vars['row']->value['child_id'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['feedback'];?>
" id="feedback_<?php echo $_smarty_tpl->tpl_vars['row']->value['child_id'];?>
">
                </tr>
                <?php if ($_smarty_tpl->tpl_vars['row']->value['is_parent']) {?>
                    <tr>
                        <td colspan="3" align="center"><?php echo __("Resign");?>
: <?php echo $_smarty_tpl->tpl_vars['row']->value['resign_time'];?>
</td>
                    </tr>
                <?php }?>
                <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>


            <?php if ($_smarty_tpl->tpl_vars['idx']->value == 1) {?>
                <tr class="odd">
                    <td valign="top" align="center" colspan="7" class="dataTables_empty">
                        <?php echo __("No data available in table");?>

                    </td>
                </tr>
            <?php }?>

            </tbody>
        </table>
    </div>

    <div class="tab-pane fade" id="go_attendance" role="tabpanel" aria-labelledby="go-tab">
        <div class="form-group">
            <div class="col-sm-9">
                <button type="submit" class="btn btn-primary padrl30" <?php if ((count($_smarty_tpl->tpl_vars['data_cameback']->value['detail']) == 0)) {?>disabled<?php }?>><?php echo __("Save");?>
</button>
            </div>
        </div>

        <div>
            <?php if (($_smarty_tpl->tpl_vars['data_cameback']->value['isRollUp'] == 0)) {?>
                
            <?php } else { ?>
                <?php echo $_smarty_tpl->tpl_vars['data_cameback']->value['user_fullname'];?>
  <?php echo __("have rolled up at");?>
  <?php echo $_smarty_tpl->tpl_vars['data_cameback']->value['recorded_at'];?>

                <br/>
                <strong><?php echo __("Came back");?>
</strong>:&nbsp;<?php echo $_smarty_tpl->tpl_vars['data_cameback']->value['came_back_count'];?>
&nbsp;
                <strong><?php echo __("Not came back");?>
</strong>:&nbsp;<?php echo $_smarty_tpl->tpl_vars['data_came_back']->value['not_came_back_count'];?>

            <?php }?>
        </div>
        <input type="hidden" name="attendance_id" value="<?php echo $_smarty_tpl->tpl_vars['data_cameback']->value['attendance_id'];?>
"/>
        <table class="table table-striped table-bordered table-hover">
            <thead>
            <tr>
                <th>#</th>
                <th><?php echo __("Child picture");?>
</th>
                <th><?php echo __("Full name");?>
</th>
                <th><?php echo __("Out of school");?>
</th>
                <th><?php echo __("Note");?>
</th>
                <th><?php echo __("Came back");?>
 <input type="checkbox" id="attendance_came_back_checkall"> </th>
            </tr>
            </thead>
            <tbody>
            <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['data_cameback']->value['detail'], 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
                <?php if ($_smarty_tpl->tpl_vars['row']->value['child_status'] == 1 && $_smarty_tpl->tpl_vars['row']->value['status'] != 0 && $_smarty_tpl->tpl_vars['row']->value['status'] != 4) {?>
                    <tr <?php if ($_smarty_tpl->tpl_vars['row']->value['child_status'] == 0 || $_smarty_tpl->tpl_vars['row']->value['status'] == 0 || $_smarty_tpl->tpl_vars['row']->value['status'] == 4) {?> class="row-disable" <?php }?>>
                        <td align="center" class="align-middle"><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</td>
                        <td align="center" class="align-middle">
                            <div class="img_picture_box">
                                <img class="img-responsive" src="<?php echo $_smarty_tpl->tpl_vars['row']->value['child_picture'];?>
">
                            </div>
                        </td>
                        <td class="align-middle">
                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/attendance/child/<?php echo $_smarty_tpl->tpl_vars['row']->value['child_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['row']->value['child_name'];?>
</a>
                        </td>
                        <td align="center" class="align-middle">
                            <input type='text' name="cameback_time[]" class="cameback_time"
                                   id="cameback_time_<?php echo $_smarty_tpl->tpl_vars['row']->value['child_id'];?>
" value="<?php if ($_smarty_tpl->tpl_vars['row']->value['came_back_status']) {
echo $_smarty_tpl->tpl_vars['row']->value['came_back_time'];
} else {
echo $_smarty_tpl->tpl_vars['hour']->value;
}?>"
                                   style="width: 70px; color:blue; text-align: center"/>
                        </td>
                        <td align="center" class="align-middle">
                            <input type='text' name="cameback_note[]" class="cameback_note" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['came_back_note'];?>
"/>
                        </td>
                        <td class="align-middle" align="center">
                            
                            <input type="checkbox" name="status_<?php echo $_smarty_tpl->tpl_vars['row']->value['child_id'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['came_back_status'];?>
" class="attendance_cameback_check" <?php if ($_smarty_tpl->tpl_vars['row']->value['came_back_status']) {?>checked<?php }?>>
                        </td>
                        <input type="hidden" name="allChildBackIds[]" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['child_id'];?>
">
                    </tr>
                    <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                <?php }?>
            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>


            <?php if ($_smarty_tpl->tpl_vars['idx']->value == 1) {?>
                <tr class="odd">
                    <td valign="top" align="center" colspan="5" class="dataTables_empty">
                        <?php echo __("No data available in table");?>

                    </td>
                </tr>
            <?php }?>

            </tbody>
        </table>
    </div>
</div>

<?php echo '<script'; ?>
>
    $(document).ready(function(){
        $('.cameback_time').datetimepicker({
            format: TIME_FORMAT,
            sideBySide: true
        });
    });
    $('body').on('focus', '.cameback_time', function () {
        var currentTIme = $(this).val();
        if ($(this).val() == '') {
            var time = new Date();
            var curent = time.getHours() + ':' + time.getMinutes();
            $(this).val(curent);
        }
    });
<?php echo '</script'; ?>
>
<?php }
}
