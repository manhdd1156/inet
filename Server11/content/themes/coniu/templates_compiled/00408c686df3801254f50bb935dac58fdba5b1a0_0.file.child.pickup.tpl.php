<?php
/* Smarty version 3.1.31, created on 2021-03-31 10:48:01
  from "D:\workplace\Server11\content\themes\coniu\templates\ci\child\child.pickup.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_6063f0f1858f36_30330375',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '00408c686df3801254f50bb935dac58fdba5b1a0' => 
    array (
      0 => 'D:\\workplace\\Server11\\content\\themes\\coniu\\templates\\ci\\child\\child.pickup.tpl',
      1 => 1561246270,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:ci/child/ajax.child.pickup.tpl' => 1,
  ),
),false)) {
function content_6063f0f1858f36_30330375 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
            <div class="pull-right flip">
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/child/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
/pickup/list" class="btn btn-default">
                    <i class="fa fa-list"></i>  <?php echo __("Lists");?>

                </a>
            </div>
        <?php }?>
        

        <i class="fa fa-universal-access fa-fw fa-lg pr10"></i>
        <?php echo __("Late pickup");?>

        <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
            &rsaquo; <?php echo __("Information");?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "list") {?>
            &rsaquo; <?php echo __("Lists");?>

        <?php }?>
    </div>
    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
        <div class="panel-body">
            <div class="form-horizontal" style="font-size: 14px">

                <div class="form-group mb0">
                    <div class="text-center">
                        <label style="font-size: 15px"><?php echo __("THÔNG TIN DỊCH VỤ ĐÓN MUỘN");?>
<br><br>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">
                        <?php echo __("Pickup beginning time");?>

                    </label>
                    <div class="col-sm-2">
                        <input class="form-control text-center" style="background-color: #FFFFFF"
                               value="<?php if (!empty($_smarty_tpl->tpl_vars['template']->value['price_list'][0]['beginning_time'])) {?> <?php echo $_smarty_tpl->tpl_vars['template']->value['price_list'][0]['beginning_time'];?>
 <?php } else { ?> - <?php }?>" readonly/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">
                        <?php echo __("Bảng giá");?>

                    </label>
                    <div class="col-sm-9">

                        <table class="table table-bordered table-hover mb20" id="pickup_price_list">
                            <thead>
                            <tr>
                                <th class="col-sm-3"><center><?php echo __("Pickup before");?>
</center></th>
                                <th class="col-sm-3"><center><?php echo __("Unit price");?>
</center></th>
                            </tr>
                            </thead>
                            <tbody>

                            <?php if (isset($_smarty_tpl->tpl_vars['template']->value['price_list']) && count($_smarty_tpl->tpl_vars['template']->value['price_list'] > 0)) {?>
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['template']->value['price_list'], 'value');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['value']->value) {
?>
                                    <tr>
                                        <td align="center"><?php echo $_smarty_tpl->tpl_vars['value']->value['ending_time'];?>
</td>
                                        <td align="center"><?php echo number_format($_smarty_tpl->tpl_vars['value']->value['unit_price'],0,'.',',');?>
</td>
                                    </tr>
                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                            <?php }?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">
                        <?php echo __("Services");?>

                    </label>
                    <div class="col-sm-9">

                        <table class="table table-bordered table-hover mb0" id="pickup_service">
                            <thead>
                            <tr>
                                <th class="col-sm-2"><center><?php echo __("Service name");?>
</center></th>
                                <th class="col-sm-2"><center><?php echo __("Unit price");?>
</center></th>
                                <th class="col-sm-2"><center><?php echo __("Description");?>
</center></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (isset($_smarty_tpl->tpl_vars['template']->value['services']) && count($_smarty_tpl->tpl_vars['template']->value['services'] > 0)) {?>
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['template']->value['services'], 'service');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['service']->value) {
?>
                                    <tr>
                                        <td align="center"><?php echo $_smarty_tpl->tpl_vars['service']->value['service_name'];?>
</td>
                                        <td align="center"><?php echo number_format($_smarty_tpl->tpl_vars['service']->value['fee'],0,'.',',');?>
</td>
                                        <td align="center"><?php echo $_smarty_tpl->tpl_vars['service']->value['description'];?>
</td>
                                    </tr>
                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                            <?php }?>
                            </tbody>
                        </table>

                    </div>
                </div>

            </div>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "list") {?>
        <div class="panel-body with-table">
            <div class="row">
                <div class='col-sm-12'>
                    <div class='col-sm-2'></div>
                    <div class='col-sm-3'>
                        <div class='input-group date' id='fromDate_pickup'>
                            <input type='text' name="fromDate" id="fromDate" value="<?php echo $_smarty_tpl->tpl_vars['fromDate']->value;?>
" class="form-control" placeholder="<?php echo __("From date");?>
"/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                    <div class='col-sm-3'>
                        <div class='input-group date' id='toDate_pickup'>
                            <input type='text' name="toDate" id="toDate" value="<?php echo $_smarty_tpl->tpl_vars['toDate']->value;?>
" class="form-control" placeholder="<?php echo __("To date");?>
"/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                    <div class='col-sm-3'>
                        <div class="form-group">
                            <button class="btn btn-default js_pickup-search" data-child="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
" ><?php echo __("Search");?>
</button>
                        </div>
                    </div>
                </div>
            </div>
            <br/>

            <div class="table-responsive" id="pickup_list_child">
                <?php $_smarty_tpl->_subTemplateRender("file:ci/child/ajax.child.pickup.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

            </div>
        </div>
    <?php }?>
</div><?php }
}
