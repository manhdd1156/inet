<?php
/* Smarty version 3.1.31, created on 2019-06-24 10:49:47
  from "D:\Working\Coniu\ServerSvn\trunk\Server11\content\themes\coniu\templates\_header_new.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5d10485b13d463_90347958',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '5bace0e58d20b91bd36ddb65d93a0925d91579ef' => 
    array (
      0 => 'D:\\Working\\Coniu\\ServerSvn\\trunk\\Server11\\content\\themes\\coniu\\templates\\_header_new.tpl',
      1 => 1561348164,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d10485b13d463_90347958 (Smarty_Internal_Template $_smarty_tpl) {
?>
<body class="header-fixed" data-chat-enabled="1">
<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<?php echo '<script'; ?>
>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js#xfbml=1&version=v2.12&autoLogAppEvents=1';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));<?php echo '</script'; ?>
>

<!-- Your customer chat code -->
<div class="fb-customerchat"
     attribution="setup_tool"
     page_id="1024684347570874"
     theme_color="#0084ff"
     logged_in_greeting="Xin chào, Coniu có thể giúp gì cho bạn?"
     logged_out_greeting="Xin chào, Coniu có thể giúp gì cho bạn?">
</div>
<div class="wrapper">
    <!--=== Header v6 ===-->
    <div class="header-v6 header-classic-white header-sticky header-fixed-shrink">
        <!-- Navbar -->
        <div class="navbar mega-menu" role="navigation">
            <div class="container menu_container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="menu-container">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Navbar Brand -->
                    <div class="navbar-brand">
                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
">
                            <div class="logo-img">
                                <img class="shrink-logo" src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/content/themes/<?php echo $_smarty_tpl->tpl_vars['system']->value['theme'];?>
/images/logo.png" alt="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_title'];?>
 | <?php echo __("School management and connection software");?>
" title="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_title'];?>
 | <?php echo __("School management and connection software");?>
">
                            </div>
                            <div class="logo-slogan"><?php echo __("Connect love");?>
</div>
                        </a>
                    </div>
                    <!-- ENd Navbar Brand -->

                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse navbar-responsive-collapse">
                    <div class="menu-container">
                        <ul class="nav navbar-nav">
                            <li class="active"><a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
"><?php echo __("Home");?>
</a></li>
                            <li class=""><a href="https://blog.coniu.vn/coniu-la-gi/" target="_blank"><?php echo __("Introduction");?>
</a></li>

                            
                            
                            
                            <li class=""><a href="https://blog.coniu.vn/category/tin-mam-non" target="_blank"><?php echo __("Pre - school");?>
</a></li>
                        </ul>
                    </div>
                </div><!--/navbar-collapse-->
            </div>
        </div>
    </div>
    <!-- End Navbar -->
    <!--=== End Header v6 ===--><?php }
}
