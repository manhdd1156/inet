<?php
/* Smarty version 3.1.31, created on 2021-05-19 11:26:07
  from "D:\workplace\mascom-edu-server\Server11\content\themes\coniu\templates\ci\school\school.pickup.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_60a4935f64deb4_94938490',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '273ffac7c87b9e957c2eb93b4e71d633974b9bef' => 
    array (
      0 => 'D:\\workplace\\mascom-edu-server\\Server11\\content\\themes\\coniu\\templates\\ci\\school\\school.pickup.tpl',
      1 => 1620722262,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:ci/school/ajax.school.pickup.assign.tpl' => 1,
    'file:ci/school/ajax.school.pickuplist.tpl' => 1,
    'file:ci/school/ajax.child.pickup.tpl' => 1,
    'file:ci/class/ajax.pickupchild.tpl' => 1,
  ),
),false)) {
function content_60a4935f64deb4_94938490 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="" style="position: relative">
    <?php if ($_smarty_tpl->tpl_vars['school']->value['school_step'] == SCHOOL_STEP_FINISH || $_smarty_tpl->tpl_vars['school']->value['grade'] != 0) {?>
        <div class="dashboard_fixed">
            <button class="btn btn-xs btn-default js_school-pickup-guide" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"
                    data-id="<?php echo $_smarty_tpl->tpl_vars['user']->value->_data["user_id"];?>
">
                <i class="fa fa-chevron-down <?php if ($_smarty_tpl->tpl_vars['cookie']->value) {?>hidden<?php }?>" aria-hidden="true" id="guide_hidden"></i>
                <i class="fa fa-chevron-up <?php if (!$_smarty_tpl->tpl_vars['cookie']->value) {?>hidden<?php }?>" aria-hidden="true" id="guide_show"></i>
            </button>
        </div>
        <div class="panel panel-default <?php if ($_smarty_tpl->tpl_vars['cookie']->value == 0) {?>hidden<?php }?>" id="guide_pickup">
            <div class="guide_school">
                <div class="init_school text-left pad5 full_width">
                    <div class="full_width pl5">
                        <strong><?php echo __("The execution's order guide");?>
:</strong>
                    </div>
                    <div class="pad5">
                        <strong><?php echo __("Handlings with late pickup");?>
:</strong>
                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/pickup/template"
                           class="pad5 font12">1. <?php echo __("Setting and create price table");?>
</a>
                        <i class="fas fa-long-arrow-alt-right" aria-hidden="true"></i>
                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/pickup/assign"
                           class="pad5 font12">2. <?php echo __("Assign teacher");?>
</a>
                        <i class="fas fa-long-arrow-alt-right" aria-hidden="true"></i>
                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/pickup/list"
                           class="pad5 font12">3. <?php echo __("Lists");?>
</a>
                        <i class="fas fa-long-arrow-alt-right" aria-hidden="true"></i>
                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/pickup/child"
                           class="pad5 font12">4. <?php echo __("Detail one student");?>
</a>
                    </div>
                </div>
            </div>
        </div>
    <?php }?>
    <div class="panel panel-default">
        <div class="panel-heading with-icon">
            <?php if (($_smarty_tpl->tpl_vars['sub_view']->value == "detail") && $_smarty_tpl->tpl_vars['canEdit']->value) {?>
                <div class="pull-right flip">
                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/pickup/add/<?php echo $_smarty_tpl->tpl_vars['pickup']->value['pickup_id'];?>
"
                       class="btn btn-default">
                        <i class="fa fa-plus"></i> <?php echo __("Add child");?>

                    </a>
                </div>
            <?php }?>
            <?php if (($_smarty_tpl->tpl_vars['sub_view']->value == "list") && $_smarty_tpl->tpl_vars['canEdit']->value) {?>
                <div class="pull-right flip">

                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/pickup/child" class="btn btn-default">
                        <i class="fa fa-history"></i> <?php echo __("Using late Pickup information");?>

                    </a>

                    
                </div>
            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['school']->value['school_step'] == SCHOOL_STEP_FINISH || $_smarty_tpl->tpl_vars['school']->value['grade'] != 0) {?>
                <div class="pull-right flip" style="margin-right: 5px">
                    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value != "template" && $_smarty_tpl->tpl_vars['sub_view']->value != "assign") {?>
                        <a href="https://blog.coniu.vn/huong-dan-quan-ly-cau-hinh-tra-muon-tren-website-coniu/"
                           class="btn btn-info  btn_guide" target="_blank">
                            <i class="fa fa-info-circle"></i> <?php echo __("Guide");?>

                        </a>
                    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "template") {?>
                        <a href="https://blog.coniu.vn/huong-dan-thiet-lap-chuc-nang-tra-tre-muon/"
                           class="btn btn-info  btn_guide" target="_blank">
                            <i class="fa fa-info-circle"></i> <?php echo __("Late pickup configuration guide");?>

                        </a>
                    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "assign") {?>
                        <a href="https://blog.coniu.vn/huong-dan-phan-cong-giao-vien-vao-lop-trong-muon/"
                           class="btn btn-info  btn_guide" target="_blank">
                            <i class="fa fa-info-circle"></i> <?php echo __("Assign teacher guide");?>

                        </a>
                    <?php }?>
                </div>
            <?php }?>
            <i class="fa fa-universal-access fa-fw fa-lg pr10"></i>
            <?php echo __("Late PickUp");?>

            <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == "template") {?>
                &rsaquo; <?php echo __("Setting and create price table");?>

            <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "assign") {?>
                &rsaquo; <?php echo __("Assign teacher");?>

            <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "list") {?>
                &rsaquo; <?php echo __("Lists");?>

            <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "child") {?>
                &rsaquo; <?php echo __("Using late Pickup information");?>

            <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "detail") {?>
                &rsaquo; <?php echo __("Detail");?>

            <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
                &rsaquo; <?php echo __("Add child");?>

            <?php }?>
        </div>

        <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == "template") {?>
            <div class="panel-body">
                <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_pickup.php">
                    <input type="hidden" name="school_username" id="school_username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                    <input type="hidden" name="do" value="add_template"/>

                    <div class="form-group">
                        <div class="text-center">
                            <?php if (is_null($_smarty_tpl->tpl_vars['template']->value)) {?>
                                <strong>
                                    <font color="#ff4500"><label><?php echo __("The school has not establish the late pickup configuration");?>
</label></font>
                                </strong>
                            <?php } else { ?>
                                <strong>
                                    <font color="#228b22"><label><?php echo __("The school has established the late pickup configuration");?>
</label></font>
                                </strong>
                            <?php }?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left"><?php echo __("Pickup beginning time");?>
 (*)</label>
                        <div class='col-sm-3'>
                            <div class='input-group date' id='pickup_beginpicker'>
                                <input type='text' name="begin_pickup_time" class="form-control text-center"
                                       value="<?php if (!empty($_smarty_tpl->tpl_vars['template']->value['price_list'][0]['beginning_time'])) {?> <?php echo $_smarty_tpl->tpl_vars['template']->value['price_list'][0]['beginning_time'];?>
 <?php } else { ?>17:00<?php }?>"
                                       required/>
                                <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">
                            <?php echo __("Price table detail");?>
 (*)
                        </label>
                        <div class="col-sm-9">

                            <table class="table table-bordered table-hover mb0" id="pickup_price_list">
                                <thead>
                                <tr>
                                    <th class="col-sm-3" align="center"><?php echo __("Pickup before");?>
</th>
                                    <th class="col-sm-3" align="center"><?php echo __("Unit price");?>
</th>
                                    <th class="col-sm-2" align="center"><?php echo __("Actions");?>
</th>
                                </tr>
                                </thead>
                                <tbody>

                                <?php if (isset($_smarty_tpl->tpl_vars['template']->value['price_list']) && count($_smarty_tpl->tpl_vars['template']->value['price_list'] > 0)) {?>
                                    <?php $_smarty_tpl->_assignInScope('i', 1);
?>
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['template']->value['price_list'], 'value');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['value']->value) {
?>
                                        <tr>
                                            
                                            <td><input type="text" name="ending_time[]"
                                                       class="form-control ending_time text-center"
                                                       value="<?php echo $_smarty_tpl->tpl_vars['value']->value['ending_time'];?>
" required></td>
                                            <td><input type="text" name="unit_price[]"
                                                       class="form-control text-center money_tui"
                                                       value="<?php echo $_smarty_tpl->tpl_vars['value']->value['unit_price'];?>
" required></td>
                                            <?php if ($_smarty_tpl->tpl_vars['i']->value == 1) {?>
                                                <td></td>
                                            <?php } else { ?>
                                                <td class="align-middle" align="center">
                                                    <a class="btn btn-xs btn-danger mt5 js_pickup_price-delete"
                                                       data-handle="delete">
                                                        <?php echo __("Delete");?>

                                                    </a>
                                                </td>
                                            <?php }?>
                                        </tr>
                                        <?php $_smarty_tpl->_assignInScope('i', $_smarty_tpl->tpl_vars['i']->value+1);
?>
                                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                <?php } else { ?>
                                    <tr>
                                        
                                        <td><input type="text" name="ending_time[]"
                                                   class="form-control ending_time text-center" required></td>
                                        <td><input type="text" name="unit_price[]"
                                                   class="form-control text-center money_tui" required></td>
                                        <td></td>
                                    </tr>
                                <?php }?>

                                </tbody>
                            </table>
                            
                            <button type="button" class="btn btn-success js_pickup_price-add">+</button>

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">
                            <?php echo __("Pickup services");?>

                        </label>
                        <div class="col-sm-9">
                            <?php echo __("Tạo các dịch vụ đi kèm với đón muộn (ví dụ: uống sữa muộn, ...).");?>

                            <table class="table table-bordered table-hover mb0" id="pickup_service">
                                <thead>
                                <tr>
                                    <th class="col-sm-2" align="center"><?php echo __("Service name");?>
</th>
                                    <th class="col-sm-2" align="center"><?php echo __("Unit price");?>
</th>
                                    <th class="col-sm-2" align="center"><?php echo __("Description");?>
</th>
                                    <th class="col-sm-2" align="center"><?php echo __("Actions");?>
</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if (isset($_smarty_tpl->tpl_vars['template']->value['services']) && count($_smarty_tpl->tpl_vars['template']->value['services'] > 0)) {?>
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['template']->value['services'], 'service');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['service']->value) {
?>
                                        <tr>
                                            <td class="x-hidden"><input type="hidden" name="service_id[]"
                                                                        value="<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
" required></td>
                                            <td><input type="text" name="service_name[]" class="form-control"
                                                       value="<?php echo $_smarty_tpl->tpl_vars['service']->value['service_name'];?>
" required></td>
                                            <td><input type="text" name="service_price[]"
                                                       class="form-control text-center money_tui"
                                                       value="<?php echo $_smarty_tpl->tpl_vars['service']->value['fee'];?>
" required></td>
                                            <td><input type="text" name="service_description[]" class="form-control"
                                                       value="<?php echo $_smarty_tpl->tpl_vars['service']->value['description'];?>
"></td>
                                            <td class="align-middle" align="center">
                                                <a class="btn btn-xs btn-danger mt5 js_pickup_service-delete"
                                                   data-handle="delete" data-id="<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
">
                                                    <?php echo __("Delete");?>

                                                </a>
                                            </td>
                                        </tr>
                                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                <?php }?>
                                </tbody>
                            </table>
                            
                            <button type="button" class="btn btn-success js_pickup_service-add">+</button>

                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">
                            <?php echo __("Late pickup classes");?>
 (*)
                        </label>
                        <div class="col-sm-9">

                            <table class="table table-bordered table-hover mb0" id="pickup_classes">
                                <thead>
                                <tr>
                                    <th class="col-sm-3">
                                        <center><?php echo __("Class name");?>
</center>
                                    </th>
                                    <th class="col-sm-3">
                                        <center><?php echo __("Description");?>

                                            <center>
                                    </th>
                                    <th class="col-sm-2">
                                        <center><?php echo __("Actions");?>
</center>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if (!empty($_smarty_tpl->tpl_vars['template']->value['classes'])) {?>
                                    <?php $_smarty_tpl->_assignInScope('i', 1);
?>
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['template']->value['classes'], 'class');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class']->value) {
?>
                                        <tr>
                                            <td class="x-hidden"><input type="hidden" name="class_id[]"
                                                                        value="<?php echo $_smarty_tpl->tpl_vars['class']->value['pickup_class_id'];?>
" required>
                                            </td>
                                            <td><input type="text" name="class_name[]" class="form-control"
                                                       value="<?php echo $_smarty_tpl->tpl_vars['class']->value['class_name'];?>
" required></td>
                                            <td><input type="text" name="class_description[]" class="form-control"
                                                       value="<?php echo $_smarty_tpl->tpl_vars['class']->value['description'];?>
"></td>
                                            <?php if ($_smarty_tpl->tpl_vars['i']->value == 1) {?>
                                                <td></td>
                                            <?php } else { ?>
                                                <td class="align-middle" align="center">
                                                    <a class="btn btn-xs btn-danger mt5 js_pickup_class-delete"
                                                       data-handle="delete" data-id="<?php echo $_smarty_tpl->tpl_vars['class']->value['pickup_class_id'];?>
">
                                                        <?php echo __("Delete");?>

                                                    </a>
                                                </td>
                                            <?php }?>
                                        </tr>
                                        <?php $_smarty_tpl->_assignInScope('i', $_smarty_tpl->tpl_vars['i']->value+1);
?>
                                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                <?php } else { ?>
                                    <tr>
                                        <td><input type="text" name="class_name[]" class="form-control"
                                                   value="Lớp đón muộn" required></td>
                                        <td><input type="text" name="class_description[]" class="form-control"></td>
                                        <td></td>
                                    </tr>
                                <?php }?>
                                </tbody>
                            </table>
                            <button type="button" class="btn btn-success js_pickup_class-add">+</button>

                        </div>
                    </div>

                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    

                    <div class="form-group">
                        <div class="col-sm-12 text-center">
                            <button type="submit" class="btn btn-primary padrl30"><?php echo __("Save");?>
</button>
                        </div>
                    </div>

                    <!-- success -->
                    <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                    <!-- success -->

                    <!-- error -->
                    <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                    <!-- error -->
                </form>
            </div>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "assign") {?>
            <div class="panel-body with-table" style="position: relative">
                <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_pickup.php">
                    <input type="hidden" name="school_username" id="school_username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                    <input type="hidden" name="do" value="assign"/>

                    <div class="row">
                        <div class='col-sm-12'>

                            <div class="form-group">
                                <label class="col-sm-5 control-label">
                                    <?php echo __("Phân công theo");?>

                                </label>
                                <div class="col-sm-7">
                                    <div class="radio radio-primary radio-inline">
                                        <input class="assign_radio" type="radio" name="assign_type" value="week" <?php if ($_smarty_tpl->tpl_vars['assign_type']->value == 'week') {?>checked<?php }?>>
                                        <label for="assign_week"><?php echo __("Week");?>
</label>
                                    </div>
                                    <div class="radio radio-primary radio-inline">
                                        <input class="assign_radio" type="radio" name="assign_type" value="month" <?php if ($_smarty_tpl->tpl_vars['assign_type']->value == 'month') {?>checked<?php }?>>
                                        <label for="assign_month"><?php echo __("Month");?>
</label>
                                    </div>
                                </div>
                            </div>
                            <br/>

                            <div class="form-group">
                                <div class='col-sm-1'></div>
                                <div class='col-sm-4'>
                                    <select name="pickup_class_assign" id="pickup_class_assign" class="form-control">
                                        <option value="" disabled><?php echo __("Select Class");?>
...</option>
                                        <?php $_smarty_tpl->_assignInScope('i', 1);
?>
                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['classes']->value, 'class');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class']->value) {
?>
                                            <option value="<?php echo $_smarty_tpl->tpl_vars['class']->value['pickup_class_id'];?>
" <?php if ($_smarty_tpl->tpl_vars['i']->value == 1) {?> selected <?php }?>><?php echo $_smarty_tpl->tpl_vars['class']->value['class_name'];?>
</option>
                                            <?php $_smarty_tpl->_assignInScope('i', -1);
?>
                                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                    </select>
                                </div>
                                <div class='col-sm-3 <?php if ($_smarty_tpl->tpl_vars['assign_type']->value == 'week') {?>x-hidden<?php }?>' id="div_assign_month">
                                    <div class='input-group date' id='pickup_month_picker'>
                                        <input type='text' name="month" id="month" class="form-control"
                                               placeholder="<?php echo __("Month");?>
 (*)" required/>
                                        <span class="input-group-addon"><span class="fas fa-calendar-alt"></span></span>
                                    </div>
                                </div>

                                <div class='col-sm-3  <?php if ($_smarty_tpl->tpl_vars['assign_type']->value == 'month') {?>x-hidden<?php }?>' id="div_assign_date">
                                    <div class='input-group date' id='pickup_date_picker'>
                                        <input type='text' name="date" id="date" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['monday']->value;?>
"
                                               placeholder="<?php echo __("Date");?>
 (*)" required/>
                                        <span class="input-group-addon"><span class="fas fa-calendar-alt"></span></span>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <br/>


                    <div style="z-index: 2">
                        <div><strong><?php echo __("School assign all table");?>
</strong></div>
                    </div>


                    <div id="table_assign">
                        <?php $_smarty_tpl->_subTemplateRender("file:ci/school/ajax.school.pickup.assign.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                    </div>

                    <div class="form-group mt10 <?php if ($_smarty_tpl->tpl_vars['assign_type']->value == 'month') {?>x-hidden<?php }?>" id="div_repeat">
                        <div class="col-sm-9">
                            <div class="onoffswitch">
                                <input type="checkbox" name="is_repeat" class="onoffswitch-checkbox" id="is_repeat"
                                       <?php if ($_smarty_tpl->tpl_vars['is_repeat']->value == 1) {?>checked<?php }?>>
                                <label class="onoffswitch-label" for="is_repeat"></label>
                            </div>
                            <span class="help-block">
                            <?php echo __("Repeating schedule assignment by week?");?>

                        </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-9">
                            <button type="submit"
                                    class="btn btn-primary padrl30"><?php echo __("Save");?>
</button>
                        </div>
                    </div>

                </form>

            </div>
        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "list") {?>
            <div class="panel-body with-table">
                <div class="row">
                    <div class='col-sm-12'>
                        <div class='col-sm-2'></div>
                        <div class='col-sm-3'>
                            <div class='input-group date' id='fromdatepicker'>
                                <input type='text' name="fromDate" id="fromDate" value="<?php echo $_smarty_tpl->tpl_vars['fromDate']->value;?>
"
                                       class="form-control" placeholder="<?php echo __("From date");?>
"/>
                                <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                            </div>
                        </div>
                        <div class='col-sm-3'>
                            <div class='input-group date' id='todatepicker'>
                                <input type='text' name="toDate" id="toDate" value="<?php echo $_smarty_tpl->tpl_vars['toDate']->value;?>
" class="form-control"
                                       placeholder="<?php echo __("To date");?>
"/>
                                <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                            </div>
                        </div>
                        <div class='col-sm-3'>
                            <div class="form-group">
                                <button class="btn btn-default js_pickup-search" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"
                                        data-id="<?php echo $_smarty_tpl->tpl_vars['school']->value['page_id'];?>
"><?php echo __("Search");?>
</button>
                            </div>
                        </div>
                    </div>
                </div>
                <br/>

                <div class="table-responsive" id="pickup_list">
                    <?php $_smarty_tpl->_subTemplateRender("file:ci/school/ajax.school.pickuplist.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                </div>
            </div>
        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "child") {?>
            <div class="panel-body">
                <div class="js_ajax-forms form-horizontal">
                    <input type="hidden" name="school_username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                    <input type="hidden" name="do" value="save_child_rollup"/>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-right"><?php echo __("Select student");?>
 (*)</label>
                        <div class='col-sm-3'>
                            <select name="class_id" id="attendance_class_id" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"
                                    class="form-control" autofocus>
                                <option value=""><?php echo __("Select class");?>
...</option>
                                <?php $_smarty_tpl->_assignInScope('class_level', -1);
?>
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['data']->value['classes'], 'class');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class']->value) {
?>
                                    <?php if (($_smarty_tpl->tpl_vars['class_level']->value != $_smarty_tpl->tpl_vars['class']->value['class_level_id'])) {?>
                                        <option value="" disabled>-----<?php echo $_smarty_tpl->tpl_vars['class']->value['class_level_name'];?>
-----</option>
                                    <?php }?>
                                    <option value="<?php echo $_smarty_tpl->tpl_vars['class']->value['group_id'];?>
"
                                            <?php if ($_smarty_tpl->tpl_vars['data']->value['child']['class_id'] == $_smarty_tpl->tpl_vars['class']->value['group_id']) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['class']->value['group_title'];?>
</option>
                                    <?php $_smarty_tpl->_assignInScope('class_level', $_smarty_tpl->tpl_vars['class']->value['class_level_id']);
?>
                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>


                            </select>
                        </div>
                        <div class="col-sm-4">
                            <select name="child_id" id="attendance_child_id" class="form-control" required>
                                <option value=""><?php echo __("Select student");?>
...</option>
                                <?php if ($_smarty_tpl->tpl_vars['data']->value['child'] != '') {?>
                                    <option value="<?php echo $_smarty_tpl->tpl_vars['data']->value['child']['child_id'];?>
"
                                            selected><?php echo $_smarty_tpl->tpl_vars['data']->value['child']['child_name'];?>
</option>
                                <?php }?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-right"><?php echo __("Time");?>
 (*)</label>
                        <div class='col-sm-3'>
                            <div class='input-group date' id='fromdatepicker'>
                                <input type='text' name="fromDate" id="fromDate" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['fromDate'];?>
"
                                       class="form-control" placeholder="<?php echo __("From date");?>
"/>
                                <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                            </div>
                        </div>

                        <div class='col-md-3'>
                            <div class='input-group date' id='todatepicker'>
                                <input type='text' name="toDate" id="toDate" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['toDate'];?>
"
                                       class="form-control" placeholder="<?php echo __("To date");?>
"/>
                                <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <a href="#" id="search" class="btn btn-default js_school-pickup_child"
                               data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"
                               <?php if (!($_smarty_tpl->tpl_vars['data']->value['child'] != '')) {?>disabled="true"<?php }?>><?php echo __("Search");?>
</a>
                            <label id="loading" class="btn btn-info x-hidden"><?php echo __("Loading");?>
...</label>
                        </div>
                    </div>
                    

                    <div class="table-responsive" id="pickup_list_child">
                        <?php $_smarty_tpl->_subTemplateRender("file:ci/school/ajax.child.pickup.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                    </div>

                </div>
            </div>
        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
            <div class="panel-body with-table">
                <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_pickup.php">
                    <input type="hidden" name="school_username" id="school_username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                    
                    <input type="hidden" name="do" value="add_child"/>

                    <div class="form-group">
                        <label class="col-sm-2"></label>
                        <label class="col-sm-4 control-label text-left"><?php echo mb_strtoupper(__("Add child into late pickup class on"), 'UTF-8');?>

                            (*)</label>
                        <div class="col-sm-3">
                            <div class='input-group date' id='pickup_datepicker'>
                                <input type='text' name="date" id="add_to_date" value="<?php echo $_smarty_tpl->tpl_vars['today']->value;?>
" class="form-control"
                                       placeholder="<?php echo __("Time");?>
 (*)" required/>
                                <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                            </div>
                        </div>
                    </div>

                    <?php $_smarty_tpl->_assignInScope('count', count($_smarty_tpl->tpl_vars['pickup_class']->value));
?>
                    <?php if ($_smarty_tpl->tpl_vars['count']->value == 1) {?>
                        <div class="form-group">
                            <label class="col-sm-2"></label>
                            <label class="col-sm-3 control-label text-left"><?php echo __("Add to");?>
 (*)</label>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['pickup_class']->value[0]['class_name'];?>
"
                                       disabled/>
                                <input type="radio" class="pickup_class_id x-hidden" name="pickup_class_id"
                                       value="<?php echo $_smarty_tpl->tpl_vars['pickup_class']->value[0]['pickup_class_id'];?>
" checked/>
                                
                            </div>
                        </div>
                    <?php } elseif ($_smarty_tpl->tpl_vars['count']->value > 1) {?>
                        <div class="form-group">
                            <label class="col-sm-2"></label>
                            <label class="col-sm-3 control-label text-left"><?php echo __("Add to");?>
 (*)</label>
                            <div class="col-sm-6">
                                <?php $_smarty_tpl->_assignInScope('i', 1);
?>
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['pickup_class']->value, 'class');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class']->value) {
?>
                                    <input type="radio" class="pickup_class_id" name="pickup_class_id"
                                           value="<?php echo $_smarty_tpl->tpl_vars['class']->value['pickup_class_id'];?>
"
                                           data-name="<?php echo $_smarty_tpl->tpl_vars['class']->value['class_name'];?>
" <?php if ($_smarty_tpl->tpl_vars['i']->value == 1) {?> checked<?php }?>/>
                                    <?php echo $_smarty_tpl->tpl_vars['class']->value['class_name'];?>

                                    <br>
                                    <?php $_smarty_tpl->_assignInScope('i', -1);
?>
                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                            </div>
                        </div>
                    <?php } else { ?>
                        <input type="radio" class="pickup_class_id x-hidden" name="pickup_class_id" value="0" checked/>
                        
                    <?php }?>

                    <div class="form-group">
                        <label class="col-sm-2"></label>
                        <label class="col-sm-3 control-label text-left"><?php echo __("Class");?>
 (*)</label>
                        <div class="col-sm-3">
                            <select name="class_id" id="pickup_class" class="form-control" required>
                                <?php $_smarty_tpl->_assignInScope('class_level', -1);
?>
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['classes']->value, 'class');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class']->value) {
?>
                                    <?php if (($_smarty_tpl->tpl_vars['class_level']->value != $_smarty_tpl->tpl_vars['class']->value['class_level_id'])) {?>
                                        <option value="" disabled style="color: blue">-----<?php echo $_smarty_tpl->tpl_vars['class']->value['class_level_name'];?>

                                            -----
                                        </option>
                                    <?php }?>
                                    <option value="<?php echo $_smarty_tpl->tpl_vars['class']->value['group_id'];?>
"
                                            <?php if ($_smarty_tpl->tpl_vars['class_id']->value == $_smarty_tpl->tpl_vars['class']->value['group_id']) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['class']->value['group_title'];?>
</option>
                                    <?php $_smarty_tpl->_assignInScope('class_level', $_smarty_tpl->tpl_vars['class']->value['class_level_id']);
?>
                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                            </select>
                        </div>
                    </div>


                    <div class="table-responsive" id="pickup_school_list">
                        <?php $_smarty_tpl->_subTemplateRender("file:ci/class/ajax.pickupchild.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                    </div>

                    <div class="form-group pl5">
                        <div class="col-sm-9">
                            <button type="submit"
                                    class="btn btn-primary padrl30" <?php if ($_smarty_tpl->tpl_vars['disableSave']->value) {?> disabled<?php }?>><?php echo __("Save");?>
</button>
                        </div>
                    </div>

                    <!-- success -->
                    <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                    <!-- success -->

                    <!-- error -->
                    <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                    <!-- error -->
                </form>
            </div>
        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "detail") {?>
            <div class="panel-body with-table">
                <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_pickup.php">
                    <input type="hidden" name="school_username" id="school_username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                    <input type="hidden" name="pickup_date" id="pickup_date" value="<?php echo $_smarty_tpl->tpl_vars['today']->value;?>
"/>
                    <input type="hidden" id="pickup_id" name="pickup_id" value="<?php echo $_smarty_tpl->tpl_vars['pickup']->value['pickup_id'];?>
"/>
                    <input type="hidden" name="do" value="save"/>
                    <div id="time_option" class="x-hidden" data-value="<?php echo $_smarty_tpl->tpl_vars['time_option']->value;?>
"></div>

                    <div class="text-center">
                        <strong><label style="font-size: 15px"><?php echo __("Late pickup on");?>
: <?php echo $_smarty_tpl->tpl_vars['today']->value;?>
</label></strong><br>
                        <?php if (!is_empty($_smarty_tpl->tpl_vars['pickup']->value['class_name'])) {?>
                            <strong><label style="font-size: 15px"><?php echo __("Class");?>

                                    : <?php echo $_smarty_tpl->tpl_vars['pickup']->value['class_name'];?>
</label></strong>
                            <br>
                            <br>
                        <?php } else { ?>
                            <br>
                        <?php }?>
                    </div>
                    <div class="col-sm-12">
                        <label class="col-sm-6 text-right" style="font-size: 15px"><?php echo __("Late pickup teachers");?>
:</label>
                        <?php if (count($_smarty_tpl->tpl_vars['pickup']->value['assign']) > 0) {?>
                            <div class="col-sm-6">
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['pickup']->value['assign'], 'assign');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['assign']->value) {
?>
                                    <strong>
                                        <label>&#9679;&#09;<?php echo $_smarty_tpl->tpl_vars['assign']->value['user_fullname'];?>
</label>
                                    </strong>
                                    <br>
                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                            </div>
                        <?php } else { ?>
                            <div class="col-sm-6 text-left"
                                 style="font-size: 15px;color: red"><?php echo __("Not assigned");?>
</div>
                        <?php }?>
                    </div>


                    <div>
                        <strong><?php echo __("Total student");?>
: <font color="red"><?php echo count($_smarty_tpl->tpl_vars['children']->value);?>
</font> |
                            <?php echo __("Picked up");?>
: <font color="red"><?php echo $_smarty_tpl->tpl_vars['child_count']->value;?>
</font>
                        </strong>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th width="5%">#</th>
                                <th width="25%"><?php echo __("Student name");?>
</th>
                                <th width="20%"><?php echo __("Birthdate");?>
</th>
                                <th width="20%"><?php echo __("Class");?>
</th>
                                <th width="15%"><?php echo __("Status");?>
</th>
                                <th width="15%"><?php echo __("Total amount");?>
</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['children']->value, 'child');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['child']->value) {
?>
                                <input type="hidden" name="child[]" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
"/>
                                <tr <?php if ($_smarty_tpl->tpl_vars['child']->value['child_status'] == 0) {?> class="row-disable" <?php }?>>
                                    <td align="center"><strong><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</strong></td>
                                    <td><strong><?php echo $_smarty_tpl->tpl_vars['child']->value['child_name'];?>
</strong></td>
                                    <td align="center"><?php echo $_smarty_tpl->tpl_vars['child']->value['birthday'];?>
</td>
                                    <td align="center"><?php echo $_smarty_tpl->tpl_vars['child']->value['group_title'];?>
</td>
                                    <td rowspan="4" align="center">
                                        <?php if ($_smarty_tpl->tpl_vars['canEdit']->value) {?>
                                            <?php if (is_empty($_smarty_tpl->tpl_vars['child']->value['pickup_at'])) {?>
                                                <?php if ($_smarty_tpl->tpl_vars['is_today']->value) {?>
                                                    <a class="btn btn-xs btn-default js_school-pickup-one-child"
                                                       data-handle="pickup"
                                                       data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-child="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
"
                                                       data-pickup="<?php echo $_smarty_tpl->tpl_vars['child']->value['pickup_id'];?>
">
                                                        <?php echo __("Pickup");?>

                                                    </a>
                                                <?php }?>
                                            <?php }?>
                                            <a class="btn btn-xs btn-danger js_school-pickup" data-handle="cancel"
                                               data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-child="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
"
                                               data-pickup="<?php echo $_smarty_tpl->tpl_vars['child']->value['pickup_id'];?>
"><?php echo __("Cancel");?>

                                            </a>
                                        <?php }?>
                                    </td>
                                    <td rowspan="1"></td>
                                </tr>
                                <tr <?php if ($_smarty_tpl->tpl_vars['child']->value['child_status'] == 0) {?> class="row-disable" <?php }?>>
                                    <td rowspan="3"></td>
                                    <td class="align-middle text-left" colspan="3" style="padding-left: 35px">
                                        <div class="col-sm-4 pt3">
                                            <?php echo __("Pickup at");?>

                                            : &nbsp;
                                        </div>

                                        <?php if (!is_empty($_smarty_tpl->tpl_vars['child']->value['pickup_at'])) {?>
                                            <input type='text' name="pickup_time[]" class="pickup_time"
                                                   id="pickup_time_<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['pickup_at'];?>
"
                                                   style="width: 70px; color:blue; text-align: center"/>
                                            
                                        <?php } else { ?>
                                            <input type='text' name="pickup_time[]" class="pickup_time"
                                                   id="pickup_time_<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
" value=""
                                                   style="width: 70px; color: red; text-align: center"/>
                                            &nbsp;
                                        <?php }?>
                                    </td>
                                    <td colspan="1" align="right">
                                        <input type="text" class="text-right money_tui"
                                               id="total_pickup_<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
"
                                               data-child="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
"
                                               name="pickup_fee[]" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['late_pickup_fee'];?>
"
                                               maxlength="10" style="width: 100px; font-weight: bold"/>
                                        
                                    </td>
                                </tr>
                                <tr <?php if ($_smarty_tpl->tpl_vars['child']->value['child_status'] == 0) {?> class="row-disable" <?php }?>>
                                    <td class="align-middle text-left" colspan="3" style="padding-left: 35px">
                                        <?php if (!empty($_smarty_tpl->tpl_vars['child']->value['services'])) {?>
                                            <div class="col-sm-4 pt3">
                                                <?php echo __("Using service");?>
: &nbsp;
                                            </div>
                                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['child']->value['services'], 'service');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['service']->value) {
?>
                                                <input type="checkbox" class="pickup_service_fee"
                                                       name="service_<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
[]"
                                                       id="pickup_service_fee_<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
_<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
"
                                                       data-child="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
"
                                                       data-service="<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
"
                                                       data-fee="<?php echo $_smarty_tpl->tpl_vars['service']->value['price'];?>
"
                                                       value="<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
" <?php if (!is_null($_smarty_tpl->tpl_vars['service']->value['using_at'])) {?> checked <?php }?>
                                                />
                                                <?php echo $_smarty_tpl->tpl_vars['service']->value['service_name'];?>

                                                
                                                &nbsp;&nbsp;&nbsp;
                                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                        <?php } else { ?>
                                            &nbsp;
                                        <?php }?>
                                    </td>
                                    <td colspan="1" align="right">
                                        <input type="text" class="text-right money_tui"
                                               name="service_fee[]"
                                               id="total_service_<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
"
                                               data-child="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
"
                                               value="<?php echo $_smarty_tpl->tpl_vars['child']->value['using_service_fee'];?>
"
                                               maxlength="10" style="width: 100px; font-weight: bold"/>
                                        
                                    </td>
                                </tr>
                                <tr <?php if ($_smarty_tpl->tpl_vars['child']->value['child_status'] == 0) {?> class="row-disable" <?php }?>>
                                    <td class="align-middle text-left" colspan="3" style="padding-left: 35px">
                                        
                                        <div class="col-sm-4 pt3">
                                            <?php echo __("Note");?>
 : &nbsp;
                                        </div>
                                        <input type='text' name="pickup_note[]" id="pickup_note_<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['description'];?>
"
                                               style="width: 60%; color: blue"/>

                                    </td>
                                    <td colspan="1" style="vertical-align: middle">
                                        <input type="text" class="text-right money_tui"
                                               id="total_<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
"
                                               name="total_child[]"
                                               value="<?php echo $_smarty_tpl->tpl_vars['child']->value['total_amount'];?>
"
                                               maxlength="10"
                                               style="width: 100px; font-weight: bold; color: blue; border: none; padding-right: 2px"
                                               readonly/>
                                        
                                    </td>
                                </tr>
                                <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                            <tr>
                                <td colspan="6"></td>
                            </tr>
                            <tr>
                                <td colspan="5" align="right"><strong><?php echo __("Total");?>
</strong></td>
                                <td colspan="1" align="right">
                                    
                                    <input type="text" class="text-right money_tui" id="total" name="total"
                                           value="<?php echo $_smarty_tpl->tpl_vars['pickup']->value['total'];?>
"
                                           style="width: 100px; font-weight: bold; color: red; border: none; padding-right: 2px"
                                           readonly/>
                                    
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <?php if ($_smarty_tpl->tpl_vars['canEdit']->value) {?>
                        <div class="form-group">
                            <div class="col-sm-9 pl10">
                                <button type="submit"
                                        class="btn btn-primary padrl30 <?php if (count($_smarty_tpl->tpl_vars['children']->value) == 0) {?> x-hidden <?php }?>"><?php echo __("Save");?>
</button>
                            </div>
                        </div>
                        <!-- success -->
                        <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                        <!-- success -->
                        <!-- error -->
                        <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                        <!-- error -->
                    <?php }?>
                </form>
            </div>
        <?php }?>
    </div>
</div><?php }
}
