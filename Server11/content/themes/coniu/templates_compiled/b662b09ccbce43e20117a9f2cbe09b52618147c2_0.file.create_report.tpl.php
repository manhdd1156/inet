<?php
/* Smarty version 3.1.31, created on 2021-03-31 10:19:40
  from "D:\workplace\Server11\content\themes\coniu\templates\ci\email_templates\create_report.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_6063ea4ca9e547_44204010',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b662b09ccbce43e20117a9f2cbe09b52618147c2' => 
    array (
      0 => 'D:\\workplace\\Server11\\content\\themes\\coniu\\templates\\ci\\email_templates\\create_report.tpl',
      1 => 1552404704,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6063ea4ca9e547_44204010 (Smarty_Internal_Template $_smarty_tpl) {
?>
Kính gửi ông/bà, <br/>
Phụ huynh bé <?php echo $_smarty_tpl->tpl_vars['child_name']->value;?>
<br/><br/>

<b><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</b><br/><br/>

<?php echo $_smarty_tpl->tpl_vars['content']->value;?>


Thân ái,<br/>
<?php echo $_smarty_tpl->tpl_vars['class_name']->value;?>
.

<?php }
}
