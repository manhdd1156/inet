<?php
/* Smarty version 3.1.31, created on 2021-03-31 10:47:42
  from "D:\workplace\Server11\content\themes\coniu\templates\ci\child\ajax.child.attendance.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_6063f0de411770_63865893',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '356006cf0fd1e59c140a6f9593e036f9ea0832ed' => 
    array (
      0 => 'D:\\workplace\\Server11\\content\\themes\\coniu\\templates\\ci\\child\\ajax.child.attendance.tpl',
      1 => 1573458087,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6063f0de411770_63865893 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div><strong><?php echo __("Attendance");?>
&nbsp;(<?php echo __("Total");?>
: <?php echo count($_smarty_tpl->tpl_vars['data']->value['attendance']['attendance']);?>
&nbsp;|&nbsp;<?php echo __("Present");?>
: <?php echo $_smarty_tpl->tpl_vars['data']->value['attendance']['present_count'];?>
&nbsp;|&nbsp;<?php echo __("Absence");?>
: <?php echo $_smarty_tpl->tpl_vars['data']->value['attendance']['absence_count'];?>
)</strong></div>
<table class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th>#</th>
        <th><?php echo __("Attendance date");?>
</th>
        <th><?php echo __("Status");?>
</th>
        <th><?php echo __("Teacher");?>
</th>
    </tr>
    </thead>
    <tbody>
    <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['data']->value['attendance']['attendance'], 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
        <tr>
            <td align="center"><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</td>
            <td align="center"><?php echo $_smarty_tpl->tpl_vars['row']->value['attendance_date'];?>
</td>
            <td align="center">
                <?php if ($_smarty_tpl->tpl_vars['row']->value['status'] == @constant('ATTENDANCE_PRESENT')) {?>
                    <?php echo __("Present");?>

                <?php } elseif ($_smarty_tpl->tpl_vars['row']->value['status'] == @constant('ATTENDANCE_COME_LATE')) {?>
                    <?php echo __("Come late");?>

                <?php } elseif ($_smarty_tpl->tpl_vars['row']->value['status'] == @constant('ATTENDANCE_EARLY_LEAVE')) {?>
                    <?php echo __("Leave early");?>

                <?php } elseif ($_smarty_tpl->tpl_vars['row']->value['status'] == @constant('ATTENDANCE_ABSENCE_NO_REASON')) {?>
                    <?php echo __("Without permission");?>

                <?php } else { ?>
                    <strong><?php echo __("With permission");?>
</strong>&nbsp;(
                    <?php if ((!isset($_smarty_tpl->tpl_vars['row']->value['reason']) || ($_smarty_tpl->tpl_vars['row']->value['reason'] == ''))) {?>
                        <?php echo __("No reason");?>

                    <?php } else { ?>
                        <?php echo $_smarty_tpl->tpl_vars['row']->value['reason'];?>

                    <?php }?>)
                <?php }?>
            </td>
            <td><?php echo $_smarty_tpl->tpl_vars['row']->value['user_fullname'];?>
</td>
        </tr>
        <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

    </tbody>
</table><?php }
}
