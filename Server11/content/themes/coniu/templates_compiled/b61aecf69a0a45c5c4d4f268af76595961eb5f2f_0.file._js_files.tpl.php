<?php
/* Smarty version 3.1.31, created on 2021-04-23 11:22:07
  from "D:\workplace\mascom-edu-server\Server11\content\themes\coniu\templates\_js_files.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_60824b6f6bf6c2_32093766',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b61aecf69a0a45c5c4d4f268af76595961eb5f2f' => 
    array (
      0 => 'D:\\workplace\\mascom-edu-server\\Server11\\content\\themes\\coniu\\templates\\_js_files.tpl',
      1 => 1575865658,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60824b6f6bf6c2_32093766 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!-- HTML5 shim, for IE6-8 support of HTML5 elements --><!--[if lt IE 9]><?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['system']->value['cdn_url'];?>
/includes/assets/js/plugins/html5shiv/html5shiv.js"><?php echo '</script'; ?>
><![endif]--><!-- Initialize --><?php echo '<script'; ?>
 type="text/javascript">/* initialize vars */var site_title = "<?php echo $_smarty_tpl->tpl_vars['system']->value['system_title'];?>
";var site_path = "<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
";var ajax_path = site_path+'/includes/ajax/';var ajax_api_path = site_path+'/api/';var uploads_path = "<?php echo $_smarty_tpl->tpl_vars['system']->value['system_uploads'];?>
";var current_page = "<?php echo $_smarty_tpl->tpl_vars['page']->value;?>
";var avatar_default_path = site_path + '/content/themes/coniu/images/blank_profile_male.jpg';var avatar_group_default_path = site_path + '/content/themes/coniu/images/blank_group_picture.png';<?php if (isset($_smarty_tpl->tpl_vars['user']->value)) {?>var data_user_id = "_<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_id'];?>
";var data_user_name = "<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_name'];?>
";var data_user_fullname = "<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_fullname'];?>
";var data_user_picture = "<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_picture'];?>
";<?php }?>var secret = '<?php echo $_smarty_tpl->tpl_vars['secret']->value;?>
';var min_data_heartbeat = "<?php echo $_smarty_tpl->tpl_vars['system']->value['data_heartbeat']*1000;?>
";var min_chat_heartbeat = "<?php echo $_smarty_tpl->tpl_vars['system']->value['chat_heartbeat']*1000;?>
";var chat_enabled = <?php if ($_smarty_tpl->tpl_vars['system']->value['chat_enabled']) {?>true<?php } else { ?>false<?php }?>;var geolocation_enabled = <?php if ($_smarty_tpl->tpl_vars['system']->value['geolocation_enabled']) {?>true<?php } else { ?>false<?php }?>;/* ConIu - BEGIN */var DATE_FORMAT = "DD/MM/YYYY";var DATETIME_FORMAT = "HH:mm DD/MM/YYYY";var TIME_FORMAT = "HH:mm";/* ConIu - END */<?php echo '</script'; ?>
><?php echo '<script'; ?>
 type="text/javascript">/* i18n for JS */var __ = [];__["Describe your item (optional)"] = "<?php echo __('Describe your item (optional)');?>
";__["Ask something"] = "<?php echo __('Ask something');?>
";__["Verification Requset"] = "<?php echo __('Verification Requset');?>
";__["Add Friend"] = "<?php echo __('Add Friend');?>
";__["Friends"] = "<?php echo __('Friends');?>
";__["Friend Request Sent"] = "<?php echo __('Friend Request Sent');?>
";__["Following"] = "<?php echo __('Following');?>
";__["Follow"] = "<?php echo __('Follow');?>
";__["Pending"] = "<?php echo __('Pending');?>
";__["Remove"] = "<?php echo __('Remove');?>
";__["Error"] = "<?php echo __('Error');?>
";__["Success"] = "<?php echo __('Success');?>
";__["Loading"] = "<?php echo __('Loading');?>
";__["Like"] = "<?php echo __('Like');?>
";__["Unlike"] = "<?php echo __('Unlike');?>
";__["Joined"] = "<?php echo __('Joined');?>
";__["Join"] = "<?php echo __('Join');?>
";__["Remove Admin"] = "<?php echo __('Remove Admin');?>
";__["Make Admin"] = "<?php echo __('Make Admin');?>
";__["Going"] = "<?php echo __('Going');?>
";__["Interested"] = "<?php echo __('Interested');?>
";__["Delete"] = "<?php echo __('Delete');?>
";__["Delete Cover"] = "<?php echo __('Delete Cover');?>
";__["Delete Picture"] = "<?php echo __('Delete Picture');?>
";__["Delete Post"] = "<?php echo __('Delete Post');?>
";__["Delete Comment"] = "<?php echo __('Delete Comment');?>
";__["Delete Conversation"] = "<?php echo __('Delete Conversation');?>
";__["Share Post"] = "<?php echo __('Share Post');?>
";__["Report"] = "<?php echo __('Report');?>
";__["Block User"] = "<?php echo __('Block User');?>
";__["Unblock User"] = "<?php echo __('Unblock User');?>
";__["Mark as Available"] = "<?php echo __('Mark as Available');?>
";__["Mark as Sold"] = "<?php echo __('Mark as Sold');?>
";__["Save Post"] = "<?php echo __('Save Post');?>
";__["Unsave Post"] = "<?php echo __('Unsave Post');?>
";__["Boost Post"] = "<?php echo __('Boost Post');?>
";__["Unboost Post"] = "<?php echo __('Unboost Post');?>
";__["Pin Post"] = "<?php echo __('Pin Post');?>
";__["Unpin Post"] = "<?php echo __('Unpin Post');?>
";__["Verify"] = "<?php echo __('Verify');?>
";__["Decline"] = "<?php echo __('Decline');?>
";__["Boost"] = "<?php echo __('Boost');?>
";__["Unboost"] = "<?php echo __('Unboost');?>
";__["Mark as Paid"] = "<?php echo __('Mark as Paid');?>
";__["Read more"] = "<?php echo __('Read more');?>
";__["Read less"] = "<?php echo __('Read less');?>
";__['Turn On Chat'] = "<?php echo __('Turn On Chat');?>
";__['Turn Off Chat'] = "<?php echo __('Turn Off Chat');?>
";__["Monthly Average"] = "<?php echo __('Monthly Average');?>
";__["Jan"] = "<?php echo __('Jan');?>
";__["Feb"] = "<?php echo __('Feb');?>
";__["Mar"] = "<?php echo __('Mar');?>
";__["Apr"] = "<?php echo __('Apr');?>
";__["May"] = "<?php echo __('May');?>
";__["Jun"] = "<?php echo __('Jun');?>
";__["Jul"] = "<?php echo __('Jul');?>
";__["Aug"] = "<?php echo __('Aug');?>
";__["Sep"] = "<?php echo __('Sep');?>
";__["Oct"] = "<?php echo __('Oct');?>
";__["Nov"] = "<?php echo __('Nov');?>
";__["Dec"] = "<?php echo __('Dec');?>
";__["Users"] = "<?php echo __('Users');?>
";__["Pages"] = "<?php echo __('Pages');?>
";__["Groups"] = "<?php echo __('Groups');?>
";__["Events"] = "<?php echo __('Events');?>
";__["Posts"] = "<?php echo __('Posts');?>
";__["Translated"] = "<?php echo __('Translated');?>
";__["Are you sure you want to delete this?"] = "<?php echo __('Are you sure you want to delete this?');?>
";__["Are you sure you want to remove your cover photo?"] = "<?php echo __('Are you sure you want to remove your cover photo?');?>
";__["Are you sure you want to remove your profile picture?"] = "<?php echo __('Are you sure you want to remove your profile picture?');?>
";__["Are you sure you want to delete this post?"] = "<?php echo __('Are you sure you want to delete this post?');?>
";__["Are you sure you want to share this post?"] = "<?php echo __('Are you sure you want to share this post?');?>
";__["Are you sure you want to delete this comment?"] = "<?php echo __('Are you sure you want to delete this comment?');?>
";__["Are you sure you want to delete this conversation?"] = "<?php echo __('Are you sure you want to delete this conversation?');?>
";__["Are you sure you want to report this?"] = "<?php echo __('Are you sure you want to report this?');?>
";__["Are you sure you want to block this user?"] = "<?php echo __('Are you sure you want to block this user?');?>
";__["Are you sure you want to unblock this user?"] = "<?php echo __('Are you sure you want to unblock this user?');?>
";__["Are you sure you want to delete your account?"] = "<?php echo __('Are you sure you want to delete your account?');?>
";__["Are you sure you want to verify this request?"] = "<?php echo __('Are you sure you want to verify this request?');?>
";__["Are you sure you want to decline this request?"] = "<?php echo __('Are you sure you want to decline this request?');?>
";__["Are you sure you want to approve this request?"] = "<?php echo __('Are you sure you want to approve this request?');?>
";__["Are you sure you want to do this?"] = "<?php echo __('Are you sure you want to do this?');?>
";__["There is something that went wrong!"] = "<?php echo __('There is something that went wrong!');?>
";__["There is no more data to show"] = "<?php echo __('There is no more data to show');?>
";__["This has been shared to your Timeline"] = "<?php echo __('This has been shared to your Timeline');?>
";/* ConIu - Begin */__['Close'] = '<?php echo __("Close");?>
';__["Cancel"] = '<?php echo __("Cancel");?>
';__["Actions"] = '<?php echo __("Actions");?>
';__["Are you sure you want to cancel this?"] = '<?php echo __("Are you sure you want to cancel this?");?>
';__["Count illness"] = '<?php echo __("Count illness");?>
';__["Are you sure you want to do this?"] = '<?php echo __("Are you sure you want to do this?");?>
';__["Add new row"] = '<?php echo __("Add new row");?>
';__["Content"] = '<?php echo __("Content");?>
';__["Directory"] = '<?php echo __("Directory");?>
';__["Activity"] = '<?php echo __("Activity");?>
';__["No"] = '<?php echo __("No");?>
';__["Yes"] = '<?php echo __("Yes");?>
';__["Count illness"] = '<?php echo __("Count illness");?>
';__["Pregnancy check"] = '<?php echo __("Pregnancy check");?>
';__["Vaccination"] = '<?php echo __("Vaccination");?>
';__["Information"] = '<?php echo __("Information");?>
';__["It is impossible to remove when student is under the management of the school"] = '<?php echo __("It is impossible to remove when student is under the management of the school");?>
';__["Height development chart"] = '<?php echo __("Height development chart");?>
';__["Weight development chart"] = '<?php echo __("Weight development chart");?>
';__["BMI development chart"] = '<?php echo __("BMI development chart");?>
';__["Height"] = '<?php echo __("Height");?>
';__["Weight"] = '<?php echo __("Weight");?>
';__["Above standard"] = '<?php echo __("Above standard");?>
';__["Below standard"] = '<?php echo __("Below standard");?>
';__["Standard"] = '<?php echo __("Standard");?>
';__["It looks like this post has no content yet. Please write something or attach a link or image to post"] = '<?php echo __("It looks like this post has no content yet. Please write something or attach a link or image to post");?>
';__["Please enter a valid mobile number"] = '<?php echo __("Please enter a valid phone number");?>
';__["Please enter a valid phone number"] = '<?php echo __("Please enter a valid phone number");?>
';__["Please select a valid birth month"] = '<?php echo __("Please select a valid birth month");?>
';__["Please select a valid birth day"] = '<?php echo __("Please select a valid birth day");?>
';__["Please select a valid birth year"] = '<?php echo __("Please select a valid birth year");?>
';__["Search"] = '<?php echo __("Search");?>
';__["Show"] = '<?php echo __("Show");?>
';__["results"] = '<?php echo __("results");?>
';__["Loading..."] = '<?php echo __("Loading...");?>
';__["Processing..."] = '<?php echo __("Processing...");?>
';__["No matching records found"] = '<?php echo __("No matching records found");?>
';__["No data available in table"] = '<?php echo __("No data available in table");?>
';__["Showing _START_ to _END_ of _TOTAL_ results"] = '<?php echo __("Showing _START_ to _END_ of _TOTAL_ results");?>
';__["Showing 0 to 0 of 0 results"] = '<?php echo __("Showing 0 to 0 of 0 results");?>
';__["First"] = '<?php echo __("First");?>
';__["Last"] = '<?php echo __("Last");?>
';__["Next"] = '<?php echo __("Next");?>
';__["Previous"] = '<?php echo __("Previous");?>
';/* ConIu - END */<?php echo '</script'; ?>
><!-- Initialize --><!-- Dependencies Libs [jQuery|Bootstrap|Mustache] --><?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['system']->value['cdn_url'];?>
/includes/assets/js/jquery/jquery-1.12.2.min.js"><?php echo '</script'; ?>
><?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['system']->value['cdn_url'];?>
/includes/assets/js/bootstrap/bootstrap.min.js"><?php echo '</script'; ?>
><?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['system']->value['cdn_url'];?>
/includes/assets/js/mustache/mustache.min.js"><?php echo '</script'; ?>
><!-- Dependencies Libs [jQuery|Bootstrap|Mustache] --><!-- Dependencies Plugins [JS] [fastclick|slimscroll|autogrow|moment|form|inview|videojs|mediaelementplayer] --><?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['system']->value['cdn_url'];?>
/includes/assets/js/plugins/fastclick/fastclick.min.js"><?php echo '</script'; ?>
><?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['system']->value['cdn_url'];?>
/includes/assets/js/plugins/slimscroll/slimscroll.js"><?php echo '</script'; ?>
><?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['system']->value['cdn_url'];?>
/includes/assets/js/plugins/autosize/autosize.min.js" <?php if (!$_smarty_tpl->tpl_vars['user']->value->_logged_in) {?>defer<?php }?>><?php echo '</script'; ?>
><?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['system']->value['cdn_url'];?>
/includes/assets/js/plugins/readmore/readmore.min.js" <?php if (!$_smarty_tpl->tpl_vars['user']->value->_logged_in) {?>defer<?php }?>><?php echo '</script'; ?>
><?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['system']->value['cdn_url'];?>
/includes/assets/js/plugins/autogrow/autogrow.min.js"><?php echo '</script'; ?>
><?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['system']->value['cdn_url'];?>
/includes/assets/js/plugins/moment/moment-with-locales.min.js"><?php echo '</script'; ?>
><?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['system']->value['cdn_url'];?>
/includes/assets/js/plugins/inview/inview.min.js"><?php echo '</script'; ?>
><?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['system']->value['cdn_url'];?>
/includes/assets/js/plugins/form/form.min.js"><?php echo '</script'; ?>
><?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['system']->value['cdn_url'];?>
/includes/assets/js/plugins/videojs/video.min.js"><?php echo '</script'; ?>
><?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['system']->value['cdn_url'];?>
/includes/assets/js/plugins/mediaelementplayer/mediaelement-and-player.min.js"><?php echo '</script'; ?>
><?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['system']->value['cdn_url'];?>
/includes/assets/js/plugins/datetimepicker/datetimepicker.min.js"><?php echo '</script'; ?>
><!-- Dependencies Plugins [JS] [fastclick|slimscroll|autogrow|moment|form|inview|videojs|mediaelementplayer] --><!-- Dependencies Plugins [CSS] [videojs|mediaelementplayer] --><link rel="stylesheet" type='text/css' href="<?php echo $_smarty_tpl->tpl_vars['system']->value['cdn_url'];?>
/includes/assets/js/plugins/videojs/video-js.min.css"><link rel="stylesheet" type='text/css' href="<?php echo $_smarty_tpl->tpl_vars['system']->value['cdn_url'];?>
/includes/assets/js/plugins/mediaelementplayer/mediaelementplayer.min.css"><link rel="stylesheet" type='text/css' href="<?php echo $_smarty_tpl->tpl_vars['system']->value['cdn_url'];?>
/includes/assets/js/plugins/datetimepicker/datetimepicker.min.css"><!-- Dependencies Plugins [CSS] [videojs|mediaelementplayer] --><!-- Dependencies CSS [Twemoji-Awesome|Flag-Icon] --><link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['system']->value['cdn_url'];?>
/includes/assets/css/twemoji-awesome/twemoji-awesome.min.css" onload="if(media!='all')media='all'"><link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['system']->value['cdn_url'];?>
/includes/assets/css/flag-icon/css/flag-icon.min.css" onload="if(media!='all')media='all'"><!-- Dependencies CSS [Twemoji-Awesome|Flag-Icon] --><link rel="stylesheet" type='text/css' href="<?php echo $_smarty_tpl->tpl_vars['system']->value['cdn_url'];?>
/includes/assets/js/plugins/metisMenu/metisMenu.min.css"><!-- Core [JS] --><?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['system']->value['cdn_url'];?>
/includes/assets/js/core<?php echo $_smarty_tpl->tpl_vars['system']->value["js_path"];?>
/core.js"><?php echo '</script'; ?>
><!-- CICommon [JS] --><?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['system']->value['cdn_url'];?>
/includes/assets/js/coniu<?php echo $_smarty_tpl->tpl_vars['system']->value["js_path"];?>
/cicommon.js"><?php echo '</script'; ?>
><?php if ($_smarty_tpl->tpl_vars['user']->value->_logged_in) {?><!-- JS Initialize Firebase --><?php if ($_smarty_tpl->tpl_vars['system']->value['sync_firebase']) {
echo $_smarty_tpl->tpl_vars['system']->value["initialize_firebase"];
echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['system']->value['cdn_url'];?>
/includes/assets/js/core<?php echo $_smarty_tpl->tpl_vars['system']->value["js_path"];?>
/chat.js"><?php echo '</script'; ?>
><?php }?><!-- JS Initialize Firebase --><?php if ($_smarty_tpl->tpl_vars['system']->value['geolocation_enabled']) {
echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/includes/assets/js/plugins/jquery.geocomplete/jquery.geocomplete.min.js"><?php echo '</script'; ?>
><?php echo '<script'; ?>
 src="https://maps.googleapis.com/maps/api/js?libraries=places&key=<?php echo $_smarty_tpl->tpl_vars['system']->value['geolocation_key'];?>
"><?php echo '</script'; ?>
><?php }?><!-- Bootstrap select --><?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['system']->value['cdn_url'];?>
/includes/assets/js/plugins/bootstrap.select/bootstrap-select.min.js"><?php echo '</script'; ?>
><link rel="stylesheet" type='text/css' href="<?php echo $_smarty_tpl->tpl_vars['system']->value['cdn_url'];?>
/includes/assets/js/plugins/bootstrap.select/bootstrap-select.min.css"><!-- Bootstrap select --><?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['system']->value['cdn_url'];?>
/includes/assets/js/plugins/noty/noty.min.js"><?php echo '</script'; ?>
><link rel="stylesheet" type='text/css' href="<?php echo $_smarty_tpl->tpl_vars['system']->value['cdn_url'];?>
/includes/assets/js/plugins/noty/noty.css"><!-- CI - Trình soạn thảo văn bản (Dùng cho article) --><?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['system']->value['cdn_url'];?>
/includes/assets/js/core<?php echo $_smarty_tpl->tpl_vars['system']->value["js_path"];?>
/user.js"><?php echo '</script'; ?>
><?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['system']->value['cdn_url'];?>
/includes/assets/js/core<?php echo $_smarty_tpl->tpl_vars['system']->value["js_path"];?>
/post.js"><?php echo '</script'; ?>
><?php }
echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['system']->value['cdn_url'];?>
/includes/assets/js/plugins/metisMenu/metisMenu.min.js"><?php echo '</script'; ?>
><!-- Core [JS] --><?php if ($_smarty_tpl->tpl_vars['page']->value == "admin") {?><!-- Dependencies Plugins [JS] --><?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['system']->value['cdn_url'];?>
/includes/assets/js/plugins/metisMenu/metisMenu.min.js"><?php echo '</script'; ?>
><?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['system']->value['cdn_url'];?>
/includes/assets/js/plugins/dataTables/jquery.dataTables.min.js"><?php echo '</script'; ?>
><?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['system']->value['cdn_url'];?>
/includes/assets/js/plugins/dataTables/dataTables.bootstrap.min.js"><?php echo '</script'; ?>
><?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['system']->value['cdn_url'];?>
/includes/assets/js/plugins/colorpicker/bootstrap-colorpicker.min.js"><?php echo '</script'; ?>
><!-- Dependencies Plugins [JS] --><!-- Dependencies Plugins [CSS] --><link rel="stylesheet" type='text/css' href="<?php echo $_smarty_tpl->tpl_vars['system']->value['cdn_url'];?>
/includes/assets/js/plugins/metisMenu/metisMenu.min.css"><link rel="stylesheet" type='text/css' href="<?php echo $_smarty_tpl->tpl_vars['system']->value['cdn_url'];?>
/includes/assets/js/plugins/dataTables/dataTables.bootstrap.min.css"><link rel="stylesheet" type='text/css' href="<?php echo $_smarty_tpl->tpl_vars['system']->value['cdn_url'];?>
/includes/assets/js/plugins/colorpicker/bootstrap-colorpicker.min.css"><link rel="stylesheet" type='text/css' href="<?php echo $_smarty_tpl->tpl_vars['system']->value['cdn_url'];?>
/includes/assets/js/plugins/datetimepicker/datetimepicker.min.css"><!-- Dependencies Plugins [CSS] --><!-- Core [JS] --><?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['system']->value['cdn_url'];?>
/includes/assets/js/core<?php echo $_smarty_tpl->tpl_vars['system']->value["js_path"];?>
/admin.js"><?php echo '</script'; ?>
><!-- Core [JS] --><!-- Admin Charts --><?php echo '<script'; ?>
 src="https://code.highcharts.com/highcharts.js"><?php echo '</script'; ?>
><?php echo '<script'; ?>
 src="https://code.highcharts.com/modules/exporting.js"><?php echo '</script'; ?>
><?php if ($_smarty_tpl->tpl_vars['view']->value == "dashboard") {
echo '<script'; ?>
>$(function () {Highcharts.setOptions();$('#admin-chart-dashboard').highcharts({chart: {type: 'column'},title: {text: __["Monthly Average"]},xAxis: {categories: [__["Jan"],__["Feb"],__["Mar"],__["Apr"],__["May"],__["Jun"],__["Jul"],__["Aug"],__["Sep"],__["Oct"],__["Nov"],__["Dec"]],crosshair: true},yAxis: {min: 0,title: {text: "<?php echo date('Y');?>
"}},tooltip: {headerFormat: '<span style="font-size:10px">{point.key}</span><table>',pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +'<td style="padding:0"><b>{point.y}</b></td></tr>',footerFormat: '</table>',shared: true,useHTML: true},plotOptions: {column: {pointPadding: 0.2,borderWidth: 0}},series: [{name: __["Users"],data: [<?php echo implode(',',$_smarty_tpl->tpl_vars['chart']->value['users']);?>
]}, {name: __["Pages"],data: [<?php echo implode(',',$_smarty_tpl->tpl_vars['chart']->value['pages']);?>
]}, {name: __["Groups"],data: [<?php echo implode(',',$_smarty_tpl->tpl_vars['chart']->value['groups']);?>
]}, {name: __["Events"],data: [<?php echo implode(',',$_smarty_tpl->tpl_vars['chart']->value['events']);?>
]}, {name: __["Posts"],data: [<?php echo implode(',',$_smarty_tpl->tpl_vars['chart']->value['posts']);?>
]}]});});<?php echo '</script'; ?>
><?php }
if ($_smarty_tpl->tpl_vars['view']->value == "packages" && $_smarty_tpl->tpl_vars['sub_view']->value == "earnings") {
echo '<script'; ?>
>$(function () {Highcharts.setOptions();$('#admin-chart-earnings').highcharts({chart: {type: 'column'},title: {text: __["Monthly Average"]},xAxis: {categories: [__["Jan"],__["Feb"],__["Mar"],__["Apr"],__["May"],__["Jun"],__["Jul"],__["Aug"],__["Sep"],__["Oct"],__["Nov"],__["Dec"]],crosshair: true},yAxis: {min: 0,title: {text: "<?php echo date('Y');?>
"}},tooltip: {headerFormat: '<span style="font-size:10px">{point.key}</span><table>',pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +'<td style="padding:0"><b>{point.y}</b></td></tr>',footerFormat: '</table>',shared: true,useHTML: true},plotOptions: {column: {pointPadding: 0.2,borderWidth: 0}},series: [<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['rows']->value, 'value', false, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['value']->value) {
?>{name: "<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
",data: [<?php echo implode(',',$_smarty_tpl->tpl_vars['value']->value['months_sales']);?>
]},<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>
]});});<?php echo '</script'; ?>
><?php }?><!-- Admin Charts --><!-- Admin Code Editor --><?php if ($_smarty_tpl->tpl_vars['view']->value == "design") {
echo '<script'; ?>
>
            $(function () {
                CodeMirror.fromTextArea(document.getElementById('custome_js_header'), {
                    mode: "javascript",
                    lineNumbers: true,
                    readOnly: false
                });

                CodeMirror.fromTextArea(document.getElementById('custome_js_footer'), {
                    mode: "javascript",
                    lineNumbers: true,
                    readOnly: false
                });

                CodeMirror.fromTextArea(document.getElementById('custom-css'), {
                    mode: "css",
                    lineNumbers: true,
                    readOnly: false
                });
            });
        <?php echo '</script'; ?>
><?php }?><!-- Admin Code Editor --><?php }?><!-- ConIu - BEGIN --><?php if (($_smarty_tpl->tpl_vars['page']->value == "school") || ($_smarty_tpl->tpl_vars['page']->value == "class") || ($_smarty_tpl->tpl_vars['page']->value == "child") || ($_smarty_tpl->tpl_vars['page']->value == "noga") || $_smarty_tpl->tpl_vars['page']->value == "childinfo" || ($_smarty_tpl->tpl_vars['page']->value == '') || ($_smarty_tpl->tpl_vars['page']->value == "region")) {?><!-- Dependencies Plugins [JS] --><?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['system']->value['cdn_url'];?>
/includes/assets/js/plugins/metisMenu/metisMenu.min.js"><?php echo '</script'; ?>
><?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['system']->value['cdn_url'];?>
/includes/assets/js/plugins/dataTables/jquery.dataTables.min.js"><?php echo '</script'; ?>
><?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['system']->value['cdn_url'];?>
/includes/assets/js/plugins/dataTables/dataTables.bootstrap.min.js"><?php echo '</script'; ?>
><?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['system']->value['cdn_url'];?>
/includes/assets/js/plugins/colorpicker/bootstrap-colorpicker.min.js"><?php echo '</script'; ?>
><?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['system']->value['cdn_url'];?>
/includes/assets/js/jquery/jquery-ui-1.12.1.min.js"><?php echo '</script'; ?>
><?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['system']->value['cdn_url'];?>
/includes/assets/js/jquery/jquery.mask.min.js"><?php echo '</script'; ?>
><!-- Dependencies Plugins [JS] --><!-- Dependencies Plugins [CSS] --><link rel="stylesheet" type='text/css' href="<?php echo $_smarty_tpl->tpl_vars['system']->value['cdn_url'];?>
/includes/assets/js/jquery/jquery-ui-1.12.1.min.css"><link rel="stylesheet" type='text/css' href="<?php echo $_smarty_tpl->tpl_vars['system']->value['cdn_url'];?>
/includes/assets/js/plugins/metisMenu/metisMenu.min.css"><link rel="stylesheet" type='text/css' href="<?php echo $_smarty_tpl->tpl_vars['system']->value['cdn_url'];?>
/includes/assets/js/plugins/dataTables/dataTables.bootstrap.min.css"><link rel="stylesheet" type='text/css' href="<?php echo $_smarty_tpl->tpl_vars['system']->value['cdn_url'];?>
/includes/assets/js/plugins/colorpicker/bootstrap-colorpicker.min.css"><link rel="stylesheet" type='text/css' href="<?php echo $_smarty_tpl->tpl_vars['system']->value['cdn_url'];?>
/includes/assets/js/plugins/datetimepicker/datetimepicker.min.css"><!-- Dependencies Plugins [CSS] --><?php if ($_smarty_tpl->tpl_vars['page']->value == "school") {
if (@constant('DEBUGGING')) {
echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['system']->value['cdn_url'];?>
/includes/assets/js/coniu<?php echo $_smarty_tpl->tpl_vars['system']->value['js_path'];?>
/school.js"><?php echo '</script'; ?>
><?php } else {
echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['system']->value['cdn_url'];?>
/includes/assets/js/coniu<?php echo $_smarty_tpl->tpl_vars['system']->value['js_path'];?>
/school.min.js"><?php echo '</script'; ?>
><?php }
} elseif ($_smarty_tpl->tpl_vars['page']->value == "class") {
if (@constant('DEBUGGING')) {
echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['system']->value['cdn_url'];?>
/includes/assets/js/coniu<?php echo $_smarty_tpl->tpl_vars['system']->value['js_path'];?>
/class.js"><?php echo '</script'; ?>
><?php } else {
echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['system']->value['cdn_url'];?>
/includes/assets/js/coniu<?php echo $_smarty_tpl->tpl_vars['system']->value['js_path'];?>
/class.min.js"><?php echo '</script'; ?>
><?php }
} elseif ($_smarty_tpl->tpl_vars['page']->value == "noga") {
if (@constant('DEBUGGING')) {
echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['system']->value['cdn_url'];?>
/includes/assets/js/coniu<?php echo $_smarty_tpl->tpl_vars['system']->value['js_path'];?>
/noga.js"><?php echo '</script'; ?>
><?php } else {
echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['system']->value['cdn_url'];?>
/includes/assets/js/coniu<?php echo $_smarty_tpl->tpl_vars['system']->value['js_path'];?>
/noga.min.js"><?php echo '</script'; ?>
><?php }
} elseif ($_smarty_tpl->tpl_vars['page']->value == "region") {
if (@constant('DEBUGGING')) {
echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['system']->value['cdn_url'];?>
/includes/assets/js/coniu<?php echo $_smarty_tpl->tpl_vars['system']->value['js_path'];?>
/region.js"><?php echo '</script'; ?>
><?php } else {
echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['system']->value['cdn_url'];?>
/includes/assets/js/coniu<?php echo $_smarty_tpl->tpl_vars['system']->value['js_path'];?>
/region.min.js"><?php echo '</script'; ?>
><?php }
} else {
if (@constant('DEBUGGING')) {
echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['system']->value['cdn_url'];?>
/includes/assets/js/coniu<?php echo $_smarty_tpl->tpl_vars['system']->value['js_path'];?>
/child.js"><?php echo '</script'; ?>
><?php } else {
echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['system']->value['cdn_url'];?>
/includes/assets/js/coniu<?php echo $_smarty_tpl->tpl_vars['system']->value['js_path'];?>
/child.min.js"><?php echo '</script'; ?>
><?php }
}?><!-- BEGIN - Thêm plugin datepicker --><!--<?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['system']->value['cdn_url'];?>
/includes/assets/js/coniu/datetimepicker/js/bootstrap-datetimepicker.min.js"><?php echo '</script'; ?>
><link rel="stylesheet" type='text/css' href="<?php echo $_smarty_tpl->tpl_vars['system']->value['cdn_url'];?>
/includes/assets/js/coniu/datetimepicker/css/bootstrap-datetimepicker.min.css">--><!-- END - Thêm plugin datepicker --><?php }
if (@constant('DEBUGGING')) {
echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['system']->value['cdn_url'];?>
/includes/assets/js/coniu<?php echo $_smarty_tpl->tpl_vars['system']->value['js_path'];?>
/notify.js"><?php echo '</script'; ?>
><?php } else {
echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['system']->value['cdn_url'];?>
/includes/assets/js/coniu<?php echo $_smarty_tpl->tpl_vars['system']->value['js_path'];?>
/notify.min.js"><?php echo '</script'; ?>
><?php }?><!-- ConIu - END --><?php }
}
