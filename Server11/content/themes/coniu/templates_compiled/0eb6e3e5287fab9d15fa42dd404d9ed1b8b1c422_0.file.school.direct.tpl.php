<?php
/* Smarty version 3.1.31, created on 2021-05-19 11:13:26
  from "D:\workplace\mascom-edu-server\Server11\content\themes\coniu\templates\ci\school\school.direct.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_60a49066022893_82298435',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '0eb6e3e5287fab9d15fa42dd404d9ed1b8b1c422' => 
    array (
      0 => 'D:\\workplace\\mascom-edu-server\\Server11\\content\\themes\\coniu\\templates\\ci\\school\\school.direct.tpl',
      1 => 1619677394,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60a49066022893_82298435 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="school_direct" align="center" style="margin-top: 20px">
    <?php if ($_smarty_tpl->tpl_vars['view']->value == '') {?>
    <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "classlevels") {?>
        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" class="btn btn-default"><i class="fas fa-chevron-left"></i> <?php echo __("Back");?>
</a>
        <?php if (count($_smarty_tpl->tpl_vars['rows']->value) > 0 && count($_smarty_tpl->tpl_vars['classesStep']->value) > 0) {?>
            <a href="#" class="btn btn-success js_school-step" data-step="1" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-view="classes" data-subview=""><?php echo __("Next");?>
 <i class="fas fa-chevron-right"></i></a>
        <?php } elseif (count($_smarty_tpl->tpl_vars['rows']->value) > 0 && count($_smarty_tpl->tpl_vars['classesStep']->value) == 0) {?>
            <a href="#" class="btn btn-success js_school-step" data-step="1" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-view="classes" data-subview="add"><?php echo __("Next");?>
 <i class="fas fa-chevron-right"></i></a>
        <?php }?>

    <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "classes") {?>
        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/classlevels" class="btn btn-default"><i class="fas fa-chevron-left"></i> <?php echo __("Back");?>
</a>
        <?php if (count($_smarty_tpl->tpl_vars['teachersStep']->value) > 0) {?>
            <a href="#" class="btn btn-success js_school-step" data-step="2" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-view="teachers" data-subview=""><?php echo __("Next");?>
 <i class="fas fa-chevron-right"></i></a>
        <?php } else { ?>
            <a href="#" class="btn btn-success js_school-step" data-step="2" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-view="teachers" data-subview="add"><?php echo __("Next");?>
 <i class="fas fa-chevron-right"></i></a>
        <?php }?>

    <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "teachers") {?>
        <?php if (count($_smarty_tpl->tpl_vars['classesStep']->value) > 0) {?>
            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/classes" class="btn btn-default"><i class="fas fa-chevron-left"></i> <?php echo __("Back");?>
</a>
        <?php } else { ?>
            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/classes/add" class="btn btn-default"><i class="fas fa-chevron-left"></i> <?php echo __("Back");?>
</a>
        <?php }?>
        <?php if (count($_smarty_tpl->tpl_vars['servicesStep']->value) > 0) {?>
            <a href="#" class="btn btn-success js_school-step" data-step="3" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-view="services" data-subview=""><?php echo __("Next");?>
 <i class="fas fa-chevron-right"></i></a>
        <?php } else { ?>
            <a href="#" class="btn btn-success js_school-step" data-step="3" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-view="services" data-subview="add"><?php echo __("Next");?>
 <i class="fas fa-chevron-right"></i></a>
        <?php }?>

    <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "services") {?>
        <?php if (count($_smarty_tpl->tpl_vars['teachersStep']->value > 0)) {?>
            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/teachers" class="btn btn-default"><i class="fas fa-chevron-left"></i> <?php echo __("Back");?>
</a>
        <?php } else { ?>
            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/teachers/add" class="btn btn-default"><i class="fas fa-chevron-left"></i> <?php echo __("Back");?>
</a>
        <?php }?>
        <?php if (count($_smarty_tpl->tpl_vars['feesStep']->value) > 0) {?>
            <a href="#" class="btn btn-success js_school-step" data-step="4" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-view="fees" data-subview=""><?php echo __("Next");?>
 <i class="fas fa-chevron-right"></i></a>
        <?php } else { ?>
            <a href="#" class="btn btn-success js_school-step" data-step="4" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-view="fees" data-subview="add"><?php echo __("Next");?>
 <i class="fas fa-chevron-right"></i></a>
        <?php }?>

    <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "fees") {?>
        <?php if (count($_smarty_tpl->tpl_vars['servicesStep']->value) > 0) {?>
            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/services" class="btn btn-default"><i class="fas fa-chevron-left"></i> <?php echo __("Back");?>
</a>
        <?php } else { ?>
            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/services/add" class="btn btn-default"><i class="fas fa-chevron-left"></i> <?php echo __("Back");?>
</a>
        <?php }?>
        <a href="#" class="btn btn-success js_school-step" data-step="5" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-view="pickup" data-subview="template"><?php echo __("Next");?>
 <i class="fas fa-chevron-right"></i></a>

    <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "pickup" && $_smarty_tpl->tpl_vars['sub_view']->value == "template") {?>
        <?php if (count($_smarty_tpl->tpl_vars['feesStep']->value) > 0) {?>
            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/fees" class="btn btn-default"><i class="fas fa-chevron-left"></i> <?php echo __("Back");?>
</a>
        <?php } else { ?>
            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/fees/add" class="btn btn-default"><i class="fas fa-chevron-left"></i> <?php echo __("Back");?>
</a>
        <?php }?>
        <a href="#" class="btn btn-success js_school-step" data-step="6" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-view="pickup" data-subview="assign"><?php echo __("Next");?>
 <i class="fas fa-chevron-right"></i></a>

    <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "pickup" && $_smarty_tpl->tpl_vars['sub_view']->value == "assign") {?>
        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/pickup/template" class="btn btn-default"><i class="fas fa-chevron-left"></i> <?php echo __("Back");?>
</a>
        <a href="#" class="btn btn-success js_school-step" data-step="7" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-view="children" data-subview=""><?php echo __("Next");?>
 <i class="fas fa-chevron-right"></i></a>

    <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "children") {?>
        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/pickup/assign" class="btn btn-default"><i class="fas fa-chevron-left"></i> <?php echo __("Back");?>
</a>
        <a href="#" class="btn btn-success js_school-step" data-step="100" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-view="" data-subview=""><?php echo __("Finish");?>
 <i class="fas fa-chevron-right"></i></a>
    <?php }?>
</div><?php }
}
