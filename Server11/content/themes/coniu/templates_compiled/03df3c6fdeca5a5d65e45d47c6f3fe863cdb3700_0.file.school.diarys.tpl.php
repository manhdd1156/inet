<?php
/* Smarty version 3.1.31, created on 2021-03-31 10:41:30
  from "D:\workplace\Server11\content\themes\coniu\templates\ci\school\school.diarys.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_6063ef6a7eb541_13098956',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '03df3c6fdeca5a5d65e45d47c6f3fe863cdb3700' => 
    array (
      0 => 'D:\\workplace\\Server11\\content\\themes\\coniu\\templates\\ci\\school\\school.diarys.tpl',
      1 => 1552404702,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:ci/ajax.school.journal.list.tpl' => 1,
  ),
),false)) {
function content_6063ef6a7eb541_13098956 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <?php if ($_smarty_tpl->tpl_vars['canEdit']->value) {?>
            <div class="pull-right flip">
                <a href="https://blog.coniu.vn/huong-dan-tao-nhat-ky-cho-tre/" target="_blank" class="btn btn-info btn_guide">
                    <i class="fa fa-info"></i> <?php echo __("Guide");?>

                </a>
                <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/diarys/add" class="btn btn-default">
                        <i class="fa fa-plus"></i> <?php echo __("Add New");?>

                    </a>
                <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/diarys" class="btn btn-default">
                        <i class="fa fa-list"></i> <?php echo __("Diary lists");?>

                    </a>
                <?php }?>
            </div>
        <?php }?>
        <div class="pull-right flip" style="margin-right: 5px">
            
                
            
        </div>
        <i class="fa fa-image fa-fw fa-lg pr10"></i>
        <?php echo __("Diary corner");?>

        <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
            &rsaquo; <?php echo __("Lists");?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
            &rsaquo; <?php echo __("Add New");?>

        <?php }?>
    </div>

    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
        <div class="panel-body">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_child.php">
                <input type="hidden" name="school_username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                <input type="hidden" name="do" value="search_diary_class"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-right"><?php echo __("Select child");?>
 (*)</label>
                    <div class='col-sm-3'>
                        <select name="class_id" id="attendance_class_id" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" class="form-control" autofocus required>
                            <option value=""><?php echo __("Select Class");?>
...</option>
                            <?php $_smarty_tpl->_assignInScope('class_level', -1);
?>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['classes']->value, 'class');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class']->value) {
?>
                                <?php if (($_smarty_tpl->tpl_vars['class_level']->value != $_smarty_tpl->tpl_vars['class']->value['class_level_id'])) {?>
                                    <option value="" disabled>-----<?php echo $_smarty_tpl->tpl_vars['class']->value['class_level_name'];?>
-----</option>
                                <?php }?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['class']->value['group_id'];?>
" <?php if ($_smarty_tpl->tpl_vars['classId']->value != '' && $_smarty_tpl->tpl_vars['classId']->value == $_smarty_tpl->tpl_vars['class']->value['group_id']) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['class']->value['group_title'];?>
</option>
                                <?php $_smarty_tpl->_assignInScope('class_level', $_smarty_tpl->tpl_vars['class']->value['class_level_id']);
?>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>


                        </select>
                    </div>
                    <div class="col-sm-4">
                        <select name="child_id" id="attendance_child_id" class="form-control" required>
                            <option value=""><?php echo __("Select child");?>
...</option>
                            <?php if ($_smarty_tpl->tpl_vars['childId']->value != '') {?>
                                <?php if (isset($_smarty_tpl->tpl_vars['children']->value) && !empty($_smarty_tpl->tpl_vars['children']->value)) {?>
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['children']->value, 'child');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['child']->value) {
?>
                                        <option value="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
" <?php if (($_smarty_tpl->tpl_vars['child']->value['child_id'] == $_smarty_tpl->tpl_vars['childId']->value)) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['child']->value['child_name'];?>
</option>
                                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                
                                    
                                <?php }?>
                            <?php }?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Year");?>
</label>
                    <div class="col-sm-3">
                        <select name="year" id="year" class="form-control">
                            <option value = "0"><?php echo __("Select year ...");?>
</option>
                            <?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable(null, $_smarty_tpl->isRenderingCache);
$_smarty_tpl->tpl_vars['i']->value = $_smarty_tpl->tpl_vars['year_begin']->value;
if ($_smarty_tpl->tpl_vars['i']->value < $_smarty_tpl->tpl_vars['year_end']->value) {
for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value < $_smarty_tpl->tpl_vars['year_end']->value; $_smarty_tpl->tpl_vars['i']->value++) {
?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" <?php if ($_smarty_tpl->tpl_vars['year']->value != '' && $_smarty_tpl->tpl_vars['year']->value == $_smarty_tpl->tpl_vars['i']->value) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['i']->value;?>
</option>
                            <?php }
}
?>

                        </select>
                    </div>
                    <div class="col-md-3">
                        <a href="#" id="search" class="btn btn-default js_school-diary-search" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-isnew="1"><?php echo __("Search");?>
</a>
                        <label id="loading" class="btn btn-info x-hidden"><?php echo __("Loading");?>
...</label>
                    </div>
                </div>
                <div id = "journal_list">
                    <?php $_smarty_tpl->_subTemplateRender("file:ci/ajax.school.journal.list.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                </div>
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
            </form>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
        <div class="panel-body">
            <form class="form-horizontal" enctype="multipart/form-data" method="post" id="add_child_journal_school">
                <input type="hidden" name="school_username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                <input type="hidden" name="do" value="add_photo"/>
                <input type="hidden" name="reload" value="1">
                <input type="hidden" name="module" value="1">
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Select child");?>
 (*)</label>
                    <div class="col-sm-4">
                        <select name="class_id" id="medicine_class_id" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" class="form-control" autofocus required>
                            <option value=""><?php echo __("Select class");?>
</option>
                            <?php $_smarty_tpl->_assignInScope('class_level', -1);
?>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['classes']->value, 'class');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class']->value) {
?>
                                <?php if (($_smarty_tpl->tpl_vars['class_level']->value != $_smarty_tpl->tpl_vars['class']->value['class_level_id'])) {?>
                                    <option value="" disabled style="color: blue">-----<?php echo $_smarty_tpl->tpl_vars['class']->value['class_level_name'];?>
-----</option>
                                <?php }?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['class']->value['group_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['class']->value['group_title'];?>
</option>
                                <?php $_smarty_tpl->_assignInScope('class_level', $_smarty_tpl->tpl_vars['class']->value['class_level_id']);
?>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </select>
                    </div>
                    <div class="col-sm-5">
                        <select name="child_id" id="medicine_child_id" class="form-control" required></select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Caption");?>
</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="caption" placeholder="<?php echo __("Caption");?>
" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Picture");?>
</label>
                    <div class="col-sm-6">
                        <input name="file[]" type="file" multiple="true" required>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30"><?php echo __("Save");?>
</button>
                    </div>
                </div>
            </form>
        </div>
    <?php }?>
</div><?php }
}
