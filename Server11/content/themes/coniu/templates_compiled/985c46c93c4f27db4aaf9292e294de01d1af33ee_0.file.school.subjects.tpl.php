<?php
/* Smarty version 3.1.31, created on 2021-04-23 17:52:38
  from "D:\workplace\mascom-edu-server\Server11\content\themes\coniu\templates\ci\school\school.subjects.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_6082a6f65a1475_32626913',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '985c46c93c4f27db4aaf9292e294de01d1af33ee' => 
    array (
      0 => 'D:\\workplace\\mascom-edu-server\\Server11\\content\\themes\\coniu\\templates\\ci\\school\\school.subjects.tpl',
      1 => 1619174970,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:ci/ajax.classlevellist.tpl' => 1,
  ),
),false)) {
function content_6082a6f65a1475_32626913 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <i class="fa fa-book fa-fw fa-lg pr10"></i>
        <?php echo __("Subject");?>

        <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
            &rsaquo; <?php echo __('Lists');?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "edit") {?>
            &rsaquo; <?php echo $_smarty_tpl->tpl_vars['data']->value['subject_name'];?>

        <?php }?>
    </div>
    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
        <div class="panel-body with-table">
            <div class="mb5"><strong><?php echo __("subject list");?>
&nbsp;(<?php echo count($_smarty_tpl->tpl_vars['rows']->value);?>
 <?php echo __("Subject");?>
)</strong></div>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th width="5%">#</th>
                            <th width="20%"><?php echo __("Subject");?>
</th>
                            <th width="60%"><?php echo __("Class level");?>
</th>
                            <th width="15%"><?php echo __("Actions");?>
</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['rows']->value, 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
                            <tr>
                                <td class="align-middle" align="center"><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</td>
                                <td class="align-middle"><?php echo $_smarty_tpl->tpl_vars['row']->value['subject_name'];?>
</td>
                                <td class="align-middle">
                                    <?php if (count($_smarty_tpl->tpl_vars['row']->value['class_levels']) == 0) {?>
                                        <?php echo __("No class level");?>

                                    <?php } else { ?>
                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['row']->value['class_levels'], 'class_level', false, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['class_level']->value) {
?>
                                            <span class="name js_user-popover" data-uid="<?php echo $_smarty_tpl->tpl_vars['class_level']->value['class_level_id'];?>
">
                                                <span ><?php echo $_smarty_tpl->tpl_vars['class_level']->value['class_level_name'];?>
 </span>
                                            </span>
                                            <?php if ($_smarty_tpl->tpl_vars['key']->value != (count($_smarty_tpl->tpl_vars['row']->value['class_levels'])-1)) {?>
                                                <span>,</span>
                                            <?php }?>
                                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                    <?php }?>
                                </td class="align-middle">
                                <td class="align-middle">
                                    <?php if ($_smarty_tpl->tpl_vars['canEdit']->value) {?>
                                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/subjects/edit/<?php echo $_smarty_tpl->tpl_vars['row']->value['subject_id'];?>
" class="btn btn-xs btn-default"><?php echo __("Edit");?>
</a>
                                    <?php }?>
                                </td>
                            </tr>
                            <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>


                        <?php if (count($_smarty_tpl->tpl_vars['rows']->value) == 0) {?>
                            <tr class="odd">
                                <td valign="top" align="center" colspan="6" class="dataTables_empty">
                                    <?php echo __("No data available in table");?>

                                </td>
                            </tr>
                        <?php }?>
                    </tbody>
                </table>
            </div>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "edit") {?>
        <div class="panel-body">

            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_subject.php">
                <input type="hidden" name="school_id" id="school_id" value="<?php echo $_smarty_tpl->tpl_vars['school_id']->value;?>
"/>
                <input type="hidden" name="school_username" id="school_username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                <input type="hidden" name="subject_id" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['subject_id'];?>
"/>
                <input type="hidden" name="do" value="edit"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">
                        <?php echo __("Class level");?>

                    </label>
                    <div class="col-sm-9">

                        <input name="search-classlevel" id="search-classlevel" type="text" class="form-control" placeholder="<?php echo __("Enter class level name to search");?>
" autocomplete="off">
                        <div id="search-classlevel-results" class="dropdown-menu dropdown-widget dropdown-search">
                            <div class="dropdown-widget-header">
                                <?php echo __("Search Results");?>

                            </div>
                            <div class="dropdown-widget-body">
                                <div class="loader loader_small ptb10"></div>
                            </div>
                        </div>
                        <br/>
                        <div class="col-sm-9" id="class_level_list" name="class_level_list">

                            <?php if (count($_smarty_tpl->tpl_vars['class_levels_assigned']->value) > 0) {?>

                                <?php $_smarty_tpl->_subTemplateRender('file:ci/ajax.classlevellist.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('results'=>$_smarty_tpl->tpl_vars['class_levels_assigned']->value), 0, false);
?>

                            <?php } else { ?>
                                <?php echo __("No class level");?>

                            <?php }?>
                        </div>
                    </div>
                </div>





                
                    
                        
                    
                    
                        
                    
                
                
                    
                        
                    
                    
                        
                    
                
                
                    
                        
                    
                    
                        
                    
                









                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30 js_show-loading"><?php echo __("Save");?>
</button>
                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/subjects" class="btn btn-default"><?php echo __("Lists");?>
</a>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->
                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    <?php }?>
</div><?php }
}
