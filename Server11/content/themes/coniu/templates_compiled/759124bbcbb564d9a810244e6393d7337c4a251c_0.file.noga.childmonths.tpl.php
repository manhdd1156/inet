<?php
/* Smarty version 3.1.31, created on 2021-04-05 13:07:25
  from "D:\workplace\Server11\content\themes\coniu\templates\ci\noga\noga.childmonths.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_606aa91d6a2351_84690141',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '759124bbcbb564d9a810244e6393d7337c4a251c' => 
    array (
      0 => 'D:\\workplace\\Server11\\content\\themes\\coniu\\templates\\ci\\noga\\noga.childmonths.tpl',
      1 => 1552404703,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_606aa91d6a2351_84690141 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
            <div class="pull-right flip">
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/noga/childmonths/add" class="btn btn-default">
                    <i class="fa fa-plus"></i> <?php echo __("Add New");?>

                </a>
            </div>
        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
            <div class="pull-right flip">
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/noga/childmonths" class="btn btn-default">
                    <i class="fa fa-list"></i> <?php echo __("Lists");?>

                </a>
            </div>
        <?php }?>
        <i class="fas fa-calendar-alt fa-fw fa-lg pr10"></i>
        <?php echo __("Month age information");?>

        <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
            &rsaquo; <?php echo __("Add New");?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "edit") {?>
            &rsaquo; <?php echo __("Edit");?>

        <?php }?>
    </div>
    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
        <div class="panel-body with-table form-horizontal">
            <div class="table-responsive" name = "schedule_all">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr><th colspan="5"><?php echo __("Month age information");?>
</th></tr>
                    <tr>
                        <th>
                            <?php echo __("No.");?>

                        </th>
                        <th>
                            <?php echo __("Month age");?>

                        </th>
                        <th>
                            <?php echo __("Title");?>

                        </th>
                        <th>
                            <?php echo __("Link");?>

                        </th>
                        <th>
                            <?php echo __("Actions");?>

                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['childMonths']->value, 'row', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['row']->value) {
?>
                        <tr>
                            <td align="center"><?php echo $_smarty_tpl->tpl_vars['k']->value+1;?>
</td>
                            <td align="center"><a href = "<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/noga/childmonths/edit/<?php echo $_smarty_tpl->tpl_vars['row']->value['child_based_on_month_id'];?>
"><?php echo __("Month information");?>
 <?php echo $_smarty_tpl->tpl_vars['row']->value['month'];?>
</a></td>
                            <td><a href = "<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/noga/childmonths/edit/<?php echo $_smarty_tpl->tpl_vars['row']->value['child_based_on_month_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['row']->value['title'];?>
</a></td>
                            <td>
                                <a href="<?php echo $_smarty_tpl->tpl_vars['row']->value['link'];?>
" target="_blank"><?php echo $_smarty_tpl->tpl_vars['row']->value['link'];?>
</a>
                            </td>
                            <td>
                                <a class="btn btn-xs btn-default" href = "<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/noga/childmonths/edit/<?php echo $_smarty_tpl->tpl_vars['row']->value['child_based_on_month_id'];?>
"><?php echo __("Edit");?>
</a>
                                <a href="#" class = "btn-xs btn btn-danger js_noga-child-month-delete" data-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['child_based_on_month_id'];?>
"><?php echo __("Delete");?>
</a>
                            </td>
                        </tr>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                    </tbody>
                </table>
            </div>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
        <div class="panel-body">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/noga/bo_noga_childmonth.php">
                <input type="hidden" name="do" value="add"/>
                <div class="form-group">
                    <label class="col-sm-4 control-label text-left" style="float: left"><?php echo __("Enter child month information");?>
</label>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id = "myTableChildMonth">
                        <thead>
                        <tr>
                            <th>
                                <?php echo __('No.');?>

                            </th>
                            <th>
                                <?php echo __('Option');?>

                            </th>
                            <th>
                                <?php echo __('Month age');?>

                            </th>
                            <th>
                                <?php echo __('Title');?>

                            </th>
                            <th>
                                <?php echo __('Link');?>

                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td align="center" class = "col_no">1</td>
                            <td align="center">
                                <a class = "btn btn-default js_cate-delete_null"> <?php echo __("Delete");?>
 </a>
                            </td>
                            <td>
                                <input type = "number" min="0" step="1" name = "months[]" placeholder="" style = "padding-left: 10px; height: 35px; width: 100%; ">
                            </td>
                            <td>
                                <textarea class = "note js_autosize" type="text" name = "titles[]" style="width:100%"></textarea>
                            </td>
                            <td>
                                <textarea class = "note js_autosize" type="text" name = "links[]" style="width:100%"></textarea>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <div class = "form-group mt20">
                        <div class = "col-sm-12">
                            <a class="btn btn-default js_add-child-month"><?php echo __("Add new row");?>
</a>
                            <button type="submit" class="btn btn-primary padrl30"><?php echo __("Save");?>
</button>
                        </div>
                    </div>
                </div>
                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->
                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->

            </form>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "edit") {?>
        <div class="panel-body">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/noga/bo_noga_childmonth.php">
                <input type="hidden" name="do" value="edit"/>
                <input type="hidden" name="child_based_on_month_id" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['child_based_on_month_id'];?>
"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Month information");?>
 (*)</label>
                    <div class="col-sm-9">
                        <input type = "text" class="form-control" name="month" required value = "<?php echo $_smarty_tpl->tpl_vars['data']->value['month'];?>
">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Title");?>
</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="title" rows="6"><?php echo $_smarty_tpl->tpl_vars['data']->value['title'];?>
</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Link");?>
</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="link" rows="6"><?php echo $_smarty_tpl->tpl_vars['data']->value['link'];?>
</textarea>
                    </div>
                </div>
                <div class = "form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-default"><?php echo __("Save");?>
</button>
                    </div>
                </div>
                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->
                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    <?php }?>
</div><?php }
}
