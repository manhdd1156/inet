<?php
/* Smarty version 3.1.31, created on 2021-05-13 14:01:27
  from "D:\workplace\mascom-edu-server\Server11\content\themes\coniu\templates\ci\noga\ajax.noga.schoollist.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_609ccec7d08344_36464268',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '7345287483c7bea3ea1e3f38fd24a418976ee58f' => 
    array (
      0 => 'D:\\workplace\\mascom-edu-server\\Server11\\content\\themes\\coniu\\templates\\ci\\noga\\ajax.noga.schoollist.tpl',
      1 => 1620098255,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_609ccec7d08344_36464268 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover js_dataTable">
        <thead>
        <tr><th><?php echo __("School list");?>
&nbsp;(<?php echo count($_smarty_tpl->tpl_vars['rows']->value);?>
)</th></tr>
        </thead>
        <tbody>
        <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['rows']->value, 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
            <tr>
                <td>
                    <div>
                        <?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
 - <?php echo $_smarty_tpl->tpl_vars['row']->value['page_title'];?>

                        &nbsp;|&nbsp;<a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/pages/<?php echo $_smarty_tpl->tpl_vars['row']->value['page_name'];?>
"><?php echo __("Timeline");?>
</a>
                        &nbsp;|&nbsp;<?php echo $_smarty_tpl->tpl_vars['row']->value['page_name'];?>

                        <?php if (($_smarty_tpl->tpl_vars['row']->value['telephone'] != '')) {?> &nbsp;|&nbsp;<?php echo $_smarty_tpl->tpl_vars['row']->value['telephone'];?>
 <?php }?>
                        <?php if (($_smarty_tpl->tpl_vars['row']->value['website'] != '')) {?> &nbsp;|&nbsp;<a href="<?php echo $_smarty_tpl->tpl_vars['row']->value['website'];?>
"><?php echo $_smarty_tpl->tpl_vars['row']->value['website'];?>
</a><?php }?>
                        <?php if (($_smarty_tpl->tpl_vars['row']->value['email'] != '')) {?> &nbsp;|&nbsp;<a href="mailto:<?php echo $_smarty_tpl->tpl_vars['row']->value['email'];?>
"><?php echo $_smarty_tpl->tpl_vars['row']->value['email'];?>
</a> <?php }?>
                    </div>
                    <div class="pull-right flip">
                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/noga/schools/module/<?php echo $_smarty_tpl->tpl_vars['row']->value['page_id'];?>
" class="btn btn-xs btn-primary"><?php echo __("Module");?>
</a>
                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/noga/schools/edit/<?php echo $_smarty_tpl->tpl_vars['row']->value['page_id'];?>
/basic" class="btn btn-xs btn-default"><?php echo __("Edit");?>
</a>
                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/noga/schools/viewuser/<?php echo $_smarty_tpl->tpl_vars['row']->value['page_id'];?>
" class="btn btn-xs btn-default"><?php echo __("View parent");?>
</a>
                        <select name="school_status_<?php echo $_smarty_tpl->tpl_vars['row']->value['page_id'];?>
" class="school_status_change" style="color: #3f729b" data-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['page_id'];?>
">
                            <option value="1" <?php if ($_smarty_tpl->tpl_vars['row']->value['school_status'] == SCHOOL_USING_CONIU) {?>selected<?php }?>><?php echo __("Using Mascom - Edu");?>
</option>
                            <option value="2" <?php if ($_smarty_tpl->tpl_vars['row']->value['school_status'] == SCHOOL_HAVE_PAGE_CONIU) {?>selected<?php }?>><?php echo __("Having Mascom - Edu's page");?>
</option>
                            <option value="3" <?php if ($_smarty_tpl->tpl_vars['row']->value['school_status'] == SCHOOL_WAITING_CONFIRM) {?>selected<?php }?>><?php echo __("Waiting confirmation");?>
</option>
                        </select>
                    </div>
                </td>
            </tr>
            <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

        </tbody>
    </table>
</div>

<?php echo '<script'; ?>
 type="text/javascript">
    $(function() {
        // run DataTable
        $('.js_dataTable').DataTable({
            "aoColumnDefs": [ { 'aDataSort': false, 'aTargets': [ -1 ] } ],
            "language": {
                "decimal":        "",
                "emptyTable":     __["No data available in table"],
                "info":           __["Showing _START_ to _END_ of _TOTAL_ results"],
                "infoEmpty":      __["Showing 0 to 0 of 0 results"],
                "infoFiltered":   "(filtered from _MAX_ total entries)",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     __["Show"] + " " + "_MENU_" + " " + __["results"],
                "loadingRecords": __["Loading..."],
                "processing":     __["Processing..."],
                "search":         __["Search"],
                "zeroRecords":    __["No matching records found"],
                "paginate": {
                    "first":      __["First"],
                    "last":       __["Last"],
                    "next":       __["Next"],
                    "previous":   __["Previous"]
                },
                "aria": {
                    "sortAscending":  ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                }
            }
        });
    });
<?php echo '</script'; ?>
><?php }
}
