<?php
/* Smarty version 3.1.31, created on 2019-06-24 11:01:43
  from "D:\Working\Coniu\ServerSvn\trunk\Server11\content\themes\coniu\templates\ci\school\ajax.school.attendance.conabs.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5d104b27e2b698_10676029',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '0f10e3f39658acdc102a457b3b0ee037e225ea85' => 
    array (
      0 => 'D:\\Working\\Coniu\\ServerSvn\\trunk\\Server11\\content\\themes\\coniu\\templates\\ci\\school\\ajax.school.attendance.conabs.tpl',
      1 => 1552404702,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d104b27e2b698_10676029 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div><strong><?php echo __("Consecutive absent student list");?>
&nbsp;(<?php echo count($_smarty_tpl->tpl_vars['children']->value);?>
&nbsp;<?php echo __("Child");?>
)</strong></div>
<table class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th>#</th>
        <th><?php echo __("Full name");?>
</th>
        <th><?php echo __("Class");?>
</th>
        <th><?php echo __("Absent day");?>
</th>
        <th><?php echo __("Reason");?>
</th>
    </tr>
    </thead>
    <tbody>

        <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['children']->value, 'child');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['child']->value) {
?>
            <tr>
                <td align="center"><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</td>
                <td><a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/attendance/child/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['child']->value['child_name'];?>
</a></td>
                <td align="center"><?php echo $_smarty_tpl->tpl_vars['child']->value['group_title'];?>
</td>
                <td align="center">
                    <?php if (isset($_smarty_tpl->tpl_vars['child']->value['number_of_absent_day']) && ($_smarty_tpl->tpl_vars['child']->value['number_of_absent_day'] > 0)) {?>
                        <?php ob_start();
echo $_smarty_tpl->tpl_vars['child']->value['number_of_absent_day'];
$_prefixVariable1=ob_get_clean();
if ($_prefixVariable1 >= @constant('MAX_CONSECUTIVE_ABSENT_DAY')) {?>
                            <?php echo __("More than");?>
&nbsp;<?php echo @constant('MAX_CONSECUTIVE_ABSENT_DAY');?>

                        <?php } else { ?>
                            <?php echo $_smarty_tpl->tpl_vars['child']->value['number_of_absent_day'];?>

                        <?php }?>
                        &nbsp;<?php echo __("day(s)");?>

                    <?php }?>
                </td>
                <td><?php echo $_smarty_tpl->tpl_vars['child']->value['reason'];?>
</td>
            </tr>
            <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

        <?php if (count($_smarty_tpl->tpl_vars['children']->value) == 0) {?>
            <tr class="odd">
                <td valign="top" align="center" colspan="5" class="dataTables_empty">
                    <?php echo __("No data available in table");?>

                </td>
            </tr>
        <?php }?>

    </tbody>
</table><?php }
}
