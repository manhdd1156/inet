<?php
/* Smarty version 3.1.31, created on 2021-03-31 09:03:47
  from "D:\workplace\Server11\content\themes\coniu\templates\ci\school\school.classlevels.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_6063d8831eae14_27655305',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'eedd7e85dd7f64a713de6e5ca24561fa5cd92fc7' => 
    array (
      0 => 'D:\\workplace\\Server11\\content\\themes\\coniu\\templates\\ci\\school\\school.classlevels.tpl',
      1 => 1575865657,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6063d8831eae14_27655305 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <?php if (($_smarty_tpl->tpl_vars['sub_view']->value == '') && $_smarty_tpl->tpl_vars['canEdit']->value) {?>
            <div class="pull-right flip">
                <a href="#" class="btn btn-default js_school-class-level-add">
                    <i class="fa fa-plus"></i> <?php echo __("Add New Class Level");?>

                </a>
            </div>
        <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['school']->value['school_step'] == SCHOOL_STEP_FINISH || $_smarty_tpl->tpl_vars['school']->value['grade'] != 0) {?>
            <div class="pull-right flip" style="margin-right: 5px">
                <a href="https://blog.coniu.vn/huong-dan-tao-thong-tin-khoi/" class="btn btn-info btn_guide" target="_blank">
                    <i class="fa fa-info-circle"></i> <?php echo __("Guide");?>

                </a>
            </div>
        <?php }?>
        <i class="fa fa-tree fa-fw fa-lg pr10"></i>
        <?php echo __("Class level");?>

        <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == "edit") {?>
            &rsaquo; <?php echo $_smarty_tpl->tpl_vars['data']->value['class_level_name'];?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
            &rsaquo; <?php echo __('Add New');?>

        <?php }?>
    </div>
    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
        <div class="panel-body with-table">
            <div class="x-hidden add_class_level_box" style="position: relative">
                <div class="" style="position: absolute; top: 0; right: 0;"><a href="#" class="close_box_add"><i style="font-size: 20px" class="fas fa-window-close"></i></a></div>
                <div class="" style="border: 1px solid #b1b1b1; padding: 5px; margin-bottom: 20px">
                    <div><strong><?php echo __("Add New Class Level");?>
:</strong></div>
                    <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_class_level.php">
                        <input type="hidden" name="school_username" id="school_username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                        <input type="hidden" name="do" value="add"/>
                        <div class="form-group">
                            <label class="col-sm-3 control-label text-left">
                                <?php echo __("Class level name");?>

                            </label>
                            <div class="col-sm-9">
                                <input class="form-control" name="class_level_name" required maxlength="45">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label text-left">
                                <?php echo __("Description");?>

                            </label>
                            <div class="col-sm-9">
                                <textarea class="form-control" name="description" rows="3" maxlength="300"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-9 col-sm-offset-3">
                                <?php if ($_smarty_tpl->tpl_vars['canEdit']->value) {?>
                                    <button type="submit" class="btn btn-primary padrl30"><?php echo __("Save");?>
</button>
                                <?php }?>
                            </div>
                        </div>

                        <!-- success -->
                        <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                        <!-- success -->

                        <!-- error -->
                        <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                        <!-- error -->
                    </form>
                </div>
            </div>
            <div class="edit_box x-hidden" style="position: relative">
                <div class="" style="position: absolute; top: 0; right: 0;"><a href="#" class="close_box_edit"><i style="font-size: 20px" class="fas fa-window-close"></i></a></div>
                <div class="" style="border: 1px solid #b1b1b1; padding: 5px; margin-bottom: 20px">
                    <div><strong><?php echo __("Edit class level");?>
: <span class="span_class_level_name"></span></strong></div>
                    <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_class_level.php">
                        <input type="hidden" name="school_username" id="school_username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                        <input type="hidden" name="class_level_id" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['class_level_id'];?>
"/>
                        <input type="hidden" name="do" value="edit"/>
                        <div class="form-group">
                            <label class="col-sm-3 control-label text-left">
                                <?php echo __("Class level name");?>
 (*)
                            </label>
                            <div class="col-sm-9">
                                <input class="form-control" name="class_level_name" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['class_level_name'];?>
" required maxlength="45">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label text-left">
                                <?php echo __("Description");?>

                            </label>
                            <div class="col-sm-9">
                                <textarea class="form-control" name="description" rows="3" maxlength="300"><?php echo $_smarty_tpl->tpl_vars['data']->value['description'];?>
</textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-5 col-sm-offset-3">
                                <?php if ($_smarty_tpl->tpl_vars['canEdit']->value) {?>
                                    <button type="submit" class="btn btn-primary padrl30"><?php echo __("Save");?>
</button>
                                <?php }?>
                            </div>
                        </div>

                        <!-- success -->
                        <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                        <!-- success -->

                        <!-- error -->
                        <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                        <!-- error -->
                    </form>
                </div>
            </div>
            <div><strong><?php echo __("Class level list");?>
&nbsp;(<?php echo count($_smarty_tpl->tpl_vars['rows']->value);?>
)</strong></div>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th><?php echo __("Class level name");?>
</th>
                            <th><?php echo __("Total");?>
</th>
                            <th><?php echo __("Description");?>
</th>
                            <th><?php echo __("Actions");?>
</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['rows']->value, 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
                            <tr>
                                <td class="align-middle"><?php echo $_smarty_tpl->tpl_vars['row']->value['class_level_name'];?>
</td>
                                <td  class="align-middle" align="center"><?php echo $_smarty_tpl->tpl_vars['row']->value['cnt'];?>
 <?php echo __("Class");?>
</td>
                                <td class="align-middle"><?php echo $_smarty_tpl->tpl_vars['row']->value['description'];?>
</td>
                                <td  class="align-middle" align="center" style="vertical-align: middle">
                                    <?php if ($_smarty_tpl->tpl_vars['canEdit']->value) {?>
                                        <a href="#" class="btn btn-xs btn-default js_class-level-edit" data-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['class_level_id'];?>
" data-name="<?php echo $_smarty_tpl->tpl_vars['row']->value['class_level_name'];?>
" data-description="<?php echo $_smarty_tpl->tpl_vars['row']->value['description'];?>
"><?php echo __("Edit");?>
</a>
                                        <?php if (!$_smarty_tpl->tpl_vars['row']->value['cnt']) {?>
                                            <button class="btn btn-xs btn-danger js_school-delete" data-handle="class_level" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['class_level_id'];?>
"><?php echo __("Delete");?>
</button>
                                        <?php }?>
                                    <?php }?>
                                </td>
                            </tr>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                    </tbody>
                </table>
            </div>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "edit") {?>
        <div class="panel-body">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_class_level.php">
                <input type="hidden" name="school_username" id="school_username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                <input type="hidden" name="class_level_id" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['class_level_id'];?>
"/>
                <input type="hidden" name="do" value="edit"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">
                        <?php echo __("Class level name");?>
 (*)
                    </label>
                    <div class="col-sm-9">
                        <input class="form-control" name="class_level_name" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['class_level_name'];?>
" required autofocus maxlength="45">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">
                        <?php echo __("Description");?>

                    </label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="description" rows="3" maxlength="300"><?php echo $_smarty_tpl->tpl_vars['data']->value['description'];?>
</textarea>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-5 col-sm-offset-3">
                        <?php if ($_smarty_tpl->tpl_vars['canEdit']->value) {?>
                            <button type="submit" class="btn btn-primary padrl30"><?php echo __("Save");?>
</button>
                        <?php }?>
                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/classlevels" class="btn btn-default"><?php echo __("Lists");?>
</a>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
        <div class="panel-body">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_class_level.php">
                <input type="hidden" name="school_username" id="school_username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                <input type="hidden" name="do" value="add"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">
                        <?php echo __("Class level name");?>

                    </label>
                    <div class="col-sm-9">
                        <input class="form-control" name="class_level_name" required autofocus maxlength="45">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">
                        <?php echo __("Description");?>

                    </label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="description" rows="3" maxlength="300"></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <?php if ($_smarty_tpl->tpl_vars['canEdit']->value) {?>
                            <button type="submit" class="btn btn-primary padrl30"><?php echo __("Save");?>
</button>
                        <?php }?>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    <?php }?>
</div><?php }
}
