<?php
/* Smarty version 3.1.31, created on 2021-03-31 10:55:00
  from "D:\workplace\Server11\content\themes\coniu\templates\__feeds_comment.text.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_6063f29485c927_90291680',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '1703442a81c8c9831d137a0e4396b61f4a6b2fa0' => 
    array (
      0 => 'D:\\workplace\\Server11\\content\\themes\\coniu\\templates\\__feeds_comment.text.tpl',
      1 => 1552404706,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6063f29485c927_90291680 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="comment-replace">
    <div class="comment-text js_readmore" dir="auto"><?php echo $_smarty_tpl->tpl_vars['_comment']->value['text'];?>
</div>
    <div class="comment-text-plain hidden"><?php echo $_smarty_tpl->tpl_vars['_comment']->value['text_plain'];?>
</div>
    <?php if ($_smarty_tpl->tpl_vars['_comment']->value['image'] != '') {?>
        <span class="text-link js_lightbox-nodata" data-image="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_uploads'];?>
/<?php echo $_smarty_tpl->tpl_vars['_comment']->value['image'];?>
">
            <img alt="" class="img-responsive" src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_uploads'];?>
/<?php echo $_smarty_tpl->tpl_vars['_comment']->value['image'];?>
">
        </span>
    <?php }?>
</div>
<?php }
}
