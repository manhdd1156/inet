<?php
/* Smarty version 3.1.31, created on 2021-05-13 09:17:09
  from "D:\workplace\mascom-edu-server\Server11\content\themes\coniu\templates\ci\school\ajax.school.children.list.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_609c8c25eca1a4_97779198',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '89aca10af61b25efb7dce12045ac880c47ff501c' => 
    array (
      0 => 'D:\\workplace\\mascom-edu-server\\Server11\\content\\themes\\coniu\\templates\\ci\\school\\ajax.school.children.list.tpl',
      1 => 1619677394,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_609c8c25eca1a4_97779198 (Smarty_Internal_Template $_smarty_tpl) {
?>
<strong><?php echo __("Children list");?>
&nbsp;(<?php echo $_smarty_tpl->tpl_vars['result']->value['total'];?>
 <?php echo __("Children");?>
)</strong>
<div class="pull-right flip mb10">
    <a href="#" id="export2excel" class="btn btn-success js_school-export-children" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"><?php echo __("Export to Excel");?>
</a>
    <label id="export_processing" class="btn btn-info x-hidden"><?php echo __("Loading");?>
...</label>
</div>
<table class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th><?php echo __("#");?>
</th>
            <th><?php echo __("Full name");?>
</th>
            <th><?php echo __("Gender");?>
</th>
            <th><?php echo __("Birthdate");?>
</th>
            <th><?php echo __("Parent phone");?>
</th>
            <th><?php echo __("Parent");?>
</th>
            <th><?php echo __("Actions");?>
</th>
        </tr>
    </thead>
    <tbody>
    <?php $_smarty_tpl->_assignInScope('classId', -1);
?>
    <?php $_smarty_tpl->_assignInScope('idx', ($_smarty_tpl->tpl_vars['result']->value['page']-1)*@constant('PAGING_LIMIT')+1);
?>
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['result']->value['children'], 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
        <?php if ((!isset($_smarty_tpl->tpl_vars['result']->value['class_id']) || ($_smarty_tpl->tpl_vars['result']->value['class_id'] == '')) && ($_smarty_tpl->tpl_vars['classId']->value != $_smarty_tpl->tpl_vars['row']->value['class_id'])) {?>
            <tr>
                <td colspan="7">
                    <?php if ($_smarty_tpl->tpl_vars['row']->value['class_id'] > 0) {?>
                        <?php echo __("Class");?>
:&nbsp;<?php echo $_smarty_tpl->tpl_vars['row']->value['group_title'];?>

                    <?php } else { ?>
                        <?php echo __("No class");?>

                    <?php }?>
                </td>
            </tr>
        <?php }?>
        <tr>
            <td class="align-middle" align="center"><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</td>
            <td class="align-middle"><a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/children/detail/<?php echo $_smarty_tpl->tpl_vars['row']->value['child_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['row']->value['child_name'];?>
</a></td>
            <td class="align-middle" align="center"><?php if ($_smarty_tpl->tpl_vars['row']->value['gender'] == @constant('MALE')) {
echo __("Male");
} else {
echo __("Female");
}?></td>
            <td class="align-middle"><?php echo $_smarty_tpl->tpl_vars['row']->value['birthday'];?>
</td>
            <td class="align-middle">
                <?php if ($_smarty_tpl->tpl_vars['row']->value['parent_phone'] != '') {?>
                    <?php echo $_smarty_tpl->tpl_vars['row']->value['parent_phone'];?>

                <?php } else { ?>
                    <?php echo $_smarty_tpl->tpl_vars['row']->value['parent_phone_dad'];?>

                <?php }?>
            </td>
            <td class="align-middle">
                <?php if (count($_smarty_tpl->tpl_vars['row']->value['parent']) == 0) {?>
                    <?php echo __("No parent");?>

                <?php } else { ?>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['row']->value['parent'], '_user');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['_user']->value) {
?>
                        <span class="name js_user-popover" data-uid="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_id'];?>
">
                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_name'];?>
"><?php echo $_smarty_tpl->tpl_vars['_user']->value['user_fullname'];?>
</a>
                        </span>
                        <?php if ($_smarty_tpl->tpl_vars['_user']->value['user_id'] != $_smarty_tpl->tpl_vars['user']->value->_data['user_id']) {?>
                            <a href="#" class="far fa-comments fa-lg js_chat-start" data-name="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_fullname'];?>
" data-uid="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_id'];?>
"></a>
                        <?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['_user']->value['suggest'] == 1 && $_smarty_tpl->tpl_vars['canEdit']->value) {?>
                            <button class="btn btn-xs btn-info js_school-approve-parent" id="button_<?php echo $_smarty_tpl->tpl_vars['row']->value['child_id'];?>
_<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_id'];?>
" data-parent="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_id'];?>
" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['child_id'];?>
"><?php echo __("Approve");?>
</button>
                        <?php }?>
                        <br/>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                <?php }?>
            </td>

            <td class="align-middle action_col" align="left">
                <?php if ($_smarty_tpl->tpl_vars['canEdit']->value) {?>
                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/children/edit/<?php echo $_smarty_tpl->tpl_vars['row']->value['child_id'];?>
" class="btn btn-xs btn-default edit_width"><?php echo __("Edit");?>
</a>
                    <?php if ($_smarty_tpl->tpl_vars['school']->value['school_step'] == SCHOOL_STEP_FINISH || $_smarty_tpl->tpl_vars['school']->value['grade'] != 0) {?>
                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/children/moveclass/<?php echo $_smarty_tpl->tpl_vars['row']->value['child_id'];?>
" class="btn btn-xs btn-default moveclass_width"><?php echo __("Move class");?>
</a>
                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/children/leave/<?php echo $_smarty_tpl->tpl_vars['row']->value['child_id'];?>
" class="btn btn-xs btn-warning"><?php echo __("Leave school");?>
</a>
                    <?php }?>
                    <?php if (count($_smarty_tpl->tpl_vars['row']->value['parent']) == 0) {?>
                        <button class="btn btn-xs btn-danger js_school-delete" data-handle="child" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['child_id'];?>
"><?php echo __("Delete");?>
</button>
                    <?php }?>
                <?php }?>
            </td>
        </tr>
        <?php $_smarty_tpl->_assignInScope('classId', $_smarty_tpl->tpl_vars['row']->value['class_id']);
?>
        <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>


    <?php if (count($_smarty_tpl->tpl_vars['result']->value['children']) == 0) {?>
        <tr class="odd">
            <td valign="top" align="center" colspan="7" class="dataTables_empty">
                <?php echo __("No data available in table");?>

            </td>
        </tr>
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['result']->value['page_count'] > 1) {?>
        <tr>
            <td colspan="7">
                <div class="pull-right flip">
                    <ul class="pagination">
                        <?php
$_smarty_tpl->tpl_vars['idx'] = new Smarty_Variable(null, $_smarty_tpl->isRenderingCache);$_smarty_tpl->tpl_vars['idx']->step = 1;$_smarty_tpl->tpl_vars['idx']->total = (int) ceil(($_smarty_tpl->tpl_vars['idx']->step > 0 ? $_smarty_tpl->tpl_vars['result']->value['page_count']+1 - (1) : 1-($_smarty_tpl->tpl_vars['result']->value['page_count'])+1)/abs($_smarty_tpl->tpl_vars['idx']->step));
if ($_smarty_tpl->tpl_vars['idx']->total > 0) {
for ($_smarty_tpl->tpl_vars['idx']->value = 1, $_smarty_tpl->tpl_vars['idx']->iteration = 1;$_smarty_tpl->tpl_vars['idx']->iteration <= $_smarty_tpl->tpl_vars['idx']->total;$_smarty_tpl->tpl_vars['idx']->value += $_smarty_tpl->tpl_vars['idx']->step, $_smarty_tpl->tpl_vars['idx']->iteration++) {
$_smarty_tpl->tpl_vars['idx']->first = $_smarty_tpl->tpl_vars['idx']->iteration == 1;$_smarty_tpl->tpl_vars['idx']->last = $_smarty_tpl->tpl_vars['idx']->iteration == $_smarty_tpl->tpl_vars['idx']->total;?>
                            <?php if ($_smarty_tpl->tpl_vars['idx']->value == $_smarty_tpl->tpl_vars['result']->value['page']) {?>
                                <li class="active"><a href="#"><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</a></li>
                            <?php } else { ?>
                                <li>
                                    <a href="#" class="js_child-search" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['school']->value['page_id'];?>
" data-page="<?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</a>
                                </li>
                            <?php }?>
                        <?php }
}
?>

                    </ul>
                </div>
            </td>
        </tr>
    <?php }?>
    </tbody>
</table>
<?php }
}
