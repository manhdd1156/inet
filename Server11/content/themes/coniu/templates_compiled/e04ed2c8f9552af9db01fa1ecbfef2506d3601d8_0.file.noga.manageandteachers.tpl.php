<?php
/* Smarty version 3.1.31, created on 2021-03-30 15:57:04
  from "D:\workplace\Server11\content\themes\coniu\templates\ci\noga\noga.manageandteachers.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_6062e7e01f4558_94717060',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e04ed2c8f9552af9db01fa1ecbfef2506d3601d8' => 
    array (
      0 => 'D:\\workplace\\Server11\\content\\themes\\coniu\\templates\\ci\\noga\\noga.manageandteachers.tpl',
      1 => 1552404704,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6062e7e01f4558_94717060 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <div class="pull-right flip">
            <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
            <?php }?>
        </div>
        <i class="fa fa-user fa-fw fa-lg pr10"></i>
        <?php echo __("Manage and teacher");?>

        <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
            &rsaquo; <?php echo __("Search");?>

        <?php }?>
    </div>
    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
        <div class="panel-body with-table form-horizontal">
            <div class="row">
                <div class='col-sm-3'>
                    <select name="school_role" id="school_role" class="form-control">
                        <option value="1"><?php echo __("Manage");?>
</option>
                        <option value="2"><?php echo __("Teacher");?>
</option>
                    </select>
                </div>
                <div class='col-sm-6'>
                    <div class="form-group">
                        <select name="school_status" id="school_status_no_js" class="form-control">
                            <option value="0"><?php echo __("Select satuts");?>
...</option>
                            <option value="1"><?php echo __("Using Mascom - Edu");?>
</option>
                            <option value="2"><?php echo __("Having Mascom - Edu's page");?>
</option>
                            <option value="3"><?php echo __("Waiting confirmation");?>
</option>
                        </select>
                    </div>
                </div>
                <div class='col-sm-3'>
                    <div class="form-group">
                        <a href="#" id="search" class="btn btn-default js_noga-role-search"><?php echo __("Search");?>
</a>
                        <label id="loading" class="btn btn-info x-hidden"><?php echo __("Loading");?>
...</label>
                    </div>
                </div>
            </div>
            <div class="table-responsive pt10" id="role_list">
                
            </div>
        </div>
    <?php }?>
</div><?php }
}
