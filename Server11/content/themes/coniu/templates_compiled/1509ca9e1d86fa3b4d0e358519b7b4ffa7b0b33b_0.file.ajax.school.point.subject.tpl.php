<?php
/* Smarty version 3.1.31, created on 2021-04-27 09:06:41
  from "D:\workplace\mascom-edu-server\Server11\content\themes\coniu\templates\ci\school\ajax.school.point.subject.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_608771b1a46462_85343783',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '1509ca9e1d86fa3b4d0e358519b7b4ffa7b0b33b' => 
    array (
      0 => 'D:\\workplace\\mascom-edu-server\\Server11\\content\\themes\\coniu\\templates\\ci\\school\\ajax.school.point.subject.tpl',
      1 => 1619081216,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_608771b1a46462_85343783 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_assignInScope('idx', 1);
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['results']->value, 'subject');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['subject']->value) {
?>
    <div class="form-group">
        <div class="col-sm-3">



            <strong><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
. <?php echo $_smarty_tpl->tpl_vars['subject']->value['subject_name'];?>
</strong>
            <input type="hidden" name="subject_ids[]" value="<?php echo $_smarty_tpl->tpl_vars['subject']->value['subject_id'];?>
">
        </div>
        <div class="col-sm-3">
            <select name="teacher_ids[]" class="form-control">
                <option value="0"><?php echo __("Select teacher");?>
</option>
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['teachers']->value, 'teacher');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['teacher']->value) {
?>
                    <option value="<?php echo $_smarty_tpl->tpl_vars['teacher']->value['user_id'];?>
" <?php if ($_smarty_tpl->tpl_vars['subject']->value['teacher_id'] == $_smarty_tpl->tpl_vars['teacher']->value['user_id']) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['teacher']->value['user_fullname'];?>
</option>
                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

            </select>
        </div>
    </div>

    <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
}
}
