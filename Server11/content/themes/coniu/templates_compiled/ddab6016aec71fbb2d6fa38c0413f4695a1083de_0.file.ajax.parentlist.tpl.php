<?php
/* Smarty version 3.1.31, created on 2021-03-31 10:43:17
  from "D:\workplace\Server11\content\themes\coniu\templates\ci\ajax.parentlist.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_6063efd52f72b8_18465192',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ddab6016aec71fbb2d6fa38c0413f4695a1083de' => 
    array (
      0 => 'D:\\workplace\\Server11\\content\\themes\\coniu\\templates\\ci\\ajax.parentlist.tpl',
      1 => 1552404703,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6063efd52f72b8_18465192 (Smarty_Internal_Template $_smarty_tpl) {
?>
<ul>
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['results']->value, '_user');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['_user']->value) {
?>
        <li class="feeds-item" data-id="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_name'];?>
">
            <input type="hidden" name="user_id[]" value="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_id'];?>
"/>
            <div class="data-container small">
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_name'];?>
">
                    <img class="data-avatar" src="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_picture'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_fullname'];?>
">
                </a>
                <div class="data-content">
                    <div class="pull-right flip">
                        <div class="btn btn-default js_parent-set-admin <?php if ($_smarty_tpl->tpl_vars['_user']->value['user_id'] == $_smarty_tpl->tpl_vars['child_admin']->value) {?>x-hidden<?php }?>" data-uid="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_id'];?>
"><?php echo __("Set admin");?>
</div> <div class="btn btn-default js_parent-remove <?php if ($_smarty_tpl->tpl_vars['_user']->value['user_id'] == $_smarty_tpl->tpl_vars['child_admin']->value) {?>x-hidden<?php }?>" data-uid="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_id'];?>
"><?php echo __("Remove");?>
</div>
                    </div>
                    <div>
                        <span class="name js_user-popover" data-uid="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_id'];?>
">
                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_name'];?>
"><?php echo $_smarty_tpl->tpl_vars['_user']->value['user_fullname'];?>
</a>
                        </span><br/>
                        <?php echo $_smarty_tpl->tpl_vars['_user']->value['user_phone'];?>
&nbsp;&nbsp;&nbsp;<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_email'];?>

                    </div>
                </div>
            </div>
        </li>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

</ul><?php }
}
