<?php
/* Smarty version 3.1.31, created on 2021-05-08 16:19:15
  from "D:\workplace\mascom-edu-server\Server11\content\themes\coniu\templates\ci\school\ajax.school.feedbacklist.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_60965793a4be09_73029989',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '841f881c6557a4b7a8b2c536d80ea603ba6d229c' => 
    array (
      0 => 'D:\\workplace\\mascom-edu-server\\Server11\\content\\themes\\coniu\\templates\\ci\\school\\ajax.school.feedbacklist.tpl',
      1 => 1620098619,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60965793a4be09_73029989 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div><strong><?php echo __("Feedback list");?>
&nbsp;(<span class="count_feedback"><?php echo count($_smarty_tpl->tpl_vars['rows']->value);?>
</span>)</strong></div>
<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover">
        <thead>
        <tr>
            <th>#</th>
            <th><?php echo __("Title");?>
</th>
            <th><?php echo __("Time");?>
</th>
            <th><?php echo __("Sender's name");?>
</th>
            <th><?php echo __("Parent of student");?>
</th>
            <th><?php echo __("Email");?>
</th>
            <th><?php echo __("Class");?>
</th>
            <th><?php echo __("Actions");?>
</th>
        </tr>
        </thead>
        <tbody>
        <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['rows']->value, 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
            <?php if ($_smarty_tpl->tpl_vars['idx']->value > 1) {?>
                <tr><td colspan="8"></td></tr>
            <?php }?>
            <tr class="list_feedback_<?php echo $_smarty_tpl->tpl_vars['row']->value['feedback_id'];?>
">
                <td rowspan="2" align="center" style="vertical-align:middle"><strong><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</td>
                <td>
                    <?php if ($_smarty_tpl->tpl_vars['row']->value['level'] == @constant('SCHOOL_LEVEL')) {?>
                        <?php echo __("Feedback for the school");?>

                    <?php } elseif ($_smarty_tpl->tpl_vars['row']->value['level'] == @constant('CLASS_LEVEL')) {?>
                        <?php echo __("Feedback for class");?>

                    <?php }?>
                </td>
                <td align="center" style="vertical-align:middle"><?php echo $_smarty_tpl->tpl_vars['row']->value['created_at'];?>
</td>
                <?php if ($_smarty_tpl->tpl_vars['row']->value['is_incognito']) {?>
                    <td colspan="3" align="center" style="vertical-align:middle"><?php echo __("Incognito");?>
</td>
                <?php } else { ?>
                    <td align="center" style="vertical-align:middle"> <?php echo $_smarty_tpl->tpl_vars['row']->value['name'];?>

                    </td>
                    <td align="center" style="vertical-align:middle"><?php echo $_smarty_tpl->tpl_vars['row']->value['child_name'];?>
</td>
                    <td align="center" style="vertical-align:middle"><?php echo $_smarty_tpl->tpl_vars['row']->value['email'];?>
</td>
                <?php }?>
                <td align="center" style="vertical-align:middle">
                    <?php if (!is_empty($_smarty_tpl->tpl_vars['row']->value['group_title'])) {?>
                        <?php echo $_smarty_tpl->tpl_vars['row']->value['group_title'];?>

                    <?php } else { ?>
                        -
                    <?php }?>

                </td>
                <td align="center" style="vertical-align:middle" rowspan="2">
                    <?php if ($_smarty_tpl->tpl_vars['canEdit']->value) {?>
                        <?php if (!$_smarty_tpl->tpl_vars['row']->value['confirm']) {?>
                            <?php if ($_smarty_tpl->tpl_vars['row']->value['level'] == @constant('SCHOOL_LEVEL')) {?>
                                <button class="btn btn-xs btn-primary js_school-feedback" data-handle="confirm" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"
                                        data-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['feedback_id'];?>
"><?php echo __("Confirm");?>
</button>
                            <?php }?>
                        <?php }?>
                        <div class="feddback_confirmed_<?php echo $_smarty_tpl->tpl_vars['row']->value['feedback_id'];?>
 <?php if (!$_smarty_tpl->tpl_vars['row']->value['confirm']) {?>x-hidden<?php }?>">
                            <strong><?php echo __("Confirmed");?>
</strong>
                            <?php if ($_smarty_tpl->tpl_vars['row']->value['level'] == @constant('SCHOOL_LEVEL')) {?>
                                <button class="btn btn-xs btn-danger js_school-feedback" data-handle="delete" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"
                                        data-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['feedback_id'];?>
"><?php echo __("Delete");?>
</button>
                            <?php }?>
                        </div>
                    <?php }?>
                </td>
            </tr>
            <tr class="list_feedback_<?php echo $_smarty_tpl->tpl_vars['row']->value['feedback_id'];?>
">
                <td colspan="2" align="center" style="vertical-align:middle"><strong><?php echo __("Content");?>
</strong></td>
                <td colspan="4" style="vertical-align:middle"> <?php echo $_smarty_tpl->tpl_vars['row']->value['content'];?>
</td>
            </tr>
            <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>


        <?php if (count($_smarty_tpl->tpl_vars['rows']->value) == 0) {?>
            <tr class="odd">
                <td valign="top" align="center" colspan="8" class="dataTables_empty">
                    <?php echo __("No data available in table");?>

                </td>
            </tr>
        <?php }?>
        </tbody>
    </table>
</div><?php }
}
