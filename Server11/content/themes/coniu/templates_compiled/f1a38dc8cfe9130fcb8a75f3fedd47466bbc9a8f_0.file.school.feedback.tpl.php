<?php
/* Smarty version 3.1.31, created on 2019-06-24 11:14:21
  from "D:\Working\Coniu\ServerSvn\trunk\Server11\content\themes\coniu\templates\ci\school\school.feedback.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5d104e1d6f58d1_00887936',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'f1a38dc8cfe9130fcb8a75f3fedd47466bbc9a8f' => 
    array (
      0 => 'D:\\Working\\Coniu\\ServerSvn\\trunk\\Server11\\content\\themes\\coniu\\templates\\ci\\school\\school.feedback.tpl',
      1 => 1552404702,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:ci/school/ajax.school.feedbacklist.tpl' => 1,
  ),
),false)) {
function content_5d104e1d6f58d1_00887936 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <i class="fa fa-envelope fa-fw fa-lg pr10"></i>
        <?php echo __("Parent feedback");?>
 › <?php echo __("Lists");?>

    </div>
    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
        <div class="panel-body with-table">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_feedback.php">
                <input type="hidden" name="school_username" id="school_username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                <input type="hidden" name="do" value="search"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Select class");?>
</label>
                    <div class="col-sm-9">
                        <div class="col-sm-4">
                            <select name="class_id" id="class_id" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" class="form-control">
                                <option value="0"> <?php echo __('Select class');?>
 </option>
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['classes']->value, 'class');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class']->value) {
?>
                                    <option value="<?php echo $_smarty_tpl->tpl_vars['class']->value['group_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['class']->value['group_title'];?>
</option>
                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                            </select>
                        </div>
                        <div class="col-sm-3">
                            <a class="btn btn-default js_school-feedback-search" data-username = "<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-handle = "search"><?php echo __("Search");?>
</a>
                        </div>
                    </div>
                </div>
            </form>
            <div id="feedback_list">
                <?php $_smarty_tpl->_subTemplateRender("file:ci/school/ajax.school.feedbacklist.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

            </div>
        </div>
    <?php }?>
</div><?php }
}
