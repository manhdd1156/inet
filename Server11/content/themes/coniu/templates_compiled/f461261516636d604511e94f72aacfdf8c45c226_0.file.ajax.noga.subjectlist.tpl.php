<?php
/* Smarty version 3.1.31, created on 2021-04-22 14:13:50
  from "D:\workplace\Server11\content\themes\coniu\templates\ci\noga\ajax.noga.subjectlist.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_6081222e94a088_99065496',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'f461261516636d604511e94f72aacfdf8c45c226' => 
    array (
      0 => 'D:\\workplace\\Server11\\content\\themes\\coniu\\templates\\ci\\noga\\ajax.noga.subjectlist.tpl',
      1 => 1619075591,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6081222e94a088_99065496 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover js_dataTable">
        <thead>
        <tr><th><?php echo __("Subject list");?>
&nbsp;(<?php echo count($_smarty_tpl->tpl_vars['rows']->value);?>
)</th></tr>
        </thead>
        <tbody>
        <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['rows']->value, 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
            <tr>
                <td>
                    <div>
                        <?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
 - <?php echo $_smarty_tpl->tpl_vars['row']->value['subject_name'];?>

                         | <?php if ($_smarty_tpl->tpl_vars['row']->value['re_exam'] == 1) {
echo __("re-exam");
} else {
echo __("not re-exam");
}?>
                    </div>
                    <div class="pull-right flip">
                        <form class="js_ajax-forms form-horizontal" action="#" enctype="multipart/form-data" id="subject_list_form" method="post">

                            <input type="hidden" name="do" value="delete"/>
                            <input type="hidden" name="subject_id" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['subject_id'];?>
"/>

                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/noga/subjects/edit/<?php echo $_smarty_tpl->tpl_vars['row']->value['subject_id'];?>
" class="btn btn-xs btn-default"><?php echo __("Edit");?>
</a>
                            <a href="#"  class="delete_subject btn btn-xs btn-default"><?php echo __("Delete");?>
</a>

                        </form>
                    </div>
                </td>
            </tr>
            <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

        </tbody>
    </table>
    <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
    <div class="alert alert-warning mb0 mt10 x-hidden" role="alert"></div>
</div>

<?php echo '<script'; ?>
 type="text/javascript">
    $(function() {
        // run DataTable
        $('.js_dataTable').DataTable({
            "aoColumnDefs": [ { 'aDataSort': false, 'aTargets': [ -1 ] } ],
            "language": {
                "decimal":        "",
                "emptyTable":     __["No data available in table"],
                "info":           __["Showing _START_ to _END_ of _TOTAL_ results"],
                "infoEmpty":      __["Showing 0 to 0 of 0 results"],
                "infoFiltered":   "(filtered from _MAX_ total entries)",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     __["Show"] + " " + "_MENU_" + " " + __["results"],
                "loadingRecords": __["Loading..."],
                "processing":     __["Processing..."],
                "search":         __["Search"],
                "zeroRecords":    __["No matching records found"],
                "paginate": {
                    "first":      __["First"],
                    "last":       __["Last"],
                    "next":       __["Next"],
                    "previous":   __["Previous"]
                },
                "aria": {
                    "sortAscending":  ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                }
            }
        });
    });
<?php echo '</script'; ?>
><?php }
}
