<?php
/* Smarty version 3.1.31, created on 2021-04-23 11:22:06
  from "D:\workplace\mascom-edu-server\Server11\content\themes\coniu\templates\_header.messages.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_60824b6e3d9983_80240122',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c9f9832053380ebec334c2f4a3f3cbb77b45aa88' => 
    array (
      0 => 'D:\\workplace\\mascom-edu-server\\Server11\\content\\themes\\coniu\\templates\\_header.messages.tpl',
      1 => 1552404706,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60824b6e3d9983_80240122 (Smarty_Internal_Template $_smarty_tpl) {
?>
<li class="dropdown js_live-messages">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <i class="fa fa-comments fa-lg"></i>
        
        
        <span class="label hidden">

        </span>
    </a>
    <div class="dropdown-menu dropdown-widget with-arrow">
        <div class="dropdown-widget-header">
            <?php echo __("Messages");?>

            <a class="pull-right flip text-link js_chat-start" href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/messages/new"><?php echo __("Send a New Message");?>
</a>
        </div>
        <div class="dropdown-widget-body">
            <div class="js_scroller">
                
                
                <ul id="item_chat_header">
                    
                    
                    
                </ul>
                
                
                    
                
                
            </div>
        </div>
        
    </div>
</li><?php }
}
