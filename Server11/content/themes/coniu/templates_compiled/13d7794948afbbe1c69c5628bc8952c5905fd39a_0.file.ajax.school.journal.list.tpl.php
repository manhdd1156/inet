<?php
/* Smarty version 3.1.31, created on 2021-03-31 10:41:30
  from "D:\workplace\Server11\content\themes\coniu\templates\ci\ajax.school.journal.list.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_6063ef6a9bf648_25228819',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '13d7794948afbbe1c69c5628bc8952c5905fd39a' => 
    array (
      0 => 'D:\\workplace\\Server11\\content\\themes\\coniu\\templates\\ci\\ajax.school.journal.list.tpl',
      1 => 1552404704,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:__feeds_photo_child_school.tpl' => 1,
  ),
),false)) {
function content_6063ef6a9bf648_25228819 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="panel panel-default panel-photos">
    <div class="panel-body">
        <?php if (count($_smarty_tpl->tpl_vars['results']->value) > 0) {?>
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['results']->value, 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
                <div class="row" style="margin-bottom: 20px">
                    <div class="journal_album">
                        <div class="col-sm-12 mb5">
                            <div class="diary_date"><i class="far fa-calendar-check" aria-hidden="true"></i> <strong><?php echo $_smarty_tpl->tpl_vars['row']->value['created_at'];?>
 (<?php echo count($_smarty_tpl->tpl_vars['row']->value['pictures']);?>
 <?php echo __("photo");?>
)</strong> - <span class="caption_show"><?php echo $_smarty_tpl->tpl_vars['row']->value['caption'];?>
</span></div>
                            <div class="caption_edit x-hidden">
                                <textarea type="text" class="caption_edit x-hidden caption_<?php echo $_smarty_tpl->tpl_vars['row']->value['child_journal_album_id'];?>
 js_school-journal-edit-caption_change" name="caption" data-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['child_journal_album_id'];?>
" data-child="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-handle="edit_caption"><?php echo $_smarty_tpl->tpl_vars['row']->value['caption'];?>
</textarea>
                                <a href="#" class="js_school-journal-edit-caption" data-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['child_journal_album_id'];?>
" data-child="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-handle="edit_caption"><i class="fa fa-save" aria-hidden="true"></i> </a>
                            </div>
                            <div class="pull-right flip">
                                <button type="button" class="btn btn-default btn-xs" data-toggle="modal" data-url="ci/bo/modal.php?do=add_photos_school&id=<?php echo $_smarty_tpl->tpl_vars['row']->value['child_journal_album_id'];?>
&username=<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
&childid=<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
&module=<?php echo $_smarty_tpl->tpl_vars['is_module']->value;?>
">
                                    <i class="fa fa-plus-circle"></i> <?php echo __("Add Photos");?>

                                </button>
                                <a class="btn btn-xs btn-default edit_caption"><?php echo __("Edit caption");?>
</a>
                                <a class="btn btn-xs btn-default show_or_hidden_diary" data-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['child_journal_album_id'];?>
"><?php echo __("Show/hide");?>
</a>
                                <a class="btn btn-xs btn-danger js_school-journal-delete-album" data-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['child_journal_album_id'];?>
" data-child="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-handle="delete_album"><?php echo __("Delete Album");?>
</a>
                            </div>
                        </div>
                        <div class="album_images_<?php echo $_smarty_tpl->tpl_vars['row']->value['child_journal_album_id'];?>
">
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['row']->value['pictures'], 'photo');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['photo']->value) {
?>
                                <?php $_smarty_tpl->_subTemplateRender('file:__feeds_photo_child_school.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('_context'=>"photos",'_small'=>true), 0, true);
?>

                                <?php $_smarty_tpl->_assignInScope('day', $_smarty_tpl->tpl_vars['photo']->value['created_at']);
?>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </div>
                    </div>
                </div>
            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

        <?php } else { ?>
            <div class="" style = "font-weight: bold; text-align: center">
                <?php echo __("No data");?>

            </div>
        <?php }?>
    </div>
</div>
<?php }
}
