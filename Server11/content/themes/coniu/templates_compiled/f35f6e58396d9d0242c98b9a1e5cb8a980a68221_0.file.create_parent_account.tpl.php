<?php
/* Smarty version 3.1.31, created on 2021-03-31 09:52:57
  from "D:\workplace\Server11\content\themes\coniu\templates\ci\email_templates\create_parent_account.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_6063e4090bb1b1_17787526',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'f35f6e58396d9d0242c98b9a1e5cb8a980a68221' => 
    array (
      0 => 'D:\\workplace\\Server11\\content\\themes\\coniu\\templates\\ci\\email_templates\\create_parent_account.tpl',
      1 => 1552404704,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6063e4090bb1b1_17787526 (Smarty_Internal_Template $_smarty_tpl) {
?>
Kính gửi ông/bà <?php echo $_smarty_tpl->tpl_vars['full_name']->value;?>
,
Phụ huynh bé <?php echo $_smarty_tpl->tpl_vars['child_name']->value;?>


Nhằm tạo điều kiện thuận lợi cho phụ huynh theo dõi thông tin của bé, nhà trường đã đưa vào sử dụng "Ứng dụng tương tác mầm non - Coniu" làm công cụ quản lý và là kênh tương tác/trao đổi giữa nhà trường, giáo viên và phụ huynh.
Hiện tại, nhà trường đã tạo sẵn tài khoản cho ông/bà là:
    - Tài khoản: <?php echo $_smarty_tpl->tpl_vars['username']->value;?>

    - Mật khẩu: <?php echo $_smarty_tpl->tpl_vars['password']->value;?>

Ông/bà có thể thay đổi thông tin tài khoản sau khi truy cập.

Để sử dụng Coniu với tài khoản được cấp, ông/bà có thể áp dụng 01 trong 02 cách sau:
    1 - Truy cập địa chỉ website Coniu tại địa chỉ: https://coniu.vn hoặc;
    2 - Tải ứng dụng 'Coniu' về điện thoại (chỉ áp dụng với điện thoại iPhone và điện thoại sử dụng hệ điều hành Android).

Trân trọng cám ơn sự hợp tác của ông/bà!
Thân ái,
Ban Giám hiệu nhà trường.

<?php }
}
