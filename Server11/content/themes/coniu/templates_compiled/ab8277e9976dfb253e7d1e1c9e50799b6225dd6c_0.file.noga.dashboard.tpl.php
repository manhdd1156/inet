<?php
/* Smarty version 3.1.31, created on 2021-03-30 15:56:44
  from "D:\workplace\Server11\content\themes\coniu\templates\ci\noga\noga.dashboard.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_6062e7ccf117b6_75010842',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ab8277e9976dfb253e7d1e1c9e50799b6225dd6c' => 
    array (
      0 => 'D:\\workplace\\Server11\\content\\themes\\coniu\\templates\\ci\\noga\\noga.dashboard.tpl',
      1 => 1552404703,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6062e7ccf117b6_75010842 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <i class="fas fa-tachometer-alt fa-fw fa-lg pr10"></i>
        <?php echo __("Dashboard");?>

    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-6">
                <div class="box-primary">
                    <div class="box-header">
                        <strong><?php echo __("School list");?>
</strong>
                    </div>
                    <div class="list-group">
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['insights']->value['schools'], 'school');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['school']->value) {
?>
                            <div class="list-group-item">
                                <?php echo $_smarty_tpl->tpl_vars['school']->value['page_title'];?>

                            </div>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                    </div>
                </div>

            </div>
        </div>
    </div>
</div><?php }
}
