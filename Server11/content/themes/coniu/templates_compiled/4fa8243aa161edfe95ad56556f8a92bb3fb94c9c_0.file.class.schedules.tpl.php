<?php
/* Smarty version 3.1.31, created on 2021-03-31 10:28:49
  from "D:\workplace\Server11\content\themes\coniu\templates\ci\class\class.schedules.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_6063ec71ea8287_70327436',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '4fa8243aa161edfe95ad56556f8a92bb3fb94c9c' => 
    array (
      0 => 'D:\\workplace\\Server11\\content\\themes\\coniu\\templates\\ci\\class\\class.schedules.tpl',
      1 => 1552404703,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6063ec71ea8287_70327436 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <i class="fas fa-calendar-alt fa-fw fa-lg pr10"></i>
        <?php echo __("Schedule");?>

        <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
            &rsaquo; <?php echo __("Schedule list");?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "detail") {?>
            &rsaquo; <?php echo __("Detail schedule");?>
 <?php echo $_smarty_tpl->tpl_vars['data']->value['schedule_name'];?>

        <?php }?>
    </div>
    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
    <div class="panel-body with-table">
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover js_dataTable">
                <thead>
                <tr><th colspan="6"><?php echo __("Schedule list");?>
&nbsp;(<?php echo count($_smarty_tpl->tpl_vars['rows']->value);?>
)</th></tr>
                <tr>
                    <th>
                        <?php echo __("#");?>

                    </th>
                    <th>
                        <?php echo __("Schedule name");?>

                    </th>
                    <th>
                        <?php echo __("Begin");?>

                    </th>
                    <th>
                        <?php echo __("Scope");?>

                    </th>
                    <th>
                        <?php echo __("Use category");?>

                    </th>
                    <th>
                        <?php echo __("Study saturday");?>

                    </th>
                </tr>
                </thead>
                <tbody>
                <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['rows']->value, 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
                    <tr>
                        <td class="align-middle" align="center">
                            <?php echo $_smarty_tpl->tpl_vars['idx']->value;?>

                        </td>
                        <td class="align-middle">
                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/schedules/detail/<?php echo $_smarty_tpl->tpl_vars['row']->value['schedule_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['row']->value['schedule_name'];?>
</a> <?php if (!$_smarty_tpl->tpl_vars['row']->value['use']) {?>(<?php echo __("Not use now");?>
)<?php }?>
                        </td>
                        <td class="align-middle" align="center">
                            <?php echo $_smarty_tpl->tpl_vars['row']->value['begin'];?>

                        </td>
                        <td class="align-middle" align="center">
                            <?php if ($_smarty_tpl->tpl_vars['row']->value['applied_for'] == @constant('SCHOOL_LEVEL')) {?>
                                <?php echo __("School");?>

                            <?php } elseif ($_smarty_tpl->tpl_vars['row']->value['applied_for'] == @constant('CLASS_LEVEL_LEVEL')) {?>
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['class_levels']->value, 'cl');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['cl']->value) {
?>
                                    <?php if ($_smarty_tpl->tpl_vars['cl']->value['class_level_id'] == $_smarty_tpl->tpl_vars['row']->value['class_level_id']) {
echo $_smarty_tpl->tpl_vars['cl']->value['class_level_name'];
}?>
                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                            <?php } elseif ($_smarty_tpl->tpl_vars['row']->value['applied_for'] == @constant('CLASS_LEVEL')) {?>
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['classes']->value, 'value');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['value']->value) {
?>
                                    <?php if ($_smarty_tpl->tpl_vars['value']->value['group_id'] == $_smarty_tpl->tpl_vars['row']->value['class_id']) {
echo $_smarty_tpl->tpl_vars['value']->value['group_title'];
}?>
                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                            <?php }?>
                        </td>
                        <td class="align-middle" align="center">
                            <?php if ($_smarty_tpl->tpl_vars['row']->value['is_category']) {?>
                                <?php echo __('Yes');?>

                            <?php } else { ?>
                                <?php echo __('No');?>

                            <?php }?>
                        </td>
                        <td class="align-middle" align="center">
                            <?php if ($_smarty_tpl->tpl_vars['row']->value['is_saturday']) {?>
                                <?php echo __('Yes');?>

                            <?php } else { ?>
                                <?php echo __('No');?>

                            <?php }?>
                        </td>
                    </tr>
                    <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>


                
                    
                        
                            
                        
                    
                
                </tbody>
            </table>
        </div>
    </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "detail") {?>
        <div class="panel-body with-table">
            <table class = "table table-bordered">
                <tbody>
                    <tr>
                        <td class = "col-sm-3 text-right"><strong><?php echo __('Schedule name');?>
</strong></td>
                        <td>
                            <?php echo $_smarty_tpl->tpl_vars['data']->value['schedule_name'];?>

                        </td>
                    </tr>
                    <tr>
                        <td class = "col-sm-3 text-right"><strong><?php echo __('Scope');?>
</strong></td>
                        <td>
                            <?php if (isset($_smarty_tpl->tpl_vars['classes']->value)) {?> <?php echo $_smarty_tpl->tpl_vars['classes']->value['group_title'];?>

                            <?php } elseif (isset($_smarty_tpl->tpl_vars['class_level']->value)) {
echo $_smarty_tpl->tpl_vars['class_level']->value['class_level_name'];?>

                            <?php } elseif (!isset($_smarty_tpl->tpl_vars['classes']->value) && !isset($_smarty_tpl->tpl_vars['class_level']->value)) {?> <?php echo __('School');?>

                            <?php }?>
                        </td>
                    </tr>
                    <tr>
                        <td class = "col-sm-3 text-right"><strong><?php echo __('Begin');?>
</strong></td>
                        <td>
                            <?php echo $_smarty_tpl->tpl_vars['data']->value['begin'];?>

                        </td>
                    </tr>
                    
                        
                        
                            
                                
                            
                                
                            
                        
                    
                    
                        
                        
                            
                                
                            
                                
                            
                        
                    
                    <?php if ($_smarty_tpl->tpl_vars['data']->value['description'] != '') {?>
                        <tr>
                            <td class = "col-sm-3 text-right"><strong><?php echo __('Description');?>
</strong></td>
                            <td>
                                <?php echo $_smarty_tpl->tpl_vars['data']->value['description'];?>

                            </td>
                        </tr>
                    <?php }?>
                </tbody>
            </table>
            <div class = "table-responsive">
                <table class = "table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th> <center><?php echo __('#');?>
</center> </th>
                        <th> <center><?php echo __('Time');?>
</center> </th>
                        <th <?php if (!$_smarty_tpl->tpl_vars['data']->value['is_category']) {?>class="hidden"<?php }?>> <center><?php echo __('Activity');?>
</center> </th>
                        <th> <center><?php echo __('Monday');?>
</center> </th>
                        <th> <center><?php echo __('Tuesday');?>
</center> </th>
                        <th> <center><?php echo __('Wednesday');?>
</center> </th>
                        <th> <center><?php echo __('Thursday');?>
</center> </th>
                        <th> <center><?php echo __('Friday');?>
</center> </th>
                        <th <?php if (!$_smarty_tpl->tpl_vars['data']->value['is_saturday']) {?>class="hidden"<?php }?>> <center><?php echo __('Saturday');?>
</center> </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['data']->value['details'], 'row', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['row']->value) {
?>
                        <?php $_smarty_tpl->_assignInScope('array', array_values($_smarty_tpl->tpl_vars['row']->value));
?>
                        <?php $_smarty_tpl->_assignInScope('temp', array());
?>
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['array']->value, 'value', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['value']->value) {
?>
                            <?php if ($_smarty_tpl->tpl_vars['data']->value['is_saturday']) {?>
                                <?php if (($_smarty_tpl->tpl_vars['k']->value >= 4) && ($_smarty_tpl->tpl_vars['k']->value < (count($_smarty_tpl->tpl_vars['array']->value)))) {?>
                                    <?php $_tmp_array = isset($_smarty_tpl->tpl_vars['temp']) ? $_smarty_tpl->tpl_vars['temp']->value : array();
if (!is_array($_tmp_array) || $_tmp_array instanceof ArrayAccess) {
settype($_tmp_array, 'array');
}
$_tmp_array[] = $_smarty_tpl->tpl_vars['value']->value;
$_smarty_tpl->_assignInScope('temp', $_tmp_array);
?>
                                <?php }?>
                            <?php } else { ?>
                                <?php if (($_smarty_tpl->tpl_vars['k']->value >= 4) && ($_smarty_tpl->tpl_vars['k']->value < (count($_smarty_tpl->tpl_vars['array']->value)-1))) {?>
                                    <?php $_tmp_array = isset($_smarty_tpl->tpl_vars['temp']) ? $_smarty_tpl->tpl_vars['temp']->value : array();
if (!is_array($_tmp_array) || $_tmp_array instanceof ArrayAccess) {
settype($_tmp_array, 'array');
}
$_tmp_array[] = $_smarty_tpl->tpl_vars['value']->value;
$_smarty_tpl->_assignInScope('temp', $_tmp_array);
?>
                                <?php }?>
                            <?php }?>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        <tr>
                            <td class="align-middle" align="center">
                                <center><strong><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</strong></center>
                            </td>
                            <td class="align-middle" align="center">
                                <strong><?php echo $_smarty_tpl->tpl_vars['row']->value['subject_time'];?>
</strong>
                            </td>
                            <td <?php if (!$_smarty_tpl->tpl_vars['data']->value['is_category']) {?>class="hidden"<?php }?> align="center">
                                <strong><?php echo $_smarty_tpl->tpl_vars['row']->value['subject_name'];?>
</strong>
                            </td>
                            <?php $_smarty_tpl->_assignInScope('col', 1);
?>
                            <?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable(null, $_smarty_tpl->isRenderingCache);
$_smarty_tpl->tpl_vars['i']->value = 0;
if ($_smarty_tpl->tpl_vars['i']->value < count($_smarty_tpl->tpl_vars['temp']->value)) {
for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value < count($_smarty_tpl->tpl_vars['temp']->value); $_smarty_tpl->tpl_vars['i']->value++) {
?>
                                <?php if ($_smarty_tpl->tpl_vars['temp']->value[$_smarty_tpl->tpl_vars['i']->value] === $_smarty_tpl->tpl_vars['temp']->value[($_smarty_tpl->tpl_vars['i']->value+1)]) {?>
                                    <?php $_smarty_tpl->_assignInScope('col', $_smarty_tpl->tpl_vars['col']->value+1);
?>
                                <?php } else { ?>
                                    <td colspan = "<?php echo $_smarty_tpl->tpl_vars['col']->value;?>
">
                                        <center><?php echo nl2br($_smarty_tpl->tpl_vars['temp']->value[$_smarty_tpl->tpl_vars['i']->value]);?>
</center>
                                    </td>
                                    <?php $_smarty_tpl->_assignInScope('col', 1);
?>
                                <?php }?>
                            <?php }
}
?>

                        </tr>
                        <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                    </tbody>
                </table>
            </div>
        </div>
    <?php }?>
</div><?php }
}
