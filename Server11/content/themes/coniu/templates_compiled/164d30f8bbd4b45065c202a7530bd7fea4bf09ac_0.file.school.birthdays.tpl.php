<?php
/* Smarty version 3.1.31, created on 2021-03-31 09:59:13
  from "D:\workplace\Server11\content\themes\coniu\templates\ci\school\school.birthdays.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_6063e581b3ba72_14276097',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '164d30f8bbd4b45065c202a7530bd7fea4bf09ac' => 
    array (
      0 => 'D:\\workplace\\Server11\\content\\themes\\coniu\\templates\\ci\\school\\school.birthdays.tpl',
      1 => 1552404702,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6063e581b3ba72_14276097 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <div class="pull-right flip">
            <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/birthdays/parent" class="btn btn-default">
                    <i class="fa fa-universal-access"></i> <?php echo __("Parent");?>

                </a>
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/birthdays/teacher" class="btn btn-default">
                    <i class="fa fa-street-view"></i> <?php echo __("Teacher");?>

                </a>
            <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "parent") {?>
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/birthdays" class="btn btn-default">
                    <i class="fa fa-child"></i> <?php echo __("Children");?>

                </a>
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/birthdays/teacher" class="btn btn-default">
                    <i class="fa fa-street-view"></i> <?php echo __("Teacher");?>

                </a>
            <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "teacher") {?>
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/birthdays" class="btn btn-default">
                    <i class="fa fa-child"></i> <?php echo __("Children");?>

                </a>
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/birthdays/parent" class="btn btn-default">
                    <i class="fa fa-universal-access"></i> <?php echo __("Parent");?>

                </a>
            <?php }?>
        </div>
        <i class="fa fa-birthday-cake fa-fw fa-lg pr10"></i>
        <?php echo __("Birthday");?>

        <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
            &rsaquo; <?php echo __("Child");?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "parent") {?>
            &rsaquo; <?php echo __('Parent');?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "teacher") {?>
            &rsaquo; <?php echo __('Teacher');?>

        <?php }?>
    </div>
    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
        <div class="panel-body with-table form-horizontal">
            <div class="row">
                <div class='col-sm-4'>
                    <select name="class_id" id="birthday_class_id" class="form-control">
                        <option value=""><?php echo __("Select Class");?>
...</option>
                        <?php $_smarty_tpl->_assignInScope('class_level', -1);
?>
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['classes']->value, 'class');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class']->value) {
?>
                            <?php if (($_smarty_tpl->tpl_vars['class_level']->value != $_smarty_tpl->tpl_vars['class']->value['class_level_id'])) {?>
                                <option value="0" disabled>-----<?php echo $_smarty_tpl->tpl_vars['class']->value['class_level_name'];?>
-----</option>
                            <?php }?>
                            <option value="<?php echo $_smarty_tpl->tpl_vars['class']->value['group_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['class']->value['group_title'];?>
</option>
                            <?php $_smarty_tpl->_assignInScope('class_level', $_smarty_tpl->tpl_vars['class']->value['class_level_id']);
?>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                    </select>
                </div>
                <div class='col-sm-3'>
                    <div class='input-group date' id='fromdatepicker'>
                        <input type='text' name="fromDate" id="fromDate" value="<?php echo $_smarty_tpl->tpl_vars['fromDate']->value;?>
" class="form-control" placeholder="<?php echo __("From date");?>
"/>
                        <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                    </div>
                </div>
                <div class='col-sm-3'>
                    <div class='input-group date' id='todatepicker'>
                        <input type='text' name="toDate" id="toDate" value="<?php echo $_smarty_tpl->tpl_vars['toDate']->value;?>
" class="form-control" placeholder="<?php echo __("To date");?>
"/>
                        <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                    </div>
                </div>
                <div class='col-sm-2'>
                    <div class="form-group">
                        <a href="#" id="search" class="btn btn-default js_birthday-search-child" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['school']->value['page_id'];?>
"><?php echo __("Search");?>
</a>
                        <label id="loading" class="btn btn-info x-hidden"><?php echo __("Loading");?>
...</label>
                    </div>
                </div>
            </div>

            <div class="table-responsive pt10" id="birthday">
                
            </div>
        </div>

    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "parent") {?>
        <div class="panel-body with-table form-horizontal">
            <div class="row">
                <div class='col-sm-4'>
                    <select name="class_id" id="birthday_class_id" class="form-control">
                        <option value=""><?php echo __("Select Class");?>
...</option>
                        <?php $_smarty_tpl->_assignInScope('class_level', -1);
?>
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['classes']->value, 'class');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class']->value) {
?>
                            <?php if (($_smarty_tpl->tpl_vars['class_level']->value != $_smarty_tpl->tpl_vars['class']->value['class_level_id'])) {?>
                                <option value="0" disabled>-----<?php echo $_smarty_tpl->tpl_vars['class']->value['class_level_name'];?>
-----</option>
                            <?php }?>
                            <option value="<?php echo $_smarty_tpl->tpl_vars['class']->value['group_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['class']->value['group_title'];?>
</option>
                            <?php $_smarty_tpl->_assignInScope('class_level', $_smarty_tpl->tpl_vars['class']->value['class_level_id']);
?>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                    </select>
                </div>
                <div class='col-sm-3'>
                    <div class='input-group date' id='fromdatepicker'>
                        <input type='text' name="fromDate" id="fromDate" value="<?php echo $_smarty_tpl->tpl_vars['fromDate']->value;?>
" class="form-control" placeholder="<?php echo __("From date");?>
"/>
                        <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                    </div>
                </div>
                <div class='col-sm-3'>
                    <div class='input-group date' id='todatepicker'>
                        <input type='text' name="toDate" id="toDate" value="<?php echo $_smarty_tpl->tpl_vars['toDate']->value;?>
" class="form-control" placeholder="<?php echo __("To date");?>
"/>
                        <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                    </div>
                </div>
                <div class='col-sm-2'>
                    <div class="form-group">
                        <a href="#" id="search" class="btn btn-default js_birthday-search-parent" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['school']->value['page_id'];?>
"><?php echo __("Search");?>
</a>
                        <label id="loading" class="btn btn-info x-hidden"><?php echo __("Loading");?>
...</label>
                    </div>
                </div>
            </div>
            <br/>
            <div class="table-responsive" id = "birthday">
                
            </div>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "teacher") {?>
        <div class="panel-body with-table form-horizontal">
            <div class="row">
                <div class='col-sm-3'>
                    <div class='input-group date' id='fromdatepicker'>
                        <input type='text' name="fromDate" id="fromDate" value="<?php echo $_smarty_tpl->tpl_vars['fromDate']->value;?>
" class="form-control" placeholder="<?php echo __("From date");?>
"/>
                        <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                    </div>
                </div>
                <div class='col-sm-3'>
                    <div class='input-group date' id='todatepicker'>
                        <input type='text' name="toDate" id="toDate" value="<?php echo $_smarty_tpl->tpl_vars['toDate']->value;?>
" class="form-control" placeholder="<?php echo __("To date");?>
"/>
                        <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                    </div>
                </div>
                <div class='col-sm-2'>
                    <div class="form-group">
                        <a href="#" id="search" class="btn btn-default js_birthday-search-teacher" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['school']->value['page_id'];?>
"><?php echo __("Search");?>
</a>
                        <label id="loading" class="btn btn-info x-hidden"><?php echo __("Loading");?>
...</label>
                    </div>
                </div>
            </div>
            <br/>
            <div class="table-responsive" id = "birthday">
                
            </div>
        </div>
    

    <?php }?>
</div><?php }
}
