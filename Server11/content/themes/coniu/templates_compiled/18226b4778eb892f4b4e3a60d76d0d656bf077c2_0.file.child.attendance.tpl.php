<?php
/* Smarty version 3.1.31, created on 2021-03-31 10:47:29
  from "D:\workplace\Server11\content\themes\coniu\templates\ci\child\child.attendance.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_6063f0d148cd28_83814725',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '18226b4778eb892f4b4e3a60d76d0d656bf077c2' => 
    array (
      0 => 'D:\\workplace\\Server11\\content\\themes\\coniu\\templates\\ci\\child\\child.attendance.tpl',
      1 => 1552404702,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:ci/child/ajax.child.attendance.tpl' => 1,
  ),
),false)) {
function content_6063f0d148cd28_83814725 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == "resign") {?>
            <div class="pull-right flip">
                <a href="https://blog.coniu.vn/huong-dan-phu-huynh-xin-nghi-hoc-cho-tre-tren-website-coniu/" class="btn btn-info btn_guide" target="_blank">
                    <i class="fa fa-info-circle" aria-hidden="true"></i> <?php echo __("Guide");?>

                </a>
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/child/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
/attendance" class="btn btn-default">
                    <?php echo __("Attendance information");?>

                </a>
            </div>
        <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
            <div class="pull-right flip">
                <a href="https://blog.coniu.vn/huong-dan-xin-nghi-hoc-cho-tre-tai-khoan-phu-huynh-tren-ung-dung-mam-non-coniu/" class="btn btn-info btn_guide" target="_blank">
                    <i class="fa fa-info-circle" aria-hidden="true"></i> <?php echo __("Guide");?>

                </a>
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/child/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
/attendance/resign" class="btn btn-default">
                    <i class="fa fa-plus"></i> <?php echo __("Resign");?>

                </a>
            </div>
        <?php }?>

        <i class="fa fa-tasks fa-fw fa-lg pr10"></i>
        <?php echo __("Resign");?>

        <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == "search") {?>
            &rsaquo; <?php echo __("Search");?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
            &rsaquo; <?php echo __("Attendance information");?>

        <?php }?>
    </div>
    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
        <div class="panel-body with-table">
            <div class="row form-horizontal">
                <div class="form-group">
                    <label class="col-sm-2 control-label text-left"><?php echo __("Time");?>
</label>
                    <div class="col-sm-10">
                        <div class='col-sm-4'>
                            <div class='input-group date' id='fromdate_picker'>
                                <input type='text' name="fromDate" id="fromDate" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['fromDate'];?>
" class="form-control" placeholder="<?php echo __("From date");?>
"/>
                                <span class="input-group-addon">
                                    <span class="fas fa-calendar-alt"></span>
                                </span>
                            </div>
                        </div>
                        <div class='col-sm-1 text-center'><i class="fas fa-long-arrow-alt-right fa-lg fa-fw pr10 pt10"></i></div>
                        <div class='col-sm-4'>
                            <div class='input-group date' id='todate_picker'>
                                <input type='text' name="toDate" id="toDate" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['toDate'];?>
" class="form-control" placeholder="<?php echo __("To date");?>
"/>
                                <span class="input-group-addon">
                                    <span class="fas fa-calendar-alt"></span>
                                </span>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <a class="btn btn-default js_child-attendance" data-id="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
"><?php echo __("Search");?>
</a>
                        </div>
                    </div>
                </div>
            </div><br/>
            <div class="table-responsive" id="attendance_list">
                <?php $_smarty_tpl->_subTemplateRender("file:ci/child/ajax.child.attendance.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

            </div>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "resign") {?>
        <div class="panel-body">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/child/bochild_all.php">
                <input type="hidden" name="child_id" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
"/>
                <input type="hidden" name="do" value="resign"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Date");?>
</label>
                    <div class='col-sm-3'>
                        <div class='input-group date' id='medicine_beginpicker_new'>
                            <input type='text' name="start_date" class="form-control" placeholder="<?php echo __("Begin");?>
 (*)" required/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                    <div class='col-sm-1 text-center'><i class="fas fa-long-arrow-alt-right fa-lg fa-fw pr10 pt10"></i></div>
                    <div class='col-sm-3'>
                        <div class='input-group date' id='medicine_endpicker_new'>
                            <input type='text' name="end_date" class="form-control" placeholder="<?php echo __("End");?>
 (*)" required/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>

                    
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Absent reason");?>
</label>
                    <div class='col-sm-8'>
                        <input type="text" class="form-control" name="reason" maxlength="512" placeholder="<?php echo __("Absent reason");?>
">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-3">

                    </div>
                    <div class="col-sm-9">
                        <button type="submit" class="btn btn-default"><?php echo __("Resign");?>
</button>
                    </div>
                </div>
                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    <?php }?>
</div><?php }
}
