<?php
/* Smarty version 3.1.31, created on 2021-03-31 10:27:15
  from "D:\workplace\Server11\content\themes\coniu\templates\ci\class\class.events.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_6063ec134ec619_43684653',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'caa47f16a180a598326bf077470e5aacd167d064' => 
    array (
      0 => 'D:\\workplace\\Server11\\content\\themes\\coniu\\templates\\ci\\class\\class.events.tpl',
      1 => 1552404703,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6063ec134ec619_43684653 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <div class="pull-right flip">
            <a href="https://blog.coniu.vn/huong-dan-giao-vien-dang-ky-su-kien-tren-website-coniu/" class="btn btn-info btn_guide" target="_blank">
                <i class="fa fa-info-circle"></i> <?php echo __("Guide");?>

            </a>
        </div>
        
            
                
                    
                
            
        
        <i class="fa fa-bell fa-lg fa-fw pr10"></i>
        <?php echo __("Notification - Event");?>

        
            
        <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == "detail") {?>
            &rsaquo; <?php echo $_smarty_tpl->tpl_vars['data']->value['event_name'];?>

        
            
                
            
        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "participants") {?>
            &rsaquo; <?php echo __("Participants");?>
 &rsaquo; <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/events/detail/<?php echo $_smarty_tpl->tpl_vars['event']->value['event_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['event']->value['event_name'];?>
</a>
        <?php }?>
    </div>
    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
        <div class="panel-body with-table">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover js_dataTable">
                    <thead>
                    <tr><th colspan="4"><?php echo __("Notification - Event list");?>
&nbsp;(<?php echo count($_smarty_tpl->tpl_vars['rows']->value);?>
)</th></tr>
                    <tr>
                        <th>#</th>
                        <th><?php echo __("Notification - Event");?>
</th>
                        <th><?php echo __("Event level");?>
</th>
                        
                        
                        
                        <th><?php echo __("Actions");?>
</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['rows']->value, 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
                        <tr>
                            <?php if ($_smarty_tpl->tpl_vars['idx']->value > 1) {?><tr><td colspan="7"></td></tr><?php }?>
                            <td align="center" rowspan="2" style="vertical-align:middle"><strong><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</strong></td>
                            <td style="vertical-align:middle">
                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/events/detail/<?php echo $_smarty_tpl->tpl_vars['row']->value['event_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['row']->value['event_name'];?>
</a> <a href="#" data-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['event_id'];?>
" style="float: right; color: #5D9D58" class="js_event_toggle"><?php echo __("Show/hide");?>
</a>
                                <?php if ($_smarty_tpl->tpl_vars['row']->value['status'] == @constant('EVENT_STATUS_CANCELLED')) {?>
                                    <img src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/content/themes/<?php echo $_smarty_tpl->tpl_vars['system']->value['theme'];?>
/images/cancel.png"/>
                                <?php }?>
                            </td>
                            <td align="center" style="vertical-align:middle">
                                <?php if ($_smarty_tpl->tpl_vars['row']->value['level'] == @constant('SCHOOL_LEVEL')) {?>
                                    <?php echo __("School");?>

                                <?php } elseif ($_smarty_tpl->tpl_vars['row']->value['level'] == @constant('CLASS_LEVEL_LEVEL')) {?>
                                    <?php echo __("Class level");?>

                                <?php } elseif ($_smarty_tpl->tpl_vars['row']->value['level'] == @constant('CLASS_LEVEL')) {?>
                                    <?php echo __("Class");?>

                                <?php } elseif ($_smarty_tpl->tpl_vars['row']->value['level'] == @constant('TEACHER_LEVEL')) {?>
                                    <?php echo __("Teacher");?>

                                <?php }?>
                            </td>
                            
                                
                                    
                                
                                    
                                
                            
                            
                                
                                    
                                
                                    
                                
                            
                            
                                
                                    
                                
                            
                            <td align="center" style="vertical-align:middle">
                                
                                    
                                        
                                    
                                
                                <?php if ($_smarty_tpl->tpl_vars['row']->value['must_register']) {?>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/events/participants/<?php echo $_smarty_tpl->tpl_vars['row']->value['event_id'];?>
" class="btn btn-xs btn-default">
                                        <?php echo __("Participants");?>

                                    </a>
                                <?php }?>

                                
                                    
                                    
                                        
                                    
                                        
                                    
                                
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" style="display: none" class="event_description_<?php echo $_smarty_tpl->tpl_vars['row']->value['event_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['row']->value['description'];?>
</td>
                        </tr>
                        <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                    </tbody>
                </table>
            </div>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "detail") {?>
        <div class="panel-body with-table">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <tbody>
                    <tr>
                        <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Notification - Event");?>
</strong></td>
                        <td>
                            <?php echo $_smarty_tpl->tpl_vars['data']->value['event_name'];?>

                            <?php if ($_smarty_tpl->tpl_vars['data']->value['status'] == @constant('EVENT_STATUS_CANCELLED')) {?>
                                <img src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/content/themes/<?php echo $_smarty_tpl->tpl_vars['system']->value['theme'];?>
/images/cancel.png"/>
                            <?php }?>
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Event level");?>
</strong></td>
                        <td>
                            <?php if ($_smarty_tpl->tpl_vars['data']->value['level'] == @constant('SCHOOL_LEVEL')) {?>
                                <?php echo __("School");?>

                            <?php } elseif ($_smarty_tpl->tpl_vars['data']->value['level'] == @constant('CLASS_LEVEL_LEVEL')) {?>
                                <?php echo $_smarty_tpl->tpl_vars['data']->value['class_level_name'];?>

                            <?php } elseif ($_smarty_tpl->tpl_vars['data']->value['level'] == @constant('CLASS_LEVEL')) {?>
                                <?php echo $_smarty_tpl->tpl_vars['data']->value['group_title'];?>

                            <?php } elseif ($_smarty_tpl->tpl_vars['data']->value['level'] == @constant('TEACHER_LEVEL')) {?>
                                <?php echo __("Teacher");?>

                            <?php }?>
                        </td>
                    </tr>
                    
                        
                        
                    
                    
                        
                        
                    
                    
                        
                            
                            
                                
                            
                        
                    
                    <?php if ($_smarty_tpl->tpl_vars['data']->value['must_register'] == '1') {?>
                        
                            
                            
                        
                        
                            
                            
                        
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Must register");?>
?</strong></td>
                            <td><?php echo __('Yes');?>
</td>
                        </tr>
                        <?php if ($_smarty_tpl->tpl_vars['data']->value['registration_deadline'] != '') {?>
                            <tr>
                                <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Registration deadline");?>
</strong></td>
                                <td><font color="red"><?php echo $_smarty_tpl->tpl_vars['data']->value['registration_deadline'];?>
</font></td>
                            </tr>
                        <?php }?>
                        
                            
                            
                        
                        
                            
                                
                                
                            
                        
                        
                            
                            
                        
                        
                            
                                
                                
                            
                        
                    <?php }?>
                    <tr style="background: #fff">
                        
                        <td colspan="2"><?php echo $_smarty_tpl->tpl_vars['data']->value['description'];?>
</td>
                    </tr>
                    
                        
                        
                    
                    
                        
                        
                    
                    
                        
                            
                            
                        
                    
                    </tbody>
                </table>
            </div>
            <div class="form-group pl5">
                <div class="col-sm-12">
                    <a class="btn btn-default" href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/events"><?php echo __("Lists");?>
</a>
                    <?php if ($_smarty_tpl->tpl_vars['data']->value['must_register']) {?>
                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/events/participants/<?php echo $_smarty_tpl->tpl_vars['data']->value['event_id'];?>
" class="btn btn-default"><?php echo __("Participants");?>
</a>
                    <?php }?>
                    
                        
                            
                            
                                
                            
                        
                        
                            
                        
                            
                        
                    
                </div>
            </div>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "participants") {?>
        <div class="panel-body with-table">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/class/boclass_event.php">
                <input type="hidden" name="username" id="username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                <input type="hidden" name="event_id" id="event_id" value="<?php echo $_smarty_tpl->tpl_vars['event']->value['event_id'];?>
"/>
                <input type="hidden" name="event_name" id="event_name" value="<?php echo $_smarty_tpl->tpl_vars['event']->value['event_name'];?>
"/>
                <input type="hidden" name="do" value="add_pp"/>

                <?php if ($_smarty_tpl->tpl_vars['event']->value['can_register'] == 1) {?>
                    <div>
                        <?php if ($_smarty_tpl->tpl_vars['event']->value['for_child'] && $_smarty_tpl->tpl_vars['event']->value['for_parent']) {?>
                            <input type="checkbox" id="eventCheckAll">&nbsp;<?php echo __("All");?>

                        <?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['event']->value['for_child']) {?>
                            &nbsp;&nbsp;<input type="checkbox" id="eventCheckAllChildren">&nbsp;<?php echo __("All children");?>

                        <?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['event']->value['for_parent']) {?>
                            &nbsp;&nbsp;<input type="checkbox" id="eventCheckAllParent">&nbsp;<?php echo __("All parent");?>

                        <?php }?>
                    </div>
                <?php }?>
                <div class="table-responsive">
                    <div>
                        <strong>
                            <?php echo __("Participants");?>
 (<?php echo __("Registered");?>
:
                            <?php if ($_smarty_tpl->tpl_vars['event']->value['for_child']) {
echo $_smarty_tpl->tpl_vars['childCount']->value;?>
 <?php echo __("children");
}?>
                            <?php if ($_smarty_tpl->tpl_vars['event']->value['for_child'] && $_smarty_tpl->tpl_vars['event']->value['for_parent']) {?>&nbsp;|&nbsp;<?php }?>
                            <?php if ($_smarty_tpl->tpl_vars['event']->value['for_parent']) {
echo $_smarty_tpl->tpl_vars['parentCount']->value;?>
 <?php echo __("parent");
}?>)
                        </strong>
                    </div>
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th><?php echo __("Select");?>
</th>
                            <th><?php echo __("Child");?>
</th>
                            <th><?php echo __("Parent phone");?>
</th>
                            <th><?php echo __("Parent");?>
</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['participants']->value, 'child');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['child']->value) {
?>
                            <tr <?php if ($_smarty_tpl->tpl_vars['child']->value['child_status'] == 0) {?> class="row-disable" <?php }?>>
                                <td align="center"><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</td>
                                <td align="center">
                                    <?php if ($_smarty_tpl->tpl_vars['event']->value['for_child']) {?>
                                        <input type="checkbox" class="child" name="childIds[]" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
"
                                               <?php if ($_smarty_tpl->tpl_vars['child']->value['event_id'] > 0) {?>checked <?php if ($_smarty_tpl->tpl_vars['child']->value['created_user_id'] != $_smarty_tpl->tpl_vars['user']->value->_data['user_id']) {?>disabled<?php }
}?>
                                                <?php if ($_smarty_tpl->tpl_vars['event']->value['can_register'] == 0) {?>disabled<?php }?>>

                                        <?php if ($_smarty_tpl->tpl_vars['event']->value['can_register'] > 0) {?>
                                            
                                            <input type="hidden" name="child_name_<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_name'];?>
"/>
                                        <?php }?>

                                        <?php if ($_smarty_tpl->tpl_vars['child']->value['event_id'] > 0 && ($_smarty_tpl->tpl_vars['child']->value['created_user_id'] == $_smarty_tpl->tpl_vars['user']->value->_data['user_id'])) {?>
                                            <input type="hidden" name="oldChildIds[]" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
"/>
                                        <?php }?>
                                    <?php }?>
                                </td>
                                <td><?php echo $_smarty_tpl->tpl_vars['child']->value['child_name'];?>
 (<?php echo $_smarty_tpl->tpl_vars['child']->value['birthday'];?>
)
                                    <?php if (($_smarty_tpl->tpl_vars['child']->value['event_id'] > 0) && ($_smarty_tpl->tpl_vars['child']->value['created_user_id'] != $_smarty_tpl->tpl_vars['user']->value->_data['user_id'])) {?>
                                        <br/>
                                        <?php if ($_smarty_tpl->tpl_vars['event']->value['can_register'] > 0) {?>
                                            <a href="#" class="btn btn-xs btn-warning js_class-event-remove-participant" data-type="<?php echo @constant('PARTICIPANT_TYPE_CHILD');?>
" data-uid="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
" data-name="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_name'];?>
">
                                                <?php echo __("Cancel");?>

                                            </a>
                                        <?php } else { ?>
                                            <?php echo __("Registrar");?>
:
                                        <?php }?> <b><?php echo $_smarty_tpl->tpl_vars['child']->value['created_fullname'];?>
</b>
                                    <?php }?>
                                </td>
                                <td><?php echo $_smarty_tpl->tpl_vars['child']->value['parent_phone'];?>
</td>
                                <td>
                                    <?php if ($_smarty_tpl->tpl_vars['event']->value['for_parent']) {?>
                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['child']->value['parent'], 'parent');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['parent']->value) {
?>
                                            <input type="checkbox" class="parent" name="parentIds[]" value="<?php echo $_smarty_tpl->tpl_vars['parent']->value['user_id'];?>
"
                                                   <?php if ($_smarty_tpl->tpl_vars['parent']->value['event_id'] > 0) {?>checked <?php if ($_smarty_tpl->tpl_vars['parent']->value['created_user_id'] != $_smarty_tpl->tpl_vars['user']->value->_data['user_id']) {?>disabled<?php }
}?>
                                                    <?php if ($_smarty_tpl->tpl_vars['event']->value['can_register'] == 0) {?>disabled<?php }?>>
                                            <?php if ($_smarty_tpl->tpl_vars['event']->value['can_register'] > 0) {?>
                                                
                                                <input type="hidden" name="child_id_of_parent_<?php echo $_smarty_tpl->tpl_vars['parent']->value['user_id'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
"/>
                                                <input type="hidden" name="parent_name_<?php echo $_smarty_tpl->tpl_vars['parent']->value['user_id'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['parent']->value['user_fullname'];?>
"/>
                                            <?php }?>
                                            <span class="name js_user-popover" data-uid="<?php echo $_smarty_tpl->tpl_vars['parent']->value['user_id'];?>
">
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['parent']->value['user_name'];?>
"><?php echo $_smarty_tpl->tpl_vars['parent']->value['user_fullname'];?>
</a>
                                            </span>
                                            <?php if ($_smarty_tpl->tpl_vars['parent']->value['user_id'] != $_smarty_tpl->tpl_vars['user']->value->_data['user_id']) {?>
                                                <a href="#" class="far fa-comments fa-lg js_chat-start" data-name="<?php echo $_smarty_tpl->tpl_vars['parent']->value['user_fullname'];?>
" data-uid="<?php echo $_smarty_tpl->tpl_vars['parent']->value['user_id'];?>
"></a>
                                            <?php }?>
                                            <?php if (($_smarty_tpl->tpl_vars['parent']->value['event_id'] > 0) && ($_smarty_tpl->tpl_vars['parent']->value['created_user_id'] != $_smarty_tpl->tpl_vars['user']->value->_data['user_id'])) {?>
                                                <br/>
                                                <?php if ($_smarty_tpl->tpl_vars['event']->value['can_register'] > 0) {?>
                                                    <a href="#" class="btn btn-xs btn-warning js_class-event-remove-participant" data-type="<?php echo @constant('PARTICIPANT_TYPE_PARENT');?>
" data-uid="<?php echo $_smarty_tpl->tpl_vars['parent']->value['user_id'];?>
" data-name="<?php echo $_smarty_tpl->tpl_vars['parent']->value['user_fullname'];?>
" data-child="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
">
                                                        <?php echo __("Cancel");?>

                                                    </a>
                                                <?php } else { ?>
                                                    <?php echo __("Registrar");?>
:
                                                <?php }?> <b><?php echo $_smarty_tpl->tpl_vars['parent']->value['created_fullname'];?>
</b>
                                            <?php }?>
                                            <?php if ($_smarty_tpl->tpl_vars['parent']->value['event_id'] > 0 && ($_smarty_tpl->tpl_vars['parent']->value['created_user_id'] == $_smarty_tpl->tpl_vars['user']->value->_data['user_id'])) {?>
                                                <input type="hidden" name="oldParentIds[]" value="<?php echo $_smarty_tpl->tpl_vars['parent']->value['user_id'];?>
"/>
                                            <?php }?>
                                            <br/>
                                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                    <?php }?>
                                </td>
                            </tr>
                            <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </tbody>
                    </table>
                </div>
                <?php if ((count($_smarty_tpl->tpl_vars['participants']->value) > 0) && ($_smarty_tpl->tpl_vars['event']->value['can_register'] == 1)) {?>
                    <div class="form-group pl5">
                        <div class="col-sm-9">
                            <button type="submit" class="btn btn-primary padrl30"><?php echo __("Save");?>
</button>
                        </div>
                    </div>
                <?php } elseif ($_smarty_tpl->tpl_vars['event']->value['can_register'] == 0) {?>
                    <div class="form-group pl5">
                        <div class="col-sm-9">
                            <?php echo __("The registration deadline has passed or the event has been cancelled");?>
.
                        </div>
                    </div>
                <?php }?>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "edit") {?>
        <div class="panel-body">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/class/boclass_event.php">
                <input type="hidden" name="username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                <input type="hidden" name="event_id" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['event_id'];?>
"/>
                <input type="hidden" name="post_ids" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['post_ids'];?>
"/>
                <input type="hidden" name="do" value="edit"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Notification - Event");?>
 (*)</label>
                    <div class="col-sm-9">
                        <input class="form-control" name="event_name" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['event_name'];?>
" required autofocus maxlength="250">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Description");?>
 (*)</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="description" rows="5" required><?php echo $_smarty_tpl->tpl_vars['data']->value['description'];?>
</textarea>
                    </div>
                </div>
                <div class="form-group" name="event_post_on_wall">
                    <label class="col-sm-3 control-label text-left">
                        <?php echo __("Post on page");?>
?
                    </label>
                    <div class="col-sm-9">
                        <div class="onoffswitch">
                            <input type="checkbox" name="post_on_wall" class="onoffswitch-checkbox" id="post_on_wall" <?php if ($_smarty_tpl->tpl_vars['data']->value['post_on_wall'] == '1') {?>checked<?php }?>>
                            <label class="onoffswitch-label" for="post_on_wall"></label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">
                        <?php echo __("Notify immediately");?>
?
                    </label>
                    <div class="col-sm-9">
                        <div class="onoffswitch">
                            <input type="checkbox" name="notify_immediately" class="onoffswitch-checkbox" id="notify_immediately" <?php if ($_smarty_tpl->tpl_vars['data']->value['is_notified'] == '1') {?>checked<?php }?>>
                            <label class="onoffswitch-label" for="notify_immediately"></label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Time");?>
</label>
                    <div class="col-sm-9">
                        <div class='col-sm-5'>
                            <div class='input-group date' id='beginpicker'>
                                <input type='text' name="begin" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['begin'];?>
" class="form-control" placeholder="<?php echo __("Begin");?>
"/>
                                <span class="input-group-addon">
                                    <span class="fas fa-calendar-alt"></span>
                                </span>
                            </div>
                        </div>

                        <div class='col-md-5'>
                            <div class='input-group date' id='endpicker'>
                                <input type='text' name="end" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['end'];?>
" class="form-control" placeholder="<?php echo __("End");?>
"/>
                                <span class="input-group-addon">
                                    <span class="fas fa-calendar-alt"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <?php if ($_smarty_tpl->tpl_vars['data']->value['must_register'] == '0') {?>
                    <div class="form-group" id="event_more_information_button">
                        <div class="col-sm-12">
                            <a href="#" class="btn btn-default pull-right js_event-more-information"><?php echo __("More information");?>
</a>
                        </div>
                    </div>
                <?php }?>
                <div <?php if ($_smarty_tpl->tpl_vars['data']->value['must_register'] == '0') {?>class="x-hidden"<?php }?> id="event_more_information">
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left"><?php echo __("Event location");?>
</label>
                        <div class="col-sm-9">
                            <input class="form-control" name="location" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['location'];?>
" maxlength="250">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left"><?php echo __("Participation cost");?>
</label>
                        <div class="col-sm-9">
                            <div class="input-group col-md-5">
                                <input type="number" min="0" step="1000" class="form-control" name="price" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['price'];?>
" maxlength="10">
                                <span class="input-group-addon"><?php echo @constant('MONEY_UNIT');?>
</span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left"><?php echo __("Must register");?>
?</label>
                        <div class="col-sm-9">
                            <div class="onoffswitch">
                                <input type="checkbox" name="must_register" class="onoffswitch-checkbox" id="must_register" <?php if ($_smarty_tpl->tpl_vars['data']->value['must_register'] == '1') {?>checked<?php }?>>
                                <label class="onoffswitch-label" for="must_register"></label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group" id="event_registration_deadline">
                        <label class="col-sm-3 control-label text-left"><?php echo __("Registration deadline");?>
</label>
                        <div class='col-sm-4'>
                            <div class='input-group date' id='registration_deadlinepicker'>
                                <input type='text' name="registration_deadline" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['registration_deadline'];?>
" class="form-control" placeholder="<?php echo __("Registration deadline");?>
"/>
                                <span class="input-group-addon">
                                    <span class="fas fa-calendar-alt"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group" id="event_for_child">
                        <label class="col-sm-3 control-label text-left"><?php echo __("For child");?>
?</label>
                        <div class="col-sm-9">
                            <div class="onoffswitch">
                                <input type="checkbox" name="for_child" class="onoffswitch-checkbox" id="for_child" <?php if ($_smarty_tpl->tpl_vars['data']->value['for_child'] == '1') {?>checked<?php }?>>
                                <label class="onoffswitch-label" for="for_child"></label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group" id="event_for_parent">
                        <label class="col-sm-3 control-label text-left"><?php echo __("For parent");?>
?</label>
                        <div class="col-sm-9">
                            <div class="onoffswitch">
                                <input type="checkbox" name="for_parent" class="onoffswitch-checkbox" id="for_parent" <?php if ($_smarty_tpl->tpl_vars['data']->value['for_parent'] == '1') {?>checked<?php }?>>
                                <label class="onoffswitch-label" for="for_parent"></label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30"><?php echo __("Save");?>
</button>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "add" && $_smarty_tpl->tpl_vars['schoolConfig']->value['allow_class_event']) {?>
        <div class="panel-body">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/class/boclass_event.php">
                <input type="hidden" name="username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                <input type="hidden" name="do" value="add"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Notification - Event");?>
 (*)</label>
                    <div class="col-sm-9">
                        <input class="form-control" name="event_name" required autofocus maxlength="250">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Description");?>
 (*)</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="description" rows="5" required></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Post on page");?>
?</label>
                    <div class="col-sm-9">
                        <div class="onoffswitch">
                            <input type="checkbox" name="post_on_wall" class="onoffswitch-checkbox" id="post_on_wall" checked>
                            <label class="onoffswitch-label" for="post_on_wall"></label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Notify immediately");?>
?</label>
                    <div class="col-sm-9">
                        <div class="onoffswitch">
                            <input type="checkbox" name="notify_immediately" class="onoffswitch-checkbox" id="notify_immediately">
                            <label class="onoffswitch-label" for="notify_immediately"></label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Time");?>
</label>
                    <div class="col-sm-9">
                        <div class='col-sm-5'>
                            <div class='input-group date' id='beginpicker'>
                                <input type='text' name="begin" class="form-control" placeholder="<?php echo __("Begin");?>
"/>
                                    <span class="input-group-addon">
                                        <span class="fas fa-calendar-alt"></span>
                                    </span>
                            </div>
                        </div>

                        <div class='col-md-5'>
                            <div class='input-group date' id='endpicker'>
                                <input type='text' name="end" class="form-control" placeholder="<?php echo __("End");?>
"/>
                                    <span class="input-group-addon">
                                        <span class="fas fa-calendar-alt"></span>
                                    </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group" id="event_more_information_button">
                    <div class="col-sm-12">
                        <a href="#" class="btn btn-default pull-right js_event-more-information"><?php echo __("More information");?>
</a>
                    </div>
                </div>
                <div class="x-hidden" id="event_more_information">
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left"><?php echo __("Event location");?>
</label>
                        <div class="col-sm-9">
                            <input class="form-control" name="location" value="<?php echo __("At class");?>
" maxlength="250">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left"><?php echo __("Participation cost");?>
</label>
                        <div class="col-sm-9">
                            <div class="input-group col-md-5">
                                <input type="number" min="0" step="1000" class="form-control" name="price" value="0" maxlength="10">
                                <span class="input-group-addon"><?php echo @constant('MONEY_UNIT');?>
</span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left"><?php echo __("Must register");?>
?</label>
                        <div class="col-sm-9">
                            <div class="onoffswitch">
                                <input type="checkbox" name="must_register" class="onoffswitch-checkbox" id="must_register">
                                <label class="onoffswitch-label" for="must_register"></label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group x-hidden" id="event_registration_deadline">
                        <label class="col-sm-3 control-label text-left"><?php echo __("Registration deadline");?>
</label>
                        <div class='col-sm-4'>
                            <div class='input-group date' id='registration_deadlinepicker'>
                                <input type='text' name="registration_deadline" class="form-control" placeholder="<?php echo __("Registration deadline");?>
"/>
                                <span class="input-group-addon">
                                    <span class="fas fa-calendar-alt"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group x-hidden" id="event_for_child">
                        <label class="col-sm-3 control-label text-left"><?php echo __("For child");?>
?</label>
                        <div class="col-sm-9">
                            <div class="onoffswitch">
                                <input type="checkbox" name="for_child" class="onoffswitch-checkbox" id="for_child">
                                <label class="onoffswitch-label" for="for_child"></label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group x-hidden" id="event_for_parent">
                        <label class="col-sm-3 control-label text-left"><?php echo __("For parent");?>
?</label>
                        <div class="col-sm-9">
                            <div class="onoffswitch">
                                <input type="checkbox" name="for_parent" class="onoffswitch-checkbox" id="for_parent">
                                <label class="onoffswitch-label" for="for_parent"></label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30"><?php echo __("Save");?>
</button>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    <?php }?>
</div><?php }
}
