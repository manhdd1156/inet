<?php
/* Smarty version 3.1.31, created on 2021-04-29 17:22:13
  from "D:\workplace\mascom-edu-server\Server11\content\themes\coniu\templates\ci\class\ajax.class.categorydetail.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_608a88d5e78a33_16146244',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '4f9514c2dff4563c26f47d309eaeb5e0a3b4adc0' => 
    array (
      0 => 'D:\\workplace\\mascom-edu-server\\Server11\\content\\themes\\coniu\\templates\\ci\\class\\ajax.class.categorydetail.tpl',
      1 => 1619677394,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_608a88d5e78a33_16146244 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="form-group">
    <?php echo $_smarty_tpl->tpl_vars['results']->value['category_name'];?>

</div>
<div class = "table-responsive">
    <table class="table table-striped table-bordered table-hover js_dataTable">
        <thead>
        <tr>
            <th><?php echo __("#");?>
</th>
            <th><?php echo __("Category suggest");?>
</th>
        </tr>
        </thead>
        <tbody>
        <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['results']->value['suggests'], 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
            <tr>
                <td class="align-middle">
                    <center><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</center>
                </td>
                <td class="align-middle">
                    <?php echo $_smarty_tpl->tpl_vars['row']->value;?>

                </td>
            </tr>
            <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

        </tbody>
    </table>
</div><?php }
}
