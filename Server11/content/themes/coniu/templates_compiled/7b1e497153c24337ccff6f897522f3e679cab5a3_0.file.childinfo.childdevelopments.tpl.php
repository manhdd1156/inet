<?php
/* Smarty version 3.1.31, created on 2021-03-31 14:08:26
  from "D:\workplace\Server11\content\themes\coniu\templates\ci\childinfo\childinfo.childdevelopments.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_60641feada8fb0_77977230',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '7b1e497153c24337ccff6f897522f3e679cab5a3' => 
    array (
      0 => 'D:\\workplace\\Server11\\content\\themes\\coniu\\templates\\ci\\childinfo\\childinfo.childdevelopments.tpl',
      1 => 1552404704,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60641feada8fb0_77977230 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <div class="pull-right flip">
            <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == "detail") {?>
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/childinfo/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_parent_id'];?>
/childdevelopments" class="btn btn-default">
                    <i class="fa fa-list"></i> <?php echo __("Lists");?>

                </a>
            <?php }?>
        </div>
        <i class="fa fa-child fa-fw fa-lg pr10"></i>
        <?php echo __('Child development');?>

        <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == "detail") {?>
            &rsaquo; <?php echo __('Detail');?>

        <?php }?>
    </div>
    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
        <div class="panel-body with-table form-horizontal">
            <div class="row" align="center">
                <button class="js_general-show btn btn-default selected_chart"><?php echo __("General");?>
</button>
                <button class="js_missed-show btn btn-default"><?php echo __("Missed");?>
</button>
                <button class="js_vaccinating-show btn btn-default"><?php echo __("Vaccinating");?>
</button>
            </div>
            <br/>
            <div id="general">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover js_dataTable">
                        <thead>
                        <tr>
                            <th colspan="5"><?php echo __("General");?>
 (<?php echo count($_smarty_tpl->tpl_vars['child_development']->value);?>
)</th>
                        </tr>
                        <tr>
                            <th><?php echo __('No.');?>
</th>
                            <th><?php echo __("Time");?>
</th>
                            <th><?php echo __('Title');?>
</th>
                            <th><?php echo __('Status');?>
</th>
                            <th><?php echo __('Actions');?>
</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['child_development']->value, 'row', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['row']->value) {
?>
                            <tr>
                                <td align="center"><?php echo $_smarty_tpl->tpl_vars['k']->value+1;?>
</td>
                                <td align="center"><?php echo $_smarty_tpl->tpl_vars['row']->value['time'];?>
</td>
                                <td><a href = "<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/childinfo/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_parent_id'];?>
/childdevelopments/detail/<?php echo $_smarty_tpl->tpl_vars['row']->value['child_development_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['row']->value['title'];?>
</a></td>
                                <td align="center">
                                    <?php if ($_smarty_tpl->tpl_vars['row']->value['type'] == 1) {?>
                                        <?php if ($_smarty_tpl->tpl_vars['row']->value['vaccinationed']) {?>
                                            <div><strong><?php echo __('Vaccinationed');?>
</strong></div>
                                        <?php } else { ?>
                                            <div><strong><?php echo __('Not vaccinating');?>
</strong></div>
                                        <?php }?>
                                    <?php }?>
                                </td>
                                <td align="center">
                                    <?php if ($_smarty_tpl->tpl_vars['row']->value['type'] == 1) {?>
                                        <?php if (!$_smarty_tpl->tpl_vars['row']->value['vaccinationed']) {?>
                                            <a class = "btn btn-success btn-xs js_child-vaccinationed" href="#" data-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['child_development_id'];?>
" data-child="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_parent_id'];?>
"><?php echo __("Vaccination");?>
</a>
                                        <?php } else { ?>
                                            <a class = "btn btn-danger btn-xs js_child-cancel-vaccination" href="#" data-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['child_development_id'];?>
" data-child="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_parent_id'];?>
"><?php echo __("Cancel vaccination");?>
</a>
                                        <?php }?>
                                    <?php }?>
                                </td>
                            </tr>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </tbody>
                    </table>
                </div>
            </div>
            <div id="missed">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover js_dataTable">
                        <thead>
                        <tr>
                            <th colspan="5"><?php echo __("Missed");?>
 (<?php echo count($_smarty_tpl->tpl_vars['child_vaccination_history']->value);?>
)</th>
                        </tr>
                        <tr>
                            <th><?php echo __('No.');?>
</th>
                            <th><?php echo __("Time");?>
</th>
                            <th><?php echo __('Title');?>
</th>
                            <th><?php echo __('Status');?>
</th>
                            <th><?php echo __('Actions');?>
</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['child_vaccination_history']->value, 'row', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['row']->value) {
?>
                            <tr>
                                <td align="center"><?php echo $_smarty_tpl->tpl_vars['k']->value+1;?>
</td>
                                <td align="center"><?php echo $_smarty_tpl->tpl_vars['row']->value['time'];?>
</td>
                                <td><a href = "<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/childinfo/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_parent_id'];?>
/childdevelopments/detail/<?php echo $_smarty_tpl->tpl_vars['row']->value['child_development_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['row']->value['title'];?>
</a></td>
                                <td align="center">
                                    <?php if ($_smarty_tpl->tpl_vars['row']->value['type'] == 1) {?>
                                        <?php if ($_smarty_tpl->tpl_vars['row']->value['vaccinationed']) {?>
                                            <div><strong><?php echo __('Vaccinationed');?>
</strong></div>
                                        <?php } else { ?>
                                            <div><strong><?php echo __('Not vaccinating');?>
</strong></div>
                                        <?php }?>
                                    <?php }?>
                                </td>
                                <td align="center">
                                    <?php if ($_smarty_tpl->tpl_vars['row']->value['type'] == 1) {?>
                                        <?php if (!$_smarty_tpl->tpl_vars['row']->value['vaccinationed']) {?>
                                            <a class = "btn btn-success btn-xs js_child-vaccinationed" href="#" data-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['child_development_id'];?>
" data-child="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_parent_id'];?>
"><?php echo __("Vaccination");?>
</a>
                                        <?php } else { ?>
                                            <a class = "btn btn-danger btn-xs js_child-cancel-vaccination" href="#" data-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['child_development_id'];?>
" data-child="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_parent_id'];?>
"><?php echo __("Cancel vaccination");?>
</a>
                                        <?php }?>
                                    <?php }?>
                                </td>
                            </tr>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </tbody>
                    </table>
                </div>
            </div>
            <div id="vaccinating">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover js_dataTable">
                        <thead>
                        <tr>
                            <th colspan="5"><?php echo __("Vaccinating");?>
 (<?php echo count($_smarty_tpl->tpl_vars['child_vaccinating']->value);?>
)</th>
                        </tr>
                        <tr>
                            <th><?php echo __('No.');?>
</th>
                            <th><?php echo __("Time");?>
</th>
                            <th><?php echo __('Title');?>
</th>
                            <th><?php echo __('Status');?>
</th>
                            <th><?php echo __('Actions');?>
</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['child_vaccinating']->value, 'row', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['row']->value) {
?>
                            <tr>
                                <td align="center"><?php echo $_smarty_tpl->tpl_vars['k']->value+1;?>
</td>
                                <td align="center"><?php echo $_smarty_tpl->tpl_vars['row']->value['time'];?>
</td>
                                <td><a href = "<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/childinfo/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_parent_id'];?>
/childdevelopments/detail/<?php echo $_smarty_tpl->tpl_vars['row']->value['child_development_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['row']->value['title'];?>
</a></td>
                                <td align="center">
                                    <?php if ($_smarty_tpl->tpl_vars['row']->value['type'] == 1) {?>
                                        <?php if ($_smarty_tpl->tpl_vars['row']->value['vaccinationed']) {?>
                                            <div><strong><?php echo __('Vaccinationed');?>
</strong></div>
                                        <?php } else { ?>
                                            <div><strong><?php echo __('Not vaccinating');?>
</strong></div>
                                        <?php }?>
                                    <?php }?>
                                </td>
                                <td align="center">
                                    <?php if ($_smarty_tpl->tpl_vars['row']->value['type'] == 1) {?>
                                        <?php if (!$_smarty_tpl->tpl_vars['row']->value['vaccinationed']) {?>
                                            <a class = "btn btn-success btn-xs js_child-vaccinationed" href="#" data-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['child_development_id'];?>
" data-child="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_parent_id'];?>
"><?php echo __("Vaccination");?>
</a>
                                        <?php } else { ?>
                                            <a class = "btn btn-danger btn-xs js_child-cancel-vaccination" href="#" data-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['child_development_id'];?>
" data-child="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_parent_id'];?>
"><?php echo __("Cancel vaccination");?>
</a>
                                        <?php }?>
                                    <?php }?>
                                </td>
                            </tr>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <?php echo '<script'; ?>
>
            function stateChange() {
                setTimeout(function () {
                    $("#missed").hide();
                    $("#vaccinating").hide();
                }, 500);
            }
            stateChange();
        <?php echo '</script'; ?>
>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "detail") {?>
        <div class="panel-body with-table form-horizontal">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <tbody>
                    <tr>
                        <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Month");?>
/<?php echo __("Day");?>
</strong></td>
                        <td><?php echo $_smarty_tpl->tpl_vars['data']->value['month_push'];?>
/<?php echo $_smarty_tpl->tpl_vars['data']->value['day_push'];?>
</td>
                    </tr>
                    <tr>
                        <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Title");?>
</strong></td>
                        <td><?php echo $_smarty_tpl->tpl_vars['data']->value['title'];?>
</td>
                    </tr>
                    <?php if ($_smarty_tpl->tpl_vars['data']->value['content_type'] != 2) {?>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Content");?>
</strong></td>
                            <td><?php echo $_smarty_tpl->tpl_vars['data']->value['content'];?>
</td>
                        </tr>
                    <?php }?>
                    <?php if ($_smarty_tpl->tpl_vars['data']->value['content_type'] != 1) {?>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Link");?>
</strong></td>
                            <td><a href = "<?php echo $_smarty_tpl->tpl_vars['data']->value['link'];?>
" target="_blank"><strong><?php echo __("View detail");?>
</strong></a></td>
                        </tr>
                    <?php }?>
                    <tr>
                        <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Type");?>
</strong></td>
                        <td><?php if ($_smarty_tpl->tpl_vars['data']->value['type'] == 1) {?> <?php echo __("Vaccination");?>
 <?php } else { ?> <?php echo __("Information");?>
 <?php }?></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "referion") {?>
        <div class="panel-body with-table form-horizontal">
            <div class="row" align="center">
                <button class="js_general-show btn btn-default selected_chart"><?php echo __("General");?>
</button>
                <button class="js_missed-show btn btn-default"><?php echo __("Missed knowledge");?>
</button>
                <button class="js_vaccinating-show btn btn-default"><?php echo __("Comming knowledge");?>
</button>
            </div>
            <br/>
            <div id="general">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover js_dataTable">
                        <thead>
                        <tr>
                            <th colspan="3"><?php echo __("General");?>
 (<?php echo count($_smarty_tpl->tpl_vars['child_development']->value);?>
)</th>
                        </tr>
                        <tr>
                            <th><?php echo __('No.');?>
</th>
                            <th><?php echo __("Time");?>
</th>
                            <th><?php echo __('Title');?>
</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['child_development']->value, 'row', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['row']->value) {
?>
                            <tr>
                                <td align="center"><?php echo $_smarty_tpl->tpl_vars['k']->value+1;?>
</td>
                                <td align="center"><?php echo $_smarty_tpl->tpl_vars['row']->value['time'];?>
</td>
                                <td align="center"><a href = "<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/childinfo/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_parent_id'];?>
/childdevelopments/detail/<?php echo $_smarty_tpl->tpl_vars['row']->value['child_development_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['row']->value['title'];?>
</a></td>
                            </tr>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </tbody>
                    </table>
                </div>
            </div>
            <div id="missed">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover js_dataTable">
                        <thead>
                        <tr>
                            <th colspan="3"><?php echo __("Missed knowledge");?>
 (<?php echo count($_smarty_tpl->tpl_vars['child_info_history']->value);?>
)</th>
                        </tr>
                        <tr>
                            <th><?php echo __('No.');?>
</th>
                            <th><?php echo __("Time");?>
</th>
                            <th><?php echo __('Title');?>
</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['child_info_history']->value, 'row', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['row']->value) {
?>
                            <tr>
                                <td align="center"><?php echo $_smarty_tpl->tpl_vars['k']->value+1;?>
</td>
                                <td align="center"><?php echo $_smarty_tpl->tpl_vars['row']->value['time'];?>
</td>
                                <td align="center"><a href = "<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/childinfo/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_parent_id'];?>
/childdevelopments/detail/<?php echo $_smarty_tpl->tpl_vars['row']->value['child_development_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['row']->value['title'];?>
</a></td>
                            </tr>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </tbody>
                    </table>
                </div>
            </div>
            <div id="vaccinating">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover js_dataTable">
                        <thead>
                        <tr>
                            <th colspan="3"><?php echo __("Comming knowledge");?>
 (<?php echo count($_smarty_tpl->tpl_vars['info_future']->value);?>
)</th>
                        </tr>
                        <tr>
                            <th><?php echo __('No.');?>
</th>
                            <th><?php echo __("Time");?>
</th>
                            <th><?php echo __('Title');?>
</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['info_future']->value, 'row', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['row']->value) {
?>
                            <tr>
                                <td align="center"><?php echo $_smarty_tpl->tpl_vars['k']->value+1;?>
</td>
                                <td align="center"><?php echo $_smarty_tpl->tpl_vars['row']->value['time'];?>
</td>
                                <td align="center"><a href = "<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/childinfo/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_parent_id'];?>
/childdevelopments/detail/<?php echo $_smarty_tpl->tpl_vars['row']->value['child_development_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['row']->value['title'];?>
</a></td>
                            </tr>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <?php echo '<script'; ?>
>
            function stateChange() {
                setTimeout(function () {
                    $("#missed").hide();
                    $("#vaccinating").hide();
                }, 500);
            }
            stateChange();
        <?php echo '</script'; ?>
>
    <?php }?>
</div><?php }
}
