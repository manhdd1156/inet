<?php
/* Smarty version 3.1.31, created on 2021-05-14 12:05:04
  from "D:\workplace\mascom-edu-server\Server11\content\themes\coniu\templates\ci\school\ajax.points.searchresult.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_609e0500ce36b4_66670338',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'aca04444e2adb3ef2e05607158d3121f195b16b5' => 
    array (
      0 => 'D:\\workplace\\mascom-edu-server\\Server11\\content\\themes\\coniu\\templates\\ci\\school\\ajax.points.searchresult.tpl',
      1 => 1620968701,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_609e0500ce36b4_66670338 (Smarty_Internal_Template $_smarty_tpl) {
if (count($_smarty_tpl->tpl_vars['rows']->value) > 0) {?>
    
    
    
    
    
    
    
    <div class="table-responsive" id="example">
        <?php if ($_smarty_tpl->tpl_vars['grade']->value == 2) {?>
            <?php if ($_smarty_tpl->tpl_vars['search_with']->value == 'search_with_subject') {?>
                <?php if ($_smarty_tpl->tpl_vars['semester']->value == 0) {?>
                    <table class="table table-striped table-bordered table-pinned" style="z-index: 1">
                        <thead>
                        <tr bgcolor="#fff">
                            <th rowspan="2" class="pinned" style="padding: 8px 6px" id="width_no">#</th>
                            <th rowspan="2" nowrap="true" class="pinned"
                                style="padding: 8px 6px"><?php echo __("Student name");?>
</th>
                            <th rowspan="2" nowrap="true"
                                style="padding: 8px 6px"><?php echo __("Student code");?>
</th>
                            <th colspan="4"><?php echo __("Semester 1");?>
</th>
                            <th colspan="4"><?php echo __("Semester 2");?>
</th>
                            <th colspan="2"><?php echo __("Average semesterly");?>
</th>
                            <th colspan="2"><?php echo __("End semester");?>
</th>
                            <th rowspan="2"><?php echo __("End year");?>
</th>
                            <th rowspan="2"><?php echo __("Re exam");?>
</th>
                            <th rowspan="2"><?php echo __("Without permission");?>
</th>
                            <th rowspan="2"><?php echo __("With permission");?>
</th>
                            <th rowspan="2"><?php echo __("Status");?>
</th>
                        </tr>
                        <tr>
                            <th>M1</th>
                            <th>M2</th>
                            <th>M3</th>
                            <th><?php echo __("Last");?>
</th>
                            <th>M1</th>
                            <th>M2</th>
                            <th>M3</th>
                            <th><?php echo __("Last");?>
</th>
                            <th>S1</th>
                            <th>S2</th>
                            <th>S1</th>
                            <th>S2</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $_smarty_tpl->_assignInScope('rowIdx', 1);
?>
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['rows']->value, 'row', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['row']->value) {
?>
                            <tr>
                                <td align="center" class="pinned"><?php echo $_smarty_tpl->tpl_vars['rowIdx']->value;?>
</td>
                                <td nowrap="true" class="pinned text-bold color-blue">
                                    <strong><?php echo $_smarty_tpl->tpl_vars['row']->value['child_lastname'];?>
 <?php echo $_smarty_tpl->tpl_vars['row']->value['child_firstname'];?>
</strong></td>
                                <td nowrap="true" class="text-bold color-blue"><a
                                            href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/children/detail/<?php echo $_smarty_tpl->tpl_vars['row']->value['child_id'];?>
"><strong><?php echo $_smarty_tpl->tpl_vars['row']->value['child_code'];?>
</strong></a>
                                </td>
                                <?php $_smarty_tpl->_assignInScope('rowIdx', $_smarty_tpl->tpl_vars['rowIdx']->value+1);
?>
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['subject_key']->value, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value) {
?>
                                    <td><?php echo $_smarty_tpl->tpl_vars['row']->value[$_smarty_tpl->tpl_vars['key']->value];?>
</td>
                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                            </tr>
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </tbody>
                    </table>
                <?php } else { ?>
                    <table class="table table-striped table-bordered table-pinned" style="z-index: 1">
                        <thead>
                        <tr bgcolor="#fff">
                            <th rowspan="2" class="pinned" style="padding: 8px 6px" id="width_no">#</th>

                            <th rowspan="2" nowrap="true" class="pinned"
                                style="padding: 8px 6px"><?php echo __("Student name");?>
</th>
                            <th rowspan="2" nowrap="true" class="pinned"
                                style="padding: 8px 6px"><?php echo __("Student code");?>
</th>
                            
                            <th colspan="4"><?php echo __("Semester");?>
</th>
                            
                            
                            
                            
                            
                            
                        </tr>
                        <tr>
                            <th>M1</th>
                            <th>M2</th>
                            <th>M3</th>
                            <th><?php echo __("Last");?>
</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $_smarty_tpl->_assignInScope('rowIdx', 1);
?>
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['rows']->value, 'row', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['row']->value) {
?>
                            <tr>
                                <td align="center" class="pinned"><?php echo $_smarty_tpl->tpl_vars['rowIdx']->value;?>
</td>
                                <td nowrap="true" class="pinned text-bold color-blue">
                                    <strong><?php echo $_smarty_tpl->tpl_vars['row']->value['child_lastname'];?>
 <?php echo $_smarty_tpl->tpl_vars['row']->value['child_firstname'];?>
</strong></td>
                                <td nowrap="true" class="pinned text-bold color-blue"><a
                                            href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/children/detail/<?php echo $_smarty_tpl->tpl_vars['row']->value['child_id'];?>
"><strong><?php echo $_smarty_tpl->tpl_vars['row']->value['child_code'];?>
</strong></a>
                                </td>
                                <?php $_smarty_tpl->_assignInScope('rowIdx', $_smarty_tpl->tpl_vars['rowIdx']->value+1);
?>
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['subject_key']->value, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value) {
?>
                                    <td><?php echo $_smarty_tpl->tpl_vars['row']->value[$_smarty_tpl->tpl_vars['key']->value];?>
</td>
                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                            </tr>
                            
                            <?php if ($_smarty_tpl->tpl_vars['rowIdx']->value%10 == 0) {?>
                                <tr bgcolor="#fff">
                                    <th rowspan="2" class="pinned" style="padding: 8px 6px" id="width_no">#</th>
                                    <th rowspan="2" nowrap="true" class="pinned"
                                        style="padding: 8px 6px"><?php echo __("Student code");?>
</th>
                                    <th rowspan="2" nowrap="true" class="pinned"
                                        style="padding: 8px 6px"><?php echo __("Student name");?>
</th>
                                    <th rowspan="2" nowrap="true" class="pinned"
                                        style="padding: 8px 6px"><?php echo __("Student name");?>
</th>
                                    <th colspan="4"><?php echo __("Semester");?>
</th>
                                    
                                    
                                    
                                    
                                    
                                    
                                </tr>
                                <tr>
                                    <th>M1</th>
                                    <th>M2</th>
                                    <th>M3</th>
                                    <th><?php echo __("Last semester");?>
</th>
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                </tr>
                            <?php }?>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </tbody>
                    </table>
                <?php }?>
            <?php } elseif ($_smarty_tpl->tpl_vars['search_with']->value == 'search_with_student') {?>
                
                
                
                
                
                <strong style="float: right"><?php echo __("Status ");?>
 :
                    <?php if ($_smarty_tpl->tpl_vars['status']->value == 'Pass') {?>
                        <strong style="color:lawngreen"><?php ob_start();
echo $_smarty_tpl->tpl_vars['status']->value;
$_prefixVariable1=ob_get_clean();
echo __($_prefixVariable1);?>
</strong>
                    <?php } elseif ($_smarty_tpl->tpl_vars['status']->value == 'Fail') {?>
                        <strong style="color:red"><?php ob_start();
echo $_smarty_tpl->tpl_vars['status']->value;
$_prefixVariable2=ob_get_clean();
echo __($_prefixVariable2);?>
</strong>
                    <?php } elseif ($_smarty_tpl->tpl_vars['status']->value == 'Re-exam') {?>
                        <strong style="color:orange"><?php ob_start();
echo $_smarty_tpl->tpl_vars['status']->value;
$_prefixVariable3=ob_get_clean();
echo __($_prefixVariable3);?>
</strong>
                    <?php } else { ?>
                        <strong><?php ob_start();
echo $_smarty_tpl->tpl_vars['status']->value;
$_prefixVariable4=ob_get_clean();
echo __($_prefixVariable4);?>
</strong>
                    <?php }?>

                </strong>
                <table class="table table-striped table-bordered" style="z-index: 1">
                    <thead>
                    <tr bgcolor="#fff">
                        <th rowspan="2" class="pinned" style="padding: 8px 6px" id="width_no">#</th>
                        <th rowspan="2" nowrap="true" class="pinned"
                            style="padding: 8px 6px"><?php echo __("Subject name");?>
</th>
                        <th colspan="4"><?php echo __("Semester 1");?>
</th>
                        <th colspan="4"><?php echo __("Semester 2");?>
</th>
                    </tr>
                    <tr>
                        <th>M1</th>
                        <th>M2</th>
                        <th>M3</th>
                        <th>D1</th>
                        <th>M1</th>
                        <th>M2</th>
                        <th>M3</th>
                        <th>D2</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $_smarty_tpl->_assignInScope('rowIdx', 1);
?>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['rows']->value, 'row', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['row']->value) {
?>
                        <tr>
                            <td align="center" class="pinned"><?php echo $_smarty_tpl->tpl_vars['rowIdx']->value;?>
</td>

                            <td nowrap="true" class="pinned text-bold color-blue">
                                <strong><?php echo $_smarty_tpl->tpl_vars['row']->value['subject_name'];?>
</strong>
                            </td>
                            <?php $_smarty_tpl->_assignInScope('rowIdx', $_smarty_tpl->tpl_vars['rowIdx']->value+1);
?>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['subject_key']->value, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value) {
?>
                                <td><?php echo $_smarty_tpl->tpl_vars['row']->value[strtolower($_smarty_tpl->tpl_vars['key']->value)];?>
</td>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </tr>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                    
                    <tr>
                        <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                            <strong><?php echo __("Average monthly");?>
</strong>
                        </td>
                        <td><?php echo number_format($_smarty_tpl->tpl_vars['children_point_avgs']->value['a1'],2);?>
</td>
                        <td><?php echo number_format($_smarty_tpl->tpl_vars['children_point_avgs']->value['b1'],2);?>
</td>
                        <td><?php echo number_format($_smarty_tpl->tpl_vars['children_point_avgs']->value['c1'],2);?>
</td>
                        <td><?php echo number_format($_smarty_tpl->tpl_vars['children_point_avgs']->value['d1'],2);?>
</td>
                        <td><?php echo number_format($_smarty_tpl->tpl_vars['children_point_avgs']->value['a2'],2);?>
</td>
                        <td><?php echo number_format($_smarty_tpl->tpl_vars['children_point_avgs']->value['b2'],2);?>
</td>
                        <td><?php echo number_format($_smarty_tpl->tpl_vars['children_point_avgs']->value['c2'],2);?>
</td>
                        <td><?php echo number_format($_smarty_tpl->tpl_vars['children_point_avgs']->value['d2'],2);?>
</td>

                    </tr>
                    <tr>
                        <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                            <strong><?php echo __("Average semesterly");?>
</strong>
                        </td>
                        <td colspan="3" style="text-align: center"><?php echo number_format($_smarty_tpl->tpl_vars['children_point_avgs']->value['x1'],2);?>
</td>
                        <td></td>
                        <td colspan="3" style="text-align: center"><?php echo number_format($_smarty_tpl->tpl_vars['children_point_avgs']->value['x2'],2);?>
</td>
                        <td></td>
                    </tr>
                    
                    <tr>
                        <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                            <strong><?php echo __("End semester");?>
</strong>
                        </td>
                        <td colspan="4" style="text-align: center"><?php echo number_format($_smarty_tpl->tpl_vars['children_point_avgs']->value['e1'],2);?>
</td>
                        <td colspan="4" style="text-align: center"><?php echo number_format($_smarty_tpl->tpl_vars['children_point_avgs']->value['e2'],2);?>
</td>
                    </tr>
                    
                    <tr>
                        <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                            <strong><?php echo __("End year");?>
</strong>
                        </td>
                        <td colspan="8" style="text-align: center"><?php echo number_format($_smarty_tpl->tpl_vars['children_point_avgs']->value['y'],2);?>
</td>
                    </tr>
                    
                    <tr>
                        <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                            <strong><?php echo __("Absent has permission");?>
</strong>
                        </td>
                        <td colspan="8" style="text-align: center"><?php echo $_smarty_tpl->tpl_vars['child_absent']->value['absent_true'];?>
</td>
                    </tr>
                    
                    <tr>
                        <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                            <strong><?php echo __("Absent without permission");?>
</strong>
                        </td>
                        <td colspan="8" style="text-align: center"><?php echo $_smarty_tpl->tpl_vars['child_absent']->value['absent_false'];?>
</td>
                    </tr>
                    </tbody>
                </table>
                <strong><?php echo __("Re-Exam");?>
</strong>
                <table class="table table-striped table-bordered" style="z-index: 1;">
                    <thead>
                    <tr bgcolor="#fff">
                        <th rowspan="2" class="pinned" style="padding: 8px 6px" id="width_no">#</th>
                        <th rowspan="2" nowrap="true" class="pinned"
                            style="padding: 8px 6px"><?php echo __("Subject name");?>

                        </th>
                        <th colspan="1"><?php echo __("Point");?>
</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $_smarty_tpl->_assignInScope('rowIdx', 1);
?>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['children_subject_reexams']->value, 'row', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['row']->value) {
?>
                        <tr>
                            <td align="center" class="pinned"><?php echo $_smarty_tpl->tpl_vars['rowIdx']->value;?>
</td>
                            <td nowrap="true" class="pinned text-bold color-blue">
                                <strong><?php echo $_smarty_tpl->tpl_vars['row']->value['name'];?>
</strong>
                            </td>
                            <td style="text-align: center"><?php echo $_smarty_tpl->tpl_vars['row']->value['point'];?>
</td>
                            <?php $_smarty_tpl->_assignInScope('rowIdx', $_smarty_tpl->tpl_vars['rowIdx']->value+1);
?>
                        </tr>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                    <tr>
                        <td colspan="2" nowrap="true" class="pinned text-bold color-blue"
                            style="text-align: center">
                            <strong><?php echo __("Result Re-exam");?>
</strong>
                        </td>
                        <td colspan="3" style="text-align: center"><?php echo number_format($_smarty_tpl->tpl_vars['result_exam']->value,2);?>
</td>
                    </tr>
                    </tbody>
                </table>
            <?php } elseif ($_smarty_tpl->tpl_vars['search_with']->value == 'showDataImport') {?>
                <?php if ($_smarty_tpl->tpl_vars['semester']->value == 0) {?>
                    <table class="table table-striped table-bordered table-pinned" style="z-index: 1">
                        <thead>
                        <tr bgcolor="#fff">
                            <th rowspan="2" class="pinned" style="padding: 8px 6px" id="width_no">#</th>
                            <th rowspan="2" nowrap="true" class="pinned"
                                style="padding: 8px 6px"><?php echo __("Student name");?>
</th>
                            <th rowspan="2" nowrap="true"
                                style="padding: 8px 6px"><?php echo __("Student code");?>
</th>

                            
                            
                            <th colspan="4"><?php echo __("Semester 1");?>
</th>
                            <th colspan="4"><?php echo __("Semester 2");?>
</th>
                            <th rowspan="2"><?php echo __("Re exam");?>
</th>
                        </tr>
                        <tr>
                            <th>M1</th>
                            <th>M2</th>
                            <th>M3</th>
                            <th><?php echo __("Last");?>
</th>
                            <th>M1</th>
                            <th>M2</th>
                            <th>M3</th>
                            <th><?php echo __("Last");?>
</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $_smarty_tpl->_assignInScope('rowIdx', 1);
?>
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['rows']->value, 'row', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['row']->value) {
?>
                            <tr>
                                <td align="center" class="pinned"><?php echo $_smarty_tpl->tpl_vars['rowIdx']->value;?>
</td>
                                <td nowrap="true" class="pinned text-bold color-blue">
                                    <strong><?php echo $_smarty_tpl->tpl_vars['row']->value['child_lastname'];?>
 <?php echo $_smarty_tpl->tpl_vars['row']->value['child_firstname'];?>
</strong></td>
                                <td nowrap="true" class="text-bold color-blue"><a
                                            href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/children/detail/<?php echo $_smarty_tpl->tpl_vars['row']->value['child_id'];?>
"><strong><?php echo $_smarty_tpl->tpl_vars['row']->value['child_code'];?>
</strong></a>
                                </td>

                                
                                
                                <?php $_smarty_tpl->_assignInScope('rowIdx', $_smarty_tpl->tpl_vars['rowIdx']->value+1);
?>
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['subject_key']->value, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value) {
?>
                                    
                                    <?php if ($_smarty_tpl->tpl_vars['key']->value == 'reexam' && !$_smarty_tpl->tpl_vars['row']->value['is_reexam']) {?>
                                        <td><?php echo $_smarty_tpl->tpl_vars['row']->value[$_smarty_tpl->tpl_vars['key']->value];?>
</td>
                                    <?php } else { ?>
                                        <td><input name="point" type="number" min="0" style="max-width: 50px"
                                                   value="<?php echo $_smarty_tpl->tpl_vars['row']->value[$_smarty_tpl->tpl_vars['key']->value];?>
"/></td>
                                    <?php }?>
                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                            </tr>
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </tbody>
                    </table>
                <?php } else { ?>
                    <table class="table table-striped table-bordered table-pinned" style="z-index: 1">
                        <thead>
                        <tr bgcolor="#fff">
                            <th rowspan="2" class="pinned" style="padding: 8px 6px" id="width_no">#</th>
                            <th rowspan="2" nowrap="true" class="pinned"
                                style="padding: 8px 6px"><?php echo __("Student name");?>
</th>
                            <th rowspan="2" nowrap="true" class="pinned"
                                style="padding: 8px 6px"><?php echo __("Student code");?>
</th>
                            
                            <th colspan="4"><?php echo __("Semester");?>
</th>
                            
                            
                            
                            
                            
                            
                        </tr>
                        <tr>
                            <th>M1</th>
                            <th>M2</th>
                            <th>M3</th>
                            <th><?php echo __("Last");?>
</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $_smarty_tpl->_assignInScope('rowIdx', 1);
?>
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['rows']->value, 'row', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['row']->value) {
?>
                            <tr>
                                <td align="center" class="pinned"><?php echo $_smarty_tpl->tpl_vars['rowIdx']->value;?>
</td>
                                <td nowrap="true" class="pinned text-bold color-blue">
                                    <strong><?php echo $_smarty_tpl->tpl_vars['row']->value['child_lastname'];?>
 <?php echo $_smarty_tpl->tpl_vars['row']->value['child_firstname'];?>
</strong></td>
                                <td nowrap="true" class="pinned text-bold color-blue"><a
                                            href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/children/detail/<?php echo $_smarty_tpl->tpl_vars['row']->value['child_id'];?>
"><strong><?php echo $_smarty_tpl->tpl_vars['row']->value['child_code'];?>
</strong></a>
                                </td>
                                <?php $_smarty_tpl->_assignInScope('rowIdx', $_smarty_tpl->tpl_vars['rowIdx']->value+1);
?>
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['subject_key']->value, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value) {
?>
                                    
                                    <td><input type="number" style="max-width: 50px" value="<?php echo $_smarty_tpl->tpl_vars['row']->value[$_smarty_tpl->tpl_vars['key']->value];?>
"/></td>
                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                            </tr>
                            
                            <?php if ($_smarty_tpl->tpl_vars['rowIdx']->value%10 == 0) {?>
                                <tr bgcolor="#fff">
                                    <th rowspan="2" class="pinned" style="padding: 8px 6px" id="width_no">#</th>
                                    <th rowspan="2" nowrap="true" class="pinned"
                                        style="padding: 8px 6px"><?php echo __("Student code");?>
</th>
                                    <th rowspan="2" nowrap="true" class="pinned"
                                        style="padding: 8px 6px"><?php echo __("Student name");?>
</th>
                                    <th rowspan="2" nowrap="true" class="pinned"
                                        style="padding: 8px 6px"><?php echo __("Student name");?>
</th>
                                    <th colspan="4"><?php echo __("Semester");?>
</th>
                                    
                                    
                                    
                                    
                                    
                                    
                                </tr>
                                <tr>
                                    <th>M1</th>
                                    <th>M2</th>
                                    <th>M3</th>
                                    <th><?php echo __("Last semester");?>
</th>
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                </tr>
                            <?php }?>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </tbody>
                    </table>
                <?php }?>
            <?php }?>
        <?php } elseif ($_smarty_tpl->tpl_vars['grade']->value == 1) {?>
            <table class="table table-striped table-bordered table-pinned" style="z-index: 1">
                <thead>
                <tr bgcolor="#fff">
                    <th class="pinned" style="padding: 8px 6px" id="width_no">#</th>
                    <th nowrap="true" class="pinned" style="padding: 8px 6px"><?php echo __("Student code");?>
</th>
                    <th nowrap="true" class="pinned" style="padding: 8px 6px"><?php echo __("Last name");?>
</th>
                    <th nowrap="true" class="pinned" style="padding: 8px 6px"><?php echo __("First name");?>
</th>
                    <th><?php echo __("Nhận xét");?>
</th>
                    <th><?php echo __("Năng lực");?>
</th>
                    <th><?php echo __("Phẩm chất");?>
</th>
                    <th><?php echo __("KTCK");?>
</th>
                    <th><?php echo __("XLCK");?>
</th>
                </tr>
                </thead>
                <tbody>
                <?php $_smarty_tpl->_assignInScope('rowIdx', 1);
?>
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['rows']->value, 'row', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['row']->value) {
?>
                    <tr>
                        <td align="center" class="pinned"><?php echo $_smarty_tpl->tpl_vars['rowIdx']->value;?>
</td>
                        <td nowrap="true" class="pinned text-bold color-blue"><a
                                    href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/children/detail/<?php echo $_smarty_tpl->tpl_vars['row']->value['child_id'];?>
"><strong><?php echo $_smarty_tpl->tpl_vars['row']->value['child_code'];?>
</strong></a>
                        </td>
                        <td nowrap="true" class="pinned text-bold color-blue"><strong><?php echo $_smarty_tpl->tpl_vars['row']->value['child_lastname'];?>
</strong>
                        </td>
                        <td nowrap="true" class="pinned text-bold color-blue"><strong><?php echo $_smarty_tpl->tpl_vars['row']->value['child_firstname'];?>
</strong>
                        </td>
                        <?php $_smarty_tpl->_assignInScope('rowIdx', $_smarty_tpl->tpl_vars['rowIdx']->value+1);
?>
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['subject_key']->value, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value) {
?>
                            <td><?php echo $_smarty_tpl->tpl_vars['row']->value[$_smarty_tpl->tpl_vars['key']->value];?>
</td>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                    </tr>
                    
                    <?php if ($_smarty_tpl->tpl_vars['rowIdx']->value%10 == 0) {?>
                        <tr bgcolor="#fff">
                            <th class="pinned" style="padding: 8px 6px" id="width_no">#</th>
                            <th nowrap="true" class="pinned" style="padding: 8px 6px"><?php echo __("Student code");?>
</th>
                            <th nowrap="true" class="pinned" style="padding: 8px 6px"><?php echo __("Last name");?>
</th>
                            <th nowrap="true" class="pinned" style="padding: 8px 6px"><?php echo __("First name");?>
</th>
                            <th><?php echo __("Nhận xét");?>
</th>
                            <th><?php echo __("Năng lực");?>
</th>
                            <th><?php echo __("Phẩm chất");?>
</th>
                            <th><?php echo __("KTCK");?>
</th>
                            <th><?php echo __("XLCK");?>
</th>
                        </tr>
                    <?php }?>
                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                </tbody>
            </table>
        <?php }?>
    </div>
<?php } else { ?>
    <div align="center"><strong style="color: red"><?php echo __("Chưa có thông tin điểm");?>
</strong></div>
<?php }?>

<?php echo '<script'; ?>
 type="text/javascript">

    var $table = $('.table-pinned');
    var $fixedColumn = $table.clone().insertBefore($table).addClass('fixed-column');
    $fixedColumn.find('th').each(function (i, elem) {
        $(this).width($table.find('th:eq(' + i + ')').width());
    });
    $fixedColumn.find('td').each(function (i, elem) {
        $(this).width($table.find('td:eq(' + i + ')').width());
    });
    $fixedColumn.find('th,td').not('.pinned').hide();
    $fixedColumn.find('[id]').each(function () {
        $(this).removeAttr('id');
    });

    $fixedColumn.find('tr').each(function (i, elem) {
        $(this).height($table.find('tr:eq(' + i + ')').height());
    });

    $(window).resize(function () {
        $fixedColumn.find('tr').each(function (i, elem) {
            $(this).height($table.find('tr:eq(' + i + ')').height());
        });
        $fixedColumn.find('td').each(function (i, elem) {
            $(this).addClass('white-space_nowrap');
            $(this).width($table.find('td:eq(' + i + ')').width());
        });
    });
    //$fixedColumn.find('td').addClass('white-space_nowrap');
    //    $("#right").on("click", function() {
    //        var leftPos = $('#example').scrollLeft();
    //        console.log(leftPos);
    //        $("#example").animate({
    //            scrollLeft: leftPos - 200
    //        }, 800);
    //    });
    $('.right').click(function (event) {
        var width_col = $('.table-pinned').find('td:eq(' + 4 + ')').width();
        var pos = $('#example').scrollLeft() + width_col + 100;
        return false;
        $('#example').scrollLeft(pos);
    });
    $('.left').click(function (event) {
        var width_col = $('.table-pinned').find('td:eq(' + 4 + ')').width();
        var pos = $('#example').scrollLeft() - width_col - 100;
        $('#example').scrollLeft(pos);
    });

    jQuery(function ($) {
        function fixDiv() {
            var $cache = $('#getFixed');
            var $button = $('#table_button');
            if ($(window).scrollTop() > 100) {
                $cache.css({
                    'position': 'fixed',
                    'top': '50px'
                });
                $cache.width($('#school_list_point').width() - 1);
            } else
                $cache.css({
                    'position': 'relative',
                    'top': 'auto'
                });

            if ($(window).scrollTop() > 100)
                $button.css({
                    'position': 'fixed',
                    'top': '90px'
                });
            else
                $button.css({
                    'position': 'relative',
                    'top': 'auto'
                });
        }

        $(window).scroll(fixDiv);
        fixDiv();
    });
<?php echo '</script'; ?>
><?php }
}
