<?php
/* Smarty version 3.1.31, created on 2021-05-13 09:14:40
  from "D:\workplace\mascom-edu-server\Server11\content\themes\coniu\templates\ci\class\class.children.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_609c8b9097e0b7_04453317',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '5c3c6f4bc928c3fb60c7025a154c37656daa43df' => 
    array (
      0 => 'D:\\workplace\\mascom-edu-server\\Server11\\content\\themes\\coniu\\templates\\ci\\class\\class.children.tpl',
      1 => 1620722160,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:ci/ajax.parentlist.tpl' => 2,
    'file:ci/class/ajax.childeditlist.tpl' => 1,
    'file:ci/class/ajax.class.chartsearch.tpl' => 1,
    'file:ci/ajax.class.journal.list.tpl' => 1,
  ),
),false)) {
function content_609c8b9097e0b7_04453317 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <i class="fa fa-child fa-fw fa-lg pr10"></i>
        <?php echo __("Student");?>

        <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == "edit") {?>
            &rsaquo; <?php echo $_smarty_tpl->tpl_vars['child']->value['child_name'];?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "detail") {?>
            &rsaquo; <?php echo $_smarty_tpl->tpl_vars['child']->value['child_name'];?>

            <div class="pull-right flip">
                <a href="https://blog.coniu.vn/huong-dan-quan-ly-giao-vien-tao-cac-buoc-de-nhap-thong-tin-tre-tren-website-coniu/" class="btn btn-info btn_guide" target="_blank">
                    <i class="fa fa-info-circle" aria-hidden="true"></i> <?php echo __("Guide");?>

                </a>
                <?php if (isset($_smarty_tpl->tpl_vars['data']->value['status']) && $_smarty_tpl->tpl_vars['data']->value['status'] == 0) {?>
                    <button class="btn btn-default js_paid_child" data-username = "<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-id = <?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
>
                        <?php echo __("Paid student");?>

                    </button>
                <?php }?>
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/children/health/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
" class="btn btn-default">
                    <i class="fa fa-heartbeat" aria-hidden="true"></i> <?php echo __("Health information");?>

                </a>
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/children/journal/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
" class="btn btn-default">
                    <i class="fa fa-image"></i> <?php echo __("List of diary");?>

                </a>
            </div>
        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
            &rsaquo; <?php echo __("Lists");?>

            <div class="pull-right flip">
                <a href="https://blog.coniu.vn/huong-dan-quan-ly-giao-vien-tao-cac-buoc-de-nhap-thong-tin-tre-tren-website-coniu/" class="btn btn-info btn_guide" target="_blank">
                    <i class="fa fa-info-circle" aria-hidden="true"></i> <?php echo __("Guide");?>

                </a>
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/children/addhealthindex" class="btn btn-default">
                    <i class="fa fa-plus"></i> <?php echo __("Add health index");?>

                </a>
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/children/add" class="btn btn-default">
                    <i class="fa fa-plus"></i> <?php echo __("Add New");?>

                </a>
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/children/addexisting" class="btn btn-default">
                    <i class="fa fa-plus"></i> <?php echo __("Add existing child");?>

                </a>
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/children/adddiary" class="btn btn-default">
                    <i class="fa fa-image"></i> <?php echo __("Add diary");?>

                </a>
            </div>
        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
            &rsaquo; <?php echo __("Add New");?>

            <div class="pull-right flip">
                <a href="https://blog.coniu.vn/huong-dan-quan-ly-giao-vien-tao-cac-buoc-de-nhap-thong-tin-tre-tren-website-coniu/" class="btn btn-info btn_guide" target="_blank">
                    <i class="fa fa-info-circle" aria-hidden="true"></i> <?php echo __("Guide");?>

                </a>
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/children" class="btn btn-default">
                    <?php echo __("Lists");?>

                </a>
            </div>
        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "import") {?>
            &rsaquo; <?php echo __("Import from Excel file");?>

            <div class="pull-right flip">
                <a href="https://blog.coniu.vn/huong-dan-quan-ly-giao-vien-tao-cac-buoc-de-nhap-thong-tin-tre-tren-website-coniu/" class="btn btn-info btn_guide" target="_blank">
                    <i class="fa fa-info-circle" aria-hidden="true"></i> <?php echo __("Guide");?>

                </a>
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/children" class="btn btn-default">
                    <?php echo __("Lists");?>

                </a>
            </div>
        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "health") {?>
            &rsaquo; <?php echo $_smarty_tpl->tpl_vars['child']->value['child_name'];?>

            <div class="pull-right flip">
                <a href="https://blog.coniu.vn/huong-dan-giao-vien-them-chi-suc-khoe-tren-website-coniu/" class="btn btn-info btn_guide" target="_blank">
                    <i class="fa fa-info-circle" aria-hidden="true"></i> <?php echo __("Guide");?>

                </a>
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/children/addgrowth/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
" class="btn btn-default">
                    <i class="fa fa-plus" aria-hidden="true"></i> <?php echo __("Add New");?>

                </a>
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/children/detail/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
" class="btn btn-default">
                    <i class="fa fa-info" aria-hidden="true"></i> <?php echo __("Student information detail");?>

                </a>
            </div>
        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "addgrowth") {?>
            &rsaquo; <?php echo $_smarty_tpl->tpl_vars['child']->value['child_name'];?>

            <div class="pull-right flip">
                <a href="https://blog.coniu.vn/huong-dan-giao-vien-them-chi-suc-khoe-tren-website-coniu/" class="btn btn-info btn_guide" target="_blank">
                    <i class="fa fa-info-circle" aria-hidden="true"></i> <?php echo __("Guide");?>

                </a>
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/children/detail/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
" class="btn btn-default">
                    <i class="fa fa-info" aria-hidden="true"></i> <?php echo __("Student information detail");?>

                </a>
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/children/health/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
" class="btn btn-default">
                    <i class="fa fa-heartbeat" aria-hidden="true"></i> <?php echo __("Health information");?>

                </a>
            </div>
        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "editgrowth") {?>
            &rsaquo; <?php echo $_smarty_tpl->tpl_vars['child']->value['child_name'];?>

            <div class="pull-right flip">
                <a href="https://blog.coniu.vn/huong-dan-giao-vien-them-chi-suc-khoe-tren-website-coniu/" class="btn btn-info btn_guide" target="_blank">
                    <i class="fa fa-info-circle" aria-hidden="true"></i> <?php echo __("Guide");?>

                </a>
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/children/detail/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
" class="btn btn-default">
                    <i class="fa fa-info" aria-hidden="true"></i> <?php echo __("Student information detail");?>

                </a>
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/children/health/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
" class="btn btn-default">
                    <i class="fa fa-heartbeat" aria-hidden="true"></i> <?php echo __("Health information");?>

                </a>
            </div>
        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "addhealthindex") {?>
            &rsaquo; <?php echo __("Add health index");?>

            <div class="pull-right flip">
                <a href="https://blog.coniu.vn/huong-dan-giao-vien-them-chi-suc-khoe-tren-website-coniu/" class="btn btn-info btn_guide" target="_blank">
                    <i class="fa fa-info-circle" aria-hidden="true"></i> <?php echo __("Guide");?>

                </a>
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/children" class="btn btn-default">
                    <i class="fa fa-list" aria-hidden="true"></i> <?php echo __("Lists");?>

                </a>
            </div>
        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "addexisting") {?>
            &rsaquo; <?php echo __('Add Existing Account');?>

            <div class="pull-right flip">
                <a href="https://blog.coniu.vn/huong-dan-quan-ly-tao-cac-buoc-de-nhap-thong-tin-tre-tren-website-coniu/" class="btn btn-info btn_guide" target="_blank">
                    <i class="fa fa-info-circle" aria-hidden="true"></i> <?php echo __("Guide");?>

                </a>
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/children" class="btn btn-default">
                    <?php echo __("Lists");?>

                </a>
            </div>
        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "journal") {?>
            &rsaquo; <?php echo __('List of diary');?>
 &rsaquo; <?php echo $_smarty_tpl->tpl_vars['child']->value['child_name'];?>

            <div class="pull-right flip">
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/children/detail/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
" class="btn btn-default">
                    <i class="fa fa-info" aria-hidden="true"></i> <?php echo __("Student information detail");?>

                </a>
                <?php if ($_smarty_tpl->tpl_vars['child']->value['status'] == 1) {?>
                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/children/addphoto/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
" class="btn btn-default">
                        <i class="fa fa-image"></i> <?php echo __("Add diary");?>

                    </a>
                <?php }?>
            </div>
        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "addphoto") {?>
            &rsaquo; <?php echo __("Add diary");?>

            <div class="pull-right flip">
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/children/detail/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
" class="btn btn-default">
                    <i class="fa fa-info" aria-hidden="true"></i> <?php echo __("Student information detail");?>

                </a>
                <?php if ($_smarty_tpl->tpl_vars['child']->value['status'] == 1) {?>
                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/children/journal/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
" class="btn btn-default">
                        <i class="fa fa-image"></i> <?php echo __("List of diary");?>

                    </a>
                <?php }?>
            </div>
        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "adddiary") {?>
            &rsaquo; <?php echo __("Add diary");?>

            <div class="pull-right flip">
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/children" class="btn btn-default">
                    <i class="fa fa-list" aria-hidden="true"></i> <?php echo __("Lists");?>

                </a>
            </div>
        <?php }?>
    </div>
    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
        <div class="panel-body with-table">
            <div class="table-responsive">
                <div><strong><?php echo __("Children list");?>
&nbsp;(<?php echo count($_smarty_tpl->tpl_vars['rows']->value);?>
)</strong></div>
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th><?php echo __("#");?>
</th>
                        <th><?php echo __("Full name");?>
</th>
                        <th><?php echo __("Gender");?>
</th>
                        <th><?php echo __("Birthdate");?>
</th>
                        <th><?php echo __("Parent phone");?>
</th>
                        <th><?php echo __("Parent");?>
</th>
                        <th><?php echo __("Actions");?>
</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['rows']->value, 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
                            <tr>
                                <td align="center" class="align-middle"> <?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
 </td>
                                <td class="align-middle"><a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/children/detail/<?php echo $_smarty_tpl->tpl_vars['row']->value['child_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['row']->value['child_name'];?>
</a></td>
                                <td align="center" class="align-middle"><?php if ($_smarty_tpl->tpl_vars['row']->value['gender'] == @constant('MALE')) {
echo __("Male");
} else {
echo __("Female");
}?></td>
                                <td align="center" class="align-middle"><?php echo $_smarty_tpl->tpl_vars['row']->value['birthday'];?>
</td>
                                <td align="center" class="align-middle">
                                    <?php if ($_smarty_tpl->tpl_vars['row']->value['parent_phone'] != '') {?>
                                        <?php echo $_smarty_tpl->tpl_vars['row']->value['parent_phone'];?>

                                    <?php } else { ?>
                                        <?php echo $_smarty_tpl->tpl_vars['row']->value['parent_phone_dad'];?>

                                    <?php }?>
                                </td>
                                <td>
                                    <?php if (count($_smarty_tpl->tpl_vars['row']->value['parent']) == 0) {?>
                                        <?php echo __("No parent");?>

                                    <?php } else { ?>
                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['row']->value['parent'], '_user');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['_user']->value) {
?>
                                            <span class="name js_user-popover" data-uid="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_id'];?>
">
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_name'];?>
"><?php echo $_smarty_tpl->tpl_vars['_user']->value['user_fullname'];?>
</a>
                                            </span>
                                            <?php if ($_smarty_tpl->tpl_vars['_user']->value['user_id'] != $_smarty_tpl->tpl_vars['user']->value->_data['user_id']) {?>
                                                <a href="#" class="far fa-comments fa-lg js_chat-start" data-name="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_fullname'];?>
" data-uid="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_id'];?>
"></a>
                                            <?php }?>
                                            <?php if ($_smarty_tpl->tpl_vars['_user']->value['suggest'] == 1) {?>
                                                <button class="btn btn-xs btn-danger js_class-approve-parent" id="button_<?php echo $_smarty_tpl->tpl_vars['row']->value['child_id'];?>
_<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_id'];?>
" data-parent="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_id'];?>
" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['child_id'];?>
"><?php echo __("Approve");?>
</button>
                                            <?php }?>
                                            <br/>
                                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                    <?php }?>
                                </td>
                                <td align = "center" class="align-middle">
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/children/edit/<?php echo $_smarty_tpl->tpl_vars['row']->value['child_id'];?>
" class="btn btn-xs btn-default"><?php echo __("Edit");?>
</a>
                                </td>
                            </tr>
                            <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>


                        <?php if (count($_smarty_tpl->tpl_vars['rows']->value) == 0) {?>
                            <tr class="odd">
                                <td valign="top" align="center" colspan="7" class="dataTables_empty">
                                    <?php echo __("No data available in table");?>

                                </td>
                            </tr>
                        <?php }?>
                    </tbody>
                </table>
            </div>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "edit") {?>
        <div class="panel-body">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/class/boclass_child.php">
                <input type="hidden" name="username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                <input type="hidden" name="child_id" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
"/>
                <input type="hidden" name="child_parent_id" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_parent_id'];?>
"/>
                <input type="hidden" name="child_code" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_code'];?>
"/>
                <input type="hidden" name="do" value="edit"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Full name");?>
 (*)</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="last_name" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['last_name'];?>
" placeholder="<?php echo __("Last name");?>
" required maxlength="34">
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="first_name" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['first_name'];?>
" placeholder="<?php echo __("First name");?>
" required maxlength="15">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Gender");?>
 (*)</label>
                    <div class="col-sm-3">
                        <select name="gender" id="gender" class="form-control">
                            <option value="<?php echo @constant('MALE');?>
" <?php if ($_smarty_tpl->tpl_vars['child']->value['gender'] == @constant('MALE')) {?>selected<?php }?>><?php echo __("Male");?>
</option>
                            <option value="<?php echo @constant('FEMALE');?>
" <?php if ($_smarty_tpl->tpl_vars['child']->value['gender'] == @constant('FEMALE')) {?>selected<?php }?>><?php echo __("Female");?>
</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Birthdate");?>
 (*)</label>
                    <div class='col-sm-4'>
                        <div class='input-group date' id='birthdate_picker'>
                            <input type='text' name="birthday" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['birthday'];?>
" class="form-control" placeholder="<?php echo __("Birthdate");?>
 (*)" required/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Parent");?>
</label>
                    <div class="col-sm-9">
                        <input name="search-parent" id="search-parent" type="text" class="form-control" placeholder="<?php echo __("Enter username, email or fullname to search");?>
" autocomplete="off">
                        <div id="search-parent-results" class="dropdown-menu dropdown-widget dropdown-search">
                            <div class="dropdown-widget-header">
                                <?php echo __("Search Results");?>

                            </div>
                            <div class="dropdown-widget-body">
                                <div class="loader loader_small ptb10"></div>
                            </div>
                        </div>
                        <br/>
                        <div class="col-sm-9" id="parent_list" name="parent_list">
                            <?php if (count($_smarty_tpl->tpl_vars['parent']->value) > 0) {?>
                                <?php $_smarty_tpl->_subTemplateRender('file:ci/ajax.parentlist.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('results'=>$_smarty_tpl->tpl_vars['parent']->value), 0, false);
?>

                            <?php } else { ?>
                                <?php echo __("No parent");?>

                            <?php }?>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Mother's name");?>
</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="parent_name" name="parent_name" placeholder="<?php echo __("Mother's name");?>
" maxlength="50" value="<?php echo convertText4Web($_smarty_tpl->tpl_vars['child']->value['parent_name']);?>
">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Telephone");?>
</label>
                    <div class="col-xs-6">
                        <input type="text" class="form-control" id="parent_phone" name="parent_phone" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['parent_phone'];?>
" placeholder="<?php echo __("Telephone");?>
" maxlength="50">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Job");?>
</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="parent_job" name="parent_job" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['parent_job'];?>
" placeholder="<?php echo __("Job");?>
" maxlength="50">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Father's name");?>
</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="parent_name_dad" name="parent_name_dad" placeholder="<?php echo __("Father's name");?>
" maxlength="50" value="<?php echo convertText4Web($_smarty_tpl->tpl_vars['child']->value['parent_name_dad']);?>
">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Telephone");?>
</label>
                    <div class="col-xs-6">
                        <input type="text" class="form-control" id="parent_phone_dad" name="parent_phone_dad" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['parent_phone_dad'];?>
" placeholder="<?php echo __("Telephone");?>
" maxlength="50">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Job");?>
</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="parent_job_dad" name="parent_job_dad" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['parent_job_dad'];?>
" placeholder="<?php echo __("Job");?>
" maxlength="50">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Parent email");?>
</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="parent_email" name="parent_email" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['parent_email'];?>
" placeholder="<?php echo __("Parent email");?>
" maxlength="50">
                        <div><?php echo __("If parent do not have any email, please let it empty");?>
.</div>
                    </div>
                    <div class="col-sm-4">
                        <input type="checkbox" value="1" name="create_parent_account" id="create_parent_account" <?php if (count($_smarty_tpl->tpl_vars['parent']->value) > 0) {?>disabled<?php } else { ?>checked<?php }?>>&nbsp;<?php echo __("Auto-create parent account");?>
?
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Address");?>
</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="address" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['address'];?>
" placeholder="<?php echo __("Address");?>
" maxlength="250">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Study start date");?>
 (*)</label>
                    <div class='col-sm-4'>
                        <div class='input-group date' id='beginat_datepicker'>
                            <input type='text' name="begin_at" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['begin_at'];?>
" class="form-control" placeholder="<?php echo __("Study start date");?>
"/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Description");?>
</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="description" placeholder="<?php echo __("Write about your child...");?>
" maxlength="300"><?php echo $_smarty_tpl->tpl_vars['child']->value['description'];?>
</textarea>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30"><?php echo __("Save");?>
</button>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
        <div class="panel-body">
            <form class="js_ajax-add-child-form form-horizontal" data-url="ci/bo/class/boclass_child.php">
                <input type="hidden" name="username" id="username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                <input type="hidden" name="do" value="add"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Child code");?>
</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="child_code" id="child_code" placeholder="<?php echo __("Child code");?>
" disabled autofocus maxlength="30">
                    </div>
                    <div class="col-sm-4">
                        <input type="checkbox" name="code_auto" id="code_auto" value="1" checked/> <?php echo __("Generate automatically");?>

                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Full name");?>
 (*)</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="last_name" id="last_name" placeholder="<?php echo __("Last name");?>
" required maxlength="34" autofocus>
                    </div>
                    <div class="col-sm-2">
                        <input type="text" class="form-control" name="first_name" id="first_name" placeholder="<?php echo __("First name");?>
" required maxlength="15">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Gender");?>
 (*)</label>
                    <div class="col-sm-3">
                        <select name="gender" id="gender" class="form-control">
                            <option value="<?php echo @constant('MALE');?>
"><?php echo __("Male");?>
</option>
                            <option value="<?php echo @constant('FEMALE');?>
"><?php echo __("Female");?>
</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Birthdate");?>
 (*)</label>
                    <div class='col-sm-3'>
                        <div class='input-group date' id='birthdate_picker'>
                            <input type='text' name="birthday" id="birthday" class="form-control" placeholder="<?php echo __("Birthdate");?>
 (*)" required/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Parent");?>
</label>
                    <div class="col-sm-7">
                        <input name="search-parent" id="search-parent" type="text" class="form-control" placeholder="<?php echo __("Enter username, email or fullname to search");?>
" autocomplete="off">
                        <div id="search-parent-results" class="dropdown-menu dropdown-widget dropdown-search">
                            <div class="dropdown-widget-header">
                                <?php echo __("Search Results");?>

                            </div>
                            <div class="dropdown-widget-body">
                                <div class="loader loader_small ptb10"></div>
                            </div>
                        </div>
                        <div><?php echo __("Enter at least 4 characters");?>
.</div>
                        <br/>
                        <div class="col-sm-9" id="parent_list" name="parent_list"></div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Mother's name");?>
</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="parent_name" name="parent_name" placeholder="<?php echo __("Mother's name");?>
" maxlength="50">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Telephone");?>
</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="parent_phone" id="parent_phone" placeholder="<?php echo __("Telephone");?>
" maxlength="50">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Job");?>
</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="parent_job" id="parent_job" placeholder="<?php echo __("Job");?>
" maxlength="500">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Father's name");?>
</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="parent_name_dad" name="parent_name_dad" placeholder="<?php echo __("Father's name");?>
" maxlength="50">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Telephone");?>
</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="parent_phone_dad" id="parent_phone_dad" placeholder="<?php echo __("Telephone");?>
" maxlength="50">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Job");?>
</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="parent_job_dad" id="parent_job_dad" placeholder="<?php echo __("Job");?>
" maxlength="500">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Parent email");?>
</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="parent_email" name="parent_email" placeholder="<?php echo __("Parent email");?>
" maxlength="100">
                        <div><?php echo __("If parent do not have any email, please let it empty");?>
.</div>
                    </div>
                    <div class="col-sm-4">
                        <input type="checkbox" value="1" name="create_parent_account" id="create_parent_account" checked>&nbsp;<?php echo __("Auto-create parent account");?>
?
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Address");?>
</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="address" id="address" placeholder="<?php echo __("Address");?>
" maxlength="250">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Class");?>
 (*)</label>
                    <div class="col-sm-6">
                        <select name="class_id" id="class_id" class="form-control">
                            <option value="<?php echo $_smarty_tpl->tpl_vars['class']->value['group_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['class']->value['group_title'];?>
</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Study start date");?>
 (*)</label>
                    <div class='col-sm-3'>
                        <div class='input-group date' id='beginat_datepicker'>
                            <input type='text' name="begin_at" class="form-control" placeholder="<?php echo __("Study start date");?>
"/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Description");?>
</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="description" id="description" placeholder="<?php echo __("Write about your child...");?>
" maxlength="300"></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-3 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30"><?php echo __("Save");?>
</button>
                    </div>
                    <div class="col-sm-4">
                        <a href="#" class="btn btn-default js_add_child_clear"><?php echo __("Clear Data");?>
</a>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "import") {?>
        <div class="panel-body">
            <form class="form-horizontal" action="#" enctype="multipart/form-data" method="post" id="import_excel_form">
                <input type="hidden" name="username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                <input type="hidden" name="do" value="import"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Select Class");?>
 (*)</label>
                    <div class="col-sm-5">
                        <select name="class_id" id="class_id" class="form-control" required>
                            <option value="<?php echo $_smarty_tpl->tpl_vars['class']->value['group_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['class']->value['group_title'];?>
</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Select Excel file");?>
</label>
                    <div class="col-sm-5">
                        <input type="file" name="file" id="file"/>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-3 col-sm-offset-3">
                        <button type="submit" id="submit_id" class="btn btn-primary padrl30"><?php echo __("Save");?>
</button>
                    </div>
                </div>
                <br/>
                <div class="table-responsive" id="result_info" name="result_info"></div>
                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "detail") {?>
        <div class="panel-body with-table">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <tbody>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Full name");?>
</strong></td>
                            <td><?php echo $_smarty_tpl->tpl_vars['child']->value['child_name'];?>
</td>
                        </tr>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Child code");?>
</strong></td>
                            <td><?php echo $_smarty_tpl->tpl_vars['child']->value['child_code'];?>
</td>
                        </tr>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Nickname");?>
</strong></td>
                            <td><?php echo $_smarty_tpl->tpl_vars['child']->value['child_nickname'];?>
</td>
                        </tr>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Gender");?>
</strong></td>
                            <td><?php if ($_smarty_tpl->tpl_vars['child']->value['gender'] == @constant('MALE')) {
echo __("Male");
} else {
echo __("Female");
}?></td>
                        </tr>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Birthdate");?>
</strong></td>
                            <td><?php echo $_smarty_tpl->tpl_vars['child']->value['birthday'];?>
</td>
                        </tr>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Parent");?>
</strong></td>
                            <td>
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['parent']->value, '_user');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['_user']->value) {
?>
                                    <span class="name js_user-popover" data-uid="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_id'];?>
">
                                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_name'];?>
"><?php echo $_smarty_tpl->tpl_vars['_user']->value['user_fullname'];?>
</a>
                                    </span>
                                    <?php if ($_smarty_tpl->tpl_vars['_user']->value['user_id'] != $_smarty_tpl->tpl_vars['user']->value->_data['user_id']) {?>
                                        <a href="#" class="far fa-comments fa-lg js_chat-start" data-name="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_fullname'];?>
" data-uid="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_id'];?>
"></a>
                                    <?php }?>
                                    <br/>
                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                            </td>
                        </tr>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Mother's name");?>
</strong></td>
                            <td><?php echo $_smarty_tpl->tpl_vars['child']->value['parent_name'];?>
</td>
                        </tr>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Telephone");?>
</strong></td>
                            <td><?php echo $_smarty_tpl->tpl_vars['child']->value['parent_phone'];?>
</td>
                        </tr>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Job");?>
</strong></td>
                            <td><?php echo $_smarty_tpl->tpl_vars['child']->value['parent_job'];?>
</td>
                        </tr>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Father's name");?>
</strong></td>
                            <td><?php echo $_smarty_tpl->tpl_vars['child']->value['parent_name_dad'];?>
</td>
                        </tr>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Telephone");?>
</strong></td>
                            <td><?php echo $_smarty_tpl->tpl_vars['child']->value['parent_phone_dad'];?>
</td>
                        </tr>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Job");?>
</strong></td>
                            <td><?php echo $_smarty_tpl->tpl_vars['child']->value['parent_job_dad'];?>
</td>
                        </tr>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Parent email");?>
</strong></td>
                            <td><?php echo $_smarty_tpl->tpl_vars['child']->value['parent_email'];?>
</td>
                        </tr>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Address");?>
</strong></td>
                            <td><?php echo $_smarty_tpl->tpl_vars['child']->value['address'];?>
</td>
                        </tr>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Study start date");?>
</strong></td>
                            <td><?php echo $_smarty_tpl->tpl_vars['child']->value['begin_at'];?>
</td>
                        </tr>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Description");?>
</strong></td>
                            <td><?php echo $_smarty_tpl->tpl_vars['child']->value['description'];?>
</td>
                        </tr>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Pickup information");?>
</strong></td>
                            <td>
                                <table class = "table table-bordered">
                                    <tbody>
                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['data']->value, 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
                                            <tr>
                                                <td style="width: 60%">
                                                    <?php if (!is_empty($_smarty_tpl->tpl_vars['row']->value['picker_source_file'])) {?>
                                                        <a href="<?php echo $_smarty_tpl->tpl_vars['row']->value['picker_source_file'];?>
" target="_blank"><img src = "<?php echo $_smarty_tpl->tpl_vars['row']->value['picker_source_file'];?>
" style="width: 100%" class = "img-responsive"></a>
                                                    <?php } else { ?>
                                                        <?php echo __("No information");?>

                                                    <?php }?>
                                                </td>
                                                <td style="width: 40%">
                                                    <strong><?php echo __("Picker name");?>
:</strong> <?php echo $_smarty_tpl->tpl_vars['row']->value['picker_name'];?>
 <br/>
                                                    <strong><?php echo __("Relation with student");?>
:</strong> <?php echo $_smarty_tpl->tpl_vars['row']->value['picker_relation'];?>
 <br/>
                                                    <strong><?php echo __("Telephone");?>
:</strong> <?php echo $_smarty_tpl->tpl_vars['row']->value['picker_phone'];?>
 <br/>
                                                    <strong><?php echo __("Address");?>
:</strong> <?php echo $_smarty_tpl->tpl_vars['row']->value['picker_address'];?>
 <br/>
                                                    <strong><?php echo __("Creator");?>
:</strong> <?php echo $_smarty_tpl->tpl_vars['row']->value['user_fullname'];?>

                                                </td>
                                            </tr>
                                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "listchildedit") {?>
        <div class="panel-body with-table">
            <div class="table-responsive" id="child_list" name="child_list">
                <?php $_smarty_tpl->_subTemplateRender("file:ci/class/ajax.childeditlist.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

            </div>
            <br>
            <!-- success -->
            <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
            <!-- success -->

            <!-- error -->
            <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
            <!-- error -->
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "reedit") {?>
        <div class="panel-body">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/class/boclass_child.php">
                <input type="hidden" name="username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                <input type="hidden" name="child_id" value="<?php echo $_smarty_tpl->tpl_vars['child_edit']->value['child_id'];?>
"/>
                <input type="hidden" name="child_code" value="<?php echo $_smarty_tpl->tpl_vars['child_edit']->value['child_code'];?>
"/>
                <input type="hidden" name="do" value="edit"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Child code");?>
</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="child_code" value="<?php echo $_smarty_tpl->tpl_vars['child_edit']->value['child_code'];?>
" disabled>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Full name");?>
 (*)</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="last_name" value="<?php echo $_smarty_tpl->tpl_vars['child_edit']->value['last_name'];?>
" placeholder="<?php echo __("Last name");?>
" required maxlength="34" autofocus>
                    </div>
                    <div class="col-sm-2">
                        <input type="text" class="form-control" name="first_name" value="<?php echo $_smarty_tpl->tpl_vars['child_edit']->value['first_name'];?>
" placeholder="<?php echo __("First name");?>
" required maxlength="15">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Gender");?>
 (*)</label>
                    <div class="col-sm-3">
                        <select name="gender" id="gender" class="form-control">
                            <option value="<?php echo @constant('MALE');?>
" <?php if ($_smarty_tpl->tpl_vars['child_edit']->value['gender'] == @constant('MALE')) {?>selected<?php }?>><?php echo __("Male");?>
</option>
                            <option value="<?php echo @constant('FEMALE');?>
" <?php if ($_smarty_tpl->tpl_vars['child_edit']->value['gender'] == @constant('FEMALE')) {?>selected<?php }?>><?php echo __("Female");?>
</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Birthdate");?>
 (*)</label>
                    <div class='col-sm-3'>
                        <div class='input-group date' id='birthdate_picker'>
                            <input type='text' name="birthday" value="<?php echo $_smarty_tpl->tpl_vars['child_edit']->value['birthday'];?>
" class="form-control" placeholder="<?php echo __("Birthdate");?>
 (*)" required/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Parent");?>
</label>
                    <div class="col-sm-7">
                        <input name="search-parent" id="search-parent" type="text" class="form-control" placeholder="<?php echo __("Enter username, email or fullname to search");?>
" autocomplete="off">
                        <div id="search-parent-results" class="dropdown-menu dropdown-widget dropdown-search">
                            <div class="dropdown-widget-header">
                                <?php echo __("Search Results");?>

                            </div>
                            <div class="dropdown-widget-body">
                                <div class="loader loader_small ptb10"></div>
                            </div>
                        </div>
                        <div><?php echo __("Enter at least 4 characters");?>
.</div>
                        <br/>
                        <div class="col-sm-9" id="parent_list" name="parent_list">
                            <?php if (count($_smarty_tpl->tpl_vars['child_edit']->value['parent']) > 0) {?>
                                <?php $_smarty_tpl->_subTemplateRender('file:ci/ajax.parentlist.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('results'=>$_smarty_tpl->tpl_vars['child_edit']->value['parent']), 0, true);
?>

                            <?php } else { ?>
                                <?php echo __("No parent");?>

                            <?php }?>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Parent name");?>
</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="parent_name" name="parent_name" placeholder="<?php echo __("Parent name");?>
" <?php if (count($_smarty_tpl->tpl_vars['child_edit']->value['parent']) > 0) {?>disabled <?php } else { ?> value = "<?php echo $_smarty_tpl->tpl_vars['child_edit']->value['parent_name'];?>
"<?php }?> maxlength="50">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Parent phone");?>
</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="parent_phone" name="parent_phone" value="<?php echo $_smarty_tpl->tpl_vars['child_edit']->value['parent_phone'];?>
" placeholder="<?php echo __("Parent phone");?>
" maxlength="50">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Parent email");?>
</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="parent_email" name="parent_email" value="<?php echo $_smarty_tpl->tpl_vars['child_edit']->value['parent_email'];?>
" placeholder="<?php echo __("Parent email");?>
" maxlength="100">
                    </div>
                    <div class="col-sm-4">
                        <input type="checkbox" value="<?php echo $_smarty_tpl->tpl_vars['child_edit']->value['create_parent_account'];?>
" name="create_parent_account" id="create_parent_account" <?php if (count($_smarty_tpl->tpl_vars['child_edit']->value['parent']) > 0) {?>disabled<?php } else { ?>checked<?php }?>>&nbsp;<?php echo __("Auto-create parent account");?>
?
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Address");?>
</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="address" value="<?php echo $_smarty_tpl->tpl_vars['child_edit']->value['address'];?>
" placeholder="<?php echo __("Address");?>
" maxlength="250">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Class");?>
</label>
                    <div class="col-sm-9">
                        <?php if ($_smarty_tpl->tpl_vars['child_edit']->value['class_id'] == '0') {?>
                            <input type="text" class="form-control" name="class_name" value="<?php echo __("No class ");?>
" disabled>
                        <?php } else { ?>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['classes']->value, 'class');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class']->value) {
?>
                                <?php if ($_smarty_tpl->tpl_vars['child_edit']->value['class_id'] == $_smarty_tpl->tpl_vars['class']->value['group_id']) {?>
                                    <input type="text" class="form-control" name="class_name" value="<?php echo $_smarty_tpl->tpl_vars['class']->value['group_title'];?>
" disabled>
                                    <?php
break 1;?>
                                <?php }?>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        <?php }?>
                        <input type="hidden" name="class_id" value="<?php echo $_smarty_tpl->tpl_vars['child_edit']->value['class_id'];?>
"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Study start date");?>
 (*)</label>
                    <div class='col-sm-3'>
                        <div class='input-group date' id='beginat_datepicker'>
                            <input type='text' name="begin_at" value="<?php echo $_smarty_tpl->tpl_vars['child_edit']->value['begin_at'];?>
" class="form-control" placeholder="<?php echo __("Study start date");?>
" required/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Description");?>
</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="description" placeholder="<?php echo __("Write about your child...");?>
" maxlength="300"><?php echo $_smarty_tpl->tpl_vars['child_edit']->value['description'];?>
</textarea>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-default"><?php echo __("Edit");?>
</button>
                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/children/listchildedit" class="btn btn-default"><?php echo __("Waiting list student confirm");?>
</a>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->
                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "health") {?>
        <div class = "panel-body">
            
            <div class="js_ajax-forms form-horizontal">
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Time");?>
</label>
                    <div class='col-sm-3'>
                        <div class='input-group date' id='begin_chart_picker'>
                            <input type='text' name="begin" class="form-control" id="begin"/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                    <div class='col-sm-1'><i class="fas fa-long-arrow-alt-right fa-lg fa-fw pr10 pt10"></i></div>
                    <div class='col-md-3'>
                        <div class='input-group date' id='end_chart_picker'>
                            <input type='text' name="end" class="form-control" id="end"/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-default js_class-chart-search" data-child="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"><?php echo __("Search");?>
</button>
                    </div>
                </div>

                <div id="chart_list" name="chart_list">
                    <?php $_smarty_tpl->_subTemplateRender("file:ci/class/ajax.class.chartsearch.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                </div>
                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
            </div>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "addgrowth") {?>
        <div class="panel-body form-horizontal">
            <form class="form-horizontal" enctype="multipart/form-data" method="post" id="class_edit_child_health">
                <input type="hidden" name="username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                <input type="hidden" name="child_id" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
"/>
                <input type="hidden" name="child_parent_id" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_parent_id'];?>
"/>
                <input type="hidden" name="do" value="add_growth"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Date");?>
 (*)</label>
                    <div class='col-sm-4'>
                        <div class='input-group date' id='birthdate_picker'>
                            <input type='text' name="recorded_at" class="form-control"/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Height");?>
 (*)</label>
                    <div class="col-sm-4">
                        <input type="number" min="0" class="form-control" name="height" step="any" required>
                    </div>
                    <label class="col-sm-1 control-label text-left" style = "text-align: left"><?php echo __("cm");?>
</label>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Weight");?>
 (*)</label>
                    <div class="col-sm-4">
                        <input type="number" min="0" class="form-control" name="weight" step="any" required>
                    </div>
                    <label class="col-sm-1 control-label text-left" style = "text-align: left"><?php echo __("kg");?>
</label>
                </div>
                <div class = "form-group">
                    <label class="col-sm-3 control-label"><?php echo __("Nutritional status");?>
</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="nutriture_status">
                    </div>
                </div>
                <div class = "form-group">
                    <label class="col-sm-3 control-label"><?php echo __("Heartbeat");?>
</label>
                    <div class="col-sm-5">
                        <input type="number" class="form-control" name="heart" min="0" step="1">
                    </div>
                    <label class="col-sm-2 control-label"><?php echo __("Times/minute");?>
</label>
                </div>
                <div class = "form-group">
                    <label class="col-sm-3 control-label"><?php echo __("Blood pressure");?>
</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="blood_pressure">
                    </div>
                </div>
                <div class = "form-group">
                    <label class="col-sm-3 control-label"><?php echo __("Ear");?>
</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="ear">
                    </div>
                </div>
                <div class = "form-group">
                    <label class="col-sm-3 control-label"><?php echo __("Eye");?>
</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="eye">
                    </div>
                </div>
                <div class = "form-group">
                    <label class="col-sm-3 control-label"><?php echo __("Nose");?>
</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="nose">
                    </div>
                </div>
                <div class = "form-group">
                    <label class="col-sm-3 control-label"><?php echo __("Description");?>
</label>
                    <div class="col-sm-9">
                        <textarea type="text" class="form-control" name="description"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("File attachment");?>
</label>
                    <div class="col-sm-6">
                        <input type="file" name="file" id="file"/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30"><?php echo __("Save");?>
</button>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "editgrowth") {?>
        <div class = "panel-body">
            <form class="form-horizontal" enctype="multipart/form-data" method="post" id="class_edit_child_health">
                <input type="hidden" name="username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                <input type="hidden" name="child_growth_id" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['child_growth_id'];?>
"/>
                <input type="hidden" name="child_id" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
"/>
                <input type="hidden" name="is_module" value="<?php echo $_smarty_tpl->tpl_vars['is_module']->value;?>
"/>
                <input type="hidden" name="do" value="edit_growth"/>
                <div class="form-group">
                    <label class = "col-sm-3 control-label text-left"><?php echo __("Date");?>
 (*)</label>
                    <div class='col-sm-3'>
                        <div class='input-group date' id='birthdate_picker'>
                            <input type='text' name="recorded_at" value = "<?php echo $_smarty_tpl->tpl_vars['data']->value['recorded_at'];?>
" id="recorded_at" class="form-control"/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                </div>
                <div class = "form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Height");?>
</label>
                    <div class="col-sm-3">
                        <input type="number" min="0" step = "any" class="form-control" name="height" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['height'];?>
" placeholder="<?php echo __("Height");?>
">
                    </div>
                    <label class="col-sm-2 control-label text-left" style = "text-align: left"><?php echo __("cm");?>
</label>
                </div>
                <div class = "form-group">
                    <label class="col-sm-3 control-label"><?php echo __("Weight");?>
</label>
                    <div class="col-sm-3">
                        <input type="number" min="0" step = "any" class="form-control" name="weight" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['weight'];?>
" placeholder="<?php echo __("Weight");?>
">
                    </div>
                    <label class="col-sm-2 control-label text-left" style = "text-align: left"><?php echo __("kg");?>
</label>
                </div>
                <div class = "form-group">
                    <label class="col-sm-3 control-label"><?php echo __("Nutritional status");?>
</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="nutriture_status" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['nutriture_status'];?>
">
                    </div>
                </div>
                <div class = "form-group">
                    <label class="col-sm-3 control-label"><?php echo __("Heartbeat");?>
</label>
                    <div class="col-sm-5">
                        <input type="number" class="form-control" name="heart" min="0" step="1" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['heart'];?>
">
                    </div>
                    <label class="col-sm-2 control-label"><?php echo __("Times/minute");?>
</label>
                </div>
                <div class = "form-group">
                    <label class="col-sm-3 control-label"><?php echo __("Blood pressure");?>
</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="blood_pressure" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['blood_pressure'];?>
">
                    </div>
                </div>
                <div class = "form-group">
                    <label class="col-sm-3 control-label"><?php echo __("Ear");?>
</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="ear" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['ear'];?>
">
                    </div>
                </div>
                <div class = "form-group">
                    <label class="col-sm-3 control-label"><?php echo __("Eye");?>
</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="eye" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['eye'];?>
">
                    </div>
                </div>
                <div class = "form-group">
                    <label class="col-sm-3 control-label"><?php echo __("Nose");?>
</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="nose" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['nose'];?>
">
                    </div>
                </div>
                <div class = "form-group">
                    <label class="col-sm-3 control-label"><?php echo __("Description");?>
</label>
                    <div class="col-sm-9">
                        <textarea type="text" class="form-control" name="description"><?php echo $_smarty_tpl->tpl_vars['data']->value['description'];?>
</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("File attachment");?>
</label>
                    <div class="col-sm-6">
                        <?php if (!is_empty($_smarty_tpl->tpl_vars['data']->value['source_file'])) {?>
                            <a href="<?php echo $_smarty_tpl->tpl_vars['data']->value['source_file'];?>
" target="_blank"><img src = "<?php echo $_smarty_tpl->tpl_vars['data']->value['source_file'];?>
" class = "img-responsive"></a>
                            <br>
                            <label class="control-label"><?php echo __("Choose file replace");?>
</label>
                            <br>
                        <?php }?>
                        <input type="file" name="file" id="file"/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30"><?php echo __("Save");?>
</button>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->
                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "addhealthindex") {?>
        <div class = "panel-body with-table">
            <form class="form-horizontal" enctype="multipart/form-data" method="post" id="class_add_health_index">
                <input type="hidden" name="username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                <input type="hidden" name="do" value="add_health_index"/>
                <div class="form-group">
                    <label class = "col-sm-4 control-label text-left"><?php echo __("Measure time");?>
 (*)</label>
                    <div class='col-sm-3'>
                        <div class='input-group date' id='birthdate_picker'>
                            <input type='text' name="recorded_at" id="recorded_at" class="form-control"/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-4 health_left">
                        <div class="table-responsive">
                            <div class="mb10 ml10"><strong><?php echo __("Children list");?>
&nbsp;(<?php echo count($_smarty_tpl->tpl_vars['children']->value);?>
)</strong></div>
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th><?php echo __("#");?>
</th>
                                    <th><?php echo __("Child");?>
</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['children']->value, 'child');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['child']->value) {
?>
                                    <tr>
                                        <td align="center"><strong><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</strong></td>
                                        <td>
                                            <a href="#" class="js_class-child-health" id="child_<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['child']->value['child_name'];?>
</a>
                                        </td>
                                    </tr>
                                    <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-sm-8 health_right">
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['children']->value, 'child');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['child']->value) {
?>
                            <div class="hidden hideAll" id="health_<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
">
                                <input type="hidden" name="childIds[]" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
">
                                <div class="mb10"><strong><?php echo __("Add health index");?>
 <?php echo __("for");?>
:&nbsp;<?php echo $_smarty_tpl->tpl_vars['child']->value['child_name'];?>
</strong></div>
                                <div class="" style="border: 1px solid #b7b7b7; padding: 10px 10px">
                                    <div class = "form-group">
                                        <label class="col-sm-3 control-label text-left"><?php echo __("Height");?>
</label>
                                        <div class="col-sm-5">
                                            <input type="number" min="0" step = "any" class="form-control" name="height_<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
" placeholder="<?php echo __("Height");?>
">
                                        </div>
                                        <label class="col-sm-2 control-label text-left" style = "text-align: left"><?php echo __("cm");?>
</label>
                                    </div>
                                    <div class = "form-group">
                                        <label class="col-sm-3 control-label"><?php echo __("Weight");?>
</label>
                                        <div class="col-sm-5">
                                            <input type="number" min="0" step = "any" class="form-control" name="weight_<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
" placeholder="<?php echo __("Weight");?>
">
                                        </div>
                                        <label class="col-sm-2 control-label text-left" style = "text-align: left"><?php echo __("kg");?>
</label>
                                    </div>
                                    <div class = "form-group">
                                        <label class="col-sm-3 control-label"><?php echo __("Nutritional status");?>
</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="nutriture_status_<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
">
                                        </div>
                                    </div>
                                    <div class = "form-group">
                                        <label class="col-sm-3 control-label"><?php echo __("Heartbeat");?>
</label>
                                        <div class="col-sm-5">
                                            <input type="number" class="form-control" name="heart_<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
" min="0" step="1">
                                        </div>
                                        <label class="col-sm-2 control-label"><?php echo __("Times/minute");?>
</label>
                                    </div>
                                    <div class = "form-group">
                                        <label class="col-sm-3 control-label"><?php echo __("Blood pressure");?>
</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="blood_pressure_<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
">
                                        </div>
                                    </div>
                                    <div class = "form-group">
                                        <label class="col-sm-3 control-label"><?php echo __("Ear");?>
</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="ear_<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
">
                                        </div>
                                    </div>
                                    <div class = "form-group">
                                        <label class="col-sm-3 control-label"><?php echo __("Eye");?>
</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="eye_<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
">
                                        </div>
                                    </div>
                                    <div class = "form-group">
                                        <label class="col-sm-3 control-label"><?php echo __("Nose");?>
</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="nose_<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
">
                                        </div>
                                    </div>
                                    <div class = "form-group">
                                        <label class="col-sm-3 control-label"><?php echo __("Description");?>
</label>
                                        <div class="col-sm-9">
                                            <textarea type="text" class="form-control" name="description_<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label text-left"><?php echo __("File attachment");?>
</label>
                                        <div class="col-sm-6">
                                            <?php if (!is_empty($_smarty_tpl->tpl_vars['data']->value['source_file'])) {?>
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['data']->value['source_file'];?>
" target="_blank"><img src = "<?php echo $_smarty_tpl->tpl_vars['data']->value['source_file'];?>
" class = "img-responsive"></a>
                                                <br>
                                                <label class="control-label"><?php echo __("Choose file replace");?>
</label>
                                                <br>
                                            <?php }?>
                                            <input type="file" name="file_<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
" id="file"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                    </div>
                </div>
                <div class="form-group hidden submit_hidden" id="">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30"><?php echo __("Save");?>
</button>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->
                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "addexisting") {?>
        <div class="panel-body">
            <div class="panel-body with-table">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-right pt5"><?php echo __("Child code");?>
 (*)</label>
                        <div class='col-sm-3'>
                            <input type='text' name="childcode" id="childcode" class="form-control" min="1" placeholder="<?php echo __("Child code");?>
"/>
                        </div>
                        <div class='col-sm-2'>
                            <div class="form-group">
                                <a href="#" id="search" class="btn btn-default js_child-addexisting" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-school_username="<?php echo $_smarty_tpl->tpl_vars['school']->value['page_name'];?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['school']->value['page_id'];?>
" data-class_id="<?php echo $_smarty_tpl->tpl_vars['class']->value['group_id'];?>
"><?php echo __("Search");?>
</a>
                                <label id="loading" class="btn btn-info x-hidden"><?php echo __("Loading");?>
...</label>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div class="" id="table_child_addexisting">
                </div>
            </div>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "addphoto") {?>
        <div class="panel-body">
            <div class="mb10" align="center">
                <strong><?php echo __("Add photo to diary");?>
 <?php echo __("for");?>
 <?php echo $_smarty_tpl->tpl_vars['child']->value['child_name'];?>
</strong>
            </div>
            <form class="form-horizontal" enctype="multipart/form-data" method="post" id="add_child_journal_class">
                <input type="hidden" name="username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                <input type="hidden" name="child_id" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
"/>
                <input type="hidden" name="child_parent_id" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_parent_id'];?>
"/>
                <input type="hidden" name="do" value="add_photo"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Caption");?>
</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="caption" placeholder="<?php echo __("Caption");?>
">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Picture");?>
</label>
                    <div class="col-sm-6">
                        <input name="file[]" type="file" multiple="true">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30"><?php echo __("Save");?>
</button>
                    </div>
                </div>
            </form>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "journal") {?>
        <div class="panel-body with-table form-horizontal">
            <div class="form-group">
                <label class="col-sm-3 control-label text-left"><?php echo __("Year");?>
</label>
                <div class="col-sm-3">
                    <select name="year" id="year" class="form-control">
                        <option value = "0"><?php echo __("Select year ...");?>
</option>
                        <?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable(null, $_smarty_tpl->isRenderingCache);
$_smarty_tpl->tpl_vars['i']->value = $_smarty_tpl->tpl_vars['year_begin']->value;
if ($_smarty_tpl->tpl_vars['i']->value < $_smarty_tpl->tpl_vars['year_end']->value) {
for ($_foo=true;$_smarty_tpl->tpl_vars['i']->value < $_smarty_tpl->tpl_vars['year_end']->value; $_smarty_tpl->tpl_vars['i']->value++) {
?>
                            <option value="<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['i']->value;?>
</option>
                        <?php }
}
?>

                    </select>
                </div>
                <div class="col-sm-3">
                    <a class="btn btn-default js_class-journal-search" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
" data-handle = "search"><?php echo __("Search");?>
</a>
                </div>
            </div>
            <div id = "journal_list">
                <?php $_smarty_tpl->_subTemplateRender("file:ci/ajax.class.journal.list.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

            </div>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "adddiary") {?>
        <div class="panel-body">
            <form class="form-horizontal" enctype="multipart/form-data" method="post" id="add_child_journal_class">
                <input type="hidden" name="username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                <input type="hidden" name="reload" value="1"/>
                <input type="hidden" name="do" value="add_photo"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Select student");?>
 (*)</label>
                    <div class="col-sm-5">
                        <select name="child_id" class="form-control" autofocus>
                            <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['children']->value, 'child');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['child']->value) {
?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
 - <?php echo $_smarty_tpl->tpl_vars['child']->value['child_name'];?>
 - <?php echo $_smarty_tpl->tpl_vars['child']->value['birthday'];?>
</option>
                                <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Caption");?>
</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="caption" placeholder="<?php echo __("Caption");?>
">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Picture");?>
</label>
                    <div class="col-sm-6">
                        <input name="file[]" type="file" multiple="true">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30"><?php echo __("Save");?>
</button>
                    </div>
                </div>
            </form>
        </div>
    <?php }?>
</div>
<?php }
}
