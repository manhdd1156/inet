<?php
/* Smarty version 3.1.31, created on 2021-03-31 10:27:26
  from "D:\workplace\Server11\content\themes\coniu\templates\ci\class\class.attendance.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_6063ec1e376362_31991013',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '0444dfb3f93689039cab72a6ef2ed76eca88eab1' => 
    array (
      0 => 'D:\\workplace\\Server11\\content\\themes\\coniu\\templates\\ci\\class\\class.attendance.tpl',
      1 => 1559554745,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:ci/class/ajax.class.attendancesearch.tpl' => 1,
    'file:ci/class/ajax.class.attendance.child.tpl' => 1,
    'file:ci/class/ajax.class.attendancelist.tpl' => 1,
  ),
),false)) {
function content_6063ec1e376362_31991013 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <div class="pull-right flip">
            <a href="https://blog.coniu.vn/huong-dan-giao-vien-diem-danh-tren-website-coniu/" class="btn btn-info btn_guide" target="_blank">
                <i class="fa fa-info-circle" aria-hidden="true"></i> <?php echo __("Guide");?>

            </a>
            <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/class/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/attendance/rollup" class="btn btn-default">
                    <i class="fa fa-plus"></i> <?php echo __("Roll up");?>

                </a>
            <?php }?>
        </div>
        <i class="fa fa-tasks fa-fw fa-lg pr10"></i>
        <?php echo __("Attendance");?>

        <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == "search") {?>
            &rsaquo; <?php echo __("Search");?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "rollup") {?>
            &rsaquo; <?php echo __("Roll up");?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "child") {?>
            &rsaquo; <?php echo $_smarty_tpl->tpl_vars['data']->value['child']['child_name'];?>

        <?php }?>
    </div>
    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
        <div class="panel-body with-table">
            <div class="row">
                <div class="form-group">
                    <label class="col-sm-2 control-label text-right pt10"><?php echo __("Time");?>
</label>
                    <div class="col-sm-10">
                        <div class='col-sm-4'>
                            <div class='input-group date' id='fromdate_picker'>
                                <input type='text' name="fromDate" id="fromDate" value="<?php echo $_smarty_tpl->tpl_vars['fromDate']->value;?>
" class="form-control" placeholder="<?php echo __("From date");?>
"/>
                                <span class="input-group-addon">
                                    <span class="fas fa-calendar-alt"></span>
                                </span>
                            </div>
                        </div>

                        <div class='col-md-4'>
                            <div class='input-group date' id='todate_picker'>
                                <input type='text' name="toDate" id="toDate" value="<?php echo $_smarty_tpl->tpl_vars['toDate']->value;?>
" class="form-control" placeholder="<?php echo __("To date");?>
"/>
                                <span class="input-group-addon">
                                    <span class="fas fa-calendar-alt"></span>
                                </span>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <a href="#" id="search" class="btn btn-default js_class-attendancesearch" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"><?php echo __("Search");?>
</a>
                            <label id="loading" class="btn btn-info x-hidden"><?php echo __("Loading");?>
...</label>
                        </div>
                    </div>
                </div>
            </div><br/>
            <div class="table-responsive" id="attendance_list">
                <?php $_smarty_tpl->_subTemplateRender("file:ci/class/ajax.class.attendancesearch.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

            </div>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "child") {?>
        <div class="panel-body with-table">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/class/boclass_attendance.php">
                <input type="hidden" name="username" id="username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                <input type="hidden" name="do" value="update_child"/>
                <div class="form-group">
                    <label class="col-sm-2 control-label text-right pt10"><?php echo __("Time");?>
</label>
                    <div class="col-sm-10">
                        <div class='col-sm-4'>
                            <div class='input-group date' id='fromdate_picker'>
                                <input type='text' name="fromDate" id="fromDate" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['fromDate'];?>
" class="form-control" placeholder="<?php echo __("From date");?>
"/>
                                <span class="input-group-addon">
                                    <span class="fas fa-calendar-alt"></span>
                                </span>
                            </div>
                        </div>

                        <div class='col-md-4'>
                            <div class='input-group date' id='todate_picker'>
                                <input type='text' name="toDate" id="toDate" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['toDate'];?>
" class="form-control" placeholder="<?php echo __("To date");?>
"/>
                                <span class="input-group-addon">
                                    <span class="fas fa-calendar-alt"></span>
                                </span>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <a class="btn btn-default js_class-attendancechild" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['data']->value['child']['child_id'];?>
"><?php echo __("Search");?>
</a>
                            <label class="btn btn-info processing_label x-hidden"><?php echo __("Processing");?>
...</label>
                        </div>
                    </div>
                </div>
                <br/>
                <div class="table-responsive" id="attendance_list">
                    <?php $_smarty_tpl->_subTemplateRender("file:ci/class/ajax.class.attendance.child.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                </div>
                <div class="form-group">
                    <div class="col-sm-9 col-md-offset-1">
                        <button type="submit" class="btn btn-primary padrl30" <?php if ((count($_smarty_tpl->tpl_vars['data']->value['attendance']['attendance']) == 0)) {?>disabled<?php }?>><?php echo __("Save");?>
</button>
                    </div>
                </div>
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
            </form>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "rollup") {?>
        <div class="panel-body">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/class/boclass_attendance.php">
                <input type="hidden" name="username" id="username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                <input type="hidden" name="do" value="rollup" id="attendance_do"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Date");?>
</label>
                    <div class='col-sm-4'>
                        <div class='input-group date' id='attendance_picker'>
                            <input type='text' name="attendance_date" id="attendance_date" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['attendance_date'];?>
" class="form-control" placeholder="<?php echo __("Date");?>
 (*)" required/>
                                <span class="input-group-addon">
                                    <span class="fas fa-calendar-alt"></span>
                                </span>
                        </div>
                    </div>
                </div>
                <div id="attendance_list">
                    <?php $_smarty_tpl->_subTemplateRender("file:ci/class/ajax.class.attendancelist.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                </div>
                <div class="form-group">
                    <div class="col-sm-9">
                        <button type="submit" class="btn btn-primary padrl30" <?php if (count($_smarty_tpl->tpl_vars['data']->value['detail']) == 0) {?>disabled<?php }?>><?php echo __("Save");?>
</button>
                    </div>
                </div>
                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    <?php }?>
</div><?php }
}
