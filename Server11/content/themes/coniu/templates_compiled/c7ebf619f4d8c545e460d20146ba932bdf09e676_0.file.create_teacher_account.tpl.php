<?php
/* Smarty version 3.1.31, created on 2021-05-07 14:12:21
  from "D:\workplace\mascom-edu-server\Server11\content\themes\coniu\templates\ci\email_templates\create_teacher_account.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_6094e8550f4b04_44834825',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c7ebf619f4d8c545e460d20146ba932bdf09e676' => 
    array (
      0 => 'D:\\workplace\\mascom-edu-server\\Server11\\content\\themes\\coniu\\templates\\ci\\email_templates\\create_teacher_account.tpl',
      1 => 1619677394,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6094e8550f4b04_44834825 (Smarty_Internal_Template $_smarty_tpl) {
?>
Kính gửi ông/bà <?php echo $_smarty_tpl->tpl_vars['full_name']->value;?>
,<br/><br/>

Hiện tại, nhà trường đã đưa vào sử dụng "Ứng dụng tương tác mầm non - Coniu" làm công cụ quản lý và là kênh tương tác/trao đổi giữa nhà trường, giáo viên và phụ huynh.<br/>
Ban Giám hiệu nhà trường đã tạo sẵn tài khoản cho ông/bà là:<br/>
&emsp;&emsp;- Tài khoản: <b><?php echo $_smarty_tpl->tpl_vars['username']->value;?>
</b><br/>
&emsp;&emsp;- Mật khẩu: <b><?php echo $_smarty_tpl->tpl_vars['password']->value;?>
</b><br/>
Ông/bà có thể thay đổi thông tin tài khoản sau khi truy cập.<br/><br/>

Để sử dụng Coniu với tài khoản được cấp, ông/bà có thể áp dụng 01 trong 02 cách sau:<br/>
&emsp;&emsp;1 - Truy cập địa chỉ website Coniu tại địa chỉ: https://coniu.vn hoặc;<br/>
&emsp;&emsp;2 - Tải ứng dụng 'Coniu' về điện thoại (chỉ áp dụng với điện thoại iPhone và điện thoại sử dụng hệ điều hành Android).<br/><br/>

Trân trọng cám ơn sự hợp tác của ông/bà!<br/>
Thân ái,<br/>
Ban Giám hiệu nhà trường.<br/>
<?php }
}
