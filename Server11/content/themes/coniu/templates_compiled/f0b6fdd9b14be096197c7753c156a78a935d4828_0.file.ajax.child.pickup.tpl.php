<?php
/* Smarty version 3.1.31, created on 2021-03-31 10:48:07
  from "D:\workplace\Server11\content\themes\coniu\templates\ci\child\ajax.child.pickup.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_6063f0f7b49939_68577173',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'f0b6fdd9b14be096197c7753c156a78a935d4828' => 
    array (
      0 => 'D:\\workplace\\Server11\\content\\themes\\coniu\\templates\\ci\\child\\ajax.child.pickup.tpl',
      1 => 1552404702,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6063f0f7b49939_68577173 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="mb5"><strong><?php echo __("Total");?>
: <?php echo count($_smarty_tpl->tpl_vars['results']->value);?>
&nbsp;</strong></div>
<table class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th width="10%">#</th>
        <th width="20%"><?php echo __("Date");?>
</th>
        <th width="25%"><?php echo __("Class");?>
</th>
        <th width="20%"><?php echo __("Status");?>
</th>
        <th width="15%"><?php echo __("Money amount");?>
</th>
    </tr>
    </thead>
    <tbody>
    <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['results']->value, 'result');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['result']->value) {
?>
        <tr>
            <td align="center"><strong><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</strong></td>
            <td align="center">
                <strong>
                    <font color="#ff4500">
                        <?php echo $_smarty_tpl->tpl_vars['result']->value['pickup_time'];?>
 - <?php echo __($_smarty_tpl->tpl_vars['result']->value['pickup_day']);?>

                    </font>
                </strong>
            </td>
            <td align="center">
                <?php if (!is_empty($_smarty_tpl->tpl_vars['result']->value['class_name'])) {?>
                    <?php echo $_smarty_tpl->tpl_vars['result']->value['class_name'];?>

                <?php } else { ?>
                    <strong>&#150;</strong>
                <?php }?>
            </td>
            <td align="center">
                <?php if (!is_empty($_smarty_tpl->tpl_vars['result']->value['pickup_at'])) {?>
                    <i class="fa fa-check" style="color:mediumseagreen" aria-hidden="true"></i>
                <?php } else { ?>
                    <i class="fa fa-times" aria-hidden="true"></i>
                <?php }?>
            </td>
            <td></td>
        </tr>
        <tr>
            <td rowspan="3"></td>
            <td colspan="3" align="left" style="padding-left: 30px">
                <div class="col-sm-4"><?php echo __("Pickup at");?>
:&nbsp;</div>
                    <?php if (!is_empty($_smarty_tpl->tpl_vars['result']->value['pickup_at'])) {?>
                        <div class="col-sm-8">
                            <font color="blue"><?php echo $_smarty_tpl->tpl_vars['result']->value['pickup_at'];?>
</font>
                        </div>
                    <?php } else { ?>
                        <div class="col-sm-8" align="center">
                            <font color="#ff4500">&#150;</font>
                        </div>
                    <?php }?>
                </div>
            </td>
            <td colspan="1" align="right">
                <strong>
                    <?php echo number_format($_smarty_tpl->tpl_vars['result']->value['late_pickup_fee'],0,'.',',');?>

                </strong>
            </td>
        </tr>
        <tr>
            <td colspan="3" align="left" style="padding-left: 30px">
                <?php if (!empty($_smarty_tpl->tpl_vars['result']->value['services'])) {?>
                    <div class="col-sm-4"><?php echo __("Using service");?>
:&nbsp;</div>
                    <div class="col-sm-8">
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['result']->value['services'], 'service');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['service']->value) {
?>
                            <input type="checkbox" <?php if (!is_null($_smarty_tpl->tpl_vars['service']->value['using_at'])) {?> checked <?php }?> onclick="return false;"/>
                            <?php echo $_smarty_tpl->tpl_vars['service']->value['service_name'];?>

                            &nbsp;&nbsp;&nbsp;
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                    </div>
                <?php } else { ?>
                    &nbsp;
                <?php }?>
            </td>
            <td colspan="1" align="right">
                <strong>
                    <?php echo number_format($_smarty_tpl->tpl_vars['result']->value['using_service_fee'],0,'.',',');?>

                </strong>
            </td>
        </tr>
        <tr>
            <td colspan="3" align="left" style="padding-left: 30px" class="col-sm-12">
                <div class="col-sm-4"><?php echo __("Late pickup teachers");?>
: &nbsp;</div>
                <?php $_smarty_tpl->_assignInScope('assign_count', count($_smarty_tpl->tpl_vars['result']->value['assigns']));
?>
                <?php if ($_smarty_tpl->tpl_vars['assign_count']->value > 0) {?>
                        <?php $_smarty_tpl->_assignInScope('i', 1);
?>
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['result']->value['assigns'], 'assign');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['assign']->value) {
?>
                            <div class="col-xs-12 col-sm-4">
                                &#9679;&#09;<?php echo $_smarty_tpl->tpl_vars['assign']->value['user_fullname'];?>

                            </div>
                            <?php if ($_smarty_tpl->tpl_vars['i']->value%2 == 0 && $_smarty_tpl->tpl_vars['i']->value != $_smarty_tpl->tpl_vars['assign_count']->value) {?>
                                <div class="col-sm-4"></div>
                            <?php } elseif ($_smarty_tpl->tpl_vars['i']->value%2 == 1 && $_smarty_tpl->tpl_vars['i']->value == $_smarty_tpl->tpl_vars['assign_count']->value) {?>
                                <br>
                            <?php }?>
                            <?php $_smarty_tpl->_assignInScope('i', $_smarty_tpl->tpl_vars['i']->value+1);
?>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                    <?php } else { ?>
                        <div class="col-sm-8" align="center">
                            <font color="#ff4500">&#150;</font>
                        </div>
                    <?php }?>
                <br>
                <?php if (!empty($_smarty_tpl->tpl_vars['result']->value['description'])) {?>
                    <div class="col-sm-4"><?php echo __("Note");?>
: &nbsp;</div>
                    <div class="col-xs-12 col-sm-4" style="color: blue">
                        <?php echo $_smarty_tpl->tpl_vars['result']->value['description'];?>

                    </div>
                <?php }?>

            </td>
            <td colspan="1" align="right" style="vertical-align: middle">
                <strong>
                    <font color="blue">
                        <?php echo number_format($_smarty_tpl->tpl_vars['result']->value['total_amount'],0,'.',',');?>

                    </font>
                </strong>
            </td>
        </tr>
        <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

    <tr><td colspan="5"></td></tr>
    <tr>
        <td colspan="4" align="right"><strong><?php echo __("Total");?>
</strong></td>
        <td colspan="1" align="right" style="vertical-align: middle">
            <strong>
                <font color="#ff4500">
                    <?php echo number_format($_smarty_tpl->tpl_vars['total']->value,0,'.',',');?>

                </font>
            </strong>
        </td>
    </tr>
    </tbody>
</table><?php }
}
