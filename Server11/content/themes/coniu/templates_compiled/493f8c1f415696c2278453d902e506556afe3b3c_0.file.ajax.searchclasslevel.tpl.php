<?php
/* Smarty version 3.1.31, created on 2021-04-23 15:46:04
  from "D:\workplace\mascom-edu-server\Server11\content\themes\coniu\templates\ci\school\ajax.searchclasslevel.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_6082894c6bf495_98066269',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '493f8c1f415696c2278453d902e506556afe3b3c' => 
    array (
      0 => 'D:\\workplace\\mascom-edu-server\\Server11\\content\\themes\\coniu\\templates\\ci\\school\\ajax.searchclasslevel.tpl',
      1 => 1619167559,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6082894c6bf495_98066269 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="js_scroller">
    <ul>
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['results']->value, 'classlevel');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['classlevel']->value) {
?>
            <li class="feeds-item" data-id="<?php echo $_smarty_tpl->tpl_vars['classlevel']->value['class_level_name'];?>
">
                <div class="data-container <?php if ($_smarty_tpl->tpl_vars['_small']->value) {?>small<?php }?>">



                        <div class="pull-right flip">
                            <div class="btn btn-default js_classlevel-select" data-uid="<?php echo $_smarty_tpl->tpl_vars['classlevel']->value['class_level_id'];?>
" data-ufullname="<?php echo $_smarty_tpl->tpl_vars['classlevel']->value['class_level_name'];?>
"><?php echo __("Add");?>
</div>
                        </div>
                        <div>

                            <span class="name js_classlevel-popover" data-uid="<?php echo $_smarty_tpl->tpl_vars['classlevel']->value['class_level_id'];?>
">
                              <?php echo $_smarty_tpl->tpl_vars['classlevel']->value['class_level_name'];?>

                            </span>
                        </div>
                </div>
            </li>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

    </ul>
</div><?php }
}
