<?php
/* Smarty version 3.1.31, created on 2021-05-08 16:13:04
  from "D:\workplace\mascom-edu-server\Server11\content\themes\coniu\templates\ci\school\school.tuitions.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_60965620d0a829_95231730',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'dac43c7d69b7669419d1f375f5ad430b1ea6134a' => 
    array (
      0 => 'D:\\workplace\\mascom-edu-server\\Server11\\content\\themes\\coniu\\templates\\ci\\school\\school.tuitions.tpl',
      1 => 1620098630,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:ci/school/school.tuitions.newchild.tpl' => 1,
    'file:ci/school/school.tuitions.history.tpl' => 1,
    'file:ci/school/school.tuitions.detail.tpl' => 1,
    'file:ci/school/school.tuitions.edit.tpl' => 1,
  ),
),false)) {
function content_60965620d0a829_95231730 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="" style="position: relative">
    <?php if ($_smarty_tpl->tpl_vars['school']->value['school_step'] == SCHOOL_STEP_FINISH || $_smarty_tpl->tpl_vars['school']->value['grade'] != 0) {?>
        <div class="dashboard_fixed">
            <button class="btn btn-xs btn-default js_school-tuition-guide" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['user']->value->_data["user_id"];?>
">
                <i class="fa fa-chevron-down <?php if ($_smarty_tpl->tpl_vars['cookie']->value) {?>hidden<?php }?>" aria-hidden="true" id="guide_hidden"></i>
                <i class="fa fa-chevron-up <?php if (!$_smarty_tpl->tpl_vars['cookie']->value) {?>hidden<?php }?>" aria-hidden="true" id="guide_show"></i>
            </button>
        </div>
        <div class="panel panel-default <?php if ($_smarty_tpl->tpl_vars['cookie']->value == 0) {?>hidden<?php }?>" id="guide_tuition">
            <div class="guide_school row" style="margin-left: 0px; margin-right: 0px">
                <div class="col-sm-4" id="left_height">
                    <div class="init_school text-left pad5 full_width">
                        <div class="full_width pl5">
                            <strong><?php echo __("The execution's order guide");?>
:</strong>
                        </div>
                        <div style="border: 1px solid #c9c9c9" class="pad5">
                            <div class="full_width">
                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/fees/add" class="pad5 font12 full_width">1. <?php echo __("Create fees");?>
</a>
                            </div>
                            <div class="full_width">
                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/attendance" class="pad5 font12 full_width">2. <?php echo __("Attendance");?>
</a>
                            </div>
                            <div class="full_width">
                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/useservices/reg" class=" pad5 font12 full_width">3. <?php echo __("Register service");?>
</a>
                            </div>
                            <div class="full_width">
                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/pickup/list" class="pad5 font12 full_width">4. <?php echo __("Late pickup");?>
</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div style="width: 15px; float: left; vertical-align: middle; position: relative" align="center" class="height_auto mobile_hide">
                    <div class="" style="position: absolute; top: 45%">
                        <i class="fas fa-long-arrow-alt-right" aria-hidden="true"></i>
                    </div>
                </div>
                <div class="col-sm-7 height_auto" style="position: relative">
                    <div class="init_school text-left pad5 full_width" style="position: absolute; top: 33%">
                        <div class="pad5">
                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/tuitions/add" class="pad5 font12">5. <?php echo __("Add monthly tuition");?>
</a>
                            <i class="fas fa-long-arrow-alt-right mobile_hide" aria-hidden="true"></i>
                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/tuitions" class="pad5 font12">6. <?php echo __("Tuition list");?>
</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php }?>
    <div class="panel panel-default">
        <div class="panel-heading with-icon">
            <?php if ($_smarty_tpl->tpl_vars['canEdit']->value) {?>
                <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
                    <div class="pull-right flip">
                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/tuitions/add" class="btn btn-default">
                            <i class="fa fa-plus"></i> <?php echo __("Add monthly tuition");?>

                        </a>
                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/tuitions/summarypaid" class="btn btn-default">
                            <i class="fas fa-money-bill-wave"></i> <?php echo __("Summary tuition paid");?>

                        </a>
                    </div>
                <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
                    <div class="pull-right flip">
                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/tuitions/" class="btn btn-default">
                            <i class="fa fa-list"></i> <?php echo __("Monthly tuition list");?>

                        </a>
                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/fees/add" class="btn btn-default">
                            <i class="fa fa-plus"></i> <?php echo __("Add New Fee");?>

                        </a>
                    </div>
                <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "detail") {?>
                    <div class="pull-right flip">
                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/tuitions/" class="btn btn-default">
                            <i class="fa fa-list"></i> <?php echo __("Monthly tuition list");?>

                        </a>
                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/tuitions/add" class="btn btn-default">
                            <i class="fa fa-plus"></i> <?php echo __("Add monthly tuition");?>

                        </a>
                    </div>
                <?php }?>
            <?php }?>
            <div class="pull-right flip" style="margin-right: 5px">
                <a href="https://blog.coniu.vn/huong-dan-tao-hoc-phi-thang-cho-lop-va-tre/" class="btn btn-info btn_guide" target="_blank">
                    <i class="fa fa-info"></i> <?php echo __("Guide");?>

                </a>
            </div>
            <i class="fas fa-money-bill-alt fa-lg fa-fw pr10"></i>
            <?php echo __("Tuition");?>

            <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == "edit") {?>
                &rsaquo; <?php echo __("Edit");?>

            <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "history") {?>
                &rsaquo; <?php echo __("History");?>

            <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
                &rsaquo; <?php echo __('Add monthly tuition');?>

            <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "newchild") {?>
                &rsaquo; <?php echo __('Add tuition for new child');?>

            <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "detail") {?>
                &rsaquo; <?php echo __('Detail');?>

            <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "4leave") {?>
                &rsaquo; <?php echo __('Account for leave');?>

            <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "summarypaid") {?>
                &rsaquo; <?php echo __('Summary tuition paid');?>

            <?php }?>
        </div>
        <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
            <div class="panel-body with-table">
                <div><strong><?php echo __("Tuition list");?>
&nbsp;(<?php echo count($_smarty_tpl->tpl_vars['rows']->value);?>
)</strong></div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th nowrap="true"><?php echo __("Class");?>
</th>
                            <th><?php echo __("Paid");?>
<br/>(<?php echo @constant('MONEY_UNIT');?>
)</th>
                            <th><?php echo __("Total");?>
<br/>(<?php echo @constant('MONEY_UNIT');?>
)</th>
                            <th><?php echo __("Paid studentren");?>
</th>
                            <th><?php echo __("Option");?>
</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $_smarty_tpl->_assignInScope('month', "-1");
?>
                        <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['rows']->value, 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
                            <?php if (($_smarty_tpl->tpl_vars['month']->value != $_smarty_tpl->tpl_vars['row']->value['month'])) {?>
                                <?php if ($_smarty_tpl->tpl_vars['idx']->value > 1) {?>
                                    
                                    <tr>
                                        <td colspan="2" align="center" style="vertical-align:middle"><strong><?php echo __("Month total");?>
</strong></td>
                                        <td class="text-right" style="vertical-align:middle"><strong><span class="tuition_paid_month_<?php echo str_replace('/','_',$_smarty_tpl->tpl_vars['month']->value);?>
"><?php echo moneyFormat($_smarty_tpl->tpl_vars['totalPaid']->value);?>
</span></strong></td>
                                        <td class="text-right" style="vertical-align:middle"><strong><span class="tuition_total_month_<?php echo str_replace('/','_',$_smarty_tpl->tpl_vars['month']->value);?>
"><?php echo moneyFormat($_smarty_tpl->tpl_vars['totalTution']->value);?>
</span></strong></td>
                                        <td align="center" style="vertical-align:middle"><strong><span class="tuition_count_month_<?php echo str_replace('/','_',$_smarty_tpl->tpl_vars['month']->value);?>
"><?php echo $_smarty_tpl->tpl_vars['totalPaidCount']->value;?>
</span></strong></td>
                                        <td></td>
                                    </tr>
                                <?php }?>

                                <tr><td colspan="6"><strong><?php echo __("Month");?>
:&nbsp;<?php echo $_smarty_tpl->tpl_vars['row']->value['month'];?>
</strong></td></tr>
                                <?php $_smarty_tpl->_assignInScope('totalPaid', 0);
?>
                                <?php $_smarty_tpl->_assignInScope('totalTution', 0);
?>
                                <?php $_smarty_tpl->_assignInScope('totalPaidCount', 0);
?>
                                <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                            <?php }?>
                            <tr>
                                <td align="center" style="vertical-align:middle"><strong><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</strong></td>
                                <td nowrap="true" style="vertical-align:middle"><a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/tuitions/detail/<?php echo $_smarty_tpl->tpl_vars['row']->value['tuition_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['row']->value['group_title'];?>
</a></td>
                                <td class="text-right tuition_paid_list_<?php echo $_smarty_tpl->tpl_vars['row']->value['tuition_id'];?>
" style="vertical-align:middle"><?php echo moneyFormat($_smarty_tpl->tpl_vars['row']->value['paid_amount']);?>
</td>
                                <td class="text-right tuition_total_list_<?php echo $_smarty_tpl->tpl_vars['row']->value['tuition_id'];?>
" style="vertical-align:middle"><?php echo moneyFormat($_smarty_tpl->tpl_vars['row']->value['total_amount']);?>
</td>
                                <td align="center" class="tuition_count_list_<?php echo $_smarty_tpl->tpl_vars['row']->value['tuition_id'];?>
" style="vertical-align:middle"><?php echo $_smarty_tpl->tpl_vars['row']->value['paid_count'];?>
</td>
                                <td align="left" style="vertical-align:middle">
                                    <a class="btn btn-success btn-xs js_school-tuition-export" data-handle="export_all" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['tuition_id'];?>
"><?php echo __('Export all to Excel');?>
</a>
                                    <a class="btn btn-default btn-xs" href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/tuitions/detail/<?php echo $_smarty_tpl->tpl_vars['row']->value['tuition_id'];?>
"><?php echo __('Detail');?>
</a>
                                    <label class="btn btn-info btn-xs processing_label x-hidden"><?php echo __("Processing");?>
...</label>
                                    <?php if ($_smarty_tpl->tpl_vars['canEdit']->value) {?>
                                        <?php if (!$_smarty_tpl->tpl_vars['row']->value['is_notified']) {?>
                                            <button class="btn btn-xs btn-default js_school-tuition" data-handle="notify" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['tuition_id'];?>
">
                                                <?php echo __("Notify");?>

                                            </button>
                                        <?php }?>
                                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/tuitions/newchild/<?php echo $_smarty_tpl->tpl_vars['row']->value['tuition_id'];?>
" class="btn btn-xs btn-default"><?php echo __("Add child");?>
</a>
                                    <?php }?>
                                    
                                    <?php if ($_smarty_tpl->tpl_vars['canEdit']->value) {?>
                                        <?php if ($_smarty_tpl->tpl_vars['row']->value['paid_amount'] != $_smarty_tpl->tpl_vars['row']->value['total_amount']) {?>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/tuitions/edit/<?php echo $_smarty_tpl->tpl_vars['row']->value['tuition_id'];?>
"
                                               class="btn btn-xs btn-default"><?php echo __("Edit");?>
</a>
                                        <?php }?>
                                        <button class="btn btn-xs btn-danger js_school-tuition" data-handle="remove" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-month="<?php echo $_smarty_tpl->tpl_vars['row']->value['month'];?>
"
                                                    data-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['tuition_id'];?>
"><?php echo __("Delete");?>
</button>
                                    <?php }?>
                                </td>
                            </tr>

                            <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                            <?php $_smarty_tpl->_assignInScope('totalPaid', $_smarty_tpl->tpl_vars['totalPaid']->value+$_smarty_tpl->tpl_vars['row']->value['paid_amount']);
?>
                            <?php $_smarty_tpl->_assignInScope('totalPaidCount', $_smarty_tpl->tpl_vars['totalPaidCount']->value+$_smarty_tpl->tpl_vars['row']->value['paid_count']);
?>
                            <?php $_smarty_tpl->_assignInScope('totalTution', $_smarty_tpl->tpl_vars['totalTution']->value+$_smarty_tpl->tpl_vars['row']->value['total_amount']);
?>
                            <?php $_smarty_tpl->_assignInScope('month', $_smarty_tpl->tpl_vars['row']->value['month']);
?>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        
                        <tr>
                            <td colspan="2" align="center" style="vertical-align:middle"><strong><?php echo __("Month total");?>
</strong></td>
                            <td class="text-right" style="vertical-align:middle"><strong><span class="tuition_total_month_<?php echo str_replace('/','_',$_smarty_tpl->tpl_vars['month']->value);?>
"><?php echo moneyFormat($_smarty_tpl->tpl_vars['totalPaid']->value);?>
</span></strong></td>
                            <td class="text-right" style="vertical-align:middle"><strong><span class="tuition_total_month_<?php echo str_replace('/','_',$_smarty_tpl->tpl_vars['month']->value);?>
"><?php echo moneyFormat($_smarty_tpl->tpl_vars['totalTution']->value);?>
</span></strong></td>
                            <td align="center" style="vertical-align:middle"><strong><span class="tuition_count_month_<?php echo str_replace('/','_',$_smarty_tpl->tpl_vars['month']->value);?>
"><?php echo $_smarty_tpl->tpl_vars['totalPaidCount']->value;?>
</span></strong></td>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "newchild") {?>
            <?php $_smarty_tpl->_subTemplateRender("file:ci/school/school.tuitions.newchild.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "history") {?>
            <?php $_smarty_tpl->_subTemplateRender("file:ci/school/school.tuitions.history.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "detail") {?>
            <?php $_smarty_tpl->_subTemplateRender("file:ci/school/school.tuitions.detail.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "edit") {?>
            <?php $_smarty_tpl->_subTemplateRender("file:ci/school/school.tuitions.edit.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
            <div class="panel-body">
                <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_tuition.php">
                    <input type="hidden" name="school_username" id="school_username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                    <input type="hidden" name="do" value="add_tuition"/>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left"><?php echo __("Class");?>
 (*)</label>
                        <div class="col-sm-9">
                            <div class="col-xs-5">
                                <select name="class_id" id="tuition_class_id" class="form-control" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" required autofocus>
                                    <option value=""><?php echo __("Select class");?>
...</option>
                                    <?php $_smarty_tpl->_assignInScope('class_level', -1);
?>
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['classes']->value, 'class');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class']->value) {
?>
                                        <?php if (($_smarty_tpl->tpl_vars['class_level']->value != $_smarty_tpl->tpl_vars['class']->value['class_level_id'])) {?>
                                            <option value="" disabled style="color: blue">-----<?php echo $_smarty_tpl->tpl_vars['class']->value['class_level_name'];?>
-----</option>
                                        <?php }?>
                                        <option value="<?php echo $_smarty_tpl->tpl_vars['class']->value['group_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['class']->value['group_title'];?>
</option>
                                        <?php $_smarty_tpl->_assignInScope('class_level', $_smarty_tpl->tpl_vars['class']->value['class_level_id']);
?>
                                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left"><?php echo __("Month");?>
 (*)</label>
                        <div class="col-sm-9">
                            <div class='col-sm-5'>
                                <div class='input-group date' id='month_picker'>
                                    <input type='text' name="month" id="month" class="form-control" placeholder="<?php echo __("Month");?>
 (*)" required/>
                                    <span class="input-group-addon"><span class="fas fa-calendar-alt"></span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left"><?php echo __("Number of school days");?>
 (*)</label>
                        <div class="col-sm-9">
                            <div class="col-sm-2">
                                <input type='number' min="1" max="31" name="day_of_month" id="day_of_month" value="<?php echo $_smarty_tpl->tpl_vars['attCount']->value;?>
" class="form-control"/>
                            </div>
                            <span class="help-block"><?php echo __("The number of working days in the selected month (except Saturday and Sunday). You can edit after selecting month");?>
.</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left"><?php echo __("Notify immediately");?>
?</label>
                        <div class="col-sm-9">
                            <div class="onoffswitch">
                                <input type="checkbox" name="notify_immediately" class="onoffswitch-checkbox" id="notify_immediately">
                                <label class="onoffswitch-label" for="notify_immediately"></label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left"><?php echo __("Description");?>
</label>
                        <div class="col-sm-9">
                            <textarea class="form-control" name="description" rows="2"></textarea>
                        </div>
                    </div>
                    <div class="form-group" id="getFixed">
                        <div class="col-sm-9 col-sm-offset-3">
                            <button type="submit" class="btn btn-primary padrl30"><?php echo __("Save");?>
</button>
                            <div id="service_usage_open_dialog" class="x-hidden" title="<?php echo mb_strtoupper(__("Service usage in previous month"), 'UTF-8');?>
"></div>
                            <a href="#" class="btn btn-default js_tuition_service_usage x-hidden"><?php echo __("Check service usage");?>
</a>
                            <label id="loading" class="btn btn-info x-hidden"><?php echo __("Loading");?>
...</label>
                        </div>
                    </div>
                    <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                    <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                    <div class="table-responsive" id="children_list"></div>
                    <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                    <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                </form>
            </div>
        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "summarypaid") {?>
            <div class="panel-body with-table">
                <div class="row">
                    <div class='col-sm-12'>
                        <div class='col-sm-3'>
                            <div class="form-group">
                                <select name="status" id="status" class="form-control js_change_status_summary" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
">
                                    <option value=""><?php echo __("Select status");?>
...</option>
                                    <option value="2"><?php echo __("Paid");?>
</option>
                                    <option value="1"><?php echo __("Waiting confirmation");?>
</option>
                                    <option value="0"><?php echo __("Not paid yet");?>
</option>
                                </select>
                            </div>
                        </div>
                        <div class='col-sm-3'>
                            <div class="form-group">
                                <?php $_smarty_tpl->_assignInScope('class_level', -1);
?>
                                <select name="class_id" id="class_id" class="form-control">
                                    <option value=""><?php echo mb_strtoupper(__("Whole school"), 'UTF-8');?>
</option>
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['classes']->value, 'class');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class']->value) {
?>
                                        <?php if (($_smarty_tpl->tpl_vars['class_level']->value != $_smarty_tpl->tpl_vars['class']->value['class_level_id'])) {?>
                                            <option value="" disabled>-----<?php echo $_smarty_tpl->tpl_vars['class']->value['class_level_name'];?>
-----</option>
                                        <?php }?>
                                        <option value="<?php echo $_smarty_tpl->tpl_vars['class']->value['group_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['class']->value['group_title'];?>
</option>
                                        <?php $_smarty_tpl->_assignInScope('class_level', $_smarty_tpl->tpl_vars['class']->value['class_level_id']);
?>
                                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                </select>
                            </div>
                        </div>
                        <div class='col-sm-3 x-hidden' id="fromDateClass">
                            <div class='input-group date' id='fromdatepicker'>
                                <input type='text' name="fromDate" id="fromDate" value="<?php echo $_smarty_tpl->tpl_vars['fromDate']->value;?>
" class="form-control" placeholder="<?php echo __("From date");?>
"/>
                                <span class="input-group-addon">
                                    <span class="fas fa-calendar-alt"></span>
                                </span>
                            </div>
                        </div>
                        <div class='col-sm-3 x-hidden' id="toDateClass">
                            <div class="form-group">
                                <div class='input-group date' id='todatepicker'>
                                    <input type='text' name="toDate" id="toDate" value="<?php echo $_smarty_tpl->tpl_vars['toDate']->value;?>
" class="form-control" placeholder="<?php echo __("To date");?>
"/>
                                    <span class="input-group-addon">
                                    <span class="fas fa-calendar-alt"></span>
                                </span>
                                </div>
                            </div>
                        </div>
                        <div class='col-sm-3 x-hidden' id="cashier">
                            <div class="form-group">
                                <select name="cashier_id" id="cashier_id" class="form-control" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
">
                                    <option value=""><?php echo __("Everyone");?>
...</option>
                                    <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['managers']->value, 'manager');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['manager']->value) {
?>
                                        <option value="<?php echo $_smarty_tpl->tpl_vars['manager']->value['user_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
. <?php echo $_smarty_tpl->tpl_vars['manager']->value['user_fullname'];?>
</option>
                                        <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                </select>
                            </div>
                        </div>
                        <div class='col-sm-3'>
                            <div class="form-group">
                                <a href="#" id="search" class="btn btn-default js_school-tuition-paid-search" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['school']->value['page_id'];?>
" disabled="true"><?php echo __("Search");?>
</a>
                                <label id="loading" class="btn btn-info x-hidden"><?php echo __("Loading");?>
...</label>
                            </div>
                        </div>
                        <div class='col-sm-3'>
                            <div class="form-group">
                                <a href="#" id="export_excel" class="btn btn-success js_school-tuition-summary-export-excel" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['school']->value['page_id'];?>
" disabled="true"><?php echo __("Export to Excel");?>
</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="summary_paid_list"></div>
            </div>
        <?php }?>
    </div>
</div>

<?php }
}
