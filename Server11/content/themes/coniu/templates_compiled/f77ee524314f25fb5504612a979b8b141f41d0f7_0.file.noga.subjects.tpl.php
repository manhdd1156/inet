<?php
/* Smarty version 3.1.31, created on 2021-04-22 10:51:05
  from "D:\workplace\Server11\content\themes\coniu\templates\ci\noga\noga.subjects.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_6080f2a9283944_21805558',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'f77ee524314f25fb5504612a979b8b141f41d0f7' => 
    array (
      0 => 'D:\\workplace\\Server11\\content\\themes\\coniu\\templates\\ci\\noga\\noga.subjects.tpl',
      1 => 1619063437,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:ci/noga/ajax.noga.subjectlist.tpl' => 1,
  ),
),false)) {
function content_6080f2a9283944_21805558 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
            <div class="pull-right flip">
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/noga/subjects/add" class="btn btn-default">
                    <i class="fa fa-plus"></i> <?php echo __("Add New");?>

                </a>
            </div>
        <?php }?>
        <i class="fa fa-university fa-fw fa-lg pr10"></i>
        <?php echo __("Subject");?>

        <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == "edit") {?>
            &rsaquo; <?php echo $_smarty_tpl->tpl_vars['data']->value['subject_name'];?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
            &rsaquo; <?php echo __('Create subject');?>

        <?php }?>
    </div>
    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
        <div class="panel-body with-table">
            <div id="subject_list">
                <?php $_smarty_tpl->_subTemplateRender("file:ci/noga/ajax.noga.subjectlist.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

            </div>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "edit") {?>
            <div class="panel-body">
                <form class="js_ajax-forms form-horizontal" data-url="ci/bo/noga/bo_noga_subject.php">
                    <input type="hidden" name="do" value="edit"/>
                    <input type="hidden" name="subject_id" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['subject_id'];?>
"/>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">
                            <?php echo __("Subject name");?>
 (*)
                        </label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" name="subject_name" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['subject_name'];?>
" required autofocus maxlength="255">
                        </div>
                        <div class="col-sm-4">
                        <input type="checkbox" name="re_exam" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['re_exam'];?>
" <?php if (($_smarty_tpl->tpl_vars['data']->value['re_exam'] == 1)) {?>checked<?php }?>>
                        <label for="re_exam"> <?php echo __("re_exam");?>
</label>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-3">
                            <button type="submit" class="btn btn-primary padrl30"><?php echo __("Save");?>
</button>
                        </div>
                    </div>

                    <!-- success -->
                    <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                    <!-- success -->

                    <!-- error -->
                    <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                    <!-- error -->
                </form>
            </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
        <div class="panel-body">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/noga/bo_noga_subject.php">
                <input type="hidden" name="do" value="add"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">
                        <?php echo __("Subject name");?>
 (*)
                    </label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="subject_name" placeholder="<?php echo __("Subject name");?>
" required autofocus maxlength="255">
                    </div>
                    <div class="col-sm-4">
                    <input type="checkbox"  name="re_exam" >
                    <label class = "control-label"> <?php echo __("re exam");?>
</label>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30"><?php echo __("Save");?>
</button>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>

    <?php }?>
</div><?php }
}
