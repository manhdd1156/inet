<?php
/* Smarty version 3.1.31, created on 2021-04-16 14:20:14
  from "D:\workplace\Server11\content\themes\coniu\templates\ci\school\ajax.school.attendance.child.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_60793aae230ea0_37923762',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '4d81edc97d9df18ce076e5f73e45d72886738cd2' => 
    array (
      0 => 'D:\\workplace\\Server11\\content\\themes\\coniu\\templates\\ci\\school\\ajax.school.attendance.child.tpl',
      1 => 1575865657,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60793aae230ea0_37923762 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div><strong><?php echo $_smarty_tpl->tpl_vars['data']->value['child']['child_name'];?>
&nbsp;(<?php echo __("Total");?>
: <?php echo count($_smarty_tpl->tpl_vars['data']->value['attendance']['attendance']);?>
&nbsp;|&nbsp;<?php echo __("Present");?>
: <?php echo $_smarty_tpl->tpl_vars['data']->value['attendance']['present_count'];?>
&nbsp;|&nbsp;<?php echo __("Absence");?>
: <?php echo $_smarty_tpl->tpl_vars['data']->value['attendance']['absence_count'];?>
)</strong></div>
<table class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th>#</th>
        <th><?php echo __("Attendance date");?>
</th>
        <th><?php echo __("Status");?>
</th>
        <th><?php echo __("Reason");?>
</th>
        <th><?php echo __("Teacher");?>
</th>
        <th><?php echo __("Actions");?>
</th>
    </tr>
    </thead>
    <tbody>
    <input type="hidden" name="child_id" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['child']['child_id'];?>
"
    <?php $_smarty_tpl->_assignInScope('disabled', !canEdit($_smarty_tpl->tpl_vars['username']->value,'attendance'));
?>
    <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['data']->value['attendance']['attendance'], 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
        <tr>
            <td class="text-center"><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</td>
            <td class="text-center"><?php echo $_smarty_tpl->tpl_vars['row']->value['attendance_date'];?>
</td>
            <td class="text-center">
                <input type="hidden" name="attendanceIds[]" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['attendance_id'];?>
">
                <input type="hidden" name="allOldStatus[]" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['status'];?>
">
                <select name="allStatus[]" id="status" <?php if ($_smarty_tpl->tpl_vars['disabled']->value) {?>disabled<?php }?>>
                    <option value="<?php echo @constant('ATTENDANCE_ABSENCE');?>
" <?php if ($_smarty_tpl->tpl_vars['row']->value['status'] == @constant('ATTENDANCE_ABSENCE')) {?>selected<?php }?>>
                        --------<?php echo mb_strtoupper(__("With permission"), 'UTF-8');?>

                    </option>
                    <option value="<?php echo @constant('ATTENDANCE_PRESENT');?>
" <?php if ($_smarty_tpl->tpl_vars['row']->value['status'] == @constant('ATTENDANCE_PRESENT')) {?>selected<?php }?>>
                        <?php echo __("Present");?>

                    </option>
                    <?php if ($_smarty_tpl->tpl_vars['school']->value['attendance_use_come_late']) {?>
                        <option value="<?php echo @constant('ATTENDANCE_COME_LATE');?>
" <?php if ($_smarty_tpl->tpl_vars['row']->value['status'] == @constant('ATTENDANCE_COME_LATE')) {?>selected<?php }?>>
                            <?php echo mb_strtoupper(__("Come late"), 'UTF-8');?>

                        </option>
                    <?php }?>
                    <?php if ($_smarty_tpl->tpl_vars['school']->value['attendance_use_leave_early']) {?>
                        <option value="<?php echo @constant('ATTENDANCE_EARLY_LEAVE');?>
" <?php if ($_smarty_tpl->tpl_vars['row']->value['status'] == @constant('ATTENDANCE_EARLY_LEAVE')) {?>selected<?php }?>>
                            <?php echo mb_strtoupper(__("Leave early"), 'UTF-8');?>

                        </option>
                    <?php }?>
                    <?php if ($_smarty_tpl->tpl_vars['school']->value['attendance_absence_no_reason']) {?>
                        <option value="<?php echo @constant('ATTENDANCE_ABSENCE_NO_REASON');?>
" <?php if ($_smarty_tpl->tpl_vars['row']->value['status'] == @constant('ATTENDANCE_ABSENCE_NO_REASON')) {?>selected<?php }?>>
                            <?php echo __("Without permission");?>

                        </option>
                    <?php }?>
                    <?php if (($_smarty_tpl->tpl_vars['row']->value['status'] == @constant('ATTENDANCE_EARLY_LEAVE') && !$_smarty_tpl->tpl_vars['school']->value['attendance_use_leave_early'])) {?>
                        <option value="<?php echo @constant('ATTENDANCE_EARLY_LEAVE');?>
" selected>
                            <?php echo mb_strtoupper(__("Leave early"), 'UTF-8');?>

                        </option>
                    <?php }?>
                    <?php if (($_smarty_tpl->tpl_vars['row']->value['status'] == @constant('ATTENDANCE_COME_LATE') && !$_smarty_tpl->tpl_vars['school']->value['attendance_use_come_late'])) {?>
                        <option value="<?php echo @constant('ATTENDANCE_COME_LATE');?>
" selected>
                            <?php echo mb_strtoupper(__("Come late"), 'UTF-8');?>

                        </option>
                    <?php }?>
                    <?php if (($_smarty_tpl->tpl_vars['row']->value['status'] == @constant('ATTENDANCE_ABSENCE_NO_REASON') && !$_smarty_tpl->tpl_vars['school']->value['attendance_absence_no_reason'])) {?>
                        <option value="<?php echo @constant('ATTENDANCE_ABSENCE_NO_REASON');?>
" selected>
                            <?php echo mb_strtoupper(__("Without permission"), 'UTF-8');?>

                        </option>
                    <?php }?>
                </select>
            </td>
            <td>
                <input type="text" name="allReasons[]" maxlength="512" style="width: 100%" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['reason'];?>
" placeholder="<?php echo __("Absent reason");?>
" <?php if ($_smarty_tpl->tpl_vars['disabled']->value) {?>disabled<?php }?>>
            </td>
            <td><?php echo $_smarty_tpl->tpl_vars['row']->value['user_fullname'];?>
</td>
            <td>
                <button class="btn btn-danger btn-xs delete_attendance_child" data-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['attendance_detail_id'];?>
" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"><?php echo __("Delete");?>
</button>
            </td>
        </tr>
        <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>


    <?php if (count($_smarty_tpl->tpl_vars['data']->value['attendance']['attendance']) == 0) {?>
        <tr class="odd">
            <td valign="top" align="center" colspan="5" class="dataTables_empty">
                <?php echo __("No data available in table");?>

            </td>
        </tr>
    <?php }?>
    </tbody>
</table><?php }
}
