<?php
/* Smarty version 3.1.31, created on 2021-03-31 10:34:55
  from "D:\workplace\Server11\content\themes\coniu\templates\ci\school\school.useservices.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_6063eddfe61de1_04429708',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '19443952ad79e9d6b74a67088d503667435ec375' => 
    array (
      0 => 'D:\\workplace\\Server11\\content\\themes\\coniu\\templates\\ci\\school\\school.useservices.tpl',
      1 => 1552404702,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:ci/school/ajax.service.foodsvr.tpl' => 1,
  ),
),false)) {
function content_6063eddfe61de1_04429708 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <?php if (($_smarty_tpl->tpl_vars['sub_view']->value == "history") && $_smarty_tpl->tpl_vars['canEdit']->value) {?>
            <div class="pull-right flip">
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/useservices/reg" class="btn btn-default">
                    <i class="fa fa-plus"></i> <?php echo __("Register service");?>

                </a>
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/useservices/class" class="btn btn-default">
                    <i class="fa fa-square"></i><?php echo __("Count-based service summary");?>

                </a>
            </div>
        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "reg") {?>
            <div class="pull-right flip">
                <?php if ($_smarty_tpl->tpl_vars['canEdit']->value) {?>
                    <a href="https://blog.coniu.vn/huong-dan-dang-ky-dich-vu/" target="_blank" class="btn btn-info btn_guide">
                        <i class="fa fa-info"></i> <?php echo __("Register service guide");?>

                    </a>
                <?php }?>
                <?php if ($_smarty_tpl->tpl_vars['canEdit']->value && $_smarty_tpl->tpl_vars['school']->value['tuition_use_mdservice_deduction']) {?>
                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/useservices/deduction" class="btn btn-default">
                        <i class="fa fa-square"></i> <?php echo __("Deduction note");?>

                    </a>
                <?php }?>
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/useservices/history" class="btn btn-default">
                    <i class="fa fa-history"></i> <?php echo __("Service usage information");?>

                </a>
            </div>
        <?php }?>
        <i class="fa fa-life-ring fa-fw fa-lg pr10"></i>
        <?php echo __("Service");?>

        <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == "reg") {?>
            &rsaquo; <?php echo __('Register service');?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "deduction") {?>
            &rsaquo; <?php echo __('Deduction note');?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "history") {?>
            &rsaquo; <?php echo __('Service usage information');?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "foodsvr") {?>
            &rsaquo; <?php echo __('Daily food service summary');?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "class") {?>
            &rsaquo; <?php echo __('Count-based service summary');?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "classdate") {?>
            &rsaquo; <?php echo __('Count-based service summary');?>

        <?php }?>
    </div>
    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == "reg") {?>
        <div class="panel-body with-table">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_service.php">
                <input type="hidden" name="school_username" id="school_username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                <input type="hidden" name="do" id="do" value="reg"/>
                <div class="form-group pl5">
                    <div class='col-sm-4'>
                        <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                        <?php $_smarty_tpl->_assignInScope('type', -1);
?>
                        <select name="service_id" id="reg_service_id" class="form-control">
                            <option value=""><?php echo __('Select service');?>
...</option>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['services']->value, 'service');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['service']->value) {
?>
                                <?php if (($_smarty_tpl->tpl_vars['type']->value != $_smarty_tpl->tpl_vars['service']->value['type'])) {?>
                                    <option value="" disabled>-----
                                        <?php if ($_smarty_tpl->tpl_vars['service']->value['type'] == SERVICE_TYPE_MONTHLY) {?>
                                            <?php echo __("Monthly");?>

                                        <?php } elseif ($_smarty_tpl->tpl_vars['service']->value['type'] == SERVICE_TYPE_DAILY) {?>
                                            <?php echo __("Daily");?>

                                        <?php } else { ?>
                                            <?php echo __("Count-based");?>

                                        <?php }?>-----</option>
                                <?php }?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
" data-type="<?php echo $_smarty_tpl->tpl_vars['service']->value['type'];?>
"><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
 - <?php echo $_smarty_tpl->tpl_vars['service']->value['service_name'];?>
</option>
                                <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                                <?php $_smarty_tpl->_assignInScope('type', $_smarty_tpl->tpl_vars['service']->value['type']);
?>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </select>
                    </div>
                    <div class='col-sm-3'>
                        <?php $_smarty_tpl->_assignInScope('class_level', -1);
?>
                        <select name="class_id" id="class_id" class="form-control">
                            <option value=""><?php echo mb_strtoupper(__("Whole school"), 'UTF-8');?>
</option>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['classes']->value, 'class');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class']->value) {
?>
                                <?php if (($_smarty_tpl->tpl_vars['class_level']->value != $_smarty_tpl->tpl_vars['class']->value['class_level_id'])) {?>
                                    <option value="" disabled>-----<?php echo $_smarty_tpl->tpl_vars['class']->value['class_level_name'];?>
-----</option>
                                <?php }?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['class']->value['group_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['class']->value['group_title'];?>
</option>
                                <?php $_smarty_tpl->_assignInScope('class_level', $_smarty_tpl->tpl_vars['class']->value['class_level_id']);
?>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </select>
                    </div>
                    <div class="col-sm-3 x-hidden" id="using_at_div">
                        <div class='input-group date' id='using_time_picker'>
                            <input type='text' name="using_at" id="using_at" class="form-control" placeholder="<?php echo __("Using time");?>
 (*)" required/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>

                    <div class='col-sm-2'>
                        <div class="form-group pl10">
                            <a href="#" id="search" class="btn btn-default js_service-search-reg" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" disabled="true"><?php echo __("Search");?>
</a>
                            <label id="loading" class="btn btn-info x-hidden"><?php echo __("Loading");?>
...</label>
                        </div>
                    </div>
                </div>
                <div class="table-responsive" id="child_list" name="child_list"></div>

                <div class="form-group pl5" id="service_btnSave">
                    <div class="col-sm-9">
                        <button type="submit" class="btn btn-primary padrl30" disabled><?php echo __("Save");?>
</button>
                    </div>
                </div>
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
            </form>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "deduction") {?>
        <div class="panel-body with-table">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_service.php">
                <input type="hidden" name="school_username" id="school_username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                <input type="hidden" name="do" id="do" value="deduction"/>
                <div class="form-group pl5">
                    <div>
                        <trong>Chức năng Ghi chú giảm trừ áp dụng cho trường hợp trẻ đăng ký sử dụng dịch vụ thu phí theo THÁNG và theo ĐIỂM DANH. Nếu có ngày trẻ đi học nhưng KHÔNG sử dụng dịch vụ đó, nhà trường lưu lại trường hợp này để giảm trừ khi tính học phí</trong>.
                    </div>
                </div>
                <div class="form-group pl5">
                    <div class='col-sm-4'>
                        <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                        <?php $_smarty_tpl->_assignInScope('type', -1);
?>
                        <select name="service_id" id="service_id" class="form-control" required>
                            <option value=""><?php echo __('Select service');?>
...</option>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['services']->value, 'service');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['service']->value) {
?>
                                <?php if (($_smarty_tpl->tpl_vars['service']->value['type'] == SERVICE_TYPE_MONTHLY) || ($_smarty_tpl->tpl_vars['service']->value['type'] == SERVICE_TYPE_DAILY)) {?> <?php echo __("Daily");?>

                                    <?php if (($_smarty_tpl->tpl_vars['type']->value != $_smarty_tpl->tpl_vars['service']->value['type'])) {?>
                                        <option value="" disabled>-----<?php if ($_smarty_tpl->tpl_vars['service']->value['type'] == SERVICE_TYPE_MONTHLY) {
echo __("Monthly");
} else {
echo __("Daily");
}?>-----</option>
                                    <?php }?>

                                    <option value="<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
 - <?php echo $_smarty_tpl->tpl_vars['service']->value['service_name'];?>
</option>
                                    <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                                    <?php $_smarty_tpl->_assignInScope('type', $_smarty_tpl->tpl_vars['service']->value['type']);
?>
                                <?php }?>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </select>
                    </div>
                    <div class='col-sm-3'>
                        <?php $_smarty_tpl->_assignInScope('class_level', -1);
?>
                        <select name="class_id" id="class_id" class="form-control" required>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['classes']->value, 'class');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class']->value) {
?>
                                <?php if (($_smarty_tpl->tpl_vars['class_level']->value != $_smarty_tpl->tpl_vars['class']->value['class_level_id'])) {?>
                                    <option value="" disabled>-----<?php echo $_smarty_tpl->tpl_vars['class']->value['class_level_name'];?>
-----</option>
                                <?php }?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['class']->value['group_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['class']->value['group_title'];?>
</option>
                                <?php $_smarty_tpl->_assignInScope('class_level', $_smarty_tpl->tpl_vars['class']->value['class_level_id']);
?>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </select>
                    </div>
                    <div class="col-sm-3" id="using_at_div">
                        <div class='input-group date' id='using_time_picker'>
                            <input type='text' name="deduction_date" id="deduction_date" class="form-control" placeholder="<?php echo __("Deduction date");?>
 (*)" required/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                    <div class='col-sm-2'>
                        <div class="form-group pl10">
                            <a href="#" id="search" class="btn btn-default js_service-search-deduction" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"><?php echo __("Search");?>
</a>
                            <label id="loading" class="btn btn-info x-hidden"><?php echo __("Loading");?>
...</label>
                        </div>
                    </div>
                </div>
                <div class="table-responsive" id="child_list" name="child_list"></div>

                <div class="form-group pl5" id="service_btnSave">
                    <div class="col-sm-9">
                        <button type="submit" class="btn btn-primary padrl30" disabled><?php echo __("Save");?>
</button>
                    </div>
                </div>
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
            </form>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "history") {?>
        <div class="panel-body with-table">
            <form class="js_ajax-forms form-horizontal" data-url="#">
                <div class="form-group pl5">
                    <div class='col-sm-4'>
                        <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                        <?php $_smarty_tpl->_assignInScope('type', -1);
?>
                        <select name="service_id" id="service_id" class="form-control js_school-history-serviceid">
                            <option value=""><?php echo __('All services');?>
</option>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['services']->value, 'service');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['service']->value) {
?>
                                <?php if (($_smarty_tpl->tpl_vars['type']->value != $_smarty_tpl->tpl_vars['service']->value['type'])) {?>
                                    <option value="" disabled>-----
                                        <?php if ($_smarty_tpl->tpl_vars['service']->value['type'] == SERVICE_TYPE_MONTHLY) {?> <?php echo __("Monthly");?>

                                        <?php } elseif ($_smarty_tpl->tpl_vars['service']->value['type'] == SERVICE_TYPE_DAILY) {?> <?php echo __("Daily");?>

                                        <?php } else { ?> <?php echo __("Count-based");?>

                                        <?php }?>-----</option>
                                <?php }?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['service']->value['service_id'];?>
" data-type="<?php echo $_smarty_tpl->tpl_vars['service']->value['type'];?>
"><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
 - <?php echo $_smarty_tpl->tpl_vars['service']->value['service_name'];?>
</option>
                                <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                                <?php $_smarty_tpl->_assignInScope('type', $_smarty_tpl->tpl_vars['service']->value['type']);
?>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </select>
                    </div>
                    <div class='col-sm-3'>
                        
                        <select name="class_id" id="service_class_id" class="form-control" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
">
                            <option value=""><?php echo mb_strtoupper(__("Whole school"), 'UTF-8');?>
</option>
                            <?php $_smarty_tpl->_assignInScope('class_level', -1);
?>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['classes']->value, 'class');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class']->value) {
?>
                                <?php if (($_smarty_tpl->tpl_vars['class_level']->value != $_smarty_tpl->tpl_vars['class']->value['class_level_id'])) {?>
                                    <option value="" disabled style="color: blue">-----<?php echo $_smarty_tpl->tpl_vars['class']->value['class_level_name'];?>
-----</option>
                                <?php }?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['class']->value['group_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['class']->value['group_title'];?>
</option>
                                <?php $_smarty_tpl->_assignInScope('class_level', $_smarty_tpl->tpl_vars['class']->value['class_level_id']);
?>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </select>
                    </div>
                    <div class='col-sm-5'>
                        <select name="child_id" id="service_child_id" class="form-control">
                            <option value=""><?php echo __("Select child");?>
...</option>
                        </select>
                    </div>
                </div>
                <div class="form-group pl5">
                    <div class="col-sm-3">
                        <div class='input-group date' id='history_begin_picker'>
                            <input type='text' name="begin" id="begin" class="form-control" placeholder="<?php echo __("From date");?>
"/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class='input-group date' id='history_end_picker'>
                            <input type='text' name="end" id="end" class="form-control" placeholder="<?php echo __("To date");?>
"/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                    <div class='col-sm-2'>
                        <div class="form-group pl10">
                            <a href="#" id="search" class="btn btn-default js_service-search-history" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"><?php echo __("Search");?>
</a>
                            <label id="loading" class="btn btn-info x-hidden"><?php echo __("Loading");?>
...</label>
                        </div>
                    </div>
                </div>
                <div class="table-responsive" id="usage_history" name="usage_history"></div>
            </form>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "class") {?>
        <div class="panel-body with-table">
            <form class="js_ajax-forms form-horizontal" data-url="#">
                <div class="form-group pl5">
                    <div class='col-sm-3'>
                        <select name="class_id" id="class_id" class="form-control" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
">
                            <option value=""><?php echo __("Select class");?>
...</option>
                            <?php $_smarty_tpl->_assignInScope('class_level', -1);
?>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['classes']->value, 'class');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class']->value) {
?>
                                <?php if (($_smarty_tpl->tpl_vars['class_level']->value != $_smarty_tpl->tpl_vars['class']->value['class_level_id'])) {?>
                                    <option value="" disabled>-----<?php echo $_smarty_tpl->tpl_vars['class']->value['class_level_name'];?>
-----</option>
                                <?php }?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['class']->value['group_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['class']->value['group_title'];?>
</option>
                                <?php $_smarty_tpl->_assignInScope('class_level', $_smarty_tpl->tpl_vars['class']->value['class_level_id']);
?>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </select>
                    </div>
                    <div class="col-sm-3">
                        <div class='input-group date' id='history_begin_picker'>
                            <input type='text' name="date" id="date" class="form-control" placeholder="<?php echo __("From date");?>
"/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                    <div class='col-sm-2'>
                        <div class="form-group pl10">
                            <a href="#" id="search" class="btn btn-default js_service-search-class" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"><?php echo __("Search");?>
</a>
                            <label id="loading" class="btn btn-info x-hidden"><?php echo __("Loading");?>
...</label>
                        </div>
                    </div>
                </div>
                <div class="table-responsive" id="usage_class" name="usage_class"></div>
            </form>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "classdate") {?>
        <div align="center" class="mt10">
            <strong><?php echo __("List registered countbase service of");?>
 <?php echo $_smarty_tpl->tpl_vars['classTitle']->value;?>
 <?php echo __("day");?>
 <?php echo $_smarty_tpl->tpl_vars['date_pos']->value;?>
</strong>
        </div>
        <div class="panel-body with-table">
            <table class="table table-striped table-bordered table-hover">
                <tbody>
                    <tr>
                        <td align="center" class="align_middle">
                            <strong>#</strong>
                        </td>
                        <td align="center" class="align_middle">
                            <strong><?php echo __("Children list");?>
</strong>
                        </td>
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['servicesCB']->value, 'service');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['service']->value) {
?>
                            <td align="center">
                                <strong><?php echo $_smarty_tpl->tpl_vars['service']->value['service_name'];?>
</strong>
                            </td>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                    </tr>

                    <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['childList']->value, 'child');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['child']->value) {
?>
                        <tr>
                            <td align="center" style="vertical-align: middle"><strong><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</strong></td>
                            <td><strong><?php echo $_smarty_tpl->tpl_vars['child']->value['child_name'];?>
</strong></td>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['servicesCB']->value, 'service');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['service']->value) {
?>
                                <td align="center" style="vertical-align: middle">
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['child']->value['services']['service'], 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
                                        <?php if ($_smarty_tpl->tpl_vars['row']->value['service_id'] == $_smarty_tpl->tpl_vars['service']->value['service_id']) {?>
                                            <?php if ($_smarty_tpl->tpl_vars['row']->value['using_at'] != null) {?>
                                                <i class="fa fa-check" aria-hidden="true" style="color: #26b31a"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times" aria-hidden="true"></i>
                                            <?php }?>
                                        <?php }?>
                                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                </td>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </tr>
                        <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                </tbody>
            </table>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "foodsvr") {?>
        <div class="panel-body with-table">
            <form class="js_ajax-forms form-horizontal" data-url="#">
                <input type="hidden" name="school_username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" id="school_username">
                <div class="form-group center-block">
                    <div class="col-sm-3 col-sm-offset-1">
                        <div class='input-group date' id='history_begin_picker_food'>
                            <input type='text' name="date" id="date" class="form-control" placeholder="<?php echo __("Select date");?>
..."/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                    
                        
                            
                            
                        
                    
                </div>
                <div class="table-responsive" id="foodsvr" name="foodsvr">
                    <?php $_smarty_tpl->_subTemplateRender("file:ci/school/ajax.service.foodsvr.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                </div>
            </form>
        </div>
    <?php }?>
</div><?php }
}
