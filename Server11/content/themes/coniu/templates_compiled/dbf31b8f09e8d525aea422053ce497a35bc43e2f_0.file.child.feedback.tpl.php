<?php
/* Smarty version 3.1.31, created on 2021-03-31 10:51:37
  from "D:\workplace\Server11\content\themes\coniu\templates\ci\child\child.feedback.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_6063f1c9194ed2_22737505',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'dbf31b8f09e8d525aea422053ce497a35bc43e2f' => 
    array (
      0 => 'D:\\workplace\\Server11\\content\\themes\\coniu\\templates\\ci\\child\\child.feedback.tpl',
      1 => 1552404702,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6063f1c9194ed2_22737505 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="panel panel-default">

    <div class="panel-heading with-icon">
        <div class="pull-right flip">
            <a href="https://blog.coniu.vn/huong-dan-phu-huynh-thao-tac-gop-y-nha-truong-tren-webiste-coniu/" class="btn btn-info btn_guide">
                <i class="fa fa-info"></i> <?php echo __("Guide");?>

            </a>
            <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/child/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
/feedback/list" class="btn btn-default">
                    <i class="fa fa-list"></i> <?php echo __("Feedback list");?>

                </a>
            <?php }?>
        </div>
        <i class="fa fa-envelope fa-lg fa-fw pr10"></i>
        <?php echo __("Feedback for the school");?>

        <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
            &rsaquo; <?php echo __('Add New');?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "list") {?>
            &rsaquo; <?php echo __('Lists');?>

        <?php }?>
    </div>

    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
        <div class="panel-body">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/child/bochild_all.php">
                <input type="hidden" name="child_id" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
"/>
                <input type="hidden" name="feedback_level" value="<?php echo @constant('SCHOOL_LEVEL');?>
"/>
                <input type="hidden" name="do" value="feedback"/>

                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Send to");?>
: </label>
                    <div class="col-sm-9">
                        <label class="control-label text-left"><?php echo $_smarty_tpl->tpl_vars['school']->value['page_title'];?>
</label>
                        
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Hiển thị thông tin người gửi");?>
?</label>
                    <div class="col-sm-9">
                        <div class="onoffswitch">
                            <input type="checkbox" name="incognito" class="onoffswitch-checkbox"
                                   id="incognito" checked>
                            <label class="onoffswitch-label" for="incognito"></label>
                        </div>
                        <span class="help-block">
                        <?php echo __("Hiển thị thông tin người gửi? (Tắt là ẩn danh)");?>

                    </span>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Content");?>
 (*)</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="content" rows="6" required></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30"><?php echo __("Save");?>
</button>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "list") {?>
        <div class="panel-body with-table">
            <div><strong><?php echo __("Feedback list");?>
&nbsp;(<?php echo count($_smarty_tpl->tpl_vars['rows']->value);?>
)</strong></div>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th><?php echo __("Feedback for");?>
</th>
                        <th><?php echo __("Time");?>
</th>
                        <th><?php echo __("Content");?>
</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['rows']->value, 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
                            <tr>
                                <td align="center" style="vertical-align:middle"><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</td>
                                <td align="center">
                                    <?php if ($_smarty_tpl->tpl_vars['row']->value['level'] == @constant('SCHOOL_LEVEL')) {?>
                                        <?php echo __("School");?>

                                    <?php } elseif ($_smarty_tpl->tpl_vars['row']->value['level'] == @constant('CLASS_LEVEL')) {?>
                                        <?php echo __("Class");?>

                                    <?php }?>
                                    <?php if ($_smarty_tpl->tpl_vars['row']->value['confirm']) {?>
                                        <i class="fa fa-check" aria-hidden="true"></i>
                                    <?php } else { ?>
                                        <i class="fa fa-times" aria-hidden="true"></i>
                                    <?php }?>
                                </td>
                                <td align="center"><?php echo $_smarty_tpl->tpl_vars['row']->value['created_at'];?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['row']->value['content'];?>
</td>
                            </tr>

                            <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>


                        <?php if ($_smarty_tpl->tpl_vars['idx']->value == 1) {?>
                            <tr class="odd">
                                <td valign="top" align="center" colspan="4" class="dataTables_empty">
                                    <?php echo __("No data available in table");?>

                                </td>
                            </tr>
                        <?php }?>
                    </tbody>
                </table>
            </div>
        </div>
    <?php }?>

</div><?php }
}
