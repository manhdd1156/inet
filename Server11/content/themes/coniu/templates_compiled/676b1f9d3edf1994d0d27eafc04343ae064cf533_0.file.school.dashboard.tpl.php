<?php
/* Smarty version 3.1.31, created on 2021-03-31 09:03:42
  from "D:\workplace\Server11\content\themes\coniu\templates\ci\school\school.dashboard.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_6063d87e3928c2_58217812',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '676b1f9d3edf1994d0d27eafc04343ae064cf533' => 
    array (
      0 => 'D:\\workplace\\Server11\\content\\themes\\coniu\\templates\\ci\\school\\school.dashboard.tpl',
      1 => 1575865657,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6063d87e3928c2_58217812 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!-- Modal danh sách group lớp -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?php echo __("Group class lists");?>
</h4>
            </div>
            <div class="modal-body">
                <ul class="row">
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['classes']->value, '_group');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['_group']->value) {
?>
                        <li class="col-md-3 col-sm-6">
                            <div class="box_dash">
                                <div class="group_img">
                                    <a class="box-picture_dash" href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/groups/<?php echo $_smarty_tpl->tpl_vars['_group']->value['group_name'];?>
" style="background-image:url('<?php echo $_smarty_tpl->tpl_vars['_group']->value['group_picture'];?>
');"></a>
                                </div>
                                <div class="box-content_dash">
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/groups/<?php echo $_smarty_tpl->tpl_vars['_group']->value['group_name'];?>
" class="title">
                                        <?php echo $_smarty_tpl->tpl_vars['_group']->value['group_title'];?>

                                    </a>
                                    <div class="text clearfix">
                                        <div>
                                            <span class="count_member_<?php echo $_smarty_tpl->tpl_vars['_group']->value['group_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['_group']->value['group_members'];?>
</span> <?php echo __("Members");?>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                </ul>
            </div>
        </div>
    </div>
</div>
<div class="" style="position: relative">
    <?php if ($_smarty_tpl->tpl_vars['school']->value['school_step'] != SCHOOL_STEP_FINISH && $_smarty_tpl->tpl_vars['school']->value['grade'] == 0 && $_smarty_tpl->tpl_vars['school']->value['grade'] == 0) {?>
        <div class="school_start" align="center">
            <div class="panel panel-default" style="padding: 20px">
                <h3 style="text-transform: uppercase"><?php echo $_smarty_tpl->tpl_vars['school']->value['page_title'];?>
</h3><br/>
                <p style="font-size: 20px; margin-bottom: 10px"><?php echo __("Chào mừng bạn đến với Coniu");?>
</p>
                <p style="font-size: 16px"><?php echo __("Mời bạn nhập thông tin ban đầu để sử dụng Coniu");?>
</p>
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/classlevels" class="btn btn-primary"><?php echo __("Start");?>
</a>
            </div>
        </div>
    <?php } else { ?>
        <div class="panel panel-default">
            <div class="panel-heading with-icon">
                <div class="pull-right flip">
                    <?php if (canView($_smarty_tpl->tpl_vars['username']->value,"attendance")) {?>
                        <a class="btn btn-warning" href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/reports"><?php echo __("Contact book");?>
</a>
                    <?php }?>
                    <?php if (canView($_smarty_tpl->tpl_vars['username']->value,"pickup")) {?>
                        <a class="btn btn-info" href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/pickup/list"><?php echo __("Late PickUp");?>
</a>
                    <?php }?>
                    <?php if (canView($_smarty_tpl->tpl_vars['username']->value,"attendance")) {?>
                        <a class="btn btn-success" href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/attendance"><?php echo __("Attendance");?>
</a>
                    <?php }?>
                    <?php if (canView($_smarty_tpl->tpl_vars['username']->value,"services")) {?>
                        <a class="btn btn-danger" href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/useservices/history"><?php echo __("Service");?>
</a>
                    <?php }?>
                    <div class="mt10" align="center">
                        <a class="btn btn-primary" href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/pages/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"><?php echo __("School page");?>
</a>
                        <!-- show các group lớp của trường -->
                        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#myModal">
                            <?php echo __("Groups class");?>

                        </button>
                    </div>
                </div>
                <i class="fas fa-tachometer-alt fa-fw fa-lg pr10"></i>
                <?php echo __("Dashboard");?>
:&nbsp;<?php echo $_smarty_tpl->tpl_vars['school']->value['page_title'];?>

            </div>

            <?php if (isAdmin($_smarty_tpl->tpl_vars['username']->value) && (($_smarty_tpl->tpl_vars['school']->value['teacher_count'] == 0) || ($_smarty_tpl->tpl_vars['school']->value['class_count'] == 0) || ($_smarty_tpl->tpl_vars['insights']->value['child_cnt'] == 0))) {?>
                <div class="panel-body color_red">
                    <strong>ĐỂ SỬ DỤNG HỆ THỐNG, BẠN PHẢI NHẬP ĐẦY ĐỦ THÔNG TIN SAU:</strong>
                    <?php if (($_smarty_tpl->tpl_vars['school']->value['teacher_count'] == 0)) {?>
                        <br/><br/>- Chưa có tài khoản giáo viên/nhân viên trong trường. <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/teachers/add"><?php echo __("Add New");?>
</a> | <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/teachers/addexisting"><?php echo __("Add Existing Account");?>
</a>
                    <?php }?>
                    <?php if (($_smarty_tpl->tpl_vars['school']->value['class_count'] == 0)) {?>
                        <br/><br/>- Chưa có lớp nào trong trường. <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/classes/add"><?php echo __("Add New");?>
</a>
                    <?php }?>
                    <?php if (($_smarty_tpl->tpl_vars['school']->value['child_cnt'] == 0)) {?>
                        <br/><br/>- Chưa có trẻ nào trong trường. <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/children/add"><?php echo __("Add New");?>
</a> | <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/children/import"><?php echo __("Import from Excel file");?>
</a>
                    <?php }?>
                    <br/><br/><div class="pull-right flip"><i>Bảng tin sẽ xuất hiện khi bạn nhập đủ thông tin cơ bản.</i></div>
                </div>
            <?php } else { ?>
                <div class="panel-body">
                    <div class="row">
                        
                        <div class="col-sm-6">
                            <?php if (canView($_smarty_tpl->tpl_vars['username']->value,"attendance")) {?>
                                
                                <div class="box-primary">
                                    <div class="box-header">
                                        <i class="fa fa-tasks"></i>
                                        <strong><?php echo __("Attendance");?>
:</strong>&nbsp;<?php echo $_smarty_tpl->tpl_vars['insights']->value['today'];?>

                                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/attendance" title="<?php echo __("Attendance");?>
" class="pull-right flip"><?php echo __("Detail");?>
</a>
                                    </div>
                                    <div class="list-group">
                                        <table class="table table-striped table-bordered table-hover">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th><?php echo __("Class");?>
</th>
                                                <th><?php echo __("Status");?>
</th>
                                                <th><?php echo __("Number of student");?>
</th>
                                                <th><?php echo __("Present");?>
</th>
                                                <th><?php echo __("Absence");?>
</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php $_smarty_tpl->_assignInScope('totalPresent', 0);
?>
                                            <?php $_smarty_tpl->_assignInScope('totalAbsence', 0);
?>
                                            <?php $_smarty_tpl->_assignInScope('total', 0);
?>
                                            <?php $_smarty_tpl->_assignInScope('idx', 1);
?>

                                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['insights']->value['attendances'], 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
                                                <tr>
                                                    <td align="center"><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</td>
                                                    <td><?php echo $_smarty_tpl->tpl_vars['row']->value['group_title'];?>
</td>
                                                    <td align="center">
                                                        <?php if ($_smarty_tpl->tpl_vars['row']->value['is_checked'] == 0) {?>
                                                            <i class="fa fa-times" aria-hidden="true"></i>
                                                        <?php } else { ?>
                                                            <i class="fa fa-check" aria-hidden="true"></i>
                                                        <?php }?>
                                                    </td>
                                                    <td align="center">
                                                        <?php if ($_smarty_tpl->tpl_vars['row']->value['present_count'] > 0 || $_smarty_tpl->tpl_vars['row']->value['absence_count'] > 0) {?>
                                                            <?php echo $_smarty_tpl->tpl_vars['row']->value['present_count']+$_smarty_tpl->tpl_vars['row']->value['absence_count'];?>

                                                        <?php } else { ?>
                                                            <?php echo $_smarty_tpl->tpl_vars['row']->value['total'];?>

                                                        <?php }?>
                                                    </td>
                                                    <td align="center"><?php echo $_smarty_tpl->tpl_vars['row']->value['present_count'];?>
</td>
                                                    <td align="center"><?php echo $_smarty_tpl->tpl_vars['row']->value['absence_count'];?>
</td>

                                                    <?php $_smarty_tpl->_assignInScope('totalPresent', $_smarty_tpl->tpl_vars['totalPresent']->value+$_smarty_tpl->tpl_vars['row']->value['present_count']);
?>
                                                    <?php $_smarty_tpl->_assignInScope('totalAbsence', $_smarty_tpl->tpl_vars['totalAbsence']->value+$_smarty_tpl->tpl_vars['row']->value['absence_count']);
?>
                                                    <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                                                    <?php $_smarty_tpl->_assignInScope('total', $_smarty_tpl->tpl_vars['total']->value+$_smarty_tpl->tpl_vars['row']->value['total']);
?>
                                                </tr>
                                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                            <tr>
                                                <td colspan="3" align="center"><strong><font color="red"><?php echo __('TOTAL OF SCHOOL');?>
</font></strong></td>
                                                <td align="center"><strong><font color="red"><?php echo $_smarty_tpl->tpl_vars['total']->value;?>
</font></strong></td>
                                                <td align="center"><strong><font color="red"><?php echo $_smarty_tpl->tpl_vars['totalPresent']->value;?>
</font></strong></td>
                                                <td align="center"><strong><font color="red"><?php echo $_smarty_tpl->tpl_vars['totalAbsence']->value;?>
</font></strong></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                
                                <?php $_smarty_tpl->_assignInScope('cnt', count($_smarty_tpl->tpl_vars['insights']->value['conabs']['list']));
?>
                                <div id="conabs_list">
                                    <div class="box-primary">
                                        <div class="box-header">
                                            <i class="fa fa-bomb"></i>
                                            <strong><?php echo __("Consecutive absent student");?>
</strong>
                                            <br/>
                                            <?php echo __("Last update");?>
: <?php echo $_smarty_tpl->tpl_vars['insights']->value['conabs']['updated_at'];?>

                                            
                                            <a class="btn btn-xs js_school_conabs_dashboard pull-right flip" data-username = "<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" title="<?php echo __("Refresh");?>
"><strong><i class="fas fa-redo-alt"></i> <?php echo __("Refresh");?>
</strong></a>
                                        </div>

                                        <div class="list-group">
                                            <table class="table table-striped table-bordered table-hover" id="hide_absent">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th><?php echo __("Full name");?>
</th>
                                                    <th><?php echo __("Class");?>
</th>
                                                    <th><?php echo __("Absent day");?>
</th>
                                                    <th><?php echo __("Reason");?>
</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php if ($_smarty_tpl->tpl_vars['cnt']->value > 0) {?>
                                                    <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['insights']->value['conabs']['list'], 'child', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['child']->value) {
?>
                                                        <?php if ($_smarty_tpl->tpl_vars['k']->value < 10) {?>
                                                            <tr>
                                                                <td align="center"><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</td>
                                                                <td><a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/attendance/child/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['child']->value['child_name'];?>
</a></td>
                                                                <td align="center"><?php echo $_smarty_tpl->tpl_vars['child']->value['group_title'];?>
</td>
                                                                <td align="center">
                                                                    <?php if (isset($_smarty_tpl->tpl_vars['child']->value['number_of_absent_day']) && ($_smarty_tpl->tpl_vars['child']->value['number_of_absent_day'] > 0)) {?>
                                                                        <?php ob_start();
echo $_smarty_tpl->tpl_vars['child']->value['number_of_absent_day'];
$_prefixVariable1=ob_get_clean();
if ($_prefixVariable1 > 9) {
echo __("More than");?>
&nbsp;<?php }
echo $_smarty_tpl->tpl_vars['child']->value['number_of_absent_day'];?>
&nbsp;<?php echo __("day(s)");?>

                                                                    <?php } else { ?>
                                                                        <?php echo __("More than");?>
&nbsp;10&nbsp;<?php echo __("day(s)");?>

                                                                    <?php }?>
                                                                </td>
                                                                <td><?php echo $_smarty_tpl->tpl_vars['child']->value['reason'];?>
</td>
                                                            </tr>
                                                            <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                                                        <?php }?>
                                                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                                    <?php if (count($_smarty_tpl->tpl_vars['insights']->value['conabs']['list']) > 10) {?>
                                                        <tr>
                                                            <td colspan="5" align="center" style="padding: 5px">
                                                                <button class="full_absent_but btn btn-xs btn-info mb0"><?php echo __("See More");?>
</button>
                                                            </td>
                                                        </tr>
                                                    <?php }?>
                                                <?php } elseif ($_smarty_tpl->tpl_vars['cnt']->value == 0) {?>
                                                    <tr class="odd">
                                                        <td valign="top" align="center" colspan="5" class="dataTables_empty">
                                                            <?php echo __("No data available in table");?>

                                                        </td>
                                                    </tr>
                                                <?php }?>
                                                </tbody>
                                            </table>
                                            <table class="table table-striped table-bordered table-hover x-hidden" id="full_absent">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th><?php echo __("Full name");?>
</th>
                                                    <th><?php echo __("Class");?>
</th>
                                                    <th><?php echo __("Absent day");?>
</th>
                                                    <th><?php echo __("Reason");?>
</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php if ($_smarty_tpl->tpl_vars['cnt']->value > 0) {?>
                                                    <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['insights']->value['conabs']['list'], 'child');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['child']->value) {
?>
                                                        <tr>
                                                            <td align="center"><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</td>
                                                            <td><a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/attendance/child/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['child']->value['child_name'];?>
</a></td>
                                                            <td align="center"><?php echo $_smarty_tpl->tpl_vars['child']->value['group_title'];?>
</td>
                                                            <td align="center">
                                                                <?php if (isset($_smarty_tpl->tpl_vars['child']->value['number_of_absent_day']) && ($_smarty_tpl->tpl_vars['child']->value['number_of_absent_day'] > 0)) {?>
                                                                    <?php ob_start();
echo $_smarty_tpl->tpl_vars['child']->value['number_of_absent_day'];
$_prefixVariable2=ob_get_clean();
if ($_prefixVariable2 > 9) {
echo __("More than");?>
&nbsp;<?php }
echo $_smarty_tpl->tpl_vars['child']->value['number_of_absent_day'];?>
&nbsp;<?php echo __("day(s)");?>

                                                                <?php } else { ?>
                                                                    <?php echo __("More than");?>
&nbsp;10&nbsp;<?php echo __("day(s)");?>

                                                                <?php }?>
                                                            </td>
                                                            <td><?php echo $_smarty_tpl->tpl_vars['child']->value['reason'];?>
</td>
                                                        </tr>
                                                        <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                                                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                                    <?php if (count($_smarty_tpl->tpl_vars['insights']->value['conabs']['list']) > 10) {?>
                                                        <tr>
                                                            <td colspan="5" align="center" style="padding: 5px">
                                                                <button class="hide_absent_but btn btn-xs btn-info mb0"><?php echo __("Read less");?>
</button>
                                                            </td>
                                                        </tr>
                                                    <?php }?>
                                                <?php } elseif ($_smarty_tpl->tpl_vars['cnt']->value == 0) {?>
                                                    <tr class="odd">
                                                        <td valign="top" align="center" colspan="5" class="dataTables_empty">
                                                            <?php echo __("No data available in table");?>

                                                        </td>
                                                    </tr>
                                                <?php }?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            <?php }?>

                            <?php if (isAdmin($_smarty_tpl->tpl_vars['username']->value) || in_array($_smarty_tpl->tpl_vars['user']->value->_data['user_id'],$_smarty_tpl->tpl_vars['principals']->value)) {?>
                                
                                <div class="box-primary">
                                    <div class="box-header">
                                        <i class="fa fa-info-circle"></i>
                                        <strong><?php echo __("General");?>
</strong>
                                    </div>
                                    <div class="list-group">
                                        <a class="list-group-item" href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/classes">
                                            <span class="badge"><?php echo $_smarty_tpl->tpl_vars['school']->value['class_count'];?>
</span>
                                            <i class="fa fa-graduation-cap"></i> <?php echo __("Class");?>

                                        </a>
                                        <a class="list-group-item" href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/children">
                                            <span class="badge"><?php echo $_smarty_tpl->tpl_vars['insights']->value['child_cnt'];?>
</span>
                                            <i class="fa fa-child"></i> <?php echo __("Children");?>

                                        </a>
                                        <a class="list-group-item">
                                            <span class="badge"><?php echo $_smarty_tpl->tpl_vars['school']->value['male_count'];?>
</span>
                                            <i class="fa fa-male"></i> <?php echo __("Males");?>

                                        </a>
                                        <a class="list-group-item">
                                            <span class="badge"><?php echo $_smarty_tpl->tpl_vars['school']->value['female_count'];?>
</span>
                                            <i class="fa fa-female"></i> <?php echo __("Females");?>

                                        </a>
                                        <a class="list-group-item" href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/teachers">
                                            <span class="badge"><?php echo $_smarty_tpl->tpl_vars['school']->value['teacher_count'];?>
</span>
                                            <i class="fa fa-street-view"></i> <?php echo __("Teacher");?>

                                        </a>
                                    </div>
                                </div>
                            <?php }?>
                        </div>
                        

                        
                        <div class="col-sm-6">
                            <?php if (canView($_smarty_tpl->tpl_vars['username']->value,"medicines")) {?>
                                
                                <div class="box-primary">
                                    <div class="box-header">
                                        <i class="fa fa-medkit"></i>
                                        <strong><?php echo __("Medicines");?>
</strong>&nbsp;
                                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/medicines/all" title="<?php echo __("See All");?>
" class="pull-right flip"><?php echo __("See All");?>
</a>
                                    </div>
                                    <div class="list-group">
                                        <table class="table table-striped table-bordered table-hover">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th><?php echo __("Child");?>
</th>
                                                <th><?php echo __("Class");?>
</th>
                                                <th><?php echo __("Medicine list");?>
</th>
                                                <th><?php echo __("Times/day");?>
</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['insights']->value['medicines'], 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
                                                <?php if (is_array($_smarty_tpl->tpl_vars['row']->value)) {?>
                                                    <tr>
                                                        <td align="center" style="vertical-align:middle"><strong><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</strong></td>
                                                        <td style="vertical-align:middle">
                                                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/medicines/detail/<?php echo $_smarty_tpl->tpl_vars['row']->value['medicine_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['row']->value['child_name'];?>
</a>
                                                            <?php if ($_smarty_tpl->tpl_vars['row']->value['status'] == @constant('MEDICINE_STATUS_NEW')) {?>
                                                                <img src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/content/themes/<?php echo $_smarty_tpl->tpl_vars['system']->value['theme'];?>
/images/new.gif"/>
                                                            <?php } elseif ($_smarty_tpl->tpl_vars['row']->value['status'] == @constant('MEDICINE_STATUS_CONFIRMED')) {?>
                                                                <i class="fa fa-check"></i>
                                                            <?php }?>
                                                            <br/>(<?php echo $_smarty_tpl->tpl_vars['row']->value['birthday'];?>
)
                                                        </td>
                                                        <td align="center" style="vertical-align:middle"><?php echo $_smarty_tpl->tpl_vars['row']->value['group_title'];?>
</td>
                                                        <td style="vertical-align:middle">
                                                            <?php echo $_smarty_tpl->tpl_vars['row']->value['medicine_list'];?>

                                                            <?php if ($_smarty_tpl->tpl_vars['row']->value['source_file'] != null) {?><br/><br/><a href = "<?php echo $_smarty_tpl->tpl_vars['row']->value['source_file'];?>
" target="_blank"><?php echo mb_strtoupper(__("Doctor prescription"), 'UTF-8');?>
</a><?php }?>
                                                        </td>
                                                        <td align="center" style="vertical-align:middle"><?php echo count($_smarty_tpl->tpl_vars['row']->value['detail']);?>
/<?php echo $_smarty_tpl->tpl_vars['row']->value['time_per_day'];?>
</td>
                                                    </tr>
                                                    <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                                                <?php }?>
                                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>


                                            <?php if (count($_smarty_tpl->tpl_vars['insights']->value['medicines']) == 0) {?>
                                                <tr class="odd">
                                                    <td valign="top" align="center" colspan="7" class="dataTables_empty">
                                                        <?php echo __("No data available in table");?>

                                                    </td>
                                                </tr>
                                            <?php }?>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            <?php }?>

                            <?php if (canView($_smarty_tpl->tpl_vars['username']->value,"birthdays") && (count($_smarty_tpl->tpl_vars['insights']->value['birthdays']) > 0)) {?>
                                
                                <div class="box-primary">
                                    <div class="box-header">
                                        <i class="fa fa-birthday-cake"></i>
                                        <strong><?php echo __("Birthday");?>
</strong>&nbsp;
                                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/birthdays" title="<?php echo __("See All");?>
" class="pull-right flip"><?php echo __("See All");?>
</a>
                                    </div>
                                    <div class="list-group">
                                        <table class="table table-striped table-bordered table-hover" id="hide_birthday">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th><?php echo __('Children');?>
</th>
                                                <th><?php echo __('Birthdate');?>
</th>
                                                <th><?php echo __('Class');?>
</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['insights']->value['birthdays'], 'row', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['row']->value) {
?>
                                                <?php if ($_smarty_tpl->tpl_vars['k']->value < 10) {?>
                                                    <tr>
                                                        <td align="center"><?php echo $_smarty_tpl->tpl_vars['k']->value+1;?>
</td>
                                                        <td><?php echo convertText4Web($_smarty_tpl->tpl_vars['row']->value['child_name']);?>
</td>
                                                        <td align="center"><?php echo toSysDate($_smarty_tpl->tpl_vars['row']->value['birthday']);?>
</td>
                                                        <td nowrap="true">
                                                            <?php if ($_smarty_tpl->tpl_vars['row']->value['group_title'] == null) {?>
                                                                <?php echo __("No class");?>

                                                            <?php } else { ?>
                                                                <?php echo convertText4Web($_smarty_tpl->tpl_vars['row']->value['group_title']);?>

                                                            <?php }?>
                                                        </td>
                                                    </tr>
                                                <?php }?>
                                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                            <?php if (count($_smarty_tpl->tpl_vars['insights']->value['birthdays']) > 10) {?>
                                                <tr>
                                                    <td colspan="4" align="center" style="padding: 5px">
                                                        <button class="full_birthday_but btn btn-xs btn-info mb0"><?php echo __("See More");?>
</button>
                                                    </td>
                                                </tr>
                                            <?php }?>
                                            </tbody>
                                        </table>
                                        <table class="table table-striped table-bordered table-hover x-hidden" id="full_birthday">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th><?php echo __('Children');?>
</th>
                                                <th><?php echo __('Birthdate');?>
</th>
                                                <th><?php echo __('Class');?>
</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['insights']->value['birthdays'], 'row', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['row']->value) {
?>
                                                <tr>
                                                    <td align="center"><?php echo $_smarty_tpl->tpl_vars['k']->value+1;?>
</td>
                                                    <td><?php echo convertText4Web($_smarty_tpl->tpl_vars['row']->value['child_name']);?>
</td>
                                                    <td align="center"><?php echo toSysDate($_smarty_tpl->tpl_vars['row']->value['birthday']);?>
</td>
                                                    <td nowrap="true">
                                                        <?php if ($_smarty_tpl->tpl_vars['row']->value['group_title'] == null) {?>
                                                            <?php echo __("No class");?>

                                                        <?php } else { ?>
                                                            <?php echo convertText4Web($_smarty_tpl->tpl_vars['row']->value['group_title']);?>

                                                        <?php }?>
                                                    </td>
                                                </tr>
                                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                            <?php if (count($_smarty_tpl->tpl_vars['insights']->value['birthdays']) > 10) {?>
                                                <tr>
                                                    <td colspan="4" align="center" style="padding: 5px">
                                                        <button class="hide_birthday_but btn btn-xs btn-info mb0"><?php echo __("Read less");?>
</button>
                                                    </td>
                                                </tr>
                                            <?php }?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            <?php }?>

                            <?php if (canView($_smarty_tpl->tpl_vars['username']->value,"birthdays") && (count($_smarty_tpl->tpl_vars['insights']->value['birthday_teacher']) > 0)) {?>
                                
                                <div class="box-primary">
                                    <div class="box-header">
                                        <i class="fa fa-birthday-cake"></i>
                                        <strong><?php echo __("Teacher birthday");?>
</strong>&nbsp;
                                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/birthdays" title="<?php echo __("See All");?>
" class="pull-right flip"><?php echo __("See All");?>
</a>
                                    </div>
                                    <div class="list-group">
                                        <table class="table table-striped table-bordered table-hover" id="hide_birthday_teacher">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th><?php echo __('Teacher');?>
</th>
                                                <th><?php echo __('Birthdate');?>
</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['insights']->value['birthday_teacher'], 'row', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['row']->value) {
?>
                                                <?php if ($_smarty_tpl->tpl_vars['k']->value < 10) {?>
                                                    <tr>
                                                        <td align="center"><?php echo $_smarty_tpl->tpl_vars['k']->value+1;?>
</td>
                                                        <td><?php echo convertText4Web($_smarty_tpl->tpl_vars['row']->value['user_fullname']);?>
</td>
                                                        <td align="center"><?php echo $_smarty_tpl->tpl_vars['row']->value['user_birthdate'];?>
</td>
                                                    </tr>
                                                <?php }?>
                                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                            <?php if (count($_smarty_tpl->tpl_vars['insights']->value['birthday_teacher']) > 10) {?>
                                                <tr>
                                                    <td colspan="3" align="center" style="padding: 5px">
                                                        <button class="full_birthday_teacher_but btn btn-xs btn-info mb0"><?php echo __("See More");?>
</button>
                                                    </td>
                                                </tr>
                                            <?php }?>
                                            </tbody>
                                        </table>
                                        <table class="table table-striped table-bordered table-hover x-hidden" id="full_birthday_teacher">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th><?php echo __('Teacher');?>
</th>
                                                <th><?php echo __('Birthdate');?>
</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['insights']->value['birthday_teacher'], 'row', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['row']->value) {
?>
                                                <tr>
                                                    <td align="center"><?php echo $_smarty_tpl->tpl_vars['k']->value+1;?>
</td>
                                                    <td><?php echo convertText4Web($_smarty_tpl->tpl_vars['row']->value['user_fullname']);?>
</td>
                                                    <td align="center"><?php echo $_smarty_tpl->tpl_vars['row']->value['user_birthdate'];?>
</td>
                                                </tr>
                                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                            <?php if (count($_smarty_tpl->tpl_vars['insights']->value['birthday_teacher']) > 10) {?>
                                                <tr>
                                                    <td colspan="4" align="center" style="padding: 5px">
                                                        <button class="hide_birthday_teacher_but btn btn-xs btn-info mb0"><?php echo __("Read less");?>
</button>
                                                    </td>
                                                </tr>
                                            <?php }?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            <?php }?>

                            <?php if (isAdmin($_smarty_tpl->tpl_vars['username']->value) || in_array($_smarty_tpl->tpl_vars['user']->value->_data['user_id'],$_smarty_tpl->tpl_vars['principals']->value)) {?>
                                
                                <div class="box-primary">
                                    <div class="box-header">
                                        <i class="fa fa-info-circle"></i>
                                        <strong><?php echo __("Interactive synthesis");?>
&#32;&#47;&#32;<?php echo __("month");?>
</strong>
                                        
                                    </div>
                                    <div class="list-group">
                                        <table class="table table-striped table-bordered table-hover">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th><?php echo __('Interactive section');?>
</th>
                                                <th><?php echo __('Created');?>
</th>
                                                <th><?php echo __('Number of interactions');?>
</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php $_smarty_tpl->_assignInScope('index', 1);
?>
                                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['insights']->value['statistics'], 'row', false, 'type');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['type']->value => $_smarty_tpl->tpl_vars['row']->value) {
?>
                                                <?php if ($_smarty_tpl->tpl_vars['type']->value == 'page' || $_smarty_tpl->tpl_vars['type']->value == 'post' || $_smarty_tpl->tpl_vars['type']->value == "attendance" || $_smarty_tpl->tpl_vars['type']->value == 'event' || $_smarty_tpl->tpl_vars['type']->value == 'schedule' || $_smarty_tpl->tpl_vars['type']->value == 'menu' || $_smarty_tpl->tpl_vars['type']->value == 'report' || $_smarty_tpl->tpl_vars['type']->value == 'tuition' || $_smarty_tpl->tpl_vars['type']->value == 'medicine' || $_smarty_tpl->tpl_vars['type']->value == 'pickup' || $_smarty_tpl->tpl_vars['type']->value == 'diary' || $_smarty_tpl->tpl_vars['type']->value == 'health') {?>
                                                    <tr>
                                                        <td align="center" class="align_middle"><?php echo $_smarty_tpl->tpl_vars['index']->value;?>
</td>
                                                        <td>
                                                            <?php if ($_smarty_tpl->tpl_vars['type']->value == 'page') {?>
                                                                <?php echo __("School page");?>

                                                            <?php } elseif ($_smarty_tpl->tpl_vars['type']->value == 'post') {?>
                                                                <?php echo __("Posts");?>

                                                            <?php } elseif ($_smarty_tpl->tpl_vars['type']->value == 'attendance') {?>
                                                                <?php echo __("Resign");?>

                                                            <?php } elseif ($_smarty_tpl->tpl_vars['type']->value == 'event') {?>
                                                                <?php echo __("Notification - Event");?>

                                                            <?php } elseif ($_smarty_tpl->tpl_vars['type']->value == 'schedule') {?>
                                                                <?php echo __("Schedule");?>

                                                            <?php } elseif ($_smarty_tpl->tpl_vars['type']->value == 'menu') {?>
                                                                <?php echo __("Menu");?>

                                                            <?php } elseif ($_smarty_tpl->tpl_vars['type']->value == 'report') {?>
                                                                <?php echo __("Contact book");?>

                                                            <?php } elseif ($_smarty_tpl->tpl_vars['type']->value == 'tuition') {?>
                                                                <?php echo __("Tuition");?>

                                                            <?php } elseif ($_smarty_tpl->tpl_vars['type']->value == 'medicine') {?>
                                                                <?php echo __("Medicines");?>

                                                            <?php } elseif ($_smarty_tpl->tpl_vars['type']->value == 'pickup') {?>
                                                                <?php echo __("Late pickup");?>

                                                            <?php } elseif ($_smarty_tpl->tpl_vars['type']->value == 'diary') {?>
                                                                <?php echo __("Diary corner");?>

                                                            <?php } elseif ($_smarty_tpl->tpl_vars['type']->value == 'health') {?>
                                                                <?php echo __("Health");?>

                                                            <?php }?>
                                                        </td>
                                                        <td align="center" class="align_middle">
                                                            <?php if ($_smarty_tpl->tpl_vars['type']->value == 'page') {?>
                                                                -
                                                            <?php } elseif ($_smarty_tpl->tpl_vars['type']->value == 'post') {?>
                                                                -
                                                            <?php } elseif ($_smarty_tpl->tpl_vars['type']->value == 'attendance') {?>
                                                                -
                                                            <?php } elseif ($_smarty_tpl->tpl_vars['type']->value == 'event') {?>
                                                                -
                                                            <?php } elseif ($_smarty_tpl->tpl_vars['type']->value == 'schedule') {?>
                                                                -
                                                            <?php } elseif ($_smarty_tpl->tpl_vars['type']->value == 'menu') {?>
                                                                -
                                                            <?php } elseif ($_smarty_tpl->tpl_vars['type']->value == 'report') {?>
                                                                -
                                                            <?php } elseif ($_smarty_tpl->tpl_vars['type']->value == 'tuition') {?>
                                                                -
                                                            <?php } elseif ($_smarty_tpl->tpl_vars['type']->value == 'medicine') {?>
                                                                <?php echo $_smarty_tpl->tpl_vars['row']->value['parent_createds'];?>

                                                            <?php } elseif ($_smarty_tpl->tpl_vars['type']->value == 'pickup') {?>
                                                                -
                                                            <?php } elseif ($_smarty_tpl->tpl_vars['type']->value == 'diary') {?>
                                                                <?php echo $_smarty_tpl->tpl_vars['row']->value['parent_createds'];?>

                                                            <?php } elseif ($_smarty_tpl->tpl_vars['type']->value == 'health') {?>
                                                                <?php echo $_smarty_tpl->tpl_vars['row']->value['parent_createds'];?>

                                                            <?php }?>
                                                        </td>
                                                        <td align="center" class="align_middle">
                                                            <?php if ($_smarty_tpl->tpl_vars['type']->value == 'page' || $_smarty_tpl->tpl_vars['type']->value == 'post') {?>
                                                                <?php if (isset($_smarty_tpl->tpl_vars['row']->value['school_views'])) {?>
                                                                    <?php echo $_smarty_tpl->tpl_vars['row']->value['school_views'];?>

                                                                <?php } else { ?>
                                                                    0
                                                                <?php }?>
                                                            <?php } else { ?>
                                                                <?php if (isset($_smarty_tpl->tpl_vars['row']->value['parent_views'])) {?>
                                                                    <?php echo $_smarty_tpl->tpl_vars['row']->value['parent_views'];?>

                                                                <?php } else { ?>
                                                                    0
                                                                <?php }?>
                                                            <?php }?>
                                                        </td>
                                                    </tr>
                                                <?php }?>
                                                <?php $_smarty_tpl->_assignInScope('index', $_smarty_tpl->tpl_vars['index']->value+1);
?>
                                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            <?php }?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <?php if (canView($_smarty_tpl->tpl_vars['username']->value,"review")) {?>
                                
                                <div class="box-primary">
                                    <div class="box-header">
                                        <i class="fa fa-star"></i>
                                        <strong><?php echo __("Parent review your child's school and teachers");?>
</strong>
                                    </div>
                                    <div class="list-group">
                                        <table class="table table-striped table-bordered table-hover">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th><?php echo __("Overall review");?>
</th>
                                                <th><?php echo __("Average review");?>
</th>
                                                <th><?php echo __("New review");?>
</th>
                                                <th><?php echo __("Actions");?>
</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td class="align_middle"><?php echo __("School");?>
</td>
                                                <td class="align_middle" align="center"><?php echo $_smarty_tpl->tpl_vars['insights']->value['reviews']['school']['total_review'];?>
</td>
                                                <td class="align_middle" align="center"><?php echo $_smarty_tpl->tpl_vars['insights']->value['reviews']['school']['average_review'];?>
</td>
                                                <td class="align_middle" align="center"><?php echo $_smarty_tpl->tpl_vars['insights']->value['reviews']['school']['cnt_new_review'];?>
</td>
                                                <td align="center" class="align_middle">
                                                    <a class="btn btn-xs btn-default" href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/reviews"><?php echo __("Detail");?>
</a>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td class="align_middle"><?php echo __("Teacher");?>
</td>
                                                <td class="align_middle" align="center"><?php echo $_smarty_tpl->tpl_vars['insights']->value['reviews']['teacher']['total_review'];?>
</td>
                                                <td class="align_middle" align="center"><?php echo $_smarty_tpl->tpl_vars['insights']->value['reviews']['teacher']['average_review'];?>
</td>
                                                <td class="align_middle" align="center"><?php echo $_smarty_tpl->tpl_vars['insights']->value['reviews']['teacher']['cnt_new_review'];?>
</td>
                                                <td align="center" class="align_middle">
                                                    <a class="btn btn-xs btn-default" href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/reviews/teachers"><?php echo __("Detail");?>
</a>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            <?php }?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <?php if (canView($_smarty_tpl->tpl_vars['username']->value,"services")) {?>
                                
                                <div class="box-primary">
                                    <div class="box-header">
                                        <i class="fa fa-beer"></i>
                                        <strong><?php echo __("Feed service summary");?>
</strong>
                                    </div>
                                    <?php $_smarty_tpl->_assignInScope('col', 3);
?>
                                    <div class="list-group">
                                        <table class="table table-striped table-bordered table-hover">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th><?php echo __("Class");?>
</th>
                                                <th><?php echo __("Present");?>
 +<br/><?php echo __("Without permission");?>
</th>
                                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['insights']->value['food_services']['service_list'], 'service');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['service']->value) {
?>
                                                    <th><?php echo $_smarty_tpl->tpl_vars['service']->value['service_name'];?>
</th>
                                                    <?php $_smarty_tpl->_assignInScope('col', $_smarty_tpl->tpl_vars['col']->value+1);
?>
                                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                                            <?php $_smarty_tpl->_assignInScope('presentTotal', 0);
?>
                                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['insights']->value['food_services']['classes'], 'class');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class']->value) {
?>
                                                <tr>
                                                    <td align="center"><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</td>
                                                    <td nowrap="true"><?php echo $_smarty_tpl->tpl_vars['class']->value['group_title'];?>
</td>
                                                    <td align="center"><?php echo $_smarty_tpl->tpl_vars['class']->value['present_total'];?>
</td>
                                                    <?php if (isset($_smarty_tpl->tpl_vars['class']->value['not_countbased_services']) && (count($_smarty_tpl->tpl_vars['class']->value['not_countbased_services']) > 0)) {?>
                                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['class']->value['not_countbased_services'], 'service');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['service']->value) {
?>
                                                            <td align="center"><?php echo $_smarty_tpl->tpl_vars['service']->value['total'];?>
</td>
                                                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                                    <?php } else { ?>
                                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['insights']->value['food_services']['service_list'], 'service');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['service']->value) {
?>
                                                            <?php if ($_smarty_tpl->tpl_vars['service']->value['type'] != @constant('SERVICE_TYPE_COUNT_BASED')) {?>
                                                                <td align="center">0</td>
                                                            <?php }?>
                                                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                                    <?php }?>
                                                    <?php if (isset($_smarty_tpl->tpl_vars['class']->value['countbased_services']) && (count($_smarty_tpl->tpl_vars['class']->value['countbased_services']) > 0)) {?>
                                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['class']->value['countbased_services'], 'service');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['service']->value) {
?>
                                                            <td align="center"><?php echo $_smarty_tpl->tpl_vars['service']->value['total'];?>
</td>
                                                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                                    <?php } else { ?>
                                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['insights']->value['food_services']['service_list'], 'service');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['service']->value) {
?>
                                                            <?php if ($_smarty_tpl->tpl_vars['service']->value['type'] == @constant('SERVICE_TYPE_COUNT_BASED')) {?>
                                                                <td align="center">0</td>
                                                            <?php }?>
                                                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                                    <?php }?>
                                                </tr>
                                                <?php $_smarty_tpl->_assignInScope('presentTotal', $_smarty_tpl->tpl_vars['presentTotal']->value+$_smarty_tpl->tpl_vars['class']->value['present_total']);
?>
                                                <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                            <?php if ($_smarty_tpl->tpl_vars['idx']->value > 2) {?>
                                                <tr>
                                                    <td colspan="2" align="center" style="vertical-align:middle"><strong><font color="red"><?php echo mb_strtoupper(__("Total"), 'UTF-8');?>
</font></strong></td>
                                                    <td align="center" style="vertical-align:middle"><strong><font color="red"><?php echo $_smarty_tpl->tpl_vars['presentTotal']->value;?>
</font></strong></td>
                                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['insights']->value['food_services']['service_total'], 'total');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['total']->value) {
?>
                                                        <td align="center" style="vertical-align:middle"><strong><font color="red"><?php echo $_smarty_tpl->tpl_vars['total']->value;?>
</font></strong></td>
                                                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                                </tr>
                                            <?php }?>

                                            <?php if ($_smarty_tpl->tpl_vars['idx']->value == 1) {?>
                                                <tr class="odd">
                                                    <td valign="top" align="center" colspan="<?php echo $_smarty_tpl->tpl_vars['col']->value;?>
" class="dataTables_empty">
                                                        <?php echo __("No data available in table");?>

                                                    </td>
                                                </tr>
                                            <?php }?>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            <?php }?>
                        </div>
                    </div>
                </div>
            <?php }?>
            <?php if (isAdmin($_smarty_tpl->tpl_vars['username']->value)) {?>
                <div class="mb10" align="center">
                    <a href="#" class="js_school-step-restart btn btn-warning btn-xs" data-step="0" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"><strong><?php echo __("Restart school");?>
</strong></a> <br/>
                    <?php echo __('Bấm vào "Khởi tạo lại trường" để thiết lập lại từ đầu (không bị mất những dữ liệu đã tạo trước đó)');?>

                </div>
            <?php }?>
        </div>
    <?php }?>
</div>

<?php }
}
