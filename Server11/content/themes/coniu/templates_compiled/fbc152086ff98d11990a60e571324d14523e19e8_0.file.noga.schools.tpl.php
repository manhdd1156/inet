<?php
/* Smarty version 3.1.31, created on 2021-03-29 11:17:43
  from "D:\Working\Coniu\ServerSvn\trunk\Server11\content\themes\coniu\templates\ci\noga\noga.schools.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_606154e7cd8375_89990548',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'fbc152086ff98d11990a60e571324d14523e19e8' => 
    array (
      0 => 'D:\\Working\\Coniu\\ServerSvn\\trunk\\Server11\\content\\themes\\coniu\\templates\\ci\\noga\\noga.schools.tpl',
      1 => 1575865657,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:ci/noga/ajax.noga.schoollist.tpl' => 1,
    'file:ci/noga/ajax.noga.managerlist.tpl' => 1,
    'file:ci/noga/ajax.noga.children.list.tpl' => 1,
  ),
),false)) {
function content_606154e7cd8375_89990548 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
            <div class="pull-right flip">
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/noga/schools/add" class="btn btn-default">
                    <i class="fa fa-plus"></i> <?php echo __("Add New");?>

                </a>
            </div>
        <?php }?>
        <i class="fa fa-university fa-fw fa-lg pr10"></i>
        <?php echo __("School");?>

        <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == "edit") {?>
            &rsaquo; <?php echo $_smarty_tpl->tpl_vars['data']->value['page_title'];?>

            <?php if ($_smarty_tpl->tpl_vars['configure']->value == 'basic') {?> &rsaquo; <?php echo __("Basic");?>

                <?php } elseif ($_smarty_tpl->tpl_vars['configure']->value == 'services') {?> &rsaquo; <?php echo __("Service");?>

                <?php } elseif ($_smarty_tpl->tpl_vars['configure']->value == 'addmission') {?> &rsaquo; <?php echo __("Admission");?>

                <?php } elseif ($_smarty_tpl->tpl_vars['configure']->value == 'info') {?> &rsaquo; <?php echo __("More information");?>

            <?php }?>
        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "addsystemschool") {?>
            &rsaquo; <?php echo __('Add new system school');?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "listsystemschool") {?>
            &rsaquo; <?php echo __('List system school');?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "editsystemschool") {?>
            &rsaquo; <?php echo __('Edit system school');?>
 &rsaquo; <?php echo $_smarty_tpl->tpl_vars['data']->value['school_group_title'];?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
            &rsaquo; <?php echo __('Create school');?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "viewuser") {?>
            &rsaquo; <?php echo __('View parent list of school');?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "module") {?>
            &rsaquo; <?php echo $_smarty_tpl->tpl_vars['schoolInfo']->value['page_title'];?>
 &rsaquo; <?php echo __('Module');?>

        <?php }?>
    </div>
    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
        <div class="panel-body with-table">
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-1 control-label text-left"><?php echo __("City");?>
</label>
                            <div class="col-sm-3">
                                <select class="form-control" name="city_id" id = "city_id">
                                    <option value="0"><?php echo __('Select city');?>
</option>
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['cities']->value, 'city');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['city']->value) {
?>
                                        <option value="<?php echo $_smarty_tpl->tpl_vars['city']->value['city_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['city']->value['city_name'];?>
</option>
                                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                </select>
                            </div>
                            <label class="col-sm-2 control-label text-left col-sm-offset-2"><?php echo __("District");?>
</label>
                            <div class="col-sm-4">
                                <select class="form-control" name="district_slug" id = "district_slug">
                                    <option value=""><?php echo __('Select district');?>
</option>








                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-6">
                                <input type="hidden" id="nogaRole" value="<?php echo $_smarty_tpl->tpl_vars['nogaRole']->value;?>
" name="nogaRole">
                                <select name="school_status" id="school_status" class="form-control">
                                    <option value="0"><?php echo __("Select satuts");?>
...</option>
                                    <option value="1"><?php echo __("Using Mascom - Edu");?>
</option>
                                    <option value="2"><?php echo __("Having Mascom - Edu's page");?>
</option>
                                    <option value="3"><?php echo __("Waiting confirmation");?>
</option>
                                </select>
                            </div>
                            <div class="col-sm-6">
                                <button class="btn btn-default" id="noga_search_school"><?php echo __('Search');?>
</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="school_list">
                <?php $_smarty_tpl->_subTemplateRender("file:ci/noga/ajax.noga.schoollist.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

            </div>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "addsystemschool") {?>
        <div class = "panel-body with-table">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/noga/bo_noga_school.php">
                <input type="hidden" name="do" value="addsystemschool"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">
                        <?php echo __("System school name");?>
 (*)
                    </label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="system_title" placeholder="<?php echo __("System school name");?>
" required autofocus maxlength="255">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">
                        <?php echo __("System school username");?>
 (*)
                    </label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="system_username" placeholder="<?php echo __("Username, e.g. mamnonthienthan");?>
" required maxlength="50">
                    </div>
                </div>
                
                    
                        
                    
                    
                        
                    
                
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Description");?>
</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="system_description" placeholder="<?php echo __("Write about your system school...");?>
" rows="4"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30"><?php echo __("Save");?>
</button>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "listsystemschool") {?>
        <div class="panel-body with-table">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover js_dataTable">
                    <thead>
                    <tr><th><?php echo __("System school list");?>
&nbsp;(<?php echo count($_smarty_tpl->tpl_vars['results']->value);?>
)</th></tr>
                    </thead>
                    <tbody>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['results']->value, 'row', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['row']->value) {
?>
                        <tr>
                            <td>
                                <div>
                                    <?php echo $_smarty_tpl->tpl_vars['k']->value+1;?>
 - <a href = "<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/noga/schools/editsystemschool/<?php echo $_smarty_tpl->tpl_vars['row']->value['school_group_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['row']->value['school_group_title'];?>
 </a>
                                    &nbsp;|&nbsp;<?php echo $_smarty_tpl->tpl_vars['row']->value['school_group_name'];?>

                                </div>
                                <div class="pull-right flip">
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/noga/schools/editsystemschool/<?php echo $_smarty_tpl->tpl_vars['row']->value['school_group_id'];?>
" class="btn btn-xs btn-default"><?php echo __("Edit");?>
</a>
                                </div>
                            </td>
                        </tr>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                    </tbody>
                </table>
            </div>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "editsystemschool") {?>
        <div class = "panel-body with-table">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/noga/bo_noga_school.php">
                <input type="hidden" name="do" value="editsystemschool"/>
                <input type="hidden" name = "system_school_id" value = "<?php echo $_smarty_tpl->tpl_vars['data']->value['school_group_id'];?>
"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">
                        <?php echo __("System school name");?>
 (*)
                    </label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="system_title" placeholder="<?php echo __("System school name");?>
" value = "<?php echo $_smarty_tpl->tpl_vars['data']->value['school_group_title'];?>
" required autofocus maxlength="255">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">
                        <?php echo __("System username");?>
 (*)
                    </label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="system_username" placeholder="<?php echo __("Username, e.g. mamnonthienthan");?>
" value = "<?php echo $_smarty_tpl->tpl_vars['data']->value['school_group_name'];?>
" required maxlength="50">
                    </div>
                </div>
                
                
                
                
                
                
                
                
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Description");?>
</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="system_description" placeholder="<?php echo __("Write about your system school...");?>
" rows="4"> <?php echo $_smarty_tpl->tpl_vars['data']->value['description'];?>
 </textarea>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30"><?php echo __("Save");?>
</button>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "edit") {?>
        <?php if ($_smarty_tpl->tpl_vars['configure']->value == 'basic') {?>
            <div class="panel-body">
                <form class="js_ajax-forms form-horizontal" data-url="ci/bo/noga/bo_noga_school.php">
                    <input type="hidden" name="do" value="basic"/>
                    <input type="hidden" name="page_id" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['page_id'];?>
"/>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">
                            <?php echo __("School name");?>
 (*)
                        </label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" name="title" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['page_title'];?>
" required autofocus maxlength="255">
                        </div>
                        <label class="col-sm-1 control-label text-left">
                            <?php echo __("Grade");?>

                        </label>
                        <div class="col-sm-4">
                            <select name="grade" id="grade" class="form-control">
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['grades']->value, 'grade', false, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['grade']->value) {
?>
                                    <option value="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
" <?php if ($_smarty_tpl->tpl_vars['key']->value == $_smarty_tpl->tpl_vars['dataCon']->value['grade']) {?> selected <?php }?>><?php echo $_smarty_tpl->tpl_vars['grade']->value;?>
</option>
                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left"><?php echo __("Username");?>
</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="username" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['page_name'];?>
" maxlength="50">
                        </div>
                        <div class="col-sm-6">
                            <select name="inside_system_school" id="inside_system_school" class="form-control">
                                <option value="0"><?php echo __("Select inside school");?>
</option>
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['systemSchools']->value, 'rows');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['rows']->value) {
?>
                                    <option value="<?php echo $_smarty_tpl->tpl_vars['rows']->value['school_group_id'];?>
" <?php if ($_smarty_tpl->tpl_vars['rows']->value['school_group_id'] == $_smarty_tpl->tpl_vars['dataCon']->value['school_group_id']) {?> selected <?php }?>><?php echo $_smarty_tpl->tpl_vars['rows']->value['school_group_title'];?>
 - <?php echo $_smarty_tpl->tpl_vars['rows']->value['school_group_name'];?>
</option>
                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left"><?php echo __("School manager");?>
</label>
                        <div class="col-sm-9">
                            <input name="search-manager" id="search-manager" type="text" class="form-control" placeholder="<?php echo __("Enter username, email or fullname to search");?>
" autocomplete="off">
                            <div id="search-manager-results" class="dropdown-menu dropdown-widget dropdown-search">
                                <div class="dropdown-widget-header">
                                    <?php echo __("Search Results");?>

                                </div>
                                <div class="dropdown-widget-body">
                                    <div class="loader loader_small ptb10"></div>
                                </div>
                            </div>
                            <br/>
                            <div class="col-sm-9" id="manager_list" name="manager_list">
                                <?php if (count($_smarty_tpl->tpl_vars['managers']->value) > 0) {?>
                                    <?php $_smarty_tpl->_subTemplateRender('file:ci/noga/ajax.noga.managerlist.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('results'=>$_smarty_tpl->tpl_vars['managers']->value), 0, false);
?>

                                <?php } else { ?>
                                    <?php echo __("No manager");?>

                                <?php }?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left"><?php echo __("School type");?>
 (*)</label>
                        <div class="col-sm-3">
                            <select name="school_type" id="school_type" class="form-control">
                                <option value="0"><?php echo __("Select school type");?>
</option>
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['schoolTypes']->value, 'type');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['type']->value) {
?>
                                    <option value="<?php echo $_smarty_tpl->tpl_vars['type']->value['type_value'];?>
" <?php if ($_smarty_tpl->tpl_vars['type']->value['type_value'] == $_smarty_tpl->tpl_vars['dataCon']->value['type']) {?> selected <?php }?>><?php echo $_smarty_tpl->tpl_vars['type']->value['type_name'];?>
</option>
                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                            </select>
                        </div>
                        <label class="col-sm-3 control-label text-left"><?php echo __("Email");?>
</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="email" name="email" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['email'];?>
" maxlength="50">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left"><?php echo __("Telephone");?>
</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="telephone" name="telephone" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['telephone'];?>
" maxlength="50">
                        </div>
                        <label class="col-sm-3 control-label text-left"><?php echo __("Website");?>
</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="website" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['website'];?>
" maxlength="50">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left"><?php echo __("Address");?>
</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="address" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['address'];?>
" maxlength="400">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left"><?php echo __("City");?>
</label>
                        <div class="col-sm-3">
                            <select class="form-control" name="city_id" id = "city_id">
                                <option value="0"><?php echo __("Select city");?>
</option>
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['cities']->value, 'city');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['city']->value) {
?>
                                    <option value="<?php echo $_smarty_tpl->tpl_vars['city']->value['city_id'];?>
" <?php if ($_smarty_tpl->tpl_vars['data']->value['city_id'] == $_smarty_tpl->tpl_vars['city']->value['city_id']) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['city']->value['city_name'];?>
</option>
                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                            </select>
                        </div>
                        <label class="col-sm-3 control-label text-left"><?php echo __("District");?>
</label>
                        <div class="col-sm-3">
                            <select class="form-control" name="district_slug" id = "district_slug">
                                <option value="<?php echo $_smarty_tpl->tpl_vars['dataCon']->value['district_slug'];?>
" selected><?php echo $_smarty_tpl->tpl_vars['dataCon']->value['district_name'];?>
</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left"> <?php echo __("Location");?>
: Long (Kinh độ)</label>
                        <div class="col-sm-3">
                            <input class="form-control" name="longtitude" value = <?php echo $_smarty_tpl->tpl_vars['dataCon']->value['lng'];?>
>
                        </div>
                        <label class="col-sm-3 control-label text-left"> Lat (Vĩ độ) </label>
                        <div class="col-sm-3">
                            <input class="form-control" name="latitude" value = "<?php echo $_smarty_tpl->tpl_vars['dataCon']->value['lat'];?>
">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left"><?php echo __("Directions");?>
</label>
                        <div class="col-sm-9">
                            <textarea class="form-control" name="direct" placeholder="<?php echo __("Write about direct go to your school...");?>
" rows="1"> <?php echo $_smarty_tpl->tpl_vars['dataCon']->value['directions'];?>
 </textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left"><?php echo __("Short description");?>
</label>
                        <div class="col-sm-9">
                            <textarea class="form-control" name="short_description" placeholder="<?php echo __("Write about your school...");?>
" rows="2"><?php echo $_smarty_tpl->tpl_vars['dataCon']->value['short_overview'];?>
</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left"><?php echo __("Description");?>
</label>
                        <div class="col-sm-9">
                            <textarea class="form-control" name="description" placeholder="<?php echo __("Write about the school...");?>
" rows="4"><?php echo $_smarty_tpl->tpl_vars['data']->value['page_description'];?>
</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-3">
                            <button type="submit" class="btn btn-primary padrl30"><?php echo __("Save");?>
</button>
                        </div>
                    </div>

                    <!-- success -->
                    <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                    <!-- success -->

                    <!-- error -->
                    <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                    <!-- error -->
                </form>
            </div>
        <?php } elseif ($_smarty_tpl->tpl_vars['configure']->value == 'services') {?>
            <div class="panel-body">
                <form class="js_ajax-forms form-horizontal" data-url="ci/bo/noga/bo_noga_school.php">
                    <input type="hidden" name="do" value="services"/>
                    <input type="hidden" name="page_id" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['page_id'];?>
"/>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left"> <?php echo __("Facilities");?>
</label>
                        <div class="col-sm-4">
                            <input type="checkbox" name = "facilities[0]" value="1" <?php if (($_smarty_tpl->tpl_vars['dataCon']->value['facility_pool'] == 1)) {?>checked<?php }?>><label class = "control-label"> <?php echo __('Pool');?>
</label><br/>
                            <input type="checkbox" name = "facilities[1]" value="1" <?php if (($_smarty_tpl->tpl_vars['dataCon']->value['facility_playground_out'] == 1)) {?>checked<?php }?>><label class = "control-label"> <?php echo __('Outdoor playground');?>
</label><br/>
                            <input type="checkbox" name = "facilities[2]" value="1" <?php if (($_smarty_tpl->tpl_vars['dataCon']->value['facility_playground_in'] == 1)) {?>checked<?php }?>><label class = "control-label"> <?php echo __('Indoor playground');?>
</label><br/>
                        </div>
                        <div class="col-sm-4">
                            <input type="checkbox" name = "facilities[3]" value="1" <?php if (($_smarty_tpl->tpl_vars['dataCon']->value['facility_library'] == 1)) {?>checked<?php }?>><label class = "control-label"> <?php echo __("Library");?>
</label><br/>
                            <input type="checkbox" name = "facilities[4]" value="1" <?php if (($_smarty_tpl->tpl_vars['dataCon']->value['facility_camera'] == 1)) {?>checked<?php }?>><label class = "control-label"> <?php echo __("View camera online");?>
</label><br/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left"><?php echo __("Facility note");?>
</label>
                        <div class="col-sm-9">
                            <textarea class="form-control" name="facility_note" rows="4"> <?php echo $_smarty_tpl->tpl_vars['dataCon']->value['note_for_facility'];?>
</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left"> <?php echo __("Service");?>
</label>
                        <div class="col-sm-4">
                            <input type="checkbox" name = "services[0]" value="1" <?php if (($_smarty_tpl->tpl_vars['dataCon']->value['service_bus'] == 1)) {?>checked<?php }?>><label class = "control-label"><?php echo __('Transportation');?>
</label> <br/>
                            <input type="checkbox" name = "services[1]" value="1" <?php if (($_smarty_tpl->tpl_vars['dataCon']->value['service_breakfast'] == 1)) {?>checked<?php }?>><label class = "control-label"><?php echo __('Breakfast');?>
</label> <br/>
                        </div>
                        <div class="col-sm-4">
                            <input type="checkbox" name = "services[2]" value="1" <?php if (($_smarty_tpl->tpl_vars['dataCon']->value['service_belated'] == 1)) {?>checked<?php }?>><label class = "control-label"><?php echo __('Late PickUp');?>
</label> <br/>
                            <input type="checkbox" name = "services[3]" value="1" <?php if (($_smarty_tpl->tpl_vars['dataCon']->value['service_saturday'] == 1)) {?>checked<?php }?>><label class = "control-label"><?php echo __("Looks saturday");?>
</label> <br/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left"><?php echo __("Service note");?>
</label>
                        <div class="col-sm-9 control-label">
                            <textarea class="form-control" name="service_note" rows="4"> <?php echo $_smarty_tpl->tpl_vars['dataCon']->value['note_for_service'];?>
</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-3">
                            <button type="submit" class="btn btn-primary padrl30"><?php echo __("Save");?>
</button>
                        </div>
                    </div>

                    <!-- success -->
                    <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                    <!-- success -->

                    <!-- error -->
                    <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                    <!-- error -->
                </form>
            </div>
            <?php } elseif ($_smarty_tpl->tpl_vars['configure']->value == 'addmission') {?>
            <div class = "panel-body">
                <form class="js_ajax-forms form-horizontal" data-url="ci/bo/noga/bo_noga_school.php">
                    <input type="hidden" name="do" value="addmission"/>
                    <input type="hidden" name="page_id" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['page_id'];?>
"/>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left"><?php echo __("Age");?>
</label>
                        <label class="col-sm-1 control-label text-left"><?php echo __("From");?>
</label>
                        <div class="col-sm-3 text-left">
                            <input type="text" name = "age_begin" id ="age_begin" value="<?php echo $_smarty_tpl->tpl_vars['dataCon']->value['start_age'];?>
" class = "form-control"/>
                        </div>
                        <label class="col-sm-1 control-label text-left"><?php echo __("To");?>
</label>
                        <div class="col-sm-3 text-left">
                            <input type="text" name = "age_end" id ="age_end" value="<?php echo $_smarty_tpl->tpl_vars['dataCon']->value['end_age'];?>
" class = "form-control"/>
                        </div>
                        <label class="col-sm-1 control-label text-left"><?php echo __("Month");?>
</label>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left"><?php echo __("Tuition fee");?>
</label>
                        <label class="col-sm-1 control-label text-left"><?php echo __("From");?>
</label>
                        <div class="col-sm-3 text-left">
                            <input type="text" name = "tuition_begin" id ="tuition_begin" value="<?php echo $_smarty_tpl->tpl_vars['dataCon']->value['start_tuition_fee'];?>
" class = "form-control"/>
                        </div>
                        <label class="col-sm-1 control-label text-left"><?php echo __("To");?>
</label>
                        <div class="col-sm-3 text-left">
                            <input type="text" name = "tuition_end" id ="tuition_end" value="<?php echo $_smarty_tpl->tpl_vars['dataCon']->value['end_tuition_fee'];?>
" class = "form-control"/>
                        </div>
                        <label class="col-sm-1 control-label text-left"><?php echo @constant('MONEY_UNIT');?>
</label>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left"><?php echo __("Tuition note");?>
</label>
                        <div class="col-sm-9">
                            <textarea class="form-control" name="tuition_note" rows="2"> <?php echo $_smarty_tpl->tpl_vars['dataCon']->value['note_for_tuition'];?>
</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left"><?php echo __("Number of student addmission");?>
</label>
                        <div class="col-sm-9">
                            <input type="text" name = "addmission" id ="addmission" value="<?php echo $_smarty_tpl->tpl_vars['dataCon']->value['admission'];?>
" class = "form-control"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left"><?php echo __("Addmission note");?>
</label>
                        <div class="col-sm-9">
                            <textarea class="form-control" name="addmission_note" rows="5"> <?php echo $_smarty_tpl->tpl_vars['dataCon']->value['note_for_admission'];?>
</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-3">
                            <button type="submit" class="btn btn-primary padrl30"><?php echo __("Save");?>
</button>
                        </div>
                    </div>
                    <!-- success -->
                    <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                    <!-- success -->

                    <!-- error -->
                    <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                    <!-- error -->
                </form>
            </div>
            <?php } elseif ($_smarty_tpl->tpl_vars['configure']->value == 'info') {?>
                <div class = "panel-body">
                    <form class="js_ajax-forms form-horizontal" data-url="ci/bo/noga/bo_noga_school.php">
                        <input type="hidden" name="do" value="info"/>
                        <input type="hidden" name="page_id" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['page_id'];?>
"/>
                        <div class="form-group">
                            <label class="col-sm-3 control-label text-left"><?php echo __("Leadership");?>
</label>
                            <div class="col-sm-9">
                                <textarea class="form-control" name="leader" rows="6"> <?php echo $_smarty_tpl->tpl_vars['dataCon']->value['info_leader'];?>
</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label text-left"><?php echo __("Methods of education");?>
</label>
                            <div class="col-sm-9">
                                <textarea class="form-control" name="method" rows="6"> <?php echo $_smarty_tpl->tpl_vars['dataCon']->value['info_method'];?>
</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label text-left"><?php echo __("Teachers");?>
</label>
                            <div class="col-sm-9">
                                <textarea class="form-control" name="teacher" rows="6"> <?php echo $_smarty_tpl->tpl_vars['dataCon']->value['info_teacher'];?>
</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label text-left"><?php echo __("Nutrition");?>
</label>
                            <div class="col-sm-9">
                                <textarea class="form-control" name="nutrition" rows="6"> <?php echo $_smarty_tpl->tpl_vars['dataCon']->value['info_nutrition'];?>
</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-9 col-sm-offset-3">
                                <button type="submit" class="btn btn-primary padrl30"><?php echo __("Save");?>
</button>
                            </div>
                        </div>
                        <!-- success -->
                        <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                        <!-- success -->

                        <!-- error -->
                        <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                        <!-- error -->
                    </form>
                </div>
        <?php }?>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
        <div class="panel-body">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/noga/bo_noga_school.php">
                <input type="hidden" name="do" value="add"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">
                        <?php echo __("School name");?>
 (*)
                    </label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="title" placeholder="<?php echo __("School name");?>
" required autofocus maxlength="255">
                    </div>
                    <label class="col-sm-1 control-label text-left">
                        <?php echo __("Grade");?>

                    </label>
                    <div class="col-sm-4">
                        <select name="grade" id="grade" class="form-control">
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['grades']->value, 'grade', false, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['grade']->value) {
?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
"> <?php echo __($_smarty_tpl->tpl_vars['grade']->value);?>
 </option>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Username");?>
</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="username" placeholder="<?php echo __("Username, e.g. mamnonthienthan");?>
" maxlength="50">
                    </div>
                    <div class="col-sm-6">
                        <select name="inside_system_school" id="inside_system_school" class="form-control">
                            <option value="0"> <?php echo __("Select system school");?>
 </option>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['systemSchools']->value, 'rows');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['rows']->value) {
?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['rows']->value['school_group_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['rows']->value['school_group_title'];?>
 - <?php echo $_smarty_tpl->tpl_vars['rows']->value['school_group_name'];?>
</option>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("School manager");?>
</label>
                    <div class="col-sm-9">
                        <input name="search-manager" id="search-manager" type="text" class="form-control" placeholder="<?php echo __("Enter username, email or fullname to search");?>
" autocomplete="off">
                        <div id="search-manager-results" class="dropdown-menu dropdown-widget dropdown-search">
                            <div class="dropdown-widget-header">
                                <?php echo __("Search Results");?>

                            </div>
                            <div class="dropdown-widget-body">
                                <div class="loader loader_small ptb10"></div>
                            </div>
                        </div>
                        <br/>
                        <div class="col-sm-9" id="manager_list" name="manager_list"></div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("School type");?>
 (*)</label>
                    <div class="col-sm-3">
                        <select name="school_type" id="school_type" class="form-control">
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['schoolTypes']->value, 'type');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['type']->value) {
?>
                                <option value = "<?php echo $_smarty_tpl->tpl_vars['type']->value['type_value'];?>
"> <?php echo $_smarty_tpl->tpl_vars['type']->value['type_name'];?>
 </option>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </select>
                    </div>
                    <label class="col-sm-3 control-label text-left"><?php echo __("Email");?>
</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="email" name="email" placeholder="<?php echo __("Email");?>
" maxlength="50">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Telephone");?>
</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="telephone" name="telephone" placeholder="<?php echo __("Telephone");?>
" maxlength="50">
                    </div>
                    <label class="col-sm-3 control-label text-left"><?php echo __("Website");?>
</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="website" placeholder="<?php echo __("Website");?>
" maxlength="50">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Address");?>
</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="address" placeholder="<?php echo __("Address");?>
" maxlength="400">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("City");?>
</label>
                    <div class="col-sm-3">
                        <select class="form-control" name="city_id" id = "city_id">
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['cities']->value, 'city');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['city']->value) {
?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['city']->value['city_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['city']->value['city_name'];?>
</option>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </select>
                    </div>
                    <label class="col-sm-3 control-label text-left"><?php echo __("District");?>
</label>
                    <div class="col-sm-3">
                        <select class="form-control" name="district_slug" id = "district_slug">
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['cities']->value, 'city');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['city']->value) {
?>
                                <?php if ($_smarty_tpl->tpl_vars['city']->value['city_id'] == 1) {?>
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['city']->value['district'], 'district');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['district']->value) {
?>
                                        <option value="<?php echo $_smarty_tpl->tpl_vars['district']->value['district_slug'];?>
"> <?php echo $_smarty_tpl->tpl_vars['district']->value['district_name'];?>

                                        </option>
                                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                <?php }?>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"> <?php echo __("Location");?>
: Long (Kinh độ)</label>
                    <div class="col-sm-3"><input class="form-control" name="longtitude"></div>
                    <label class="col-sm-3 control-label text-left"> Lat (Vĩ độ)</label>
                    <div class="col-sm-3"><input class="form-control" name="latitude"></div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Directions");?>
</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="direct" placeholder="<?php echo __("Write about direct go to your school...");?>
" rows="1"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Short description");?>
</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="short_description" placeholder="<?php echo __("Write about your school...");?>
" rows="2"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Description");?>
</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="description" placeholder="<?php echo __("Write about your school...");?>
" rows="4"></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30"><?php echo __("Save");?>
</button>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "viewuser") {?>
        <div class="panel-body with-table">
            <div class="js_ajax-forms form-horizontal">
                <input type="hidden" name="do" value="view_user"/>
                <input type="hidden" name="school_id" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['page_id'];?>
"/>
                <input type="hidden" name="page_id" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['page_id'];?>
" id = "page_id"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Scope");?>
</label>
                    <div class="col-sm-3">
                        <select name="class_id" id="class_id" class="form-control">
                            <option value=""><?php echo __("School");?>
</option>
                            <?php $_smarty_tpl->_assignInScope('class_level', -1);
?>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['classes']->value, 'class');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class']->value) {
?>
                                <?php if (($_smarty_tpl->tpl_vars['class_level']->value != $_smarty_tpl->tpl_vars['class']->value['class_level_id'])) {?>
                                    <option value="" disabled style="color: blue">-----<?php echo $_smarty_tpl->tpl_vars['class']->value['class_level_name'];?>
-----</option>
                                <?php }?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['class']->value['group_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['class']->value['group_title'];?>
</option>
                                <?php $_smarty_tpl->_assignInScope('class_level', $_smarty_tpl->tpl_vars['class']->value['class_level_id']);
?>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>


                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Last active");?>
</label>
                    <div class='col-sm-3'>
                        <div class='input-group date' id='fromDate'>
                            <input type='text' name="begin" class="form-control" id="begin" value="<?php echo $_smarty_tpl->tpl_vars['begin']->value;?>
"/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                    
                    <div class='col-md-3'>
                        <div class='input-group date' id='toDate'>
                            <input type='text' name="end" class="form-control" id="end"/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <button type="submit" class="btn btn-default js_child-search" data-username="<?php echo $_smarty_tpl->tpl_vars['data']->value['page_name'];?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['data']->value['page_id'];?>
" data-isnew="1" style="margin-left: 5px;"><?php echo __("Search");?>
</button>
                    </div>
                </div>

                <div class="table-responsive" id="child_list" name="child_list">
                    <?php $_smarty_tpl->_subTemplateRender("file:ci/noga/ajax.noga.children.list.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                </div>
                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>

            </div>
        </div>

    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "module") {?>
        <div class = "panel-body with-table">
            <h4><?php echo __("Select the school module allowed to use");?>
</h4>
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/noga/bo_noga_school.php">
                <input type="hidden" name="do" value="edit_module"/>
                <input type="hidden" name="school_id" value="<?php echo $_smarty_tpl->tpl_vars['school']->value['school_id'];?>
"/>
                <input type="hidden" name="grade" value="<?php echo $_smarty_tpl->tpl_vars['school']->value['grade'];?>
"/>
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['defaultModule']->value[$_smarty_tpl->tpl_vars['school']->value['grade']], 'value', false, 'module');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['module']->value => $_smarty_tpl->tpl_vars['value']->value) {
?>
                    <input type="checkbox" value="1" name="<?php echo $_smarty_tpl->tpl_vars['module']->value;?>
" <?php if ($_smarty_tpl->tpl_vars['school']->value[$_smarty_tpl->tpl_vars['module']->value]) {?>checked<?php }?>> <?php echo __($_smarty_tpl->tpl_vars['moduleName']->value[$_smarty_tpl->tpl_vars['module']->value]);?>
 <br/>
                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>


                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30"><?php echo __("Save");?>
</button>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    <?php }?>
</div><?php }
}
