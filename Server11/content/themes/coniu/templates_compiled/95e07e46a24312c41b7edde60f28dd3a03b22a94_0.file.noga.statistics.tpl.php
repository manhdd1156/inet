<?php
/* Smarty version 3.1.31, created on 2021-03-31 08:54:21
  from "D:\workplace\Server11\content\themes\coniu\templates\ci\noga\noga.statistics.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_6063d64d6d6ee8_28609127',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '95e07e46a24312c41b7edde60f28dd3a03b22a94' => 
    array (
      0 => 'D:\\workplace\\Server11\\content\\themes\\coniu\\templates\\ci\\noga\\noga.statistics.tpl',
      1 => 1552404703,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:ci/noga/ajax.noga.statistic.school.tpl' => 2,
    'file:ci/noga/ajax.noga.statistic.parent.tpl' => 2,
    'file:ci/noga/ajax.noga.statistic.useronline.tpl' => 1,
    'file:ci/noga/ajax.noga.statistic.topic.tpl' => 1,
  ),
),false)) {
function content_6063d64d6d6ee8_28609127 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <i class="fa fa-pie-chart fa-fw fa-lg pr10"></i>
        <?php echo __("Statistic");?>

        <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == "schools") {?>
            &rsaquo; <?php echo __('School');?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "users") {?>
            &rsaquo; <?php echo __('Users');?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "usersonline") {?>
            &rsaquo; <?php echo __('Users online');?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "topics") {?>
            &rsaquo; <?php echo __('Topic');?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "schooldetail") {?>
            &rsaquo; <?php echo __('School');?>
 &rsaquo; <?php echo $_smarty_tpl->tpl_vars['school']->value['page_title'];?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "reviewschool") {?>
            &rsaquo; <?php echo __('School');?>
 &rsaquo; <?php echo $_smarty_tpl->tpl_vars['school']->value['page_title'];?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "reviewteachers") {?>
            &rsaquo; <?php echo $_smarty_tpl->tpl_vars['school']->value['page_title'];?>
 &rsaquo;  <?php echo __('Teacher Reviews');?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "reviewteacher") {?>
            &rsaquo; <?php echo $_smarty_tpl->tpl_vars['school']->value['page_title'];?>

            &rsaquo; <?php echo __('Teacher Reviews');?>

            &rsaquo; <?php echo $_smarty_tpl->tpl_vars['rows']->value['user_fullname'];?>

        <?php }?>
    </div>
    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == "schools") {?>
        
            
        
        <div class="panel-body with-table">
            <div class="row">
                <div class="form-group">
                    <div class="col-sm-2"></div>
                    <div class='col-sm-3'>
                        <div class='input-group date' id='fromDate'>
                            <input type='text' name="begin" class="form-control" id="begin" value="<?php echo $_smarty_tpl->tpl_vars['begin']->value;?>
"/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                    <div class='col-sm-1' align="center"><i class="fas fa-long-arrow-alt-right fa-lg fa-fw pr10 pt10"></i></div>
                    <div class='col-md-3'>
                        <div class='input-group date' id='toDate'>
                            <input type='text' name="end" class="form-control" id="end"/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>

                    <div class='col-sm-3'>
                        <div class="form-group">
                            <button class="btn btn-default js_statistic-search"><?php echo __("Search");?>
</button>
                        </div>
                    </div>
                </div>
            </div>

            <br>
            <div align="center"><strong><?php echo __("TỔNG HỢP TƯƠNG TÁC CỦA TRƯỜNG THEO NGÀY");?>
</strong></div>
            <br>
            <div class="table-responsive">
                <?php $_smarty_tpl->_subTemplateRender("file:ci/noga/ajax.noga.statistic.school.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

            </div>
            <br/>
            <div align="center"><strong><?php echo __("TỔNG HỢP TƯƠNG TÁC CỦA PHỤ HUYNH VỚI TRƯỜNG THEO NGÀY");?>
</strong></div>
            <br>
            
            <div class="table-responsive">
                <?php $_smarty_tpl->_subTemplateRender("file:ci/noga/ajax.noga.statistic.parent.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

            </div>
            <br/>
            <div id="school_list">

                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover js_dataTable">
                        <thead>
                        <tr><th><?php echo __("School list");?>
&nbsp;(<?php echo count($_smarty_tpl->tpl_vars['rows']->value);?>
)</th></tr>
                        </thead>
                        <tbody>
                        <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['rows']->value, 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
                            <tr>
                                <td>
                                    <div>
                                        <?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
 - <?php echo $_smarty_tpl->tpl_vars['row']->value['page_title'];?>

                                        &nbsp;|&nbsp;<a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/pages/<?php echo $_smarty_tpl->tpl_vars['row']->value['page_name'];?>
"><?php echo __("Timeline");?>
</a>
                                        &nbsp;|&nbsp;<?php echo $_smarty_tpl->tpl_vars['row']->value['page_name'];?>

                                        <?php if (($_smarty_tpl->tpl_vars['row']->value['telephone'] != '')) {?> &nbsp;|&nbsp;<?php echo $_smarty_tpl->tpl_vars['row']->value['telephone'];?>
 <?php }?>
                                        <?php if (($_smarty_tpl->tpl_vars['row']->value['website'] != '')) {?> &nbsp;|&nbsp;<a href="<?php echo $_smarty_tpl->tpl_vars['row']->value['website'];?>
"><?php echo $_smarty_tpl->tpl_vars['row']->value['website'];?>
</a><?php }?>
                                        <?php if (($_smarty_tpl->tpl_vars['row']->value['email'] != '')) {?> &nbsp;|&nbsp;<a href="mailto:<?php echo $_smarty_tpl->tpl_vars['row']->value['email'];?>
"><?php echo $_smarty_tpl->tpl_vars['row']->value['email'];?>
</a> <?php }?>
                                    </div>
                                    <br>
                                    <div class="pull-left flip">
                                        - <?php echo __("Lượt tương tác");?>
 : <font color="red"><?php echo $_smarty_tpl->tpl_vars['row']->value['total_interactive'];?>
</font>
                                    </div>
                                    <div class="pull-right flip">
                                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/noga/statistics/schooldetail/<?php echo $_smarty_tpl->tpl_vars['row']->value['page_id'];?>
" class="btn btn-xs btn-default"><?php echo __("Detail");?>
</a>
                                    </div>
                                </td>
                            </tr>
                            <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </tbody>
                    </table>
                </div>
            </div>

        </div>

    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "schooldetail") {?>
        <div class="panel-body with-table">
            <div class="form-group">
                <div class="col-sm-2"></div>
                <div class='col-sm-3'>
                    <div class='input-group date' id='fromDate'>
                        <input type='text' name="begin" class="form-control" id="begin" value="<?php echo $_smarty_tpl->tpl_vars['begin']->value;?>
"/>
                        <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                    </div>
                </div>
                <div class='col-sm-1' align="center"><i class="fas fa-long-arrow-alt-right fa-lg fa-fw pr10 pt10"></i></div>
                <div class='col-md-3'>
                    <div class='input-group date' id='toDate'>
                        <input type='text' name="end" class="form-control" id="end"/>
                        <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                    </div>
                </div>

                <div class='col-sm-3'>
                    <div class="form-group">
                        <button class="btn btn-default js_statistic_school-search" data-id="<?php echo $_smarty_tpl->tpl_vars['school']->value['page_id'];?>
"><?php echo __("Search");?>
</button>
                    </div>
                </div>
            </div>
            <br>
            <div align="center"><strong><?php echo __("TỔNG HỢP TƯƠNG TÁC CỦA TRƯỜNG THEO NGÀY");?>
</strong></div>
            <br>
            <div class="table-responsive">
                <?php $_smarty_tpl->_subTemplateRender("file:ci/noga/ajax.noga.statistic.school.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

            </div>
        </div>

        <div class="panel-body with-table">
            <div align="center"><strong><?php echo __("TỔNG HỢP TƯƠNG TÁC CỦA PHỤ HUYNH VỚI TRƯỜNG THEO NGÀY");?>
</strong></div>
            <br>
            
            <div class="table-responsive">
                <?php $_smarty_tpl->_subTemplateRender("file:ci/noga/ajax.noga.statistic.parent.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

            </div>
            <br>
        </div>

        <div class="panel-body with-table">
            <div align="center"><strong><?php echo mb_strtoupper(__("Parent review your child's school and teachers"), 'UTF-8');?>
</strong></div>
            <br>

            <div class="form-group">

                <div class="box-primary">
                    <div class="list-group">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th><?php echo __("Overall review");?>
</th>
                                <th><?php echo __("Average review");?>
</th>
                                <th><?php echo __("New review");?>
</th>
                                <th><?php echo __("Actions");?>
</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td class="align_middle"><?php echo __("School");?>
</td>
                                <td class="align_middle" align="center"><?php echo $_smarty_tpl->tpl_vars['reviews']->value['school']['total_review'];?>
</td>
                                <td class="align_middle" align="center"><?php echo $_smarty_tpl->tpl_vars['reviews']->value['school']['average_review'];?>
</td>
                                <td class="align_middle" align="center"><?php echo $_smarty_tpl->tpl_vars['reviews']->value['school']['cnt_new_review'];?>
</td>
                                <td align="center" class="align_middle">
                                    <a class="btn btn-xs btn-default" href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/noga/statistics/reviewschool/<?php echo $_smarty_tpl->tpl_vars['school']->value['page_id'];?>
"><?php echo __("Detail");?>
</a>
                                </td>
                            </tr>

                            <tr>
                                <td class="align_middle"><?php echo __("Teacher");?>
</td>
                                <td class="align_middle" align="center"><?php echo $_smarty_tpl->tpl_vars['reviews']->value['teacher']['total_review'];?>
</td>
                                <td class="align_middle" align="center"><?php echo $_smarty_tpl->tpl_vars['reviews']->value['teacher']['average_review'];?>
</td>
                                <td class="align_middle" align="center"><?php echo $_smarty_tpl->tpl_vars['reviews']->value['teacher']['cnt_new_review'];?>
</td>
                                <td align="center" class="align_middle">
                                    <a class="btn btn-xs btn-default" href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/noga/statistics/reviewteachers/<?php echo $_smarty_tpl->tpl_vars['school']->value['page_id'];?>
"><?php echo __("Detail");?>
</a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
            <br>
            <br>

        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "usersonline") {?>
        <div class="panel-body with-table">
            <div class="form-group">
                <div class="col-sm-2"></div>
                <div class='col-sm-3'>
                    <div class='input-group date' id='fromDate'>
                        <input type='text' name="begin" class="form-control" id="begin" value="<?php echo $_smarty_tpl->tpl_vars['begin']->value;?>
"/>
                        <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                    </div>
                </div>
                <div class='col-sm-1' align="center"><i class="fas fa-long-arrow-alt-right fa-lg fa-fw pr10 pt10"></i></div>
                <div class='col-md-3'>
                    <div class='input-group date' id='toDate'>
                        <input type='text' name="end" class="form-control" id="end"/>
                        <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                    </div>
                </div>

                <div class='col-sm-3'>
                    <div class="form-group">
                        <button class="btn btn-default js_statistic_useronline-search"><?php echo __("Search");?>
</button>
                    </div>
                </div>
            </div>
            <br>
            <div align="center"><strong><?php echo __("TỔNG HỢP USER ONLINE THEO NGÀY");?>
</strong></div>
            <br>
            <div class="table-responsive" id="statistic_useronline">
                <?php $_smarty_tpl->_subTemplateRender("file:ci/noga/ajax.noga.statistic.useronline.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

            </div>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "topics") {?>

        <div class="panel-body with-table">
            <div align="center"><strong><?php echo __("TỔNG HỢP TƯƠNG TÁC THEO NGÀY");?>
</strong></div>
            <br>

            <div class="form-group">
                <div class="col-sm-2"></div>
                <div class='col-sm-3'>
                    <div class='input-group date' id='fromDate'>
                        <input type='text' name="begin" class="form-control" id="begin" value="<?php echo $_smarty_tpl->tpl_vars['begin']->value;?>
"/>
                        <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                    </div>
                </div>
                <div class='col-sm-1' align="center"><i class="fas fa-long-arrow-alt-right fa-lg fa-fw pr10 pt10"></i></div>
                <div class='col-md-3'>
                    <div class='input-group date' id='toDate'>
                        <input type='text' name="end" class="form-control" id="end"/>
                        <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                    </div>
                </div>

                <div class='col-sm-3'>
                    <div class="form-group">
                        <button class="btn btn-default js_statistic_topic-search" ><?php echo __("Search");?>
</button>
                    </div>
                </div>
            </div>
            <br>
            <br>

            <?php $_smarty_tpl->_subTemplateRender("file:ci/noga/ajax.noga.statistic.topic.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "reviewschool") {?>

        <div class="panel-body with-table">
            <?php if (isset($_smarty_tpl->tpl_vars['rows']->value['review_info'])) {?>
            <div class="pl10">
                <strong><?php echo __("Overall review");?>
&#58;&nbsp;<font color="red"><?php echo $_smarty_tpl->tpl_vars['rows']->value['review_info']['total_review'];?>
</font></strong>&emsp;
                <strong><?php echo __("Average review");?>
&#58;&nbsp;<font color="red"><?php echo $_smarty_tpl->tpl_vars['rows']->value['review_info']['average_review'];?>
</font></strong>&emsp;
                <strong><?php echo __("New review");?>
&#58;&nbsp;<font color="red"><?php echo $_smarty_tpl->tpl_vars['rows']->value['review_info']['cntNewReview'];?>
</font></strong>&emsp;
            </div>
            <?php } else { ?>
            <div class="pl10">
                <strong><?php echo __("Overall review");?>
&#58;&nbsp;<font color="red">0</font></strong>&emsp;
                <strong><?php echo __("Average review");?>
&#58;&nbsp;<font color="red">0</font></strong>&emsp;
                <strong><?php echo __("New review");?>
&#58;&nbsp;<font color="red">0</font></strong>&emsp;
            </div>
            <?php }?><br>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover js_dataTable">
                    <thead>
                    <tr>
                        <th width="10%">#</th>
                        <th width="20%"><?php echo __('Full name');?>
</th>
                        <th width="15%"><?php echo __('Rating');?>
</th>
                        <th width="40%"><?php echo __("Review content");?>
</th>
                        <th width="15%"><?php echo __("Time");?>
</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['rows']->value['review_detail'], 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
                        <tr>
                            <td align="center" class="align_middle"><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</td>
                            <td class="align_middle">
                                    <span class="name js_user-popover" data-uid="<?php echo $_smarty_tpl->tpl_vars['row']->value['user_id'];?>
">
                                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['row']->value['user_name'];?>
"><?php echo $_smarty_tpl->tpl_vars['row']->value['user_fullname'];?>
</a>
                                    </span>
                            </td>
                            <td align="center" class="align_middle"><?php echo $_smarty_tpl->tpl_vars['row']->value['rating'];?>
</td>
                            <td class="align_middle"><?php echo $_smarty_tpl->tpl_vars['row']->value['comment'];?>
</td>
                            <td align="center" class="align_middle"><?php echo toSysDate($_smarty_tpl->tpl_vars['row']->value['created_at']);?>
</td>
                        </tr>
                        <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>


                    </tbody>
                </table>
            </div>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "reviewteachers") {?>

        <div class="panel-body with-table">
            <?php if (isset($_smarty_tpl->tpl_vars['rows']->value['review_info'])) {?>
            <div class="pl10">
                <strong><?php echo __("Overall review");?>
&#58;&nbsp;<font color="red"><?php echo $_smarty_tpl->tpl_vars['rows']->value['review_info']['total_review'];?>
</font></strong>&emsp;
                <strong><?php echo __("Average review");?>
&#58;&nbsp;<font color="red"><?php echo $_smarty_tpl->tpl_vars['rows']->value['review_info']['average_review'];?>
</font></strong>&emsp;
                <strong><?php echo __("New review");?>
&#58;&nbsp;<font color="red"><?php echo $_smarty_tpl->tpl_vars['rows']->value['review_info']['cntNewReview'];?>
</font></strong>&emsp;
            </div>
            <?php } else { ?>
            <div class="pl10">
                <strong><?php echo __("Overall review");?>
&#58;&nbsp;<font color="red">0</font></strong>&emsp;
                <strong><?php echo __("Average review");?>
&#58;&nbsp;<font color="red">0</font></strong>&emsp;
                <strong><?php echo __("New review");?>
&#58;&nbsp;<font color="red">0</font></strong>&emsp;
            </div>
            <?php }?><br>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover js_dataTable">
                    <thead>
                    <tr>
                        <th width="7%">#</th>
                        <th width="25%"><?php echo __('Teacher');?>
</th>
                        <th width="20%"><?php echo __('Class');?>
</th>
                        <th width="20%"><?php echo __("Overall review");?>
</th>
                        <th width="15%"><?php echo __("Average");?>
</th>
                        <th width="13%"><?php echo __("Actions");?>
</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['rows']->value['review_detail'], 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
                        <tr>
                            <td align="center" class="align_middle"><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</td>
                            <td class="align_middle">
                                    <span class="name js_user-popover" data-uid="<?php echo $_smarty_tpl->tpl_vars['row']->value['user_id'];?>
">
                                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['row']->value['user_name'];?>
"><?php echo $_smarty_tpl->tpl_vars['row']->value['user_fullname'];?>
</a>
                                    </span>
                            </td>
                            <td class="align_middle"><?php echo $_smarty_tpl->tpl_vars['row']->value['group_title'];?>
</td>
                            <td align="center" class="align_middle"><?php echo $_smarty_tpl->tpl_vars['row']->value['total_review'];?>
</td>
                            <td  align="center" class="align_middle"><?php echo $_smarty_tpl->tpl_vars['row']->value['average_review'];?>
</td>
                            <td align="center" class="align_middle">
                                <a class="btn btn-xs btn-default" href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/noga/statistics/reviewteacher/<?php echo $_smarty_tpl->tpl_vars['row']->value['group_id'];?>
/<?php echo $_smarty_tpl->tpl_vars['row']->value['user_id'];?>
"><?php echo __("Detail");?>
</a>
                            </td>
                        </tr>
                        <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>


                    </tbody>
                </table>
            </div>
        </div>

    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "reviewteacher") {?>

        <div class="panel-body with-table">

            <div class="pl10">
                <strong><?php echo __("Teacher");?>
 &#58;&nbsp; </strong> <?php echo $_smarty_tpl->tpl_vars['rows']->value['teacher']['user_fullname'];?>
 <br>
                <strong><?php echo __("Class");?>
&#58;&nbsp;</strong> <?php echo $_smarty_tpl->tpl_vars['rows']->value['class']['group_title'];?>
 <br>
                <strong><?php echo __("Overall review");?>
&#58;&nbsp;</strong> <?php echo count($_smarty_tpl->tpl_vars['rows']->value['reviews']);?>

            </div>
            <br>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover js_dataTable">
                    <thead>
                    <tr>
                        <th width="7%">#</th>
                        <th width="25%"><?php echo __('Reviewer');?>
</th>
                        <th width="15%"><?php echo __('Rating');?>
</th>
                        <th width="40%"><?php echo __("Review content");?>
</th>
                        <th width="13%"><?php echo __("Time");?>
</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['rows']->value['reviews'], 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
                        <tr>
                            <td align="center" class="align_middle"><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</td>
                            <td class="align_middle">
                                <span class="name js_user-popover" data-uid="<?php echo $_smarty_tpl->tpl_vars['row']->value['user_id'];?>
">
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['row']->value['user_name'];?>
"><?php echo $_smarty_tpl->tpl_vars['row']->value['user_fullname'];?>
</a>
                                </span>
                            </td>
                            <td align="center" class="align_middle"><?php echo $_smarty_tpl->tpl_vars['row']->value['rating'];?>
</td>
                            <td class="align_middle">
                                <?php echo $_smarty_tpl->tpl_vars['row']->value['comment'];?>

                            </td>
                            <td align="center" class="align_middle"><?php echo $_smarty_tpl->tpl_vars['row']->value['time'];?>
</td>
                        </tr>
                        <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>


                    </tbody>
                </table>
            </div>

        </div>

    <?php }?>
</div><?php }
}
