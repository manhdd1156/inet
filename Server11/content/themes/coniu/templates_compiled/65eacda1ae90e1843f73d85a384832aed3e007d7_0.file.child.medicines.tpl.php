<?php
/* Smarty version 3.1.31, created on 2021-04-19 11:18:42
  from "D:\workplace\Server11\content\themes\coniu\templates\ci\child\child.medicines.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_607d04a24a8557_98867132',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '65eacda1ae90e1843f73d85a384832aed3e007d7' => 
    array (
      0 => 'D:\\workplace\\Server11\\content\\themes\\coniu\\templates\\ci\\child\\child.medicines.tpl',
      1 => 1552404702,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_607d04a24a8557_98867132 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <div class="pull-right flip">
            <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
                <a href="https://blog.coniu.vn/huong-dan-phu-huynh-thao-tac-gui-thuoc-tren-website-coniu/" class="btn btn-info btn_guide" target="_blank">
                    <i class="fa fa-info"></i> <?php echo __("Guide");?>

                </a>
                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/child/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
/medicines/add" class="btn btn-default">
                    <i class="fa fa-plus"></i> <?php echo __("Add New Medicine");?>

                </a>
            <?php }?>
        </div>
        <i class="fa fa-medkit fa-lg fa-fw pr10"></i>
        <?php echo __("Medicines");?>

        <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == "edit") {?>
            &rsaquo; <?php echo $_smarty_tpl->tpl_vars['data']->value['medicine_list'];?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
            &rsaquo; <?php echo __('Add New');?>

        <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "detail") {?>
            &rsaquo; <?php echo $_smarty_tpl->tpl_vars['data']->value['medicine_list'];?>

        <?php }?>
    </div>
    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
        <div class="panel-body with-table">
            <div class="table-responsive">
                <div><strong><?php echo __("Medicine list");?>
&nbsp;(<?php echo count($_smarty_tpl->tpl_vars['rows']->value);?>
)</strong></div>
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th><?php echo __("Medicine list");?>
</th>
                        <th><?php echo __("Times/day");?>
</th>
                        <th><?php echo __("Time");?>
</th>
                        <th><?php echo __("Actions");?>
</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['rows']->value, 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
                        <?php if ($_smarty_tpl->tpl_vars['idx']->value > 1) {?>
                            <tr><td colspan="5"></td></tr>
                        <?php }?>
                        <tr>
                            <?php $_smarty_tpl->_assignInScope('rowspan', 1);
?>
                            <?php if ($_smarty_tpl->tpl_vars['row']->value['guide'] != '') {
$_smarty_tpl->_assignInScope('rowspan', $_smarty_tpl->tpl_vars['rowspan']->value+1);
}?>
                            <td align="center" rowspan="<?php echo $_smarty_tpl->tpl_vars['rowspan']->value;?>
" style="vertical-align:middle"><strong><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</strong></td>
                            <td style="vertical-align:middle">
                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/child/<?php echo $_smarty_tpl->tpl_vars['row']->value['child_id'];?>
/medicines/detail/<?php echo $_smarty_tpl->tpl_vars['row']->value['medicine_id'];?>
"><?php echo nl2br($_smarty_tpl->tpl_vars['row']->value['medicine_list']);?>
</a>
                                <?php if ($_smarty_tpl->tpl_vars['row']->value['status'] == @constant('MEDICINE_STATUS_NEW')) {?>
                                    <img src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/content/themes/<?php echo $_smarty_tpl->tpl_vars['system']->value['theme'];?>
/images/new.gif"/>
                                <?php } elseif ($_smarty_tpl->tpl_vars['row']->value['status'] == @constant('MEDICINE_STATUS_CONFIRMED')) {?>
                                    <i class="fa fa-check"></i>
                                <?php } else { ?>
                                    <i class="fa fa-trash-alt"></i>
                                <?php }?>
                                <?php if ($_smarty_tpl->tpl_vars['row']->value['source_file'] != null) {?><br/><br/><a href = "<?php echo $_smarty_tpl->tpl_vars['row']->value['source_file'];?>
" target="_blank"><?php echo mb_strtoupper(__("Doctor prescription"), 'UTF-8');?>
</a><?php }?>
                            </td>
                            <td align="center" style="vertical-align:middle"><?php if ($_smarty_tpl->tpl_vars['school']->value['config']['school_allow_medicate']) {
echo $_smarty_tpl->tpl_vars['row']->value['count'];?>
/<?php }
echo $_smarty_tpl->tpl_vars['row']->value['time_per_day'];?>
</td>
                            <td align="center" style="vertical-align:middle">
                                <?php echo $_smarty_tpl->tpl_vars['row']->value['begin'];?>
<br/>
                                <font color="#dc143c"><?php echo $_smarty_tpl->tpl_vars['row']->value['end'];?>
</font>
                            </td>
                            <td align="center" nowrap="true">
                                <?php if ($_smarty_tpl->tpl_vars['row']->value['editable'] == 1) {?>
                                    <?php if ($_smarty_tpl->tpl_vars['row']->value['status'] != @constant('MEDICINE_STATUS_CANCEL')) {?>
                                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/child/<?php echo $_smarty_tpl->tpl_vars['row']->value['child_id'];?>
/medicines/edit/<?php echo $_smarty_tpl->tpl_vars['row']->value['medicine_id'];?>
" class="btn btn-xs btn-default">
                                            <?php echo __("Edit");?>

                                        </a>
                                    <?php }?>
                                    <button class="btn btn-xs btn-danger js_child-medicine-delete" data-child="<?php echo $_smarty_tpl->tpl_vars['row']->value['child_id'];?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['medicine_id'];?>
">
                                        <?php echo __("Delete");?>

                                    </button>
                                <?php } else { ?>
                                    <?php if ($_smarty_tpl->tpl_vars['row']->value['status'] != @constant('MEDICINE_STATUS_CANCEL')) {?>
                                        <button class="btn btn-xs btn-warning js_child-medicine-cancel" data-handle="cancel" data-child="<?php echo $_smarty_tpl->tpl_vars['row']->value['child_id'];?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['medicine_id'];?>
">
                                            <?php echo __("Cancel");?>

                                        </button>
                                    <?php }?>
                                <?php }?>
                            </td>
                        </tr>
                        <?php if ($_smarty_tpl->tpl_vars['row']->value['guide'] != '') {?>
                            <tr>
                                <td align="center" style="vertical-align:middle"><strong><?php echo __("Guide");?>
</strong></td>
                                <td colspan="3"><?php echo nl2br($_smarty_tpl->tpl_vars['row']->value['guide']);?>
</td>
                            </tr>
                        <?php }?>
                        <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                    </tbody>
                </table>
            </div>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "edit") {?>
        <div class="panel-body">
            <form class="form-horizontal" enctype="multipart/form-data" method="post" id="create_medicine">
                <input type="hidden" name="child_id" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
"/>
                <input type="hidden" name="medicine_id" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['medicine_id'];?>
"/>
                <input type="hidden" name="do" value="edit"/>

                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Medicine list");?>
 (*)</label>
                    <div class="col-sm-9">
                        <textarea class="form-control js_autosize" name="medicine_list" required maxlength="400"><?php echo $_smarty_tpl->tpl_vars['data']->value['medicine_list'];?>
</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Usage guide");?>
 (*)</label>
                    <div class="col-sm-9">
                        <textarea class="form-control js_autosize" name="guide" rows="6" required><?php echo $_smarty_tpl->tpl_vars['data']->value['guide'];?>
</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Time per day");?>
 (*)</label>
                    <div class="col-sm-3">
                        <input class="form-control" type="number" name="time_per_day" min="1" step="1" maxlength="2" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['time_per_day'];?>
" required>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Time");?>
 (*)</label>
                    <div class='col-sm-3'>
                        <div class='input-group date' id='medicine_beginpicker'>
                            <input type='text' name="begin" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['begin'];?>
" class="form-control" placeholder="<?php echo __("Begin");?>
 (*)" required/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                    <div class='col-sm-1'><i class="fas fa-long-arrow-alt-right fa-lg fa-fw pl10 pt10"></i></div>
                    <div class='col-md-3'>
                        <div class='input-group date' id='medicine_endpicker_new'>
                            <input type='text' name="end" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['end'];?>
" class="form-control" placeholder="<?php echo __("End");?>
 (*)" required/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                </div>
                
                    
                    
                        
                            
                            
                            
                            
                        
                        
                    
                
                <input type = "hidden" id = "is_file" name = "is_file" value = "1">
                <div class="form-group" id = "file_old">
                    <label class="rp-file col-sm-3 control-label text-left" style="padding-top: 0px"><?php echo __("Doctor prescription");?>
</label>
                    <div class="col-sm-6">
                        <?php if (!is_empty($_smarty_tpl->tpl_vars['data']->value['source_file'])) {?>
                            <a href="<?php echo $_smarty_tpl->tpl_vars['data']->value['source_file'];?>
" target="_blank"><img src = "<?php echo $_smarty_tpl->tpl_vars['data']->value['source_file'];?>
" class = "img-responsive"></a>
                        <?php } else { ?> <?php echo __('No file attachment');?>

                        <?php }?>
                    </div>
                </div>
                <div class = "form-group">
                    <label class="control-label col-sm-3">
                        <?php if (is_empty($_smarty_tpl->tpl_vars['data']->value['source_file'])) {?>
                            <?php echo __("Choose file");?>

                        <?php } else { ?>
                            <?php echo __("Choose file replace");?>

                        <?php }?>
                    </label>
                    <div class = "col-sm-6">
                        <input type="file" name="file" id="file"/>
                    </div>
                    <a class = "delete_image btn btn-danger btn-xs text-left"><?php echo __('Delete');?>
</a>
                </div>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30"><?php echo __("Save");?>
</button>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "add") {?>
        <div class="panel-body">
            <form class="form-horizontal" enctype="multipart/form-data" method="post" id="create_medicine">
                <input type="hidden" name="child_id" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
"/>
                <input type="hidden" name="do" value="add"/>

                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Medicine list");?>
 (*)</label>
                    <div class="col-sm-9">
                        <textarea class="form-control js_autosize" name="medicine_list" required maxlength="400"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Usage guide");?>
 (*)</label>
                    <div class="col-sm-9">
                        <textarea class="form-control js_autosize" name="guide" rows="6" required></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Time per day");?>
 (*)</label>
                    <div class="col-sm-3">
                        <input class="form-control" type="number" name="time_per_day" min="1" step="1" maxlength="2" value="2" required>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("Time");?>
 (*)</label>
                    <div class='col-sm-3'>
                        <div class='input-group date' id='medicine_beginpicker_new'>
                            <input type='text' name="begin" class="form-control" placeholder="<?php echo __("Begin");?>
 (*)" required/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                    <div class='col-sm-1'><i class="fas fa-long-arrow-alt-right fa-lg fa-fw pl10 pt10"></i></div>
                    <div class='col-sm-3'>
                        <div class='input-group date' id='medicine_endpicker_new'>
                            <input type='text' name="end" class="form-control" placeholder="<?php echo __("End");?>
 (*)" required/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"><?php echo __("File attachment");?>
</label>
                    <div class="col-sm-6">
                        <input type="file" name="file" id="file"/>
                    </div>
                </div>
                
                    
                
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30"><?php echo __("Save");?>
</button>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "detail") {?>
        <div class="panel-body with-table">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <tbody>
                    <tr>
                        <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Medicine list");?>
</strong></td>
                        <td>
                            <?php echo nl2br($_smarty_tpl->tpl_vars['data']->value['medicine_list']);?>

                            <?php if ($_smarty_tpl->tpl_vars['data']->value['status'] == @constant('MEDICINE_STATUS_NEW')) {?>
                                <img src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/content/themes/<?php echo $_smarty_tpl->tpl_vars['system']->value['theme'];?>
/images/new.gif"/>
                            <?php } elseif ($_smarty_tpl->tpl_vars['data']->value['status'] == @constant('MEDICINE_STATUS_CONFIRMED')) {?>
                                <i class="fa fa-check"></i>
                            <?php } else { ?>
                                <i class="fa fa-trash-alt"></i>
                            <?php }?>
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Usage guide");?>
</strong></td>
                        <td><?php echo nl2br($_smarty_tpl->tpl_vars['data']->value['guide']);?>
</td>
                    </tr>
                    <tr>
                        <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Time per day");?>
</strong></td>
                        <td><?php echo $_smarty_tpl->tpl_vars['data']->value['time_per_day'];?>
</td>
                    </tr>
                    <tr>
                        <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Time");?>
</strong></td>
                        <td><?php echo $_smarty_tpl->tpl_vars['data']->value['begin'];?>
 - <?php echo $_smarty_tpl->tpl_vars['data']->value['end'];?>
</td>
                    </tr>
                    <tr>
                        <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo mb_strtoupper(__("Doctor prescription"), 'UTF-8');?>
</strong></td>
                        <td>
                            <?php if (!is_empty($_smarty_tpl->tpl_vars['data']->value['source_file'])) {?>
                                <a href = "<?php echo $_smarty_tpl->tpl_vars['data']->value['source_file'];?>
" target="_blank"><strong>
                                        <?php echo __("File attachment");?>

                                    </strong>
                                </a>
                            <?php } else { ?>
                                <?php echo __("No file attachment");?>

                            <?php }?>
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Creator");?>
</strong></td>
                        <td><a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['data']->value['user_name'];?>
" target="_blank"><?php echo $_smarty_tpl->tpl_vars['data']->value['user_fullname'];?>
</a></td>
                    </tr>
                    <tr>
                        <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("User confirmed");?>
</strong></td>
                        <td><a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['data']->value['confirm_username'];?>
" target="_blank"><?php echo $_smarty_tpl->tpl_vars['data']->value['confirm_user'];?>
</a></td>
                    </tr>
                    <tr>
                        <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Created time");?>
</strong></td>
                        <td><?php echo $_smarty_tpl->tpl_vars['data']->value['created_at'];?>
</td>
                    </tr>
                    <?php if ($_smarty_tpl->tpl_vars['data']->value['updated_at'] != '') {?>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Last updated");?>
</strong></td>
                            <td><?php echo $_smarty_tpl->tpl_vars['data']->value['updated_at'];?>
</td>
                        </tr>
                    <?php }?>
                    <?php if (count($_smarty_tpl->tpl_vars['data']->value['detail']) > 0 && $_smarty_tpl->tpl_vars['school']->value['config']['school_allow_medicate']) {?>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Medicated");?>
</strong></td>
                            <td>
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th><?php echo __('Usage date');?>
</th>
                                        <th><?php echo __('Creator');?>
</th>
                                        <th><?php echo __('Created time');?>
</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['data']->value['detail'], 'detail');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['detail']->value) {
?>
                                        <tr>
                                            <td align="center"><?php echo $_smarty_tpl->tpl_vars['detail']->value['time_on_day'];?>
</td>
                                            <td><?php echo $_smarty_tpl->tpl_vars['detail']->value['usage_date'];?>
</td>
                                            <td>
                                                <?php echo $_smarty_tpl->tpl_vars['detail']->value['user_fullname'];?>

                                                <?php if ($_smarty_tpl->tpl_vars['detail']->value['created_user_id'] != $_smarty_tpl->tpl_vars['user']->value->_data['user_id']) {?>
                                                    <a href="#" class="far fa-comments fa-lg js_chat-start" data-name="<?php echo $_smarty_tpl->tpl_vars['detail']->value['user_fullname'];?>
" data-uid="<?php echo $_smarty_tpl->tpl_vars['detail']->value['created_user_id'];?>
"></a>
                                                <?php }?>
                                            </td>
                                            <td><?php echo $_smarty_tpl->tpl_vars['detail']->value['created_at'];?>
</td>
                                        </tr>
                                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    <?php }?>
                    </tbody>
                </table>
            </div>
            <div class="form-group pl5">
                <div class="col-sm-12">
                    <a class="btn btn-default" href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/child/<?php echo $_smarty_tpl->tpl_vars['data']->value['child_id'];?>
/medicines"><?php echo __("Lists");?>
</a>
                    <?php if (count($_smarty_tpl->tpl_vars['data']->value['detail']) == 0) {?>
                        <?php if ($_smarty_tpl->tpl_vars['data']->value['status'] != @constant('MEDICINE_STATUS_CANCEL')) {?>
                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/child/<?php echo $_smarty_tpl->tpl_vars['data']->value['child_id'];?>
/medicines/edit/<?php echo $_smarty_tpl->tpl_vars['data']->value['medicine_id'];?>
" class="btn btn-default">
                                <?php echo __("Edit");?>

                            </a>
                        <?php }?>
                        <button class="btn btn-danger js_child-medicine-delete" data-child="<?php echo $_smarty_tpl->tpl_vars['data']->value['child_id'];?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['data']->value['medicine_id'];?>
">
                            <?php echo __("Delete");?>

                        </button>
                    <?php } else { ?>
                        <?php if ($_smarty_tpl->tpl_vars['data']->value['status'] != @constant('MEDICINE_STATUS_CANCEL')) {?>
                            <button class="btn btn-warning js_child-medicine-cancel" data-handle="cancel" data-child="<?php echo $_smarty_tpl->tpl_vars['data']->value['child_id'];?>
" data-child="<?php echo $_smarty_tpl->tpl_vars['data']->value['child_id'];?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['data']->value['medicine_id'];?>
">
                                <?php echo __("Cancel");?>

                            </button>
                        <?php }?>
                    <?php }?>
                </div>
            </div>
        </div>
    <?php }?>
</div><?php }
}
