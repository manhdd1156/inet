<?php
/* Smarty version 3.1.31, created on 2021-04-23 14:45:34
  from "D:\workplace\mascom-edu-server\Server11\content\themes\coniu\templates\ci\school\ajax.searchteacher.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_60827b1e064659_51649550',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'f701377d9d99ddef76a856085fad8b65a91d4590' => 
    array (
      0 => 'D:\\workplace\\mascom-edu-server\\Server11\\content\\themes\\coniu\\templates\\ci\\school\\ajax.searchteacher.tpl',
      1 => 1552404702,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60827b1e064659_51649550 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="js_scroller">
    <ul>
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['results']->value, '_user');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['_user']->value) {
?>
            <li class="feeds-item" data-id="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_name'];?>
">
                <div class="data-container <?php if ($_smarty_tpl->tpl_vars['_small']->value) {?>small<?php }?>">
                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_name'];?>
">
                        <img class="data-avatar" src="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_picture'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_fullname'];?>
">
                    </a>
                    <div class="data-content">
                        <div class="pull-right flip">
                            <div class="btn btn-default js_teacher-select" data-uid="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_id'];?>
" data-ufullname="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_fullname'];?>
"><?php echo __("Add");?>
</div>
                        </div>
                        <div>
                            <span class="name js_user-popover" data-uid="<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_id'];?>
">
                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['_user']->value['user_name'];?>
"><?php echo $_smarty_tpl->tpl_vars['_user']->value['user_fullname'];?>
</a>
                            </span>
                        </div>
                    </div>
                </div>
            </li>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

    </ul>
</div><?php }
}
