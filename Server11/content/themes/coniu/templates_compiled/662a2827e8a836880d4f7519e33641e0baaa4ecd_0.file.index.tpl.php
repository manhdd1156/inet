<?php
/* Smarty version 3.1.31, created on 2021-03-29 10:31:54
  from "D:\Working\Coniu\ServerSvn\trunk\Server11\content\themes\coniu\templates\index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_60614a2a883245_41691353',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '662a2827e8a836880d4f7519e33641e0baaa4ecd' => 
    array (
      0 => 'D:\\Working\\Coniu\\ServerSvn\\trunk\\Server11\\content\\themes\\coniu\\templates\\index.tpl',
      1 => 1573458087,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:homepage.tpl' => 1,
    'file:_head.tpl' => 1,
    'file:_header.tpl' => 1,
    'file:_sidebar.tpl' => 1,
    'file:_announcements.tpl' => 1,
    'file:_publisher.tpl' => 1,
    'file:_posts.tpl' => 2,
    'file:_contact.tpl' => 1,
    'file:_addchild.tpl' => 1,
    'file:ci/index.".((string)$_smarty_tpl->tpl_vars[\'view\']->value).".tpl' => 1,
    'file:_ads.tpl' => 1,
    'file:_widget.tpl' => 1,
    'file:__feeds_group.tpl' => 1,
    'file:_footer.tpl' => 1,
  ),
),false)) {
function content_60614a2a883245_41691353 (Smarty_Internal_Template $_smarty_tpl) {
if (!$_smarty_tpl->tpl_vars['user']->value->_logged_in) {?>
    <?php $_smarty_tpl->_subTemplateRender('file:homepage.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php } else { ?>
    <?php $_smarty_tpl->_subTemplateRender('file:_head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

    <?php $_smarty_tpl->_subTemplateRender('file:_header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


    <!-- page content -->
    <div class="container mt20 offcanvas height_min"> <!-- class height_min để set height tối thiểu của container -->
        <div class="row">

            <!-- left panel -->
            <div class="col-sm-4 col-md-2 offcanvas-sidebar js_sticky-sidebar">
                <?php $_smarty_tpl->_subTemplateRender('file:_sidebar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

            </div>
            <!-- left panel -->

            <div class="col-sm-8 col-md-10 offcanvas-mainbar">
                <div class="row">
                    <!-- center panel -->
                    <div class="col-sm-12 col-md-8">

                        <!-- announcments -->
                        <?php $_smarty_tpl->_subTemplateRender('file:_announcements.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                        <!-- announcments -->

                        <!-- CI - Chủ đề quan tâm  -->
                        <?php if (!$_smarty_tpl->tpl_vars['system']->value['topics_enabled']) {?>
                            <div class="panel panel-default panel-users">
                                <div class="panel-heading light no_border">
                                    <strong class="text-muted"><?php echo __("Topics you are interested in?");?>
</strong>
                                    

                                </div>
                                <div class="panel-body pt5">
                                    <div class="row">

                                        

                                        <!-- users stories -->
                                        <div class="col-xs-2">
                                            <div class="user-picture-wrapper">
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/groups/?category=thaiky" class="user-picture" style="background-image:url('content/themes/<?php echo $_smarty_tpl->tpl_vars['system']->value['theme'];?>
/images/small/thai_ky.jpg');" data-toggle="tooltip" data-placement="top" title='<?php echo __("Preganancy");?>
'>
                                                </a>

                                            </div>
                                            <div class="text-center">
                                                <?php echo __("Preganancy");?>

                                            </div>
                                        </div>

                                        <div class="col-xs-2">
                                            <div class="user-picture-wrapper">
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/groups/?category=chamcon" class="user-picture" style="background-image:url('content/themes/<?php echo $_smarty_tpl->tpl_vars['system']->value['theme'];?>
/images/small/cham_con.jpg');" data-toggle="tooltip" data-placement="top" title='<?php echo __("Child care");?>
'>
                                                </a>
                                            </div>
                                            <div class="text-center">
                                                <?php echo __("Child care");?>

                                            </div>
                                        </div>

                                        <div class="col-xs-2">
                                            <div class="user-picture-wrapper">
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/groups/?category=daycon" class="user-picture" style="background-image:url('content/themes/<?php echo $_smarty_tpl->tpl_vars['system']->value['theme'];?>
/images/small/day_con.jpg');" data-toggle="tooltip" data-placement="top" title='<?php echo __("Parenting");?>
'>
                                                </a>
                                            </div>
                                            <div class="text-center">
                                                <?php echo __("Parenting");?>

                                            </div>
                                        </div>

                                        <div class="col-xs-2">
                                            <div class="user-picture-wrapper">
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/groups/?category=giadinh" class="user-picture" style="background-image:url('content/themes/<?php echo $_smarty_tpl->tpl_vars['system']->value['theme'];?>
/images/small/hon_nhan_gia_dinh.jpg');" data-toggle="tooltip" data-placement="top" title=' <?php echo __("Marriage and family");?>
'>
                                                </a>
                                            </div>
                                            <div class="text-center">
                                                <?php echo __("Marriage and family");?>

                                            </div>
                                        </div>
                                        <!-- users stories -->
                                    </div>
                                </div>
                            </div>
                        <?php }?>
                        <!-- stories -->

                        <?php if ($_smarty_tpl->tpl_vars['view']->value == '') {?>
                            <!-- publisher -->
                            <!-- Coniu - Thêm điều kiện IF allow_user_post_on_wall -->
                            <?php if ($_smarty_tpl->tpl_vars['system']->value['allow_user_post_on_wall']) {?>
                                <?php $_smarty_tpl->_subTemplateRender('file:_publisher.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('_handle'=>"me",'_privacy'=>true), 0, false);
?>

                            <?php }?>
                            <!-- publisher -->

                            <!-- posts stream -->
                            <?php $_smarty_tpl->_subTemplateRender('file:_posts.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('_get'=>"newsfeed"), 0, false);
?>

                            <!-- posts stream -->

                        <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "saved") {?>
                            <!-- saved posts stream -->
                            <?php $_smarty_tpl->_subTemplateRender('file:_posts.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('_get'=>"saved",'_title'=>__("Saved Posts")), 0, true);
?>

                            <!-- saved posts stream -->

                        <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "contact") {?>
                            <!-- contact taila -->
                            <?php $_smarty_tpl->_subTemplateRender('file:_contact.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('_get'=>"contact",'_title'=>__("Contact")), 0, false);
?>

                            <!-- contact taila -->
                        <?php } elseif ($_smarty_tpl->tpl_vars['view']->value == "addchild") {?>
                            <!-- add new child -->
                            <?php $_smarty_tpl->_subTemplateRender('file:_addchild.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('_get'=>"addchild",'_title'=>__("Add new child")), 0, false);
?>

                            <!-- add new child -->

                            <!-- ConIu - School -->
                        <?php } elseif (($_smarty_tpl->tpl_vars['view']->value == "create_school") || ($_smarty_tpl->tpl_vars['view']->value == "create_child")) {?>
                            <?php $_smarty_tpl->_subTemplateRender("file:ci/index.".((string)$_smarty_tpl->tpl_vars['view']->value).".tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

                            <!-- ConIu - End -->
                        <?php }?>
                    </div>
                    <!-- center panel -->

                    <!-- right panel -->
                    <div class="col-sm-12 col-md-4">

                        <?php $_smarty_tpl->_subTemplateRender('file:_ads.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                        <?php $_smarty_tpl->_subTemplateRender('file:_widget.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


                        <!-- suggested groups -->
                        <?php if (count($_smarty_tpl->tpl_vars['new_groups']->value) > 0) {?>
                            <div class="panel panel-default panel-widget">
                                <div class="panel-heading">
                                    <div class="pull-right flip">
                                        <small><a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/groups/discover"><?php echo __("See All");?>
</a></small>
                                    </div>
                                    <strong><?php echo __("Suggested Groups");?>
</strong>
                                </div>
                                <div class="panel-body">
                                    <ul>
                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['new_groups']->value, '_group');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['_group']->value) {
?>
                                            <?php $_smarty_tpl->_subTemplateRender('file:__feeds_group.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('_tpl'=>"list"), 0, true);
?>

                                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                    </ul>
                                </div>
                            </div>
                        <?php }?>
                        <!-- suggested groups -->

                        <!-- suggested pages -->
                        
                        <!-- suggested pages -->

                        <!-- people you may know -->
                        
                            
                                
                                    
                                        
                                    
                                    
                                
                                
                                    
                                        
                                            
                                        
                                    
                                
                            
                        
                        <!-- people you may know -->

                        <!-- mini footer -->
                        <?php if (count($_smarty_tpl->tpl_vars['user']->value->_data['new_people']) > 0 || count($_smarty_tpl->tpl_vars['new_pages']->value) > 0 || count($_smarty_tpl->tpl_vars['new_groups']->value) > 0 || count($_smarty_tpl->tpl_vars['new_events']->value) > 0) {?>
                            <div class="row plr10 hidden-xs">
                                <div class="col-xs-12 mb5">
                                    <?php if (count($_smarty_tpl->tpl_vars['static_pages']->value) > 0) {?>
                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['static_pages']->value, 'static_page', true);
$_smarty_tpl->tpl_vars['static_page']->iteration = 0;
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['static_page']->value) {
$_smarty_tpl->tpl_vars['static_page']->iteration++;
$_smarty_tpl->tpl_vars['static_page']->last = $_smarty_tpl->tpl_vars['static_page']->iteration == $_smarty_tpl->tpl_vars['static_page']->total;
$__foreach_static_page_1_saved = $_smarty_tpl->tpl_vars['static_page'];
?>
                                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/static/<?php echo $_smarty_tpl->tpl_vars['static_page']->value['page_url'];?>
">
                                            <?php echo $_smarty_tpl->tpl_vars['static_page']->value['page_title'];?>

                                            </a><?php if (!$_smarty_tpl->tpl_vars['static_page']->last) {?> · <?php }?>
                                        <?php
$_smarty_tpl->tpl_vars['static_page'] = $__foreach_static_page_1_saved;
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                    <?php }?>
                                    <?php if ($_smarty_tpl->tpl_vars['system']->value['contact_enabled']) {?>
                                        ·
                                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/contacts">
                                            <?php echo __("Contacts");?>

                                        </a>
                                    <?php }?>
                                    <?php if ($_smarty_tpl->tpl_vars['system']->value['directory_enabled']) {?>
                                        ·
                                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/directory">
                                            <?php echo __("Directory");?>

                                        </a>
                                    <?php }?>
                                    <?php if ($_smarty_tpl->tpl_vars['system']->value['market_enabled']) {?>
                                        ·
                                        <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/market">
                                            <?php echo __("Market");?>

                                        </a>
                                    <?php }?>
                                </div>
                                <div class="col-xs-12">
                                    &copy; <?php echo date('Y');?>
 <?php echo $_smarty_tpl->tpl_vars['system']->value['system_title'];?>
 · <span class="text-link" data-toggle="modal" data-url="#translator"><?php echo $_smarty_tpl->tpl_vars['system']->value['language']['title'];?>
</span> |
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/privacy"><?php echo __("Privacy");?>
</a> | <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/terms"><?php echo __("Terms");?>
</a>
                                </div>
                            </div>
                        <?php }?>
                        <!-- mini footer -->

                    </div>
                    <!-- right panel -->
                </div>
            </div>

        </div>
    </div>
    <!-- page content -->
    <?php $_smarty_tpl->_subTemplateRender('file:_footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php }?>

<?php }
}
