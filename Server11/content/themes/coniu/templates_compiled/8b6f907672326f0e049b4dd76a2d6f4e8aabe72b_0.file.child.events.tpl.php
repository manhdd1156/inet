<?php
/* Smarty version 3.1.31, created on 2021-03-31 10:48:24
  from "D:\workplace\Server11\content\themes\coniu\templates\ci\child\child.events.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_6063f1083e0ca2_79860252',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '8b6f907672326f0e049b4dd76a2d6f4e8aabe72b' => 
    array (
      0 => 'D:\\workplace\\Server11\\content\\themes\\coniu\\templates\\ci\\child\\child.events.tpl',
      1 => 1552404702,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6063f1083e0ca2_79860252 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <i class="fa fa-bell fa-lg fa-fw pr10"></i>
        <?php echo __("Notification - Event");?>

        <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == "detail") {?>
            &rsaquo; <?php echo $_smarty_tpl->tpl_vars['data']->value['event_name'];?>

        <?php }?>
    </div>
    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
        <div class="panel-body with-table">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th><?php echo __("Notification - Event");?>
</th>
                        <th><?php echo __("Event level");?>
</th>
                        
                        <th><?php echo __("Registration deadline");?>
</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['rows']->value, 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
                        <tr>
                            <?php if ($_smarty_tpl->tpl_vars['idx']->value > 1) {?><tr><td colspan="5"></td></tr><?php }?>
                            <td align="center" rowspan="2" style="vertical-align:middle"><strong><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</strong></td>
                            <td style="vertical-align:middle">
                                <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/child/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
/events/detail/<?php echo $_smarty_tpl->tpl_vars['row']->value['event_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['row']->value['event_name'];?>
</a> <a href="#" data-id="<?php echo $_smarty_tpl->tpl_vars['row']->value['event_id'];?>
" style="float: right; color: #5D9D58" class="js_event_toggle"><?php echo __("Show/hide");?>
</a>
                                <?php if ($_smarty_tpl->tpl_vars['row']->value['status'] == @constant('EVENT_STATUS_CANCELLED')) {?>
                                    <img src="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/content/themes/<?php echo $_smarty_tpl->tpl_vars['system']->value['theme'];?>
/images/cancel.png"/>
                                <?php }?>
                            </td>
                            <td align="center" style="vertical-align:middle">
                                <?php if ($_smarty_tpl->tpl_vars['row']->value['level'] == @constant('SCHOOL_LEVEL')) {?>
                                    <?php echo __("School");?>

                                <?php } elseif ($_smarty_tpl->tpl_vars['row']->value['level'] == @constant('CLASS_LEVEL_LEVEL')) {?>
                                    <?php echo __("Class level");?>

                                <?php } elseif ($_smarty_tpl->tpl_vars['row']->value['level'] == @constant('CLASS_LEVEL')) {?>
                                    <?php echo __("Class");?>

                                <?php }?>
                            </td>
                            
                                
                                    
                                
                            
                            <td align="center" style="vertical-align:middle">
                                <?php if ((isset($_smarty_tpl->tpl_vars['row']->value['registration_deadline']) && ($_smarty_tpl->tpl_vars['row']->value['registration_deadline'] != ''))) {?>
                                    <font color="red"><?php echo $_smarty_tpl->tpl_vars['row']->value['registration_deadline'];?>
</font>
                                <?php }?>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" style="display: none" class="event_description_<?php echo $_smarty_tpl->tpl_vars['row']->value['event_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['row']->value['description'];?>
</td>
                        </tr>
                        <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                    </tbody>
                </table>
            </div>
        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['sub_view']->value == "detail") {?>
        <div class="panel-body with-table">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/child/bochild_all.php">
                <input type="hidden" name="school_username" id="school_username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
"/>
                <input type="hidden" name="do" id="do" value="reg_event"/>
                <input type="hidden" id="school_id" name="school_id" value="<?php echo $_smarty_tpl->tpl_vars['school']->value['page_id'];?>
"/>
                <input type="hidden" id="child_id" name="child_id" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
"/>
                <input type="hidden" id="event_id" name="event_id" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['event_id'];?>
"/>
                <input type="hidden" name="event_name" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['event_name'];?>
"/>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <tbody>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Notification - Event");?>
</strong></td>
                            <td><?php echo $_smarty_tpl->tpl_vars['data']->value['event_name'];?>
</td>
                        </tr>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Event level");?>
</strong></td>
                            <td>
                                <?php if ($_smarty_tpl->tpl_vars['data']->value['level'] == @constant('SCHOOL_LEVEL')) {?>
                                    <?php echo __("School");?>

                                <?php } elseif ($_smarty_tpl->tpl_vars['data']->value['level'] == @constant('CLASS_LEVEL_LEVEL')) {?>
                                    <?php echo $_smarty_tpl->tpl_vars['data']->value['class_level_name'];?>

                                <?php } elseif ($_smarty_tpl->tpl_vars['data']->value['level'] == @constant('CLASS_LEVEL')) {?>
                                    <?php echo $_smarty_tpl->tpl_vars['data']->value['group_title'];?>

                                <?php }?>
                            </td>
                        </tr>
                        
                            
                                
                                
                                    
                                
                            
                        
                        <?php if ($_smarty_tpl->tpl_vars['data']->value['must_register'] == '1') {?>
                            
                                
                                
                            
                            
                                
                                
                            
                            <tr>
                                <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Must register");?>
?</strong></td>
                                <td><?php echo __('Yes');?>
</td>
                            </tr>
                            <?php if ($_smarty_tpl->tpl_vars['data']->value['registration_deadline'] != '') {?>
                                <tr>
                                    <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Registration deadline");?>
</strong></td>
                                    <td><font color="red"><?php echo $_smarty_tpl->tpl_vars['data']->value['registration_deadline'];?>
</font></td>
                                </tr>
                            <?php }?>
                            <tr>
                                <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("For child");?>
?</strong></td>
                                <td><?php if ($_smarty_tpl->tpl_vars['data']->value['for_child'] == '1') {
echo __('Yes');
} else {
echo __('No');
}?></td>
                            </tr>
                            <?php if ($_smarty_tpl->tpl_vars['data']->value['for_child'] == '1') {?>
                                <tr>
                                    <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Children total");?>
</strong></td>
                                    <td><?php echo $_smarty_tpl->tpl_vars['data']->value['child_participants'];?>
</td>
                                </tr>
                            <?php }?>
                            <tr>
                                <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("For parent");?>
?</strong></td>
                                <td><?php if ($_smarty_tpl->tpl_vars['data']->value['for_parent'] == '1') {
echo __('Yes');
} else {
echo __('No');
}?></td>
                            </tr>
                            <?php if ($_smarty_tpl->tpl_vars['data']->value['for_parent'] == '1') {?>
                                <tr>
                                    <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Parent total");?>
</strong></td>
                                    <td><?php echo $_smarty_tpl->tpl_vars['data']->value['parent_participants'];?>
</td>
                                </tr>
                            <?php }?>
                        <?php }?>
                        
                            
                            
                        
                        
                            
                                
                                
                            
                        
                        <tr>
                            
                            <td colspan="2"><?php echo $_smarty_tpl->tpl_vars['data']->value['description'];?>
</td>
                        </tr>
                        <?php if ($_smarty_tpl->tpl_vars['data']->value['must_register']) {?>
                            <tr>
                                <td nowrap="true" align="right" style="vertical-align:middle"><strong><?php echo __("Register");?>
</strong></td>
                                <td>
                                    <?php if ($_smarty_tpl->tpl_vars['data']->value['for_child']) {?>
                                        <input type="checkbox" name="childId" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
"
                                               <?php if ($_smarty_tpl->tpl_vars['data']->value['participants']['child']['is_registered']) {?>checked<?php }?> <?php if ($_smarty_tpl->tpl_vars['data']->value['can_register'] == 0) {?>disabled<?php }?>> <?php echo $_smarty_tpl->tpl_vars['child']->value['child_name'];?>

                                        <?php if ($_smarty_tpl->tpl_vars['data']->value['participants']['child']['is_registered']) {?>
                                            <input type="hidden" name="old_child_id" value="<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
"/>
                                        <?php }?>
                                    <?php }?>
                                    <br/>
                                    <?php if ($_smarty_tpl->tpl_vars['data']->value['for_parent']) {?>
                                        <input type="checkbox" name="parent_id"  value="<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_id'];?>
" event_id="<?php echo $_smarty_tpl->tpl_vars['data']->value['event_id'];?>
"
                                               <?php if ($_smarty_tpl->tpl_vars['data']->value['participants']['parent']['is_registered']) {?>checked<?php }?> <?php if ($_smarty_tpl->tpl_vars['data']->value['can_register'] == 0) {?>disabled<?php }?>> <?php echo __("You");?>

                                        <?php if ($_smarty_tpl->tpl_vars['data']->value['participants']['parent']['is_registered']) {?>
                                            <input type="hidden" name="old_parent_id" value="<?php echo $_smarty_tpl->tpl_vars['user']->value->_data['user_id'];?>
"/>
                                        <?php }?>
                                    <?php }?>
                                </td>
                            </tr>
                        <?php }?>
                        </tbody>
                    </table>
                </div>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-1">
                        <?php if ($_smarty_tpl->tpl_vars['data']->value['can_register'] > 0 && $_smarty_tpl->tpl_vars['data']->value['must_register']) {?>
                            <button type="submit" class="btn btn-primary padrl30"><?php echo __("Save");?>
</button>
                        <?php }?>
                        <a class="btn btn-default" href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/child/<?php echo $_smarty_tpl->tpl_vars['child']->value['child_id'];?>
/events"><?php echo __("Lists");?>
</a>
                    </div>
                </div>
                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->
                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
            </form>
        </div>
    <?php }?>
</div><?php }
}
