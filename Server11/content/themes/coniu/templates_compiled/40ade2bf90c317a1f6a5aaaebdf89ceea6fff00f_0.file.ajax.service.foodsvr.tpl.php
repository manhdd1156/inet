<?php
/* Smarty version 3.1.31, created on 2021-04-29 17:19:15
  from "D:\workplace\mascom-edu-server\Server11\content\themes\coniu\templates\ci\school\ajax.service.foodsvr.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_608a88235bf431_99207585',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '40ade2bf90c317a1f6a5aaaebdf89ceea6fff00f' => 
    array (
      0 => 'D:\\workplace\\mascom-edu-server\\Server11\\content\\themes\\coniu\\templates\\ci\\school\\ajax.service.foodsvr.tpl',
      1 => 1619677394,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_608a88235bf431_99207585 (Smarty_Internal_Template $_smarty_tpl) {
if ((!isset($_smarty_tpl->tpl_vars['service_list']->value) || (!isset($_smarty_tpl->tpl_vars['classes']->value)))) {?>
    <div class="pl20"><strong><font color="red"><?php echo __("There is no feed service or token-attendance class");?>
</font></strong></div>
<?php } else { ?>
    <div><strong><?php echo __("Daily food service summary");?>
</strong></div>
    <table class="table table-striped table-bordered table-hover">
        <thead>
        <tr>
            <th>#</th>
            <th><?php echo __("Class");?>
</th>
            <th><?php echo __("Present");?>
</th>
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['service_list']->value, 'service');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['service']->value) {
?>
                <th><?php echo $_smarty_tpl->tpl_vars['service']->value['service_name'];?>
</th>
            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

        </tr>
        </thead>
        <tbody>
        <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
        <?php $_smarty_tpl->_assignInScope('presentTotal', 0);
?>
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['classes']->value, 'class');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class']->value) {
?>
            <tr>
                <td align="center"><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</td>
                <td nowrap="true"><?php echo $_smarty_tpl->tpl_vars['class']->value['group_title'];?>
</td>
                <td align="center"><?php echo $_smarty_tpl->tpl_vars['class']->value['present_total'];?>
</td>
                <?php if (isset($_smarty_tpl->tpl_vars['class']->value['not_countbased_services']) && (count($_smarty_tpl->tpl_vars['class']->value['not_countbased_services']) > 0)) {?>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['class']->value['not_countbased_services'], 'service');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['service']->value) {
?>
                        <td align="center"><?php echo $_smarty_tpl->tpl_vars['service']->value['total'];?>
</td>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                <?php } else { ?>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['service_list']->value, 'service');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['service']->value) {
?>
                        <?php if ($_smarty_tpl->tpl_vars['service']->value['type'] != @constant('SERVICE_TYPE_COUNT_BASED')) {?>
                            <td align="center">0</td>
                        <?php }?>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                <?php }?>
                <?php if (isset($_smarty_tpl->tpl_vars['class']->value['countbased_services']) && (count($_smarty_tpl->tpl_vars['class']->value['countbased_services']) > 0)) {?>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['class']->value['countbased_services'], 'service');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['service']->value) {
?>
                        <td align="center"><?php echo $_smarty_tpl->tpl_vars['service']->value['total'];?>
</td>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                <?php } else { ?>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['service_list']->value, 'service');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['service']->value) {
?>
                        <?php if ($_smarty_tpl->tpl_vars['service']->value['type'] == @constant('SERVICE_TYPE_COUNT_BASED')) {?>
                            <td align="center">0</td>
                        <?php }?>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                <?php }?>
            </tr>
            <?php $_smarty_tpl->_assignInScope('presentTotal', $_smarty_tpl->tpl_vars['presentTotal']->value+$_smarty_tpl->tpl_vars['class']->value['present_total']);
?>
            <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

        <?php if ($_smarty_tpl->tpl_vars['idx']->value > 2) {?>
            <tr>
                <td colspan="2" align="center"><strong><font color="red"><?php echo mb_strtoupper(__("Total"), 'UTF-8');?>
</font></strong></td>
                <td align="center"><strong><font color="red"><?php echo $_smarty_tpl->tpl_vars['presentTotal']->value;?>
</font></strong></td>
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['service_total']->value, 'total');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['total']->value) {
?>
                    <td align="center"><strong><font color="red"><?php echo $_smarty_tpl->tpl_vars['total']->value;?>
</font></strong></td>
                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

            </tr>
        <?php }?>
        </tbody>
    </table>
<?php }
}
}
