<?php
/* Smarty version 3.1.31, created on 2021-05-19 11:26:30
  from "D:\workplace\mascom-edu-server\Server11\content\themes\coniu\templates\ci\school\ajax.school.pickup.assign.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_60a493769fa442_91816896',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c313506201ed807ceb3456f2547404b5a3521464' => 
    array (
      0 => 'D:\\workplace\\mascom-edu-server\\Server11\\content\\themes\\coniu\\templates\\ci\\school\\ajax.school.pickup.assign.tpl',
      1 => 1619677394,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60a493769fa442_91816896 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="table-responsive">
    <table class="table table-striped table-bordered">
        <thead>
        <tr bgcolor="#ffebcd">
            <th class="pinned" id="width_no">#</th>
            <th nowrap="true" class="pinned"><?php echo __("Full name");?>
</th>

            <?php $_smarty_tpl->_assignInScope('dayIdx', 0);
?>
            <?php $_smarty_tpl->_assignInScope('sunDayIdxs', array());
?>
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['dates']->value, 'date');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['date']->value) {
?>
                <?php if ($_smarty_tpl->tpl_vars['days']->value[$_smarty_tpl->tpl_vars['dayIdx']->value] == 'CN') {?>
                    <?php $_tmp_array = isset($_smarty_tpl->tpl_vars['sunDayIdxs']) ? $_smarty_tpl->tpl_vars['sunDayIdxs']->value : array();
if (!is_array($_tmp_array) || $_tmp_array instanceof ArrayAccess) {
settype($_tmp_array, 'array');
}
$_tmp_array[] = $_smarty_tpl->tpl_vars['dayIdx']->value+1;
$_smarty_tpl->_assignInScope('sunDayIdxs', $_tmp_array);
?>
                    <th align="center" bgcolor="#a9a9a9">
                <?php } else { ?>
                    <th align="center">
                <?php }?>
                    <?php echo $_smarty_tpl->tpl_vars['date']->value;?>
<br>
                    <label style="color: blue"><?php echo $_smarty_tpl->tpl_vars['days']->value[$_smarty_tpl->tpl_vars['dayIdx']->value];?>
</label>
                </th>
                <?php $_smarty_tpl->_assignInScope('dayIdx', $_smarty_tpl->tpl_vars['dayIdx']->value+1);
?>
            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

            
        </tr>
        </thead>
        <tbody>
        <?php $_smarty_tpl->_assignInScope('date_count', count($_smarty_tpl->tpl_vars['dates']->value));
?>
        <?php $_smarty_tpl->_assignInScope('rowIdx', 1);
?>
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['rows']->value, 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>

            <input type="hidden" name="teacher[]" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['user_id'];?>
" />
            <tr>
                <td align="center"><?php echo $_smarty_tpl->tpl_vars['rowIdx']->value;?>
</td>
                <td nowrap="true"><a><?php echo $_smarty_tpl->tpl_vars['row']->value['teacher_name'];?>
</a></td>

                <?php $_smarty_tpl->_assignInScope('presentCnt', 0);
?>
                <?php $_smarty_tpl->_assignInScope('totalCnt', 0);
?>
                <?php $_smarty_tpl->_assignInScope('idx', 1);
?>

                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['row']->value['cells'], 'cell');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['cell']->value) {
?>

                    <td align="center" style="padding: 0; <?php if (in_array($_smarty_tpl->tpl_vars['idx']->value,$_smarty_tpl->tpl_vars['sunDayIdxs']->value)) {?>background-color: #a9a9a9<?php }?> ">
                        <label class="container_cb">
                        <input type="checkbox" name="teacher_<?php echo $_smarty_tpl->tpl_vars['row']->value['user_id'];?>
[]" value="<?php echo $_smarty_tpl->tpl_vars['cell']->value['date_row'];?>
"
                               <?php if ($_smarty_tpl->tpl_vars['cell']->value['is_checked'] == 1) {?>checked<?php }?>/>
                            <span class="checkmark"></span>
                        </label>
                    </td>
                    <?php $_smarty_tpl->_assignInScope('totalCnt', $_smarty_tpl->tpl_vars['totalCnt']->value+1);
?>
                    <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                
                
                
                

                <?php $_smarty_tpl->_assignInScope('rowIdx', $_smarty_tpl->tpl_vars['rowIdx']->value+1);
?>
            </tr>
            
            <?php if ($_smarty_tpl->tpl_vars['rowIdx']->value%16 == 0) {?>
                <tr bgcolor="#ffebcd">
                    <th class="pinned" id="width_no">#</th>
                    <th nowrap="true" class="pinned"><?php echo __("Full name");?>
</th>
                    <?php $_smarty_tpl->_assignInScope('dayIdx', 0);
?>
                    <?php $_smarty_tpl->_assignInScope('sunDayIdxs', array());
?>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['dates']->value, 'date');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['date']->value) {
?>
                        <?php if ($_smarty_tpl->tpl_vars['days']->value[$_smarty_tpl->tpl_vars['dayIdx']->value] == 'CN') {?>
                            <?php $_tmp_array = isset($_smarty_tpl->tpl_vars['sunDayIdxs']) ? $_smarty_tpl->tpl_vars['sunDayIdxs']->value : array();
if (!is_array($_tmp_array) || $_tmp_array instanceof ArrayAccess) {
settype($_tmp_array, 'array');
}
$_tmp_array[] = $_smarty_tpl->tpl_vars['dayIdx']->value+1;
$_smarty_tpl->_assignInScope('sunDayIdxs', $_tmp_array);
?>
                            <th align="center" bgcolor="#a9a9a9">
                                <?php } else { ?>
                            <th align="center">
                        <?php }?>
                        <?php echo $_smarty_tpl->tpl_vars['date']->value;?>
<br>
                        <label style="color: blue"><?php echo $_smarty_tpl->tpl_vars['days']->value[$_smarty_tpl->tpl_vars['dayIdx']->value];?>
</label>
                        </th>
                        <?php $_smarty_tpl->_assignInScope('dayIdx', $_smarty_tpl->tpl_vars['dayIdx']->value+1);
?>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                    
                </tr>
            <?php }?>

        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

        

        </tbody>
    </table>
</div>



<?php }
}
