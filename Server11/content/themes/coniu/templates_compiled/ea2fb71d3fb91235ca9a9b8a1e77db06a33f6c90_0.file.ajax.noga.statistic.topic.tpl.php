<?php
/* Smarty version 3.1.31, created on 2021-03-31 08:54:40
  from "D:\workplace\Server11\content\themes\coniu\templates\ci\noga\ajax.noga.statistic.topic.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_6063d660b55769_89145589',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ea2fb71d3fb91235ca9a9b8a1e77db06a33f6c90' => 
    array (
      0 => 'D:\\workplace\\Server11\\content\\themes\\coniu\\templates\\ci\\noga\\ajax.noga.statistic.topic.tpl',
      1 => 1552404704,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6063d660b55769_89145589 (Smarty_Internal_Template $_smarty_tpl) {
?>
<table class="table table-striped table-bordered table-hover" id="statistic_topic_detail">
    <thead>
    <tr>
        <th><?php echo __("Groups");?>
</th>
        <th><?php echo __("Lượt tương tác");?>
</th>

    </tr>
    </thead>
    <tbody>

    <tr>
        <td><strong><?php echo __("Coniu+");?>
</strong></td>
        <td align="center"><?php echo $_smarty_tpl->tpl_vars['rows']->value[@constant('GROUP_NAME_CONIU')];?>
</td>
    </tr>
    <tr>
        <td><strong><?php echo __("Ăn dặm");?>
</strong></td>
        <td align="center"><?php echo $_smarty_tpl->tpl_vars['rows']->value[@constant('GROUP_NAME_ANDAM')];?>
</td>
    </tr>
    <tr>
        <td><strong><?php echo __("Dinh dưỡng thai kỳ");?>
</strong></td>
        <td align="center"><?php echo $_smarty_tpl->tpl_vars['rows']->value[@constant('GROUP_NAME_DINHDUONG')];?>
</td>
    </tr>
    <tr>
        <td><strong><?php echo __("Dạy con đúng cách");?>
</strong></td>
        <td align="center"><?php echo $_smarty_tpl->tpl_vars['rows']->value[@constant('GROUP_NAME_DAYCON')];?>
</td>
    </tr>
    <tr>
        <td><strong><?php echo __("Thai giáo");?>
</strong></td>
        <td align="center"><?php echo $_smarty_tpl->tpl_vars['rows']->value[@constant('GROUP_NAME_THAIGIAO')];?>
</td>
    </tr>
    <tr>
        <td><strong><?php echo __("Hiếm muộn");?>
</strong></td>
        <td align="center"><?php echo $_smarty_tpl->tpl_vars['rows']->value[@constant('GROUP_NAME_HIEMMUON')];?>
</td>
    </tr>
    <tr>
        <td><strong><?php echo __("Hôn nhân gia đình");?>
</strong></td>
        <td align="center"><?php echo $_smarty_tpl->tpl_vars['rows']->value[@constant('GROUP_NAME_HONNHAN')];?>
</td>
    </tr>
    <tr>
        <td><strong><?php echo __("Thực phẩm Organic");?>
</strong></td>
        <td align="center"><?php echo $_smarty_tpl->tpl_vars['rows']->value[@constant('GROUP_NAME_THUCPHAMORGANIC')];?>
</td>
    </tr>

    </tbody>
</table><?php }
}
