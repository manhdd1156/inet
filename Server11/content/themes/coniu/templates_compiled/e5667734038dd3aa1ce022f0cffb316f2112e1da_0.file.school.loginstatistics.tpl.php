<?php
/* Smarty version 3.1.31, created on 2021-03-31 10:42:18
  from "D:\workplace\Server11\content\themes\coniu\templates\ci\school\school.loginstatistics.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_6063ef9abde697_85826405',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e5667734038dd3aa1ce022f0cffb316f2112e1da' => 
    array (
      0 => 'D:\\workplace\\Server11\\content\\themes\\coniu\\templates\\ci\\school\\school.loginstatistics.tpl',
      1 => 1552404702,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:ci/school/ajax.school.children.statistics.list.tpl' => 1,
  ),
),false)) {
function content_6063ef9abde697_85826405 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <i class="fa fa-child fa-fw fa-lg pr10"></i>
        <?php echo __("Login statistics");?>

    </div>
    <?php if ($_smarty_tpl->tpl_vars['sub_view']->value == '') {?>
        <div class="panel-body with-table">
            <div class="row">
                <div class='col-sm-3'>
                    <div class="form-group">
                        <select name="class_id" id="class_id" class="form-control">
                            <option value=""><?php echo __("Select Class");?>
...</option>
                            <?php if ($_smarty_tpl->tpl_vars['school']->value['children_use_no_class']) {?>
                                <option value="0" <?php if ($_smarty_tpl->tpl_vars['result']->value['class_id'] == '0') {?>selected<?php }?>><?php echo __("No class");?>
</option>
                            <?php }?>
                            <?php $_smarty_tpl->_assignInScope('class_level', -1);
?>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['classes']->value, 'class');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class']->value) {
?>
                                <?php if (($_smarty_tpl->tpl_vars['class_level']->value != $_smarty_tpl->tpl_vars['class']->value['class_level_id'])) {?>
                                    <option value="" disabled>-----<?php echo $_smarty_tpl->tpl_vars['class']->value['class_level_name'];?>
-----</option>
                                <?php }?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['class']->value['group_id'];?>
" <?php if ($_smarty_tpl->tpl_vars['result']->value['class_id'] == $_smarty_tpl->tpl_vars['class']->value['group_id']) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['class']->value['group_title'];?>
</option>
                                <?php $_smarty_tpl->_assignInScope('class_level', $_smarty_tpl->tpl_vars['class']->value['class_level_id']);
?>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                        </select>
                    </div>
                </div>
                <div class='col-sm-2'>
                    <div class = "row">
                        <div class = "col-sm-6">
                            <div class="form-group">
                                <a href="#" id="search" class="btn btn-default js_child-statistics-search" data-username="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" data-isnew="1" data-id="<?php echo $_smarty_tpl->tpl_vars['school']->value['page_id'];?>
"><?php echo __("Search");?>
</a>
                                <label id="loading" class="btn btn-info x-hidden"><?php echo __("Loading");?>
...</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br/>
            <div class="table-responsive" id="child_list" name="child_list">
                <?php $_smarty_tpl->_subTemplateRender("file:ci/school/ajax.school.children.statistics.list.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

            </div>
        </div>
    <?php }?>
</div><?php }
}
