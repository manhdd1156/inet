<?php
/* Smarty version 3.1.31, created on 2021-03-31 10:26:48
  from "D:\workplace\Server11\content\themes\coniu\templates\ci\class\ajax.class.categorydetail.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_6063ebf8475ab4_18890565',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'd024204e6e9c2ea7a9b27d6409347e43a8856f84' => 
    array (
      0 => 'D:\\workplace\\Server11\\content\\themes\\coniu\\templates\\ci\\class\\ajax.class.categorydetail.tpl',
      1 => 1552404703,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6063ebf8475ab4_18890565 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="form-group">
    <?php echo $_smarty_tpl->tpl_vars['results']->value['category_name'];?>

</div>
<div class = "table-responsive">
    <table class="table table-striped table-bordered table-hover js_dataTable">
        <thead>
        <tr>
            <th><?php echo __("#");?>
</th>
            <th><?php echo __("Category suggest");?>
</th>
        </tr>
        </thead>
        <tbody>
        <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['results']->value['suggests'], 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
            <tr>
                <td class="align-middle">
                    <center><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</center>
                </td>
                <td class="align-middle">
                    <?php echo $_smarty_tpl->tpl_vars['row']->value;?>

                </td>
            </tr>
            <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

        </tbody>
    </table>
</div><?php }
}
