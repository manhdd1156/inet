<?php
/* Smarty version 3.1.31, created on 2021-04-01 15:18:51
  from "D:\workplace\Server11\content\themes\coniu\templates\ci\school\ajax.childnewlist.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_606581eb17d492_48417660',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '30bac35fdcbb0df9a68a65aaf97a22efddb49c22' => 
    array (
      0 => 'D:\\workplace\\Server11\\content\\themes\\coniu\\templates\\ci\\school\\ajax.childnewlist.tpl',
      1 => 1552404702,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_606581eb17d492_48417660 (Smarty_Internal_Template $_smarty_tpl) {
?>
<strong><?php echo __("Children list");?>
&nbsp;(<?php echo count($_smarty_tpl->tpl_vars['results']->value);?>
 <?php echo __("Children");?>
)</strong>
<table class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th><?php echo __("#");?>
</th>
        <th><?php echo __("Full name");?>
</th>
        <th><?php echo __("Study start date");?>
</th>
        <th><?php echo __("Creator");?>
</th>
    </tr>
    </thead>
    <tbody>
        <?php if (count($_smarty_tpl->tpl_vars['results']->value) > 0) {?>
            <?php $_smarty_tpl->_assignInScope('classId', "-1" ,true);
?>
            <?php $_smarty_tpl->_assignInScope('idx', 1);
?>
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['results']->value, 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
                <?php if (($_smarty_tpl->tpl_vars['classId']->value != $_smarty_tpl->tpl_vars['row']->value['class_id'])) {?>
                    <tr>
                        <td colspan="4">
                            <?php if ($_smarty_tpl->tpl_vars['row']->value['class_id'] > 0) {?>
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['classes']->value, 'class');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['class']->value) {
?>
                                    <?php if ($_smarty_tpl->tpl_vars['class']->value['group_id'] == $_smarty_tpl->tpl_vars['row']->value['class_id']) {?>
                                        <?php echo __("Class");?>
:&nbsp;<?php echo $_smarty_tpl->tpl_vars['class']->value['group_title'];?>

                                    <?php }?>
                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                            <?php } else { ?>
                                <?php echo __("No class");?>

                            <?php }?>
                        </td>
                    </tr>
                <?php }?>
                <tr>
                    <td align="center" style ="vertical-align: middle"><?php echo $_smarty_tpl->tpl_vars['idx']->value;?>
</td>
                    
                    <td style ="vertical-align: middle" align="center"><a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/school/<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
/children/detail/<?php echo $_smarty_tpl->tpl_vars['row']->value['child_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['row']->value['child_name'];?>
</a></td>
                    <td style ="vertical-align: middle" align="center"><?php echo $_smarty_tpl->tpl_vars['row']->value['begin_at'];?>
</td>
                    <td style ="vertical-align: middle" align="center"><?php echo $_smarty_tpl->tpl_vars['row']->value['user_fullname'];?>
</td>
                </tr>
                <?php $_smarty_tpl->_assignInScope('classId', $_smarty_tpl->tpl_vars['row']->value['class_id'] ,true);
?>
                <?php $_smarty_tpl->_assignInScope('idx', $_smarty_tpl->tpl_vars['idx']->value+1);
?>
            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

        <?php } else { ?>
            <tr>
                <td colspan="4"><?php echo __("No student study begin in this time");?>
</td>
            </tr>
        <?php }?>
    </tbody>
</table><?php }
}
