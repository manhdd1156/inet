{include file='_head.tpl'}
{include file='_header.tpl'}

<!-- page content -->
<div class="container mt20"
     {if ($view == 'attendance' && $sub_view == '')}style="width: 100%!important; padding-left: 10px; padding-right: 10px;"{/if}>
    <div class="row">
        <div class="{if $view == 'attendance' && $sub_view == ''}col-sm-2 col-md-2{else} col-sm-3 col-md-3{/if}"
             id="menu_left">
            <nav class="navbar navbar-default" role="navigation">
                <div class="panel panel-default">
                    <div class="menu_school">
                        {if $view == ""}
                            <i class="fas fa-tachometer-alt fa-fw fa-lg pr10"></i>
                            {__("Dashboard")}
                        {elseif $view == "attendance"}
                            {if $sub_view == ""}
                                <i class="fa fa-tasks fa-fw fa-lg pr10"></i>
                                {__("Attendance")}
                                <i class="fa fa-caret-right fa-fw pr10"></i>
                                {__("Lists")}
                            {elseif $sub_view == "rollup"}
                                <i class="fa fa-tasks fa-fw fa-lg pr10"></i>
                                {__("Attendance")}
                                <i class="fa fa-caret-right fa-fw pr10"></i>
                                {__("Roll up")}
                            {/if}
                        {elseif $view == "medicines"}
                            {if $sub_view == ""}
                                <i class="fa fa-medkit fa-lg fa-fw pr10"></i>
                                {__("Medicines")}
                                <i class="fa fa-caret-right fa-fw pr10"></i>
                                {__("Today lists")}
                            {elseif $sub_view == "all"}
                                <i class="fa fa-medkit fa-lg fa-fw pr10"></i>
                                {__("Medicines")}
                                <i class="fa fa-caret-right fa-fw pr10"></i>
                                {__("List all")}
                            {elseif $sub_view == "add"}
                                <i class="fa fa-medkit fa-lg fa-fw pr10"></i>
                                {__("Medicines")}
                                <i class="fa fa-caret-right fa-fw pr10"></i>
                                {__("Add New")}
                            {/if}
                        {elseif ($view == "schedules")}
                            <i class="fas fa-calendar-alt fa-lg fa-fw pr10"></i>
                            {__("Schedule")}
                        {elseif ($view == "menus")}
                            <i class="fas fa-utensils fa-lg fa-fw pr10"></i>
                            {__("Menu")}
                        {elseif ($view == "useservices") || ($view == "services")}
                            {if $sub_view == "record"}
                                <i class="fa fa-life-ring fa-lg fa-fw pr10"></i>
                                {__("Service")}
                                <i class="fa fa-caret-right fa-fw pr10"></i>
                                {__("Register service")}
                            {elseif $sub_view == "history"}
                                <i class="fa fa-life-ring fa-lg fa-fw pr10"></i>
                                {__("Service")}
                                <i class="fa fa-caret-right fa-fw pr10"></i>
                                {__("Service usage information")}
                            {/if}
                        {elseif ($view == "tuitions") || ($view == "fees")}
                            <i class="fas fa-money-bill-alt fa-lg fa-fw pr10"></i>
                            {__("Tuition fee")}
                        {elseif $view == "reports"}
                            {if $sub_view == ""}
                                <i class="fa fa-book fa-lg fa-fw pr10"></i>
                                {__("Contact book")}
                                <i class="fa fa-caret-right fa-fw pr10"></i>
                                {__("Lists")}
                            {elseif $sub_view == "add"}
                                <i class="fa fa-book fa-lg fa-fw pr10"></i>
                                {__("Contact book")}
                                <i class="fa fa-caret-right fa-fw pr10"></i>
                                {__("Add New")}
                            {elseif $sub_view == "listtemp"}
                                <i class="fa fa-book fa-lg fa-fw pr10"></i>
                                {__("Contact book")}
                                <i class="fa fa-caret-right fa-fw pr10"></i>
                                {__("List template")}
                            {/if}
                        {elseif $view == "points"}
                            {if $sub_view == "lists_by_subject"}
                                <i class="fa fa-book fa-lg fa-fw pr10"></i>
                                {__("Points")}
                                <i class="fa fa-caret-right fa-fw pr10"></i>
                                {__("Lists by subject")}
                            {elseif $sub_view == "lists_by_student"}
                                <i class="fa fa-book fa-lg fa-fw pr10"></i>
                                {__("Points")}
                                <i class="fa fa-caret-right fa-fw pr10"></i>
                                {__("Lists by student")}
                            {elseif $sub_view == "importManual"}
                                <i class="fa fa-book fa-lg fa-fw pr10"></i>
                                {__("Points")}
                                <i class="fa fa-caret-right fa-fw pr10"></i>
                                {__("Import point")}
                            {elseif $sub_view == "comment"}
                                <i class="fa fa-book fa-lg fa-fw pr10"></i>
                                {__("Points")}
                                <i class="fa fa-caret-right fa-fw pr10"></i>
                                {__("Comment")}
                            {/if}
                        {elseif $view == "diarys"}
                            {if $sub_view == ""}
                                <i class="fa fa-image fa-lg fa-fw pr10"></i>
                                {__("Diary corner")}
                                <i class="fa fa-caret-right fa-fw pr10"></i>
                                {__("Lists")}
                            {elseif $sub_view == "add"}
                                <i class="fa fa-image fa-lg fa-fw pr10"></i>
                                {__("Diary corner")}
                                <i class="fa fa-caret-right fa-fw pr10"></i>
                                {__("Add New")}
                            {/if}
                        {elseif $view == "healths"}
                            {if $sub_view == ""}
                                <i class="fa fa-heart fa-lg fa-fw pr10"></i>
                                {__("Health information")}
                                <i class="fa fa-caret-right fa-fw pr10"></i>
                                {__("Lists")}
                            {elseif $sub_view == "add"}
                                <i class="fa fa-heart fa-lg fa-fw pr10"></i>
                                {__("Health information")}
                                <i class="fa fa-caret-right fa-fw pr10"></i>
                                {__("Add New")}
                            {/if}
                        {elseif $view == "children"}
                            <i class="fa fa-child fa-fw fa-lg pr10"></i>
                            {__("Student")}
                            {if $sub_view==""}
                                <i class="fa fa-caret-right fa-fw pr10"></i>
                                {__("Lists")}
                            {elseif $sub_view=="import"}
                                <i class="fa fa-caret-right fa-fw pr10"></i>
                                {__("Import from Excel file")}
                            {elseif $sub_view=="add"}
                                <i class="fa fa-caret-right fa-fw pr10"></i>
                                {__("Add New")}
                            {/if}
                        {/if}
                        <!-- end children -->
                        <!-- Event -->
                        {if $schoolConfig['allow_class_event']}
                            {if $view == "events" && $sub_view == ""}
                                <i class="fa fa-bell fa-lg fa-fw pr10"></i>
                                {__("Notification - Event")}
                                <i class="fa fa-caret-right fa-fw pr10"></i>
                                {__("Lists")}
                            {/if}
                            {*{if $view == "events" && $sub_view == "add"}*}
                            {*<i class="fa fa-bell fa-lg fa-fw pr10"></i> {__("Notification - Event")} <i class="fa fa-caret-right fa-fw pr10"></i>{__("Add New")}*}
                            {*{/if}*}
                        {else}
                            {if $view == "events"}
                                <i class="fa fa-bell fa-lg fa-fw pr10"></i>
                                {__("Notification - Event")}
                            {/if}
                        {/if}
                        <!-- end Event -->

                    </div>
                    <div class="navbar-header">

                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    {if count($schoolList) > 0 || count($classList) > 0 || count($childrenList) > 0}
                        <div class="select_object_box">
                            <select name="object" id="select_object">
                                {if count($schoolList) > 0}
                                    <option disabled>----- {__("School")} -----</option>
                                    {foreach $schoolList as $school}
                                        <option data-type="school" value="{$school['page_name']}"
                                                {if $school['page_name']==$username}selected{/if}>{$school['page_title']}</option>
                                    {/foreach}
                                {/if}
                                {if count($classList) > 0}
                                    <option disabled>----- {__("Class")} -----</option>
                                    {foreach $classList as $class}
                                        <option data-type="class" value="{$class['group_name']}"
                                                {if $class['group_name']==$username}selected{/if}>{$class['group_title']}</option>
                                    {/foreach}a
                                {/if}
                                {if count($childrenList) > 0}
                                    <option disabled>----- {__("Child")} -----</option>
                                    {foreach $childrenList as $childOb}
                                        <option {if $childOb['school_id'] > 0}data-type="child"
                                                value="{$childOb['child_id']}"
                                                {if $childOb['child_id'] == $child['child_id']}selected{/if}
                                                {else}data-type="childinfo"
                                                value="{$childOb['child_parent_id']}" {if $childOb['child_parent_id'] == $child['child_parent_id']}selected{/if}{/if}>{$childOb['child_name']}</option>
                                    {/foreach}
                                {/if}
                            </select>
                        </div>
                    {/if}
                    <div class="panel-body with-nav collapse navbar-collapse" id="collapse">
                        <ul class="side-nav metismenu js_metisMenu nav navbar-nav navbar-right">
                            <!-- Dashboard -->
                            <li {if $view == ""}class="active selected"{/if}>
                                <a href="{$system['system_url']}/class/{$username}">
                                    <i class="fas fa-tachometer-alt fa-fw fa-lg pr10"></i> {__("Dashboard")}
                                </a>
                            </li>
                            <!-- Dashboard -->
                            {*Begin khối thông tin học phí*}
                            <li class="menu_header border_top">
                                <a>{__("Tuition info")}</a>
                            </li>
                            <!-- Attendance -->
                            {if $schoolConfig['attendance']}
                                <li {if $view == "attendance"}class="active"{/if}>
                                    <a href="{$system['system_url']}/class/{$username}/attendance">
                                        <i class="fa fa-tasks fa-fw fa-lg pr10"></i> {__("Attendance")}
                                        <span class="fa arrow"></span>
                                    </a>
                                    <ul>
                                        <li {if $view == "attendance" && $sub_view == ""}class="active selected"{/if}>
                                            <a href="{$system['system_url']}/class/{$username}/attendance">
                                                <i class="fa fa-caret-right fa-fw pr10"></i>{__("Summary")}
                                            </a>
                                        </li>
                                        <li {if $view == "attendance" && $sub_view == "rollup"}class="active selected"{/if}>
                                            <a href="{$system['system_url']}/class/{$username}/attendance/rollup">
                                                <i class="fa fa-caret-right fa-fw pr10"></i>{__("Roll up")}
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            {/if}
                            <!-- Attendance -->
                            <!-- Service -->
                            {if $schoolConfig['services']}
                                <li {if ($view == "useservices") || ($view == "services")}class="active"{/if}>
                                    <a href="{$system['system_url']}/class/{$username}/useservices">
                                        <i class="fa fa-life-ring fa-lg fa-fw pr10"></i> {__("Service")}
                                        <span class="fa arrow"></span>
                                    </a>
                                    <ul>
                                        <li {if $view == "useservices" && $sub_view == "record"}class="active selected"{/if}>
                                            <a href="{$system['system_url']}/class/{$username}/useservices/record">
                                                <i class="fa fa-caret-right fa-fw pr10"></i>{__("Register service")}
                                            </a>
                                        </li>
                                        <li {if $view == "useservices" && $sub_view == "history"}class="active selected"{/if}>
                                            <a href="{$system['system_url']}/class/{$username}/useservices/history">
                                                <i class="fa fa-caret-right fa-fw pr10"></i>{__("Service usage information")}
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            {/if}
                            <!-- Service -->
                            <!-- Fee -->
                            {if $schoolConfig['tuitions']}
                                {if $schoolConfig['allow_class_see_tuition']}
                                    <!-- Fee -->
                                    <li {if ($view == "tuitions") || ($view == "fees")}class="active"{/if}>
                                        <a href="{$system['system_url']}/class/{$username}/tuitions">
                                            <i class="fas fa-money-bill-alt fa-lg fa-fw pr10"></i> {__("Tuition fee")}
                                        </a>
                                    </li>
                                {/if}
                            {/if}
                            <!-- Fee -->

                            <!-- Late Pickup -->
                            {*{if $pickup_is_configured && $pickup_is_teacher}*}
                            {*<li {if $view=="pickup"}class="active"{/if}>*}
                            {*<a href="{$system['system_url']}/class/{$username}/pickup/history">*}
                            {*<i class="fa fa-universal-access fa-lg fa-fw pr10"></i> {__("Late pickup")}*}
                            {*<span class="fa arrow"></span>*}
                            {*</a>*}
                            {*<ul>*}
                            {*{if $pickup_is_assigned}*}
                            {*<li {if $view == "pickup" && $sub_view == "manage"}class="active selected"{/if}>*}
                            {*<a href="{$system['system_url']}/class/{$username}/pickup/manage">*}
                            {*<i class="fa fa-caret-right fa-fw pr10"></i>{__("Manage")}*}
                            {*</a>*}
                            {*</li>*}
                            {*{/if}*}
                            {*<li {if $view == "pickup" && $sub_view == "history"}class="active selected"{/if}>*}
                            {*<a href="{$system['system_url']}/class/{$username}/pickup/history">*}
                            {*<i class="fa fa-caret-right fa-fw pr10"></i>{__("History")}*}
                            {*</a>*}
                            {*</li>*}
                            {*<li {if $view == "pickup" && $sub_view == "assign"}class="active selected"{/if}>*}
                            {*<a href="{$system['system_url']}/class/{$username}/pickup/assign">*}
                            {*<i class="fa fa-caret-right fa-fw pr10"></i>{__("Assign")}*}
                            {*</a>*}
                            {*</li>*}
                            {*</ul>*}
                            {*</li>*}
                            {*{/if}*}
                            <!-- END Late Pickup -->

                            {*End khối thông tin học phí*}
                            {* Begin Khối thông tin cho trẻ*}
                            <li class="menu_header border_top">
                                <a>{__("Information for student")}</a>
                            </li>
                            <!-- Schedule -->
                            {if $schoolConfig['schedules']}
                                <li {if ($view == "schedules")}class="active"{/if}>
                                    <a href="{$system['system_url']}/class/{$username}/schedules">
                                        <i class="fas fa-calendar-alt fa-lg fa-fw pr10"></i> {__("Schedule")}
                                    </a>
                                </li>
                            {/if}
                            <!-- End schedule -->

                            <!-- Menu -->
                            {if $schoolConfig['menus']}
                                <li {if ($view == "menus")}class="active"{/if}>
                                    <a href="{$system['system_url']}/class/{$username}/menus">
                                        <i class="fas fa-utensils fa-lg fa-fw pr10"></i> {__("Menu")}
                                    </a>
                                </li>
                            {/if}
                            <!-- End Menu -->
                            <!-- Contact book -->
                            {if $schoolConfig['reports']}
                                <li {if $view == "reports"}class="active"{/if}>
                                    <a href="{$system['system_url']}/class/{$username}/reports">
                                        <i class="fa fa-book fa-lg fa-fw pr10"></i> {__("Contact book")}
                                        <span class="fa arrow"></span>
                                    </a>
                                    <ul>
                                        <li {if ($view == "reports") && ($sub_view == "")}class="active selected"{/if}>
                                            <a href="{$system['system_url']}/class/{$username}/reports">
                                                <i class="fa fa-caret-right fa-fw pr10"></i>{__("Lists")}
                                            </a>
                                        </li>
                                        <li {if $view == "reports" && $sub_view == "add"}class="active selected"{/if}>
                                            <a href="{$system['system_url']}/class/{$username}/reports/add">
                                                <i class="fa fa-caret-right fa-fw pr10"></i>{__("Add New")}
                                            </a>
                                        </li>
                                        <li {if $view == "reports" && $sub_view == "listtemp"}class="active selected"{/if}>
                                            <a href="{$system['system_url']}/class/{$username}/reports/listtemp">
                                                <i class="fa fa-caret-right fa-fw pr10"></i>{__("List template")}
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            {/if}
                            <!-- End Contact book -->
                            <!-- Points -->
                            {if $school['points']}
                                <li id="li-point" {if $view=="points"}class="active{/if}">
                                    <a href="{$system['system_url']}/class/{$username}/points">
                                        <i class="fa fa-book fa-lg fa-fw pr10"></i> {__("Point")}
                                        <span class="fa arrow"></span>
                                    </a>
                                    <ul>
                                        <li id="li-point-list"
                                            {if ($view=="points") && ($sub_view=="listsbysubject")}class="active selected"{/if}>
                                            <a href="{$system['system_url']}/class/{$username}/points/listsbysubject">
                                                <i class="fa fa-caret-right fa-fw pr10"></i>{__("Lists by subject")}
                                            </a>
                                        </li>
                                        <li id="li-point-list"
                                            {if ($view=="points") && ($sub_view=="listsbystudent")}class="active selected"{/if}>
                                            <a href="{$system['system_url']}/class/{$username}/points/listsbystudent">
                                                <i class="fa fa-caret-right fa-fw pr10"></i>{__("Lists by student")}
                                            </a>
                                        </li>
                                        <li id="li-point-assign"
                                            {if $view=="points" && $sub_view=="importManual"}class="active selected"{/if}>
                                            <a href="{$system['system_url']}/class/{$username}/points/importManual">
                                                <i class="fa fa-caret-right fa-fw pr10"></i>{__("Import point")}
                                            </a>
                                        </li>
                                        <li id="li-point-comment"
                                            {if $view=="points" && $sub_view=="comment"}class="active selected"{/if}>
                                            <a href="{$system['system_url']}/class/{$username}/points/comment">
                                                <i class="fa fa-caret-right fa-fw pr10"></i>{__("Comment")}
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            {/if}
                            <!-- End Points -->
                            <!-- Event -->
                            {if $schoolConfig['events']}
                                {if $schoolConfig['allow_class_event']}
                                    <li {if $view == "events"}class="active"{/if}>
                                        <a href="{$system['system_url']}/class/{$username}/events">
                                            <i class="fa fa-bell fa-lg fa-fw pr10"></i>{__("Notification - Event")}
                                        </a>
                                    </li>
                                {else}
                                    <li {if $view == "events"}class="active"{/if}>
                                        <a href="{$system['system_url']}/class/{$username}/events">
                                            <i class="fa fa-bell fa-lg fa-fw pr10"></i>{__("Notification - Event")}
                                        </a>
                                    </li>
                                {/if}
                            {/if}
                            <!-- Event -->
                            {*End khối thông tin cho trẻ*}
                            {*Begin khối tiện ích nhà trường*}
                            <li class="menu_header border_top">
                                <a>{__("School utility")}</a>
                            </li>
                            {*End khối tiện ích nhà trường*}
                            <!-- Diary -->
                            {if $schoolConfig['diarys']}
                                <li {if $view == "diarys"}class="active"{/if}>
                                    <a href="{$system['system_url']}/class/{$username}/diarys">
                                        <i class="fa fa-image fa-lg fa-fw pr10"></i> {__("Diary corner")}
                                        <span class="fa arrow"></span>
                                    </a>
                                    <ul>
                                        <li {if $view == "diarys" && $sub_view == ""}class="active selected"{/if}>
                                            <a href="{$system['system_url']}/class/{$username}/diarys">
                                                <i class="fa fa-caret-right fa-fw pr10"></i>{__("Diary lists")}
                                            </a>
                                        </li>
                                        <li {if $view == "diarys" && $sub_view == "add"}class="active selected"{/if}>
                                            <a href="{$system['system_url']}/class/{$username}/diarys/add">
                                                <i class="fa fa-caret-right fa-fw pr10"></i>{__("Add New")}
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            {/if}
                            <!-- Diary -->
                            <!-- Health -->
                            {if $schoolConfig['healths']}
                                <li {if $view == "healths"}class="active"{/if}>
                                    <a href="{$system['system_url']}/class/{$username}/healths">
                                        <i class="fa fa-heart fa-lg fa-fw pr10"></i> {__("Health information")}
                                        <span class="fa arrow"></span>
                                    </a>
                                    <ul>
                                        <li {if $view == "healths" && $sub_view == ""}class="active selected"{/if}>
                                            <a href="{$system['system_url']}/class/{$username}/healths">
                                                <i class="fa fa-caret-right fa-fw pr10"></i>{__("Lists")}
                                            </a>
                                        </li>
                                        <li {if $view == "healths" && $sub_view == "add"}class="active selected"{/if}>
                                            <a href="{$system['system_url']}/class/{$username}/healths/add">
                                                <i class="fa fa-caret-right fa-fw pr10"></i>{__("Add New")}
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            {/if}

                            <!-- Health -->
                            <!-- Medicine -->
                            {if $schoolConfig['medicines']}
                                <li {if $view == "medicines"}class="active"{/if}>
                                    <a href="{$system['system_url']}/class/{$username}/medicines">
                                        <i class="fa fa-medkit fa-lg fa-fw pr10"></i> {__("Medicines")}
                                        <span class="fa arrow"></span>
                                    </a>
                                    <ul>
                                        <li {if $view == "medicines" && $sub_view == ""}class="active selected"{/if}>
                                            <a href="{$system['system_url']}/class/{$username}/medicines">
                                                <i class="fa fa-caret-right fa-fw pr10"></i>{__("Today lists")}
                                            </a>
                                        </li>
                                        <li {if $view == "medicines" && $sub_view == "all"}class="active selected"{/if}>
                                            <a href="{$system['system_url']}/class/{$username}/medicines/all">
                                                <i class="fa fa-caret-right fa-fw pr10"></i>{__("List all")}
                                            </a>
                                        </li>
                                        <li {if $view == "medicines" && $sub_view == "add"}class="active selected"{/if}>
                                            <a href="{$system['system_url']}/class/{$username}/medicines/add">
                                                <i class="fa fa-caret-right fa-fw pr10"></i>{__("Add New")}
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            {/if}
                            <!-- Medicine -->

                            <!-- BEGIN - Children -->
                            {if $schoolConfig['children']}
                                <li {if $view == "children"}class="active"{/if}>
                                    <a href="{$system['system_url']}/class/{$username}/children">
                                        <i class="fa fa-child fa-fw fa-lg pr10"></i>{__("Student")}
                                        <span class="fa arrow"></span>
                                    </a>
                                    <ul>
                                        <li {if $view=="children" && $sub_view==""}class="active selected"{/if}>
                                            <a href="{$system['system_url']}/class/{$username}/children">
                                                <i class="fa fa-caret-right fa-fw pr10"></i>{__("Lists")}
                                            </a>
                                        </li>
                                        <li {if $view=="children" && $sub_view=="import"}class="active selected"{/if}>
                                            <a href="{$system['system_url']}/class/{$username}/children/import">
                                                <i class="fa fa-caret-right fa-fw pr10"></i>{__("Import from Excel file")}
                                            </a>
                                        </li>
                                        <li {if $view=="children" && $sub_view=="add"}class="active selected"{/if}>
                                            <a href="{$system['system_url']}/class/{$username}/children/add">
                                                <i class="fa fa-caret-right fa-fw pr10"></i>{__("Add New")}
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            {/if}
                            <!-- END - Children -->
                            <!-- Reports -->
                            <!--
                        <li {if $view == "reports"}class="active selected"{/if}>
                            <a href="{$system['system_url']}/class/{$username}/reports">
                                <i class="fa fa-pie-chart fa-lg pr10"></i> {__("Reports")}
                            </a>
                        </li>
                        -->
                            <!-- END - Reports -->
                            <!-- BEGIN - Feedback -->
                            <!--
                        <li {if $view == "feedback"}class="active"{/if}>
                            <a href="{$system['system_url']}/class/{$username}/feedback">
                                <i class="fa fa-envelope fa-fw fa-lg pr10"></i> {__("Feedback")}
                            </a>
                        </li>
                        -->
                            <!-- END - Feedback -->
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
        {include file='ci/class/class.help.tpl'}
        <div class="{if $view == 'attendance' && $sub_view == ''}col-sm-10 col-md-10 {else} col-sm-9 col-md-9{/if}">
            {if $view == ""}
                {include file='ci/class/class.dashboard.tpl'}
            {else}
                {include file="ci/class/class.$view.tpl"}
            {/if}
        </div>
    </div>
</div>
<!-- page content -->


{include file='_footer.tpl'}