{include file='_head.tpl'}
{include file='_header.tpl'}

<div class="container mt20 height_min"> <!-- class height_min để set height tối thiểu của container -->
    <div class="row">
        <div class="col-md-3 col-sm-3" id = "menu_left">
            <nav class="navbar navbar-default" role="navigation">
                <div class="panel panel-default">
                    <div class = "menu_school">
                        <!-- Dashbroad -->
                        {if $view == ""}
                            <i class="fas fa-tachometer-alt fa-fw fa-lg pr10"></i> {__("Dashboard")} [{$child['child_name']}]
                        {elseif $view == "medicines"}
                            <i class="fa fa-medkit fa-lg fa-fw pr10"></i> {__("Medicines")}
                        {elseif $view == "attendance"}
                            <i class="fa fa-tasks fa-fw fa-lg pr10"></i> {__("Resign")}
                        {elseif $view == "contact"}
                            <i class="fa fa-list fa-lg fa-fw pr10"></i> {__("Contact list")}
                        {elseif ($view == "services")}
                            {if $sub_view == ""}
                                <i class="fa fa-life-ring fa-lg fa-fw pr10"></i> {__('Service usage information')}
                            {elseif $sub_view == "reg"}
                                <i class="fa fa-life-ring fa-lg fa-fw pr10"></i> {__("Register service")}
                            {/if}
                        {elseif ($view == "pickup")}
                            {if $sub_view == ""}
                                <i class="fa fa-universal-access fa-lg fa-fw pr10"></i> {__("Register")}
                            {elseif $sub_view == "list"}
                                <i class="fa fa-universal-access fa-lg fa-fw pr10"></i> {__("Lists")}
                            {/if}
                        {elseif $view == "informations"}
                            <i class="fa fa-info-circle fa-lg fa-fw pr10"></i> {__("Pickup information")}
                        {elseif $view == "schedules"}
                            <i class="fas fa-calendar-alt fa-lg fa-fw pr10"></i> {__("Schedule")}
                        {elseif $view == "menus"}
                            <i class="fas fa-utensils fa-lg fa-fw pr10"></i> {__("Menu")}
                        {elseif $view == "tuitions"}
                            {if $view == "tuitions" && $sub_view == ""}
                                <i class="fa fa-caret-right fa-lg fa-fw pr10"></i> {__("Tuition fee")} <i class="fa fa-fw pr10"></i> {__('Monthly Fee')}
                            {/if}
                        {elseif $view == "events"}
                            <i class="fa fa-bell fa-lg fa-fw pr10"></i> {__("Notification - Event")}
                        {elseif $view == "reports"}
                            <i class="fa fa-envelope fa-lg fa-fw pr10"></i> {__("Contact book")}
                        {elseif $view == "points"}
                            {if $sub_view == "detail"}
                                <i class="fa fa-book fa-lg fa-fw pr10"></i>
                                {__("Points")}
                                <i class="fa fa-caret-right fa-fw pr10"></i>
                                {__("Detail")}
{*                            <i class="fa fa-envelope fa-lg fa-fw pr10"></i> {__("Points")}*}
                            {elseif $sub_view == "comment"}
                                <i class="fa fa-book fa-lg fa-fw pr10"></i>
                                {__("Points")}
                                <i class="fa fa-caret-right fa-fw pr10"></i>
                                {__("Comment")}
                            {/if}
                        {/if}
                    </div>
                    <div class="navbar-header">

                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    {if count($schoolList) > 0 || count($classList) > 0 || count($childrenList) > 0}
                        <div class="select_object_box">
                            <select name="object" id="select_object">
                                {if count($schoolList) > 0}
                                    <option disabled>----- {__("School")} -----</option>
                                    {foreach $schoolList as $school}
                                        <option data-type="school" value="{$school['page_name']}" {if $school['page_name']==$username}selected{/if}>{$school['page_title']}</option>
                                    {/foreach}
                                {/if}
                                {if count($classList) > 0}
                                    <option disabled>-----{__("Class")}-----</option>
                                    {foreach $classList as $class}
                                        <option data-type="class" value="{$class['group_name']}" {if $class['group_name']==$username}selected{/if}>{$class['group_title']}</option>
                                    {/foreach}a
                                {/if}
                                {if count($childrenList) > 0}
                                    <option disabled>-----{__("Child")}-----</option>
                                    {foreach $childrenList as $childOb}
                                        <option {if $childOb['school_id'] > 0}data-type="child" value="{$childOb['child_id']}" {if $childOb['child_id'] == $child['child_id']}selected{/if} {else}data-type="childinfo" value="{$childOb['child_parent_id']}" {if $childOb['child_parent_id'] == $child['child_parent_id']}selected{/if}{/if}>{$childOb['child_name']}</option>
                                    {/foreach}
                                {/if}
                            </select>
                        </div>
                    {/if}
                    <div class="panel-body with-nav collapse navbar-collapse" id="collapse">
                        <ul class="side-nav metismenu js_metisMenu nav navbar-nav navbar-right">
                            <li {if $view == ""}class="active selected"{/if}>
                                <a href="{$system['system_url']}/child/{$child['child_id']}">
                                    <i class="fas fa-tachometer-alt fa-fw fa-lg pr10"></i> {__("Dashboard")}
                                    [{$child['child_name']}]
                                </a>
                            </li>
                            {*Begin khối thông tin học phí*}
                            <li class="menu_header border_top">
                                <a>{__("Tuition info")}</a>
                            </li>
                            {if $school['config']['attendance']}
                                <li {if $view == "attendance"}class="active"{/if}>
                                    <a href="{$system['system_url']}/child/{$child['child_id']}/attendance/resign">
                                        <i class="fa fa-tasks fa-lg fa-fw pr10"></i> {__("Resign")}
                                    </a>
                                </li>
                            {/if}
                            {if $school['config']['services']}
                                <li {if $view == "services"}class="active"{/if}>
                                    <a href="{$system['system_url']}/child/{$child['child_id']}/services">
                                        <i class="fa fa-life-ring fa-lg fa-fw pr10"></i> {__("Service")}
                                        <span class="fa arrow"></span>
                                    </a>
                                    <ul>
                                        <li {if $view == "services" && $sub_view == ""} class="active selected"{/if}>
                                            <a href="{$system['system_url']}/child/{$child['child_id']}/services">
                                                <i class="fa fa-caret-right fa-fw pr10"></i> {__("Service usage information")}
                                            </a>
                                        </li>
                                        {if $school['config']['allow_parent_register_service'] == 1}
                                            <li {if $view == "services" && $sub_view == "reg"} class="active selected" {/if}>
                                                <a href="{$system['system_url']}/child/{$child['child_id']}/services/reg">
                                                    <i class="fa fa-caret-right fa-fw pr10"></i> {__("Register service")}
                                                </a>
                                            </li>
                                        {/if}
                                    </ul>
                                </li>
                            {/if}
                            {if $school['config']['pickup']}
                                {if $pickup_configured}
                                    <li {if $view == "pickup"}class="active"{/if}>
                                        <a href="{$system['system_url']}/child/{$child['child_id']}/pickup">
                                            <i class="fa fa-universal-access fa-lg fa-fw pr10"></i> {__("Đón muộn")}
                                            <span class="fa arrow"></span>
                                        </a>
                                        <ul>
                                            <li {if $view == "pickup" && $sub_view == ""} class="active selected"{/if}>
                                                <a href="{$system['system_url']}/child/{$child['child_id']}/pickup">
                                                    <i class="fa fa-caret-right fa-fw pr10"></i> {__("Information")}
                                                </a>
                                            </li>
                                            <li {if $view == "pickup" && $sub_view == "list"} class="active selected" {/if}>
                                                <a href="{$system['system_url']}/child/{$child['child_id']}/pickup/list">
                                                    <i class="fa fa-caret-right fa-fw pr10"></i> {__("Lists")}
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                {/if}
                            {/if}
                            {if $school['config']['tuitions']}
                                <li {if $view == "tuitions"}class="active"{/if}>
                                    <a href="{$system['system_url']}/child/{$child['child_id']}/tuitions">
                                        <i class="fas fa-money-bill-alt fa-lg fa-fw pr10"></i> {__("Tuition")}
                                    </a>
                                </li>
                            {/if}
                            {*End khối học phí*}
                            {* Begin Khối thông tin cho trẻ*}
                            <li class="menu_header border_top">
                                <a>{__("Information for student")}</a>
                            </li>
                            {if $school['config']['schedules']}
                                <li {if $view == "schedules"}class="active"{/if}>
                                    <a href="{$system['system_url']}/child/{$child['child_id']}/schedules">
                                        <i class="fas fa-calendar-alt fa-lg fa-fw pr10"></i> {__("Schedule")}
                                    </a>
                                </li>
                            {/if}
                            {if $school['config']['menus']}
                                <li {if $view == "menus"}class="active"{/if}>
                                    <a href="{$system['system_url']}/child/{$child['child_id']}/menus">
                                        <i class="fas fa-utensils fa-lg fa-fw pr10"></i> {__("Menu")}
                                    </a>
                                </li>
                            {/if}
                            {if $school['config']['events']}
                                <li {if $view == "events"}class="active"{/if}>
                                    <a href="{$system['system_url']}/child/{$child['child_id']}/events">
                                        <i class="fa fa-bell fa-lg fa-fw pr10"></i> {__("Notification - Event")}
                                    </a>
                                </li>
                            {/if}

                            {if $school['config']['reports']}
                                <li {if $view == "reports"}class="active"{/if}>
                                    <a href="{$system['system_url']}/child/{$child['child_id']}/reports">
                                        <i class="fa fa-book fa-lg fa-fw pr10"></i> {__("Contact book")}
                                    </a>
                                </li>
                            {/if}
                            {if $school['config']['points']}
                                <li {if $view == "points"}class="active"{/if}>
                                    <a href="{$system['system_url']}/child/{$child['child_id']}/points">
                                        <i class="fa fa-book fa-lg fa-fw pr10"></i> {__("Points")}
                                        <span class="fa arrow"></span>
                                    </a>
                                    <ul>
                                        <li id="li-point-comment"
                                            {if $view=="points" && $sub_view=="detail"}class="active selected"{/if}>
                                            <a href="{$system['system_url']}/child/{$child['child_id']}/points/detail">
                                                <i class="fa fa-caret-right fa-fw pr10"></i>{__("Detail")}
                                            </a>
                                        </li>
                                        <li id="li-point-comment"
                                            {if $view=="points" && $sub_view=="comment"}class="active selected"{/if}>
                                            <a href="{$system['system_url']}/child/{$child['child_id']}/points/comment">
                                                <i class="fa fa-caret-right fa-fw pr10"></i>{__("Comment")}
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            {/if}
                            {*End khối thông tin cho trẻ*}
                            {*Begin khối tiện ích nhà trường*}
                            <li class="menu_header border_top">
                                <a>{__("School utility")}</a>
                            </li>
                            {if $school['config']['medicines']}
                                <li {if $view == "medicines"}class="active"{/if}>
                                    <a href="{$system['system_url']}/child/{$child['child_id']}/medicines">
                                        <i class="fa fa-medkit fa-lg fa-fw pr10"></i> {__("Medicines")}
                                    </a>
                                </li>
                            {/if}
                            {if $school['config']['display_children_list'] == 1}
                                <li {if $view == "contact"}class="active"{/if}>
                                    <a href="{$system['system_url']}/child/{$child['child_id']}/contact">
                                        <i class="fa fa-list fa-lg fa-fw pr10"></i> {__("Contact list")}
                                    </a>
                                </li>
                            {/if}
                            {if $school['config']['children']}
                                <li {if $view == "informations"}class="active"{/if}>
                                    <a href="{$system['system_url']}/child/{$child['child_id']}/informations">
                                        <i class="fa fa-info-circle fa-lg fa-fw pr10"></i> {__("Pickup information")}
                                    </a>
                                </li>
                            {/if}
                            {if $school['config']['feedback']}
                                <li id="li-child-feedback" {if $view == "feedback"}class="active"{/if}>
                                    <a href="{$system['system_url']}/child/{$child['child_id']}/feedback">
                                        <i class="fa fa-envelope fa-lg fa-fw pr10"></i> {__("Feedback for the school")}
                                    </a>
                                </li>
                            {/if}
                            {*End khối tiện ích nhà trường*}
                            <li {if $view == "leaveschool"}class="active"{/if}>
                                <a href="{$system['system_url']}/child/{$child['child_id']}/leaveschool">
                                    <i class="fa fa-minus-circle fa-lg fa-fw pr10"></i> {__("Leave school")}
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>

        <div class="col-md-9 col-sm-9">
            {include file='ci/child/child.help.tpl'}
        </div>
        <div class="col-md-9 col-sm-9">
            {if $view == ""}
                {include file='ci/child/child.dashboard.tpl'}
            {else}
                {include file="ci/child/child.$view.tpl"}
            {/if}
        </div>
    </div>
</div>
<!-- page content -->


{include file='_footer.tpl'}