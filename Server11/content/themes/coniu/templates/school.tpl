{include file='_head.tpl'}
{include file='_header.tpl'}
{$schoolUser = getSchool($username)}
<!-- page content -->
<div class="container mt20 height_min" id="container_width"
     {if (($view == 'pickup' && $sub_view == 'assign'))}style="width: 100%!important; padding-left: 50px"{/if}>
    <div class="row">
        {if $school['school_step'] == SCHOOL_STEP_FINISH || $school['grade'] != 0}
            <div class="menu_fixed {if ($status)}hidden{/if}">
                <div class="panel-body with-nav">
                    <ul class="side-nav nav navbar-nav navbar-right">
                        {if $schoolUser['is_teacher'] != 1}
                            <!-- Dashboard -->
                            <li {if $view==""}class="active selected"{/if}>
                                <a href="{$system['system_url']}/school/{$username}">
                                    <i class="fas fa-tachometer-alt fa-fw fa-lg pr10"></i>
                                </a>
                            </li>
                            <!-- Dashboard -->
                        {/if}
                        <!-- Attendance -->
                        {if $school['attendance']}
                            {if canView($username, "attendance")}
                                <li {if $view=="attendance"}class="active selected"{/if}>
                                    <a href="{$system['system_url']}/school/{$username}/attendance">
                                        <i class="fa fa-tasks fa-fw fa-lg pr10"></i>
                                    </a>

                                    <ul>
                                        <li {if $view=="attendance" && $sub_view==""}class="active selected"{/if}>
                                            <a href="{$system['system_url']}/school/{$username}/attendance">
                                                <i class="fa fa-caret-right fa-fw pr10"></i>{__("Whole school")}
                                            </a>
                                        </li>
                                        {if canEdit($username, "attendance")}
                                            <li {if $view=="attendance" && $sub_view=="rollup"}class="active selected"{/if}>
                                                <a href="{$system['system_url']}/school/{$username}/attendance/rollup">
                                                    <i class="fa fa-caret-right fa-fw pr10"></i> {__("Roll up")}
                                                </a>
                                            </li>
                                        {/if}
                                        <li {if $view=="attendance" && $sub_view=="classatt"}class="active selected"{/if}>
                                            <a href="{$system['system_url']}/school/{$username}/attendance/classatt">
                                                <i class="fa fa-caret-right fa-fw pr10"></i> {__("Class attendance summary")}
                                            </a>
                                        </li>
                                        <li {if $view=="attendance" && $sub_view=="child"}class="active selected"{/if}>
                                            <a href="{$system['system_url']}/school/{$username}/attendance/child">
                                                <i class="fa fa-caret-right fa-fw pr10"></i> {__("Student attendance summary")}
                                            </a>
                                        </li>
                                        <li {if $view=="attendance" && $sub_view=="conabs"}class="active selected"{/if}>
                                            <a href="{$system['system_url']}/school/{$username}/attendance/conabs">
                                                <i class="fa fa-caret-right fa-fw pr10"></i> {__("Consecutive absent student")}
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            {/if}
                        {/if}
                        <!-- Attendance -->

                        <!-- Service -->
                        {if $school['services']}
                            {if canView($username, "services")}
                                <li {if ($view=="useservices") || ($view=="services")}class="active"{/if}>
                                    <a href="{$system['system_url']}/school/{$username}/useservices">
                                        <i class="fa fa-life-ring fa-lg fa-fw pr10"></i>
                                    </a>
                                    <ul>
                                        <li {if $view=="services" && $sub_view==""}class="active selected"{/if}>
                                            <a href="{$system['system_url']}/school/{$username}/services">
                                                <i class="fa fa-caret-right fa-fw pr10"></i>{__("Service list")}
                                            </a>
                                        </li>
                                        {if canEdit($username, "services")}
                                            <li {if $view=="useservices" && $sub_view=="reg"}class="active selected"{/if}>
                                                <a href="{$system['system_url']}/school/{$username}/useservices/reg">
                                                    <i class="fa fa-caret-right fa-fw pr10"></i>{__("Register service")}
                                                </a>
                                            </li>
                                        {/if}
                                        <li {if $view=="useservices" && $sub_view=="history"}class="active selected"{/if}>
                                            <a href="{$system['system_url']}/school/{$username}/useservices/history">
                                                <i class="fa fa-caret-right fa-fw pr10"></i>{__("Service usage information")}
                                            </a>
                                        </li>
                                        <li {if $view=="useservices" && $sub_view=="foodsvr"}class="active selected"{/if}>
                                            <a href="{$system['system_url']}/school/{$username}/useservices/foodsvr">
                                                <i class="fa fa-caret-right fa-fw pr10"></i>{__("Daily food service summary")}
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            {/if}
                        {/if}
                        <!-- Service ->

                        <!-- Late Pickup -->
                        {if $school['pickup']}
                            {if canView($username, "pickup")}
                                <li {if $view=="pickup"}class="active"{/if}>
                                    <a href="{$system['system_url']}/school/{$username}/pickup/list">
                                        <i class="fa fa-universal-access fa-lg fa-fw pr10"></i>
                                    </a>
                                    <ul>
                                        {if canEdit($username, "pickup")}
                                            <li {if $view=="pickup" && $sub_view=="template"}class="active selected"{/if}>
                                                <a href="{$system['system_url']}/school/{$username}/pickup/template">
                                                    <i class="fa fa-caret-right fa-fw pr10"></i>{__("Setting and create price table")}
                                                </a>
                                            </li>
                                            {if $pickup_is_configured}
                                                <li {if $view=="pickup" && $sub_view=="assign"}class="active selected"{/if}>
                                                    <a href="{$system['system_url']}/school/{$username}/pickup/assign">
                                                        <i class="fa fa-caret-right fa-fw pr10"></i>{__("Assign teacher")}
                                                    </a>
                                                </li>
                                            {/if}
                                        {/if}
                                        {if $pickup_is_configured}
                                            <li {if $view=="pickup" && $sub_view=="history"}class="active selected"{/if}>
                                                <a href="{$system['system_url']}/school/{$username}/pickup/history">
                                                    <i class="fa fa-caret-right fa-fw pr10"></i>{__("Assignment history")}
                                                </a>
                                            </li>
                                            <li {if $view=="pickup" && $sub_view=="list"}class="active selected"{/if}>
                                                <a href="{$system['system_url']}/school/{$username}/pickup/list">
                                                    <i class="fa fa-caret-right fa-fw pr10"></i>{__("Lists")}
                                                </a>
                                            </li>
                                        {/if}
                                    </ul>
                                </li>
                            {/if}
                        {/if}

                        {if $school['pickupteacher']}
                            {if $pickup_is_configured && $pickup_is_teacher}
                                <li {if $view=="pickupteacher"}class="active"{/if}>
                                    <a href="{$system['system_url']}/school/{$username}/pickupteacher/history">
                                        <i class="fa fa-universal-access fa-lg fa-fw pr10"></i>
                                    </a>
                                    <ul>
                                        {if $pickup_is_assigned}
                                            <li {if $view == "pickup" && $sub_view == "manage"}class="active selected"{/if}>
                                                <a href="{$system['system_url']}/school/{$username}/pickupteacher/manage">
                                                    <i class="fa fa-caret-right fa-fw pr10"></i>{__("Add students - Pickup")}
                                                </a>
                                            </li>
                                        {/if}
                                        <li {if $view == "pickup" && $sub_view == "history"}class="active selected"{/if}>
                                            <a href="{$system['system_url']}/school/{$username}/pickupteacher/history">
                                                <i class="fa fa-caret-right fa-fw pr10"></i>{__("History")}
                                            </a>
                                        </li>
                                        <li {if $view == "pickup" && $sub_view == "assign"}class="active selected"{/if}>
                                            <a href="{$system['system_url']}/school/{$username}/pickupteacher/assign">
                                                <i class="fa fa-caret-right fa-fw pr10"></i>{__("Schedule assignment")}
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            {/if}
                        {/if}
                        <!-- END Late Pickup -->

                        <!-- Fee -->
                        {if $school['tuitions']}
                            {if canView($username, "tuitions")}
                                <li {if ($view=="tuitions") || ($view=="fees")}class="active"{/if}>
                                    <a href="{$system['system_url']}/school/{$username}/tuitions">
                                        <i class="fas fa-money-bill-alt fa-lg fa-fw pr10"></i>
                                    </a>
                                    <ul>
                                        <li {if $view=="tuitions" && $sub_view==""}class="active selected"{/if}>
                                            <a href="{$system['system_url']}/school/{$username}/tuitions">
                                                <i class="fa fa-caret-right fa-fw pr10"></i>{__("Monthly tuition list")}
                                            </a>
                                        </li>
                                        {if canEdit($username, "tuitions")}
                                            <li {if $view=="tuitions" && $sub_view=="add"} class="active selected" {/if}>
                                                <a href="{$system['system_url']}/school/{$username}/tuitions/add">
                                                    <i class="fa fa-caret-right fa-fw pr10"></i>{__("Add monthly tuition")}
                                                </a>
                                            </li>
                                        {/if}
                                        <li {if $view=="fees" && $sub_view==""}class="active selected"{/if}>
                                            <a href="{$system['system_url']}/school/{$username}/fees">
                                                <i class="fa fa-caret-right fa-fw pr10"></i>{__("Fee list")}
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            {/if}
                        {/if}
                        <!-- Fee -->

                        <!-- Event -->
                        {if $school['events']}
                            {*{if canView($username, "events")}*}
                            <li {if $view=="events"}class="active"{/if}>
                                <a href="{$system['system_url']}/school/{$username}/events">
                                    <i class="fa fa-bell fa-lg fa-fw pr10"></i>
                                </a>
                                {if canEdit($username, "events")}
                                    <ul>
                                        <li {if $view=="events" && $sub_view==""}class="active selected"{/if}>
                                            <a href="{$system['system_url']}/school/{$username}/events">
                                                <i class="fa fa-caret-right fa-fw pr10"></i>{__("Lists")}
                                            </a>
                                        </li>
                                        <li {if $view=="events" && $sub_view=="add"}class="active selected"{/if}>
                                            <a href="{$system['system_url']}/school/{$username}/events/add">
                                                <i class="fa fa-caret-right fa-fw pr10"></i>{__("Add New")}
                                            </a>
                                        </li>
                                    </ul>
                                {/if}
                            </li>
                            {*{/if}*}
                        {/if}
                        <!-- Event -->

                        <!-- Schedule -->
                        {if $school['schedules']}
                            {if canView($username, "schedules")}
                                <li {if ($view=="schedules")}class="active"{/if}>
                                    <a href="{$system['system_url']}/school/{$username}/schedules">
                                        <i class="fas fa-calendar-alt fa-lg fa-fw pr10"></i>
                                    </a>
                                    {if canEdit($username, "schedules")}
                                        <ul>
                                            <li {if $view=="schedules" && $sub_view==""}class="active selected"{/if}>
                                                <a href="{$system['system_url']}/school/{$username}/schedules">
                                                    <i class="fa fa-caret-right fa-fw pr10"></i>{__("Schedule list")}
                                                </a>
                                            </li>
                                            <li {if $view=="schedules" && $sub_view=="add"}class="active selected"{/if}>
                                                <a href="{$system['system_url']}/school/{$username}/schedules/add">
                                                    <i class="fa fa-caret-right fa-fw pr10"></i>{__("Add schedule")}
                                                </a>
                                            </li>
                                            <li {if $view=="schedules" && $sub_view=="import"}class="active selected"{/if}>
                                                <a href="{$system['system_url']}/school/{$username}/schedules/import">
                                                    <i class="fa fa-caret-right fa-fw pr10"></i>{__("Import from Excel file")}
                                                </a>
                                            </li>
                                        </ul>
                                    {/if}
                                </li>
                            {/if}
                        {/if}
                        <!-- End schedule -->

                        <!-- MENU -->
                        {if $school['menus']}
                            {if canView($username, "menus")}
                                <li {if ($view=="menus")}class="active"{/if}>
                                    <a href="{$system['system_url']}/school/{$username}/menus">
                                        <i class="fas fa-utensils fa-lg fa-fw pr10"></i>
                                    </a>
                                    {if canEdit($username, "menus")}
                                        <ul>
                                            <li {if $view=="menus" && $sub_view==""}class="active selected"{/if}>
                                                <a href="{$system['system_url']}/school/{$username}/menus">
                                                    <i class="fa fa-caret-right fa-fw pr10"></i>{__("Menu list")}
                                                </a>
                                            </li>
                                            <li {if $view=="menus" && $sub_view=="add"}class="active selected"{/if}>
                                                <a href="{$system['system_url']}/school/{$username}/menus/add">
                                                    <i class="fa fa-caret-right fa-fw pr10"></i>{__("Add menu")}
                                                </a>
                                            </li>
                                        </ul>
                                    {/if}
                                </li>
                            {/if}
                        {/if}
                        <!-- End menu -->

                        <!-- Contact book -->
                        {if $school['reports']}
                            {if canView($username, "reports")}
                                <li {if $view=="reports"}class="active"{/if}>
                                    <a href="{$system['system_url']}/school/{$username}/reports">
                                        <i class="fa fa-book fa-lg fa-fw pr10"></i>
                                    </a>
                                    <ul>
                                        <li {if ($view=="reports") && ($sub_view=="")}class="active selected"{/if}>
                                            <a href="{$system['system_url']}/school/{$username}/reports">
                                                <i class="fa fa-caret-right fa-fw pr10"></i>{__("Lists")}
                                            </a>
                                        </li>
                                        {if canEdit($username, "reports")}
                                            <li {if $view=="reports" && $sub_view=="add"}class="active selected"{/if}>
                                                <a href="{$system['system_url']}/school/{$username}/reports/add">
                                                    <i class="fa fa-caret-right fa-fw pr10"></i>{__("Add New")}
                                                </a>
                                            </li>
                                        {/if}
                                        <li {if $view=="reports" && $sub_view=="listtemp"}class="active selected"{/if}>
                                            <a href="{$system['system_url']}/school/{$username}/reports/listtemp">
                                                <i class="fa fa-caret-right fa-fw pr10"></i>{__("List template")}
                                            </a>
                                        </li>
                                        <li {if $view=="reports" && $sub_view=="listcategory"}class="active selected"{/if}>
                                            <a href="{$system['system_url']}/school/{$username}/reports/listcate">
                                                <i class="fa fa-caret-right fa-fw pr10"></i>{__("List category")}
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            {/if}
                        {/if}
                        <!-- End Contact book -->
                        <!-- Point -->
                        {if $school['points']}
                            {if canView($username, "points")}
                                <li {if $view=="points"}class="active"{/if}>
                                    <a href="{$system['system_url']}/school/{$username}/points">
                                        <i class="fa fa-book fa-lg fa-fw pr10"></i>
                                    </a>
                                    <ul>
                                        <li {if $view=="points" && $sub_view=="listsbysubject"}class="active selected"{/if}>
                                            <a href="{$system['system_url']}/school/{$username}/points/listsbysubject">
                                                <i class="fa fa-caret-right fa-fw pr10"></i>{__("Lists by subject")}
                                            </a>
                                        </li>

                                        <li {if $view=="points" && $sub_view=="listsbystudent"}class="active selected"{/if}>
                                            <a href="{$system['system_url']}/school/{$username}/points/listsbystudent">
                                                <i class="fa fa-caret-right fa-fw pr10"></i>{__("Lists by student")}
                                            </a>
                                        </li>
                                        {if canEdit($username, "points")}
                                            <li {if $view=="points" && $sub_view=="importManual"}class="active selected"{/if}>
                                                <a href="{$system['system_url']}/school/{$username}/points/importManual">
                                                    <i class="fa fa-caret-right fa-fw pr10"></i>{__("Manual Import")}
                                                </a>
                                            </li>
                                            <li {if $view=="points" && $sub_view=="importExcel"}class="active selected"{/if}>
                                                <a href="{$system['system_url']}/school/{$username}/points/importExcel">
                                                    <i class="fa fa-caret-right fa-fw pr10"></i>{__("Import from Excel file")}
                                                </a>
                                            </li>
                                            <li {if $view=="points" && $sub_view=="assign"}class="active selected"{/if}>
                                                <a href="{$system['system_url']}/school/{$username}/points/assign">
                                                    <i class="fa fa-caret-right fa-fw pr10"></i>{__("Manual Import")}
                                                </a>
                                            </li>
                                            <li {if $view=="points" && $sub_view=="comment"}class="active selected"{/if}>
                                                <a href="{$system['system_url']}/school/{$username}/points/comment">
                                                    <i class="fa fa-caret-right fa-fw pr10"></i>{__("comment")}
                                                </a>
                                            </li>
                                        {/if}
                                    </ul>
                                </li>
                            {/if}
                        {/if}
                        <!-- End Point -->
                        <!-- Medicine -->
                        {if $school['medicines']}
                            {if canView($username, "medicines")}
                                <li {if $view=="medicines"}class="active"{/if}>
                                    <a href="{$system['system_url']}/school/{$username}/medicines">
                                        <i class="fa fa-medkit fa-lg fa-fw pr10"></i>
                                    </a>
                                    <ul>
                                        <li {if $view=="medicines" && $sub_view==""}class="active selected"{/if}>
                                            <a href="{$system['system_url']}/school/{$username}/medicines">
                                                <i class="fa fa-caret-right fa-fw pr10"></i>{__("Today lists")}
                                            </a>
                                        </li>
                                        <li {if $view=="medicines" && $sub_view=="all"}class="active selected"{/if}>
                                            <a href="{$system['system_url']}/school/{$username}/medicines/all">
                                                <i class="fa fa-caret-right fa-fw pr10"></i>{__("List all")}
                                            </a>
                                        </li>
                                        {if canEdit($username, "medicines")}
                                            <li {if $view=="medicines" && $sub_view=="add"}class="active selected"{/if}>
                                                <a href="{$system['system_url']}/school/{$username}/medicines/add">
                                                    <i class="fa fa-caret-right fa-fw pr10"></i>{__("Add New")}
                                                </a>
                                            </li>
                                        {/if}
                                    </ul>
                                </li>
                            {/if}
                        {/if}
                        <!-- Medicine -->

                        <!-- Feedback -->
                        {if $school['feedback']}
                            {if canView($username, "feedback")}
                                <li {if $view=="feedback"}class="active selected"{/if}>
                                    <a href="{$system['system_url']}/school/{$username}/feedback">
                                        <i class="fa fa-envelope fa-fw fa-lg pr10"></i>
                                    </a>
                                </li>
                            {/if}
                        {/if}
                        <!-- Feedback -->

                        <!-- Birthday -->
                        {if $chool['birthdays']}
                            {if canView($username, "birthdays")}
                                <li {if $view=="birthdays"}class="active"{/if}>
                                    <a href="{$system['system_url']}/school/{$username}/birthdays">
                                        <i class="fa fa-birthday-cake fa-fw fa-lg pr10"></i>
                                    </a>
                                </li>
                            {/if}
                        {/if}
                        <!-- End Birthday -->

                        <!-- Diary -->
                        {if $school['diarys']}
                            {if canView($username, "diarys")}
                                <li {if $view=="diarys"}class="active"{/if}>
                                    <a href="{$system['system_url']}/school/{$username}/diarys">
                                        <i class="fa fa-medkit fa-lg fa-fw pr10"></i>
                                    </a>
                                    <ul>
                                        <li {if $view=="diarys" && $sub_view==""}class="active selected"{/if}>
                                            <a href="{$system['system_url']}/school/{$username}/diarys">
                                                <i class="fa fa-caret-right fa-fw pr10"></i>{__("Diary corner")}
                                            </a>
                                        </li>
                                        {if canEdit($username, "diarys")}
                                            <li {if $view=="diarys" && $sub_view=="add"}class="active selected"{/if}>
                                                <a href="{$system['system_url']}/school/{$username}/diarys/add">
                                                    <i class="fa fa-caret-right fa-fw pr10"></i>{__("Add New")}
                                                </a>
                                            </li>
                                        {/if}
                                    </ul>
                                </li>
                            {/if}
                        {/if}
                        <!-- End Diary -->

                        <!-- Health -->
                        {if $school['healths']}
                            {if canView($username, "healths")}
                                <li {if $view=="healths"}class="active"{/if}>
                                    <a href="{$system['system_url']}/school/{$username}/healths">
                                        <i class="fa fa-medkit fa-lg fa-fw pr10"></i>
                                    </a>
                                    <ul>
                                        <li {if $view=="healths" && $sub_view==""}class="active selected"{/if}>
                                            <a href="{$system['system_url']}/school/{$username}/healths">
                                                <i class="fa fa-caret-right fa-fw pr10"></i>{__("Health information")}
                                            </a>
                                        </li>
                                        {if canEdit($username, "healths")}
                                            <li {if $view=="healths" && $sub_view=="add"}class="active selected"{/if}>
                                                <a href="{$system['system_url']}/school/{$username}/healths/add">
                                                    <i class="fa fa-caret-right fa-fw pr10"></i>{__("Add New")}
                                                </a>
                                            </li>
                                        {/if}
                                    </ul>
                                </li>
                            {/if}
                        {/if}
                        <!-- End Healths -->

                        <!-- Classes -->
                        {if $school['classes'] || $school['classlevels']}
                            {if canView($username, "classes") || canView($username, "classlevels")}
                                <li {if ($view=="classes" || $view=="classlevels")}class="active"{/if}>
                                    <a href="{$system['system_url']}/school/{$username}/classes">
                                        <i class="fa fa-graduation-cap fa-fw fa-lg pr10"></i>
                                    </a>
                                    {if canView($username, "classes") || canView($username, "classlevels")}
                                        <ul>
                                            {if $school['classlevels']}
                                                {if canView($username, "classlevels")}
                                                    <li {if $view=="classlevels"}class="active selected"{/if}>
                                                        <a href="{$system['system_url']}/school/{$username}/classlevels">
                                                            <i class="fa fa-caret-right fa-fw pr10"></i>{__("Class level")}
                                                        </a>
                                                    </li>
                                                {/if}
                                            {/if}
                                            {if $school['classes']}
                                                {if canView($username, "classes")}
                                                    <li {if $view=="classes" && $sub_view==""}class="active selected"{/if}>
                                                        <a href="{$system['system_url']}/school/{$username}/classes">
                                                            <i class="fa fa-caret-right fa-fw pr10"></i>{__("Class lists")}
                                                        </a>
                                                    </li>
                                                {/if}
                                                {if canEdit($username, "classes")}
                                                    <li {if ($view=="classes") && ($sub_view=="add")}class="active selected"{/if}>
                                                        <a href="{$system['system_url']}/school/{$username}/classes/add">
                                                            <i class="fa fa-caret-right fa-fw pr10"></i>{__("Add new class")}
                                                        </a>
                                                    </li>
                                                {/if}
                                            {/if}
                                        </ul>
                                    {/if}
                                </li>
                            {/if}
                        {/if}
                        <!-- Classes -->
                        <!-- Subject -->
                        {if $school['subjects']}
                            {if canView($username, "subjects")}
                                <li {if $view=="subjects"}class="active"{/if}>
                                    <a href="{$system['system_url']}/school/{$username}/subjects">
                                        <i class="fa fa-book fa-lg fa-fw pr10"></i>
                                    </a>
                                    <ul>
                                        <li {if $view=="subjects" && $sub_view==""}class="active selected"{/if}>
                                            <a href="{$system['system_url']}/school/{$username}/subjects">
                                                <i class="fa fa-caret-right fa-fw pr10"></i>{__("Lists")}
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            {/if}
                        {/if}
                        <!-- End Subject -->

                        <!-- Teachers/Employee -->
                        {if $school['teachers']}
                            {if canView($username, "teachers")}
                                <li {if $view=="teachers"}class="active"{/if}>
                                    <a href="{$system['system_url']}/school/{$username}/teachers">
                                        <i class="fa fa-street-view fa-fw fa-lg pr10"></i>
                                    </a>
                                    {if canEdit($username, "teachers")}
                                        <ul>
                                            <li {if $view=="teachers" && $sub_view==""}class="active selected"{/if}>
                                                <a href="{$system['system_url']}/school/{$username}/teachers">
                                                    <i class="fa fa-caret-right fa-fw pr10"></i>{__("Lists")}
                                                </a>
                                            </li>
                                            <li {if $view=="teachers" && $sub_view=="addexisting"}class="active selected"{/if}>
                                                <a href="{$system['system_url']}/school/{$username}/teachers/addexisting">
                                                    <i class="fa fa-caret-right fa-fw pr10"></i>{__("Add Existing Account")}
                                                </a>
                                            </li>
                                            <li {if $view=="teachers" && $sub_view=="add"}class="active selected"{/if}>
                                                <a href="{$system['system_url']}/school/{$username}/teachers/add">
                                                    <i class="fa fa-caret-right fa-fw pr10"></i>{__("Add New")}
                                                </a>
                                            </li>
                                        </ul>
                                    {/if}
                                </li>
                            {/if}
                        {/if}
                        <!-- Teachers -->

                        <!-- BEGIN - Children -->
                        {if $school['children']}
                            {if canView($username, "children")}
                                <li {if $view=="children"}class="active"{/if}>
                                    <a href="{$system['system_url']}/school/{$username}/children">
                                        <i class="fa fa-child fa-fw fa-lg pr10"></i>
                                    </a>
                                    {if canEdit($username, "children")}
                                        <ul>
                                            <li {if $view=="children" && $sub_view==""}class="active selected"{/if}>
                                                <a href="{$system['system_url']}/school/{$username}/children">
                                                    <i class="fa fa-caret-right fa-fw pr10"></i>{__("Lists")}
                                                </a>
                                            </li>
                                            <li {if $view=="children" && $sub_view==""}class="active selected"{/if}>
                                                <a href="{$system['system_url']}/school/{$username}/children/listchildnew">
                                                    <i class="fa fa-caret-right fa-fw pr10"></i>{__("New student study begin in month")}
                                                </a>
                                            </li>
                                            <li {if $view=="children" && $sub_view=="import"}class="active selected"{/if}>
                                                <a href="{$system['system_url']}/school/{$username}/children/import">
                                                    <i class="fa fa-caret-right fa-fw pr10"></i>{__("Import from Excel file")}
                                                </a>
                                            </li>
                                            <li {if $view=="children" && $sub_view=="add"}class="active selected"{/if}>
                                                <a href="{$system['system_url']}/school/{$username}/children/add">
                                                    <i class="fa fa-caret-right fa-fw pr10"></i>{__("Add New")}
                                                </a>
                                            </li>
                                        </ul>
                                    {/if}
                                </li>
                            {/if}
                        {/if}
                        <!-- END - Children -->

                        <!-- Permission -->
                        {if $school['roles']}
                            {if canView($username, "roles")}
                                <li {if $view=="roles"}class="active"{/if}>
                                    <a href="{$system['system_url']}/school/{$username}/roles">
                                        <i class="fa fa-unlock-alt fa-fw fa-lg pr10"></i>
                                    </a>
                                </li>
                            {/if}
                        {/if}
                        <!-- End permission -->

                        <!-- Settings -->
                        {if $school['settings']}
                            {if canView($username, "settings")}
                                <li {if $view=="settings"}class="active selected"{/if}>
                                    <a href="{$system['system_url']}/school/{$username}/settings">
                                        <i class="fa fa-cog fa-fw fa-lg pr10"></i>
                                    </a>
                                </li>
                            {/if}
                        {/if}
                        <!-- End Setting -->

                        <!-- Ẩn/ hiển thị menu -->
                        <li>
                            <input type="hidden" id="full_screen"
                                   value="{if $view == 'attendance' && $sub_view == 'classatt'}1{else} 0{/if}">
                            <a href="#" class="js_school_hidden" data-username="{$username}"> <i
                                        class="fa fa-bars fa-fw pr10 fa-lg"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="{if ($view == "pickup" && $sub_view == 'assign')}col-md-2 col-sm-2 {else}col-md-3 col-sm-3{/if} {if !$status}hidden{/if}"
                 id="menu_left">
                <div class="js_scroller" data-slimscroll-height="100%">
                    <nav class="navbar navbar-default" role="navigation">
                        <div class="panel panel-default">
                            <div class="menu_school">
                                {if $view==""}
                                    <i class="fas fa-tachometer-alt fa-fw fa-lg pr10"></i>
                                    {__("Dashboard")}
                                {elseif $view=="attendance"}
                                    <i class="fa fa-tasks fa-fw fa-lg pr10"></i>
                                    {__("Attendance")}
                                    {if $sub_view==""}
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        {__("Whole school")}
                                    {elseif $sub_view=="rollup"}
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        {__("Roll up")}
                                    {elseif $sub_view=="classatt"}
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        {__("Class attendance summary")}
                                    {elseif $sub_view=="child"}
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        {__("Student attendance summary")}
                                    {elseif $sub_view=="conabs"}
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        {__("Consecutive absent student")}
                                    {/if}>
                                {elseif $view=="medicines"}
                                    <i class="fa fa-medkit fa-lg fa-fw pr10"></i>
                                    {__("Medicines")}
                                    {if $sub_view==""}
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        {__("Today lists")}
                                    {elseif $sub_view=="all"}
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        {__("List all")}
                                    {elseif $sub_view=="add"}
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        {__("Add New")}
                                    {/if}
                                {elseif $view=="diarys"}
                                    <i class="fa fa-medkit fa-lg fa-fw pr10"></i>
                                    {__("Diary corner")}
                                    {if $sub_view==""}
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        {__("List all")}
                                    {elseif $sub_view=="add"}
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        {__("Add New")}
                                    {/if}
                                {elseif ($view=="useservices") || ($view=="services")}
                                    <i class="fa fa-life-ring fa-lg fa-fw pr10"></i>
                                    {__("Service")}
                                    {if $sub_view==""}
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        {__("Service list")}
                                    {elseif $sub_view=="history"}
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        {__("Service usage information")}
                                    {elseif $sub_view=="reg"}
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        {__("Register service")}
                                    {elseif $sub_view=="foodsvr"}
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        {__("Daily food service summary")}
                                    {/if}
                                {elseif ($view=="tuitions") || ($view=="fees")}
                                    <i class="fas fa-money-bill-alt fa-lg fa-fw pr10"></i>
                                    {__("Tuition fee")}
                                    {if $view=="tuitions" && $sub_view==""}
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        {__("Monthly tuition list")}
                                    {elseif $view=="tuitions" && $sub_view=="add"}
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        {__("Add monthly tuition")}
                                    {elseif $view=="fees" && $sub_view==""}
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        {__("Fee list")}
                                    {/if}
                                {elseif ($view=="schedules")}
                                    <i class="fas fa-calendar-alt fa-lg fa-fw pr10"></i>
                                    {__("Schedule")}
                                    {if $sub_view==""}
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        {__("Schedule list")}
                                    {elseif $sub_view=="add"}
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        {__("Add schedule")}
                                    {elseif $sub_view=="import"}
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        {__("Import from Excel file")}
                                    {/if}
                                {elseif ($view=="menus")}
                                    <i class="fas fa-utensils fa-lg fa-fw pr10"></i>
                                    {__("Menu")}
                                    {if $sub_view==""}
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        {__("Menu list")}
                                    {elseif $sub_view=="add"}
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        {__("Add menu")}
                                    {/if}
                                    <!-- Late pickup-->
                                {elseif ($view=="pickup")}
                                    {if $sub_view=="template"}
                                        <i class="fa fa-universal-access fa-lg fa-fw pr10"></i>
                                        {__("Late pickup")}
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        {__("Setting and create price table")}
                                    {elseif $sub_view=="assign"}
                                        <i class="fa fa-universal-access fa-lg fa-fw pr10"></i>
                                        {__("Late pickup")}
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        {__("Assign teacher")}
                                    {elseif $sub_view=="list"}
                                        <i class="fa fa-universal-access fa-lg fa-fw pr10"></i>
                                        {__("Late pickup")}
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        {__("Lists")}
                                    {/if}

                                {elseif ($view=="pickupteacher")}
                                    {if $sub_view=="history"}
                                        <i class="fa fa-universal-access fa-lg fa-fw pr10"></i>
                                        {__("Late pickup")}
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        {__("History")}
                                    {elseif $sub_view=="assign"}
                                        <i class="fa fa-universal-access fa-lg fa-fw pr10"></i>
                                        {__("Late pickup")}
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        {__("Schedule assignment")}
                                    {elseif $sub_view=="manage"}
                                        <i class="fa fa-universal-access fa-lg fa-fw pr10"></i>
                                        {__("Late pickup")}
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        {__("Add students - Pickup")}
                                    {/if}

                                    <!-- END Late pickup-->
                                {elseif $view=="events"}
                                    <i class="fa fa-bell fa-lg fa-fw pr10"></i>
                                    {__("Notification - Event")}
                                    {if $sub_view==""}
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        {__("Lists")}
                                    {elseif $sub_view=="add"}
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        {__("Add New")}
                                    {/if}
                                {elseif $view=="children"}
                                    <i class="fa fa-child fa-fw fa-lg pr10"></i>
                                    {__("Student")}
                                    {if $sub_view==""}
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        {__("Lists")}
                                    {elseif $sub_view=="listchildnew"}
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        {__("New student study begin in month")}
                                    {elseif $sub_view=="import"}
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        {__("Import from Excel file")}
                                    {elseif $sub_view=="add"}
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        {__("Add New")}
                                    {/if}
                                {elseif $view=="classes"}
                                    <i class="fa fa-graduation-cap fa-fw fa-lg pr10"></i>
                                    {__("Class")}
                                    {if $sub_view==""}
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        {__("Lists")}
                                    {elseif $sub_view=="add"}
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        {__("Add New")}
                                    {/if}
                                    <!-- Contact book -->
                                {elseif $view=="reports"}
                                    {if $view=="reports" && $sub_view==""}
                                        <i class="fa fa-book fa-lg fa-fw pr10"></i>
                                        {__("Contact book")}
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        {__("Lists")}
                                    {/if}
                                    {if $view=="reports" && $sub_view=="add"}
                                        <i class="fa fa-book fa-lg fa-fw pr10"></i>
                                        {__("Contact book")}
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        {__("Add New")}
                                    {/if}
                                    {if $view=="reports" && $sub_view=="listtemp"}
                                        <i class="fa fa-book fa-lg fa-fw pr10"></i>
                                        {__("Contact book")}
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        {__("List template")}
                                    {/if}
                                    {if $view=="reports" && $sub_view=="listcate"}
                                        <i class="fa fa-book fa-lg fa-fw pr10"></i>
                                        {__("Contact book")}
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        {__("List category")}
                                    {/if}
                                    <!-- end Contact book -->
                                    <!-- Point -->
                                {elseif $view=="points"}
                                    {if $view=="points" && $sub_view=="listsbysubject"}
                                        <i class="fa fa-book fa-lg fa-fw pr10"></i>
                                        {__("Points")}
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        {__("Lists by subject")}
                                    {/if}
                                    {if $view=="points" && $sub_view=="listsbystudent"}
                                        <i class="fa fa-book fa-lg fa-fw pr10"></i>
                                        {__("Points")}
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        {__("Lists by student")}
                                    {/if}
                                    {if $view=="points" && $sub_view=="importManual"}
                                        <i class="fa fa-book fa-lg fa-fw pr10"></i>
                                        {__("Points")}
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        {__("Manual Import")}
                                    {/if}
                                    {if $view=="points" && $sub_view=="importExcel"}
                                        <i class="fa fa-book fa-lg fa-fw pr10"></i>
                                        {__("Points")}
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        {__("Import from Excel file")}
                                    {/if}
                                    {if $view=="points" && $sub_view=="assign"}
                                        <i class="fa fa-book fa-lg fa-fw pr10"></i>
                                        {__("Points")}
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        {__("Assign teacher")}
                                    {/if}
                                    {if $view=="points" && $sub_view=="comment"}
                                        <i class="fa fa-book fa-lg fa-fw pr10"></i>
                                        {__("Points")}
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        {__("Comment")}
                                    {/if}
                                    <!-- end Point -->
                                {elseif $view=="teachers"}
                                    <i class="fa fa-street-view fa-fw fa-lg pr10"></i>
                                    {__("Teacher")}/{__("Employee")}
                                    {if $sub_view==""}
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        {__("Lists")}
                                    {elseif $sub_view=="addexisting"}
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        {__("Add Existing Account")}
                                    {elseif $sub_view=="add"}
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        {__("Add New")}
                                    {/if}
                                {elseif $view=="classlevels"}
                                    <i class="fa fa-tree fa-fw fa-lg pr10"></i>
                                    {__("Class level")}
                                {elseif $view=="birthdays"}
                                    <i class="fa fa-birthday-cake fa-fw fa-lg pr10"></i>
                                    {__("Birthdate")}
                                    {if $sub_view==""}
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        {__("Child")}
                                    {elseif $sub_view=="parent"}
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        {__("Parent")}
                                    {elseif $sub_view=="teacher"}
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        {__("Teacher - Employee")}
                                    {/if}
                                {elseif $view=="diarys"}
                                    <i class="fa fa-image fa-fw fa-lg pr10"></i>
                                    {__("Journal corner")}
                                    {if $sub_view==""}
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        {__("Lists")}
                                    {elseif $sub_view=="add"}
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        {__("Add New")}
                                    {/if}
                                {elseif $view=="healths"}
                                    <i class="fa fa-heart fa-fw fa-lg pr10"></i>
                                    {__("Health information")}
                                    {if $sub_view==""}
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        {__("Lists")}
                                    {elseif $sub_view=="add"}
                                        <i class="fa fa-caret-right fa-fw pr10"></i>
                                        {__("Add New")}
                                    {/if}
                                {elseif $view=="settings"}
                                    <i class="fa fa-cog fa-fw fa-lg pr10"></i>
                                    {__("Settings")}
                                {elseif $view=="roles"}
                                    <i class="fa fa-cog fa-fw fa-lg pr10"></i>
                                    {__('Permission')}
                                {elseif $view=="feedback"}
                                    <i class="fa fa-envelope fa-fw fa-lg pr10"></i>
                                    {__("Parent feedback")}
                                {elseif $view=="points"}
                                    <i class="fa fa-envelope fa-fw fa-lg pr10"></i>
                                    {__("Point")}
                                {/if}
                                <!-- end Feedback -->

                            </div>
                            <div class="navbar-header">

                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                        data-target="#collapse">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            {if count($schoolList) > 0 || count($classList) > 0 || count($childrenList) > 0}
                                <div class="select_object_box">
                                    <select name="object" id="select_object">
                                        {if count($schoolList) > 0}
                                            <option disabled>----- {__("School")} -----</option>
                                            {foreach $schoolList as $school}
                                                <option data-type="school" value="{$school['page_name']}"
                                                        {if $school['page_name']==$username}selected
                                                        style="background: #3aff77"{/if}>{$school['page_title']}</option>
                                            {/foreach}
                                        {/if}
                                        {if count($classList) > 0}
                                            <option disabled>-----{__("Class")}-----</option>
                                            {foreach $classList as $class}
                                                <option data-type="class" value="{$class['group_name']}"
                                                        {if $class['group_name']==$username}selected{/if}>{$class['group_title']}</option>
                                            {/foreach}a
                                        {/if}
                                        {if count($childrenList) > 0}
                                            <option disabled>-----{__("Child")}-----</option>
                                            {foreach $childrenList as $childOb}
                                                <option {if $childOb['school_id'] > 0}data-type="child"
                                                        value="{$childOb['child_id']}"
                                                        {if $childOb['child_id'] == $child['child_id']}selected{/if}
                                                        {else}data-type="childinfo"
                                                        value="{$childOb['child_parent_id']}" {if $childOb['child_parent_id'] == $child['child_parent_id']}selected{/if}{/if}>{$childOb['child_name']}</option>
                                            {/foreach}
                                        {/if}
                                    </select>
                                </div>
                            {/if}
                            <div class="panel-body with-nav collapse navbar-collapse" id="collapse">
                                <ul class="side-nav metismenu js_metisMenu nav navbar-nav navbar-right">
                                    {if $schoolUser['is_teacher'] != 1}
                                        <!-- Dashboard -->
                                        <li class="{if $view==""}active selected{/if}">
                                            <a href="{$system['system_url']}/school/{$username}">
                                                <i class="fas fa-tachometer-alt fa-fw fa-lg pr10"></i> {__("Dashboard")}
                                            </a>
                                        </li>
                                        <!-- Dashboard -->
                                    {/if}
                                    {* Begin khối thông tin học phí*}
                                    {if $school['attendance'] || $school['services'] || $school['pickup'] || $school['tuitions']}
                                        {if canView($username, "attendance") || canView($username, "services") || canView($username, "pickup") || canView($username, "tuitions")
                                        || ($pickup_is_configured && $pickup_is_teacher)}
                                            <li class="menu_header border_top">
                                                <a>{__("Tuition info")}</a>
                                            </li>
                                        {/if}
                                    {/if}
                                    <!-- Attendance -->
                                    {if $school['attendance']}
                                        {if canView($username, "attendance")}
                                            <li {if $view=="attendance"}class="active"{/if}>
                                                <a href="{$system['system_url']}/school/{$username}/attendance">
                                                    <i class="fa fa-tasks fa-fw fa-lg pr10"></i> {__("Attendance")}
                                                    <span class="fa arrow"></span>
                                                </a>
                                                <ul>
                                                    <li {if $view=="attendance" && $sub_view==""}class="active selected"{/if}>
                                                        <a href="{$system['system_url']}/school/{$username}/attendance">
                                                            <i class="fa fa-caret-right fa-fw pr10"></i>{__("Whole school")}
                                                        </a>
                                                    </li>
                                                    {if canEdit($username, "attendance")}
                                                        <li {if $view=="attendance" && $sub_view=="rollup"}class="active selected"{/if}>
                                                            <a href="{$system['system_url']}/school/{$username}/attendance/rollup">
                                                                <i class="fa fa-caret-right fa-fw pr10"></i> {__("Roll up")}
                                                            </a>
                                                        </li>
                                                    {/if}
                                                    <li {if $view=="attendance" && $sub_view=="classatt"}class="active selected"{/if}>
                                                        <a href="{$system['system_url']}/school/{$username}/attendance/classatt">
                                                            <i class="fa fa-caret-right fa-fw pr10"></i> {__("Class attendance summary")}
                                                        </a>
                                                    </li>
                                                    <li {if $view=="attendance" && $sub_view=="child"}class="active selected"{/if}>
                                                        <a href="{$system['system_url']}/school/{$username}/attendance/child">
                                                            <i class="fa fa-caret-right fa-fw pr10"></i> {__("Student attendance summary")}
                                                        </a>
                                                    </li>
                                                    <li {if $view=="attendance" && $sub_view=="conabs"}class="active selected"{/if}>
                                                        <a href="{$system['system_url']}/school/{$username}/attendance/conabs">
                                                            <i class="fa fa-caret-right fa-fw pr10"></i> {__("Consecutive absent student")}
                                                        </a>
                                                    </li>
                                                </ul>
                                            </li>
                                        {/if}
                                    {/if}
                                    <!-- Attendance -->

                                    <!-- Service -->
                                    {if $school['services']}
                                        {if canView($username, "services")}
                                            <li {if ($view=="useservices") || ($view=="services")}class="active"{/if}>
                                                <a href="{$system['system_url']}/school/{$username}/services">
                                                    <i class="fa fa-life-ring fa-lg fa-fw pr10"></i> {__("Service")}
                                                    <span class="fa arrow"></span>
                                                </a>
                                                <ul>
                                                    <li {if $view=="services" && $sub_view==""}class="active selected"{/if}>
                                                        <a href="{$system['system_url']}/school/{$username}/services">
                                                            <i class="fa fa-caret-right fa-fw pr10"></i>{__("Service list")}
                                                        </a>
                                                    </li>
                                                    {if canEdit($username, "services")}
                                                        <li {if $view=="useservices" && $sub_view=="reg"}class="active selected"{/if}>
                                                            <a href="{$system['system_url']}/school/{$username}/useservices/reg">
                                                                <i class="fa fa-caret-right fa-fw pr10"></i>{__("Register service")}
                                                            </a>
                                                        </li>
                                                    {/if}
                                                    <li {if $view=="useservices" && $sub_view=="history"}class="active selected"{/if}>
                                                        <a href="{$system['system_url']}/school/{$username}/useservices/history">
                                                            <i class="fa fa-caret-right fa-fw pr10"></i>{__("Service usage information")}
                                                        </a>
                                                    </li>
                                                    <li {if $view=="useservices" && $sub_view=="foodsvr"}class="active selected"{/if}>
                                                        <a href="{$system['system_url']}/school/{$username}/useservices/foodsvr">
                                                            <i class="fa fa-caret-right fa-fw pr10"></i>{__("Daily food service summary")}
                                                        </a>
                                                    </li>
                                                </ul>
                                            </li>
                                        {/if}
                                    {/if}
                                    <!-- Service -->

                                    <!-- Late Pickup -->
                                    {if $school['pickup']}
                                        {if canView($username, "pickup")}
                                            <li {if $view=="pickup"}class="active"{/if}>
                                                <a href="{$system['system_url']}/school/{$username}/pickup/list">
                                                    <i class="fa fa-universal-access fa-lg fa-fw pr10"></i> {__("Late pickup")}
                                                    - {__("Manage")}
                                                    <span class="fa arrow"></span>
                                                </a>
                                                <ul>
                                                    {if canEdit($username, "pickup")}
                                                        <li {if $view=="pickup" && $sub_view=="template"}class="active selected"{/if}>
                                                            <a href="{$system['system_url']}/school/{$username}/pickup/template">
                                                                <i class="fa fa-caret-right fa-fw pr10"></i>{__("Setting and create price table")}
                                                            </a>
                                                        </li>
                                                        {if $pickup_is_configured}
                                                            <li {if $view=="pickup" && $sub_view=="assign"}class="active selected"{/if}>
                                                                <a href="{$system['system_url']}/school/{$username}/pickup/assign">
                                                                    <i class="fa fa-caret-right fa-fw pr10"></i>{__("Assign teacher")}
                                                                </a>
                                                            </li>
                                                        {/if}
                                                    {/if}
                                                    {if $pickup_is_configured}
                                                        <li {if $view=="pickup" && $sub_view=="list"}class="active selected"{/if}>
                                                            <a href="{$system['system_url']}/school/{$username}/pickup/list">
                                                                <i class="fa fa-caret-right fa-fw pr10"></i>{__("Lists")}
                                                            </a>
                                                        </li>
                                                    {/if}
                                                </ul>
                                            </li>
                                        {/if}
                                    {/if}

                                    {if $school['pickupteacher']}
                                        {if $pickup_is_configured && $pickup_is_teacher}
                                            <li {if $view=="pickupteacher"}class="active"{/if}>
                                                <a href="{$system['system_url']}/school/{$username}/pickupteacher/history">
                                                    <i class="fa fa-universal-access fa-lg fa-fw pr10"></i> {__("Late pickup")}
                                                    - {__("Teacher")}
                                                    <span class="fa arrow"></span>
                                                </a>
                                                <ul>
                                                    {if $pickup_is_assigned}
                                                        <li {if $view == "pickupteacher" && $sub_view == "manage"}class="active selected"{/if}>
                                                            <a href="{$system['system_url']}/school/{$username}/pickupteacher/manage">
                                                                <i class="fa fa-caret-right fa-fw pr10"></i>{__("Add students - Pickup")}
                                                            </a>
                                                        </li>
                                                    {/if}
                                                    <li {if $view == "pickupteacher" && $sub_view == "history"}class="active selected"{/if}>
                                                        <a href="{$system['system_url']}/school/{$username}/pickupteacher/history">
                                                            <i class="fa fa-caret-right fa-fw pr10"></i>{__("History")}
                                                        </a>
                                                    </li>
                                                    <li {if $view == "pickupteacher" && $sub_view == "assign"}class="active selected"{/if}>
                                                        <a href="{$system['system_url']}/school/{$username}/pickupteacher/assign">
                                                            <i class="fa fa-caret-right fa-fw pr10"></i>{__("Schedule assignment")}
                                                        </a>
                                                    </li>
                                                </ul>
                                            </li>
                                        {/if}
                                    {/if}
                                    <!-- END Late Pickup -->

                                    <!-- Fee -->
                                    {if $school['tuitions']}
                                        {if canView($username, "tuitions")}
                                            <li class="{if ($view=="tuitions") || ($view=="fees")}active{/if}">
                                                <a href="{$system['system_url']}/school/{$username}/tuitions">
                                                    <i class="fas fa-money-bill-alt fa-lg fa-fw pr10"></i> {__("Tuition fee")}
                                                    <span class="fa arrow"></span>
                                                </a>
                                                <ul>
                                                    <li {if $view=="tuitions" && $sub_view==""}class="active selected"{/if}>
                                                        <a href="{$system['system_url']}/school/{$username}/tuitions">
                                                            <i class="fa fa-caret-right fa-fw pr10"></i>{__("Monthly tuition list")}
                                                        </a>
                                                    </li>
                                                    {if canEdit($username, "tuitions")}
                                                        <li {if $view=="tuitions" && $sub_view=="add"} class="active selected" {/if}>
                                                            <a href="{$system['system_url']}/school/{$username}/tuitions/add">
                                                                <i class="fa fa-caret-right fa-fw pr10"></i>{__("Add monthly tuition")}
                                                            </a>
                                                        </li>
                                                    {/if}
                                                    <li {if $view=="fees" && $sub_view==""}class="active selected"{/if}>
                                                        <a href="{$system['system_url']}/school/{$username}/fees">
                                                            <i class="fa fa-caret-right fa-fw pr10"></i>{__("Fee list")}
                                                        </a>
                                                    </li>
                                                </ul>
                                            </li>
                                        {/if}
                                    {/if}
                                    <!-- Fee -->
                                    {*End khối thông tin học phí*}

                                    {* Begin Khối thông tin cho trẻ*}
                                    <li class="menu_header border_top">
                                        <a>{__("Information for student")}</a>
                                    </li>
                                    <!-- Event -->
                                    {if $school['events']}
                                        <li {if $view=="events"}class="active"{/if}>
                                            <a href="{$system['system_url']}/school/{$username}/events">
                                                <i class="fa fa-bell fa-lg fa-fw pr10"></i> {__("Notification - Event")}
                                                {if canEdit($username, "events")}
                                                    <span class="fa arrow"></span>
                                                {/if}
                                            </a>
                                            {if canEdit($username, "events")}
                                                <ul>
                                                    <li {if $view=="events" && $sub_view==""}class="active selected"{/if}>
                                                        <a href="{$system['system_url']}/school/{$username}/events">
                                                            <i class="fa fa-caret-right fa-fw pr10"></i>{__("Lists")}
                                                        </a>
                                                    </li>
                                                    <li {if $view=="events" && $sub_view=="add"}class="active selected"{/if}>
                                                        <a href="{$system['system_url']}/school/{$username}/events/add">
                                                            <i class="fa fa-caret-right fa-fw pr10"></i>{__("Add New")}
                                                        </a>
                                                    </li>
                                                </ul>
                                            {/if}
                                        </li>
                                    {/if}
                                    <!-- Event -->

                                    <!-- Schedule -->
                                    {if $school['schedules']}
                                        {if canView($username, "schedules")}
                                            <li id="li-schedule" {if ($view=="schedules")}class="active"{/if}>
                                                <a href="{$system['system_url']}/school/{$username}/schedules">
                                                    <i class="fas fa-calendar-alt fa-lg fa-fw pr10"></i> {__("Schedule")}
                                                    {if canEdit($username, "schedules")}
                                                        <span class="fa arrow"></span>
                                                    {/if}
                                                </a>
                                                {if canEdit($username, "schedules")}
                                                    <ul>
                                                        <li id="li-schedule-list"
                                                            {if $view=="schedules" && $sub_view==""}class="active selected"{/if}>
                                                            <a href="{$system['system_url']}/school/{$username}/schedules">
                                                                <i class="fa fa-caret-right fa-fw pr10"></i>{__("Schedule list")}
                                                            </a>
                                                        </li>
                                                        <li id="li-schedule-add"
                                                            {if $view=="schedules" && $sub_view=="add"}class="active selected"{/if}>
                                                            <a href="{$system['system_url']}/school/{$username}/schedules/add">
                                                                <i class="fa fa-caret-right fa-fw pr10"></i>{__("Add schedule")}
                                                            </a>
                                                        </li>
                                                    </ul>
                                                {/if}
                                            </li>
                                        {/if}
                                    {/if}
                                    <!-- End schedule -->

                                    <!-- Menu -->
                                    {if $school['menus']}
                                        {if canView($username, "menus")}
                                            <li {if ($view=="menus")}class="active"{/if}>
                                                <a href="{$system['system_url']}/school/{$username}/menus">
                                                    <i class="fas fa-utensils fa-lg fa-fw pr10"></i> {__("Menu")}
                                                    {if canEdit($username, "menus")}
                                                        <span class="fa arrow"></span>
                                                    {/if}
                                                </a>
                                                {if canEdit($username, "menus")}
                                                    <ul>
                                                        <li {if $view=="menus" && $sub_view==""}class="active selected"{/if}>
                                                            <a href="{$system['system_url']}/school/{$username}/menus">
                                                                <i class="fa fa-caret-right fa-fw pr10"></i>{__("Menu list")}
                                                            </a>
                                                        </li>
                                                        <li {if $view=="menus" && $sub_view=="add"}class="active selected"{/if}>
                                                            <a href="{$system['system_url']}/school/{$username}/menus/add">
                                                                <i class="fa fa-caret-right fa-fw pr10"></i>{__("Add menu")}
                                                            </a>
                                                        </li>
                                                    </ul>
                                                {/if}
                                            </li>
                                        {/if}
                                    {/if}
                                    <!-- End menu -->

                                    <!-- Contact book -->
                                    {if $school['reports']}
                                        {if canView($username, "reports")}
                                            <li id="li-report" class="{if $view=="reports"}active{/if}">
                                                <a href="{$system['system_url']}/school/{$username}/reports">
                                                    <i class="fa fa-book fa-lg fa-fw pr10"></i> {__("Contact book")}
                                                    <span class="fa arrow"></span>
                                                </a>
                                                <ul>
                                                    <li id="li-report-list"
                                                        {if ($view=="reports") && ($sub_view=="")}class="active selected"{/if}>
                                                        <a href="{$system['system_url']}/school/{$username}/reports">
                                                            <i class="fa fa-caret-right fa-fw pr10"></i>{__("Lists")}
                                                        </a>
                                                    </li>
                                                    {if canEdit($username, "reports")}
                                                        <li id="li-report-add"
                                                            {if $view=="reports" && $sub_view=="add"}class="active selected"{/if}>
                                                            <a href="{$system['system_url']}/school/{$username}/reports/add">
                                                                <i class="fa fa-caret-right fa-fw pr10"></i>{__("Add New")}
                                                            </a>
                                                        </li>
                                                    {/if}
                                                    <li id="li-report-template"
                                                        {if $view=="reports" && $sub_view=="listtemp"}class="active selected"{/if}>
                                                        <a href="{$system['system_url']}/school/{$username}/reports/listtemp">
                                                            <i class="fa fa-caret-right fa-fw pr10"></i>{__("List template")}
                                                        </a>
                                                    </li>
                                                    <li id="li-report-category"
                                                        {if $view=="reports" && $sub_view=="listcate"}class="active selected"{/if}>
                                                        <a href="{$system['system_url']}/school/{$username}/reports/listcate">
                                                            <i class="fa fa-caret-right fa-fw pr10"></i>{__("List category")}
                                                        </a>
                                                    </li>
                                                </ul>
                                            </li>
                                        {/if}
                                    {/if}
                                    <!-- End Contact book -->

                                    <!-- Points -->
                                    {if $school['points']}
                                        {if canView($username, "points")}
                                            <li id="li-point" class="{if $view=="points"}active{/if}">
                                                <a href="{$system['system_url']}/school/{$username}/points">
                                                    <i class="fa fa-book fa-lg fa-fw pr10"></i> {__("Point")}
                                                    <span class="fa arrow"></span>
                                                </a>
                                                <ul>
                                                    <li id="li-point-list"
                                                        {if ($view=="points") && ($sub_view=="listsbysubject")}class="active selected"{/if}>
                                                        <a href="{$system['system_url']}/school/{$username}/points/listsbysubject">
                                                            <i class="fa fa-caret-right fa-fw pr10"></i>{__("Lists by subject")}
                                                        </a>
                                                    </li>
                                                    <li id="li-point-list"
                                                        {if ($view=="points") && ($sub_view=="listsbystudent")}class="active selected"{/if}>
                                                        <a href="{$system['system_url']}/school/{$username}/points/listsbystudent">
                                                            <i class="fa fa-caret-right fa-fw pr10"></i>{__("Lists by student")}
                                                        </a>
                                                    </li>
                                                    {if canEdit($username, "points")}
                                                        <li id="li-point-assign"
                                                            {if $view=="points" && $sub_view=="importManual"}class="active selected"{/if}>
                                                            <a href="{$system['system_url']}/school/{$username}/points/importManual">
                                                                <i class="fa fa-caret-right fa-fw pr10"></i>{__("Manual Import")}
                                                            </a>
                                                        </li>
                                                        <li id="li-point-assign"
                                                            {if $view=="points" && $sub_view=="importExcel"}class="active selected"{/if}>
                                                            <a href="{$system['system_url']}/school/{$username}/points/importExcel">
                                                                <i class="fa fa-caret-right fa-fw pr10"></i>{__("Import from Excel file")}
                                                            </a>
                                                        </li>
                                                        <li id="li-point-assign"
                                                            {if $view=="points" && $sub_view=="assign"}class="active selected"{/if}>
                                                            <a href="{$system['system_url']}/school/{$username}/points/assign">
                                                                <i class="fa fa-caret-right fa-fw pr10"></i>{__("Assign teacher")}
                                                            </a>
                                                        </li>
                                                        <li id="li-point-comment"
                                                            {if $view=="points" && $sub_view=="comment"}class="active selected"{/if}>
                                                            <a href="{$system['system_url']}/school/{$username}/points/comment">
                                                                <i class="fa fa-caret-right fa-fw pr10"></i>{__("Comment")}
                                                            </a>
                                                        </li>
                                                    {/if}
                                                </ul>
                                            </li>
                                        {/if}
                                    {/if}
                                    <!-- End Points -->

                                    {*End khối thông tin cho trẻ*}

                                    {*Begin khối tiện ích nhà trường*}
                                    {if $school['medicines'] || $school['feedback'] || $school['birthdays'] || $school['diarys']}
                                        {if canView($username, "medicines") || canView($username, "feedback") || canView($username, "birthdays") || canView($username, "diarys")}
                                            <li class="menu_header border_top">
                                                <a>{__("School utility")}</a>
                                            </li>
                                        {/if}
                                    {/if}
                                    <!-- Medicine -->
                                    {if $school['medicines']}
                                        {if canView($username, "medicines")}
                                            <li id="li-medicine" {if $view=="medicines"}class="active"{/if}>
                                                <a href="{$system['system_url']}/school/{$username}/medicines">
                                                    <i class="fa fa-medkit fa-lg fa-fw pr10"></i> {__("Medicines")}
                                                    <span class="fa arrow"></span>
                                                </a>
                                                <ul>
                                                    <li id="li-medicine-today"
                                                        {if $view=="medicines" && $sub_view==""}class="active selected"{/if}>
                                                        <a href="{$system['system_url']}/school/{$username}/medicines">
                                                            <i class="fa fa-caret-right fa-fw pr10"></i>{__("Today lists")}
                                                        </a>
                                                    </li>
                                                    <li id="li-medicine-all"
                                                        {if $view=="medicines" && $sub_view=="all"}class="active selected"{/if}>
                                                        <a href="{$system['system_url']}/school/{$username}/medicines/all">
                                                            <i class="fa fa-caret-right fa-fw pr10"></i>{__("List all")}
                                                        </a>
                                                    </li>
                                                    {if canEdit($username, "medicines")}
                                                        <li id="li-medicine-add"
                                                            {if $view=="medicines" && $sub_view=="add"}class="active selected"{/if}>
                                                            <a href="{$system['system_url']}/school/{$username}/medicines/add">
                                                                <i class="fa fa-caret-right fa-fw pr10"></i>{__("Add New")}
                                                            </a>
                                                        </li>
                                                    {/if}
                                                </ul>
                                            </li>
                                        {/if}
                                    {/if}
                                    <!-- Medicine -->

                                    <!-- Feedback -->
                                    {if $school['feedback']}
                                        {if canView($username, "feedback")}
                                            <li id="li-feeback" {if $view=="feedback"}class="active selected"{/if}>
                                                <a href="{$system['system_url']}/school/{$username}/feedback">
                                                    <i class="fa fa-envelope fa-fw fa-lg pr10"></i> {__("Parent feedback")}
                                                </a>
                                            </li>
                                        {/if}
                                    {/if}
                                    <!-- Feedback -->

                                    <!-- Birthday -->
                                    {if $school['birthdays']}
                                        {if canView($username, "birthdays")}
                                            <li class="{if $view=="birthdays"}active{/if}">
                                                <a href="{$system['system_url']}/school/{$username}/birthdays">
                                                    <i class="fa fa-birthday-cake fa-fw fa-lg pr10"></i> {__("Birthday")}
                                                </a>
                                            </li>
                                        {/if}
                                    {/if}
                                    <!-- End Birthday -->

                                    <!-- Diary -->
                                    {if $school['diarys']}
                                        {if canView($username, "diarys")}
                                            <li {if $view=="diarys"}class="active"{/if}>
                                                <a href="{$system['system_url']}/school/{$username}/diarys">
                                                    <i class="fa fa-image fa-lg fa-fw pr10"></i> {__("Diary corner")}
                                                    <span class="fa arrow"></span>
                                                </a>
                                                <ul>
                                                    <li {if $view=="diarys" && $sub_view==""}class="active selected"{/if}>
                                                        <a href="{$system['system_url']}/school/{$username}/diarys">
                                                            <i class="fa fa-caret-right fa-fw pr10"></i>{__("Diary lists")}
                                                        </a>
                                                    </li>
                                                    {if canEdit($username, "diarys")}
                                                        <li id="li-medicine-add"
                                                            {if $view=="diarys" && $sub_view=="add"}class="active selected"{/if}>
                                                            <a href="{$system['system_url']}/school/{$username}/diarys/add">
                                                                <i class="fa fa-caret-right fa-fw pr10"></i>{__("Add New")}
                                                            </a>
                                                        </li>
                                                    {/if}
                                                </ul>
                                            </li>
                                        {/if}
                                    {/if}
                                    <!-- Health -->
                                    {if $school['healths']}
                                        {if canView($username, "healths")}
                                            <li {if $view=="healths"}class="active"{/if}>
                                                <a href="{$system['system_url']}/school/{$username}/healths">
                                                    <i class="fa fa-heart fa-lg fa-fw pr10"></i> {__("Health information")}
                                                    <span class="fa arrow"></span>
                                                </a>
                                                <ul>
                                                    <li {if $view=="healths" && $sub_view==""}class="active selected"{/if}>
                                                        <a href="{$system['system_url']}/school/{$username}/healths">
                                                            <i class="fa fa-caret-right fa-fw pr10"></i>{__("Lists")}
                                                        </a>
                                                    </li>
                                                    {if canEdit($username, "healths")}
                                                        <li {if $view=="healths" && $sub_view=="add"}class="active selected"{/if}>
                                                            <a href="{$system['system_url']}/school/{$username}/healths/add">
                                                                <i class="fa fa-caret-right fa-fw pr10"></i>{__("Add New")}
                                                            </a>
                                                        </li>
                                                    {/if}
                                                </ul>
                                            </li>
                                        {/if}
                                    {/if}
                                    <!-- Health -->
                                    {*End khối tiện ích nhà trường*}

                                    {*Begin khối nhà trường*}
                                    {if $school['classes'] || $school['classlevels'] || $school['teachers'] || $school['children'] || $school['roles']}
                                        {if canView($username, "classes") || canView($username, "classlevels") || canView($username, "teachers") || canView($username, "children")|| canView($username, "roles") || canView($username, "settings")}
                                            <li class="menu_header border_top">
                                                <a>{__("School")}</a>
                                            </li>
                                        {/if}
                                    {/if}
                                    <!-- Classes -->
                                    {if $school['classes'] || $school['classlevels']}
                                        {if canView($username, "classes") || canView($username, "classlevels")}
                                            <li {if ($view=="classes" || $view=="classlevels")}class="active"{/if}>
                                                <a href="{$system['system_url']}/school/{$username}/classes">
                                                    <i class="fa fa-graduation-cap fa-fw fa-lg pr10"></i> {__("Class level")}
                                                    - {__("Class")}
                                                    {if canEdit($username, "classes") || canView($username, "classlevels")}
                                                        <span class="fa arrow"></span>
                                                    {/if}
                                                </a>
                                                {if canEdit($username, "classes") || canView($username, "classlevels")}
                                                    <ul>
                                                        {if $school['classlevels']}
                                                            {if canView($username, "classlevels")}
                                                                <li {if $view=="classlevels"}class="active selected"{/if}>
                                                                    <a href="{$system['system_url']}/school/{$username}/classlevels">
                                                                        <i class="fa fa-caret-right fa-fw pr10"></i>{__("Class level")}
                                                                    </a>
                                                                </li>
                                                            {/if}
                                                        {/if}
                                                        {if $school['classes']}
                                                            {if canView($username, "classes")}
                                                                <li {if $view=="classes" && $sub_view==""}class="active selected"{/if}>
                                                                    <a href="{$system['system_url']}/school/{$username}/classes">
                                                                        <i class="fa fa-caret-right fa-fw pr10"></i>{__("Class lists")}
                                                                    </a>
                                                                </li>
                                                            {/if}
                                                            {if canEdit($username, "classes")}
                                                                <li {if ($view=="classes") && ($sub_view=="add")}class="active selected"{/if}>
                                                                    <a href="{$system['system_url']}/school/{$username}/classes/add">
                                                                        <i class="fa fa-caret-right fa-fw pr10"></i>{__("Add new class")}
                                                                    </a>
                                                                </li>
                                                                <li {if ($view=="classes") && ($sub_view=="classup")}class="active selected"{/if}>
                                                                    <a href="{$system['system_url']}/school/{$username}/classes/classup">
                                                                        <i class="fa fa-caret-right fa-fw pr10"></i>{__("Class up")}
                                                                    </a>
                                                                </li>
                                                                <li {if ($view=="classes") && ($sub_view=="graduates")}class="active selected"{/if}>
                                                                    <a href="{$system['system_url']}/school/{$username}/classes">
                                                                        <i class="fa fa-caret-right fa-fw pr10"></i>{__("Graduate")}
                                                                    </a>
                                                                </li>
                                                            {/if}
                                                        {/if}
                                                    </ul>
                                                {/if}
                                            </li>
                                        {/if}
                                    {/if}
                                    <!-- Classes -->
                                    <!-- Subjects -->
                                    {if $school['subjects']}
                                        {if canView($username, "subjects")}
                                            <li {if ($view=="subjects")}class="active"{/if}>
                                                <a href="{$system['system_url']}/school/{$username}/subjects">
                                                    <i class="fa fa-book fa-fw fa-lg pr10"></i> {__("Subject")}
                                                    {if canEdit($username, "subjects")}
                                                        <span class="fa arrow"></span>
                                                    {/if}
                                                </a>
                                                {if canEdit($username, "subjects")}
                                                    <ul>
                                                        {if $school['subjects']}
                                                            {if canView($username, "subjects")}
                                                                <li {if $view=="subjects"}class="active selected"{/if}>
                                                                    <a href="{$system['system_url']}/school/{$username}/subjects">
                                                                        <i class="fa fa-caret-right fa-fw pr10"></i>{__("List")}
                                                                    </a>
                                                                </li>
                                                            {/if}
                                                        {/if}
                                                    </ul>
                                                {/if}
                                            </li>
                                        {/if}
                                    {/if}
                                    <!-- Subjects -->
                                    <!-- Teachers/Employee -->
                                    {if $school['teachers']}
                                        {if canView($username, "teachers")}
                                            <li {if $view=="teachers"}class="active"{/if}>
                                                <a href="{$system['system_url']}/school/{$username}/teachers">
                                                    <i class="fa fa-street-view fa-fw fa-lg pr10"></i> {__("Teacher - Employee")}
                                                    {if canEdit($username, "teachers")}
                                                        <span class="fa arrow"></span>
                                                    {/if}
                                                </a>
                                                {if canEdit($username, "teachers")}
                                                    <ul>
                                                        <li {if $view=="teachers" && $sub_view==""}class="active selected"{/if}>
                                                            <a href="{$system['system_url']}/school/{$username}/teachers">
                                                                <i class="fa fa-caret-right fa-fw pr10"></i>{__("Lists")}
                                                            </a>
                                                        </li>
                                                        <li {if $view=="teachers" && $sub_view=="addexisting"}class="active selected"{/if}>
                                                            <a href="{$system['system_url']}/school/{$username}/teachers/addexisting">
                                                                <i class="fa fa-caret-right fa-fw pr10"></i>{__("Add Existing Account")}
                                                            </a>
                                                        </li>
                                                        <li {if $view=="teachers" && $sub_view=="add"}class="active selected"{/if}>
                                                            <a href="{$system['system_url']}/school/{$username}/teachers/add">
                                                                <i class="fa fa-caret-right fa-fw pr10"></i>{__("Add New")}
                                                            </a>
                                                        </li>
                                                    </ul>
                                                {/if}
                                            </li>
                                        {/if}
                                    {/if}
                                    <!-- Teachers -->

                                    <!-- BEGIN - Children -->
                                    {if $school['children']}
                                        {if canView($username, "children")}
                                            <li {if $view=="children"}class="active"{/if}>
                                                <a href="{$system['system_url']}/school/{$username}/children">
                                                    <i class="fa fa-child fa-fw fa-lg pr10"></i> {__("Student")}
                                                    {if canEdit($username, "children")}
                                                        <span class="fa arrow"></span>
                                                    {/if}
                                                </a>
                                                {if canEdit($username, "children")}
                                                    <ul>
                                                        <li {if $view=="children" && $sub_view==""}class="active selected"{/if}>
                                                            <a href="{$system['system_url']}/school/{$username}/children">
                                                                <i class="fa fa-caret-right fa-fw pr10"></i>{__("Lists")}
                                                            </a>
                                                        </li>
                                                        <li {if $view=="children" && $sub_view=="listchildnew"}class="active selected"{/if}>
                                                            <a href="{$system['system_url']}/school/{$username}/children/listchildnew">
                                                                <i class="fa fa-caret-right fa-fw pr10"></i>{__("New student study begin in month")}
                                                            </a>
                                                        </li>
                                                        <li {if $view=="children" && $sub_view=="import"}class="active selected"{/if}>
                                                            <a href="{$system['system_url']}/school/{$username}/children/import">
                                                                <i class="fa fa-caret-right fa-fw pr10"></i>{__("Import from Excel file")}
                                                            </a>
                                                        </li>
                                                        <li {if $view=="children" && $sub_view=="add"}class="active selected"{/if}>
                                                            <a href="{$system['system_url']}/school/{$username}/children/add">
                                                                <i class="fa fa-caret-right fa-fw pr10"></i>{__("Add New")}
                                                            </a>
                                                        </li>
                                                    </ul>
                                                {/if}
                                            </li>
                                        {/if}
                                    {/if}
                                    <!-- END - Children -->

                                    <!-- Permission -->
                                    {if $school['roles']}
                                        {if canView($username, "roles")}
                                            <li {if $view=="roles"}class="active"{/if}>
                                                <a href="{$system['system_url']}/school/{$username}/roles">
                                                    <i class="fa fa-unlock-alt fa-fw fa-lg pr10"></i> {__('Permission')}
                                                </a>
                                            </li>
                                        {/if}
                                    {/if}
                                    <!-- End Permission -->
                                    <!-- Thống kê đăng nhập -->
                                    {if $school['loginstatistics']}
                                        {if canView($username, "loginstatistics")}
                                            <li class="{if $view=="loginstatistics"}active selected{/if}">
                                                <a href="{$system['system_url']}/school/{$username}/loginstatistics">
                                                    <i class="fa fa-user fa-fw fa-lg pr10"></i> {__("Login statistics")}
                                                </a>
                                            </li>
                                        {/if}
                                    {/if}
                                    <!-- End Thống kê đăng nhập -->
                                    <!-- Settings -->
                                    {if $school['settings']}
                                        {if canView($username, "settings")}
                                            <li class="{if $view=="settings"}active selected{/if}">
                                                <a href="{$system['system_url']}/school/{$username}/settings">
                                                    <i class="fa fa-cog fa-fw fa-lg pr10"></i> {__("Setting and control")}
                                                </a>
                                            </li>
                                        {/if}
                                    {/if}
                                    <!-- End Setting -->
                                    {*End khối nhà trường*}
                                    <!-- Ẩn hiện menu -->
                                    <li class="shmenu">
                                        <input type="hidden" id="full_screen"
                                               value="{if $view == 'attendance' && $sub_view == 'classatt'}1{else} 0{/if}">
                                        <a href="#" class="js_school_hidden" data-username="{$username}"> <i
                                                    class="fa fa-bars fa-fw pr10 fa-lg"></i> {__("Show/hide menu")}
                                            <span class="fa fa-angle-left pull-right"></span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
        {else}
            <div class="col-xs-0 col-sm-3" style="height: 500px;"></div>
        {/if}
        {if $school['school_step'] != SCHOOL_STEP_FINISH && $school['grade'] == 0}
            {include file='ci/school/school.steps.tpl'}
        {/if}
        {include file='ci/school/school.help.tpl'}
        <div class="{if $status} {if ($view == 'pickup' && $sub_view == 'assign' && ($school['school_step'] == SCHOOL_STEP_FINISH  || $school['grade'] != 0))}col-md-10 col-sm-10 {else} col-md-9 col-sm-9{/if} {else} col-md-12 col-sm-12 {/if}"
             id="content_right">
            {if $view==""}
                {include file='ci/school/school.dashboard.tpl'}
            {else}
                {include file="ci/school/school.$view.tpl"}
            {/if}
            {if $school['school_step'] == SCHOOL_STEP_FINISH  || $school['grade'] != 0}
                {include file='ci/school/school.faq.tpl'}
            {/if}
            {if $school['school_step'] != SCHOOL_STEP_FINISH && $school['grade'] == 0}
                {include file='ci/school/school.direct.tpl'}
            {/if}
        </div>
    </div>
</div>
<!-- page content -->

{include file='_footer.tpl'}