{include file='_head.tpl'}
{include file='_header.tpl'}

<!-- page content -->
<div class="container mt20 height_min"> <!-- class height_min để set height tối thiểu của container -->
    <div class="row">

        <!-- left panel -->
        <div class="col-sm-3 offcanvas-sidebar">
            <div class="panel panel-default">
                <div class="panel-body with-nav">
                    <ul class="side-nav">
                        <li {if $view == ""}class="active"{/if}>
                            <a href="{$system['system_url']}/settings"><i class="fa fa-cog fa-fw fa-lg pr10"></i> {__("Account Settings")}</a>
                        </li>
                        <li {if $view == "profile"}class="active"{/if}>
                            <a href="{$system['system_url']}/settings/profile"><i class="fa fa-user fa-fw fa-lg pr10"></i> {__("Edit Profile")}</a>
                        </li>
                        <li {if $view == "privacy"}class="active"{/if}>
                            <a href="{$system['system_url']}/settings/privacy"><i class="fa fa-lock fa-fw fa-lg pr10"></i> {__("Privacy Settings")}</a>
                        </li>
                        <li {if $view == "security"}class="active"{/if}>
                            <a href="{$system['system_url']}/settings/security"><i class="fa fa-shield-alt fa-fw fa-lg pr10"></i> {__("Security Settings")}</a>
                        </li>
                        {if $system['email_notifications']}
                            {if $system['email_post_likes'] || $system['email_post_comments'] || $system['email_post_shares'] || $system['email_wall_posts'] || $system['email_mentions'] || $system['email_profile_visits'] || $system['email_friend_requests']}
                                <li {if $view == "notifications"}class="active"{/if}>
                                    <a href="{$system['system_url']}/settings/notifications"><i class="fa fa-envelope-open fa-fw fa-lg pr10"></i> {__("Email Notifications")}</a>
                                </li>
                            {/if}
                        {/if}
                        {if $system['social_login_enabled']}
                            {if $system['facebook_login_enabled'] || $system['twitter_login_enabled'] || $system['google_login_enabled'] || $system['instagram_login_enabled'] || $system['linkedin_login_enabled'] || $system['vkontakte_login_enabled']}
                                <li {if $view == "linked"}class="active"{/if}>
                                    <a href="{$system['system_url']}/settings/linked"><i class="fa fa-share-alt fa-fw fa-lg pr10"></i> {__("Linked Accounts")}</a>
                                </li>
                            {/if}
                        {/if}
                        <li {if $view == "blocking"}class="active"{/if}>
                            <a href="{$system['system_url']}/settings/blocking"><i class="fa fa-minus-circle fa-fw fa-lg pr10"></i> {__("Blocking")}</a>
                        </li>
                        {if $system['packages_enabled']}
                            <li {if $view == "membership"}class="active"{/if}>
                                <a href="{$system['system_url']}/settings/membership"><i class="fa fa-id-card fa-fw fa-lg pr10"></i> {__("Membership")}</a>
                            </li>
                        {/if}
                        {if $system['affiliates_enabled']}
                            <li {if $view == "affiliates"}class="active"{/if}>
                                <a href="{$system['system_url']}/settings/affiliates"><i class="fa fa-exchange-alt fa-fw fa-lg pr10"></i> {__("Affiliates")}</a>
                            </li>
                        {/if}
                        {if $system['verification_requests']}
                            <li {if $view == "verification"}class="active"{/if}>
                                <a href="{$system['system_url']}/settings/verification"><i class="fa fa-check-circle fa-fw fa-lg pr10"></i> {__("Verification")}</a>
                            </li>
                        {/if}
                        {if $system['delete_accounts_enabled']}
                            <li {if $view == "delete"}class="active"{/if}>
                                <a href="{$system['system_url']}/settings/delete"><i class="fa fa-trash fa-fw fa-lg pr10"></i> {__("Delete Account")}</a>
                            </li>
                        {/if}
                    </ul>
                </div>
            </div>
        </div>
        <!-- left panel -->

        <!-- right panel -->
        <div class="col-sm-9 offcanvas-mainbar">
            <div class="panel panel-default">

                {if $view == ""}
                    <div class="panel-heading with-icon with-nav">
                        <!-- panel title -->
                        <div class="mb20">
                            <i class="fa fa-cog pr5 panel-icon"></i>
                            <strong>{__("Account Settings")}</strong>
                        </div>
                        <!-- panel title -->

                        <!-- panel nav -->
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#username" data-toggle="tab">
                                    <i class="fa fa-cog fa-fw mr5"></i><strong class="pr5">{__("Username")}</strong>
                                </a>
                            </li>
                            <li>
                                <a href="#email" data-toggle="tab">
                                    <i class="fa fa-envelope fa-fw mr5"></i><strong class="pr5">{__("Email")}</strong>
                                </a>
                            </li>
                            {*{if $system['activation_enabled'] && $system['activation_type'] == "sms"}*}
                                <li>
                                    <a href="#phone" data-toggle="tab">
                                        <i class="fa fa-mobile fa-fw mr5"></i><strong class="pr5">{__("Phone")}</strong>
                                    </a>
                                </li>
                            {*{/if}*}
                            <li>
                                <a href="#password" data-toggle="tab">
                                    <i class="fa fa-key fa-fw mr5"></i><strong class="pr5">{__("Password")}</strong>
                                </a>
                            </li>
                        </ul>
                        <!-- panel nav -->
                    </div>
                    <div class="panel-body tab-content">
                        <!-- username -->
                        <div class="tab-pane active" id="username">
                            <form class="js_ajax-forms form-horizontal" data-url="users/settings.php?edit=username">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        {__("Username")}
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="input-group">
                                            <span class="input-group-addon">{$system['system_url']}/</span>
                                            <input type="text" class="form-control" name="username" value="{$user->_data['user_name']}">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-9 col-sm-offset-3">
                                        <button type="submit" class="btn btn-primary">{__("Save Changes")}</button>
                                    </div>
                                </div>

                                <!-- success -->
                                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                                <!-- success -->

                                <!-- error -->
                                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                                <!-- error -->
                            </form>
                        </div>
                        <!-- username -->

                        <!-- email -->
                        <div class="tab-pane" id="email">
                            <form class="js_ajax-forms form-horizontal" data-url="users/settings.php?edit=email">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        {__("Email Address")}
                                    </label>
                                    <div class="col-sm-9">
                                        <input type="email" id="mail" class="form-control" name="email" value="{$user->_data['user_email']}">
                                    </div>
                                </div>


                                <div class="form-group {if !$user->_data['user_activated'] || is_empty($user->_data['user_email_activation'])}x-hidden{/if}" id="info-email-activation">
                                    <div class="col-sm-9 col-sm-offset-3">
                                        <span class="help-block" style="color: royalblue">
                                            {__("Email")} <strong> {$user->_data['user_email_activation']} </strong> {__("chưa được xác thực, vui lòng click vào link được gửi về email để hoàn tất quá trình thay đổi")}
                                        </span>
                                    </div>

                                    <div class="col-sm-9 col-sm-offset-3">
                                        <span class="text-link col-sm-7 pl0" onclick="finishEmailVerify();">
                                            {__("Đã xác nhận, hoàn tất")}
                                        </span>

                                        <span class="text-link" data-toggle="modal" data-url="core/activation_email_resend.php">
                                            {__("Resend Verification Email")}
                                        </span>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <div class="col-sm-9 col-sm-offset-3">
                                        <button type="submit" class="btn btn-primary">{__("Save Changes")}</button>
                                    </div>
                                </div>

                                <!-- success -->
                                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                                <!-- success -->

                                <!-- error -->
                                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                                <!-- error -->
                            </form>
                        </div>
                        <!-- email -->

                        <!-- phone -->
                        {*{if $system['activation_enabled'] && $system['activation_type'] == "sms"}*}
                            <div class="tab-pane" id="phone">
                                <form class="js_ajax-forms form-horizontal" data-url="users/settings.php?edit=phone">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">
                                            {__("Mobile number")}
                                        </label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="phone_number" name="phone" value="{$user->_data['user_phone_signin']}" placeholder='{__("Mobile number (eg. 098...)")}'>
                                            <span class="help-block">
                                                {__("Hệ thống sẽ gửi tin nhắn SMS để xác minh số của bạn")}
                                            </span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-9 col-sm-offset-3">
                                            <a onclick="phoneVerification();" class="btn btn-primary">{__("Xác nhận và lưu thay đổi")}</a>
                                        </div>
                                    </div>

                                    <!-- success -->
                                    <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                                    <!-- success -->

                                    <!-- error -->
                                    <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                                    <!-- error -->
                                </form>
                            </div>
                        {*{/if}*}
                        <!-- phone -->

                        <!-- password -->
                        <div class="tab-pane" id="password">
                            <form class="js_ajax-forms form-horizontal" data-url="users/settings.php?edit=password">

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        {__("Current")}
                                    </label>
                                    <div class="col-sm-9">
                                        <input type="password" class="form-control" name="current">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        {__("New")}
                                    </label>
                                    <div class="col-sm-9">
                                        <input type="password" class="form-control" name="new">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        {__("Re-type New")}
                                    </label>
                                    <div class="col-sm-9">
                                        <input type="password" class="form-control" name="confirm">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-9 col-sm-offset-3">
                                        <button type="submit" class="btn btn-primary">{__("Save Changes")}</button>
                                    </div>
                                </div>

                                <!-- success -->
                                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                                <!-- success -->

                                <!-- error -->
                                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                                <!-- error -->
                            </form>
                        </div>
                        <!-- password -->
                    </div>
                {elseif $view == "profile"}
                    <div class="panel-heading with-icon with-nav">
                        <!-- panel title -->
                        <div class="mb20">
                            <i class="fa fa-user pr5 panel-icon"></i>
                            <strong>{__("Edit Profile")}</strong>
                        </div>
                        <!-- panel title -->

                        <!-- panel nav -->
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#basic" data-toggle="tab">
                                    <i class="fa fa-user fa-fw mr5"></i><strong class="pr5">{__("Basic")}</strong>
                                </a>
                            </li>
                            <li>
                                <a href="#work" data-toggle="tab">
                                    <i class="fa fa-briefcase fa-fw mr5"></i><strong class="pr5">{__("Work")}</strong>
                                </a>
                            </li>
                            <li>
                                <a href="#location" data-toggle="tab">
                                    <i class="fa fa-map-marker fa-fw mr5"></i><strong class="pr5">{__("Location")}</strong>
                                </a>
                            </li>
                            <li>
                                <a href="#education" data-toggle="tab">
                                    <i class="fa fa-graduation-cap fa-fw mr5"></i><strong class="pr5">{__("Education")}</strong>
                                </a>
                            </li>
                        </ul>
                        <!-- panel nav -->
                    </div>
                    <div class="panel-body tab-content">
                        <!-- basic tab -->
                        <div class="tab-pane active" id="basic">
                            <form class="js_ajax-forms form-horizontal" data-url="users/settings.php?edit=basic">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        {__("Last Name")}
                                    </label>
                                    <div class="col-sm-9">
                                        <input class="form-control" name="lastname" value="{$user->_data['user_lastname']}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        {__("First Name")}
                                    </label>
                                    <div class="col-sm-9">
                                        <input class="form-control" name="firstname" value="{$user->_data['user_firstname']}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        {__("I am")}
                                    </label>
                                    <div class="col-sm-9">
                                        <select name="gender" class="form-control">
                                            <option value="none">{__("Select Sex")}</option>
                                            <option {if $user->_data['user_gender'] == "male"}selected{/if} value="male">{__("Male")}</option>
                                            <option {if $user->_data['user_gender'] == "female"}selected{/if} value="female">{__("Female")}</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        {__("Birthdate")}
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="row">
                                            <div class="col-xs-4">
                                                <select class="form-control" name="birth_month">
                                                    <option value="none">{__("Select Month")}</option>
                                                    <option {if $user->_data['user_birthdate_parsed']['month'] == '1'}selected{/if} value="1">{__("Jan")}</option>
                                                    <option {if $user->_data['user_birthdate_parsed']['month'] == '2'}selected{/if} value="2">{__("Feb")}</option>
                                                    <option {if $user->_data['user_birthdate_parsed']['month'] == '3'}selected{/if} value="3">{__("Mar")}</option>
                                                    <option {if $user->_data['user_birthdate_parsed']['month'] == '4'}selected{/if} value="4">{__("Apr")}</option>
                                                    <option {if $user->_data['user_birthdate_parsed']['month'] == '5'}selected{/if} value="5">{__("May")}</option>
                                                    <option {if $user->_data['user_birthdate_parsed']['month'] == '6'}selected{/if} value="6">{__("Jun")}</option>
                                                    <option {if $user->_data['user_birthdate_parsed']['month'] == '7'}selected{/if} value="7">{__("Jul")}</option>
                                                    <option {if $user->_data['user_birthdate_parsed']['month'] == '8'}selected{/if} value="8">{__("Aug")}</option>
                                                    <option {if $user->_data['user_birthdate_parsed']['month'] == '9'}selected{/if} value="9">{__("Sep")}</option>
                                                    <option {if $user->_data['user_birthdate_parsed']['month'] == '10'}selected{/if} value="10">{__("Oct")}</option>
                                                    <option {if $user->_data['user_birthdate_parsed']['month'] == '11'}selected{/if} value="11">{__("Nov")}</option>
                                                    <option {if $user->_data['user_birthdate_parsed']['month'] == '12'}selected{/if} value="12">{__("Dec")}</option>
                                                </select>
                                            </div>
                                            <div class="col-xs-4">
                                                <select class="form-control" name="birth_day">
                                                    <option value="none">{__("Select Day")}</option>
                                                    {for $i=1 to 31}
                                                        <option {if $user->_data['user_birthdate_parsed']['day'] == $i}selected{/if} value="{$i}">{$i}</option>
                                                    {/for}
                                                </select>
                                            </div>
                                            <div class="col-xs-4">
                                                <select class="form-control" name="birth_year">
                                                    <option value="none">{__("Select Year")}</option>
                                                    {for $i=1905 to 2015}
                                                        <option {if $user->_data['user_birthdate_parsed']['year'] == $i}selected{/if} value="{$i}">{$i}</option>
                                                    {/for}
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- ConIu - Begin - Thêm trường phone, city_id -->
                               {* <div class="form-group">
                                    <label class="col-sm-3 control-label">{__("Phone")}</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="user_phone" id="user_phone" value="{$user->_data['user_phone']}" maxlength="50" required>
                                    </div>
                                </div>*}
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">{__("City")}</label>
                                    <div class="col-sm-9">
                                        <select name="city_id" class="form-control">
                                            <option value="0">{__("Select city")}</option>
                                            {foreach $cities as $city}
                                                <option value="{$city['city_id']}" {if $city['city_id']==$user->_data['city_id']}selected{/if}>{$city['city_name']}</option>
                                            {/foreach}
                                        </select>
                                    </div>
                                </div>
                                <!-- ConIu - End - Thêm trường phone, city_id -->

                                <div class="form-group">
                                    <div class="col-sm-9 col-sm-offset-3">
                                        <button type="submit" class="btn btn-primary">{__("Save Changes")}</button>
                                    </div>
                                </div>

                                <!-- success -->
                                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                                <!-- success -->

                                <!-- error -->
                                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                                <!-- error -->
                            </form>
                        </div>
                        <!-- basic tab -->

                        <!-- work tab -->
                        <div class="tab-pane" id="work">
                            <form class="js_ajax-forms form-horizontal" data-url="users/settings.php?edit=work">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        {__("Work Title")}
                                    </label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="work_title" value="{$user->_data['user_work_title']}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        {__("Work Place")}
                                    </label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="work_place" value="{$user->_data['user_work_place']}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        {__("Work Website")}
                                    </label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="work_url" value="{$user->_data['user_work_url']}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-9 col-sm-offset-3">
                                        <button type="submit" class="btn btn-primary">{__("Save Changes")}</button>
                                    </div>
                                </div>

                                <!-- success -->
                                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                                <!-- success -->

                                <!-- error -->
                                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                                <!-- error -->
                            </form>
                        </div>
                        <!-- work tab -->

                        <!-- location tab -->
                        <div class="tab-pane" id="location">
                            <form class="js_ajax-forms form-horizontal" data-url="users/settings.php?edit=location">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        {__("Current City")}
                                    </label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control js_geocomplete" name="city" value="{$user->_data['user_current_city']}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        {__("Hometown")}
                                    </label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control js_geocomplete" name="hometown" value="{$user->_data['user_hometown']}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-9 col-sm-offset-3">
                                        <button type="submit" class="btn btn-primary">{__("Save Changes")}</button>
                                    </div>
                                </div>

                                <!-- success -->
                                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                                <!-- success -->

                                <!-- error -->
                                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                                <!-- error -->
                            </form>
                        </div>
                        <!-- location tab -->

                        <!-- education tab -->
                        <div class="tab-pane" id="education">
                            <form class="js_ajax-forms form-horizontal" data-url="users/settings.php?edit=education">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        {__("School")}
                                    </label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="edu_school" value="{$user->_data['user_edu_school']}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        {__("Major")}
                                    </label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="edu_major" value="{$user->_data['user_edu_major']}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        {__("Class")}
                                    </label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="edu_class" value="{$user->_data['user_edu_class']}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-9 col-sm-offset-3">
                                        <button type="submit" class="btn btn-primary">{__("Save Changes")}</button>
                                    </div>
                                </div>

                                <!-- success -->
                                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                                <!-- success -->

                                <!-- error -->
                                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                                <!-- error -->
                            </form>
                        </div>
                        <!-- education tab -->

                    </div>
                {elseif $view == "privacy"}
                    <div class="panel-heading with-icon">
                        <!-- panel title -->
                        <i class="fa fa-lock pr5 panel-icon"></i>
                        <strong>{__("Privacy Settings")}</strong>
                        <!-- panel title -->
                    </div>
                    <div class="panel-body">
                        <form class="js_ajax-forms form-horizontal" data-url="users/settings.php?edit=privacy">
                            {if $system['chat_enabled']}
                                <div class="form-group">
                                    <label class="col-sm-5 control-label" for="privacy_chat">
                                        {__("Chat")}
                                    </label>
                                    <div class="col-sm-3">
                                        <select class="form-control" name="privacy_chat" id="privacy_chat">
                                            <option {if $user->_data['user_chat_enabled'] == 0}selected{/if} value="0">
                                                {__("Offline")}
                                            </option>
                                            <option {if $user->_data['user_chat_enabled'] == 1}selected{/if} value="1">
                                                {__("Online")}
                                            </option>
                                        </select>
                                    </div>
                                </div>
                            {/if}
                            <div class="form-group">
                                <label class="col-sm-5 control-label" for="privacy_wall">
                                    {__("Who can post on your wall")}
                                </label>
                                <div class="col-sm-3">
                                    <select class="form-control" name="privacy_wall" id="privacy_wall">
                                        <option {if $user->_data['user_privacy_wall'] == "public"}selected{/if} value="public">
                                            {__("Everyone")}
                                        </option>
                                        <option {if $user->_data['user_privacy_wall'] == "friends"}selected{/if} value="friends">
                                            {__("Friends")}
                                        </option>
                                        <option {if $user->_data['user_privacy_wall'] == "me"}selected{/if} value="me">
                                            {__("Just Me")}
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-5 control-label" for="privacy_birthdate">
                                    {__("Who can see your")} {__("birthdate")}
                                </label>
                                <div class="col-sm-3">
                                    <select class="form-control" name="privacy_birthdate" id="privacy_birthdate">
                                        <option {if $user->_data['user_privacy_birthdate'] == "public"}selected{/if} value="public">
                                            {__("Everyone")}
                                        </option>
                                        <option {if $user->_data['user_privacy_birthdate'] == "friends"}selected{/if} value="friends">
                                            {__("Friends")}
                                        </option>
                                        <option {if $user->_data['user_privacy_birthdate'] == "me"}selected{/if} value="me">
                                            {__("Just Me")}
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-5 control-label" for="privacy_relationship">
                                    {__("Who can see your")} {__("relationship")}
                                </label>
                                <div class="col-sm-3">
                                    <select class="form-control" name="privacy_relationship" id="privacy_relationship">
                                        <option {if $user->_data['user_privacy_relationship'] == "public"}selected{/if} value="public">
                                            {__("Everyone")}
                                        </option>
                                        <option {if $user->_data['user_privacy_relationship'] == "friends"}selected{/if} value="friends">
                                            {__("Friends")}
                                        </option>
                                        <option {if $user->_data['user_privacy_relationship'] == "me"}selected{/if} value="me">
                                            {__("Just Me")}
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-5 control-label" for="privacy_basic">
                                    {__("Who can see your")} {__("basic info")}
                                </label>
                                <div class="col-sm-3">
                                    <select class="form-control" name="privacy_basic" id="privacy_basic">
                                        <option {if $user->_data['user_privacy_basic'] == "public"}selected{/if} value="public">
                                            {__("Everyone")}
                                        </option>
                                        <option {if $user->_data['user_privacy_basic'] == "friends"}selected{/if} value="friends">
                                            {__("Friends")}
                                        </option>
                                        <option {if $user->_data['user_privacy_basic'] == "me"}selected{/if} value="me">
                                            {__("Just Me")}
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-5 control-label" for="privacy_work">
                                    {__("Who can see your")} {__("work info")}
                                </label>
                                <div class="col-sm-3">
                                    <select class="form-control" name="privacy_work" id="privacy_work">
                                        <option {if $user->_data['user_privacy_work'] == "public"}selected{/if} value="public">
                                            {__("Everyone")}
                                        </option>
                                        <option {if $user->_data['user_privacy_work'] == "friends"}selected{/if} value="friends">
                                            {__("Friends")}
                                        </option>
                                        <option {if $user->_data['user_privacy_work'] == "me"}selected{/if} value="me">
                                            {__("Just Me")}
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-5 control-label" for="privacy_location">
                                    {__("Who can see your")} {__("location info")}
                                </label>
                                <div class="col-sm-3">
                                    <select class="form-control" name="privacy_location" id="privacy_location">
                                        <option {if $user->_data['user_privacy_location'] == "public"}selected{/if} value="public">
                                            {__("Everyone")}
                                        </option>
                                        <option {if $user->_data['user_privacy_location'] == "friends"}selected{/if} value="friends">
                                            {__("Friends")}
                                        </option>
                                        <option {if $user->_data['user_privacy_location'] == "me"}selected{/if} value="me">
                                            {__("Just Me")}
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-5 control-label" for="privacy_education">
                                    {__("Who can see your")} {__("education info")}
                                </label>
                                <div class="col-sm-3">
                                    <select class="form-control" name="privacy_education" id="privacy_education">
                                        <option {if $user->_data['user_privacy_education'] == "public"}selected{/if} value="public">
                                            {__("Everyone")}
                                        </option>
                                        <option {if $user->_data['user_privacy_education'] == "friends"}selected{/if} value="friends">
                                            {__("Friends")}
                                        </option>
                                        <option {if $user->_data['user_privacy_education'] == "me"}selected{/if} value="me">
                                            {__("Just Me")}
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-5 control-label" for="privacy_other">
                                    {__("Who can see your")} {__("other info")}
                                </label>
                                <div class="col-sm-3">
                                    <select class="form-control" name="privacy_other" id="privacy_other">
                                        <option {if $user->_data['user_privacy_other'] == "public"}selected{/if} value="public">
                                            {__("Everyone")}
                                        </option>
                                        <option {if $user->_data['user_privacy_other'] == "friends"}selected{/if} value="friends">
                                            {__("Friends")}
                                        </option>
                                        <option {if $user->_data['user_privacy_other'] == "me"}selected{/if} value="me">
                                            {__("Just Me")}
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-5 control-label" for="privacy_friends">
                                    {__("Who can see your")} {__("friends")}
                                </label>
                                <div class="col-sm-3">
                                    <select class="form-control" name="privacy_friends" id="privacy_friends">
                                        <option {if $user->_data['user_privacy_friends'] == "public"}selected{/if} value="public">
                                            {__("Everyone")}
                                        </option>
                                        <option {if $user->_data['user_privacy_friends'] == "friends"}selected{/if} value="friends">
                                            {__("Friends")}
                                        </option>
                                        <option {if $user->_data['user_privacy_friends'] == "me"}selected{/if} value="me">
                                            {__("Just Me")}
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-5 control-label" for="privacy_photos">
                                    {__("Who can see your")} {__("photos")}
                                </label>
                                <div class="col-sm-3">
                                    <select class="form-control" name="privacy_photos" id="privacy_photos">
                                        <option {if $user->_data['user_privacy_photos'] == "public"}selected{/if} value="public">
                                            {__("Everyone")}
                                        </option>
                                        <option {if $user->_data['user_privacy_photos'] == "friends"}selected{/if} value="friends">
                                            {__("Friends")}
                                        </option>
                                        <option {if $user->_data['user_privacy_photos'] == "me"}selected{/if} value="me">
                                            {__("Just Me")}
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-5 control-label" for="privacy_pages">
                                    {__("Who can see your")} {__("liked pages")}
                                </label>
                                <div class="col-sm-3">
                                    <select class="form-control" name="privacy_pages" id="privacy_pages">
                                        <option {if $user->_data['user_privacy_pages'] == "public"}selected{/if} value="public">
                                            {__("Everyone")}
                                        </option>
                                        <option {if $user->_data['user_privacy_pages'] == "friends"}selected{/if} value="friends">
                                            {__("Friends")}
                                        </option>
                                        <option {if $user->_data['user_privacy_pages'] == "me"}selected{/if} value="me">
                                            {__("Just Me")}
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-5 control-label" for="privacy_groups">
                                    {__("Who can see your")} {__("joined groups")}
                                </label>
                                <div class="col-sm-3">
                                    <select class="form-control" name="privacy_groups" id="privacy_groups">
                                        <option {if $user->_data['user_privacy_groups'] == "public"}selected{/if} value="public">
                                            {__("Everyone")}
                                        </option>
                                        <option {if $user->_data['user_privacy_groups'] == "friends"}selected{/if} value="friends">
                                            {__("Friends")}
                                        </option>
                                        <option {if $user->_data['user_privacy_groups'] == "me"}selected{/if} value="me">
                                            {__("Just Me")}
                                        </option>
                                    </select>
                                </div>
                            </div>
                            {*<div class="form-group">
                                <label class="col-sm-5 control-label" for="privacy_events">
                                    {__("Who can see your")} {__("joined events")}
                                </label>
                                <div class="col-sm-3">
                                    <select class="form-control" name="privacy_events" id="privacy_events">
                                        <option {if $user->_data['user_privacy_events'] == "public"}selected{/if} value="public">
                                            {__("Everyone")}
                                        </option>
                                        <option {if $user->_data['user_privacy_events'] == "friends"}selected{/if} value="friends">
                                            {__("Friends")}
                                        </option>
                                        <option {if $user->_data['user_privacy_events'] == "me"}selected{/if} value="me">
                                            {__("Just Me")}
                                        </option>
                                    </select>
                                </div>
                            </div>*}
                            <div class="form-group">
                                <div class="col-sm-7 col-sm-offset-5">
                                    <button type="submit" class="btn btn-primary">{__("Save Changes")}</button>
                                </div>
                            </div>

                            <!-- success -->
                            <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                            <!-- success -->

                            <!-- error -->
                            <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                            <!-- error -->
                        </form>
                    </div>
                {elseif $view == "security"}
                    <div class="panel-heading with-icon">
                        <!-- panel title -->
                        <i class="fa fa-shield-alt pr5 panel-icon"></i>
                        <strong>{__("Security Settings")}</strong>
                        <!-- panel title -->
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>{__("ID")}</th>
                                    <th>{__("Browser")}</th>
                                    <th>{__("OS")}</th>
                                    <th>{__("Date")}</th>
                                    <th>{__("IP")}</th>
                                    <th>{__("Actions")}</th>
                                </tr>
                                </thead>
                                <tbody>
                                {foreach $sessions as $session}
                                    <tr {if $session['session_token'] == $user->_data['active_session']}class="success"{/if}>
                                        <td>{$session@iteration}</td>
                                        <td>{$session['user_browser']} {if $session['session_token'] == $user->_data['active_session']}<span class="label label-info">{__("Active Session")}</span>{/if}</td>
                                        <td>{$session['user_os']}</td>
                                        <td>
                                            <span class="js_moment" data-time="{$session['session_date']}">{$session['session_date']}</span>
                                        </td>
                                        <td>{$session['user_ip']}</td>
                                        <td>
                                            <button data-toggle="tooltip" data-placement="top" title='{__("End Session")}' class="btn btn-xs btn-danger js_session-deleter" data-id="{$session['session_id']}">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </td>
                                    </tr>
                                {/foreach}
                                </tbody>
                            </table>
                        </div>
                    </div>
                {elseif $view == "linked"}
                    <div class="panel-heading with-icon">
                        <!-- panel title -->
                        <i class="fa fa-share-alt pr5 panel-icon"></i>
                        <strong>{__("Linked Accounts")}</strong>
                        <!-- panel title -->
                    </div>
                    <div class="panel-body">
                        <ul>
                            {if $system['facebook_login_enabled']}
                                <li class="feeds-item">
                                    <div class="data-container">
                                        <div class="data-avatar">
                                            <i class="fa fa-facebook-square" style="color: #3B579D"></i>
                                        </div>
                                        <div class="data-content">
                                            <div class="pull-right flip">
                                                {if $user->_data['facebook_connected']}
                                                    <a class="btn btn-danger" href="{$system['system_url']}/revoke/facebook">{__("Disconnect")}</a>
                                                {else}
                                                    <a class="btn btn-primary" href="{$system['system_url']}/connect/facebook">{__("Connect")}</a>
                                                {/if}
                                            </div>
                                            <div>
                                                <div class="name mt5 text-primary">
                                                    {__("Facebook")}
                                                </div>
                                                {if $user->_data['facebook_connected']}
                                                    {__("Your account is connected to")} {__("Facebook")}
                                                {else}
                                                    {__("Connect your account to")} {__("Facebook")}
                                                {/if}
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            {/if}
                            {if $system['twitter_login_enabled']}
                                <li class="feeds-item">
                                    <div class="data-container">
                                        <div class="data-avatar">
                                            <i class="fa fa-twitter-square" style="color: #55ACEE"></i>
                                        </div>
                                        <div class="data-content">
                                            <div class="pull-right flip">
                                                {if $user->_data['twitter_connected']}
                                                    <a class="btn btn-danger" href="{$system['system_url']}/revoke/twitter">{__("Disconnect")}</a>
                                                {else}
                                                    <a class="btn btn-primary" href="{$system['system_url']}/connect/twitter">{__("Connect")}</a>
                                                {/if}
                                            </div>
                                            <div>
                                                <div class="name mt5 text-primary">
                                                    {__("Twitter")}
                                                </div>
                                                {if $user->_data['twitter_connected']}
                                                    {__("Your account is connected to")} {__("Twitter")}
                                                {else}
                                                    {__("Connect your account to")} {__("Twitter")}
                                                {/if}
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            {/if}
                            {if $system['google_login_enabled']}
                                <li class="feeds-item">
                                    <div class="data-container">
                                        <div class="data-avatar">
                                            <i class="fa fa-google-plus-square" style="color: #DC4A38"></i>
                                        </div>
                                        <div class="data-content">
                                            <div class="pull-right flip">
                                                {if $user->_data['google_connected']}
                                                    <a class="btn btn-danger" href="{$system['system_url']}/revoke/google">{__("Disconnect")}</a>
                                                {else}
                                                    <a class="btn btn-primary" href="{$system['system_url']}/connect/google">{__("Connect")}</a>
                                                {/if}
                                            </div>
                                            <div>
                                                <div class="name mt5 text-primary">
                                                    {__("Google")}
                                                </div>
                                                {if $user->_data['google_connected']}
                                                    {__("Your account is connected to")} {__("Google")}
                                                {else}
                                                    {__("Connect your account to")} {__("Google")}
                                                {/if}
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            {/if}
                            {if $system['instagram_login_enabled']}
                                <li class="feeds-item">
                                    <div class="data-container">
                                        <div class="data-avatar">
                                            <i class="fa fa-instagram" style="color: #3f729b"></i>
                                        </div>
                                        <div class="data-content">
                                            <div class="pull-right flip">
                                                {if $user->_data['instagram_connected']}
                                                    <a class="btn btn-danger" href="{$system['system_url']}/revoke/instagram">{__("Disconnect")}</a>
                                                {else}
                                                    <a class="btn btn-primary" href="{$system['system_url']}/connect/instagram">{__("Connect")}</a>
                                                {/if}
                                            </div>
                                            <div>
                                                <div class="name mt5 text-primary">
                                                    {__("Instagram")}
                                                </div>
                                                {if $user->_data['instagram_connected']}
                                                    {__("Your account is connected to")} {__("Instagram")}
                                                {else}
                                                    {__("Connect your account to")} {__("Instagram")}
                                                {/if}
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            {/if}
                            {if $system['linkedin_login_enabled']}
                                <li class="feeds-item">
                                    <div class="data-container">
                                        <div class="data-avatar">
                                            <i class="fa fa-linkedin" style="color: #1A84BC"></i>
                                        </div>
                                        <div class="data-content">
                                            <div class="pull-right flip">
                                                {if $user->_data['linkedin_connected']}
                                                    <a class="btn btn-danger" href="{$system['system_url']}/revoke/linkedin">{__("Disconnect")}</a>
                                                {else}
                                                    <a class="btn btn-primary" href="{$system['system_url']}/connect/linkedin">{__("Connect")}</a>
                                                {/if}
                                            </div>
                                            <div>
                                                <div class="name mt5 text-primary">
                                                    {__("Linkedin")}
                                                </div>
                                                {if $user->_data['linkedin_connected']}
                                                    {__("Your account is connected to")} {__("Linkedin")}
                                                {else}
                                                    {__("Connect your account to")} {__("Linkedin")}
                                                {/if}
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            {/if}
                            {if $system['vkontakte_login_enabled']}
                                <li class="feeds-item">
                                    <div class="data-container">
                                        <div class="data-avatar">
                                            <i class="fa fa-vk" style="color: #527498"></i>
                                        </div>
                                        <div class="data-content">
                                            <div class="pull-right flip">
                                                {if $user->_data['vkontakte_connected']}
                                                    <a class="btn btn-danger" href="{$system['system_url']}/revoke/vkontakte">{__("Disconnect")}</a>
                                                {else}
                                                    <a class="btn btn-primary" href="{$system['system_url']}/connect/vkontakte">{__("Connect")}</a>
                                                {/if}
                                            </div>
                                            <div>
                                                <div class="name mt5 text-primary">
                                                    {__("Vkontakte")}
                                                </div>
                                                {if $user->_data['vkontakte_connected']}
                                                    {__("Your account is connected to")} {__("Vkontakte")}
                                                {else}
                                                    {__("Connect your account to")} {__("Vkontakte")}
                                                {/if}
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            {/if}
                        </ul>
                    </div>
                {elseif $view == "blocking"}
                    <div class="panel-heading with-icon">
                        <!-- panel title -->
                        <i class="fa fa-minus-circle pr5 panel-icon"></i>
                        <strong>{__("Manage Blocking")}</strong>
                        <!-- panel title -->
                    </div>
                    <div class="panel-body">
                        <div class="alert alert-info">
                            <i class="fa fa-info-circle fa-lg mr10"></i>{__("Once you block someone, that person can no longer see things you post on your timeline")}<br>
                        </div>

                        {if count($blocks) > 0}
                            <ul>
                                {foreach $blocks as $_user}
                                    {include file='__feeds_user.tpl' _connection="blocked"}
                                {/foreach}
                            </ul>
                        {else}
                            <p class="text-center text-muted">
                                {__("No blocked users")}
                            </p>
                        {/if}

                        {if count($blocks) >= $system['max_results']}
                            <!-- see-more -->
                            <div class="alert alert-info see-more js_see-more" data-get="blocks">
                                <span>{__("See More")}</span>
                                <div class="loader loader_small x-hidden"></div>
                            </div>
                            <!-- see-more -->
                        {/if}
                    </div>
                {elseif $view == "delete"}
                    <div class="panel-heading with-icon">
                        <!-- panel title -->
                        <i class="fa fa-trash pr5 panel-icon"></i>
                        <strong>{__("Delete Account")}</strong>
                        <!-- panel title -->
                    </div>
                    <div class="panel-body">
                        <div class="alert alert-warning">
                            {__("Once you delete your account you will no longer can access it again")}<br>
                            {__("Note: All your data will be deleted")}
                        </div>

                        <div class="text-center">
                            <button class="btn btn-danger js_delete-user">{__("Delete My Account")}</button>
                        </div>

                        {if count($blocks) >= $system['max_results']}
                            <!-- see-more -->
                            <div class="alert alert-info see-more js_see-more" data-get="blocks">
                                <span>{__("See More")}</span>
                                <div class="loader loader_small x-hidden"></div>
                            </div>
                            <!-- see-more -->
                        {/if}
                    </div>
                {/if}

            </div>
        </div>

    </div>
</div>
<!-- page content -->

{include file='_footer.tpl'}

<!-- HTTPS required. HTTP will give a 403 forbidden response -->
<script src="https://sdk.accountkit.com/vi_VN/sdk.js"></script>

{literal}
    <script>
        var timeModal = 1500;
        // initialize Account Kit with CSRF protection
        AccountKit_OnInteractive = function(){
            AccountKit.init(
                {
                    appId:1638502962897148,
                    state:"coniuahihi",
                    version:"v1.1",
                    fbAppEventsEnabled:true,
                    redirect:"https://coniu.vn"
                }
            );
        };

        var api = [];
        api['core/verify']  = ajax_path+"core/verify_phone.php";
        api['users/check_email_verified']  = ajax_path+"users/check_email_verified.php";

        // login callback
        function loginCallback(response) {
            if (response.status === "PARTIALLY_AUTHENTICATED") {

                $.post(api['core/verify'], {
                    'do': 'edit_phone',
                    'code': response.code,
                    'csrf': response.state
                }, function (response) {

                    if (response.callback) {
                        eval(response.callback);
                    } else if (response.error) {
                        modal('#modal-error', {title: __['Error'], message: response.message});
                    } else if (response.success) {
                        modal('#modal-success', {title: __['Success'], message: response.message});
                        modal_hidden(timeModal);
                    } else {
                        window.location.reload();
                    }

                }, 'json');
            }
            else if (response.status === "NOT_AUTHENTICATED") {
                // handle authentication failure
                console.log("Authentication failure");
            }
            else if (response.status === "BAD_PARAMS") {
                // handle bad parameters
                console.log("Bad parameters");
            }
        }

        // phone form submission handler
        function phoneVerification() {
            //var countryCode = document.getElementById("country_code").value;
            var phoneNumber = document.getElementById("phone_number").value;
            AccountKit.login(
                'PHONE',
                {countryCode: '+84', phoneNumber: phoneNumber}, // will use default values if not specified
                loginCallback
            );
        }

        // phone form submission handler
        function finishEmailVerify() {
            $("#loading_full_screen").removeClass('hidden');
            $.post(api['users/check_email_verified'], {}, function (response) {

                $("#loading_full_screen").addClass('hidden');
                console.log(response.is_verified);
                if (response.is_verified) {
                    $('#info-email-activation').addClass('x-hidden');
                    $('#mail').val(response.email);
                } else if (response.error){
                    modal('#modal-error', {title: __['Error'], message: response.message});
                }

            }, 'json');

        }

    </script>
{/literal}