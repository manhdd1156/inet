<!-- ads -->
{include file='_ads.tpl' _ads=$ads_master['footer'] _master=true}
<!-- ads -->

<!-- footer -->
<div class="container">
	<div class="row footer">
		<div class="col-lg-6 col-md-6 col-sm-6">
			&copy; {'Y'|date} {$system['system_title']} · <span class="text-link" data-toggle="modal" data-url="#translator">{$system['language']['title']}</span> |
			<a href="{$system['system_url']}/privacy">{__("Privacy")}</a> | <a href="{$system['system_url']}/terms">{__("Terms")}</a>
		</div>

		<div class="col-lg-6 col-md-6 col-sm-6 links">
            {if count($static_pages) > 0}
                {foreach $static_pages as $static_page}
				<a href="{$system['system_url']}/static/{$static_page['page_url']}">
                    {$static_page['page_title']}
					</a>{if !$static_page@last} · {/if}
                {/foreach}
            {/if}

            {if $system['contact_enabled']}
				·
				<a href="{$system['system_url']}/contacts">
                    {__("Contacts")}
				</a>
            {/if}

            {*{if $system['system_public']}*}
				{*<!--Coniu - Xóa link thư mục-->				·*}
				{*<a href="{$system['system_url']}/search">*}
                    {*{__("Search")}*}
				{*</a>*}
            {*{/if}*}

            {if $system['directory_enabled']}
				·
				<a href="{$system['system_url']}/directory">
                    {__("Directory")}
				</a>
            {/if}
            {if $system['market_enabled']}
				·
				<a href="{$system['system_url']}/market">
                    {__("Market")}
				</a>
            {/if}
		</div>
	</div>
</div>
<!-- footer -->

<div id="app_android" style="display: none" class="app_pop">
	<div class="android_down app_down_box" {if !$cookie_app}style="display: none"{/if}>
		<a href="{$smarty.const.GOOGLEPLAY_URL}" target="_blank"> <img
					class=""
					src="{$system['system_url']}/content/themes/{$system['theme']}/images/favicon.png"
					alt="Ứng dụng mầm non Coniu CH Play"/> {__("Download Mascom - Edu App")}</a>
		<div class="down_app_hide" data-id="{$user->_data['user_id']}">
			<i class="fa fa-close"></i>
		</div>
	</div>
</div>
<div id="app_ios" style="display: none" class="app_pop">
	<div class="ios_down app_down_box" {if !$cookie_app}style="display: none"{/if}>
		<a href="{$smarty.const.APPSTORE_URL}" target="_blank"><img class=""
																	src="{$system['system_url']}/content/themes/{$system['theme']}/images/favicon.png"
																	alt="Ứng dụng mầm non Coniu App store"/> {__("Download Mascom - Edu App")}</a>
		<div class="down_app_hide" data-id="{$user->_data['user_id']}">
			<i class="fa fa-close"></i>
		</div>
	</div>
</div>
<script>
    function getMobileOperatingSystem() {
        var userAgent = navigator.userAgent || navigator.vendor || window.opera;
        // Windows Phone must come first because its UA also contains "Android"
        if (/windows phone/i.test(userAgent)) {
            return "Windows Phone";
        }

        if (/android/i.test(userAgent)) {
            document.getElementById("app_android").style.display = 'block';
            return "Android";
        }

        // iOS detection from: http://stackoverflow.com/a/9039885/177710
        if (/iPad|iPhone|iPod/i.test(userAgent)) {
            document.getElementById("app_ios").style.display = 'block';
            return "iOS";
        }
        return "Web";
    }
    getMobileOperatingSystem();
    // if(x == Adroi)
    // document.getElementById("osmobile").innerHTML = x;
</script>

</div>
<!-- main wrapper -->

<!-- Dependencies CSS [Twemoji-Awesome|Flag-Icon] -->
{if !$user->_logged_in}
	<link rel="stylesheet" href="{$system['system_url']}/includes/assets/css/font-awesome/css/fontawesome-all.min.css">
{/if}
<link rel="stylesheet" href="{$system['system_url']}/includes/assets/css/twemoji-awesome/twemoji-awesome.min.css">
<link rel="stylesheet" href="{$system['system_url']}/includes/assets/css/flag-icon/css/flag-icon.min.css">
<!-- Dependencies CSS [Twemoji-Awesome|Flag-Icon] -->

<!-- JS Files -->
{include file='_js_files.tpl'}
<!-- JS Files -->

<!-- JS Templates -->
{include file='_js_templates.tpl'}
<!-- JS Templates -->

<!-- Analytics Code -->
{if $system['analytics_code']}{html_entity_decode($system['analytics_code'], ENT_QUOTES)}{/if}
<!-- Analytics Code -->


{if $user->_logged_in}
	<!-- Notification -->
	<audio id="notification_sound">
		<source src="{$system['system_url']}/includes/assets/sounds/notification.mp3" type="audio/mpeg">
	</audio>
	<!-- Notification -->
	<!-- Chat -->
	<audio id="chat_sound">
		<source src="{$system['system_url']}/includes/assets/sounds/chat.mp3" type="audio/mpeg">
	</audio>
	<!-- Chat -->
	<!-- Call -->
	<!-- Coniu - tam bo call.mp3 -->
{*	<audio id="call_sound">*}
{*		<source src="{$system['system_url']}/includes/assets/sounds/call.mp3" type="audio/mpeg">*}
{*	</audio>*}
	<!-- Call -->
	<!-- Video -->
	<audio id="video_sound">
		<source src="{$system['system_url']}/includes/assets/sounds/video.mp3" type="audio/mpeg">
	</audio>
	<!-- Video -->
{/if}


</body>
</html>

