{include file = "_head_new.tpl"}
{include file = '_header_new.tpl'}

<!--=== Breadcrumbs ===-->
<div class="breadcrumbs">
    <div class="container">
        <h1 class="pull-left">{__("Terms")}</h1>
        <ul class="pull-right breadcrumb">
            <li><a href="{$system['system_url']}">{__("Home")}</a></li>
            <li class="active">{__("Terms")}</li>
        </ul>
    </div>
</div><!--/breadcrumbs-->
<!--=== End Breadcrumbs ===-->

<!--=== Content Part ===-->
<div class="container content">
    <div class="row-fluid privacy">
        <p>
            Vui lòng đọc kỹ Thỏa Thuận Sử Dụng (“Thỏa Thuận”) trước khi bạn tiến hành tải, cài đặt, sử dụng tất cả hoặc bất kỳ phần nào của Phần mềm mầm non “Mascom Edu” (“Phần Mềm”: bao gồm nhưng không giới hạn phần mềm, các file và các tài liệu liên quan) hoặc sử dụng các dịch vụ do Công ty Cổ phần NOGA cung cấp để kết nối đến Phần Mềm. Bạn chấp thuận và đồng ý bị ràng buộc bởi các quy định và điều kiện trong Thỏa Thuận này khi thực hiện các thao tác trên đây. Trường hợp bạn không đồng ý với bất kỳ điều khoản sử dụng nào của chúng tôi (phiên bản này và các phiên bản cập nhật), bạn vui lòng không tải, cài đặt, sử dụng Phần Mềm hoặc tháo gỡ Phần Mềm ra khỏi thiết bị di động của bạn.
        </p>
        <p style = "font-weight: bold">1. Quyền riêng tư</p>
        <p>
            Quyền riêng tư của bạn rất quan trọng với chúng tôi. Chúng tôi đã thiết kế Chính sách dữ liệu của mình nhằm tiết lộ những thông tin quan trọng về cách bạn có thể sử dụng Mascom Edu để chia sẻ với người khác cũng như cách chúng tôi thu thập và có thể sử dụng nội dung, thông tin của bạn. Chúng tôi khuyến khích bạn đọc Chính sách dữ liệu và sử dụng chính sách này để giúp bạn đưa ra các quyết định sáng suốt.
        </p>
        <p style = "font-weight: bold">2. Cập nhật</p>
        <p>
            Thỏa Thuận này có thể được cập nhật thường xuyên bởi Mascom Edu, phiên bản cập nhật sẽ được chúng tôi công bố tại website https://edu.mascom.com.vn Phiên bản cập nhật sẽ thay thế cho các quy định và điều kiện trong thỏa thuận ban đầu. Bạn có thể truy cập vào Phần Mềm hoặc vào website trên đây để xem nội dung chi tiết của phiên bản cập nhật.
        </p>
        <p style = "font-weight: bold">3. Giới thiệu về phần mềm</p>
        <p>
            Mascom Edu là phần mềm tương tác mầm non kết nối giữa nhà trường và phụ huynh. Phần mềm giúp nhằm giúp Ban giám hiệu nhà trường, Giáo viên và Phụ huynh trao đổi, tương tác thông tin về các con một cách dễ dàng và nhanh chóng thông qua các chức năng chính như: Nhắn tin, nhận thông báo, điểm danh, gửi thuốc, xin nghỉ, học phí, quản lý trẻ, giáo viên. Phần mềm hỗ trợ tất cả các nền tảng Android, IOS và trên Web. Thiết bị sử dụng phần mềm điện thoại, máy tính và máy tính bảng.
        </p>
        <p style = "font-weight: bold">4. Quyền sở hữu phần mềm</p>
        <p>
            Phần Mềm này được phát triển và sở hữu bởi Công ty Cổ phần NOGA, tất cả các quyền sở hữu trí tuệ liên quan đến Phần Mềm (bao gồm nhưng không giới hạn mã nguồn, hình ảnh, dữ liệu, thông tin, nội dung chứa đựng trong Phần Mềm; các sửa đổi, bổ sung, cập nhật của Phần Mềm) và các tài liệu hướng dẫn liên quan (nếu có) sẽ thuộc quyền sở hữu duy nhất bởi Công ty Cổ phần NOGA và không cá nhân, tổ chức nào được phép sao chép, tái tạo, phân phối, hoặc hình thức khác xâm phạm tới quyền của chủ sở hữu nếu không có sự đồng ý và cho phép bằng văn bản của Công ty Cổ phần NOGA.
        </p>
        <p style = "font-weight: bold">5. Tài khoản</p>
        <p>
            Để sử dụng Phần Mềm bạn phải tạo một tài khoản theo yêu cầu của chúng tôi, bạn cam kết rằng việc sử dụng tài khoản phải tuân thủ các quy định của Công ty Cổ phần NOGA, đồng thời tất cả các thông tin bạn cung cấp cho chúng tôi là đúng, chính xác, đầy đủ với tại thời điểm được yêu cầu. Mọi quyền lợi và nghĩa vụ của bạn sẽ căn cứ trên thông tin tài khoản bạn đã đăng ký, do đó nếu có bất kỳ thông tin sai lệch nào chúng tôi sẽ không chịu trách nhiệm trong trường hợp thông tin đó làm ảnh hưởng hoặc hạn chế quyền lợi của bạn.
        </p>
        <p style = "font-weight: bold">6. Quy Định Sử Dụng Tài Khoản</p>
        <p>
            Chúng tôi nỗ lực hết sức mình để giữ cho Mascom Edu an toàn nhưng chúng tôi không thể đảm bảo điều đó. Chúng tôi cần sự giúp đỡ của bạn để giữ Mascom Edu an toàn với các cam kết sau từ phía bạn: <br/>

            1.	Bạn sẽ không đăng nội dung: về các vấn đề chính trị, tôn giáo. <br/>
            2.	Bạn sẽ không đăng nội dung: có ngôn từ kích động thù địch, mang tính đe dọa hay khiêu dâm; kích động bạo lực hoặc chứa ảnh khỏa thân hoặc bạo lực hay nội dung quá mức bạo lực (bao gồm quảng cáo). <br/>
            3.	Bạn sẽ không dùng Mascom Edu để làm bất kỳ điều gì vi phạm pháp luật Việt Nam, sai trái, độc hại hoặc phân biệt đối xử. <br/>
            4.	Bạn sẽ không đăng các thông tin thương mại trái phép (như spam) lên Mascom Edu.<br/>
            5.	Bạn sẽ không thu thập nội dung hoặc thông tin của người dùng hoặc truy cập vào Mascom Edu bằng các phương thức được tự động hóa (như trình khai thác địa chỉ email, rô-bốt, gián điệp hoặc trình thu thập) mà không có sự cho phép trước của chúng tôi. <br/>
            6.	Bạn sẽ không tham gia vào hoạt động tiếp thị đa cấp bất hợp pháp như mô hình kinh doanh dạng kim tự tháp trên Mascom Edu. <br/>
            7.	Bạn sẽ không tải lên vi-rút hoặc mã độc hại khác. <br/>
            8.	Bạn sẽ không thu thập thông tin đăng nhập hoặc truy cập vào tài khoản của người khác. <br/>
            9.	Bạn sẽ không bắt nạt, hăm dọa hoặc quấy rối bất kỳ người dùng nào. <br/>
            10.	Bạn sẽ không làm bất kỳ điều gì có thể làm vô hiệu hóa, quá tải hoặc ảnh hưởng đến hoạt động hay giao diện của Mascom Edu, chẳng hạn như phủ nhận tấn công dịch vụ hoặc can thiệp vào việc hiển thị trang hay các chức năng khác của Mascom Edu. <br/>
            11.	Bạn sẽ không tạo điều kiện hoặc khuyến khích bất kỳ hành vi nào vi phạm Tuyên bố này hay chính sách của chúng tôi.

        </p>
        <p style = "font-weight: bold">7. Xử lý vi phạm	</p>
        <p>
            Trường hợp bạn vi phạm bất kỳ quy định nào trong Thỏa Thuận này, Công ty Cổ phần NOGA có quyền ngay lập tức khóa tài khoản của bạn và/hoặc xóa bỏ toàn bộ các thông tin, nội dung vi phạm, đồng thời tùy thuộc vào tính chất, mức độ vi phạm bạn sẽ phải chịu trách nhiệm trước cơ quan có thẩm quyền, Công ty Cổ phần NOGA và bên thứ ba về mọi thiệt hại gây ra bởi hoặc xuất phát từ hành vi vi phạm của bạn.
        </p>
        <p style = "font-weight: bold">8. Quyền truy cập và Thu thập thông tin	</p>
        <p>
            Khi sử dụng Phần Mềm, bạn thừa nhận rằng chúng tôi có quyền sử dụng những API hệ thống sau để truy cập vào dữ liệu trên điện thoại của bạn: (1) Đọc và ghi vào danh bạ điện thoại, (2) Lấy vị trí hiện tại của bạn khi được sự đồng ý, (3) Ghi dữ liệu của Phần Mềm lên thẻ nhớ, (4) Truy cập vào Internet từ thiết bị của bạn. Tất cả các truy cập này đều được chúng tôi thực hiện sau khi có sự đồng ý của bạn, vì vậy bạn cam kết và thừa nhận rằng, khi bạn đã cấp quyền cho chúng tôi, bạn sẽ không có bất kỳ khiếu nại nào đối với Công ty Cổ phần NOGA về việc truy cập này. <br/>
            &emsp;Cùng với quyền truy cập, chúng tôi sẽ thu thập các thông tin sau của bạn <br/>
            &emsp;-	Thông tin cá nhân: bao gồm các thông tin bạn cung cấp cho chúng tôi để xác nhận tài khoản như tên, số điện thoại, số chứng minh nhân dân, địa chỉ email; <br/>
            &emsp;-	Thông tin chung: như các thông tin về cấu hình điện thoại của bạn, thông tin phiên bản Mascom Edu mà bạn đang sử dụng cho điện thoại của mình; <br/>
            &emsp;-	Thông tin vị trí của bạn: dữ liệu về vị trí địa lý của bạn sẽ được lưu trữ trên máy chủ nhằm giúp bạn sử dụng chức năng tìm kiếm của Phần Mềm; <br/>
            &emsp;-	Danh bạ điện thoại: chúng tôi sẽ lưu trữ danh bạ điện thoại của bạn trên máy chủ nhằm hỗ trợ tốt nhất cho bạn trong việc sử dụng Phần Mềm và tránh trường hợp bạn bị mất dữ liệu. Chúng tôi cam kết sẽ tôn trọng và không sử dụng danh bạ điện thoại của bạn vì bất kỳ mục đích nào nếu không có sự đồng ý của bạn; <br/>
            &emsp;-	Thông tin dữ liệu trẻ, giáo viên: Chúng tôi sẽ lưu trữ thông tin trên hệ thống nhưng cam kết không sử dụng dữ liệu vì bất kỳ mục đích nào nếu không có sự đồng ý của bạn.<br/>
            &emsp;-	Chúng tôi không sử dụng bất kỳ biện pháp nào để theo dõi nội dung tin nhắn, thông tin quản lý của trường gồm học phí, điểm danh, số lượng trẻ, giáo viên, trao đổi hoặc hình thức khác nhằm theo dõi người dùng khi sử dụng Phần Mềm này.<br/>
        </p>
        <p style = "font-weight: bold">9. Cam kết bảo mật thông tin	</p>
        <p>
            Công ty Cổ phần NOGA sử dụng các phương thức truyền tin an toàn https và mã hóa để truyền tải và lưu trữ các dữ liệu cá nhân và giao tiếp của bạn. Chúng tôi cam kết giữ bí mật tất cả thông tin mà bạn cung cấp cho Công ty Cổ phần NOGA hoặc chúng tôi thu thập từ bạn và không tiết lộ với bất kỳ bên thứ ba nào trừ khi có yêu cầu từ Cơ quan Nhà nước có thẩm quyền.
        </p>
        <p style = "font-weight: bold">10. Quảng cáo và các nội dung thương mại được phân phối bởi NOGA	</p>
        <p>
            Khi sử dụng phần mềm mầm non Mascom Edu, bạn thừa nhận rằng chúng tôi có quyền sử dụng các thông tin không định danh của bạn nhằm cung cấp các nội dung quảng cáo đúng đối tượng.
        </p>
        <p style = "font-weight: bold">11. Thanh toán	</p>
        <p>
            Nếu bạn thanh toán trên Mascom Edu, bạn đồng ý với Điều khoản thanh toán của chúng tôi trừ khi có điều khoản khác áp dụng.
        </p>
        <p style = "font-weight: bold">12. Liên hệ với chúng tôi	</p>
        <p>
            Mọi thông tin hãy liên hệ với Công ty Cổ phần NOGA qua hoặc Ban Quản trị Hệ thống Phần mềm mầm non Mascom Edu tại địa chỉ: <br/>
            -	Website: <a href = "https://mascom.vn/">https://mascom.vn/</a> hoặc <a href = "https://edu.mascom.com.vn/">https://edu.mascom.com.vn/</a><br/>
            -	Email: <a href="mailto:contact@mascom.vn" title="Liên hệ Ban quan trị Mascom Edu">contact@mascom.vn</a>
        </p>

    </div><!--/row-fluid-->
</div><!--/container-->
<!--=== End Content Part ===-->

{include file = '_footer_new.tpl'}