<div class="{if $_small}col-xs-6 col-sm-3{else}col-xs-6 col-sm-2{/if}" style="position: relative">
    <div class="fa-border">
        <a target="_blank" class="pg_photo {if !$_small}large{/if}" href="{$system['system_uploads']}/{$photo['source_file_path']}" style="background-image:url({$system['system_uploads']}/{$photo['source_file_path']});">
        </a>
        <div class="" style="position: absolute; top: 4px; right: 4px">
            <a href="#" class="js_child-journal-delete" data-id="{$photo['child_journal_id']}" data-child="{$child_parent_id}" data-handle="delete">
                <img src="{$system['system_url']}/content/themes/{$system['theme']}/images/delete.png"/>
            </a>
        </div>
    </div>
</div>