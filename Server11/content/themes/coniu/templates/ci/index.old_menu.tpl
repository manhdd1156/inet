<!-- Phần left menu liên quan đến Group và Pages -->
{if $user->_data['user_group'] < 3 || $system['pages_enabled']}
    <!-- pages -->
    <li class="ptb5">
        <small class="text-muted">{__("pages")|upper}</small>
    </li>

    {if count($pages) > 0}
        {foreach $pages as $page}
            <!-- ConIu - nếu page là truong ==> bo qua -->
            {if $page['page_category'] != $smarty.const.SCHOOL_CATEGORY_ID}
                <li>
                    <a href="{$system['system_url']}/pages/{$page['page_name']}">
                        <img src="{$page['page_picture']}" alt="">
                        <span>{$page['page_title']}</span>
                    </a>
                </li>
            {/if}
        {/foreach}
    {/if}

    <li {if $view == "create_page"}class="active"{/if}>
        <a href="{$system['system_url']}/create/page"><i class="fa fa-flag fa-fw pr10"></i> {__("Create Page")}</a>
    </li>
    <li {if $view == "pages"}class="active"{/if}>
        <a href="{$system['system_url']}/pages"><i class="fa fa-cubes fa-fw pr10"></i> {__("Manage Pages")}</a>
    </li>
    <!-- pages -->
{/if}

{if $user->_data['user_group'] < 3 || $system['groups_enabled']}
    <!-- groups -->
    <li class="ptb5">
        <small class="text-muted">{__("groups")|upper}</small>
    </li>

    {if count($groups) > 0}
        {foreach $groups as $group}
            <!-- ConIu - nếu group là lop ==> bo qua -->
            {if $group['class_level_id'] == 0}
                <li>
                    <a href="{$system['system_url']}/groups/{$group['group_name']}">
                        <img src="{$group['group_picture']}" alt="">
                        <span>{$group['group_title']}</span>
                    </a>
                </li>
            {/if}
        {/foreach}
    {/if}

    <li {if $view == "create_group"}class="active"{/if}>
        <a href="{$system['system_url']}/create/group"><i class="fa fa-users fa-fw pr10"></i> {__("Create Group")}</a>
    </li>
    <li {if $view == "groups"}class="active"{/if}>
        <a href="{$system['system_url']}/groups"><i class="fa fa-cubes fa-fw pr10"></i> {__("Manage Groups")}</a>
    </li>
    <!-- groups -->
{/if}