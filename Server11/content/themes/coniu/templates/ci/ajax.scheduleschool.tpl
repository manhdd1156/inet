{if !empty($results)}
    {if count($results) == 1}
        <input type = "hidden" name = "category_id" value="{$results[0]['category_id']}">
        <table class="table table-striped table-bordered table-hover">
            <thead>
            <tr>
                <th>
                    {__('#')}
                </th>
                <th>
                    {__('Option')}
                </th>
                <th>
                    {__('Time')}
                </th>
                <th>
                    {__('Activity')}
                </th>
                <th>
                    {__('Monday')}
                </th>
                <th>
                    {__('Tuesday')}
                </th>
                <th>
                    {__('Wednesday')}
                </th>
                <th>
                    {__('Thursday')}
                </th>
                <th>
                    {__('Friday')}
                </th>
                <th>
                    {__('Saturday')}
                </th>
            </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
        <div class = "form-group">
            <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
        </div>
    {else}
        <div class = "alert alert-danger mb0 mt10" role="alert">
            <center><p class=""><strong> Có {count($results)} chương trình học đang được áp dụng cho đối tượng này, vui lòng vào danh sách chương trình học, tắt đi những chương trình học không dùng đến. </strong></p></center>
        </div>

    {/if}

{/if}
{if empty($results)}
    {*<input type="hidden" name="do" value="add_no_cate"/>*}
    <table class="table table-striped table-bordered table-hover" id = "myTableSchedule">
            <thead>
            <tr>
                <th>
                    {__('#')}
                </th>
                <th>
                    {__('Option')}
                </th>
                <th>
                    {__('Time')}
                </th>
                <th>
                    {__('Activity')}
                </th>
                <th>
                    {__('Monday')}
                </th>
                <th>
                    {__('Tuesday')}
                </th>
                <th>
                    {__('Wednesday')}
                </th>
                <th>
                    {__('Thursday')}
                </th>
                <th>
                    {__('Friday')}
                </th>
                <th>
                    {__('Saturday')}
                </th>
            </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        1
                    </td>
                    <td>
                        <a class = "btn btn-default js_cate-delete_null"> Xóa </a>
                    </td>
                    <td>
                        <input type = "text" name = "start[]" placeholder="08:00" required style = "padding-left: 10px; height: 35px">
                    </td>
                    <td>
                        <input type="text" name = "subject_name[]" placeholder="Tập thể dục" required style = "padding-left: 10px; height: 35px">
                    </td>
                    <td>
                        <input type="text" name = "subject_detail_mon[]" style = "padding-left: 10px; height: 35px">
                    </td>
                    <td>
                        <input type="text" name = "subject_detail_tue[]" style = "padding-left: 10px; height: 35px">
                    </td>
                    <td>
                        <input type="text" name = "subject_detail_wed[]" style = "padding-left: 10px; height: 35px">
                    </td>
                    <td>
                        <input type="text" name = "subject_detail_thu[]" style = "padding-left: 10px; height: 35px">
                    </td>
                    <td>
                        <input type="text" name = "subject_detail_fri[]" style = "padding-left: 10px; height: 35px">
                    </td>
                    <td>
                        <input type="text" name = "subject_detail_sat[]" style = "padding-left: 10px; height: 35px" >
                    </td>
                </tr>
            </tbody>
        </table>

    <div class = "form-group">
        <a class="btn btn-default js_cate-add-schedule">{__("Add new row")}</a>
        <button type="submit" class="btn btn-primary padrl30 js_school-add-schedule-no-category">{__("Save")}</button>
    </div>
{/if}
