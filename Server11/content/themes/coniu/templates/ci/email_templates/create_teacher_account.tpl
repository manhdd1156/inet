Kính gửi ông/bà {$full_name},<br/><br/>

Hiện tại, nhà trường đã đưa vào sử dụng "Ứng dụng tương tác mầm non - Coniu" làm công cụ quản lý và là kênh tương tác/trao đổi giữa nhà trường, giáo viên và phụ huynh.<br/>
Ban Giám hiệu nhà trường đã tạo sẵn tài khoản cho ông/bà là:<br/>
&emsp;&emsp;- Tài khoản: <b>{$username}</b><br/>
&emsp;&emsp;- Mật khẩu: <b>{$password}</b><br/>
Ông/bà có thể thay đổi thông tin tài khoản sau khi truy cập.<br/><br/>

Để sử dụng Coniu với tài khoản được cấp, ông/bà có thể áp dụng 01 trong 02 cách sau:<br/>
&emsp;&emsp;1 - Truy cập địa chỉ website Coniu tại địa chỉ: https://coniu.vn hoặc;<br/>
&emsp;&emsp;2 - Tải ứng dụng 'Coniu' về điện thoại (chỉ áp dụng với điện thoại iPhone và điện thoại sử dụng hệ điều hành Android).<br/><br/>

Trân trọng cám ơn sự hợp tác của ông/bà!<br/>
Thân ái,<br/>
Ban Giám hiệu nhà trường.<br/>
