<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h5 class="modal-title">{__("Chọn nhóm giáo viên đón muộn")}</h5>
</div>
<div class="modal-body">

    <div class="form-group">
        <div align="left">
            <strong class="red">{__("Lưu ý")}</strong> &colon;
            Đây là chọn giáo viên vào nhóm giáo viên đón muộn, không phải là phân công giáo viên
        </div>
    </div>

    <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_pickup.php">
        <input type="hidden" name="school_username" id="school_username" value="{$username}"/>
        <input type="hidden" name="do" value="save_teacher_assign"/>

            <table class="table table-bordered mb10" id="tbody-teacher">
                <tbody>
                <tr>
                    <th>
                        {foreach $teachers as $teacher }
                            <label class="col-sm-4 text-left">
                                <input type="checkbox" name="teacher[]" value="{$teacher['user_id']}" {if $teacher['selected']} checked{/if}> {$teacher['user_fullname']}
                                <input type="hidden" name="teacher_fullname_{$teacher['user_id']}" value="{$teacher['user_fullname']}">
                                <input type="hidden" name="teacher_name_{$teacher['user_id']}" value="{$teacher['user_name']}">
                            </label>
                        {/foreach}
                    </th>
                </tr>
                </tbody>
            </table>

        <div class="form-group" >
            <div class="col-sm-12 text-center">
                <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
            </div>
        </div>

        <!-- success -->
        <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
        <!-- success -->

        <!-- error -->
        <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
        <!-- error -->
    </form>

</div>
