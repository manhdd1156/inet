<table class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th colspan="6">{__("Teacher/employee list")}&nbsp;({$rows|count})</th>
    </tr>
    <tr>
        <th>{__('#')}</th>
        <th>{__('Teacher')}</th>
        <th>{__('Birthdate')}</th>
        <th>{__('Phone')}</th>
        <th>{__('Email')}</th>
        <th>{__('Class')}</th>
    </tr>
    </thead>
    <tbody>
    {foreach $rows as $k => $row}
        <tr>
            <td align="center">{$k + 1 }</td>
            <td><a href = "{$system['system_url']}/{$row['user_name']}"> {convertText4Web($row['user_fullname'])} </a></td>
            <td>{$row['user_birthdate']}</td>
            <td>{$row['user_phone']}</td>
            <td><a href="mailto:{$row['user_email']}">{$row['user_email']}</a></td>
            <td>
                {if empty($row['classes'])}
                    {__("No class")}
                {else}
                    {foreach $row['classes'] as $k => $class}
                        {$class['group_title']}{if $k < count($row['classes']) - 1} - {/if}
                    {/foreach}
                    <a href = "{$system['system_url']}/groups/{$row['class_name']}">
                        {convertText4Web($row['class_title'])}
                    </a>
                {/if}
            </td>
        </tr>
    {/foreach}

    {if $rows|count == 0}
        <tr class="odd">
            <td valign="top" align="center" colspan="7" class="dataTables_empty">
                {__("There is no teacher having birthday in this duration")}
            </td>
        </tr>
    {/if}
    </tbody>
</table>