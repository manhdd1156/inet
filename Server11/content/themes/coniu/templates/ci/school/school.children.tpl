<div class="panel panel-default">
    {if ($school['school_step'] != SCHOOL_STEP_FINISH  && $school['grade'] == 0) && $sub_view == '' && count($result['children']) == 0}
        <div class="panel-body">
            <div class="" align="center">
                <a href="{$system['system_url']}/school/{$username}/children/add" class="btn btn-default">{__("Add new student")}</a>
                <a href="{$system['system_url']}/school/{$username}/children/import" class="btn btn-default">{__("Import from Excel file")}</a>
            </div>
        </div>
    {else}
        <div class="panel-heading with-icon">
            {if $canEdit}
                <div class="pull-right flip">
                    {if $sub_view == ""}
                        {if $school['school_step'] == SCHOOL_STEP_FINISH  || $school['grade'] != 0}
                            <a href="{$system['system_url']}/school/{$username}/children/developmentindex" class="btn btn-default">
                                <i class="fa fa-heart"></i> {__("Development index")}
                            </a>
                            <a href="{$system['system_url']}/school/{$username}/children/addhealthindex" class="btn btn-default">
                                <i class="fa fa-plus"></i> {__("Add health index")}
                            </a>
                        {/if}
                        <a href="{$system['system_url']}/school/{$username}/children/add" class="btn btn-default">
                            <i class="fa fa-plus"></i> {__("Add child")}
                        </a>
                        <a href="{$system['system_url']}/school/{$username}/children/addexisting" class="btn btn-default">
                            <i class="fa fa-plus"></i> {__("Add existing child")}
                        </a>
                        {if $school['school_step'] != 100}
                            <a href="{$system['system_url']}/school/{$username}/children/import" class="btn btn-default">
                                <i class="fa fa-plus"></i> {__("Import from Excel file")}
                            </a>
                        {/if}
                        {if $school['school_step'] == SCHOOL_STEP_FINISH  || $school['grade'] != 0}
                            <br/>
                            <div style="margin-top: 8px">
                                <a href="{$system['system_url']}/school/{$username}/children/leaveschool" class="btn btn-default">
                                    <i class="fa fa-lock"></i> {__("Left children")}
                                </a>
                                <a href="{$system['system_url']}/school/{$username}/children/adddiary" class="btn btn-default">
                                    <i class="fa fa-image"></i> {__("Add diary")}
                                </a>
                                <a href="{$system['system_url']}/school/{$username}/children/movesclass" class="btn btn-default">
                                    <i class="fas fa-exchange-alt"></i> {__("Move class for student")}
                                </a>
                            </div>
                        {/if}
                    {elseif $sub_view == "import"}
                        <a href="https://blog.coniu.vn/huong-dan-nhap-nhieu-tre-bang-file-excel/" class="btn btn-info btn_guide" target="_blank">
                            <i class="fa fa-info"></i> {__("Guide")}
                        </a>
                        <a href="{$system['system_url']}/school/{$username}/children/add" class="btn btn-default">
                            <i class="fa fa-plus"></i> {__("Add child")}
                        </a>
                        <a href="{$system['system_url']}/school/{$username}/children" class="btn btn-default">
                            <i class="fa fa-list"></i> {__("Lists")}
                        </a>
                    {elseif $sub_view == "add"}
                        {if $school['school_step'] == SCHOOL_STEP_FINISH  || $school['grade'] != 0}
                            <a href="https://blog.coniu.vn/huong-dan-them-moi-tre-vao-truong/" class="btn btn-info btn_guide" target="_blank">
                                <i class="fa fa-info"></i> {__("Guide")}
                            </a>
                        {/if}
                        <a href="{$system['system_url']}/school/{$username}/children/addexisting" class="btn btn-default">
                            <i class="fa fa-plus"></i> {__("Add existing child")}
                        </a>
                        <a href="{$system['system_url']}/school/{$username}/children/import" class="btn btn-default">
                            <i class="far fa-file-excel"></i> {__("Import from Excel file")}
                        </a>
                        <a href="{$system['system_url']}/school/{$username}/children" class="btn btn-default">
                            <i class="fa fa-list"></i> {__("Lists")}
                        </a>
                    {elseif $sub_view == "listchildnew"}
                        <a href="{$system['system_url']}/school/{$username}/children" class="btn btn-default">
                            <i class="fa fa-list"></i> {__("Lists")}
                        </a>
                    {elseif $sub_view == "addhealthindex"}
                        <a href="https://blog.coniu.vn/huong-dan-chi-suc-khoe-tai-khoan-quan-ly-tren-ung-dung-mam-non-coniu/" class="btn btn-info" target="_blank">
                            <i class="fa fa-info-circle"></i> {__("Guide health index")}
                        </a>
                        <a href="{$system['system_url']}/school/{$username}/children" class="btn btn-default">
                            <i class="fa fa-list"></i> {__("Lists")}
                        </a>
                    {elseif $sub_view == "detail"}
                        {if $school['school_step'] == 100}
                            <a href="{$system['system_url']}/school/{$username}/children/health/{$child['child_id']}" class="btn btn-default">
                                <i class="fa fa-heartbeat"></i> {__("Health information")}
                            </a>
                            <a href="{$system['system_url']}/school/{$username}/children/journal/{$child['child_id']}" class="btn btn-default">
                                <i class="fa fa-image"></i> {__("List of diary")}
                            </a>
                        {/if}
                    {elseif $sub_view == "journal"}
                        <a href="{$system['system_url']}/school/{$username}/children/detail/{$child['child_id']}" class="btn btn-default">
                            <i class="fa fa-info" aria-hidden="true"></i> {__("Student information detail")}
                        </a>
                        {if $child['status'] == 1}
                            <a href="{$system['system_url']}/school/{$username}/children/addphoto/{$child['child_id']}" class="btn btn-default">
                                <i class="fa fa-image"></i> {__("Add diary")}
                            </a>
                        {/if}
                    {elseif $sub_view == "health"}
                        <a href="https://blog.coniu.vn/huong-dan-quan-ly-them-chi-suc-khoe-tai-tren-website-coniu/" class="btn btn-info" target="_blank">
                            <i class="fa fa-info-circle"></i> {__("Guide health index")}
                        </a>
                        {if $child['status']}
                            <a href="{$system['system_url']}/school/{$username}/children/addgrowth/{$child['child_id']}" class="btn btn-default">
                                <i class="fa fa-plus" aria-hidden="true"></i> {__("Add New")}
                            </a>
                        {/if}
                        <a href="{$system['system_url']}/school/{$username}/children/detail/{$child['child_id']}" class="btn btn-default">
                            <i class="fa fa-info" aria-hidden="true"></i> {__("Student information detail")}
                        </a>
                    {elseif $sub_view == "addgrowth" || $sub_view == "editgrowth"}
                        <a href="https://blog.coniu.vn/huong-dan-quan-ly-them-chi-suc-khoe-tai-tren-website-coniu/" class="btn btn-info" target="_blank">
                            <i class="fa fa-info-circle"></i> {__("Guide health index")}
                        </a>
                        <a href="{$system['system_url']}/school/{$username}/children/detail/{$child['child_id']}" class="btn btn-default">
                            <i class="fa fa-info" aria-hidden="true"></i> {__("Student information detail")}
                        </a>
                        <a href="{$system['system_url']}/school/{$username}/children/health/{$child['child_id']}" class="btn btn-default">
                            <i class="fa fa-heartbeat"></i> {__("Health information")}
                        </a>
                    {elseif $sub_view == "developmentindex" || $sub_view == "developmentclass"}
                        <a href="https://blog.coniu.vn/huong-dan-quan-ly-them-chi-suc-khoe-tai-tren-website-coniu/" class="btn btn-info" target="_blank">
                            <i class="fa fa-info-circle"></i> {__("Guide health index")}
                        </a>
                        <a href="{$system['system_url']}/school/{$username}/children/addhealthindex" class="btn btn-default">
                            <i class="fa fa-plus"></i> {__("Add health index")}
                        </a>
                    {elseif $sub_view == "adddiary" || $sub_view == "movesclass"}
                        <a href="{$system['system_url']}/school/{$username}/children" class="btn btn-default">
                            <i class="fa fa-list"></i> {__("Lists")}
                        </a>
                    {/if}
                </div>
            {/if}
            <i class="fa fa-child fa-fw fa-lg pr10"></i>
            {__("Student")}
            {if $sub_view == "edit"}
                &rsaquo; {__('Edit')} &rsaquo; {$data['child_name']}
            {elseif $sub_view == "add"}
                &rsaquo; {__('Add New')}
            {elseif $sub_view == "addexisting"}
                &rsaquo; {__('Add existing child')}
            {elseif $sub_view == "listchildnew"}
                &rsaquo; {__('New student study begin in month list')}
            {elseif $sub_view == "moveclass"}
                &rsaquo; {__('Move class')} &rsaquo; {$child['child_name']}
            {elseif $sub_view == "leaveschool"}
                &rsaquo; {__('Left children list')}
            {elseif $sub_view == "leave"}
                &rsaquo; {__('Leave')} &rsaquo; {$child['child_name']}
            {elseif $sub_view == "import"}
                &rsaquo; {__('Import from Excel file')}
            {elseif $sub_view == "addexisting"}
                &rsaquo; {__('Add Existing Account')}
            {elseif $sub_view == "detail"}
                &rsaquo; {$child['child_name']}
            {elseif $sub_view == "addhealthindex"}
                &rsaquo; {__("Add health index")}
            {elseif $sub_view == "health"}
                &rsaquo; {__("Health information")} &rsaquo; {$child['child_name']}
            {elseif $sub_view == "developmentindex"}
                &rsaquo; {__("Development index")}
            {elseif $sub_view == "developmentclass"}
                &rsaquo; {__("Development index of child in class")}
            {elseif $sub_view == "addphoto"}
                &rsaquo; {__("Add diary")}
            {elseif $sub_view == "journal"}
                &rsaquo; {__("List of diary")} &rsaquo;{$child['child_name']}
            {elseif $sub_view == "adddiary"}
                &rsaquo; {__("Add diary")}
            {elseif $sub_view == "movesclass"}
                &rsaquo; {__("Move class for student")}
            {/if}
        </div>
        {if $sub_view == ""}
            <div class="panel-body with-table">
                <div class="row">
                    <div class='col-sm-4'>
                        <div class="form-group">
                            <input type="text" name="keyword" id="keyword" class="form-control" value="{$result['keyword']}" placeholder="{__("Enter keyword to search")}">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <select name="child_month" id="child_month" class="form-control">
                                <option value="0">{__("Select month")}...</option>
                                {for $i = 1; $i <= 60; $i++}
                                    <option value="{$i}">{$i} {__("Month")}</option>
                                {/for}
                            </select>
                        </div>
                    </div>
                    <div class='col-sm-3'>
                        <div class="form-group">
                            <select name="class_id" id="class_id" class="form-control">
                                <option value="">{__("Select Class")}...</option>
                                {if $school['children_use_no_class']}
                                    <option value="0" {if $result['class_id'] == '0'}selected{/if}>{__("No class")}</option>
                                {/if}
                                {$class_level = -1}
                                {foreach $classes as $class}
                                    {if ($class_level != $class['class_level_id'])}
                                        <option value="" disabled>-----{$class['class_level_name']}-----</option>
                                    {/if}
                                    <option value="{$class['group_id']}" {if $result['class_id'] == $class['group_id']}selected{/if}>{$class['group_title']}</option>
                                    {$class_level = $class['class_level_id']}
                                {/foreach}
                            </select>
                        </div>
                    </div>
                    <div class='col-sm-2'>
                        <div class = "row">
                            <div class = "col-sm-6">
                                <div class="form-group">
                                    <a href="#" id="search" class="btn btn-default js_child-search" data-username="{$username}" data-isnew="1" data-id="{$school['page_id']}">{__("Search")}</a>
                                    <label id="loading" class="btn btn-info x-hidden">{__("Loading")}...</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br/>
                <div class="table-responsive" id="child_list" name="child_list">
                    {include file="ci/school/ajax.school.children.list.tpl"}
                </div>
            </div>
        {elseif $sub_view == "listchildedit"}
            <div class="panel-body with-table">
                <div class="row">
                    <div class="form-group">
                        <div class="col-sm-3 col-sm-offset-3">
                            <select name="day_search" id="child_edit_day_search" class="form-control" data-username="{$username}">
                                <option value="month">{__("Nearest month")}</option> {*danh sách 1 tháng gần nhất*}
                                <option value="week">{__("Nearest week")}</option> {* danh sách 1 tuần gần nhất*}
                            </select>
                        </div>
                    </div>
                </div>
                <br/>
                <div class="table-responsive" id="child_list" name="child_list">
                    {include file="ci/school/ajax.childeditlist.tpl"}
                </div>
                <br>
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
            </div>
        {elseif $sub_view == "listchildnew"}
            <div class="panel-body with-table">
                <div class="row">
                    <div class="form-group">
                        <div class="col-sm-3 col-sm-offset-3">
                            <select name="day_search" id="child_new_day_search" class="form-control" data-username="{$username}">
                                <option value="month">{__("Nearest month")}</option> {*danh sách 1 tháng gần nhất*}
                                <option value="week">{__("Nearest week")}</option> {* danh sách 1 tuần gần nhất*}
                            </select>
                        </div>
                    </div>
                </div>
                <br/>
                <div class="table-responsive" id="child_list" name="child_list">
                    {include file="ci/school/ajax.childnewlist.tpl"}
                </div>
                <br>
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
            </div>
        {elseif $sub_view == "import"}
            <div class="panel-body">
                <form class="form-horizontal" action="#" enctype="multipart/form-data" method="post" id="import_excel_form">
                    <input type="hidden" name="school_username" value="{$username}"/>
                    <input type="hidden" name="do" value="import"/>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Select Class")} (*)</label>
                        <div class="col-sm-4">
                            <select name="class_id" id="class_id" class="form-control" required>
                                {if $school['children_use_no_class']}
                                    <option value="0">{__("No class")}</option>
                                {/if}
                                {$class_level = -1}
                                {foreach $classes as $class}
                                    {if ($class_level != $class['class_level_id'])}
                                        <option value="" disabled>-----{$class['class_level_name']}-----</option>
                                    {/if}
                                    <option value="{$class['group_id']}">{$class['group_title']}</option>
                                    {$class_level = $class['class_level_id']}
                                {/foreach}
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Select Excel file")}</label>
                        <div class="col-sm-6">
                            <input type="file" name="file" id="file" {if count($classes) <= 0}disabled{/if}/>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-3">
                            <button type="submit" id="submit_id" class="btn btn-primary padrl30" {if count($classes) <= 0}disabled{/if}>{__("Save")}</button>
                        </div>
                    </div>
                    <br/>
                    <div class="table-responsive" id="result_info" name="result_info"></div>

                    <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                    <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                </form>
            </div>
        {elseif $sub_view == "moveclass"}
            <div class="panel-body">
                <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_child.php">
                    <input type="hidden" name="school_username" value="{$username}"/>
                    <input type="hidden" name="child_id" value="{$child['child_id']}"/>
                    <input type="hidden" name="old_class_id" value="{$child['class_id']}"/>
                    <input type="hidden" name="do" value="moveclass"/>
                    <div>
                        <strong>
                            {__("Lưu ý: Nếu muốn tạo học phí tháng cho trẻ ở lớp mới, bạn nên xóa học phí của trẻ ở lớp cũ (nếu đã tạo trước đấy, trong cùng một tháng).")}
                        </strong>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Child")}</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="child_name" value="{$child['child_name']}" disabled>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Birthdate")}</label>
                        <div class='col-sm-3'>
                            <input type="text" class="form-control" name="birthday" value="{$child['birthday']}" disabled>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Old class")}</label>
                        <div class="col-sm-9">
                            {if $child['class_id'] == '0'}
                                <input type="text" class="form-control" name="class_name" value="{__("No class")}" disabled>
                            {else}
                                {foreach $classes as $class}
                                    {if $child['class_id'] == $class['group_id']}
                                        <input type="text" class="form-control" name="class_name" value="{$class['group_title']}" disabled>
                                        {break}
                                    {/if}
                                {/foreach}
                            {/if}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("New class")}</label>
                        <div class="col-sm-4">
                            <select name="new_class_id" class="form-control">
                                {foreach $classes as $class}
                                    {if $child['class_id'] == $class['group_id']}
                                        <option value="{$class['group_id']}" disabled>{$class['group_title']} - {__("Old class")}</option>
                                    {else}
                                        <option value="{$class['group_id']}">{$class['group_title']}</option>
                                    {/if}
                                {/foreach}
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-3">
                            <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                            <a href="{$system['system_url']}/school/{$username}/children" class="btn btn-default">{__("Lists")}</a>
                        </div>
                    </div>

                    <!-- success -->
                    <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                    <!-- success -->
                    <!-- error -->
                    <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                    <!-- error -->
                </form>
            </div>
        {elseif $sub_view == "leave"}
            <div class="panel-body">
                <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_child.php">
                    <input type="hidden" id="school_username" name="school_username" value="{$username}"/>
                    <input type="hidden" id="child_id" name="child_id" value="{$child['child_id']}"/>
                    <input type="hidden" id="class_id" name="class_id" value="{$child['class_id']}"/>
                    <input type="hidden" name="do" value="leave_school"/>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Child")}</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="child_name" value="{$child['child_name']}" disabled>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Birthdate")}</label>
                        <div class='col-sm-3'>
                            <input type="text" class="form-control" name="birthday" value="{$child['birthday']}" disabled>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Class")}</label>
                        <div class="col-sm-3">
                            {if $child['class_id'] == '0'}
                                <input type="text" class="form-control" name="class_name" value="{__("No class")}" disabled>
                            {else}
                                <input type="text" class="form-control" name="class_name" value="{$child['group_title']}" disabled>
                            {/if}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Leaving date")} (*)</label>
                        <div class='col-sm-3 text-left'>
                            <div class='input-group date' id='leave_datepicker'>
                                <input type='text' name="end_at" id="end_at" {if !is_null($child['end_at'])} value="{$child['end_at']}" {/if}class="form-control" placeholder="{__("Leaving date")}"/>
                                <span class="input-group-addon">
                            <span class="fas fa-calendar-alt"></span>
                        </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-3">
                            <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                            <a href="{$system['system_url']}/school/{$username}/children" class="btn btn-default">{__("Lists")}</a>
                        </div>
                    </div>
                    <div class="table-responsive" id="children_info">
                        {include file="ci/school/ajax.school.tuition.4leave.tpl"}
                    </div>
                    <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                    <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                </form>
            </div>
        {elseif $sub_view == "editleave"}
            <div class="panel-body">
                <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_child.php">
                    <input type="hidden" id="school_username" name="school_username" value="{$username}"/>
                    <input type="hidden" id="child_id" name="child_id" value="{$child['child_id']}"/>
                    <input type="hidden" id="class_id" name="class_id" value="{$child['class_id']}"/>
                    <input type="hidden" id="tuition_4leave_id" name="tuition_4leave_id" value="{$data['tuition_4leave_id']}"/>
                    <input type="hidden" name="do" value="edit_leave"/>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Child")}</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="child_name" value="{$child['child_name']}" disabled>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Birthdate")}</label>
                        <div class='col-sm-3'>
                            <input type="text" class="form-control" name="birthday" value="{$child['birthday']}" disabled>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Class")}</label>
                        <div class="col-sm-3">
                            {if $child['class_id'] == '0'}
                                <input type="text" class="form-control" name="class_name" value="{__("No class")}" disabled>
                            {else}
                                <input type="text" class="form-control" name="class_name" value="{$child['group_title']}" disabled>
                            {/if}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Leaving date")} (*)</label>
                        <div class='col-sm-3 text-left'>
                            <input type='hidden' name="old_end_at" value="{$child['end_at']}"/>
                            <div class='input-group date' id='leave_datepicker'>
                                <input type='text' name="end_at" id="end_at" {if !is_null($child['end_at'])} value="{$child['end_at']}" {/if}class="form-control" placeholder="{__("Leaving date")}"/>
                                <span class="input-group-addon">
                            <span class="fas fa-calendar-alt"></span>
                        </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-3">
                            <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                            <a href="{$system['system_url']}/school/{$username}/children/leaveschool" class="btn btn-default">{__("Left children list")}</a>
                        </div>
                    </div>
                    <div class="table-responsive" id="children_info">
                        {include file="ci/school/ajax.school.tuition.4leave.tpl"}
                    </div>
                    <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                    <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                </form>
            </div>
        {elseif $sub_view == "leaveschool"}
            <div class="panel-body with-table">
                <div class="row">
                    <div class='col-sm-4'>
                        <div class="form-group">
                            <input type="text" name="keyword" id="keyword" class="form-control" value="{$condition['keyword']}" placeholder="{__("Enter keyword to search")}">
                        </div>
                    </div>
                    <div class='col-sm-3'>
                        <div class="form-group">
                            <select name="class_id" id="class_id" class="form-control">
                                <option value="">{__("Select Class")}...</option>
                                {if $school['children_use_no_class']}
                                    <option value="0" {if $condition['class_id'] == '0'}selected{/if}>{__("No class")}</option>
                                {/if}
                                {$class_level = -1}
                                {foreach $classes as $class}
                                    {if ($class_level != $class['class_level_id'])}
                                        <option value="" disabled>-----{$class['class_level_name']}-----</option>
                                    {/if}
                                    <option value="{$class['group_id']}" {if $condition['class_id'] == $class['group_id']}selected{/if}>{$class['group_title']}</option>
                                    {$class_level = $class['class_level_id']}
                                {/foreach}
                            </select>
                        </div>
                    </div>
                    <div class='col-sm-2'>
                        <div class = "row">
                            <div class = "col-sm-6">
                                <div class="form-group">
                                    <a href="#" id="search" class="btn btn-default js_leave-school-child-search" data-username="{$username}" data-isnew="1" data-id="{$school['page_id']}">{__("Search")}</a>
                                    <label id="loading" class="btn btn-info x-hidden">{__("Loading")}...</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="table-responsive" id="child_list" name="child_list">
                    {include file="ci/school/ajax.child.leaveschoollist.tpl"}
                </div>
                <br>
            </div>
        {elseif $sub_view == "edit"}
            <div class="panel-body">
                <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_child.php">
                    <input type="hidden" name="school_username" value="{$username}"/>
                    <input type="hidden" name="child_id" value="{$child['child_id']}" id="child_id"/>
                    <input type="hidden" name="child_parent_id" value="{$child['child_parent_id']}"/>
                    <input type="hidden" name="child_parent_id" value="{$child['child_parent_id']}"/>
                    <input type="hidden" name="child_admin" value="{$child['child_admin']}" id="child_admin"/>
                    <input type="hidden" name="do" value="edit"/>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Student code")}</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="child_code" value="{$child['child_code']}" disabled>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Full name")} (*)</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="last_name" value="{$child['last_name']}" placeholder="{__("Last name")}" required maxlength="34" autofocus>
                        </div>
                        <div class="col-sm-2">
                            <input type="text" class="form-control" name="first_name" value="{$child['first_name']}" placeholder="{__("First name")}" required maxlength="15">
                        </div>
                    </div>
{*                    <div class="form-group">*}
{*                        <label class="col-sm-3 control-label text-left">{__("Student email")} (*)</label>*}
{*                        <div class="col-sm-3">*}
{*                            <input type="text" class="form-control" id="child_email" name="child_email" value="{$child['child_email']}" placeholder="{__("Student email")}" required maxlength="100">*}
{*                        </div>*}
{*                    </div>*}
{*                    <div class="form-group">*}
{*                        <label class="col-sm-3 control-label text-left">{__("Children password")} (*)</label>*}
{*                        <div class="col-sm-3">*}
{*                            <input type="password" class="form-control" id="child_password" name="child_password" value="{$child['child_password']}" placeholder="{__("Children password")}" required maxlength="100">*}
{*                        </div>*}
{*                        <div class="col-sm-3">*}
{*                            <input type="password" class="form-control" id="child_password_confirm" name="child_password_confirm" value="{$child['child_password_confirm']}" placeholder="{__("Confirm")}" required maxlength="100">*}
{*                        </div>*}
{*                    </div>*}

                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Gender")} (*)</label>
                        <div class="col-sm-3">
                            <select name="gender" id="gender" class="form-control">
                                <option value="{$smarty.const.MALE}" {if $child['gender'] == $smarty.const.MALE}selected{/if}>{__("Male")}</option>
                                <option value="{$smarty.const.FEMALE}" {if $child['gender'] == $smarty.const.FEMALE}selected{/if}>{__("Female")}</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Birthdate")} (*)</label>
                        <div class='col-sm-3'>
                            <div class='input-group date' id='birthdate_picker'>
                                <input type='text' name="birthday" value="{$child['birthday']}" class="form-control" placeholder="{__("Birthdate")} (*)" required/>
                                <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Class")} (*)</label>
                        <div class="col-sm-9">
                            {if $child['class_id'] == '0'}
                                <input type="text" class="form-control" name="class_name" value="{__("No class")}" disabled>
                            {else}
                                {foreach $classes as $class}
                                    {if $child['class_id'] == $class['group_id']}
                                        <input type="text" class="form-control" name="class_name" value="{$class['group_title']}" disabled>
                                        {break}
                                    {/if}
                                {/foreach}
                            {/if}
                            <input type="hidden" name="class_id" value="{$child['class_id']}"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Study start date")} (*)</label>
                        <div class='col-sm-3'>
                            <div class='input-group date' id='beginat_datepicker'>
                                <input type='text' name="begin_at" value="{$child['begin_at']}" class="form-control" placeholder="{__("Study start date")}" required/>
                                <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-3 mb5"><strong>{__("Search for available users on the system as a student's parent")}</strong></div>
                        <div class="col-sm-9 col-sm-offset-3">
                            <input name="search-parent" id="search-parent" type="text" class="form-control mb5" placeholder="{__("Enter username, email or fullname to search")}" autocomplete="off">
                            <div id="search-parent-results" class="dropdown-menu dropdown-widget dropdown-search">
                                <div class="dropdown-widget-header">
                                    {__("Search Results")}
                                </div>
                                <div class="dropdown-widget-body">
                                    <div class="loader loader_small ptb10"></div>
                                </div>
                            </div>
                            <div>{__("Enter at least 4 characters")}.</div>
                            <br/>
                            <div class="col-sm-9" id="parent_list" name="parent_list">
                                {if count($parent) > 0}
                                    {include file='ci/ajax.parentlist.tpl' results=$parent}
                                {else}
                                    {__("No parent")}
                                {/if}
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Mother's name")}</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" id="parent_name" name="parent_name" placeholder="{__("Mother's name")}" maxlength="50" value="{convertText4Web($child['parent_name'])}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Telephone")}</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="parent_phone" name="parent_phone" value="{$child['parent_phone']}" placeholder="{__("Telephone")}" maxlength="50">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Job")}</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="parent_job" name="parent_job" value="{$child['parent_job']}" placeholder="{__("Job")}" maxlength="50">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Father's name")}</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" id="parent_name_dad" name="parent_name_dad" placeholder="{__("Father's name")}" maxlength="50" value="{convertText4Web($child['parent_name_dad'])}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Telephone")}</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="parent_phone_dad" name="parent_phone_dad" value="{$child['parent_phone_dad']}" placeholder="{__("Telephone")}" maxlength="50">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Job")}</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="parent_job_dad" name="parent_job_dad" value="{$child['parent_job_dad']}" placeholder="{__("Job")}" maxlength="50">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Parent email")}</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" id="parent_email" name="parent_email" value="{$child['parent_email']}" placeholder="{__("Parent email")}" maxlength="100">
                            <div>{__("If parent do not have any email, please let it empty")}.</div>
                        </div>
                        <div class="col-sm-4">
                            <input type="checkbox" value="1" name="create_parent_account" id="create_parent_account" {if count($parent) > 0}disabled{else}checked{/if}>&nbsp;{__("Auto-create parent account")}?
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Address")}</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="address" value="{$child['address']}" placeholder="{__("Address")}" maxlength="250">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Description")}</label>
                        <div class="col-sm-9">
                            <textarea class="form-control" name="description" placeholder="{__("Write about your child...")}" maxlength="300">{$child['description']}</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-3">
                            <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                            <a href="{$system['system_url']}/school/{$username}/children" class="btn btn-default">{__("Lists")}</a>
                            {if $school['school_step'] == SCHOOL_STEP_FINISH  || $school['grade'] != 0}
                                <a href="{$system['system_url']}/school/{$username}/children/moveclass/{$child['child_id']}" class="btn btn-warning">{__("Move class")}</a>
                                <a href="{$system['system_url']}/school/{$username}/children/leave/{$child['child_id']}" class="btn btn-danger">{__("Leave")}</a>
                            {/if}
                        </div>
                    </div>

                    <!-- success -->
                    <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                    <!-- success -->
                    <!-- error -->
                    <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                    <!-- error -->
                </form>
            </div>
        {elseif $sub_view == "confirm"}
            <div class="panel-body">
                <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_child.php">
                    <input type="hidden" name="school_username" value="{$username}"/>
                    <input type="hidden" name="child_id" value="{$child_edit['child_id']}"/>
                    <input type="hidden" name="child_parent_id" value="{$child['child_parent_id']}"/>
                    <input type="hidden" name="do" value="edit"/>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Student code")}</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="child_code" value="{$child_edit['child_code']}" disabled>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Full name")} (*)</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="last_name" value="{$child_edit['last_name']}" placeholder="{__("Last name")}" required maxlength="34" autofocus>
                        </div>
                        <div class="col-sm-2">
                            <input type="text" class="form-control" name="first_name" value="{$child_edit['first_name']}" placeholder="{__("First name")}" required maxlength="15">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Gender")} (*)</label>
                        <div class="col-sm-3">
                            <select name="gender" id="gender" class="form-control">
                                <option value="{$smarty.const.MALE}" {if $child_edit['gender'] == $smarty.const.MALE}selected{/if}>{__("Male")}</option>
                                <option value="{$smarty.const.FEMALE}" {if $child_edit['gender'] == $smarty.const.FEMALE}selected{/if}>{__("Female")}</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Birthdate")} (*)</label>
                        <div class='col-sm-3'>
                            <div class='input-group date' id='birthdate_picker'>
                                <input type='text' name="birthday" value="{$child_edit['birthday']}" class="form-control" placeholder="{__("Birthdate")} (*)" required/>
                                <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Parent")}</label>
                        <div class="col-sm-7">
                            <input name="search-parent" id="search-parent" type="text" class="form-control" placeholder="{__("Enter username, email or fullname to search")}" autocomplete="off">
                            <div id="search-parent-results" class="dropdown-menu dropdown-widget dropdown-search">
                                <div class="dropdown-widget-header">
                                    {__("Search Results")}
                                </div>
                                <div class="dropdown-widget-body">
                                    <div class="loader loader_small ptb10"></div>
                                </div>
                            </div>
                            <div>{__("Enter at least 4 characters")}.</div>
                            <br/>
                            <div class="col-sm-9" id="parent_list" name="parent_list">
                                {if count($child_edit['parent']) > 0}
                                    {include file='ci/ajax.parentlist.tpl' results=$child_edit['parent']}
                                {else}
                                    {__("No parent")}
                                {/if}
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Parent name")}</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" id="parent_name" name="parent_name" placeholder="{__("Parent name")}" {if count($child_edit['parent']) > 0}disabled {else} value = "{$child_edit['parent_name']}"{/if} maxlength="50">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Parent phone")}</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="parent_phone" name="parent_phone" value="{$child_edit['parent_phone']}" placeholder="{__("Parent phone")}" maxlength="50">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Parent email")}</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" id="parent_email" name="parent_email" value="{$child_edit['parent_email']}" placeholder="{__("Parent email")}" maxlength="100">
                        </div>
                        <div class="col-sm-4">
                            <input type="checkbox" value="{$child_edit['create_parent_account']}" name="create_parent_account" id="create_parent_account" {if count($child_edit['parent']) > 0}disabled{else}checked{/if}>&nbsp;{__("Auto-create parent account")}?
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Address")}</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="address" value="{$child_edit['address']}" placeholder="{__("Address")}" maxlength="250">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Class")}</label>
                        <div class="col-sm-9">
                            {if $child_edit['class_id'] == '0'}
                                <input type="text" class="form-control" name="class_name" value="{__("No class")}" disabled>
                            {else}
                                {foreach $classes as $class}
                                    {if $child_edit['class_id'] == $class['group_id']}
                                        <input type="text" class="form-control" name="class_name" value="{$class['group_title']}" disabled>
                                        {break}
                                    {/if}
                                {/foreach}
                            {/if}
                            <input type="hidden" name="class_id" value="{$child_edit['class_id']}"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Study start date")} (*)</label>
                        <div class='col-sm-3'>
                            <div class='input-group date' id='beginat_datepicker'>
                                <input type='text' name="begin_at" value="{$child_edit['begin_at']}" class="form-control" placeholder="{__("Study start date")}" required/>
                                <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Description")}</label>
                        <div class="col-sm-9">
                            <textarea class="form-control" name="description" placeholder="{__("Write about your child...")}" maxlength="300">{$child_edit['description']}</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-3">
                            <button type="submit" class="btn btn-default">{__("Confirm")}</button>
                            <a href="{$system['system_url']}/school/{$username}/children/listchildedit" class="btn btn-default">{__("The student list was edited by the teacher")}</a>
                        </div>
                    </div>

                    <!-- success -->
                    <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                    <!-- success -->
                    <!-- error -->
                    <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                    <!-- error -->
                </form>
            </div>
        {elseif $sub_view == "add"}
            <div class="panel-body">
                <form class="js_ajax-add-child-form form-horizontal" data-url="ci/bo/school/bo_child.php">
                    <input type="hidden" name="school_username" id="school_username" value="{$username}"/>
                    <input type="hidden" name="do" value="add"/>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Student code")}</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="child_code" id="child_code" placeholder="{__("Student code")}" disabled autofocus maxlength="30">
                        </div>
                        <div class="col-sm-4">
                            <input type="checkbox" name="code_auto" id="code_auto" value="1" checked/> {__("Generate automatically")}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Full name")} (*)</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="last_name" id="last_name" placeholder="{__("Last name")}" required maxlength="34" autofocus>
                        </div>
                        <div class="col-sm-2">
                            <input type="text" class="form-control" name="first_name" id="first_name" placeholder="{__("First name")}" required maxlength="15">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Student email")} (*)</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="child_email" name="child_email" value="{$child['child_email']}" placeholder="{__("Student email")}" required maxlength="100">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Children password")} (*)</label>
                        <div class="col-sm-3">
                            <input type="password" class="form-control" id="child_password" name="child_password" value="{$child['child_password']}" placeholder="{__("Children password")}" required maxlength="100">
                        </div>
                        <div class="col-sm-3">
                            <input type="password" class="form-control" id="child_password_confirm" name="child_password_confirm" value="{$child['child_password_confirm']}" placeholder="{__("Confirm")}" required maxlength="100">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Gender")} (*)</label>
                        <div class="col-sm-3">
                            <select name="gender" id="gender" class="form-control">
                                <option value="{$smarty.const.MALE}">{__("Male")}</option>
                                <option value="{$smarty.const.FEMALE}">{__("Female")}</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Birthdate")} (*)</label>
                        <div class='col-sm-3'>
                            <div class='input-group date' id='birthdate_picker'>
                                <input type='text' name="birthday" id="birthday" class="form-control" placeholder="{__("Birthdate")} (*)" required/>
                                <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Class")} (*)</label>
                        <div class="col-sm-4">
                            <select name="class_id" id="class_id" class="form-control">
                                {if $school['children_use_no_class']}
                                    <option value="0">{__("No class")}</option>
                                {/if}
                                {$class_level = -1}
                                {foreach $classes as $class}
                                    {if ($class_level != $class['class_level_id'])}
                                        <option value="" disabled>-----{$class['class_level_name']}-----</option>
                                    {/if}
                                    <option value="{$class['group_id']}">{$class['group_title']}</option>
                                    {$class_level = $class['class_level_id']}
                                {/foreach}
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Study start date")} (*)</label>
                        <div class='col-sm-3'>
                            <div class='input-group date' id='beginat_datepicker'>
                                <input type='text' name="begin_at" class="form-control" placeholder="{__("Study start date")}"/>
                                <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-3 mb5"><strong>{__("Search for available users on the system as a student's parent")}</strong></div>
                        <div class="col-sm-7 col-sm-offset-3">
                            <input name="search-parent" id="search-parent" type="text" class="form-control mb5" placeholder="{__("Enter username, email or fullname to search")}" autocomplete="off">
                            <div id="search-parent-results" class="dropdown-menu dropdown-widget dropdown-search">
                                <div class="dropdown-widget-header">
                                    {__("Search Results")}
                                </div>
                                <div class="dropdown-widget-body">
                                    <div class="loader loader_small ptb10"></div>
                                </div>
                            </div>
                            <div>{__("Enter at least 4 characters")}.</div>
                            <br/>
                            <div class="col-sm-9" id="parent_list" name="parent_list"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Mother's name")}</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" id="parent_name" name="parent_name" placeholder="{__("Mother's name")}" maxlength="50">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Telephone")}</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="parent_phone" id="parent_phone" placeholder="{__("Telephone")}" maxlength="50">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Job")}</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="parent_job" id="parent_job" placeholder="{__("Job")}" maxlength="500">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Father's name")}</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" id="parent_name_dad" name="parent_name_dad" placeholder="{__("Father's name")}" maxlength="50">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Telephone")}</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="parent_phone_dad" id="parent_phone_dad" placeholder="{__("Telephone")}" maxlength="50">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Job")}</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="parent_job_dad" id="parent_job_dad" placeholder="{__("Job")}" maxlength="500">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Parent email")}</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" id="parent_email" name="parent_email" placeholder="{__("Parent email")}" maxlength="100">
                            <div>{__("If parent do not have any email, please let it empty")}.</div>
                        </div>
                        <div class="col-sm-4">
                            <input type="checkbox" value="1" name="create_parent_account" id="create_parent_account" checked>&nbsp;{__("Auto-create parent account")}?
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Address")}</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="address" id="address" placeholder="{__("Address")}" maxlength="250">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Description")}</label>
                        <div class="col-sm-9">
                            <textarea class="form-control" name="description" id="description" placeholder="{__("Write about your child...")}" maxlength="300"></textarea>
                        </div>
                    </div>
                    {if $school['school_step'] == SCHOOL_STEP_FINISH  || $school['grade'] != 0}
                        {*Đăng ký dịch vụ cho trẻ*}
                        <div class="form-group">
                            <label class="col-sm-3 control-label text-left">{__("Register service for child")}?</label>
                            <div class="col-sm-9">
                                <div class="onoffswitch">
                                    <input type="checkbox" name="for_service" class="onoffswitch-checkbox" id="for_service">
                                    <label class="onoffswitch-label" for="for_service"></label>
                                </div>
                                <span class="help-block">{__("Do you want register service for student")}?</span>
                            </div>
                        </div>
                        <div class="form-group x-hidden" id="service_box">
                            <div class="col-sm-9 col-sm-offset-3"><strong>{__("Lưu ý: Bạn chỉ có thể đăng ký được dịch vụ theo tháng và ngày điểm danh, ngày bắt đầu sử dụng dịch vụ sẽ lấy là ngày trẻ bắt đầu đi học, vui lòng vào đăng ký dịch vụ sửa lại, nếu cần.")}</strong></div>
                            <label class="col-sm-3 control-label">{__("Select service")}</label>
                            <div class="col-sm-9">
                                <div class="row">
                                    {foreach $services as $service}
                                        {if $service['type'] != SERVICE_TYPE_COUNT_BASED}
                                            <div class="col-sm-6">
                                                <input type="checkbox" value="{$service['service_id']}" name="serviceIds[]" class="service_ids"> {$service['service_name']}
                                            </div>
                                        {/if}
                                    {/foreach}
                                </div>
                            </div>
                        </div>
                    {/if}
                    <div class="form-group">
                        <div class="col-sm-3 col-sm-offset-3">
                            <button type="submit" class="btn btn-primary padrl30">{__("Save and add more")}</button>
                        </div>
                        <div class="col-sm-4">
                            <a href="#" class="btn btn-default js_add_child_clear">{__("Clear Data")}</a>
                        </div>
                    </div>

                    <!-- success -->
                    <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                    <!-- success -->

                    <!-- error -->
                    <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                    <!-- error -->
                </form>
            </div>
        {elseif $sub_view == "addexisting"}
            <div class="panel-body">

                <div class="panel-body with-table">
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-3 control-label text-right pt5">{__("Student code")} (*)</label>
                            <div class='col-sm-3'>
                                <input type='text' name="childcode" id="childcode" class="form-control" min="1" placeholder="{__("Student code")}"/>
                            </div>
                            <div class='col-sm-2'>
                                <div class="form-group">
                                    <a href="#" id="search" class="btn btn-default js_child-addexisting" data-username="{$username}" data-id="{$school['page_id']}">{__("Search")}</a>
                                    <label id="loading" class="btn btn-info x-hidden">{__("Loading")}...</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="" id="table_child_addexisting">

                    </div>
                </div>

            </div>
        {elseif $sub_view == "detail"}
            <div class="panel-body with-table">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <tbody>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Full name")}</strong></td>
                            <td>{$child['child_name']}</td>
                        </tr>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Student code")}</strong></td>
                            <td>{$child['child_code']}</td>
                        </tr>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Nickname")}</strong></td>
                            <td>{$child['child_nickname']}</td>
                        </tr>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Gender")}</strong></td>
                            <td>{if $child['gender'] == $smarty.const.MALE}{__("Male")}{else}{__("Female")}{/if}</td>
                        </tr>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Birthdate")}</strong></td>
                            <td>{$child['birthday']}</td>
                        </tr>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Parent")}</strong></td>
                            <td>
                                {foreach $parent as $_user}
                                    <span class="name js_user-popover" data-uid="{$_user['user_id']}">
                                        <a href="{$system['system_url']}/{$_user['user_name']}">{$_user['user_fullname']}</a>
                                    </span>
                                    {if $_user['user_id'] != $user->_data['user_id']}
                                        <a href="#" class="far fa-comments fa-lg js_chat-start" data-name="{$_user['user_fullname']}" data-uid="{$_user['user_id']}"></a>
                                    {/if}
                                    <br/>
                                {/foreach}
                            </td>
                        </tr>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Mother's name")}</strong></td>
                            <td>{$child['parent_name']}</td>
                        </tr>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Telephone")}</strong></td>
                            <td>{$child['parent_phone']}</td>
                        </tr>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Job")}</strong></td>
                            <td>{$child['parent_job']}</td>
                        </tr>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Father's name")}</strong></td>
                            <td>{$child['parent_name_dad']}</td>
                        </tr>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Telephone")}</strong></td>
                            <td>{$child['parent_phone_dad']}</td>
                        </tr>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Job")}</strong></td>
                            <td>{$child['parent_job_dad']}</td>
                        </tr>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Parent email")}</strong></td>
                            <td>{$child['parent_email']}</td>
                        </tr>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Address")}</strong></td>
                            <td>{$child['address']}</td>
                        </tr>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Study start date")}</strong></td>
                            <td>{$child['begin_at']}</td>
                        </tr>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Description")}</strong></td>
                            <td>{$child['description']}</td>
                        </tr>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Pickup information")}</strong></td>
                            <td>
                                <table class = "table table-bordered">
                                    <tbody>
                                    {foreach $data as $row}
                                        <tr>
                                            <td style="width: 60%">
                                                {if !is_empty($row['picker_source_file'])}
                                                    <a href="{$row['picker_source_file']}" target="_blank"><img src = "{$row['picker_source_file']}" style="width: 100%" class = "img-responsive"></a>
                                                {else}
                                                    {__("No information")}
                                                {/if}
                                            </td>
                                            <td style="width: 40%">
                                                <strong>{__("Picker name")}:</strong> {$row['picker_name']} <br/>
                                                <strong>{__("Relation with student")}:</strong> {$row['picker_relation']} <br/>
                                                <strong>{__("Telephone")}:</strong> {$row['picker_phone']} <br/>
                                                <strong>{__("Address")}:</strong> {$row['picker_address']} <br/>
                                                <strong>{__("Creator")}:</strong> {$row['user_fullname']}
                                            </td>
                                        </tr>
                                    {/foreach}
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <a href="{$system['system_url']}/school/{$username}/children" class="btn btn-default">{__("Lists")}</a>
                        {if $canEdit && $child['status'] == 1}
                            <a href="{$system['system_url']}/school/{$username}/children/edit/{$child['child_id']}" class="btn btn-info">{__("Edit")}</a>
                            {if $school['school_step'] == SCHOOL_STEP_FINISH  || $school['grade'] != 0}
                                <a href="{$system['system_url']}/school/{$username}/children/moveclass/{$child['child_id']}" class="btn btn-warning">{__("Move class")}</a>
                                <a href="{$system['system_url']}/school/{$username}/children/leave/{$child['child_id']}" class="btn btn-danger">{__("Leave")}</a>
                            {/if}
                        {/if}
                    </div>
                </div>
            </div>
        {elseif $sub_view == "informations"}
            <div class="panel-body with-table">
                <table class = "table table-bordered">
                    <tbody>
                    <tr>
                        <td class = "col-sm-3 text-right">{__('Picker name')}</td>
                        <td>
                            {if $data['picker_name'] != ""}{$data['picker_name']}
                            {else} {__("No information")}
                            {/if}
                        </td>
                    </tr>
                    <tr>
                        <td class = "col-sm-3 text-right">{__('Relation with student')}</td>
                        <td>
                            {if $data['picker_relation'] != ""}{$data['picker_relation']}
                            {else} {__("No information")}
                            {/if}
                        </td>
                    </tr>
                    <tr>
                        <td class = "col-sm-3 text-right">{__('Telephone')}</td>
                        <td>
                            {if $data['picker_phone'] != ""}{$data['picker_phone']}
                            {else} {__("No information")}
                            {/if}
                        </td>
                    </tr>
                    <tr>
                        <td class = "col-sm-3 text-right">{__('Address')}</td>
                        <td>
                            {if $data['picker_address'] != ""}{$data['picker_address']}
                            {else} {__("No information")}
                            {/if}
                        </td>
                    </tr>
                    <tr>
                        <td class = "col-sm-3 text-right">{__('Picker picture')}</td>
                        <td>
                            {if !is_empty($data['picker_source_file'])}
                                <a href="{$data['picker_source_file']}" target="_blank"><img src = "{$data['picker_source_file']}" class = "img-responsive"></a>
                                <br/>
                            {else} {__("No information")}
                            {/if}
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        {elseif $sub_view == "addhealthindex"}
            <div class="panel-body with-table">
                <div class="row">
                    <div class='col-sm-6'>
                        <div class="form-group">
                            <input type="text" name="keyword" id="keyword" class="form-control" value="{$result['keyword']}" placeholder="{__("Enter keyword to search")}">
                        </div>
                    </div>
                    <div class='col-sm-3'>
                        <div class="form-group">
                            <select name="class_id" id="class_id" class="form-control">
                                <option value="">{__("Select Class")}...</option>
                                {if $school['children_use_no_class']}
                                    <option value="0" {if $result['class_id'] == '0'}selected{/if}>{__("No class")}</option>
                                {/if}
                                {$class_level = -1}
                                {foreach $classes as $class}
                                    {if ($class_level != $class['class_level_id'])}
                                        <option value="" disabled>-----{$class['class_level_name']}-----</option>
                                    {/if}
                                    <option value="{$class['group_id']}" {if $result['class_id'] == $class['group_id']}selected{/if}>{$class['group_title']}</option>
                                    {$class_level = $class['class_level_id']}
                                {/foreach}
                            </select>
                        </div>
                    </div>
                    <div class='col-sm-3'>
                        <div class = "row">
                            <div class = "col-sm-6">
                                <div class="form-group">
                                    <a href="#" id="search" class="btn btn-default js_child-health-search" data-username="{$username}" data-isnew="1" data-id="{$school['page_id']}">{__("Search")}</a>
                                    <label id="loading" class="btn btn-info x-hidden">{__("Loading")}...</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br/>
                <div id="child_health_list" name="child_health_list">
                </div>
            </div>
        {elseif $sub_view == "health"}
            <div class = "panel-body">
                {*Ô search*}
                <div class="js_ajax-forms form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-2 control-label text-left">{__("Time")}</label>
                        <div class='col-sm-3'>
                            <div class='input-group date' id='begin_chart_picker'>
                                <input type='text' name="begin" class="form-control" id="begin"/>
                                <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                            </div>
                        </div>
                        <div class='col-sm-1'><i class="fas fa-long-arrow-alt-right fa-lg fa-fw pr10 pt10"></i></div>
                        <div class='col-md-3'>
                            <div class='input-group date' id='end_chart_picker'>
                                <input type='text' name="end" class="form-control" id="end"/>
                                <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <button type="submit" class="btn btn-default js_school-chart-search" data-child="{$child['child_id']}" data-username="{$username}">{__("Search")}</button>
                        </div>
                    </div>

                    <div id="chart_list" name="chart_list">
                        {include file="ci/school/ajax.school.chartsearch.tpl"}
                    </div>
                    <!-- success -->
                    <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                    <!-- error -->
                    <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                </div>
            </div>
        {elseif $sub_view == "addgrowth"}
            <div class="panel-body form-horizontal">
                <form class="form-horizontal" enctype="multipart/form-data" method="post" id="school_edit_child_health">
                    <input type="hidden" name="school_username" value="{$username}"/>
                    <input type="hidden" name="child_id" value="{$child['child_id']}"/>
                    <input type="hidden" name="child_parent_id" value="{$child['child_parent_id']}"/>
                    <input type="hidden" name="do" value="add_growth"/>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Date")} (*)</label>
                        <div class='col-sm-4'>
                            <div class='input-group date' id='birthdate_picker'>
                                <input type='text' name="recorded_at" class="form-control"/>
                                <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Height")} (*)</label>
                        <div class="col-sm-4">
                            <input type="number" min="0" class="form-control" name="height" step="any" required>
                        </div>
                        <label class="col-sm-1 control-label text-left" style = "text-align: left">{__("cm")}</label>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Weight")} (*)</label>
                        <div class="col-sm-4">
                            <input type="number" min="0" class="form-control" name="weight" step="any" required>
                        </div>
                        <label class="col-sm-1 control-label text-left" style = "text-align: left">{__("kg")}</label>
                    </div>
                    <div class = "form-group">
                        <label class="col-sm-3 control-label">{__("Nutritional status")}</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="nutriture_status">
                        </div>
                    </div>
                    <div class = "form-group">
                        <label class="col-sm-3 control-label">{__("Heartbeat")}</label>
                        <div class="col-sm-5">
                            <input type="number" class="form-control" name="heart" min="0" step="1">
                        </div>
                        <label class="col-sm-2 control-label">{__("Times/minute")}</label>
                    </div>
                    <div class = "form-group">
                        <label class="col-sm-3 control-label">{__("Blood pressure")}</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="blood_pressure">
                        </div>
                    </div>
                    <div class = "form-group">
                        <label class="col-sm-3 control-label">{__("Ear")}</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="ear">
                        </div>
                    </div>
                    <div class = "form-group">
                        <label class="col-sm-3 control-label">{__("Eye")}</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="eye">
                        </div>
                    </div>
                    <div class = "form-group">
                        <label class="col-sm-3 control-label">{__("Nose")}</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="nose">
                        </div>
                    </div>
                    <div class = "form-group">
                        <label class="col-sm-3 control-label">{__("Description")}</label>
                        <div class="col-sm-9">
                            <textarea type="text" class="form-control" name="description"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("File attachment")}</label>
                        <div class="col-sm-6">
                            <input type="file" name="file" id="file"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-3">
                            <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                        </div>
                    </div>

                    <!-- success -->
                    <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                    <!-- success -->

                    <!-- error -->
                    <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                    <!-- error -->
                </form>
            </div>
        {elseif $sub_view == "editgrowth"}
            <div class = "panel-body">
                <form class="form-horizontal" enctype="multipart/form-data" method="post" id="school_edit_child_health">
                    <input type="hidden" name="school_username" value="{$username}"/>
                    <input type="hidden" name="child_growth_id" value="{$data['child_growth_id']}"/>
                    <input type="hidden" name="child_id" value="{$child['child_id']}"/>
                    <input type="hidden" name="is_module" value="{$is_module}"/>
                    <input type="hidden" name="do" value="edit_growth"/>
                    <div class="form-group">
                        <label class = "col-sm-3 control-label text-left">{__("Date")} (*)</label>
                        <div class='col-sm-3'>
                            <div class='input-group date' id='birthdate_picker'>
                                <input type='text' name="recorded_at" value = "{$data['recorded_at']}" id="recorded_at" class="form-control"/>
                                <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                            </div>
                        </div>
                    </div>
                    <div class = "form-group">
                        <label class="col-sm-3 control-label text-left">{__("Height")}</label>
                        <div class="col-sm-3">
                            <input type="number" min="0" step = "any" class="form-control" name="height" value="{$data['height']}" placeholder="{__("Height")}">
                        </div>
                        <label class="col-sm-2 control-label text-left" style = "text-align: left">{__("cm")}</label>
                    </div>
                    <div class = "form-group">
                        <label class="col-sm-3 control-label">{__("Weight")}</label>
                        <div class="col-sm-3">
                            <input type="number" min="0" step = "any" class="form-control" name="weight" value="{$data['weight']}" placeholder="{__("Weight")}">
                        </div>
                        <label class="col-sm-2 control-label text-left" style = "text-align: left">{__("kg")}</label>
                    </div>
                    <div class = "form-group">
                        <label class="col-sm-3 control-label">{__("Nutritional status")}</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="nutriture_status" value="{$data['nutriture_status']}">
                        </div>
                    </div>
                    <div class = "form-group">
                        <label class="col-sm-3 control-label">{__("Heartbeat")}</label>
                        <div class="col-sm-5">
                            <input type="number" class="form-control" name="heart" min="0" step="1" value="{$data['heart']}">
                        </div>
                        <label class="col-sm-2 control-label">{__("Times/minute")}</label>
                    </div>
                    <div class = "form-group">
                        <label class="col-sm-3 control-label">{__("Blood pressure")}</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="blood_pressure" value="{$data['blood_pressure']}">
                        </div>
                    </div>
                    <div class = "form-group">
                        <label class="col-sm-3 control-label">{__("Ear")}</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="ear" value="{$data['ear']}">
                        </div>
                    </div>
                    <div class = "form-group">
                        <label class="col-sm-3 control-label">{__("Eye")}</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="eye" value="{$data['eye']}">
                        </div>
                    </div>
                    <div class = "form-group">
                        <label class="col-sm-3 control-label">{__("Nose")}</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="nose" value="{$data['nose']}">
                        </div>
                    </div>
                    <div class = "form-group">
                        <label class="col-sm-3 control-label">{__("Description")}</label>
                        <div class="col-sm-9">
                            <textarea type="text" class="form-control" name="description">{$data['description']}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("File attachment")}</label>
                        <div class="col-sm-6">
                            {if !is_empty($data['source_file'])}
                                <a href="{$data['source_file']}" target="_blank"><img src = "{$data['source_file']}" class = "img-responsive"></a>
                                <br>
                                <label class="control-label">{__("Choose file replace")}</label>
                                <br>
                            {/if}
                            <input type="file" name="file" id="file"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-3">
                            <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                        </div>
                    </div>

                    <!-- success -->
                    <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                    <!-- success -->
                    <!-- error -->
                    <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                    <!-- error -->
                </form>
            </div>
        {elseif $sub_view == "developmentindex"}
            <div class="panel-body with-table">
                <div class="mb10" align="center"><strong>{__("Tăng giảm cân khi khối lượng của trẻ thay đổi từ 0.1kg trở lên")}</strong></div>
                <div class="row">
                    <div class='col-sm-3 col-sm-offset-3'>
                        <div class="form-group">
                            <select name="class_id" id="class_id" class="form-control js_development-index-search" data-username="{$username}">
                                <option value="">{__("Select Class")}...</option>
                                {if $school['children_use_no_class']}
                                    <option value="0" {if $result['class_id'] == '0'}selected{/if}>{__("No class")}</option>
                                {/if}
                                {$class_level = -1}
                                {foreach $classes as $class}
                                    {if ($class_level != $class['class_level_id'])}
                                        <option value="" disabled>-----{$class['class_level_name']}-----</option>
                                    {/if}
                                    <option value="{$class['group_id']}" {if $result['class_id'] == $class['group_id']}selected{/if}>{$class['group_title']}</option>
                                    {$class_level = $class['class_level_id']}
                                {/foreach}
                            </select>
                        </div>
                    </div>
                    {*<div class='col-sm-3'>*}
                    {*<div class = "row">*}
                    {*<div class = "col-sm-6">*}
                    {*<div class="form-group">*}
                    {*<a href="#" id="search" class="btn btn-default js_child-search" data-username="{$username}" data-isnew="1" data-id="{$school['page_id']}">{__("Search")}</a>*}
                    {*<label id="loading" class="btn btn-info x-hidden">{__("Loading")}...</label>*}
                    {*</div>*}
                    {*</div>*}
                    {*</div>*}
                    {*</div>*}
                </div>
                <br/>
                <div class="table-responsive" id="development_index">
                    {*{include file="ci/school/ajax.school.development.index.tpl"}*}
                </div>
            </div>
        {elseif $sub_view == "developmentclass"}
            <div class="panel-body with-table">
                <div class="table-responsive">
                    <div class="mb10"><strong>{__("Summarize the growth index of the child in the class")}</strong></div>
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th><strong>{__("#")}</strong></th>
                            <th>{__("Child")}</th>
                            <th>{__("Height")} (cm)</th>
                            <th>{__("Weight")} (kg)</th>
                            <th>{__("The previous weight")} (kg)</th>
                            <th>{__("Status")}</th>
                        </tr>
                        </thead>
                        <tbody>
                        {$idx = 1}
                        {foreach $childGrowths as $row}
                            <tr>
                                <td class="align-middle" align="center"><strong>{$idx}</strong></td>
                                <td class="align-middle">{$row['child_name']}</td>
                                <td class="align-middle" align="center">{$row['height']}</td>
                                <td class="align-middle" align="center">{$row['weight']}</td>
                                <td class="align-middle" align="center">{$row['last_time_weight']}</td>
                                <td class="align-middle" style="font-weight: bold">
                                    {if $row['weight_status'] == WEIGHT_UNAVAILABLE}
                                        {__("Unavailable")}
                                    {elseif $row['weight_status'] == WEIGHT_GET_WEIGHT}
                                        {__("Get weight")}
                                    {elseif $row['weight_status'] == WEIGHT_LOSE_WEIGHT}
                                        {__("Lose weight")}
                                    {elseif $row['weight_status'] == WEIGHT_NO_CHANGED}
                                        {__("Not changed")}
                                    {/if}
                                </td>
                            </tr>
                            {$idx = $idx + 1}
                        {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
        {elseif $sub_view=="addphoto"}
            <div class="panel-body">
                <div class="mb10" align="center">
                    <strong>{__("Add photo to diary")} {__("for")} {$child['child_name']}</strong>
                </div>
                <form class="form-horizontal" enctype="multipart/form-data" method="post" id="add_child_journal_school">
                    <input type="hidden" name="school_username" value="{$username}"/>
                    <input type="hidden" name="child_id" value="{$child['child_id']}"/>
                    <input type="hidden" name="child_parent_id" value="{$child['child_parent_id']}"/>
                    <input type="hidden" name="do" value="add_photo"/>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Caption")}</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="caption" placeholder="{__("Caption")}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Picture")}</label>
                        <div class="col-sm-6">
                            <input name="file[]" type="file" multiple="true">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-3">
                            <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                        </div>
                    </div>
                </form>
            </div>
        {elseif $sub_view=="journal"}
            <div class="panel-body with-table form-horizontal">
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Year")}</label>
                    <div class="col-sm-3">
                        <select name="year" id="year" class="form-control">
                            <option value = "0">{__("Select year ...")}</option>
                            {for $i = $year_begin; $i < $year_end; $i++}
                                <option value="{$i}">{$i}</option>
                            {/for}
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <a class="btn btn-default js_school-journal-search" data-username="{$username}" data-id="{$child['child_id']}" data-handle = "search">{__("Search")}</a>
                    </div>
                </div>
                <div id = "journal_list">
                    {include file="ci/ajax.school.journal.list.tpl"}
                </div>
            </div>
        {elseif $sub_view == "adddiary"}
            <div class="panel-body">
                <form class="form-horizontal" enctype="multipart/form-data" method="post" id="add_child_journal_school">
                    <input type="hidden" name="school_username" value="{$username}"/>
                    <input type="hidden" name="do" value="add_photo"/>
                    <input type="hidden" name="reload" value="1">
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Select student")} (*)</label>
                        <div class="col-sm-4">
                            <select name="class_id" id="medicine_class_id" data-username="{$username}" class="form-control" autofocus>
                                <option value="">{__("Select class")}</option>
                                {$class_level = -1}
                                {foreach $classes as $class}
                                    {if ($class_level != $class['class_level_id'])}
                                        <option value="" disabled style="color: blue">-----{$class['class_level_name']}-----</option>
                                    {/if}
                                    <option value="{$class['group_id']}">{$class['group_title']}</option>
                                    {$class_level = $class['class_level_id']}
                                {/foreach}
                            </select>
                        </div>
                        <div class="col-sm-5">
                            <select name="child_id" id="medicine_child_id" class="form-control" required></select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Caption")}</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="caption" placeholder="{__("Caption")}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Picture")}</label>
                        <div class="col-sm-6">
                            <input name="file[]" type="file" multiple="true">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-3">
                            <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                        </div>
                    </div>
                </form>
            </div>
        {elseif $sub_view == "movesclass"}
            <div class="panel-body with-table">
                <form class="js_ajax-forms-success-reload form-horizontal" data-url="ci/bo/school/bo_child.php">
                    <input type="hidden" name="school_username" id="school_username" value="{$username}"/>
                    <input type="hidden" name="do" id="do" value="movesclass"/>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Select Class")} (*)</label>
                        <div class="col-sm-3">
                            <select name="old_class_id" id="class_id" data-username="{$username}" class="form-control js_school-move-class-search" autofocus>
                                <option value="">{__("Select class")}</option>
                                <option value="0">{__("No class")}</option>
                                {$class_level = -1}
                                {foreach $classes as $class}
                                    {if ($class_level != $class['class_level_id'])}
                                        <option value="" disabled style="color: blue">-----{$class['class_level_name']}-----</option>
                                    {/if}
                                    <option value="{$class['group_id']}">{$class['group_title']}</option>
                                    {$class_level = $class['class_level_id']}
                                {/foreach}
                            </select>
                        </div>
                    </div>
                    <div class = "form-group">
                        <label class="col-sm-3 control-label text-left">
                            {__("Select student")} (*)
                        </label>
                        <div id="list_child" class = "col-sm-9">

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("New class")} (*)</label>
                        <div class="col-sm-4">
                            <select name="new_class_id" class="form-control">
                                {foreach $classes as $class}
                                    <option value="{$class['group_id']}">{$class['group_title']}</option>
                                {/foreach}
                            </select>
                        </div>
                    </div>
                    <div class="form-group pl5" id="btnSave_disable">
                        <div class="col-sm-9">
                            <button type="submit" class="btn btn-primary padrl30" disabled>{__("Save")}</button>
                        </div>
                    </div>
                    <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                    <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                </form>
            </div>
        {/if}
    {/if}
</div>