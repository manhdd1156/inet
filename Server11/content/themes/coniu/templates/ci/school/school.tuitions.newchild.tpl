<div class="panel-body">
    <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_tuition.php">
        <input type="hidden" name="school_username" id="school_username" value="{$username}"/>
        <input type="hidden" name="do" value="newchild"/>
        <input type="hidden" name="tuition_id" value="{$data['tuition_id']}"/>
        <div class="form-group">
            <label class="col-sm-3 control-label text-left">{__("Class")}</label>
            <div class="col-sm-3">
                <select name="class_id" id="tuition_class_id" class="form-control" data-username="{$username}" readonly="true">
                    <option value="{$data['class_id']}">{$data['group_title']}</option>
                </select>
            </div>
        </div>
        <input type="hidden" name="is_notified" value="{$data['is_notified']}"/>
        <div class="form-group">
            <label class="col-sm-3 control-label text-left">{__("Month")}</label>
            <div class='col-sm-2'>
                <input type='text' name="month" id="month" value="{$data['month']}" class="form-control" readonly/>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label text-left">{__("Number of school days")}&nbsp;({$smarty.const.MONEY_UNIT})</label>
            <div class="col-sm-2">
                <input type='text' name="day_of_month" value="{$data['day_of_month']}" id="day_of_month" class="form-control" readonly/>
            </div>
        </div>
        <div id="open_dialog" class="x-hidden" title="{__("Adding fees to calculate tuition")|upper}"></div>
        <div class="form-group">
            <div class="col-sm-9 col-sm-offset-3">
                <button type="submit" class="btn btn-primary" {if count($results['children']) == 0}disabled{/if}>{__("Save")}</button>
            </div>
        </div>
        {if count($results['children']) > 0}
            <div class="table-responsive" id="children_list">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>
                            <div class="pull-left flip">{__("Children list")|upper}&nbsp;({$results['children']|count})</div>
                            <br/>
                            <div class="pull-right flip">
                                &nbsp;&nbsp;{__("New tuition total of class")}&nbsp;({$smarty.const.MONEY_UNIT})
                                <input style="width: 100px;" type="text" class="text-right money_tui" id="class_total" name="class_total" value="{$results['total_amount']}" readonly/>
                            </div>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    {$childIdx = 0}
                    {foreach $results['children'] as $child}
                        {$childIdx = $childIdx + 1}
                        <tr id="child_row_{$child['child_id']}">
                            <td id="child_tuition_{$child['child_id']}">
                                {include file="ci/school/ajax.school.tuition.child.tpl"}
                            </td>
                        </tr>
                    {/foreach}
                        <tr>
                            <td>
                                <div class="pull-right flip">
                                    <strong>
                                        &nbsp;&nbsp;{__("New tuition total of class")}&nbsp;({$smarty.const.MONEY_UNIT})
                                        <input style="width: 100px;" type="text" class="text-right money_tui" id="class_total_2" name="class_total_2" value="{$results['total_amount']}" readonly/>
                                    </strong>
                                </div>
                                <br/>
                                <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
            <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
        {else}
            <div><strong>{__("There is no new student in the class")}.</strong></div>
        {/if}
        {* Để thông tin toàn bộ lớp bên dưới để khi tính lại tổng học phí trên màn hình lớp cho dễ *}
        {*<input type="hidden" name="total_child_deduction[]" value="{$data['total_deduction']}"/>*}
        <input type="hidden" class="money_tui" name="child_total[]" value="{$data['total_amount']}"/>
    </form>
</div>
