<div><strong>{__("Consecutive absent student list")}&nbsp;({$children|count}&nbsp;{__("Child")})</strong></div>
<table class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th>#</th>
        <th>{__("Full name")}</th>
        <th>{__("Class")}</th>
        <th>{__("Absent day")}</th>
        <th>{__("Reason")}</th>
    </tr>
    </thead>
    <tbody>

        {$idx = 1}
        {foreach $children as $child}
            <tr>
                <td align="center">{$idx}</td>
                <td><a href="{$system['system_url']}/school/{$username}/attendance/child/{$child['child_id']}">{$child['child_name']}</a></td>
                <td align="center">{$child['group_title']}</td>
                <td align="center">
                    {if isset($child['number_of_absent_day']) && ($child['number_of_absent_day'] > 0)}
                        {if {$child['number_of_absent_day']} >= $smarty.const.MAX_CONSECUTIVE_ABSENT_DAY}
                            {__("More than")}&nbsp;{$smarty.const.MAX_CONSECUTIVE_ABSENT_DAY}
                        {else}
                            {$child['number_of_absent_day']}
                        {/if}
                        &nbsp;{__("day(s)")}
                    {/if}
                </td>
                <td>{$child['reason']}</td>
            </tr>
            {$idx = $idx + 1}
        {/foreach}
        {if $children|count == 0}
            <tr class="odd">
                <td valign="top" align="center" colspan="5" class="dataTables_empty">
                    {__("No data available in table")}
                </td>
            </tr>
        {/if}

    </tbody>
</table>