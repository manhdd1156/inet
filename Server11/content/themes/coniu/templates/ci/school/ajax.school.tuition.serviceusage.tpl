{* Nội dung màn hình popup hiển thị thông tin sử dụng dịch vụ của lớp tháng trước*}
<table class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th>#</th>
            <th>{__("Full name")}</th>
            <th>{__("Charged day")}</th>
            <th>
                {__("No-fee day")}<br/><i>({__("Absent day")}, {__("Deduction day")})</i>
            </th>
            {foreach $daily_services as $service}
                <th>{$service['service_name']}<br/><i>({__("Unit price")}: {moneyFormat($service['fee'])})</i></th>
            {/foreach}
            {foreach $cbservices as $service}
                <th>{$service['service_name']}<br/><i>({__("Unit price")}: {moneyFormat($service['fee'])})</i></th>
            {/foreach}
            <th>{__("Late PickUp")}</th>
            <th>{__("Previous debt")}</th>
        </tr>
    </thead>
    <tbody>
    {foreach $children as $idx=>$child}
        <tr>
            <td align="center"><strong>{$idx + 1}</strong></td>
            <td>{$child['child_name']}</td>
            <td align="center">{$child['attendance_count']}</td>
            <td align="center">{$child['absent_count']}</td>
            {foreach $daily_services as $service}
                <td align="center">
                    {if in_array($service['service_id'], $children_services[$child['child_id']])}
                        {$child['attendance_count']}{if $service['monthly_daily_deduction'] > 0} - {$service['monthly_daily_deduction']}{/if}
                    {/if}
                </td>
            {/foreach}
            {foreach $cbservices as $service}
                <td align="center">
                    {if in_array($service['service_id'], $children_services[$child['child_id']])}
                        {$service['CNT']}
                    {/if}
                </td>
            {/foreach}
            <td class="text-right">{moneyFormat($child['pickup'])}</td>
            <td class="text-right">{moneyFormat($child['pre_month_debt_amount'])}</td>
        </tr>
    {/foreach}
    </tbody>
</table>
<div class="pull-right flip">
    <a class="btn btn-warning js_tuition_us_popup_close">{__("Close")}</a>
</div>
