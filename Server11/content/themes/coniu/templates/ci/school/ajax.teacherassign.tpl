<thead>
<tr>
    <th class="col-sm-3"><center>Thời gian</center></th>
    <th class="col-sm-9"><center>Người được phân công</center></th>
</tr>
<tr>
    <th class="col-sm-3"><center></center></th>
    <th class="col-sm-9">
        <div id="teacher_list_2" {if $day['mon'] != $day['begin']} class="x-hidden"{/if}>
            {foreach $teachers as $teacher }
                <label class="col-xs-12 col-sm-6 text-left">
                    <input type="checkbox" class="teachers" value="{$teacher['user_id']}" data-name="{$teacher['user_fullname']}"
                            {if (in_array($teacher['user_id'], $list[2]))} checked{/if}> {$teacher['user_fullname']}
                </label>
            {/foreach}
        </div>

        <div id="teacher_list_3" {if $day['tue'] != $day['begin']} class="x-hidden"{/if}>
            {foreach $teachers as $teacher }
                <label class="col-xs-12 col-sm-6 text-left">
                    <input type="checkbox" class="teachers" value="{$teacher['user_id']}" data-name="{$teacher['user_fullname']}"
                            {if (in_array($teacher['user_id'], $list[3]))} checked{/if}> {$teacher['user_fullname']}
                </label>
            {/foreach}
        </div>

        <div id="teacher_list_4" {if $day['wed'] != $day['begin']} class="x-hidden"{/if}>
            {foreach $teachers as $teacher }
                <label class="col-xs-12 col-sm-6 text-left">
                    <input type="checkbox" class="teachers" value="{$teacher['user_id']}" data-name="{$teacher['user_fullname']}"
                            {if (in_array($teacher['user_id'], $list[4]))} checked{/if}> {$teacher['user_fullname']}
                </label>
            {/foreach}
        </div>

        <div id="teacher_list_5" {if $day['thu'] != $day['begin']} class="x-hidden"{/if}>
            {foreach $teachers as $teacher }
                <label class="col-xs-12 col-sm-6 text-left">
                    <input type="checkbox" class="teachers" value="{$teacher['user_id']}" data-name="{$teacher['user_fullname']}"
                            {if (in_array($teacher['user_id'], $list[5]))} checked{/if}> {$teacher['user_fullname']}
                </label>
            {/foreach}
        </div>

        <div id="teacher_list_6" {if $day['fri'] != $day['begin']} class="x-hidden"{/if}>
            {foreach $teachers as $teacher }
                <label class="col-xs-12 col-sm-6 text-left">
                    <input type="checkbox" class="teachers" value="{$teacher['user_id']}" data-name="{$teacher['user_fullname']}"
                            {if (in_array($teacher['user_id'], $list[6]))} checked{/if}> {$teacher['user_fullname']}
                </label>
            {/foreach}
        </div>

        <div id="teacher_list_7" {if $day['sat'] != $day['begin']} class="x-hidden"{/if}>
            {foreach $teachers as $teacher }
                <label class="col-xs-12 col-sm-6 text-left">
                    <input type="checkbox" class="teachers" value="{$teacher['user_id']}" data-name="{$teacher['user_fullname']}"
                            {if (in_array($teacher['user_id'], $list[7]))} checked{/if}> {$teacher['user_fullname']}
                </label>
            {/foreach}
        </div>

        <div id="teacher_list_8" {if $day['sun'] != $day['begin']} class="x-hidden"{/if}>
            {foreach $teachers as $teacher }
                <label class="col-xs-12 col-sm-6 text-left">
                    <input type="checkbox" class="teachers" value="{$teacher['user_id']}" data-name="{$teacher['user_fullname']}"
                            {if (in_array($teacher['user_id'], $list[8]))} checked{/if}> {$teacher['user_fullname']}
                </label>
            {/foreach}
        </div>

    </th>
</tr>
</thead>
<tbody>
<tr>
    <td>
        <label>
            <input type="radio" class="day" name="day" value="2" {if $day['mon'] == $day['begin']} checked{/if}/> Thứ 2 - {$day['mon']}
        </label>
        {*<input type="checkbox" class="child" name="day" value="monday" checked />*}
    </td>
    <td><center>
            <div id="teacher_mon" class="col-sm-9">
                {if isset($data[2]) }
                    {foreach $data[2] as $dt }
                        <div id="assign_teacher_mon_{$dt['user_id']}">
                            <label class="col-xs-12 col-sm-6 text-left">&#9679;&#09;{$dt['user_fullname']}
                                <input type="text" class="x-hidden" name="assign_2[]" value="{$dt['user_id']}">
                            </label>
                        </div>
                    {/foreach}
                {/if}
            </div>
        </center>
    </td>
</tr>
<tr>
    <td>
        <label>
            <input type="radio" class="day" name="day" value="3" {if $day['tue'] == $day['begin']} checked{/if}/> Thứ 3 - {$day['tue']}
        </label>
        {*<input type="checkbox" class="child" name="day" value="tuesday" checked />*}
    </td>
    <td><center>
            <div id="teacher_tue" class="col-sm-9">
                {if isset($data[3]) }
                    {foreach $data[3] as $dt }
                        <div id="assign_teacher_tue_{$dt['user_id']}">
                            <label class="col-xs-12 col-sm-6 text-left">&#9679;&#09;{$dt['user_fullname']}
                                <input type="text" class="x-hidden" name="assign_3[]" value="{$dt['user_id']}">
                            </label>
                        </div>
                    {/foreach}
                {/if}
            </div>
        </center>
    </td>
</tr>
<tr>
    <td>
        <label>
            <input type="radio" class="day" name="day" value="4" {if $day['wed'] == $day['begin']} checked{/if}/> Thứ 4 - {$day['wed']}
        </label>
        {*<input type="checkbox" class="child" name="day" value="wednesday" checked />*}
    </td>
    <td><center>
            <div id="teacher_wed" class="col-sm-9">
                {if isset($data[4]) }
                    {foreach $data[4] as $dt }
                        <div id="assign_teacher_wed_{$dt['user_id']}">
                            <label class="col-xs-12 col-sm-6 text-left">&#9679;&#09;{$dt['user_fullname']}
                                <input type="text" class="x-hidden" name="assign_4[]" value="{$dt['user_id']}">
                            </label>
                        </div>
                    {/foreach}
                {/if}
            </div>
        </center>
    </td>
</tr>
<tr>
    <td>
        <label>
            <input type="radio" class="day" name="day" value="5" {if $day['thu'] == $day['begin']} checked{/if}/> Thứ 5 - {$day['thu']}
        </label>
        {*<input type="checkbox" class="child" name="day" value="thursday" checked />*}

    </td>
    <td><center>
            <div id="teacher_thu" class="col-sm-9">
                {if isset($data[5]) }
                    {foreach $data[5] as $dt }
                        <div id="assign_teacher_thu_{$dt['user_id']}">
                            <label class="col-xs-12 col-sm-6 text-left">&#9679;&#09;{$dt['user_fullname']}
                                <input type="text" class="x-hidden" name="assign_5[]" value="{$dt['user_id']}">
                            </label>
                        </div>
                    {/foreach}
                {/if}
            </div>
        </center>
    </td>
</tr>
<tr>
    <td>
        <label>
            <input type="radio" class="day" name="day" value="6" {if $day['fri'] == $day['begin']} checked{/if}/> Thứ 6 - {$day['fri']}
        </label>
        {*<input type="checkbox" class="child" name="day" value="friday" checked />*}
    </td>
    <td><center>
            <div id="teacher_fri" class="col-sm-9">
                {if isset($data[6]) }
                    {foreach $data[6] as $dt }
                        <div id="assign_teacher_fri_{$dt['user_id']}">
                            <label class="col-xs-12 col-sm-6 text-left">&#9679;&#09;{$dt['user_fullname']}
                                <input type="text" class="x-hidden" name="assign_6[]" value="{$dt['user_id']}">
                            </label>
                        </div>
                    {/foreach}
                {/if}
            </div>
        </center>
    </td>
</tr>
<tr>
    <td>
        <label>
            <input type="radio" class="day" name="day" value="7" {if $day['sat'] == $day['begin']} checked{/if}/> Thứ 7 - {$day['sat']}
        </label>
        {*<input type="checkbox" class="child" name="day" value="saturday" checked />*}
    </td>
    <td><center>
            <div id="teacher_sat" class="col-sm-9">
                {if isset($data[7]) }
                    {foreach $data[7] as $dt }
                        <div id="assign_teacher_sat_{$dt['user_id']}">
                            <label class="col-xs-12 col-sm-6 text-left">&#9679;&#09;{$dt['user_fullname']}
                                <input type="text" class="x-hidden" name="assign_7[]" value="{$dt['user_id']}">
                            </label>
                        </div>
                    {/foreach}
                {/if}
            </div>
        </center>
    </td>
</tr>
<tr>
    <td>
        <label>
            <input type="radio" class="day" name="day" value="8" {if $day['sun'] == $day['begin']} checked{/if}/> Chủ nhật - {$day['sun']}
        </label>
        {*<input type="checkbox" class="child" name="day" value="sunday" checked />*}
    </td>
    <td><center>
            <div id="teacher_sun" class="col-sm-9">
                {if isset($data[8]) }
                    {foreach $data[8] as $dt }
                        <div id="assign_teacher_sun_{$dt['user_id']}">
                            <label class="col-xs-12 col-sm-6 text-left">&#9679;&#09;{$dt['user_fullname']}
                                <input type="text" class="x-hidden" name="assign_8[]" value="{$dt['user_id']}">
                            </label>
                        </div>
                    {/foreach}
                {/if}
            </div>
        </center>
    </td>
</tr>
</tbody>