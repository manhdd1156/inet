<div class="panel-body with-table">
    <table class = "table table-bordered">
        <tbody>
        <tr>
            <td class = "col-sm-3 text-right">{__('Menu name')}</td>
            <td>
                {$data['menu_name']}
            </td>
        </tr>
        <tr>
            <td class = "col-sm-3 text-right">{__('Scope')}</td>
            <td>
                {if isset($class)} {$class['group_title']}
                {elseif isset($class_level)}{$class_level['class_level_name']}
                {elseif !isset($class) && !isset($class_level)} {__('School')}
                {/if}
            </td>
        </tr>
        <tr>
            <td class = "col-sm-3 text-right">{__('Begin')}</td>
            <td>
                {$data['begin']}
            </td>
        </tr>
        <tr>
            <td class = "col-sm-3 text-right">{__('Use meal')}</td>
            <td>
                {if $data['is_meal']}
                    {__('Yes')}
                {else}
                    {__('No')}
                {/if}
            </td>
        </tr>
        <tr>
            <td class = "col-sm-3 text-right">{__('Study saturday')}</td>
            <td>
                {if $data['is_saturday']}
                    {__('Yes')}
                {else}
                    {__('No')}
                {/if}
            </td>
        </tr>
        <tr>
            <td class = "col-sm-3 text-right">{__('Description')}</td>
            <td>
                {nl2br($data['description'])}
            </td>
        </tr>
        </tbody>
    </table>
    <div class = "table-responsive">
        <table class = "table table-striped table-bordered table-hover">
            <thead>
            <tr>
                <th>{__('#')}</th>
                <th>{__('Time')} </th>
                <th {if !$data['is_meal']}class="hidden"{/if}>{__('Meal')}</th>
                <th>{__('Monday')} </th>
                <th>{__('Tuesday')}</th>
                <th> {__('Wednesday')}</th>
                <th>{__('Thursday')}</th>
                <th>{__('Friday')} </th>
                <th {if !$data['is_saturday']}class="hidden"{/if}>{__('Saturday')}</th>
            </tr>
            </thead>
            <tbody>
            {$idx = 1}
            {foreach $data['details'] as $k => $row}
                {$array = array_values($row)}
                {$temp = array()}
                {foreach $array as $k => $value}
                    {if $data['is_saturday']}
                        {if ($k >= 2) && ($k < (count($array)))}
                            {$temp[] = $value}
                        {/if}
                    {else}
                        {if ($k >= 2) && ($k < (count($array) - 1))}
                            {$temp[] = $value}
                        {/if}
                    {/if}
                {/foreach}
                <tr>
                    <td style="vertical-align: middle" align="center">
                        <strong>{$idx}</strong>
                    </td>
                    <td style="vertical-align: middle" align="center">
                        <strong>{$row['meal_time']}</strong>
                    </td>
                    <td {if !$data['is_meal']}class="hidden"{/if} align="center" style = "vertical-align: middle;">
                        <strong>{$row['meal_name']}</strong>
                    </td>
                    {$col = 1}
                    {for $i = 0; $i < count($temp); $i++}
                        {if $temp[$i] === $temp[($i+1)]}
                            {$col = $col + 1}
                        {else}
                            <td colspan = "{$col}" align="center">
                                {nl2br($temp[$i])}
                            </td>
                            {$col = 1}
                        {/if}
                    {/for}
                </tr>
                {$idx = $idx + 1}
            {/foreach}
            </tbody>
        </table>
    </div>
</div>