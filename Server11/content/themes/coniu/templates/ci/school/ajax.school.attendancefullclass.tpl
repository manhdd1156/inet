{if $school['attendance_use_leave_early'] || $school['attendance_use_come_late'] || $school['attendance_absence_no_reason']}
    <div class="table-responsive" id="getFixed">
        <table class="table table-striped table-bordered">
            <tbody>
            <tr>
                <td align="right"><strong>{__("Symbol")}:</strong></td>
                {if $school['attendance_use_come_late']}
                    <td bgcolor="#6495ed" align="center"><i class="fa fa-check" aria-hidden="true"></i></td>
                    <td align="left">{__("Come late")}</td>
                {/if}
                {if $school['attendance_use_leave_early']}
                    <td bgcolor="#008b8b" align="center"><i class="fa fa-check" aria-hidden="true"></i></td>
                    <td align="left">{__("Leave early")}</td>
                {/if}
                <td bgcolor="#fff" align="center"><i class="fa fa-check" aria-hidden="true"></i></td>
                <td align="left">{__("Present")}</td>

                {if $school['attendance_absence_no_reason']}
                    <td bgcolor="#ff1493" align="center"><i class="fa fa-times" aria-hidden="true"></i></td>
                    <td align="left">{__("Without permission")}</td>
                {/if}

                <td bgcolor="#deb887" align="center"><i class="fa fa-times" aria-hidden="true"></i></td>
                <td align="left">{__("With permission")}</td>
            </tr>
            </tbody>
        </table>
    </div>
{/if}
{if $rows|count > 0}
    <div style="z-index: 2">
        <div><strong>{__("The whole class attendance")}</strong></div>
        <div id="table_button">
            <button id="left">&larr;</button>
            <button id="right">&rarr;</button>
        </div>
    </div>
    <div class="table-responsive" id="example">
        <table class="table table-striped table-bordered table-pinned" style="z-index: 1">
            <thead>
            <tr bgcolor="#ffebcd">
                <th class="pinned" style="padding: 8px 6px" id="width_no">#</th>
                <th nowrap="true" class="pinned" style="padding: 8px 6px">{__("Full name")}</th>
                {$dayIdx = 0}
                {$sunDayIdxs = array()}
                {foreach $dates as $date}
                    {if $days[$dayIdx] == 'CN'}
                        {$sunDayIdxs[] = $dayIdx + 1}
                        <th align="center" bgcolor="#a9a9a9">
                            {else}
                        <th align="center">
                    {/if}
                    {$date}<br>
                    <label style="color: blue">{$days[$dayIdx]}</label>
                    </th>
                    {$dayIdx = $dayIdx + 1}
                {/foreach}
                <th nowrap="true" style="padding: 8px 6px">{__("Present")}</th>
                <th nowrap="true" style="padding: 8px 6px">{__("Absence")}</th>
            </tr>
            </thead>
            <link rel="stylesheet" href="{$system['system_url']}/includes/assets/css/font-awesome/css/font-awesome.css">
            <tbody>
            {$rowIdx = 1}
            {foreach $rows as $k => $row}
                <tr {if $row['child_status'] == 0} class="row-disable" {/if}>
                    <td align="center" class="pinned">{$rowIdx}</td>
                    <td nowrap="true" class="pinned"><a href="{$system['system_url']}/school/{$username}/attendance/child/{$row['child_id']}">{$row['child_name']}</a></td>

                    {$rowIdx = $rowIdx + 1}
                    {$presentCnt = 0}
                    {$absenceCnt = 0}
                    {foreach $row['cells'] as $cell}
                        {if $cell['is_checked'] == 1}
                            <td align="center" style="vertical-align: middle; padding: 0px; position: relative">
                                <select {if !$canEdit}disabled{/if} data-username="{$username}" data-id="{$cell['attendance_id']}" data-child="{$cell['child_id']}" data-date="{$cell['attendance_date']}" data-status="{$cell['status']}" name="attendance_status" class="js_school-attendance-status" style="border:none; font-family: 'FontAwesome', 'Second Font name'; position: absolute; top: 0; left: 0; width: 100%; height: 100%; {if $cell['status'] == $smarty.const.ATTENDANCE_COME_LATE}
                                        background: #6495ed;
                                {elseif $cell['status'] == $smarty.const.ATTENDANCE_PRESENT}
                                        background: #fff;
                                {elseif $cell['status'] == $smarty.const.ATTENDANCE_EARLY_LEAVE}
                                        background: #008b8b;
                                {elseif $cell['status'] == $smarty.const.ATTENDANCE_ABSENCE_NO_REASON}
                                        background: #ff1493;
                                {elseif $cell['status'] == $smarty.const.ATTENDANCE_ABSENCE}
                                        background: #deb887;
                                {/if};
                                        -webkit-appearance: none;
                                        -moz-appearance: none;
                                        text-indent: 1px;
                                        text-overflow: '';
                                        text-align-last: center;
                                        ">
                                    <option value="{$smarty.const.ATTENDANCE_PRESENT}" style="background: #fff; padding: 5px 0; font-size: 20px; text-align: center">&#xf00c</option>
                                    {if $school['attendance_use_come_late'] || $cell['status'] == $smarty.const.ATTENDANCE_COME_LATE}
                                        <option value="{$smarty.const.ATTENDANCE_COME_LATE}" style="background: #6495ed; padding: 5px 0; font-size: 20px; text-align: center" {if $cell['status'] == $smarty.const.ATTENDANCE_COME_LATE} selected {/if}>&#xf00c</option>
                                    {/if}
                                    {if $school['attendance_use_leave_early'] || $cell['status'] == $smarty.const.ATTENDANCE_EARLY_LEAVE}
                                        <option value="{$smarty.const.ATTENDANCE_EARLY_LEAVE}" style="background: #008b8b; padding: 5px 0; font-size: 20px; text-align: center" {if $cell['status'] == $smarty.const.ATTENDANCE_EARLY_LEAVE} selected {/if}>&#xf00c</option>
                                    {/if}
                                    {if $school['attendance_absence_no_reason'] || $cell['status'] == $smarty.const.ATTENDANCE_ABSENCE_NO_REASON}
                                        <option value="{$smarty.const.ATTENDANCE_ABSENCE_NO_REASON}" style="background: #ff1493; padding: 5px 0; font-size: 20px; text-align: center" {if $cell['status'] == $smarty.const.ATTENDANCE_ABSENCE_NO_REASON} selected {/if}>&#xf00d</option>
                                    {/if}
                                    <option value="{$smarty.const.ATTENDANCE_ABSENCE}" style="background: #deb887; padding: 5px 0; font-size: 20px; text-align: center" {if $cell['status'] == $smarty.const.ATTENDANCE_ABSENCE}selected{/if}>&#xf00d</option>

                                    {*<option value="{$smarty.const.ATTENDANCE_PRESENT}" style="background: #fff; padding: 5px 0; font-size: 20px;">Đi học</option>*}
                                    {*<option value="{$smarty.const.ATTENDANCE_COME_LATE}" style="background: #6495ed; padding: 5px 0; font-size: 20px;" {if $cell['status'] == $smarty.const.ATTENDANCE_COME_LATE} selected {/if}>Đi học muộn</option>*}
                                    {*<option value="{$smarty.const.ATTENDANCE_EARLY_LEAVE}" style="background: #008b8b; padding: 5px 0; font-size: 20px;" {if $cell['status'] == $smarty.const.ATTENDANCE_EARLY_LEAVE} selected {/if}>Về sớm</option>*}
                                    {*<option value="{$smarty.const.ATTENDANCE_ABSENCE_NO_REASON}" style="background: #ff1493; padding: 5px 0; font-size: 20px;" {if $cell['status'] == $smarty.const.ATTENDANCE_ABSENCE_NO_REASON} selected {/if}>Nghỉ học vẫn tính phí</option>*}
                                    {*<option value="{$smarty.const.ATTENDANCE_ABSENCE}" style="background: #deb887; padding: 5px 0; font-size: 20px;" {if $cell['status'] == $smarty.const.ATTENDANCE_ABSENCE}selected{/if}>Nghỉ học</option>*}
                                </select>
                                {if ($cell['status'] == $smarty.const.ATTENDANCE_PRESENT) || ($cell['status'] == $smarty.const.ATTENDANCE_COME_LATE)
                                || ($cell['status'] == $smarty.const.ATTENDANCE_EARLY_LEAVE)}
                                    {$presentCnt = $presentCnt + 1}
                                {else}
                                    {$absenceCnt = $absenceCnt + 1}
                                {/if}
                            </td>
                        {else}
                            <td bgcolor="#a9a9a9"></td>
                        {/if}
                    {/foreach}
                    <td align="center" id="present_{$row['child_id']}" style="color: blue; font-weight: bold">{$presentCnt}</td>
                    <td align="center" id="absence_{$row['child_id']}" style="color: red; font-weight: bold">{$absenceCnt}</td>
                </tr>
                {*Sau 15 trẻ thì thêm header cho dễ nhìn*}
                {if $rowIdx % 16 == 0}
                    <tr bgcolor="#ffebcd">
                        <th class="pinned">#</th>
                        <th nowrap="true" class="pinned">{__("Full name")}</th>
                        {$dayIdx = 0}
                        {$sunDayIdxs = array()}
                        {foreach $dates as $date}
                            {if $days[$dayIdx] == 'CN'}
                                {$sunDayIdxs[] = $dayIdx + 1}
                                <th align="center" bgcolor="#a9a9a9">
                                    {else}
                                <th align="center">
                            {/if}
                            {$date}<br>
                            <label style="color: blue">{$days[$dayIdx]}</label>
                            </th>
                            {$dayIdx = $dayIdx + 1}
                        {/foreach}
                        <th nowrap="true">{__("Present")}</th>
                        <th nowrap="true">{__("Absence")}</th>
                    </tr>
                {/if}
            {/foreach}
            {if ($rowIdx > 2)}
                <tr>
                    <td colspan="2" align="center" class="pinned"><strong>{__('Present total of class')}</strong></td>
                    {$presentTotal = 0}
                    {foreach $last_rows as $cell}
                        <td id="present_count_{$cell['attendance_id']}" align="center" bgcolor="#ffebcd" style="color: blue; font-weight: bold">
                            {if $cell['is_checked'] == 1}
                                {$cell['present_count']}
                                {$presentTotal = $presentTotal + $cell['present_count']}
                            {/if}
                        </td>
                    {/foreach}
                    {*Ô tổng hợp ĐI HỌC cả lớp trong toàn khoản thời gian*}
                    <td id="total_present" align="center" bgcolor="#ffebcd" style="color: blue; font-weight: bold">
                        {$presentTotal}
                    </td>
                    <td align="center" bgcolor="#ffebcd"></td>
                </tr>
                <tr>
                    <td colspan="2" align="center" class="pinned"><strong>{__('Absence total of class')}</strong></td>
                    {$absentTotal = 0}
                    {foreach $last_rows as $cell}
                        <td id="absence_count_{$cell['attendance_id']}" align="center" bgcolor="#ffebcd" style="color: red; font-weight: bold">
                            {if $cell['is_checked'] == 1}
                                {$cell['absence_count']}
                                {$absentTotal = $absentTotal + $cell['absence_count']}
                            {/if}
                        </td>
                    {/foreach}
                    <td align="center" bgcolor="#ffebcd"></td>
                    {*Ô tổng hợp NGHỈ HỌC cả lớp trong toàn khoản thời gian*}
                    <td id="total_absence" align="center" bgcolor="#ffebcd" style="color: red; font-weight: bold">
                        {$absentTotal}
                    </td>
                </tr>
                {if $canEdit && $school['attendance_allow_delete_old']}
                    <tr>
                        <td colspan="2" align="center" style="vertical-align:middle" class="pinned"><strong><strong><font color="red">{__('Delete attendance')}</font></strong></td>
                        {foreach $last_rows as $cell}
                            <td align="center">{if $cell['is_checked'] == 1}<a href="#" class="btn btn-xs btn-danger js_school-delete-attendance" data-username="{$username}" data-id="{$cell['attendance_id']}">X</a>{/if}</td>
                        {/foreach}
                    </tr>
                {/if}
            {/if}
            </tbody>
        </table>
    </div>
{else}
    <div align="center"><strong style="color: red">{__("Chưa có thông tin điểm danh")}</strong></div>
{/if}
{*Jquery Cố định cột số thứ tự và họ tên*}
<script type="text/javascript">

        var $table = $('.table-pinned');
        var $fixedColumn = $table.clone().insertBefore($table).addClass('fixed-column');
        $fixedColumn.find('th').each(function (i, elem) {
            $(this).width($table.find('th:eq(' + i + ')').width());
        });
        $fixedColumn.find('td').each(function (i, elem) {
            $(this).width($table.find('td:eq(' + i + ')').width());
        });
        $fixedColumn.find('th,td').not('.pinned').hide();
        $fixedColumn.find('[id]').each(function () {
            $(this).removeAttr('id');
        });

        $fixedColumn.find('tr').each(function (i, elem) {
            $(this).height($table.find('tr:eq(' + i + ')').height());
        });

        $(window).resize(function () {
            $fixedColumn.find('tr').each(function (i, elem) {
                $(this).height($table.find('tr:eq(' + i + ')').height());
            });
            $fixedColumn.find('td').each(function (i, elem) {
                $(this).addClass('white-space_nowrap');
                $(this).width($table.find('td:eq(' + i + ')').width());
            });
        });
    //$fixedColumn.find('td').addClass('white-space_nowrap');
//    $("#right").on("click", function() {
//        var leftPos = $('#example').scrollLeft();
//        console.log(leftPos);
//        $("#example").animate({
//            scrollLeft: leftPos - 200
//        }, 800);
//    });
    $('#right').click(function(event) {
        var width_col = $('.table-pinned').find('td:eq(' + 2 + ')').width();
        var pos = $('#example').scrollLeft() + width_col + 100;
        $('#example').scrollLeft(pos);
    });
    $('#left').click(function(event) {
        var width_col = $('.table-pinned').find('td:eq(' + 2 + ')').width();
        var pos = $('#example').scrollLeft() - width_col - 100;
        $('#example').scrollLeft(pos);
    });

    jQuery(function($) {
        function fixDiv() {
            var $cache = $('#getFixed');
            var $button = $('#table_button');
            if ($(window).scrollTop() > 100) {
                $cache.css({
                    'position': 'fixed',
                    'top': '50px'
                });
                $cache.width($('#attendance_list').width() - 1);
            }
            else
                $cache.css({
                    'position': 'relative',
                    'top': 'auto'
                });

            if ($(window).scrollTop() > 100)
                $button.css({
                    'position': 'fixed',
                    'top': '90px'
                });
            else
                $button.css({
                    'position': 'relative',
                    'top': 'auto'
                });
        }
        $(window).scroll(fixDiv);
        fixDiv();
    });
</script>