<div class="panel panel-default">
    <div class="panel-heading with-icon">
        {if ($sub_view == "") && $canEdit}
            <div class = "pull-right flip">
                <a href="{$system['system_url']}/school/{$username}/roles/assignprincipal" class="btn btn-default">
                    <i class="fa fa-plus"></i> {__("Assign principal")}
                </a>
                <a href="{$system['system_url']}/school/{$username}/roles/add" class="btn btn-default">
                    <i class="fa fa-plus"></i> {__("Add role")}
                </a>
            </div>
        {/if}
        <div class="pull-right flip" style="margin-right: 5px">
            <a href="https://blog.coniu.vn/huong-dan-phan-mot-quyen-nao-do-cho-nhan-vien-trong-truong/" target="_blank" class="btn btn-info  btn_guide">
                <i class="fa fa-info"></i> {__("Guide")}
            </a>
        </div>
        <i class="fa fa-tasks fa-fw fa-lg pr10"></i>
        {__('Role')}
        {if $sub_view == "edit"}
            &rsaquo; {$data['role_name']}
        {elseif $sub_view == "add"}
            &rsaquo; {__('Add New')}
        {elseif $sub_view == "assignrole"}
            &rsaquo; {__('Assign role')}
        {elseif $sub_view == "assignprincipal"}
            &rsaquo; {__('Assign principal')}
        {/if}
    </div>
    {if $sub_view == ""}
        <div class = "panel-body with-table">
            <div class = "table-responsive">
                <div class = "form-group">
                    <strong>{__('Role list')} ({$rows|count})</strong>
                </div>
                <table class = "table table-striped table-bordered table-hover">
                    <thead>
                        <tr><th>#</th>
                            <th>{__('Role')}</th>
                            <th>{__('Employee')}</th>
                            <th>{__('Description')}</th>
                            <th>{__("Actions")}</th>
                        </tr>
                    </thead>
                    <tbody>
                        {if count($rows) > 0}
                            {foreach $rows as $k => $row}
                                <tr>
                                    <td align="center" style="vertical-align:middle"><strong>{$k + 1}</strong></td>
                                    <td style="vertical-align:middle" nowrap="true">{$row['role_name']}</td>
                                    <td style="vertical-align:middle" nowrap="true">
                                        {if count($row['users']) == 0}
                                            {__("No employee")}
                                        {else}
                                            {foreach $row['users'] as $key => $employee}
                                                {$key+1} - {$employee['user_fullname']}<br/>
                                            {/foreach}
                                        {/if}
                                    </td>
                                    <td style="vertical-align:middle">{$row['description']}</td>
                                    <td align="center" style="vertical-align:middle">
                                        {if $canEdit}
                                            <a href="{$system['system_url']}/school/{$username}/roles/assignrole/{$row['role_id']}" class="btn btn-xs btn-primary">
                                                {__("Assign role")}
                                            </a><br/>
                                            <a href="{$system['system_url']}/school/{$username}/roles/edit/{$row['role_id']}" class="btn btn-xs btn-default">
                                                {__("Edit")}
                                            </a>
                                            {if count($row['users']) == 0}
                                                <a class="btn btn-xs btn-danger js_school-delete" data-handle="role" data-username="{$username}" data-id="{$row['role_id']}">
                                                    {__("Delete")}
                                                </a>
                                            {/if}
                                        {/if}
                                    </td>
                                </tr>
                            {/foreach}
                        {else}
                            <tr>
                                <td align="center" colspan="5">
                                    {__("There is no role in your school. Do you want to create default roles?")}<br/>
                                    <a href="#" id="default" data-username="{$username}" class="btn btn-primary js_school-default-role">{__("Create default roles")}</a>
                                    <label id="processing" class="btn btn-info x-hidden">{__("Processing")}...</label>
                                </td>
                            </tr>
                        {/if}
                    </tbody>
                </table>
            </div>
        </div>
    {elseif $sub_view == "edit"}
        <div class="panel-body">
            <form class = "js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_role.php">
                <input type="hidden" name="school_username" id="school_username" value="{$username}"/>
                <input type="hidden" name="role_id" value="{$data['role_id']}"/>
                <input type="hidden" name="do" value="edit"/>

                <div class="form-group">
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Role name")} (*)</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="role_name" value="{$data['role_name']}" maxlength="255" required autofocus/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Description")}</label>
                        <div class="col-sm-9">
                            <textarea class="form-control" rows="2" name="description" maxlength="300">{$data['description']}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-3">
                            <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                            <a href="{$system['system_url']}/school/{$username}/roles" class="btn btn-default">{__("Lists")}</a>
                        </div>
                    </div>
                    <!-- success -->
                    <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                    <!-- success -->
                    <!-- error -->
                    <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                    <!-- error -->
                </div>
                <div class = "table-responsive">
                    <table class = "table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th rowspan="2">#</th>
                            <th rowspan="2">{__('Module')}</th>
                            <th>{__('No permission')}</th>
                            <th>{__('View only')}</th>
                            <th>{__('Can edit')}</th>
                        </tr>
                        <tr>
                            <th><input type="checkbox" id="checkAllNo"/>{__('All')}</th>
                            <th><input type="checkbox" id="checkAllView"/>{__('All')}</th>
                            <th><input type="checkbox" id="checkAllAll"/>{__('All')}</th>
                        </tr>
                        </thead>
                        <tbody>
                        {foreach $modules as $k => $row}
                            <tr>
                                <td align="center"><strong>{$k + 1}</strong></td>
                                <td>{__($row['module_name'])}</td>
                                <td align="center">
                                    <input type="radio" name="{$row['module']}" value="{$smarty.const.PERM_NONE}" class="no_permission" {if $row['value']==$smarty.const.PERM_NONE}checked{/if}>
                                </td>
                                <td align="center">
                                    <input type="radio" name="{$row['module']}" value="{$smarty.const.PERM_VIEW}" class="view_permission" {if $row['value']==$smarty.const.PERM_VIEW}checked{/if}>
                                </td>
                                <td align="center">
                                    <input type="radio" name="{$row['module']}" value="{$smarty.const.PERM_EDIT}" class="all_permission" {if $row['value']==$smarty.const.PERM_EDIT}checked{/if}>
                                </td>
                            </tr>
                        {/foreach}
                        </tbody>
                    </table>
                </div>
            </form>
        </div>
    {elseif $sub_view == "add"}
        <div class="panel-body ">
            <form class = "js_ajax-forms form-horizontal" data-url = "ci/bo/school/bo_role.php">
                <input type="hidden" name="school_username" id = "school_username" value="{$username}"/>
                <input type="hidden" name="do" value="add"/>

                <div class="form-group">
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Role name")} (*)</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="role_name" placeholder="{__("Role name")}" maxlength="255" required autofocus/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Description")}</label>
                        <div class="col-sm-9">
                            <textarea class="form-control" rows="2" name="description" maxlength="300"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-3">
                            <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                        </div>
                    </div>
                    <!-- success -->
                    <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                    <!-- success -->
                    <!-- error -->
                    <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                    <!-- error -->
                </div>
                <div class = "table-responsive">
                    <table class = "table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th rowspan="2">#</th>
                                <th rowspan="2">{__('Module')}</th>
                                <th>{__('No permission')}</th>
                                <th>{__('View only')}</th>
                                <th>{__('Can edit')}</th>
                            </tr>
                            <tr>
                                <th><input type="checkbox" id="checkAllNo" checked/>{__('All')}</th>
                                <th><input type="checkbox" id="checkAllView"/>{__('All')}</th>
                                <th><input type="checkbox" id="checkAllAll"/>{__('All')}</th>
                            </tr>
                        </thead>
                        <tbody>
                            {foreach $modules as $k => $row}
                                <tr>
                                    <td align="center"><strong>{$k + 1}</strong></td>
                                    <td>{__($row['module_name'])}</td>
                                    <td align="center"><input type="radio" name="{$row['module']}" value="{$smarty.const.PERM_NONE}" class="no_permission" checked></td>
                                    <td align="center"><input type="radio" name="{$row['module']}" value="{$smarty.const.PERM_VIEW}" class="view_permission"></td>
                                    <td align="center"><input type="radio" name="{$row['module']}" value="{$smarty.const.PERM_EDIT}" class="all_permission"></td>
                                </tr>
                            {/foreach}
                        </tbody>
                    </table>
                </div>
            </form>
        </div>
    {elseif $sub_view == "assignrole"}
        <div class="panel-body">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_role.php">
                <input type="hidden" name="school_username" id="school_username" value="{$username}"/>
                <input type="hidden" name="role_id" value="{$data['role_id']}"/>
                <input type="hidden" name="do" value="assignrole"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">
                        {__("Role name")}
                    </label>
                    <div class="col-sm-9">
                        <input class="form-control" name="role_name" value="{$data['role_name']}" disabled>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-3" align="right">
                        <label class="control-label text-right">{__("Employee")}</label>
                        <div>{__("Select employees whom you want to assign this role")}</div>
                    </div>
                    <div class="col-sm-9">
                        <div class = "table-responsive">
                            <table class = "table table-striped table-bordered table-hover">
                                <tbody>
                                {$newRow = 1}
                                {foreach $data['users'] as $k => $row}
                                    {if $newRow == 1}<tr>{/if}
                                    <td>
                                        <input type="checkbox" name="userIds[]" value="{$row['user_id']}" {if $row['role_id'] > 0}checked{/if}/>&nbsp;
                                        {$k + 1}-{$row['user_fullname']}
                                    </td>
                                    {if $newRow == 0}
                                        {$newRow = 1}
                                        </tr>
                                    {else}
                                        {$newRow = 0}
                                    {/if}
                                {/foreach}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                        <a href="{$system['system_url']}/school/{$username}/roles" class="btn btn-default">{__("Lists")}</a>
                    </div>
                </div>

                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
            </form>
        </div>
    {elseif $sub_view == "assignprincipal"}
        <div class="panel-body">
            <form class="js_ajax-forms-success-reload form-horizontal" data-url="ci/bo/school/bo_role.php">
                <input type="hidden" name="school_username" id="school_username" value="{$username}"/>
                <input type="hidden" name="do" value="assignprincipal"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">

                    </label>
                    <div class="col-sm-9">
                        {__("Assign principal")}<br/>
                        <strong>{__("Note: The person is assigned as manager he will have all permissions")}</strong>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-3" align="right">
                        <label class="control-label text-right">{__("Employee")}</label>
                        <div>{__("Select employees whom you want to assign principal")}</div>
                    </div>
                    <div class="col-sm-9">
                        <div class = "table-responsive">
                            <table class = "table table-striped table-bordered table-hover">
                                <tbody>
                                {$newRow = 1}
                                {foreach $data as $k => $row}
                                    {if $newRow == 1}<tr>{/if}
                                    <td>
                                        <input type="checkbox" name="userIds[]" value="{$row['user_id']}" {if $row['is_principal'] == 1}checked{/if}/>&nbsp;
                                        {$k + 1}-{$row['user_fullname']}
                                    </td>
                                    {if $newRow == 0}
                                        {$newRow = 1}
                                        </tr>
                                    {else}
                                        {$newRow = 0}
                                    {/if}
                                {/foreach}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                        <a href="{$system['system_url']}/school/{$username}/roles" class="btn btn-default">{__("Lists")}</a>
                    </div>
                </div>

                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
            </form>
        </div>
    {/if}
</div>