<div class="form-group pl5" id="service_btnSave">
    <div class="col-sm-9">
        <button type="submit" class="btn btn-primary padrl30" disabled>{__("Save")}</button>
    </div>
</div>

<div><strong>{__("Children list")}&nbsp;({__("Children")}: {$results['children']|count}&nbsp;|&nbsp;{__("Usage")}: {$results['usage_count']})</strong></div>
<table class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th>#</th>
            <th><input type="checkbox" id="check_checkall" style="vertical-align: middle; margin: 0"> {__("Select")}</th>
            <th>{__("Full name")}</th>
            <th>{__("Birthdate")}</th>
            <th>{__("Registration time")}</th>
            <th>{__("Registrar")}</th>
        </tr>
    </thead>
    <tbody>
        {$tempId = -1}
        {$idx = 1}
        {foreach $results['children'] as $row}
            {if ((!isset($class_id) || ($class_id <= 0)) && ($tempId != $row['class_id']))}
                {*Trường hợp tìm cả trường*}
                <tr>
                    <td colspan="6"><strong>{$row['group_title']}</strong></td>
                </tr>
            {/if}
            <tr {if ($row['child_status'] == 0) || (in_array($row['child_id'], $absent_child_ids))}class="row-disable"{/if}>
                <td align="center">{$idx}</td>
                <td align="center">
                    <input type="checkbox" class="child check_element" name="childIds[]" value="{$row['child_id']}"
                           {if $row['recorded_user_id'] > 0}checked{/if} {if (in_array($row['child_id'], $absent_child_ids))}disabled{/if}>
                    <input type="hidden" name="allChildIds[]" value="{$row['child_id']}"/>
                    {if $row['recorded_user_id'] > 0}
                        <input type="hidden" name="oldChildIds[]" value="{$row['child_id']}"/>
                    {/if}
                </td>
                <td>{$row['child_name']}</td>
                <td>{$row['birthday']}</td>
                <td>
                    {if $row['recorded_user_id'] > 0}
                        {$row['recorded_at']}
                    {/if}
                </td>
                <td>
                    {if $row['recorded_user_id'] > 0}
                        {$row['user_fullname']}
                        {if $row['recorded_user_id'] != $user->_data['user_id']}
                            &nbsp;<a href="#" class="far fa-comments fa-lg js_chat-start" data-name="{$row['user_fullname']}" data-uid="{$row['recorded_user_id']}"></a>
                        {/if}
                    {/if}
                </td>
            </tr>
            {if (!isset($class_id)) || ($class_id <= 0)}
                {$tempId = $row['class_id']}
            {/if}
            {$idx = $idx + 1}
        {/foreach}
        {if $idx == 1}
            <tr class="odd">
                <td valign="top" align="center" colspan="6" class="dataTables_empty">
                    {__("No data available in table")}
                </td>
            </tr>
        {/if}
    </tbody>
</table>