<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <div class="pull-right flip">
            {if $sub_view == ""}
                <a href="{$system['system_url']}/school/{$username}/children/developmentindex" class="btn btn-default">
                    <i class="fa fa-heart"></i> {__("Development index")}
                </a>
                {if $canEdit}
                    <a href="{$system['system_url']}/school/{$username}/healths/add" class="btn btn-default">
                        <i class="fa fa-plus"></i> {__("Add New")}
                    </a>
                {/if}
            {elseif $sub_view == "add"}
                <a href="{$system['system_url']}/school/{$username}/healths" class="btn btn-default">
                    <i class="fa fa-list"></i> {__("Lists")}
                </a>
            {/if}
        </div>
        <div class="pull-right flip" style="margin-right: 5px">
            <a href="https://blog.coniu.vn/huong-dan-them-thong-tin-suc-khoe/" class="btn btn-info btn_guide" target="_blank">
            <i class="fa fa-info-circle"></i> {__("Guide")}
            </a>
        </div>
        <i class="fa fa-image fa-fw fa-lg pr10"></i>
        {__("Health information")}
        {if $sub_view == ""}
            &rsaquo; {__("Lists")}
        {elseif $sub_view == "add"}
            &rsaquo; {__("Add New")}
        {/if}
    </div>

    {if $sub_view == ""}
        <div class = "panel-body">
            {*Ô search*}
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_child.php">
                <input type="hidden" name="school_username" value="{$username}"/>
                <input type="hidden" name="do" value="search_health_class"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-right">{__("Select student")} (*)</label>
                    <div class='col-sm-3'>
                        <select name="class_id" id="attendance_class_id" data-username="{$username}" class="form-control" autofocus required>
                            <option value="">{__("Select Class")}...</option>
                            {$class_level = -1}
                            {foreach $classes as $class}
                                {if ($class_level != $class['class_level_id'])}
                                    <option value="" disabled>-----{$class['class_level_name']}-----</option>
                                {/if}
                                <option value="{$class['group_id']}" {if $classId != '' && $classId == $class['group_id']}selected{/if}>{$class['group_title']}</option>
                                {$class_level = $class['class_level_id']}
                            {/foreach}

                        </select>
                    </div>
                    <div class="col-sm-4">
                        <select name="child_id" id="attendance_child_id" class="form-control" required>
                            <option value="">{__("Select student")}...</option>
                            {if $childId != ''}
                                {if isset($children) && !empty($children)}
                                    {foreach $children as $child}
                                        <option value="{$child['child_id']}" {if ($child['child_id'] == $childId)}selected{/if}>{$child['child_name']}</option>
                                    {/foreach}
                                    {*{else}*}
                                    {*<option value="{$data['child']['child_id']}" selected>{$data['child']['child_name']}</option>*}
                                {/if}
                            {/if}
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label text-left">{__("Time")}</label>
                    <div class='col-sm-3'>
                        <div class='input-group date' id='begin_chart_picker'>
                            <input type='text' name="begin" class="form-control" id="begin" {if isset($begin) && $begin != ''} value="{$begin}"{/if}/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                    <div class='col-sm-1'><i class="fas fa-long-arrow-alt-right fa-lg fa-fw pr10 pt10"></i></div>
                    <div class='col-md-3'>
                        <div class='input-group date' id='end_chart_picker'>
                            <input type='text' name="end" class="form-control" id="end" {if isset($end) && $end != ''} value="{$end}"{/if}/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <a href="#" id="search" class="btn btn-default js_school-health-search" data-username="{$username}" data-isnew="1">{__("Search")}</a>
                        <label id="loading" class="btn btn-info x-hidden">{__("Loading")}...</label>
                    </div>
                </div>
                <div id="chart_list" name="chart_list">
                    {include file="ci/school/ajax.school.chartsearch.tpl"}
                </div>
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
            </form>
        </div>
    {elseif $sub_view == "add"}
        <div class="panel-body with-table">
            <div class="row">
                <div class='col-sm-6'>
                    <div class="form-group">
                        <input type="text" name="keyword" id="keyword" class="form-control" value="{$result['keyword']}" placeholder="{__("Enter keyword to search")}">
                    </div>
                </div>
                <div class='col-sm-3'>
                    <div class="form-group">
                        <select name="class_id" id="class_id" class="form-control">
                            <option value="">{__("Select Class")}...</option>
                            {if $school['children_use_no_class']}
                                <option value="0" {if $result['class_id'] == '0'}selected{/if}>{__("No class")}</option>
                            {/if}
                            {$class_level = -1}
                            {foreach $classes as $class}
                                {if ($class_level != $class['class_level_id'])}
                                    <option value="" disabled>-----{$class['class_level_name']}-----</option>
                                {/if}
                                <option value="{$class['group_id']}" {if $result['class_id'] == $class['group_id']}selected{/if}>{$class['group_title']}</option>
                                {$class_level = $class['class_level_id']}
                            {/foreach}
                        </select>
                    </div>
                </div>
                <div class='col-sm-3'>
                    <div class = "row">
                        <div class = "col-sm-6">
                            <div class="form-group">
                                <a href="#" id="search" class="btn btn-default js_child-health-search" data-username="{$username}" data-isnew="1" data-id="{$school['page_id']}">{__("Search")}</a>
                                <label id="loading" class="btn btn-info x-hidden">{__("Loading")}...</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br/>
            <div id="child_health_list" name="child_health_list">
            </div>
        </div>
    {/if}
</div>