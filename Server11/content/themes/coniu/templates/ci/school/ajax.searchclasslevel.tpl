<div class="js_scroller">
    <ul>
        {foreach $results as $classlevel}
            <li class="feeds-item" data-id="{$classlevel['class_level_name']}">
                <div class="data-container {if $_small}small{/if}">
{*                    <a href="{$system['system_url']}/{$_user['user_name']}">*}
{*                        <img class="data-avatar" src="{$_user['user_picture']}" alt="{$_user['user_fullname']}">*}
{*                    </a>*}
                        <div class="pull-right flip">
                            <div class="btn btn-default js_classlevel-select" data-uid="{$classlevel['class_level_id']}" data-ufullname="{$classlevel['class_level_name']}">{__("Add")}</div>
                        </div>
                        <div>
{*                            <span class="name js_user-popover" data-uid="{$_user['user_id']}">*}
                            <span class="name js_classlevel-popover" data-uid="{$classlevel['class_level_id']}">
                              {$classlevel['class_level_name']}
                            </span>
                        </div>
                </div>
            </li>
        {/foreach}
    </ul>
</div>