<div class="panel-body with-table">
    <table class="table table-bordered">
        <tr>
            <td class="col-sm-3 text-right"><strong>{__("Class")}</strong></td>
            <td>{$data['group_title']}</td>
        </tr>
        <tr>
            <td class="col-sm-3 text-right"><strong>{__("Month")}</strong></td>
            <td>{$data['month']}</td>
        </tr>
        <tr>
            <td class="col-sm-3 text-right"><strong>{__("Paid")}</strong></td>
            <td><span class="class_paid_amount">{moneyFormat($data['paid_amount'])}</span>/{moneyFormat($data['total_amount'])}&nbsp;{$smarty.const.MONEY_UNIT}
                &nbsp;(<span class="class_paid_count">{$data['paid_count']}</span> {__("children")})
            </td>
        </tr>
        <tr>
            <td class="col-sm-3 text-right"><strong>{__("Notify")}</strong></td>
            <td>{if $data['is_notified']=='1'}{__("Notified")}{else}{__("Not notified yet")}{/if}</td>
        </tr>
        <tr>
            <td class="col-sm-3 text-right"><strong>{__("Description")}</strong></td>
            <td>{$data['description']}</td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                {if $canEdit}
                    {if !$data['is_notified']}
                        <a class="btn btn-default js_school-tuition" data-handle="notify" data-username="{$username}"
                           data-id="{$data['tuition_id']}">
                            {__("Notify")}
                        </a>
                    {/if}
                    <a class="btn btn-default"
                       href="{$system['system_url']}/school/{$username}/tuitions/newchild/{$data['tuition_id']}">{__("Add child")}</a>
                {/if}
                <a class="btn btn-default"
                   href="{$system['system_url']}/school/{$username}/tuitions/history/{$data['tuition_id']}">{__("Used last month")}</a>
                {if $canEdit}
                    {if $data['paid_amount'] != $data['total_amount']}
                        <a class="btn btn-default"
                           href="{$system['system_url']}/school/{$username}/tuitions/edit/{$data['tuition_id']}">{__("Edit")}</a>
                    {/if}
                    {if ($data['paid_amount'] == 0)}
                        <a class="btn btn-danger js_school-tuition" data-handle="remove" data-username="{$username}"
                           data-id="{$data['tuition_id']}">{__("Delete")}</a>
                    {/if}
                {/if}
            </td>
        </tr>
    </table>
    {* Hiển thị bảng ký hiệu giải thích trạng thái điểm danh *}
    {if $school['tuition_view_attandance'] && ($school['attendance_use_leave_early'] || $school['attendance_use_come_late'] || $school['attendance_absence_no_reason'])}
        <table class="table table-striped table-bordered table-hover">
            <tbody>
            <tr>
                <td align="right"><strong>{__("Symbol")}:</strong></td>
                {if $school['attendance_use_come_late']}
                    <td bgcolor="#6495ed" align="center"><i class="fa fa-check" aria-hidden="true"></td>
                    <td align="left">{__("Come late")}</td>
                {/if}
                {if $school['attendance_use_leave_early']}
                    <td bgcolor="#008b8b" align="center"><i class="fa fa-check" aria-hidden="true"></td>
                    <td align="left">{__("Leave early")}</td>
                {/if}
                <td align="center"><i class="fa fa-check" aria-hidden="true"></td>
                <td align="left">{__("Present")}</td>

                {if $school['attendance_absence_no_reason']}
                    <td bgcolor="#ff1493" align="center"><i class="fa fa-times" aria-hidden="true"></td>
                    <td align="left">{__("Without permission")}</td>
                {/if}

                <td bgcolor="#deb887" align="center"><i class="fa fa-times" aria-hidden="true"></td>
                <td align="left">{__("With permission")}</td>
            </tr>
            </tbody>
        </table>
    {/if}
    <div class="table-responsive" id="children_list">
        <table class="table table-striped table-bordered table-hover">
            <thead>
            {if count($data['tuition_child']) > 0}
                <tr>
                    <th>
                        <div style="float:right">
                            <a class="btn btn-success js_school-tuition-export" data-handle="export_all"
                               data-username="{$username}"
                               data-id="{$data['tuition_id']}">{__('Export all to Excel')}</a>
                            <label class="btn btn-info processing_label x-hidden">{__("Processing")}...</label>
                        </div>
                    </th>
                </tr>
            {/if}
            <tr>
                <th>
                    <div class="pull-left flip">{__("Children list")|upper}&nbsp;({$data['tuition_child']|count})</div>
                    <div class="pull-right flip">
                        {__("Total of class")}&nbsp;({$smarty.const.MONEY_UNIT})
                        <input type="text" class="text-right" value="{moneyFormat($data['total_amount'])}" readonly/>
                    </div>
                </th>
            </tr>
            </thead>
            <tbody>
            {$detailIdx = 1}
            {foreach $data['tuition_child'] as $k => $child}
                <tr {if $child['status'] == 0}class="bg_not_paid" {else}class="bg_paid"{/if}>
                    <td>
                        <div style=" display: inline;">
                            <strong>{$detailIdx} - {$child['child_name']}</strong>
                            {if isset($child['attendance_count']) && ($child['attendance_count'] > 0)}&nbsp;|&nbsp;
                                {__("Number of charged days")} {__("month")} {$child['pre_month']}:&nbsp;{$child['attendance_count']}
                            {/if}
                        </div>
                        <div class="pull-right flip btn_no_margin_bot">
                            {*Các button tương ứng với trạng thái thanh toán của học phí*}

                            {if $canEdit}
                                <div class="tuition_paid notpaid_box notpaid_box_{$child['tuition_child_id']} {if $child['status']!=$smarty.const.TUITION_CHILD_NOT_PAID}hidden{/if}">
                                    <input style="width: 85px;" type="text" class="text-right money_tui child_total_amount_{$child['child_id']}"
                                           id="pay_amount_{$child['child_id']}" name="pay_amount"
                                           value="{$child['total_amount']}"/>
                                    <button style="margin-right: 5px" class="btn btn-xs btn-primary js_school-tuition"
                                            data-handle="confirm" data-username="{$username}"
                                            data-amount="{$child['total_amount']}"
                                            data-tuition-id="{$data['tuition_id']}" data-child-id="{$child['child_id']}"
                                            data-id="{$child['tuition_child_id']}">{__("Pay")}</button>
                                </div>
                            {/if}

                            {if $canEdit}
                                <div class="tuition_paid parentpaid_box parentpaid_box_{$child['tuition_child_id']} {if $child['status']!=$smarty.const.TUITION_CHILD_PARENT_PAID}hidden{/if}">
                                    <button class="btn btn-xs btn-default js_school-tuition" data-handle="confirm"
                                            data-username="{$username}"
                                            data-amount="{$child['total_amount']}"
                                            data-tuition-id="{$data['tuition_id']}"
                                            data-child-id="{$child['child_id']}"
                                            data-id="{$child['tuition_child_id']}">{__("Confirm")}</button>
                                    <button style="margin-right: 5px" class="btn btn-xs btn-danger js_school-tuition"
                                            data-handle="unconfirm" data-username="{$username}"
                                            data-child-id="{$child['child_id']}" data-tuition-id="{$data['tuition_id']}" data-amount="{$child['total_amount']}" data-parent="1"
                                            data-id="{$child['tuition_child_id']}">{__("Unconfirm")}</button>
                                    <input style="width: 100px;" type="text" class="text-right money_tui"
                                           id="pay_amount_{$child['child_id']}" name="pay_amount"
                                           value="{$child['total_amount']}"/>
                                </div>
                            {/if}

                            <div class="tuition_paid paid_box paid_box_{$child['tuition_child_id']} {if $child['status']!=$smarty.const.TUITION_CHILD_CONFIRMED}hidden{/if}">
                                <strong>{__("Paid")}</strong> {if $child['paid_amount'] != $child['total_amount']}<span
                                    class="paid_amount_{$child['tuition_child_id']}">
                                    ({moneyFormat($child['paid_amount'])}/{moneyFormat($child['total_amount'])}
                                    )</span>{/if}
                                {if $canEdit}
                                    <button style="margin-right: 5px" class="btn btn-xs btn-danger js_school-tuition"
                                            data-handle="unconfirm" data-username="{$username}"
                                            data-child-id="{$child['child_id']}" data-tuition-id="{$data['tuition_id']}" data-amount="{$child['total_amount']}" data-parent="0"
                                            data-id="{$child['tuition_child_id']}">{__("Unconfirm")}</button>
                                    <input type="hidden" id="pay_amount_{$child['child_id']}" name="pay_amount"
                                           value="{$child['paid_amount']}" class="amount_{$child['child_id']}"/>
                                {/if}
                            </div>

                            {if $canEdit}
                                <button style="margin-right: 5px"
                                        class="btn btn-xs btn-success js_school-tuition-export"
                                        data-handle="export" data-username="{$username}"
                                        data-id="{$child['tuition_id']}" data-child-id="{$child['child_id']}"
                                        data-amount="{moneyFormat($child['total_amount'])}"> {__('Export to Excel')} </button>
                                <a class="btn btn-xs btn-default js_show_hide_tuition_tbody"
                                   child_id="{$child['child_id']}">{__("Show/hide")}</a>
                                <button class="btn btn-xs btn-danger js_school-delete-tuition-child"
                                        data-username="{$username}" data-total="{$child['total_amount']}"
                                        data-id="{$child['tuition_child_id']}">{__('Delete')}</button>
                            {/if}
                        </div>
                        {*Hiển thị thông tin điểm danh chi tiết của trẻ tháng trước*}
                        {if $school['tuition_view_attandance'] && (count($child['attendances']) > 0)}
                            <table class="table table-striped table-bordered table-hover"
                                   id="tbl_attendance_{$child['child_id']}" style="display: none">
                                <tr>
                                    <td style="width: 90px"><a
                                                href="{$system['system_url']}/school/{$username}/attendance/child/{$child['child_id']}"
                                                target="_blank"><strong>{__("Attendance")}</strong></a></td>
                                    {foreach $child['attendances'] as $attendance}
                                        <td style="padding: 5px" align="center" class="align_middle"
                                                {if $attendance['status'] == $smarty.const.ATTENDANCE_COME_LATE}
                                                    bgcolor="#6495ed"
                                                {elseif $attendance['status'] == $smarty.const.ATTENDANCE_EARLY_LEAVE}
                                                    bgcolor="#008b8b"
                                                {elseif $attendance['status'] == $smarty.const.ATTENDANCE_ABSENCE_NO_REASON}
                                                    bgcolor="#ff1493"
                                                {elseif $attendance['status'] == $smarty.const.ATTENDANCE_ABSENCE}
                                                    bgcolor="#deb887"
                                                {/if}
                                        >{$attendance['display_date']}</td>
                                    {/foreach}
                                </tr>
                            </table>
                        {/if}
                        <br/>
                        {if count($child['tuition_detail']) > 0}
                            <table class="table table-striped table-bordered table-hover bg_white"
                                   id="tbl_fee_{$child['child_id']}" style="display: none">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>{__("Fee name")}</th>
                                    <th style="width: 52px">{__("Qty")}</th>
                                    <th style="width: 117px">{__("Unit price")}</th>
                                    <th style="width: 107px">{__("Deduction")}</th>
                                    <th style="width: 117px">{__("Money amount")}</th>
                                </tr>
                                </thead>
                                <tbody>
                                {*Hiển thị danh sách phí/dịch vụ của trẻ*}
                                {$idx = 1}
                                {foreach $child['tuition_detail'] as $detail}
                                    <tr>
                                        <td align="center">{$idx}</td>
                                        {if $detail['type'] == $smarty.const.TUITION_DETAIL_FEE}
                                            <td>{$detail['fee_name']}</td>
                                            <td class="text-center">{$detail['quantity']}</td>
                                            <td class="text-right">{moneyFormat($detail['unit_price'])}</td>
                                            {if $detail['fee_type'] != $smarty.const.FEE_TYPE_MONTHLY}
                                                <td class="text-center">{if $detail['quantity_deduction']!=0}{$detail['quantity_deduction']} ({__('Day')}){/if}</td>
                                            {else}
                                                <td class="text-right">{if $detail['unit_price_deduction']!=0 && $detail['quantity_deduction'] != 0}{moneyFormat($detail['unit_price_deduction'])}({$smarty.const.MONEY_UNIT}){/if}</td>
                                            {/if}
                                        {elseif $detail['type'] == $smarty.const.TUITION_DETAIL_SERVICE}
                                            <td>{$detail['service_name']}</td>
                                            <td class="text-center">{$detail['quantity']}</td>
                                            <td class="text-right">{moneyFormat($detail['unit_price'])}</td>
                                            {if $detail['service_type'] == $smarty.const.SERVICE_TYPE_DAILY}
                                                <td class="text-center">{if $detail['quantity_deduction']!=0}{$detail['quantity_deduction']} ({__('Day')}){/if}</td>
                                            {elseif $detail['service_type'] == $smarty.const.SERVICE_TYPE_MONTHLY}
                                                <td class="text-right">{if $detail['unit_price_deduction']!=0 && $detail['quantity_deduction']!=0}{moneyFormat($detail['unit_price_deduction'])}({$smarty.const.MONEY_UNIT}){/if}</td>
                                            {else}
                                                <td class="text-center">{if $detail['quantity_deduction']!=0}{$detail['quantity_deduction']} ({__('Times')}){/if}</td>
                                            {/if}
                                        {else}
                                            <td colspan="4">{__('Late PickUp')}</td>
                                        {/if}
                                        <td class="text-right">
                                            {if $detail['type'] != $smarty.const.LATE_PICKUP_FEE}
                                                {moneyFormat(($detail['quantity']  * $detail['unit_price'] - $detail['quantity_deduction'] * $detail['unit_price_deduction']))}
                                            {else}
                                                {moneyFormat($detail['unit_price'])}
                                            {/if}
                                        </td>
                                    </tr>
                                    {$idx = $idx + 1}
                                {/foreach}

                                {* Hiển thị nợ tháng trước *}
                                {if $child['debt_amount'] != 0}
                                    <tr>
                                        <td colspan="5" class="text-right">{__("Debt amount of previous month")}
                                            &nbsp;({$smarty.const.MONEY_UNIT})&nbsp;{$child['pre_month']}</td>
                                        <td class="text-right">{moneyFormat($child['debt_amount'])}</td>
                                    </tr>
                                {/if}
                                <tr>
                                    <td colspan="3">

                                    </td>
                                    <td colspan="2" class="text-right"><strong>{__("Total")}
                                            &nbsp;({$smarty.const.MONEY_UNIT})</strong></td>
                                    <td class="text-right"><strong>{moneyFormat($child['total_amount'])}</strong></td>
                                </tr>
                                {* Hiển thị nợ tháng này *}

                                    <tr class="tuition_paid_amount_{$child['child_id']} {if ($child['status']!=$smarty.const.TUITION_CHILD_CONFIRMED)}x-hidden{/if}">
                                        <td colspan="5" class="text-right"><strong>{__("Paid")}
                                                &nbsp;({$smarty.const.MONEY_UNIT})</strong></td>
                                        <td class="text-right"><strong><span class="paid_amount_td_{$child['child_id']}">{moneyFormat($child['paid_amount'])}</span></strong>
                                        </td>
                                    </tr>
                                    <tr class="tuition_debt_amount_{$child['child_id']} {if ($child['status']!=$smarty.const.TUITION_CHILD_CONFIRMED)}x-hidden{/if}">
                                        <td colspan="5" class="text-right"><strong>{__("Debt amount of this month")}
                                                &nbsp;({$smarty.const.MONEY_UNIT})</strong></td>
                                        <td class="text-right">
                                            <strong><span class="debt_amount_td_{$child['child_id']}">{moneyFormat($child['total_amount'] - $child['paid_amount'])}</span></strong>
                                        </td>
                                    </tr>

                                {if (isset($child['description']) && ({$child['description']} != ''))}
                                    <tr>
                                        <td colspan="2" class="text-right"><strong>{__("Description")}</strong></td>
                                        <td colspan="4">{$child['description']}</td>
                                    </tr>
                                {/if}
                                </tbody>
                            </table>
                        {/if}
                    </td>
                </tr>
                <tr height="10"></tr>
                {$detailIdx = $detailIdx + 1}
            {/foreach}
            </tbody>
        </table>
        {if count($data['tuition_child']) > 0}
            <a class="btn btn-success js_school-tuition-export" data-handle="export_all" data-username="{$username}"
               data-id="{$child['tuition_id']}">{__('Export all to Excel')}</a>
            <label class="btn btn-info processing_label x-hidden">{__("Processing")}...</label>
        {/if}
        {*<div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>*}
        {*<div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>*}
    </div>
</div>
