<div class="panel panel-default">
    <div class="panel-heading with-icon">
        {if $canEdit}
            <div class="pull-right flip">
                <a href="https://blog.coniu.vn/huong-dan-tao-nhat-ky-cho-tre/" target="_blank" class="btn btn-info btn_guide">
                    <i class="fa fa-info"></i> {__("Guide")}
                </a>
                {if $sub_view == ""}
                    <a href="{$system['system_url']}/school/{$username}/diarys/add" class="btn btn-default">
                        <i class="fa fa-plus"></i> {__("Add New")}
                    </a>
                {elseif $sub_view == "add"}
                    <a href="{$system['system_url']}/school/{$username}/diarys" class="btn btn-default">
                        <i class="fa fa-list"></i> {__("Diary lists")}
                    </a>
                {/if}
            </div>
        {/if}
        <div class="pull-right flip" style="margin-right: 5px">
            {*<a href="https://blog.coniu.vn/huong-dan-quan-ly-diem-danh-tren-website-coniu/" class="btn btn-info btn_guide" target="_blank">*}
                {*<i class="fa fa-info-circle"></i> {__("Guide")}*}
            {*</a>*}
        </div>
        <i class="fa fa-image fa-fw fa-lg pr10"></i>
        {__("Diary corner")}
        {if $sub_view == ""}
            &rsaquo; {__("Lists")}
        {elseif $sub_view == "add"}
            &rsaquo; {__("Add New")}
        {/if}
    </div>

    {if $sub_view == ""}
        <div class="panel-body">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_child.php">
                <input type="hidden" name="school_username" value="{$username}"/>
                <input type="hidden" name="do" value="search_diary_class"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-right">{__("Select student")} (*)</label>
                    <div class='col-sm-3'>
                        <select name="class_id" id="attendance_class_id" data-username="{$username}" class="form-control" autofocus required>
                            <option value="">{__("Select Class")}...</option>
                            {$class_level = -1}
                            {foreach $classes as $class}
                                {if ($class_level != $class['class_level_id'])}
                                    <option value="" disabled>-----{$class['class_level_name']}-----</option>
                                {/if}
                                <option value="{$class['group_id']}" {if $classId != '' && $classId == $class['group_id']}selected{/if}>{$class['group_title']}</option>
                                {$class_level = $class['class_level_id']}
                            {/foreach}

                        </select>
                    </div>
                    <div class="col-sm-4">
                        <select name="child_id" id="attendance_child_id" class="form-control" required>
                            <option value="">{__("Select student")}...</option>
                            {if $childId != ''}
                                {if isset($children) && !empty($children)}
                                    {foreach $children as $child}
                                        <option value="{$child['child_id']}" {if ($child['child_id'] == $childId)}selected{/if}>{$child['child_name']}</option>
                                    {/foreach}
                                {*{else}*}
                                    {*<option value="{$data['child']['child_id']}" selected>{$data['child']['child_name']}</option>*}
                                {/if}
                            {/if}
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Year")}</label>
                    <div class="col-sm-3">
                        <select name="year" id="year" class="form-control">
                            <option value = "0">{__("Select year ...")}</option>
                            {for $i = $year_begin; $i < $year_end; $i++}
                                <option value="{$i}" {if $year != '' && $year == $i}selected{/if}>{$i}</option>
                            {/for}
                        </select>
                    </div>
                    <div class="col-md-3">
                        <a href="#" id="search" class="btn btn-default js_school-diary-search" data-username="{$username}" data-isnew="1">{__("Search")}</a>
                        <label id="loading" class="btn btn-info x-hidden">{__("Loading")}...</label>
                    </div>
                </div>
                <div id = "journal_list">
                    {include file="ci/ajax.school.journal.list.tpl"}
                </div>
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
            </form>
        </div>
    {elseif $sub_view == "add"}
        <div class="panel-body">
            <form class="form-horizontal" enctype="multipart/form-data" method="post" id="add_child_journal_school">
                <input type="hidden" name="school_username" value="{$username}"/>
                <input type="hidden" name="do" value="add_photo"/>
                <input type="hidden" name="reload" value="1">
                <input type="hidden" name="module" value="1">
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Select student")} (*)</label>
                    <div class="col-sm-4">
                        <select name="class_id" id="medicine_class_id" data-username="{$username}" class="form-control" autofocus required>
                            <option value="">{__("Select class")}</option>
                            {$class_level = -1}
                            {foreach $classes as $class}
                                {if ($class_level != $class['class_level_id'])}
                                    <option value="" disabled style="color: blue">-----{$class['class_level_name']}-----</option>
                                {/if}
                                <option value="{$class['group_id']}">{$class['group_title']}</option>
                                {$class_level = $class['class_level_id']}
                            {/foreach}
                        </select>
                    </div>
                    <div class="col-sm-5">
                        <select name="child_id" id="medicine_child_id" class="form-control" required></select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Caption")}</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="caption" placeholder="{__("Caption")}" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Picture")}</label>
                    <div class="col-sm-6">
                        <input name="file[]" type="file" multiple="true" required>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                    </div>
                </div>
            </form>
        </div>
    {/if}
</div>