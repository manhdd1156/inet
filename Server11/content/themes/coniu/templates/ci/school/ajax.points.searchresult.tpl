{if $rows|count > 0}
    {*<div style="z-index: 2">*}
    {*<div><strong>{__("The whole class attendance")}</strong></div>*}
    {*<div id="table_button">*}
    {*<button class="left">&larr;</button>*}
    {*<button class="right">&rarr;</button>*}
    {*</div>*}
    {*</div>*}
    <div class="table-responsive" id="example">
        {if $grade == 2}
            {if $search_with == 'search_with_subject' }
                {if $semester == 0}
                    <table class="table table-striped table-bordered table-pinned" style="z-index: 1">
                        <thead>
                        <tr bgcolor="#fff">
                            <th rowspan="2" class="pinned" style="padding: 8px 6px" id="width_no">#</th>
                            <th rowspan="2" nowrap="true" class="pinned"
                                style="padding: 8px 6px">{__("Student name")}</th>
                            <th rowspan="2" nowrap="true"
                                style="padding: 8px 6px">{__("Student code")}</th>
                            <th colspan="4">{__("Semester 1")}</th>
                            <th colspan="4">{__("Semester 2")}</th>
                            <th colspan="2">{__("Average semesterly")}</th>
                            <th colspan="2">{__("End semester")}</th>
                            <th rowspan="2">{__("End year")}</th>
                            <th rowspan="2">{__("Re exam")}</th>
                            <th rowspan="2">{__("Without permission")}</th>
                            <th rowspan="2">{__("With permission")}</th>
                            <th rowspan="2">{__("Status")}</th>
                        </tr>
                        <tr>
                            <th>M1</th>
                            <th>M2</th>
                            <th>M3</th>
                            <th>{__("Last")}</th>
                            <th>M1</th>
                            <th>M2</th>
                            <th>M3</th>
                            <th>{__("Last")}</th>
                            <th>S1</th>
                            <th>S2</th>
                            <th>S1</th>
                            <th>S2</th>
                        </tr>
                        </thead>
                        <tbody>
                        {$rowIdx = 1}
                        {foreach $rows as $k => $row}
                            <tr>
                                <td align="center" class="pinned">{$rowIdx}</td>
                                <td nowrap="true" class="pinned text-bold color-blue">
                                    <strong>{$row['child_lastname']} {$row['child_firstname']}</strong></td>
                                <td nowrap="true" class="text-bold color-blue"><a
                                            href="{$system['system_url']}/school/{$username}/children/detail/{$row['child_id']}"><strong>{$row['child_code']}</strong></a>
                                </td>
                                {$rowIdx = $rowIdx + 1}
                                {foreach $subject_key as $key}
                                    <td>{$row[$key]}</td>
                                {/foreach}
                            </tr>
                            {*                            *}{*Sau 15 trẻ thì thêm header cho dễ nhìn*}
                            {*                            {if $rowIdx % 10 == 0}*}
                            {*                                <tr bgcolor="#fff">*}
                            {*                                    <th class="pinned" style="padding: 8px 6px" id="width_no">#</th>*}
                            {*                                    <th nowrap="true" class="pinned" style="padding: 8px 6px">{__("Child code")}</th>*}
                            {*                                    <th nowrap="true" class="pinned" style="padding: 8px 6px">{__("Student name")}</th>*}
                            {*                                    <th nowrap="true" class="pinned" style="padding: 8px 6px">{__("Student name")}</th>*}
                            {*                                    <th>{__("TBHK1")}</th>*}
                            {*                                    <th>{__("TBHK2")}</th>*}
                            {*                                    <th>{__("TB Cả năm")}</th>*}
                            {*                                    <th>{__("Điểm thi lại")}</th>*}
                            {*                                </tr>*}
                            {*                            {/if}*}
                        {/foreach}
                        </tbody>
                    </table>
                {else}
                    <table class="table table-striped table-bordered table-pinned" style="z-index: 1">
                        <thead>
                        <tr bgcolor="#fff">
                            <th rowspan="2" class="pinned" style="padding: 8px 6px" id="width_no">#</th>

                            <th rowspan="2" nowrap="true" class="pinned"
                                style="padding: 8px 6px">{__("Student name")}</th>
                            <th rowspan="2" nowrap="true" class="pinned"
                                style="padding: 8px 6px">{__("Student code")}</th>
                            {*                        /* UPDATE START - ManhDD 06/04/2021 */*}
                            <th colspan="4">{__("Semester")}</th>
                            {*                        <th colspan="3">{__("Mouth")}</th>*}
                            {*                        <th colspan="3">{__("15 Minutes")}</th>*}
                            {*                        <th colspan="8">{__("1 Tiết")}</th>*}
                            {*                            <th rowspan="2">{__("Học kỳ")}</th>*}
                            {*                        <th rowspan="2">{__("Điểm TBHK")}</th>*}
                            {*                        /* UPDATE START - ManhDD 06/04/2021 */*}
                        </tr>
                        <tr>
                            <th>M1</th>
                            <th>M2</th>
                            <th>M3</th>
                            <th>{__("Last")}</th>
                        </tr>
                        </thead>
                        <tbody>
                        {$rowIdx = 1}
                        {foreach $rows as $k => $row}
                            <tr>
                                <td align="center" class="pinned">{$rowIdx}</td>
                                <td nowrap="true" class="pinned text-bold color-blue">
                                    <strong>{$row['child_lastname']} {$row['child_firstname']}</strong></td>
                                <td nowrap="true" class="pinned text-bold color-blue"><a
                                            href="{$system['system_url']}/school/{$username}/children/detail/{$row['child_id']}"><strong>{$row['child_code']}</strong></a>
                                </td>
                                {$rowIdx = $rowIdx + 1}
                                {foreach $subject_key as $key}
                                    <td>{$row[$key]}</td>
                                {/foreach}
                            </tr>
                            {*Sau 15 trẻ thì thêm header cho dễ nhìn*}
                            {if $rowIdx % 10 == 0}
                                <tr bgcolor="#fff">
                                    <th rowspan="2" class="pinned" style="padding: 8px 6px" id="width_no">#</th>
                                    <th rowspan="2" nowrap="true" class="pinned"
                                        style="padding: 8px 6px">{__("Student code")}</th>
                                    <th rowspan="2" nowrap="true" class="pinned"
                                        style="padding: 8px 6px">{__("Student name")}</th>
                                    <th rowspan="2" nowrap="true" class="pinned"
                                        style="padding: 8px 6px">{__("Student name")}</th>
                                    <th colspan="4">{__("Semester")}</th>
                                    {*                         DEL START - ManhDD 06/04/2021*}
                                    {*                                <th colspan="3">{__("15 Minute")}</th>*}
                                    {*                                <th colspan="8">{__("1 Tiết")}</th>*}
                                    {*                         DEL END - ManhDD 06/04/2021*}
                                    {*                                    <th rowspan="2">{__("Semester")}</th>*}
                                    {*                                <th rowspan="2">{__("Điểm TBHK")}</th>*}
                                </tr>
                                <tr>
                                    <th>M1</th>
                                    <th>M2</th>
                                    <th>M3</th>
                                    <th>{__("Last semester")}</th>
                                    {*                         DEL START - ManhDD 06/04/2021*}
                                    {*                                <th>P1</th>*}
                                    {*                                <th>P2</th>*}
                                    {*                                <th>P3</th>*}
                                    {*                                <th>V1</th>*}
                                    {*                                <th>V2</th>*}
                                    {*                                <th>V3</th>*}
                                    {*                                <th>V4</th>*}
                                    {*                                <th>V5</th>*}
                                    {*                                <th>V6</th>*}
                                    {*                                <th>V7</th>*}
                                    {*                                <th>V8</th>*}
                                    {*                         DEL END - ManhDD 06/04/2021*}
                                </tr>
                            {/if}
                        {/foreach}
                        </tbody>
                    </table>
                {/if}
            {elseif $search_with == 'search_with_student' }
                {*                <div>*}
                {*                    <strong style="float: right">{__("Status ")} : {$status} </strong>*}
                {*                    {if($child_enough_point==true)}<strong>(g)</strong>{/if}*}
                {*                    {else}<strong>(s)</strong>{/if}*}
                {*                </div>*}
                <strong style="float: right">{__("Status ")} :
                    {if $status == 'Pass' }
                        <strong style="color:lawngreen">{__({$status})}</strong>
                    {elseif $status == 'Fail'}
                        <strong style="color:red">{__({$status})}</strong>
                    {elseif $status == 'Re-exam'}
                        <strong style="color:orange">{__({$status})}</strong>
                    {else}
                        <strong>{__({$status})}</strong>
                    {/if}

                </strong>
                <table class="table table-striped table-bordered" style="z-index: 1">
                    <thead>
                    <tr bgcolor="#fff">
                        <th rowspan="2" class="pinned" style="padding: 8px 6px" id="width_no">#</th>
                        <th rowspan="2" nowrap="true" class="pinned"
                            style="padding: 8px 6px">{__("Subject name")}</th>
                        <th colspan="4">{__("Semester 1")}</th>
                        <th colspan="4">{__("Semester 2")}</th>
                    </tr>
                    <tr>
                        <th>M1</th>
                        <th>M2</th>
                        <th>M3</th>
                        <th>D1</th>
                        <th>M1</th>
                        <th>M2</th>
                        <th>M3</th>
                        <th>D2</th>
                    </tr>
                    </thead>
                    <tbody>
                    {$rowIdx = 1}
                    {foreach $rows as $k => $row}
                        <tr>
                            <td align="center" class="pinned">{$rowIdx}</td>

                            <td nowrap="true" class="pinned text-bold color-blue">
                                <strong>{$row['subject_name']}</strong>
                            </td>
                            {$rowIdx = $rowIdx + 1}
                            {foreach $subject_key as $key}
                                <td>{$row[strtolower($key)]}</td>
                            {/foreach}
                        </tr>
                    {/foreach}
                    {*                    Các điểm trung bình*}
                    <tr>
                        <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                            <strong>{__("Average monthly")}</strong>
                        </td>
                        <td>{number_format($children_point_avgs['a1'] ,2)}</td>
                        <td>{number_format($children_point_avgs['b1'] ,2)}</td>
                        <td>{number_format($children_point_avgs['c1'] ,2)}</td>
                        <td>{number_format($children_point_avgs['d1'] ,2)}</td>
                        <td>{number_format($children_point_avgs['a2'] ,2)}</td>
                        <td>{number_format($children_point_avgs['b2'] ,2)}</td>
                        <td>{number_format($children_point_avgs['c2'] ,2)}</td>
                        <td>{number_format($children_point_avgs['d2'] ,2)}</td>

                    </tr>
                    <tr>
                        <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                            <strong>{__("Average semesterly")}</strong>
                        </td>
                        <td colspan="3" style="text-align: center">{number_format($children_point_avgs['x1'] ,2)}</td>
                        <td></td>
                        <td colspan="3" style="text-align: center">{number_format($children_point_avgs['x2'] ,2)}</td>
                        <td></td>
                    </tr>
                    {*                    kết thúc kỳ*}
                    <tr>
                        <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                            <strong>{__("End semester")}</strong>
                        </td>
                        <td colspan="4" style="text-align: center">{number_format($children_point_avgs['e1'] ,2)}</td>
                        <td colspan="4" style="text-align: center">{number_format($children_point_avgs['e2'] ,2)}</td>
                    </tr>
                    {*                    kết thúc năm *}
                    <tr>
                        <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                            <strong>{__("End year")}</strong>
                        </td>
                        <td colspan="8" style="text-align: center">{number_format($children_point_avgs['y'] ,2)}</td>
                    </tr>
                    {*                    Nghỉ có phép*}
                    <tr>
                        <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                            <strong>{__("Absent has permission")}</strong>
                        </td>
                        <td colspan="8" style="text-align: center">{$child_absent['absent_true']}</td>
                    </tr>
                    {*                    nghỉ không phép*}
                    <tr>
                        <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                            <strong>{__("Absent without permission")}</strong>
                        </td>
                        <td colspan="8" style="text-align: center">{$child_absent['absent_false']}</td>
                    </tr>
                    </tbody>
                </table>
                <strong>{__("Re-Exam")}</strong>
                <table class="table table-striped table-bordered" style="z-index: 1;">
                    <thead>
                    <tr bgcolor="#fff">
                        <th rowspan="2" class="pinned" style="padding: 8px 6px" id="width_no">#</th>
                        <th rowspan="2" nowrap="true" class="pinned"
                            style="padding: 8px 6px">{__("Subject name")}
                        </th>
                        <th colspan="1">{__("Point")}</th>
                    </tr>
                    </thead>
                    <tbody>
                    {$rowIdx = 1}
                    {foreach $children_subject_reexams as $k => $row}
                        <tr>
                            <td align="center" class="pinned">{$rowIdx}</td>
                            <td nowrap="true" class="pinned text-bold color-blue">
                                <strong>{$row['name']}</strong>
                            </td>
                            <td style="text-align: center">{$row['point']}</td>
                            {$rowIdx = $rowIdx + 1}
                        </tr>
                    {/foreach}
                    <tr>
                        <td colspan="2" nowrap="true" class="pinned text-bold color-blue"
                            style="text-align: center">
                            <strong>{__("Result Re-exam")}</strong>
                        </td>
                        <td colspan="3" style="text-align: center">{number_format($result_exam ,2)}</td>
                    </tr>
                    </tbody>
                </table>
            {elseif $search_with == 'showDataImport' }
                {if $semester == 0}
                    <table class="table table-striped table-bordered table-pinned" style="z-index: 1">
                        <thead>
                        <tr bgcolor="#fff">
                            <th rowspan="2" class="pinned" style="padding: 8px 6px" id="width_no">#</th>
                            <th rowspan="2" nowrap="true" class="pinned"
                                style="padding: 8px 6px">{__("Student name")}</th>
                            <th rowspan="2" nowrap="true"
                                style="padding: 8px 6px">{__("Student code")}</th>

                            {*                            <th rowspan="2" nowrap="true" class="pinned"*}
                            {*                                style="padding: 8px 6px">{__("Student name")}</th>*}
                            <th colspan="4">{__("Semester 1")}</th>
                            <th colspan="4">{__("Semester 2")}</th>
                            <th rowspan="2">{__("Re exam")}</th>
                        </tr>
                        <tr>
                            <th>M1</th>
                            <th>M2</th>
                            <th>M3</th>
                            <th>{__("Last")}</th>
                            <th>M1</th>
                            <th>M2</th>
                            <th>M3</th>
                            <th>{__("Last")}</th>
                        </tr>
                        </thead>
                        <tbody>
                        {$rowIdx = 1}
                        {foreach $rows as $k => $row}
                            <tr>
                                <td align="center" class="pinned">{$rowIdx}</td>
                                <td nowrap="true" class="pinned text-bold color-blue">
                                    <strong>{$row['child_lastname']} {$row['child_firstname']}</strong></td>
                                <td nowrap="true" class="text-bold color-blue"><a
                                            href="{$system['system_url']}/school/{$username}/children/detail/{$row['child_id']}"><strong>{$row['child_code']}</strong></a>
                                </td>

                                {*                                <td nowrap="true" class="pinned text-bold color-blue">*}
                                {*                                    <strong>{$row['child_firstname']}</strong></td>*}
                                {$rowIdx = $rowIdx + 1}
                                {foreach $subject_key as $key}
                                    {*                                    <td>{$row[$key]}</td>*}
                                    {if $key=='reexam' && !$row['is_reexam'] }
                                        <td>{$row[$key]}</td>
                                    {else}
                                        <td><input name="point" type="number" min="0" style="max-width: 50px"
                                                   value="{$row[$key]}"/></td>
                                    {/if}
                                {/foreach}
                            </tr>
                            {*                            *}{*Sau 15 trẻ thì thêm header cho dễ nhìn*}
                            {*                            {if $rowIdx % 10 == 0}*}
                            {*                                <tr bgcolor="#fff">*}
                            {*                                    <th class="pinned" style="padding: 8px 6px" id="width_no">#</th>*}
                            {*                                    <th nowrap="true" class="pinned" style="padding: 8px 6px">{__("Child code")}</th>*}
                            {*                                    <th nowrap="true" class="pinned" style="padding: 8px 6px">{__("Student name")}</th>*}
                            {*                                    <th nowrap="true" class="pinned" style="padding: 8px 6px">{__("Student name")}</th>*}
                            {*                                    <th>{__("TBHK1")}</th>*}
                            {*                                    <th>{__("TBHK2")}</th>*}
                            {*                                    <th>{__("TB Cả năm")}</th>*}
                            {*                                    <th>{__("Điểm thi lại")}</th>*}
                            {*                                </tr>*}
                            {*                            {/if}*}
                        {/foreach}
                        </tbody>
                    </table>
                {else}
                    <table class="table table-striped table-bordered table-pinned" style="z-index: 1">
                        <thead>
                        <tr bgcolor="#fff">
                            <th rowspan="2" class="pinned" style="padding: 8px 6px" id="width_no">#</th>
                            <th rowspan="2" nowrap="true" class="pinned"
                                style="padding: 8px 6px">{__("Student name")}</th>
                            <th rowspan="2" nowrap="true" class="pinned"
                                style="padding: 8px 6px">{__("Student code")}</th>
                            {*                        /* UPDATE START - ManhDD 06/04/2021 */*}
                            <th colspan="4">{__("Semester")}</th>
                            {*                        <th colspan="3">{__("Mouth")}</th>*}
                            {*                        <th colspan="3">{__("15 Minutes")}</th>*}
                            {*                        <th colspan="8">{__("1 Tiết")}</th>*}
                            {*                            <th rowspan="2">{__("Học kỳ")}</th>*}
                            {*                        <th rowspan="2">{__("Điểm TBHK")}</th>*}
                            {*                        /* UPDATE START - ManhDD 06/04/2021 */*}
                        </tr>
                        <tr>
                            <th>M1</th>
                            <th>M2</th>
                            <th>M3</th>
                            <th>{__("Last")}</th>
                        </tr>
                        </thead>
                        <tbody>
                        {$rowIdx = 1}
                        {foreach $rows as $k => $row}
                            <tr>
                                <td align="center" class="pinned">{$rowIdx}</td>
                                <td nowrap="true" class="pinned text-bold color-blue">
                                    <strong>{$row['child_lastname']} {$row['child_firstname']}</strong></td>
                                <td nowrap="true" class="pinned text-bold color-blue"><a
                                            href="{$system['system_url']}/school/{$username}/children/detail/{$row['child_id']}"><strong>{$row['child_code']}</strong></a>
                                </td>
                                {$rowIdx = $rowIdx + 1}
                                {foreach $subject_key as $key}
                                    {*                                    <td>{$row[$key]}</td>*}
                                    <td><input type="number" style="max-width: 50px" value="{$row[$key]}"/></td>
                                {/foreach}
                            </tr>
                            {*Sau 15 trẻ thì thêm header cho dễ nhìn*}
                            {if $rowIdx % 10 == 0}
                                <tr bgcolor="#fff">
                                    <th rowspan="2" class="pinned" style="padding: 8px 6px" id="width_no">#</th>
                                    <th rowspan="2" nowrap="true" class="pinned"
                                        style="padding: 8px 6px">{__("Student code")}</th>
                                    <th rowspan="2" nowrap="true" class="pinned"
                                        style="padding: 8px 6px">{__("Student name")}</th>
                                    <th rowspan="2" nowrap="true" class="pinned"
                                        style="padding: 8px 6px">{__("Student name")}</th>
                                    <th colspan="4">{__("Semester")}</th>
                                    {*                         DEL START - ManhDD 06/04/2021*}
                                    {*                                <th colspan="3">{__("15 Minute")}</th>*}
                                    {*                                <th colspan="8">{__("1 Tiết")}</th>*}
                                    {*                         DEL END - ManhDD 06/04/2021*}
                                    {*                                    <th rowspan="2">{__("Semester")}</th>*}
                                    {*                                <th rowspan="2">{__("Điểm TBHK")}</th>*}
                                </tr>
                                <tr>
                                    <th>M1</th>
                                    <th>M2</th>
                                    <th>M3</th>
                                    <th>{__("Last semester")}</th>
                                    {*                         DEL START - ManhDD 06/04/2021*}
                                    {*                                <th>P1</th>*}
                                    {*                                <th>P2</th>*}
                                    {*                                <th>P3</th>*}
                                    {*                                <th>V1</th>*}
                                    {*                                <th>V2</th>*}
                                    {*                                <th>V3</th>*}
                                    {*                                <th>V4</th>*}
                                    {*                                <th>V5</th>*}
                                    {*                                <th>V6</th>*}
                                    {*                                <th>V7</th>*}
                                    {*                                <th>V8</th>*}
                                    {*                         DEL END - ManhDD 06/04/2021*}
                                </tr>
                            {/if}
                        {/foreach}
                        </tbody>
                    </table>
                {/if}
            {/if}
        {elseif $grade == 1}
            <table class="table table-striped table-bordered table-pinned" style="z-index: 1">
                <thead>
                <tr bgcolor="#fff">
                    <th class="pinned" style="padding: 8px 6px" id="width_no">#</th>
                    <th nowrap="true" class="pinned" style="padding: 8px 6px">{__("Student code")}</th>
                    <th nowrap="true" class="pinned" style="padding: 8px 6px">{__("Last name")}</th>
                    <th nowrap="true" class="pinned" style="padding: 8px 6px">{__("First name")}</th>
                    <th>{__("Nhận xét")}</th>
                    <th>{__("Năng lực")}</th>
                    <th>{__("Phẩm chất")}</th>
                    <th>{__("KTCK")}</th>
                    <th>{__("XLCK")}</th>
                </tr>
                </thead>
                <tbody>
                {$rowIdx = 1}
                {foreach $rows as $k => $row}
                    <tr>
                        <td align="center" class="pinned">{$rowIdx}</td>
                        <td nowrap="true" class="pinned text-bold color-blue"><a
                                    href="{$system['system_url']}/school/{$username}/children/detail/{$row['child_id']}"><strong>{$row['child_code']}</strong></a>
                        </td>
                        <td nowrap="true" class="pinned text-bold color-blue"><strong>{$row['child_lastname']}</strong>
                        </td>
                        <td nowrap="true" class="pinned text-bold color-blue"><strong>{$row['child_firstname']}</strong>
                        </td>
                        {$rowIdx = $rowIdx + 1}
                        {foreach $subject_key as $key}
                            <td>{$row[$key]}</td>
                        {/foreach}
                    </tr>
                    {*Sau 10 trẻ thì thêm header cho dễ nhìn*}
                    {if $rowIdx % 10 == 0}
                        <tr bgcolor="#fff">
                            <th class="pinned" style="padding: 8px 6px" id="width_no">#</th>
                            <th nowrap="true" class="pinned" style="padding: 8px 6px">{__("Student code")}</th>
                            <th nowrap="true" class="pinned" style="padding: 8px 6px">{__("Last name")}</th>
                            <th nowrap="true" class="pinned" style="padding: 8px 6px">{__("First name")}</th>
                            <th>{__("Nhận xét")}</th>
                            <th>{__("Năng lực")}</th>
                            <th>{__("Phẩm chất")}</th>
                            <th>{__("KTCK")}</th>
                            <th>{__("XLCK")}</th>
                        </tr>
                    {/if}
                {/foreach}
                </tbody>
            </table>
        {/if}
    </div>
{else}
    <div align="center"><strong style="color: red">{__("Chưa có thông tin điểm")}</strong></div>
{/if}
{*Jquery Cố định cột số thứ tự và họ tên*}
<script type="text/javascript">

    var $table = $('.table-pinned');
    var $fixedColumn = $table.clone().insertBefore($table).addClass('fixed-column');
    $fixedColumn.find('th').each(function (i, elem) {
        $(this).width($table.find('th:eq(' + i + ')').width());
    });
    $fixedColumn.find('td').each(function (i, elem) {
        $(this).width($table.find('td:eq(' + i + ')').width());
    });
    $fixedColumn.find('th,td').not('.pinned').hide();
    $fixedColumn.find('[id]').each(function () {
        $(this).removeAttr('id');
    });

    $fixedColumn.find('tr').each(function (i, elem) {
        $(this).height($table.find('tr:eq(' + i + ')').height());
    });

    $(window).resize(function () {
        $fixedColumn.find('tr').each(function (i, elem) {
            $(this).height($table.find('tr:eq(' + i + ')').height());
        });
        $fixedColumn.find('td').each(function (i, elem) {
            $(this).addClass('white-space_nowrap');
            $(this).width($table.find('td:eq(' + i + ')').width());
        });
    });
    //$fixedColumn.find('td').addClass('white-space_nowrap');
    //    $("#right").on("click", function() {
    //        var leftPos = $('#example').scrollLeft();
    //        console.log(leftPos);
    //        $("#example").animate({
    //            scrollLeft: leftPos - 200
    //        }, 800);
    //    });
    $('.right').click(function (event) {
        var width_col = $('.table-pinned').find('td:eq(' + 4 + ')').width();
        var pos = $('#example').scrollLeft() + width_col + 100;
        return false;
        $('#example').scrollLeft(pos);
    });
    $('.left').click(function (event) {
        var width_col = $('.table-pinned').find('td:eq(' + 4 + ')').width();
        var pos = $('#example').scrollLeft() - width_col - 100;
        $('#example').scrollLeft(pos);
    });

    jQuery(function ($) {
        function fixDiv() {
            var $cache = $('#getFixed');
            var $button = $('#table_button');
            if ($(window).scrollTop() > 100) {
                $cache.css({
                    'position': 'fixed',
                    'top': '50px'
                });
                $cache.width($('#school_list_point').width() - 1);
            } else
                $cache.css({
                    'position': 'relative',
                    'top': 'auto'
                });

            if ($(window).scrollTop() > 100)
                $button.css({
                    'position': 'fixed',
                    'top': '90px'
                });
            else
                $button.css({
                    'position': 'relative',
                    'top': 'auto'
                });
        }

        $(window).scroll(fixDiv);
        fixDiv();
    });
</script>