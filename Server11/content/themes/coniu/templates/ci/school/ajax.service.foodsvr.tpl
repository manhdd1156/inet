{if (!isset($service_list) || (!isset($classes)))}
    <div class="pl20"><strong><font color="red">{__("There is no feed service or token-attendance class")}</font></strong></div>
{else}
    <div><strong>{__("Daily food service summary")}</strong></div>
    <table class="table table-striped table-bordered table-hover">
        <thead>
        <tr>
            <th>#</th>
            <th>{__("Class")}</th>
            <th>{__("Present")}</th>
            {foreach $service_list as $service}
                <th>{$service['service_name']}</th>
            {/foreach}
        </tr>
        </thead>
        <tbody>
        {$idx = 1}
        {$presentTotal = 0}
        {foreach $classes as $class}
            <tr>
                <td align="center">{$idx}</td>
                <td nowrap="true">{$class['group_title']}</td>
                <td align="center">{$class['present_total']}</td>
                {if isset($class['not_countbased_services']) && (count($class['not_countbased_services']) > 0)}
                    {foreach $class['not_countbased_services'] as $service}
                        <td align="center">{$service['total']}</td>
                    {/foreach}
                {else}
                    {foreach $service_list as $service}
                        {if $service['type'] != $smarty.const.SERVICE_TYPE_COUNT_BASED}
                            <td align="center">0</td>
                        {/if}
                    {/foreach}
                {/if}
                {if isset($class['countbased_services']) && (count($class['countbased_services']) > 0)}
                    {foreach $class['countbased_services'] as $service}
                        <td align="center">{$service['total']}</td>
                    {/foreach}
                {else}
                    {foreach $service_list as $service}
                        {if $service['type'] == $smarty.const.SERVICE_TYPE_COUNT_BASED}
                            <td align="center">0</td>
                        {/if}
                    {/foreach}
                {/if}
            </tr>
            {$presentTotal = $presentTotal + $class['present_total']}
            {$idx = $idx + 1}
        {/foreach}
        {if $idx > 2}
            <tr>
                <td colspan="2" align="center"><strong><font color="red">{__("Total")|upper}</font></strong></td>
                <td align="center"><strong><font color="red">{$presentTotal}</font></strong></td>
                {foreach $service_total as $total}
                    <td align="center"><strong><font color="red">{$total}</font></strong></td>
                {/foreach}
            </tr>
        {/if}
        </tbody>
    </table>
{/if}
