<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <i class="fa fa-cog pr5 panel-icon"></i>
        {__("Settings")}
    </div>
    <div class="panel-body">

        <!-- tabs nav -->
        <ul class="nav nav-tabs mb20">
            {if $canEdit}
                <li class="active">
                    <a href="#School" data-toggle="tab">
                        <strong class="pr5">{__("School")}</strong>
                    </a>
                </li>
                <li>
                    <a href="#Class" data-toggle="tab">
                        <strong class="pr5">{__("Class")}</strong>
                    </a>
                </li>
                <li>
                    <a href="#Attendance" data-toggle="tab">
                        <strong class="pr5">{__("Attendance")}</strong>
                    </a>
                </li>
                <li>
                    <a href="#Tuition" data-toggle="tab">
                        <strong class="pr5">{__("Tuition")}</strong>
                    </a>
                </li>
                <li>
                    <a href="#Camera" data-toggle="tab">
                        <strong class="pr5">{__("Camera")}</strong>
                    </a>
                </li>
                <li>
                    <a href="#Report" data-toggle="tab">
                        <strong class="pr5">{__("Contact book")}</strong>
                    </a>
                </li>
            {/if}
            <li {if !$canEdit}class="active"{/if}>
                <a href="#Notification" data-toggle="tab">
                    <strong class="pr5">{__("Notification")}</strong>
                </a>
            </li>
        </ul>
        <!-- tabs nav -->

        <!-- tabs content -->
        <div class="tab-content">
            {if $canEdit}
                <!-- School -->
                <div class="tab-pane active" id="School">
                    {if canView($username, 'settings')}
                        <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_setting.php">
                            <input type="hidden" name="do" value="school"/>
                            <input type="hidden" name="school_username" value="{$username}"/>
                            <div class="form-group">
                                <label class="col-sm-3 control-label text-left">{__("Allow commenting")}</label>
                                <div class="col-sm-9">
                                    <div class="onoffswitch">
                                        <input type="checkbox" name="school_allow_comment" class="onoffswitch-checkbox" id="school_allow_comment" {if $school['school_allow_comment']}checked{/if}>
                                        <label class="onoffswitch-label" for="school_allow_comment"></label>
                                    </div>
                                    <span class="help-block">{__("Allow comment on the school's page")}?</span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label text-left">{__("Parent can see contact list")}</label>
                                <div class="col-sm-9">
                                    <div class="onoffswitch">
                                        <input type="checkbox" name="display_children_list" class="onoffswitch-checkbox" id="display_children_list" {if $school['display_children_list']}checked{/if}>
                                        <label class="onoffswitch-label" for="display_children_list"></label>
                                    </div>
                                    <span class="help-block">{__("Allow parent seeing contact list of class")}?</span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label text-left">{__("Display parent information for others")}</label>
                                <div class="col-sm-9">
                                    <div class="onoffswitch">
                                        <input type="checkbox" name="display_parent_info_for_others" class="onoffswitch-checkbox" id="display_parent_info_for_others" {if $school['display_parent_info_for_others']}checked{/if}>
                                        <label class="onoffswitch-label" for="display_parent_info_for_others"></label>
                                    </div>
                                    <span class="help-block">{__("Display parent information for others in contact list")}?</span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">{__("Parent register service")}</label>
                                <div class="col-sm-9">
                                    <div class="onoffswitch">
                                        <input type="checkbox" name="allow_parent_register_service" class="onoffswitch-checkbox" id="allow_parent_register_service" {if $school['allow_parent_register_service']}checked{/if}>
                                        <label class="onoffswitch-label" for="allow_parent_register_service"></label>
                                    </div>
                                    <span class="help-block">{__("Allow parent register service")}?</span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">{__("Giáo viên sửa thông tin đón muộn trong quá khứ")}</label>
                                <div class="col-sm-9">
                                    <div class="onoffswitch">
                                        <input type="checkbox" name="allow_teacher_edit_pickup_before" class="onoffswitch-checkbox" id="allow_teacher_edit_pickup_before" {if $school['allow_teacher_edit_pickup_before']}checked{/if}>
                                        <label class="onoffswitch-label" for="allow_teacher_edit_pickup_before"></label>
                                    </div>
                                    <span class="help-block">{__("Giáo viên có được sửa thông tin đón muộn ngày trong quá khứ không")}?</span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">{__("To take medicine")}</label>
                                <div class="col-sm-9">
                                    <div class="onoffswitch">
                                        <input type="checkbox" name="school_allow_medicate" class="onoffswitch-checkbox" id="school_allow_medicate" {if $school['school_allow_medicate']}checked{/if}>
                                        <label class="onoffswitch-label" for="school_allow_medicate"></label>
                                    </div>
                                    <span class="help-block">{__("To take medicine")}?</span>
                                </div>
                            </div>

                            {if $canEdit}
                                <div class="form-group">
                                    <div class="col-sm-9 col-sm-offset-3">
                                        <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                                    </div>
                                </div>
                            {/if}
                            <!-- success -->
                            <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                            <!-- success -->
                            <!-- error -->
                            <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                            <!-- error -->
                        </form>
                    {/if}
                </div>
                <!-- School -->

                <!-- Class -->
                <div class="tab-pane" id="Class">
                    {if canView($username, 'settings')}
                        <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_setting.php">
                            <input type="hidden" name="do" value="class"/>
                            <input type="hidden" name="school_username" value="{$username}"/>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">{__("Parent posting")}</label>
                                <div class="col-sm-9">
                                    <div class="onoffswitch">
                                        <input type="checkbox" name="class_allow_post" class="onoffswitch-checkbox" id="class_allow_post" {if $school['class_allow_post']}checked{/if}>
                                        <label class="onoffswitch-label" for="class_allow_post"></label>
                                    </div>
                                    <span class="help-block">{__("Allow parent posting on class wall")}?</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">{__("Parent comment post of class")}</label>
                                <div class="col-sm-9">
                                    <div class="onoffswitch">
                                        <input type="checkbox" name="class_allow_comment" class="onoffswitch-checkbox" id="class_allow_comment" {if $school['class_allow_comment']}checked{/if}>
                                        <label class="onoffswitch-label" for="class_allow_comment"></label>
                                    </div>
                                    <span class="help-block">{__("Allow parent commenting post of class")}?</span>
                                </div>
                            </div>
                            {*<div class="form-group">*}
                                {*<label class="col-sm-3 control-label">{__("Create class-event")}</label>*}
                                {*<div class="col-sm-9">*}
                                    {*<div class="onoffswitch">*}
                                        {*<input type="checkbox" name="allow_class_event" class="onoffswitch-checkbox" id="allow_class_event" {if $school['allow_class_event']}checked{/if}>*}
                                        {*<label class="onoffswitch-label" for="allow_class_event"></label>*}
                                    {*</div>*}
                                    {*<span class="help-block">{__("Allow teacher creating class-event")}?</span>*}
                                {*</div>*}
                            {*</div>*}
                            <div class="form-group">
                                <label class="col-sm-3 control-label">{__("Use un-assigned class for child mode")}?</label>
                                <div class="col-sm-9">
                                    <div class="onoffswitch">
                                        <input type="checkbox" name="children_use_no_class" class="onoffswitch-checkbox" id="children_use_no_class" {if $school['children_use_no_class']}checked{/if}>
                                        <label class="onoffswitch-label" for="children_use_no_class"></label>
                                    </div>
                                    <span class="help-block">{__("When creating child, can creator choose un-assigned class for child")}?</span>
                                </div>
                            </div>
                            {if $canEdit}
                                <div class="form-group">
                                    <div class="col-sm-9 col-sm-offset-3">
                                        <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                                    </div>
                                </div>
                            {/if}
                            <!-- success -->
                            <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                            <!-- success -->

                            <!-- error -->
                            <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                            <!-- error -->
                        </form>
                    {/if}
                </div>
                <!-- Class -->

                <!-- Attendance -->
                <div class="tab-pane" id="Attendance">
                    {if canView($username, 'settings')}
                        <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_setting.php">
                            <input type="hidden" name="do" value="attendance"/>
                            <input type="hidden" name="school_username" value="{$username}"/>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">{__("The teacher rolls in the days before")}</label>
                                <div class="col-sm-9">
                                    <div class="onoffswitch">
                                        <input type="checkbox" name="allow_teacher_rolls_days_before" class="onoffswitch-checkbox" id="allow_teacher_rolls_days_before" {if $school['allow_teacher_rolls_days_before']}checked{/if}>
                                        <label class="onoffswitch-label" for="allow_teacher_rolls_days_before"></label>
                                    </div>
                                    <span class="help-block">{__("Allow teacher rolls in the days before")}?</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">{__("Allow to delete past attendance")}?</label>
                                <div class="col-sm-9">
                                    <div class="onoffswitch">
                                        <input type="checkbox" name="attendance_allow_delete_old" id="attendance_allow_delete_old" class="onoffswitch-checkbox" {if $school['attendance_allow_delete_old']}checked{/if}>
                                        <label class="onoffswitch-label" for="attendance_allow_delete_old"></label>
                                    </div>
                                    <span class="help-block">{__("Check this to allow deleting past attendance")}.</span>
                                </div>
                            </div>
                            {*<div class="form-group">*}
                                {*<label class="col-sm-3 control-label">{__("Use leavel-early mode")}?</label>*}
                                {*<div class="col-sm-9">*}
                                    {*<div class="onoffswitch">*}
                                        {*<input type="checkbox" name="attendance_use_leave_early" id="attendance_use_leave_early" class="onoffswitch-checkbox" {if $school['attendance_use_leave_early']}checked{/if}>*}
                                        {*<label class="onoffswitch-label" for="attendance_use_leave_early"></label>*}
                                    {*</div>*}
                                    {*<span class="help-block">{__("In this mode, when children leaving school early then service using and eating are free for the children")}.</span>*}
                                {*</div>*}
                            {*</div>*}
                            {*<div class="form-group">*}
                                {*<label class="col-sm-3 control-label">{__("Use come-late mode")}?</label>*}
                                {*<div class="col-sm-9">*}
                                    {*<div class="onoffswitch">*}
                                        {*<input type="checkbox" name="attendance_use_come_late" id="attendance_use_come_late" class="onoffswitch-checkbox" {if $school['attendance_use_come_late']}checked{/if}>*}
                                        {*<label class="onoffswitch-label" for="attendance_use_come_late"></label>*}
                                    {*</div>*}
                                    {*<span class="help-block">{__("Adding more come-late mode to attendance")}.</span>*}
                                {*</div>*}
                            {*</div>*}
                            {*<div class="form-group">*}
                                {*<label class="col-sm-3 control-label">{__("Use charging absence mode")}?</label>*}
                                {*<div class="col-sm-9">*}
                                    {*<div class="onoffswitch">*}
                                        {*<input type="checkbox" name="attendance_absence_no_reason" id="attendance_absence_no_reason" class="onoffswitch-checkbox" {if $school['attendance_absence_no_reason']}checked{/if}>*}
                                        {*<label class="onoffswitch-label" for="attendance_absence_no_reason"></label>*}
                                    {*</div>*}
                                    {*<span class="help-block">{__("In some cases with absent child , using serivces and food expenses are still charged to this child")}.</span>*}
                                {*</div>*}
                            {*</div>*}
                            {if $canEdit}
                                <div class="form-group">
                                    <div class="col-sm-9 col-sm-offset-3">
                                        <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                                    </div>
                                </div>
                            {/if}
                            <!-- success -->
                            <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                            <!-- success -->
                            <!-- error -->
                            <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                            <!-- error -->
                        </form>
                    {/if}
                </div>
                <!-- Attendance -->

                <!-- Tuition -->
                <div class="tab-pane" id="Tuition">
                    {if canView($username, 'settings')}
                        <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_setting.php">
                            <input type="hidden" name="do" value="tuition"/>
                            <input type="hidden" name="school_username" value="{$username}"/>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">{__("Tuition notification interval (day)")}</label>
                                <div class="col-sm-9">
                                    <input type="number" class="form-control" name="notification_tuition_interval" min="1" max="30" value="{$school['notification_tuition_interval']}" autofocus>
                                </div>
                            </div>
                            <div class = "form-group">
                                <label class="col-sm-3 control-label">{__("Transfer credit information")}</label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" name="bank_account" rows="4" {if $user->_data['user_id'] != $school['page_admin']}disabled{/if}>{$school['bank_account']}</textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">{__("Teachers can see tuition information")}</label>
                                <div class="col-sm-9">
                                    <div class="onoffswitch">
                                        <input type="checkbox" name="allow_class_see_tuition" class="onoffswitch-checkbox" id="allow_class_see_tuition" {if $school['allow_class_see_tuition']}checked{/if}>
                                        <label class="onoffswitch-label" for="allow_class_see_tuition"></label>
                                    </div>
                                    <span class="help-block">{__("Allow teachers seeing tuition information of class")}?</span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">{__("View attendance when creating/updating tuition")}?</label>
                                <div class="col-sm-9">
                                    <div class="onoffswitch">
                                        <input type="checkbox" name="tuition_view_attandance" id="tuition_view_attandance" class="onoffswitch-checkbox" {if $school['tuition_view_attandance']}checked{/if}>
                                        <label class="onoffswitch-label" for="tuition_view_attandance"></label>
                                    </div>
                                    <span class="help-block">{__("Select this option to view attendance information in creating and updating tuition screen")}.</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">{__("Use deduction mode for monthly and daily service")}?</label>
                                <div class="col-sm-9">
                                    <div class="onoffswitch">
                                        <input type="checkbox" name="tuition_use_mdservice_deduction" id="tuition_use_mdservice_deduction" class="onoffswitch-checkbox" {if $school['tuition_use_mdservice_deduction']}checked{/if}>
                                        <label class="onoffswitch-label" for="tuition_use_mdservice_deduction"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">{__("Child tuition report template")}</label>
                                <div class="col-sm-9">
                                    <select name="tuition_child_report_template" id="tuition_child_report_template" class="form-control">
                                        <option value="A5" {if $school['tuition_child_report_template']=='A5'}selected{/if}>A5</option>
                                        <option value="A4" {if $school['tuition_child_report_template']=='A4'}selected{/if}>A4</option>
                                    </select>
                                    <span class="help-block">{__("Select the template for tuition report sent to parent")} (A4/A5).</span>
                                </div>
                            </div>
                            {if $canEdit}
                                <div class="form-group">
                                    <div class="col-sm-9 col-sm-offset-3">
                                        <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                                    </div>
                                </div>
                            {/if}
                            <!-- success -->
                            <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                            <!-- success -->
                            <!-- error -->
                            <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                            <!-- error -->
                        </form>
                    {/if}
                </div>
                <!-- Tuition -->

                <!-- Camera -->
                <div class="tab-pane" id="Camera">
                    {if canView($username, 'settings')}
                        <form class="form-horizontal" enctype="multipart/form-data" method="post" id="update_camera_guide">
                            <input type="hidden" name="do" value="camera"/>
                            <input type="hidden" name="school_username" value="{$username}"/>
                            <div class="form-group">
                                <label class="col-sm-12 control-label" style="text-align: center">{__("Camera guide")}</label>
                            </div>
                            <input type = "hidden" id = "is_file" name = "is_file" value = "1">
                            <div class="form-group" id = "file_old">
                                <label class="rp-file col-sm-3 control-label text-left" style="padding-top: 0px">{__("File attachment")}</label>
                                <div class="col-sm-6">
                                    {if !is_empty($school['camera_guide'])}
                                        <a href="{$school['camera_guide']}" download="{__("Camera guide")}">{__("Camera guide")}</a>
                                    {else} {__('No file attachment')}
                                    {/if}
                                </div>
                            </div>
                            <div class = "form-group">
                                <label class="control-label col-sm-3">
                                    {if is_empty($school['camera_guide'])}
                                        {__("Choose file")}
                                    {else}
                                        {__("Choose file replace")}
                                    {/if}
                                </label>
                                <div class = "col-sm-6">
                                    <input type="file" name="file" id="file"/>
                                </div>
                                <a class = "delete_image btn btn-danger btn-xs text-left">{__('Delete')}</a>
                            </div>
                            {if $canEdit}
                                <div class="form-group">
                                    <div class="col-sm-9 col-sm-offset-3">
                                        <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                                    </div>
                                </div>
                            {/if}
                            <!-- success -->
                            <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                            <!-- success -->
                            <!-- error -->
                            <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                            <!-- error -->
                        </form>
                    {/if}
                </div>
                <!-- Camera -->

                <!-- Report -->
                <div class="tab-pane" id="Report">
                {if canView($username, 'settings')}
                    <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_setting.php">
                        <input type="hidden" name="do" value="report"/>
                        <input type="hidden" name="school_username" value="{$username}"/>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">{__("Contact book creat interval for student")}</label>
                            <div class="col-sm-9">
                                <select name="report_interval" id="report_interval" class="form-control">
                                    <option value="">{__("Select interval")}</option>
                                    <option value="1" {if $school['report_cycle']=='1'}selected{/if}>{__("Daily")}</option>
                                    <option value="2" {if $school['report_cycle']=='2'}selected{/if}>{__("Weekly")}</option>
                                    <option value="3" {if $school['report_cycle']=='3'}selected{/if}>{__("Monthly")}</option>
                                </select>
                                <span class="help-block">{__("Select the interval of writing the contact book for the student")}.</span>
                            </div>
                        </div>
                        {if $canEdit}
                            <div class="form-group">
                                <div class="col-sm-9 col-sm-offset-3">
                                    <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                                </div>
                            </div>
                        {/if}
                        <!-- success -->
                        <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                        <!-- success -->
                        <!-- error -->
                        <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                        <!-- error -->
                    </form>
                {/if}
                </div>
                <!-- Report -->
            {/if}
            <!-- Notification -->
            <div class="tab-pane {if !$canEdit}active{/if}" id="Notification">
                <div class = "form-group" align="center"><strong>{__("Notifications setting for you")}: {__("How do you get notifications from objects and functions?")}</strong></div>
                <form class = "js_ajax-forms form-horizontal" data-url = "ci/bo/school/bo_setting.php">
                    <input type="hidden" name="school_username" id = "school_username" value="{$username}"/>
                    <input type="hidden" name="do" value="notification"/>
                    <div class = "table-responsive">
                        <table class = "table table-striped table-bordered table-hover">
                            <thead>
                            <tr>
                                <th rowspan="2">#</th>
                                <th rowspan="2">{__('Module')}</th>
                                <th>{__('Teacher')}</th>
                                <th>{__('Parent')}</th>
                            </tr>
                            <tr>
                                <th><input type="checkbox" id="checkAllTeacherNotify"/>{__('All')}</th>
                                <th><input type="checkbox" id="checkAllParentNotify"/>{__('All')}</th>
                            </tr>
                            </thead>
                            <tbody>
                            {foreach $modules as $k => $row}
                                <tr>
                                    {if $row['show']}
                                        <td align="center"><strong>{$k + 1}</strong></td>
                                        <td>{__($row['module_name'])}</td>
                                        <td align="center">
                                            <input type="checkbox" name="{$row['module']}_teacher" value="{$smarty.const.STATUS_ACTIVE}" class="teacher {if ($row['module_type'] == $smarty.const.NOTIFY_PARENT)} hidden {/if}"  {if (($row['value'] == $smarty.const.NOTIFY_ONLY_TEACHER) || ($row['value'] == $smarty.const.NOTIFY_ALL))} checked {/if}>
                                        </td>
                                        <td align="center">
                                            <input type="checkbox" name="{$row['module']}_parent" value="{$smarty.const.STATUS_ACTIVE}" class="parent {if ($row['module_type'] == $smarty.const.NOTIFY_TEACHER)} hidden {/if}" {if (($row['value'] == $smarty.const.NOTIFY_ONLY_PARENT) || ($row['value'] == $smarty.const.NOTIFY_ALL))} checked {/if}>
                                        </td>
                                    {/if}
                                </tr>
                            {/foreach}
                            </tbody>
                        </table>
                    </div>
                    <div class="form-group">
                        <div class="form-group">
                            <div class="col-sm-9 col-sm-offset-3">
                                <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                            </div>
                        </div>
                        <!-- success -->
                        <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                        <!-- success -->
                        <!-- error -->
                        <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                        <!-- error -->
                    </div>
                </form>
            </div>
            <!-- Notification -->
        </div>
        <!-- tabs content -->
    </div>
</div>