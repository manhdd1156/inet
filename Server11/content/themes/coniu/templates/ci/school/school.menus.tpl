<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <div class="pull-right flip">
            <a href="https://blog.coniu.vn/huong-dan-them-thuc-don-tren-website/" target="_blank" class="btn btn-info btn_guide">
                <i class="fa fa-info-circle"></i> {__("Guide")}
            </a>
            {if ($sub_view == "") && $canEdit}
                <a href="{$system['system_url']}/school/{$username}/menus/add" class="btn btn-default">
                    <i class="fa fa-plus"></i> {__("Add New")}
                </a>
            {elseif $sub_view == "add"}
                <a href="{$system['system_url']}/school/{$username}/menus" class="btn btn-default">
                    <i class="fa fa-list"></i> {__("Lists")}
                </a>
            {/if}
        </div>
        <i class="fas fa-utensils fa-fw fa-lg pr10"></i>
        {__("Menu")}
        {if $sub_view == ""}
            &rsaquo; {__("Menu list")}
        {elseif $sub_view == "add"}
            &rsaquo; {__('Add menu')}
        {elseif $sub_view == "edit"}
            &rsaquo; {__('Edit menu')}
        {elseif $sub_view == "copy"}
            &rsaquo; {__('Copy menu')}
        {elseif $sub_view == "detail"}
            &rsaquo; {__('Detail menu')} {$data['menu_name']}
        {/if}
    </div>
{if $sub_view == ""}
    <div class="panel-body with-table form-horizontal">
        <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_menu.php">
            <input type="hidden" name="school_username" id="school_username" value="{$username}"/>
            <input type="hidden" name="do" value="search"/>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left">{__("Scope")}</label>
                <div class="col-sm-9">
                    <div class="col-xs-4">
                        <select name="level" id="schedule_level" class="form-control" data-username = "{$username}" data-id = "{$school['page_id']}">
                            <option value="0">{__("Select scope")}</option>
                            <option value="{$smarty.const.SCHOOL_LEVEL}">{__("School")}</option>
                            <option value="{$smarty.const.CLASS_LEVEL_LEVEL}">{__("Class level")}</option>
                            <option value="{$smarty.const.CLASS_LEVEL}">{__("Class")}</option>
                        </select>
                    </div>
                    <div class="col-xs-4 x-hidden" name="schedule_class_level">
                        <select name="class_level_id" id="class_level_id" class="form-control">
                            <option value="0">{__("Select class level")}</option>
                            {foreach $class_levels as $class_level}
                                <option value="{$class_level['class_level_id']}">{$class_level['class_level_name']}</option>
                            {/foreach}
                        </select>
                    </div>
                    <div class="col-xs-4 x-hidden" name="schedule_class">
                        <select name="class_id" id="class_id" data-username="{$username}" class="form-control">
                            <option value="0">{__("Select class")}</option>
                            {$class_level = -1}
                            {foreach $classes as $class}
                                {if ($class_level != $class['class_level_id'])}
                                    <option value="" disabled style="color: blue">-----{$class['class_level_name']}-----</option>
                                {/if}
                                <option value="{$class['group_id']}">{$class['group_title']}</option>
                                {$class_level = $class['class_level_id']}
                            {/foreach}

                        </select>
                    </div>
                    <div class="col-sm-3">
                        <a class="btn btn-default js_school_menu_search" data-username = "{$username}" data-handle = "search">{__("Search")}</a>
                    </div>
                </div>
            </div>

        </form>
        <div class="table-responsive" name = "menu_all">
            <table class="table table-striped table-bordered table-hover js_dataTable">
                <thead>
                <tr><th colspan="7">{__("Menu list")}&nbsp;(<span class="count_menu">{$rows|count}</span>)</th></tr>
                <tr>
                    <th>#</th>
                    <th>
                        {__("Menu name")}
                    </th>
                    <th>
                        {__("Begin")}
                    </th>
                    <th>
                        {__("Scope")}
                    </th>
                    <th>
                        {__("Use meal")}
                    </th>
                    <th>
                        {__("Study saturday")}
                    </th>
                    <th>
                        {__("Actions")}
                    </th>
                </tr>
                </thead>
                <tbody>
                {$idx = 1}
                {foreach $rows as $row}
                    <tr>
                        <td class="align-middle" align="center">
                            {$idx}
                        </td>
                        <td class="align-middle">
                            <a href="{$system['system_url']}/school/{$username}/menus/detail/{$row['menu_id']}">{$row['menu_name']}</a>
                        </td>
                        <td class="align-middle" align="center">
                            {$row['begin']}
                        </td>
                        <td class="align-middle" align="center">
                            {if $row['applied_for']==$smarty.const.SCHOOL_LEVEL}
                                {__("School")}
                            {elseif $row['applied_for']==$smarty.const.CLASS_LEVEL_LEVEL}
                                {foreach $class_levels as $cl}
                                    {if $cl['class_level_id'] == $row['class_level_id']}{$cl['class_level_name']}{/if}
                                {/foreach}
                            {elseif $row['applied_for']==$smarty.const.CLASS_LEVEL}
                                {foreach $classes as $value}
                                    {if $value['group_id'] == $row['class_id']}{$value['group_title']}{/if}
                                {/foreach}
                            {/if}
                        </td>
                        <td class="align-middle" align="center">
                            {if $row['is_meal']}
                                {__('Yes')}
                            {else}
                                {__('No')}
                            {/if}
                        </td>
                        <td class="align-middle" align="center">
                            {if $row['is_saturday']}
                                {__('Yes')}
                            {else}
                                {__('No')}
                            {/if}
                        </td>
                        <td class="align-middle">
                            {if $canEdit}
                                <div>
                                    <a href="{$system['system_url']}/school/{$username}/menus/copy/{$row['menu_id']}" class="btn btn-xs btn-default">{__("Copy")}</a>
                                    <a href="{$system['system_url']}/school/{$username}/menus/edit/{$row['menu_id']}" class="btn btn-xs btn-default">{__("Edit")}</a>
                                    <button class="btn btn-xs btn-danger js_school-menu-delete" data-username="{$username}" data-id="{$row['menu_id']}" data-handle = "delete_menu">{__("Delete")}</button>
                                    {if !$row['is_notified']}
                                            <div class = "form-label"><button class="btn btn-xs btn-default js_school-menu-notify" data-handle="notify" data-username="{$username}" data-id="{$row['menu_id']}">{__("Notify")}</button></div>
                                    {/if}
                                </div>
                            {/if}
                            <button class="btn btn-xs btn-success js_school-menu-export" data-username="{$username}" data-id="{$row['menu_id']}" data-handle = "export">{__("Export to Excel")}</button>
                        </td>
                    </tr>
                    {$idx = $idx + 1}
                {/foreach}

                {if $rows|count == 0}
                    <tr class="odd">
                        <td valign="top" align="center" colspan="7" class="dataTables_empty">
                            {__("No data available in table")}
                        </td>
                    </tr>
                {/if}
                </tbody>
            </table>
        </div>
        <div id = "cate_list"></div>
    </div>
{elseif $sub_view == "detail"}
    <div class="panel-body with-table">
        <table class = "table table-bordered">
            <tbody>
                <tr>
                    <td class = "col-sm-3 text-right"><strong>{__('Menu name')}</strong></td>
                    <td>
                        {$data['menu_name']}
                    </td>
                </tr>
                <tr>
                    <td class = "col-sm-3 text-right"><strong>{__('Scope')}</strong></td>
                    <td>
                        {if isset($class) && $data['applied_for'] == 3} {$class['group_title']}
                        {elseif isset($class_level) && $data['applied_for'] == 2}{$class_level['class_level_name']}
                        {elseif $data['applied_for'] == 1} {__('School')}
                        {/if}
                    </td>
                </tr>
                <tr>
                    <td class = "col-sm-3 text-right">{__('Begin')}</td>
                    <td>
                        {$data['begin']}
                    </td>
                </tr>
                {*<tr>*}
                    {*<td class = "col-sm-3 text-right">{__('Use meal')}</td>*}
                    {*<td>*}
                        {*{if $data['is_meal']}*}
                            {*{__('Yes')}*}
                        {*{else}*}
                            {*{__('No')}*}
                        {*{/if}*}
                    {*</td>*}
                {*</tr>*}
                {*<tr>*}
                    {*<td class = "col-sm-3 text-right">{__('Study saturday')}</td>*}
                    {*<td>*}
                        {*{if $data['is_saturday']}*}
                            {*{__('Yes')}*}
                        {*{else}*}
                            {*{__('No')}*}
                        {*{/if}*}
                    {*</td>*}
                {*</tr>*}
                {if $data['description'] != ''}
                    <tr>
                        <td class = "col-sm-3 text-right"><strong>{__('Description')}</strong></td>
                        <td>
                            {nl2br($data['description'])}
                        </td>
                    </tr>
                {/if}
            </tbody>
        </table>
        <div class = "table-responsive">
            <table class = "table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th> <center>#</center> </th>
                        <th> <center>{__('Time')}</center> </th>
                        <th {if !$data['is_meal']}class="hidden"{/if}> <center>{__('Meal')}</center> </th>
                        <th> <center>{__('Monday')}</center> </th>
                        <th> <center>{__('Tuesday')}</center> </th>
                        <th> <center>{__('Wednesday')}</center> </th>
                        <th> <center>{__('Thursday')}</center> </th>
                        <th> <center>{__('Friday')}</center> </th>
                        <th {if !$data['is_saturday']}class="hidden"{/if}> <center>{__('Saturday')}</center> </th>
                    </tr>
                </thead>
                <tbody>
                    {$idx = 1}
                    {foreach $data['details'] as $k => $row}
                        {$array = array_values($row)}
                        {$temp = array()}
                        {foreach $array as $k => $value}
                            {if $data['is_saturday']}
                                {if ($k >= 4) && ($k < (count($array)))}
                                    {$temp[] = $value}
                                {/if}
                            {else}
                                {if ($k >= 4) && ($k < (count($array) - 1))}
                                    {$temp[] = $value}
                                {/if}
                            {/if}
                        {/foreach}
                        <tr>
                            <td style="vertical-align: middle" align="center">
                                {$idx}
                            </td>
                            <td style="vertical-align: middle" align="center">
                                <strong>{$row['meal_time']}</strong>
                            </td>
                            <td {if !$data['is_meal']}class="hidden"{/if} align="center">
                                <strong>{$row['meal_name']}</strong>
                            </td>
                            {$col = 1}
                            {for $i = 0; $i < count($temp); $i++}
                                {if $temp[$i] === $temp[($i+1)]}
                                    {$col = $col + 1}
                                {else}
                                    <td colspan = "{$col}" align="center">
                                        {nl2br($temp[$i])}
                                    </td>
                                    {$col = 1}
                                {/if}
                            {/for}
                        </tr>
                        {$idx = $idx + 1}
                    {/foreach}
                </tbody>
            </table>
        </div>
        <div class="form-group pl5">
            <a class="btn btn-default" href="{$system['system_url']}/school/{$username}/menus">{__("Lists")}</a>
            {if $canEdit}
                <a class="btn btn-default" href="{$system['system_url']}/school/{$username}/menus/add">{__("Add New")}</a>
                <a href="{$system['system_url']}/school/{$username}/menus/copy/{$data['menu_id']}" class="btn btn-default">{__("Copy")}</a>
                {if !$data['is_notified']}
                    <button class="btn btn-default js_school-menu-notify" data-handle="notify" data-username="{$username}" data-id="{$data['menu_id']}">{__("Notify")}</button>
                {/if}
                <a href="{$system['system_url']}/school/{$username}/menus/edit/{$data['menu_id']}" class="btn btn-default">{__("Edit")}</a>
                <button class="btn btn-danger js_school-menu-delete" data-username="{$username}" data-id="{$data['menu_id']}" data-handle = "delete_menu">{__("Delete")}</button>
            {/if}
        </div>
    </div>
{elseif $sub_view == "add"}
    <div class="panel-body">
        <div id="open_dialog_menu" class="x-hidden" title="{__("Menu preview")|upper}">
            {include file="ci/school/ajax.school.menu.preview.tpl"}
        </div>
        <form class="form-horizontal" action="#" enctype="multipart/form-data" method="post" id="import_menu_excel_form">
            <input type="hidden" name="school_username" id="school_username" value="{$username}"/>
            <input type="hidden" name="do" value="add" id="do"/>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left">{__("Menu name")} (*)</label>
                <div class="col-sm-9">
                    <input class="form-control" name="menu_name" required autofocus maxlength="100">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left">{__("Scope")} (*)</label>
                <div class="col-sm-3">
                    <select name="level" id="schedule_level" class="form-control" data-username = "{$username}" data-id = "{$school['page_id']}">
                        <option value="{$smarty.const.SCHOOL_LEVEL}">{__("School")}</option>
                        <option value="{$smarty.const.CLASS_LEVEL_LEVEL}">{__("Class level")}</option>
                        <option value="{$smarty.const.CLASS_LEVEL}">{__("Class")}</option>
                    </select>
                </div>
                <div class="col-sm-3 x-hidden" name="schedule_class_level">
                    <select name="class_level_id" id="class_level_id" class="form-control">
                        <option value="0">{__("Select class level")}</option>
                        {foreach $class_levels as $class_level}
                            <option value="{$class_level['class_level_id']}">{$class_level['class_level_name']}</option>
                        {/foreach}
                    </select>
                </div>
                <div class="col-sm-3 x-hidden" name="schedule_class">
                    <select name="class_id" id="class_id" data-username="{$username}" class="form-control">
                        <option value="0">{__("Select class")}</option>
                        {$class_level = -1}
                        {foreach $classes as $class}
                            {if ($class_level != $class['class_level_id'])}
                                <option value="" disabled style="color: blue">-----{$class['class_level_name']}-----</option>
                            {/if}
                            <option value="{$class['group_id']}">{$class['group_title']}</option>
                            {$class_level = $class['class_level_id']}
                        {/foreach}
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left">{__("Begin")} (*)</label>
                <div class='col-sm-3'>
                    <div class='input-group date' id='schedule_beginpicker_new'>
                        <input type='text' name="begin" class="form-control" placeholder="{__("Begin")} (*)" value="{$monday}" required/>
                        <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left"></label>
                <div class="col-sm-9">
                    <strong> {__('Note: start date must be monday')} </strong>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left">{__("Description")}</label>
                <div class="col-sm-9">
                    <textarea class="form-control js_autosize" name="description" rows="3"></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left">{__("Use meal")}?</label>
                <div class="col-sm-1">
                    <div class="onoffswitch">
                        <input type="checkbox" name="use_mea" class="onoffswitch-checkbox" id="use_meal" checked>
                        <label class="onoffswitch-label" for="use_meal"></label>
                    </div>
                </div>
                <label class="col-sm-2 control-label text-left">{__("Study saturday")}?</label>
                <div class="col-sm-1">
                    <div class="onoffswitch">
                        <input type="checkbox" name="study_saturday" class="onoffswitch-checkbox" id="study_saturday" checked>
                        <label class="onoffswitch-label" for="study_saturday"></label>
                    </div>
                </div>
                <label class="col-sm-3 control-label text-left">{__("Notify immediately")}?</label>
                <div class="col-sm-1">
                    <div class="onoffswitch">
                        <input type="checkbox" name="notify_immediately" class="onoffswitch-checkbox" id="notify_immediately">
                        <label class="onoffswitch-label" for="notify_immediately"></label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left">{__("Import from Excel file")}?</label>
                <div class="col-sm-1">
                    <div class="onoffswitch">
                        <input type="checkbox" name="import_excel" class="onoffswitch-checkbox" id="import_excel">
                        <label class="onoffswitch-label" for="import_excel"></label>
                    </div>
                </div>
            </div>
            <div class="table-responsive" id="cate_list">
                <table class="table table-striped table-bordered table-hover" id = "myTableMenu">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>
                            {__('Option')}
                        </th>
                        <th>
                            {__('Time')}
                        </th>
                        <th>
                            {__('Meal')}
                        </th>
                        <th>
                            {__('Monday')}
                        </th>
                        <th>
                            {__('Tuesday')}
                        </th>
                        <th>
                            {__('Wednesday')}
                        </th>
                        <th>
                            {__('Thursday')}
                        </th>
                        <th>
                            {__('Friday')}
                        </th>
                        <th>
                            {__('Saturday')}
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td align="center" class = "col_no">1</td>
                        <td align="center">
                            <a class = "btn btn-default js_cate-delete_null"> {__("Delete")} </a>
                        </td>
                        <td>
                            <input type = "text" name = "start[]" placeholder="12:00" style = "padding-left: 10px; height: 35px; min-width: 200px;">
                        </td>
                        <td>
                            <input type="text" name = "meal[]" placeholder="Ăn sáng" style = "padding-left: 10px; height: 35px; min-width: 200px;">
                        </td>
                        <td>
                            <textarea class = "note" type="text" name = "meal_detail_mon[]"> </textarea>
                        </td>
                        <td>
                            <textarea class = "note" type="text" name = "meal_detail_tue[]"> </textarea>
                        </td>
                        <td>
                            <textarea class = "note" type="text" name = "meal_detail_wed[]"> </textarea>
                        </td>
                        <td>
                            <textarea class = "note" type="text" name = "meal_detail_thu[]"> </textarea>
                        </td>
                        <td>
                            <textarea class = "note" type="text" name = "meal_detail_fri[]"> </textarea>
                        </td>
                        <td>
                            <textarea class = "note" type="text" name = "meal_detail_sat[]"> </textarea>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <div class = "form-group">
                    <div class = "col-sm-12">
                        <a class="btn btn-default js_cate-add-menu">+</a>
                    </div>
                </div>
            </div>
            <div class="hidden" id="select_excel">
                <div class="form-group color_red">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-9">
                        <strong>- {__("You must use the Excel file format specified by the system, download form at")} <a target="_blank" href="https://drive.google.com/file/d/1aK0p6--KoiSny59huSglKqedtxX2QeWv">{__("HERE")}</a>.</strong>
                        <br/>
                        - {__("You must")} <strong>{__("configure")}</strong> {__("the menu before SAVING")}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Select Excel file")}</label>
                    <div class="col-sm-6">
                        <input type="file" name="file" id="file"/>
                    </div>
                </div>
            </div>
            <div class = "form-group mt20">
                {*<div class = "col-sm-3 col-sm-offset-3">*}
                    {*<a class="btn btn-default js_cate-add-menu">{__("Add new row")}</a>*}
                    {*<button type="submit" id="submit" class="btn btn-primary padrl30">{__("Save")}</button>*}
                {*</div>*}
                <div class="col-sm-9 col-sm-offset-3">
                    <a href="#" id="preview_normal" class="btn btn-default text-left js_school-menu-preview">{__("Preview")}</a>
                    <button type="submit" id="preview_excel" class="btn btn-default text-left js_school-menu-preview-excel x-hidden">{__("Preview")}</button>
                    <button type="submit" id="submit" class="btn btn-primary padrl30 text-left menu_submit">{__("Save")}</button>
                </div>
            </div>

            <div class="table-responsive" id="result_info" name="result_info"></div>

            <!-- success -->
            <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
            <!-- success -->
            <!-- error -->
            <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
            <!-- error -->

        </form>
    </div>

{elseif $sub_view == "edit"}
    <div class="panel-body">
        <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_menu.php">
            <input type="hidden" name="school_username" id="school_username" value="{$username}"/>
            <input type="hidden" name="do" value="edit"/>
            <input type="hidden" name="menu_id" value="{$data['menu_id']}"/>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left">{__("Menu name")} (*)</label>
                <div class="col-sm-9">
                    <input class="form-control" name="menu_name" required autofocus maxlength="100" value = "{$data['menu_name']}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left">{__("Scope")} (*)</label>
                <div class="col-sm-3">
                    <select name="level" id="cate_level" class="form-control">
                        <option value="{$smarty.const.SCHOOL_LEVEL}" {if $data['applied_for'] == $smarty.const.SCHOOL_LEVEL} selected {/if}>{__("School")}</option>
                        <option value="{$smarty.const.CLASS_LEVEL_LEVEL}" {if $data['applied_for'] == $smarty.const.CLASS_LEVEL_LEVEL} selected {/if}>{__("Class level")}</option>
                        <option value="{$smarty.const.CLASS_LEVEL}" {if $data['applied_for'] == $smarty.const.CLASS_LEVEL} selected {/if}>{__("Class")}</option>
                    </select>
                </div>
                <div class="col-sm-3 {if $data['applied_for'] != $smarty.const.CLASS_LEVEL_LEVEL}x-hidden{/if}" name="cate_class_level">
                    <select name="class_level_id" id="class_level_id" class="form-control">
                        <option value="0"> {__('Select class level')}</option>
                        {foreach $class_levels as $class_level}
                            <option value="{$class_level['class_level_id']}" {if $data['class_level_id']==$class_level['class_level_id']}selected{/if}>{$class_level['class_level_name']}</option>
                        {/foreach}
                    </select>
                </div>
                <div class="col-sm-3 {if $data['applied_for'] != ($smarty.const.CLASS_LEVEL)}x-hidden{/if}" name="cate_class">
                    <select name="class_id" id="class_id" data-username="{$username}" class="form-control">
                        <option value="0"> {__('Select class')} </option>

                        {$class_level = -1}
                        {foreach $classes as $class}
                            {if ($class_level != $class['class_level_id'])}
                                <option value="" disabled style="color: blue">-----{$class['class_level_name']}-----</option>
                            {/if}
                            <option value="{$class['group_id']}" {if $data['class_id']==$class['group_id']}selected{/if}>{$class['group_title']}</option>
                            {$class_level = $class['class_level_id']}
                        {/foreach}
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left">{__("Begin")} (*)</label>
                <div class='col-sm-3'>
                    <div class='input-group date' id='schedule_beginpicker_new'>
                        <input type='text' name="begin" class="form-control" placeholder="{__("Begin")} (*)" value="{$data['begin']}" required/>
                        <span class="input-group-addon">
                            <span class="fas fa-calendar-alt"></span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left"></label>
                <div class="col-sm-9">
                    <strong> {__('Note: start date must be monday')} </strong>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left">{__("Description")}</label>
                <div class="col-sm-9">
                    <textarea class="form-control js_autosize" name="description" rows="3">{$data['description']}</textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left">{__("Use meal")}?</label>
                <div class="col-sm-1">
                    <div class="onoffswitch">
                        <input type="checkbox" name="use_meal" class="onoffswitch-checkbox" id="use_meal" {if $data['is_meal']}checked{/if}>
                        <label class="onoffswitch-label" for="use_meal"></label>
                    </div>
                </div>
                <label class="col-sm-2 control-label text-left">{__("Study saturday")}?</label>
                <div class="col-sm-1">
                    <div class="onoffswitch">
                        <input type="checkbox" name="study_saturday" class="onoffswitch-checkbox" id="study_saturday" {if $data['is_saturday']}checked{/if}>
                        <label class="onoffswitch-label" for="study_saturday"></label>
                    </div>
                </div>
                <label class="col-sm-3 control-label text-left">{__("Notify immediately")}?</label>
                <div class="col-sm-1">
                    <div class="onoffswitch">
                        <input type="checkbox" name="notify_immediately" class="onoffswitch-checkbox" id="notify_immediately" {if $data['is_notified']}checked{/if}>
                        <label class="onoffswitch-label" for="notify_immediately"></label>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover" id = "myTableMenu">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>
                            {__('Option')}
                        </th>
                        <th>
                            {__('Time')}
                        </th>
                        <th>
                            {__('Meal')}
                        </th>
                        <th>
                            {__('Monday')}
                        </th>
                        <th>
                            {__('Tuesday')}
                        </th>
                        <th>
                            {__('Wednesday')}
                        </th>
                        <th>
                            {__('Thursday')}
                        </th>
                        <th>
                            {__('Friday')}
                        </th>
                        <th>
                            {__('Saturday')}
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    {$idx = 1}
                    {foreach $data['details'] as $detail}
                        <tr>
                            <td align="center" class = "col_no">
                                {$idx}
                                <input type="hidden" name="menu_detail_id[]" value="{$detail['menu_detail_id']}"/>
                            </td>
                            <td align="center">
                                <a class = "btn btn-default js_cate-delete_null"> {__("Delete")} </a>
                            </td>
                            <td>
                                <input type="text" name="start[]" value="{$detail['meal_time']}" class="form-control" style = "min-width: 200px;"/>
                            </td>
                            <td>
                                <input type = "text" name="meal[]" value="{$detail['meal_name']}" class = "form-control" style = "min-width: 200px;"/>
                            </td>
                            <td>
                                <textarea type = "text" class = "text-left form-control note" name = "meal_detail_mon[]">{$detail['monday']}</textarea>
                            </td>
                            <td>
                                <textarea type = "text" class = "text-left form-control note" name = "meal_detail_tue[]">{$detail['tuesday']}</textarea>
                            </td>
                            <td>
                                <textarea type = "text" class = "text-left form-control note" name = "meal_detail_wed[]">{$detail['wednesday']}</textarea>
                            </td>
                            <td>
                                <textarea type = "text" class = "text-left form-control note" name = "meal_detail_thu[]">{$detail['thursday']}</textarea>
                            </td>
                            <td>
                                <textarea type = "text" class = "text-left form-control note" name = "meal_detail_fri[]">{$detail['friday']}</textarea>
                            </td>
                            <td>
                                <textarea type = "text" class = "text-left form-control note" name = "meal_detail_sat[]">{$detail['saturday']}</textarea>
                            </td>
                        </tr>
                        {$idx = $idx + 1}
                    {/foreach}
                    </tbody>
                </table>
                <div class = "form-group">
                    <div class = "col-sm-12">
                        <a class="btn btn-default js_cate-add-menu">+</a>
                    </div>
                </div>
            </div>
            <div class = "form-group">
                <div class="col-sm-12 mt20">
                    {*<a class="btn btn-default js_cate-add-menu">{__("Add new row")}</a>*}
                    <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                </div>
            </div>
            <!-- success -->
            <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
            <!-- success -->
            <!-- error -->
            <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
            <!-- error -->
        </form>
    </div>
{elseif $sub_view == "copy"}
    <div class="panel-body">
        <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_menu.php">
            <input type="hidden" name="school_username" id="school_username" value="{$username}"/>
            <input type="hidden" name="do" value="copy"/>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left">{__("Menu name")} (*)</label>
                <div class="col-sm-9">
                    <input class="form-control" name="menu_name" required autofocus maxlength="100" value = "{$data['menu_name']}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left">{__("Scope")} (*)</label>
                <div class="col-sm-3">
                    <select name="level" id="cate_level" class="form-control">
                        <option value="{$smarty.const.SCHOOL_LEVEL}" {if $data['applied_for'] == $smarty.const.SCHOOL_LEVEL} selected {/if}>{__("School")}</option>
                        <option value="{$smarty.const.CLASS_LEVEL_LEVEL}" {if $data['applied_for'] == $smarty.const.CLASS_LEVEL_LEVEL} selected {/if}>{__("Class level")}</option>
                        <option value="{$smarty.const.CLASS_LEVEL}" {if $data['applied_for'] == $smarty.const.CLASS_LEVEL} selected {/if}>{__("Class")}</option>
                    </select>
                </div>
                <div class="col-sm-3 {if $data['applied_for'] != $smarty.const.CLASS_LEVEL_LEVEL}x-hidden{/if}" name="cate_class_level">
                    <select name="class_level_id" id="class_level_id" class="form-control">
                        <option value="0"> {__('Select class level')}</option>
                        {foreach $class_levels as $class_level}
                            <option value="{$class_level['class_level_id']}" {if $data['class_level_id']==$class_level['class_level_id']}selected{/if}>{$class_level['class_level_name']}</option>
                        {/foreach}
                    </select>
                </div>
                <div class="col-sm-3 {if $data['applied_for'] != ($smarty.const.CLASS_LEVEL)}x-hidden{/if}" name="cate_class">
                    <select name="class_id" id="class_id" data-username="{$username}" class="form-control">
                        <option value="0"> {__('Select class')} </option>

                        {$class_level = -1}
                        {foreach $classes as $class}
                            {if ($class_level != $class['class_level_id'])}
                                <option value="" disabled style="color: blue">-----{$class['class_level_name']}-----</option>
                            {/if}
                            <option value="{$class['group_id']}" {if $data['class_id']==$class['group_id']}selected{/if}>{$class['group_title']}</option>
                            {$class_level = $class['class_level_id']}
                        {/foreach}
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left">{__("Begin")} (*)</label>
                <div class='col-sm-3'>
                    <div class='input-group date' id='schedule_beginpicker_new'>
                        <input type='text' name="begin" class="form-control" placeholder="{__("Begin")} (*)" value="{$data['begin']}" required/>
                        <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left"></label>
                <div class="col-sm-9">
                    <strong> {__('Note: start date must be monday')} </strong>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left">{__("Use meal")}?</label>
                <div class="col-sm-9">
                    <div class="onoffswitch">
                        <input type="checkbox" name="use_meal" class="onoffswitch-checkbox" id="use_meal" {if $data['is_meal']}checked{/if}>
                        <label class="onoffswitch-label" for="use_meal"></label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left">{__("Study saturday")}?</label>
                <div class="col-sm-9">
                    <div class="onoffswitch">
                        <input type="checkbox" name="study_saturday" class="onoffswitch-checkbox" id="study_saturday" {if $data['is_saturday']}checked{/if}>
                        <label class="onoffswitch-label" for="study_saturday"></label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left">{__("Notify immediately")}?</label>
                <div class="col-sm-9">
                    <div class="onoffswitch">
                        <input type="checkbox" name="notify_immediately" class="onoffswitch-checkbox" id="notify_immediately" checked>
                        <label class="onoffswitch-label" for="notify_immediately"></label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left">{__("Description")} (*)</label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="description" rows="6">{$data['description']}</textarea>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover" id = "myTableMenu">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>
                            {__('Option')}
                        </th>
                        <th>
                            {__('Time')}
                        </th>
                        <th>
                            {__('Meal')}
                        </th>
                        <th>
                            {__('Monday')}
                        </th>
                        <th>
                            {__('Tuesday')}
                        </th>
                        <th>
                            {__('Wednesday')}
                        </th>
                        <th>
                            {__('Thursday')}
                        </th>
                        <th>
                            {__('Friday')}
                        </th>
                        <th>
                            {__('Saturday')}
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    {$idx = 1}
                    {foreach $data['details'] as $detail}
                        <tr>
                            <td align="center" class = "col_no">
                                {$idx}
                                <input type="hidden" name="menu_detail_id[]" value="{$detail['menu_detail_id']}"/>
                            </td>
                            <td align="center">
                                <a class = "btn btn-default js_cate-delete_null"> {__('Delete')} </a>
                            </td>
                            <td>
                                <input type="text" name="start[]" class="form-control" value = "{$detail['meal_time']}" style = "min-width: 200px;">
                            </td>
                            <td>
                                <input type = "text" name="meal[]" value="{$detail['meal_name']}" class = "form-control" style = "min-width: 200px;"/>
                            </td>
                            <td>
                                <textarea type = "text" class = "text-left form-control note" name = "meal_detail_mon[]">{$detail['monday']}</textarea>
                            </td>
                            <td>
                                <textarea type = "text" class = "text-left form-control note" name = "meal_detail_tue[]">{$detail['tuesday']}</textarea>
                            </td>
                            <td>
                                <textarea type = "text" class = "text-left form-control note" name = "meal_detail_wed[]">{$detail['wednesday']}</textarea>
                            </td>
                            <td>
                                <textarea type = "text" class = "text-left form-control note" name = "meal_detail_thu[]">{$detail['thursday']}</textarea>
                            </td>
                            <td>
                                <textarea type = "text" class = "text-left form-control note" name = "meal_detail_fri[]">{$detail['friday']}</textarea>
                            </td>
                            <td>
                                <textarea type = "text" class = "text-left form-control note" name = "meal_detail_sat[]">{$detail['saturday']}</textarea>
                            </td>
                        </tr>
                        {$idx =$idx + 1}
                    {/foreach}
                    </tbody>
                </table>
            </div>
            <div class = "form-group">
                <div class="col-sm-12 mt20">
                    <a class="btn btn-default js_cate-add-menu">{__("Add new row")}</a>
                    <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                </div>
            </div>
            <!-- success -->
            <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
            <!-- success -->
            <!-- error -->
            <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
            <!-- error -->
        </form>
    </div>
{/if}
</div>