<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <i class="fa fa-book fa-fw fa-lg pr10"></i>
        {__("Subject")}
        {if $sub_view == ""}
            &rsaquo; {__('Lists')}
        {elseif $sub_view == "edit"}
            &rsaquo; {$data['subject_name']}
        {/if}
    </div>
    {if $sub_view == ""}
        <div class="panel-body with-table">
            <div class="mb5"><strong>{__("subject list")}&nbsp;({$rows|count} {__("Subject")})</strong></div>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th width="5%">#</th>
                            <th width="20%">{__("Subject")}</th>
                            <th width="60%">{__("Class level")}</th>
                            <th width="15%">{__("Actions")}</th>
                        </tr>
                    </thead>
                    <tbody>
                        {$idx = 1}
                        {foreach $rows as $row}
                            <tr>
                                <td class="align-middle" align="center">{$idx}</td>
                                <td class="align-middle">{$row['subject_name']}</td>
                                <td class="align-middle">
                                    {if count($row['class_levels']) == 0}
                                        {__("No class level")}
                                    {else}
                                        {foreach $row['class_levels'] as $key => $class_level}
                                            <span class="name js_user-popover" data-uid="{$class_level['class_level_id']}">
                                                <span >{$class_level['class_level_name']} </span>
                                            </span>
                                            {if $key!= (count($row['class_levels'])-1)}
                                                <span>,</span>
                                            {/if}
                                        {/foreach}
                                    {/if}
                                </td class="align-middle">
                                <td class="align-middle">
                                    {if $canEdit}
                                        <a href="{$system['system_url']}/school/{$username}/subjects/edit/{$row['subject_id']}" class="btn btn-xs btn-default">{__("Edit")}</a>
                                    {/if}
                                </td>
                            </tr>
                            {$idx = $idx + 1}
                        {/foreach}

                        {if $rows|count == 0}
                            <tr class="odd">
                                <td valign="top" align="center" colspan="6" class="dataTables_empty">
                                    {__("No data available in table")}
                                </td>
                            </tr>
                        {/if}
                    </tbody>
                </table>
            </div>
        </div>
    {elseif $sub_view == "edit"}
        <div class="panel-body">

            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_subject.php">
                <input type="hidden" name="school_id" id="school_id" value="{$school_id}"/>
                <input type="hidden" name="school_username" id="school_username" value="{$username}"/>
                <input type="hidden" name="subject_id" value="{$data['subject_id']}"/>
                <input type="hidden" name="do" value="edit"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">
                        {__("Class level")}
                    </label>
                    <div class="col-sm-9">
{*                        <input name="search-teacher" id="search-teacher" type="text" class="form-control" placeholder="{__("Enter class level name to search")}" autocomplete="off">*}
                        <input name="search-classlevel" id="search-classlevel" type="text" class="form-control" placeholder="{__("Enter class level name to search")}" autocomplete="off">
                        <div id="search-classlevel-results" class="dropdown-menu dropdown-widget dropdown-search">
                            <div class="dropdown-widget-header">
                                {__("Search Results")}
                            </div>
                            <div class="dropdown-widget-body">
                                <div class="loader loader_small ptb10"></div>
                            </div>
                        </div>
                        <br/>
                        <div class="col-sm-9" id="class_level_list" name="class_level_list">
{*                            {if count($teachers) > 0}*}
                            {if count($class_levels_assigned) > 0}
{*                                {include file='ci/ajax.teacherlist.tpl' results=$teachers}*}
                                {include file='ci/ajax.classlevellist.tpl' results=$class_levels_assigned}
                            {else}
                                {__("No class level")}
                            {/if}
                        </div>
                    </div>
                </div>
{*                <div class="form-group">*}
{*                    <div class="col-sm-offset-3">*}
{*                        <a style="margin-left: 10px" class="btn btn-xs btn-default js_school-teacher-add"><i class="fa fa-plus"></i> {__("Add new teacher")}</a>*}
{*                    </div>*}
{*                </div>*}
                {*<div class="form-group">*}
                    {*<label class="col-sm-3 control-label text-left">*}
                        {*{__("Telephone")}*}
                    {*</label>*}
                    {*<div class="col-sm-9">*}
                        {*<input class="form-control" id="telephone" name="telephone" value="{$data['telephone']}" placeholder="{__("Telephone")}" maxlength="50">*}
                    {*</div>*}
                {*</div>*}
                {*<div class="form-group">*}
                    {*<label class="col-sm-3 control-label text-left">*}
                        {*{__("Email")}*}
                    {*</label>*}
                    {*<div class="col-sm-9">*}
                        {*<input class="form-control" id="email" name="email" value="{$data['email']}" placeholder="{__("Email")}" maxlength="50">*}
                    {*</div>*}
                {*</div>*}
                {*<div class="form-group">*}
                    {*<label class="col-sm-3 control-label text-left">*}
                        {*{__("Camera URL")}*}
                    {*</label>*}
                    {*<div class="col-sm-9">*}
                        {*<input class="form-control" name="camera_url" value="{$data['camera_url']}" placeholder="{__("Camera URL")}" maxlength="300">*}
                    {*</div>*}
                {*</div>*}
{*                <div class="form-group">*}
{*                    <label class="col-sm-3 control-label text-left">*}
{*                        {__("Description")}*}
{*                    </label>*}
{*                    <div class="col-sm-9">*}
{*                        <textarea class="form-control" name="description" id="description" placeholder="{__("Write about your class...")}" rows="3">{$data['group_description']}</textarea>*}
{*                    </div>*}
{*                </div>*}

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30 js_show-loading">{__("Save")}</button>
                        <a href="{$system['system_url']}/school/{$username}/subjects" class="btn btn-default">{__("Lists")}</a>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->
                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    {/if}
</div>