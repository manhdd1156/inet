<div align="center" class="mt10">
    <strong>{__("List registered countbase service of")} {$classTitle} {__("day")} {$date_pos}</strong>
</div>
<div class="panel-body with-table">
    <table class="table table-striped table-bordered table-hover">
        <tbody>
        <tr>
            <td align="center" class="align_middle">
                <strong>#</strong>
            </td>
            <td align="center" class="align_middle">
                <strong>{__("Children list")}</strong>
            </td>
            {foreach $servicesCB as $service}
                <td align="center">
                    <strong>{$service['service_name']}</strong>
                </td>
            {/foreach}
        </tr>
        {$idx = 1}
        {foreach $childList as $child}
            <tr>
                <td align="center" style="vertical-align: middle"><strong>{$idx}</strong></td>
                <td><strong>{$child['child_name']}</strong></td>
                {foreach $servicesCB as $service}
                    <td align="center" style="vertical-align: middle">
                        {foreach $child['services']['service'] as $row}
                            {if $row['service_id'] == $service['service_id']}
                                {if $row['using_at'] != null}
                                    <i class="fa fa-check" aria-hidden="true" style="color: #26b31a"></i>
                                {else}
                                    <i class="fa fa-times" aria-hidden="true"></i>
                                {/if}
                            {/if}
                        {/foreach}
                    </td>
                {/foreach}
            </tr>
            {$idx = $idx + 1}
        {/foreach}
        </tbody>
    </table>
</div>