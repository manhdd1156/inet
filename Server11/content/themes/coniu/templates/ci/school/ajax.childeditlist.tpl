<strong>{__("Children list")}&nbsp;({$results|count} {__("Children")})</strong>
<table class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th>{__("#")}</th>
        <th>{__("Full name")}</th>
        <th>{__("Last update")}</th>
        <th>{__("Edit by")}</th>
    </tr>
    </thead>
    <tbody>
        {if count($results) > 0}
            {assign var="classId" value="-1" nocache}
            {$idx = 1}
            {foreach $results as $row}
                {if ($classId != $row['class_id'])}
                    <tr>
                        <td colspan="4">
                            {if $row['class_id'] > 0}
                                {foreach $classes as $class}
                                    {if $class['group_id'] == $row['class_id']}
                                        {__("Class")}:&nbsp;{$class['group_title']}
                                    {/if}
                                {/foreach}
                            {else}
                                {__("No class")}
                            {/if}
                        </td>
                    </tr>
                {/if}
                <tr>
                    <td align="center" style ="vertical-align: middle">{$idx}</td>
                    {*<td>{$row['child_code']}</td>*}
                    <td style ="vertical-align: middle" align="center"><a href="{$system['system_url']}/school/{$username}/children/detail/{$row['child_id']}">{$row['child_name']}</a></td>
                    <td style ="vertical-align: middle" align="center">{$row['last_update']}</td>
                    <td style ="vertical-align: middle" align="center">{$row['user_fullname']}</td>
                </tr>
                {assign var="classId" value=$row['class_id'] nocache}
                {$idx = $idx + 1}
            {/foreach}
        {else}
            <tr>
                <td colspan="4">{__("No student was edited by teacher in this time")}</td>
            </tr>
        {/if}
    </tbody>
</table>