<strong>{__("Children list")}&nbsp;({$result['total']} {__("Children")})</strong>
<div class="pull-right flip mb10">
    <a href="#" id="export2excel" class="btn btn-success js_school-export-children" data-username="{$username}">{__("Export to Excel")}</a>
    <label id="export_processing" class="btn btn-info x-hidden">{__("Loading")}...</label>
</div>
<table class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th>{__("#")}</th>
            <th>{__("Full name")}</th>
            <th>{__("Gender")}</th>
            <th>{__("Birthdate")}</th>
            <th>{__("Parent phone")}</th>
            <th>{__("Parent")}</th>
            <th>{__("Actions")}</th>
        </tr>
    </thead>
    <tbody>
    {$classId = -1}
    {$idx = ($result['page'] - 1) * $smarty.const.PAGING_LIMIT + 1}
    {foreach $result['children'] as $row}
        {if (!isset($result['class_id']) || ($result['class_id']=="")) && ($classId != $row['class_id'])}
            <tr>
                <td colspan="7">
                    {if $row['class_id'] > 0}
                        {__("Class")}:&nbsp;{$row['group_title']}
                    {else}
                        {__("No class")}
                    {/if}
                </td>
            </tr>
        {/if}
        <tr>
            <td class="align-middle" align="center">{$idx}</td>
            <td class="align-middle"><a href="{$system['system_url']}/school/{$username}/children/detail/{$row['child_id']}">{$row['child_name']}</a></td>
            <td class="align-middle" align="center">{if $row['gender'] == $smarty.const.MALE}{__("Male")}{else}{__("Female")}{/if}</td>
            <td class="align-middle">{$row['birthday']}</td>
            <td class="align-middle">
                {if $row['parent_phone'] != ''}
                    {$row['parent_phone']}
                {else}
                    {$row['parent_phone_dad']}
                {/if}
            </td>
            <td class="align-middle">
                {if count($row['parent']) == 0}
                    {__("No parent")}
                {else}
                    {foreach $row['parent'] as $_user}
                        <span class="name js_user-popover" data-uid="{$_user['user_id']}">
                            <a href="{$system['system_url']}/{$_user['user_name']}">{$_user['user_fullname']}</a>
                        </span>
                        {if $_user['user_id'] != $user->_data['user_id']}
                            <a href="#" class="far fa-comments fa-lg js_chat-start" data-name="{$_user['user_fullname']}" data-uid="{$_user['user_id']}"></a>
                        {/if}
                        {if $_user['suggest'] == 1 && $canEdit}
                            <button class="btn btn-xs btn-info js_school-approve-parent" id="button_{$row['child_id']}_{$_user['user_id']}" data-parent="{$_user['user_id']}" data-username="{$username}" data-id="{$row['child_id']}">{__("Approve")}</button>
                        {/if}
                        <br/>
                    {/foreach}
                {/if}
            </td>

            <td class="align-middle action_col" align="left">
                {if $canEdit}
                    <a href="{$system['system_url']}/school/{$username}/children/edit/{$row['child_id']}" class="btn btn-xs btn-default edit_width">{__("Edit")}</a>
                    {if $school['school_step'] == SCHOOL_STEP_FINISH  || $school['grade'] != 0}
                        <a href="{$system['system_url']}/school/{$username}/children/moveclass/{$row['child_id']}" class="btn btn-xs btn-default moveclass_width">{__("Move class")}</a>
                        <a href="{$system['system_url']}/school/{$username}/children/leave/{$row['child_id']}" class="btn btn-xs btn-warning">{__("Leave school")}</a>
                    {/if}
                    {if count($row['parent']) == 0}
                        <button class="btn btn-xs btn-danger js_school-delete" data-handle="child" data-username="{$username}" data-id="{$row['child_id']}">{__("Delete")}</button>
                    {/if}
                {/if}
            </td>
        </tr>
        {$classId = $row['class_id']}
        {$idx = $idx + 1}
    {/foreach}

    {if $result['children']|count == 0}
        <tr class="odd">
            <td valign="top" align="center" colspan="7" class="dataTables_empty">
                {__("No data available in table")}
            </td>
        </tr>
    {/if}
    {if $result['page_count'] > 1}
        <tr>
            <td colspan="7">
                <div class="pull-right flip">
                    <ul class="pagination">
                        {for $idx = 1 to $result['page_count']}
                            {if $idx == $result['page']}
                                <li class="active"><a href="#">{$idx}</a></li>
                            {else}
                                <li>
                                    <a href="#" class="js_child-search" data-username="{$username}" data-id="{$school['page_id']}" data-page="{$idx}">{$idx}</a>
                                </li>
                            {/if}
                        {/for}
                    </ul>
                </div>
            </td>
        </tr>
    {/if}
    </tbody>
</table>
