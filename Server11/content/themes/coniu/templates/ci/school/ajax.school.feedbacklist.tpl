<div><strong>{__("Feedback list")}&nbsp;(<span class="count_feedback">{$rows|count}</span>)</strong></div>
<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover">
        <thead>
        <tr>
            <th>#</th>
            <th>{__("Title")}</th>
            <th>{__("Time")}</th>
            <th>{__("Sender's name")}</th>
            <th>{__("Parent of student")}</th>
            <th>{__("Email")}</th>
            <th>{__("Class")}</th>
            <th>{__("Actions")}</th>
        </tr>
        </thead>
        <tbody>
        {$idx = 1}
        {foreach $rows as $row}
            {if $idx > 1}
                <tr><td colspan="8"></td></tr>
            {/if}
            <tr class="list_feedback_{$row['feedback_id']}">
                <td rowspan="2" align="center" style="vertical-align:middle"><strong>{$idx}</td>
                <td>
                    {if $row['level']==$smarty.const.SCHOOL_LEVEL}
                        {__("Feedback for the school")}
                    {elseif $row['level']==$smarty.const.CLASS_LEVEL}
                        {__("Feedback for class")}
                    {/if}
                </td>
                <td align="center" style="vertical-align:middle">{$row['created_at']}</td>
                {if $row['is_incognito']}
                    <td colspan="3" align="center" style="vertical-align:middle">{__("Incognito")}</td>
                {else}
                    <td align="center" style="vertical-align:middle"> {$row['name']}
                    </td>
                    <td align="center" style="vertical-align:middle">{$row['child_name']}</td>
                    <td align="center" style="vertical-align:middle">{$row['email']}</td>
                {/if}
                <td align="center" style="vertical-align:middle">
                    {if !is_empty($row['group_title'])}
                        {$row['group_title']}
                    {else}
                        -
                    {/if}

                </td>
                <td align="center" style="vertical-align:middle" rowspan="2">
                    {if $canEdit}
                        {if !$row['confirm']}
                            {if $row['level']==$smarty.const.SCHOOL_LEVEL}
                                <button class="btn btn-xs btn-primary js_school-feedback" data-handle="confirm" data-username="{$username}"
                                        data-id="{$row['feedback_id']}">{__("Confirm")}</button>
                            {/if}
                        {/if}
                        <div class="feddback_confirmed_{$row['feedback_id']} {if !$row['confirm']}x-hidden{/if}">
                            <strong>{__("Confirmed")}</strong>
                            {if $row['level']==$smarty.const.SCHOOL_LEVEL}
                                <button class="btn btn-xs btn-danger js_school-feedback" data-handle="delete" data-username="{$username}"
                                        data-id="{$row['feedback_id']}">{__("Delete")}</button>
                            {/if}
                        </div>
                    {/if}
                </td>
            </tr>
            <tr class="list_feedback_{$row['feedback_id']}">
                <td colspan="2" align="center" style="vertical-align:middle"><strong>{__("Content")}</strong></td>
                <td colspan="4" style="vertical-align:middle"> {$row['content']}</td>
            </tr>
            {$idx = $idx + 1}
        {/foreach}

        {if $rows|count == 0}
            <tr class="odd">
                <td valign="top" align="center" colspan="8" class="dataTables_empty">
                    {__("No data available in table")}
                </td>
            </tr>
        {/if}
        </tbody>
    </table>
</div>