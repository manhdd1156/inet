<label class = "col-sm-3 control-label text-left">{__("Select student")}</label>
<div class="col-sm-9">
    {$idx = 1}
    {foreach $children as $child}
        <label class="col-xs-12 col-sm-6">
            <input type="checkbox" name="child_ids[]" value="{$child['child_id']}" {if $child['checked']}checked{/if}>{$idx} - {$child['child_name']} - {$child['birthday']}
        </label>
        {$idx = $idx + 1}
    {/foreach}
</div>
<input type="hidden" id="remanding_child_ids" name="remanding_child_ids" value="{$remanding_child_ids}">

