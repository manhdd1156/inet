<tr>
    <td class="text-center">
        +
        <input type="hidden" name="pickup_{$child_id}" value="1"/>
        <input type="hidden" class="service_id_{$child_id}" name="service_id_{$child_id}[]" value="0"/>
        <input type="hidden" name="service_type_{$child_id}[]" value="0"/>
        <input type="hidden" id="service_quantity_{$child_id}_0" name="service_quantity_{$child_id}[]" value="1"/>
        <input type="hidden" id="service_quantity_deduction_{$child_id}_0" name="service_quantity_deduction_{$child_id}[]" value="0"/>
        <input type="hidden" id="service_unit_price_{$child_id}_0" name="service_unit_price_{$child_id}[]" value="0"/>
        <input type="hidden" id="service_unit_price_deduction_{$child_id}_0" name="service_unit_price_deduction_{$child_id}[]" value="0"/>
    </td>
    <td colspan="4">{__('Late PickUp')}</td>
    <td><input style="width: 100px;" type="text" min="0" step="1" class="text-right tuition_pick_up money_tui" id="service_amount_{$child_id}_0" name="service_amount_{$child_id}[]" child_id="{$child_id}" value="0"/></td>
    <td align="center"><button class="btn-danger js_del_tuition_item" child_id="{$child_id}">{__("Delete")}</button></td>
</tr>