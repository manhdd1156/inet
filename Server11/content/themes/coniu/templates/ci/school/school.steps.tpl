{if $view != ""}
    <div class="col-sm-9">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">
                    <div class="col-xs-2">
                        <div class="top_direct" align="center">
                            {if $view == ""}
                            {elseif $view == "classlevels"}
                                <a href="{$system['system_url']}/school/{$username}" class="btn btn-default btn-xs"><i class="fas fa-chevron-left"></i> {__("Back")}</a>

                            {elseif $view == "classes"}
                                <a href="{$system['system_url']}/school/{$username}/classlevels" class="btn btn-default btn-xs"><i class="fas fa-chevron-left"></i> {__("Back")}</a>

                            {elseif $view == "teachers"}
                                {if count($classesStep) > 0}
                                    <a href="{$system['system_url']}/school/{$username}/classes" class="btn btn-default btn-xs"><i class="fas fa-chevron-left"></i> {__("Back")}</a>
                                {else}
                                    <a href="{$system['system_url']}/school/{$username}/classes/add" class="btn btn-default btn-xs"><i class="fas fa-chevron-left"></i> {__("Back")}</a>
                                {/if}

                            {elseif $view == "services"}
                                {if count($teachersStep > 0)}
                                    <a href="{$system['system_url']}/school/{$username}/teachers" class="btn btn-default btn-xs"><i class="fas fa-chevron-left"></i> {__("Back")}</a>
                                {else}
                                    <a href="{$system['system_url']}/school/{$username}/teachers/add" class="btn btn-default btn-xs"><i class="fas fa-chevron-left"></i> {__("Back")}</a>
                                {/if}

                            {elseif $view == "fees"}
                                {if count($servicesStep) > 0}
                                    <a href="{$system['system_url']}/school/{$username}/services" class="btn btn-default btn-xs"><i class="fas fa-chevron-left"></i> {__("Back")}</a>
                                {else}
                                    <a href="{$system['system_url']}/school/{$username}/services/add" class="btn btn-default btn-xs"><i class="fas fa-chevron-left"></i> {__("Back")}</a>
                                {/if}

                            {elseif $view == "pickup" && $sub_view == "template"}
                                {if count($feesStep) > 0}
                                    <a href="{$system['system_url']}/school/{$username}/fees" class="btn btn-default btn-xs"><i class="fas fa-chevron-left"></i> {__("Back")}</a>
                                {else}
                                    <a href="{$system['system_url']}/school/{$username}/fees/add" class="btn btn-default btn-xs"><i class="fas fa-chevron-left"></i> {__("Back")}</a>
                                {/if}

                            {elseif $view == "pickup" && $sub_view == "assign"}
                                <a href="{$system['system_url']}/school/{$username}/pickup/template" class="btn btn-default btn-xs"><i class="fas fa-chevron-left"></i> {__("Back")}</a>

                            {elseif $view == "children"}
                                <a href="{$system['system_url']}/school/{$username}/pickup/assign" class="btn btn-default btn-xs"><i class="fas fa-chevron-left"></i> {__("Back")}</a>
                            {/if}
                        </div>
                    </div>
                    <div class="col-xs-8">
                        <div align="center"><h4 style="font-size: 20px">{__("Trình tự khởi tạo trường")}</h4></div>
                    </div>
                    <div class="col-xs-2">
                        <div class="top_direct" align="center">
                            {if $view == ""}
                            {elseif $view == "classlevels"}
                                {if count($rows) > 0 && count($classesStep) > 0}
                                    <a href="#" class="btn btn-success btn-xs js_school-step" data-step="1" data-username="{$username}" data-view="classes" data-subview="">{__("Next")} <i class="fas fa-chevron-right"></i></a>
                                {elseif count($rows) > 0 && count($classesStep) == 0}
                                    <a href="#" class="btn btn-success btn-xs js_school-step" data-step="1" data-username="{$username}" data-view="classes" data-subview="add">{__("Next")} <i class="fas fa-chevron-right"></i></a>
                                {/if}

                            {elseif $view == "classes"}
                                {if count($teachersStep) > 0}
                                    <a href="#" class="btn btn-success btn-xs js_school-step" data-step="2" data-username="{$username}" data-view="teachers" data-subview="">{__("Next")} <i class="fas fa-chevron-right"></i></a>
                                {else}
                                    <a href="#" class="btn btn-success btn-xs js_school-step" data-step="2" data-username="{$username}" data-view="teachers" data-subview="add">{__("Next")} <i class="fas fa-chevron-right"></i></a>
                                {/if}

                            {elseif $view == "teachers"}
                                {if count($servicesStep) > 0}
                                    <a href="#" class="btn btn-success btn-xs js_school-step" data-step="3" data-username="{$username}" data-view="services" data-subview="">{__("Next")} <i class="fas fa-chevron-right"></i></a>
                                {else}
                                    <a href="#" class="btn btn-success btn-xs js_school-step" data-step="3" data-username="{$username}" data-view="services" data-subview="add">{__("Next")} <i class="fas fa-chevron-right"></i></a>
                                {/if}

                            {elseif $view == "services"}
                                {if count($feesStep) > 0}
                                    <a href="#" class="btn btn-success btn-xs js_school-step" data-step="4" data-username="{$username}" data-view="fees" data-subview="">{__("Next")} <i class="fas fa-chevron-right"></i></a>
                                {else}
                                    <a href="#" class="btn btn-success btn-xs js_school-step" data-step="4" data-username="{$username}" data-view="fees" data-subview="add">{__("Next")} <i class="fas fa-chevron-right"></i></a>
                                {/if}

                            {elseif $view == "fees"}
                                <a href="#" class="btn btn-success btn-xs js_school-step" data-step="5" data-username="{$username}" data-view="pickup" data-subview="template">{__("Next")} <i class="fas fa-chevron-right"></i></a>

                            {elseif $view == "pickup" && $sub_view == "template"}
                                <a href="#" class="btn btn-success btn-xs js_school-step" data-step="6" data-username="{$username}" data-view="pickup" data-subview="assign">{__("Next")} <i class="fas fa-chevron-right"></i></a>

                            {elseif $view == "pickup" && $sub_view == "assign"}
                                <a href="#" class="btn btn-success btn-xs js_school-step" data-step="7" data-username="{$username}" data-view="children" data-subview="">{__("Next")} <i class="fas fa-chevron-right"></i></a>

                            {elseif $view == "children"}
                                <a href="#" class="btn btn-success btn-xs js_school-step" data-step="100" data-username="{$username}" data-view="" data-subview="">{__("Finish")} <i class="fas fa-chevron-right"></i></a>
                            {/if}
                        </div>
                    </div>
                </div>
                <div class="step_box" align="center">
                    <div class="main_step">
                        <a href="{$system['system_url']}/school/{$username}/classlevels" {if $view == "classlevels"}class="color_grey btn-success btn btn-xs" {else}class="btn-success btn btn-xs"{/if}>1. {__("Class level")}</a>
                        <i class="fas fa-hand-point-right"></i>
                        {if count($classlevelsStep) > 0}
                            {if $school['school_step'] >= 1}
                                {if count($classesStep) > 0}
                                    <a href="{$system['system_url']}/school/{$username}/classes" {if $view == "classes"}class="color_grey btn-success btn btn-xs" {else}class="btn-success btn btn-xs"{/if}>2. {__("Class")}</a>
                                {else}
                                    <a href="{$system['system_url']}/school/{$username}/classes/add" {if $view == "classes"}class="color_grey btn-success btn btn-xs" {else}class="btn-success btn btn-xs"{/if}>2. {__("Class")}</a>
                                {/if}
                            {else}
                                <button class="btn btn-default btn-xs">2. {__("Class")}</button>
                            {/if}

                            <i class="fas fa-hand-point-right"></i>
                            {if $school['school_step'] >= 2}
                                {if count($teachersStep) > 0}
                                    <a href="{$system['system_url']}/school/{$username}/teachers" {if $view == "teachers"}class="color_grey btn-success btn btn-xs" {else}class="btn-success btn btn-xs"{/if}>3. {__("Teacher")}</a>
                                {else}
                                    <a href="{$system['system_url']}/school/{$username}/teachers/add" {if $view == "teachers"}class="color_grey btn btn-success btn-xs" {else}class="btn-success btn btn-xs"{/if}>3. {__("Teacher")}</a>
                                {/if}
                            {else}
                                <button class="btn btn-default btn-xs">3. {__("Teacher")}</button>
                            {/if}
                            <i class="fas fa-hand-point-right"></i>
                            {if $school['school_step'] >= 3}
                                {if count($servicesStep) > 0}
                                    <a href="{$system['system_url']}/school/{$username}/services" {if $view == "fees" || $view == "services"}class="color_grey btn btn-success btn-xs" {else}class="btn-success btn btn-xs"{/if}>4. {__("Tuition settings")}</a>
                                {else}
                                    <a href="{$system['system_url']}/school/{$username}/services/add" {if $view == "services" || $view == "fees"}class="color_grey btn btn-success btn-xs" {else}class="btn-success btn btn-xs"{/if}>4. {__("Tuition settings")}</a>
                                {/if}
                            {else}
                                <button class="btn btn-default btn-xs">4. {__("Tuition settings")}</button>
                            {/if}
                            <i class="fas fa-hand-point-right"></i>
                            {if $school['school_step'] >= 5}
                                <a href="{$system['system_url']}/school/{$username}/pickup/template" {if $view == "pickup" && ($sub_view == "template" || $sub_view == "assign")}class="color_grey btn btn-success btn-xs" {else}class="btn-success btn btn-xs"{/if}>5. {__("Pickup settings")}</a>
                            {else}
                                <button class="btn btn-default btn-xs">5. {__("Pickup settings")}</button>
                            {/if}
                            <i class="fas fa-hand-point-right"></i>
                            {if $school['school_step'] >= 7}
                                <a href="{$system['system_url']}/school/{$username}/children" {if $view == "children"}class="color_grey btn btn-success btn-xs" {else}class="btn-success btn btn-xs"{/if}>6. {__("Child")}</a>
                            {else}
                                <button class="btn btn-default btn-xs">6. {__("Child")}</button>
                            {/if}

                        {else}
                            <button class="btn btn-default btn-xs">2. {__("Class")}</button>
                            <i class="fas fa-hand-point-right"></i>
                            <button class="btn btn-default btn-xs">3. {__("Teacher")}</button>
                            <i class="fas fa-hand-point-right"></i>
                            <button class="btn btn-default btn-xs">4. {__("Tuition settings")}</button>
                            <i class="fas fa-hand-point-right"></i>
                            <button class="btn btn-default btn-xs">5. {__("Pickup settings")}</button>
                            <i class="fas fa-hand-point-right"></i>
                            <button class="btn btn-default btn-xs">6. {__("Child")}</button>
                        {/if}
                    </div>
                    <div class="sub_step">
                        {if $view == "fees" || $view == "services"}
                            {if $school['school_step'] >= 3}
                                {if count($servicesStep) > 0}
                                    <a href="{$system['system_url']}/school/{$username}/services" {if $view == "services"}class="color_sub" {else}class=""{/if}>4.1. {__("Add new service")}</a>
                                {else}
                                    <a href="{$system['system_url']}/school/{$username}/services/add" {if $view == "services"}class="color_sub" {else}class=""{/if}>4.1. {__("Add new service")}</a>
                                {/if}
                                <i class="fas fa-hand-point-right"></i>
                                {if $school['school_step'] >= 4}
                                    {if count($feesStep) > 0}
                                        <a href="{$system['system_url']}/school/{$username}/fees" {if $view == "fees"}class="color_sub" {else}class=""{/if}>4.2. {__("Add New Fee")}</a>
                                    {else}
                                        <a href="{$system['system_url']}/school/{$username}/fees/add" {if $view == "fees"}class="color_sub" {else}class=""{/if}>4.2. {__("Add New Fee")}</a>
                                    {/if}
                                {else}
                                    <a href="#" style="color: #5e5e5e">4.2. {__("Add New Fee")}</a>
                                {/if}
                            {/if}
                        {/if}
                        {if $view == "pickup"}
                            {if $school['school_step'] >= 5}
                                 <a href="{$system['system_url']}/school/{$username}/pickup/template" {if $view == "pickup" && $sub_view == "template"}class="color_sub " {else}class=""{/if}>5.1. {__("Setting and create price table")}</a>
                                <i class="fas fa-hand-point-right"></i>
                                {if $school['school_step'] >= 6}
                                    <a href="{$system['system_url']}/school/{$username}/pickup/assign" {if $view == "pickup" && $sub_view == "assign"}class="color_sub" {else}class=""{/if}>5.2. {__("Assign teacher")}</a>
                                {else}
                                    <a href="#" style="color: #5e5e5e">5.2. {__("Assign teacher")}</a>
                                {/if}
                            {/if}
                        {/if}
                    </div>
                </div>
            </div>
        </div>
    </div>
{/if}
