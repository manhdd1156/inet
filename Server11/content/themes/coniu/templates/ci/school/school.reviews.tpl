<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <i class="fa fa-star fa-lg fa-fw pr10"></i>
        {__("Reviews")}
        {if $sub_view == ""}
            &rsaquo; {__('School Reviews')}
        {elseif $sub_view == "teachers"}
            &rsaquo; {__('Teacher Reviews')}
        {elseif $sub_view == "teacher"}
            &rsaquo; {__('Teacher Reviews')}
            &rsaquo; {$rows['teacher']['user_fullname']}
        {/if}
    </div>
    {if $sub_view == ""}
        <div class="panel-body with-table">
            {if isset($rows['review_info'])}
                <div class="pl10">
                    <strong>{__("Overall review")}&#58;&nbsp;<font color="red">{$rows['review_info']['total_review']}</font></strong>&emsp;
                    <strong>{__("Average review")}&#58;&nbsp;<font color="red">{$rows['review_info']['average_review']}</font></strong>&emsp;
                    <strong>{__("New review")}&#58;&nbsp;<font color="red">{$rows['review_info']['cntNewReview']}</font></strong>&emsp;
                </div>
            {else}
                <div class="pl10">
                    <strong>{__("Overall review")}&#58;&nbsp;<font color="red">0</font></strong>&emsp;
                    <strong>{__("Average review")}&#58;&nbsp;<font color="red">0</font></strong>&emsp;
                    <strong>{__("New review")}&#58;&nbsp;<font color="red">0</font></strong>&emsp;
                </div>
            {/if}<br>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover js_dataTable">
                    <thead>
                        <tr>
                            <th width="10%">#</th>
                            {*<th width="20%">{__('Full name')}</th>*}
                            <th width="15%">{__('Rating')}</th>
                            <th width="60%">{__("Review content")}</th>
                            <th width="15%">{__("Time")}</th>
                        </tr>
                    </thead>
                    <tbody>
                        {$idx = 1}
                        {foreach $rows['review_detail'] as $row}
                            <tr>
                                <td align="center" class="align_middle">{$idx}</td>
                               {* <td class="align_middle">
                                    <span class="name js_user-popover" data-uid="{$row['user_id']}">
                                        <a href="{$system['system_url']}/{$row['user_name']}">{$row['user_fullname']}</a>
                                    </span>
                                </td>*}
                                <td align="center" class="align_middle">{$row['rating']}</td>

                                {if is_empty($row['comment']) }
                                    <td align="center" class="align_middle">
                                        &#150;
                                    </td>
                                {else}
                                    <td class="align_middle">
                                        {$row['comment']}
                                    </td>
                                {/if}

                                <td align="center" class="align_middle">{$row['created_at']}</td>
                            </tr>
                            {$idx = $idx + 1}
                        {/foreach}

                    </tbody>
                </table>
            </div>

            <br>
            <div class="alert alert-warning">
                {__("Chú ý: Số sao mỗi lần phụ huynh được đánh giá tối đa là (5*)")}
            </div>

        </div>

    {elseif $sub_view == "teachers"}
        <div class="panel-body with-table">
            {if isset($rows['review_info'])}
            <div class="pl10">
                <strong>{__("Overall review")}&#58;&nbsp;<font color="red">{$rows['review_info']['total_review']}</font></strong>&emsp;
                <strong>{__("Average review")}&#58;&nbsp;<font color="red">{$rows['review_info']['average_review']}</font></strong>&emsp;
                <strong>{__("New review")}&#58;&nbsp;<font color="red">{$rows['review_info']['cntNewReview']}</font></strong>&emsp;
            </div>
            {else}
            <div class="pl10">
                <strong>{__("Overall review")}&#58;&nbsp;<font color="red">0</font></strong>&emsp;
                <strong>{__("Average review")}&#58;&nbsp;<font color="red">0</font></strong>&emsp;
                <strong>{__("New review")}&#58;&nbsp;<font color="red">0</font></strong>&emsp;
            </div>
            {/if}<br>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover js_dataTable">
                    <thead>
                    <tr>
                        <th width="7%">#</th>
                        <th width="25%">{__('Teacher')}</th>
                        <th width="20%">{__('Class')}</th>
                        <th width="20%">{__("Overall review")}</th>
                        <th width="15%">{__("Average")}</th>
                        <th width="13%">{__("Actions")}</th>
                    </tr>
                    </thead>
                    <tbody>
                    {$idx = 1}
                    {foreach $rows['review_detail'] as $row}
                        <tr>
                            <td align="center" class="align_middle">{$idx}</td>
                            <td class="align_middle">
                                    <span class="name js_user-popover" data-uid="{$row['user_id']}">
                                        <a href="{$system['system_url']}/{$row['user_name']}">{$row['user_fullname']}</a>
                                    </span>
                            </td>
                            <td class="align_middle">{$row['group_title']}</td>
                            <td align="center" class="align_middle">{$row['total_review']}</td>
                            <td  align="center" class="align_middle">{$row['average_review']}</td>
                            <td align="center" class="align_middle">
                                <a class="btn btn-xs btn-default" href="{$system['system_url']}/school/{$username}/reviews/teacher/{$row['group_id']}/{$row['user_id']}">{__("Detail")}</a>
                            </td>
                        </tr>
                        {$idx = $idx + 1}
                    {/foreach}

                    </tbody>
                </table>
            </div>
        </div>

    {elseif $sub_view == "teacher"}
        <div class="panel-body with-table">

            <div class="pl10">
                <strong>{__("Teacher")} &#58;&nbsp; </strong> {$rows['teacher']['user_fullname']} <br>
                <strong>{__("Class")}&#58;&nbsp;</strong> {$rows['class']['group_title']} <br>
                <strong>{__("Overall review")}&#58;&nbsp;</strong> {$rows['reviews']|count}
            </div>
            <br>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover js_dataTable">
                    <thead>
                    <tr>
                        <th width="10%">#</th>
                        {*<th width="25%">{__('Reviewer')}</th>*}
                        <th width="15%">{__('Rating')}</th>
                        <th width="60%">{__("Review content")}</th>
                        <th width="15%">{__("Time")}</th>
                    </tr>
                    </thead>
                    <tbody>
                    {$idx = 1}
                    {foreach $rows['reviews'] as $row}
                        <tr>
                            <td align="center" class="align_middle">{$idx}</td>
                            {*<td class="align_middle">
                                <span class="name js_user-popover" data-uid="{$row['user_id']}">
                                    <a href="{$system['system_url']}/{$row['user_name']}">{$row['user_fullname']}</a>
                                </span>
                            </td>*}
                            <td align="center" class="align_middle">{$row['rating']}</td>

                            {if is_empty($row['comment']) }
                                <td align="center" class="align_middle">
                                    &#150;
                                </td>
                            {else}
                                <td class="align_middle">
                                    {$row['comment']}
                                </td>
                            {/if}

                            <td align="center" class="align_middle">{$row['created_at']}</td>
                        </tr>
                        {$idx = $idx + 1}
                    {/foreach}

                    </tbody>
                </table>
            </div>

            <br>
            <div class="alert alert-warning">
                {__("Chú ý: Số sao mỗi lần phụ huynh được đánh giá tối đa là (5*)")}
            </div>

        </div>

    {/if}
</div>