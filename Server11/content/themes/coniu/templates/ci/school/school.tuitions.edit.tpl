<div class="panel-body">
    <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_tuition.php">
        <input type="hidden" name="school_username" id="school_username" value="{$username}"/>
        <input type="hidden" name="do" value="edit_tuition"/>
        <input type="hidden" name="tuition_id" value="{$data['tuition_id']}"/>
        <div class="form-group">
            <label class="col-sm-3 control-label text-left">{__("Class")}</label>
            <div class="col-sm-9">
                <div class="col-xs-5">
                    <input type="hidden" name="class_id" value="{$data['class_id']}"/>
                    <input type='text' value="{$data['group_title']}" class="form-control" readonly/>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label text-left">{__("Month")}</label>
            <div class="col-sm-9">
                <div class='col-sm-5'>
                    <input type='text' name="month" id="month" value="{$data['month']}" class="form-control" readonly/>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label text-left">{__("Number of school days")}</label>
            <div class="col-sm-9">
                <div class="col-sm-2">
                    <input type='text' name="day_of_month" value="{$data['day_of_month']}" class="form-control" readonly/>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label text-left">{__("Notify immediately")}?</label>
            <div class="col-sm-9">
                <div class="onoffswitch">
                    <input type="checkbox" name="notify_immediately" class="onoffswitch-checkbox"
                           id="notify_immediately" {if $data['is_notified']=='1'}checked{/if}>
                    <label class="onoffswitch-label" for="notify_immediately"></label>
                    <input type="hidden" name="is_notified_old" value="{$data['is_notified']}">
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label text-left">{__("Description")}</label>
            <div class="col-sm-9">
                <textarea class="form-control" name="description" rows="2">{$data['description']}</textarea>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-9 col-sm-offset-3">
                <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                <a href="{$system['system_url']}/school/{$username}/tuitions" class="btn btn-default">{__("Lists")}</a>
            </div>
        </div>
        <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
        <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>

        <div id="open_dialog" class="x-hidden" title="{__("Adding fees to calculate tuition")|upper}"></div>

        {* Hiển thị bảng ký hiệu giải thích trạng thái điểm danh *}
        {if $school['tuition_view_attandance'] && ($school['attendance_use_leave_early'] || $school['attendance_use_come_late'] || $school['attendance_absence_no_reason'])}
            <table class="table table-striped table-bordered table-hover">
                <tbody>
                <tr>
                    <td align="right"><strong>{__("Symbol")}:</strong></td>
                    {if $school['attendance_use_come_late']}
                        <td bgcolor="#6495ed" align="center"><i class="fa fa-check" aria-hidden="true"></td>
                        <td align="left">{__("Come late")}</td>
                    {/if}
                    {if $school['attendance_use_leave_early']}
                        <td bgcolor="#008b8b" align="center"><i class="fa fa-check" aria-hidden="true"></td>
                        <td align="left">{__("Leave early")}</td>
                    {/if}
                    <td align="center"><i class="fa fa-check" aria-hidden="true"></td>
                    <td align="left">{__("Present")}</td>

                    {if $school['attendance_absence_no_reason']}
                        <td bgcolor="#ff1493" align="center"><i class="fa fa-times" aria-hidden="true"></td>
                        <td align="left">{__("Without permission")}</td>
                    {/if}

                    <td bgcolor="#deb887" align="center"><i class="fa fa-times" aria-hidden="true"></td>
                    <td align="left">{__("With permission")}</td>
                </tr>
                </tbody>
            </table>
        {/if}
        <div class="table-responsive" id="children_list">
            <table class="table table-striped table-bordered table-hover">
                <thead>
                <tr>
                    <th>
                        <div class="pull-left flip">{__("Children list")|upper}&nbsp;({$data['tuition_child']|count})</div>
                        <div class="pull-right flip">
                            {__("Total of class")}&nbsp;({$smarty.const.MONEY_UNIT})
                            <input type="text" class="text-right money_tui" id="class_total" name="total_amount" value="{$data['total_amount']}" readonly/>
                        </div>
                    </th>
                </tr>
                </thead>
                <tbody>
                {foreach $data['tuition_child'] as $child}
                    <input type="hidden" name="old_tuition_child_id[]" value="{$child['tuition_child_id']}"/>
                {/foreach}
                {$childIdx = 1}
                {foreach $data['tuition_child'] as $child}
                    <tr id="child_row_{$child['child_id']}">
                        <td id="child_tuition_{$child['child_id']}">
                            <div style = "color: #597BA5; display: inline;">
                                <strong>{$childIdx} - {$child['child_name']} ({$child['month']} {__("Month")})</strong>
                                {if count($child['attendances']) > 0}&nbsp;|&nbsp;
                                    {__("Number of charged days from last tuition")}:&nbsp;{$child['attendance_count']}
                                {/if}
                                <input type="hidden" name="child_id[]" value="{$child['child_id']}"/>
                                <input type="hidden" name="tuition_child_id[]" value="{$child['tuition_child_id']}"/>

                                <div class="pull-right flip">
                                    {if $child['status'] == $smarty.const.TUITION_CHILD_NOT_PAID}
                                        <a class="btn btn-xs btn-default js_display_fee_2_add" child_id="{$child['child_id']}">{__("Add")}</a>
                                    {/if}
                                    {*<a class="btn btn-xs btn-default js_reload_child_tuition" child_id="{$child['child_id']}" child_idx="{$childIdx}">{__("Reload")}</a>*}
                                    <a class="btn btn-xs btn-default js_show_hide_tuition_tbody" child_id="{$child['child_id']}">{__("Show/hide")}</a>
                                    <a class="btn btn-xs btn-danger js_new_tuition_del_child" child_id="{$child['child_id']}">{__("Delete")}</a>
                                </div>
                            </div>
                            {*Số ngày đi học ước tính của riêng trẻ*}
                            <div class="mb10">
                                {__("The number of school days in a student's month")} {$child['child_name']}
                                <input style="width: 40px" type="number" name="day_of_child_{$child['child_id']}" id="day_of_child_{$child['child_id']}" class="day_of_child" value="" child_id="{$child['child_id']}" {if $child['status']!=$smarty.const.TUITION_CHILD_NOT_PAID}readonly{/if}>
                            </div>
                            {*Hiển thị thông tin điểm danh chi tiết của trẻ tháng trước*}
                            {if $school['tuition_view_attandance'] && (count($child['attendances']) > 0)}
                                <table class="table table-striped table-bordered table-hover">
                                    <tr>
                                        <td style="padding-left: 5px; padding-right: 5px"><a href="{$system['system_url']}/school/{$username}/attendance/child/{$child['child_id']}" target="_blank"><strong>{__("ATT")}</strong></a></td>
                                        {foreach $child['attendances'] as $attendance}
                                            <td style="padding: 5px" align="center"
                                                    {if $attendance['status'] == $smarty.const.ATTENDANCE_COME_LATE}
                                                        bgcolor="#6495ed"
                                                    {elseif $attendance['status'] == $smarty.const.ATTENDANCE_EARLY_LEAVE}
                                                        bgcolor="#008b8b"
                                                    {elseif $attendance['status'] == $smarty.const.ATTENDANCE_ABSENCE_NO_REASON}
                                                        bgcolor="#ff1493"
                                                    {elseif $attendance['status'] == $smarty.const.ATTENDANCE_ABSENCE}
                                                        bgcolor="#deb887"
                                                    {/if}
                                            >{$attendance['display_date']}</td>
                                        {/foreach}
                                    </tr>
                                </table>
                            {/if}
                            <br/>
                            <table class="table table-striped table-bordered table-hover" id="tbl_fee_{$child['child_id']}">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>{__("Fee name")}</th>
                                    <th style="width: 52px">{__("Qty")}</th>
                                    <th style="width: 117px">{__("Unit price")}</th>
                                    <th style="width: 107px">{__("Deduction")}</th>
                                    <th style="width: 117px">{__("Money amount")}</th>
                                    <th style="width: 80px">{__("Option")}</th>
                                </tr>
                                </thead>
                                <tbody>
                                {*Hiển thị danh sách phí của trẻ*}
                                {$idx = 1}
                                {foreach $child['tuition_detail'] as $detail}
                                    {if $detail['type'] == $smarty.const.TUITION_DETAIL_FEE}
                                        <tr>
                                            <td align="center">
                                                {$idx}
                                                <input type="hidden" class="fee_id_{$child['child_id']}" name="fee_id_{$child['child_id']}[]" value="{$detail['fee_id']}"/>
                                            </td>
                                            <td>{$detail['fee_name']}</td>
                                            <td>
                                                <input type="number" id="fee_quantity_{$child['child_id']}_{$detail['fee_id']}"
                                                   child_id="{$child['child_id']}" fee_id="{$detail['fee_id']}" name="fee_quantity_{$child['child_id']}[]"
                                                   {if $detail['fee_type']==$smarty.const.FEE_TYPE_MONTHLY}
                                                       class="text-center fee_quantity" value="1" readonly style="width: 35px; background-color: #EEEEEE"
                                                   {else} class="text-center fee_quantity day_of_child_val_{$child['child_id']}" data-type="1"
                                                       min="0" value="{$detail['quantity']}" step="1" style="width: 35px;"
                                                       {if $child['status']!=$smarty.const.TUITION_CHILD_NOT_PAID}readonly{/if}
                                                   {/if}
                                                />
                                            </td>
                                            <td>
                                                <input style="width: 100px; background-color: #EEEEEE" type="text" class="text-right fee_unit_price money_tui" id="fee_unit_price_{$child['child_id']}_{$detail['fee_id']}"
                                                       child_id="{$child['child_id']}" fee_id="{$detail['fee_id']}" name="fee_unit_price_{$child['child_id']}[]"
{*                                                       min="0" value="{$detail['unit_price']}" {if $child['status']!=$smarty.const.TUITION_CHILD_NOT_PAID}readonly{/if}/>*}
                                                       min="0" value="{$detail['unit_price']}" readonly/>
                                            </td>
                                            <td>
                                                {if $detail['fee_type'] == $smarty.const.FEE_TYPE_MONTHLY}
                                                    <input type="hidden" id="fee_quantity_deduction_{$child['child_id']}_{$detail['fee_id']}" name="fee_quantity_deduction_{$child['child_id']}[]" value="1"/>
                                                    <input style="width: 90px;" type="text" class="text-right fee_unit_price_deduction money_tui" id="fee_unit_price_deduction_{$child['child_id']}_{$detail['fee_id']}"
                                                           child_id="{$child['child_id']}" fee_id="{$detail['fee_id']}"
                                                           name="fee_unit_price_deduction_{$child['child_id']}[]" value="{if $detail['unit_price_deduction']!= 0 && $detail['quantity_deduction']!= 0}{$detail['unit_price_deduction']}{/if}" step="1000" placeholder="({$smarty.const.MONEY_UNIT})" {if $child['status']!=$smarty.const.TUITION_CHILD_NOT_PAID}readonly{/if}/>
                                                {else}
                                                    <input style="width: 35px;" type="number" class="text-center fee_quantity_deduction" id="fee_quantity_deduction_{$child['child_id']}_{$detail['fee_id']}"
                                                           child_id="{$child['child_id']}" fee_id="{$detail['fee_id']}" name="fee_quantity_deduction_{$child['child_id']}[]"
                                                           value="{$detail['quantity_deduction']}" step="1" {if $child['status']!=$smarty.const.TUITION_CHILD_NOT_PAID}readonly{/if}/>({__('Day')})
                                                    <input type="hidden" id="fee_unit_price_deduction_{$child['child_id']}_{$detail['fee_id']}" name="fee_unit_price_deduction_{$child['child_id']}[]" value="{$detail['unit_price_deduction']}"/>
                                                {/if}
                                            </td>
                                            <td>
                                                <input style="width: 100px; background-color: #EEEEEE" type="text" class="text-right money_tui" id="fee_amount_{$child['child_id']}_{$detail['fee_id']}" name="fee_amount_{$child['child_id']}[]"
                                                       value="{$detail['quantity']*$detail['unit_price'] - $detail['quantity_deduction']*$detail['unit_price_deduction']}" readonly/>
                                            </td>
                                            <td align="center">
                                                {if $child['status'] == $smarty.const.TUITION_CHILD_NOT_PAID}
                                                    <button class="btn-danger js_del_tuition_item" child_id="{$child['child_id']}">{__("Delete")}</button>
                                                {/if}
                                            </td>
                                        </tr>
                                        {$idx = $idx + 1}
                                    {/if}
                                {/foreach}
                                {*Đoạn dưới làm mốc để add các hàng cần thêm vào*}
                                <tr id="fee_checkpoint_{$child['child_id']}"></tr>

                                {*Hiển thị các loại dịch vụ (CHỈ DỊCH VỤ THEO THÁNG VÀ THEO ĐIỂM DANH) sẻ dùng tháng hiện tại*}
                                {foreach $child['tuition_detail'] as $detail}
                                    {if $detail['type'] == $smarty.const.TUITION_DETAIL_SERVICE}
                                        <tr>
                                            <td align="center">
                                                {$idx}
                                                <input type="hidden" class="service_id_{$child['child_id']}" name="service_id_{$child['child_id']}[]" value="{$detail['service_id']}"/>
                                                <input type="hidden" id="service_type_{$child['child_id']}_{$detail['service_id']}" name="service_type_{$child['child_id']}[]" value="{$detail['service_type']}"/>
                                            </td>
                                            <td>{$detail['service_name']}</td>
                                            <td>
                                                <input type="number" id="service_quantity_{$child['child_id']}_{$detail['service_id']}" child_id="{$child['child_id']}"
                                                       service_id="{$detail['service_id']}" name="service_quantity_{$child['child_id']}[]" value="{$detail['quantity']}" step="1"
                                                       {if ($child['status']!=$smarty.const.TUITION_CHILD_NOT_PAID) || ($detail['service_type'] == $smarty.const.SERVICE_TYPE_MONTHLY)} class="text-center service_quantity" style="width: 35px; background-color: #EEEEEE" readonly{else} {if $detail['service_type'] == $smarty.const.SERVICE_TYPE_DAILY}class="text-center service_quantity day_of_child_val_{$child['child_id']}"{else}class="text-center service_quantity"{/if} data-type="2" style="width: 35px;"{/if}/>
                                            </td>
                                            <td>
                                                <input type="text" class="text-right service_unit_price money_tui" id="service_unit_price_{$child['child_id']}_{$detail['service_id']}"
                                                       child_id="{$child['child_id']}" service_id="{$detail['service_id']}" name="service_unit_price_{$child['child_id']}[]" value="{$detail['unit_price']}"
                                                       {if $child['status']!=$smarty.const.TUITION_CHILD_NOT_PAID}style="width: 100px; background-color: #EEEEEE" readonly{else}style="width: 100px;"{/if}/>
                                            </td>
                                            <td>
                                                {if $detail['service_type'] == $smarty.const.SERVICE_TYPE_MONTHLY}
                                                    <input type="text" class="text-right service_unit_price_deduction money_tui" id="service_unit_price_deduction_{$child['child_id']}_{$detail['service_id']}" child_id="{$child['child_id']}"
                                                           service_id="{$detail['service_id']}" name="service_unit_price_deduction_{$child['child_id']}[]" value="{if $detail['unit_price_deduction']!= 0 && $detail['quantity_deduction']!= 0}{$detail['unit_price_deduction']}{/if}"
                                                           step="1" placeholder="({$smarty.const.MONEY_UNIT})"
                                                           {if $child['status']!=$smarty.const.TUITION_CHILD_NOT_PAID}style="width: 90px; background-color: #EEEEEE" readonly{else}style="width: 90px;"{/if}/>
                                                    <input type="hidden" name="service_quantity_deduction_{$child['child_id']}[]" value="1"/>
                                                {elseif $detail['service_type'] == $smarty.const.SERVICE_TYPE_DAILY}
                                                    <input  type="number" class="text-center service_quantity_deduction" id="service_quantity_deduction_{$child['child_id']}_{$detail['service_id']}" child_id="{$child['child_id']}"
                                                            service_id="{$detail['service_id']}" name="service_quantity_deduction_{$child['child_id']}[]" value="{$detail['quantity_deduction']}" step="1"
                                                            {if $child['status']!=$smarty.const.TUITION_CHILD_NOT_PAID}style="width: 35px; background-color: #EEEEEE" readonly{else}style="width: 35px;"{/if}/>({__('Day')})
                                                    <input type="hidden" id="service_unit_price_deduction_{$child['child_id']}_{$detail['service_id']}" name="service_unit_price_deduction_{$child['child_id']}[]" value="{$detail['unit_price_deduction']}"/>
                                                {else}
                                                    <input  type="number" class="text-center service_quantity_deduction" id="service_quantity_deduction_{$child['child_id']}_{$detail['service_id']}" child_id="{$child['child_id']}"
                                                            service_id="{$detail['service_id']}" name="service_quantity_deduction_{$child['child_id']}[]" value="{$detail['quantity_deduction']}" step="1"
                                                            {if $child['status']!=$smarty.const.TUITION_CHILD_NOT_PAID}style="width: 35px; background-color: #EEEEEE" readonly{else}style="width: 35px;"{/if}/>({__('Times')})
                                                    <input type="hidden" id="service_unit_price_deduction_{$child['child_id']}_{$detail['service_id']}" name="service_unit_price_deduction_{$child['child_id']}[]" value="{$detail['unit_price_deduction']}"/>
                                                {/if}
                                            </td>
                                            <td><input style="width: 100px;" type="text" class="text-right money_tui" id="service_amount_{$child['child_id']}_{$detail['service_id']}"
                                                       name="service_amount_{$child['child_id']}[]" value="{$detail['quantity']*$detail['unit_price'] - $detail['quantity_deduction']*$detail['unit_price_deduction']}" readonly/></td>
                                            <td align="center">
                                                {if $child['status'] == $smarty.const.TUITION_CHILD_NOT_PAID}
                                                    <button class="btn-danger js_del_tuition_item" child_id="{$child['child_id']}">{__("Delete")}</button>
                                                {/if}
                                            </td>
                                        </tr>
                                        {$idx = $idx + 1}
                                    {elseif $detail['type'] == $smarty.const.LATE_PICKUP_FEE}
                                        <tr>
                                            <td class="text-center">
                                                {$idx}
                                                <input type="hidden" name="pickup_{$child['child_id']}" value="1"/>
                                                <input type="hidden" class="service_id_{$child['child_id']}" name="service_id_{$child['child_id']}[]" value="0"/>
                                                <input type="hidden" name="service_type_{$child['child_id']}[]" value="0"/>
                                                <input type="hidden" id="service_quantity_{$child['child_id']}_0" name="service_quantity_{$child['child_id']}[]" value="1"/>
                                                <input type="hidden" id="service_quantity_deduction_{$child['child_id']}_0" name="service_quantity_deduction_{$child['child_id']}[]" value="0"/>
                                                <input type="hidden" id="service_unit_price_{$child['child_id']}_0" name="service_unit_price_{$child['child_id']}[]" value="{$detail['unit_price']}"/>
                                                <input type="hidden" id="service_unit_price_deduction_{$child['child_id']}_0" name="service_unit_price_deduction_{$child['child_id']}[]" value="0"/>
                                            </td>
                                            <td colspan="4">{__('Late PickUp')}</td>
                                            <td><input type="text" class="text-right tuition_pick_up money_tui" min="0" step="1" id="service_amount_{$child['child_id']}_0" name="service_amount_{$child['child_id']}[]" child_id="{$child['child_id']}"
                                                       value="{$detail['unit_price']}" {if $child['status'] != $smarty.const.TUITION_CHILD_NOT_PAID}style="width: 100px; background-color: #EEEEEE"{else}style="width: 100px;"{/if}/></td>
                                            <td align="center">{if $child['status'] == $smarty.const.TUITION_CHILD_NOT_PAID}<button class="btn-danger js_del_tuition_item" child_id="{$child['child_id']}">{__("Delete")}</button>{/if}</td>
                                        </tr>
                                        {$idx = $idx + 1}
                                    {/if}
                                {/foreach}
                                <tr id="service_checkpoint_{$child['child_id']}"></tr>

                                <tr>
                                    <td colspan="6">
                                        <div class="pull-right flip">
                                            <strong>
                                                {*Hiển thị khoản nợ tháng trước*}
                                                {__("Debt amount of previous month")}({$smarty.const.MONEY_UNIT})&nbsp;{$child['pre_month']}&nbsp;
                                                <input style="width: 100px;" type="text" class="text-right tuition_debt_amount money_tui" id="debt_amount_{$child['child_id']}" name="debt_amount[]" child_id="{$child['child_id']}" value="{$child['debt_amount']}" {if $child['status']!=$smarty.const.TUITION_CHILD_NOT_PAID}readonly{/if}/>
                                            </strong>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="6">
                                        <div class="pull-right flip">
                                            <strong>
                                                {__("Total")}({$smarty.const.MONEY_UNIT})&nbsp;
                                                <input style="width: 100px;" type="text" class="text-right money_tui" id="total_{$child['child_id']}" name="child_total[]" value="{$child['total_amount']}" readonly/>
                                                <input type="hidden" name="old_child_total[]" value="{$child['total_amount']}"/>
                                            </strong>
                                        </div>
                                    </td>
                                </tr>
                                {if $child['status']!=$smarty.const.TUITION_CHILD_NOT_PAID}
                                    <tr>
                                        <td colspan="6">
                                            <div class="pull-right flip">
                                                <strong>
                                                    {__("Paid")}({$smarty.const.MONEY_UNIT})&nbsp;
                                                    <input style="width: 100px;" type="text" class="text-right money_tui" value="{$child['paid_amount']}" readonly/>
                                                </strong>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="6">
                                            <div class="pull-right flip">
                                                <strong>
                                                    {__("Debt amount of this month")}({$smarty.const.MONEY_UNIT})&nbsp;
                                                    <input style="width: 100px;" type="text" class="text-right money_tui" value="{$child['total_amount'] - $child['paid_amount']}" readonly/>
                                                </strong>
                                            </div>
                                        </td>
                                    </tr>
                                {/if}
                                <tr>
                                    <td colspan="2" class="text-right"><strong>{__("Description")}</strong></td>
                                    <td colspan="5" ><input type="text" name="detail_description[]" style="width: 100%" placeholder="{__("Detail description for this tuition (if necessary)")}" value="{$child['description']}" maxlength="300"/></td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    {$childIdx = $childIdx + 1}
                {/foreach}
                {if count($data['tuition_child']) > 0}
                    <tr>
                        <td>
                            <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                            <div class="pull-right flip">
                                <strong>
                                    {__("Total of class")}&nbsp;({$smarty.const.MONEY_UNIT})
                                    <input type="text" class="text-right money_tui" id="class_total_2" name="class_total_2" value="{$data['total_amount']}" readonly/>
                                </strong>
                            </div>
                        </td>
                    </tr>
                {/if}
                </tbody>
            </table>
        </div>
        <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
        <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
    </form>
</div>
