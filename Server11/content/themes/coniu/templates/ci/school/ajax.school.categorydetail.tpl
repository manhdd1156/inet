<div class="mt10" align="center">
    <strong>{__("Edit category")}</strong>
</div>
<div class="panel-body with-table">
    <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_report.php">
        <input type="hidden" name="school_username" value="{$username}"/>
        <input type="hidden" name="do" value="edit_cate_in_temp"/>
        <input type="hidden" name="category_id" value="{$results['report_template_category_id']}"/>
        <div class = "form-group">
            <label class="col-sm-12 control-label text-left" style="text-align: center">{__("Category name")}: {$results['category_name']}</label>
            <input type="hidden" class="form-control" name="category_name" value="{$results['category_name']}">
        </div>
        <div class="table-responsive" id="cate_school_add">
            <table class="table table-striped table-bordered table-hover" id = "editCategoryTable">
                <thead>
                <tr><th colspan="3">{__("Suggest content for category")}</th></tr>
                <tr>
                    <th>
                        {__('No.')}
                    </th>
                    <th>
                        {__('Title')}
                    </th>
                    <th>
                        {__('Actions')}
                    </th>
                </tr>
                </thead>
                <tbody>
                {$idx = 1}
                {foreach $results['suggests'] as $row}
                    <tr>
                        <td class = "col_no align_middle"  align = "center">{$idx}</td>
                        <td>
                            <input type="text" name = "suggests" class = "form-control" value="{$row}">
                        </td>
                        <td align="center" class="align_middle"> <a class="btn btn-danger btn-xs js_report_template-delete"> {__("Delete")} </a></td>
                    </tr>
                    {$idx = $idx + 1}
                {/foreach}
                </tbody>
            </table>
            {*{if $canEdit}*}
                <a class="btn btn-default js_report_suggest-add-temp">{__("Add new suggest")}</a>
                <a class="btn btn-primary padrl30 js_report-category-edit">{__("Save")}</a>
            {*{/if}*}
        </div>
        <!-- success -->
        <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
        <!-- success -->

        <!-- error -->
        <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
        <!-- error -->
    </form>
</div>