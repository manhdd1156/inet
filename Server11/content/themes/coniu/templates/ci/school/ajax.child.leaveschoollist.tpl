<strong>{__("Children list")}&nbsp;({$result['total']} {__("Children")})</strong>
<input type="hidden" name="hid_total" id="hid_total" value="{$result['total']}">
<table class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th>{__("#")}</th>
        <th>{__("Full name")}</th>
        <th>{__("Gender")}</th>
        <th>{__("Birthdate")}</th>
        <th>{__("Study duration")}</th>
        <th>{__("Actions")}</th>
    </tr>
    </thead>
    <tbody>
    {assign var="classId" value="-1" nocache}
    {$idx = ($result['page'] - 1) * $smarty.const.PAGING_LIMIT + 1}
    {foreach $result['children'] as $row}
        {if (!isset($result['class_id']) || ($result['class_id']=="")) && ($classId != $row['class_id'])}
            <tr>
                <td colspan="7">
                    {if $row['class_id'] > 0}
                        {__("Class")}:&nbsp;{$row['group_title']}
                    {else}
                        {__("No class")}
                    {/if}
                </td>
            </tr>
        {/if}
        <tr>
            <td class="align-middle" align="center">{$idx}</td>
            {*<td>{$row['child_code']}</td>*}
            <td class="align-middle"><a href="{$system['system_url']}/school/{$username}/children/detail/{$row['child_id']}">{$row['child_name']}</a></td>
            <td class="align-middle" align="center">{if $row['gender'] == $smarty.const.MALE}{__("Male")}{else}{__("Female")}{/if}</td>
            <td class="align-middle" align="center">{$row['birthday']}</td>
            <td class="align-middle" align="center"><strong>{$row['begin_at']}-{$row['end_at']}</strong></td>
            <td class="align-middle" align="center">
                {if $canEdit}
                    {if ($row['is_leave_fee'])}
                        <a href="{$system['system_url']}/school/{$username}/children/editleave/{$row['child_id']}" class="btn btn-xs btn-default">{__("Edit")}</a>
                    {else}
                        <a href="{$system['system_url']}/school/{$username}/children/leave/{$row['child_id']}" class="btn btn-xs btn-default">{__("Edit")}</a>
                    {/if}
                {/if}
                <a href="#" class="btn btn-xs btn-default js_school-export-account-4leave" id="export_to_excel_{$row['child_id']}" data-username="{$username}" data-id="{$row['child_id']}">{__("Export to Excel")}</a>
                <a href="#" class="btn btn-xs btn-danger js_school-delete-child-4leave" id="delete_child_{$row['child_id']}" data-username="{$username}" data-id="{$row['child_id']}">{__("Delete")}</a>
                <label class="btn btn-info btn-xs x-hidden" id="processing_label_{$row['child_id']}">{__("Processing")}...</label>
            </td>
        </tr>
        {assign var="classId" value=$row['class_id'] nocache}
        {$idx = $idx + 1}
    {/foreach}

    {if $result['children']|count == 0}
        <tr class="odd">
            <td valign="top" align="center" colspan="7" class="dataTables_empty">
                {__("No data available in table")}
            </td>
        </tr>
    {/if}
    <input type="hidden" name="hid_page_count" id="hid_page_count" value="{$result['page_count']}">
    {if $result['page_count'] > 1}
        <tr>
            <td colspan="7">
                <div class="pull-right flip">
                    <ul class="pagination">
                        {for $idx = 1 to $result['page_count']}
                            {if $idx == $result['page']}
                                <li class="active"><a href="#">{$idx}</a></li>
                            {else}
                                <li>
                                    <a href="#" class="js_leave-school-child-search" data-username="{$username}" data-id="{$school['page_id']}" data-page="{$idx}">{$idx}</a>
                                </li>
                            {/if}
                        {/for}
                    </ul>
                </div>
            </td>
        </tr>
    {/if}
    </tbody>
</table>