<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <div class="pull-right flip">
            {if $sub_view == ""}
                <a href="{$system['system_url']}/school/{$username}/birthdays/parent" class="btn btn-default">
                    <i class="fa fa-universal-access"></i> {__("Parent")}
                </a>
                <a href="{$system['system_url']}/school/{$username}/birthdays/teacher" class="btn btn-default">
                    <i class="fa fa-street-view"></i> {__("Teacher")}
                </a>
            {elseif $sub_view == "parent"}
                <a href="{$system['system_url']}/school/{$username}/birthdays" class="btn btn-default">
                    <i class="fa fa-child"></i> {__("Children")}
                </a>
                <a href="{$system['system_url']}/school/{$username}/birthdays/teacher" class="btn btn-default">
                    <i class="fa fa-street-view"></i> {__("Teacher")}
                </a>
            {elseif $sub_view == "teacher"}
                <a href="{$system['system_url']}/school/{$username}/birthdays" class="btn btn-default">
                    <i class="fa fa-child"></i> {__("Children")}
                </a>
                <a href="{$system['system_url']}/school/{$username}/birthdays/parent" class="btn btn-default">
                    <i class="fa fa-universal-access"></i> {__("Parent")}
                </a>
            {/if}
        </div>
        <i class="fa fa-birthday-cake fa-fw fa-lg pr10"></i>
        {__("Birthday")}
        {if $sub_view == ""}
            &rsaquo; {__("Child")}
        {elseif $sub_view == "parent"}
            &rsaquo; {__('Parent')}
        {elseif $sub_view == "teacher"}
            &rsaquo; {__('Teacher')}
        {/if}
    </div>
    {if $sub_view == ""}
        <div class="panel-body with-table form-horizontal">
            <div class="row">
                <div class='col-sm-4'>
                    <select name="class_id" id="birthday_class_id" class="form-control">
                        <option value="">{__("Select Class")}...</option>
                        {$class_level = -1}
                        {foreach $classes as $class}
                            {if ($class_level != $class['class_level_id'])}
                                <option value="0" disabled>-----{$class['class_level_name']}-----</option>
                            {/if}
                            <option value="{$class['group_id']}">{$class['group_title']}</option>
                            {$class_level = $class['class_level_id']}
                        {/foreach}
                    </select>
                </div>
                <div class='col-sm-3'>
                    <div class='input-group date' id='fromdatepicker'>
                        <input type='text' name="fromDate" id="fromDate" value="{$fromDate}" class="form-control" placeholder="{__("From date")}"/>
                        <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                    </div>
                </div>
                <div class='col-sm-3'>
                    <div class='input-group date' id='todatepicker'>
                        <input type='text' name="toDate" id="toDate" value="{$toDate}" class="form-control" placeholder="{__("To date")}"/>
                        <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                    </div>
                </div>
                <div class='col-sm-2'>
                    <div class="form-group">
                        <a href="#" id="search" class="btn btn-default js_birthday-search-child" data-username="{$username}" data-id="{$school['page_id']}">{__("Search")}</a>
                        <label id="loading" class="btn btn-info x-hidden">{__("Loading")}...</label>
                    </div>
                </div>
            </div>

            <div class="table-responsive pt10" id="birthday">
                {*{include file="ci/school/ajax.child.birthday.tpl"}*}
            </div>
        </div>

    {elseif $sub_view == "parent"}
        <div class="panel-body with-table form-horizontal">
            <div class="row">
                <div class='col-sm-4'>
                    <select name="class_id" id="birthday_class_id" class="form-control">
                        <option value="">{__("Select Class")}...</option>
                        {$class_level = -1}
                        {foreach $classes as $class}
                            {if ($class_level != $class['class_level_id'])}
                                <option value="0" disabled>-----{$class['class_level_name']}-----</option>
                            {/if}
                            <option value="{$class['group_id']}">{$class['group_title']}</option>
                            {$class_level = $class['class_level_id']}
                        {/foreach}
                    </select>
                </div>
                <div class='col-sm-3'>
                    <div class='input-group date' id='fromdatepicker'>
                        <input type='text' name="fromDate" id="fromDate" value="{$fromDate}" class="form-control" placeholder="{__("From date")}"/>
                        <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                    </div>
                </div>
                <div class='col-sm-3'>
                    <div class='input-group date' id='todatepicker'>
                        <input type='text' name="toDate" id="toDate" value="{$toDate}" class="form-control" placeholder="{__("To date")}"/>
                        <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                    </div>
                </div>
                <div class='col-sm-2'>
                    <div class="form-group">
                        <a href="#" id="search" class="btn btn-default js_birthday-search-parent" data-username="{$username}" data-id="{$school['page_id']}">{__("Search")}</a>
                        <label id="loading" class="btn btn-info x-hidden">{__("Loading")}...</label>
                    </div>
                </div>
            </div>
            <br/>
            <div class="table-responsive" id = "birthday">
                {*{include file="ci/school/ajax.parent.birthday.tpl"}*}
            </div>
        </div>
    {elseif $sub_view == "teacher"}
        <div class="panel-body with-table form-horizontal">
            <div class="row">
                <div class='col-sm-3'>
                    <div class='input-group date' id='fromdatepicker'>
                        <input type='text' name="fromDate" id="fromDate" value="{$fromDate}" class="form-control" placeholder="{__("From date")}"/>
                        <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                    </div>
                </div>
                <div class='col-sm-3'>
                    <div class='input-group date' id='todatepicker'>
                        <input type='text' name="toDate" id="toDate" value="{$toDate}" class="form-control" placeholder="{__("To date")}"/>
                        <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                    </div>
                </div>
                <div class='col-sm-2'>
                    <div class="form-group">
                        <a href="#" id="search" class="btn btn-default js_birthday-search-teacher" data-username="{$username}" data-id="{$school['page_id']}">{__("Search")}</a>
                        <label id="loading" class="btn btn-info x-hidden">{__("Loading")}...</label>
                    </div>
                </div>
            </div>
            <br/>
            <div class="table-responsive" id = "birthday">
                {*{include file="ci/school/ajax.teacher.birthday.tpl"}*}
            </div>
        </div>
    {*{elseif $sub_view == "add"}*}

    {/if}
</div>