<table class="table table-striped table-bordered table-hover js_dataTable">
    <thead>
    <tr>
        <th width="7%">#</th>
        <th width="15%">{__('Thời gian')}</th>
        <th width="42%">{__('Giáo viên')}</th>
        <th width="10%">{__('Số trẻ')}</th>
        <th width="13%">{__('Tổng tiền')}</th>
        <th width="13%">{__("Status")}</th>
    </tr>
    </thead>
    <tbody>
    {$idx = 1}
    {foreach $pickups as $pickup}
        <tr>
            <td align="center" style="vertical-align:middle"><strong>{$idx}</strong></td>
            <td align="center" style="vertical-align:middle">
                <strong>{$pickup['pickup_time']}</strong><br>
                {__($pickup['pickup_day'])}
            </td>
            <td align="center" style="vertical-align:middle">
                {foreach $pickup['pickup_assign'] as $assign}
                    <strong>
                        <label class="col-xs-12 col-sm-6 text-left">&#9679;&#09;{$assign['user_fullname']}
                    </strong>
                {/foreach}
            </td>
            <td align="center" style="vertical-align:middle">
                <font color="#ff4500"><strong>{$pickup['total_child']}</strong></font>
            </td>
            <td  align="center" style="vertical-align:middle">
                <font color="#ff4500"><strong>{moneyFormat($pickup['total'])}</strong></font>
            </td>
            <td align="center" nowrap="true" style="vertical-align:middle">
                {if $pickup['total_child'] == 0}
                    <i class="fa fa-times" style="color:black" title="Chưa có thông tin" aria-hidden="true"></i>
                {else}
                    <a href="{$system['system_url']}/school/{$school['page_name']}/pickup/detail/{$pickup['pickup_id']}" class="btn btn-xs btn-default">
                        {__("Detail")}
                    </a>
                {/if}
            </td>
        </tr>
        {$idx = $idx + 1}
    {/foreach}
    </tbody>
</table>
