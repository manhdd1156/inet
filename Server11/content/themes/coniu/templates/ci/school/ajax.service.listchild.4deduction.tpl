<div><strong>{__("Children list")}&nbsp;({__("Children")}: {$results|count}</strong></div>
<table class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th>#</th>
            <th>{__("Select")}</th>
            <th>{__("Full name")}</th>
            <th>{__("Birthday")}</th>
            <th>{__("Registration time")}</th>
        </tr>
    </thead>
    <tbody>
        {$idx = 1}
        {foreach $results as $row}
            <tr>
                <td align="center">{$idx}</td>
                <td align="center">
                    <input type="checkbox" name="childIds[]" value="{$row['child_id']}" {if $row['deduction_date'] != ''}checked{/if}>
                    {if $row['deduction_date'] != ''}
                        <input type="hidden" name="oldChildIds[]" value="{$row['child_id']}">
                    {/if}
                </td>
                <td>{$row['child_name']}</td>
                <td>{$row['birthday']}</td>
                <td>
                    {$row['begin']}&nbsp;-&nbsp;{$row['end']}
                </td>
            </tr>
            {$idx = $idx + 1}
        {/foreach}
    </tbody>
</table>