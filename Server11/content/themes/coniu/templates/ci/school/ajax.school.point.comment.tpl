<div class="form-group">
    <label class="col-sm-3 control-label text-left">{__("comment")}</label>
    <div class="col-sm-9">
        <textarea rows="4" class="form-control" name="child_comment" id="child_comment" placeholder="{__("Write a comment...")}">{$results}</textarea>
    </div>
</div>
