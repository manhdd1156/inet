<script type="text/javascript">
    $('#birthdate_picker').datetimepicker({
        format: DATE_FORMAT,
        defaultDate: new Date()
    });
</script>
<form class="form-horizontal" enctype="multipart/form-data" method="post" id="school_add_health_index">
    <input type="hidden" name="school_username" value="{$username}"/>
    <input type="hidden" name="do" value="add_health_index"/>
    <div class="form-group">
        <label class = "col-sm-4 control-label text-left">{__("Measure time")} (*)</label>
        <div class='col-sm-3'>
            <div class='input-group date' id='birthdate_picker'>
                <input type='text' name="recorded_at" id="recorded_at" class="form-control"/>
                <span class="input-group-addon">
                    <span class="fas fa-calendar-alt"></span>
                </span>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-4 health_left">
            <div class="table-responsive">
                <div class="mb10 ml10"><strong>{__("Children list")}&nbsp;({$children['children']|count})</strong></div>
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>{__("#")}</th>
                        <th>{__("Child")}</th>
                    </tr>
                    </thead>
                    <tbody>
                    {$idx = 1}
                    {foreach $children['children'] as $child}
                        <tr>
                            <td align="center"><strong>{$idx}</strong></td>
                            <td>
                                <a href="#" class="js_school-child-health" id="child_{$child['child_id']}" data-id="{$child['child_id']}">{$child['child_name']}</a>
                            </td>
                        </tr>
                        {$idx = $idx + 1}
                    {/foreach}
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-sm-8 health_right">
            {foreach $children['children'] as $child}
                <div class="hidden hideAll" id="health_{$child['child_id']}">
                    <input type="hidden" name="childIds[]" value="{$child['child_id']}">
                    <div class="mb10"><strong>{__("Add health index")} {__("for")}:&nbsp;{$child['child_name']}</strong></div>
                    <div class="" style="border: 1px solid #b7b7b7; padding: 10px 10px">
                        <div class = "form-group">
                            <label class="col-sm-3 control-label text-left">{__("Height")}</label>
                            <div class="col-sm-5">
                                <input type="number" min="0" step = "any" class="form-control" name="height_{$child['child_id']}" placeholder="{__("Height")}">
                            </div>
                            <label class="col-sm-2 control-label text-left" style = "text-align: left">{__("cm")}</label>
                        </div>
                        <div class = "form-group">
                            <label class="col-sm-3 control-label">{__("Weight")}</label>
                            <div class="col-sm-5">
                                <input type="number" min="0" step = "any" class="form-control" name="weight_{$child['child_id']}" placeholder="{__("Weight")}">
                            </div>
                            <label class="col-sm-2 control-label text-left" style = "text-align: left">{__("kg")}</label>
                        </div>
                        <div class = "form-group">
                            <label class="col-sm-3 control-label">{__("Nutritional status")}</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="nutriture_status_{$child['child_id']}">
                            </div>
                        </div>
                        <div class = "form-group">
                            <label class="col-sm-3 control-label">{__("Heartbeat")}</label>
                            <div class="col-sm-5">
                                <input type="number" class="form-control" name="heart_{$child['child_id']}" min="0" step="1">
                            </div>
                            <label class="col-sm-2 control-label">{__("Times/minute")}</label>
                        </div>
                        <div class = "form-group">
                            <label class="col-sm-3 control-label">{__("Blood pressure")}</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="blood_pressure_{$child['child_id']}">
                            </div>
                        </div>
                        <div class = "form-group">
                            <label class="col-sm-3 control-label">{__("Ear")}</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="ear_{$child['child_id']}">
                            </div>
                        </div>
                        <div class = "form-group">
                            <label class="col-sm-3 control-label">{__("Eye")}</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="eye_{$child['child_id']}">
                            </div>
                        </div>
                        <div class = "form-group">
                            <label class="col-sm-3 control-label">{__("Nose")}</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="nose_{$child['child_id']}">
                            </div>
                        </div>
                        <div class = "form-group">
                            <label class="col-sm-3 control-label">{__("Description")}</label>
                            <div class="col-sm-9">
                                <textarea type="text" class="form-control" name="description_{$child['child_id']}"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label text-left">{__("File attachment")}</label>
                            <div class="col-sm-6">
                                {if !is_empty($data['source_file'])}
                                    <a href="{$data['source_file']}" target="_blank"><img src = "{$data['source_file']}" class = "img-responsive"></a>
                                    <br>
                                    <label class="control-label">{__("Choose file replace")}</label>
                                    <br>
                                {/if}
                                <input type="file" name="file_{$child['child_id']}" id="file"/>
                            </div>
                        </div>
                    </div>
                </div>
            {/foreach}
            <br/>
            <div class="form-group hidden submit_hidden" id="">
                <div class="col-sm-9">
                    <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group hidden submit_hidden" id="">
        <div class="col-sm-9">
            <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
        </div>
    </div>

    <!-- success -->
    <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
    <!-- success -->
    <!-- error -->
    <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
    <!-- error -->
</form>