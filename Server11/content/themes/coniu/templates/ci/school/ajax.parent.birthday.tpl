<table class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th colspan="7">{__("Parent list")}&nbsp;({$rows|count})</th>
    </tr>
    <tr>
        <th>{__('#')}</th>
        <th>{__('Parent name')}</th>
        <th>{__('Birthdate')}</th>
        <th>{__("Parent of student")} ({__("Class")})</th>
        <th>{__("Phone")}</th>
        <th>{__("Email")}</th>
    </tr>
    </thead>
    <tbody>
    {foreach $rows as $k => $row}
        <tr>
            <td align="center">{$k + 1}</td>
            <td><a href = "{$system['system_url']}/{$row['user_name']}"> {convertText4Web({$row['user_fullname']})}</a></td>
            <td>{$row['user_birthdate']}</td>
            <td>
                {foreach $row['children'] as $child}
                    {$child['child_name']} ({$child['group_title']})
                    <br/>
                {/foreach}
            </td>
            <td>{$row['user_phone']}</td>
            <td><a href="mailto:{$row['user_email']}">{$row['user_email']}</a></td>
        </tr>
    {/foreach}

    {if $rows|count == 0}
        <tr class="odd">
            <td valign="top" align="center" colspan="7" class="dataTables_empty">
                {__("There is no parent having birthday in this duration")}
            </td>
        </tr>
    {/if}
    </tbody>
</table>