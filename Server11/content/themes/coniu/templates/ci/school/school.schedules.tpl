<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <div class="pull-right flip">
            <a href="https://blog.coniu.vn/huong-dan-tao-lich-hoc-tren-website/" target="_blank" class="btn btn-info  btn_guide">
                <i class="fa fa-info-circle"></i> {__("Guide")}
            </a>
            {if ($sub_view == "") && $canEdit}
                <a href="{$system['system_url']}/school/{$username}/schedules/add" class="btn btn-default">
                    <i class="fa fa-plus"></i> {__("Add New")}
                </a>
            {elseif $sub_view == "add"}
                <a href="{$system['system_url']}/school/{$username}/schedules" class="btn btn-default">
                    <i class="fa fa-list"></i> {__("Lists")}
                </a>
                {*<a href="{$system['system_url']}/school/{$username}/schedules/import" class="btn btn-default">*}
                    {*<i class="fa fa-file-excel-o"></i> {__("Import from Excel file")}*}
                {*</a>*}
            {elseif $sub_view == "import"}
                <a href="{$system['system_url']}/school/{$username}/schedules" class="btn btn-default">
                    <i class="fa fa-list"></i> {__("Lists")}
                </a>
                <a href="{$system['system_url']}/school/{$username}/schedules/add" class="btn btn-default">
                    <i class="fa fa-plus"></i> {__("Add New")}
                </a>
            {/if}
        </div>
        <i class="fas fa-calendar-alt fa-fw fa-lg pr10"></i>
        {__("Schedule")}
        {if $sub_view == ""}
            &rsaquo; {__("Schedule list")}
        {elseif $sub_view == "add"}
            &rsaquo; {__('Add schedule')}
        {elseif $sub_view == "edit"}
            &rsaquo; {__('Edit schedule')}
        {elseif $sub_view == "copy"}
            &rsaquo; {__('Copy schedule')}
        {elseif $sub_view == "detail"}
            &rsaquo; {__('Detail schedule')} {$data['schedule_name']}
        {/if}
    </div>
{if $sub_view == ""}
    <div class="panel-body with-table form-horizontal">
        <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_schedule.php">
            <input type="hidden" name="school_username" id="school_username" value="{$username}"/>
            <input type="hidden" name="do" value="search"/>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left">{__("Scope")}</label>
                <div class="col-sm-9">
                    <div class="col-sm-4">
                        <select name="level" id="schedule_level" class="form-control" data-username = "{$username}" data-id = "{$school['page_id']}">
                            <option value="0">{__("Select scope")}</option>
                            <option value="{$smarty.const.SCHOOL_LEVEL}">{__("School")}</option>
                            <option value="{$smarty.const.CLASS_LEVEL_LEVEL}">{__("Class level")}</option>
                            <option value="{$smarty.const.CLASS_LEVEL}">{__("Class")}</option>
                        </select>
                    </div>
                    <div class="col-sm-4 x-hidden" name="schedule_class_level">
                        <select name="class_level_id" id="class_level_id" class="form-control">
                            <option value="0">{__("Select class level")}</option>
                            {foreach $class_levels as $class_level}
                                <option value="{$class_level['class_level_id']}">{$class_level['class_level_name']}</option>
                            {/foreach}
                        </select>
                    </div>
                    <div class="col-sm-4 x-hidden" name="schedule_class">
                        <select name="class_id" id="class_id" data-username="{$username}" class="form-control">
                            <option value="0">{__("Select class")}</option>
                            {$class_level = -1}
                            {foreach $classes as $class}
                                {if ($class_level != $class['class_level_id'])}
                                    <option value="" disabled style="color: blue">-----{$class['class_level_name']}-----</option>
                                {/if}
                                <option value="{$class['group_id']}">{$class['group_title']}</option>
                                {$class_level = $class['class_level_id']}
                            {/foreach}
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <a class="btn btn-default js_school_schedule_search" data-username = "{$username}" data-handle = "search">{__("Search")}</a>
                    </div>
                </div>
            </div>

        </form>
        <div class="table-responsive" name = "schedule_all">
            <table class="table table-striped table-bordered table-hover js_dataTable">
                <thead>
                <tr><th colspan="7">{__("Schedule list")}&nbsp;(<span class="count_schedule">{$rows|count}</span>)</th></tr>
                <tr>
                    <th>
                        {__("#")}
                    </th>
                    <th>
                        {__("Schedule name")}
                    </th>
                    <th>
                        {__("Begin")}
                    </th>
                    <th>
                        {__("Scope")}
                    </th>
                    <th>
                        {__("Use category")}
                    </th>
                    <th>
                        {__("Study saturday")}
                    </th>
                    <th>
                        {__("Actions")}
                    </th>
                </tr>
                </thead>
                <tbody>
                {$idx = 1}
                {foreach $rows as $row}
                    <tr>
                        <td align="center" class="align-middle">
                            {$idx}
                        </td>
                        <td class="align-middle">
                            <a href="{$system['system_url']}/school/{$username}/schedules/detail/{$row['schedule_id']}">{$row['schedule_name']}</a>
                        </td>
                        <td class="align-middle" align="center">
                            {$row['begin']}
                        </td>
                        <td class="align-middle" align="center">
                            {if $row['applied_for']==$smarty.const.SCHOOL_LEVEL}
                                {__("School")}
                            {elseif $row['applied_for']==$smarty.const.CLASS_LEVEL_LEVEL}
                                {foreach $class_levels as $cl}
                                    {if $cl['class_level_id'] == $row['class_level_id']}{$cl['class_level_name']}{/if}
                                {/foreach}
                            {elseif $row['applied_for']==$smarty.const.CLASS_LEVEL}
                                {foreach $classes as $value}
                                    {if $value['group_id'] == $row['class_id']}{$value['group_title']}{/if}
                                {/foreach}
                            {/if}
                        </td>
                        <td class="align-middle" align="center">
                            {if $row['is_category']}
                                {__('Yes')}
                            {else}
                                {__('No')}
                            {/if}
                        </td>
                        <td class="align-middle" align="center">
                            {if $row['is_saturday']}
                                {__('Yes')}
                            {else}
                                {__('No')}
                            {/if}
                        </td>
                        <td class="align-middle">
                            {if $canEdit}
                                <div>
                                    <a href="{$system['system_url']}/school/{$username}/schedules/copy/{$row['schedule_id']}" class="btn btn-xs btn-default">{__("Copy")}</a>
                                    <a href="{$system['system_url']}/school/{$username}/schedules/edit/{$row['schedule_id']}" class="btn btn-xs btn-default">{__("Edit")}</a>
                                    <button class="btn btn-xs btn-danger js_school-schedule-delete" data-username="{$username}" data-id="{$row['schedule_id']}" data-handle = "delete_schedule">{__("Delete")}</button>
                                    {if !$row['is_notified']}
                                            <div class = "form-label"><button class="btn btn-xs btn-default js_school-schedule-notify" data-handle="notify" data-username="{$username}" data-id="{$row['schedule_id']}">{__("Notify")}</button></div>
                                    {/if}
                                </div>
                            {/if}
                            <button class="btn btn-xs btn-success js_school-schedule-export" data-username="{$username}" data-id="{$row['schedule_id']}" data-handle = "export">{__("Export to Excel")}</button>
                        </td>
                    </tr>
                    {$idx = $idx + 1}
                {/foreach}

                {if $rows|count == 0}
                    <tr class="odd">
                        <td valign="top" align="center" colspan="7" class="dataTables_empty">
                            {__("No data available in table")}
                        </td>
                    </tr>
                {/if}

                </tbody>
            </table>
        </div>
        <div id="cate_list"></div>
    </div>
{elseif $sub_view == "detail"}
    <div class="panel-body with-table">
        <table class = "table table-bordered">
            <tbody>
                <tr>
                    <td class = "col-sm-3 text-right"><strong>{__('Schedule name')}</strong></td>
                    <td>
                        {$data['schedule_name']}
                    </td>
                </tr>
                <tr>
                    <td class = "col-sm-3 text-right"><strong>{__('Scope')}</strong></td>
                    <td>
                        {if isset($class) && $data['applied_for'] == 3} {$class['group_title']}
                        {elseif isset($class_level) && $data['applied_for'] == 2}{$class_level['class_level_name']}
                        {elseif $data['applied_for'] == 1} {__('School')}
                        {/if}
                    </td>
                </tr>
                <tr>
                    <td class = "col-sm-3 text-right"><strong>{__('Begin')}</strong></td>
                    <td>
                        {$data['begin']}
                    </td>
                </tr>
                {*<tr>*}
                    {*<td class = "col-sm-3 text-right">{__('Use category')}</td>*}
                    {*<td>*}
                        {*{if $data['is_category']}*}
                            {*{__('Yes')}*}
                        {*{else}*}
                            {*{__('No')}*}
                        {*{/if}*}
                    {*</td>*}
                {*</tr>*}
                {*<tr>*}
                    {*<td class = "col-sm-3 text-right">{__('Study saturday')}</td>*}
                    {*<td>*}
                        {*{if $data['is_saturday']}*}
                            {*{__('Yes')}*}
                        {*{else}*}
                            {*{__('No')}*}
                        {*{/if}*}
                    {*</td>*}
                {*</tr>*}
                {if $data['description'] != ''}
                    <tr>
                        <td class = "col-sm-3 text-right"><strong>{__('Description')}</strong></td>
                        <td>
                            {nl2br($data['description'])}
                        </td>
                    </tr>
                {/if}
            </tbody>
        </table>
        <div class = "table-responsive">
            <table class = "table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th>{__('#')}</th>
                        <th>{__('Time')} </th>
                        <th {if !$data['is_category']}class="hidden"{/if}>{__('Activity')}</th>
                        <th>{__('Monday')} </th>
                        <th>{__('Tuesday')}</th>
                        <th> {__('Wednesday')}</th>
                        <th>{__('Thursday')}</th>
                        <th>{__('Friday')} </th>
                        <th {if !$data['is_saturday']}class="hidden"{/if}>{__('Saturday')}</th>
                    </tr>
                </thead>
                <tbody>
                    {$idx = 1}
                    {foreach $data['details'] as $k => $row}
                        {$array = array_values($row)}
                        {$temp = array()}
                        {foreach $array as $k => $value}
                            {if $data['is_saturday']}
                                {if ($k >= 4) && ($k < (count($array)))}
                                    {$temp[] = $value}
                                {/if}
                            {else}
                                {if ($k >= 4) && ($k < (count($array) - 1))}
                                    {$temp[] = $value}
                                {/if}
                            {/if}
                        {/foreach}
                        <tr>
                            <td style="vertical-align: middle" align="center">
                                <strong>{$idx}</strong>
                            </td>
                            <td style="vertical-align: middle" align="center">
                                <strong>{$row['subject_time']}</strong>
                            </td>
                            <td {if !$data['is_category']}class="hidden"{/if} align="center" style = "vertical-align: middle;">
                                <strong>{$row['subject_name']}</strong>
                            </td>
                            {$col = 1}
                            {for $i = 0; $i < count($temp); $i++}
                                {if $temp[$i] === $temp[($i+1)]}
                                    {$col = $col + 1}
                                {else}
                                    <td colspan = "{$col}" align="center">
                                        {nl2br($temp[$i])}
                                    </td>
                                    {$col = 1}
                                {/if}
                            {/for}
                        </tr>
                        {$idx = $idx + 1}
                    {/foreach}
                </tbody>
            </table>
        </div>
        <div class="form-group pl5">
            <a class="btn btn-default" href="{$system['system_url']}/school/{$username}/schedules">{__("Lists")}</a>
            {if $canEdit}
                <a class="btn btn-default" href="{$system['system_url']}/school/{$username}/schedules/add">{__("Add New")}</a>
                <a href="{$system['system_url']}/school/{$username}/schedules/copy/{$data['schedule_id']}" class="btn btn-default">{__("Copy")}</a>
                {if !$data['is_notified']}
                    <button class="btn btn-default js_school-schedule-notify" data-handle="notify" data-username="{$username}" data-id="{$data['schedule_id']}">{__("Notify")}</button>
                {/if}
                <a href="{$system['system_url']}/school/{$username}/schedules/edit/{$data['schedule_id']}" class="btn btn-default">{__("Edit")}</a>
                <button class="btn btn-danger js_school-schedule-delete" data-username="{$username}" data-id="{$data['schedule_id']}" data-handle = "delete_schedule">{__("Delete")}</button>
            {/if}
        </div>
    </div>
{elseif $sub_view == "add"}
    <div class="panel-body">
        <div id="open_dialog_schedule" class="x-hidden" title="{__("Schedule preview")|upper}">
            {include file="ci/school/ajax.school.schedule.preview.tpl"}
        </div>
        <form class="form-horizontal" action="#" enctype="multipart/form-data" method="post" id="import_schedule_excel_form">
            <input type="hidden" name="school_username" id="school_username" value="{$username}"/>
            <input type="hidden" name="do" value="add" id="do"/>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left">{__("Schedule name")} (*)</label>
                <div class="col-sm-9">
                    <input class="form-control" name="schedule_name" required autofocus maxlength="100">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left">{__("Scope")} (*)</label>
                <div class="col-sm-3">
                    <select name="level" id="schedule_level" class="form-control" data-username = "{$username}" data-id = "{$school['page_id']}">
                        <option value="{$smarty.const.SCHOOL_LEVEL}">{__("School")}</option>
                        <option value="{$smarty.const.CLASS_LEVEL_LEVEL}">{__("Class level")}</option>
                        <option value="{$smarty.const.CLASS_LEVEL}">{__("Class")}</option>
                    </select>
                </div>
                <div class="col-sm-3 x-hidden" name="schedule_class_level">
                    <select name="class_level_id" id="class_level_id" class="form-control">
                        <option value="0">{__("Select class level")}</option>
                        {foreach $class_levels as $class_level}
                            <option value="{$class_level['class_level_id']}">{$class_level['class_level_name']}</option>
                        {/foreach}
                    </select>
                </div>
                <div class="col-sm-3 x-hidden" name="schedule_class">
                    <select name="class_id" id="class_id" data-username="{$username}" class="form-control">
                        <option value="0">{__("Select class")}</option>
                        {$class_level = -1}
                        {foreach $classes as $class}
                            {if ($class_level != $class['class_level_id'])}
                                <option value="" disabled style="color: blue">-----{$class['class_level_name']}-----</option>
                            {/if}
                            <option value="{$class['group_id']}">{$class['group_title']}</option>
                            {$class_level = $class['class_level_id']}
                        {/foreach}
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left">{__("Begin")} (*)</label>
                <div class='col-sm-3'>
                    <div class='input-group date' id='schedule_beginpicker_new'>
                        <input type='text' name="begin" class="form-control" placeholder="{__("Begin")} (*)" value="{$monday}" required/>
                        <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left"></label>
                <div class="col-sm-9">
                    <strong> {__('Note: start date must be monday')} </strong>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left">{__("Description")}</label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="description" rows="3"></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left">{__("Have active column")}?</label>
                <div class="col-sm-1">
                    <div class="onoffswitch">
                        <input type="checkbox" name="use_cate" class="onoffswitch-checkbox" id="use_category" checked>
                        <label class="onoffswitch-label" for="use_category"></label>
                    </div>
                </div>
                <label class="col-sm-2 control-label text-left">{__("Study saturday")}?</label>
                <div class="col-sm-2">
                    <div class="onoffswitch">
                        <input type="checkbox" name="study_saturday" class="onoffswitch-checkbox" id="study_saturday" checked>
                        <label class="onoffswitch-label" for="study_saturday"></label>
                    </div>
                </div>
                <label class="col-sm-2 control-label text-left">{__("Notify immediately")}?</label>
                <div class="col-sm-2">
                    <div class="onoffswitch">
                        <input type="checkbox" name="notify_immediately" class="onoffswitch-checkbox" id="notify_immediately">
                        <label class="onoffswitch-label" for="notify_immediately"></label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left">{__("Import from Excel file")}?</label>
                <div class="col-sm-9">
                    <div class="onoffswitch">
                        <input type="checkbox" name="import_excel" class="onoffswitch-checkbox" id="import_excel">
                        <label class="onoffswitch-label" for="import_excel"></label>
                    </div>
                </div>
            </div>
            <div class="table-responsive" id="cate_list">
                <table style="margin-bottom: 0px" class="table table-striped table-bordered table-hover" id = "myTableSchedule">
                    <thead>
                    <tr>
                        <th>
                            {__('#')}
                        </th>
                        <th>
                            {__('Option')}
                        </th>
                        <th>
                            {__('Time')}
                        </th>
                        <th>
                            {__('Activity')}
                        </th>
                        <th>
                            {__('Monday')}
                        </th>
                        <th>
                            {__('Tuesday')}
                        </th>
                        <th>
                            {__('Wednesday')}
                        </th>
                        <th>
                            {__('Thursday')}
                        </th>
                        <th>
                            {__('Friday')}
                        </th>
                        <th>
                            {__('Saturday')}
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td align="center" class = "col_no">1</td>
                        <td align="center">
                            <a class = "btn btn-default js_cate-delete_null"> {__("Delete")} </a>
                        </td>
                        <td>
                            <input type = "text" name = "start[]" placeholder="12:00" style = "padding-left: 10px; height: 35px; min-width: 200px;">
                        </td>
                        <td>
                            <input type="text" name = "category[]" placeholder="Tập thể dục" style = "padding-left: 10px; height: 35px; min-width: 200px;">
                        </td>
                        <td>
                            <textarea class = "note" type="text" name = "subject_detail_mon[]"> </textarea>
                        </td>
                        <td>
                            <textarea class = "note" type="text" name = "subject_detail_tue[]"> </textarea>
                        </td>
                        <td>
                            <textarea class = "note" type="text" name = "subject_detail_wed[]"> </textarea>
                        </td>
                        <td>
                            <textarea class = "note" type="text" name = "subject_detail_thu[]"> </textarea>
                        </td>
                        <td>
                            <textarea class = "note" type="text" name = "subject_detail_fri[]"> </textarea>
                        </td>
                        <td>
                            <textarea class = "note" type="text" name = "subject_detail_sat[]"> </textarea>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <div class = "form-group">
                    <div class = "col-sm-12">
                        <a class="btn btn-default js_cate-add-schedule">+</a>
                    </div>
                </div>
            </div>
            <div class="hidden" id="select_excel">
                <div class="form-group color_red">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-9">
                        <strong>- {__("You must use the Excel file format specified by the system, download form at")} <a target="_blank" href="https://drive.google.com/file/d/1XtB3hnhu453FSsg8hkSldDZzvopKiCG0">{__("HERE")}</a>.</strong>
                        <br/>
                        - {__("You must")} <strong>{__("configure")}</strong> {__("the schedule before SAVING")}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Select Excel file")}</label>
                    <div class="col-sm-6">
                        <input type="file" name="file" id="file"/>
                    </div>
                </div>
            </div>
            <div class="form-group mt10">
                <div class="col-sm-9 col-sm-offset-3">
                    <a href="#" id="preview_normal" class="btn btn-default text-left js_school-schedule-preview">{__("Preview")}</a>
                    <button type="submit" id="preview_excel" class="btn btn-default text-left js_school-schedule-preview-excel x-hidden">{__("Preview")}</button>
                    <button type="submit" id="submit" class="btn btn-primary padrl30 text-left schedule_submit">{__("Save")}</button>
                </div>
            </div>
            <br/>
            <div class="table-responsive" id="result_info" name="result_info"></div>
            <!-- success -->
            <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
            <!-- success -->
            <!-- error -->
            <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
            <!-- error -->

        </form>
    </div>

{elseif $sub_view == "edit"}
    <div class="panel-body">
        <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_schedule.php">
            <input type="hidden" name="school_username" id="school_username" value="{$username}"/>
            <input type="hidden" name="do" value="edit"/>
            <input type="hidden" name="schedule_id" value="{$data['schedule_id']}"/>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left">{__("Schedule name")} (*)</label>
                <div class="col-sm-9">
                    <input class="form-control" name="schedule_name" required autofocus maxlength="100" value = "{$data['schedule_name']}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left">{__("Scope")} (*)</label>
                <div class="col-sm-3">
                    <select name="level" id="cate_level" class="form-control">
                        <option value="{$smarty.const.SCHOOL_LEVEL}" {if $data['applied_for'] == $smarty.const.SCHOOL_LEVEL} selected {/if}>{__("School")}</option>
                        <option value="{$smarty.const.CLASS_LEVEL_LEVEL}" {if $data['applied_for'] == $smarty.const.CLASS_LEVEL_LEVEL} selected {/if}>{__("Class level")}</option>
                        <option value="{$smarty.const.CLASS_LEVEL}" {if $data['applied_for'] == $smarty.const.CLASS_LEVEL} selected {/if}>{__("Class")}</option>
                    </select>
                </div>
                <div class="col-sm-3 {if $data['applied_for'] != $smarty.const.CLASS_LEVEL_LEVEL}x-hidden{/if}" name="cate_class_level">
                    <select name="class_level_id" id="class_level_id" class="form-control">
                        <option value="0"> {__('Select class level')}</option>
                        {foreach $class_levels as $class_level}
                            <option value="{$class_level['class_level_id']}" {if $data['class_level_id']==$class_level['class_level_id']}selected{/if}>{$class_level['class_level_name']}</option>
                        {/foreach}
                    </select>
                </div>
                <div class="col-sm-3 {if $data['applied_for'] != ($smarty.const.CLASS_LEVEL)}x-hidden{/if}" name="cate_class">
                    <select name="class_id" id="class_id" data-username="{$username}" class="form-control">
                        <option value="0"> {__('Select class')} </option>
                        {$class_level = -1}
                        {foreach $classes as $class}
                            {if ($class_level != $class['class_level_id'])}
                                <option value="" disabled style="color: blue">-----{$class['class_level_name']}-----</option>
                            {/if}
                            <option value="{$class['group_id']}" {if $data['class_id']==$class['group_id']}selected{/if}>{$class['group_title']}</option>
                            {$class_level = $class['class_level_id']}
                        {/foreach}
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left">{__("Begin")} (*)</label>
                <div class='col-sm-3'>
                    <div class='input-group date' id='schedule_beginpicker_new'>
                        <input type='text' name="begin" class="form-control" placeholder="{__("Begin")} (*)" value="{$data['begin']}" required/>
                        <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left"></label>
                <div class="col-sm-9">
                    <strong> {__('Note: start date must be monday')} </strong>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left">{__("Description")}</label>
                <div class="col-sm-9">
                    <textarea class="form-control js_autosize" name="description" rows="3">{$data['description']}</textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left">{__("Have active column")}?</label>
                <div class="col-sm-1">
                    <div class="onoffswitch">
                        <input type="checkbox" name="use_category" class="onoffswitch-checkbox" id="use_category" {if $data['is_category']}checked{/if}>
                        <label class="onoffswitch-label" for="use_category"></label>
                    </div>
                </div>
                <label class="col-sm-2 control-label text-left">{__("Study saturday")}?</label>
                <div class="col-sm-1">
                    <div class="onoffswitch">
                        <input type="checkbox" name="study_saturday" class="onoffswitch-checkbox" id="study_saturday" {if $data['is_saturday']}checked{/if}>
                        <label class="onoffswitch-label" for="study_saturday"></label>
                    </div>
                </div>
                <label class="col-sm-3 control-label text-left">{__("Notify immediately")}?</label>
                <div class="col-sm-1">
                    <div class="onoffswitch">
                        <input type="checkbox" name="notify_immediately" class="onoffswitch-checkbox" id="notify_immediately" {if $data['is_notified']}checked{/if}>
                        <label class="onoffswitch-label" for="notify_immediately"></label>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover" id = "myTableSchedule">
                    <thead>
                    <tr>
                        <th>
                            {__('#')}
                        </th>
                        <th>
                            {__('Option')}
                        </th>
                        <th>
                            {__('Time')}
                        </th>
                        <th>
                            {__('Activity')}
                        </th>
                        <th>
                            {__('Monday')}
                        </th>
                        <th>
                            {__('Tuesday')}
                        </th>
                        <th>
                            {__('Wednesday')}
                        </th>
                        <th>
                            {__('Thursday')}
                        </th>
                        <th>
                            {__('Friday')}
                        </th>
                        <th>
                            {__('Saturday')}
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    {$idx = 1}
                    {foreach $data['details'] as $detail}
                        <tr>
                            <td align="center" class = "col_no">
                                {$idx}
                                <input type="hidden" name="schedule_detail_id[]" value="{$detail['schedule_detail_id']}"/>
                            </td>
                            <td align="center">
                                <a class = "btn btn-default js_cate-delete_null"> {__("Delete")} </a>
                            </td>
                            <td>
                                <input type="text" name="start[]" value="{$detail['subject_time']}" class="form-control" style = "min-width: 200px;"/>
                            </td>
                            <td>
                                <input type = "text" name="category[]" value="{$detail['subject_name']}" class = "form-control" style = "min-width: 200px;"/>
                            </td>
                            <td>
                                <textarea type = "text" class = "text-left form-control note" name = "subject_detail_mon[]">{$detail['monday']}</textarea>
                            </td>
                            <td>
                                <textarea type = "text" class = "text-left form-control note" name = "subject_detail_tue[]">{$detail['tuesday']}</textarea>
                            </td>
                            <td>
                                <textarea type = "text" class = "text-left form-control note" name = "subject_detail_wed[]">{$detail['wednesday']}</textarea>
                            </td>
                            <td>
                                <textarea type = "text" class = "text-left form-control note" name = "subject_detail_thu[]">{$detail['thursday']}</textarea>
                            </td>
                            <td>
                                <textarea type = "text" class = "text-left form-control note" name = "subject_detail_fri[]">{$detail['friday']}</textarea>
                            </td>
                            <td>
                                <textarea type = "text" class = "text-left form-control note" name = "subject_detail_sat[]">{$detail['saturday']}</textarea>
                            </td>
                        </tr>
                        {$idx = $idx + 1}
                    {/foreach}
                    </tbody>
                </table>
                <div class = "form-group">
                    <div class = "col-sm-12">
                        <a class="btn btn-default js_cate-add-schedule">+</a>
                    </div>
                </div>
            </div>
            <div class = "form-group">
                <div class="col-sm-9 mt20">
                    {*<a class="btn btn-default js_cate-add-schedule">{__("Add new row")}</a>*}
                    <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                </div>
            </div>
            <!-- success -->
            <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
            <!-- success -->
            <!-- error -->
            <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
            <!-- error -->
        </form>
    </div>
{elseif $sub_view == "import"}
    <div class="panel-body">
        <form class="form-horizontal" action="#" enctype="multipart/form-data" method="post" id="import_schedule_excel_form">
            <input type="hidden" name="school_username" id="school_username" value="{$username}"/>
            <input type="hidden" name="do" value="import"/>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left">{__("Schedule name")} (*)</label>
                <div class="col-sm-9">
                    <input class="form-control" name="schedule_name" required autofocus maxlength="100">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left">{__("Scope")} (*)</label>
                <div class="col-sm-3">
                    <select name="level" id="schedule_level" class="form-control" data-username = "{$username}" data-id = "{$school['page_id']}">
                        <option value="{$smarty.const.SCHOOL_LEVEL}">{__("School")}</option>
                        <option value="{$smarty.const.CLASS_LEVEL_LEVEL}">{__("Class level")}</option>
                        <option value="{$smarty.const.CLASS_LEVEL}">{__("Class")}</option>
                    </select>
                </div>
                <div class="col-sm-3 x-hidden" name="schedule_class_level">
                    <select name="class_level_id" id="class_level_id" class="form-control">
                        <option value="0">{__("Select class level")}</option>
                        {foreach $class_levels as $class_level}
                            <option value="{$class_level['class_level_id']}">{$class_level['class_level_name']}</option>
                        {/foreach}
                    </select>
                </div>
                <div class="col-sm-3 x-hidden" name="schedule_class">
                    <select name="class_id" id="class_id" data-username="{$username}" class="form-control">
                        <option value="0">{__("Select class")}</option>
                        {$class_level = -1}
                        {foreach $classes as $class}
                            {if ($class_level != $class['class_level_id'])}
                                <option value="" disabled style="color: blue">-----{$class['class_level_name']}-----</option>
                            {/if}
                            <option value="{$class['group_id']}">{$class['group_title']}</option>
                            {$class_level = $class['class_level_id']}
                        {/foreach}
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left">{__("Begin")} (*)</label>
                <div class='col-sm-3'>
                    <div class='input-group date' id='schedule_beginpicker_new'>
                        <input type='text' name="begin" class="form-control" placeholder="{__("Begin")} (*)" required/>
                        <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left"></label>
                <div class="col-sm-9">
                    <strong> {__('Note: start date must be monday')} </strong>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left">{__("Use category")}?</label>
                <div class="col-sm-9">
                    <div class="onoffswitch">
                        <input type="checkbox" name="use_category" class="onoffswitch-checkbox" id="use_category" checked>
                        <label class="onoffswitch-label" for="use_category"></label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left">{__("Study saturday")}?</label>
                <div class="col-sm-9">
                    <div class="onoffswitch">
                        <input type="checkbox" name="study_saturday" class="onoffswitch-checkbox" id="study_saturday" checked>
                        <label class="onoffswitch-label" for="study_saturday"></label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left">{__("Notify immediately")}?</label>
                <div class="col-sm-9">
                    <div class="onoffswitch">
                        <input type="checkbox" name="notify_immediately" class="onoffswitch-checkbox" id="notify_immediately" checked>
                        <label class="onoffswitch-label" for="notify_immediately"></label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left">{__("Description")}</label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="description" rows="6"></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left">{__("Select Excel file")}</label>
                <div class="col-sm-6">
                    <input type="file" name="file" id="file" {if count($classes) <= 0}disabled{/if}/>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-9 col-sm-offset-3">
                    <button type="submit" id="submit_id" class="btn btn-primary padrl30" {if count($classes) <= 0}disabled{/if}>{__("Save")}</button>
                </div>
            </div>
            <br/>
            <div class="table-responsive" id="result_info" name="result_info"></div>
            <!-- success -->
            <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
            <!-- success -->

            <!-- error -->
            <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
            <!-- error -->
        </form>
    </div>
{elseif $sub_view == "copy"}
    <div class="panel-body">
        <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_schedule.php">
            <input type="hidden" name="school_username" id="school_username" value="{$username}"/>
            <input type="hidden" name="do" value="copy"/>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left">{__("Schedule name")} (*)</label>
                <div class="col-sm-9">
                    <input class="form-control" name="schedule_name" required autofocus maxlength="100" value = "{$data['schedule_name']}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left">{__("Scope")} (*)</label>
                <div class="col-sm-3">
                    <select name="level" id="cate_level" class="form-control">
                        <option value="{$smarty.const.SCHOOL_LEVEL}" {if $data['applied_for'] == $smarty.const.SCHOOL_LEVEL} selected {/if}>{__("School")}</option>
                        <option value="{$smarty.const.CLASS_LEVEL_LEVEL}" {if $data['applied_for'] == $smarty.const.CLASS_LEVEL_LEVEL} selected {/if}>{__("Class level")}</option>
                        <option value="{$smarty.const.CLASS_LEVEL}" {if $data['applied_for'] == $smarty.const.CLASS_LEVEL} selected {/if}>{__("Class")}</option>
                    </select>
                </div>
                <div class="col-sm-3 {if $data['applied_for'] != $smarty.const.CLASS_LEVEL_LEVEL}x-hidden{/if}" name="cate_class_level">
                    <select name="class_level_id" id="class_level_id" class="form-control">
                        <option value="0"> {__('Select class level')}</option>
                        {foreach $class_levels as $class_level}
                            <option value="{$class_level['class_level_id']}" {if $data['class_level_id']==$class_level['class_level_id']}selected{/if}>{$class_level['class_level_name']}</option>
                        {/foreach}
                    </select>
                </div>
                <div class="col-sm-3 {if $data['applied_for'] != ($smarty.const.CLASS_LEVEL)}x-hidden{/if}" name="cate_class">
                    <select name="class_id" id="class_id" data-username="{$username}" class="form-control">
                        <option value="0"> {__('Select class')} </option>
                        {$class_level = -1}
                        {foreach $classes as $class}
                            {if ($class_level != $class['class_level_id'])}
                                <option value="" disabled style="color: blue">-----{$class['class_level_name']}-----</option>
                            {/if}
                            <option value="{$class['group_id']}" {if $data['class_id']==$class['group_id']}selected{/if}>{$class['group_title']}</option>
                            {$class_level = $class['class_level_id']}
                        {/foreach}
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left">{__("Begin")} (*)</label>
                <div class='col-sm-3'>
                    <div class='input-group date' id='schedule_beginpicker_new'>
                        <input type='text' name="begin" class="form-control" placeholder="{__("Begin")} (*)" value="{$data['begin']}" required/>
                        <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left"></label>
                <div class="col-sm-9">
                    <strong> {__('Note: start date must be monday')} </strong>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left">{__("Use category")}?</label>
                <div class="col-sm-9">
                    <div class="onoffswitch">
                        <input type="checkbox" name="use_category" class="onoffswitch-checkbox" id="use_category" {if $data['is_category']}checked{/if}>
                        <label class="onoffswitch-label" for="use_category"></label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left">{__("Study saturday")}?</label>
                <div class="col-sm-9">
                    <div class="onoffswitch">
                        <input type="checkbox" name="study_saturday" class="onoffswitch-checkbox" id="study_saturday" {if $data['is_saturday']}checked{/if}>
                        <label class="onoffswitch-label" for="study_saturday"></label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left">{__("Notify immediately")}?</label>
                <div class="col-sm-9">
                    <div class="onoffswitch">
                        <input type="checkbox" name="notify_immediately" class="onoffswitch-checkbox" id="notify_immediately" checked>
                        <label class="onoffswitch-label" for="notify_immediately"></label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left">{__("Description")} (*)</label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="description" rows="6">{$data['description']}</textarea>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover" id = "myTableSchedule">
                    <thead>
                    <tr>
                        <th>
                            {__('#')}
                        </th>
                        <th>
                            {__('Option')}
                        </th>
                        <th>
                            {__('Time')}
                        </th>
                        <th>
                            {__('Activity')}
                        </th>
                        <th>
                            {__('Monday')}
                        </th>
                        <th>
                            {__('Tuesday')}
                        </th>
                        <th>
                            {__('Wednesday')}
                        </th>
                        <th>
                            {__('Thursday')}
                        </th>
                        <th>
                            {__('Friday')}
                        </th>
                        <th>
                            {__('Saturday')}
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    {$idx = 1}
                    {foreach $data['details'] as $detail}
                        <tr>
                            <td align="center" class = "col_no">
                                {$detail['index']}
                                <input type="hidden" name="schedule_detail_id[]" value="{$detail['schedule_detail_id']}"/>
                            </td>
                            <td align="center">
                                <a class = "btn btn-default js_cate-delete_null"> {__('Delete')} </a>
                            </td>
                            <td>
                                <input type="text" name="start[]" class="form-control" value = "{$detail['subject_time']}" style = "min-width: 200px;">
                            </td>
                            <td>
                                <input type = "text" name="category[]" value="{$detail['subject_name']}" class = "form-control" style = "min-width: 200px;"/>
                            </td>
                            <td>
                                <textarea type = "text" class = "text-left form-control note" name = "subject_detail_mon[]">{$detail['monday']}</textarea>
                            </td>
                            <td>
                                <textarea type = "text" class = "text-left form-control note" name = "subject_detail_tue[]">{$detail['tuesday']}</textarea>
                            </td>
                            <td>
                                <textarea type = "text" class = "text-left form-control note" name = "subject_detail_wed[]">{$detail['wednesday']}</textarea>
                            </td>
                            <td>
                                <textarea type = "text" class = "text-left form-control note" name = "subject_detail_thu[]">{$detail['thursday']}</textarea>
                            </td>
                            <td>
                                <textarea type = "text" class = "text-left form-control note" name = "subject_detail_fri[]">{$detail['friday']}</textarea>
                            </td>
                            <td>
                                <textarea type = "text" class = "text-left form-control note" name = "subject_detail_sat[]">{$detail['saturday']}</textarea>
                            </td>
                        </tr>
                    {/foreach}
                    </tbody>
                </table>
            </div>
            <div class = "form-group">
                <div class="col-sm-12 mt20">
                    <a class="btn btn-default js_cate-add-schedule">{__("Add new row")}</a>
                    <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                </div>
            </div>
            <!-- success -->
            <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
            <!-- success -->
            <!-- error -->
            <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
            <!-- error -->
        </form>
    </div>
{/if}
</div>