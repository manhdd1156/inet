<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <i class="fa fa-child fa-fw fa-lg pr10"></i>
        {__("Login statistics")}
    </div>
    {if $sub_view == ""}
        <div class="panel-body with-table">
            <div class="row">
                <div class='col-sm-3'>
                    <div class="form-group">
                        <select name="class_id" id="class_id" class="form-control">
                            <option value="">{__("Select Class")}...</option>
                            {if $school['children_use_no_class']}
                                <option value="0" {if $result['class_id'] == '0'}selected{/if}>{__("No class")}</option>
                            {/if}
                            {$class_level = -1}
                            {foreach $classes as $class}
                                {if ($class_level != $class['class_level_id'])}
                                    <option value="" disabled>-----{$class['class_level_name']}-----</option>
                                {/if}
                                <option value="{$class['group_id']}" {if $result['class_id'] == $class['group_id']}selected{/if}>{$class['group_title']}</option>
                                {$class_level = $class['class_level_id']}
                            {/foreach}
                        </select>
                    </div>
                </div>
                <div class='col-sm-2'>
                    <div class = "row">
                        <div class = "col-sm-6">
                            <div class="form-group">
                                <a href="#" id="search" class="btn btn-default js_child-statistics-search" data-username="{$username}" data-isnew="1" data-id="{$school['page_id']}">{__("Search")}</a>
                                <label id="loading" class="btn btn-info x-hidden">{__("Loading")}...</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br/>
            <div class="table-responsive" id="child_list" name="child_list">
                {include file="ci/school/ajax.school.children.statistics.list.tpl"}
            </div>
        </div>
    {/if}
</div>