<div class="table-responsive">
    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <th>#</th>
            <th nowrap="true">{__("Full name")}</th>
            {foreach $dates as $date}
                <th align="center">{$date}</th>
            {/foreach}
            <th style="width: 200px">{__("Assigned")}</th>
        </tr>
        </thead>
        <tbody>
        {$date_count = $dates|count}
        {$rowIdx = 1}
        {foreach $rows as $row}
            <tr>
                <td align="center">{$rowIdx}</td>
                <td nowrap="true"><a>{$row['teacher_name']}</a></td>

                {$rowIdx = $rowIdx + 1}
                {$presentCnt = 0}
                {$totalCnt = 0}
                {foreach $row['cells'] as $cell}
                    {if $cell['is_checked'] == 1}
                        <td align="center">
                            <a href="{$system['system_url']}/school/{$username}/pickup/detail/{$cell['pickup_id']}">
                                <i class="fa fa-check" style="color:mediumseagreen" aria-hidden="true"></i>
                            </a>
                        </td>
                        {$presentCnt = $presentCnt + 1}
                    {else}
                        <td bgcolor="#a9a9a9"></td>
                    {/if}
                    </td>
                    {$totalCnt = $totalCnt + 1}
                {/foreach}
                <td align="center">
                    <strong>
                        <font color="blue">{$presentCnt}</font>/<font color="red">{$totalCnt}</font>
                    </strong></td>
            </tr>
        {/foreach}
        {if ($rowIdx >= 2)}
            <tr><td colspan="{$totalCnt+2}"></td></tr>
            <tr>
                <td colspan="2" align="center"><strong>{__('Number of teachers')}</strong></td>
                {foreach $last_rows as $cell}
                    <td align="center" bgcolor="#ffebcd">{if $cell['is_checked'] == 1}<strong><font color="blue">{$cell['pickup_count']}</font></strong>{/if}</td>
                {/foreach}
            </tr>
        {else}
            <tr class="odd">
                <td valign="top" align="center" colspan="{$date_count+3}" class="dataTables_empty">
                    {__("No data available in table")}
                </td>
            </tr>
        {/if}

        </tbody>
    </table>
</div>