<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <i class="fa fa-envelope fa-fw fa-lg pr10"></i>
        {__("Parent feedback")} › {__("Lists")}
    </div>
    {if $sub_view == ""}
        <div class="panel-body with-table">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_feedback.php">
                <input type="hidden" name="school_username" id="school_username" value="{$username}"/>
                <input type="hidden" name="do" value="search"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Select class")}</label>
                    <div class="col-sm-9">
                        <div class="col-sm-4">
                            <select name="class_id" id="class_id" data-username="{$username}" class="form-control">
                                <option value="0"> {__('Select class')} </option>
                                {foreach $classes as $class}
                                    <option value="{$class['group_id']}">{$class['group_title']}</option>
                                {/foreach}
                            </select>
                        </div>
                        <div class="col-sm-3">
                            <a class="btn btn-default js_school-feedback-search" data-username = "{$username}" data-handle = "search">{__("Search")}</a>
                        </div>
                    </div>
                </div>
            </form>
            <div id="feedback_list">
                {include file="ci/school/ajax.school.feedbacklist.tpl"}
            </div>
        </div>
    {/if}
</div>