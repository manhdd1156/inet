{*Hiển thị 1 dịch vụ để ước lượng phí tháng hiện tại*}
<tr>
    <td class="text-center">
        +
        <input type="hidden" class="service_id_{$child_id}" name="service_id_{$child_id}[]" value="{$service['service_id']}"/>
    </td>
    <td>{$service['service_name']}</td>
    <td>
        <input  type="number" class="text-center service_quantity" id="service_quantity_{$child_id}_{$service['service_id']}" child_id="{$child_id}" service_id="{$service['service_id']}" name="service_quantity_{$child_id}[]"
                {if $service['type']==$smarty.const.SERVICE_TYPE_MONTHLY}value="1" readonly style="width: 40px; background-color:#EEEEEE"{else}min="0" value="0" step="1" style="width: 40px;"{/if}/>
        <input type="hidden" id="service_type_{$child_id}_{$service['service_id']}" name="service_type_{$child_id}[]" value="{$service['type']}"/>
    </td>
    <td>
        <input style="width: 100px;" type="text" class="text-right service_unit_price money_tui" id="service_unit_price_{$child_id}_{$service['service_id']}" child_id="{$child_id}" service_id="{$service['service_id']}"
               name="service_unit_price_{$child_id}[]" value="{$service['fee']}"/>
    </td>
    <td>
        {if $service['type'] == $smarty.const.SERVICE_TYPE_MONTHLY}
            <input  style="width: 90px;" type="text" class="text-right service_unit_price_deduction money_tui" id="service_unit_price_deduction_{$child_id}_{$service['service_id']}" child_id="{$child_id}"
                    service_id="{$service['service_id']}" name="service_unit_price_deduction_{$child_id}[]" value="" step="1" placeholder="({$smarty.const.MONEY_UNIT})"/>
            <input type="hidden" name="service_quantity_deduction_{$child_id}[]" value="1"/>
        {elseif $service['type'] == $smarty.const.SERVICE_TYPE_DAILY}
            <input  style="width: 40px;" type="number" class="text-center service_quantity_deduction" id="service_quantity_deduction_{$child_id}_{$service['service_id']}" child_id="{$child_id}"
                    service_id="{$service['service_id']}" name="service_quantity_deduction_{$child_id}[]" value="0" step="1"/>({__('Daily')})
            <input type="hidden" id="service_unit_price_deduction_{$child_id}_{$service['service_id']}" name="service_unit_price_deduction_{$child_id}[]" value="{$service['fee']}"/>
        {else}
            <input  style="width: 40px;" type="number" class="text-center service_quantity_deduction" id="service_quantity_deduction_{$child_id}_{$service['service_id']}" child_id="{$child_id}"
                    service_id="{$service['service_id']}" name="service_quantity_deduction_{$child_id}[]" value="0" step="1"/>({__('Times')})
            <input type="hidden" id="service_unit_price_deduction_{$child_id}_{$service['service_id']}" name="service_unit_price_deduction_{$child_id}[]" value="{$service['fee']}"/>
        {/if}
    </td>
    <td><input style="width: 100px; background-color: #EEEEEE" type="text" class="text-right money_tui" id="service_amount_{$child_id}_{$service['service_id']}" name="service_amount_{$child_id}[]"
               value="{if $service['type']==$smarty.const.SERVICE_TYPE_MONTHLY}{$service['fee']}{else}0{/if}" readonly/></td>
    <td align="center"><button class="btn-danger js_del_tuition_item" child_id="{$child_id}">{__("Delete")}</button></td>
</tr>