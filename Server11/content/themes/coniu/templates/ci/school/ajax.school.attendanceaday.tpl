<div><strong>{__("Attendance list")}&nbsp;({$rows|count}&nbsp;{__("Class")})</strong></div>
<table class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th>#</th>
        {if $class_id <= 0}<th>{__("Class")}</th>{/if}
        <th>{__("Status")}</th>
        <th>{__("Number of student")}</th>
        <th>{__("Present")}</th>
        <th>{__("Absence")}</th>
        <th>{__("Time")}</th>
        <th>{__("Actions")}</th>
    </tr>
    </thead>
    <tbody>
    {$totalPresent = 0}
    {$totalAbsence = 0}
    {$total = 0}
    {$idx = 1}

    {$classLevelId = -1}
    {$classLevelPresent = 0}
    {$classLevelAbsence = 0}
    {$classLevelTotal = 0}

    {foreach $rows as $row}
        {*Tong ket cua khoi truoc do*}
        {if ($idx == 1)}
            <tr><td colspan="8" align="left"><strong>{$row['class_level_name']}</strong></td></tr>
        {elseif ($idx > 1) && ($classLevelId != $row['class_level_id'])}
            <tr>
                <td colspan="3" align="center"><strong><font color="blue">{__('Total of grade')}</font></strong></td>
                <td align="center"><strong><font color="blue">{$classLevelTotal}</font></strong></td>
                <td align="center"><strong><font color="blue">{if {$classLevelPresent} > 0}{$classLevelPresent}{/if}</font></strong></td>
                <td align="center"><strong><font color="blue">{if {$classLevelPresent} > 0}{$classLevelAbsence}{/if}</font></strong></td>
                <td colspan="2"></td>
            </tr>
            <tr><td colspan="8"></td></tr>
            {$classLevelPresent = 0}
            {$classLevelAbsence = 0}
            {$classLevelTotal = 0}
            <tr><td colspan="8" align="left"><strong>{$row['class_level_name']}</strong></td></tr>
        {/if}
        <tr>
            <td align="center">{$idx}</td>
            {if $class_id <= 0}
                <td>
                    <a href="{$system['system_url']}/school/{$username}/attendance/rollup/{$row['group_id']}">{$row['group_title']}</a>
                </td>
            {/if}
            <td align="center">
                {if $row['is_checked']==0}
                    <i class="fa fa-times" aria-hidden="true"></i>
                {else}
                    <i class="fa fa-check" aria-hidden="true"></i>
                {/if}
            </td>
            <td align="center">{$row['total']}</td>
            <td align="center">{if $row['is_checked']}{$row['present_count']}{/if}</td>
            <td align="center">{if $row['is_checked']}{$row['absence_count']}{/if}</td>
            <td>{if $row['is_checked'] == 1}{$row['recorded_at']}{/if}</td>
            <td align="center">
                {if $row['is_checked'] == 1}
                    <a class="btn btn-xs btn-default" href="{$system['system_url']}/school/{$username}/attendance/detail/{$row['attendance_id']}">{__("Detail")}</a>
                {/if}
                {if canEdit($username, 'attendance')}
                    <a class="btn btn-xs btn-default" href="{$system['system_url']}/school/{$username}/attendance/rollup/{$row['group_id']}">{__("Roll up")}</a>
                {/if}
            </td>
            {$totalPresent = $totalPresent + $row['present_count']}
            {$totalAbsence = $totalAbsence + $row['absence_count']}
            {$idx = $idx + 1}
            {$classLevelId = $row['class_level_id']}
            {$classLevelPresent = $classLevelPresent + $row['present_count']}
            {$classLevelAbsence = $classLevelAbsence + $row['absence_count']}
            {$classLevelTotal = $classLevelTotal + $row['total']}
            {$total = $total + $row['total']}
        </tr>
    {/foreach}
    {if ($idx > 1)}
        <tr>
            <td colspan="3" align="center"><strong><font color="blue">{__('Total of grade')}</font></strong></td>
            <td align="center"><strong><font color="blue">{$classLevelTotal}</font></strong></td>
            <td align="center"><strong><font color="blue">{if $classLevelPresent > 0}{$classLevelPresent}{/if}</font></strong></td>
            <td align="center"><strong><font color="blue">{if $classLevelPresent > 0}{$classLevelAbsence}{/if}</font></strong></td>
            <td colspan="2"></td>
        </tr>
    {/if}
    <tr>
        <td colspan="3" align="center"><strong><font color="red">{__('TOTAL OF SCHOOL')}</font></strong></td>
        <td align="center"><strong><font color="red">{$total}</font></strong></td>
        <td align="center"><strong><font color="red">{if {$totalPresent} > 0}{$totalPresent}{/if}</font></strong></td>
        <td align="center"><strong><font color="red">{if {$totalPresent} > 0}{$totalAbsence}{/if}</font></strong></td>
        <td colspan="2"></td>
    </tr>
    </tbody>
</table>