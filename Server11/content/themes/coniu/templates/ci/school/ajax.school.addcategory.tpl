<div class="mt10" align="center">
    <strong>{__("Add new category and add suggest for category")}</strong>
</div>
<div class="panel-body with-table">
    <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_report.php">
        <input type="hidden" id="school_username" name="school_username" value="{$username}"/>
        <input type="hidden" name="do" value="add_cate_temp"/>
        <input type="hidden" name="report_template_id" value="{$data['report_template_id']}"/>
        <div class = "form-group">
            <label class="col-sm-3 control-label text-left">{__("Category name")} (*)</label>
            <div class="col-sm-9">
                <input class="form-control" name="category_name_add" required maxlength="300">
            </div>
        </div>
        <div class="table-responsive" id="cate_school_add">
            <table class="table table-striped table-bordered table-hover" id = "addCategoryTable">
                <thead>
                <tr><th colspan="3">{__("Suggest content for category")}</th></tr>
                <tr>
                    <th>
                        {__('No.')}
                    </th>
                    <th>
                        {__('Title')}
                    </th>
                    <th>
                        {__('Actions')}
                    </th>
                </tr>
                </thead>
                <tbody class="table_suggest">
                <tr>
                    <td class = "col_no align_middle"  align = "center">1</td>
                    <td>
                        <input type="text" name = "suggests_add" required class = "form-control suggest" placeholder="{__("Suggest content for category")}">
                    </td>
                    <td align="center" class="align_middle"> <a class="btn btn-danger btn-xs js_report_template-delete"> {__("Delete")} </a></td>
                </tr>
                </tbody>
            </table>
            <a class="btn btn-default js_report_suggest-add">{__("Add new row")}</a>
            <a class="btn btn-default js_school-category-add-done">{__("Add")}</a>
        </div>
        <!-- success -->
        <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
        <!-- success -->

        <!-- error -->
        <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
        <!-- error -->
    </form>
</div>