{* File này hiển thị thông tin học phí dự kiến của một trẻ *}
{$childId = $child['child_id']}
<div style = "color: #9f191f">
    <strong>{$childIdx} - {$child['child_name']} ({$child['month']} {__("Month")})</strong>
    {if !empty($child['pre_month'])}&nbsp;|&nbsp;{__("Number of charged days from last tuition")} ({__("Month")} {$child['pre_month']}):&nbsp;{$child['attendance_count']}{/if}
    <div class="pull-right flip">
        <a class="btn btn-xs btn-default js_display_fee_2_add" child_id="{$childId}">{__("Add")}</a>
        <a class="btn btn-xs btn-default js_reload_child_tuition" child_id="{$childId}" child_idx="{$childIdx}">{__("Reload")}</a>
        <a class="btn btn-xs btn-default js_show_hide_tuition_tbody" child_id="{$childId}">{__("Show/hide")}</a>
        <a class="btn btn-xs btn-danger js_new_tuition_del_child" child_id="{$childId}">{__("Delete")}</a>
    </div>
</div>
{*Số ngày đi học ước tính của riêng trẻ*}
<div class="mb10">
    {__("The number of school days in a student's month")} {$child['child_name']}
    <input style="width: 40px" type="number" name="day_of_child_{$childId}" id="day_of_child_{$childId}" class="day_of_child" value="{$attCount}" child_id="{$childId}">
</div>
{*Hiển thị thông tin điểm danh chi tiết của trẻ tháng trước*}
{if $school['tuition_view_attandance'] && (count($child['attendances']) > 0)}
    <table class="table table-striped table-bordered table-hover">
        <tr>
            <td style="padding-left: 5px; padding-right: 5px"><a href="{$system['system_url']}/school/{$username}/attendance/child/{$childId}" target="_blank"><strong>{__("ATT")}</strong></a></td>
            {foreach $child['attendances'] as $attendance}
                <td align="center" style="padding-left: 5px; padding-right: 5px"
                        {if $attendance['status'] == $smarty.const.ATTENDANCE_COME_LATE}
                            bgcolor="#6495ed"
                        {elseif $attendance['status'] == $smarty.const.ATTENDANCE_EARLY_LEAVE}
                            bgcolor="#008b8b"
                        {elseif $attendance['status'] == $smarty.const.ATTENDANCE_ABSENCE_NO_REASON}
                            bgcolor="#ff1493"
                        {elseif $attendance['status'] == $smarty.const.ATTENDANCE_ABSENCE}
                            bgcolor="#deb887"
                        {/if}
                >{$attendance['display_date']}</td>
            {/foreach}
        </tr>
    </table>
{/if}
<input type="hidden" name="child_id[]" value="{$childId}"/>
<input type="hidden" name="pre_month[]" value="{$child['pre_month']}"/>
<table class="table table-striped table-bordered table-hover" id="tbl_fee_{$childId}">
    <thead>
    <tr>
        <th>#</th>
        <th>{__("Fee name")}</th>
        <th style="width: 52px">{__("Qty")}</th>
        <th style="width: 117px">{__("Unit price")}</th>
        <th style="width: 107px">{__("Deduction")}</th>
        <th style="width: 117px">{__("Money amount")}</th>
        <th style="width: 80px">{__("Option")}</th>
    </tr>
    </thead>
    <tbody>
    {*Hiển thị các loại phí phải trả tháng hiện tại*}
    {$idx = 1}
    {foreach $child['fees'] as $fee}
        <tr>
            <td class="text-center">
                {$idx}
                <input type="hidden" class="fee_id_{$childId}" name="fee_id_{$childId}[]" value="{$fee['fee_id']}"/>
            </td>
            <td>{$fee['fee_name']}</td>
            <td>
                <input type="number" id="fee_quantity_{$childId}_{$fee['fee_id']}" child_id="{$childId}" fee_id="{$fee['fee_id']}" name="fee_quantity_{$childId}[]"
                       {if $fee['fee_type']==$smarty.const.FEE_TYPE_MONTHLY} class="text-center fee_quantity" value="1" readonly style="width: 35px; background-color: #EEEEEE"{else} class="text-center fee_quantity day_of_child_val_{$childId}" data-type="1" value="{$fee['quantity']}" step="1" style="width: 35px;"{/if}/>
            </td>
            <td>
                <input readonly style="width: 100px; background-color: #EEEEEE" type="text" class="text-right fee_unit_price money_tui" id="fee_unit_price_{$childId}_{$fee['fee_id']}" child_id="{$childId}" fee_id="{$fee['fee_id']}"
                       name="fee_unit_price_{$childId}[]" value="{$fee['unit_price']}"/>
                {if ($fee['fee_type']!=$smarty.const.FEE_TYPE_MONTHLY) && ($fee['unit_price']!=$fee['unit_price_deduction'])}
                    <br/>{__('Pm')}: {moneyFormat($fee['unit_price_deduction'])}
                {/if}
            </td>
            <td>
                {if $fee['fee_type'] == $smarty.const.FEE_TYPE_MONTHLY}
                    <input type="hidden" id="fee_quantity_deduction_{$childId}_{$fee['fee_id']}" name="fee_quantity_deduction_{$childId}[]" value="1"/>
                    <input style="width: 90px;" type="text" class="text-right fee_unit_price_deduction money_tui" id="fee_unit_price_deduction_{$childId}_{$fee['fee_id']}" child_id="{$childId}" fee_id="{$fee['fee_id']}"
                           name="fee_unit_price_deduction_{$childId}[]" value="" step="500" placeholder="({$smarty.const.MONEY_UNIT})"/>
                {else}
                    <input style="width: 35px;" type="number" class="text-center fee_quantity_deduction" id="fee_quantity_deduction_{$childId}_{$fee['fee_id']}" child_id="{$childId}" fee_id="{$fee['fee_id']}"
                           name="fee_quantity_deduction_{$childId}[]" value="{$fee['quantity_deduction']}" step="1"/>({__('Daily')})
                    <input type="hidden"  id="fee_unit_price_deduction_{$childId}_{$fee['fee_id']}" name="fee_unit_price_deduction_{$childId}[]" value="{$fee['unit_price_deduction']}"/>
                {/if}
            </td>
            <td><input style="width: 100px; background-color: #EEEEEE" type="text" class="text-right money_tui" id="fee_amount_{$childId}_{$fee['fee_id']}" name="fee_amount_{$childId}[]" value="{$fee['amount']}" readonly/></td>
            <td class="text-center"><button class="btn-danger js_del_tuition_item" child_id="{$childId}">{__("Delete")}</button></td>
        </tr>
        {$idx = $idx + 1}
    {/foreach}
    {*Đoạn dưới làm mốc để add các hàng cần thêm vào*}
    <tr id="fee_checkpoint_{$childId}"></tr>

    {*<tr><td colspan="7">Bat dau service</td></tr>*}
    {*Hiển thị các loại dịch vụ (CHỈ DỊCH VỤ THEO THÁNG VÀ THEO ĐIỂM DANH) sẻ dùng tháng hiện tại*}
    {foreach $child['services'] as $service}
        <tr>
            <td class="text-center">
                {$idx}
                <input type="hidden" class="service_id_{$childId}" name="service_id_{$childId}[]" value="{$service['service_id']}"/>
            </td>
            <td>
                {$service['service_name']}
                {if isset($service['monthly_daily_deduction']) && ($service['monthly_daily_deduction'] > 0)}
                    <i>({__("Deduction")}: {$service['monthly_daily_deduction']})</i>
                {/if}
            </td>
            <td>
                <input  type="number" id="service_quantity_{$childId}_{$service['service_id']}" child_id="{$childId}" service_id="{$service['service_id']}" name="service_quantity_{$childId}[]"
                        {if $service['service_type']==$smarty.const.SERVICE_TYPE_MONTHLY}class="text-center service_quantity" value="1" readonly style="width: 35px; background-color:#EEEEEE"{else}class="text-center service_quantity day_of_child_val_{$childId}" data-type="2" value="{$service['quantity']}" step="1" style="width: 35px;"{/if}/>
                <input type="hidden" id="service_type_{$childId}_{$service['service_id']}" name="service_type_{$childId}[]" value="{$service['service_type']}"/>
            </td>
            <td>
                <input style="width: 100px;" type="text" class="text-right service_unit_price money_tui" id="service_unit_price_{$childId}_{$service['service_id']}" child_id="{$childId}" service_id="{$service['service_id']}"
                       name="service_unit_price_{$childId}[]" value="{$service['fee']}"/>
                {if ($service['service_type']!=$smarty.const.SERVICE_TYPE_MONTHLY) && ($service['fee']!=$service['unit_price_deduction'])}
                    <br/>{__('Pm')}: {moneyFormat($service['unit_price_deduction'])}
                {/if}
            </td>
            <td>
                {if $service['service_type'] == $smarty.const.SERVICE_TYPE_MONTHLY}
                    <input  style="width: 90px;" type="text" class="text-right service_unit_price_deduction money_tui" id="service_unit_price_deduction_{$childId}_{$service['service_id']}" child_id="{$childId}"
                            service_id="{$service['service_id']}" name="service_unit_price_deduction_{$childId}[]" value="" step="1" placeholder="({$smarty.const.MONEY_UNIT})"/>
                    <input type="hidden" name="service_quantity_deduction_{$childId}[]" value="1"/>
                {else}
                    <input  style="width: 35px;" type="number" class="text-center service_quantity_deduction" id="service_quantity_deduction_{$childId}_{$service['service_id']}" child_id="{$childId}"
                            service_id="{$service['service_id']}" name="service_quantity_deduction_{$childId}[]" value="{$service['quantity_deduction']}" step="1"/>({__('Daily')})
                    <input type="hidden" id="service_unit_price_deduction_{$childId}_{$service['service_id']}" name="service_unit_price_deduction_{$childId}[]" value="{$service['unit_price_deduction']}"/>
                {/if}
            </td>
            <td>
                <input style="width: 100px; background-color: #EEEEEE" type="text" class="text-right money_tui" id="service_amount_{$childId}_{$service['service_id']}" name="service_amount_{$childId}[]" value="{$service['amount']}" readonly/>
            </td>
            <td align="center"><button class="btn-danger js_del_tuition_item" child_id="{$childId}">{__("Delete")}</button></td>
        </tr>
        {$idx = $idx + 1}
    {/foreach}
    <tr id="service_checkpoint_{$childId}"></tr>

    {*<tr><td colspan="7">Bat dau cb-service</td></tr>*}
    {* Các khoản phí sử dụng dịch vụ theo LẦN SỬ DỤNG ở tháng TRƯỚC *}
    {foreach $child['cbservices'] as $service}
        <tr>
            <td class="text-center">
                {$idx}
                <input type="hidden" class="service_id_{$childId}" name="service_id_{$childId}[]" value="{$service['service_id']}"/>
            </td>
            <td>{$service['service_name']}</td>
            <td>
                <input style="width: 35px;" type="number" class="text-center service_quantity" id="service_quantity_{$childId}_{$service['service_id']}" child_id="{$childId}" service_id="{$service['service_id']}"
                       name="service_quantity_{$childId}[]" value="{$service['CNT']}" step="1"/>
                <input type="hidden" id="service_type_{$childId}_{$service['service_id']}" name="service_type_{$childId}[]" value="{$smarty.const.SERVICE_TYPE_COUNT_BASED}"/>
            </td>
            <td>
                <input style="width: 100px;" type="text" class="text-right service_unit_price money_tui" id="service_unit_price_{$childId}_{$service['service_id']}" child_id="{$childId}" service_id="{$service['service_id']}"
                       name="service_unit_price_{$childId}[]" value="{$service['fee']}"/>
                <input type="hidden" id="service_unit_price_deduction_{$childId}_{$service['service_id']}" name="service_unit_price_deduction_{$childId}[]" value="{$service['fee']}"/>
            </td>
            <td>
                <input  style="width: 35px;" type="number" class="text-center service_quantity_deduction" id="service_quantity_deduction_{$childId}_{$service['service_id']}" child_id="{$childId}"
                        service_id="{$service['service_id']}" name="service_quantity_deduction_{$childId}[]" value="0"/>({__('Times')})
            </td>
            <td>
                <input style="width: 100px; background-color: #EEEEEE" type="text" class="text-right money_tui" id="service_amount_{$childId}_{$service['service_id']}" name="service_amount_{$childId}[]" value="{$service['amount']}" readonly/>
            </td>
            <td align="center"><button class="btn-danger js_del_tuition_item" child_id="{$childId}">{__("Delete")}</button></td>
        </tr>
        {$idx = $idx + 1}
    {/foreach}

    {* Phí đón muộn *}
    {if $child['pickup'] > 0}
        <tr>
            <td class="text-center">
                {$idx}
                <input type="hidden" name="pickup_{$childId}" value="1"/>
                <input type="hidden" class="service_id_{$childId}" name="service_id_{$childId}[]" value="0"/>
                <input type="hidden" name="service_type_{$childId}[]" value="0"/>
                <input type="hidden" id="service_quantity_{$childId}_0" name="service_quantity_{$childId}[]" value="1"/>
                <input type="hidden" id="service_quantity_deduction_{$childId}_0" name="service_quantity_deduction_{$childId}[]" value="0"/>
                <input type="hidden" id="service_unit_price_{$childId}_0" name="service_unit_price_{$childId}[]" value="{$child['pickup']}"/>
                <input type="hidden" id="service_unit_price_deduction_{$childId}_0" name="service_unit_price_deduction_{$childId}[]" value="0"/>
            </td>
            <td colspan="4">{__('Late PickUp')}</td>
            <td><input style="width: 100px;" type="text" class="text-right tuition_pick_up money_tui" step="1" id="service_amount_{$childId}_0" name="service_amount_{$childId}[]" child_id="{$childId}" value="{$child['pickup']}"/></td>
            <td align="center"><button class="btn-danger js_del_tuition_item" child_id="{$childId}">{__("Delete")}</button></td>
        </tr>
        {$idx = $idx + 1}
    {/if}
    <tr>
        <td class="text-center">{$idx}</td>
        <td colspan="4">{__('Debt amount of previous month')}&nbsp;{$child['pre_month']}</td>
        <td>
            <input style="width: 100px;" type="text" class="text-right tuition_debt_amount money_tui" id="debt_amount_{$childId}" name="debt_amount[]" child_id="{$childId}" value="{$child['pre_month_debt_amount']}"/>
        </td>
        <td></td>
    </tr>
    <tr>
        <td colspan="6">
            <div class="pull-right flip">
                <strong>
                    {__("Total")}({$smarty.const.MONEY_UNIT})&nbsp;
                    <input style="width: 100px; background-color: #EEEEEE" type="text" class="text-right money_tui" id="total_{$childId}" name="child_total[]" value="{$child['child_total_amount']}" readonly/>
                </strong>
            </div>
        </td>
        <td></td>
    </tr>
    <tr>
        <td colspan="2" class="text-right"><strong>{__("Description")}</strong></td>
        <td colspan="5" ><input type="text" name="detail_description[]" style="width: 100%" placeholder="{__("Detail description for this tuition (if necessary)")}" value="" maxlength="300"/></td>
    </tr>
    </tbody>
</table>
{*Hàm remove dấu , sau dấu -*}
<script>
    function stateChange() {
        setTimeout(function () {
            $('body .money_tui').each(function () {
                var a = $(this).val();
                a = a.toString().replace(/^-,/, '-');
                $(this).val(a);
            });
        }, 1000);
    }
    stateChange();
</script>
