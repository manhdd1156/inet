<tr>
    <td align="center" class = "align_middle">{$idx}</td>
    <td class="align_middle">
        <input type = "checkbox" name = "category_ids[]" value="{$category_new['report_template_category_id']}">
        {$category_new['category_name']}
    </td>
    <td>
        <textarea type="text" class="noteArea" name="content_{$category_new['report_template_category_id']}" style="width: 100%"></textarea>
    </td>
    <td align="center" class="align_middle">
        {if $reportTemplateId != null}
            {*<a href="{$system['system_url']}/school/{$username}/reports/editcate/{$category_new['report_template_category_id']}/2/{$reportTemplateId}" class="btn btn-xs btn-default">{__("Category suggests")}</a>*}
            <a class="btn btn-xs btn-default js_school-category-detail" data-id="{$category_new['report_template_category_id']}">{__("Category suggests")}</a>
        {else}
            <a class="btn btn-xs btn-default js_school-category-detail" data-id="{$category_new['report_template_category_id']}">{__("Category suggests")}</a>
        {/if}
    </td>
</tr>
{*jquery auto height*}
<script type="text/javascript">
    $('.noteArea').each(function () {
        this.setAttribute('style', 'height:35px;' + 'px;overflow-y:hidden;width:100%;resize:vertical');
    }).on('input', function () {
        this.style.height = 'auto';
        this.style.height = (this.scrollHeight) + 'px';
    });
</script>