<div class="panel panel-default">
    <div class="panel-heading with-icon">
        {if $canEdit}
            <div class="pull-right flip">
                {if $sub_view == ""}
                    <a href="{$system['system_url']}/school/{$username}/attendance/rollup" class="btn btn-danger">
                        <i class="fa fa-plus-circle"></i> {__("Roll up")}
                    </a>
                {/if}
            </div>
        {/if}
        <div class="pull-right flip" style="margin-right: 5px">
            <a href="https://blog.coniu.vn/huong-dan-diem-danh-tren-website-coniu-vn/" class="btn btn-info btn_guide" target="_blank">
                <i class="fa fa-info-circle"></i> {__("Guide")}
            </a>
        </div>
        <i class="fa fa-tasks fa-fw fa-lg pr10"></i>
        {__("Attendance")}
        {if $sub_view == "detail"}
            &rsaquo; {__("Attendance detail")} &rsaquo; {$data['group_title']}
        {elseif $sub_view == "child"}
            &rsaquo; {__("Student attendance summary")} {if $data['child'] != ''}&rsaquo; {$data['child']['child_name']}{/if}
        {elseif $sub_view == "classatt"}
            &rsaquo; {__("Class attendance summary")}
        {elseif $sub_view == "conabs"}
            &rsaquo; {__("Consecutive absent student")}
        {elseif $sub_view == "rollup"}
            &rsaquo; {__("Roll up")}
        {elseif $sub_view == ""}
            &rsaquo; {__("Whole school")}
        {/if}
    </div>

    {if $sub_view == ""}
        <div class="panel-body with-table">
            <form class="js_ajax-forms form-horizontal">
                <input type="hidden" id="username" value="{$username}"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Select attendance date")}</label>
                    <div class="col-sm-3">
                        <div class='input-group date' id='att_aday_picker'>
                            <input type='text' name="attendanceDate" id="attendanceDate" value="{$attendanceDate}" class="form-control" placeholder="{__("Attendance date")}"/>
                                <span class="input-group-addon">
                                    <span class="fas fa-calendar-alt"></span>
                                </span>
                        </div>
                    </div>
                    <label id="loading" class="btn btn-info x-hidden">{__("Loading")}...</label>
                </div>
            </form>
            <br/>
            <div class="table-responsive" id="attendance_list">
                {include file="ci/school/ajax.school.attendanceaday.tpl"}
            </div>
        </div>
    {elseif $sub_view == "classatt"}
        <div class="panel-body with-table" style="position: relative">
            <div class="row">
                <div class='col-sm-12'>
                    <div class='col-sm-4'>
                        <select name="class_id" id="classatt_class_id" class="form-control">
                            <option value="">{__("Select Class")}...</option>
                            {$class_level = -1}
                            {foreach $classes as $class}
                                {if ($class_level != $class['class_level_id'])}
                                    <option value="" disabled>-----{$class['class_level_name']}-----</option>
                                {/if}
                                <option value="{$class['group_id']}">{$class['group_title']}</option>
                                {$class_level = $class['class_level_id']}
                            {/foreach}
                        </select>
                    </div>
                    <div class='col-sm-2'>
                        <div class='input-group date' id='fromdatepicker'>
                            <input type='text' name="fromDate" id="fromDate" value="{$fromDate}" class="form-control" placeholder="{__("From date")}"/>
                                <span class="input-group-addon">
                                    <span class="fas fa-calendar-alt"></span>
                                </span>
                        </div>
                    </div>
                    <div class='col-sm-2'>
                        <div class='input-group date' id='todatepicker'>
                            <input type='text' name="toDate" id="toDate" value="{$toDate}" class="form-control" placeholder="{__("To date")}"/>
                                <span class="input-group-addon">
                                    <span class="fas fa-calendar-alt"></span>
                                </span>
                        </div>
                    </div>
                    <div class='col-sm-2'>
                        <div class="form-group">
                            <a href="#" id="search" class="btn btn-default js_attendance-search-class" data-username="{$username}" data-id="{$school['page_id']}" disabled="true">{__("Search")}</a>
                            <label id="loading" class="btn btn-info x-hidden">{__("Loading")}...</label>
                        </div>
                    </div>
                    <div class='col-sm-2'>
                        <div class="form-group">
                            <a href="#" id="export_excel" class="btn btn-success js_attendance-export-excel" data-username="{$username}" data-id="{$school['page_id']}" disabled="true">{__("Export to Excel")}</a>
                        </div>
                    </div>
                </div>
            </div>
            <br/>
            <div id="attendance_list"></div>
        </div>
    {elseif $sub_view == "rollup"}
        <div class="panel-body">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_attendance.php">
                <input type="hidden" name="school_username" value="{$username}"/>
                <input type="hidden" name="do" value="save_rollup" id="attendance_do"/>
                <div class="form-group">
                    <div class='col-sm-4'>
                        <select name="class_id" id="attendance_rollup_class_id" class="form-control">
                            <option value="">{__("Select Class")}...</option>
                            {$class_level = -1}
                            {foreach $classes as $class}
                                {if ($class_level != $class['class_level_id'])}
                                    <option value="" disabled>-----{$class['class_level_name']}-----</option>
                                {/if}
                                <option value="{$class['group_id']}" {if $class_id == $class['group_id']}selected{/if}>{$class['group_title']}</option>
                                {$class_level = $class['class_level_id']}
                            {/foreach}
                        </select>
                    </div>
                    <div class='col-sm-3'>
                        <div class='input-group date' id='attendance_picker'>
                            <input type='text' name="attendance_date" id="attendance_date" value="{$data['attendance_date']}" class="form-control" placeholder="{__("Attendance date")}"/>
                                <span class="input-group-addon">
                                    <span class="fas fa-calendar-alt"></span>
                                </span>
                        </div>
                    </div>
                    <div class='col-sm-3'>
                        <a href="#" id="search" class="btn btn-default js_attendance-search-4rollup" data-username="{$username}" data-id="{$school['page_id']}" {if $class_id == 0}disabled="true"{/if}>{__("Search")}</a>
                        <label id="loading" class="btn btn-info x-hidden">{__("Loading")}...</label>
                    </div>
                </div>
                <div class="table-responsive" id="attendance_list">
                    {if $class_id > 0}
                        {include file="ci/school/ajax.school.attendance.list4rollup.tpl"}
                    {/if}
                </div>
                <div class="form-group">
                    <div class="col-sm-9">
                        <button type="submit" class="btn btn-primary padrl30" {if ($class_id == 0) || (count($data['detail']) == 0)}disabled{/if}>{__("Save")}</button>
                    </div>
                </div>

                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
            </form>
        </div>
    {elseif $sub_view == "child"}
        <div class="panel-body">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_attendance.php">
                <input type="hidden" name="school_username" value="{$username}"/>
                <input type="hidden" name="do" value="save_child_rollup"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-right">{__("Select student")} (*)</label>
                    <div class='col-sm-3'>
                        <select name="class_id" id="attendance_class_id" data-username="{$username}" class="form-control" autofocus>
                            <option value="">{__("Select Class")}...</option>
                            {$class_level = -1}
                            {foreach $data['classes'] as $class}
                                {if ($class_level != $class['class_level_id'])}
                                    <option value="" disabled>-----{$class['class_level_name']}-----</option>
                                {/if}
                                <option value="{$class['group_id']}" {if $data['child']['class_id'] == $class['group_id']}selected{/if}>{$class['group_title']}</option>
                                {$class_level = $class['class_level_id']}
                            {/foreach}

                        </select>
                    </div>
                    <div class="col-sm-4">
                        <select name="child_id" id="attendance_child_id" class="form-control" required>
                            <option value="">{__("Select student")}...</option>
                            {if $data['child'] != ''}
                                {if isset($data['class_child']) && !empty($data['class_child'])}
                                    {foreach $data['class_child'] as $child}
                                        <option value="{$child['child_id']}" {if ($child['child_id'] == $data['child']['child_id'])}selected{/if}>{$child['child_name']}</option>
                                    {/foreach}
                                {else}
                                    <option value="{$data['child']['child_id']}" selected>{$data['child']['child_name']}</option>
                                {/if}
                            {/if}
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-right">{__("Time")} (*)</label>
                    <div class='col-sm-3'>
                        <div class='input-group date' id='fromdatepicker'>
                            <input type='text' name="fromDate" id="fromDate" value="{$data['fromDate']}" class="form-control" placeholder="{__("From date")}"/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>

                    <div class='col-md-3'>
                        <div class='input-group date' id='todatepicker'>
                            <input type='text' name="toDate" id="toDate" value="{$data['toDate']}" class="form-control" placeholder="{__("To date")}"/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <a href="#" id="search" class="btn btn-default js_school-attendance_child" data-username="{$username}" {if !($data['child'] != '')}disabled="true"{/if}>{__("Search")}</a>
                        <label id="loading" class="btn btn-info x-hidden">{__("Loading")}...</label>
                    </div>
                </div>
                <div class="table-responsive" id="attendance_list">
                    {if $data['child'] != ''}
                        {include file="ci/school/ajax.school.attendance.child.tpl"}
                    {/if}
                </div>
                <div class="form-group">
                    <div class="col-sm-9">
                        <button type="submit" class="btn btn-primary padrl30" {if (!canEdit($username, 'attendance')) || (count($data['attendance']['attendance']) == 0)}disabled{/if}>{__("Save")}</button>
                    </div>
                </div>
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
            </form>
        </div>
    {elseif $sub_view == "detail"}
        <div class="panel-body with-table">
            <table class="table table-striped table-bordered table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>{__("Full name")}</th>
                    <th>{__("Status")}</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td colspan="3" align="">
                        <strong>{__("Date")}: &nbsp;{$data['attendance_date']}&nbsp;&nbsp;-&nbsp;&nbsp;{__("Present")}:&nbsp;{$data['present_count']}&nbsp;&nbsp;-&nbsp;&nbsp;{__("Absence")}:&nbsp;{$data['absence_count']}</strong>
                    </td>
                </tr>
                {$idx = 1}
                {foreach $data['detail'] as $row}
                    <tr {if $row['child_status'] == 0} class="row-disable" {/if}>
                        <td align="center">{$idx}</td>
                        <td>
                            <a href="{$system['system_url']}/school/{$username}/attendance/child/{$row['child_id']}">
                                {if $row['status'] == 1}
                                    {$row['child_name']}
                                {else}
                                    <strong>{$row['child_name']}</strong>
                                {/if}
                            </a>
                        </td>
                        <td>
                            {if $row['status'] == $smarty.const.ATTENDANCE_PRESENT}
                                {__("Present")}
                            {elseif $row['status'] == $smarty.const.ATTENDANCE_COME_LATE}
                                {__("Come late")}
                            {elseif $row['status'] == $smarty.const.ATTENDANCE_EARLY_LEAVE}
                                {__("Leave early")}
                            {elseif $row['status'] == $smarty.const.ATTENDANCE_ABSENCE_NO_REASON}
                                {__("Without permission")}
                            {else}
                                <strong>{__("With permission")}</strong>&nbsp;
                                {if (!isset($row['reason']) || ($row['reason'] == ""))}
                                    ({__("No reason")})
                                {else}
                                    ({$row['reason']})
                                {/if}
                            {/if}
                        </td>
                    </tr>
                    {$idx = $idx + 1}
                {/foreach}
                </tbody>
            </table>
        </div>
    {elseif $sub_view == "conabs"}
        <div class="panel-body with-table">
            <div class="row">
                <div class="form-group">
                    <label class="col-sm-3 control-label text-right pt5">{__("Min number of days")} (*)</label>
                    <div class='col-sm-2'>
                        <input type='number' name="daynum" id="daynum" class="form-control" value="{$daynum}" min="1" max="31" placeholder="{__("Min number of days")}"/>
                    </div>
                    <div class='col-sm-2'>
                        <div class="form-group">
                            <a href="#" id="search" class="btn btn-default js_attendance-search-conabs" data-username="{$username}" data-id="{$school['page_id']}">{__("Search")}</a>
                            <label id="loading" class="btn btn-info x-hidden">{__("Loading")}...</label>
                        </div>
                    </div>
                </div>
            </div>
            <br/>
            <div class="table-responsive" id="attendance_list">
                {include file="ci/school/ajax.school.attendance.conabs.tpl"}
            </div>
        </div>
    {/if}
</div>