<div class="panel panel-default">
    <div class="panel-heading with-icon">
        {if ($sub_view == "") && $canEdit}
            <div class="pull-right flip">
                <a href="{$system['system_url']}/school/{$username}/classes/add" class="btn btn-default">
                    <i class="fa fa-plus"></i> {__("Add New Class")}
                </a>
                {if $school['school_step'] == SCHOOL_STEP_FINISH}
                    <a href="{$system['system_url']}/school/{$username}/classes/classup" class="btn btn-default">
                        <i class="fa fa-arrow-circle-up"></i> {__("Class up")}
                    </a>
                {/if}
            </div>
        {elseif $sub_view == "classup" || $sub_view == "add" || $sub_view == "edit"}
            <div class="pull-right flip">
                <a href="{$system['system_url']}/school/{$username}/classes" class="btn btn-default">
                    <i class="fa fa-list"></i> {__("Lists")}
                </a>
            </div>
        {/if}
        {if $school['school_step'] == SCHOOL_STEP_FINISH}
            <div class="pull-right flip" style="margin-right: 5px">
                <a href="https://blog.coniu.vn/huong-dan-tao-them-mot-lop-moi/" class="btn btn-info btn_guide" target="_blank">
                    <i class="fa fa-info-circle"></i> {__("Guide")}
                </a>
            </div>
        {/if}
        <i class="fa fa-graduation-cap fa-fw fa-lg pr10"></i>
        {__("Class")}
        {if $sub_view == "edit"}
            &rsaquo; {$data['group_title']}
        {elseif $sub_view == "add"}
            &rsaquo; {__('Add New')}
        {elseif $sub_view == ""}
            &rsaquo; {__('Lists')}
        {elseif $sub_view == "classup"}
            &rsaquo; {__('Class up')}
        {/if}
    </div>
    {if $sub_view == ""}
        <div class="panel-body with-table">
            <div class="mb5"><strong>{__("Class list")}&nbsp;({$rows|count} {__("Class")})</strong></div>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th width="5%">#</th>
                            <th width="20%">{__("Class")}</th>
                            <th width="30%">{__("Teacher")}</th>
                            <th width="10%">{__("Total")}</th>
                            <th width="20%">{__("Class level")}</th>
                            {*<th>{__("Telephone")}</th>
                            <th>{__("Email")}</th>*}
                            <th width="15%">{__("Actions")}</th>
                        </tr>
                    </thead>
                    <tbody>
                        {$idx = 1}
                        {foreach $rows as $row}
                            <tr>
                                <td class="align-middle" align="center">{$idx}</td>
                                <td class="align-middle">{$row['group_title']}</td>
                                <td class="align-middle">
                                    {if count($row['teachers']) == 0}
                                        {__("No teacher")}
                                    {else}
                                        {foreach $row['teachers'] as $teacher}
                                            <span class="name js_user-popover" data-uid="{$teacher['user_id']}">
                                                <a href="{$system['system_url']}/{$teacher['user_name']}">{$teacher['user_fullname']}</a>
                                            </span>
                                            {if $teacher['user_id'] != $user->_data['user_id']}
                                                <a href="#" class="far fa-comments fa-lg js_chat-start" data-name="{$teacher['user_fullname']}" data-uid="{$teacher['user_id']}"></a>
                                            {/if}
                                            &nbsp;&nbsp;&nbsp;
                                        {/foreach}
                                    {/if}
                                </td class="align-middle">
                                <td class="align-middle" align="center">{$row['cnt']} {__("children")}</td>
                                <td class="align-middle">{$row['class_level_name']}</td>
                                {*<td class="align-middle">{$row['telephone']}</td>
                                <td class="align-middle"><a href="mailto:{$row['email']}">{$row['email']}</a></td>*}
                                <td class="align-middle">
                                    <a href="{$system['system_url']}/groups/{$row['group_name']}">{__("Timeline")}</a>
                                    {if $canEdit}
                                        <a href="{$system['system_url']}/school/{$username}/classes/edit/{$row['group_id']}" class="btn btn-xs btn-default">{__("Edit")}</a>
                                        {if $school['school_step'] == SCHOOL_STEP_FINISH}
                                            <a href="{$system['system_url']}/school/{$username}/classes/graduate/{$row['group_id']}" class="btn btn-xs btn-default">{__("Graduate")}</a>
                                        {/if}
                                        {if (!$row['cnt'])}
                                            <button class="btn btn-xs btn-danger js_school-delete" data-handle="class" data-username="{$username}" data-id="{$row['group_id']}">{__("Delete")}</button>
                                        {/if}
                                    {/if}
                                </td>
                            </tr>
                            {$idx = $idx + 1}
                        {/foreach}

                        {if $rows|count == 0}
                            <tr class="odd">
                                <td valign="top" align="center" colspan="6" class="dataTables_empty">
                                    {__("No data available in table")}
                                </td>
                            </tr>
                        {/if}
                    </tbody>
                </table>
            </div>
        </div>
    {elseif $sub_view == "edit"}
        <div class="panel-body">
            <div id="open_dialog_teacher_add" class="x-hidden" title="{__("Add new teacher")|upper}">
                {include file="ci/school/ajax.school.addnewteacher.tpl"}
            </div>
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_class.php">
                <input type="hidden" name="school_username" id="school_username" value="{$username}"/>
                <input type="hidden" name="class_id" value="{$data['group_id']}"/>
                <input type="hidden" name="do" value="edit"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">
                        {__("Class name")} (*)
                    </label>
                    <div class="col-sm-9">
                        <input class="form-control" name="title" value="{$data['group_title']}" placeholder="{__("Class name")}" required autofocus maxlength="255">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">
                        {__("Class level")} (*)
                    </label>
                    <div class="col-sm-9">
                        <select class="form-control" name="class_level_id">
                            {foreach $class_levels as $class_level}
                                <option {if $data['class_level_id']==$class_level['class_level_id']}selected{/if} value="{$class_level['class_level_id']}">{$class_level['class_level_name']}</option>
                            {/foreach}
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">
                        {__("Teacher")}
                    </label>
                    <div class="col-sm-9">
                        <input name="search-teacher" id="search-teacher" type="text" class="form-control" placeholder="{__("Enter username, email or fullname to search")}" autocomplete="off">
                        <div id="search-teacher-results" class="dropdown-menu dropdown-widget dropdown-search">
                            <div class="dropdown-widget-header">
                                {__("Search Results")}
                            </div>
                            <div class="dropdown-widget-body">
                                <div class="loader loader_small ptb10"></div>
                            </div>
                        </div>
                        <br/>
                        <div class="col-sm-9" id="teacher_list" name="teacher_list">
                            {if count($teachers) > 0}
                                {include file='ci/ajax.teacherlist.tpl' results=$teachers}
                            {else}
                                {__("No teacher")}
                            {/if}
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-3">
                        <a style="margin-left: 10px" class="btn btn-xs btn-default js_school-teacher-add"><i class="fa fa-plus"></i> {__("Add new teacher")}</a>
                    </div>
                </div>
                {*<div class="form-group">*}
                    {*<label class="col-sm-3 control-label text-left">*}
                        {*{__("Telephone")}*}
                    {*</label>*}
                    {*<div class="col-sm-9">*}
                        {*<input class="form-control" id="telephone" name="telephone" value="{$data['telephone']}" placeholder="{__("Telephone")}" maxlength="50">*}
                    {*</div>*}
                {*</div>*}
                {*<div class="form-group">*}
                    {*<label class="col-sm-3 control-label text-left">*}
                        {*{__("Email")}*}
                    {*</label>*}
                    {*<div class="col-sm-9">*}
                        {*<input class="form-control" id="email" name="email" value="{$data['email']}" placeholder="{__("Email")}" maxlength="50">*}
                    {*</div>*}
                {*</div>*}
                {*<div class="form-group">*}
                    {*<label class="col-sm-3 control-label text-left">*}
                        {*{__("Camera URL")}*}
                    {*</label>*}
                    {*<div class="col-sm-9">*}
                        {*<input class="form-control" name="camera_url" value="{$data['camera_url']}" placeholder="{__("Camera URL")}" maxlength="300">*}
                    {*</div>*}
                {*</div>*}
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">
                        {__("Description")}
                    </label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="description" id="description" placeholder="{__("Write about your class...")}" rows="3">{$data['group_description']}</textarea>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30 js_show-loading">{__("Save")}</button>
                        <a href="{$system['system_url']}/school/{$username}/classes" class="btn btn-default">{__("Lists")}</a>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->
                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    {elseif $sub_view == "add"}
        <div class="panel-body">
            <div id="open_dialog_teacher_add" class="x-hidden" title="{__("Add new teacher")|upper}">
                {include file="ci/school/ajax.school.addnewteacher.tpl"}
            </div>
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_class.php">
                <input type="hidden" name="school_username" id="school_username" value="{$username}"/>
                <input type="hidden" name="do" value="add"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">
                        {__("Class name")} (*)
                    </label>
                    <div class="col-sm-9">
                        <input class="form-control" name="title" placeholder="{__("Class name")}" required autofocus maxlength="255">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">
                        {__("Class level")} (*)
                    </label>
                    <div class="col-sm-9">
                        <select class="form-control" name="class_level_id">
                            {foreach $class_levels as $class_level}
                                <option value="{$class_level['class_level_id']}">{$class_level['class_level_name']}</option>
                            {/foreach}
                        </select>
                    </div>
                </div>
                {if $school['school_step'] == SCHOOL_STEP_FINISH  || $school['grade'] != 0}
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">
                            {__("Teacher")}
                        </label>
                        <div class="col-sm-9">
                            <input name="search-teacher" id="search-teacher" type="text" class="form-control" placeholder="{__("Enter username, email or fullname to search")}" autocomplete="off">
                            <div id="search-teacher-results" class="dropdown-menu dropdown-widget dropdown-search">
                                <div class="dropdown-widget-header">
                                    {__("Search Results")}
                                </div>
                                <div class="dropdown-widget-body">
                                    <div class="loader loader_small ptb10"></div>
                                </div>
                            </div>
                            <br/>
                            <div class="col-sm-9" id="teacher_list" name="teacher_list"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-3">
                            <a style="margin-left: 10px" class="btn btn-xs btn-default js_school-teacher-add"><i class="fa fa-plus"></i> {__("Add new teacher")}</a>
                        </div>
                    </div>
                {/if}
                {*<div class="form-group">*}
                    {*<label class="col-sm-3 control-label text-left">*}
                        {*{__("Telephone")}*}
                    {*</label>*}
                    {*<div class="col-sm-9">*}
                        {*<input class="form-control" id="telephone" name="telephone" placeholder="{__("Telephone")}" maxlength="50">*}
                    {*</div>*}
                {*</div>*}
                {*<div class="form-group">*}
                    {*<label class="col-sm-3 control-label text-left">*}
                        {*{__("Email")}*}
                    {*</label>*}
                    {*<div class="col-sm-9">*}
                        {*<input class="form-control" id="email" name="email" placeholder="{__("Email")}" maxlength="50">*}
                    {*</div>*}
                {*</div>*}
                {*<div class="form-group">*}
                    {*<label class="col-sm-3 control-label text-left">*}
                        {*{__("Camera URL")}*}
                    {*</label>*}
                    {*<div class="col-sm-9">*}
                        {*<input class="form-control" name="camera_url" placeholder="{__("Camera URL")}" maxlength="300">*}
                    {*</div>*}
                {*</div>*}
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">
                        {__("Description")}
                    </label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="description" id="description" placeholder="{__("Write about your class...")}" rows="3"></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30 js_show-loading">{__("Save")}</button>
                    </div>
                </div>

                {*<!-- success -->*}
                {*<div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>*}
                {*<!-- success -->*}

                {*<!-- error -->*}
                {*<div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>*}
                {*<!-- error -->*}
            </form>
        </div>
    {elseif $sub_view == "classup"}
        <div class="panel-body">
            <form class="js_ajax-forms-success-reload form-horizontal" data-url="ci/bo/school/bo_class.php">
                <input type="hidden" name="school_username" id="school_username" value="{$username}"/>
                <input type="hidden" name="class_id" value="{$data['group_id']}"/>
                <input type="hidden" name="do" value="class_up"/>
                <div class="form-group">
                    <div align="center"><strong>{__("Lưu ý: Nếu bạn muốn chuyển lên lớp đã tồn tại trước đó, bạn phải cho lớp đó lên lớp hoặc tốt nghiệp trước.")}</strong></div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Select Class")} (*)</label>
                    <div class="col-sm-4">
                        <select name="class_id" id="classup_class_id" data-username="{$username}" class="form-control" autofocus>
                            <option value="">{__("Select class")}</option>
                            {$class_level = -1}
                            {foreach $classes as $class}
                                {if ($class_level != $class['class_level_id'])}
                                    <option value="" disabled style="color: blue">-----{$class['class_level_name']}-----</option>
                                {/if}
                                <option value="{$class['group_id']}">{$class['group_title']}</option>
                                {$class_level = $class['class_level_id']}
                            {/foreach}
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("New class title")} (*)</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="new_title" value="" required maxlength="255">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">
                        {__("Class level")} (*)
                    </label>
                    <div class="col-sm-4">
                        <select class="form-control" name="class_level_id">
                            <option value="">{__("Select class level")}</option>
                            {foreach $class_levels as $class_level}
                                <option {if $data['class_level_id']==$class_level['class_level_id']}selected{/if} value="{$class_level['class_level_id']}">{$class_level['class_level_name']}</option>
                            {/foreach}
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30 js_show-loading">{__("Save")}</button>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->
                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    {elseif $sub_view == "graduate"}
        <div class="panel-body">
            <form class="js_ajax-forms-confirm form-horizontal" data-url="ci/bo/school/bo_class.php">
                <input type="hidden" id="school_username" name="school_username" value="{$username}"/>
                <input type="hidden" id="class_id" name="class_id" value="{$data['group_id']}"/>
                <input type="hidden" id="class_level_id" name="class_level_id" value="{$data['class_level_id']}"/>
                <input type="hidden" name="do" value="class_leave_school"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Class")}</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="group_title" value="{$data['group_title']}" disabled>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Leaving date")} (*)</label>
                    <div class='col-sm-3 text-left'>
                        <div class='input-group date' id='class_leave_datepicker'>
                            <input type='text' name="class_end_at" id="class_end_at" class="form-control" placeholder="{__("Leaving date")}"/>
                            <span class="input-group-addon">
                            <span class="fas fa-calendar-alt"></span>
                        </span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                    </div>
                </div>
                <div class="table-responsive" id="class_children_info">
                    {include file="ci/school/ajax.school.graduate.tpl"}
                </div>
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
            </form>
        </div>
    {/if}
</div>