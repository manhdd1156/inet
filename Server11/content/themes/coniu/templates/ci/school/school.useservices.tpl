<div class="panel panel-default">
    <div class="panel-heading with-icon">
        {if ($sub_view == "history") && $canEdit}
            <div class="pull-right flip">
                <a href="{$system['system_url']}/school/{$username}/useservices/reg" class="btn btn-default">
                    <i class="fa fa-plus"></i> {__("Register service")}
                </a>
                <a href="{$system['system_url']}/school/{$username}/useservices/class" class="btn btn-default">
                    <i class="fa fa-square"></i>{__("Count-based service summary")}
                </a>
            </div>
        {elseif $sub_view == "reg"}
            <div class="pull-right flip">
                {if $canEdit}
                    <a href="https://blog.coniu.vn/huong-dan-dang-ky-dich-vu/" target="_blank" class="btn btn-info btn_guide">
                        <i class="fa fa-info"></i> {__("Register service guide")}
                    </a>
                {/if}
                {if $canEdit && $school['tuition_use_mdservice_deduction']}
                    <a href="{$system['system_url']}/school/{$username}/useservices/deduction" class="btn btn-default">
                        <i class="fa fa-square"></i> {__("Deduction note")}
                    </a>
                {/if}
                <a href="{$system['system_url']}/school/{$username}/useservices/history" class="btn btn-default">
                    <i class="fa fa-history"></i> {__("Service usage information")}
                </a>
            </div>
        {/if}
        <i class="fa fa-life-ring fa-fw fa-lg pr10"></i>
        {__("Service")}
        {if $sub_view == "reg"}
            &rsaquo; {__('Register service')}
        {elseif $sub_view == "deduction"}
            &rsaquo; {__('Deduction note')}
        {elseif $sub_view == "history"}
            &rsaquo; {__('Service usage information')}
        {elseif $sub_view == "foodsvr"}
            &rsaquo; {__('Daily food service summary')}
        {elseif $sub_view == "class"}
            &rsaquo; {__('Count-based service summary')}
        {elseif $sub_view == "classdate"}
            &rsaquo; {__('Count-based service summary')}
        {/if}
    </div>
    {if $sub_view == "reg"}
        <div class="panel-body with-table">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_service.php">
                <input type="hidden" name="school_username" id="school_username" value="{$username}"/>
                <input type="hidden" name="do" id="do" value="reg"/>
                <div class="form-group pl5">
                    <div class='col-sm-4'>
                        {$idx = 1}
                        {$type = -1}
                        <select name="service_id" id="reg_service_id" class="form-control">
                            <option value="">{__('Select service')}...</option>
                            {foreach $services as $service}
                                {if ($type != $service['type'])}
                                    <option value="" disabled>-----
                                        {if $service['type'] == SERVICE_TYPE_MONTHLY}
                                            {__("Monthly")}
                                        {elseif $service['type'] == SERVICE_TYPE_DAILY}
                                            {__("Daily")}
                                        {else}
                                            {__("Count-based")}
                                        {/if}-----</option>
                                {/if}
                                <option value="{$service['service_id']}" data-type="{$service['type']}">{$idx} - {$service['service_name']}</option>
                                {$idx = $idx + 1}
                                {$type = $service['type']}
                            {/foreach}
                        </select>
                    </div>
                    <div class='col-sm-3'>
                        {$class_level = -1}
                        <select name="class_id" id="class_id" class="form-control">
                            <option value="">{__("Whole school")|upper}</option>
                            {foreach $classes as $class}
                                {if ($class_level != $class['class_level_id'])}
                                    <option value="" disabled>-----{$class['class_level_name']}-----</option>
                                {/if}
                                <option value="{$class['group_id']}">{$class['group_title']}</option>
                                {$class_level = $class['class_level_id']}
                            {/foreach}
                        </select>
                    </div>
                    <div class="col-sm-3 x-hidden" id="using_at_div">
                        <div class='input-group date' id='using_time_picker'>
                            <input type='text' name="using_at" id="using_at" class="form-control" placeholder="{__("Using time")} (*)" required/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>

                    <div class='col-sm-2'>
                        <div class="form-group pl10">
                            <a href="#" id="search" class="btn btn-default js_service-search-reg" data-username="{$username}" disabled="true">{__("Search")}</a>
                            <label id="loading" class="btn btn-info x-hidden">{__("Loading")}...</label>
                        </div>
                    </div>
                </div>
                <div class="table-responsive" id="child_list" name="child_list"></div>

                <div class="form-group pl5" id="service_btnSave">
                    <div class="col-sm-9">
                        <button type="submit" class="btn btn-primary padrl30" disabled>{__("Save")}</button>
                    </div>
                </div>
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
            </form>
        </div>
    {elseif $sub_view == "deduction"}
        <div class="panel-body with-table">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_service.php">
                <input type="hidden" name="school_username" id="school_username" value="{$username}"/>
                <input type="hidden" name="do" id="do" value="deduction"/>
                <div class="form-group pl5">
                    <div>
                        <trong>Chức năng Ghi chú giảm trừ áp dụng cho trường hợp trẻ đăng ký sử dụng dịch vụ thu phí theo THÁNG và theo ĐIỂM DANH. Nếu có ngày trẻ đi học nhưng KHÔNG sử dụng dịch vụ đó, nhà trường lưu lại trường hợp này để giảm trừ khi tính học phí</trong>.
                    </div>
                </div>
                <div class="form-group pl5">
                    <div class='col-sm-4'>
                        {$idx = 1}
                        {$type = -1}
                        <select name="service_id" id="service_id" class="form-control" required>
                            <option value="">{__('Select service')}...</option>
                            {foreach $services as $service}
                                {if ($service['type'] == SERVICE_TYPE_MONTHLY) || ($service['type'] == SERVICE_TYPE_DAILY)} {__("Daily")}
                                    {if ($type != $service['type'])}
                                        <option value="" disabled>-----{if $service['type'] == SERVICE_TYPE_MONTHLY}{__("Monthly")}{else}{__("Daily")}{/if}-----</option>
                                    {/if}

                                    <option value="{$service['service_id']}">{$idx} - {$service['service_name']}</option>
                                    {$idx = $idx + 1}
                                    {$type = $service['type']}
                                {/if}
                            {/foreach}
                        </select>
                    </div>
                    <div class='col-sm-3'>
                        {$class_level = -1}
                        <select name="class_id" id="class_id" class="form-control" required>
                            {foreach $classes as $class}
                                {if ($class_level != $class['class_level_id'])}
                                    <option value="" disabled>-----{$class['class_level_name']}-----</option>
                                {/if}
                                <option value="{$class['group_id']}">{$class['group_title']}</option>
                                {$class_level = $class['class_level_id']}
                            {/foreach}
                        </select>
                    </div>
                    <div class="col-sm-3" id="using_at_div">
                        <div class='input-group date' id='using_time_picker'>
                            <input type='text' name="deduction_date" id="deduction_date" class="form-control" placeholder="{__("Deduction date")} (*)" required/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                    <div class='col-sm-2'>
                        <div class="form-group pl10">
                            <a href="#" id="search" class="btn btn-default js_service-search-deduction" data-username="{$username}">{__("Search")}</a>
                            <label id="loading" class="btn btn-info x-hidden">{__("Loading")}...</label>
                        </div>
                    </div>
                </div>
                <div class="table-responsive" id="child_list" name="child_list"></div>

                <div class="form-group pl5" id="service_btnSave">
                    <div class="col-sm-9">
                        <button type="submit" class="btn btn-primary padrl30" disabled>{__("Save")}</button>
                    </div>
                </div>
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
            </form>
        </div>
    {elseif $sub_view == "history"}
        <div class="panel-body with-table">
            <form class="js_ajax-forms form-horizontal" data-url="#">
                <div class="form-group pl5">
                    <div class='col-sm-4'>
                        {$idx = 1}
                        {$type = -1}
                        <select name="service_id" id="service_id" class="form-control js_school-history-serviceid">
                            <option value="">{__('All services')}</option>
                            {foreach $services as $service}
                                {if ($type != $service['type'])}
                                    <option value="" disabled>-----
                                        {if $service['type'] == SERVICE_TYPE_MONTHLY} {__("Monthly")}
                                        {elseif $service['type'] == SERVICE_TYPE_DAILY} {__("Daily")}
                                        {else} {__("Count-based")}
                                        {/if}-----</option>
                                {/if}
                                <option value="{$service['service_id']}" data-type="{$service['type']}">{$idx} - {$service['service_name']}</option>
                                {$idx = $idx + 1}
                                {$type = $service['type']}
                            {/foreach}
                        </select>
                    </div>
                    <div class='col-sm-3'>
                        {*<select name="class_id" id="service_class_id" class="form-control" data-username="{$username}" {if $service_id > 0}disabled{/if}>*}
                        <select name="class_id" id="service_class_id" class="form-control" data-username="{$username}">
                            <option value="">{__("Whole school")|upper}</option>
                            {$class_level = -1}
                            {foreach $classes as $class}
                                {if ($class_level != $class['class_level_id'])}
                                    <option value="" disabled style="color: blue">-----{$class['class_level_name']}-----</option>
                                {/if}
                                <option value="{$class['group_id']}">{$class['group_title']}</option>
                                {$class_level = $class['class_level_id']}
                            {/foreach}
                        </select>
                    </div>
                    <div class='col-sm-5'>
                        <select name="child_id" id="service_child_id" class="form-control">
                            <option value="">{__("Select student")}...</option>
                        </select>
                    </div>
                </div>
                <div class="form-group pl5">
                    <div class="col-sm-3">
                        <div class='input-group date' id='history_begin_picker'>
                            <input type='text' name="begin" id="begin" class="form-control" placeholder="{__("From date")}"/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class='input-group date' id='history_end_picker'>
                            <input type='text' name="end" id="end" class="form-control" placeholder="{__("To date")}"/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                    <div class='col-sm-2'>
                        <div class="form-group pl10">
                            <a href="#" id="search" class="btn btn-default js_service-search-history" data-username="{$username}">{__("Search")}</a>
                            <label id="loading" class="btn btn-info x-hidden">{__("Loading")}...</label>
                        </div>
                    </div>
                </div>
                <div class="table-responsive" id="usage_history" name="usage_history"></div>
            </form>
        </div>
    {elseif $sub_view == "class"}
        <div class="panel-body with-table">
            <form class="js_ajax-forms form-horizontal" data-url="#">
                <div class="form-group pl5">
                    <div class='col-sm-3'>
                        <select name="class_id" id="class_id" class="form-control" data-username="{$username}">
                            <option value="">{__("Select class")}...</option>
                            {$class_level = -1}
                            {foreach $classes as $class}
                                {if ($class_level != $class['class_level_id'])}
                                    <option value="" disabled>-----{$class['class_level_name']}-----</option>
                                {/if}
                                <option value="{$class['group_id']}">{$class['group_title']}</option>
                                {$class_level = $class['class_level_id']}
                            {/foreach}
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <div class='input-group date' id='history_begin_picker'>
                            <input type='text' name="date" id="date" class="form-control" placeholder="{__("From date")}"/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                    <div class='col-sm-2'>
                        <div class="form-group pl10">
                            <a href="#" id="search" class="btn btn-default js_service-search-class" data-username="{$username}">{__("Search")}</a>
                            <label id="loading" class="btn btn-info x-hidden">{__("Loading")}...</label>
                        </div>
                    </div>
                </div>
                <div class="table-responsive" id="usage_class" name="usage_class"></div>
            </form>
        </div>
    {elseif $sub_view == "classdate"}
        <div align="center" class="mt10">
            <strong>{__("List registered countbase service of")} {$classTitle} {__("day")} {$date_pos}</strong>
        </div>
        <div class="panel-body with-table">
            <table class="table table-striped table-bordered table-hover">
                <tbody>
                    <tr>
                        <td align="center" class="align_middle">
                            <strong>#</strong>
                        </td>
                        <td align="center" class="align_middle">
                            <strong>{__("Children list")}</strong>
                        </td>
                        {foreach $servicesCB as $service}
                            <td align="center">
                                <strong>{$service['service_name']}</strong>
                            </td>
                        {/foreach}
                    </tr>

                    {$idx = 1}
                    {foreach $childList as $child}
                        <tr>
                            <td align="center" style="vertical-align: middle"><strong>{$idx}</strong></td>
                            <td><strong>{$child['child_name']}</strong></td>
                            {foreach $servicesCB as $service}
                                <td align="center" style="vertical-align: middle">
                                    {foreach $child['services']['service'] as $row}
                                        {if $row['service_id'] == $service['service_id']}
                                            {if $row['using_at'] != null}
                                                <i class="fa fa-check" aria-hidden="true" style="color: #26b31a"></i>
                                            {else}
                                                <i class="fa fa-times" aria-hidden="true"></i>
                                            {/if}
                                        {/if}
                                    {/foreach}
                                </td>
                            {/foreach}
                        </tr>
                        {$idx = $idx + 1}
                    {/foreach}
                </tbody>
            </table>
        </div>
    {elseif $sub_view == "foodsvr"}
        <div class="panel-body with-table">
            <form class="js_ajax-forms form-horizontal" data-url="#">
                <input type="hidden" name="school_username" value="{$username}" id="school_username">
                <div class="form-group center-block">
                    <div class="col-sm-3 col-sm-offset-1">
                        <div class='input-group date' id='history_begin_picker_food'>
                            <input type='text' name="date" id="date" class="form-control" placeholder="{__("Select date")}..."/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                    {*<div class='col-sm-2'>*}
                        {*<div class="form-group pl10">*}
                            {*<a href="#" id="search" class="btn btn-default js_service-search-foodservice" data-username="{$username}">{__("Search")}</a>*}
                            {*<label id="loading" class="btn btn-info x-hidden">{__("Loading")}...</label>*}
                        {*</div>*}
                    {*</div>*}
                </div>
                <div class="table-responsive" id="foodsvr" name="foodsvr">
                    {include file="ci/school/ajax.service.foodsvr.tpl"}
                </div>
            </form>
        </div>
    {/if}
</div>