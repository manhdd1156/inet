<div class="mb5"><strong>{__("Total")}: {$results|count}&nbsp;</strong></div>
<table class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th width="10%">#</th>
        <th width="20%">{__("Date")}</th>
        <th width="25%">{__("Class")}</th>
        <th width="20%">{__("Status")}</th>
        <th width="15%">{__("Money amount")}</th>
    </tr>
    </thead>
    <tbody>
    {$idx = 1}
    {foreach $results as $result}
        <tr>
            <td align="center"><strong>{$idx}</strong></td>
            <td align="center">
                <strong>
                    <font color="#ff4500">
                        {$result['pickup_time']} - {__($result['pickup_day'])}
                    </font>
                </strong>
            </td>
            <td align="center">
                {if !is_empty($result['class_name'])}
                    {$result['class_name']}
                {else}
                    <strong>&#150;</strong>
                {/if}
            </td>
            <td align="center">
                {if !is_empty($result['pickup_at']) }
                    <i class="fa fa-check" style="color:mediumseagreen" aria-hidden="true"></i>
                {else}
                    <i class="fa fa-times" aria-hidden="true"></i>
                {/if}
            </td>
            <td></td>
        </tr>
        <tr>
            <td rowspan="3"></td>
            <td colspan="3" align="left" style="padding-left: 30px">
                <div class="col-sm-4">{__("Pickup at")}:&nbsp;</div>
                    {if !is_empty($result['pickup_at']) }
                        <div class="col-sm-8">
                            <font color="blue">{$result['pickup_at']}</font>
                        </div>
                    {else}
                        <div class="col-sm-8" align="center">
                            <font color="#ff4500">&#150;</font>
                        </div>
                    {/if}
                </div>
            </td>
            <td colspan="1" align="right">
                <strong>
                    {number_format($result['late_pickup_fee'],0,'.',',')}
                </strong>
            </td>
        </tr>
        <tr>
            <td colspan="3" align="left" style="padding-left: 30px">
                {if !empty($result['services'])}
                    <div class="col-sm-4">{__("Using service")}:&nbsp;</div>
                    <div class="col-sm-8">
                        {foreach $result['services'] as $service}
                            <input type="checkbox" {if !is_null($service['using_at']) } checked {/if} onclick="return false;"/>
                            {$service['service_name']}
                            &nbsp;&nbsp;&nbsp;
                        {/foreach}
                    </div>
                {else}
                    &nbsp;
                {/if}
            </td>
            <td colspan="1" align="right">
                <strong>
                    {number_format($result['using_service_fee'],0,'.',',')}
                </strong>
            </td>
        </tr>
        <tr>
            <td colspan="3" align="left" style="padding-left: 30px" class="col-sm-12">
                <div class="col-sm-4">{__("Assigned teachers")}: &nbsp;</div>
                {$assign_count = $result['assigns']|count}
                    {if $assign_count > 0 }
                        {$i = 1}
                        {foreach $result['assigns'] as $assign}
                            <div class="col-xs-12 col-sm-4">
                                &#9679;&#09;{$assign['user_fullname']}
                            </div>
                            {if $i%2 == 0 && $i != $assign_count}
                                <div class="col-sm-4"></div>
                            {elseif $i%2 == 1 && $i == $assign_count}
                                <br>
                            {/if}
                            {$i = $i +1}
                        {/foreach}
                    {else}
                        <div class="col-sm-8" align="center">
                            <font color="#ff4500">&#150;</font>
                        </div>
                    {/if}
                <br>
                {if !empty($result['description']) }
                    <div class="col-sm-4">{__("Note")}: &nbsp;</div>
                    <div class="col-xs-12 col-sm-4" style="color: blue">
                        {$result['description']}
                    </div>
                {/if}

            </td>
            <td colspan="1" align="right" style="vertical-align: middle">
                <strong>
                    <font color="blue">
                        {number_format($result['total_amount'],0,'.',',')}
                    </font>
                </strong>
            </td>
        </tr>
        {$idx = $idx + 1}
    {/foreach}
    <tr><td colspan="5"></td></tr>
    <tr>
        <td colspan="4" align="right"><strong>{__("Total")}</strong></td>
        <td colspan="1" align="right" style="vertical-align: middle">
            <strong>
                <font color="#ff4500">
                    {number_format($total,0,'.',',')}
                </font>
            </strong>
        </td>
    </tr>
    </tbody>
</table>