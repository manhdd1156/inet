<!-- Modal danh sách group lớp -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">{__("Group class lists")}</h4>
            </div>
            <div class="modal-body">
                <ul class="row">
                    {foreach $classes as $_group}
                        <li class="col-md-3 col-sm-6">
                            <div class="box_dash">
                                <div class="group_img">
                                    <a class="box-picture_dash" href="{$system['system_url']}/groups/{$_group['group_name']}" style="background-image:url('{$_group['group_picture']}');"></a>
                                </div>
                                <div class="box-content_dash">
                                    <a href="{$system['system_url']}/groups/{$_group['group_name']}" class="title">
                                        {$_group['group_title']}
                                    </a>
                                    <div class="text clearfix">
                                        <div>
                                            <span class="count_member_{$_group['group_id']}">{$_group['group_members']}</span> {__("Members")}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    {/foreach}
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="" style="position: relative">
    {if $school['school_step'] != SCHOOL_STEP_FINISH  && $school['grade'] == 0 && $school['grade'] == 0}
        <div class="school_start" align="center">
            <div class="panel panel-default" style="padding: 20px">
                <h3 style="text-transform: uppercase">{$school['page_title']}</h3><br/>
                <p style="font-size: 20px; margin-bottom: 10px">{__("Chào mừng bạn đến với Mascom Edu")}</p>
                <p style="font-size: 16px">{__("Mời bạn nhập thông tin ban đầu để sử dụng Mascom Edu")}</p>
                <a href="{$system['system_url']}/school/{$username}/classlevels" class="btn btn-primary">{__("Start")}</a>
            </div>
        </div>
    {else}
        <div class="panel panel-default">
            <div class="panel-heading with-icon">
                <div class="pull-right flip">
                    {if canView($username, "attendance")}
                        <a class="btn btn-warning" href="{$system['system_url']}/school/{$username}/reports">{__("Contact book")}</a>
                    {/if}
                    {if canView($username, "pickup")}
                        <a class="btn btn-info" href="{$system['system_url']}/school/{$username}/pickup/list">{__("Late PickUp")}</a>
                    {/if}
                    {if canView($username, "attendance")}
                        <a class="btn btn-success" href="{$system['system_url']}/school/{$username}/attendance">{__("Attendance")}</a>
                    {/if}
                    {if canView($username, "services")}
                        <a class="btn btn-danger" href="{$system['system_url']}/school/{$username}/useservices/history">{__("Service")}</a>
                    {/if}
                    <div class="mt10" align="center">
                        <a class="btn btn-primary" href="{$system['system_url']}/pages/{$username}">{__("School page")}</a>
                        <!-- show các group lớp của trường -->
                        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#myModal">
                            {__("Groups class")}
                        </button>
                    </div>
                </div>
                <i class="fas fa-tachometer-alt fa-fw fa-lg pr10"></i>
                {__("Dashboard")}:&nbsp;{$school['page_title']}
            </div>

            {if isAdmin($username) && (($school['teacher_count'] == 0) || ($school['class_count'] == 0) || ($insights['child_cnt'] == 0))}
                <div class="panel-body color_red">
                    <strong>ĐỂ SỬ DỤNG HỆ THỐNG, BẠN PHẢI NHẬP ĐẦY ĐỦ THÔNG TIN SAU:</strong>
                    {if ($school['teacher_count'] == 0)}
                        <br/><br/>- Chưa có tài khoản giáo viên/nhân viên trong trường. <a href="{$system['system_url']}/school/{$username}/teachers/add">{__("Add New")}</a> | <a href="{$system['system_url']}/school/{$username}/teachers/addexisting">{__("Add Existing Account")}</a>
                    {/if}
                    {if ($school['class_count'] == 0)}
                        <br/><br/>- Chưa có lớp nào trong trường. <a href="{$system['system_url']}/school/{$username}/classes/add">{__("Add New")}</a>
                    {/if}
                    {if ($school['child_cnt'] == 0)}
                        <br/><br/>- Chưa có trẻ nào trong trường. <a href="{$system['system_url']}/school/{$username}/children/add">{__("Add New")}</a> | <a href="{$system['system_url']}/school/{$username}/children/import">{__("Import from Excel file")}</a>
                    {/if}
                    <br/><br/><div class="pull-right flip"><i>Bảng tin sẽ xuất hiện khi bạn nhập đủ thông tin cơ bản.</i></div>
                </div>
            {else}
                <div class="panel-body">
                    <div class="row">
                        {*Cột trái*}
                        <div class="col-sm-6">
                            {if canView($username, "attendance")}
                                {*Điểm danh*}
                                <div class="box-primary">
                                    <div class="box-header">
                                        <i class="fa fa-tasks"></i>
                                        <strong>{__("Attendance")}:</strong>&nbsp;{$insights['today']}
                                        <a href="{$system['system_url']}/school/{$username}/attendance" title="{__("Attendance")}" class="pull-right flip">{__("Detail")}</a>
                                    </div>
                                    <div class="list-group">
                                        <table class="table table-striped table-bordered table-hover">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>{__("Class")}</th>
                                                <th>{__("Status")}</th>
                                                <th>{__("Number of student")}</th>
                                                <th>{__("Present")}</th>
                                                <th>{__("Absence")}</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            {$totalPresent = 0}
                                            {$totalAbsence = 0}
                                            {$total = 0}
                                            {$idx = 1}

                                            {foreach $insights['attendances'] as $row}
                                                <tr>
                                                    <td align="center">{$idx}</td>
                                                    <td>{$row['group_title']}</td>
                                                    <td align="center">
                                                        {if $row['is_checked']==0}
                                                            <i class="fa fa-times" aria-hidden="true"></i>
                                                        {else}
                                                            <i class="fa fa-check" aria-hidden="true"></i>
                                                        {/if}
                                                    </td>
                                                    <td align="center">
                                                        {if $row['present_count'] > 0 || $row['absence_count'] > 0}
                                                            {$row['present_count'] + $row['absence_count']}
                                                        {else}
                                                            {$row['total']}
                                                        {/if}
                                                    </td>
                                                    <td align="center">{$row['present_count']}</td>
                                                    <td align="center">{$row['absence_count']}</td>

                                                    {$totalPresent = $totalPresent + $row['present_count']}
                                                    {$totalAbsence = $totalAbsence + $row['absence_count']}
                                                    {$idx = $idx + 1}
                                                    {$total = $total + $row['total']}
                                                </tr>
                                            {/foreach}
                                            <tr>
                                                <td colspan="3" align="center"><strong><font color="red">{__('TOTAL OF SCHOOL')}</font></strong></td>
                                                <td align="center"><strong><font color="red">{$total}</font></strong></td>
                                                <td align="center"><strong><font color="red">{$totalPresent}</font></strong></td>
                                                <td align="center"><strong><font color="red">{$totalAbsence}</font></strong></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                {*Trẻ nghỉ nhiều liên tục*}
                                {$cnt = count($insights['conabs']['list'])}
                                <div id="conabs_list">
                                    <div class="box-primary">
                                        <div class="box-header">
                                            <i class="fa fa-bomb"></i>
                                            <strong>{__("Consecutive absent student")}</strong>
                                            <br/>
                                            {__("Last update")}: {$insights['conabs']['updated_at']}
                                            {*<i class="fas fa-sync-alt pull-right flip"></i>*}
                                            <a class="btn btn-xs js_school_conabs_dashboard pull-right flip" data-username = "{$username}" title="{__("Refresh")}"><strong><i class="fas fa-redo-alt"></i> {__("Refresh")}</strong></a>
                                        </div>

                                        <div class="list-group">
                                            <table class="table table-striped table-bordered table-hover" id="hide_absent">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>{__("Full name")}</th>
                                                    <th>{__("Class")}</th>
                                                    <th>{__("Absent day")}</th>
                                                    <th>{__("Reason")}</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                {if $cnt > 0}
                                                    {$idx = 1}
                                                    {foreach $insights['conabs']['list'] as $k => $child}
                                                        {if $k < 10}
                                                            <tr>
                                                                <td align="center">{$idx}</td>
                                                                <td><a href="{$system['system_url']}/school/{$username}/attendance/child/{$child['child_id']}">{$child['child_name']}</a></td>
                                                                <td align="center">{$child['group_title']}</td>
                                                                <td align="center">
                                                                    {if isset($child['number_of_absent_day']) && ($child['number_of_absent_day'] > 0)}
                                                                        {if {$child['number_of_absent_day']} > 9}{__("More than")}&nbsp;{/if}{$child['number_of_absent_day']}&nbsp;{__("day(s)")}
                                                                    {else}
                                                                        {__("More than")}&nbsp;10&nbsp;{__("day(s)")}
                                                                    {/if}
                                                                </td>
                                                                <td>{$child['reason']}</td>
                                                            </tr>
                                                            {$idx = $idx + 1}
                                                        {/if}
                                                    {/foreach}
                                                    {if count($insights['conabs']['list']) > 10}
                                                        <tr>
                                                            <td colspan="5" align="center" style="padding: 5px">
                                                                <button class="full_absent_but btn btn-xs btn-info mb0">{__("See More")}</button>
                                                            </td>
                                                        </tr>
                                                    {/if}
                                                {elseif $cnt == 0}
                                                    <tr class="odd">
                                                        <td valign="top" align="center" colspan="5" class="dataTables_empty">
                                                            {__("No data available in table")}
                                                        </td>
                                                    </tr>
                                                {/if}
                                                </tbody>
                                            </table>
                                            <table class="table table-striped table-bordered table-hover x-hidden" id="full_absent">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>{__("Full name")}</th>
                                                    <th>{__("Class")}</th>
                                                    <th>{__("Absent day")}</th>
                                                    <th>{__("Reason")}</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                {if $cnt > 0}
                                                    {$idx = 1}
                                                    {foreach $insights['conabs']['list'] as $child}
                                                        <tr>
                                                            <td align="center">{$idx}</td>
                                                            <td><a href="{$system['system_url']}/school/{$username}/attendance/child/{$child['child_id']}">{$child['child_name']}</a></td>
                                                            <td align="center">{$child['group_title']}</td>
                                                            <td align="center">
                                                                {if isset($child['number_of_absent_day']) && ($child['number_of_absent_day'] > 0)}
                                                                    {if {$child['number_of_absent_day']} > 9}{__("More than")}&nbsp;{/if}{$child['number_of_absent_day']}&nbsp;{__("day(s)")}
                                                                {else}
                                                                    {__("More than")}&nbsp;10&nbsp;{__("day(s)")}
                                                                {/if}
                                                            </td>
                                                            <td>{$child['reason']}</td>
                                                        </tr>
                                                        {$idx = $idx + 1}
                                                    {/foreach}
                                                    {if count($insights['conabs']['list']) > 10}
                                                        <tr>
                                                            <td colspan="5" align="center" style="padding: 5px">
                                                                <button class="hide_absent_but btn btn-xs btn-info mb0">{__("Read less")}</button>
                                                            </td>
                                                        </tr>
                                                    {/if}
                                                {elseif $cnt == 0}
                                                    <tr class="odd">
                                                        <td valign="top" align="center" colspan="5" class="dataTables_empty">
                                                            {__("No data available in table")}
                                                        </td>
                                                    </tr>
                                                {/if}
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            {/if}

                            {if isAdmin($username) || in_array($user->_data['user_id'], $principals)}
                                {*Tổng hợp một số thông tin của trường*}
                                <div class="box-primary">
                                    <div class="box-header">
                                        <i class="fa fa-info-circle"></i>
                                        <strong>{__("General")}</strong>
                                    </div>
                                    <div class="list-group">
                                        <a class="list-group-item" href="{$system.system_url}/school/{$username}/classes">
                                            <span class="badge">{$school['class_count']}</span>
                                            <i class="fa fa-graduation-cap"></i> {__("Class")}
                                        </a>
                                        <a class="list-group-item" href="{$system.system_url}/school/{$username}/children">
                                            <span class="badge">{$insights['child_cnt']}</span>
                                            <i class="fa fa-child"></i> {__("Children")}
                                        </a>
                                        <a class="list-group-item">
                                            <span class="badge">{$school['male_count']}</span>
                                            <i class="fa fa-male"></i> {__("Males")}
                                        </a>
                                        <a class="list-group-item">
                                            <span class="badge">{$school['female_count']}</span>
                                            <i class="fa fa-female"></i> {__("Females")}
                                        </a>
                                        <a class="list-group-item" href="{$system.system_url}/school/{$username}/teachers">
                                            <span class="badge">{$school['teacher_count']}</span>
                                            <i class="fa fa-street-view"></i> {__("Teacher")}
                                        </a>
                                    </div>
                                </div>
                            {/if}
                        </div>
                        {*Hết cột trái*}

                        {*Cột bên phải*}
                        <div class="col-sm-6">
                            {if canView($username, "medicines")}
                                {*Khối thông tin gửi thuốc*}
                                <div class="box-primary">
                                    <div class="box-header">
                                        <i class="fa fa-medkit"></i>
                                        <strong>{__("Medicines")}</strong>&nbsp;
                                        <a href="{$system['system_url']}/school/{$username}/medicines/all" title="{__("See All")}" class="pull-right flip">{__("See All")}</a>
                                    </div>
                                    <div class="list-group">
                                        <table class="table table-striped table-bordered table-hover">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>{__("Child")}</th>
                                                <th>{__("Class")}</th>
                                                <th>{__("Medicine list")}</th>
                                                <th>{__("Times/day")}</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            {$idx = 1}
                                            {foreach $insights['medicines'] as $row}
                                                {if is_array($row)}
                                                    <tr>
                                                        <td align="center" style="vertical-align:middle"><strong>{$idx}</strong></td>
                                                        <td style="vertical-align:middle">
                                                            <a href="{$system['system_url']}/school/{$username}/medicines/detail/{$row['medicine_id']}">{$row['child_name']}</a>
                                                            {if $row['status']== $smarty.const.MEDICINE_STATUS_NEW}
                                                                <img src="{$system['system_url']}/content/themes/{$system['theme']}/images/new.gif"/>
                                                            {elseif $row['status']== $smarty.const.MEDICINE_STATUS_CONFIRMED}
                                                                <i class="fa fa-check"></i>
                                                            {/if}
                                                            <br/>({$row['birthday']})
                                                        </td>
                                                        <td align="center" style="vertical-align:middle">{$row['group_title']}</td>
                                                        <td style="vertical-align:middle">
                                                            {$row['medicine_list']}
                                                            {if $row['source_file'] != null}<br/><br/><a href = "{$row['source_file']}" target="_blank">{__("Doctor prescription")|upper}</a>{/if}
                                                        </td>
                                                        <td align="center" style="vertical-align:middle">{count($row['detail'])}/{$row['time_per_day']}</td>
                                                    </tr>
                                                    {$idx = $idx + 1}
                                                {/if}
                                            {/foreach}

                                            {if $insights['medicines']|count == 0}
                                                <tr class="odd">
                                                    <td valign="top" align="center" colspan="7" class="dataTables_empty">
                                                        {__("No data available in table")}
                                                    </td>
                                                </tr>
                                            {/if}

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            {/if}

                            {if canView($username, "birthdays") && (count($insights['birthdays']) > 0)}
                                {*Khối sinh nhật*}
                                <div class="box-primary">
                                    <div class="box-header">
                                        <i class="fa fa-birthday-cake"></i>
                                        <strong>{__("Birthday")}</strong>&nbsp;
                                        <a href="{$system['system_url']}/school/{$username}/birthdays" title="{__("See All")}" class="pull-right flip">{__("See All")}</a>
                                    </div>
                                    <div class="list-group">
                                        <table class="table table-striped table-bordered table-hover" id="hide_birthday">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>{__('Children')}</th>
                                                <th>{__('Birthdate')}</th>
                                                <th>{__('Class')}</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            {foreach $insights['birthdays'] as $k => $row}
                                                {if $k < 10}
                                                    <tr>
                                                        <td align="center">{$k + 1 }</td>
                                                        <td>{convertText4Web($row['child_name'])}</td>
                                                        <td align="center">{toSysDate($row['birthday'])}</td>
                                                        <td nowrap="true">
                                                            {if $row['group_title'] == null}
                                                                {__("No class")}
                                                            {else}
                                                                {convertText4Web($row['group_title'])}
                                                            {/if}
                                                        </td>
                                                    </tr>
                                                {/if}
                                            {/foreach}
                                            {if count($insights['birthdays']) > 10}
                                                <tr>
                                                    <td colspan="4" align="center" style="padding: 5px">
                                                        <button class="full_birthday_but btn btn-xs btn-info mb0">{__("See More")}</button>
                                                    </td>
                                                </tr>
                                            {/if}
                                            </tbody>
                                        </table>
                                        <table class="table table-striped table-bordered table-hover x-hidden" id="full_birthday">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>{__('Children')}</th>
                                                <th>{__('Birthdate')}</th>
                                                <th>{__('Class')}</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            {foreach $insights['birthdays'] as $k => $row}
                                                <tr>
                                                    <td align="center">{$k + 1 }</td>
                                                    <td>{convertText4Web($row['child_name'])}</td>
                                                    <td align="center">{toSysDate($row['birthday'])}</td>
                                                    <td nowrap="true">
                                                        {if $row['group_title'] == null}
                                                            {__("No class")}
                                                        {else}
                                                            {convertText4Web($row['group_title'])}
                                                        {/if}
                                                    </td>
                                                </tr>
                                            {/foreach}
                                            {if count($insights['birthdays']) > 10}
                                                <tr>
                                                    <td colspan="4" align="center" style="padding: 5px">
                                                        <button class="hide_birthday_but btn btn-xs btn-info mb0">{__("Read less")}</button>
                                                    </td>
                                                </tr>
                                            {/if}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            {/if}

                            {if canView($username, "birthdays") && (count($insights['birthday_teacher']) > 0)}
                                {*Khối sinh nhật giáo viên*}
                                <div class="box-primary">
                                    <div class="box-header">
                                        <i class="fa fa-birthday-cake"></i>
                                        <strong>{__("Teacher birthday")}</strong>&nbsp;
                                        <a href="{$system['system_url']}/school/{$username}/birthdays" title="{__("See All")}" class="pull-right flip">{__("See All")}</a>
                                    </div>
                                    <div class="list-group">
                                        <table class="table table-striped table-bordered table-hover" id="hide_birthday_teacher">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>{__('Teacher')}</th>
                                                <th>{__('Birthdate')}</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            {foreach $insights['birthday_teacher'] as $k => $row}
                                                {if $k < 10}
                                                    <tr>
                                                        <td align="center">{$k + 1 }</td>
                                                        <td>{convertText4Web($row['user_fullname'])}</td>
                                                        <td align="center">{$row['user_birthdate']}</td>
                                                    </tr>
                                                {/if}
                                            {/foreach}
                                            {if count($insights['birthday_teacher']) > 10}
                                                <tr>
                                                    <td colspan="3" align="center" style="padding: 5px">
                                                        <button class="full_birthday_teacher_but btn btn-xs btn-info mb0">{__("See More")}</button>
                                                    </td>
                                                </tr>
                                            {/if}
                                            </tbody>
                                        </table>
                                        <table class="table table-striped table-bordered table-hover x-hidden" id="full_birthday_teacher">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>{__('Teacher')}</th>
                                                <th>{__('Birthdate')}</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            {foreach $insights['birthday_teacher'] as $k => $row}
                                                <tr>
                                                    <td align="center">{$k + 1 }</td>
                                                    <td>{convertText4Web($row['user_fullname'])}</td>
                                                    <td align="center">{$row['user_birthdate']}</td>
                                                </tr>
                                            {/foreach}
                                            {if count($insights['birthday_teacher']) > 10}
                                                <tr>
                                                    <td colspan="4" align="center" style="padding: 5px">
                                                        <button class="hide_birthday_teacher_but btn btn-xs btn-info mb0">{__("Read less")}</button>
                                                    </td>
                                                </tr>
                                            {/if}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            {/if}

                            {if isAdmin($username) || in_array($user->_data['user_id'], $principals)}
                                {*Tổng hợp lượt tương tác của trường*}
                                <div class="box-primary">
                                    <div class="box-header">
                                        <i class="fa fa-info-circle"></i>
                                        <strong>{__("Interactive synthesis")}&#32;&#47;&#32;{__("month")}</strong>
                                        {*<a href="{$system['system_url']}/school/{$username}/statistics" title="{__("See All")}" class="pull-right flip">{__("Detail")}</a>*}
                                    </div>
                                    <div class="list-group">
                                        <table class="table table-striped table-bordered table-hover">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>{__('Interactive section')}</th>
                                                <th>{__('Created')}</th>
                                                <th>{__('Number of interactions')}</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            {$index = 1}
                                            {foreach $insights['statistics'] as $type => $row}
                                                {if $type == 'page' || $type == 'post' || $type == "attendance" || $type == 'event' || $type == 'schedule' || $type == 'menu' || $type == 'report' || $type == 'tuition' || $type == 'medicine' || $type == 'pickup' || $type == 'diary' || $type == 'health'}
                                                    <tr>
                                                        <td align="center" class="align_middle">{$index}</td>
                                                        <td>
                                                            {if $type == 'page'}
                                                                {__("School page")}
                                                            {elseif $type == 'post'}
                                                                {__("Posts")}
                                                            {elseif $type == 'attendance'}
                                                                {__("Resign")}
                                                            {elseif $type == 'event'}
                                                                {__("Notification - Event")}
                                                            {elseif $type == 'schedule'}
                                                                {__("Schedule")}
                                                            {elseif $type == 'menu'}
                                                                {__("Menu")}
                                                            {elseif $type == 'report'}
                                                                {__("Contact book")}
                                                            {elseif $type == 'tuition'}
                                                                {__("Tuition")}
                                                            {elseif $type == 'medicine'}
                                                                {__("Medicines")}
                                                            {elseif $type == 'pickup'}
                                                                {__("Late pickup")}
                                                            {elseif $type == 'diary'}
                                                                {__("Diary corner")}
                                                            {elseif $type == 'health'}
                                                                {__("Health")}
                                                            {/if}
                                                        </td>
                                                        <td align="center" class="align_middle">
                                                            {if $type == 'page'}
                                                                -
                                                            {elseif $type == 'post'}
                                                                -
                                                            {elseif $type == 'attendance'}
                                                                -
                                                            {elseif $type == 'event'}
                                                                -
                                                            {elseif $type == 'schedule'}
                                                                -
                                                            {elseif $type == 'menu'}
                                                                -
                                                            {elseif $type == 'report'}
                                                                -
                                                            {elseif $type == 'tuition'}
                                                                -
                                                            {elseif $type == 'medicine'}
                                                                {$row['parent_createds']}
                                                            {elseif $type == 'pickup'}
                                                                -
                                                            {elseif $type == 'diary'}
                                                                {$row['parent_createds']}
                                                            {elseif $type == 'health'}
                                                                {$row['parent_createds']}
                                                            {/if}
                                                        </td>
                                                        <td align="center" class="align_middle">
                                                            {if $type == 'page' || $type == 'post'}
                                                                {if isset($row['school_views'])}
                                                                    {$row['school_views']}
                                                                {else}
                                                                    0
                                                                {/if}
                                                            {else}
                                                                {if isset($row['parent_views'])}
                                                                    {$row['parent_views']}
                                                                {else}
                                                                    0
                                                                {/if}
                                                            {/if}
                                                        </td>
                                                    </tr>
                                                {/if}
                                                {$index = $index + 1}
                                            {/foreach}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            {/if}
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            {if canView($username, "review")}
                                {*Tổng hợp đánh giá trường và giáo viên*}
                                <div class="box-primary">
                                    <div class="box-header">
                                        <i class="fa fa-star"></i>
                                        <strong>{__("Parent review your child's school and teachers")}</strong>
                                    </div>
                                    <div class="list-group">
                                        <table class="table table-striped table-bordered table-hover">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>{__("Overall review")}</th>
                                                <th>{__("Average review")}</th>
                                                <th>{__("New review")}</th>
                                                <th>{__("Actions")}</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td class="align_middle">{__("School")}</td>
                                                <td class="align_middle" align="center">{$insights['reviews']['school']['total_review']}</td>
                                                <td class="align_middle" align="center">{$insights['reviews']['school']['average_review']}</td>
                                                <td class="align_middle" align="center">{$insights['reviews']['school']['cnt_new_review']}</td>
                                                <td align="center" class="align_middle">
                                                    <a class="btn btn-xs btn-default" href="{$system['system_url']}/school/{$username}/reviews">{__("Detail")}</a>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td class="align_middle">{__("Teacher")}</td>
                                                <td class="align_middle" align="center">{$insights['reviews']['teacher']['total_review']}</td>
                                                <td class="align_middle" align="center">{$insights['reviews']['teacher']['average_review']}</td>
                                                <td class="align_middle" align="center">{$insights['reviews']['teacher']['cnt_new_review']}</td>
                                                <td align="center" class="align_middle">
                                                    <a class="btn btn-xs btn-default" href="{$system['system_url']}/school/{$username}/reviews/teachers">{__("Detail")}</a>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            {/if}
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            {if canView($username, "services")}
                                {*Tổng hợp về dịch vụ ăn uống*}
                                <div class="box-primary">
                                    <div class="box-header">
                                        <i class="fa fa-beer"></i>
                                        <strong>{__("Feed service summary")}</strong>
                                    </div>
                                    {$col = 3}
                                    <div class="list-group">
                                        <table class="table table-striped table-bordered table-hover">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>{__("Class")}</th>
                                                <th>{__("Present")} +<br/>{__("Without permission")}</th>
                                                {foreach $insights['food_services']['service_list'] as $service}
                                                    <th>{$service['service_name']}</th>
                                                    {$col = $col + 1}
                                                {/foreach}
                                            </tr>
                                            </thead>
                                            <tbody>
                                            {$idx = 1}
                                            {$presentTotal = 0}
                                            {foreach $insights['food_services']['classes'] as $class}
                                                <tr>
                                                    <td align="center">{$idx}</td>
                                                    <td nowrap="true">{$class['group_title']}</td>
                                                    <td align="center">{$class['present_total']}</td>
                                                    {if isset($class['not_countbased_services']) && (count($class['not_countbased_services']) > 0)}
                                                        {foreach $class['not_countbased_services'] as $service}
                                                            <td align="center">{$service['total']}</td>
                                                        {/foreach}
                                                    {else}
                                                        {foreach $insights['food_services']['service_list'] as $service}
                                                            {if $service['type'] != $smarty.const.SERVICE_TYPE_COUNT_BASED}
                                                                <td align="center">0</td>
                                                            {/if}
                                                        {/foreach}
                                                    {/if}
                                                    {if isset($class['countbased_services']) && (count($class['countbased_services']) > 0)}
                                                        {foreach $class['countbased_services'] as $service}
                                                            <td align="center">{$service['total']}</td>
                                                        {/foreach}
                                                    {else}
                                                        {foreach $insights['food_services']['service_list'] as $service}
                                                            {if $service['type'] == $smarty.const.SERVICE_TYPE_COUNT_BASED}
                                                                <td align="center">0</td>
                                                            {/if}
                                                        {/foreach}
                                                    {/if}
                                                </tr>
                                                {$presentTotal = $presentTotal + $class['present_total']}
                                                {$idx = $idx + 1}
                                            {/foreach}
                                            {if $idx > 2}
                                                <tr>
                                                    <td colspan="2" align="center" style="vertical-align:middle"><strong><font color="red">{__("Total")|upper}</font></strong></td>
                                                    <td align="center" style="vertical-align:middle"><strong><font color="red">{$presentTotal}</font></strong></td>
                                                    {foreach $insights['food_services']['service_total'] as $total}
                                                        <td align="center" style="vertical-align:middle"><strong><font color="red">{$total}</font></strong></td>
                                                    {/foreach}
                                                </tr>
                                            {/if}

                                            {if $idx == 1}
                                                <tr class="odd">
                                                    <td valign="top" align="center" colspan="{$col}" class="dataTables_empty">
                                                        {__("No data available in table")}
                                                    </td>
                                                </tr>
                                            {/if}

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            {/if}
                        </div>
                    </div>
                </div>
            {/if}
            {if isAdmin($username)}
                <div class="mb10" align="center">
                    <a href="#" class="js_school-step-restart btn btn-warning btn-xs" data-step="0" data-username="{$username}"><strong>{__("Restart school")}</strong></a> <br/>
                    {__('Bấm vào "Khởi tạo lại trường" để thiết lập lại từ đầu (không bị mất những dữ liệu đã tạo trước đó)')}
                </div>
            {/if}
        </div>
    {/if}
</div>

