{if $sheet['error'] == 1}
    <table class="table table-striped table-bordered table-hover">
        <thead>
        <tr><th>{__("Detail results")}</th></tr>
        </thead>
        <tbody>
        <tr><td><div>{$sheet['message']}</div></td></tr>
        </tbody>
    </table>
{else}
    <table class="table table-striped table-bordered table-hover">
        <thead>
        <tr><th colspan="4">{__("Activity created")}&nbsp;({$sheet['menu_list']|count})</th></tr>
        <tr>
            <th>#</th>
            <th>{__("Time")}</th>
            <th>{__("Activity")}</th>
            <th>{__("Status")}</th>
        </tr>
        </thead>
        <tbody>
        {$idx = 1}
        {foreach $sheet['menu_list'] as $subject}
            <tr>
                <td>{$idx}</td>
                <td>{$subject['start']}</td>
                <td>{$subject['meal']}</td>
                <td>
                    {if $subject['error'] == 0}
                        {__("Create menu successfull")}
                    {else}
                        {$subject['message']}
                    {/if}
                </td>
            </tr>
            {$idx = $idx + 1}
        {/foreach}
        </tbody>
    </table>
{/if}