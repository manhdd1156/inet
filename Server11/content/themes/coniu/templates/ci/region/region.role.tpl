<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <i class="fa fa-university fa-fw fa-lg pr10"></i>
        {__("Permission")}
        {*{if $sub_view == "edit"}*}
        {*&rsaquo; {$data['page_title']}*}
        {*{if $configure == 'basic'} &rsaquo; {__("Basic")}*}
        {*{elseif $configure == 'services'} &rsaquo; {__("Service")}*}
        {*{elseif $configure == 'addmission'} &rsaquo; {__("Admission")}*}
        {*{elseif $configure == 'info'} &rsaquo; {__("More information")}*}
        {*{/if}*}
        {*{/if}*}
    </div>
    {if $sub_view == ""}
        <div class="panel-body with-table">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/region/bo_region_userregion.php">
                <input type="hidden" name="do" value="add_region_user"/>
                <input type="hidden" name="city_id" value="{$city_id}">
                <div class="form-group">
{*                    <label class="col-sm-3 control-label text-left">{__("City")}</label>*}
{*                    <div class="col-sm-3">*}
{*                        <select class="form-control" name="city_id" id = "city_id">*}
{*                            {foreach $cities as $city}*}
{*                                <option value="{$city['city_id']}">{$city['city_name']}</option>*}
{*                            {/foreach}*}
{*                        </select>*}
{*                    </div>*}
                    <div class="{if $region_manage['district_slug']}x-hidden{/if}">
                        <label class="col-sm-3 control-label text-left">{__("District")}</label>
                        <div class="col-sm-3">
                            <select class="form-control" name="district_slug" id = "district_slug">
                                <option value="0">{__('Select district')}</option>
                                {foreach $cities as $city}
                                    {if $city['city_id'] == $city_id}
                                        {foreach $city['district'] as $district}
                                            <option value="{$district['district_slug']}" {if $district['district_slug'] == $region_manage['district_slug']}selected{/if}> {$district['district_name']}
                                            </option>
                                        {/foreach}
                                    {/if}
                                {/foreach}
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Grade")}</label>
                    <div class="col-sm-3">
                        <select class="form-control" name="grade" id = "grade">
                            <option value="">
                                {__("Grade select")}
                            </option>
                            {foreach $grades as $key => $grade}
                                <option value="{$key}">{__($grade)}</option>
                            {/foreach}
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("REGION Management")}</label>
                    <div class="col-sm-9">
                        <input name="search-manager" id="search-manager" type="text" class="form-control" placeholder="{__("Enter username, email or fullname to search")}" autocomplete="off">
                        <div id="search-manager-results" class="dropdown-menu dropdown-widget dropdown-search">
                            <div class="dropdown-widget-header">
                                {__("Search Results")}
                            </div>
                            <div class="dropdown-widget-body">
                                <div class="loader loader_small ptb10"></div>
                            </div>
                        </div>
                        <br/>
                        <div class="col-sm-9" id="manager_list" name="manager_list"></div>
                    </div>
                </div>
                {if $region_manage['provider']}
                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-3">
                            <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                        </div>
                    </div>

                {else}
                    <div class="form-group" align="center">
                        <div class="col-sm-12">
                            <strong>{__("Bạn không được phân quyền cho quản lý khác")}</strong>
                        </div>
                    </div>
                {/if}

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>

            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover js_dataTable">
                    <thead>
                    <tr>
                        <th>{__("#")}</th>
                        <th>{__("District")}</th>
                        <th>{__("Grade")}</th>
                        <th>{__("Account")}</th>
                        <th>{__("Actions")}</th>
                    </tr>
                    </thead>
                    <tbody>
                    {$idx = 1}
                    {foreach $result as $row}
                        <tr>
                            <td>{$idx}</td>
                            <td>
                                {foreach $cities as $city}
                                    {if $city['city_id'] == $row['city_id']}
                                        {foreach $city['district'] as $district}
                                            {if $district['district_slug'] == $row['district_slug']}
                                                {$district['district_name']}
                                            {/if}
                                        {/foreach}
                                    {/if}
                                {/foreach}
                            </td>
                            <td>
                                {$grades[$row['grade']]}
                            </td>
                            <td>
                                {$row['user_email']}
                            </td>

                            <td>
                                {if $region_manage['provider']}
                                    <button class="btn btn-xs btn-danger js_region_manage-delete-region" data-id="{$row['region_manage_id']}">
                                        <i class="fas fa-trash"></i>
                                    </button>
                                {/if}
                            </td>
                        </tr>
                        {$idx = $idx + 1}
                    {/foreach}
                    </tbody>
                </table>
            </div>
        </div>
    {/if}
</div>