<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover js_dataTable">
        <thead>
        <tr><th colspan="2">{__("School list")}&nbsp;({$rows|count})</th></tr>
        </thead>
        <tbody>
        {$idx = 1}
        {$grade = -1}
        {foreach $rows as $row}
            {if $row['config']['grade'] != $grade}
                {$grade = $row['config']['grade']}
                {if $row['config']['grade'] == 0}
                    <tr style="font-size: 15px">
                        <td colspan="2">
                            <strong>{__("Pre - school")} ({$count[0]})</strong>
                        </td>
                    </tr>
                {elseif $row['config']['grade'] == 1}
                    <tr style="font-size: 15px"><td colspan="2"><strong>{__("Cấp 1")} ({$count[1]})</strong></td></tr>
                {elseif $row['config']['grade'] == 2}
                    <tr style="font-size: 15px"><td colspan="2"><strong>{__("Cấp 2")} ({$count[2]})</strong></td></tr>
                {elseif $row['config']['grade'] == 99}
                    <tr style="font-size: 15px"><td colspan="2"><strong>{__("Liên cấp")} ({$count[99]})</strong></td></tr>
                {else}
                    <tr style="font-size: 15px"><td colspan="2"><strong>{__("Chưa xác đinh")} ({$count[100]})</strong></td></tr>
                {/if}
            {/if}
            <tr>
                <td>
                    <a href="{$system['system_url']}/school/{$row['info']['page_name']}"><strong>{$idx}. {$row['info']['page_title']} </strong></a><br/>
                    {$row['info']['address']}
                </td>
                <td align="center" width="60">
                    <strong>{$row['children']}</strong> {__("Student")}
                </td>
            </tr>
            {$idx = $idx + 1}
        {/foreach}
        </tbody>
    </table>
</div>

<script type="text/javascript">
    $(function() {
        // run DataTable
        $('.js_dataTable').DataTable({
            "aoColumnDefs": [ { 'aDataSort': false, 'aTargets': [ -1 ] } ],
            "language": {
                "decimal":        "",
                "emptyTable":     __["No data available in table"],
                "info":           __["Showing _START_ to _END_ of _TOTAL_ results"],
                "infoEmpty":      __["Showing 0 to 0 of 0 results"],
                "infoFiltered":   "(filtered from _MAX_ total entries)",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     __["Show"] + " " + "_MENU_" + " " + __["results"],
                "loadingRecords": __["Loading..."],
                "processing":     __["Processing..."],
                "search":         __["Search"],
                "zeroRecords":    __["No matching records found"],
                "paginate": {
                    "first":      __["First"],
                    "last":       __["Last"],
                    "next":       __["Next"],
                    "previous":   __["Previous"]
                },
                "aria": {
                    "sortAscending":  ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                }
            }
        });
    });
</script>