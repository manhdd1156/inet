{$idx = 1}
{foreach $results['details'] as $row}
    <table class="table table-striped table-bordered table-hover" id = "addTempTable">
        <tr>
            <td><strong>{$idx} - {$row['category_name']}</strong></td>
            <input type="hidden" name="category_ids[]" value="{$row['report_template_category_id']}">
            <input type="hidden" name="category_name_{$row['report_template_category_id']}" value="{$row['category_name']}">
        </tr>
        <tr>
            <td>
                <textarea placeholder="{__("Other comment")}" class="col-sm-12 mt10 mb10 noteArea" name="content_{$row['report_template_category_id']}">{$row['template_content']}</textarea>
                {foreach $row['suggests'] as $suggest}
                    <div class="col-sm-4">
                        <input type="checkbox" value="{$suggest}" name="suggest_{$row['report_template_category_id']}[]"> {$suggest}
                    </div>
                {/foreach}
            </td>
        </tr>
    </table>
    {$idx = $idx + 1}
    {*jquery auto height*}
    <script type="text/javascript">
        $('.noteArea').each(function () {
            this.setAttribute('style', 'height:' + (this.scrollHeight) + 'px;overflow-y:hidden;');
        }).on('input', function () {
            this.style.height = 'auto';
            this.style.height = (this.scrollHeight) + 'px';
        });
    </script>
{/foreach}

