<div class="panel panel-default">
    <div class="panel-heading with-icon">
        {if $sub_view == ""}
            <div class="pull-right flip">
                <a href="{$system['system_url']}/noga/childdevelopments/add" class="btn btn-default">
                    <i class="fa fa-plus"></i> {__("Add New")}
                </a>
            </div>
        {elseif $sub_view == "add"}
            <div class="pull-right flip">
                <a href="{$system['system_url']}/noga/childdevelopments" class="btn btn-default">
                    <i class="fa fa-list"></i> {__("Lists")}
                </a>
            </div>
        {/if}
        <i class="fas fa-calendar-alt fa-fw fa-lg pr10"></i>
        {__("Child development")}
        {if $sub_view == ""}
        {elseif $sub_view == "add"}
            &rsaquo; {__("Add information about your child's development")}
        {elseif $sub_view == "edit"}
            &rsaquo; {__("Update information about your child's development")}
        {elseif $sub_view == "detail"}
            &rsaquo; {__('Detail')}
        {/if}
    </div>
    {if $sub_view == ""}
        <div class="panel-body with-table form-horizontal">
            <div class="row" style="padding-left: 11px">
                <div class='col-sm-3'>
                    <div class="form-group">
                        <select name="child_type" id="child_type" class="form-control js_noga-child-development-search">
                            <option value="0">{__("All")}</option>
                            <option value="1">{__("Vaccination")}</option>
                            <option value="2">{__("Information")}</option>
                        </select>
                    </div>
                </div>
            </div>
            <div id="noga_development_list" class="pt10">
                {include file="ci/noga/ajax.noga.childdevelopment.list.tpl"}
            </div>
        </div>
    {elseif $sub_view == "add"}
        <div class="panel-body">
            <form class="form-horizontal" enctype="multipart/form-data" method="post" id="create_child_development">
                <input type="hidden" name="do" value="add"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Type")}</label>
                    <div class="col-sm-3">
                        <select name = "type" class="form-control">
                            <option value = "1">{__("Vaccination")}</option>
                            <option value = "2">{__("Information")}</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Month push notification")} (*)</label>
                    <div class="col-sm-2">
                        <input type = "number" class="form-control" name="month_push" required autofocus min="1" value="{$data['month_push']}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Day")}</label>
                    <div class="col-sm-2">
                        <input type = "number" class="form-control" name="day_push" min="1" max="365" required>
                    </div>

                    <label class="col-sm-2 control-label text-left">{__("Time")}</label>
                    <div class="col-sm-2">
                        <input type = "text" class="form-control" name="time_push" required>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Notice before")}?</label>
                    <div class="col-sm-4">
                        <div class="onoffswitch">
                            <input type="checkbox" name="is_notice_before" class="onoffswitch-checkbox" id="is_notice_before">
                            <label class="onoffswitch-label" for="is_notice_before"></label>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <input type = "number" class="form-control" name="notice_before_days" value="0" min="0" required>
                    </div>
                    <span class="control-label inl-block">{__("day")}</span>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Reminder before")}?</label>
                    <div class="col-sm-4">
                        <div class="onoffswitch">
                            <input type="checkbox" name="is_reminder_before" class="onoffswitch-checkbox" id="is_reminder_before">
                            <label class="onoffswitch-label" for="is_reminder_before"></label>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <input type = "number" class="form-control" name="reminder_before_days" value="0" min="0" required>
                    </div>
                    <span class="control-label inl-block">{__("day")}</span>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Title")} (*)</label>
                    <div class="col-sm-9">
                        <input type = "text" class="form-control" name="title" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Text or Link?")}</label>
                    <div class="col-sm-3">
                        <select name = "content_type" class="form-control" id="content_type_select">
                            <option value = "3">{__("Text and Link")}</option>
                            <option value = "1">{__("Text")}</option>
                            <option value = "2">{__("Link")}</option>
                        </select>
                    </div>
                </div>
                <div class="form-group content_text">
                    <label class="col-sm-3 control-label text-left">{__("Text")}</label>
                    <div class="col-sm-9">
                        <textarea class="form-control js_autosize text" name="content_text" rows="6"></textarea>
                    </div>
                </div>
                <div class="form-group content_link">
                    <label class="col-sm-3 control-label text-left">{__("Link")}</label>
                    <div class="col-sm-9">
                        <textarea class="form-control link" name="link" rows="2"></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Picture")}</label>
                    <div class="col-sm-6">
                        <input type="file" name="file" id="file"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Show in too development")}</label>
                    <div class="col-sm-3">
                        <select name = "is_development" class="form-control">
                            <option value = "1" {if $data['is_development'] == 1}selected{/if}>{__("Yes")}</option>
                            <option value = "0" {if $data['is_development'] == 0}selected{/if}>{__("No")}</option>
                        </select>
                    </div>
                </div>
                <div class = "form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                    </div>
                </div>
            </form>
        </div>

    {elseif $sub_view == "edit"}
        <div class="panel-body">
            <form class="form-horizontal" enctype="multipart/form-data" method="post" id="create_child_development">
                <input type="hidden" name="do" value="edit"/>
                <input type="hidden" name="child_development_id" value="{$data['child_development_id']}"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Type")}</label>
                    <div class="col-sm-3">
                        <select name = "type" class="form-control">
                            <option value = "1" {if $data['type'] == VACCINATION}selected{/if}>{__("Vaccination")}</option>
                            <option value = "2" {if $data['type'] == FOETUS_INFORMATION}selected{/if}>{__("Information")}</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Month push notification")} (*)</label>
                    <div class="col-sm-2">
                        <input type = "number" class="form-control" name="month_push" required autofocus min="1" value="{$data['month_push']}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Day")}</label>
                    <div class="col-sm-2">
                        <input type = "number" class="form-control" name="day_push" value = "{$data['day_push']}">
                    </div>

                    <label class="col-sm-2 control-label text-left">{__("Time")}</label>
                    <div class="col-sm-2">
                        <input type = "text" class="form-control" name="time_push" value = "{$data['time_push']}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Notice before")}?</label>
                    <div class="col-sm-4">
                        <div class="onoffswitch">
                            <input type="checkbox" name="is_notice_before" class="onoffswitch-checkbox" id="is_notice_before" {if $data['is_notice_before']}checked{/if}>
                            <label class="onoffswitch-label" for="is_notice_before"></label>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <input type="number" class="form-control" name="notice_before_days" value="{$data['notice_before_days']}">
                    </div>
                    <span class="control-label inl-block">{__("day")}</span>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Reminder before")}?</label>
                    <div class="col-sm-4">
                        <div class="onoffswitch">
                            <input type="checkbox" name="is_reminder_before" class="onoffswitch-checkbox" id="is_reminder_before" {if $data['is_reminder_before']}checked{/if}>
                            <label class="onoffswitch-label" for="is_reminder_before"></label>
                        </div>
                        {*<span class="help-block">
                            {__("")}
                        </span>*}
                    </div>

                    <div class="col-sm-2">
                        <input type = "number" class="form-control" name="reminder_before_days" value = "{$data['reminder_before_days']}">
                    </div>
                    <span class="control-label inl-block">{__("day")}</span>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Title")} (*)</label>
                    <div class="col-sm-9">
                        <input type = "text" class="form-control" name="title" required value = "{$data['title']}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Text or Link?")}</label>
                    <div class="col-sm-3">
                        <select name = "content_type" class="form-control" id="content_type_select">
                            <option value = "3" {if $data['content_type'] == 3}selected{/if}>{__("Text and Link")}</option>
                            <option value = "1" {if $data['content_type'] == 1}selected{/if}>{__("Text")}</option>
                            <option value = "2" {if $data['content_type'] == 2}selected{/if}>{__("Link")}</option>
                        </select>
                    </div>
                </div>
                <div class="form-group content_text {if $data['content_type'] == 2}x-hidden{/if}">
                    <label class="col-sm-3 control-label text-left">{__("Text")}</label>
                    <div class="col-sm-9">
                        <textarea class="form-control js_autosize text" name="content_text" rows="6">{$data['content']}</textarea>
                    </div>
                </div>
                <div class="form-group content_link {if $data['content_type'] == 1}x-hidden{/if}">
                    <label class="col-sm-3 control-label text-left">{__("Link")}</label>
                    <div class="col-sm-9">
                        <textarea class="form-control link" name="link" rows="2">{$data['link']}</textarea>
                    </div>
                </div>

                <input type = "hidden" id="is_file" name="is_file" value = "1">
                <div class="form-group" id="file_old">
                    <label class="rp-file col-sm-3 control-label text-left" style="padding-top: 0px">{__("Picture")}</label>
                    <div class="col-sm-6">
                        {if !is_empty($data['image_source'])}
                            <a href="{$data['image_source']}" target="_blank"><img src="{$data['image_source']}" class = "img-responsive"></a>
                            <input type = "hidden" name="old_src" value = "{$data['image']}">
                        {else} {__('No file attachment')}
                            <input type = "hidden" name="old_src" value = "">
                        {/if}
                    </div>
                </div>
                <div class = "form-group">
                    <label class="control-label col-sm-3">
                        {if is_empty($data['image_source'])}
                            {__("Choose file")}
                        {else}
                            {__("Choose file replace")}
                        {/if}
                    </label>
                    <div class = "col-sm-6">
                        <input type="file" name="file" id="file"/>
                    </div>
                    <a class = "delete_image btn btn-danger btn-xs text-left">{__('Delete')}</a>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Show in too development")}</label>
                    <div class="col-sm-3">
                        <select name = "is_development" class="form-control">
                            <option value = "1" {if $data['is_development'] == 1}selected{/if}>{__("Yes")}</option>
                            <option value = "0" {if $data['is_development'] == 0}selected{/if}>{__("No")}</option>
                        </select>
                    </div>
                </div>
                <div class = "form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                    </div>
                </div>
                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->
                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    {elseif $sub_view == "push"}
        <div class="panel-body">
            <a href = "#" class="js_push-notify-child btn btn-default">{__("Push notify")}</a>
        </div>
    {/if}
</div>