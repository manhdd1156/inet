<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover js_dataTable">
        <thead>
        <tr>
            <th>{__("ID")}</th>
            <th>{__("Picture")}</th>
            <th>{__("URL")}</th>
            <th>{__("Title")}</th>
            <th>{__("Likes")}</th>
            <th>{__("Post time")}</th>
        </tr>
        </thead>
        <tbody>
        {foreach $results as $row}
            <tr>
                <td>
                    <a href="{$system['system_url']}/pages/{$row['page_name']}" target="_blank">
                        {$row['page_id']}
                    </a>
                </td>
                <td class="post-avatar">
                    <a target="_blank" class="post-avatar-picture" href="{$system['system_url']}/pages/{$row['page_name']}" style="background-image:url({$row['page_picture']});">
                    </a>
                </td>
                <td>
                    <a href="{$system['system_url']}/pages/{$row['page_name']}" target="_blank">
                        {$row['page_name']}
                    </a>
                </td>
                <td>
                    <a href="{$system['system_url']}/pages/{$row['page_name']}" target="_blank">
                        {$row['page_title']}
                    </a>
                </td>
                <td>{$row['page_likes']}</td>
                <td>
                    {$row['time']}
                </td>
            </tr>
        {/foreach}
        </tbody>
    </table>
</div>