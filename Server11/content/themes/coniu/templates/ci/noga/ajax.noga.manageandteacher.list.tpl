<strong>{__("School list")}&nbsp;({$results|count})</strong>
<div class="pull-right flip">
    <a href="#" id="export2excel" class="btn btn-primary js_noga-export-user-role" data-username="{$username}" data-id="{$school['page_id']}">{__("Export to Excel")}</a>
    <label id="export_processing" class="btn btn-info x-hidden">{__("Loading")}...</label>
</div>
<div class="pt20">
    <table class="table table-striped table-bordered table-hover js_dataTable">
        <thead>
        <tr>
            <th>{__("#")}</th>
            <th>{__("Full name")}</th>
            <th>{__("Username")}</th>
            <th>{__("Email")}</th>
            <th>{__("Phone")}</th>
            <th>{__("Birthday")}</th>
            <th>{__("Gender")}</th>
        </tr>
        </thead>
        <tbody>
        {foreach $results as $school}
            <tr>
                <td colspan="7"><strong>{$school['page_title']}</strong> ({$school['users']|count}) ({if $school['school_status'] == 1}{__("Using Mascom - Edu")}{elseif $school['school_status'] == 2}{__("Having Mascom - Edu's page")}{else}{__("Waiting confirmation")}{/if})</td>
            </tr>
            {$idx = 1}
            {foreach $school['users'] as $row}
                <tr>
                    <td align="center"><strong>{$idx}</strong></td>
                    <td><a href="{$system['system_url']}/{$row['user_name']}"><strong>{$row['user_fullname']}</strong></a></td>
                    <td>{$row['user_name']}</td>
                    <td>{$row['user_email']}</td>
                    <td>{$row['user_phone']}</td>
                    <td align="center">{$row['user_birthdate']}</td>
                    <td>
                        {if $row['user_gender'] == 'male'}
                            {__("Male")}
                        {else}
                            {__("Female")}
                        {/if}
                    </td>
                </tr>
                {$idx = $idx + 1}
            {/foreach}
        {/foreach}
        </tbody>
    </table>
</div>
<script type="text/javascript">
    $(function() {
        // run DataTable
        $('.js_dataTable').DataTable({
            "aoColumnDefs": [ { 'aDataSort': false, 'aTargets': [ -1 ] } ],
            "language": {
                "decimal":        "",
                "emptyTable":     __["No data available in table"],
                "info":           __["Showing _START_ to _END_ of _TOTAL_ results"],
                "infoEmpty":      __["Showing 0 to 0 of 0 results"],
                "infoFiltered":   "(filtered from _MAX_ total entries)",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     __["Show"] + " " + "_MENU_" + " " + __["results"],
                "loadingRecords": __["Loading..."],
                "processing":     __["Processing..."],
                "search":         __["Search"],
                "zeroRecords":    __["No matching records found"],
                "paginate": {
                    "first":      __["First"],
                    "last":       __["Last"],
                    "next":       __["Next"],
                    "previous":   __["Previous"]
                },
                "aria": {
                    "sortAscending":  ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                }
            }
        });
    });
</script>