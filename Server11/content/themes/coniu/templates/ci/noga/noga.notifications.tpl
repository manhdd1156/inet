<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <i class="fa fa-bell fa-fw fa-lg pr10"></i>
        {__("Notifications")}
        {if $sub_view == "immediately"}
            &rsaquo; {__("Notify immediately")}
        {elseif $sub_view == "schedule"}
            &rsaquo; {__("Appointment")}
        {elseif $sub_view == "lists"}
            &rsaquo; {__('Lists')}
        {elseif $sub_view == "edit"}
            &rsaquo; {$data['title']}
        {/if}
    </div>
    {if $sub_view == "immediately"}

        <div class="panel-body">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/noga/bo_noga_notification.php">
                <input type="hidden" name="do" value="add"/>

                <div class="form-group">
                    <label class="col-sm-3 control-label">
                        {__("Loại thông báo")} (*)
                    </label>
                    <div class="col-sm-9">
                        <div id="type_notification" class="col-xs-4">
                            <select class="form-control" name="type" id="type_notification">
                                <option value="">-----{__("Chọn loại thông báo")}-----</option>
                                <option value="notification_coniu">{__("Thông báo từ coniu")}</option>
                                <option value="notification_coniu_info">{__("Thông tin hot muốn truyền tải")}</option>
                                <option value="notification_coniu_congratulations">{__("Sự kiện, chúc mừng")}</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">
                        {__("Đối tượng")} (*)
                    </label>
                    <div class="col-sm-9 mt10">
                        <div class="col-xs-12">
                            <input type="checkbox" id="cb_type_user" name="cb_type_user" value="1">{__("Đối tượng")}
                        </div>
                        <div id="div_type_user" class="col-xs-4 x-hidden">
                            <select class="form-control col-sm-3" name="type_user">
                                <option value="1">{__("Tất cả user")}</option>
                                <option value="2">{__("Quản lý")}</option>
                                <option value="3">{__("Giáo viên")}</option>
                                <option value="4">{__("Phụ huynh")}</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label"></label>
                    <div class="col-sm-9">
                        <div class="col-xs-2">
                            <input type="checkbox" id="cb_area" name="cb_area" value="1">{__("Khu vực")}
                        </div>

                        <div id="div_area_city" class="col-xs-4 x-hidden">
                            <select class="form-control" name="city_id" id="area_city">
                                <option value="">{__("Select city")}</option>
                                {foreach $cities as $city}
                                    <option value="{$city['city_id']}" {if $data['city_id']==$city['city_id']}selected{/if}>{$city['city_name']}</option>
                                {/foreach}
                            </select>
                        </div>

                        <div class="col-xs-2">
                            <input type="checkbox" id="cb_school" name="cb_school" value="1">{__("Trường")}
                        </div>
                       <div id="div_school" class="col-xs-3 x-hidden">
                            <select class="form-control" name="district_school" id="school_slug">
                                {*<option value="{$dataCon['district_slug']}" selected>{$dataCon['district_name']}</option>*}
                            </select>
                        </div>

                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label"></label>
                    <div class="col-sm-9">
                        <div class="col-xs-12">
                            <input type="checkbox" id="cb_gender" name="cb_gender" value="1">{__("Gender")}
                        </div>
                        <div id="div_gender" class="col-xs-4 x-hidden">
                            <select class="form-control col-sm-3" name="gender">
                                <option value="1">{__("Male")}</option>
                                <option value="2">{__("Female")}</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label"></label>
                    <div class="col-sm-9">
                        <div class="col-xs-12">
                            <input type="checkbox" id="cb_age" name="cb_age" value="1">{__("Age")}
                        </div>
                        <div id="div_from_age" class="col-xs-2 x-hidden">
                            <input class="form-control" name="from_age" type="number" min="0" step="1">
                        </div>
                        <div id="div_to_age" class="col-xs-2 x-hidden">
                            <input class="form-control" name="to_age" type="number" min="0" step="1">
                        </div>

                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Title")} (*)</label>
                    <div class="col-sm-9">
                        <input class="form-control" name="title" required maxlength="250">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("URL")}</label>
                    <div class="col-sm-9">
                        <input class="form-control" name="link" maxlength="250">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Content")} (*)</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="content" rows="7" required></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Description")}</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="description" rows="3" required></textarea>
                    </div>
                </div>


                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>

    {elseif $sub_view == "schedule"}

        <div class="panel-body">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/noga/bo_noga_notification.php">
                <input type="hidden" name="do" value="add"/>

                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Time")} (*)</label>
                    <div class='col-sm-3'>
                        <div class='input-group date' id='push_time'>
                            <input type='text' name="push_time" class="form-control" placeholder="{__("Time")}" required/>
                            <span class="input-group-addon">
                                    <span class="fas fa-calendar-alt"></span>
                                </span>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">
                        {__("Đối tượng")} (*)
                    </label>
                    <div class="col-sm-9 mt10">
                        <div class="col-xs-12">
                            <input type="checkbox" id="cb_type_user" name="cb_type_user" value="1">{__("Đối tượng")}
                        </div>
                        <div id="div_type_user" class="col-xs-4 x-hidden">
                            <select class="form-control col-sm-3" name="type_user">
                                <option value="1">{__("Tất cả user")}</option>
                                <option value="2">{__("Quản lý")}</option>
                                <option value="3">{__("Giáo viên")}</option>
                                <option value="4">{__("Phụ huynh")}</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label"></label>
                    <div class="col-sm-9">
                        <div class="col-xs-12">
                            <input type="checkbox" id="cb_area" name="cb_area" value="1">{__("Khu vực")}
                        </div>

                        <div id="div_area_city" class="col-xs-4 x-hidden">
                            <select class="form-control" name="city_id" id="area_city">
                                <option value="">{__("Select city")}</option>
                                {foreach $cities as $city}
                                    <option value="{$city['city_id']}" {if $data['city_id']==$city['city_id']}selected{/if}>{$city['city_name']}</option>
                                {/foreach}
                            </select>
                        </div>
                        {*<div id="div_area_district" class="col-xs-4 x-hidden">
                             <select class="form-control" name="district_slug" id="district_slug">
                                 <option value="{$dataCon['district_slug']}" selected>{$dataCon['district_name']}</option>
                             </select>
                         </div>*}

                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label"></label>
                    <div class="col-sm-9">
                        <div class="col-xs-12">
                            <input type="checkbox" id="cb_gender" name="cb_gender" value="1">{__("Gender")}
                        </div>
                        <div id="div_gender" class="col-xs-4 x-hidden">
                            <select class="form-control col-sm-3" name="gender">
                                <option value="1">{__("Male")}</option>
                                <option value="2">{__("Female")}</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label"></label>
                    <div class="col-sm-9">
                        <div class="col-xs-12">
                            <input type="checkbox" id="cb_age" name="cb_age" value="1">{__("Age")}
                        </div>
                        <div id="div_from_age" class="col-xs-2 x-hidden">
                            <input class="form-control" name="from_age" type="number" min="0" step="1">
                        </div>
                        <div id="div_to_age" class="col-xs-2 x-hidden">
                            <input class="form-control" name="to_age" type="number" min="0" step="1">
                        </div>

                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Lặp lại")}?</label>
                    <div class="col-sm-9">
                        <div class="onoffswitch">
                            <input type="checkbox" name="is_repeat" class="onoffswitch-checkbox" id="is_repeat" checked>
                            <label class="onoffswitch-label" for="is_repeat"></label>
                        </div>
                    </div>
                </div>

                <div class="form-group" id="cyclic">
                    <label class="col-sm-3 control-label text-left">{__("Chu kỳ")}</label>
                    <div class="col-xs-3">
                        <select name="repeat_time" id="repeat_time" class="form-control">
                            <option value="1">{__("Năm")}</option>
                            <option value="2">{__("Tháng")}</option>
                            <option value="3">{__("Ngày")}</option>
                            <option value="4">{__("Tuần")}</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Title")} (*)</label>
                    <div class="col-sm-9">
                        <input class="form-control" name="title" required maxlength="250">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("URL")}</label>
                    <div class="col-sm-9">
                        <input class="form-control" name="link" maxlength="250">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Content")} (*)</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="content" rows="7" required></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Description")}</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="description" rows="3" required></textarea>
                    </div>
                </div>


                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>

    {elseif $sub_view == "list"}
        <div class="panel-body with-table">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover js_dataTable">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>{__('Title')}</th>
                        <th>{__('Content')}</th>
                        <th>{__("Time")}</th>
                        <th>{__("Chu kỳ lặp")}</th>
                        <th>{__("Đã gửi")}</th>
                        <th>{__("Action")}</th>
                    </tr>
                    </thead>
                    <tbody>
                    {$idx = 1}
                    {foreach $rows as $row}
                        <tr>
                            <td align="center" class="align-middle"><strong>{$idx}</strong></td>
                            <td class="align-middle">
                                <a href="{$system['system_url']}/noga/notifications/edit/{$row['noga_notification_id']}">
                                    {$row['title']}
                                </a>
                            </td>
                            <td class="align-middle">
                                {$row['content']}
                            </td>
                            <td class="align-middle">
                                {$row['time']}
                            </td>
                            <td align="center" class="align-middle" nowrap="true">
                                {if $row['repeate_time'] == 0}
                                    {__("No")}
                                {elseif $row['repeate_time'] == 1}
                                    {__("Year")}
                                {elseif $row['repeate_time'] == 2}
                                    {__("Month")}
                                {elseif $row['repeate_time'] == 3}
                                    {__("Week")}
                                {elseif $row['repeate_time'] == 4}
                                    {__("Day")}
                                {/if}
                            </td>
                            <td align="center" class="align-middle">
                                {if $row['status'] == 1}
                                    {__("Yes")}
                                {else}
                                    {__("No")}
                                {/if}
                            </td>
                            <td align="center" class="align-middle">
                                <a href="{$system['system_url']}/noga/notifications/edit/{$row['noga_notification_id']}" class="btn btn-xs btn-default">{__("Edit")}</a>
                                <button class="btn btn-xs btn-danger js_noga-notification-delete" data-id="{$row['noga_notification_id']}" data-handle = "delete_notification">{__("Delete")}</button>
                            </td>

                        </tr>
                        {$idx = $idx + 1}
                    {/foreach}
                    </tbody>
                </table>
            </div>
        </div>

    {elseif $sub_view == "edit"}

        <div class="panel-body">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/noga/bo_noga_notification.php">
                <input type="hidden" name="notification_id" value="{$data['noga_notification_id']}"/>
                <input type="hidden" name="do" value="edit"/>

                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Time")} (*)</label>
                    <div class='col-sm-3'>
                        <div class='input-group date' id='push_time'>
                            <input type='text' name="push_time" value="{$data['time']}" class="form-control" placeholder="{__("Time")}" required/>
                            <span class="input-group-addon">
                                    <span class="fas fa-calendar-alt"></span>
                                </span>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">
                        {__("Đối tượng")} (*)
                    </label>
                    <div class="col-sm-9 mt10">
                        <div class="col-xs-12">
                            <input type="checkbox" id="cb_type_user" name="cb_type_user" value="1" {if $data['type_user']!= 0}checked{/if}>{__("Đối tượng")}
                        </div>
                        <div id="div_type_user" class="col-xs-4 {if $data['type_user']== 0}x-hidden{/if}">
                            <select class="form-control col-sm-3" name="type_user">
                                <option value="1" {if $data['type_user']== 1}selected{/if}>{__("Tất cả user")}</option>
                                <option value="2" {if $data['type_user']== 2}selected{/if}>{__("Quản lý")}</option>
                                <option value="3" {if $data['type_user']== 3}selected{/if}>{__("Giáo viên")}</option>
                                <option value="4" {if $data['type_user']== 4}selected{/if}>{__("Phụ huynh")}</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label"></label>
                    <div class="col-sm-9">
                        <div class="col-xs-12">
                            <input type="checkbox" id="cb_area" name="cb_area" value="1" {if $data['city_id']!= 0}checked{/if}>{__("Khu vực")}
                        </div>

                        <div id="div_area_city" class="col-xs-4 {if $data['city_id'] == 0}x-hidden{/if}">
                            <select class="form-control" name="city_id" id="area_city">
                                <option value="">{__("Select city")}</option>
                                {foreach $cities as $city}
                                    <option value="{$city['city_id']}" {if $data['city_id']==$city['city_id']}selected{/if}>{$city['city_name']}</option>
                                {/foreach}
                            </select>
                        </div>
                        {*<div id="div_area_district" class="col-xs-4 x-hidden">
                             <select class="form-control" name="district_slug" id="district_slug">
                                 <option value="{$dataCon['district_slug']}" selected>{$dataCon['district_name']}</option>
                             </select>
                         </div>*}

                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label"></label>
                    <div class="col-sm-9">
                        <div class="col-xs-12">
                            <input type="checkbox" id="cb_gender" name="cb_gender" value="1" {if $data['gender'] != 0}checked{/if}>{__("Gender")}
                        </div>
                        <div id="div_gender" class="col-xs-4 {if $data['gender'] == 0}x-hidden{/if}">
                            <select class="form-control col-sm-3" name="gender">
                                <option value="1" {if $data['gender'] == 1}selected{/if}>{__("Male")}</option>
                                <option value="2" {if $data['gender'] == 2}selected{/if}>{__("Female")}</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label"></label>
                    <div class="col-sm-9">
                        <div class="col-xs-12">
                            <input type="checkbox" id="cb_age" name="cb_age" value="3" {if $data['to_age'] != 0}checked{/if}>{__("Age")}
                        </div>
                        <div id="div_from_age" class="col-xs-2 {if $data['from_age'] == 0}x-hidden{/if}">
                            <input class="form-control" name="from_age" type="number" min="0" step="1" value="{$data['from_age']}">
                        </div>
                        <div id="div_to_age" class="col-xs-2 {if $data['to_age'] == 0}x-hidden{/if}">
                            <input class="form-control" name="to_age" type="number" min="0" step="1" value="{$data['to_age']}">
                        </div>

                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Lặp lại")}?</label>
                    <div class="col-sm-9">
                        <div class="onoffswitch">
                            <input type="checkbox" name="is_repeat" class="onoffswitch-checkbox" id="is_repeat" {if $data['is_repeat'] == 1}checked{/if}>
                            <label class="onoffswitch-label" for="is_repeat"></label>
                        </div>
                    </div>
                </div>

                <div class="form-group {if $data['is_repeat'] == 0}}x-hidden{/if}" id="cyclic">
                    <label class="col-sm-3 control-label text-left">{__("Chu kỳ")}</label>
                    <div class="col-sm-3">
                        <select name="repeat_time" id="repeat_time" class="form-control">
                            <option value="1" {if $data['repeat_time'] == 1}selected{/if}>{__("Năm")}</option>
                            <option value="2" {if $data['repeat_time'] == 2}selected{/if}>{__("Tháng")}</option>
                            <option value="3" {if $data['repeat_time'] == 3}selected{/if}>{__("Ngày")}</option>
                            <option value="4" {if $data['repeat_time'] == 4}selected{/if}>{__("Tuần")}</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Title")} (*)</label>
                    <div class="col-sm-9">
                        <input class="form-control" name="title" value="{$data['title']}" required maxlength="250">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("URL")}</label>
                    <div class="col-sm-9">
                        <input class="form-control" name="link" value="{$data['link']}" maxlength="250">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Content")} (*)</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="content" rows="7" required>{$data['content']}</textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Description")}</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="description" rows="3" required>{$data['description']}</textarea>
                    </div>
                </div>


                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>

    {/if}
</div>