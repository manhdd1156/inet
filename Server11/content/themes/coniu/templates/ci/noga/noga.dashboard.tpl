<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <i class="fas fa-tachometer-alt fa-fw fa-lg pr10"></i>
        {__("Dashboard")}
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-6">
                <div class="box-primary">
                    <div class="box-header">
                        <strong>{__("School list")}</strong>
                    </div>
                    <div class="list-group">
                        {foreach $insights['schools'] as $school}
                            <div class="list-group-item">
                                {$school['page_title']}
                            </div>
                        {/foreach}
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>