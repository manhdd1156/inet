<div class="panel panel-default">
    <div class="panel-heading with-icon">
        {if $sub_view == ""}
            <div class="pull-right flip">
                <a href="{$system['system_url']}/noga/foetusknowledges/add" class="btn btn-default">
                    <i class="fa fa-plus"></i> {__("Add New")}
                </a>
            </div>
        {elseif $sub_view == "add"}
            <div class="pull-right flip">
                <a href="{$system['system_url']}/noga/foetusknowledges" class="btn btn-default">
                    <i class="fa fa-list"></i> {__("Lists")}
                </a>
            </div>
        {/if}
        <i class="fas fa-calendar-alt fa-fw fa-lg pr10"></i>
        {__("Foetus knowledge")}
        {if $sub_view == ""}
        {elseif $sub_view == "add"}
            &rsaquo; {__("Add information about your foetus's knowledge")}
        {elseif $sub_view == "edit"}
            &rsaquo; {__("Update information about your foetus's knowledge")}
        {elseif $sub_view == "detail"}
            &rsaquo; {__('Detail')}
        {/if}
    </div>
    {if $sub_view == ""}
        <div class="panel-body with-table form-horizontal">
            <div class="table-responsive" name = "schedule_all">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr><th colspan="4">{__("Foetus knowledge")}</th></tr>
                    <tr>
                        <th>
                            {__("No.")}
                        </th>
                        <th>
                            {__("Title")}
                        </th>
                        <th>
                            {__("Link")}
                        </th>
                        <th>
                            {__("Actions")}
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    {foreach $foetus_knowledges as $k => $row}
                        <tr>
                            <td align="center">{$k + 1}</td>
                            <td align="center"><a href = "{$system['system_url']}/noga/foetusknowledges/edit/{$row['foetus_knowledge_id']}">{$row['title']}</a></td>
                            <td>
                                {$row['link']}
                            </td>
                            <td>
                                <a class="btn btn-xs btn-default" href = "{$system['system_url']}/noga/foetusknowledges/edit/{$row['foetus_knowledge_id']}">{__("Edit")}</a>
                                <a href="#" class = "btn-xs btn btn-danger js_noga-foetus-know-delete" data-id="{$row['foetus_knowledge_id']}">{__("Delete")}</a>
                            </td>
                        </tr>
                    {/foreach}
                    </tbody>
                </table>
            </div>
        </div>
    {elseif $sub_view == "detail"}
        <div class="panel-body with-table form-horizontal">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <tbody>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Title")}</strong></td>
                            <td>{$data['title']}</td>
                        </tr>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Link")}</strong></td>
                            <td>{$data['link']}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    {elseif $sub_view == "add"}
        <div class="panel-body">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/noga/bo_noga_foetusknowledge.php">
                <input type="hidden" name="do" value="add"/>
                <div class="form-group">
                    <label class="col-sm-4 control-label text-left" style="float: left">{__("Enter foetus knowledge")}</label>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id = "myTableFoetusKnow">
                        <thead>
                        <tr>
                            <th>
                                {__('No.')}
                            </th>
                            <th>
                                {__('Option')}
                            </th>
                            <th>
                                {__('Title')}
                            </th>
                            <th>
                                {__('Link')}
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td align="center" class = "col_no">1</td>
                            <td align="center">
                                <a class = "btn btn-default js_cate-delete_null"> {__("Delete")} </a>
                            </td>
                            <td>
                                <input type = "text" name = "title[]" placeholder="" style = "padding-left: 10px; height: 35px; width: 100%; ">
                            </td>
                            <td>
                                <textarea class = "note" type="text" name = "links[]" style="width:100%"></textarea>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <div class = "form-group mt20">
                        <div class = "col-sm-12">
                            <a class="btn btn-default js_add-foetus-know">{__("Add new row")}</a>
                            <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                        </div>
                    </div>
                </div>
                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->
                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->

            </form>
        </div>
    {elseif $sub_view == "edit"}
        <div class="panel-body">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/noga/bo_noga_foetusknowledge.php">
                <input type="hidden" name="do" value="edit"/>
                <input type="hidden" name="foetus_knowledge_id" value="{$data['foetus_knowledge_id']}"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Title")} (*)</label>
                    <div class="col-sm-9">
                        <input type = "text" class="form-control" name="title" required value = "{$data['title']}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Link")}</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="link" rows="6">{$data['link']}</textarea>
                    </div>
                </div>
                <div class = "form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                    </div>
                </div>
                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->
                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    {/if}
</div>