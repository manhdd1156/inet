<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover js_dataTable">
        <thead>
        <tr><th>{__("Subject list")}&nbsp;({$rows|count})</th></tr>
        </thead>
        <tbody>
        {$idx = 1}
        {foreach $rows as $row}
            <tr>
                <td>
                    <div>
                        {$idx} - {$row['subject_name']}
                         | {if $row['re_exam'] == 1}{__("re-exam")}{else}{__("not re-exam")}{/if}
                    </div>
                    <div class="pull-right flip">
                        <form class="js_ajax-forms form-horizontal" action="#" enctype="multipart/form-data" id="subject_list_form" method="post">
{*                            class="form-horizontal" action="#"  data-url="ci/bo/noga/bo_noga_subject.php" enctype="multipart/form-data" method="post" id="point_search_form"*}
                            <input type="hidden" name="do" value="delete"/>
                            <input type="hidden" name="subject_id" value="{$row['subject_id']}"/>

                            <a href="{$system['system_url']}/noga/subjects/edit/{$row['subject_id']}" class="btn btn-xs btn-default">{__("Edit")}</a>
                            <a href="#"  class="delete_subject btn btn-xs btn-default">{__("Delete")}</a>
{*                            <input type="submit" name="delete" class="btn btn-xs btn-default" value="delete"  />*}
                        </form>
                    </div>
                </td>
            </tr>
            {$idx = $idx + 1}
        {/foreach}
        </tbody>
    </table>
    <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
    <div class="alert alert-warning mb0 mt10 x-hidden" role="alert"></div>
</div>

<script type="text/javascript">
    $(function() {
        // run DataTable
        $('.js_dataTable').DataTable({
            "aoColumnDefs": [ { 'aDataSort': false, 'aTargets': [ -1 ] } ],
            "language": {
                "decimal":        "",
                "emptyTable":     __["No data available in table"],
                "info":           __["Showing _START_ to _END_ of _TOTAL_ results"],
                "infoEmpty":      __["Showing 0 to 0 of 0 results"],
                "infoFiltered":   "(filtered from _MAX_ total entries)",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     __["Show"] + " " + "_MENU_" + " " + __["results"],
                "loadingRecords": __["Loading..."],
                "processing":     __["Processing..."],
                "search":         __["Search"],
                "zeroRecords":    __["No matching records found"],
                "paginate": {
                    "first":      __["First"],
                    "last":       __["Last"],
                    "next":       __["Next"],
                    "previous":   __["Previous"]
                },
                "aria": {
                    "sortAscending":  ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                }
            }
        });
    });
</script>