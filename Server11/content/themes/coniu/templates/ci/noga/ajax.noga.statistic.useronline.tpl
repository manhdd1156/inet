{*<pre>*}
{*{print_r($statistic)}*}
{*</pre>*}
<table class="table table-striped table-bordered table-hover js_dataTable">
    <thead>
    <tr>
        <th>#</th>
        <th>{__("Date")}</th>
        <th>{__("User fullname")}</th>
        <th>{__("Username")}</th>
        <th>{__("User email")}</th>
        {*<th>{__("Manage")}</th>*}
    </tr>
    </thead>
    <tbody>
    {$idx = 1}
    {foreach $useronline as $date => $users}
        {foreach $users as $user}
            <tr>
                <td align="center">{$idx}</td>
                <td align="center">{$date}</td>
                <td><a href="{$system['system_url']}/{$user['user_name']}" target="_blank">{$user['user_fullname']}</a>{if $user['of_school']} (uos){else} (nos){/if}</td>
                <td>{$user['user_name']}</td>
                <td>{$user['user_email']}</td>
                {*<td>*}
                    {*{if count($user['schools']) > 0}*}
                        {*<strong>{__("School")}:</strong>*}
                        {*{foreach $user['schools'] as $school}*}
                            {*{$school['page_title']},&nbsp;*}
                        {*{/foreach}*}
                    {*{/if}*}
                    {*{if count($user['classes']) > 0}*}
                        {*<br/>*}
                        {*<strong>{__("Class")}:</strong>*}
                        {*{foreach $user['classes'] as $class}*}
                            {*{$class['group_title']},&nbsp;*}
                        {*{/foreach}*}
                    {*{/if}*}
                    {*{if count($user['children']) > 0}*}
                        {*<br/>*}
                        {*<strong>{__("Child")}:</strong>*}
                        {*{foreach $user['children'] as $child}*}
                            {*{$child['child_name']}{if $child['school_id'] > 0}({$child['school_id']}){/if},&nbsp;*}
                        {*{/foreach}*}
                    {*{/if}*}
                {*</td>*}
            </tr>
            {$idx = $idx + 1}
        {/foreach}
    {/foreach}
    </tbody>
</table>

<script type="text/javascript">
    $(function() {
        // run DataTable
        $('.js_dataTable').DataTable({
            "aoColumnDefs": [ { 'aDataSort': false, 'aTargets': [ -1 ] } ],
            "language": {
                "decimal":        "",
                "emptyTable":     __["No data available in table"],
                "info":           __["Showing _START_ to _END_ of _TOTAL_ results"],
                "infoEmpty":      __["Showing 0 to 0 of 0 results"],
                "infoFiltered":   "(filtered from _MAX_ total entries)",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     __["Show"] + " " + "_MENU_" + " " + __["results"],
                "loadingRecords": __["Loading..."],
                "processing":     __["Processing..."],
                "search":         __["Search"],
                "zeroRecords":    __["No matching records found"],
                "paginate": {
                    "first":      __["First"],
                    "last":       __["Last"],
                    "next":       __["Next"],
                    "previous":   __["Previous"]
                },
                "aria": {
                    "sortAscending":  ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                }
            }
        });
    });
</script>