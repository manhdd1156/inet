<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover js_dataTable">
        <thead>
        <tr><th>{__("School list")}&nbsp;({$rows|count})</th></tr>
        </thead>
        <tbody>
        {$idx = 1}
        {foreach $rows as $row}
            <tr>
                <td>
                    <div>
                        {$idx} - {$row['page_title']}
                        &nbsp;|&nbsp;<a href="{$system['system_url']}/pages/{$row['page_name']}">{__("Timeline")}</a>
                        &nbsp;|&nbsp;{$row['page_name']}
                        {if ($row['telephone'] != '')} &nbsp;|&nbsp;{$row['telephone']} {/if}
                        {if ($row['website'] != '')} &nbsp;|&nbsp;<a href="{$row['website']}">{$row['website']}</a>{/if}
                        {if ($row['email'] != '')} &nbsp;|&nbsp;<a href="mailto:{$row['email']}">{$row['email']}</a> {/if}
                    </div>
                    <div class="pull-right flip">
                        <a href="{$system['system_url']}/noga/schools/module/{$row['page_id']}" class="btn btn-xs btn-primary">{__("Module")}</a>
                        <a href="{$system['system_url']}/noga/schools/edit/{$row['page_id']}/basic" class="btn btn-xs btn-default">{__("Edit")}</a>
                        <a href="{$system['system_url']}/noga/schools/viewuser/{$row['page_id']}" class="btn btn-xs btn-default">{__("View parent")}</a>
                        <select name="school_status_{$row['page_id']}" class="school_status_change" style="color: #3f729b" data-id="{$row['page_id']}">
                            <option value="1" {if $row['school_status'] == SCHOOL_USING_CONIU}selected{/if}>{__("Using Mascom - Edu")}</option>
                            <option value="2" {if $row['school_status'] == SCHOOL_HAVE_PAGE_CONIU}selected{/if}>{__("Having Mascom - Edu's page")}</option>
                            <option value="3" {if $row['school_status'] == SCHOOL_WAITING_CONFIRM}selected{/if}>{__("Waiting confirmation")}</option>
                        </select>
                    </div>
                </td>
            </tr>
            {$idx = $idx + 1}
        {/foreach}
        </tbody>
    </table>
</div>

<script type="text/javascript">
    $(function() {
        // run DataTable
        $('.js_dataTable').DataTable({
            "aoColumnDefs": [ { 'aDataSort': false, 'aTargets': [ -1 ] } ],
            "language": {
                "decimal":        "",
                "emptyTable":     __["No data available in table"],
                "info":           __["Showing _START_ to _END_ of _TOTAL_ results"],
                "infoEmpty":      __["Showing 0 to 0 of 0 results"],
                "infoFiltered":   "(filtered from _MAX_ total entries)",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     __["Show"] + " " + "_MENU_" + " " + __["results"],
                "loadingRecords": __["Loading..."],
                "processing":     __["Processing..."],
                "search":         __["Search"],
                "zeroRecords":    __["No matching records found"],
                "paginate": {
                    "first":      __["First"],
                    "last":       __["Last"],
                    "next":       __["Next"],
                    "previous":   __["Previous"]
                },
                "aria": {
                    "sortAscending":  ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                }
            }
        });
    });
</script>