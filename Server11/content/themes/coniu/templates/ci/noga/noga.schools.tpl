<div class="panel panel-default">
    <div class="panel-heading with-icon">
        {if $sub_view == ""}
            <div class="pull-right flip">
                <a href="{$system['system_url']}/noga/schools/add" class="btn btn-default">
                    <i class="fa fa-plus"></i> {__("Add New")}
                </a>
            </div>
        {/if}
        <i class="fa fa-university fa-fw fa-lg pr10"></i>
        {__("School")}
        {if $sub_view == "edit"}
            &rsaquo; {$data['page_title']}
            {if $configure == 'basic'} &rsaquo; {__("Basic")}
                {elseif $configure == 'services'} &rsaquo; {__("Service")}
                {elseif $configure == 'addmission'} &rsaquo; {__("Admission")}
                {elseif $configure == 'info'} &rsaquo; {__("More information")}
            {/if}
        {elseif $sub_view == "addsystemschool"}
            &rsaquo; {__('Add new system school')}
        {elseif $sub_view == "listsystemschool"}
            &rsaquo; {__('List system school')}
        {elseif $sub_view == "editsystemschool"}
            &rsaquo; {__('Edit system school')} &rsaquo; {$data['school_group_title']}
        {elseif $sub_view == "add"}
            &rsaquo; {__('Create school')}
        {elseif $sub_view == "viewuser"}
            &rsaquo; {__('View parent list of school')}
        {elseif $sub_view == "module"}
            &rsaquo; {$schoolInfo['page_title']} &rsaquo; {__('Module')}
        {/if}
    </div>
    {if $sub_view == ""}
        <div class="panel-body with-table">
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-1 control-label text-left">{__("City")}</label>
                            <div class="col-sm-3">
                                <select class="form-control" name="city_id" id = "city_id">
                                    <option value="0">{__('Select city')}</option>
                                    {foreach $cities as $city}
                                        <option value="{$city['city_id']}">{$city['city_name']}</option>
                                    {/foreach}
                                </select>
                            </div>
                            <label class="col-sm-2 control-label text-left col-sm-offset-2">{__("District")}</label>
                            <div class="col-sm-4">
                                <select class="form-control" name="district_slug" id = "district_slug">
                                    <option value="">{__('Select district')}</option>
{*                                    {foreach $cities as $city}*}
{*                                        {if $city['city_id'] == 1}*}
{*                                            {foreach $city['district'] as $district}*}
{*                                                <option value="{$district['district_slug']}"> {$district['district_name']}*}
{*                                                </option>*}
{*                                            {/foreach}*}
{*                                        {/if}*}
{*                                    {/foreach}*}
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-6">
                                <input type="hidden" id="nogaRole" value="{$nogaRole}" name="nogaRole">
                                <select name="school_status" id="school_status" class="form-control">
                                    <option value="0">{__("Select satuts")}...</option>
                                    <option value="1">{__("Using Mascom - Edu")}</option>
                                    <option value="2">{__("Having Mascom - Edu's page")}</option>
                                    <option value="3">{__("Waiting confirmation")}</option>
                                </select>
                            </div>
                            <div class="col-sm-6">
                                <button class="btn btn-default" id="noga_search_school">{__('Search')}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="school_list">
                {include file="ci/noga/ajax.noga.schoollist.tpl"}
            </div>
        </div>
    {elseif $sub_view == "addsystemschool"}
        <div class = "panel-body with-table">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/noga/bo_noga_school.php">
                <input type="hidden" name="do" value="addsystemschool"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">
                        {__("System school name")} (*)
                    </label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="system_title" placeholder="{__("System school name")}" required autofocus maxlength="255">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">
                        {__("System school username")} (*)
                    </label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="system_username" placeholder="{__("Username, e.g. mamnonthienthan")}" required maxlength="50">
                    </div>
                </div>
                {*<div class="form-group">*}
                    {*<label class="col-sm-3 control-label text-left">*}
                        {*{__("System school cover image")} (*)*}
                    {*</label>*}
                    {*<div class="col-sm-9">*}
                        {*<input type="text" class="form-control" name="system_school_image" placeholder="{__("Username, e.g. mamnonthienthan")}" required maxlength="50">*}
                    {*</div>*}
                {*</div>*}
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Description")}</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="system_description" placeholder="{__("Write about your system school...")}" rows="4"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    {elseif $sub_view == "listsystemschool"}
        <div class="panel-body with-table">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover js_dataTable">
                    <thead>
                    <tr><th>{__("System school list")}&nbsp;({$results|count})</th></tr>
                    </thead>
                    <tbody>
                    {foreach $results as $k => $row}
                        <tr>
                            <td>
                                <div>
                                    {$k + 1} - <a href = "{$system['system_url']}/noga/schools/editsystemschool/{$row['school_group_id']}">{$row['school_group_title']} </a>
                                    &nbsp;|&nbsp;{$row['school_group_name']}
                                </div>
                                <div class="pull-right flip">
                                    <a href="{$system['system_url']}/noga/schools/editsystemschool/{$row['school_group_id']}" class="btn btn-xs btn-default">{__("Edit")}</a>
                                </div>
                            </td>
                        </tr>
                    {/foreach}
                    </tbody>
                </table>
            </div>
        </div>
    {elseif $sub_view == "editsystemschool"}
        <div class = "panel-body with-table">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/noga/bo_noga_school.php">
                <input type="hidden" name="do" value="editsystemschool"/>
                <input type="hidden" name = "system_school_id" value = "{$data['school_group_id']}"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">
                        {__("System school name")} (*)
                    </label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="system_title" placeholder="{__("System school name")}" value = "{$data['school_group_title']}" required autofocus maxlength="255">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">
                        {__("System username")} (*)
                    </label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="system_username" placeholder="{__("Username, e.g. mamnonthienthan")}" value = "{$data['school_group_name']}" required maxlength="50">
                    </div>
                </div>
                {*<div class="form-group">*}
                {*<label class="col-sm-3 control-label text-left">*}
                {*{__("System school cover image")} (*)*}
                {*</label>*}
                {*<div class="col-sm-9">*}
                {*<input type="text" class="form-control" name="system_school_image" placeholder="{__("Username, e.g. mamnonthienthan")}" required maxlength="50">*}
                {*</div>*}
                {*</div>*}
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Description")}</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="system_description" placeholder="{__("Write about your system school...")}" rows="4"> {$data['description']} </textarea>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    {elseif $sub_view == "edit"}
        {if $configure == 'basic'}
            <div class="panel-body">
                <form class="js_ajax-forms form-horizontal" data-url="ci/bo/noga/bo_noga_school.php">
                    <input type="hidden" name="do" value="basic"/>
                    <input type="hidden" name="page_id" value="{$data['page_id']}"/>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">
                            {__("School name")} (*)
                        </label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" name="title" value="{$data['page_title']}" required autofocus maxlength="255">
                        </div>
                        <label class="col-sm-1 control-label text-left">
                            {__("Grade")}
                        </label>
                        <div class="col-sm-4">
                            <select name="grade" id="grade" class="form-control">
                                {foreach $grades as $key => $grade}
                                    <option value="{$key}" {if $key == $dataCon['grade']} selected {/if}>{$grade}</option>
                                {/foreach}
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Username")}</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="username" value="{$data['page_name']}" maxlength="50">
                        </div>
                        <div class="col-sm-6">
                            <select name="inside_system_school" id="inside_system_school" class="form-control">
                                <option value="0">{__("Select inside school")}</option>
                                {foreach $systemSchools as $rows}
                                    <option value="{$rows['school_group_id']}" {if $rows['school_group_id'] == $dataCon['school_group_id']} selected {/if}>{$rows['school_group_title']} - {$rows['school_group_name']}</option>
                                {/foreach}
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("School manager")}</label>
                        <div class="col-sm-9">
                            <input name="search-manager" id="search-manager" type="text" class="form-control" placeholder="{__("Enter username, email or fullname to search")}" autocomplete="off">
                            <div id="search-manager-results" class="dropdown-menu dropdown-widget dropdown-search">
                                <div class="dropdown-widget-header">
                                    {__("Search Results")}
                                </div>
                                <div class="dropdown-widget-body">
                                    <div class="loader loader_small ptb10"></div>
                                </div>
                            </div>
                            <br/>
                            <div class="col-sm-9" id="manager_list" name="manager_list">
                                {if count($managers) > 0}
                                    {include file='ci/noga/ajax.noga.managerlist.tpl' results=$managers}
                                {else}
                                    {__("No manager")}
                                {/if}
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("School type")} (*)</label>
                        <div class="col-sm-3">
                            <select name="school_type" id="school_type" class="form-control">
                                <option value="0">{__("Select school type")}</option>
                                {foreach $schoolTypes as $type}
                                    <option value="{$type['type_value']}" {if $type['type_value'] == $dataCon['type']} selected {/if}>{$type['type_name']}</option>
                                {/foreach}
                            </select>
                        </div>
                        <label class="col-sm-3 control-label text-left">{__("Email")}</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="email" name="email" value="{$data['email']}" maxlength="50">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Telephone")}</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="telephone" name="telephone" value="{$data['telephone']}" maxlength="50">
                        </div>
                        <label class="col-sm-3 control-label text-left">{__("Website")}</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="website" value="{$data['website']}" maxlength="50">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Address")}</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="address" value="{$data['address']}" maxlength="400">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("City")}</label>
                        <div class="col-sm-3">
                            <select class="form-control" name="city_id" id = "city_id">
                                <option value="0">{__("Select city")}</option>
                                {foreach $cities as $city}
                                    <option value="{$city['city_id']}" {if $data['city_id']==$city['city_id']}selected{/if}>{$city['city_name']}</option>
                                {/foreach}
                            </select>
                        </div>
                        <label class="col-sm-3 control-label text-left">{__("District")}</label>
                        <div class="col-sm-3">
                            <select class="form-control" name="district_slug" id = "district_slug">
                                <option value="{$dataCon['district_slug']}" selected>{$dataCon['district_name']}</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left"> {__("Location")}: Long (Kinh độ)</label>
                        <div class="col-sm-3">
                            <input class="form-control" name="longtitude" value = {$dataCon['lng']}>
                        </div>
                        <label class="col-sm-3 control-label text-left"> Lat (Vĩ độ) </label>
                        <div class="col-sm-3">
                            <input class="form-control" name="latitude" value = "{$dataCon['lat']}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Directions")}</label>
                        <div class="col-sm-9">
                            <textarea class="form-control" name="direct" placeholder="{__("Write about direct go to your school...")}" rows="1"> {$dataCon['directions']} </textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Short description")}</label>
                        <div class="col-sm-9">
                            <textarea class="form-control" name="short_description" placeholder="{__("Write about your school...")}" rows="2">{$dataCon['short_overview']}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Description")}</label>
                        <div class="col-sm-9">
                            <textarea class="form-control" name="description" placeholder="{__("Write about the school...")}" rows="4">{$data['page_description']}</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-3">
                            <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                        </div>
                    </div>

                    <!-- success -->
                    <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                    <!-- success -->

                    <!-- error -->
                    <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                    <!-- error -->
                </form>
            </div>
        {elseif $configure == 'services'}
            <div class="panel-body">
                <form class="js_ajax-forms form-horizontal" data-url="ci/bo/noga/bo_noga_school.php">
                    <input type="hidden" name="do" value="services"/>
                    <input type="hidden" name="page_id" value="{$data['page_id']}"/>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left"> {__("Facilities")}</label>
                        <div class="col-sm-4">
                            <input type="checkbox" name = "facilities[0]" value="1" {if ($dataCon['facility_pool'] == 1)}checked{/if}><label class = "control-label"> {__('Pool')}</label><br/>
                            <input type="checkbox" name = "facilities[1]" value="1" {if ($dataCon['facility_playground_out'] == 1)}checked{/if}><label class = "control-label"> {__('Outdoor playground')}</label><br/>
                            <input type="checkbox" name = "facilities[2]" value="1" {if ($dataCon['facility_playground_in'] == 1)}checked{/if}><label class = "control-label"> {__('Indoor playground')}</label><br/>
                        </div>
                        <div class="col-sm-4">
                            <input type="checkbox" name = "facilities[3]" value="1" {if ($dataCon['facility_library'] == 1)}checked{/if}><label class = "control-label"> {__("Library")}</label><br/>
                            <input type="checkbox" name = "facilities[4]" value="1" {if ($dataCon['facility_camera'] == 1)}checked{/if}><label class = "control-label"> {__("View camera online")}</label><br/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Facility note")}</label>
                        <div class="col-sm-9">
                            <textarea class="form-control" name="facility_note" rows="4"> {$dataCon['note_for_facility']}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left"> {__("Service")}</label>
                        <div class="col-sm-4">
                            <input type="checkbox" name = "services[0]" value="1" {if ($dataCon['service_bus'] == 1)}checked{/if}><label class = "control-label">{__('Transportation')}</label> <br/>
                            <input type="checkbox" name = "services[1]" value="1" {if ($dataCon['service_breakfast'] == 1)}checked{/if}><label class = "control-label">{__('Breakfast')}</label> <br/>
                        </div>
                        <div class="col-sm-4">
                            <input type="checkbox" name = "services[2]" value="1" {if ($dataCon['service_belated'] == 1)}checked{/if}><label class = "control-label">{__('Late PickUp')}</label> <br/>
                            <input type="checkbox" name = "services[3]" value="1" {if ($dataCon['service_saturday'] == 1)}checked{/if}><label class = "control-label">{__("Looks saturday")}</label> <br/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Service note")}</label>
                        <div class="col-sm-9 control-label">
                            <textarea class="form-control" name="service_note" rows="4"> {$dataCon['note_for_service']}</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-3">
                            <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                        </div>
                    </div>

                    <!-- success -->
                    <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                    <!-- success -->

                    <!-- error -->
                    <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                    <!-- error -->
                </form>
            </div>
            {elseif $configure == 'addmission'}
            <div class = "panel-body">
                <form class="js_ajax-forms form-horizontal" data-url="ci/bo/noga/bo_noga_school.php">
                    <input type="hidden" name="do" value="addmission"/>
                    <input type="hidden" name="page_id" value="{$data['page_id']}"/>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Age")}</label>
                        <label class="col-sm-1 control-label text-left">{__("From")}</label>
                        <div class="col-sm-3 text-left">
                            <input type="text" name = "age_begin" id ="age_begin" value="{$dataCon['start_age']}" class = "form-control"/>
                        </div>
                        <label class="col-sm-1 control-label text-left">{__("To")}</label>
                        <div class="col-sm-3 text-left">
                            <input type="text" name = "age_end" id ="age_end" value="{$dataCon['end_age']}" class = "form-control"/>
                        </div>
                        <label class="col-sm-1 control-label text-left">{__("Month")}</label>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Tuition fee")}</label>
                        <label class="col-sm-1 control-label text-left">{__("From")}</label>
                        <div class="col-sm-3 text-left">
                            <input type="text" name = "tuition_begin" id ="tuition_begin" value="{$dataCon['start_tuition_fee']}" class = "form-control"/>
                        </div>
                        <label class="col-sm-1 control-label text-left">{__("To")}</label>
                        <div class="col-sm-3 text-left">
                            <input type="text" name = "tuition_end" id ="tuition_end" value="{$dataCon['end_tuition_fee']}" class = "form-control"/>
                        </div>
                        <label class="col-sm-1 control-label text-left">{$smarty.const.MONEY_UNIT}</label>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Tuition note")}</label>
                        <div class="col-sm-9">
                            <textarea class="form-control" name="tuition_note" rows="2"> {$dataCon['note_for_tuition']}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Number of student addmission")}</label>
                        <div class="col-sm-9">
                            <input type="text" name = "addmission" id ="addmission" value="{$dataCon['admission']}" class = "form-control"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Addmission note")}</label>
                        <div class="col-sm-9">
                            <textarea class="form-control" name="addmission_note" rows="5"> {$dataCon['note_for_admission']}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-3">
                            <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                        </div>
                    </div>
                    <!-- success -->
                    <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                    <!-- success -->

                    <!-- error -->
                    <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                    <!-- error -->
                </form>
            </div>
            {elseif $configure == 'info'}
                <div class = "panel-body">
                    <form class="js_ajax-forms form-horizontal" data-url="ci/bo/noga/bo_noga_school.php">
                        <input type="hidden" name="do" value="info"/>
                        <input type="hidden" name="page_id" value="{$data['page_id']}"/>
                        <div class="form-group">
                            <label class="col-sm-3 control-label text-left">{__("Leadership")}</label>
                            <div class="col-sm-9">
                                <textarea class="form-control" name="leader" rows="6"> {$dataCon['info_leader']}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label text-left">{__("Methods of education")}</label>
                            <div class="col-sm-9">
                                <textarea class="form-control" name="method" rows="6"> {$dataCon['info_method']}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label text-left">{__("Teachers")}</label>
                            <div class="col-sm-9">
                                <textarea class="form-control" name="teacher" rows="6"> {$dataCon['info_teacher']}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label text-left">{__("Nutrition")}</label>
                            <div class="col-sm-9">
                                <textarea class="form-control" name="nutrition" rows="6"> {$dataCon['info_nutrition']}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-9 col-sm-offset-3">
                                <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                            </div>
                        </div>
                        <!-- success -->
                        <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                        <!-- success -->

                        <!-- error -->
                        <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                        <!-- error -->
                    </form>
                </div>
        {/if}
    {elseif $sub_view == "add"}
        <div class="panel-body">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/noga/bo_noga_school.php">
                <input type="hidden" name="do" value="add"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">
                        {__("School name")} (*)
                    </label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="title" placeholder="{__("School name")}" required autofocus maxlength="255">
                    </div>
                    <label class="col-sm-1 control-label text-left">
                        {__("Grade")}
                    </label>
                    <div class="col-sm-4">
                        <select name="grade" id="grade" class="form-control">
                            {foreach $grades as $key => $grade}
                                <option value="{$key}"> {__($grade)} </option>
                            {/foreach}
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Username")}</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="username" placeholder="{__("Username, e.g. mamnonthienthan")}" maxlength="50">
                    </div>
                    <div class="col-sm-6">
                        <select name="inside_system_school" id="inside_system_school" class="form-control">
                            <option value="0"> {__("Select system school")} </option>
                            {foreach $systemSchools as $rows}
                                <option value="{$rows['school_group_id']}">{$rows['school_group_title']} - {$rows['school_group_name']}</option>
                            {/foreach}
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("School manager")}</label>
                    <div class="col-sm-9">
                        <input name="search-manager" id="search-manager" type="text" class="form-control" placeholder="{__("Enter username, email or fullname to search")}" autocomplete="off">
                        <div id="search-manager-results" class="dropdown-menu dropdown-widget dropdown-search">
                            <div class="dropdown-widget-header">
                                {__("Search Results")}
                            </div>
                            <div class="dropdown-widget-body">
                                <div class="loader loader_small ptb10"></div>
                            </div>
                        </div>
                        <br/>
                        <div class="col-sm-9" id="manager_list" name="manager_list"></div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("School type")} (*)</label>
                    <div class="col-sm-3">
                        <select name="school_type" id="school_type" class="form-control">
                            {foreach $schoolTypes as $type}
                                <option value = "{$type['type_value']}"> {$type['type_name']} </option>
                            {/foreach}
                        </select>
                    </div>
                    <label class="col-sm-3 control-label text-left">{__("Email")}</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="email" name="email" placeholder="{__("Email")}" maxlength="50">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Telephone")}</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="telephone" name="telephone" placeholder="{__("Telephone")}" maxlength="50">
                    </div>
                    <label class="col-sm-3 control-label text-left">{__("Website")}</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="website" placeholder="{__("Website")}" maxlength="50">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Address")}</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="address" placeholder="{__("Address")}" maxlength="400">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("City")}</label>
                    <div class="col-sm-3">
                        <select class="form-control" name="city_id" id = "city_id">
                            {foreach $cities as $city}
                                <option value="{$city['city_id']}">{$city['city_name']}</option>
                            {/foreach}
                        </select>
                    </div>
                    <label class="col-sm-3 control-label text-left">{__("District")}</label>
                    <div class="col-sm-3">
                        <select class="form-control" name="district_slug" id = "district_slug">
                            {foreach $cities as $city}
                                {if $city['city_id'] == 1}
                                    {foreach $city['district'] as $district}
                                        <option value="{$district['district_slug']}"> {$district['district_name']}
                                        </option>
                                    {/foreach}
                                {/if}
                            {/foreach}
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left"> {__("Location")}: Long (Kinh độ)</label>
                    <div class="col-sm-3"><input class="form-control" name="longtitude"></div>
                    <label class="col-sm-3 control-label text-left"> Lat (Vĩ độ)</label>
                    <div class="col-sm-3"><input class="form-control" name="latitude"></div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Directions")}</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="direct" placeholder="{__("Write about direct go to your school...")}" rows="1"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Short description")}</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="short_description" placeholder="{__("Write about your school...")}" rows="2"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Description")}</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="description" placeholder="{__("Write about your school...")}" rows="4"></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    {elseif $sub_view == "viewuser"}
        <div class="panel-body with-table">
            <div class="js_ajax-forms form-horizontal">
                <input type="hidden" name="do" value="view_user"/>
                <input type="hidden" name="school_id" value="{$data['page_id']}"/>
                <input type="hidden" name="page_id" value="{$data['page_id']}" id = "page_id"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Scope")}</label>
                    <div class="col-sm-3">
                        <select name="class_id" id="class_id" class="form-control">
                            <option value="">{__("School")}</option>
                            {$class_level = -1}
                            {foreach $classes as $class}
                                {if ($class_level != $class['class_level_id'])}
                                    <option value="" disabled style="color: blue">-----{$class['class_level_name']}-----</option>
                                {/if}
                                <option value="{$class['group_id']}">{$class['group_title']}</option>
                                {$class_level = $class['class_level_id']}
                            {/foreach}

                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Last active")}</label>
                    <div class='col-sm-3'>
                        <div class='input-group date' id='fromDate'>
                            <input type='text' name="begin" class="form-control" id="begin" value="{$begin}"/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                    {*<div class='col-sm-1' align="center"><i class="fas fa-long-arrow-alt-right fa-lg fa-fw pr10 pt10"></i></div>*}
                    <div class='col-md-3'>
                        <div class='input-group date' id='toDate'>
                            <input type='text' name="end" class="form-control" id="end"/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <button type="submit" class="btn btn-default js_child-search" data-username="{$data['page_name']}" data-id="{$data['page_id']}" data-isnew="1" style="margin-left: 5px;">{__("Search")}</button>
                    </div>
                </div>

                <div class="table-responsive" id="child_list" name="child_list">
                    {include file="ci/noga/ajax.noga.children.list.tpl"}
                </div>
                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>

            </div>
        </div>

    {elseif $sub_view == "module"}
        <div class = "panel-body with-table">
            <h4>{__("Select the school module allowed to use")}</h4>
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/noga/bo_noga_school.php">
                <input type="hidden" name="do" value="edit_module"/>
                <input type="hidden" name="school_id" value="{$school['school_id']}"/>
                <input type="hidden" name="grade" value="{$school['grade']}"/>
                {foreach $defaultModule[$school['grade']] as $module => $value}
                    <input type="checkbox" value="1" name="{$module}" {if $school[$module]}checked{/if}> {__($moduleName[$module])} <br/>
                {/foreach}

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    {/if}
</div>