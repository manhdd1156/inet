<div class="panel panel-default">
    <div class="panel-heading with-icon">
        {if $sub_view == "edit"}
            <div class="pull-right flip">
                <a target="_blank" href="{$system['system_url']}/pages/{$data['page_name']}" class="btn btn-info">
                    {__("Go to this page")}
                </a>
            </div>
        {/if}
        <i class="fa fa-flag pr5 panel-icon"></i>
        <strong>{__("Pages")}</strong>
        {if $sub_view == "edit"} &rsaquo; <strong>{$data['page_title']}</strong>{/if}
    </div>
    {if $sub_view != "edit"}
        <div class="panel-body with-table">
            <div class="row">
                <label class="col-sm-3 control-label text-left">{__("Post time")}</label>
                <div class='col-sm-3'>
                    <div class='input-group date' id='fromuserregpicker'>
                        <input type='text' name="fromDate" id="fromDate" class="form-control" placeholder="{__("From date")}"/>
                        <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                    </div>
                </div>
                <div class='col-sm-3'>
                    <div class='input-group date' id='touserregpicker'>
                        <input type='text' name="toDate" id="toDate" class="form-control" placeholder="{__("To date")}"/>
                        <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                    </div>
                </div>
                <div class='col-sm-2'>
                    <div class="form-group">
                        <a href="#" id="search" class="btn btn-default js_noga-page-search">{__("Search")}</a>
                        <label id="loading" class="btn btn-info x-hidden">{__("Loading")}...</label>
                    </div>
                </div>
            </div>
            <div id="page_list">
                {include file="ci/noga/ajax.noga.page.list.tpl"}
            </div>
        </div>

        <script type="text/javascript">
            $(function() {
                // run DataTable
                $('.js_dataTable').DataTable({
                    "aoColumnDefs": [ { 'aDataSort': false, 'aTargets': [ -1 ] } ],
                    "language": {
                        "decimal":        "",
                        "emptyTable":     __["No data available in table"],
                        "info":           __["Showing _START_ to _END_ of _TOTAL_ results"],
                        "infoEmpty":      __["Showing 0 to 0 of 0 results"],
                        "infoFiltered":   "(filtered from _MAX_ total entries)",
                        "infoPostFix":    "",
                        "thousands":      ",",
                        "lengthMenu":     __["Show"] + " " + "_MENU_" + " " + __["results"],
                        "loadingRecords": __["Loading..."],
                        "processing":     __["Processing..."],
                        "search":         __["Search"],
                        "zeroRecords":    __["No matching records found"],
                        "paginate": {
                            "first":      __["First"],
                            "last":       __["Last"],
                            "next":       __["Next"],
                            "previous":   __["Previous"]
                        },
                        "aria": {
                            "sortAscending":  ": activate to sort column ascending",
                            "sortDescending": ": activate to sort column descending"
                        }
                    }
                });
            });
        </script>

    {elseif $sub_view == "edit"}
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-offset-3 col-xs-6 col-sm-offset-0 col-sm-2 mb10">
                    <img class="img-responsive img-thumbnail" src="{$data['page_picture']}">
                </div>
                <div class="col-xs-12 col-sm-10 mb10">
                    <ul class="list-group">
                        <li class="list-group-item">
                            <span class="badge">{$data['page_id']}</span>
                            {__("Page ID")}
                        </li>
                        <li class="list-group-item">
                            <span class="badge">{$data['page_likes']}</span>
                            {__("Likes")}
                        </li>
                    </ul>
                </div>
            </div>
            <!-- tabs nav -->
            <ul class="nav nav-tabs mb20">
                <li class="active">
                    <a href="#basic" data-toggle="tab">
                        <strong class="pr5">{__("Page Info")}</strong>
                    </a>
                </li>
            </ul>
            <!-- tabs nav -->

            <!-- tabs content -->
            <div class="tab-content">
                <!-- basic tab -->
                <div class="tab-pane active" id="basic">
                    <form class="js_ajax-forms form-horizontal" data-url="admin/page.php?id={$data['page_id']}">
                        <div class="form-group">
                            <label class="col-sm-3 control-label text-left">
                                {__("Verified Page")}
                            </label>
                            <div class="col-sm-9">
                                <div class="onoffswitch">
                                    <input type="checkbox" name="page_verified" class="onoffswitch-checkbox" id="page_verified" {if $data['page_verified']}checked{/if}>
                                    <label class="onoffswitch-label" for="page_verified"></label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label text-left">
                                {__("Category")}
                            </label>
                            <div class="col-sm-9">
                                <select class="form-control" name="page_category">
                                    {foreach $data['categories'] as $category}
                                        <option {if $data['page_category'] == $category['category_id']}selected{/if} value="{$category['category_id']}">{$category['category_name']}</option>
                                    {/foreach}
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label text-left">
                                {__("Title")}
                            </label>
                            <div class="col-sm-9">
                                <input class="form-control" name="page_title" value="{$data['page_title']}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label text-left">
                                {__("Username")}
                            </label>
                            <div class="col-sm-9">
                                <input class="form-control" name="page_name" value="{$data['page_name']}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label text-left">
                                {__("Username")}
                            </label>
                            <div class="col-sm-9">
                                <textarea class="form-control" name="page_description">{$data['page_description']}</textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-9 col-sm-offset-3">
                                <button type="submit" class="btn btn-primary">{__("Save Changes")}</button>
                            </div>
                        </div>

                        <!-- success -->
                        <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                        <!-- success -->

                        <!-- error -->
                        <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                        <!-- error -->
                    </form>
                </div>
            </div>
            <!-- tabs content -->
        </div>
    {/if}
</div>