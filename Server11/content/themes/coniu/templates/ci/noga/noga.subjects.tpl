<div class="panel panel-default">
    <div class="panel-heading with-icon">
        {if $sub_view == ""}
            <div class="pull-right flip">
                <a href="{$system['system_url']}/noga/subjects/add" class="btn btn-default">
                    <i class="fa fa-plus"></i> {__("Add New")}
                </a>
            </div>
        {/if}
        <i class="fa fa-university fa-fw fa-lg pr10"></i>
        {__("Subject")}
        {if $sub_view == "edit"}
            &rsaquo; {$data['subject_name']}
        {elseif $sub_view == "add"}
            &rsaquo; {__('Create subject')}
        {/if}
    </div>
    {if $sub_view == ""}
        <div class="panel-body with-table">
            <div id="subject_list">
                {include file="ci/noga/ajax.noga.subjectlist.tpl"}
            </div>
        </div>
    {elseif $sub_view == "edit"}
            <div class="panel-body">
                <form class="js_ajax-forms form-horizontal" data-url="ci/bo/noga/bo_noga_subject.php">
                    <input type="hidden" name="do" value="edit"/>
                    <input type="hidden" name="subject_id" value="{$data['subject_id']}"/>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">
                            {__("Subject name")} (*)
                        </label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" name="subject_name" value="{$data['subject_name']}" required autofocus maxlength="255">
                        </div>
                        <div class="col-sm-4">
                        <input type="checkbox" name="re_exam" value="{$data['re_exam']}" {if ($data['re_exam'] == 1)}checked{/if}>
                        <label for="re_exam"> {__("re_exam")}</label>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-3">
                            <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                        </div>
                    </div>

                    <!-- success -->
                    <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                    <!-- success -->

                    <!-- error -->
                    <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                    <!-- error -->
                </form>
            </div>
    {elseif $sub_view == "add"}
        <div class="panel-body">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/noga/bo_noga_subject.php">
                <input type="hidden" name="do" value="add"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">
                        {__("Subject name")} (*)
                    </label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="subject_name" placeholder="{__("Subject name")}" required autofocus maxlength="255">
                    </div>
                    <div class="col-sm-4">
                    <input type="checkbox"  name="re_exam" >
                    <label class = "control-label"> {__("re exam")}</label>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>

    {/if}
</div>