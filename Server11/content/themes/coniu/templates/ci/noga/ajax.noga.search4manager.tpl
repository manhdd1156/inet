<div class="js_scroller">
    <ul>
        {foreach $results as $_user}
            <li class="feeds-item" data-id="{$_user['user_name']}">
                <div class="data-container {if $_small}small{/if}">
                    <a href="{$system['system_url']}/{$_user['user_name']}">
                        <img class="data-avatar" src="{$_user['user_picture']}" alt="{$_user['user_fullname']}">
                    </a>
                    <div class="data-content">
                        <div class="pull-right flip">
                            <div class="btn btn-default js_manager-select" data-uid="{$_user['user_id']}">{__("Select")}</div>
                        </div>
                        <div>
                            <span class="name js_user-popover" data-uid="{$_user['user_id']}">
                                <a href="{$system['system_url']}/{$_user['user_name']}">{$_user['user_fullname']}</a>
                            </span>
                        </div>
                        {if $_connection != "me" && $_user['mutual_friends_count'] > 0}
                            <div>
                                <span class="text-underline" data-toggle="modal" data-url="users/mutual_friends.php?uid={$_user['user_id']}">{$_user['mutual_friends_count']} {__("mutual friends")}</span>
                            </div>
                        {/if}
                    </div>
                </div>
            </li>
        {/foreach}
    </ul>
</div>