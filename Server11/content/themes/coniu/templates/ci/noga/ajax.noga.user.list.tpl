<strong>{__("Children list")}&nbsp;({$result['total']} {__("Children")})</strong>
<div class="pull-right flip mb10">
    <label id="export_processing" class="btn btn-info x-hidden">{__("Loading")}...</label>
</div>
<table class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th>#</th>
        <th>{__("ID")}</th>
        <th>{__("Picture")}</th>
        <th>{__("Username")}</th>
        <th>{__("Name")}</th>
        <th>{__("IP")}</th>
        <th>{__("Joined")}</th>
        <th>{__("Activated")}</th>
        <th>{__("Actions")}</th>
    </tr>
    </thead>
    <tbody>
    {$idx = ($result['page'] - 1) * $smarty.const.PAGING_LIMIT + 1}
    {foreach $result['users'] as $row}
        <tr>
            <td class="align-middle" align="center">{$idx}</td>
            <td><a href="{$system['system_url']}/{$row['user_name']}" target="_blank">{$row['user_id']}</a></td>
            <td>
                <a target="_blank" class="x-image sm" href="{$system['system_url']}/{$row['user_name']}" style="background-image:url({$row['user_picture']});">
                </a>
            </td>
            <td>
                <a href="{$system['system_url']}/{$row['user_name']}" target="_blank">
                    {$row['user_name']}
                </a>
            </td>
            <td>
                <a href="{$system['system_url']}/{$row['user_name']}" target="_blank">
                    {$row['user_fullname']}
                </a>
            </td>
            <td>{$row['user_ip']}</td>
            <td>{$row['user_registered']|date_format:"%e %B %Y"}</td>
            <td>
                {if $row['user_activated']}
                    <span class="label label-success">{__("Yes")}</span>
                {else}
                    <span class="label label-danger">{__("No")}</span>
                {/if}
            </td>
            <td>
                <button class="btn btn-xs btn-danger js_admin-deleter" data-handle="user" data-id="{$row['user_id']}">
                    <i class="fas fa-trash"></i>
                </button>
                <a href="{$system['system_url']}/noga/users/edit/{$row['user_id']}" class="btn btn-xs btn-primary">
                    <i class="fa fa-pencil-alt"></i>
                </a>
            </td>
        </tr>
        {$idx = $idx + 1}
    {/foreach}

    {if $result['users']|count == 0}
        <tr class="odd">
            <td valign="top" align="center" colspan="9" class="dataTables_empty">
                {__("No data available in table")}
            </td>
        </tr>
    {/if}
    {if $result['page_count'] > 1}
        <tr>
            <td colspan="9">
                <div class="pull-right flip">
                    <ul class="pagination">
                        {for $idx = 1 to $result['page_count']}
                            {if $idx == $result['page']}
                                <li class="active"><a href="#">{$idx}</a></li>
                            {else}
                                <li>
                                    <a href="#" class="js_user-search" data-page="{$idx}">{$idx}</a>
                                </li>
                            {/if}
                        {/for}
                    </ul>
                </div>
            </td>
        </tr>
    {/if}
    </tbody>
</table>
