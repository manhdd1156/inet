{*<pre>*}
    {*{print_r($statistic)}*}
{*</pre>*}
<table class="table table-striped table-bordered table-hover" id="statistic_school_detail">
    <thead>
    <tr>
        <th>#</th>
        <th>{__("Page")}</th>
        <th>{__("Post")}</th>
        <th>{__("Dashboard")}</th>
        <th>{__("Attendance")}</th>
        <th>{__("Service")}</th>
        <th>{__("Late pickup")}</th>
        <th>{__("Tuition")}</th>
        <th>{__("Notification - Event")}</th>

        <th>{__("Schedule")}</th>
        <th>{__("Menu")}</th>
        <th>{__("Contact book")}</th>
        <th>{__("Medicines")}</th>
        <th>{__("Feedback")}</th>
        <th>{__("Diary")}</th>
        <th>{__("Health")}</th>
        <th>{__("Birthday")}</th>
        <th>{__("Picker")}</th>
    </tr>
    </thead>
    <tbody>

    <tr>
        <td align="center">{__("Interactive")}</td>
        {foreach $keys as $key}
            <td align="center">{$statistic[$key]['school_views']}</td>
        {/foreach}
        {*<td align="center">{$statistic['page']['school_view']}</td>*}
        {*<td align="center">{$statistic['post']['school_view']}</td>*}
        {*<td align="center">{$statistic['school_dashboard']['school_view']}</td>*}
        {*<td align="center">{$statistic['attendance']['school_view']}</td>*}
        {*<td align="center">{$statistic['service']['school_view']}</td>*}
        {*<td align="center">{$statistic['pickup']['school_view']}</td>*}
        {*<td align="center">{$statistic['tuition']['school_view']}</td>*}
        {*<td align="center">{$statistic['event']['school_view']}</td>*}
        {*<td align="center">{$statistic['schedule']['school_view']}</td>*}
        {*<td align="center">{$statistic['menu']['school_view']}</td>*}
        {*<td align="center">{$statistic['report']['school_view']}</td>*}
        {*<td align="center">{$statistic['medicine']['school_view']}</td>*}
        {*<td align="center">{$statistic['feedback']['school_view']}</td>*}
        {*<td align="center">{$statistic['diary']['school_view']}</td>*}
        {*<td align="center">{$statistic['health']['school_view']}</td>*}
        {*<td align="center">{$statistic['birthday']['school_view']}</td>*}
        {*<td align="center">{$statistic['picker']['school_view']}</td>*}
    </tr>
    <tr>
        <td align="center">{__("Add New")}</td>
        {foreach $keys as $key}
            <td align="center">{$statistic[$key]['school_createds']}</td>
        {/foreach}
    </tr>

    </tbody>
</table>