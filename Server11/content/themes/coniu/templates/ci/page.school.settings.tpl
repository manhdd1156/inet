<ul class="nav nav-tabs mb20">
    <li class="active">
        <a href="#basic" data-toggle="tab">
            <strong class="pr5">{__("Basic Info")}</strong>
        </a>
    </li>
    <li>
        <a href="#services" data-toggle="tab">
            <strong class="pr5">{__("Service")}</strong>
        </a>
    </li>
    <li>
        <a href="#addmission" data-toggle="tab">
            <strong class="pr5">{__("Admission")}</strong>
        </a>
    </li>
    <li>
        <a href="#info" data-toggle="tab">
            <strong class="pr5">{__("Information")}</strong>
        </a>
    </li>
</ul>
<div class="tab-content">
    <!-- Basic info -->
    <div class="tab-pane active" id="basic">
        <form class="js_ajax-forms" data-url="ci/bo/school/bo_school.php?do=edit&amp;id={$spage['page_id']}">
            <div class="form-group col-sm-6">
                <label for="title">{__("School name")} (*):</label>
                <input type="text" class="form-control" name="title" id="title" placeholder="{__("School name")}" value="{$spage['page_title']}">
            </div>
            <div class="form-group col-sm-6">
                <label for="username">{__("Username")} (*):</label>
                <input type="text" class="form-control" name="username" id="username" placeholder="{__("Username, e.g. mamnonthienthan")}" value="{$spage['page_name']}">
            </div>
            <div class="form-group col-sm-6">
                <label for="telephone">{__("Telephone")}:</label>
                <input type="text" class="form-control" name="telephone" id="telephone" placeholder="{__("Telephone")}" value="{$spage['telephone']}">
            </div>
            <div class="form-group col-sm-6">
                <label for="email">{__("Email")}:</label>
                <input type="text" class="form-control" name="email" id="email" placeholder="{__("Email")}" value="{$spage['email']}">
            </div>
            <div class="form-group col-sm-6">
                <label for="website">{__("Website")}:</label>
                <input type="text" class="form-control" name="website" id="website" placeholder="{__("Website")}" value="{$spage['website']}">
            </div>
            <div class="form-group col-sm-6">
                <label for="school_type">{__("School type")}:</label>
                <select name="school_type" id="school_type" class="form-control">
                    <option value="0">{__("Select school type")}</option>
                    {foreach $schoolTypes as $type}
                        <option value="{$type['type_value']}" {if $type['type_value'] == $spage['type']} selected {/if}>{$type['type_name']}</option>
                    {/foreach}
                </select>
            </div>
            <div class="form-group col-sm-12">
                <label for="address">{__("Address")}:</label>
                <input type="text" class="form-control" name="address" id="address" placeholder="{__("Address")}" value="{$spage['address']}">
            </div>
            <div class="form-group col-sm-6">
                <label for="city">{__("City")} (*):</label>
                <select class="form-control" name="city_id" id="city_id">
                    <option value="0">{__("Select city")}</option>
                    {foreach $cities as $city}
                        <option {if $spage['city_id'] == $city['city_id']}selected{/if} value="{$city['city_id']}">{$city['city_name']}</option>
                    {/foreach}
                </select>
            </div>
            <div class="form-group col-sm-6">
                <label for="district">{__("Select district")}</label>
                <select class="form-control" name="district_slug" id = "district_slug">
                    {foreach $cities as $city}
                        {if $spage['city_id'] == $city['city_id']}
                            <option value="">...</option>
                            {foreach $city['district'] as $district}
                                <option value="{$district['district_slug']}" {if $spage['district_slug'] == $district['district_slug']}selected{/if}>{$district['district_name']}</option>
                            {/foreach}
                        {/if}
                    {/foreach}
                </select>
            </div>
            <div class="form-group col-sm-12">
                <label for="schort_description">{__("Short description")}:</label>
                <textarea class="form-control" name="short_description" id="short_description" placeholder="{__("Write about your school...")}">{$spage['short_overview']}</textarea>
            </div>
            <div class="form-group col-sm-12">
                <label for="description">{__("Description")}:</label>
                <textarea rows="6" class="form-control" name="description" id="description" placeholder="{__("Write about your school...")}">{$spage['page_description']}</textarea>
            </div>

            <div class="form-group col-sm-12">
                <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
            </div>

            <!-- error -->
            <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
            <!-- error -->
        </form>
    </div>
    <div class="tab-pane" id="services">
        <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_school.php?do=editservice&amp;id={$spage['page_id']}">
            <input type="hidden" name="username" value="{$spage['page_name']}">
            <div class="form-group">
                <label class="col-sm-3 control-label text-left"> {__("Facilities")}</label>
                <div class="col-sm-4">
                    <input type="checkbox" name = "facilities[0]" value="1" {if ($spage['facility_pool'] == 1)}checked{/if}><label class = "control-label"> {__('Pool')}</label><br/>
                    <input type="checkbox" name = "facilities[1]" value="1" {if ($spage['facility_playground_out'] == 1)}checked{/if}><label class = "control-label"> {__('Outdoor playground')}</label><br/>
                    <input type="checkbox" name = "facilities[2]" value="1" {if ($spage['facility_playground_in'] == 1)}checked{/if}><label class = "control-label"> {__('Indoor playground')}</label><br/>
                </div>
                <div class="col-sm-4">
                    <input type="checkbox" name = "facilities[3]" value="1" {if ($spage['facility_library'] == 1)}checked{/if}><label class = "control-label"> {__("Library")}</label><br/>
                    <input type="checkbox" name = "facilities[4]" value="1" {if ($spage['facility_camera'] == 1)}checked{/if}><label class = "control-label"> {__("View camera online")}</label><br/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left">{__("Facility note")}</label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="facility_note" rows="4"> {$spage['note_for_facility']}</textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left"> {__("Service")}</label>
                <div class="col-sm-4">
                    <input type="checkbox" name = "services[0]" value="1" {if ($spage['service_bus'] == 1)}checked{/if}><label class = "control-label">{__('Transportation')}</label> <br/>
                    <input type="checkbox" name = "services[1]" value="1" {if ($spage['service_breakfast'] == 1)}checked{/if}><label class = "control-label">{__('Breakfast')}</label> <br/>
                </div>
                <div class="col-sm-4">
                    <input type="checkbox" name = "services[2]" value="1" {if ($spage['service_belated'] == 1)}checked{/if}><label class = "control-label">{__('Late PickUp')}</label> <br/>
                    <input type="checkbox" name = "services[3]" value="1" {if ($spage['service_saturday'] == 1)}checked{/if}><label class = "control-label">{__("Looks saturday")}</label> <br/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left">{__("Service note")}</label>
                <div class="col-sm-9 control-label">
                    <textarea class="form-control" name="service_note" rows="4"> {$spage['note_for_service']}</textarea>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-9 col-sm-offset-3">
                    <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                </div>
            </div>

            <!-- success -->
            <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
            <!-- success -->

            <!-- error -->
            <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
            <!-- error -->
        </form>
    </div>
    <div class="tab-pane" id="addmission">
        <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_school.php?do=editaddmission&amp;id={$spage['page_id']}">
            <input type="hidden" name="username" value="{$spage['page_name']}">
            <div class="form-group">
                <label class="col-sm-3 control-label text-left">{__("Age")} (*)</label>
                <label class="col-sm-1 control-label text-left">{__("From")}</label>
                <div class="col-sm-3 text-left">
                    <input type="text" name = "age_begin" id ="age_begin" value="{$spage['start_age']}" class = "form-control"/>
                </div>
                <label class="col-sm-1 control-label text-left">{__("To")}</label>
                <div class="col-sm-3 text-left">
                    <input type="text" name = "age_end" id ="age_end" value="{$spage['end_age']}" class = "form-control"/>
                </div>
                <label class="col-sm-1 control-label text-left">{__("Month")}</label>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left">{__("Tuition fee")} (*)</label>
                <label class="col-sm-1 control-label text-left">{__("From")}</label>
                <div class="col-sm-3 text-left">
                    <input type="text" name = "tuition_begin" id ="tuition_begin" value="{$spage['start_tuition_fee']}" class = "form-control"/>
                </div>
                <label class="col-sm-1 control-label text-left">{__("To")}</label>
                <div class="col-sm-3 text-left">
                    <input type="text" name = "tuition_end" id ="tuition_end" value="{$spage['end_tuition_fee']}" class = "form-control"/>
                </div>
                <label class="col-sm-1 control-label text-left">{$smarty.const.MONEY_UNIT}</label>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left">{__("Tuition note")}</label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="tuition_note" rows="2"> {$spage['note_for_tuition']}</textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left">{__("Number of student addmission")}</label>
                <div class="col-sm-9">
                    <input type="text" name = "addmission" id ="addmission" value="{$spage['admission']}" class = "form-control"/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left">{__("Addmission note")}</label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="addmission_note" rows="5"> {$spage['note_for_admission']}</textarea>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-9 col-sm-offset-3">
                    <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                </div>
            </div>
            <!-- success -->
            <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
            <!-- success -->

            <!-- error -->
            <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
            <!-- error -->
        </form>
    </div>
    <div class="tab-pane" id="info">
        <form class="js_ajax-forms form-horizontal" data-url="ci/bo/school/bo_school.php?do=editinfo&amp;id={$spage['page_id']}">
            <input type="hidden" name="username" value="{$spage['page_name']}">
            <div class="form-group">
                <label class="col-sm-3 control-label text-left">{__("Leadership")}</label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="leader" rows="6"> {$spage['info_leader']}</textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left">{__("Methods of education")}</label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="method" rows="6"> {$spage['info_method']}</textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left">{__("Teachers")}</label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="teacher" rows="6"> {$spage['info_teacher']}</textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left">{__("Nutrition")}</label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="nutrition" rows="6"> {$spage['info_nutrition']}</textarea>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-9 col-sm-offset-3">
                    <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                </div>
            </div>
            <!-- success -->
            <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
            <!-- success -->

            <!-- error -->
            <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
            <!-- error -->
        </form>
    </div>
</div>
