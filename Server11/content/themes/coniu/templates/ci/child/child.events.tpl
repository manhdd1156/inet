<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <i class="fa fa-bell fa-lg fa-fw pr10"></i>
        {__("Notification - Event")}
        {if $sub_view == "detail"}
            &rsaquo; {$data['event_name']}
        {/if}
    </div>
    {if $sub_view == ""}
        <div class="panel-body with-table">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>{__("Notification - Event")}</th>
                        <th>{__("Event level")}</th>
                        {*<th>{__("Time")}</th>*}
                        <th>{__("Registration deadline")}</th>
                    </tr>
                    </thead>
                    <tbody>
                    {$idx = 1}
                    {foreach $rows as $row}
                        <tr>
                            {if $idx > 1}<tr><td colspan="5"></td></tr>{/if}
                            <td align="center" rowspan="2" style="vertical-align:middle"><strong>{$idx}</strong></td>
                            <td style="vertical-align:middle">
                                <a href="{$system['system_url']}/child/{$child['child_id']}/events/detail/{$row['event_id']}">{$row['event_name']}</a> <a href="#" data-id="{$row['event_id']}" style="float: right; color: #5D9D58" class="js_event_toggle">{__("Show/hide")}</a>
                                {if $row['status']== $smarty.const.EVENT_STATUS_CANCELLED}
                                    <img src="{$system['system_url']}/content/themes/{$system['theme']}/images/cancel.png"/>
                                {/if}
                            </td>
                            <td align="center" style="vertical-align:middle">
                                {if $row['level']==$smarty.const.SCHOOL_LEVEL}
                                    {__("School")}
                                {elseif $row['level']==$smarty.const.CLASS_LEVEL_LEVEL}
                                    {__("Class level")}
                                {elseif $row['level']==$smarty.const.CLASS_LEVEL}
                                    {__("Class")}
                                {/if}
                            </td>
                            {*<td align="center" style="vertical-align:middle" nowrap="true">*}
                                {*{if (isset($row['begin']) && ($row['begin'] != ''))}*}
                                    {*<font color="blue">{$row['begin']}</font><br/>{$row['end']}*}
                                {*{/if}*}
                            {*</td>*}
                            <td align="center" style="vertical-align:middle">
                                {if (isset($row['registration_deadline']) && ($row['registration_deadline'] != ''))}
                                    <font color="red">{$row['registration_deadline']}</font>
                                {/if}
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" style="display: none" class="event_description_{$row['event_id']}">{$row['description']}</td>
                        </tr>
                        {$idx = $idx + 1}
                    {/foreach}
                    </tbody>
                </table>
            </div>
        </div>
    {elseif $sub_view == "detail"}
        <div class="panel-body with-table">
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/child/bochild_all.php">
                <input type="hidden" name="school_username" id="school_username" value="{$username}"/>
                <input type="hidden" name="do" id="do" value="reg_event"/>
                <input type="hidden" id="school_id" name="school_id" value="{$school['page_id']}"/>
                <input type="hidden" id="child_id" name="child_id" value="{$child['child_id']}"/>
                <input type="hidden" id="event_id" name="event_id" value="{$data['event_id']}"/>
                <input type="hidden" name="event_name" value="{$data['event_name']}"/>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <tbody>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Notification - Event")}</strong></td>
                            <td>{$data['event_name']}</td>
                        </tr>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Event level")}</strong></td>
                            <td>
                                {if $data['level']==$smarty.const.SCHOOL_LEVEL}
                                    {__("School")}
                                {elseif $data['level']==$smarty.const.CLASS_LEVEL_LEVEL}
                                    {$data['class_level_name']}
                                {elseif $data['level']==$smarty.const.CLASS_LEVEL}
                                    {$data['group_title']}
                                {/if}
                            </td>
                        </tr>
                        {*{if (isset($data['begin']) && ($data['begin'] != ''))}*}
                            {*<tr>*}
                                {*<td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Time")}</strong></td>*}
                                {*<td>*}
                                    {*<font color="blue">{$data['begin']}</font> - {$data['end']}*}
                                {*</td>*}
                            {*</tr>*}
                        {*{/if}*}
                        {if $data['must_register'] == '1'}
                            {*<tr>*}
                                {*<td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Event location")}</strong></td>*}
                                {*<td>{$data['location']}</td>*}
                            {*</tr>*}
                            {*<tr>*}
                                {*<td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Participation cost")}</strong></td>*}
                                {*<td>{$data['price']} {$smarty.const.MONEY_UNIT}</td>*}
                            {*</tr>*}
                            <tr>
                                <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Must register")}?</strong></td>
                                <td>{__('Yes')}</td>
                            </tr>
                            {if $data['registration_deadline'] != ''}
                                <tr>
                                    <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Registration deadline")}</strong></td>
                                    <td><font color="red">{$data['registration_deadline']}</font></td>
                                </tr>
                            {/if}
                            <tr>
                                <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("For child")}?</strong></td>
                                <td>{if $data['for_child'] == '1'}{__('Yes')}{else}{__('No')}{/if}</td>
                            </tr>
                            {if $data['for_child'] == '1'}
                                <tr>
                                    <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Children total")}</strong></td>
                                    <td>{$data['child_participants']}</td>
                                </tr>
                            {/if}
                            <tr>
                                <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("For parent")}?</strong></td>
                                <td>{if $data['for_parent'] == '1'}{__('Yes')}{else}{__('No')}{/if}</td>
                            </tr>
                            {if $data['for_parent'] == '1'}
                                <tr>
                                    <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Parent total")}</strong></td>
                                    <td>{$data['parent_participants']}</td>
                                </tr>
                            {/if}
                        {/if}
                        {*<tr>*}
                            {*<td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Created time")}</strong></td>*}
                            {*<td>{$data['created_at']}</td>*}
                        {*</tr>*}
                        {*{if $data['updated_at'] != ''}*}
                            {*<tr>*}
                                {*<td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Last updated")}</strong></td>*}
                                {*<td>{$data['updated_at']}</td>*}
                            {*</tr>*}
                        {*{/if}*}
                        <tr>
                            {*<td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Description")}</strong></td>*}
                            <td colspan="2">{$data['description']}</td>
                        </tr>
                        {if $data['must_register']}
                            <tr>
                                <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Register")}</strong></td>
                                <td>
                                    {if $data['for_child']}
                                        <input type="checkbox" name="childId" value="{$child['child_id']}"
                                               {if $data['participants']['child']['is_registered']}checked{/if} {if $data['can_register'] == 0}disabled{/if}> {$child['child_name']}
                                        {if $data['participants']['child']['is_registered']}
                                            <input type="hidden" name="old_child_id" value="{$child['child_id']}"/>
                                        {/if}
                                    {/if}
                                    <br/>
                                    {if $data['for_parent']}
                                        <input type="checkbox" name="parent_id"  value="{$user->_data['user_id']}" event_id="{$data['event_id']}"
                                               {if $data['participants']['parent']['is_registered']}checked{/if} {if $data['can_register'] == 0}disabled{/if}> {__("You")}
                                        {if $data['participants']['parent']['is_registered']}
                                            <input type="hidden" name="old_parent_id" value="{$user->_data['user_id']}"/>
                                        {/if}
                                    {/if}
                                </td>
                            </tr>
                        {/if}
                        </tbody>
                    </table>
                </div>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-1">
                        {if $data['can_register'] > 0  && $data['must_register']}
                            <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                        {/if}
                        <a class="btn btn-default" href="{$system['system_url']}/child/{$child['child_id']}/events">{__("Lists")}</a>
                    </div>
                </div>
                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->
                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
            </form>
        </div>
    {/if}
</div>