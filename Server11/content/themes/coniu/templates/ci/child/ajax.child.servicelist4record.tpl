<table class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th>#</th>
        <th>{__("Register")}</th>
        <th>{__("Service")}</th>
        <th>{__("Service fee")}&nbsp;({$smarty.const.MONEY_UNIT})</th>
        <th>{__("Registration time")}</th>
        <th>{__("Registrar")}</th>
    </tr>
    </thead>
    <tbody>
    {$idx = 1}
    {$total = count($cb_services)}
    {foreach $cb_services as $service}
        <tr>
            <td align="center">{$idx}</td>
            <td align="center">
                {if isset($service['service_usage_id']) && ($service['service_usage_id'] > 0)}
                    {__("Registered")}
                    {$total = $total - 1}
                {else}
                    <input type="checkbox" class="child" name="cbServiceIds[]" value="{$service['service_id']}">
                {/if}
            </td>
            <td>{$service['service_name']}</td>
            <td align="right">{moneyFormat($service['fee'])}</td>
            <td align="center">
                {if isset($service['service_usage_id']) && ($service['service_usage_id'] > 0)}
                    {$service['recorded_at']}
                {/if}
            </td>
            <td align="center">
                {if isset($service['service_usage_id']) && ($service['service_usage_id'] > 0)}
                    {$service['user_fullname']}
                {/if}
            </td>
        </tr>
        {$idx = $idx + 1}
    {/foreach}
    </tbody>
</table>