<div class="panel-body with-table">
    <table class="table table-bordered">
        <tr>
            <td class="col-sm-3 text-right">{__("Month")}</td>
            <td>{$data['month']}</td>
        </tr>
        <tr>
            <td class="col-sm-3 text-right">{__("Total")}</td>
            <td>{moneyFormat($data['total_amount'])}</td>
        </tr>
        <tr>
            <td class="col-sm-3 text-right">{__("Status")}</td>
            <td>
                {if $data['status']==$smarty.const.TUITION_CHILD_NOT_PAID}
                    <strong>{__("Not paid yet")}</strong>
                {elseif $data['status']==$smarty.const.TUITION_CHILD_PARENT_PAID}
                    <strong>{__("Paid and waiting confirmation")}</strong>
                {else}
                    <strong>{__("Paid")}</strong>
                {/if}
            </td>
        </tr>
        <tr>
            <td class="col-sm-3 text-right">{__("Description")}</td>
            <td>{$data['description']}</td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <a class="btn btn-default" href="{$system['system_url']}/child/{$child['child_id']}/tuitions/history/{$data['tuition_id']}">{__("Used last month")}</a>
                {if $data['status']==$smarty.const.TUITION_CHILD_NOT_PAID}
                    <a class="btn btn-primary js_child-tuition" data-handle="pay" data-child_id="{$data['child_id']}" data-id="{$data['tuition_child_id']}">{__("Pay")}</a>
                {/if}
            </td>
        </tr>
    </table>
    <div class="table-responsive">
        {if count($data['tuition_detail']) > 0}
            <table class="table table-striped table-bordered table-hover bg_white">
                <thead>
                <tr>
                    <th>#</th>
                    <th>{__("Fee name")}</th>
                    <th>{__("Quantity")}</th>
                    <th>{__("Unit price")}</th>
                    <th>{__("Deduction")}</th>
                    <th>{__("Money amount")}</th>
                </tr>
                </thead>
                <tbody>
                {*Hiển thị danh sách phí/dịch vụ của trẻ*}
                {$idx = 1}
                {foreach $data['tuition_detail'] as $detail}
                    <tr>
                        <td align="center">{$idx}</td>
                        {if $detail['type'] == $smarty.const.TUITION_DETAIL_FEE}
                            <td>{$detail['fee_name']}</td>
                            <td class="text-center">{$detail['quantity']}</td>
                            <td class="text-right">{moneyFormat($detail['unit_price'])}</td>
                            {if $detail['fee_type'] != $smarty.const.FEE_TYPE_MONTHLY}
                                <td class="text-center">{if $detail['quantity_deduction']!=0}{$detail['quantity_deduction']}({__('Day')}){/if}</td>
                            {else}
                                <td class="text-right">{if $detail['unit_price_deduction']!=0}{moneyFormat($detail['unit_price_deduction'])}({$smarty.const.MONEY_UNIT}){/if}</td>
                            {/if}
                        {elseif $detail['type'] == $smarty.const.TUITION_DETAIL_SERVICE}
                            <td>{$detail['service_name']}</td>
                            <td class="text-center">{$detail['quantity']}</td>
                            <td class="text-right">{moneyFormat($detail['unit_price'])}</td>
                            {if $detail['service_type'] == $smarty.const.SERVICE_TYPE_DAILY}
                                <td class="text-center">{if $detail['quantity_deduction']!=0}{$detail['quantity_deduction']} ({__('Day')}){/if}</td>
                            {elseif $detail['service_type'] == $smarty.const.SERVICE_TYPE_MONTHLY}
                                <td class="text-right">{if $detail['unit_price_deduction']!=0}{moneyFormat($detail['unit_price_deduction'])}{/if}</td>
                            {else}
                                <td class="text-center">{if $detail['quantity_deduction']!=0}{$detail['quantity_deduction']} ({__('Times')}){/if}</td>
                            {/if}
                        {else}
                            <td colspan="4">{__('Late PickUp')}</td>
                        {/if}
                        <td class="text-right">
                            {if $detail['type'] != $smarty.const.LATE_PICKUP_FEE}
                                {moneyFormat(($detail['quantity']  * $detail['unit_price'] - $detail['quantity_deduction'] * $detail['unit_price_deduction']))}
                            {else}
                                {moneyFormat($detail['unit_price'])}
                            {/if}
                        </td>
                    </tr>
                    {$idx = $idx + 1}
                {/foreach}

                {* Hiển thị nợ tháng trước *}
                {if $data['debt_amount'] != 0}
                    <tr>
                        <td colspan="5" class="text-right">{__("Debt amount of previous month")} {$data['pre_month']}</td>
                        <td class="text-right">{moneyFormat($data['debt_amount'])}</td>
                    </tr>
                {/if}
                <tr>
                    <td colspan="3"></td>
                    <td colspan="2" class="text-right"><strong>{__("Total")}</strong></td>
                    <td class="text-right"><strong>{moneyFormat($data['total_amount'])}</strong></td>
                </tr>
                {* Hiển thị nợ tháng này *}
                {if ($data['status']==$smarty.const.TUITION_CHILD_CONFIRMED) && (($data['total_amount'] - $data['paid_amount']) != 0)}
                    <tr>
                        <td colspan="5" class="text-right"><strong>{__("Paid")}</strong></td>
                        <td class="text-right"><strong>{moneyFormat($data['paid_amount'])}</strong></td>
                    </tr>
                    <tr>
                        <td colspan="5" class="text-right"><strong>{__("Debt amount of this month")}</strong></td>
                        <td class="text-right"><strong>{moneyFormat($data['total_amount'] - $data['paid_amount'])}</strong></td>
                    </tr>
                {/if}
                {if (isset($data['description']) && ({$data['description']} != ''))}
                    <tr>
                        <td colspan="2" class="text-right"><strong>{__("Description")}</strong></td>
                        <td colspan="4">{$data['description']}</td>
                    </tr>
                {/if}
                </tbody>
            </table>
        {/if}
    </div>
</div>
