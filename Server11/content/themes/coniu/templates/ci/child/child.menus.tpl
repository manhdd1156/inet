<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <div class="pull-right flip">
            {if $sub_view == "detail"}
                <a href="{$system['system_url']}/child/{$child['child_id']}/menus" class="btn btn-default">
                    <i class="fa fa-list"></i> {__("Lists")}
                </a>
            {/if}
        </div>
        <i class="fas fa-utensils fa-fw fa-lg pr10"></i>
        {__("Menu")}
        {if $sub_view == ""}
            &rsaquo; {__("Menu list")}
        {elseif $sub_view == "detail"}
            &rsaquo; {__('Detail menu')} {$data['menu_name']}
        {/if}
    </div>
    {if $sub_view == ""}
        <div class="panel-body with-table">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover js_dataTable">
                    <thead>
                    <tr><th colspan="4">{__("Menu list")}&nbsp;({$rows|count})</th></tr>
                    <tr>
                        <th>
                            {__("#")}
                        </th>
                        <th>
                            {__("Menu name")}
                        </th>
                        <th>
                            {__("Begin")}
                        </th>
                        <th>
                            {__("Scope")}
                        </th>
                        {*<th>*}
                            {*{__("Use meal")}*}
                        {*</th>*}
                        {*<th>*}
                            {*{__("Study saturday")}*}
                        {*</th>*}
                    </tr>
                    </thead>
                    <tbody>
                    {$idx = 1}
                    {foreach $rows as $row}
                        <tr>
                            <td>
                                {$idx}
                            </td>
                            <td>
                                <a href="{$system['system_url']}/child/{$child['child_id']}/menus/detail/{$row['menu_id']}">{$row['menu_name']}</a> {if !$row['use']}({__("Not use now")}){/if}
                            </td>
                            <td>
                                {$row['begin']}
                            </td>
                            <td>
                                {if $row['applied_for']==$smarty.const.SCHOOL_LEVEL}
                                    {__("School")}
                                {elseif $row['applied_for']==$smarty.const.CLASS_LEVEL_LEVEL}
                                    {foreach $class_levels as $cl}
                                        {if $cl['class_level_id'] == $row['class_level_id']}{$cl['class_level_name']}{/if}
                                    {/foreach}
                                {elseif $row['applied_for']==$smarty.const.CLASS_LEVEL}
                                    {foreach $classes as $value}
                                        {if $value['group_id'] == $row['class_id']}{$value['group_title']}{/if}
                                    {/foreach}
                                {/if}
                            </td>
                            {*<td>*}
                                {*{if $row['is_meal']}*}
                                    {*{__('Yes')}*}
                                {*{else}*}
                                    {*{__('No')}*}
                                {*{/if}*}
                            {*</td>*}
                            {*<td>*}
                                {*{if $row['is_saturday']}*}
                                    {*{__('Yes')}*}
                                {*{else}*}
                                    {*{__('No')}*}
                                {*{/if}*}
                            {*</td>*}
                        </tr>
                        {$idx = $idx + 1}
                    {/foreach}
                    </tbody>
                </table>
            </div>
        </div>
    {elseif $sub_view == "detail"}
        <div class="panel-body with-table">
            <table class = "table table-bordered">
                <tbody>
                <tr>
                    <td class = "col-sm-3 text-right"><strong>{__('Menu name')}</strong></td>
                    <td>
                        {$data['menu_name']}
                    </td>
                </tr>
                <tr>
                    <td class = "col-sm-3 text-right"><strong>{__('Scope')}</strong></td>
                    <td>
                        {if isset($classes)} {$classes['group_title']}
                        {elseif isset($class_level)}{$class_level['class_level_name']}
                        {elseif !isset($classes) && !isset($class_level)} {__('School')}
                        {/if}
                    </td>
                </tr>
                <tr>
                    <td class = "col-sm-3 text-right">{__('Begin')}</td>
                    <td>
                        {$data['begin']}
                    </td>
                </tr>
                {*<tr>*}
                    {*<td class = "col-sm-3 text-right">{__('Use meal')}</td>*}
                    {*<td>*}
                        {*{if $data['is_meal']}*}
                            {*{__('Yes')}*}
                        {*{else}*}
                            {*{__('No')}*}
                        {*{/if}*}
                    {*</td>*}
                {*</tr>*}
                {*<tr>*}
                    {*<td class = "col-sm-3 text-right">{__('Study saturday')}</td>*}
                    {*<td>*}
                        {*{if $data['is_saturday']}*}
                            {*{__('Yes')}*}
                        {*{else}*}
                            {*{__('No')}*}
                        {*{/if}*}
                    {*</td>*}
                {*</tr>*}
                {if $data['description'] != ''}
                    <tr>
                        <td class = "col-sm-3 text-right">{__('Description')}</td>
                        <td>
                            {$data['description']}
                        </td>
                    </tr>
                {/if}
                </tbody>
            </table>
            <div class = "table-responsive">
                <table class = "table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th> <center>{__('#')}</center> </th>
                        <th> <center>{__('Time')}</center> </th>
                        <th {if !$data['is_meal']}class="hidden"{/if}> <center>{__('Activity')}</center> </th>
                        <th> <center>{__('Monday')}</center> </th>
                        <th> <center>{__('Tuesday')}</center> </th>
                        <th> <center>{__('Wednesday')}</center> </th>
                        <th> <center>{__('Thursday')}</center> </th>
                        <th> <center>{__('Friday')}</center> </th>
                        <th {if !$data['is_saturday']}class="hidden"{/if}> <center>{__('Saturday')}</center> </th>
                    </tr>
                    </thead>
                    <tbody>
                    {$idx = 1}
                    {foreach $data['details'] as $k => $row}
                        {$array = array_values($row)}
                        {$temp = array()}
                        {foreach $array as $k => $value}
                            {if $data['is_saturday']}
                                {if ($k >= 4) && ($k < (count($array)))}
                                    {$temp[] = $value}
                                {/if}
                            {else}
                                {if ($k >= 4) && ($k < (count($array) - 1))}
                                    {$temp[] = $value}
                                {/if}
                            {/if}
                        {/foreach}
                        <tr>
                            <td style="vertical-align: middle" align="center">
                                <strong>{$idx}</strong>
                            </td>
                            <td style="vertical-align: middle" align="center">
                                <strong>{$row['meal_time']}</strong>
                            </td>
                            <td {if !$data['is_meal']}class="hidden"{/if} align="center">
                                <strong>{$row['meal_name']}</strong>
                            </td>
                            {$col = 1}
                            {for $i = 0; $i < count($temp); $i++}
                                {if $temp[$i] === $temp[($i+1)]}
                                    {$col = $col + 1}
                                {else}
                                    <td colspan = "{$col}" align="center">
                                        {nl2br($temp[$i])}
                                    </td>
                                    {$col = 1}
                                {/if}
                            {/for}
                        </tr>
                        {$idx = $idx + 1}
                    {/foreach}
                    </tbody>
                </table>
            </div>
        </div>
    {/if}
</div>