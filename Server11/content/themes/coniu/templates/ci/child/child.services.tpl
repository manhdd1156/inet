<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <div class="pull-right flip">
            <a href="https://blog.coniu.vn/huong-dan-phu-huynh-dang-ky-dich-vu-tren-website-coniu/" class="btn btn-info btn_guide" target="_blank">
                <i class="fa fa-info-circle"></i> {__("Guide")}
            </a>
        </div>
        <i class="fa fa-life-ring fa-lg fa-fw pr10"></i>
        {__("Service")}
        {if $sub_view == ""}
            &rsaquo; {__('Service usage information')}
        {elseif $sub_view == "reg"}
            &rsaquo; {__('Register service')}
        {/if}
    </div>
    {if $sub_view == ""}
        <div class="panel-body with-table">
            <div align="center"><strong>{__("Monthly and daily services")}</strong></div>
            <br/>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>{__("Service")}</th>
                        <th>{__("Service fee")}&nbsp;({$smarty.const.MONEY_UNIT})</th>
                        <th>{__("Service type")}</th>
                        <th>{__("Time")}</th>
                    </tr>
                    </thead>
                    <tbody>
                    {$idx = 1}
                    {foreach $ncb_services as $row}
                        <tr>
                            <td align="center">{$idx}</td>
                            <td>{$row['service_name']}</td>
                            <td align="right">{moneyFormat($row['fee'])}</td>
                            <td align="center">
                                {if $row['type']==$smarty.const.SERVICE_TYPE_MONTHLY}
                                    {__("Monthly service")}
                                {elseif $row['type']==$smarty.const.SERVICE_TYPE_DAILY}
                                    {__("Daily service")}
                                {/if}
                            </td>
                            <td align="center">{$row['begin']}&nbsp;-&nbsp;{$row['end']}</td>
                        </tr>
                        {$idx = $idx + 1}
                    {/foreach}

                    {if $idx == 1}
                        <tr class="odd">
                            <td valign="top" align="center" colspan="5" class="dataTables_empty">
                                {__("No data available in table")}
                            </td>
                        </tr>
                    {/if}

                    </tbody>
                </table>
            </div>
        </div>
        <div class="border_bot" style="width: 500px; margin: auto; padding: auto"></div>
        <div class="panel-body with-table">
            <div align="center"><strong>{__("Count-based service")}</strong></div>
            <br/>
            <form class="js_ajax-forms form-horizontal" data-url="#">
                <div class="form-group pl5">
                    <div class='col-sm-4'>
                        <select name="service_id" id="service_id" class="form-control">
                            <option value="">{__("All services")}</option>
                            {$idx = 1}
                            {foreach $cb_services as $service}
                                <option value="{$service['service_id']}">{$idx} - {$service['service_name']}</option>
                                {$idx = $idx + 1}
                            {/foreach}
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <div class='input-group date' id='history_begin_picker'>
                            <input type='text' name="begin" id="begin" value="{$begin}" class="form-control" placeholder="{__("From date")}"/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class='input-group date' id='history_end_picker'>
                            <input type='text' name="end" id="end" class="form-control" placeholder="{__("To date")}"/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                    <div class='col-sm-2'>
                        <div class="form-group pl10">
                            <a href="#" id="search" class="btn btn-default js_service-search-history" data-sid="{$school_id}" data-cid="{$child['child_id']}" {if count($cb_services)==0}disabled{/if}>{__("Search")}</a>
                            <label id="loading" class="btn btn-info x-hidden">{__("Loading")}...</label>
                        </div>
                    </div>
                </div>
                <div class="table-responsive" id="usage_history" name="usage_history">
                    {*{include file="ci/child/ajax.service.history.tpl"}*}
                </div>
            </form>
        </div>
    {elseif $sub_view == "reg"}
        <div class="panel-body with-table">
            <form class="js_ajax-forms-success-reload form-horizontal" data-url="ci/bo/child/bochild_all.php">
                <input type="hidden" name="school_username" id="school_username" value="{$username}"/>
                <input type="hidden" name="do" id="do" value="reg"/>
                <input type="hidden" id = "school_id" name="school_id" value="{$school['page_id']}"/>
                <input type="hidden" id = "child_id" name="child_id" value="{$child['child_id']}"/>

                <div><strong>{__("Monthly and daily services")}</strong></div>
                <div class = "table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>{__("Register")}</th>
                            <th>{__("Service")}</th>
                            <th>{__("Service fee")}&nbsp;({$smarty.const.MONEY_UNIT})</th>
                            <th>{__("Time")}</th>
                        </tr>
                        </thead>
                        <tbody>
                        {$idx = 1}
                        {$cnt = 0}
                        {foreach $ncb_services as $service}
                            <tr>
                                <td align="center">{$idx}</td>
                                <td align="center">
                                    {if isset($service['service_child_id']) && ($service['service_child_id'] > 0)}
                                        {__("Registered")}
                                    {else}
                                        <input type="checkbox" class="child" name="ncbServiceIds[]" value="{$service['service_id']}">
                                        {$cnt = $cnt + 1}
                                    {/if}
                                </td>
                                <td>{$service['service_name']}</td>
                                <td align="right">{moneyFormat($service['fee'])}</td>
                                <td align="center">
                                    {if isset($service['service_child_id']) && ($service['service_child_id'] > 0)}
                                        {$service['begin']}&nbsp;-&nbsp;{$service['end']}
                                    {/if}
                                </td>
                            </tr>
                            {$idx = $idx + 1}
                        {/foreach}
                        </tbody>
                    </table>
                    <input type="hidden" id="ncb_cnt" value="{$cnt}"/>
                </div>

                <div><strong>{__("Count-based service")}</strong></div>
                <div class="form-group pl5">
                    <label class="col-sm-3 control-label text-left">{__("Select date")}(*):</label>
                    <div class="col-sm-3" id="using_at_div">
                        <div class='input-group date' id='using_time_picker'>
                            <input type="text" name="using_at" id="using_at" value="{$using_at}" class="form-control" placeholder="{__("Using time")} (*)" required/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                </div>

                <div class="table-responsive" id="service_list" name="service_list">
                    {include file="ci/child/ajax.child.servicelist4record.tpl"}
                </div>
                <div class="form-group pl5" id="service_btnSave">
                    <div class="col-sm-9">
                        <button type="submit" class="btn btn-primary padrl30" {if (((isset($total) && $total == 0) || count($cb_services) == 0)) && ($cnt == 0)}disabled{/if}>{__("Save")}</button>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    {/if}
</div>