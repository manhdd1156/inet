{*Bảng dịch vụ theo SỐ LẦN*}
<div align="center"><strong>{__("Detail")}&nbsp;({$cbs_usage|count} {__("times")})</strong></div>
<table class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th>#</th>
        <th>{__("Using time")}</th>
        <th>{__("Registrar")}</th>
        <th>{__("Registered time")}</th>
    </tr>
    </thead>
    <tbody>
    {$idx = 1}
    {$serviceTotal = 0}
    {$serviceId = -1}
    {foreach $cbs_usage as $row}
        {if $row['service_id'] != $serviceId}
            {if $idx > 1}
                <tr>
                    <td colspan="2" align="center"><strong><font color="blue">{__("Total of service")}</font></strong></td>
                    <td colspan="2" align="left"><strong><font color="blue">{$serviceTotal} {__("times")}</font></strong></td>
                </tr>
            {/if}
            <tr>
                <td colspan="4"><strong>{$row['service_name']}</strong></</td>
            </tr>
            {$serviceTotal = 0}
        {/if}
        <tr>
            <td align="center">{$idx}</td>
            <td>{$row['using_at']}</td>
            <td>{$row['user_fullname']}</td>
            <td>{$row['recorded_at']}</td>
        </tr>
        {$serviceId = $row['service_id']}
        {$serviceTotal = $serviceTotal + 1}
        {$idx = $idx + 1}
    {/foreach}
    {if $idx > 2}
        <tr>
            <td colspan="2" align="center"><strong><font color="blue">{__("Total of service")}</font></strong></td>
            <td colspan="2" align="left"><strong><font color="blue">{$serviceTotal} {__("times")}</font></strong></td>
        </tr>
        <tr>
            <td colspan="2" align="center"><strong><font color="red">{__("Total")|upper}</font></strong></td>
            <td colspan="2" align="left"><strong><font color="red">{$idx - 1} {__("times")}</font></strong></td>
        </tr>
    {/if}

    {if $idx == 1}
        <tr class="odd">
            <td valign="top" align="center" colspan="4" class="dataTables_empty">
                {__("No data available in table")}
            </td>
        </tr>
    {/if}
    </tbody>
</table>