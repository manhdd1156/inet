<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <i class="fa-fw fa-lg pr10"></i>
        <div class="col-sm-6">
            <strong>{__("Child")}: {$child['child_name']}</strong>
        </div>
        {*<select name="child_id" id="select_child_id" class="col-md-6">*}
            {*{foreach $children as $_child}*}
                {*<option value="{$_child['child_id']}" {if $child['child_id']==$_child['child_id']}selected{/if}>{$_child['child_name']}</option>*}
            {*{/foreach}*}
        {*</select>*}
        <div class="pull-right flip">
            <a href="{$system['system_url']}/childinfo/{$child['child_parent_id']}/childdetail" class="btn btn-xs btn-primary" style="margin-right: 10px.........">
                <i class="fa fa-info"></i> {__("Student information")}
            </a>
            <a href="{$system['system_url']}/child/{$child['child_id']}/attendance/resign">
                <i class="fa fa-tasks fa-lg fa-fw pr10"></i> {__("Resign")}
            </a>&nbsp;|&nbsp;
            <a href="{$system['system_url']}/child/{$child['child_id']}/medicines/add">
                <i class="fa fa-medkit fa-lg fa-fw pr10"></i> {__("Medicines")}
            </a>
        </div>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-6">
                <div class="box-primary">
                    <div class="box-header">
                        <strong>
                            {__("School")}:&nbsp;<a href="{$system['system_url']}/pages/{$insights['school']['page_name']}">{$insights['school']['page_title']}</a>
                            &nbsp;|&nbsp;
                            {__("Class")}:&nbsp;<a href="{$system['system_url']}/groups/{$insights['class']['group_name']}">{$insights['class']['group_title']}</a>
                        </strong>
                    </div>
                    <div class="list-group">
                        <div class="list-group-item">
                            {__("School manager")}:
                            {if isset($insights['admin'])}
                                <span class="name js_user-popover" data-uid="{$insights['admin']['user_id']}">
                                        <a href="{$system['system_url']}/{$insights['admin']['user_name']}">{$insights['admin']['user_fullname']}</a>
                                    </span>
                                {if $insights['admin']['user_id'] != $user->_data['user_id']}
                                    <a href="#" class="far fa-comments fa-lg js_chat-start" data-name="{$insights['admin']['user_fullname']}" data-uid="{$insights['admin']['user_id']}"></a>
                                {/if}
                            {/if}
                        </div>
                        <div class="list-group-item">
                            {__("GV")}:
                            {if count($insights['teachers']) > 0}
                                {foreach $insights['teachers'] as $row}
                                    <span class="name js_user-popover" data-uid="{$row['user_id']}">
                                        <a href="{$system['system_url']}/{$row['user_name']}">{$row['user_fullname']}</a>
                                    </span>
                                    {if $row['user_id'] != $user->_data['user_id']}
                                        <a href="#" class="far fa-comments fa-lg js_chat-start" data-name="{$row['user_fullname']}" data-uid="{$row['user_id']}"></a>
                                    {/if}
                                    &nbsp;&nbsp;&nbsp;
                                {/foreach}
                            {else}
                                {__("No teacher")}
                            {/if}
                        </div>
                    </div>
                </div>

                {if count($insights['medicines']) > 0}
                    <div class="box-primary">
                        <div class="box-header">
                            <i class="fa fa-medkit"></i>
                            <strong>{__("Today medicines")}</strong>&nbsp;
                            <a href="{$system['system_url']}/child/{$child['child_id']}/medicines" title="{__("See All")}" class="pull-right flip">{__("See All")}</a>
                        </div>
                        <div class="list-group" id="medicine_on_dashboard">
                            {$idx = 1}
                            {foreach $insights['medicines'] as $row}
                                <div class="list-group-item">
                                    <strong>{$idx}</strong>-<a href="{$system['system_url']}/child/{$child['child_id']}/medicines/edit/{$row['medicine_id']}">{$row['medicine_list']}</a>
                                    {if $row['status']== $smarty.const.MEDICINE_STATUS_NEW}
                                        <img src="{$system['system_url']}/content/themes/{$system['theme']}/images/new.gif"/>
                                    {elseif $row['status']== $smarty.const.MEDICINE_STATUS_CONFIRMED}
                                        <i class="fa fa-check"></i>
                                    {/if}
                                    <br/>{$row['time_per_day']} {__("times/day")}&nbsp;|&nbsp;{$row['begin']}&nbsp;-&nbsp;{$row['end']}<br/>
                                    <i>{__("Guide")}:</i> <strong>{$row['guide']}</strong>
                                    {if count($row['detail']) > 0}
                                        <br/>{__("Medicated")}:
                                        {foreach $row['detail'] as $detail}
                                            <br/>&nbsp;&nbsp;&nbsp;<strong>{$detail['time_on_day']}</strong>&nbsp;-&nbsp;{$detail['created_at']}&nbsp;|&nbsp;{$detail['user_fullname']}
                                            {if $detail['created_user_id'] != $user->_data['user_id']}
                                                <a href="#" class="far fa-comments fa-lg js_chat-start" data-name="{$detail['user_fullname']}" data-uid="{$detail['created_user_id']}"></a>
                                            {/if}
                                        {/foreach}
                                    {/if}
                                </div>
                                {$idx = $idx + 1}
                            {/foreach}
                        </div>
                    </div>
                {/if}
                {if $school['config']['display_children_list']}
                    <div class="table-responsive">
                        <div><strong>{__("Children list")}&nbsp;({$insights['children']|count})</strong></div>
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                            <tr>
                                <th style="width: 40px">{__("No.")}</th>
                                <th {if $school['config']['display_parent_info_for_others']}width="40%"{/if}>{__("Full name")}</th>
                                {if $school['config']['display_parent_info_for_others']}
                                    <th>{__("Parent")}</th>
                                {/if}
                            </tr>
                            </thead>
                            <tbody>
                            {$idx = 1}
                            {foreach $insights['children'] as $row}
                                <tr>
                                    <td align="center" style="vertical-align:middle">{$idx}</td>
                                    <td class="align-middle">
                                        {$row['child_name']}<br/>
                                        {if !is_empty($row['birthday'])}
                                            ({$row['birthday']})
                                        {/if}
                                    </td>
                                    {if $school['config']['display_parent_info_for_others']}
                                        <td>
                                            {if count($row['parent']) == 0}
                                                {__("No parent")}
                                            {else}
                                                {foreach $row['parent'] as $_user}
                                                    <span class="name js_user-popover" data-uid="{$_user['user_id']}">
                                                        <a href="{$system['system_url']}/{$_user['user_name']}">{$_user['user_fullname']}</a>
                                                    </span>
                                                    {if $_user['user_id'] != $user->_data['user_id']}
                                                        <a href="#" class="far fa-comments fa-lg js_chat-start" data-name="{$_user['user_fullname']}" data-uid="{$_user['user_id']}"></a>
                                                    {/if}
                                                {/foreach}
                                            {/if}<br/>
                                            {if !is_empty($row['parent_phone'])}
                                                ({$row['parent_phone']})
                                            {/if}

                                        </td>
                                    {/if}
                                </tr>
                                {$idx = $idx + 1}
                            {/foreach}
                            </tbody>
                        </table>
                    </div>
                {/if}
            </div>

            <div class="col-sm-6">
                <div class="box-primary">
                    <div class="box-header">
                        <i class="fa fa-bell"></i>
                        <strong>{__("Notification - Event")}</strong>&nbsp;
                        <a href="{$system['system_url']}/child/{$child['child_id']}/events" title="{__("See All")}" class="pull-right flip">{__("See All")}</a>
                    </div>
                    <div class="list-group" id="event_on_dashboard">
                        {$idx = 1}
                        {foreach $insights['events'] as $row}
                            <div class="list-group-item">
                                <strong>{$idx} - <a href="{$system['system_url']}/child/{$child['child_id']}/events/detail/{$row['event_id']}">{$row['event_name']}</a></strong>
                                <br/>
                                {$row['description']}<br/>
                            </div>
                            {$idx = $idx + 1}
                        {/foreach}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>