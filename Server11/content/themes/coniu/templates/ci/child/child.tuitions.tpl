<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <div class="pull-right flip">
            {if $sub_view == "history"}
                <a href="{$system['system_url']}/child/{$child['child_id']}/tuitions" class="btn btn-success">{__('Tuition')}</a>
            {/if}
        </div>
        <i class="fas fa-money-bill-alt fa-lg fa-fw pr10"></i>
        {__("Tuition")}
        {if $sub_view == "history"}
            &rsaquo; {__("History")}
        {elseif $sub_view == "detail"}
            &rsaquo; {__("Detail")}
        {/if}
    </div>
    {if $sub_view == ""}
        <div class="panel-body with-table">
            <div class="col-sm-12">
                <p><b>{__("Transfer credit information")}: {nl2br($dataCon['bank_account'])}</b></p>
            </div>
            <div><strong>{__("Tuition list")}&nbsp;({$rows|count})</strong></div>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>{__("Month")}</th>
                        <th>{__("Monthly Fee")}</th>
                        {*<th>{__("Deduction")}&nbsp;({$smarty.const.MONEY_UNIT})</th>*}
                        <th>{__("Previous debt")}</th>
                        <th>{__("Total")}</th>
                        <th>{__("Paid")}</th>
                        <th>{__("Option")}</th>
                    </tr>
                    </thead>
                    <tbody>
                    {$idx = 1}
                    {foreach $rows as $row}
                        <tr>
                            <td class="text-center">{$idx}</td>
                            <td class="text-center"><a href="{$system['system_url']}/child/{$child['child_id']}/tuitions/detail/{$row['tuition_id']}">{$row['month']}</a></td>
                            <td class="text-right">{moneyFormat($row['total_amount'] - $row['debt_amount'] + $row['total_deduction'])}</td>
                            {*<td class="text-right">{moneyFormat($row['total_deduction'])}</td>*}
                            <td class="text-right">{moneyFormat($row['debt_amount'])}</td>
                            <td class="text-right">{moneyFormat($row['total_amount'])}</td>
                            <td class="text-right">{if $row['status'] == $smarty.const.TUITION_CHILD_CONFIRMED}{moneyFormat($row['paid_amount'])}{else}0{/if}</td>
                            <td class="text-center">
                                {if $row['status']==$smarty.const.TUITION_CHILD_NOT_PAID}
                                    <a class="btn btn-xs btn-default js_child-tuition" data-handle="pay" data-child_id="{$row['child_id']}" data-id="{$row['tuition_child_id']}">{__("Pay")}</a>
                                {elseif $row['status']==$smarty.const.TUITION_CHILD_PARENT_PAID}
                                    <strong>{__("Paid and waiting confirmation")}</strong>
                                {else}
                                    <strong>{__("Paid")}</strong>
                                {/if}
                            </td>
                        </tr>
                        {$idx = $idx + 1}
                    {/foreach}
                    </tbody>
                </table>
            </div>
        </div>
    {elseif $sub_view == "detail"}
        {include file="ci/child/child.tuitions.detail.tpl"}
    {elseif $sub_view == "history"}
        {include file="ci/child/child.tuitions.history.tpl"}
    {/if}
</div>