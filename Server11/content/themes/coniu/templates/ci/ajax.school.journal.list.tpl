<div class="panel panel-default panel-photos">
    <div class="panel-body">
        {if count($results) > 0}
            {foreach $results as $row}
                <div class="row" style="margin-bottom: 20px">
                    <div class="journal_album">
                        <div class="col-sm-12 mb5">
                            <div class="diary_date"><i class="far fa-calendar-check" aria-hidden="true"></i> <strong>{$row['created_at']} ({count($row['pictures'])} {__("photo")})</strong> - <span class="caption_show">{$row['caption']}</span></div>
                            <div class="caption_edit x-hidden">
                                <textarea type="text" class="caption_edit x-hidden caption_{$row['child_journal_album_id']} js_school-journal-edit-caption_change" name="caption" data-id="{$row['child_journal_album_id']}" data-child="{$child['child_id']}" data-username="{$username}" data-handle="edit_caption">{$row['caption']}</textarea>
                                <a href="#" class="js_school-journal-edit-caption" data-id="{$row['child_journal_album_id']}" data-child="{$child['child_id']}" data-username="{$username}" data-handle="edit_caption"><i class="fa fa-save" aria-hidden="true"></i> </a>
                            </div>
                            <div class="pull-right flip">
                                <button type="button" class="btn btn-default btn-xs" data-toggle="modal" data-url="ci/bo/modal.php?do=add_photos_school&id={$row['child_journal_album_id']}&username={$username}&childid={$child['child_id']}&module={$is_module}">
                                    <i class="fa fa-plus-circle"></i> {__("Add Photos")}
                                </button>
                                <a class="btn btn-xs btn-default edit_caption">{__("Edit caption")}</a>
                                <a class="btn btn-xs btn-default show_or_hidden_diary" data-id="{$row['child_journal_album_id']}">{__("Show/hide")}</a>
                                <a class="btn btn-xs btn-danger js_school-journal-delete-album" data-id="{$row['child_journal_album_id']}" data-child="{$child['child_id']}" data-username="{$username}" data-handle="delete_album">{__("Delete Album")}</a>
                            </div>
                        </div>
                        <div class="album_images_{$row['child_journal_album_id']}">
                            {foreach $row['pictures'] as $photo}
                                {include file='__feeds_photo_child_school.tpl' _context="photos" _small=true}
                                {$day = $photo['created_at']}
                            {/foreach}
                        </div>
                    </div>
                </div>
            {/foreach}
        {else}
            <div class="" style = "font-weight: bold; text-align: center">
                {__("No data")}
            </div>
        {/if}
    </div>
</div>
