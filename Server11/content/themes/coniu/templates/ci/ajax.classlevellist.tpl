<ul>
    {foreach $results as $class_level}
        <li class="feeds-item" data-id="{$class_level['class_level_name']}">
            <input type="hidden" name="classlevel_id[]" value="{$class_level['class_level_id']}"/>
            <div class="data-container">

                <div class="data-content">
                </div>
                    <strong>
                        {$class_level['class_level_name']}
                    </strong>
                    <div class="pull-right flip">
                        <div class="btn btn-default js_classlevel-remove" data-uid="{$class_level['class_level_id']}">{__("Remove")}</div>
                    </div>

            </div>
        </li>
    {/foreach}
</ul>