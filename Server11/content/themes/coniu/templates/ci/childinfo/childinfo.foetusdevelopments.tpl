<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <div class="pull-right flip">
            {if $sub_view == "detail"}
                <a href="{$system['system_url']}/childinfo/{$child['child_parent_id']}/foetusdevelopments" class="btn btn-default">
                    <i class="fa fa-list"></i> {__("Lists")}
                </a>
            {/if}
        </div>
        <i class="fa fa-child fa-fw fa-lg pr10"></i>
        {__('Foetus development')}
        {if $sub_view == "detail"}
            &rsaquo; {__('Detail')}
        {/if}
    </div>
    {if $sub_view == ""}
        <div class="panel-body with-table form-horizontal">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover js_dataTable">
                    <thead>
                    <tr>
                        <th colspan="3">{__("Pregnancy check-up")} ({count($foetus_info)})</th>
                    </tr>
                    <tr>
                        <th>{__('No.')}</th>
                        <th>{__("Week")}</th>
                        <th>{__('Title')}</th>
                    </tr>
                    </thead>
                    <tbody>
                    {foreach $foetus_info as $k => $row}
                        <tr>
                            <td align="center">{$k + 1}</td>
                            <td align="center">{$row['week']}</td>
                            <td><a href="{$system['system_url']}/childinfo/{$child['child_parent_id']}/foetusdevelopments/detail/{$row['foetus_info_id']}">{$row['title']}</a></td>
                        </tr>
                    {/foreach}
                    </tbody>
                </table>
            </div>
        </div>
    {elseif $sub_view == "referion"}
        <div class="panel-body with-table form-horizontal">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover js_dataTable">
                    <thead>
                    <tr>
                        <th colspan="3">{__("Referion information")} ({count($foetus_info)})</th>
                    </tr>
                    <tr>
                        <th>{__('No.')}</th>
                        <th>{__("Week")}</th>
                        <th>{__('Title')}</th>
                    </tr>
                    </thead>
                    <tbody>
                    {foreach $foetus_info as $k => $row}
                        <tr>
                            <td align="center">{$k + 1}</td>
                            <td align="center">{$row['week']}</td>
                            <td><a href="{$system['system_url']}/childinfo/{$child['child_parent_id']}/foetusdevelopments/detail/{$row['foetus_info_id']}">{$row['title']}</a></td>
                        </tr>
                    {/foreach}
                    </tbody>
                </table>
            </div>
        </div>
    {elseif $sub_view == "detail"}
        <div class="panel-body with-table form-horizontal">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <tbody>
                    <tr>
                        <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Type")}</strong></td>
                        <td>{if $data['type'] == 1} {__("Pregnancy check")} {else} {__("Information")} {/if}</td>
                    </tr>
                    <tr>
                        <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Week")}</strong></td>
                        <td>{$data['week']}</td>
                    </tr>
                    <tr>
                        <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Title")}</strong></td>
                        <td>{$data['title']}</td>
                    </tr>
                    {if $data['content_type'] != 2}
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Content")}</strong></td>
                            <td>{$data['content']}</td>
                        </tr>
                    {/if}
                    {if $data['content_type'] != 1}
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Link")}</strong></td>
                            <td><a href = "{$data['link']}" target="_blank"><strong>{__("View detail")}</strong></a></td>
                        </tr>
                    {/if}
                    </tbody>
                </table>
            </div>
        </div>
    {/if}
</div>