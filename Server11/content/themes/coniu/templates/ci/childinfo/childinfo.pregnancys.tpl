<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <i class="fa fa-share fa-fw fa-lg pr10"></i>
        {__('Pregnancy information')}
    </div>
    {if $sub_view == ""}
        <div class="panel-body with-table form-horizontal">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover js_dataTable">
                    <thead>
                    <tr>
                        <th colspan="3">{__("Pregnancy information")}</th>
                    </tr>
                    <tr>
                        <th>{__('No.')}</th>
                        <th>{__('Week')}</th>
                        <th>{__('Title')}</th>
                    </tr>
                    </thead>
                    <tbody>
                    {foreach $pregnancys as $k => $row}
                        <tr>
                            <td align="center">{$k + 1}</td>
                            <td align="center">{__("Week")} {$row['week']}</td>
                            <td>
                                <a href="{$row['link']}" target="_blank">{$row['title']}</a>
                            </td>
                        </tr>
                    {/foreach}
                    </tbody>
                </table>
            </div>
        </div>
    {/if}
</div>