<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <div class="pull-right flip">
            {if $sub_view == ""}
                <a href="{$system['system_url']}/childinfo/{$child['child_parent_id']}/childdetail/edit" class="btn btn-default">
                    <i class="fa fas fa-edit"></i> {__("Edit")}
                </a>
                <a href="#" class="btn btn-danger js_child-delete" data-id="{$child['child_parent_id']}">
                    <i class="fa fa-eraser"></i> {__("Delete student")}
                </a>
            {elseif $sub_view == "edit"}
                <a href="{$system['system_url']}/childinfo/{$child['child_parent_id']}/childdetail" class="btn btn-default">
                    <i class="fa fa-info"></i> {__("Detail")}
                </a>
            {/if}
        </div>
        <i class="fa fa-child fa-fw fa-lg pr10"></i>
        {__("Child")} &rsaquo; {$child['child_name']}
        {if $sub_view == "edit"}
            &rsaquo; {__('Edit')}
        {elseif $sub_view == ""}
            &rsaquo; {__('Detail')}
        {/if}
    </div>
    {if $sub_view == ""}
        <div class="panel-body with-table">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <tbody>
                    <tr>
                        <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Full name")}</strong></td>
                        <td>{$child['child_name']}</td>
                    </tr>
                    <tr>
                        <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Nickname")}</strong></td>
                        <td>{$child['child_nickname']}</td>
                    </tr>
                    <tr>
                        <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Student code")}</strong></td>
                        <td>{$child['child_code']}</td>
                    </tr>
                    {if $child['is_pregnant'] == 0}
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Gender")}</strong></td>
                            <td>{if $child['gender'] == $smarty.const.MALE}{__("Male")}{else}{__("Female")}{/if}</td>
                        </tr>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Birthdate")}</strong></td>
                            <td>{$child['birthday']}</td>
                        </tr>
                    {else}
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Pregnant week")}</strong></td>
                            <td>{if $child['pregnant_week'] > 0}{$child['pregnant_week']}{/if}</td>
                        </tr>
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Due date of childbearing")}</strong></td>
                            <td>{$child['due_date_of_childbearing']}</td>
                        </tr>
                    {/if}
                    <tr>
                        <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Parent")}</strong></td>
                        <td>
                            {foreach $childInfo['parent'] as $_user}
                                <span class="name js_user-popover" data-uid="{$_user['user_id']}">
                                        <a href="{$system['system_url']}/{$_user['user_name']}">{$_user['user_fullname']}</a>
                                    </span>
                                {if $_user['user_id'] != $user->_data['user_id']}
                                    <a href="#" class="far fa-comments fa-lg js_chat-start" data-name="{$_user['user_fullname']}" data-uid="{$_user['user_id']}"></a>
                                {/if}
                                <br/>
                            {/foreach}
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Mother's name")}</strong></td>
                        <td>{$child['parent_name']}</td>
                    </tr>
                    <tr>
                        <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Telephone")}</strong></td>
                        <td>{$child['parent_phone']}</td>
                    </tr>
                    <tr>
                        <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Job")}</strong></td>
                        <td>{$child['parent_job']}</td>
                    </tr>
                    <tr>
                        <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Father's name")}</strong></td>
                        <td>{$child['parent_name_dad']}</td>
                    </tr>
                    <tr>
                        <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Telephone")}</strong></td>
                        <td>{$child['parent_phone_dad']}</td>
                    </tr>
                    <tr>
                        <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Job")}</strong></td>
                        <td>{$child['parent_job_dad']}</td>
                    </tr>
                    <tr>
                        <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Parent email")}</strong></td>
                        <td>{$child['parent_email']}</td>
                    </tr>
                    <tr>
                        <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Address")}</strong></td>
                        <td>{$child['address']}</td>
                    </tr>
                    {if !$child['is_pregnant']}
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Study start date")}</strong></td>
                            {if isset($childInfo['school'])}
                                <td>{$child['begin_at']}</td>
                            {else}
                                <td>{__("Student not in school")}</td>
                            {/if}
                        </tr>
                    {/if}
                    <tr>
                        <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Description")}</strong></td>
                        <td>{$child['description']}</td>
                    </tr>
                    {if !$child['is_pregnant']}
                        <tr>
                            <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("Child picture")}</strong></td>
                            <td>
                                <a href="{$child['child_picture']}" target="_blank"> <img src="{$child['child_picture']}" class="img-responsive">
                                </a>
                            </td>
                        </tr>
                    {/if}
                    </tbody>
                </table>
            </div>
            <div class="form-group">
                <div class="col-sm-9 text-left pl0">
                    <a href="{$system['system_url']}/childinfo/{$child['child_parent_id']}/childdetail/edit" class="btn btn-default"><i class="fas fa-edit"></i> {__("Edit")}</a>
                </div>
            </div>
        </div>
    {elseif $sub_view == "edit"}
        <div class="panel-body kg-main">
            <form class="form-horizontal" enctype="multipart/form-data" method="post" id="edit_child_parent">
                <input type="hidden" name="school_username" value="{$username}"/>
                <input type="hidden" name="child_parent_id" value="{$child['child_parent_id']}"/>
                <input type="hidden" name="child_id" value="{$child['child_id']}"/>
                <input type="hidden" name="do" value="edit"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Student code")}</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="child_code" value="{$child['child_code']}" disabled>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Full name")} (*)</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="last_name" value="{$child['last_name']}" placeholder="{__("Last name")}" required maxlength="34" {if $child['school_id'] != 0}readonly{/if}>
                    </div>
                    <div class="col-sm-2">
                        <input type="text" class="form-control" name="first_name" value="{$child['first_name']}" placeholder="{__("First name")}" required maxlength="15" {if $child['school_id'] != 0}readonly{/if}>
                    </div>
                </div>
                <div class = "form-group">
                    <label class="col-sm-3 control-label text-left">{__("Nickname")}</label>
                    <div class="col-sm-5">
                        <input type = "text" class="form-control" name="nickname" id="nickname" placeholder="{__("Nickname")}" maxlength="50" value = "{$child['child_nickname']}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Pregnant")}</label>
                    <div class="col-sm-9">
                        <div class="onoffswitch">
                            <input type="checkbox" name="child_pregnant" class="onoffswitch-checkbox" id="child_pregnant" {if $child['is_pregnant']}checked{/if}>
                            <label class="onoffswitch-label" for="child_pregnant"></label>
                        </div>
                    </div>
                </div>
                <div id = "not_pregnant" {if $child['is_pregnant'] == 1} class = "x-hidden"{/if}>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Gender")} (*)</label>
                        <div class="col-sm-3">
                            <select name="gender" id="gender" class="form-control" {if $child['school_id'] != 0}disabled{/if}>
                                <option value="{$smarty.const.MALE}" {if $child['gender'] == $smarty.const.MALE}selected{/if}>{__("Male")}</option>
                                <option value="{$smarty.const.FEMALE}" {if $child['gender'] == $smarty.const.FEMALE}selected{/if}>{__("Female")}</option>
                            </select>
                            {if $child['school_id'] != 0}
                                <input type="text" value="{$child['gender']}" name="gender" class="hidden">
                            {/if}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-left">{__("Birthdate")} (*)</label>
                        <div class='col-sm-3'>
                            <div class='input-group date' id='birthdate_picker'>
                                <input type='text' name="birthday" value = "{$child['birthday']}" id="birthday" class="form-control" placeholder="{__("Birthdate")} (*)" {if $child['school_id'] != 0}readonly{/if}/>
                                <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                            </div>
                        </div>
                    </div>
                    <input type = "hidden" id = "is_file" name = "is_file" value = "1">
                    <div class="form-group" id = "file_old">
                        <label class="rp-file col-sm-3 control-label text-left" style="padding-top: 0px">{__("Avatar")}</label>
                        <div class="col-sm-6">
                            {if !is_empty($child['child_picture'])}
                                <a href="{$child['child_picture']}" target="_blank"><img src = "{$child['child_picture']}" class = "img-responsive"></a>
                            {else} {__('No Avatar')}
                            {/if}
                        </div>
                    </div>
                    <div class = "form-group">
                        <label class="control-label col-sm-3">
                            {if is_empty($child['child_picture'])}
                                {__("Choose Avatar")}
                            {else}
                                {__("Change Avatar")}
                            {/if}
                        </label>
                        <div class = "col-sm-6">
                            <input type="file" name="file" id="file"/>
                        </div>
                        <a class = "delete_image btn btn-danger btn-xs text-left">{__('Delete')}</a>
                    </div>
                    {*<div class="form-group">*}
                        {*<div class="file-loading">*}
                            {*<input id="file-1" name = "file" type="file" multiple class="file" data-overwrite-initial="false" data-max-file-count="1">*}
                        {*</div>*}
                    {*</div>*}
                    {*<input type="file" multiple class="file_input" name="file">*}
                    {*<p class="form_p">Drag your files here or click in this area.</p>*}
                    {*<input name="files" type="file" accept=".xlsx,.xls,image/*,.doc,audio/*,.docx,video/*,.ppt,.pptx,.txt,.pdf" multiple>*}
                </div>
                <div {if $child['is_pregnant'] == 0}class = "x-hidden"{/if} id = "is_pregnant">
                    <div class = "form-group">
                        <label class = "col-sm-3 control-label text-left">{__("Time")}</label>
                        <div class="col-sm-2">
                            <input type = "number" name = "pregnant_week" value = "{$child['pregnant_week']}" id = "pregnant_week" class = "form-control" placeholder="{__("Week")}">
                        </div>
                        <label class = "col-sm-1 control-label text-left">{__("Week")}(*)</label>
                    </div>
                    <div class = "form-group">
                        <label class="col-sm-3 control-label text-left">{__("Due date of childbearing")}</label>
                        <div class='col-sm-6'>
                            <div class='input-group date' id='due_date_picker'>
                                <input type='text' name="due_date_of_childbearing" id="due_date_of_childbearing" value = "{$child['due_date_of_childbearing']}" class="form-control" placeholder="{__("Due date of childbearing")}"/>
                                <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Parent")}</label>
                    <div class="col-sm-7">
                        <input name="search-parent" id="search-parent" type="text" class="form-control" placeholder="{__("Enter username, email or fullname to search")}" autocomplete="off">
                        <div id="search-parent-results" class="dropdown-menu dropdown-widget dropdown-search">
                            <div class="dropdown-widget-header">
                                {__("Search Results")}
                            </div>
                            <div class="dropdown-widget-body">
                                <div class="loader loader_small ptb10"></div>
                            </div>
                        </div>
                        <div>{__("Enter at least 4 characters")}.</div>
                        <br/>
                        <div class="col-sm-9" id="parent_list" name="parent_list">
                            {if count($childInfo['parent']) > 0}
                                {include file='ci/ajax.parentlist.tpl' results=$childInfo['parent']}
                            {else}
                                {__("No parent")}
                            {/if}
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Blood type")}</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="blood_type" name="blood_type" placeholder="{__("Blood type")}" value="{convertText4Web($child['blood_type'])}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Hobby")}</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="hobby" name="hobby" placeholder="{__("Hobby")}" value="{convertText4Web($child['hobby'])}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Allergy")}</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="allergy" name="allergy" placeholder="{__("Allergy")}" value="{convertText4Web($child['allergy'])}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Mother's name")}</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="parent_name" name="parent_name" placeholder="{__("Mother's name")}" value="{convertText4Web($child['parent_name'])}" maxlength="50">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Telephone")}</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="parent_phone" name="parent_phone" value="{$child['parent_phone']}" placeholder="{__("Telephone")}" maxlength="50">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Job")}</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="parent_job" name="parent_job" value="{$child['parent_job']}" placeholder="{__("Job")}" maxlength="500">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Father's name")}</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="parent_name_dad" name="parent_name_dad" placeholder="{__("Father's name")}" value="{convertText4Web($child['parent_name_dad'])}" maxlength="50">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Telephone")}</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="parent_phone_dad" name="parent_phone_dad" value="{$child['parent_phone_dad']}" placeholder="{__("Telephone")}" maxlength="50">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Job")}</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="parent_job_dad" name="parent_job_dad" value="{$child['parent_job_dad']}" placeholder="{__("Job")}" maxlength="500">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Parent email")}</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="parent_email" name="parent_email" value="{$child['parent_email']}" placeholder="{__("Parent email")}" maxlength="100">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Address")}</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="address" value="{$child['address']}" placeholder="{__("Address")}" maxlength="250">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Description")}</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="description" placeholder="{__("Write about your child...")}" maxlength="300">{$child['description']}</textarea>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->
                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    {/if}
</div>
