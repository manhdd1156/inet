<div class="row" align="center">
    <button class="js_hight-chart-show btn btn-default selected_chart">{__("Height")}</button>
    <button class="js_weight-chart-show btn btn-default">{__("Weight")}</button>
    <button class="js_bmi-chart-show btn btn-default">{__("BMI")}</button>
</div>

<script type="text/javascript" src="{$system['cdn_url']}/includes/assets/js/chart/Chart.bundle.min.js"></script>
<script type="text/javascript" src="{$system['cdn_url']}/includes/assets/js/chart/Chart.bundle.js"></script>
<script type="text/javascript" src="{$system['cdn_url']}/includes/assets/js/chart/utils.js"></script>

<div id="hight_chart">
    <div class="content">
        <div class="wrapper">
            <canvas id="chart-height"></canvas>
        </div>
    </div>
</div>
<div id="weight_chart">
    <div class="content">
        <div class="wrapper">
            <canvas id="chart-weight"></canvas>
        </div>
    </div>
</div>

<div id="bmi_chart">
    <div class="content">
        <div class="wrapper">
            <canvas id="chart-bmi"></canvas>
        </div>
    </div>
</div>
<script>
    function stateChange() {
        setTimeout(function () {
            var presets = window.chartColors;
            var utils = Samples.utils;
            var data_height = {
                labels: [{$label}],
                datasets: [{
                    backgroundColor: presets.blue,
                    borderColor: presets.blue,
                    data: [{$heightForChartChartJS}],
                    hidden: false,
                    label: __["Height"],
                    fill: false,
                    pointRadius: 3
                }, {
                    backgroundColor: utils.transparentize(presets.orange),
                    // borderColor: presets.orange,
                    data: [{$lowestForChartChartJS}],
                    hidden: false,
                    label: __["Below standard"]
                    // fill: 'end'
                }, {
                    backgroundColor: utils.transparentize(presets.red),
                    // borderColor: presets.red,
                    data: [{$highestForChartChartJS}],
                    hidden: false,
                    label: __["Above standard"],
                    fill: 'end'
                }, {
                    backgroundColor: presets.white,
                    // borderColor: presets.red,
                    data: [],
                    hidden: false,
                    label: __["Standard"],
                    fill: false
                }]
            };

            var data_weight = {
                labels: [{$label}],
                datasets: [{
                    backgroundColor: presets.blue,
                    borderColor: presets.blue,
                    data: [{$weightForChartChartJS}],
                    hidden: false,
                    label: __["Weight"],
                    fill: false,
                    pointRadius: 3
                }, {
                    backgroundColor: utils.transparentize(presets.orange),
                    // borderColor: presets.orange,
                    data: [{$lowestWeightForChartChartJS}],
                    hidden: false,
                    label: __["Below standard"]
                    // fill: 'end'
                }, {
                    backgroundColor: utils.transparentize(presets.red),
                    // borderColor: presets.red,
                    data: [{$highestWeightForChartChartJS}],
                    hidden: false,
                    label: __["Above standard"],
                    fill: 'end'
                }, {
                    backgroundColor: presets.white,
                    // borderColor: presets.red,
                    data: [],
                    hidden: false,
                    label: __["Standard"],
                    fill: false
                }]
            };
            var data_bmi = {
                labels: [{$label}],
                datasets: [{
                    backgroundColor: utils.transparentize(presets.white),
                    borderColor: presets.blue,
                    data: [{$bmiForChartChartJS}],
                    hidden: false,
                    label: 'BMI',
                    fill: false,
                    pointRadius: 3
                }]
            };
            var options = {
                maintainAspectRatio: false,
                spanGaps: true,
                elements: {
                    line: {
                        tension: 0.000001
                    },
                    point:{
                        radius: 0
                    }
                },
                scales: {
                    yAxes: [{
                        stacked: false
                    }]
                    // xAxes: [{
                    //     // position: 'bottom',
                    //     ticks: {
                    //         autoSkip: false
                    //     }
                    // }]
                },
                plugins: {
                    filler: {
                        propagate: false
                    },
                    samples_filler_analyser: {
                        target: 'chart-analyser'
                    }
                }
            };

            var chart_height = new Chart('chart-height', {
                type: 'line',
                data: data_height,
                options: options
            });

            var chart_weight = new Chart('chart-weight', {
                type: 'line',
                data: data_weight,
                options: options
            });

            var chart_bmi = new Chart('chart-bmi', {
                type: 'line',
                data: data_bmi,
                options: options
            });
        }, 1000);
    }
    stateChange();
</script>

<script>
    function stateChange() {
        setTimeout(function () {
            $("#weight_chart").hide();
            $("#bmi_chart").hide();
        }, 1000);
    }
    stateChange();
</script>
<br/>
<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover js_dataTable">
        <thead>
        <tr>
            <th colspan="6">{__("Health information")}&nbsp;({$growth|count})</th>
        </tr>
        <tr>
            <th>{__('No.')}</th>
            <th>{__('Picture')}</th>
            <th>{__("Date")}</th>
            <th>{__('Height')} (cm)</th>
            <th>{__("Weight")} (kg)</th>
            <th>{__("Actions")}</th>
        </tr>
        </thead>
        <tbody>
        {foreach $growth as $k => $row}
            <tr>
                <td align="center">{$k + 1}</td>
                <td align="center">
                    {if !is_empty($row['source_file'])}
                        <div class="news_health">
                            <div class="article_health">
                                <div class="thumb_health" style="background-image: url('{$row['source_file']}')"></div>
                            </div>
                        </div>
                    {else}
                        {__("No picture")}
                    {/if}
                </td>
                <td align="center"><a href = "{$system['system_url']}/childinfo/{$child['child_parent_id']}/health/editgrowth/{$row['child_growth_id']}">{$row['recorded_at']}</a></td>
                <td align="center">{$row['height']}</td>
                <td align="center">{$row['weight']}</td>
                <td align="center">
                    <a href ="{$system['system_url']}/childinfo/{$child['child_parent_id']}/health/editgrowth/{$row['child_growth_id']}" class = "btn btn-xs btn-default">
                        {__("Edit")}
                    </a>
                    <a href ="#" class="btn-danger btn btn-xs js_child-health-delete" data-id="{$row['child_growth_id']}" data-child="{$child['child_parent_id']}" data-handle="delete_growth">
                        {__("Delete")}
                    </a>
                </td>
            </tr>
        {/foreach}
        </tbody>
    </table>
</div>