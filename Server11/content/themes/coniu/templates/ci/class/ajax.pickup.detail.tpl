<div class="panel-body with-table">
    <form class="js_ajax-forms form-horizontal" data-url="ci/bo/class/boclass_pickup.php">
        <input type="hidden" id="username" name="username" value="{$username}"/>
        <input type="hidden" id="pickup_date" name="pickup_date" value="{$today}"/>
        <input type="hidden" id="pickup_id" name="pickup_id" value="{$pickup['pickup_id']}"/>
        <input type="hidden" name="callback" value="detail"/>
        <input type="hidden" name="do" value="save"/>
        <div id="time_option" class="x-hidden" data-value="{$time_option}"></div>

        <div class="text-center">
            <strong><label style="font-size: 15px">{__("Late pickup on")|upper}: {$today}</label></strong><br>
            {if !is_empty($pickup['class_name'])}
                <strong><label style="font-size: 15px">{__("Class")}: {$pickup['class_name']}</label></strong><br><br>
            {else}
                <br>
            {/if}
        </div>
        <div class="col-sm-12">
            <label class="col-sm-6 text-right" style="font-size: 15px">{__("Late pickup teachers")}:</label>
            {if $pickup['assign']|count > 0}
                <div class="col-sm-6">
                    {foreach $pickup['assign'] as $assign}
                        <strong>
                            <label>&#9679;&#09;{$assign['user_fullname']}</label>
                        </strong>
                        <br>
                    {/foreach}
                </div>
            {else}
                <div class="col-sm-6 text-left" style="font-size: 15px;color: red">{__("Not assigned")}</div>
            {/if}
        </div>

        <div>
            <strong>{__("Total student")}: <font color="red">{$children|count}</font> |
                {__("Picked up")}: <font color="red">{$child_count}</font>
            </strong>
        </div>
        {if $is_today || $allow_teacher_edit_pickup_before}
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th width="5%">#</th>
                        <th width="25%">{__("Student name")}</th>
                        <th width="20%">{__("Birthdate")}</th>
                        <th width="20%">{__("Class")}</th>
                        <th width="15%">{__("Status")}</th>
                    </tr>
                    </thead>
                    <tbody>
                    {$idx = 1}
                    {foreach $children as $child}
                        <input type="hidden" name="child[]" value="{$child['child_id']}"/>
                        <tr {if $child['child_status'] == 0} class="row-disable"{/if}>
                            <td align="center"><strong>{$idx}</strong></td>
                            <td><strong>{$child['child_name']}</strong></td>
                            <td align="center">{$child['birthday']}</td>
                            <td align="center">{$child['group_title']}</td>
                            <td rowspan="4" align="center">
                                {if is_empty($child['pickup_at']) }
                                    {if $is_today}
                                        <a class="btn btn-xs btn-default js_class-pickup" data-handle="pickup"
                                           data-username="{$username}" data-child="{$child['child_id']}"
                                           data-pickup="{$child['pickup_id']}">
                                            {__("Pickup")}
                                        </a>
                                    {/if}
                                {/if}
                                <a class="btn btn-xs btn-danger js_class-pickup" data-handle="cancel"
                                   data-username="{$username}" data-child="{$child['child_id']}"
                                   data-pickup="{$child['pickup_id']}">
                                    {__("Cancel")}
                                </a>
                            </td>
                        </tr>
                        <tr {if $child['child_status'] == 0} class="row-disable"{/if}>
                            <td colspan="1" rowspan="3"></td>
                            <td colspan="3" style="padding-left: 50px">
                                <div class="col-sm-4 pt3">
                                    {__("Pickup at")}
                                    : &nbsp;
                                </div>
                                {if !is_empty($child['pickup_at']) }
                                    <input type='text' name="pickup_time[]" class="pickup_time" id="pickup_time_{$child['child_id']}" value="{$child['pickup_at']}" style="width: 70px; font-weight: bold; text-align: center"/>
                                    {*{if !is_empty($child['giveback_person'])}
                                        &nbsp;-&nbsp;
                                        <strong>
                                            <font color="#228b22">{$child['giveback_person']}</font>
                                        </strong>
                                    {/if}*}
                                {else}
                                    <input type='text' name="pickup_time[]" class="pickup_time" id="pickup_time_{$child['child_id']}" value="" style="width: 70px; font-weight: bold;color: red; text-align: center"/>
                                    {*&nbsp;-&nbsp;
                                    <strong>
                                        <font color="#ff4500">{__("Chưa có thông tin")}</font>
                                    </strong>*}
                                {/if}
                                <input type="hidden" min="0"
                                       id="total_pickup_{$child['child_id']}"
                                       data-child="{$child['child_id']}"
                                       name="pickup_fee[]" value="{$child['late_pickup_fee']}"/>
                            </td>
                        </tr>
                        <tr {if $child['child_status'] == 0} class="row-disable"{/if}>
                            <td colspan="3" align="left" style="padding-left: 50px">
                                {if !empty($child['services'])}
                                    <div class="col-sm-4 pt3">
                                        {__("Using service")}: &nbsp;
                                    </div>
                                    {foreach $child['services'] as $service}
                                        <input type="checkbox" class="pickup_service_fee" name="service_{$child['child_id']}[]"
                                               id="pickup_service_fee_{$child['child_id']}_{$service['service_id']}"
                                               data-child="{$child['child_id']}"
                                               data-service="{$service['service_id']}"
                                               data-fee="{$service['price']}"
                                               value="{$service['service_id']}" {if !is_null($service['using_at']) } checked {/if}/>
                                        {$service['service_name']}
                                        {*{if !is_null($service['using_at'])}
                                            <input type="hidden" name="service_used_{$child['child_id']}[]"
                                                   value="{$service['service_id']}"/>
                                        {/if}*}
                                        &nbsp;&nbsp;&nbsp;
                                    {/foreach}
                                {else}
                                    &nbsp;
                                {/if}
                                {*<input type="hidden" min="0"
                                       name="service_fee[]"
                                       id="total_service_{$child['child_id']}"
                                       data-child="{$child['child_id']}"
                                       value="{$child['using_service_fee']}"/>*}
                            </td>

                        </tr>

                        <tr {if $child['child_status'] == 0} class="row-disable"{/if}>
                            <td colspan="3" style="padding-left: 50px">
                                <div class="col-sm-4 pt3">
                                    {__("Note")} : &nbsp;
                                </div>
                                <input type='text' name="pickup_note[]"  value="{$child['description']}" style="width: 60%; color: blue"/>
                                <input type="hidden" min="0"
                                       id="total_{$child['child_id']}"
                                       name="total_child[]"
                                       value="{$child['total_amount']}"/>
                            </td>
                        </tr>
                        {$idx = $idx + 1}
                    {/foreach}
                    {if $children|count == 0}
                        <tr class="odd">
                            <td valign="top" align="center" colspan="5" class="dataTables_empty">
                                {__("No data available in table")}
                            </td>
                        </tr>
                    {/if}
                    <input type="hidden" id="total" name="total" value="{$pickup['total']}"/>
                    </tbody>
                </table>
            </div>

            <div class="form-group">
                <div class="col-sm-9 pl10">
                    <button type="submit"
                            class="btn btn-primary {if $children|count == 0} x-hidden {/if}">{__("Save")}</button>
                </div>
            </div>
            <!-- success -->
            <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
            <!-- success -->

            <!-- error -->
            <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
            <!-- error -->

        {else}

            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th width="10%">#</th>
                        <th width="30%">{__("Student name")}</th>
                        <th width="30%">{__("Birthdate")}</th>
                        <th width="30%">{__("Class")}</th>
                    </tr>
                    </thead>
                    <tbody>
                    {$idx = 1}
                    {foreach $children as $child}
                        <input type="hidden" name="child[]" value="{$child['child_id']}"/>
                        <tr {if $child['child_status'] == 0} class="row-disable"{/if}>
                            <td align="center"><strong>{$idx}</strong></td>
                            <td><strong>{$child['child_name']}</strong></td>
                            <td align="center">{$child['birthday']}</td>
                            <td align="center">{$child['group_title']}</td>
                        </tr>
                        <tr {if $child['child_status'] == 0} class="row-disable"{/if}>
                            <td colspan="1" rowspan="3"></td>
                            <td colspan="3" style="padding-left: 50px">
                                <div class="col-sm-4 pt3">
                                    {__("Pickup at")}
                                    : &nbsp;
                                </div>
                                {if !is_empty($child['pickup_at']) }
                                    <input type='text'  value="{$child['pickup_at']}" style="width: 70px; font-weight: bold; text-align: center" readonly/>
                                    {*{if !is_empty($child['giveback_person'])}
                                        &nbsp;-&nbsp;
                                        <strong>
                                            <font color="#228b22">{$child['giveback_person']}</font>
                                        </strong>
                                    {/if}*}
                                {else}
                                    <input type='text' value="" style="width: 70px" readonly/>
                                    {*&nbsp;-&nbsp;
                                    <strong>
                                        <font color="#ff4500">{__("Chưa có thông tin")}</font>
                                    </strong>*}
                                {/if}
                            </td>
                        </tr>
                        <tr {if $child['child_status'] == 0} class="row-disable"{/if}>
                            <td colspan="3" align="left" style="padding-left: 50px">
                                {if !empty($child['services'])}
                                    <div class="col-sm-4 pt3">
                                        {__("Using service")}: &nbsp;
                                    </div>
                                    {foreach $child['services'] as $service}
                                        <input type="checkbox" {if !is_null($service['using_at']) } checked {/if} onclick="return false;"/>
                                        {$service['service_name']}
                                        &nbsp;&nbsp;&nbsp;
                                    {/foreach}
                                {else}
                                    &nbsp;
                                {/if}
                            </td>

                        </tr>

                        <tr {if $child['child_status'] == 0} class="row-disable"{/if}>
                            <td colspan="3" style="padding-left: 50px">
                                {*{if isset($child['type']) }
                                    <strong>{__("Người đăng ký")}</strong>: &nbsp;
                                    <strong>
                                        {if $child['type'] == 2}
                                        <font color="#ff4500">{__("Phụ huynh")} -
                                            {else}
                                            <font color="#228b22">
                                                {/if}
                                                {$child['subscriber']}</font>
                                    </strong>
                                    {if !is_null($child['register'])}
                                        <br/>
                                        <strong style="color: blue">{__("Dịch vụ đăng ký")}</strong> : &nbsp;
                                        {if $child['register']['service']|count > 0 }
                                            {foreach $child['register']['service'] as $srv}
                                                &#9679;&#09;{$srv['service_name']}
                                            {/foreach}
                                        {else}
                                            <strong style="color: #ff4500">{__("Không đăng ký")}</strong>
                                        {/if}
                                        <br/>
                                        <strong style="color: blue">{__("Ghi chú")}</strong> : &nbsp; {$child['register']['description']}
                                    {/if}
                                {else}
                                    <strong>&nbsp;</strong>
                                {/if}*}
                                <div class="col-sm-4 pt3">
                                    {__("Note")} : &nbsp;
                                </div>
                                <input type='text' name="pickup_note[]"  value="{$child['description']}" style="width: 60%; color: blue"/>
                            </td>
                        </tr>
                        {$idx = $idx + 1}
                    {/foreach}
                    {if $children|count == 0}
                        <tr class="odd">
                            <td valign="top" align="center" colspan="5" class="dataTables_empty">
                                {__("No data available in table")}
                            </td>
                        </tr>
                    {/if}
                    </tbody>
                </table>
            </div>

        {/if}

    </form>
</div>