<strong>{__("Children list")}&nbsp;({$result|count} {__("Children")})</strong>
<input type="hidden" name="hid_total" id="hid_total" value="{$result['total']}">
<table class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th>{__("#")}</th>
        <th>{__("Full name")}</th>
        <th>{__("Gender")}</th>
        <th>{__("Birthdate")}</th>
        <th>{__("Parent phone")}</th>
        <th>{__("Parent")}</th>
        <th>{__("Actions")}</th>
    </tr>
    </thead>
    <tbody>
    {$idx = 1}
    {foreach $result as $row}
        <tr>
            <td align="center" class="align-middle">{$idx}</td>
            {*<td>{$row['child_code']}</td>*}
            <td class="align-middle"><a href="{$system['system_url']}/class/{$username}/children/detail/{$row['child_id']}">{$row['child_name']}</a></td>
            <td align="center" class="align-middle">{if $row['gender'] == $smarty.const.MALE}{__("Male")}{else}{__("Female")}{/if}</td>
            <td class="align-middle">{$row['birthday']}</td>
            <td class="align-middle">{$row['parent_phone']}</td>
            <td class="align-middle">
                {if count($row['parent']) == 0}
                    {__("No parent")}
                {else}
                    {foreach $row['parent'] as $_user}
                        <span class="name js_user-popover" data-uid="{$_user['user_id']}">
                            <a href="{$system['system_url']}/{$_user['user_name']}">{$_user['user_fullname']}</a>
                        </span>
                        {if $_user['user_id'] != $user->_data['user_id']}
                            <a href="#" class="far fa-comments fa-lg js_chat-start" data-name="{$_user['user_fullname']}" data-uid="{$_user['user_id']}"></a>
                        {/if}
                        <br/>
                    {/foreach}
                {/if}
            </td>

            <td align="center" style = "padding: 8px 1px; vertical-align: middle">
                <a href="{$system['system_url']}/class/{$username}/children/reedit/{$row['child_id']}" class="btn btn-xs btn-default">{__("Edit")}</a>
                <a href = "#" class="btn btn-xs btn-danger js_class-child-edit" data-handle="delete_edit" data-username="{$username}" data-id="{$row['child_id']}">{__("Delete")}</a>
            </td>
        </tr>
        {$idx = $idx + 1}
    {/foreach}

    {if $result|count == 0}
        <tr class="odd">
            <td valign="top" align="center" colspan="7" class="dataTables_empty">
                {__("No data available in table")}
            </td>
        </tr>
    {/if}
    </tbody>
</table>