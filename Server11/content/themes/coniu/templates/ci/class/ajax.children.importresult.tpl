{if $sheet['error'] == 1}
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr><th>{__("Detail results")}</th></tr>
        </thead>
        <tbody>
            <tr><td><div>{$sheet['message']}</div></td></tr>
        </tbody>
    </table>
{else}
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr><th colspan="5">{__("Detail results")}&nbsp;({$sheet['child_list']|count})</th></tr>
            <tr>
                <th>#</th>
                <th>{__("Child")}</th>
                <th>{__("Birthday")}</th>
                <th>{__("Parent email")}</th>
                <th>{__("Message")}</th>
            </tr>
        </thead>
        <tbody>
        {$idx = 1}
            {foreach $sheet['child_list'] as $child}
                <tr>
                    <td align="center" style="vertical-align: middle">
                        {$idx}
                    </td>
                    <td>{$child['child_name']}</td>
                    <td>{$child['birthday']}</td>
                    <td>{$child['parent_email']}</td>
                    <td>{if $child['error'] == 0}{__("Create student successfull")}&nbsp;|&nbsp;{$child['create_account_result']} {else} {$child['message']} {/if}</td>
                </tr>
                {$idx = $idx + 1}
            {/foreach}
        </tbody>
    </table>
{/if}