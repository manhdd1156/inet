{if $schoolConfig['attendance_use_leave_early'] || $schoolConfig['attendance_use_come_late'] || $schoolConfig['attendance_absence_no_reason']}
    <div class="table-responsive" id="getFixed">
        <table class="table table-striped table-bordered" style="z-index: 1">
            <tbody>
            <tr>
                <td align="right"><strong>{__("Symbol")}:</strong></td>
                {if $schoolConfig['attendance_use_come_late']}
                    <td bgcolor="#6495ed" align="center"><i class="fa fa-check" aria-hidden="true"></i></td>
                    <td align="left">{__("Come late")}</td>
                {/if}
                {if $schoolConfig['attendance_use_leave_early']}
                    <td bgcolor="#008b8b" align="center"><i class="fa fa-check" aria-hidden="true"></i></td>
                    <td align="left">{__("Leave early")}</td>
                {/if}
                <td align="center"><i class="fa fa-check" aria-hidden="true"></i></td>
                <td align="left">{__("Present")}</td>

                {if $schoolConfig['attendance_absence_no_reason']}
                    <td bgcolor="#ff1493" align="center"><i class="fa fa-times" aria-hidden="true"></i></td>
                    <td align="left">{__("Without permission")}</td>
                {/if}

                <td bgcolor="#deb887" align="center"><i class="fa fa-times" aria-hidden="true"></i></td>
                <td align="left">{__("With permission")}</td>
            </tr>
            </tbody>
        </table>
    </div>
{/if}
{*<div><strong>{__("The whole class attendance")}</strong></div>*}
<div style="z-index: 2">
    <div><strong>{__("The whole class attendance")}</strong></div>
    <div id="table_button">
        <button id="left">&larr;</button>
        <button id="right">&rarr;</button>
    </div>
</div>
<div class="table-responsive" id="example">
    <table class="table table-striped table-bordered table-pinned" style="z-index: 1">
        <thead>
        <tr>
            <th class="pinned">#</th>
            <th nowrap="true" class="pinned">{__("Full name")}</th>
            {foreach $dates as $date}
                <th align="center">{$date}</th>
            {/foreach}
            <th nowrap="true">{__("Present")}</th>
            <th>{__("Absence")}</th>
        </tr>
        </thead>
        <tbody>
        {$rowIdx = 1}
        {foreach $rows as $row}
            <tr {if $row['child_status'] == 0} class="row-disable"{/if}>
                <td align="center" class="pinned">{$rowIdx}</td>
                <td nowrap="true" class="pinned"><a href="{$system['system_url']}/class/{$username}/attendance/child/{$row['child_id']}">{$row['child_name']}</a></td>

                {$rowIdx = $rowIdx + 1}
                {$presentCnt = 0}
                {$absenceCnt = 0}
                {$dateNow = date('Y-m-d')}
                {foreach $row['cells'] as $cell}
                    {if $cell['is_checked'] == 1}
                        {if !$schoolConfig['allow_teacher_rolls_days_before'] && ($dateNow != $cell['attendance_date'])}
                            <td align="center"
                                    {if $cell['status'] == $smarty.const.ATTENDANCE_COME_LATE}
                                        bgcolor="#6495ed"
                                    {elseif $cell['status'] == $smarty.const.ATTENDANCE_EARLY_LEAVE}
                                        bgcolor="#008b8b"
                                    {elseif $cell['status'] == $smarty.const.ATTENDANCE_ABSENCE_NO_REASON}
                                        bgcolor="#ff1493"
                                    {elseif $cell['status'] == $smarty.const.ATTENDANCE_ABSENCE}
                                        bgcolor="#deb887"
                                    {/if}
                            >
                                {if ($cell['status'] == $smarty.const.ATTENDANCE_PRESENT) || ($cell['status'] == $smarty.const.ATTENDANCE_COME_LATE)
                                || ($cell['status'] == $smarty.const.ATTENDANCE_EARLY_LEAVE)}
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                    {$presentCnt = $presentCnt + 1}
                                {else}
                                    <i class="fa fa-times" aria-hidden="true"></i>
                                    {$absenceCnt = $absenceCnt + 1}
                                {/if}
                            </td>
                        {else}
                            <td align="center" style="vertical-align: middle; padding: 0px; position: relative">
								<link rel="stylesheet" href="{$system['system_url']}/includes/assets/css/font-awesome/css/font-awesome.css">
                                <select data-username="{$username}" data-id="{$cell['attendance_id']}" data-child="{$cell['child_id']}" data-date="{$cell['attendance_date']}" data-status="{$cell['status']}" name="attendance_status" class="js_class-attendance-status" style="border:none; font-family: 'FontAwesome', 'Second Font name'; position: absolute; top: 0; left: 0; width: 100%; height: 100%; {if $cell['status'] == $smarty.const.ATTENDANCE_COME_LATE}
                                        background: #6495ed;
                                {elseif $cell['status'] == $smarty.const.ATTENDANCE_PRESENT}
                                        background: #fff;
                                {elseif $cell['status'] == $smarty.const.ATTENDANCE_EARLY_LEAVE}
                                        background: #008b8b;
                                {elseif $cell['status'] == $smarty.const.ATTENDANCE_ABSENCE_NO_REASON}
                                        background: #ff1493;
                                {elseif $cell['status'] == $smarty.const.ATTENDANCE_ABSENCE}
                                        background: #deb887;
                                {/if};
                                        -webkit-appearance: none;
                                        -moz-appearance: none;
                                        text-indent: 1px;
                                        text-overflow: '';
                                        text-align-last: center;
                                        ">
                                    <option value="{$smarty.const.ATTENDANCE_PRESENT}" style="background: #fff; padding: 5px 0; font-size: 20px;">&#xf00c</option>

                                    {if $schoolConfig['attendance_use_come_late'] || $cell['status'] == $smarty.const.ATTENDANCE_COME_LATE}
                                        <option value="{$smarty.const.ATTENDANCE_COME_LATE}" style="background: #6495ed; padding: 5px 0; font-size: 20px;" {if $cell['status'] == $smarty.const.ATTENDANCE_COME_LATE} selected {/if}>&#xf00c</option>
                                    {/if}
                                    {if $schoolConfig['attendance_use_leave_early'] || $cell['status'] == $smarty.const.ATTENDANCE_EARLY_LEAVE}
                                        <option value="{$smarty.const.ATTENDANCE_EARLY_LEAVE}" style="background: #008b8b; padding: 5px 0; font-size: 20px;" {if $cell['status'] == $smarty.const.ATTENDANCE_EARLY_LEAVE} selected {/if}>&#xf00c</option>
                                    {/if}
                                    {if $schoolConfig['attendance_absence_no_reason'] || $cell['status'] == $smarty.const.ATTENDANCE_ABSENCE_NO_REASON}
                                        <option value="{$smarty.const.ATTENDANCE_ABSENCE_NO_REASON}" style="background: #ff1493; padding: 5px 0; font-size: 20px;" {if $cell['status'] == $smarty.const.ATTENDANCE_ABSENCE_NO_REASON} selected {/if}>&#xf00d</option>
                                    {/if}
                                    <option value="{$smarty.const.ATTENDANCE_ABSENCE}" style="background: #deb887; padding: 5px 0; font-size: 20px;" {if $cell['status'] == $smarty.const.ATTENDANCE_ABSENCE}selected{/if}>&#xf00d</option>
                                </select>
                                {if ($cell['status'] == $smarty.const.ATTENDANCE_PRESENT) || ($cell['status'] == $smarty.const.ATTENDANCE_COME_LATE)
                                || ($cell['status'] == $smarty.const.ATTENDANCE_EARLY_LEAVE)}
                                    {$presentCnt = $presentCnt + 1}
                                {else}
                                    {$absenceCnt = $absenceCnt + 1}
                                {/if}
                            </td>
                        {/if}
                    {else}
                        <td bgcolor="#a9a9a9"></td>
                    {/if}
                {/foreach}
                <td align="center" id="present_{$row['child_id']}" style="color: blue; font-weight: bold">{$presentCnt}</td>
                <td align="center" id="absence_{$row['child_id']}" style="color: red; font-weight: bold">{$absenceCnt}</td>
            </tr>
            {*Sau 15 trẻ thì thêm header cho dễ nhìn*}
            {if $rowIdx % 16 == 0}
                <tr>
                    <th class="pinned">#</th>
                    <th nowrap="true" class="pinned">{__("Full name")}</th>
                    {foreach $dates as $date}
                        <th align="center">{$date}</th>
                    {/foreach}
                    <th nowrap="true">{__("Present")}</th>
                    <th nowrap="true">{__("Absence")}</th>
                </tr>
            {/if}
        {/foreach}
        {if ($rowIdx > 2)}
            <tr>
                <td colspan="2" align="center" class="pinned"><strong>{__('Present total of class')}</strong></td>
                {$presentTotal = 0}
                {foreach $last_rows as $cell}
                    <td id="present_count_{$cell['attendance_id']}" align="center" bgcolor="#ffebcd" style="color: blue; font-weight: bold">
                        {if $cell['is_checked'] == 1}
                            {$cell['present_count']}
                            {$presentTotal = $presentTotal + $cell['present_count']}
                        {/if}
                    </td>
                {/foreach}
                {*Ô tổng hợp ĐI HỌC cả lớp trong toàn khoản thời gian*}
                <td id="total_present" align="center" bgcolor="#ffebcd" style="color: blue; font-weight: bold">
                    {$presentTotal}
                </td>
                <td align="center" bgcolor="#ffebcd"></td>
            </tr>
            <tr>
                <td colspan="2" align="center" class="pinned"><strong>{__('Absence total of class')}</strong></td>
                {$absentTotal = 0}
                {foreach $last_rows as $cell}
                    <td id="absence_count_{$cell['attendance_id']}" align="center" bgcolor="#ffebcd" style="color: red; font-weight: bold">
                        {if $cell['is_checked'] == 1}
                            {$cell['absence_count']}
                            {$absentTotal = $absentTotal + $cell['absence_count']}
                        {/if}
                    </td>
                {/foreach}
                <td align="center" bgcolor="#ffebcd"></td>
                {*Ô tổng hợp NGHỈ HỌC cả lớp trong toàn khoản thời gian*}
                <td id="total_absence" align="center" bgcolor="#ffebcd" style="color: red; font-weight: bold">
                    {$absentTotal}
                </td>
            </tr>
        {/if}
        </tbody>
    </table>
</div>

{*Jquery Cố định cột số thứ tự và họ tên*}
<script type="text/javascript">

    var $table = $('.table-pinned');
    var $fixedColumn = $table.clone().insertBefore($table).addClass('fixed-column');
    $fixedColumn.find('th').each(function (i, elem) {
        $(this).width($table.find('th:eq(' + i + ')').width());
    });
    $fixedColumn.find('td').each(function (i, elem) {
        $(this).width($table.find('td:eq(' + i + ')').width());
    });
    $fixedColumn.find('th,td').not('.pinned').hide();
    $fixedColumn.find('[id]').each(function () {
        $(this).removeAttr('id');
    });
    $fixedColumn.find('tr').each(function (i, elem) {
        $(this).height($table.find('tr:eq(' + i + ')').height());
    });

    $(window).resize(function () {
        $fixedColumn.find('tr').each(function (i, elem) {
            $(this).height($table.find('tr:eq(' + i + ')').height());
        });
        $fixedColumn.find('td').each(function (i, elem) {
            $(this).addClass('white-space_nowrap');
            $(this).width($table.find('td:eq(' + i + ')').width());
        });
    });

    function fixDiv() {
        var $cache = $('#getFixed');
        var $button = $('#table_button');
        if ($(window).scrollTop() > 100) {
            $cache.css({
                'position': 'fixed',
                'top': '50px'
            });
            $cache.width($('#attendance_list').width() - 1);
        }
        else
            $cache.css({
                'position': 'relative',
                'top': 'auto'
            });

        if ($(window).scrollTop() > 100)
            $button.css({
                'position': 'fixed',
                'top': '90px'
            });
        else
            $button.css({
                'position': 'relative',
                'top': 'auto'
            });
    }
    $(window).scroll(fixDiv);
    fixDiv();
</script>