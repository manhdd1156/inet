<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <div class="pull-right flip">
            <a href="https://blog.coniu.vn/huong-dan-giao-vien-tao-don-thuoc-cho-tre-tren-website-coniu/" class="btn btn-info btn_guide" target="_blank">
                <i class="fa fa-info-circle"></i> {__("Guide")}
            </a>
            {if $sub_view == ""}
                <a href="{$system['system_url']}/class/{$username}/medicines/add" class="btn btn-default">
                    <i class="fa fa-plus"></i> {__("Add New Medicine")}
                </a>
                {if $count_no_confirm > 0}
                    <a href="#" class="btn btn-default js_class-medicine" data-handle="confirm_all_today" data-username="{$username}">{__("Confirm all")}
                    </a>
                {/if}
            {/if}
            {if $sub_view == "all"}
                <a href="{$system['system_url']}/class/{$username}/medicines/add" class="btn btn-default">
                    <i class="fa fa-plus"></i> {__("Add New Medicine")}
                </a>
                {if $count_no_confirm > 0}
                    <a href="#" class="btn btn-default js_class-medicine" data-handle="confirm_all" data-username="{$username}">{__("Confirm all")}
                    </a>
                {/if}
            {/if}
        </div>
        <i class="fa fa-medkit fa-lg fa-fw pr10"></i>
        {__("Medicines")}
        {if $sub_view == "edit"}
            &rsaquo; {$data['medicine_list']}
        {elseif $sub_view == "add"}
            &rsaquo; {__('Add New')}
        {elseif $sub_view == "all"}
            &rsaquo; {__('List all')}
        {elseif $sub_view == "detail"}
            &rsaquo; {$data['child_name']} &rsaquo; {$data['medicine_list']}
        {elseif $sub_view == ""}
            &rsaquo; {__('Today lists')}
        {/if}
    </div>
    {if $sub_view == ""}
        <div class="panel-body with-table">
            <div class="table-responsive">
                <div><strong>{__("Medicine list")}&nbsp;({$rows|count})</th></strong></div>
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>{__('Child')}</th>
                            <th>{__("Medicine list")}</th>
                            <th>{__("Times/day")}</th>
                            <th>{__("Time")}</th>
                            <th>{__("Actions")}</th>
                        </tr>
                    </thead>
                    <tbody>
                        {$idx = 1}
                        {foreach $rows as $row}
                            {if $idx > 1}
                                <tr><td colspan="6"></td></tr>
                            {/if}
                            <tr {if $row['child_status'] == 0} class="row-disable" {/if}>
                                {$rowspan = 1}
                                {if $row['guide'] != ''}{$rowspan = $rowspan + 1}{/if}
                                {if count($row['detail']) > 0}{$rowspan = $rowspan + 1}{/if}
                                <td align="center" rowspan="{$rowspan}" class="align-middle">
                                    <strong>{$idx}</strong>
                                </td>
                                <td class="align-middle">
                                    <a href="{$system['system_url']}/class/{$username}/medicines/detail/{$row['medicine_id']}">{$row['child_name']}</a>
                                    {if $row['status']== $smarty.const.MEDICINE_STATUS_NEW}
                                        <img src="{$system['system_url']}/content/themes/{$system['theme']}/images/new.gif"/>
                                    {elseif $row['status']== $smarty.const.MEDICINE_STATUS_CONFIRMED}
                                        <i class="fa fa-check"></i>
                                    {/if}
                                    <br/>({$row['birthday']})
                                </td>
                                <td class="align-middle">
                                    {nl2br($row['medicine_list'])}
                                    {if $row['source_file'] != null}<br/><br/><a href = "{$row['source_file']}" target="_blank">{__("Doctor prescription")|upper}</a>{/if}
                                </td>
                                <td align="center" class="align-middle">{if $school['school_allow_medicate']}{count($row['detail'])}/{/if}{$row['time_per_day']}</td>
                                <td align="center" class="align-middle">
                                    {$row['begin']}<br/>
                                    <font color="#dc143c">{$row['end']}</font>
                                </td>
                                <td align="center" nowrap="true" class="align-middle">
                                    {if ($row['time_per_day'] > count($row['detail'])) && ($row['status'] != $smarty.const.MEDICINE_STATUS_CANCEL) && $schoolConfig['school_allow_medicate']}
                                        <button class="btn btn-xs btn-default js_class-medicine" data-handle="medicate" data-max="{$row['time_per_day']}" data-child="{$row['child_id']}" data-username="{$username}" data-id="{$row['medicine_id']}">
                                            {__("Medicate")}
                                        </button>
                                    {/if}
                                    {if $row['status'] == $smarty.const.MEDICINE_STATUS_NEW}
                                        <button class="btn btn-xs btn-default js_class-medicine" data-handle="confirm" data-child="{$row['child_id']}" data-username="{$username}" data-id="{$row['medicine_id']}">
                                            {__("Confirm")}
                                        </button>
                                    {/if}
                                    {if $row['status'] != $smarty.const.MEDICINE_STATUS_CANCEL}
                                        {if count($row['detail']) == 0}
                                            <a href="{$system['system_url']}/class/{$username}/medicines/edit/{$row['medicine_id']}" class="btn btn-xs btn-default">
                                                {__("Edit")}
                                            </a>
                                        {/if}
                                    {/if}
                                    {if $row['can_delete']}
                                        <button class="btn btn-xs btn-danger js_class-medicine-delete" data-username="{$username}" data-id="{$row['medicine_id']}">
                                            {__("Delete")}
                                        </button>
                                    {else}
                                        {if $row['status'] != $smarty.const.MEDICINE_STATUS_CANCEL}
                                            <button class="btn btn-xs btn-warning js_class-medicine-cancel" data-screen="" data-handle="cancel" data-child="{$row['child_id']}" data-username="{$username}" data-id="{$row['medicine_id']}">
                                                {__("Cancel")}
                                            </button>
                                        {/if}
                                    {/if}
                                </td>
                            </tr>
                            {if $row['guide'] != ''}
                                <tr>
                                    <td align="center" class="align-middle"><strong>{__("Guide")}</strong></td>
                                    <td colspan="4">{nl2br($row['guide'])}</td>
                                </tr>
                            {/if}
                            {if count($row['detail']) > 0 && $schoolConfig['school_allow_medicate']}
                                <tr>
                                    <td align="center" class="align-middle"><strong>{__("Medicated")}</strong></td>
                                    <td colspan="4">
                                        {foreach $row['detail'] as $detail}
                                            <strong>{$detail['time_on_day']}</strong>&nbsp;-&nbsp;{$detail['created_at']}&nbsp;|&nbsp;{$detail['user_fullname']}
                                            {if $detail['created_user_id'] != $user->_data['user_id']}
                                                <a href="#" class="far fa-comments fa-lg js_chat-start" data-name="{$detail['user_fullname']}" data-uid="{$detail['created_user_id']}"></a>
                                            {/if}<br/>
                                        {/foreach}
                                    </td>
                                </tr>
                            {/if}
                            {$idx = $idx + 1}
                        {/foreach}

                        {if $rows|count == 0}
                            <tr class="odd">
                                <td valign="top" align="center" colspan="6" class="dataTables_empty">
                                    {__("No data available in table")}
                                </td>
                            </tr>
                        {/if}
                    </tbody>
                </table>
            </div>
        </div>
    {elseif $sub_view == "all"}
        <div class="panel-body with-table">
            <div class="table-responsive">
                {*<div><strong>{__("Medicine list")}&nbsp;{if count($rows) > 0}({$rows|count}) {else} (0) {/if}</th></strong></div>*}
                <table class="table table-striped table-bordered table-hover js_dataTable">
                    <thead>
                    <tr>
                        <th width="5%">#</th>
                        <th width="30%">{__('Child')}</th>
                        <th width="30%">{__("Medicine list")}</th>
                        <th width="12%">{__("Times/day")}</th>
                        <th width="13%">{__("Time")}</th>
                        <th width="10%">{__("Actions")}</th>
                    </tr>
                    </thead>
                    <tbody>
                    {$idx = 1}
                    {foreach $rows as $row}
                        <tr {if $row['child_status'] == 0} class="row-disable" {/if}>
                            <td align="center" class="align-middle" >
                                <strong>{$idx}</strong>
                            </td>
                            <td class="align-middle">
                                <a href="{$system['system_url']}/class/{$username}/medicines/detail/{$row['medicine_id']}">{$row['child_name']}</a>
                                {if $row['status']== $smarty.const.MEDICINE_STATUS_NEW}
                                    <img src="{$system['system_url']}/content/themes/{$system['theme']}/images/new.gif"/>
                                {elseif $row['status']== $smarty.const.MEDICINE_STATUS_CONFIRMED}
                                    <i class="fa fa-check"></i>
                                {else}
                                    <i class="fa fa-trash-alt"></i>
                                {/if}
                                <br/>({$row['birthday']})
                            </td>
                            <td class="align-middle">
                                {nl2br($row['medicine_list'])}
                                {if $row['source_file'] != null}<br/><br/><a href = "{$row['source_file']}" target="_blank">{__("Doctor prescription")|upper}</a>{/if}
                            </td>
                            <td align="center" class="align-middle">{$row['time_per_day']}</td>
                            <td align="center" class="align-middle">
                                {$row['begin']}<br/>
                                <font color="#dc143c">{$row['end']}</font>
                            </td>
                            <td align="center" nowrap="true" class="align-middle">
                                {if $row['status'] == $smarty.const.MEDICINE_STATUS_NEW}
                                    <button id="button-confirm_{$row['medicine_id']}" class="btn btn-xs btn-default js_class-medicine" data-handle="confirm" data-child="{$row['child_id']}" data-screen="all" data-username="{$username}" data-id="{$row['medicine_id']}">
                                        {__("Confirm")}
                                    </button>
                                {/if}
                                {if ($row['status'] != $smarty.const.MEDICINE_STATUS_CANCEL) && ($row['editable'] == 1)}
                                    {if $row['count'] == 0}
                                        <a href="{$system['system_url']}/class/{$username}/medicines/edit/{$row['medicine_id']}" class="btn btn-xs btn-default">
                                            {__("Edit")}
                                        </a>
                                    {/if}

                                {/if}
                                {if $row['can_delete']}
                                    <button class="btn btn-xs btn-danger js_class-medicine-delete" data-username="{$username}" data-screen="all" data-id="{$row['medicine_id']}">
                                        {__("Delete")}
                                    </button>
                                {else}
                                    {if $row['status'] != $smarty.const.MEDICINE_STATUS_CANCEL}
                                        <button class="btn btn-xs btn-warning js_class-medicine-cancel" data-screen="all" data-handle="cancel" data-child="{$row['child_id']}" data-screen="all" data-username="{$username}" data-id="{$row['medicine_id']}">
                                            {__("Cancel")}
                                        </button>
                                    {/if}
                                {/if}
                            </td>
                        </tr>
                        {$idx = $idx + 1}
                    {/foreach}
                    </tbody>
                </table>
            </div>
        </div>
    {elseif $sub_view == "edit"}
        <div class="panel-body">
            <form class="form-horizontal" enctype="multipart/form-data" method="post" id="create_medicine">
                <input type="hidden" name="username" value="{$username}"/>
                <input type="hidden" name="medicine_id" value="{$data['medicine_id']}"/>
                <input type="hidden" name="child_id" value="{$data['child_id']}"/>
                <input type="hidden" name="do" value="edit"/>

                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Select student")} (*)</label>
                    <div class="col-sm-9">
                            {*<select name="child_id" class="form-control" autofocus>*}
                                {*{foreach $children as $child}*}
                                    {*<option value="{$child['child_id']}" {if $data['child_id'] == $child['child_id']}selected{/if}>*}
                                        {*{$child['child_name']} - {$child['birthday']}*}
                                    {*</option>*}
                                {*{/foreach}*}
                            {*</select>*}
                        <input class="form-control" name="child_name" value="{$data['child_name']} - {$data['birthday']}" readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Medicine list")} (*)</label>
                    <div class="col-sm-9">
                        <textarea class="form-control js_autosize" name="medicine_list" required maxlength="400">{$data['medicine_list']}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Usage guide")} (*)</label>
                    <div class="col-sm-9">
                        <textarea class="form-control js_autosize" name="guide" rows="6" required>{$data['guide']}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Time per day")} (*)</label>
                    <div class="col-sm-3">
                        <input class="form-control" type="number" name="time_per_day" min="1" step="1" maxlength="2" value="{$data['time_per_day']}" required>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Time")} (*)</label>
                    <div class='col-sm-3'>
                        <div class='input-group date' id='medicine_beginpicker_new'>
                            <input type='text' name="begin" value="{$data['begin']}" class="form-control" placeholder="{__("Begin")} (*)" required/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                    <div class='col-sm-1'><i class="fas fa-long-arrow-alt-right fa-lg fa-fw pl10 pt10"></i></div>
                    <div class='col-md-3'>
                        <div class='input-group date' id='medicine_endpicker_new'>
                            <input type='text' name="end" value="{$data['end']}" class="form-control" placeholder="{__("End")} (*)" required/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                </div>

                {*<div class="form-group">*}
                    {*<label class="rp-file col-sm-3 control-label text-left" style="padding-top: 0px">{__("File attachment")}</label>*}
                    {*<div class="col-sm-6">*}
                        {*{if !is_empty($data['source_file'])}*}
                            {*<a href="{$data['source_file']}" target="_blank"><img src = "{$data['source_file']}" class = "img-responsive"></a>*}
                            {*<br>*}
                            {*<label class="control-label">{__("Chọn tệp thay thế")}</label>*}
                            {*<br>*}
                        {*{/if}*}
                        {*<input type="file" name="file" id="file"/>*}
                    {*</div>*}
                {*</div>*}
                <input type = "hidden" id = "is_file" name = "is_file" value = "1">
                <div class="form-group" id = "file_old">
                    <label class="rp-file col-sm-3 control-label text-left" style="padding-top: 0px">{__("Doctor prescription")}</label>
                    <div class="col-sm-6">
                        {if !is_empty($data['source_file'])}
                            <a href="{$data['source_file']}" target="_blank"><img src = "{$data['source_file']}" class = "img-responsive"></a>
                        {else} {__('No file attachment')}
                        {/if}
                    </div>
                </div>
                <div class = "form-group">
                    <label class="control-label col-sm-3">
                        {if is_empty($data['source_file'])}
                            {__("Choose file")}
                        {else}
                            {__("Choose file replace")}
                        {/if}
                    </label>
                    <div class = "col-sm-6">
                        <input type="file" name="file" id="file"/>
                    </div>
                    <a class = "delete_image btn btn-danger btn-xs text-left">{__('Delete')}</a>
                </div>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    {elseif $sub_view == "add"}
        <div class="panel-body">
            <form class="form-horizontal" enctype="multipart/form-data" method="post" id="create_medicine">
                <input type="hidden" name="username" value="{$username}"/>
                <input type="hidden" name="do" value="add"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Select student")} (*)</label>
                    <div class="col-sm-5">
                        <select name="child_id" class="form-control" autofocus>
                            {$idx = 1}
                            {foreach $children as $child}
                                <option value="{$child['child_id']}">{$idx} - {$child['child_name']} - {$child['birthday']}</option>
                                {$idx = $idx + 1}
                            {/foreach}
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Medicine list")} (*)</label>
                    <div class="col-sm-9">
                        <textarea class="form-control js_autosize" id="medicine_list_clear" name="medicine_list" required maxlength="400"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Usage guide")} (*)</label>
                    <div class="col-sm-9">
                        <textarea class="form-control js_autosize" id="guide_clear" name="guide" rows="6" required></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Time per day")} (*)</label>
                    <div class="col-sm-3">
                        <input class="form-control" type="number" name="time_per_day" min="1" step="1" maxlength="2" value="2" required>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Time")} (*)</label>
                    <div class='col-sm-3'>
                        <div class='input-group date' id='medicine_beginpicker_new'>
                            <input type='text' name="begin" class="form-control" placeholder="{__("Begin")} (*)" required/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                    <div class='col-sm-1'><i class="fas fa-long-arrow-alt-right fa-lg fa-fw pl10 pt10"></i></div>
                    <div class='col-md-3'>
                        <div class='input-group date' id='medicine_endpicker_new'>
                            <input type='text' name="end" class="form-control" placeholder="{__("End")} (*)" required/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Doctor prescription")}</label>
                    <div class="col-sm-6">
                        <input type="file" name="file" id="file"/>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    {elseif $sub_view == "detail"}
        <div class="panel-body with-table">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <tbody>
                    <tr>
                        <td nowrap="true" align="right" class="align-middle"><strong>{__("Child")}</strong></td>
                        <td><a href="{$system['system_url']}/class/{$username}/children/detail/{$data['child_id']}" target="_blank">{$data['child_name']}</a> ({$data['birthday']})</td>
                    </tr>
                    <tr>
                        <td nowrap="true" align="right" class="align-middle"><strong>{__("Medicine list")}</strong></td>
                        <td>
                            {nl2br($data['medicine_list'])}
                            {if $data['status']== $smarty.const.MEDICINE_STATUS_NEW}
                                <img src="{$system['system_url']}/content/themes/{$system['theme']}/images/new.gif"/>
                            {elseif $data['status']== $smarty.const.MEDICINE_STATUS_CONFIRMED}
                                <i class="fa fa-check"></i>
                            {else}
                                <i class="fa fa-trash-alt"></i>
                            {/if}
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="true" align="right" class="align-middle"><strong>{__("Usage guide")}</strong></td>
                        <td>{nl2br($data['guide'])}</td>
                    </tr>
                    <tr>
                        <td nowrap="true" align="right" class="align-middle"><strong>{__("Time per day")}</strong></td>
                        <td>{$data['time_per_day']}</td>
                    </tr>
                    <tr>
                        <td nowrap="true" align="right" class="align-middle"><strong>{__("Time")}</strong></td>
                        <td>{$data['begin']} - {$data['end']}</td>
                    </tr>
                    <tr>
                        <td nowrap="true" align="right" class="align-middle"><strong>{__("Doctor prescription")|upper}</strong></td>
                        <td>
                            {if !is_empty($data['source_file'])}
                                <a href = "{$data['source_file']}" target="_blank"><strong>
                                        {__("File attachment")}
                                    </strong>
                                </a>
                            {else}
                                {__("No file attachment")}
                            {/if}
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="true" align="right" class="align-middle"><strong>{__("Creator")}</strong></td>
                        <td><a href="{$system['system_url']}/{$data['user_name']}" target="_blank">{$data['user_fullname']}</a></td>
                    </tr>
                    <tr>
                        <td nowrap="true" align="right" style="vertical-align:middle"><strong>{__("User confirmed")}</strong></td>
                        <td><a href="{$system['system_url']}/{$data['confirm_username']}" target="_blank">{$data['confirm_user']}</a></td>
                    </tr>
                    <tr>
                        <td nowrap="true" align="right" class="align-middle"><strong>{__("Created time")}</strong></td>
                        <td>{$data['created_at']}</td>
                    </tr>
                    {if $data['updated_at'] != ''}
                        <tr>
                            <td nowrap="true" align="right" class="align-middle"><strong>{__("Last updated")}</strong></td>
                            <td>{$data['updated_at']}</td>
                        </tr>
                    {/if}
                    {if count($data['detail']) > 0 && $schoolConfig['school_allow_medicate']}
                        <tr>
                            <td nowrap="true" align="right" class="align-middle"><strong>{__("Medicated")}</strong></td>
                            <td>
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{__('Usage date')}</th>
                                        <th>{__('Creator')}</th>
                                        <th>{__('Created time')}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {foreach $data['detail'] as $detail}
                                        <tr>
                                            <td align="center">{$detail['time_on_day']}</td>
                                            <td>{$detail['usage_date']}</td>
                                            <td>
                                                {$detail['user_fullname']}
                                                {if $detail['created_user_id'] != $user->_data['user_id']}
                                                    <a href="#" class="far fa-comments fa-lg js_chat-start" data-name="{$detail['user_fullname']}" data-uid="{$detail['created_user_id']}"></a>
                                                {/if}
                                            </td>
                                            <td>{$detail['created_at']}</td>
                                        </tr>
                                    {/foreach}
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    {/if}
                    </tbody>
                </table>
            </div>
            <div class="form-group pl5">
                <div class="col-sm-12">
                    <a class="btn btn-default" href="{$system['system_url']}/class/{$username}/medicines">{__("Today lists")}</a>
                    <a class="btn btn-default" href="{$system['system_url']}/class/{$username}/medicines/all">{__("List all")}</a>

                    {if $data['status'] == $smarty.const.MEDICINE_STATUS_NEW}
                        <button id="button-confirm_{$data['medicine_id']}" class="btn btn-default js_class-medicine" data-handle="confirm" data-child="{$data['child_id']}" data-screen="all" data-username="{$username}" data-id="{$data['medicine_id']}">
                            {__("Confirm")}
                        </button>
                    {/if}
                    {if ($data['status'] != $smarty.const.MEDICINE_STATUS_CANCEL) && ($data['editable'] == 1)}
                        {if $data['count'] == 0}
                            <a href="{$system['system_url']}/class/{$username}/medicines/edit/{$data['medicine_id']}" class="btn btn-default">
                                {__("Edit")}
                            </a>
                        {/if}

                    {/if}
                    {if $data['can_delete']}
                        <button class="btn btn-danger js_class-medicine-delete" data-username="{$username}" data-screen="all" data-id="{$data['medicine_id']}">
                            {__("Delete")}
                        </button>
                    {else}
                        {if $row['status'] != $smarty.const.MEDICINE_STATUS_CANCEL}
                            <button class="btn btn-warning js_class-medicine-cancel" data-screen="all" data-handle="cancel" data-child="{$data['child_id']}" data-screen="all" data-username="{$username}" data-id="{$data['medicine_id']}">
                                {__("Cancel")}
                            </button>
                        {/if}
                    {/if}
                </div>
            </div>
        </div>
    {/if}
</div>