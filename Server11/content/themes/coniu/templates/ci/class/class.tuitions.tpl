<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <div class="pull-right flip">
            {*<a href="{$system['system_url']}/class/{$username}/ott/" class="btn btn-info">*}
                {*<i class="fa fa-bars"></i> {__("One-time fee")}*}
            {*</a>*}
        </div>
        <i class="fas fa-money-bill-alt fa-lg fa-fw pr10"></i>
        {__("Tuition fee")}
        {if $sub_view == "detail"}
            &rsaquo; {__('Detail')}
        {/if}
        {if $sub_view == "history"}
            &rsaquo; {__("Used last month")}
        {/if}
    </div>
    {if $sub_view == ""}
        <div class="panel-body with-table">
            <div><strong>{__("Tuition list")}&nbsp;({$rows|count})</strong></div>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th nowrap="true">{__("Month")}</th>
                            <th>{__("Paid")}<br/>({$smarty.const.MONEY_UNIT})</th>
                            <th>{__("Total")}<br/>({$smarty.const.MONEY_UNIT})</th>
                            <th>{__("Paid studentren")}</th>
                            <th>{__("Option")}</th>
                        </tr>
                    </thead>
                    <tbody>
                        {$idx = 1}
                        {foreach $rows as $row}
                            <tr>
                                <td align="center" style="vertical-align:middle"><strong>{$idx}</strong></td>
                                <td align="center" style="vertical-align:middle"><a href="{$system['system_url']}/class/{$username}/tuitions/detail/{$row['tuition_id']}">{$row['month']}</a></td>
                                <td class="text-right" style="vertical-align:middle">{moneyFormat($row['paid_amount'])}</td>
                                <td class="text-right" style="vertical-align:middle">{moneyFormat($row['total_amount'])}</td>
                                <td align="center" style="vertical-align:middle">{$row['paid_count']}</td>
                                <td align="center" style="vertical-align:middle">
                                    <a href="{$system['system_url']}/class/{$username}/tuitions/detail/{$row['tuition_id']}" class="btn btn-xs btn-default">{__("Detail")}</a>
                                    <a href="{$system['system_url']}/class/{$username}/tuitions/history/{$row['tuition_id']}" class="btn btn-xs btn-default"> {__("Used last month")} </a>
                                </td>
                            </tr>
                            {$idx = $idx + 1}
                        {/foreach}
                    </tbody>
                </table>
            </div>
        </div>
    {elseif $sub_view == "detail"}
        {include file="ci/class/class.tuitions.detail.tpl"}
    {elseif $sub_view == "history"}
        {include file="ci/class/class.tuitions.history.tpl"}
    {/if}
</div>