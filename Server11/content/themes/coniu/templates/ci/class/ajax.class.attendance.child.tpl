<div><strong>{$data['child']['child_name']}&nbsp;({__("Total")}: {$data['attendance']['attendance']|count}&nbsp;|&nbsp;{__("Present")}: {$data['attendance']['present_count']}&nbsp;|&nbsp;{__("Absence")}: {$data['attendance']['absence_count']})</strong></div>
<table class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th>#</th>
        <th>{__("Attendance date")}</th>
        <th>{__("Status")}</th>
        <th>{__("Reason")}</th>
        <th>{__("Teacher")}</th>
    </tr>
    </thead>
    <tbody>
    <input type="hidden" name="child_id" value="{$data['child']['child_id']}"
    {$idx = 1}
    {foreach $data['attendance']['attendance'] as $row}
        <tr>
            <td class="text-center">{$idx}</td>
            <td class="text-center">{$row['attendance_date']}</td>
            <td class="text-center">
                <input type="hidden" name="attendanceIds[]" value="{$row['attendance_id']}">
                <input type="hidden" name="allOldStatus[]" value="{$row['status']}">
                {*xử lý điểm danh trong qúa khứ*}
                {$dateNow = toDBDate($row['attendance_date'])}
                {if $dateNow != $today && !$school['allow_teacher_rolls_days_before']}
                    <select name="allStatus[]" id="status">
                        <option value="{$smarty.const.ATTENDANCE_ABSENCE}" {if $row['status'] == $smarty.const.ATTENDANCE_ABSENCE}selected {else}disabled{/if}>
                            --------{__("With permission")|upper}
                        </option>
                        <option value="{$smarty.const.ATTENDANCE_PRESENT}" {if $row['status'] == $smarty.const.ATTENDANCE_PRESENT}selected{else}disabled{/if}>
                            {__("Present")}
                        </option>
                        {if $school['attendance_use_come_late']}
                            <option value="{$smarty.const.ATTENDANCE_COME_LATE}" {if $row['status'] == $smarty.const.ATTENDANCE_COME_LATE}selected{else}disabled{/if}>
                                {__("Come late")|upper}
                            </option>
                        {/if}
                        {if $school['attendance_use_leave_early']}
                            <option value="{$smarty.const.ATTENDANCE_EARLY_LEAVE}" {if $row['status'] == $smarty.const.ATTENDANCE_EARLY_LEAVE}selected{else}disabled{/if}>
                                {__("Leave early")|upper}
                            </option>
                        {/if}
                        {if $school['attendance_absence_no_reason']}
                            <option value="{$smarty.const.ATTENDANCE_ABSENCE_NO_REASON}" {if $row['status'] == $smarty.const.ATTENDANCE_ABSENCE_NO_REASON}selected{else}disabled{/if}>
                                {__("Without permission")}
                            </option>
                        {/if}
                        {if ($row['status'] == $smarty.const.ATTENDANCE_EARLY_LEAVE && !$school['attendance_use_leave_early'])}
                            <option value="{$smarty.const.ATTENDANCE_EARLY_LEAVE}" selected>
                                {__("Leave early")|upper}
                            </option>
                        {/if}
                        {if ($row['status'] == $smarty.const.ATTENDANCE_COME_LATE && !$school['attendance_use_come_late'])}
                            <option value="{$smarty.const.ATTENDANCE_COME_LATE}" selected>
                                {__("Come late")|upper}
                            </option>
                        {/if}
                        {if ($row['status'] == $smarty.const.ATTENDANCE_ABSENCE_NO_REASON && !$school['attendance_absence_no_reason'])}
                            <option value="{$smarty.const.ATTENDANCE_ABSENCE_NO_REASON}" selected>
                                {__("Without permission")|upper}
                            </option>
                        {/if}
                    </select>
                {else}
                    <select name="allStatus[]" id="status">
                        <option value="{$smarty.const.ATTENDANCE_ABSENCE}" {if $row['status'] == $smarty.const.ATTENDANCE_ABSENCE}selected{/if}>
                            --------{__("With permission")|upper}
                        </option>
                        <option value="{$smarty.const.ATTENDANCE_PRESENT}" {if $row['status'] == $smarty.const.ATTENDANCE_PRESENT}selected{/if}>
                            {__("Present")}
                        </option>
                        {if $school['attendance_use_come_late']}
                            <option value="{$smarty.const.ATTENDANCE_COME_LATE}" {if $row['status'] == $smarty.const.ATTENDANCE_COME_LATE}selected{/if}>
                                {__("Come late")|upper}
                            </option>
                        {/if}
                        {if $school['attendance_use_leave_early']}
                            <option value="{$smarty.const.ATTENDANCE_EARLY_LEAVE}" {if $row['status'] == $smarty.const.ATTENDANCE_EARLY_LEAVE}selected{/if}>
                                {__("Leave early")|upper}
                            </option>
                        {/if}
                        {if $school['attendance_absence_no_reason']}
                            <option value="{$smarty.const.ATTENDANCE_ABSENCE_NO_REASON}" {if $row['status'] == $smarty.const.ATTENDANCE_ABSENCE_NO_REASON}selected{/if}>
                                {__("Without permission")}
                            </option>
                        {/if}
                        {if ($row['status'] == $smarty.const.ATTENDANCE_EARLY_LEAVE && !$school['attendance_use_leave_early'])}
                            <option value="{$smarty.const.ATTENDANCE_EARLY_LEAVE}" selected>
                                {__("Leave early")|upper}
                            </option>
                        {/if}
                        {if ($row['status'] == $smarty.const.ATTENDANCE_COME_LATE && !$school['attendance_use_come_late'])}
                            <option value="{$smarty.const.ATTENDANCE_COME_LATE}" selected>
                                {__("Come late")|upper}
                            </option>
                        {/if}
                        {if ($row['status'] == $smarty.const.ATTENDANCE_ABSENCE_NO_REASON && !$school['attendance_absence_no_reason'])}
                            <option value="{$smarty.const.ATTENDANCE_ABSENCE_NO_REASON}" selected>
                                {__("Without permission")|upper}
                            </option>
                        {/if}
                    </select>
                {/if}
            </td>
            <td>
                <input type="text" name="allReasons[]" maxlength="512" style="width: 100%" value="{$row['reason']}" placeholder="{__("Absent reason")}">
            </td>
            <td>{$row['user_fullname']}</td>
        </tr>
        {$idx = $idx + 1}
    {/foreach}

    {if $data['attendance']['attendance']|count == 0}
        <tr class="odd">
            <td valign="top" align="center" colspan="5" class="dataTables_empty">
                {__("No data available in table")}
            </td>
        </tr>
    {/if}
    </tbody>
</table>