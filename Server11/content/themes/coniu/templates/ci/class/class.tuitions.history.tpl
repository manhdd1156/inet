<div class="panel-body with-table">
    <table class="table table-bordered">
        <tr>
            <td colspan="4"><strong>{__("Fees and services usage history related to the tuition calculation")}&nbsp;{$data['month']}</strong></td>
        </tr>
    </table>
    <a class="btn btn-default" href="{$system['system_url']}/class/{$username}/tuitions/detail/{$data['tuition_id']}">{__("Tuition detail")}</a>
    <br/><br/>
    <div class="table-responsive" id="children_list">
        <table class="table table-striped table-bordered table-hover">
            <thead>
            <tr>
                <th>
                    <div class="pull-left flip">{__("Fees and services usage history of each child")}&nbsp;({$data['tuition_child']|count})</div>
                    <div class="pull-right flip">
                        {__("Usage total of class")}&nbsp;({$smarty.const.MONEY_UNIT})
                        <input type="text" class="text-right" value="{moneyFormat($data['class_total'])}" readonly/>
                    </div>
                </th>
            </tr>
            </thead>
            <tbody>
            {$detailIdx = 1}
            {foreach $data['tuition_child'] as $child}
                <tr>
                    <td>
                        <div style="color: #597BA5; display: inline;">
                            <strong>{$detailIdx} - {$child['child_name']}</strong>&nbsp;|&nbsp;{$child['birthday']}
                            {if isset($child['attendance_count'])}&nbsp;|&nbsp;
                                {__("Number of charged days last month")}:&nbsp;{$child['attendance_count']}
                            {/if}
                        </div>
                        <br/>
                        {if (count($child['tuition_detail']['fees']) + count($child['tuition_detail']['services'])) > 0}
                            <table class="table table-striped table-bordered table-hover bg_white">
                                <thead>
                                <tr>
                                    <td colspan="6"><strong>{__("Fee usage history of previous month")} {$data['pre_month']}</strong>
                                    </td>
                                </tr>
                                <tr>
                                    <th>#</th>
                                    <th>{__("Fee name")}</th>
                                    <th class="text-center">{__("Unit")}</th>
                                    <th class="text-center">{__("Quantity")}</th>
                                    <th class="text-right">{__("Unit price")}&nbsp;({$smarty.const.MONEY_UNIT})</th>
                                    <th class="text-right">{__("Money amount")}&nbsp;({$smarty.const.MONEY_UNIT})</th>
                                </tr>
                                </thead>
                                <tbody>
                                {*Hiển thị danh sách phí của trẻ*}
                                {$idx = 1}
                                {foreach $child['tuition_detail']['fees'] as $detail}
                                    {if $detail['to_money'] > 0}
                                        <tr>
                                            <td align="center">{$idx}</td>
                                            <td>{$detail['fee_name']}</td>
                                            <td class="text-center">{if $detail['fee_type'] == $smarty.const.FEE_TYPE_MONTHLY}{__('Monthly')}{else}{__('Daily')}{/if}</td>
                                            <td class="text-center">{$detail['quantity']}</td>
                                            <td class="text-right">{moneyFormat($detail['unit_price'])}</td>
                                            <td class="text-right">{moneyFormat($detail['to_money'])}</td>
                                        </tr>
                                        {$idx = $idx + 1}
                                    {/if}
                                {/foreach}
                                {*Hiển thị danh sách dịch vụ theo THÁNG và ĐIỂM DANH*}
                                {if count($child['tuition_detail']['services']) > 0}
                                    <tr><td colspan="6"><strong>{__("Service")}</strong></td></tr>
                                    {$idx = 1}
                                    {foreach $child['tuition_detail']['services'] as $detail}
                                        {if $detail['to_money'] > 0}
                                            <tr>
                                                <td align="center">{$idx}</td>
                                                <td>{$detail['service_name']}</td>
                                                <td class="text-center">{if $detail['service_type'] == $smarty.const.SERVICE_TYPE_MONTHLY}{__('Monthly')}{else}{__('Daily')}{/if}</td>
                                                <td class="text-center">{$detail['quantity']}</td>
                                                <td class="text-right">{moneyFormat($detail['unit_price'])}</td>
                                                <td class="text-right">{moneyFormat($detail['to_money'])}</td>
                                            </tr>
                                            {$idx = $idx + 1}
                                        {/if}
                                    {/foreach}
                                {/if}
                                {*Hiển thị danh sách dịch vụ theo SỐ LẦN*}
                                {if count($child['tuition_detail']['count_based_services']) > 0}
                                    <tr><td colspan="6"><strong>{__("Count-based service of previous month")} {$child['pre_month']}</strong></td></tr>
                                    {$idx = 1}
                                    {foreach $child['tuition_detail']['count_based_services'] as $detail}
                                        {if $detail['to_money'] > 0}
                                            <tr>
                                                <td align="center">{$idx}</td>
                                                <td>{$detail['service_name']}</td>
                                                <td class="text-center">{__('Times')}</td>
                                                <td class="text-center">{$detail['quantity']}</td>
                                                <td class="text-right">{moneyFormat($detail['unit_price'])}</td>
                                                <td class="text-right">{moneyFormat($detail['to_money'])}</td>
                                            </tr>
                                            {$idx = $idx + 1}
                                        {/if}
                                    {/foreach}
                                {/if}

                                <tr>
                                    <td colspan="4"></td>
                                    <td class="text-right"><strong>{__("Child usage total")}&nbsp;({$smarty.const.MONEY_UNIT})</strong></td>
                                    <td class="text-right">{moneyFormat($child['child_total'])}</td>
                                </tr>
                                </tbody>
                            </table>
                        {/if}
                    </td>
                </tr>
                <tr height = "10"></tr>
                {$detailIdx = $detailIdx + 1}
            {/foreach}
            </tbody>
        </table>
        <a class="btn btn-default" href="{$system['system_url']}/class/{$username}/tuitions/detail/{$data['tuition_id']}">{__("Tuition detail")}</a>
    </div>
</div>
