<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <i class="fas fa-tachometer-alt fa-fw fa-lg pr10"></i>
        {__("Dashboard")}:&nbsp;<a href="{$system['system_url']}/groups/{$username}">{$insights['class']['group_title']}</a>
        <div class="pull-right flip">
            <a href="{$system['system_url']}/class/{$username}/attendance/rollup">
                <i class="fa fa-tasks fa-fw pr10"></i>{__("Roll up")}
            </a>&nbsp;|&nbsp;
            <a href="{$system['system_url']}/pages/{$school_name}">{__("School page")}</a>
        </div>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-6">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead><tr><th>{__("Teacher")}</th></tr></thead>
                        <tbody>
                            <tr>
                                <td>
                                    <div>
                                        {foreach $insights['teachers'] as $row}
                                            <span class="name js_user-popover" data-uid="{$row['user_id']}">
                                                <a href="{$system['system_url']}/{$row['user_name']}">{$row['user_fullname']}</a>
                                            </span>
                                            {if $row['user_id'] != $user->_data['user_id']}
                                                <a href="#" class="far fa-comments fa-lg js_chat-start" data-name="{$row['user_fullname']}" data-uid="{$row['user_id']}"></a>
                                            {/if}
                                            &nbsp;&nbsp;&nbsp;
                                        {/foreach}
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div class="table-responsive">
                    <div><strong>{__("Children list")}&nbsp;({$insights['children']|count})</strong></div>
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>{__("#")}</th>
                            <th>{__("Full name")}</th>
                            <th>{__("Parent")}</th>
                        </tr>
                        </thead>
                        <tbody>
                        {$idx = 1}
                        {foreach $insights['children'] as $row}
                            <tr>
                                <td align="center" class="align-middle">{$idx}</td>
                                <td>
                                    <a href="{$system['system_url']}/class/{$username}/children/detail/{$row['child_id']}">{$row['child_name']}</a>
                                    <br/>({$row['birthday']})
                                </td>
                                <td>
                                    {if count($row['parent']) == 0}
                                        {__("No parent")}
                                    {else}
                                        {foreach $row['parent'] as $_user}
                                            <span class="name js_user-popover" data-uid="{$_user['user_id']}">
                                                <a href="{$system['system_url']}/{$_user['user_name']}">{$_user['user_fullname']}</a>
                                            </span>
                                            {if $_user['user_id'] != $user->_data['user_id']}
                                                <a href="#" class="far fa-comments fa-lg js_chat-start" data-name="{$_user['user_fullname']}" data-uid="{$_user['user_id']}"></a>
                                            {/if}
                                            {if $_user['suggest'] == 1}
                                                <button class="btn btn-xs btn-danger js_school-approve-parent" id="button_{$row['child_id']}_{$_user['user_id']}" data-parent="{$_user['user_id']}" data-username="{$username}" data-id="{$row['child_id']}">{__("Approve")}</button>
                                            {/if}
                                            <br/>
                                        {/foreach}
                                    {/if}
                                    {if !is_empty({$row['parent_phone']})}
                                        ({$row['parent_phone']})
                                    {/if}
                                </td>
                            </tr>
                            {$idx = $idx + 1}
                        {/foreach}

                        {if $insights['children']|count == 0}
                            <tr class="odd">
                                <td valign="top" align="center" colspan="3" class="dataTables_empty">
                                    {__("No data available in table")}
                                </td>
                            </tr>
                        {/if}
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="col-sm-6">
                {if count($insights['medicines']) > 0}
                    <div class="box-primary">
                        <div class="box-header">
                            <i class="fa fa-medkit"></i>
                            <strong>{__("Today medicines")}&nbsp;({$insights['medicines']|count})</strong>&nbsp;
                            <a href="{$system['system_url']}/class/{$username}/medicines" title="{__("See All")}" class="pull-right flip">{__("See All")}</a>
                        </div>
                        <div class="list-group" id="medicine_on_dashboard">
                            {$idx = 1}
                            {foreach $insights['medicines'] as $row}
                                {if is_array($row)}
                                    <div class="list-group-item">
                                        {$idx}-<a href="{$system['system_url']}/class/{$username}/medicines/edit/{$row['medicine_id']}">{$row['child_name']}</a>
                                        {if $row['status']== $smarty.const.MEDICINE_STATUS_NEW}
                                            <img src="{$system['system_url']}/content/themes/{$system['theme']}/images/new.gif"/>
                                        {elseif $row['status']== $smarty.const.MEDICINE_STATUS_CONFIRMED}
                                            <i class="fa fa-check"></i>
                                        {/if}
                                        &nbsp;|&nbsp;{$row['time_per_day']} {__("times/day")}&nbsp;|&nbsp;{$row['begin']}&nbsp;-&nbsp;{$row['end']}<br/>
                                        {$row['medicine_list']}<br/>
                                        <i>{__("Guide")}:</i> <strong>{$row['guide']}</strong>
                                        {if count($row['detail']) > 0}
                                            <br/>{__("Medicated")}:
                                            {foreach $row['detail'] as $detail}
                                                <br/>&nbsp;&nbsp;&nbsp;<strong>{$detail['time_on_day']}</strong>&nbsp;-&nbsp;{$detail['created_at']}&nbsp;|&nbsp;{$detail['user_fullname']}
                                                {if $detail['created_user_id'] != $user->_data['user_id']}
                                                    <a href="#" class="far fa-comments fa-lg js_chat-start" data-name="{$detail['user_fullname']}" data-uid="{$detail['created_user_id']}"></a>
                                                {/if}
                                            {/foreach}
                                        {/if}
                                        <br/>
                                        {if ($row['time_per_day'] > count($row['detail'])) && ($row['status'] != $smarty.const.MEDICINE_STATUS_CANCEL)}
                                            <button class="btn btn-xs btn-default js_class-medicine" data-handle="medicate" data-screen="" data-max="{$row['time_per_day']}" data-child="{$row['child_id']}" data-username="{$username}" data-id="{$row['medicine_id']}">
                                                <!--i class="fa fa-plus-square"></i-->
                                                {__("Medicate")}
                                            </button>
                                        {/if}
                                        {if $row['status'] == $smarty.const.MEDICINE_STATUS_NEW}
                                            <button class="btn btn-xs btn-default js_class-medicine" data-handle="confirm" data-screen="dashboard" data-max="{$row['time_per_day']}" data-child="{$row['child_id']}" data-username="{$username}" data-id="{$row['medicine_id']}">
                                                <!--i class="fa fa-check"></i-->
                                                {__("Confirm")}
                                            </button>
                                        {/if}
                                    </div>
                                {/if}
                                {$idx = $idx + 1}
                            {/foreach}
                        </div>
                    </div>
                    <br/>
                {/if}
                <div class="box-primary">
                    <div class="box-header">
                        <i class="fa fa-bell"></i>
                        <strong>{__("Notification - Event")}</strong>&nbsp;
                        <a href="{$system['system_url']}/class/{$username}/events" title="{__("See All")}" class="pull-right flip">{__("See All")}</a>
                    </div>
                    <div class="list-group" id="event_on_dashboard">
                        {$idx = 1}
                        {foreach $insights['events'] as $row}
                            <div class="list-group-item">
                                {$idx}-<a href="{$system['system_url']}/class/{$username}/events/edit/{$row['event_id']}">
                                    {$row['event_name']}
                                </a>&nbsp;|&nbsp;
                                {if $row['level']==$smarty.const.SCHOOL_LEVEL}
                                    {__("School")}
                                {elseif $row['level']==$smarty.const.CLASS_LEVEL_LEVEL}
                                    {__("Class level")}
                                {elseif $row['level']==$smarty.const.CLASS_LEVEL}
                                    {__("Class")}
                                {/if}
                                &nbsp;|&nbsp;{$row['is_notified_text']}&nbsp;|&nbsp;{$row['post_on_wall_text']}&nbsp;|&nbsp;{$row['location']}
                                {if $row['must_register']}
                                    <br/>{__("Participants")}:&nbsp;
                                    {if $row['for_child']}
                                        {$row['child_participants']} {__("children")}&nbsp;|&nbsp;
                                    {/if}
                                    {if $row['for_parent']}
                                        {$row['parent_participants']} {__("parent")}
                                    {/if}
                                {/if}
                                <br/><br/>
                                {$row['description']}<br/>
                                {if (!$row['is_notified']) && ($row['happened'] <= 0)}
                                    <button class="btn btn-xs btn-default js_class-event-notify" data-username="{$username}" data-id="{$row['event_id']}">
                                        {__("Notify")}
                                    </button>
                                {/if}
                                {if $row['must_register']}
                                    <a href="{$system['system_url']}/class/{$username}/events/participants/{$row['event_id']}" class="btn btn-xs btn-default">
                                        {__("Participants")}
                                    </a>
                                {/if}
                                {if ($row['happened'] <= 0) && $row['can_edit']}
                                    <a href="{$system['system_url']}/class/{$username}/events/edit/{$row['event_id']}" class="btn btn-xs btn-default">
                                        {__("Edit")}
                                    </a>
                                {/if}
                            </div>
                            {$idx = $idx + 1}
                        {/foreach}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>