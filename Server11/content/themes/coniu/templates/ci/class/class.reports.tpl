<div class="panel panel-default">
    <div class="panel-heading with-icon">
        {if $sub_view == ""}
            <div class="pull-right flip">
                <a href="{$system['system_url']}/class/{$username}/reports/add" class="btn btn-default">
                    <i class="fa fa-plus"></i> {__("Add New")}
                </a>
                {if !count($noNotifyReport) == 0}
                    <a class="btn btn-success js_class-report-notify-all" data-handle="notify_all" data-username="{$username}">
                        <i class="fa fa-bell"></i> {__("Notify all")}
                    </a>
                {/if}
            </div>
        {elseif $sub_view == "listtemp"}
            <div class="pull-right flip">
                <a href="{$system['system_url']}/class/{$username}/reports/addtemp" class="btn btn-default">
                    <i class="fa fa-plus"></i> {__("Add new template")}
                </a>
            </div>
        {elseif $sub_view == "addtemp" || $sub_view == "edittemp"}
            <div class="pull-right flip">
                <a href="{$system['system_url']}/class/{$username}/reports/listtemp" class="btn btn-default">
                    <i class="fa fa-list-ul"></i> {__("List template")}
                </a>
            </div>
        {elseif $sub_view == "add" || $sub_view == "edit" || $sub_view == "detail"}
            <div class="pull-right flip">
                <a href="{$system['system_url']}/class/{$username}/reports" class="btn btn-default">
                    <i class="fa fa-list-ul"></i> {__("Lists")}
                </a>
            </div>
        {/if}
        <div class="pull-right flip" style="margin-right: 5px">
            <a href="https://blog.coniu.vn/huong-dan-giao-vien-tao-lien-lac-tren-website-coniu/" class="btn btn-info btn_guide" target="_blank">
                <i class="fa fa-info-circle"></i> {__("Guide")}
            </a>
        </div>

        <i class="fa fa-life-ring fa-fw fa-lg pr10"></i>
        {__("Contact book")}
        {if $sub_view == "add"}
            &rsaquo; {__('Add New')}
        {elseif $sub_view == "listtemp"}
            &rsaquo; {__('List template')}
        {elseif $sub_view == "addtemp"}
            &rsaquo; {__('Add new template')}
        {elseif $sub_view == "detail"}
            &rsaquo; {__('Detail')}
        {elseif $sub_view == "detailtemp"}
            &rsaquo; {__('Detail template')}
        {elseif $sub_view == "edit"}
            &rsaquo; {__('Edit')}
        {elseif $sub_view == "edittemp"}
            &rsaquo; {__('Edit template')}
        {/if}
    </div>
    {if $sub_view == ""}
        <div class="panel-body with-table form-horizontal">
            <div class="form-group">
                <label class="col-sm-3 control-label text-left">{__("Child")}</label>
                <div class="col-sm-4">
                    <select name="time" id="report_child_search" class="form-control" data-username="{$username}" data-handle="child_search">
                        <option value="">{__("Select student")}...</option>

                        {$idx = 1}
                        {$child_status = -1}
                        {foreach $children as $child}
                            {if ($child_status >= 0) && ($child_status != $child['child_status'])}
                                <option value="" disabled style="color: blue">----------{__("Trẻ đã nghỉ học")}----------</option>
                            {/if}
                            <option value="{$child['child_id']}">{$idx} - {$child['child_name']} - {$child['birthday']}</option>
                            {$child_status = $child['child_status']}
                            {$idx = $idx + 1}
                        {/foreach}
                    </select>
                </div>
            </div>
            <div class = "table-responsive" id ="report_list_child">
                {include file="ci/class/ajax.reportchild.tpl"}
            </div>
        </div>
    {elseif $sub_view == "detail"}
        <div class="panel-body with-table">
            <table class = "table table-bordered">
                <tbody>
                <tr>
                    <td class = "col-sm-3 text-right">{__('Title')}</td>
                    <td>
                        <strong>{$data['report_name']}</strong>
                    </td>
                </tr>
                <tr>
                    <td class = "col-sm-3 text-right">{__('Child')}</td>
                    <td>
                        <strong>{$child['child_name']} - {$child['birthday']}</strong>
                    </td>
                </tr>
                <tr>
                    <td class = "col-sm-3 text-right">{__('Class')}</td>
                    <td>
                        <strong>{$class_name}</strong>
                    </td>
                </tr>
                <tr>
                    <td class = "col-sm-3 text-right">{__('Notification')}</td>
                    <td>
                        {if $data['is_notified']}
                            {__('Notified')}
                        {else}
                            {__('Not notified yet')}
                        {/if}
                    </td>
                </tr>
                <tr>
                    <td class = "col-sm-3 text-right">{__("File attachment")}</td>
                    <td>
                        {if !is_empty($data['source_file'])}
                            <a href = "{$data['source_file']}" target="_blank"><strong>
                                    {__("File attachment")}
                                </strong>
                            </a>
                        {else}
                            {__("No file attachment")}
                        {/if}
                    </td>
                </tr>
                </tbody>
            </table>
            <div class = "table-responsive">
                <table class = "table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th> # </th>
                        <th> {__('Title')} </th>
                        <th>{__('Content')} </th>
                    </tr>
                    </thead>
                    <tbody>
                    {foreach $data['details'] as $k => $row}
                        <tr>
                            <td align="center">
                                <strong>{$k + 1}</strong>
                            </td>
                            <td>
                                <strong>{$row['report_category_name']}</strong>
                            </td>
                            <td>
                                <div class="mb10">
                                    {$row['report_category_content']}
                                </div>
                                {foreach $row['multi_content'] as $suggest}
                                    <div class="form-group">
                                        <i class="fa fa-check" aria-hidden="true"></i> {$suggest}
                                    </div>
                                {/foreach}
                            </td>
                        </tr>
                    {/foreach}
                    </tbody>
                </table>
            </div>
            <div style="width:75%;margin-left: auto;margin-right: auto">
                <strong style="float: right">{__("Status ")} :
                    {if $status == 'Pass' }
                        <strong style="color:lawngreen">{__({$status})}</strong>
                    {elseif $status == 'Fail'}
                        <strong style="color:red">{__({$status})}</strong>
                    {elseif $status == 'Re-exam'}
                        <strong style="color:orange">{__({$status})}</strong>
                    {else}
                        <strong>{__({$status})}</strong>
                    {/if}

                </strong>
                <strong>{__("Points list")}</strong>
                <table class="table table-striped table-bordered" style="z-index: 1">
                    <thead>
                    <tr bgcolor="#fff">
                        <th rowspan="2" class="pinned" style="padding: 8px 6px" id="width_no">#</th>
                        <th rowspan="2" nowrap="true" class="pinned"
                            style="padding: 8px 6px">{__("Subject name")}</th>
                        <th colspan="4">{__("Semester 1")}</th>
                        <th colspan="4">{__("Semester 2")}</th>
                    </tr>
                    <tr>
                        <th>M1</th>
                        <th>M2</th>
                        <th>M3</th>
                        <th>D1</th>
                        <th>M1</th>
                        <th>M2</th>
                        <th>M3</th>
                        <th>D2</th>
                    </tr>
                    </thead>
                    <tbody>
                    {$rowIdx = 1}
                    {foreach $rows as $k => $row}
                        <tr>
                            <td align="center" class="pinned">{$rowIdx}</td>

                            <td nowrap="true" class="pinned text-bold color-blue">
                                <strong>{$row['subject_name']}</strong>
                            </td>
                            {$rowIdx = $rowIdx + 1}
                            {foreach $subject_key as $key}
                                <td>{$row[strtolower($key)]}</td>
                            {/foreach}
                        </tr>
                    {/foreach}
                    {*                    Các điểm trung bình*}
                    <tr>
                        <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                            <strong>{__("Average monthly")}</strong>
                        </td>
                        <td>{number_format($children_point_avgs['a1'] ,2)}</td>
                        <td>{number_format($children_point_avgs['b1'] ,2)}</td>
                        <td>{number_format($children_point_avgs['c1'] ,2)}</td>
                        <td>{number_format($children_point_avgs['d1'] ,2)}</td>
                        <td>{number_format($children_point_avgs['a2'] ,2)}</td>
                        <td>{number_format($children_point_avgs['b2'] ,2)}</td>
                        <td>{number_format($children_point_avgs['c2'] ,2)}</td>
                        <td>{number_format($children_point_avgs['d2'] ,2)}</td>

                    </tr>
                    <tr>
                        <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                            <strong>{__("Average semesterly")}</strong>
                        </td>
                        <td colspan="3" style="text-align: center">{number_format($children_point_avgs['x1'] ,2)}</td>
                        <td></td>
                        <td colspan="3" style="text-align: center">{number_format($children_point_avgs['x2'] ,2)}</td>
                        <td></td>
                    </tr>
                    {*                    kết thúc kỳ*}
                    <tr>
                        <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                            <strong>{__("End semester")}</strong>
                        </td>
                        <td colspan="4" style="text-align: center">{number_format($children_point_avgs['e1'] ,2)}</td>
                        <td colspan="4" style="text-align: center">{number_format($children_point_avgs['e2'] ,2)}</td>
                    </tr>
                    {*                    kết thúc năm *}
                    <tr>
                        <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                            <strong>{__("End year")}</strong>
                        </td>
                        <td colspan="8" style="text-align: center">{number_format($children_point_avgs['y'] ,2)}</td>
                    </tr>
                    {*                    Nghỉ có phép*}
                    <tr>
                        <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                            <strong>{__("Absent has permission")}</strong>
                        </td>
                        <td colspan="8" style="text-align: center">{$child_absent['absent_true']}</td>
                    </tr>
                    {*                    nghỉ không phép*}
                    <tr>
                        <td colspan="2" nowrap="true" class="pinned text-bold color-blue" style="text-align: center">
                            <strong>{__("Absent without permission")}</strong>
                        </td>
                        <td colspan="8" style="text-align: center">{$child_absent['absent_false']}</td>
                    </tr>
                    </tbody>
                </table>

                <strong>{__("Re-Exam")}</strong>
                <table class="table table-striped table-bordered" style="z-index: 1;">
                    <thead>
                    <tr bgcolor="#fff">
                        <th rowspan="2" class="pinned" style="padding: 8px 6px" id="width_no">#</th>
                        <th rowspan="2" nowrap="true" class="pinned"
                            style="padding: 8px 6px">{__("Subject name")}
                        </th>
                        <th colspan="1">{__("Point")}</th>
                    </tr>
                    </thead>
                    <tbody>
                    {$rowIdx = 1}
                    {foreach $children_subject_reexams as $k => $row}
                        <tr>
                            <td align="center" class="pinned">{$rowIdx}</td>
                            <td nowrap="true" class="pinned text-bold color-blue">
                                <strong>{$row['name']}</strong>
                            </td>
                            <td style="text-align: center">{$row['point']}</td>
                            {$rowIdx = $rowIdx + 1}
                        </tr>
                    {/foreach}
                    <tr>
                        <td colspan="2" nowrap="true" class="pinned text-bold color-blue"
                            style="text-align: center">
                            <strong>{__("Result Re-exam")}</strong>
                        </td>
                        <td colspan="3" style="text-align: center">{number_format($result_exam ,2)}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="form-group pl5">
                <a class="btn btn-default" href="{$system['system_url']}/class/{$username}/reports">{__("Lists")}</a>
                <a class="btn btn-default" href="{$system['system_url']}/class/{$username}/reports/add">{__("Add New")}</a>
                {if !$data['is_notified']}
                    <button class="btn btn-default js_class-report-notify" data-handle="notify" data-username="{$username}" data-id="{$data['report_id']}">{__("Notify")}</button>
                {/if}
                <a href="{$system['system_url']}/class/{$username}/reports/edit/{$data['report_id']}" class="btn btn-default">{__("Edit")}</a>
                <button class="btn btn-danger js_class-delete-report" data-username="{$username}" data-id="{$data['report_id']}" data-handle = "delete_report">{__("Delete")}</button>
            </div>
        </div>
    {elseif $sub_view == "edit"}
        <div class="panel-body">
            <form class="form-horizontal" enctype="multipart/form-data" method="post" id="class_create_report">
                <input type="hidden" name="username" id="username" value="{$username}"/>
                <input type="hidden" name="report_id" value="{$data['report_id']}"/>
                <input type="hidden" name="do" value="edit"/>
                <div class = "form-group">
                    <label class="col-sm-3 control-label text-left">{__("Title")} (*)</label>
                    <div class="col-sm-9">
                        <input type="text" name = "title" required class="form-control" autofocus value = "{$data['report_name']}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Use template")}
                    </label>
                    <div class="col-sm-9">
                        <select name="report_template_id" id="report_template_id" data-username="{$username}" data-view="add" class="form-control">
                            <option value="">{__("Select template")}</option>
                            {foreach $templates as $temp}
                                <option value="{$temp['report_template_id']}">{$temp['template_name']}</option>
                            {/foreach}
                        </select>
                    </div>
                </div>
                <div class = "form-group">
                    <label class="col-sm-3 control-label text-left">
                        {__("Child")}
                    </label>
                    <div class = "col-sm-9">
                        <input type="text" class="form-control" value = "{$child['child_name']} - {$child['birthday']}" disabled>
                    </div>
                </div>
                <div class = "table-responsive" id = "notemplate">
                    {$idx = 1}
                    {foreach $data['details'] as $row}
                        <table class="table table-striped table-bordered table-hover" id = "addTempTable">
                            <tr>
                                <td><strong>{$idx} - {$row['report_category_name']}</strong></td>
                                <input type="hidden" name="report_category_ids[]" value="{$row['report_category_id']}">
                                <input type="hidden" name="report_category_name_{$row['report_category_id']}" value="{$row['category_name']}">
                            </tr>
                            <tr>
                                <td>
                                    <textarea class="col-sm-12 mt10 mb10 note" style="width: 100%" name="report_content_{$row['report_category_id']}">{$row['report_category_content']}</textarea>
                                    {foreach $row['template_multi_content'] as $suggest}
                                        <div class="col-sm-4">
                                            <input type="checkbox" value="{$suggest}" name="report_suggest_{$row['report_category_id']}[]" {if in_array($suggest, $row['multi_content'])}checked{/if}> {$suggest}
                                        </div>
                                    {/foreach}
                                </td>
                            </tr>
                        </table>
                        {$idx = $idx + 1}
                    {/foreach}
                </div>
                <div id = "template_detail">

                </div>
                <div class = "form-group">

                </div>
                <input type = "hidden" id = "is_file" name = "is_file" value = "1">
                <div class="form-group" id = "file_old">
                    <label class="rp-file col-sm-3 control-label text-left" style="padding-top: 0px">{__("File attachment")}</label>
                    <div class="col-sm-6">
                        {if !is_empty($data['source_file'])}
                            <a href="{$data['source_file']}" download="{$data['file_name']}">{$data['file_name']}</a>
                        {else} {__('No file attachment')}
                        {/if}
                    </div>
                </div>
                <div class = "form-group">
                    <label class="control-label col-sm-3">
                        {if is_empty($data['source_file'])}
                            {__("Choose file")}
                        {else}
                            {__("Choose file replace")}
                        {/if}
                    </label>
                    <div class = "col-sm-6">
                        <input type="file" name="file" id="file"/>
                    </div>
                    <a class = "delete_image btn btn-xs btn-danger text-left">{__('Delete')}</a>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Notify immediately")}?</label>
                    <div class="col-sm-9">
                        <div class="onoffswitch">
                            <input type="checkbox" name="notify_immediately" class="onoffswitch-checkbox" id="notify_immediately" {if $data['is_notified'] == 1}checked{/if}>
                            <label class="onoffswitch-label" for="notify_immediately"></label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                    </div>
                </div>

                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    {elseif $sub_view == "add"}
        <div class="panel-body">
            <div class="panel-body">
                {if count($templates) == 0}
                    <div class = "color_red" align="center">
                        <strong>{__("You must create a template before creating a contact book")}</strong>
                    </div>
                {else}
                    <form class="form-horizontal" enctype="multipart/form-data" method="post" id="class_create_report">
                        <input type="hidden" name="username" id="username" value="{$username}"/>
                        <input type="hidden" name="do" value="add"/>
                        <div class = "form-group">
                            <label class="col-sm-3 control-label text-left">{__("Title")} (*)</label>
                            <div class="col-sm-9">
                                <input type="text" name = "title" required class="form-control" autofocus>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label text-left">{__("Notify immediately")}?</label>
                            <div class="col-sm-9">
                                <div class="onoffswitch">
                                    <input type="checkbox" name="notify_immediately" class="onoffswitch-checkbox" id="notify_immediately" unchecked>
                                    <label class="onoffswitch-label" for="notify_immediately"></label>
                                </div>
                            </div>
                        </div>
                        <div class = "form-group">
                            <label class="col-sm-3 control-label text-left">
                                {__("Send comment to")} (*)
                            </label>
                            <div class = "col-sm-9">
                                <div class="col-sm-12">
                                    <input type="checkbox" id="select_all_child" style="margin-left: 5px;"><strong>{__("Select all")}</strong>
                                </div>
                                {foreach $children as $child}
                                    <div class="col-xs-12 col-sm-6"><input type="checkbox" name="child[]" class="checkbox_child" value="{$child['child_id']}"> <strong>{$child['child_name']} </strong>
                                        <br/>
                                        {__('Latest')}: {if !is_null($child['report_new'])}{tosysDate($child['report_new'])}{else}{__("No information")}{/if} </div>
                                {/foreach}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label text-left">{__("Use template")}</label>
                            <div class="col-sm-9">
                                <select name="report_template_id" id="report_template_id" data-username="{$username}" data-view="add" class="form-control">
                                    <option value="">{__("Select template")}</option>
                                    {foreach $templates as $temp}
                                        <option value="{$temp['report_template_id']}">{$temp['template_name']}</option>
                                    {/foreach}
                                </select>
                            </div>
                        </div>
                        <div id = "template_detail">
                            {include file="ci/ajax.reporttemplatedetail.tpl"}
                        </div>

                        <div class = "form-group"></div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label text-left">{__("File attachment")}</label>
                            <div class="col-sm-6">
                                <input type="file" name="file" id="file"/>
                            </div>
                            <div class="col-sm-3">
                                <a class = "delete_image btn btn-xs btn-danger text-left">{__('Delete')}</a>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-9 col-sm-offset-3">
                                <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                            </div>
                        </div>

                        <!-- success -->
                        <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                        <!-- success -->

                        <!-- error -->
                        <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                        <!-- error -->
                    </form>
                {/if}
            </div>
        </div>
    {elseif $sub_view == "listtemp"}
        <div class="panel-body with-table">
            <div class = "table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr><th colspan="5">{__("Contact book template list")}&nbsp;({$results|count})</th></tr>
                    <tr>
                        <th>{__("#")}</th>
                        <th>{__("Title")}</th>
                        <th>{__("Scope")}</th>
                        <th>{__("Created time")}</th>
                        <th>{__("Actions")}</th>
                    </tr>
                    </thead>
                    <tbody>
                    {$idx = 1}
                    {foreach $results as $row}
                        <tr>
                            <td class="align-middle">
                                <center>{$idx}</center>
                            </td>
                            <td class="align-middle">
                                <a href="{$system['system_url']}/class/{$username}/reports/edittemp/{$row['report_template_id']}">
                                    {$row['template_name']}
                                </a>
                            </td>
                            <td class="align_middle" align="center">
                                {if $row['class_id'] != 0}
                                    {foreach $classes as $class}
                                        {if $class['group_id'] == $row['class_id']}
                                            {$class['group_title']}
                                        {/if}
                                    {/foreach}
                                {elseif $row['class_level_id'] != 0}
                                    {foreach $class_levels as $level}
                                        {if $level['class_level_id'] == $row['class_level_id']}
                                            {$level['class_level_name']}
                                        {/if}
                                    {/foreach}
                                {else}
                                    {__("School")}
                                {/if}
                            </td>
                            <td align="center" class="align_middle">
                                {$row['created_at']}
                            </td>
                            <td class="align-middle" align="center">
                                {if $row['level'] != 3}
                                    <a href="{$system['system_url']}/class/{$username}/reports/edittemp/{$row['report_template_id']}" class="btn btn-xs btn-default">{__("Detail")}</a>
                                {/if}
                                {if $row['level'] == 3}
                                    <a href="{$system['system_url']}/class/{$username}/reports/edittemp/{$row['report_template_id']}" class="btn btn-xs btn-default">{__("Edit")}</a>
                                    <button class="btn btn-xs btn-danger js_class-delete" data-handle="delete_template" data-username="{$username}" data-id="{$row['report_template_id']}">{__("Delete")}</button>
                                {/if}
                            </td>
                        </tr>
                        {$idx = $idx + 1}
                    {/foreach}
                    </tbody>
                </table>
            </div>
        </div>
    {elseif $sub_view == "edittemp"}
        <div class="panel-body with-table">
            <div id="open_dialog" class="x-hidden" title="{__("Category suggests")|upper}">
                {include file="ci/class/ajax.class.categorydetail.tpl"}
            </div>
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/class/boclass_report.php">
                <input type="hidden" name="username" id="username" value="{$username}"/>
                <input type="hidden" name="report_template_id" value="{$data['report_template_id']}"/>
                <input type="hidden" name="do" value="edit_temp"/>
                <div class = "form-group">
                    <label class="col-sm-3 control-label text-left">{__("Template name")} (*)</label>
                    <div class="col-sm-9">
                        <input class="form-control" name="template_name" required maxlength="300" value = "{$data['template_name']}">
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>
                                {__('#')}
                            </th>
                            <th align="center">
                                <input type="checkbox" id="select_all" style="float: left">{__('Category')}
                            </th>
                            <th>
                                {__('Content')}
                            </th>
                            <th>
                                {__('Category suggest')}
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        {if count($categorysTemp) > 0}
                            {$idx = 1}
                            {foreach $categorysTemp as $category}
                                {if ($category['checked'] && $data['level'] != CLASS_LEVEL) || $data['level'] == CLASS_LEVEL}
                                    <tr>
                                        <td align="center" class = "align_middle">{$idx}</td>
                                        <td align="left" class="align_middle">
                                            <input type = "checkbox" name = "category_ids[]" value="{$category['report_template_category_id']}" {if $category['checked']}checked{/if}>
                                            {$category['category_name']}
                                        </td>
                                        <td>
                                            <textarea type="text" class="note" name="content_{$category['report_template_category_id']}" style="width: 100%; resize: vertical!important;">{$category['template_content']}</textarea>
                                        </td>
                                        <td align="center" class="align_middle">
                                            <a class="btn btn-default btn-xs js_class-category-detail" data-id="{$category['report_template_category_id']}">{__("Category suggests")}</a>
                                        </td>
                                    </tr>
                                {/if}
                                {$idx = $idx + 1}
                            {/foreach}
                            <tr id="category_new_pm"></tr>
                        {else}
                            <tr>
                                <td colspan="5" align="center"><strong>{__("No category")}</strong></td>
                            </tr>
                        {/if}
                        </tbody>
                    </table>
                    {if $data['level'] == 3}
                        <div class = "col-sm-12">
                            <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                        </div>
                    {else}
                        <div class="color_red" align="center">
                            <strong>{__("You not permission edit this template")}</strong>
                        </div>
                    {/if}
                </div>
                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->

                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>
        </div>
    {elseif $sub_view == "addtemp"}
        <div class="panel-body">
            <div id="open_dialog" class="x-hidden" title="{__("Category suggests")|upper}">
                {include file="ci/class/ajax.class.categorydetail.tpl"}
            </div>
            <form class="js_ajax-forms form-horizontal" data-url="ci/bo/class/boclass_report.php">
                <input type="hidden" name="username" id="username" value="{$username}"/>
                <input type="hidden" name="do" value="add_temp"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Title")} (*)</label>
                    <div class="col-sm-9">
                        <input class="form-control" name="template_name" required autofocus maxlength="100">
                    </div>
                </div>
                {*Danh sách hạng mục của trường*}
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>
                                {__('#')}
                            </th>
                            <th>
                                <input type="checkbox" id="select_all" style="float: left">{__('Category')}
                            </th>
                            <th>
                                {__('Content')}
                            </th>
                            <th>
                                {__('Category suggest')}
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        {if count($categorys) > 0}
                            {$idx = 1}
                            {foreach $categorys as $category}
                                <tr>
                                    <td align="center" class = "align_middle">{$idx}</td>
                                    <td class="align_middle">
                                        <input type = "checkbox" name = "category_ids[]" value="{$category['report_template_category_id']}">
                                        {$category['category_name']}
                                    </td>
                                    <td>
                                        <textarea type="text" class="note" name="content_{$category['report_template_category_id']}" style="width: 100%; resize: vertical!important;"></textarea>
                                    </td>
                                    <td align="center" class="align_middle">
                                        <a class="btn btn-default btn-xs js_class-category-detail" data-id="{$category['report_template_category_id']}">{__("Category suggests")}</a>
                                    </td>
                                </tr>
                                {$idx = $idx + 1}
                            {/foreach}
                            <tr id="category_new_pm"></tr>
                        {else}
                            <tr>
                                <td colspan="5" align="center"><strong>{__("No category")}</strong></td>
                            </tr>
                        {/if}
                        </tbody>
                    </table>
                    <div class = "col-sm-12">
                        <button type="submit" class="btn btn-primary padrl30t">{__("Save")}</button>
                    </div>
                </div>
                <!-- success -->
                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                <!-- success -->
                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->

            </form>
        </div>
    {/if}
</div>