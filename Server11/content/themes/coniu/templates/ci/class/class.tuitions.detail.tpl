<div class="panel-body with-table">
    <table class="table table-bordered">
        <tr>
            <td class="col-sm-3 text-right">{__("Month")}</td>
            <td>{$data['month']}</td>
        </tr>
        <tr>
            <td class="col-sm-3 text-right">{__("Paid")}</td>
            <td>{moneyFormat($data['paid_amount'])}/{moneyFormat($data['total_amount'])}&nbsp;{$smarty.const.MONEY_UNIT}
                &nbsp;({$data['paid_count']} {__("children")})
            </td>
        </tr>
        <tr>
            <td class="col-sm-3 text-right">{__("Description")}</td>
            <td>{$data['description']}</td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <a class="btn btn-default" href="{$system['system_url']}/class/{$username}/tuitions/history/{$data['tuition_id']}">{__("Used last month")}</a>
            </td>
        </tr>
    </table>
    <div class="table-responsive" id="children_list">
        <table class="table table-striped table-bordered table-hover">
            <thead>
            <tr>
                <th>
                    <div class="pull-left flip">{__("Children list")|upper}&nbsp;({$data['tuition_child']|count})</div>
                    <div class="pull-right flip">
                        {__("Total of class")}&nbsp;({$smarty.const.MONEY_UNIT})
                        <input type="text" class="text-right" value="{moneyFormat($data['total_amount'])}" readonly/>
                    </div>
                </th>
            </tr>
            </thead>
            <tbody>
            {$detailIdx = 1}
            {foreach $data['tuition_child'] as $child}
                <tr {if $child['status'] == 0}class="bg_not_paid"{else}class="bg_paid"{/if}>
                    <td>
                        <div style="color: #597BA5; display: inline;">
                            <strong>{$detailIdx} - {$child['child_name']}</strong>
                            {if isset($child['attendance_count']) && ($child['attendance_count'] > 0)}&nbsp;|&nbsp;
                                {__("Number of charged days last month")}:&nbsp;{$child['attendance_count']}
                            {/if}
                        </div>

                        {*Hiển thị thông tin điểm danh chi tiết của trẻ tháng trước*}
                        {if $schoolConfig['tuition_view_attandance'] && (count($child['attendances']) > 0)}
                            <table class="table table-striped table-bordered table-hover">
                                <tr>
                                    <td style="width: 90px"><strong>{__("Attendance")}</strong></td>
                                    {foreach $child['attendances'] as $attendance}
                                        <td style="padding: 5px" align="center"
                                                {if $attendance['status'] == $smarty.const.ATTENDANCE_COME_LATE}
                                                    bgcolor="#6495ed"
                                                {elseif $attendance['status'] == $smarty.const.ATTENDANCE_EARLY_LEAVE}
                                                    bgcolor="#008b8b"
                                                {elseif $attendance['status'] == $smarty.const.ATTENDANCE_ABSENCE_NO_REASON}
                                                    bgcolor="#ff1493"
                                                {elseif $attendance['status'] == $smarty.const.ATTENDANCE_ABSENCE}
                                                    bgcolor="#deb887"
                                                {/if}
                                        >{$attendance['display_date']}</td>
                                    {/foreach}
                                </tr>
                            </table>
                        {/if}
                        {if count($child['tuition_detail']) > 0}
                            <table class="table table-striped table-bordered table-hover bg_white">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>{__("Fee name")}</th>
                                    <th>{__("Quantity")}</th>
                                    <th>{__("Unit price")}&nbsp;({$smarty.const.MONEY_UNIT})</th>
                                    <th>{__("Deduction")}</th>
                                    <th>{__("Money amount")}&nbsp;({$smarty.const.MONEY_UNIT})</th>
                                </tr>
                                </thead>
                                <tbody>
                                {*Hiển thị danh sách phí/dịch vụ của trẻ*}
                                {$idx = 1}
                                {foreach $child['tuition_detail'] as $detail}
                                    <tr>
                                        <td align="center">{$idx}</td>
                                        {if $detail['type'] == $smarty.const.TUITION_DETAIL_FEE}
                                            <td>{$detail['fee_name']}</td>
                                            <td class="text-center">{$detail['quantity']}</td>
                                            <td class="text-right">{moneyFormat($detail['unit_price'])}</td>
                                            {if $detail['fee_type'] != $smarty.const.FEE_TYPE_MONTHLY}
                                                <td class="text-center">{if $detail['quantity_deduction']!=0}{$detail['quantity_deduction']}({__('Day')}){/if}</td>
                                            {else}
                                                <td class="text-right">{if $detail['unit_price_deduction']!=0}{moneyFormat($detail['unit_price_deduction'])}({$smarty.const.MONEY_UNIT}){/if}</td>
                                            {/if}
                                        {elseif $detail['type'] == $smarty.const.TUITION_DETAIL_SERVICE}
                                            <td>{$detail['service_name']}</td>
                                            <td class="text-center">{$detail['quantity']}</td>
                                            <td class="text-right">{moneyFormat($detail['unit_price'])}</td>
                                            {if $detail['service_type'] == $smarty.const.SERVICE_TYPE_DAILY}
                                                <td class="text-center">{if $detail['quantity_deduction']!=0}{$detail['quantity_deduction']}({__('Day')}){/if}</td>
                                            {elseif $detail['service_type'] == $smarty.const.SERVICE_TYPE_MONTHLY}
                                                <td class="text-right">{if $detail['unit_price_deduction']!=0}{moneyFormat($detail['unit_price_deduction'])}({$smarty.const.MONEY_UNIT}){/if}</td>
                                            {else}
                                                <td class="text-center">{if $detail['quantity_deduction']!=0}{$detail['quantity_deduction']}({__('Times')}){/if}</td>
                                            {/if}
                                        {else}
                                            <td colspan="4">{__('Late PickUp')}</td>
                                        {/if}
                                        <td class="text-right">
                                            {if $detail['type'] != $smarty.const.LATE_PICKUP_FEE}
                                                {moneyFormat(($detail['quantity']  * $detail['unit_price'] - $detail['quantity_deduction'] * $detail['unit_price_deduction']))}
                                            {else}
                                                {moneyFormat($detail['unit_price'])}
                                            {/if}
                                        </td>
                                    </tr>
                                    {$idx = $idx + 1}
                                {/foreach}

                                {* Hiển thị nợ tháng trước *}
                                {if $child['debt_amount'] != 0}
                                    <tr>
                                        <td colspan="5" class="text-right">{__("Debt amount of previous month")}&nbsp;({$smarty.const.MONEY_UNIT})&nbsp;{$child['pre_month']}</td>
                                        <td class="text-right">{moneyFormat($child['debt_amount'])}</td>
                                    </tr>
                                {/if}
                                <tr>
                                    <td colspan="3">
                                        <div>
                                            {*Các button tương ứng với trạng thái thanh toán của học phí*}
                                            {if $child['status']==$smarty.const.TUITION_CHILD_NOT_PAID}
                                                {if $canEdit}
                                                    <button class="btn btn-xs btn-primary js_school-tuition" data-handle="confirm" data-username="{$username}"
                                                            data-amount="{$child['total_amount']}" data-tuition-id="{$data['tuition_id']}" data-child-id="{$child['child_id']}"
                                                            data-id="{$child['tuition_child_id']}">{__("Pay")}</button>
                                                    <input style="width: 100px;" type="number" step="1000" class="text-right" id="pay_amount_{$child['child_id']}" name="pay_amount" value="{$child['total_amount']}"/>
                                                {/if}
                                            {elseif $child['status']==$smarty.const.TUITION_CHILD_PARENT_PAID}
                                                {if $canEdit}
                                                    <button class="btn btn-xs btn-default js_school-tuition" data-handle="confirm" data-username="{$username}"
                                                            data-amount="{$child['total_amount']}" data-tuition-id="{$data['tuition_id']}"
                                                            data-child-id="{$child['child_id']}" data-id="{$child['tuition_child_id']}">{__("Confirm")}</button>
                                                    <button class="btn btn-xs btn-danger js_school-tuition" data-handle="unconfirm" data-username="{$username}"
                                                            data-child-id="{$child['child_id']}" data-tuition-id="{$data['tuition_id']}"
                                                            data-id="{$child['tuition_child_id']}">{__("Unconfirm")}</button>
                                                    <input style="width: 100px;" type="number" step="1000" class="text-right" id="pay_amount_{$child['child_id']}" name="pay_amount" value="{$child['total_amount']}"/>
                                                {/if}
                                            {else}
                                                {if $canEdit}
                                                    <button class="btn btn-xs btn-danger js_school-tuition" data-handle="unconfirm" data-username="{$username}"
                                                            data-child-id="{$child['child_id']}" data-tuition-id="{$data['tuition_id']}"
                                                            data-id="{$child['tuition_child_id']}">{__("Unconfirm")}</button>
                                                    <input type="hidden" id="pay_amount_{$child['child_id']}" name="pay_amount" value="{$child['total_amount']}"/>
                                                {/if}
                                                <strong>{__("Paid")}</strong>
                                            {/if}
                                        </div>
                                    </td>
                                    <td colspan="2" class="text-right"><strong>{__("Total")}&nbsp;({$smarty.const.MONEY_UNIT})</strong></td>
                                    <td class="text-right"><strong>{moneyFormat($child['total_amount'])}</strong></td>
                                </tr>
                                {* Hiển thị nợ tháng này *}
                                {if ($child['status']==$smarty.const.TUITION_CHILD_CONFIRMED) && (($child['total_amount'] - $child['paid_amount']) != 0)}
                                    <tr>
                                        <td colspan="5" class="text-right"><strong>{__("Paid")}&nbsp;({$smarty.const.MONEY_UNIT})</strong></td>
                                        <td class="text-right"><strong>{moneyFormat($child['paid_amount'])}</strong></td>
                                    </tr>
                                    <tr>
                                        <td colspan="5" class="text-right"><strong>{__("Debt amount of this month")}&nbsp;({$smarty.const.MONEY_UNIT})</strong></td>
                                        <td class="text-right"><strong>{moneyFormat($child['total_amount'] - $child['paid_amount'])}</strong></td>
                                    </tr>
                                {/if}
                                {if (isset($child['description']) && ({$child['description']} != ''))}
                                    <tr>
                                        <td colspan="2" class="text-right"><strong>{__("Description")}</strong></td>
                                        <td colspan="4">{$child['description']}</td>
                                    </tr>
                                {/if}
                                </tbody>
                            </table>
                        {/if}
                    </td>
                </tr>
                <tr height = "10"></tr>
                {$detailIdx = $detailIdx + 1}
            {/foreach}
            </tbody>
        </table>
    </div>
</div>
