<div class="text-left">
    <strong>{__("Total student")}: <font color="red">{$children|count}</font> |
        {__("Registered")}: <font color="red">{$child_count}</font>
    </strong>
</div>

<table class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th width="10%">#</th>
        <th width="15%">
            <input type="checkbox" id="childCheckAll" >&nbsp;{__("All")}
        </th>
        <th width="40%">{__("Children")}</th>
        <th width="35%">{__("Added to")}</th>
    </tr>
    </thead>
    <tbody>
    {$idx =1}
    {foreach $children as $child}
        <tr {if $child['child_status'] == 0} class="row-disable" {/if}>
            <td align="center">{$idx}</td>
            <td align="center">
                <input type="checkbox" class="child" name="childIds[]" value="{$child['child_id']}"
                        {if !is_empty($child['status'])}
                            checked
                        {/if}

                        {if array_key_exists($child['child_id'], $childrenRegistered)}
                            onclick="return false;" disabled
                        {/if}
                />
            </td>
            <td><strong class="ml20">{$child['child_name']}</strong></td>
            <td align="center"><strong>
                    {if !is_empty($child['class_name'])}
                        {$child['class_name']}
                    {elseif array_key_exists($child['child_id'], $childrenRegistered)}
                        {$childrenRegistered[$child['child_id']]}
                    {else}
                        &#150;
                    {/if}
            </strong></td>
            {*{if is_null($child['register'])}
                <td align="center">
                    <strong >&#150;</strong>
                </td>
            {else}
                <input type="hidden" name="registered_{$child['child_id']}" value="1">
                <input type="hidden" name="registered_user_id_{$child['child_id']}" value="{$child['register']['registered_user_id']}">
                <input type="hidden" name="registered_user_fullname_{$child['child_id']}" value="{$child['register']['registered_user_fullname']}">
                <td>
                    <i class="fa fa-check" style="color:mediumseagreen" aria-hidden="true"></i>
                    <strong class="pl5">{__("Dịch vụ")}</strong> : &nbsp;
                    {if $child['register']['service']|count > 0 }
                        {foreach $child['register']['service'] as $srv}
                            <input type="checkbox" class="registered_service" name="registered_service_{$child['child_id']}[]" value="{$srv['service_id']}" checked onclick="return false;"> {$srv['service_name']}
                            <input type="hidden" name="registered_service_fee_{$child['child_id']}[]" value="{$srv['price']}">
                        {/foreach}
                    {else}
                        <strong>{__("Không đăng ký")}</strong>
                    {/if}
                    <br/>
                    <strong class="pl20">{__("Ghi chú")}</strong> : &nbsp; {$child['register']['description']}
                </td>

            {/if}*}
        </tr>
        {$idx = $idx + 1}
    {/foreach}

    {if $idx == 1}
        <tr class="odd">
            <td valign="top" align="center" colspan="4" class="dataTables_empty">
                {__("No data available in table")}
            </td>
        </tr>
    {/if}
    </tbody>
</table>