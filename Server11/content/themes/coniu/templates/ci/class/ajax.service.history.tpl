{if $serviceId == 0}
    {*Danh sách dịch vụ sử dụng theo THÁNG và theo ĐIỂM DANH*}
    <div><strong>{__("Monthly and daily services")}</strong></div>
    <table class="table table-striped table-bordered table-hover">
        <thead>
        <tr>
            <th>#</th>
            <th>{__("Service name")}</th>
            <th>{__("Time")}</th>
        </tr>
        </thead>
        <tbody>
        {$idx = 1}
        {foreach $ncb_services as $row}
            <tr>
                <td align="center">{$idx}</td>
                <td align="center">{$row['service_name']}</td>
                <td align="center">
                    {if $row['begin'] != ''}
                        {$row['begin']}&nbsp;-&nbsp;{$row['end']}
                    {/if}
                </td>
            </tr>
            {$idx = $idx + 1}
        {/foreach}

        {if $idx == 1}
            <tr class="odd">
                <td valign="top" align="center" colspan="3" class="dataTables_empty">
                    {__("No data available in table")}
                </td>
            </tr>
        {/if}

        </tbody>
    </table>
{/if}

{*Bảng dịch vụ theo SỐ LẦN*}
<div><strong>{__("Count-based service")}&nbsp;({$history|count} {__("times")})</strong></div>
<table class="table table-striped table-bordered table-hover">
    {if $childId > 0}
        {*Lịch sử dụng của một trẻ*}
        <thead>
        <tr>
            <th>#</th>
            <th>{__("Using time")}</th>
            <th>{__("Registrar")}</th>
            <th>{__("Registered time")}</th>
        </tr>
        </thead>
        <tbody>
        {$idx = 1}
        {$serviceTotal = 0}
        {$serviceId = -1}
        {foreach $history as $row}
            {if $row['service_id'] != $serviceId}
                {if $idx > 1}
                    <tr>
                        <td colspan="2" align="center"><strong><font color="blue">{__("Total of service")}</font></strong></td>
                        <td colspan="2" align="left"><strong><font color="blue">{$serviceTotal} {__("times")}</font></strong></td>
                    </tr>
                {/if}
                <tr>
                    <td colspan="4"><strong>{$row['service_name']}</strong></</td>
                </tr>
                {$serviceTotal = 0}
            {/if}
            <tr>
                <td align="center">{$idx}</td>
                <td align="center">{$row['using_at']}</td>
                <td align="center">{$row['user_fullname']}</td>
                <td align="center">{$row['recorded_at']}</td>
            </tr>
            {$serviceId = $row['service_id']}
            {$serviceTotal = $serviceTotal + 1}
            {$idx = $idx + 1}
        {/foreach}
        {if $idx > 2}
            <tr>
                <td colspan="2" align="center"><strong><font color="blue">{__("Total of service")}</font></strong></td>
                <td colspan="2" align="left"><strong><font color="blue">{$serviceTotal} {__("times")}</font></strong></td>
            </tr>
            <tr>
                <td colspan="2" align="center"><strong><font color="red">{__("Total")|upper}</font></strong></td>
                <td colspan="2" align="left"><strong><font color="red">{$idx - 1} {__("times")}</font></strong></td>
            </tr>
        {/if}

        {if $history|count == 0}
            <tr class="odd">
                <td valign="top" align="center" colspan="4" class="dataTables_empty">
                    {__("No data available in table")}
                </td>
            </tr>
        {/if}

        </tbody>
    {else}
        {*Lịch sử dụng của CẢ LỚP*}
        <thead>
        <tr>
            <th>#</th>
            <th>{__("Full name")}</th>
            <th>{__("Registrar")}</th>
            <th>{__("Registered time")}</th>
        </tr>
        </thead>
        <tbody>
        {$idx = 1}
        {$dayTotal = 0}
        {$usingAt = 'noga'}
        {foreach $history as $row}
            {if $row['using_at'] != $usingAt}
                {if $idx > 1}
                    <tr>
                        <td colspan="2" align="center"><strong><font color="blue">{__("Total of day")}</font></strong></td>
                        <td colspan="2" align="left"><strong><font color="blue">{$dayTotal} {__("times")}</font></strong></td>
                    </tr>
                {/if}
                <tr>
                    <td colspan="4"><strong>{$row['using_at']}</strong></</td>
                </tr>
                {$dayTotal = 0}
            {/if}
            <tr {if $row['child_status'] == 0} class="row-disable" {/if}>
                <td align="center">{$idx}</td>
                <td>{$row['child_name']}</td>
                <td>{$row['user_fullname']}</td>
                <td>{$row['recorded_at']}</td>
            </tr>
            {$usingAt = $row['using_at']}
            {$dayTotal = $dayTotal + 1}
            {$idx = $idx + 1}
        {/foreach}
        {if $idx > 2}
            <tr>
                <td colspan="2" align="center"><strong><font color="blue">{__("Total of day")}</font></strong></td>
                <td colspan="2" align="left"><strong><font color="blue">{$dayTotal} {__("times")}</font></strong></td>
            </tr>
            <tr>
                <td colspan="2" align="center"><strong><font color="red">{__("Total")|upper}</font></strong></td>
                <td colspan="2" align="left"><strong><font color="red">{$idx - 1} {__("times")}</font></strong></td>
            </tr>
        {/if}

        {if $idx == 1}
            <tr class="odd">
                <td valign="top" align="center" colspan="4" class="dataTables_empty">
                    {__("No data available in table")}
                </td>
            </tr>
        {/if}
        </tbody>
    {/if}
</table>