<!-- add new child -->
<div class="panel panel-default">
    <div class="panel-heading">
        <div class="mt5">
            <strong> {__('Add new student')} </strong>
        </div>
    </div>
    <div class="panel-body">
        <div align="center" class="mb10">
            <strong>{__("Note: This funtion is only for parents")}</strong>
        </div>
        <form class="js_ajax-forms form-horizontal" data-url="ci/bo/child/bochild_child.php">
            <input type="hidden" name="school_username" id="school_username" value="{$username}"/>
            <input type="hidden" name="do" value="add"/>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left">{__("Full name")} (*)</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="last_name" id="last_name" placeholder="{__("Last name")}" required maxlength="34" autofocus>
                </div>
                <div class="col-sm-3">
                    <input type="text" class="form-control" name="first_name" id="first_name" placeholder="{__("First name")}" required maxlength="15">
                </div>
            </div>
            <div class = "form-group">
                <label class="col-sm-3 control-label text-left">{__("Nickname")}</label>
                <div class="col-sm-5">
                    <input type = "text" class="form-control" name="nickname" id="nickname" placeholder="{__("Nickname")}" maxlength="50">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left">{__("Pregnant")}</label>
                <div class="col-sm-9">
                    <div class="onoffswitch">
                        <input type="checkbox" name="child_pregnant" class="onoffswitch-checkbox" id="child_pregnant">
                        <label class="onoffswitch-label" for="child_pregnant"></label>
                    </div>
                </div>
            </div>
            <div id = "not_pregnant">
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Gender")} (*)</label>
                    <div class="col-sm-3">
                        <select name="gender" id="gender" class="form-control">
                            <option value="{$smarty.const.MALE}">{__("Male")}</option>
                            <option value="{$smarty.const.FEMALE}">{__("Female")}</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label text-left">{__("Birthdate")} (*)</label>
                    <div class='col-sm-5'>
                        <div class='input-group date' id='birthdate_picker'>
                            <input type='text' name="birthday" id="birthday" class="form-control" placeholder="{__("Birthdate")} (*)"/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class = "x-hidden" id = "is_pregnant">
                <div class = "form-group">
                    <label class = "col-sm-3 control-label text-left">{__("Time")}</label>
                    <div class="col-sm-3">
                        <input type = "number" name = "pregnant_week" id = "pregnant_week" class = "form-control" placeholder="{__("Week")}" min = "1" max="99">
                    </div>
                    <label class = "col-sm-1 control-label text-left">{__("Week")}(*)</label>
                </div>
                <div class = "form-group">
                    <label class="col-sm-3 control-label text-left">{__("Due date of childbearing")}</label>
                    <div class='col-sm-6'>
                        <div class='input-group date' id='due_date_picker'>
                            <input type='text' name="due_date_of_childbearing" id="due_date_of_childbearing" class="form-control" placeholder="{__("Due date of childbearing")}"/>
                            <span class="input-group-addon">
                                <span class="fas fa-calendar-alt"></span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label text-left">{__("Description")}</label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="description" id="description" placeholder="{__("Write about your child...")}" maxlength="300"></textarea>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-3 col-sm-offset-3">
                    <button type="submit" class="btn btn-primary padrl30">{__("Save")}</button>
                </div>
                <div class="col-sm-4">
                    <a href="#" class="btn btn-default js_add_child_clear">{__("Clear Data")}</a>
                </div>
            </div>

            <!-- success -->
            <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
            <!-- success -->

            <!-- error -->
            <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
            <!-- error -->
        </form>
    </div>
</div>