{include file='_head_new.tpl'}
{include file='_header_new.tpl'}

<!-- page content -->
<div class="container">
    <div class="row signup_row">
        <div class="col-md-4 col-md-offset-4">
            {if $system['social_login_enabled']}
                {if $system['facebook_login_enabled'] || $system['google_login_enabled']}
                    <h3 class="mb20 text-center">{__("Login with")}</h3>
                    <div class="mb20">
                        {if $system['facebook_login_enabled']}
                            <div class="col-sm-6">
                                <a href="{$system['system_url']}/connect/facebook" class="btn btn-block btn-social btn-facebook"><i class="fab fa-facebook-f"></i>{__("Facebook")}</a>
                            </div>
                        {/if}
                        {if $system['google_login_enabled']}
                            <div class="col-sm-6">
                                <a href="{$system['system_url']}/connect/google" class="btn btn-block btn-social btn-google"><i class="fa fa-google"></i>{__("Google")}</a>
                            </div>
                        {/if}
                    </div>
                {/if}
            {/if}

            <h3 class="mb20 text-center">{__("Sign up")}</h3>
            <form class="js_ajax-forms" data-url="core/signup.php">
                <div class="name_taila">
                    <div class="row row_dk">
                        <div class="col-xs-8 col_dk">
                            <div class="control-group">
                                <div class="controls">
                                    <input type="text" id="full_name" name="full_name" placeholder='{__("Full name")}' class="form-control" required/>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-4 col_dk">
                            <div class="sex_taila">
                                <div class="control-group">
                                    <div class="controls">
                                        <div class="form-group">
                                            <select name="gender" id="gender" class="form-control" required >
                                                <option value="female">{__("Female")}</option>
                                                <option value="male">{__("Male")}</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <input type="email" id="email" name="email" placeholder="Email" class="form-control" required />
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <input type="password" id="password" name="password" placeholder="{__('Password')}" class="form-control" required />
                    </div>
                </div>

                {if $system.reCAPTCHA_enabled}
                <div class="form-group">
                    <!-- reCAPTCHA -->
                    {if $system['language']['code'] == 'vi_VN'}
                        <script src='https://www.google.com/recaptcha/api.js?hl=vi'></script>
                    {else}
                        <script src='https://www.google.com/recaptcha/api.js'></script>
                    {/if}
                    <div class="g-recaptcha" data-sitekey="{$system.reCAPTCHA_site_key}"></div>
                    <!-- reCAPTCHA -->
                </div>
                {/if}
                <div class="dieukhoan">
                    <p>{__("By clicking Sign Up, you agree to our")} <a href="{$system['system_url']}/terms">{__("Terms")}</a></p>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="control-group  mar0">
                            <div class="controls-ci">
                                <button type="submit" class="btn btn-success">{__("Sign Up")}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- error -->
                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                <!-- error -->
            </form>


        </div>
    </div>
</div>
<!-- page content -->

{include file='_footer_new.tpl'}