<?php
/**
 * Hàm migrate db school_step
 *
 * @package ConIu v1
 * @author TaiLA
 */

//$path = str_replace('includes/ajax/ci/php_script/schedule', '', dirname(__FILE__));
//set_include_path($path);

// fetch bootstrap
require('bootstrap.php');

try {
    include_once(DAO_PATH . 'dao_school.php');

    $schoolDao = new SchoolDAO();

    // Lấy danh sách trường đang sử dụng coniu
    $schools = $schoolDao->getAllSchoolsUsingInet(1);
    $schoolIds = array();
    if(count($schools) > 0) {
        foreach ($schools as $school) {
            $schoolIds[] = $school['page_id'];
        }
        // update những trường này thành school_step = finish
        $schoolDao->updateSchoolStep($schoolIds, 100);
    }
}
catch (Exception $e) {
    $db->rollback();
    return_json( array('error' => true, 'message' => $e->getMessage()) );
} finally {
    $db->autocommit(true);
}


?>