<?php
/**
 * Quản lý các chức năng chính của module inet
 *
 * @package inet v1
 * @author TaiLA
 */

// set override_shutdown
$override_shutdown = true;

// fetch bootstrap
require('bootstrap.php');

// Kiểm tra user login rồi hay chưa
user_access();

include_once(DAO_PATH.'dao_school.php');
include_once(DAO_PATH.'dao_class.php');
include_once(DAO_PATH.'dao_teacher.php');
include_once(DAO_PATH.'dao_child.php');
include_once(DAO_PATH.'dao_event.php');
include_once(DAO_PATH.'dao_medicine.php');
include_once(DAO_PATH.'dao_tuition.php');
include_once(DAO_PATH.'dao_parent.php');
include_once(DAO_PATH.'dao_attendance.php');
include_once(DAO_PATH.'dao_service.php');
include_once(DAO_PATH.'dao_report.php');
include_once(DAO_PATH.'dao_feedback.php');
include_once(DAO_PATH.'dao_schedule.php');
include_once(DAO_PATH.'dao_class_level.php');
include_once(DAO_PATH.'dao_user.php');
include_once(DAO_PATH.'dao_pickup.php');
include_once(DAO_PATH.'dao_menu.php');
include_once(DAO_PATH.'dao_medical.php');
include_once(DAO_PATH.'dao_journal.php');
include_once(DAO_PATH.'dao_foetus_development.php');
include_once(DAO_PATH.'dao_foetus_knowledge.php');
include_once(DAO_PATH.'dao_child_development.php');
include_once(DAO_PATH.'dao_childmonth.php');

$parentDao = new ParentDAO();
$schoolDao = new SchoolDAO();
$classDao = new ClassDAO();
$teacherDao = new TeacherDAO();
$childDao = new ChildDAO();
$eventDao = new EventDAO();
$medicineDao = new MedicineDAO();
$tuitionDao = new TuitionDAO();
$attendanceDao = new AttendanceDAO();
$serviceDao = new ServiceDAO();
$reportDao = new ReportDAO();
$feedbackDao = new FeedbackDAO();
$scheduleDao = new ScheduleDAO();
$classLevelDao = new ClassLevelDAO();
$userDao = new UserDAO();
$pickupDao = new PickupDAO();
$menuDao = new MenuDAO();
$medicalDao = new MedicalDAO();
$journalDao = new JournalDAO();
$foetusDevelopmentDao = new FoetusDevelopmentDAO();
$childDevelopmentDao = new ChildDevelopmentDAO();
$childMonthDao = new ChildMonthDAO();

//Lấy danh sách CHILD được quản lý bởi 1 user
$children = $childDao->getChildrenForParent($user->_data['user_id']);

if (count($children) == 0) {
    _error(404);
}
$child = null;
if ((isset($_GET['child_parent_id']) && ($_GET['child_parent_id'] > 0))) {
    foreach ($children as $_child) {
        if ($_child['child_parent_id'] == $_GET['child_parent_id']) {
            $child = $_child;
            break;
        }
    }
} else {
    $child = $children[0];
}

$childInfo = $childDao->getFullInfoOfChildByParent($child['child_parent_id'], $child['child_id']);

$smarty->assign('children', $children);
$smarty->assign('child', $child);
$smarty->assign('childInfo', $childInfo);

// inet - Lấy ra danh sách schools, classes, children mà user quản lý
include_once('includes/ajax/ci/dao/dao_child.php');
$childDao = new ChildDAO();
$objects = getRelatedObjects();
// Lấy những trường đang sử dụng inet
$schoolUsing = array();
foreach ($objects['schools'] as $schoolOb) {
    if($schoolOb['school_status'] == SCHOOL_USING_CONIU) {
        $schoolUsing[] = $schoolOb;
    }
}

$smarty->assign('schoolList', $schoolUsing);

$classUsing = array();
foreach ($objects['classes'] as $classOb) {
    if($classOb['school_status'] == SCHOOL_USING_CONIU) {
        $classUsing[] = $classOb;
    }
}
$smarty->assign('classList', $classUsing);

$childrenOb = $childDao->getChildrenOfParent($user->_data['user_id']);

$smarty->assign('childrenList', $childrenOb);
// inet - END

// Cập nhật last_active
updateUserLastActive($user->_data['user_id']);

// Cập nhật users online
$today = date("Y-m-d");
updateUserOnline($user->_data['user_id']);

// page content
switch ($_GET['view']) {
    case 'childdetail':
        switch ($_GET['sub_view']) {
            case '':
                page_header(__("Child Management")." &rsaquo; ".__("Student information detail"));
                break;
            case 'edit':
                page_header(__("Child Management")." &rsaquo; ".__("Edit information detail"));
                break;
            case 'picker':
                page_header(__("Child Management")." &rsaquo; ".__("Update picker"));
                break;
            default:
                _error(404);
                break;
        }

        break;
    case 'health':
        // Tăng lượt tương tác - TaiLA
        addInteractive($child['school_id'], 'health', 'parent_view');
        switch ($_GET['sub_view']) {
            case '':
                page_header(__("Child Management")." &rsaquo; ".__("Health information"));
                if(!$child['is_pregnant']) {
                    // Lấy ngày hiện tại
                    $dateNow = date('Y-m-d');
                    $dateNowSys = toSysDate($dateNow);

                    // Lấy thông tin trẻ
                    $child = $childDao->getChildByParent($child['child_parent_id']);

                    // Lấy ngày sinh nhật của trẻ
                    $birthday = toDBDate($child['birthday']);

                    $begin = $child['birthday'];
                    $end = $dateNowSys;
                    // Khởi tạo mảng lưu chỉ số sức khỏe của trẻ
                    $childGrowth = array();

                    // Lấy dữ liệu từ ngày sinh đến ngày hiện tại
                    // Lấy dữ liệu nhập vào của trẻ
                    $childGrowth = $childDao->getChildGrowthSearchForChartBeginEnd($child['child_parent_id'], $begin, $end);

                    // Lấy ngày bắt đầu và ngày kết thúc
//                    $begin = toDBDate($begin);
//                    $end = toDBDate($end);
//
//                    $end_day = (strtotime($end) - strtotime($birthday)) / (60*60*24);
//                    $end_day = (int)$end_day;
//
//                    $begin_day = (strtotime($begin) - strtotime($birthday)) / (60*60*24);
//                    $begin_day = (int)$begin_day;

                    // Tính xem trẻ được bao nhiêu ngày tuổi
                    $day_age_now = (strtotime($dateNow) - strtotime($birthday)) / (60*60*24);

                    // đoạn này xử lý để lấy ngày bắt đầu (đặt tạm a, b, c; bí tên quá)
                    $a = $day_age_now % 30;
                    $b = $day_age_now - $a;
                    $c = $b - 30*10;
                    $dayGet = (int)$c;
                    $end_day = $day_age_now + 65;
                    if($day_age_now >= 300) {
                        $begin_day = $dayGet;
                    } else {
                        $begin_day = 0;
                    }
                    if($begin_day < 0) {
                        throw new Exception(__("You must select day begin after birthday"));
                    }

                    // Dữ liệu chuẩn người dùng nhập vào vẽ biểu đồ chiều cao (thư viện charts.js)
                    $heightForChartChartJS = getDataJsonForChartChartJS($childGrowth, $begin_day, $end_day, $birthday, 'height');

                    // Lấy dữ liệu người dùng nhập vào vẽ biểu đồ cân nặng (charts.js)
                    $weightForChartChartJS = getDataJsonForChartChartJS($childGrowth, $begin_day, $end_day, $birthday, 'weight');

                    if($child['gender'] == 'male') {
                        // Dữ liệu chuẩn
                        $hightStandardArray = getHightDataForChart($hightStandardJsonMale);
                        $weightStandardArray = getHightDataForChart($weightStandardJsonMale);
                    } else {
                        $hightStandardArray = getHightDataForChart($hightStandardJsonFeMale);
                        $weightStandardArray = getHightDataForChart($weightStandardJsonFeMale);
                    }

                    // 1. Chiều cao
                    // Lấy 2 ngày gần nhất của $begin_day có dữ liệu, nội suy chuẩn của ngày bắt đầu
                    if($begin_day < 1826) {
                        $hightStandardArray[$begin_day]['highest'] = getStandardDataBeginAndEnd($hightStandardArray, $begin_day, 'highest');

                        $hightStandardArray[$begin_day]['lowest'] = getStandardDataBeginAndEnd($hightStandardArray, $begin_day, 'lowest');
                    }
                    if($end_day <1826) {
                        // Lấy 2 ngày gần nhất của $end_day có dữ liệu, nội suy chuẩn của ngày kết thúc
                        $hightStandardArray[$end_day]['highest'] = getStandardDataBeginAndEnd($hightStandardArray, $end_day, 'highest');

                        $hightStandardArray[$end_day]['lowest'] = getStandardDataBeginAndEnd($hightStandardArray, $end_day, 'lowest');
                    }

                    // dỮ LIỆU đường cao nhất (thư viện charts.js)
                    $highestForChartChartsJS = getStandardDataForChartChartJS($hightStandardArray, $begin_day, $end_day, 'highest');

                    // Dữ liệu đường thấp nhất (thư viện charts.js)
                    $lowestForChartChartsJS = getStandardDataForChartChartJS($hightStandardArray, $begin_day, $end_day, 'lowest');

                    // 2. Cân nặng
                    // Lấy 2 ngày gần nhất của $begin_day có dữ liệu, nội suy chuẩn của ngày bắt đầu
                    if($begin_day < 1826) {
                        $weightStandardArray[$begin_day]['highest'] = getStandardDataBeginAndEnd($weightStandardArray, $begin_day, 'highest');

                        $weightStandardArray[$begin_day]['lowest'] = getStandardDataBeginAndEnd($weightStandardArray, $begin_day, 'lowest');
                    }

                    // Lấy 2 ngày gần nhất của $end_day có dữ liệu, nội suy chuẩn của ngày kết thúc
                    if($end_day < 1826) {
                        $weightStandardArray[$end_day]['highest'] = getStandardDataBeginAndEnd($weightStandardArray, $end_day, 'highest');

                        $weightStandardArray[$end_day]['lowest'] = getStandardDataBeginAndEnd($weightStandardArray, $end_day, 'lowest');
                    }

                    // Dữ liệu đường cao nhất (charts.js)
                    $highestWeightForChartChartJS = getStandardDataForChartChartJS($weightStandardArray, $begin_day, $end_day, 'highest');

                    // Dữ liệu đường thấp nhất (charts.js)
                    $lowestWeightForChartChartJS = getStandardDataForChartChartJS($weightStandardArray, $begin_day, $end_day, 'lowest');

                    // 3. BMI (charts.js)
                    $bmiForChartChartJS = getDataJsonForChartChartJS($childGrowth, $begin_day, $end_day, $birthday, 'bmi');

                    // Chiều cao for charts.js
                    $smarty->assign('highestForChartChartJS', $highestForChartChartsJS);
                    $smarty->assign('lowestForChartChartJS', $lowestForChartChartsJS);
                    $smarty->assign('heightForChartChartJS', $heightForChartChartJS);

                    //Cân nặng cho charts.js
                    $smarty->assign('highestWeightForChartChartJS', $highestWeightForChartChartJS);
                    $smarty->assign('lowestWeightForChartChartJS', $lowestWeightForChartChartJS);
                    $smarty->assign('weightForChartChartJS', $weightForChartChartJS);

                    // BMI (charts.js)
                    $smarty->assign('bmiForChartChartJS', $bmiForChartChartJS);

                    $smarty->assign('child', $child);
                    $smarty->assign('growth', array_reverse($childGrowth));

                    $label = '';
                    for($i = $begin_day; $i <= $end_day; $i++) {
                        if($i%30 == 0) {
                            $month = $i/30;
                            $label .= ' ' .$month. ',';
                        } else {
                            $label .= ' ,';
                        }
                    }
                    $smarty->assign('label', $label);

//                    print_r($highestForChartChartsJS);
//                    echo "<br/>";
//                    print_r($label);
//                    die;
                } else {
                    // Lấy thông tin ngày bắt đầu mang thai của thai nhi
                    $foetus_begin_date = $child['foetus_begin_date'];

                    // Lấy thông tin sức khỏe của thai nhi
                    $childFoetusGrowthForChar = $childDao->getChildFoetusHealthForChart($child['child_parent_id']);

                    // Lấy dữ liệu chuẩn
                    $foetusStandardArr = getFoetusDataForChart($foetusStandard);

                    // 1. Chiều cao
                    // Dữ liệu chuẩn vẽ biểu đồ (json)
                    $heightStandardForChart = getFoetusStandardDataForChartChartJS($foetusStandardArr, 'height');
//                    print_r($heightStandardForChart);
//                    die();
                    // Dữ liệu người dùng nhập vào vẽ biểu đồ
                    $heightForChart = getFoetusDataJsonForChartChartJS($childFoetusGrowthForChar, $foetus_begin_date, 'height');

                    // 2. Cân nặng
                    // Dữ liệu chuẩn vẽ biểu đồ (json)
                    $weightStandardForChart = getFoetusStandardDataForChartChartJS($foetusStandardArr, 'weight');
                    // Dữ liệu người dùng nhập vào vẽ biểu đồ
                    $weightForChart = getFoetusDataJsonForChartChartJS($childFoetusGrowthForChar, $foetus_begin_date, 'weight');

                    // assign
                    $smarty->assign('heightStandardForChart', $heightStandardForChart);
                    $smarty->assign('heightForChart', $heightForChart);

                    // Cân nặng
                    $smarty->assign('weightStandardForChart', $weightStandardForChart);
                    $smarty->assign('weightForChart', $weightForChart);

                    $smarty->assign('child', $child);
                    $smarty->assign('childFoetusGrowth', array_reverse($childFoetusGrowthForChar));

                    // Set label
                    $label = '';
                    for($i = 50; $i <= 274; $i++) {
                        if($i % 7 == 0) {
                            $week = $i/7;
                            $label .= ' ' . $week . ',';
                        } else {
                            $label .= ' ,';
                        }

                    }
                    $smarty->assign('label', $label);
                }
            case 'addgrowth':
                page_header(__("Child Management")." &rsaquo; ".__("Add information child growth"));
                break;
            case 'addfoetus':
                page_header(__("Child Management")." &rsaquo; ".__("Add information foetus health"));
                break;
            case 'editfoetus':
                page_header(__("Child Management")." &rsaquo; ".__("Edit information foetus health"));
                if(!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }
                $data = $childDao->getChildFoetusHealthById($_GET['id']);
                $smarty->assign('data', $data);
                break;
            case 'editgrowth':
                page_header(__("Child Management")." &rsaquo; ".__("Edit information foetus health"));
                if(!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }
                $data = $childDao->getChildGrowthById($_GET['id']);
                $smarty->assign('data', $data);
                break;
            default:
                _error(404);
                break;
        }
        break;
    case 'medicals':
        switch ($_GET['sub_view']) {
            case '':
                page_header(__("Child Management")." &rsaquo; ".__("Medical report book"));
                if(!$child['is_pregnant']) {
                    // Lấy năm hiện tại
                    $yearNow = date('Y');
                    $results = $medicalDao->getChildMedicalSearch($child['child_parent_id'], $yearNow);
                    $smarty->assign('results', $results);
                    // Vẽ biểu đồ
                    $medicalsChart = $medicalDao->getChildMedicalSearchForChart($child['child_parent_id'], $yearNow);

                    $jan = 0;
                    $feb = 0;
                    $mar = 0;
                    $apr = 0;
                    $may = 0;
                    $jun = 0;
                    $jul = 0;
                    $aug = 0;
                    $sep = 0;
                    $oct = 0;
                    $nov = 0;
                    $dec = 0;
                    foreach ($medicalsChart as $medical) {
                        if($medical == "01") {
                            $jan++;
                        } else if($medical == "02") {
                            $feb++;
                        } else if($medical == "03") {
                            $mar++;
                        } else if($medical == "04") {
                            $apr++;
                        } else if($medical == "05") {
                            $may++;
                        } else if($medical == "06") {
                            $jun++;
                        } else if($medical == "07") {
                            $jul++;
                        } else if($medical == "08") {
                            $aug++;
                        } else if($medical == "09") {
                            $sep++;
                        } else if($medical == "10") {
                            $oct++;
                        } else if($medical == "11") {
                            $nov++;
                        } else {
                            $dec++;
                        }
                    }
                    $count_month = array(
                        array(
                            'month' => __("Jan"),
                            'count' => $jan
                        ),
                        array(
                            'month' => __('Feb'),
                            'count' => $feb
                        ),
                        array(
                            'month' => __('Mar'),
                            'count' => $mar
                        ),
                        array(
                            'month' => __('Apr'),
                            'count' => $apr
                        ),
                        array(
                            'month' => __('May'),
                            'count' => $may
                        ),
                        array(
                            'month' => __('Jun'),
                            'count' => $jun
                        ),
                        array(
                            'month' => __('Jul'),
                            'count' => $jul
                        ),
                        array(
                            'month' => __('Aug'),
                            'count' => $aug
                        ),
                        array(
                            'month' => __('Sep'),
                            'count' => $sep
                        ),
                        array(
                            'month' => __('Oct'),
                            'count' => $oct
                        ),array(
                            'month' => __('Nov'),
                            'count' => $nov
                        ),
                        array(
                            'month' => __('Dec'),
                            'count' => $dec
                        ),

                    );
                    $chart_data = "";
                    $label = '';
                    $medicalsForChart = '';

                    foreach ($count_month as $row) {
                        $label .= " '" . $row['month'] ."',";
                        $medicalsForChart .= ' ' . $row['count'] . ',';
                    }
                    $label = trim($label, ',');
                    $medicalsForChart = trim($medicalsForChart, ',');
                    $smarty->assign('yearNow', $yearNow);
                    $smarty->assign('label', $label);
                    $smarty->assign('medicalsForChart', $medicalsForChart);
                }
                break;
            case 'edit':
                page_header(__("Child Management")." &rsaquo; ".__("Edit medical report book information"));
                if(!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }
                $data = $medicalDao->getChildMedical($_GET['id']);

                $smarty->assign('data', $data);
                break;
            case 'add':
                page_header(__("Child Management")." &rsaquo; ".__("Edit information health"));
                break;
            default:
                _error(404);
                break;
        }
        break;
    case 'journals':
        // Tăng lượt tương tác - TaiLA
        addInteractive($child['school_id'], 'diary', 'parent_view');
        switch ($_GET['sub_view']) {
            case '':
                page_header(__("Child Management")." &rsaquo; ".__("Diary corner"));
                if(!$child['is_pregnant']) {
                    $journals = $journalDao->getAllJournalChild($child['child_parent_id']);
                    $smarty->assign('results', $journals);
                    $smarty->assign('child_parent_id', $child['child_parent_id']);
                }
                break;
            case 'edit':
                page_header(__("Child Management")." &rsaquo; ".__("Edit journal corner"));
                if(!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }
                $data = $journalDao->getJournalById($_GET['id']);

                $smarty->assign('data', $data);
                break;
            case 'add':
                page_header(__("Child Management")." &rsaquo; ".__("Add journal"));
                break;
            default:
                _error(404);
                break;
        }
        break;
    case 'foetusdevelopments':
        switch ($_GET['sub_view']) {
            case '':
                page_header(__("Child Management")." &rsaquo; ".__("Too development"));
                if($child['is_pregnant']) {
                    $foetus_info = $foetusDevelopmentDao->getFoetusCheckUpInfoForParent();
                    $smarty->assign('foetus_info', $foetus_info);
                }
                break;
            case 'referion':
                page_header(__("Child Management")." &rsaquo; ".__("Too development"));
                if($child['is_pregnant']) {
                    $foetus_info = $foetusDevelopmentDao->getFoetusKnowInfoForParent();
                    $smarty->assign('foetus_info', $foetus_info);
                }
                break;
            case 'detail':
                page_header(__("Child Management")." &rsaquo; ".__("Too development") ." &rsaquo; " . __("Detail"));
                if(!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }
                $data = $foetusDevelopmentDao->getFoetusInfoDetail($_GET['id']);

                $smarty->assign('data', $data);
                break;
            default:
                _error(404);
                break;
        }
        break;

    case 'childdevelopments':
        switch ($_GET['sub_view']) {
            case '':
                page_header(__("Child Management")." &rsaquo; ".__("Too development"));
                $child_development = $childDevelopmentDao->getChildDevelopmentForParent($child['child_parent_id']);
                $smarty->assign('child_development', $child_development);

                // Lấy danh sách sắp tới thời gian tiêm
                $child_vaccinating = $childDevelopmentDao->getChildDevelopmentVaccinating($child['child_parent_id']);
                $smarty->assign('child_vaccinating', $child_vaccinating);

                // Lấy danh sách đã lỡ
                $child_vaccination_history = $childDevelopmentDao->getChildDevelopmentVaccinationHistory($child['child_parent_id']);
                $smarty->assign('child_vaccination_history', $child_vaccination_history);

                break;
            case 'referion':
                page_header(__("Child Management")." &rsaquo; ".__("Referion information"));
                $child_development = $childDevelopmentDao->getChildDevelopmentInfoForParent($child['child_parent_id']);
                $smarty->assign('child_development', $child_development);

                // Lấy danh sách thông tin trong tương lai
                $info_future = $childDevelopmentDao->getChildDevelopmentInfoFuture($child['child_parent_id']);
                $smarty->assign('info_future', $info_future);

                // Lấy danh sách thông tin trong quá khứ
                $child_info_history = $childDevelopmentDao->getChildDevelopmentInfoHistory($child['child_parent_id']);
                $smarty->assign('child_info_history', $child_info_history);
                break;
            case 'detail':
                page_header(__("Child Management")." &rsaquo; ".__("Too development") ." &rsaquo; " . __("Detail"));
                if(!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }
                $data = $childDevelopmentDao->getChildDevelopmentDetail($_GET['id']);

                $smarty->assign('data', $data);
                break;
            default:
                _error(404);
                break;
        }
        break;
    case 'childmonths':
        switch ($_GET['sub_view']) {
            case '':
                page_header(__("Child Management")." &rsaquo; ".__("Month age information"));
                $childMonths = $childMonthDao->getAllChildMonth();
                $smarty->assign('childMonths', $childMonths);
                break;
            default:
                _error(404);
                break;
        }
        break;
    case 'pregnancys':
        switch ($_GET['sub_view']) {
            case '':
                page_header(__("Child Management")." &rsaquo; ".__("Pregnancy information"));
                $pregnancys = $childMonthDao->getAllPregnancys();
                $smarty->assign('pregnancys', $pregnancys);
                break;
            default:
                _error(404);
                break;
        }
        break;
    default:
        _error(404);
}

// assign variables
$smarty->assign('view', $_GET['view']);
$smarty->assign('sub_view', $_GET['sub_view']);

// page footer
page_footer("childinfo");

?>