<?php
/**
 * group
 * 
 * @package CINet v1.1
 * @author NOGA Co., Ltd
 */

// fetch bootstrap
require('bootstrap.php');

// user access
if(!$system['system_public']) {
	user_access();
}

// check username
if(is_empty($_GET['username']) || !valid_username($_GET['username'])) {
	_error(404);
}

//Tăng lượt tương tác với các group, topic của Coniu
if (in_array($_GET['username'], $noga_statistic_topic_keys)) {
    increaseTopicInteractive($_GET['username']);
}

try {

	// [1] get main group info
    $get_group = $db->query(sprintf("SELECT G.*, CL.school_id, GC.category_name as group_category_name FROM groups G 
                 LEFT JOIN ci_class_level CL ON (G.class_level_id > 0) AND (G.class_level_id = CL.class_level_id)
                 LEFT JOIN groups_categories GC ON G.group_category = GC.category_id
                 WHERE G.group_name = %s", secure($_GET['username']) )) or _error(SQL_ERROR_THROWEN);

    if($get_group->num_rows == 0) {
        _error(404);
    }
    $group = $get_group->fetch_assoc();
    /* check username case */
    if(strtolower($_GET['username']) == strtolower($group['group_name']) && $_GET['username'] != $group['group_name']) {
        redirect('/groups/'.$group['group_name']);
    }
    /* get group picture */
    $group['group_picture_default'] = ($group['group_picture'])? false : true;
    $group['group_picture'] = User::get_picture($group['group_picture'], 'group');
    /* check group category */
    $group['group_category_name'] = (!$group['group_category_name'])? __('N/A'): $group['group_category_name']; /* in case deleted by system admin */
    /* get the connection */
    $group['i_admin'] = $user->check_group_adminship($user->_data['user_id'], $group['group_id']);
    $group['i_joined'] = $user->check_group_membership($user->_data['user_id'], $group['group_id']);

    // ConIu - BEGIN
    $permission = null;
    if ($group['class_level_id'] > 0) {
        //Lấy ra thông tin trường
        include_once(DAO_PATH.'dao_school.php');
        $schoolDao = new SchoolDAO();
        $school = getSchoolData($group['school_id'], SCHOOL_INFO);
        if (is_null($school)) {
            _error(__("Error"), __("Class does not belong to an existing school"));
        }

        //Lấy ra danh sách giáo viên của lớp
        $teachers = getClassData($group['group_id'], CLASS_TEACHERS);

        //Kiểm tra vai trò của người dùng hiện tại với hệ thống
        $permission = checkSchoolPermission($school['page_name'], "classes");
		$permission = ($permission == PERM_EDIT)? PERM_VIEW: $permission;
        if ($user->_data['user_id'] == $school['page_admin']) {
			$permission = PERM_EDIT;
        } else {
            foreach($teachers as $teacher) {
                if ($user->_data['user_id'] == $teacher['user_id']) {
					$permission = PERM_EDIT;
                    break;
                }
            }
        }

		if ((!$group['i_joined']) && ($permission == PERM_NONE)) {
			_error(403);
		}

        $smarty->assign('school', $school);
        $smarty->assign('teachers', $teachers);
        $smarty->assign('permission', $permission);
    }
    /* Coniu - Tăng thành viên nhóm */
    $group['group_members'] = increase_members_groups($group['group_name'], $group['group_members']);
    // ConIu - END

    /* get group requests */
    if($group['group_privacy'] != "public") {
        $group['total_requests'] = $user->get_group_requests_total($group['group_id']);
    }

    // [2] get view content
    /* check group privacy */
    if($group['group_privacy'] == "secret") {
        if($group['i_joined'] != "approved" && !$group['i_admin']) {
            _error(404);
        }
    }
    if($group['group_privacy'] == "closed") {
        if($group['i_joined'] != "approved" && !$group['i_admin'] && $permission == PERM_NONE) {
            $_GET['view'] = 'members';
        }
    }

	// [2] get view content
	switch ($_GET['view']) {
		case '':
            /* get custom fields */
            //$smarty->assign('custom_fields', $user->get_custom_fields( array("for" => "group", "get" => "profile", "node_id" => $group['group_id']) ));

            /* get invites */
            $group['invites'] = $user->get_group_invites($group['group_id']);

            /* get photos */
            $group['photos'] = $user->get_photos($group['group_id'], 'group');

            /* get pinned post */
            $pinned_post = $user->get_post($group['group_pinned_post']);
            $smarty->assign('pinned_post', $pinned_post);

            /* prepare publisher */
            //$smarty->assign('market_categories', $user->get_market_categories());
            //$smarty->assign('feelings', get_feelings());
            //$smarty->assign('feelings_types', get_feelings_types());

            /* get posts */
            $posts = $user->get_posts( array('get' => 'posts_group', 'id' => $group['group_id']) );
            /* assign variables */
            $smarty->assign('posts', $posts);
            break;

        case 'export':
            /* get photos */
            $group['photos'] = $user->get_photos($group['group_id'], 'group');

            /* get pinned post */
            $pinned_post = $user->get_post($group['group_pinned_post']);
            $smarty->assign('pinned_post', $pinned_post);

            /* get posts */
            $posts = $user->get_posts( array('get' => 'posts_group', 'id' => $group['group_id']) );
            // Lấy danh sách giáo viên
            $teachers = getClassData($group['group_id'], CLASS_TEACHERS);
            $teacherIds = array_keys($teachers);
            $postLast = array();
            foreach ($posts as $k => $post) {
                if(($post['post_type'] == 'photos' || $post['post_type'] == 'album' || is_empty($post['post_type'])) && in_array($post['author_id'],$teacherIds)){
                    $postLast[] = $post;
                }
            }
            $posts = $postLast;
            $smarty->assign('posts', $posts);

            break;

        case 'photos':
            /* get photos */
            $group['photos'] = $user->get_photos($group['group_id'], 'group');
            break;

        case 'albums':
            /* get albums */
            $group['albums'] = $user->get_albums($group['group_id'], 'group');
            break;

        case 'album':
            /* get album */
            $album = $user->get_album($_GET['id']);
            if(!$album || ($album['group_id'] != $group['group_id']) ) {
                _error(404);
            }
            /* assign variables */
            $smarty->assign('album', $album);
            break;

        case 'members':
            /* get members */
            if($group['group_members'] > 0) {
                $group['members'] = $user->get_group_members($group['group_id']);
            }
            break;

        case 'invites':
            /* check if the viewer is a group member */
            if($group['i_joined'] != "approved") {
                _error(404);
            }
            /* get invites */
            $group['invites'] = $user->get_group_invites($group['group_id']);
            break;

		case 'settings':
            /* check if the viewer is the admin */
            if(!$group['i_admin']) {
                _error(404);
            }
            /* get sub_view content */
            $sub_view = $_GET['id'];
            switch ($sub_view) {
                case '':
                    /* get custom fields */
                    //$smarty->assign('custom_fields', $user->get_custom_fields( array("for" => "group", "get" => "settings", "node_id" => $group['group_id']) ));

                    /* get group categories */
                    $categories = $user->get_groups_categories();
                    /* assign variables */
                    $smarty->assign('categories', $categories);
                    break;

                case 'requests':
                    if($group['group_privacy'] == "public") {
                        _error(404);
                    }
                    /* get requests */
                    if($group['total_requests'] > 0) {
                        $group['requests'] = $user->get_group_requests($group['group_id']);
                    }
                    break;

                case 'members':
                    /* get admins */
                    $group['group_admins_count'] = count($user->get_group_admins_ids($group['group_id']));
                    $group['group_admins'] = $user->get_group_admins($group['group_id']);
                    $group['group_admin_ids'] = $user->get_group_admins_ids_by_school($group['group_id']);

                    /* get members */
                    if($group['group_members'] > 0) {
                        $group['members'] = $user->get_group_members($group['group_id'], 0, true);
                    }
                    break;

                case 'delete':
                    /* check if the viewer not the super admin */
                    if($user->_data['user_id'] != $group['group_admin']) {
                        _error(404);
                    }
                    break;

                default:
                    _error(404);
                    break;
            }
            /* assign variables */
            $smarty->assign('sub_view', $sub_view);
            break;
		
		default:
			_error(404);
			break;
	}

} catch (Exception $e) {
	_error(__("Error"), $e->getMessage());
}

// ConIu - Lấy ra danh sách schools, classes, children mà user quản lý
include_once('includes/ajax/ci/dao/dao_child.php');
$childDao = new ChildDAO();
$objects = getRelatedObjects();
// Lấy những trường đang sử dụng coniu
$schoolUsing = array();
foreach ($objects['schools'] as $school) {
    if($school['school_status'] == SCHOOL_USING_CONIU) {
        $schoolUsing[] = $school;
    }
}
$smarty->assign('schools', $schoolUsing);
$smarty->assign('classes', $objects['classes']);

$children = $childDao->getChildrenOfParent($user->_data['user_id']);
$smarty->assign('children', $children);
// ConIu - END

// page header
page_header($group['group_title'], $group['group_description']);

// assign variables
$smarty->assign('group', $group);
$smarty->assign('view', $_GET['view']);

// page footer
page_footer("group");

?>