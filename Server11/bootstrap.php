<?php
/**
 * bootstrap
 *
 * @package CINet v1.1
 * @author NOGA Co., Ltd
 */

// set absolut & base path
define('ABSPATH', dirname(__FILE__) . '/');
define('BASEPATH', dirname($_SERVER['PHP_SELF']));
define('DAO_PATH', ABSPATH . 'includes/ajax/ci/dao/'); //Coniu
define('APIBO_PATH', ABSPATH . 'includes/ajax/ci/api_bo/'); //Coniu

if (!file_exists(ABSPATH . 'includes/config.php')) {
    // get functions
    require_once(ABSPATH . 'includes/functions.php');

    /* the config file doesn't exist -> start the installer */
    redirect('/install');
} else {
    //$system = array();
    /* the config file exist -> start the system */
    // get system configurations
    require_once(ABSPATH . 'includes/config.php');

    /* Coniu - Khai báo constant */
    require_once(ABSPATH . 'includes/ajax/ci/constants.php');
    /* Thông số cấu hình cho từng môi trường */
    require_once(ABSPATH . 'includes/ajax/ci/configuration_parameters.php');

    // enviroment settings
    if (DEBUGGING) {
        ini_set("display_errors", true);
//        ini_set('memory_limit', '-1');
        error_reporting(E_ALL ^ E_NOTICE);
    } else {
        ini_set("display_errors", false);
//        ini_set('memory_limit', '-1');
        error_reporting(0);
    }

    // get functions
    require_once(ABSPATH . 'includes/functions.php');

    // start session
    session_start();
    /* set session secret */
    if(!isset($_SESSION['secret'])) {
        $_SESSION['secret'] = get_hash_token();
    }

// i18n config
    require_once(ABSPATH.'includes/libs/gettext/gettext.inc');
    T_setlocale(LC_MESSAGES, DEFAULT_LOCALE);
    $domain = 'messages';
    T_bindtextdomain($domain, ABSPATH .'content/languages/locale');
    T_bind_textdomain_codeset($domain, 'UTF-8');
    T_textdomain($domain);

    // connect to the database
    $db = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
    $db->set_charset('utf8');
    if (mysqli_connect_error()) {
        _error(DB_ERROR);
    }

    // -----------------------------------------------------------------------------------
    //Coniu - Khai báo mảng options luôn
    if (!DEBUGGING) {
        $system["ID"] = "1";
        $system["system_licence"] = "e71eec6fc0fdd4c2308b9a87b21a7d49";
        $system["system_kernel"] = "36.74.5857";
        $system["system_live"] = "1";
        $system["system_message"] = "";
        $system["system_public"] = "1";
        $system["system_title"] = "MobiEdu";
        $system["system_description"] = "Mạng xã hội Nhà trường dành cho các mẹ chia sẻ kiến thức, kinh nghiệm trong quá trình nuôi con, dạy con. Inet đồng thời là phần mềm quản lý nhà trường trực tuyến hỗ trợ công việc quản lý, tổng hợp hàng ngày của nhà trường; cung cấp công cụ kết nối thông minh giữa nhà trường – phụ huynh – cộng đồng";
        $system["system_keywords"] = "phần mềm quản lý Nhà trường, phan mem quan ly nha truong, phần mềm Nhà trường, phan mem nha truong, Inet, ứng dụng nhà trường, ung dung nha truong, quản lý nhà trường, quan ly nha truong, sổ liên lạc điện tử, so lien lac dien tu, kết nối nhà trường, ket noi nha truong";
        $system["system_uploads_directory"] = "content/uploads";
        $system["wall_posts_enabled"] = "0";
        $system["newsfeed_enabled"] = "1";
        $system["pages_enabled"] = "1";
        $system["groups_enabled"] = "1";
        $system["profile_notification_enabled"] = "0";
        $system["games_enabled"] = "0";
        $system["geolocation_enabled"] = "1";
        $system["registration_enabled"] = "1";
        $system["getting_started"] = "1";
        $system["delete_accounts_enabled"] = "1";
        $system["max_accounts"] = "0";
        $system["social_login_enabled"] = "1";
        $system["facebook_login_enabled"] = "1";
        //$system["facebook_appid"] = "240270499760428";
        $system["twitter_login_enabled"] = "0";
        $system["twitter_appid"] = "";
        $system["twitter_secret"] = "";
        $system["google_login_enabled"] = "1";
        $system["linkedin_login_enabled"] = "0";
        $system["linkedin_appid"] = "";
        $system["linkedin_secret"] = "";
        $system["vkontakte_secret"] = "";
        $system["vkontakte_login_enabled"] = "0";
        $system["vkontakte_appid"] = "";
        $system["activation_enabled"] = "1";
        $system["activation_type"] = "email";
        $system["email_smtp_enabled"] = "1";
        $system["email_smtp_authentication"] = "1";
        $system["email_smtp_server"] = "smtp.gmail.com";
        $system["email_smtp_port"] = "587";
        $system["email_smtp_username"] = "hotro.inet@gmail.com";
        $system["email_smtp_password"] = "inet^&*(";
        $system["chat_enabled"] = "1";
        $system["chat_status_enabled"] = "1";
        $system["uploads_prefix"] = "inet";
        $system["max_avatar_size"] = "15360";
        $system["max_cover_size"] = "15360";
        $system["photos_enabled"] = "1";
        $system["max_photo_size"] = "15360";
        $system["videos_enabled"] = "1";
        $system["max_video_size"] = "92160";
        $system["video_extensions"] = "mp4, mov";
        $system["audio_enabled"] = "1";
        $system["max_audio_size"] = "51200";
        $system["audio_extensions"] = "mp3, wav";
        $system["file_enabled"] = "1";
        $system["max_file_size"] = "15360";
        $system["file_extensions"] = "doc, docx, xls, xlsx, ppt, pptx, pdf, jpg, png, gif, txt, zip, jpeg";
        $system["file_report_extensions"] = "doc, docx, xls, xlsx, ppt, pptx, pdf, jpg, png, gif, txt, zip, jpeg";
        $system["file_medicine_extensions"] = "jpg, png, gif, jpeg";
        $system["censored_words_enabled"] = "1";
        $system["censored_words"] = "cặc,lồn,địt,đéo,đ&eacute;o,cứt,đít,đ&iacute;t,pussy,fuck,shit,asshole,dick,tits,boobs";
        $system["data_heartbeat"] = "5";
        $system["chat_heartbeat"] = "5";
        $system["offline_time"] = "10";
        $system["min_results"] = "5";
        $system["max_results"] = "10";
        $system["min_results_even"] = "6";
        $system["max_results_even"] = "12";
        $system["analytics_code"] = "";
        $system["system_logo"] = "";
        $system["system_favicon"] = "";
        $system["system_favicon_default"] = "1";
        $system["system_ogimage"] = "";
        $system["system_ogimage_default"] = "1";
        $system["css_customized"] = "0";
        $system["css_background"] = "";
        $system["css_link_color"] = "#193648";
        $system["css_header"] = "#193648";
        $system["css_header_search"] = "";
        $system["css_header_search_color"] = "";
        $system["css_btn_primary"] = "#193648";
        $system["css_menu_background"] = "#193648";
        $system["allow_user_post_on_wall"] = "0";
        $system['smart_yt_player'] = "1";
        $system['social_share_enabled'] = "1";
        $system['show_group_member'] = "0";
        $system['mail_contact'] = "contact@mobiedu.vn";

    } else {
        // get system options
        //$get_options = $db->query("SELECT * FROM system_options") or _error(SQL_ERROR);
        //$system = $get_options->fetch_assoc();
        $system["ID"] = "1";
        $system["system_licence"] = "e71eec6fc0fdd4c2308b9a87b21a7d49";
        $system["system_kernel"] = "36.74.5857";
        $system["system_live"] = "1";
        $system["system_message"] = "";
        $system["system_public"] = "1";
        $system["system_title"] = "MobiEdu";
        $system["system_description"] = "Mạng xã hội Nhà trường dành cho các mẹ chia sẻ kiến thức, kinh nghiệm trong quá trình nuôi con, dạy con. Inet đồng thời là phần mềm quản lý nhà trường trực tuyến hỗ trợ công việc quản lý, tổng hợp hàng ngày của nhà trường; cung cấp công cụ kết nối thông minh giữa nhà trường – phụ huynh – cộng đồng";
        $system["system_keywords"] = "phần mềm quản lý Nhà trường, phan mem quan ly nha truong, phần mềm Nhà trường, phan mem nha truong, Inet, ứng dụng nhà trường, ung dung nha truong, quản lý nhà trường, quan ly nha truong, sổ liên lạc điện tử, so lien lac dien tu, kết nối nhà trường, ket noi nha truong";
        $system["system_uploads_directory"] = "content/uploads";
        $system["wall_posts_enabled"] = "1";
        $system["pages_enabled"] = "1";
        $system["groups_enabled"] = "1";
        $system["profile_notification_enabled"] = "0";
        $system["games_enabled"] = "0";
        $system["geolocation_enabled"] = "1";
        $system["registration_enabled"] = "1";
        $system["getting_started"] = "1";
        $system["delete_accounts_enabled"] = "1";
        $system["max_accounts"] = "0";
        $system["social_login_enabled"] = "1";
        $system["facebook_login_enabled"] = "1";
        $system["twitter_login_enabled"] = "0";
        $system["twitter_appid"] = "";
        $system["twitter_secret"] = "";
        $system["google_login_enabled"] = "1";
        $system["linkedin_login_enabled"] = "0";
        $system["linkedin_appid"] = "";
        $system["linkedin_secret"] = "";
        $system["vkontakte_secret"] = "";
        $system["vkontakte_login_enabled"] = "0";
        $system["vkontakte_appid"] = "";
        $system["activation_enabled"] = "1";
        $system["activation_type"] = "email";
        $system["email_smtp_enabled"] = "1";
        $system["email_smtp_authentication"] = "1";
        $system["email_smtp_server"] = "smtp.gmail.com";
        $system["email_smtp_port"] = "587";
        $system["email_smtp_username"] = "hotro.inet@gmail.com";
        $system["email_smtp_password"] = "inet^&*(";
        $system["chat_enabled"] = "1";
        $system["chat_status_enabled"] = "1";
        $system["uploads_prefix"] = "inet";
        $system["max_avatar_size"] = "15360";
        $system["max_cover_size"] = "15360";
        $system["photos_enabled"] = "1";
        $system["max_photo_size"] = "15360";
        $system["videos_enabled"] = "1";
        $system["max_video_size"] = "92160";
        $system["video_extensions"] = "mp4, mov";
        $system["audio_enabled"] = "1";
        $system["max_audio_size"] = "51200";
        $system["audio_extensions"] = "mp3, wav";
        $system["file_enabled"] = "1";
        $system["max_file_size"] = "15360";
        $system["file_extensions"] = "doc, docx, xls, xlsx, pdf, jpg, png, gif, txt, zip, jpeg";
        $system["file_report_extensions"] = "doc, docx, xls, xlsx, pdf, jpg, png, gif, txt, zip, jpeg";
        $system["file_medicine_extensions"] = "jpg, png, gif,jpeg";
        $system["censored_words_enabled"] = "1";
        $system["censored_words"] = "cặc,lồn,địt,đéo,đ&eacute;o,cứt,đít,đ&iacute;t,pussy,fuck,shit,asshole,dick,tits,boobs";
        $system["data_heartbeat"] = "5";
        $system["chat_heartbeat"] = "5";
        $system["offline_time"] = "10";
        $system["min_results"] = "5";
        $system["max_results"] = "10";
        $system["min_results_even"] = "6";
        $system["max_results_even"] = "12";
        $system["analytics_code"] = "";
        $system["system_logo"] = "";
        $system["system_favicon"] = "";
        $system["system_favicon_default"] = "1";
        $system["system_ogimage"] = "";
        $system["system_ogimage_default"] = "1";
        $system["css_customized"] = "0";
        $system["css_background"] = "";
        $system["css_link_color"] = "#193648";
        $system["css_header"] = "#193648";
        $system["css_header_search"] = "";
        $system["css_header_search_color"] = "";
        $system["css_btn_primary"] = "#193648";
        $system["css_menu_background"] = "#193648";
        $system["allow_user_post_on_wall"] = "1";
        $system["ios_version"] = IOS_VERSION;
        $system['social_share_enabled'] = "1";
        $system['show_group_member'] = "0";
        $system['mail_contact'] = "contact@mobiedu.vn";
    }
    /* set system uploads */
    $system['system_uploads'] = $system['system_url'] . '/' . $system['system_uploads_directory'];
    //Coniu - Bỏ query từ db, fix cứng chạy cho nhanh
    /* get system languages */
    //$get_languages = $db->query("SELECT * FROM system_languages WHERE enabled = '1'") or _error(SQL_ERROR);
    $languages = [
        ["language_id" => 1, "code" => "en_us", "title" => "English", "flag_icon" => "us", "dir" => "LTR", "default" => 0, "enabled" => 1],
        ["language_id" => 2, "code" => "vi_VN", "title" => "Tiếng Việt", "flag_icon" => "vn", "dir" => "LTR", "default" => 1, "enabled" => 1],
        ["language_id" => 3, "code" => "km_KH", "title" => "Khmer (Cambodia)", "flag_icon" => "km", "dir" => "LTR", "default" => 0, "enabled" => 1]
    ];
    //while($language = $get_languages->fetch_assoc()) {
    foreach ($languages as $language) {
        /* set system langauge */
        if (isset($_COOKIE['s_lang'])) {
            if ($_COOKIE['s_lang'] == $language['code']) {
                $system['language'] = $language;
                T_setlocale(LC_MESSAGES, $system['language']['code']);
            }
        } else {
            if (($language['default'])) {
                $system['language'] = $language;
                T_setlocale(LC_MESSAGES, $system['language']['code']);
            }
        }
        $system['languages'][] = $language;
    }

    //Coniu - Bỏ đoạn query này đi cho performance ngon
    /* get system theme */
    //$get_theme = $db->query("SELECT * FROM system_themes WHERE system_themes.default = '1'") or _error(SQL_ERROR);
    //$theme = $get_theme->fetch_assoc();
    //$system['theme'] = $theme['name'];
    $system['theme'] = 'inet';

    /* get system version */
    require_once(ABSPATH . 'includes/version.php');
    $system['version'] = $inet_version;
    /* check system URL */
    //_check_system_url($system['system_url']);

    //ConIu - BEGIN
    /* Định nghĩa các kiểu định dạng date, datetime */
    $system['month_format'] = "m/Y";
    $system['date_format'] = "d/m/Y";
    $system['datetime_format'] = "H:i d/m/Y";
    $system['time_format'] = "H:i";
    /* Set đối tượng request là web */
    $system['is_mobile'] = false;
    //ConIu - END

    //Coniu - Bỏ đoạn này luôn cho performance ngon, các trang tĩnh để ngoài luôn
    // static pages
    /*
    $static_pages = array();
    $get_static = $db->query("SELECT * FROM static_pages") or _error(SQL_ERROR);
    if($get_static->num_rows > 0) {
        while($static_page = $get_static->fetch_assoc()) {
            $static_pages[] = $static_page;
        }
    }
    */

    // time config
    //date_default_timezone_set( 'UTC' );
    date_default_timezone_set('Asia/Ho_Chi_Minh');
    $time = time();
    $minutes_to_add = 0;
    $DateTime = new DateTime();
    $DateTime->add(new DateInterval('PT' . $minutes_to_add . 'M'));
    $date = $DateTime->format('Y-m-d H:i:s');

    // smarty config
    require_once(ABSPATH . 'includes/libs/Smarty/Smarty.class.php');
    $smarty = new Smarty;
    $smarty->template_dir = ABSPATH . 'content/themes/' . $system['theme'] . '/templates';
    $smarty->compile_dir = ABSPATH . 'content/themes/' . $system['theme'] . '/templates_compiled';
    $smarty->cache_dir = ABSPATH . 'content/themes/' . $system['theme'] . '/cache';
    $smarty->loadFilter('output', 'trimwhitespace');

    // get user & online friends if chat enabled
    require_once(ABSPATH . 'includes/class-user.php');
    try {
        /* CI - mobile lấy ra user từ user_id và session_token */
        if (isset($_POST['user_id']) && isset($_POST['user_token'])) {
            $user = new User($_POST['user_id'], $_POST['user_token']);
        } else {
            $user = new User();
        }

    } catch (Exception $e) {
        _error(SQL_ERROR);
    }

    // check if system is live
    if(!$system['system_live'] && ( (!$user->_logged_in && !isset($override_shutdown)) || ($user->_logged_in && $user->_data['user_group'] != 1)) ) {
        _error(__('System Message'), "<p class='text-center'>".$system['system_message']."</p>");
    }

    // check if the viewer is banned
    if($user->_logged_in && $user->_data['user_group'] != '1' && $user->_data['user_banned']) {
        _error(__("System Message"), __("Your account has been blocked"));
    }

    /* set session get chat_online */
    $session_user_chat = 0;
    if(isset($_SESSION[SESSION_USER_CHAT][$user->_data['user_id']]) && $_SESSION[SESSION_USER_CHAT][$user->_data['user_id']] == 1) {
        $session_user_chat = 1;
    }

    if($session_user_chat) {
        $system["sync_firebase"] = "1";
    } else {
        $system["sync_firebase"] = "0";
    }

    //Coniu
    $smarty->assign('cities', $cities);
    $smarty->assign('schoolTypes', $schoolTypes);
    $smarty->assign('year_begin', $year_begin);
    $smarty->assign('year_end', $year_end);

    // assign system varibles
    $smarty->assign('secret', $_SESSION['secret']);
//    $smarty->assign('user_chat', $session_user_chat);
    $smarty->assign('__', __);
    $smarty->assign('system', $system);
    $smarty->assign('date', $date);
    //$smarty->assign('static_pages', $static_pages); - Coniu bỏ đi
    $smarty->assign('user', $user);
    $smarty->assign('session_user_chat', $session_user_chat);
    $smarty->assign('grades', $grades);
    $smarty->assign('isGovManager', $_SESSION['isGovManager']);
//    echo "<pre>"; print_r($_SESSION); die;

//    if($user->_logged_in) {
//        // Lấy danh sách bài viết mới nhất
//        $posts = $user->get_posts();
//        $smarty->assign('posts', $posts);
//    }

}

?>