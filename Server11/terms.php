<?php
/**
 * terms
 * 
 * @package CINet v1.1
 * @author NOGA Co., Ltd
 */

// fetch bootstrap
require('bootstrap.php');

// page header
page_header($system['system_title']." &rsaquo; ".__("Terms"));

// page footer
page_footer("terms");

?>