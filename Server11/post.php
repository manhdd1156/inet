<?php
/**
 * post
 * 
 * @package CINet v1.1
 * @author NOGA Co., Ltd
 */

// fetch bootstrap
require('bootstrap.php');

// user access
if(!$system['system_public']) {
    user_access();
}

// valid inputs
if(!isset($_GET['post_id']) || !is_numeric($_GET['post_id'])) {
    _error(404);
}

try {

    // get post
    $post = $user->get_post($_GET['post_id']);
    if(!$post)  {
        _error(404);
    }

    /* Coniu - Check user banned hoặc block*/
    if($user->banned($post['user_id'])) {
        _error(404);
    }
    /* check if blocked by the viewer */
    if($user->blocked($post['user_id'])) {
        _error(404);
    }

    //Cập nhật lượt xem bài viết của trường
    if ($post['school_id'] > 0) {
        $postIds = array();
        $postIds[] = $post['post_id'];
        increaseCountViews($post['school_id'], 'post', $postIds);
        // TaiLA
        addInteractive($post['school_id'], 'post', 'school_view', $post['post_id']);
    }
    /* END - Coniu */

    /* assign variables */
    $smarty->assign('post', $post);

    // get ads
    $ads = $user->ads('post');
    /* assign variables */
    $smarty->assign('ads', $ads);

    // get widgets
    $widgets = $user->widgets('post');
    /* assign variables */
    $smarty->assign('widgets', $widgets);

} catch (Exception $e) {
    _error(__("Error"), $e->getMessage());
}

// page header
page_header($post['og_title'], $post['og_description'], $post['og_image']);

// page footer
page_footer("post");

?>