<?php
/**
 *
 * @package Inet API
 * @author TuanAnh
 */

// fetch bootstrap
require('bootstrap.php');
T_setlocale(LC_MESSAGES, 'vi_VN');
//T_setlocale(LC_MESSAGES, 'en_us');

if (isset($_POST) && $_SERVER['REQUEST_METHOD'] === 'POST') {
    // trim các param _POST truyền lên
    $_POST = array_map("trim", $_POST);

    // Check xem user có cần phải xác thực trước khi thực hiện không
    $allow = array('signup', 'socialSignin', 'signin', 'forgetPassword', 'forgetPasswordReset', 'checkUpdate', 'checkPhoneVerified', 'started_sms', 'test');
    if (!in_array($_GET['action'], $allow)) {
        if (isset($_POST['user_id']) && isset($_POST['user_token'])) {
            if (!$user->_logged_in) {
                _api_error(100, __('You have not signed in'));
            }
        } else
            _api_error(400);
    }

    // Xét đối tượng request là mobile.
    $system['is_mobile'] = true;
    try {
        $db->autocommit(false);

        switch ($_GET['action']) {
            case 'auth':
                $get_user = $user->_data;
                return_json(array(
                    'code' => 200,
                    'message' => 'Signed',
                    'data' => array(
                        'user_id'=>$_POST['user_id'],
                        'user_fname'=>$get_user['user_firstname'],
                        'user_lname'=>$get_user['user_lastname'],
                        'user_email'=>$get_user['user_email'],
                        'user_picture'=>$get_user['user_picture'],
                    )
                ));
                break;
            //Đăng ký
            case 'signup':
                // check if registration is open
                if (!$system['registration_enabled']) {
                    _api_error(0, __('Registration is closed right now'));
                }

                // signup
                $db->begin_transaction();

                 $user_info = $user->api_sign_up($_POST);
                $user_info['user_lname'] = $_POST['last_name'];
                $user_info['user_fname'] = $_POST['first_name'];
                /* CI - Cho user like các page và join các group của hệ thống */
                include_once(DAO_PATH . 'dao_user.php');
                $userDao = new UserDAO();
                $userDao->addUsersLikeSomePages($system_page_ids, [$user_info['user_id']]);
                $userDao->addUserToSomeGroups($system_group_ids, [$user_info['user_id']]);
                /* END - CI */

                $db->commit();
                return_json(array(
                    'code' => 200,
                    'message' => 'Đăng ký thành công',
                    'data' => $user_info
                ));
                break;

            //Đăng nhập bằng facebook, google
            case 'socialSignin':
                include_once(DAO_PATH . 'dao_user.php');

                if (!isset($_POST['facebook_id']) && !isset($_POST['google_id'])
                    || (isset($_POST['facebook_id']) && isset($_POST['google_id']))
                ) {
                    _api_error(400);
                }
                $user_profile = array();
                if (isset($_POST['facebook_id'])) {
                    $user_profile['provider'] = 'facebook';
                    $user_profile['social_id'] = $_POST['facebook_id'];
                } elseif (isset($_POST['google_id'])) {
                    $user_profile['provider'] = 'google';
                    $user_profile['social_id'] = $_POST['google_id'];
                }

                $user_profile['first_name'] = $_POST['first_name'];
                $user_profile['last_name'] = $_POST['last_name'];
                $user_profile['email'] = $_POST['email'];
                $user_profile['gender'] = $_POST['gender'];
                $user_profile['avatar'] = $_POST['avatar'];
                $user_profile['device_token'] = $_POST['device_token'];

                $db->begin_transaction();
                $user_info = $user->social_signin($user_profile);
                $connected = 0;
                if (isset($user_info['connected']) && $user_info['connected'] == 0) {
                    $userDao = new UserDAO();
                    /* CI - Cho user like các page và join các group của hệ thống */
                    $userDao->addUsersLikeSomePages($system_page_ids, [$user_info['user_id']]);
                    $userDao->addUserToSomeGroups($system_group_ids, [$user_info['user_id']]);
                }
                $connected = $user_info['connected'];
                unset($user_info['connected']);
                $db->commit();

                return_json(array(
                    'code' => 200,
                    'status' => $connected,
                    'message' => 'OK',
                    'data' => $user_info
                ));

                break;
            //Đăng nhập
            case 'signin':
                if (!isset($_POST['username_email']) || !isset($_POST['password'])) {
                    _api_error(400);
                }
                $db->begin_transaction();
                $user_info = $user->api_signin($_POST['username_email'], $_POST['password'], $_POST['device_token']);
                $db->commit();

                return_json(array(
                    'code' => 200,
                    'message' => 'Đăng nhập thành công',
                    'data' => $user_info
                ));

                break;
            //Đăng xuất
            case 'signout':
                $db->begin_transaction();
                // signout
                $user->api_sign_out($_POST['user_id'], $_POST['user_token']);
                $db->commit();

                return_json(array(
                    'code' => 200,
                    'message' => 'Đăng xuất thành công',
                    'data' => null
                ));
                break;

            //Started (Yêu cầu người dùng nhập đầy đủ thông tin)
            case 'started':
                $db->begin_transaction();
                $args = array();
                if ($_POST['edit'] == 'started_profile') {
                    $user->settings($_POST);
                    $db->commit();

                    return_json(array(
                        'code' => 200,
                        'message' => __("Done, Your info has been updated"),
                        'data' => null
                    ));
                } elseif ($_POST['edit'] == 'started_account') {
                    $args['edit'] = 'started_account';
                    if (isset($_POST['new_password'])) {
                        $args['password'] = $_POST['new_password'];
                    }
                    $user->settings($args);
                    $db->commit();

                    return_json(array(
                        'code' => 200,
                        'message' => __("Done, Your account has been updated"),
                        'data' => null
                    ));
                }
                break;

            //Started (Yêu cầu người dùng nhập đầy đủ thông tin)
            case 'started_sms':
                $db->begin_transaction();

                $args = array();
                $args['last_name'] = $_POST['last_name'];
                $args['first_name'] = $_POST['first_name'];
                $args['phone'] = $_POST['phone'];

                $args['identification_card_number'] = $_POST['identification_card_number'];
                $args['date_of_issue'] = $_POST['date_of_issue'];
                $args['place_of_issue'] = $_POST['place_of_issue'];

                $args['birth_date'] = $_POST['birth_date'];
                $args['password'] = $_POST['password'];

                $user_info = $user->sign_up_via_sms($args);

                /* CI - Cho user like các page và join các group của hệ thống */
                include_once(DAO_PATH . 'dao_user.php');
                $userDao = new UserDAO();

                $userDao->addUsersLikeSomePages($system_page_ids, [$user_info['user_id']]);
                $userDao->addUserToSomeGroups($system_group_ids, [$user_info['user_id']]);
                /* END - CI */

                $db->commit();

                return_json(array(
                    'code' => 200,
                    'message' => __("Done, Your info has been updated"),
                    'data' => $user_info
                ));

                break;

            //Quên mật khẩu (Lấy token)
            case 'forgetPassword':
                $db->begin_transaction();
                // forget password
                $user->forget_password($_POST['email'], $_POST['g-recaptcha-response']);
                $db->commit();
                return_json(array(
                    'code' => 200,
                    'message' => 'Token sent to your email',
                    'data' => null
                ));

                break;

            //Quên mật khẩu (Reset pass)
            case 'forgetPasswordReset':
                $db->begin_transaction();
                // forget password reset
                $user->forget_password_confirm($_POST['email'], $_POST['reset_key']);
                $user->forget_password_reset($_POST['email'], $_POST['reset_key'], $_POST['password'], $_POST['confirm_password']);
                $db->commit();
                return_json(array(
                    'code' => 200,
                    'message' => __("Your password has been changed you can login now"),
                    'data' => null
                ));

                break;

            //Newsfeed
            case 'newsFeed':
                $info = array();
                $page = (isset($_POST['page'])) ? $_POST['page'] : 0;
                // get posts (newsfeed)
                $posts = $user->api_get_posts(array('offset' => $page));
                // get announcements
                $announcements = $user->announcements();
                $allow_post = ($system['allow_user_post_on_wall']) ? true : false;
                $newsfeed_enabled = ($system['newsfeed_enabled']) ? true : false;
                return_json(array(
                    'code' => 200,
                    'message' => 'OK',
                    'data' => array(
                        'announcements' => $announcements,
                        'allow_user_post_on_wall' => $allow_post,
                        'newsfeed_enabled' => $newsfeed_enabled,
                        'posts' => $posts
                    )
                ));
                break;
            //Bài viết đã lưu
            case 'getSavedPosts':
                $info = array();
                $page = (isset($_POST['page'])) ? $_POST['page'] : 0;
                // get posts (newsfeed)
                $posts = $user->api_get_posts(array('get' => 'saved', 'offset' => $page));

                return_json(array(
                    'code' => 200,
                    'message' => 'OK',
                    'data' => array(
                        'posts' => $posts
                    )
                ));
                break;
            //Chi tiết bài viết
            case 'getPost':
                if (!isset($_POST['post_id'])) {
                    _api_error(400);
                }
                $post_comment = array();
                $page = (isset($_POST['page'])) ? $_POST['page'] : 0;

                $post = $user->api_get_post(array(
                        'post_id' => $_POST['post_id'],
                        'offset' => $page,
                        'full_details' => true
                    )
                );
                if (!$post) _api_error(404);

                /* Inet - Check user banned hoặc block*/
                if ($user->banned($post['user_id'])) {
                    _error(404);
                }
                /* check if blocked by the viewer */
                if ($user->blocked($post['user_id'])) {
                    _error(404);
                }

                //Cập nhật lượt xem bài viết của trường
                if ($post['school_id'] > 0) {
                    $postIds = array();
                    $postIds[] = $post['post_id'];
                    increaseCountViews($post['school_id'], 'post', $postIds);
                    // TaiLA - Cập nhật số lượng xem bài viết
                    addInteractive($post['school_id'], 'post', 'school_view', $post['post_id']);
                }
                /* END - Inet */

                if (isset($post['post_comments'])) $post_comment = $post['post_comments'];
                unset($post['post_comments']);
                return_json(array(
                    'code' => 200,
                    'message' => 'OK',
                    'data' => $post,
                    'comment' => $post_comment
                ));

                break;
            //Lấy danh sách trả lời comments
            case 'getCommentReplies':
                if (!isset($_POST['comment_id'])) {
                    _api_error(400);
                }
                $page = (isset($_POST['page'])) ? $_POST['page'] : 0;
                $replies = $user->get_replies($_POST['comment_id'], $_POST['page'], false);

                return_json(array(
                    'code' => 200,
                    'message' => 'OK',
                    'data' => array(
                        'comment_replies' => $replies
                    )
                ));

                break;
            //Lấy ảnh của users, pages, groups, hay trong 1 albums
            case 'getAllPhoto':

                $photos = array();
                if (!isset($_POST['type']) || !isset($_POST['id'])) {
                    _api_error(400);
                }
                // valid inputs
                $valid['type'] = array('album', 'user', 'page', 'group');
                if (!in_array($_POST['type'], $valid['type'])) {
                    _api_error(400);
                }

                $page = (isset($_POST['page'])) ? $_POST['page'] : 0;
                switch ($_POST['type']) {
                    case 'user':
                        // [1] get main profile info
                        $get_profile = $db->query(sprintf("SELECT user_id FROM users WHERE user_id = %s", secure($_POST['id']))) or _error(SQL_ERROR_THROWEN);
                        if ($get_profile->num_rows == 0) {
                            _api_error(404);
                        }

                        /* check if banned by the system */
                        if ($user->banned($_POST['id'])) {
                            _api_error(404);
                        }
                        /* check if blocked by the viewer */
                        if ($user->blocked($_POST['id'])) {
                            _api_error(404);
                        }

                        /* get photos */
                        $photos = $user->get_photos($_POST['id'], $_POST['type'], $page, true);

                        break;

                    case 'page':
                        // [1] get main page info
                        $get_page = $db->query(sprintf("SELECT pages.page_id FROM pages LEFT JOIN pages_categories ON pages.page_category = pages_categories.category_id WHERE page_id = %s", secure($_POST['id']))) or _error(SQL_ERROR_THROWEN);
                        if ($get_page->num_rows == 0) {
                            _api_error(404);
                        }
                        /* get photos */
                        $photos = $user->get_photos($_POST['id'], $_POST['type'], $page, true);
                        break;

                    case 'group':
                        // [1] get main group info
                        $get_group = $db->query(sprintf("SELECT group_id, group_admin, group_privacy FROM groups WHERE group_id = %s", secure($_POST['id']))) or _error(SQL_ERROR_THROWEN);
                        if ($get_group->num_rows == 0) {
                            _api_error(404);
                        }
                        $group = $get_group->fetch_assoc();
                        if ($group['group_privacy'] == "secret" || $group['group_privacy'] == "closed") {
                            $group['i_joined'] = $user->check_group_membership($user->_data['user_id'], $group['group_id']);
                            if ($group['i_joined'] == "approved") {
                                /* get photos */
                                $photos = $user->get_photos($_POST['id'], $_POST['type'], $page, true);
                            }
                        }

                        break;
                }

                return_json(array(
                    'code' => 200,
                    'message' => 'OK',
                    'data' => array(
                        'photos' => $photos
                    )
                ));

                break;
            //Lấy chi tiết 1 ảnh theo photo_id
            case 'getPhoto':
                // valid inputs
                if (!isset($_POST['photo_id']) || !is_numeric($_POST['photo_id'])) {
                    _api_error(400);
                }

                $page = (isset($_POST['page'])) ? $_POST['page'] : 0;

                // get photo
                $photo = $user->api_get_photo($_POST['photo_id'], $page, true);
                if (!$photo) {
                    _api_error(404);
                }

                return_json(array(
                    'code' => 200,
                    'message' => 'OK',
                    'data' => array(
                        'photo' => $photo,
                    )
                ));

                break;
            //Lấy video của users, pages, groups
            case 'getAllVideo':

                $videos = array();

                if (!isset($_POST['type']) || !isset($_POST['id'])) {
                    _api_error(400);
                }
                // valid inputs
                $valid['type'] = array('user', 'page', 'group');
                if (!in_array($_POST['type'], $valid['type'])) {
                    _api_error(400);
                }

                $page = (isset($_POST['page'])) ? $_POST['page'] : 0;

                switch ($_POST['type']) {
                    case 'user':
                        // [1] get main profile info
                        $get_profile = $db->query(sprintf("SELECT user_id FROM users WHERE user_id = %s", secure($_POST['id']))) or _error(SQL_ERROR_THROWEN);
                        if ($get_profile->num_rows == 0) {
                            _api_error(404);
                        }

                        /* check if banned by the system */
                        if ($user->banned($_POST['id'])) {
                            _api_error(404);
                        }
                        /* check if blocked by the viewer */
                        if ($user->blocked($_POST['id'])) {
                            _api_error(404);
                        }

                        /* get videos */
                        $videos = $user->get_videos($_POST['id'], $_POST['type'], $page);

                        break;

                    case 'page':
                        // [1] get main page info
                        $get_page = $db->query(sprintf("SELECT pages.page_id FROM pages LEFT JOIN pages_categories ON pages.page_category = pages_categories.category_id WHERE page_id = %s", secure($_POST['id']))) or _error(SQL_ERROR_THROWEN);
                        if ($get_page->num_rows == 0) {
                            _api_error(404);
                        }
                        /* get videos */
                        $videos = $user->get_videos($_POST['id'], $_POST['type'], $page);
                        break;

                    case 'group':
                        // [1] get main group info
                        $get_group = $db->query(sprintf("SELECT group_id FROM groups WHERE group_id = %s", secure($_POST['id']))) or _error(SQL_ERROR_THROWEN);
                        if ($get_group->num_rows == 0) {
                            _api_error(404);
                        }
                        /* get videos */
                        $videos = $user->get_videos($_POST['id'], $_POST['type'], $page);
                        break;
                }

                return_json(array(
                    'code' => 200,
                    'message' => 'OK',
                    'data' => array(
                        'videos' => $videos
                    )
                ));

                break;
            //Lấy albums của users, pages, groups
            case 'getAlbums':

                $albums = array();
                if (!isset($_POST['type']) || !isset($_POST['username'])) {
                    _api_error(400);
                }
                // valid inputs
                $valid['type'] = array('user', 'page', 'group');
                if (!in_array($_POST['type'], $valid['type'])) {
                    _api_error(400);
                }

                $page = (isset($_POST['page'])) ? $_POST['page'] : 0;

                switch ($_POST['type']) {
                    case 'user':
                        // [1] get main profile info
                        $get_profile = $db->query(sprintf("SELECT user_id FROM users WHERE user_name = %s", secure($_POST['username']))) or _error(SQL_ERROR_THROWEN);
                        if ($get_profile->num_rows == 0) {
                            _api_error(404);
                        }
                        $profile = $get_profile->fetch_assoc();
                        /* check if banned by the system */
                        if ($user->banned($profile['user_id'])) {
                            _api_error(404);
                        }
                        /* check if blocked by the viewer */
                        if ($user->blocked($profile['user_id'])) {
                            _api_error(404);
                        }

                        /* get albums */
                        $albums = $user->get_albums($profile['user_id'], 'user', $page);

                        break;

                    case 'page':
                        // [1] get main page info
                        $get_page = $db->query(sprintf("SELECT pages.*, pages_categories.category_name FROM pages LEFT JOIN pages_categories ON pages.page_category = pages_categories.category_id WHERE page_name = %s", secure($_POST['username']))) or _error(SQL_ERROR_THROWEN);
                        if ($get_page->num_rows == 0) {
                            _api_error(404);
                        }
                        $spage = $get_page->fetch_assoc();
                        /* get albums */
                        $albums = $user->get_albums($spage['page_id'], 'page', $page);
                        break;

                    case 'group':
                        /// [1] get main group info
                        $get_group = $db->query(sprintf("SELECT G.*, CL.school_id FROM groups G LEFT JOIN ci_class_level CL ON (G.class_level_id > 0) AND (G.class_level_id = CL.class_level_id)
                                                WHERE G.group_name = %s", secure($_POST['username']))) or _error(SQL_ERROR_THROWEN);

                        //$get_group = $db->query(sprintf("SELECT * FROM groups WHERE group_name = %s", secure($_POST['username']))) or _error(SQL_ERROR_THROWEN);
                        if ($get_group->num_rows == 0) {
                            _api_error(404);
                        }
                        $group = $get_group->fetch_assoc();

                        $group['i_joined'] = false;
                        if ($user->_logged_in) {
                            $check_membership = $db->query(sprintf("SELECT * FROM groups_members WHERE group_id = %s AND user_id = %s", secure($group['group_id'], 'int'), secure($user->_data['user_id'], 'int'))) or _error(SQL_ERROR);
                            if ($check_membership->num_rows > 0) {
                                $group['i_joined'] = true;
                            }
                        }

                        // ConIu - BEGIN
                        if ($group['class_level_id'] > 0) {
                            //Lấy ra thông tin trường
                            include_once(DAO_PATH . 'dao_school.php');
                            $schoolDao = new SchoolDAO();
                            //$school = $schoolDao->getSchoolByClass($group['group_id']);
                            $school = getSchoolData($group['school_id'], SCHOOL_INFO);
                            if (is_null($school)) {
                                _api_error(0, __("Class does not belong to an existing school"));
                            }

                            //Nếu chưa phải thành viên nhóm (ko phải phụ huynh hay giao viên) và không phải admin trường thì báo lỗi
                            if ((!$group['i_joined']) && ($user->_data['user_id'] != $school['page_admin'])) {
                                _api_error(403);
                            }
                        }

                        /* get albums */
                        $albums = $user->get_albums($group['group_id'], 'group', $page);

                        break;
                }

                return_json(array(
                    'code' => 200,
                    'message' => 'OK',
                    'data' => array(
                        'albums' => $albums
                    )
                ));

                break;
            //Lấy chi tiết 1 album theo album_id
            case 'getAlbum':

                $album = array();
                if (!isset($_POST['type']) || !isset($_POST['username']) || !isset($_POST['album_id'])) {
                    _api_error(400);
                }
                // valid inputs
                $valid['type'] = array('user', 'page', 'group');
                if (!in_array($_POST['type'], $valid['type'])) {
                    _api_error(400);
                }

                $page = (isset($_POST['page'])) ? $_POST['page'] : 0;

                switch ($_POST['type']) {
                    case 'user':
                        // [1] get main profile info
                        $get_profile = $db->query(sprintf("SELECT user_id FROM users WHERE user_name = %s", secure($_POST['username']))) or _error(SQL_ERROR_THROWEN);
                        if ($get_profile->num_rows == 0) {
                            _api_error(404);
                        }
                        $profile = $get_profile->fetch_assoc();

                        /* check if banned by the system */
                        if ($user->banned($profile['user_id'])) {
                            _api_error(404);
                        }
                        /* check if blocked by the viewer */
                        if ($user->blocked($profile['user_id'])) {
                            _api_error(404);
                        }

                        /* get album */
                        $album = $user->get_album($_POST['album_id'], true, $page, true);
                        if (!$album || $album['in_group'] || $album['user_type'] == "page" || ($album['user_type'] == "user" && $album['user_id'] != $profile['user_id'])) {
                            _api_error(404);
                        }

                        break;

                    case 'page':
                        // [1] get main page info
                        $get_page = $db->query(sprintf("SELECT page_id FROM pages WHERE page_name = %s", secure($_POST['username']))) or _error(SQL_ERROR_THROWEN);
                        if ($get_page->num_rows == 0) {
                            _api_error(404);
                        }
                        $spage = $get_page->fetch_assoc();
                        /* get album */
                        $album = $user->get_album($_POST['album_id'], true, $page, true);
                        if (!$album || $album['in_group'] || $album['user_type'] == "user" || ($album['user_type'] == "page" && $album['page_id'] != $spage['page_id'])) {
                            _api_error(404);
                        }
                        break;

                    case 'group':
                        /// [1] get main group info
                        $get_group = $db->query(sprintf("SELECT G.*, CL.school_id FROM groups G LEFT JOIN ci_class_level CL ON (G.class_level_id > 0) AND (G.class_level_id = CL.class_level_id)
                                                WHERE G.group_name = %s", secure($_POST['username']))) or _error(SQL_ERROR_THROWEN);

                        //$get_group = $db->query(sprintf("SELECT * FROM groups WHERE group_name = %s", secure($_POST['username']))) or _error(SQL_ERROR_THROWEN);
                        if ($get_group->num_rows == 0) {
                            _api_error(404);
                        }
                        $group = $get_group->fetch_assoc();

                        $group['i_joined'] = false;
                        if ($user->_logged_in) {
                            $check_membership = $db->query(sprintf("SELECT * FROM groups_members WHERE group_id = %s AND user_id = %s", secure($group['group_id'], 'int'), secure($user->_data['user_id'], 'int'))) or _error(SQL_ERROR);
                            if ($check_membership->num_rows > 0) {
                                $group['i_joined'] = true;
                            }
                        }

                        // ConIu - BEGIN
                        if ($group['class_level_id'] > 0) {
                            //Lấy ra thông tin trường
                            include_once(DAO_PATH . 'dao_school.php');
                            $schoolDao = new SchoolDAO();
                            $school = getSchoolData($group['school_id'], SCHOOL_INFO);
                            if (is_null($school)) {
                                _api_error(0, __("Class does not belong to an existing school"));
                            }

                            //Nếu chưa phải thành viên nhóm (ko phải phụ huynh hay giao viên) và không phải admin trường thì báo lỗi
                            if ((!$group['i_joined']) && ($user->_data['user_id'] != $school['page_admin'])) {
                                _api_error(403);
                            }
                        }

                        /* get album */
                        $album = $user->get_album($_POST['album_id'], true, $page, true);
                        if (!$album || ($album['group_id'] != $group['group_id'])) {
                            _error(404);
                        }

                        break;
                }

                return_json(array(
                    'code' => 200,
                    'message' => 'OK',
                    'data' => array(
                        'album' => $album
                    )
                ));

                break;
            //Tạo bài viết (publisher)
            case 'createPost':
                // valid inputs
                $valid['handle'] = array('me', 'user', 'page', 'group');
                if (!in_array($_POST['handle'], $valid['handle'])) {
                    _api_error(400);
                }
                /* if both photos & link are exsist */
                if (isset($_FILES['photo']) && isset($_POST['link'])) {
                    _api_error(400);
                }
                /* if both photos & video are exsist */
                if (isset($_FILES['photo']) && isset($_FILES['video'])) {
                    _api_error(400);
                }
                /* if both photos & audio are exsist */
                if (isset($_FILES['photo']) && isset($_FILES['audio'])) {
                    _api_error(400);
                }
                /* if both photos & file are exsist */
                if (isset($_FILES['photo']) && isset($_FILES['file'])) {
                    _api_error(400);
                }
                /* if both link & video are exsist */
                if (isset($_POST['link']) && isset($_FILES['video'])) {
                    _api_error(400);
                }
                /* if both link & audio are exsist */
                if (isset($_POST['link']) && isset($_FILES['audio'])) {
                    _api_error(400);
                }
                /* if both link & file are exsist */
                if (isset($_POST['link']) && isset($_FILES['file'])) {
                    _api_error(400);
                }
                /* if both video & audio are exsist */
                if (isset($_FILES['video']) && isset($_FILES['audio'])) {
                    _api_error(400);
                }
                /* if both video & file are exsist */
                if (isset($_FILES['video']) && isset($_FILES['file'])) {
                    _api_error(400);
                }
                /* if both audio & file are exsist */
                if (isset($_FILES['audio']) && isset($_FILES['file'])) {
                    _api_error(400);
                }
                /* Xử lý khi upload files */
                /* prepare post type */
                $link = null;
                if (isset($_FILES['photo'])) {
                    $type = 'photos';
                } elseif ($_FILES['video']) {
                    $type = 'video';
                } elseif ($_FILES['audio']) {
                    $type = 'audio';
                } elseif ($_FILES['file']) {
                    $type = 'file';
                } elseif (!is_empty($_POST['link'])) {
                    $link = $user->scraper($_POST['link']);
                    if ($link['source_type'] == "link") {
                        $type = 'link';
                    } else {
                        $type = 'media';
                    }
                } else {
                    if ($_POST['location'] != '') {
                        $type = 'map';
                    } else {
                        $type = '';
                    }
                }

                $is_file = array('album', 'photos', 'video', 'audio', 'file');
                if (in_array($type, $is_file)) {
                    $multiple = false;
                    if (isset($_FILES['photo']))
                        $multiple = (count($_FILES['photo']['tmp_name']) > 1) ? true : false;

                    $handle = $_POST['type'];
                    /* upload file */
                    include_once(ABSPATH . 'includes/api_upload.php');
                }

                /* Ci - END */

                // publisher
                if (isset($post_id) && $post_id > 0) {
                    $post = $user->api_get_post(array(
                            'post_id' => $post_id,
                            'offset' => 0,
                            'full_details' => true
                        )
                    );
                } else {
                    // initialize the return array
                    $return = $inputs = array();

                    // publish the new post
                    if ($_POST['handle'] == 'user') {
                        // Post lên tường của user có user_id = id.
                        if (!isset($_POST['id']) || !is_numeric($_POST['id'])) {
                            _api_error(400);
                        }
                        $inputs['id'] = $_POST['id'];
                        /* if privacy set and not valid */
                        if (!isset($_POST['privacy']) || !in_array($_POST['privacy'], array('friends', 'public'))) {
                            _api_error(400);
                        }
                        $inputs['privacy'] = $_POST['privacy'];

                    } elseif ($_POST['handle'] == 'page') {
                        if (!isset($_POST['id']) || !is_numeric($_POST['id'])) {
                            _api_error(400);
                        }
                        $inputs['id'] = $_POST['id'];
                        $inputs['privacy'] = 'public';

                    } elseif ($_POST['handle'] == 'group') {
                        if (!isset($_POST['id']) || !is_numeric($_POST['id'])) {
                            _api_error(400);
                        }
                        $inputs['id'] = $_POST['id'];
                        $inputs['privacy'] = 'custom';

                    } else {
                        /* if privacy set and not valid */
                        if (!isset($_POST['privacy']) || !in_array($_POST['privacy'], array('me', 'friends', 'public'))) {
                            _api_error(400);
                        }
                        $inputs['privacy'] = $_POST['privacy'];
                    }

                    /* prepare inputs */
                    $inputs['handle'] = $_POST['handle'];
                    $inputs['message'] = $_POST['message'];
                    $inputs['photos'] = $_photos;
                    $inputs['album'] = $_POST['album'];
                    $inputs['video'] = $_video;
                    $inputs['audio'] = $_audio;
                    $inputs['file'] = $_file;
                    $inputs['link'] = $link;
                    $inputs['location'] = $_POST['location'];

                    $db->begin_transaction();
                    /* publish */
                    $post = $user->api_publisher($inputs);

                    /* Inet - Tương tác trường */
                    if ($post['school_id'] > 0) {
                        $postIds[] = $post['post_id'];
                        setIsAddNew($post['school_id'], 'post_created', $postIds);
                        // TaiLA
                        addInteractive($post['school_id'], 'post', 'school_view', $post['post_id'], 1);
                    }
                    /* Inet - END */

                    $db->commit();
                }

                if (!$post) _api_error(404);

                // return & exit
                return_json(array(
                    'code' => 200,
                    'message' => 'OK',
                    'data' => array(
                        'post' => $post
                    )
                ));
                break;
            //Thao tác với 1 bài viết (share, delete, save, pin, like, ...)
            case 'postAction':
                // valid inputs
                $valid['do'] = array('share', 'delete_post', 'save_post', 'unsave_post', 'pin_post', 'unpin_post', 'like_post', 'unlike_post',
                    'hide_post', 'unhide_post', 'delete_comment', 'like_comment', 'unlike_comment', 'like_photo', 'unlike_photo', 'hide_announcement', 'edit_post');
                if (!in_array($_POST['do'], $valid['do'])) {
                    _api_error(400);
                }
                /* check post id */
                if (!isset($_POST['id']) || !is_numeric($_POST['id'])) {
                    _api_error(400);
                }

                $db->begin_transaction();
                // reaction
                switch ($_POST['do']) {
                    case 'share':
                        $user->share($_POST['id']);
                        break;

                    case 'delete_post':
                        $user->delete_post($_POST['id']);
                        break;

                    case 'save_post':
                        $user->save_post($_POST['id']);
                        break;

                    case 'unsave_post':
                        $user->unsave_post($_POST['id']);
                        break;

                    case 'pin_post':
                        $user->pin_post($_POST['id']);
                        break;

                    case 'unpin_post':
                        $user->unpin_post($_POST['id']);
                        break;

                    case 'like_post':
                        $user->like_post($_POST['id']);
                        break;

                    case 'unlike_post':
                        $user->unlike_post($_POST['id']);
                        break;

                    case 'hide_post':
                        $user->hide_post($_POST['id']);
                        break;

                    case 'unhide_post':
                        $user->unhide_post($_POST['id']);
                        break;

                    case 'delete_comment':
                        $user->delete_comment($_POST['id']);
                        break;

                    case 'like_comment':
                        $user->like_comment($_POST['id']);
                        break;

                    case 'unlike_comment':
                        $user->unlike_comment($_POST['id']);
                        break;

                    case 'like_photo':
                        $user->like_photo($_POST['id']);
                        break;

                    case 'unlike_photo':
                        $user->unlike_photo($_POST['id']);
                        break;

                    case 'hide_announcement':
                        $user->hide_announcement($_POST['id']);
                        break;

                    case 'edit_post':
                        if (!isset($_POST['text']) && !isset($_POST['privacy']))
                            _api_error(400);
                        if (isset($_POST['text'])) {
                            $user->edit_post($_POST['id'], $_POST['text']);
                        }

                        if (isset($_POST['privacy'])) {
                            /* if privacy set and not valid */
                            $valid['privacy'] = array('me', 'friends', 'public');
                            if (!in_array($_POST['privacy'], $valid['privacy'])) {
                                _api_error(400);
                            }

                            // edit privacy
                            $post = $user->edit_privacy($_POST['id'], $_POST['privacy']);
                        }
                        break;
                }
                $db->commit();

                return_json(array(
                    'code' => 200,
                    'message' => 'OK',
                    'data' => null
                ));

                break;
            //Tạo nhận xét (post, phôt, comment)
            case 'createComment':

                // valid inputs
                $valid['handle'] = array('post', 'photo', 'comment');
                if (!in_array($_POST['handle'], $valid['handle'])) {
                    _api_error(400);
                }
                /* if id is set & not numeric */
                if (!isset($_POST['id']) || !is_numeric($_POST['id'])) {
                    _api_error(400);
                }
                /* if message not set */
                if (!isset($_POST['message'])) {
                    _api_error(400);
                }
                /* filter comment photo */
                if (isset($_FILES['photo'])) {
                    $multiple = false;
                    $type = 'photos';
                    $handle = 'comment';
                    /* upload file */
                    include_once(ABSPATH . 'includes/api_upload.php');
                }
                $photo = (isset($_photos[0]) ? $_photos[0] : '');

                // comment
                //$comment = $user->comment($_POST['handle'], $_POST['post_id'], $_POST['message'], $_FILES['photo']);
                $comment = $user->api_comment($_POST['handle'], $_POST['id'], $_POST['message'], $photo);

                // return & exit
                return_json(array(
                    'code' => 200,
                    'message' => 'OK',
                    'data' => $comment
                ));
                break;
            //Lấy ra user đã like
            case 'showLikes':
                // check user activated
                if ($system['activation_enabled'] && !$user->_data['user_activated']) {
                    modal(MESSAGE, __("Not Activated"), __("Before you can interact with other users, you need to confirm your email address"));
                }

                // valid inputs
                /* if post_id & photo_id & comment_id not set */
                if (!isset($_POST['post_id']) && !isset($_POST['photo_id']) && !isset($_POST['comment_id'])) {
                    _api_error(400);
                }
                /* if post_id set but not numeric */
                if (isset($_POST['post_id']) && !is_numeric($_POST['post_id'])) {
                    _api_error(400);
                }
                /* if photo_id set but not numeric */
                if (isset($_POST['photo_id']) && !is_numeric($_POST['photo_id'])) {
                    _api_error(400);
                }
                /* if comment_id set but not numeric */
                if (isset($_POST['comment_id']) && !is_numeric($_POST['comment_id'])) {
                    _api_error(400);
                }

                // get likes
                $page = (isset($_POST['page'])) ? $_POST['page'] : 0;

                // get users who
                if (isset($_POST['post_id'])) {
                    /* like this post */
                    $users = $user->who_likes(array(
                        'post_id' => $_POST['post_id'],
                        'offset' => $page
                    ));

                } elseif (isset($_POST['photo_id'])) {
                    /* like this photo */
                    $users = $user->who_likes(array(
                        'photo_id' => $_POST['photo_id'],
                        'offset' => $page
                    ));

                } else {
                    /* like this comment */
                    $users = $user->who_likes(array(
                        'comment_id' => $_POST['comment_id'],
                        'offset' => $page
                    ));

                }
                // return & exit
                return_json(array(
                    'code' => 200,
                    'message' => 'OK',
                    'data' => $users
                ));

                break;
            //Lấy ra user đã share
            case 'showShares':
                // check user activated
                if ($system['activation_enabled'] && !$user->_data['user_activated']) {
                    modal(MESSAGE, __("Not Activated"), __("Before you can interact with other users, you need to confirm your email address"));
                }

                // valid inputs
                /* if post_id not set || not numeric */
                if (!isset($_POST['post_id']) || !is_numeric($_POST['post_id'])) {
                    _api_error(400);
                }

                $page = (isset($_POST['page'])) ? $_POST['page'] : 0;

                // get shares
                $posts = $user->who_shares($_POST['post_id'], $page);

                // return & exit
                return_json(array(
                    'code' => 200,
                    'message' => 'OK',
                    'data' => $posts
                ));

                break;
            //Danh sách bạn bè chờ xác nhận
            case 'listFriendsRequest':

                $page = (isset($_POST['page'])) ? $_POST['page'] : 0;
                $friend_info = array();
                /* get friend requests */
                $data['friend_requests'] = $user->api_get_friend_requests();
                /* get new people */
                $data['new_people'] = $user->get_new_people($page);

                $friend_info = array(
                    'friend_requests' => $data['friend_requests'],
                    'friend_suggestions' => $data['new_people']
                );
                $user->live_counters_reset('friend_requests');

                return_json(array(
                    'code' => 200,
                    'message' => 'OK',
                    'data' => $friend_info
                ));

                break;
            //Thao tác add, follow, cancel, block bạn bè
            case 'friendsConnect' :

                // valid inputs
                $valid['do'] = array('block', 'unblock', 'friend-accept', 'friend-decline', 'friend-add', 'friend-cancel', 'friend-remove', 'follow', 'unfollow');
                if (!in_array($_POST['do'], $valid['do'])) {
                    _api_error(400);
                }
                /* check id */
                if (!isset($_POST['friends_id']) || !is_numeric($_POST['friends_id'])) {
                    _api_error(400);
                }

                $db->begin_transaction();
                // connect user
                $user->connect($_POST['do'], $_POST['friends_id']);
                $db->commit();

                $results = array();
                if ($_POST['do'] != 'block') {
                    $args['user_id'] = $_POST['friends_id'];
                    $args['reload'] = true;
                    $profile = $user->get_profile($args);
                    $results = array(
                        'profile' => $profile
                    );
                }

                // return & exit
                return_json(array(
                    'code' => 200,
                    'message' => 'OK',
                    'data' => $results
                ));

                break;
            //Lấy ra tất cả thông báo
            case 'notification' :
                $page = (isset($_POST['page'])) ? $_POST['page'] : 0;

                // notifications
                $info = array(
                    'friend_requests' => $user->_data['friend_requests'],
                    'notifications' => $user->get_notifications($page),
                    'count_notification' => $user->_data['user_live_notifications_counter']
                );
                //$user->live_counters_reset('notifications');

                return_json(array(
                    'code' => 200,
                    'message' => 'OK',
                    'data' => $info
                ));

                break;
            //Trang cá nhân
            case 'homePage':

                // valid inputs
                $valid['view'] = array('all', 'info', 'profile', 'posts', 'friends', 'photos', 'albums', 'album', 'followers', 'followings', 'likes', 'pages', 'groups');
                if (!in_array($_POST['view'], $valid['view'])) {
                    _api_error(400);
                }

                $args = array();
                // [1] get main profile info
                $args['user_name'] = $_POST['username'];
                $args['user_id'] = $_POST['user_id'];
                $profile = $user->get_profile($args);

                // lấy ra offset ($page)
                $page = (isset($_POST['page'])) ? $_POST['page'] : 0;

                // [2] get view content
                switch ($_POST['view']) {

                    case 'all':
                        /* get followers count */
                        $profile['followers_count'] = count($user->get_followers_ids($profile['user_id']));

                        /* get friends */
                        //$profile['friends'] = $user->get_friends($profile['user_id']);
                        $friends = $user->get_friends($profile['user_id']);
                        if (count($friends) > 0) {
                            $profile['friends_count'] = count($user->get_friends_ids($profile['user_id']));
                        }

                        /* get posts */
                        $posts = $user->api_get_posts(array('get' => 'posts_profile', 'id' => $profile['user_id'], 'offset' => $page));

                        return_json(array(
                            'code' => 200,
                            'message' => 'OK',
                            'data' => array(
                                'profile' => $profile,
                                'posts' => $posts
                            )
                        ));

                        break;

                    case 'info':

                        /* get pages */
                        $like_pages = $user->get_pages(array(
                            'user_id' => $profile['user_id'],
                            'offset' => $page
                        ));

                        // get profile user's groups
                        $join_group = $user->get_groups(array(
                            'user_id' => $profile['user_id'],
                            'offset' => $page
                        ));

                        return_json(array(
                            'code' => 200,
                            'message' => 'OK',
                            'data' => array(
                                'profile' => $profile,
                                'pages' => $like_pages,
                                'groups' => $join_group
                            )
                        ));

                        break;

                    case 'profile':

                        return_json(array(
                            'code' => 200,
                            'message' => 'OK',
                            'data' => array(
                                'profile' => $profile
                            )
                        ));

                        break;

                    case 'posts':

                        $posts = $user->api_get_posts(array(
                            'get' => 'posts_profile',
                            'id' => $profile['user_id'],
                            'offset' => $page
                        ));

                        return_json(array(
                            'code' => 200,
                            'message' => 'OK',
                            'data' => array(
                                'posts' => $posts
                            )
                        ));

                        break;

                    case 'friends':

                        /* get friends */
                        $profile['friends'] = $user->get_friends($profile['user_id'], $page);
                        if (count($profile['friends']) > 0) {
                            $profile['friends_count'] = count($user->get_friends_ids($profile['user_id']));
                        }

                        return_json(array(
                            'code' => 200,
                            'message' => 'OK',
                            'data' => array(
                                'friends' => $profile['friends']
                            )
                        ));

                        break;

                    case 'photos':

                        /* get photos */
                        $photos = $user->get_photos($profile['user_id'], 'user', $page, true);

                        return_json(array(
                            'code' => 200,
                            'message' => 'OK',
                            'data' => array(
                                'photos' => $photos
                            )
                        ));

                        break;

                    case 'albums':

                        /* get albums */
                        $albums = $user->get_albums($profile['user_id'], 'user', $page);

                        return_json(array(
                            'code' => 200,
                            'message' => 'OK',
                            'data' => array(
                                'albums' => $albums
                            )
                        ));

                        break;

                    case 'album':
                        /* get album */
                        $album = $user->get_album($_POST['id']);
                        if (!$album || $album['in_group'] || $album['user_type'] == "page" || ($album['user_type'] == "user" && $album['user_id'] != $profile['user_id'])) {
                            _api_error(400);
                        }

                        return_json(array(
                            'code' => 200,
                            'message' => 'OK',
                            'data' => array(
                                'album' => $album
                            )
                        ));

                        break;

                    case 'followers':
                        /* get followers count */
                        $profile['followers_count'] = count($user->get_followers_ids($profile['user_id']));
                        /* get followers */
                        if ($profile['followers_count'] > 0) {
                            $profile['followers'] = $user->get_followers($profile['user_id']);
                        }

                        return_json(array(
                            'code' => 200,
                            'message' => 'OK',
                            'data' => array(
                                'followers' => $profile['followers']
                            )
                        ));

                        break;

                    case 'followings':

                        /* get followings count */
                        $profile['followings_count'] = count($user->get_followings_ids($profile['user_id']));
                        /* get followings */
                        if ($profile['followings_count'] > 0) {
                            $profile['followings'] = $user->get_followings($profile['user_id'], $page);
                        }

                        return_json(array(
                            'code' => 200,
                            'message' => 'OK',
                            'data' => array(
                                'followings' => $profile['followings']
                            )
                        ));

                        break;

                    case 'pages':

                        // get my pages
                        $pages = $user->get_pages(array('offset' => $page));
                        $like_pages = $user->get_pages(array(
                            'user_id' => $profile['user_id'],
                            'offset' => $page
                        ));
                        // get new pages
                        $new_pages = $user->get_pages(array(
                            'user_id' => $profile['user_id'],
                            'offset' => $page,
                            'suggested' => true
                        ));

                        return_json(array(
                            'code' => 200,
                            'message' => 'OK',
                            'data' => array(
                                'pages' => $pages,
                                'like_pages' => $like_pages,
                                'suggested_pages' => $new_pages
                            )
                        ));

                        break;

                    case 'groups':

                        // get profile user's groups
                        $groups = $user->get_groups(array(
                            'user_id' => $profile['user_id'],
                            'offset' => $page
                        ));

                        return_json(array(
                            'code' => 200,
                            'message' => 'OK',
                            'data' => array(
                                'groups' => $groups
                            )
                        ));

                        break;

                    default:
                        _api_error(400);
                        break;
                }

                break;
            //Search chi tiết trong posts, pages, groups, users, group-page detail
            case 'searchDetail' :
                $valid = array('posts', 'pages', 'groups', 'users', 'search_in_pages', 'search_in_groups');
                if (!in_array($_POST['type'], $valid) || !isset($_POST['query'])) {
                    _api_error(400);
                }
                $page = (isset($_POST['page'])) ? $_POST['page'] : 0;
                $id = (isset($_POST['id'])) ? $_POST['id'] : 0;

                $args = array();
                $args['type'] = $_POST['type'];
                $args['query'] = $_POST['query'];
                $args['id'] = $id;
                $args['offset'] = $page;

                // get results
                $results = $user->api_search_detail($args);
                return_json(array(
                    'code' => 200,
                    'message' => 'OK',
                    'data' => !empty($results) ? $results : null
                ));

                break;
            //Search
            case 'search' :
                if (!isset($_POST['query'])) {
                    _api_error(400);
                }

                // search
                // initialize the return array
                $return = array();

                // get results
                $results = $user->search($_POST['query']);
                return_json(array(
                    'code' => 200,
                    'message' => 'OK',
                    'data' => !empty($results) ? $results : null
                ));
                break;
            //Xóa tài khoản
            case 'deleteUser' :

                // check if delete accounts enabled
                if (!$system['delete_accounts_enabled']) {
                    _api_error(0, __("This feature has been disabled by the admin"));
                }
                // delete
                if (!isset($_POST['password'])) {
                    _api_error(400);
                }
                /* validate current password */
                if (!password_verify($_POST['password'], $user->_data['user_password'])) {
                    throw new Exception(__("Your password is incorrect"));
                }

                $db->begin_transaction();
                // delete user
                $user->delete_user($user->_data['user_id']);
                $db->commit();

                return_json(array(
                    'code' => 200,
                    'message' => 'OK',
                    'data' => null
                ));

                break;
            //Danh sách đã bị user block
            case 'getBlocks' :
                // delete
                $page = (isset($_POST['page'])) ? $_POST['page'] : 0;
                // delete user
                $blocks = $user->get_blocked($page);

                return_json(array(
                    'code' => 200,
                    'message' => 'OK',
                    'data' => array(
                        'blocks' => $blocks
                    )
                ));

                break;
            //Danh sách category của group
            case 'getGroupCategory':
                $categories = $user->get_groups_categories();

                // return
                return_json(array(
                    'code' => 200,
                    'message' => 'OK',
                    'data' => array(
                        'groups_categories' => $categories
                    )
                ));
                break;
            //Lấy danh sách group theo category_id (topic)
            case 'getGroupList':
                if (!isset($_POST['category_id']) || !is_numeric($_POST['category_id'])) {
                    _api_error(400);
                }
                $groups = $user->get_groups_by_category($_POST['category_id']);

                // return
                return_json(array(
                    'code' => 200,
                    'message' => 'OK',
                    'data' => array(
                        'groups_list' => $groups
                    )
                ));
                break;
            //Lấy thông tin của 1 group
            case 'getGroup':
                if (!isset($_POST['group_name'])) {
                    _api_error(400);
                }

                $group = $user->get_group($_POST['group_name']);

                // return
                return_json(array(
                    'code' => 200,
                    'message' => 'OK',
                    'data' => $group
                ));
                break;
            //Tạo, sửa group
            case 'groupAction':

                // valid inputs
                $valid['type'] = array('page', 'group');
                if (!in_array($_POST['type'], $valid['type'])) {
                    _api_error(400);
                }
                $valid['do'] = array('create', 'edit');
                if (!in_array($_POST['do'], $valid['do'])) {
                    _api_error(400);
                }

                // (create|edit) (page|group)
                // initialize the return array
                $return = array();

                $db->begin_transaction();
                // page (create|edit)
                if ($_POST['type'] == "page") {

                    // page create
                    if ($_POST['do'] == "create") {
                        $page_id = $user->page_create($_POST['category'], $_POST['title'], $_POST['username'], $_POST['description']);
                        // $return['link'] = $system['system_url'] . '/pages/' . $_POST['username'];
                        $return['page']['page_id'] = $page_id;
                        $return['page']['page_category'] = $_POST['category'];
                        $return['page']['page_title'] = $_POST['title'];
                        $return['page']['page_name'] = $_POST['username'];
                        $return['page']['page_description'] = $_POST['description'];

                        // page edit
                    } elseif ($_POST['do'] == "edit") {
                        /* check id */
                        if (!isset($_POST['id']) || !is_numeric($_POST['id'])) {
                            _api_error(400);
                        }
                        /* check if the user is the page admin */
                        $check = $db->query(sprintf("SELECT * FROM pages WHERE page_id = %s AND page_admin = %s", secure($_POST['id'], 'int'), secure($user->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                        if ($check->num_rows == 0) {
                            _api_error(403);
                        }
                        $spage = $check->fetch_assoc();
                        /* edit page */
                        $user->page_edit($_POST['id'], $_POST['category'], $_POST['title'], $_POST['username'], $spage['page_name'], $_POST['description']);
                        // $return['link'] = $system['system_url'] . '/pages/' . $_POST['username'];
                        $return['page']['page_id'] = $_POST['id'];
                        $return['page']['page_category'] = $_POST['category'];
                        $return['page']['page_title'] = $_POST['title'];
                        $return['page']['page_name'] = $_POST['username'];
                        $return['page']['page_description'] = $_POST['description'];

                    }

                    // group (create|edit)
                } elseif ($_POST['type'] == "group") {

                    // group create
                    if ($_POST['do'] == "create") {
                        $group_id = $user->group_create($_POST['title'], $_POST['username'], $_POST['description']);
                        // $return['link'] = $system['system_url'] . '/groups/' . $_POST['username'];

                        $return['group']['group_id'] = $group_id;
                        $return['group']['group_title'] = $_POST['title'];
                        $return['group']['group_name'] = $_POST['username'];
                        $return['group']['group_description'] = $_POST['description'];

                        // group edit
                    } elseif ($_POST['do'] == "edit") {
                        /* check id */
                        if (!isset($_POST['id']) || !is_numeric($_POST['id'])) {
                            _api_error(400);
                        }
                        /* check if the user is the group admin */
                        $check = $db->query(sprintf("SELECT * FROM groups WHERE group_id = %s AND group_admin = %s", secure($_POST['id'], 'int'), secure($user->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                        if ($check->num_rows == 0) {
                            _api_error(403);
                        }
                        $group = $check->fetch_assoc();
                        /* edit group */
                        $user->group_edit($_POST['id'], $_POST['title'], $_POST['username'], $group['group_name'], $_POST['description']);
                        // $return['link'] = $system['system_url'] . '/groups/' . $_POST['username'];

                        $return['group']['group_id'] = $_POST['id'];
                        $return['group']['group_title'] = $_POST['title'];
                        $return['group']['group_name'] = $_POST['username'];
                        $return['group']['group_description'] = $_POST['description'];
                    }

                }
                $db->commit();

                // return & exit
                return_json(array(
                    'code' => 200,
                    'message' => 'OK',
                    'data' => $return
                ));

                break;
            //Xóa group
            case 'deleteGroup':
                // check user activated
                if ($system['activation_enabled'] && !$user->_data['user_activated']) {
                    modal(MESSAGE, __("Not Activated"), __("Before you can interact with other users, you need to confirm your email address"));
                }

                // valid inputs
                if (!isset($_POST['id']) || !is_numeric($_POST['id'])) {
                    _api_error(400);
                }

                $db->begin_transaction();
                // delete
                switch ($_POST['handle']) {
                    case 'page':
                        $user->delete_page($_POST['id']);
                        break;

                    case 'group':
                        $user->delete_group($_POST['id']);
                        break;

                    default:
                        _api_error(400);
                        break;
                }
                $db->commit();

                // return
                return_json(array(
                    'code' => 200,
                    'message' => 'OK',
                    'data' => null
                ));

                break;
            //Danh sách category của page
            case 'getPageCategory':
                $categories = $user->get_pages_categories();

                // return
                return_json(array(
                    'code' => 200,
                    'message' => 'OK',
                    'data' => array(
                        'category' => $categories
                    )
                ));
                break;
            //Lấy chi tiết 1 page
            case 'showPage':
                // check username
                if (is_empty($_POST['username']) || !valid_username($_POST['username'])) {
                    _api_error(400);
                }
                if ($_POST['view'] == 'abouts') {
                    $param = ", SC.type , SC.city_name, SC.district_name, SC.short_overview, SC.review_id, SC.start_age, SC.end_age, SC.start_tuition_fee, SC.end_tuition_fee, SC.note_for_tuition, SC.admission, SC.note_for_admission, SC.image_url,       
        	          SC.verify, SC.facility_pool, SC.facility_playground_out, SC.facility_playground_in, SC.facility_library, SC.facility_camera, SC.note_for_facility, SC.service_breakfast, SC.service_belated, SC.service_saturday, SC.service_bus, SC.note_for_service,
        	          SC.info_leader, SC.info_method, SC.info_teacher, SC.info_nutrition, PC.category_name";
                } else {
                    $param = '';
                }

                $strSql = sprintf("SELECT P.*, SC. school_id, SC.display_rate_school, R.total_review, R.average_review %s FROM pages P
                    LEFT JOIN pages_categories PC ON P.page_category = PC.category_id 
                    LEFT JOIN ci_school_configuration SC ON SC.school_id = P.page_id
                    LEFT JOIN ci_review R ON R.type = 'school' AND SC.school_id = R.school_id WHERE P.page_name = %s", $param, secure($_POST['username']));

                // [1] get main page info
                $get_page = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
                if ($get_page->num_rows == 0) {
                    _api_error(404);
                }
                $spage = $get_page->fetch_assoc();

                /* get page picture */
                $spage['page_picture_default'] = ($spage['page_picture']) ? false : true;
                $spage['page_picture'] = User::get_picture($spage['page_picture'], 'page');
                /* check page category */
                $spage['category_name'] = (!$spage['category_name']) ? __('N/A') : $spage['category_name']; /* in case deleted by admin */
                if ($spage['page_cover'] != null)
                    $spage['page_cover'] = $system['system_uploads'] . '/' . $spage['page_cover'];

                /* get the connection */
                $spage['i_like'] = false;
                $spage['i_admin'] = $user->check_page_adminship($user->_data['user_id'], $spage['page_id']);
                $spage['i_teacher'] = false;
                $teachers = getSchoolData($spage['page_id'], SCHOOL_TEACHERS);
                $teacherIds = array_keys($teachers);
                if (in_array($user->_data['user_id'], $teacherIds)) {
                    $spage['i_teacher'] = true;
                }
                if ($user->_logged_in) {
                    $get_likes = $db->query(sprintf("SELECT * FROM pages_likes WHERE page_id = %s AND user_id = %s", secure($spage['page_id'], 'int'), secure($user->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                    if ($get_likes->num_rows > 0) {
                        $spage['i_like'] = true;
                    }
                }
                //$spage['is_admin'] = ($spage['group_admin'] == $user->_data['user_id']) ? true : false;

                // ConIu - BEGIN
                /* get page rating, view */
                $spage['display_rate_school'] = ($spage['display_rate_school']) ? true : false;
                $spage['total_review'] = is_numeric($spage['total_review']) ? $spage['total_review'] : 0;
                $spage['average_review'] = is_numeric($spage['average_review']) ? $spage['average_review'] : 0.0;


                if ($spage['page_category'] == SCHOOL_CATEGORY_ID) {
                    //1.Cập nhật lượt xem trang của trường
                    increaseCountViews($spage['page_id'], 'page');
                    //2.Lấy số lượt xem trang trường trong 30 ngày
                    if ($_POST['view'] == 'all') {
                        $schoolStatistic = getSchoolStatistics($spage['page_id'], 'page');
                        $spage['views_in_thirty_days'] = $schoolStatistic['page']['count_views'];
                    }
                }
                // ConIu - END

                // [2] get view content
                switch ($_POST['view']) {
                    case 'all':
                        $page = (isset($_POST['page'])) ? $_POST['page'] : 0;

                        /* Inet - Kiểm tra xem user có đc đánh giá trường ko? (chỉ phụ hunh đc đánh giá)*/
                        $spage['allow_review'] = false;
                        $children = getUserManageData($user->_data['user_id'], USER_MANAGE_CHILDREN);
                        foreach ($children as $child) {
                            if ($child['school_id'] == $spage['page_id']) {
                                $spage['allow_review'] = true;
                                break;
                            }
                        }
                        /* END - Inet*/

                        /* get posts */
                        $posts = $user->api_get_posts(array(
                            'get' => 'posts_page',
                            'id' => $spage['page_id'],
                            'offset' => $page
                        ));

                        // Inet - Tương tác trường
                        if ($spage['page_category'] == SCHOOL_CATEGORY_ID) {
                            //2. Cập nhật lượt xem bài viết của trường
                            $postIds = array();
                            foreach ($posts as $post) {
                                $postIds[] = $post['post_id'];
                                addInteractive($spage['page_id'], 'post', 'school_view', $post['post_id']);
                            }
                            increaseCountViews($spage['page_id'], 'post', $postIds);
                        }
                        // ConIu - END

                        /* get pinned post */
                        $pinned_post = $user->api_get_post(array(
                            'post_id' => $spage['page_pinned_post'],
                            'offset' => $page
                        )); /* $full_details = false, $pass_privacy_check = false */

                        if (!$pinned_post) $pinned_post = null;
                        // return
                        return_json(array(
                            'code' => 200,
                            'message' => 'OK',
                            'data' => array(
                                'info_page' => $spage,
                                'post' => $posts,
                                'pinned_post' => $pinned_post
                            )
                        ));
                        break;

                    case 'photos':
                        $page = (isset($_POST['page'])) ? $_POST['page'] : 0;

                        /* get photos */
                        $spage['photos'] = $user->get_photos($spage['page_id'], 'page', $page, true);

                        // return
                        return_json(array(
                            'code' => 200,
                            'message' => 'OK',
                            'data' => array(
                                'photos' => $spage['photos']
                            )
                        ));

                        break;

                    case 'albums':
                        $page = (isset($_POST['page'])) ? $_POST['page'] : 0;

                        /* get albums */
                        $spage['albums'] = $user->get_albums($spage['page_id'], 'page', $page);

                        // return
                        return_json(array(
                            'code' => 200,
                            'message' => 'OK',
                            'data' => array(
                                'albums' => $spage['albums']
                            )
                        ));

                        break;

                    case 'album':
                        $page = (isset($_POST['page'])) ? $_POST['page'] : 0;

                        /* get album */
                        $album = $user->get_album($_POST['id'], true, $page, true);
                        if (!$album || $album['in_group'] || $album['user_type'] == "user" || ($album['user_type'] == "page" && $album['page_id'] != $spage['page_id'])) {
                            _api_error(404);
                        }

                        // return
                        return_json(array(
                            'code' => 200,
                            'message' => 'OK',
                            'data' => array(
                                'album' => $album
                            )
                        ));

                        break;

                    case 'settings':
                        /* check if the viewer is the admin */
                        if ($user->_data['user_id'] != $spage['page_admin']) {
                            _api_error(404);
                        }

                        /* get pages categories */
                        $categories = $user->get_pages_categories();

                        // return
                        return_json(array(
                            'code' => 200,
                            'message' => 'OK',
                            'data' => array(
                                'info_page' => $spage,
                                'category' => $categories
                            )
                        ));
                        break;

                    case 'abouts':
                        $fields = array('school_id', 'page_admin', 'page_category', 'page_name', 'page_title', 'page_description', 'page_picture', 'page_cover', 'page_likes', 'city_id', 'telephone', 'website', 'email', 'address',
                            'teacher_count', 'class_count', 'child_count', 'type', 'lat', 'lng', 'city_slug', 'city_name', 'district_slug', 'district_name', 'short_overview', 'review_id', 'start_age', 'end_age', 'start_tuition_fee', 'end_tuition_fee', 'note_for_tuition', 'admission', 'note_for_admission', 'image_url',
                            'verify', 'facility_pool', 'facility_playground_out', 'facility_playground_in', 'facility_library', 'facility_camera', 'note_for_facility', 'service_breakfast', 'service_belated', 'service_saturday', 'service_bus', 'note_for_service',
                            'info_leader', 'info_method', 'info_teacher', 'info_nutrition');
                        $about_page = getArrayFromKeys($spage, $fields);

                        $about_page['facility_photos'] = array();
                        if ($about_page['school_id'] > 0) {
                            include_once(DAO_PATH . 'dao_school.php');
                            $schoolDao = new SchoolDAO();
                            $about_page['facility_photos'] = $schoolDao->getSchoolFacilities($about_page['school_id']);
                        }
                        // return
                        return_json(array(
                            'code' => 200,
                            'message' => 'OK',
                            'data' => array(
                                'about_page' => $about_page
                            )
                        ));
                        break;

                    case 'reviews':
                        $_POST['do'] = 'getSchoolReview';
                        $_POST['school_id'] = $spage['page_id'];
                        include_once(APIBO_PATH . 'child/bochild_review.php');
                        break;

                    default:
                        _api_error(400);
                        break;
                }

                break;
            //Thao tác với 1 page (lie, unlike, invite)
            case 'pageConnect' :
                // valid inputs
                $valid['do'] = array('page-like', 'page-unlike', 'page-invite');
                if (!in_array($_POST['do'], $valid['do'])) {
                    _api_error(400);
                }
                /* check id */
                if (!isset($_POST['id']) || !is_numeric($_POST['id'])) {
                    _api_error(400);
                }
                $db->begin_transaction();
                // connect user
                $_POST['uid'] = isset($_POST['uid']) ? $_POST['uid'] : null;
                $user->connect($_POST['do'], $_POST['id'], $_POST['uid']);
                $db->commit();

                // return & exit
                return_json(array(
                    'code' => 200,
                    'message' => 'OK',
                    'data' => null
                ));

                break;
            //Lấy ra chi tiết 1 group
            case 'showGroup':

                // check username
                if (is_empty($_POST['username']) || !valid_username($_POST['username'])) {
                    _api_error(400);
                }
                $page = (isset($_POST['page'])) ? $_POST['page'] : 0;

                // [1] get main group info
                $get_group = $db->query(sprintf("SELECT G.*, CL.school_id, GC.category_name FROM groups G LEFT JOIN ci_class_level CL ON (G.class_level_id > 0) AND (G.class_level_id = CL.class_level_id)
                                                LEFT JOIN groups_categories GC ON G.group_category = GC.category_id
                                                WHERE G.group_name = %s", secure($_POST['username']))) or _error(SQL_ERROR_THROWEN);

                //$get_group = $db->query(sprintf("SELECT * FROM groups WHERE group_name = %s", secure($_POST['username']))) or _error(SQL_ERROR_THROWEN);
                if ($get_group->num_rows == 0) {
                    _api_error(404);
                }
                $group = $get_group->fetch_assoc();

                /* get group picture */
                $group['group_picture_default'] = ($group['group_picture']) ? false : true;
                $group['group_picture'] = User::get_picture($group['group_picture'], 'group');
                if (!is_empty($group['group_cover'])) {
                    $group['group_cover'] = $system['system_uploads'] . '/' . $group['group_cover'];
                }
                /* get the connection */
                $group['i_joined'] = $user->check_group_membership($user->_data['user_id'], $group['group_id']);
                /* Check admin - Taila */
                $group['i_admin'] = $user->check_group_adminship($user->_data['user_id'], $group['group_id']);
                /* Nếu chưa tham gia trả về i_joined = not_joined*/
                if (!$group['i_joined']) {
                    $group['i_joined'] = "not_joined";
                }
                /* get group requests */
                if ($group['group_privacy'] != "public") {
                    $group['total_requests'] = $user->get_group_requests_total($group['group_id']);
                }

                // [2] get view content
                /* check group privacy */
                if ($group['group_privacy'] == "secret") {
                    if ($group['i_joined'] != "approved" && $group['group_admin'] != $user->_data['user_id'] && !$group['i_admin']) {
                        _api_error(404);
                    }
                }

                if ($group['group_privacy'] == "closed") {
                    if ($group['i_joined'] != "approved" && $group['group_admin'] != $user->_data['user_id'] && !$group['i_admin']) {
                        //Trả về danh sách member của group
                        $members = array();
                        if ($group['group_members'] > 0) {
                            $members = $user->get_group_members($group['group_id'], $page);
                        }

                        // return
                        return_json(array(
                            'code' => 200,
                            'message' => 'OK',
                            'data' => array(
                                'info_group' => $group,
                                'members' => $members
                            )
                        ));
                    }
                }

                // ConIu - BEGIN
                if ($group['class_level_id'] > 0) {
                    //Lấy ra thông tin trường
                    include_once(DAO_PATH . 'dao_school.php');
                    $schoolDao = new SchoolDAO();
                    //$school = $schoolDao->getSchoolByClass($group['group_id']);
                    $school = getSchoolData($group['school_id'], SCHOOL_DATA);
                    if (is_null($school)) {
                        _api_error(0, __("Class does not belong to an existing school"));
                    }

                    //Nếu chưa phải thành viên nhóm (ko phải phụ huynh hay giao viên) và không phải admin trường thì báo lỗi - Thêm quản lý group ($group['i_admin']) - Taila
                    if (($group['i_joined'] != "approved") && ($user->_data['user_id'] != $school['page_admin']) && !$group['i_admin']) {
                        _api_error(404);
                    }
                }

                //Lấy ra danh sách giáo viên của lớp
                $teachers = getClassData($group['group_id'], CLASS_TEACHERS);

                //Kiểm tra vai trò của người dùng hiện tại với hệ thống
                $role_id = PERMISSION_NONE;
                if ($user->_data['user_id'] == $school['page_admin']) {
                    $role_id = PERMISSION_ALL;
                } else {
                    foreach ($teachers as $teacher) {
                        if ($user->_data['user_id'] == $teacher['user_id']) {
                            $role_id = PERMISSION_MANAGE;
                            break;
                        }
                    }
                }

                //publisher
                $group['class_allow_post'] = false;
                if ($user->_logged_in && ($group['i_joined'] == "approved" || $group['i_admin'])) {
                    //ConIu - BEGIN - Thêm điều kiện IF đối với group là lớp
                    if (($group['class_level_id'] == 0) || ($school['class_allow_post'])
                        || ($group['group_admin'] == $user->_data['user_id'])
                        || ($role_id != PERMISSION_NONE) || $group['i_admin']
                    ) {
                        $group['class_allow_post'] = true;
                    }
                }

                // Thay đổi cover và avatar
//                $canChangeIds = array();
//                $canChangeIds[] = $school['page_admin'];
//                $canChangeIds[] = $group['group_admin'];
//                foreach ($teachers as $teacher) {
//                    $canChangeIds[] = $teacher['user_id'];
//                }
                // Comment đoạn trên, chuyển qua check bằng hàm mới - Taila
                $canChangeIds = $user->get_group_admins_ids($group['group_id']);

                $group['class_change_cover_and_avatar'] = false;
                if ($user->_logged_in && ($group['i_joined'] == "approved" || $group['i_admin'])) {
                    //ConIu - BEGIN - Thêm điều kiện IF đối với group là lớp
                    if (in_array($user->_data['user_id'], $canChangeIds)) {
                        $group['class_change_cover_and_avatar'] = true;
                    }
                }

                /* Inet - Tăng thành viên nhóm */
                $group['group_members'] = increase_members_groups($group['group_name'], $group['group_members']);
                // ConIu - END

                // [2] get view content
                switch ($_POST['view']) {
                    case 'all':
                        // get posts
                        $posts = $user->api_get_posts(array(
                            'get' => 'posts_group',
                            'id' => $group['group_id'],
                            'offset' => $page
                        ));
                        /* get pinned post */
                        $pinned_post = $user->api_get_post(array(
                            'post_id' => $group['group_pinned_post'],
                            'offset' => $page,
                        )); /* $full_details = false, $pass_privacy_check = true */

                        // return
                        return_json(array(
                            'code' => 200,
                            'message' => 'OK',
                            'data' => array(
                                'info_group' => $group,
                                'posts' => $posts,
                                'pinned_post' => ($pinned_post) ? $pinned_post : null
                            )
                        ));

                        break;

                    case 'photos':
                        /* get photos */
                        $photos = $user->get_photos($group['group_id'], 'group', $page, true);

                        // return
                        return_json(array(
                            'code' => 200,
                            'message' => 'OK',
                            'data' => array(
                                'photos' => $photos
                            )
                        ));

                        break;

                    case 'albums':
                        /* get albums */
                        $albums = $user->get_albums($group['group_id'], 'group', $page);

                        // return
                        return_json(array(
                            'code' => 200,
                            'message' => 'OK',
                            'data' => array(
                                'albums' => $albums
                            )
                        ));
                        break;

                    case 'album':
                        /* get album */
                        $album = $user->get_album($_POST['id']);
                        if (!$album || ($album['group_id'] != $group['group_id'])) {
                            _api_error(404);
                        }

                        // return
                        return_json(array(
                            'code' => 200,
                            'message' => 'OK',
                            'data' => array(
                                'album' => $album
                            )
                        ));
                        break;

                    case 'members':
                        /* get members */
                        $members = array();
                        if ($group['group_members'] > 0) {
                            //$members = $user->get_members($group['group_id'], $page);
                            $members = $user->get_group_members($group['group_id'], $page);
                        }

                        // return
                        return_json(array(
                            'code' => 200,
                            'message' => 'OK',
                            'data' => array(
                                'members' => $members
                            )
                        ));
                        break;

                    case 'settings':
                        /* check if the user is the page admin */
                        if (($user->_data['user_id'] != $group['group_admin']) && !$group['i_admin']) {
                            _api_error(400);
                        }

                        // return
                        return_json(array(
                            'code' => 200,
                            'message' => 'OK',
                            'data' => array(
                                'info_group' => $group,
                            )
                        ));
                        break;

                    case 'invites':
                        /* check if the viewer is a group member */
                        if ($group['i_joined'] != "approved") {
                            _api_error(404);
                        }
                        /* get invites */
                        $invites = $user->get_group_invites($group['group_id']);
                        // return
                        return_json(array(
                            'code' => 200,
                            'message' => 'OK',
                            'data' => array(
                                'invites' => $invites
                            )
                        ));
                        break;

                    default:
                        _api_error(400);
                        break;
                }

                break;
            case 'groupConnect' :
                // valid inputs
                $valid['do'] = array('group-join', 'group-leave', 'group-invite', 'group-accept', 'group-decline');
                if (!in_array($_POST['do'], $valid['do'])) {
                    _api_error(400);
                }
                /* check id */
                if (!isset($_POST['id']) || !is_numeric($_POST['id'])) {
                    _api_error(400);
                }
                $db->begin_transaction();
                // connect user
                $user->connect($_POST['do'], $_POST['id']);
                $db->commit();

                // return & exit
                return_json(array(
                    'code' => 200,
                    'message' => 'OK',
                    'data' => null
                ));

                break;
            case 'report':

                // valid inputs
                $valid['do'] = array('report_user', 'report_page', 'report_group', 'report_post', 'unreport_post', 'report_comment', 'unreport_comment');
                if (!in_array($_POST['do'], $valid['do'])) {
                    _api_error(400);
                }
                /* check post id */
                if (!isset($_POST['id']) || !is_numeric($_POST['id'])) {
                    _api_error(400);
                }

                // report
                // initialize the return array
                $return = array();

                $db->begin_transaction();
                switch ($_POST['do']) {
                    case 'report_user':
                        $user->report($_POST['id'], 'user');
                        //modal(SUCCESS, __("Thanks"), __("Your report has been submitted"));
                        break;

                    case 'report_page':
                        $user->report($_POST['id'], 'page');
                        //modal(SUCCESS, __("Thanks"), __("Your report has been submitted"));
                        break;

                    case 'report_group':
                        $user->report($_POST['id'], 'group');
                        //modal(SUCCESS, __("Thanks"), __("Your report has been submitted"));
                        break;

                    case 'report_post':
                        $user->report($_POST['id'], 'post');
                        break;

                    case 'report_comment':
                        $user->report($_POST['id'], 'comment');
                        break;

                }
                $db->commit();

                // return
                return_json(array(
                    'code' => 200,
                    'message' => 'OK',
                    'data' => null
                ));

                break;
            case 'albumAction':
                // edit
                // initialize the return array
                $return = array();

                $db->begin_transaction();
                switch ($_POST['do']) {
                    case 'delete_album':
                        // valid inputs
                        if (!isset($_POST['id']) || !is_numeric($_POST['id'])) {
                            _api_error(400);
                        }
                        // delete album
                        $user->delete_album($_POST['id']);
                        break;

                    case 'edit_album':
                        // valid inputs
                        if (!isset($_POST['album_id']) || !is_numeric($_POST['album_id'])) {
                            _api_error(400);
                        }
                        if (!isset($_POST['title'])) {
                            _api_error(400);
                        }
                        $inputs['album_id'] = $_POST['album_id'];

                        // edit album
                        if (isset($_FILES['photo'])) {
                            /* Upload ảnh */
                            $photos = upload($_FILES['photo'], 'images', 'photos');
                            /* CI - END */

                            $inputs['photos'] = $photos;
                            $inputs['privacy'] = $_POST['privacy'];
                            $inputs['message'] = $_POST['message'];
                            $inputs['location'] = $_POST['location'];

                            $post_id = $user->add_album_photos($inputs);
                        }
                        $user->edit_album($inputs['album_id'], $_POST['title']);
                        break;

                    case 'delete_photo':
                        // valid inputs
                        if (!isset($_POST['id']) || !is_numeric($_POST['id'])) {
                            _api_error(400);
                        }
                        // delete photo
                        $user->delete_photo($_POST['id']);
                        break;

                    default:
                        _api_error(400);
                        break;
                }
                $db->commit();

                // return
                return_json(array(
                    'code' => 200,
                    'message' => 'OK',
                    'data' => null
                ));

                break;

            case 'updateProfile':
                // valid inputs
                $valid['edit'] = array('username', 'email', 'phone', 'password', 'profile', 'info', 'privacy', 'chat');
                if (!in_array($_POST['edit'], $valid['edit'])) {
                    _api_error(400);
                }

                // settings
                $db->begin_transaction();

                if ($_POST['edit'] == "info") {
                    /* valid inputs */
                    if (!isset($_POST['work_title']) || !isset($_POST['work_place'])
                        || !isset($_POST['city']) || !isset($_POST['hometown'])
                        || !isset($_POST['edu_major']) || !isset($_POST['edu_school']) || !isset($_POST['edu_class'])
                    ) {
                        _api_error(400);
                    }

                    $args = array();
                    $args['edit'] = $_POST['edit'];
                    $args['work_title'] = $_POST['work_title'];
                    $args['work_place'] = $_POST['work_place'];

                    $args['city'] = $_POST['city'];
                    $args['hometown'] = $_POST['hometown'];

                    $args['edu_major'] = $_POST['edu_major'];
                    $args['edu_school'] = $_POST['edu_school'];
                    $args['edu_class'] = $_POST['edu_class'];
                    $args['user_phone'] = standardizePhone($_POST['user_phone']);
                    /*if (!validatePhone($args['user_phone'])) {
                    _api_error(0, __("The phone number you entered is incorrect"));
                }*/

                    $user->settings($args);
                    $db->commit();
                    // return
                    return_json(array(
                        'code' => 200,
                        'message' => __("Done, Your info has been updated"),
                        'data' => null
                    ));
                } elseif ($_POST['edit'] == "username") {
                    /* valid inputs */
                    if (!isset($_POST['username'])) {
                        _error(400);
                    }
                    $args = array();
                    $args['edit'] = $_POST['edit'];
                    $args['username'] = $_POST['username'];
                    $user->settings($args);
                    $db->commit();

                    /* ---------- UPDATE - MEMCACHE ---------- */
                    //1.Cập nhật thông giáo viên
                    include_once(DAO_PATH . 'dao_class.php');
                    $classDao = new ClassDAO();
                    $classes = $classDao->getClasses($user->_data['user_id']);
                    if (!empty($classes)) {
                        updateTeacherData($user->_data['user_id'], TEACHER_INFO);
                    }
                    /* ---------- END - MEMCACHE ---------- */
                    // return
                    return_json(array(
                        'code' => 200,
                        'message' => __("Done, Your account has been updated"),
                        'data' => null
                    ));

                } elseif ($_POST['edit'] == "email") {
                    /* valid inputs */
                    if (!isset($_POST['email'])) {
                        _error(400);
                    }
                    $args = array();
                    $args['edit'] = "email";
                    $args['email'] = $_POST['email'];
                    $user->settings($args);
                    $db->commit();

                    /* ---------- UPDATE - MEMCACHE ---------- */
                    //1.Cập nhật thông giáo viên
                    include_once(DAO_PATH . 'dao_class.php');
                    $classDao = new ClassDAO();
                    $classes = $classDao->getClasses($user->_data['user_id']);
                    if (!empty($classes)) {
                        updateTeacherData($user->_data['user_id'], TEACHER_INFO);
                    }
                    /* ---------- END - MEMCACHE ---------- */

                    // return
                    return_json(array(
                        'code' => 200,
                        'message' => __("Done, Your account has been updated"),
                        'data' => null
                    ));

                } elseif ($_POST['edit'] == "phone") {
                    $phone = standardizePhone($_POST['phone']);
                    if (valid_phone($phone)) {
                        $args['edit'] = 'phone';
                        $args['phone'] = $phone;
                        $user->settings($args);
                        $db->commit();
                    } else {
                        throw new Exception(__("Please enter a valid mobile number"));
                    }

                    // return
                    return_json(array(
                        'code' => 200,
                        'message' => __("Done, Your account has been updated"),
                        'data' => null
                    ));

                } elseif ($_POST['edit'] == "password") {

                    /* valid inputs */
                    if (!isset($_POST['current_password']) || !isset($_POST['new_password']) || !isset($_POST['confirm_password'])) {
                        _api_error(400);
                    }
                    $args = array();
                    $args['edit'] = $_POST['edit'];
                    $args['current'] = $_POST['current_password'];
                    $args['new'] = $_POST['new_password'];
                    $args['confirm'] = $_POST['confirm_password'];
                    $user->settings($args);

                    $db->commit();

                    // return
                    return_json(array(
                        'code' => 200,
                        'message' => __("Done, Your password has been updated"),
                        'data' => null
                    ));

                } elseif ($_POST['edit'] == "profile") {

                    /* valid inputs */
                    if (!isset($_POST['first_name']) || !isset($_POST['last_name']) || !isset($_POST['gender'])) {
                        _api_error(400);
                    }
                    $args = array();
                    $args['edit'] = $_POST['edit'];
                    $args['firstname'] = $_POST['first_name'];
                    $args['lastname'] = $_POST['last_name'];
                    $args['gender'] = $_POST['gender'];
                    $args['birth_day'] = $_POST['birth_day'];
                    $args['city_id'] = $_POST['city_id'];
                    $args['user_phone'] = $_POST['user_phone'];

                    $args['identification_card_number'] = $_POST['identification_card_number'];
                    $args['date_of_issue'] = $_POST['date_of_issue'];
                    $args['place_of_issue'] = $_POST['place_of_issue'];

                    $args['work_title'] = $_POST['work_title'];
                    $args['work_place'] = $_POST['work_place'];

                    $args['city'] = $_POST['city'];
                    $args['hometown'] = $_POST['hometown'];

                    $args['edu_major'] = $_POST['edu_major'];
                    $args['edu_school'] = $_POST['edu_school'];
                    $args['edu_class'] = $_POST['edu_class'];

                    if (isset($_FILES['photo'])) {
                        $multiple = false;
                        $type = 'photos';
                        $handle = 'picture-user';
                        /* upload file */
                        include_once(ABSPATH . 'includes/api_upload.php');
                        $args['avatar'] = (isset($_photos[0]) ? $_photos[0] : '');
                    }
                    /* CI - END */

                    $user->settings($args);
                    $db->commit();
                    // Get profile info
                    $get_from['user_id'] = $user->_data['user_id'];
                    $profile = $user->get_profile($get_from);

                    // return
                    return_json(array(
                        'code' => 200,
                        'message' => __("Done, Your profile info has been updated"),
                        'data' => array(
                            'profile' => $profile
                        )
                    ));

                } elseif ($_POST['edit'] == "emailandphone") {
                    $args = array();
                    $args['edit'] = $_POST['edit'];
                    $args['user_email'] = $_POST['user_email'];
                    $args['user_phone'] = standardizePhone($_POST['user_phone']);

                    $user->settings($args);
                    $db->commit();

                    // return
                    return_json(array(
                        'code' => 200,
                        'message' => __("Done, Your profile info has been updated"),
                        'data' => null
                    ));

                } elseif ($_POST['edit'] == "privacy") {

                    /* valid inputs */
                    if (!isset($_POST['privacy_chat']) || !isset($_POST['privacy_birthdate']) || !isset($_POST['privacy_work']) || !isset($_POST['privacy_location']) || !isset($_POST['privacy_education'])) {
                        _api_error(400);
                    }
                    $args = array();
                    $args['edit'] = $_POST['edit'];
                    $args['privacy_chat'] = $_POST['privacy_chat'];
                    $args['privacy_birthdate'] = $_POST['privacy_birthdate'];
                    $args['privacy_work'] = $_POST['privacy_work'];
                    $args['privacy_location'] = $_POST['privacy_location'];
                    $args['privacy_education'] = $_POST['privacy_education'];
                    $args['privacy_friends'] = $_POST['privacy_friends'];
                    $args['privacy_pages'] = $_POST['privacy_pages'];
                    $args['privacy_groups'] = $_POST['privacy_groups'];
                    $user->settings($args);
                    $db->commit();
                    // return
                    return_json(array(
                        'code' => 200,
                        'message' => __("Done, Your privacy settings have been updated"),
                        'data' => null
                    ));

                } elseif ($_POST['edit'] == "chat") {

                    /* valid inputs */
                    if (!isset($_POST['privacy_chat'])) {
                        _api_error(400);
                    }
                    $args = array();
                    $args['edit'] = $_POST['edit'];
                    $args['privacy_chat'] = $_POST['privacy_chat'];
                    $user->settings($args);
                    $db->commit();
                    // return
                    return_json(array(
                        'code' => 200,
                        'message' => __("Done, Your privacy settings have been updated"),
                        'data' => null
                    ));
                }

                break;
            /* API Quản lý */
            case 'manageHome':
                $action = "manage_home";
                include_once(APIBO_PATH . 'bo_manage.php');
                break;
            case 'manageChildMedicineShow':
                $_POST['do'] = 'show';
                include_once(APIBO_PATH . 'child/bochild_medicine.php');
                break;
            case 'manageClassMedicineShow':
                // check username
                if (is_empty($_POST['group_name']) || !valid_username($_POST['group_name'])) {
                    _api_error(400);
                }

                include_once(DAO_PATH . 'dao_class.php');
                include_once(DAO_PATH . 'dao_medicine.php');
                include_once(DAO_PATH . 'dao_child.php');
                include_once(DAO_PATH . 'dao_school.php');

                $classDao = new ClassDAO();
                $medicineDao = new MedicineDAO();
                $childDao = new ChildDAO();
                $schoolDao = new SchoolDAO();

                //Lấy ra thông tin lớp, bao gồm cả role_id của user hiện tại
                //$class = $classDao->getClassByUsername($_POST['group_name'], $user->_data['user_id']);
                $class = getClassDataByUsername($_POST['group_name'], CLASS_INFO);
                if (is_null($class)) {
                    _api_error(403);
                }
                //$schoolConfig = $schoolDao->getConfiguration($class['school_id']);
                $schoolConfig = getSchoolData($class['school_id'], SCHOOL_CONFIG);
                $school_allow_medicate = $schoolConfig['school_allow_medicate'];

                //$children = $childDao->getChildrenOfClass($class['group_id']);

                if ($_POST['view'] == 'today') {
                    // $today = getCurrentTime($system['date_format']);
                    $today = date($system['date_format']);
                    /* CI-mobile true tức là lấy data cho mobile */
                    $medicines = $medicineDao->getClassMedicineOnDate($class['group_id'], $today, true, $countNoConfirm);
                } else {
                    $medicines = $medicineDao->getClassAllMedicinesApi($class['group_id']);
                }

                // return
                return_json(array(
                    'code' => 200,
                    'message' => 'OK',
                    'data' => array(
                        'school_allow_medicate' => $school_allow_medicate,
                        //'child_list' => $children,
                        'medicines' => $medicines
                    )
                ));

                break;
            case 'manageChildMedicine':
                include_once(APIBO_PATH . 'child/bochild_medicine.php');
                break;
            case 'manageClassMedicine':
                include_once(APIBO_PATH . 'class/boclass_medicine.php');
                break;

            case 'manageChildEventShow':
                $_POST['do'] = 'show_event';
                $_POST['view'] = isset($_POST['view']) ? $_POST['view'] : 'list';
                include_once(APIBO_PATH . 'child/bochild_event.php');
                break;
            case 'manageChildEvent':
                include_once(APIBO_PATH . 'child/bochild_event.php');
                break;
            case 'manageEmployeeEventShow':
                $_POST['do'] = 'show_event_for_employee';
                include_once(APIBO_PATH . 'school/bo_event.php');
                break;
            case 'manageEmployeeEvent':
                include_once(APIBO_PATH . 'school/bo_event.php');
                break;
            case 'manageClassEventShow':
                $_POST['do'] = 'show_event';
                include_once(APIBO_PATH . 'class/boclass_event.php');
                break;
            case 'manageClassEvent':
                include_once(APIBO_PATH . 'class/boclass_event.php');
                break;
            case 'manageClassTuitionShow':
                $_POST['do'] = 'show_tuition';
                $_POST['view'] = isset($_POST['view']) ? $_POST['view'] : 'list';
                include_once(APIBO_PATH . 'class/boclass_tuition.php');
                break;
            case 'manageChildTuitionShow':
                $_POST['do'] = 'show_tuition';
                $_POST['view'] = isset($_POST['view']) ? $_POST['view'] : 'list';
                include_once(APIBO_PATH . 'child/bochild_tuition.php');
                break;
            case 'manageChildTuitionPaid':
                $_POST['do'] = 'tuition_paid';
                include_once(APIBO_PATH . 'child/bochild_tuition.php');
                break;
            case 'manageClassChildShow':
                $_POST['do'] = isset($_POST['do']) ? $_POST['do'] : 'show_children';
                include_once(APIBO_PATH . 'class/boclass_children.php');
                break;
            case 'manageChildShow':
                $_POST['do'] = 'show_child';
                include_once(APIBO_PATH . 'child/bochild_all.php');
                break;
            case 'manageClassChild':
                include_once(APIBO_PATH . 'class/boclass_child.php');
                break;
            case 'manageClassAttendance':
                include_once(APIBO_PATH . 'class/boclass_attendance.php');
                break;
            case 'manageChildAttendance':
                include_once(APIBO_PATH . 'child/bochild_all.php');
                break;
            case 'pushChatNotification':
                // check username
                if (!is_numeric($_POST['friend_id']) || !is_numeric($_POST['count_chat'])) {
                    _api_error(400);
                }
                if (!isset($_POST['conversation_id']) || is_null($_POST['conversation_id'])) {
                    _api_error(400);
                }
                $args = array();
                $to_users = $user->get_friends_info($_POST['friend_id']);
                if (is_null($to_users)) {
                    return;
                }

                foreach ($to_users as $to_user) {
                    if (!is_empty($to_user['device_token'])) {
                        $args['device_token'] = $to_user['device_token'];
                        $message = isset($_POST['message']) ? $_POST['message'] : '';
                        $user_fullname = isset($_POST['user_fullname']) ? $_POST['user_fullname'] : '';
                        //$user_fullname = $user->_data['user_fullname'];
                        $count_chat = isset($_POST['count_chat']) ? $_POST['count_chat'] : 0;

                        $args['friend_id'] = $user->_data['user_id'];
                        $args['conversation_id'] = $_POST['conversation_id'];
                        $args['message'] = $user_fullname . ': ' . $message;
                        $args['badge'] = $count_chat + $to_user['user_live_notifications_counter'];

                        $user->pushNotice($args);
                    }
                }

                break;
            case 'searchUser':
                include_once(APIBO_PATH . 'bo_search.php');
                break;
            case 'notificationDetail':
                include_once(APIBO_PATH . 'bonotify_all.php');
                break;
            case 'manageClassService':
                include_once(APIBO_PATH . 'class/boclass_service.php');
                break;
            case 'manageClassServiceShow':
                $_POST['do'] = 'show_service';
                include_once(APIBO_PATH . 'class/boclass_service.php');
                break;
            case 'manageChildServiceShow':
                // check username
                if (!isset($_POST['child_id']) || ($_POST['child_id'] <= 0)) {
                    _api_error(400);
                }

                $valid = array('services', 'registered', 'services_not_countbased', 'services_countbased');
                if (!in_array($_POST['view'], $valid)) {
                    _api_error(400);
                }
                include_once(DAO_PATH . 'dao_service.php');
                include_once(DAO_PATH . 'dao_class.php');
                include_once(DAO_PATH . 'dao_child.php');
                $serviceDao = new ServiceDAO();
                $classDao = new ClassDAO();
                $childDao = new ChildDAO();

                //Lấy ra thông tin trường, bao gồm cả role_id của user hiện tại
                $children = $childDao->getChildren($user->_data['user_id']);
                if (count($children) == 0) {
                    _api_error(404);
                }

                $child = null;
                if ((isset($_POST['child_id']) && ($_POST['child_id'] > 0))) {
                    foreach ($children as $_child) {
                        if ($_child['child_id'] == $_POST['child_id']) {
                            $child = $_child;
                            break;
                        }
                    }
                } else {
                    $child = $children[0];
                }

                $class = $classDao->getClassOfChild($child['child_id']);
                if ($class == null) {
                    _api_error(404);
                }

                // Tăng lượt tương tác thêm mới - TaiLA
                addInteractive($class['school_id'], 'service', 'parent_view');
                switch ($_POST['view']) {
                    case 'services':
                        //Lấy ra danh sách dịch vụ tính phí theo số lần sử dụng
                        $services = $serviceDao->getCountBasedServices($class['school_id']);
                        $servicesDisplay = array();
                        foreach ($services as $service) {
                            if ($service['parent_display'] == 1) {
                                $servicesDisplay[] = $service;
                            }
                        }
                        // return
                        return_json(array(
                            'code' => 200,
                            'message' => 'OK',
                            'data' => array(
                                'services' => $servicesDisplay
                            )
                        ));
                        break;

                    case 'registered':
                        //Lấy ra danh sách dịch vụ (theo tháng & theo điểm danh) mà trẻ đăng ký
                        $notCountBasedServices = $serviceDao->getChildServiceNotCountBased($class['school_id'], $child['child_id']);
                        // return
                        return_json(array(
                            'code' => 200,
                            'message' => 'OK',
                            'data' => array(
                                'registered' => $notCountBasedServices
                            )
                        ));
                        break;

                    case 'services_not_countbased':

                        $school = getSchoolData($class['school_id'], SCHOOL_DATA);
                        if (is_null($school)) {
                            _api_error(400);
                        }

                        $services = array();
                        $allServices = array();
                        //Lấy ra danh sách dịch vụ phải đăng ký
                        $services = $serviceDao->getAllChildServiceNotCountBased($class['school_id'], $child['child_id']);
                        $allServices = $serviceDao->getServices($class['school_id'], 1);
                        for ($i = 0; $i < count($allServices); $i++) {
                            for ($j = 0; $j < count($services); $j++) {
                                if ($allServices[$i]['service_id'] == $services[$j]['service_id']) {
                                    $allServices[$i]['status'] = $services[$j]['status'];
                                    $allServices[$i]['begin'] = $services[$j]['begin'];
                                    $allServices[$i]['end'] = $services[$j]['end'];
                                }
                            }
                        }

                        // return
                        return_json(array(
                            'code' => 200,
                            'message' => 'OK',
                            'data' => array(
                                'services_not_countbased' => $allServices,
                                'allow_parent_register_service' => $school['allow_parent_register_service'] ? true : false
                            )
                        ));
                        break;

                    case 'services_countbased':
                        //Validate thông tin đầu vào
                        if (!isset($_POST['child_id']) || ($_POST['child_id'] <= 0)) {
                            _api_error(400);
                        }
                        if (!isset($_POST['using_at']) || ($_POST['using_at'] == '')) {
                            _api_error(400);
                        }
                        //$school = $childDao->getCurrentSchoolOfChild($_POST['child_id']);
                        $school = getSchoolData($class['school_id'], SCHOOL_DATA);
                        if (is_null($school)) {
                            _api_error(400);
                        }

                        // services_countbased
                        $servicesCb = $serviceDao->getChildService4Record($school['page_id'], $_POST['child_id'], $_POST['using_at']);
                        $services_countbased = (!is_null($servicesCb['service'])) ? $servicesCb['service'] : array();

                        // return
                        return_json(array(
                            'code' => 200,
                            'message' => 'OK',
                            'data' => array(
                                'services_countbased' => $services_countbased,
                                'allow_parent_register_service' => $school['allow_parent_register_service'] ? true : false
                            )
                        ));
                        break;

                }

                break;
            case 'manageChildService':
                include_once(APIBO_PATH . 'child/bochild_all.php');
                break;
            case 'manageChildReport':
                include_once(APIBO_PATH . 'child/bochild_report.php');
                break;
            case 'manageClassReport':
                include_once(APIBO_PATH . 'class/boclass_report.php');
                break;
            case 'manageClassPickup':
                include_once(APIBO_PATH . 'school/bo_teacherpickup.php');
                break;
            case 'manageChildPickup':
                include_once(APIBO_PATH . 'child/bochild_all.php');
                break;
            case 'manageChildFeedback':
                include_once(APIBO_PATH . 'child/bochild_all.php');
                break;
            case 'manageChildInformation':
                include_once(APIBO_PATH . 'child/bochild_all.php');
                break;
            case 'manageChildLeave':
                include_once(APIBO_PATH . 'child/bochild_all.php');
                break;
            case 'manageClassChildInformation':
                include_once(APIBO_PATH . 'class/boclass_child.php');
                break;
            case 'manageSchedule':
                if (!isset($_POST['child_id']) && !isset($_POST['group_name']) || (isset($_POST['child_id']) && isset($_POST['group_name']))) {
                    _api_error(400);
                }

                if (isset($_POST['child_id'])) {
                    $_POST['do'] = "show_schedule";
                    include_once(APIBO_PATH . 'child/bochild_schedule.php');
                } elseif (isset($_POST['group_name'])) {
                    $_POST['do'] = "show_schedule";
                    include_once(APIBO_PATH . 'class/boclass_sechedule.php');
                }
                break;
            case 'manageMenu':
                if (!isset($_POST['child_id']) && !isset($_POST['group_name']) || (isset($_POST['child_id']) && isset($_POST['group_name']))) {
                    _api_error(400);
                }
                // xem thực đơn của từng trẻ
                if (isset($_POST['child_id'])) {
                    $_POST['do'] = 'show_menu';
                    include_once(APIBO_PATH . 'child/bochild_menu.php');
                    // Xem thực đơn của lớp
                } elseif (isset($_POST['group_name'])) {
                    $_POST['do'] = 'show_menu';
                    include_once(APIBO_PATH . 'class/boclass_menu.php');
                }
                break;
            case 'contact':
                include_once(APIBO_PATH . 'bo_contact.php');
                break;
            case 'checkUpdate':
                // validate param
                /*if (!isset($_POST['platform']) && !isset($_POST['device_id']) ||
                !isset($_POST['device_name']) && !isset($_POST['build']) ||
                !isset($_POST['version']) && !isset($_POST['os_version']) ) {
                    _api_error(400);
                }*/

                if (!isset($_POST['version']) || !isset($_POST['platform'])) {
                    _api_error(400);
                }

                $result = null;
                $count_notification = 0;
                //$must_change_pass = false;
                $must_update_profile = false;
                $must_active_user = false;
                if (isset($_POST['user_id'])) {
                    //1.Trả về trường và giáo viên cần đánh giá
                    if (!checkParentReviewInMonth($_POST['user_id'])) {
                        $_POST['do'] = 'getReview';
                        include_once(APIBO_PATH . 'child/bochild_review.php');
                    }
                    //2.Cập nhật last_active
                    updateUserLastActive($_POST['user_id']);

                    //3.Thông tin đổi mật khẩu lần đầu khi đăng nhập bằng facebook, google
                    //$must_change_pass = (($user->_data['facebook_connected'] || $user->_data['google_connected']) && !$user->_data['user_changed_pass']);
                    $must_update_profile = ($system['getting_started'] && !$user->_data['user_started']);
                    //4.Kiểm tra trạng thái user active
                    $must_active_user = true;
                    if (!$system['activation_enabled'] || $user->_data['user_activated']) {
                        $must_active_user = false;
                    }

                    //4.Số lượng thông báo (trả thêm cho Android)
                    $count_notification = $user->_data['user_live_notifications_counter'];

                    // 5. Cập nhật users online - TaiLA
                    $today = date("Y-m-d");
                    updateUserOnline($user->_data['user_id']);
                }

                if ($_POST['platform'] == 'IOS') {
                    if (FORCE_UPDATE && $_POST['version'] != IOS_VERSION) {
                        return_json(array(
                            'code' => 200,
                            'status' => 1,
                            'message' => 'Inet đã có bản cập nhật mới. Vui lòng cập nhật để tiếp tục sử dụng',
                            'data' => array(
                                'update_info' => array(
                                    'update_uri' => APPSTORE_URL,
                                    'force_update' => 1,
                                    'silent_update' => 0,
                                    'country_name' => 'VIET NAM',
                                    'country_code' => 'VN',
                                    'version' => IOS_VERSION),
                                'reviews' => $result,
                                //'must_change_pass' => $must_change_pass,
                                'must_update_profile' => $must_update_profile,
                                'must_active_user' => $must_active_user,
                                'is_off_about_coniu' => true
                            )
                        ));
                    } else
                        return_json(array(
                            'code' => 200,
                            'status' => 0,
                            'message' => 'Inet đã được cập nhật',
                            'data' => array(
                                'update_info' => array(
                                    'update_uri' => APPSTORE_URL,
                                    'force_update' => 0,
                                    'silent_update' => 0,
                                    'country_name' => 'VIET NAM',
                                    'country_code' => 'VN',
                                    'version' => IOS_VERSION),
                                'reviews' => $result,
                                //'must_change_pass' => $must_change_pass,
                                'must_update_profile' => $must_update_profile,
                                'must_active_user' => $must_active_user,
                                'is_off_about_coniu' => true
                            )
                        ));
                } elseif ($_POST['platform'] == 'ANDROID') {
                    if (FORCE_UPDATE && $_POST['version'] != ANDROID_VERSION) {
                        return_json(array(
                            'code' => 200,
                            'status' => 1,
                            'message' => 'Inet đã có bản cập nhật mới. Vui lòng cập nhật để tiếp tục sử dụng',
                            'data' => array(
                                'update_info' => array(
                                    'update_uri' => GOOGLEPLAY_URL,
                                    'force_update' => 1,
                                    'silent_update' => 0,
                                    'country_name' => 'VIET NAM',
                                    'country_code' => 'VN',
                                    'version' => ANDROID_VERSION),
                                'reviews' => $result,
                                //'must_change_pass' => $must_change_pass,
                                'must_update_profile' => $must_update_profile,
                                'must_active_user' => $must_active_user,
                                'count_notification' => $count_notification
                            )
                        ));
                    } else
                        return_json(array(
                            'code' => 200,
                            'status' => 0,
                            'message' => 'Inet đã được cập nhật',
                            'data' => array(
                                'update_info' => array(
                                    'update_uri' => GOOGLEPLAY_URL,
                                    'force_update' => 0,
                                    'silent_update' => 0,
                                    'country_name' => 'VIET NAM',
                                    'country_code' => 'VN',
                                    'version' => ANDROID_VERSION),
                                'reviews' => $result,
                                //'must_change_pass' => $must_change_pass,
                                'must_update_profile' => $must_update_profile,
                                'must_active_user' => $must_active_user,
                                'count_notification' => $count_notification
                            )
                        ));
                }
                break;

            case 'createReview':
                $_POST['do'] = 'createReview';
                include_once(APIBO_PATH . 'child/bochild_review.php');
                break;

            case 'getSchoolAndTeacherReview':
                $result = array();
                //1.Trả về trường và giáo viên cần đánh giá
                if (!checkParentReviewInMonth($_POST['user_id'])) {
                    $_POST['do'] = 'getReview';
                    include_once(APIBO_PATH . 'child/bochild_review.php');
                }

                return_json(array(
                    'code' => 200,
                    'message' => 'OK',
                    'data' => array(
                        'reviews' => $result
                    )
                ));
                break;

            case 'getSchoolReview':
                $_POST['do'] = 'getSchoolReview';
                include_once(APIBO_PATH . 'child/bochild_review.php');
                break;

            case 'contactSchool':
                return_json(array(
                    'code' => 200,
                    'message' => 'OK',
                    'data' => null
                ));
                break;

            case 'setCountNotifications':
                if (!isset($_POST['value'])) {
                    _api_error(400);
                }

                switch ($_POST['value']) {
                    case 'one':
                        /* update notifications counter - 1 */
                        if ($user->_data['user_live_notifications_counter'] > 0) {
                            $db->query(sprintf("UPDATE users SET user_live_notifications_counter = user_live_notifications_counter - 1 WHERE user_id = %s", secure($user->_data['user_id']))) or _error(SQL_ERROR_THROWEN);
                        }
                        break;

                    case 'all':
                        $db->query(sprintf("UPDATE users SET user_live_notifications_counter = 0 WHERE user_id = %s", secure($user->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                        $db->query(sprintf("UPDATE notifications SET seen = '1' WHERE to_user_id = %s", secure($user->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                        break;

                    default:
                        _api_error(400);
                }

                return_json(array(
                    'code' => 200,
                    'message' => 'OK',
                    'data' => null
                ));

                break;
            case 'getPageGroupJoined':
                // get joined groups
                $groups = $user->get_groups(array('user_id' => $user->_data['user_id']));
                // get liked pages
                $pages = $user->get_pages(array('user_id' => $user->_data['user_id']));

                return_json(array(
                    'code' => 200,
                    'message' => 'OK',
                    'data' => array(
                        'pages' => $pages,
                        'groups' => $groups
                    )
                ));

                break;
            case 'uploadImages':
                /* check file */
                if (!isset($_FILES['photo'])) {
                    _api_error(400);
                }

                $type = 'uploadImage';
                $multiple = false;
                if (isset($_FILES['photo']))
                    $multiple = (count($_FILES['photo']['tmp_name']) > 1) ? true : false;

                $handle = 'upload';
                /* upload file */
                include_once(ABSPATH . 'includes/api_upload.php');

                return_json(array(
                    'code' => 200,
                    'message' => 'OK',
                    'data' => array(
                        'source_images' => $_photos
                    )
                ));

                break;
            case 'schoolNearBy':
                include_once(DAO_PATH . 'dao_nearby.php');
                $nearByDao = new NearByDAO();

                /* check file */
                if (!isset($_POST['type']) || !isset($_POST['lat']) || !isset($_POST['lng'])
                    || !is_numeric($_POST['start_tuition_fee']) || !is_numeric($_POST['end_tuition_fee'])) {
                    _api_error(400);
                }

                $args = array();
                $args['distance'] = isset($_POST['distance']) ? $_POST['distance'] : 10;
                $args['page'] = is_numeric($_POST['page']) ? $_POST['page'] : 0;
                $args['limit'] = is_numeric($_POST['limit']) ? $_POST['limit'] : 15;
                $args['start'] = $args['page'] * $args['limit'];
                $args['type'] = $_POST['type'];
                $args['lat'] = $_POST['lat'];
                $args['lng'] = $_POST['lng'];
                $args['start_tuition_fee'] = $_POST['start_tuition_fee'];
                $args['end_tuition_fee'] = $_POST['end_tuition_fee'];

                //$results = $nearByDao->getSchoolNearBy($args);
                $results = array();

                return_json(array(
                    'code' => 200,
                    'message' => 'OK',
                    'data' => array(
                        'results' => $results
                    )
                ));

                break;
            case 'searchSchoolByName':
                include_once(DAO_PATH . 'dao_nearby.php');
                $nearByDao = new NearByDAO();

                /* check file */
                if (!isset($_POST['type']) || !isset($_POST['school_name'])) {
                    _api_error(400);
                }

                $args = array();
                $args['page'] = isset($_POST['page']) ? $_POST['page'] : 0;
                $args['limit'] = isset($_POST['limit']) ? $_POST['limit'] : 15;
                $args['start'] = $args['page'] * $args['limit'];
                $args['type'] = $_POST['type'];
                $args['school_name'] = $_POST['school_name'];

                $results = $nearByDao->getSchoolByName($args);

                return_json(array(
                    'code' => 200,
                    'message' => 'OK',
                    'data' => array(
                        'results' => $results
                    )
                ));

                break;

            case 'manageSchoolEvent':
                include_once(APIBO_PATH . 'school/bo_event.php');
                break;

            case 'manageSchoolAttendance':
                include_once(APIBO_PATH . 'school/bo_attendance.php');
                //INPUT: do=aday|conabs, page_name, attendanceDate
                break;

            case 'manageSchoolService':
                include_once(APIBO_PATH . 'school/bo_service.php');
                break;

            case 'manageSchoolMenu':
                include_once(APIBO_PATH . 'school/bo_menu.php');
                break;

            case 'manageSchoolSchedule':
                include_once(APIBO_PATH . 'school/bo_schedule.php');
                break;

            case 'manageSchoolTuition':
                include_once(APIBO_PATH . 'school/bo_tuition.php');
                break;

            case 'manageSchoolFeedback':
                include_once(APIBO_PATH . 'school/bo_feedback.php');
                break;

            case 'manageSchoolClassLevel':
                include_once(APIBO_PATH . 'school/bo_class_level.php');
                break;

            case 'manageSchoolMedicine':
                include_once(APIBO_PATH . 'school/bo_medicine.php');
                break;

            case 'manageSchoolReport':
                include_once(APIBO_PATH . 'school/bo_report.php');
                break;

            // Phụ huynh quản lý thông tin trẻ
            case 'manageChildBasicInformation':
                include_once(APIBO_PATH . 'child/bochild_child.php');
                break;
            // Phụ huynh quản lý thông tin SỨC KHỎE của trẻ
            case 'manageChildHealthInformation':
                include_once(APIBO_PATH . 'child/bochild_health.php');
                break;
            // Phụ huynh quản lý thông tin Y BẠ của trẻ
            case 'manageChildMedicalInformation':
                include_once(APIBO_PATH . 'child/bochild_medical.php');
                break;
            // Quản lý nhật ký của trẻ
            case 'manageChildJournal':
                include_once(APIBO_PATH . 'child/bochild_journal.php');
                break;
            // quá trình phát triển thai nhi
            case 'manageFoetusDevelopment':
                include_once(APIBO_PATH . 'child/bochild_foetus_development.php');
                break;
            // Kiến thức thai nhi
            case 'manageFoetusKnowledge':
                include_once(APIBO_PATH . 'child/bochild_foetus_knowledge.php');
                break;
            // quá trình phát triển của trẻ
            case 'manageChildDevelopment':
                include_once(APIBO_PATH . 'child/bochild_child_development.php');
                break;
            // Màn hình quản lý hiệu trưởng - Dịch vụ
            case 'manageSchoolSevicePrincipal':
                include_once(APIBO_PATH . 'school/principal/boprincipal_service.php');
                break;
            // Màn hình quản lý hiệu trưởng - Học phí
            case 'manageSchoolTuitionPrincipal':
                include_once(APIBO_PATH . 'school/principal/boprincipal_tuition.php');
                break;
            // Màn hình quản lý hiệu trưởng - Trẻ
            case 'manageSchoolChildrenPrincipal':
                include_once(APIBO_PATH . 'school/principal/boprincipal_children.php');
                break;
            // thông tin tháng tuổi
            case 'manageChildMonth':
                include_once(APIBO_PATH . 'child/bochild_childmonth.php');
                break;
            // Thông tin tuần thai
            case 'managePregnancy':
                include_once(APIBO_PATH . 'child/bochild_pregnancy.php');
                break;
            // Overview thông tin trẻ
            case 'manageChildOverviewInformation':
                include_once(APIBO_PATH . 'child/bochild_overview.php');
                break;

            // Màn hình thông tin đánh giá trường - giáo viên
            case 'manageSchoolReview':
                include_once(APIBO_PATH . 'school/bo_review.php');
                break;

            // Màn hình xác thực tài khoản bằng email
            case 'userActive':
                switch ($_POST['do']) {
                    case 'resend_email':
                        // check user activated
                        if ($system['activation_enabled'] && (!$user->_data['user_activated'] ||
                                ($user->_data['user_activated'] && !is_empty($user->_data['user_email_activation'])))) {
                            $user->activation_email_resend();
                        }

                        return_json(array(
                            'code' => 200,
                            'message' => __("Please click on the link in that email to confirm your email address"),
                            'data' => null
                        ));
                        break;

                    case 'check':
                        $user_activated = false;
                        // check user activated
                        if (!$system['activation_enabled'] || $user->_data['user_activated']) {
                            $user_activated = true;
                        }

                        return_json(array(
                            'code' => 200,
                            'message' => __("OK"),
                            'data' => array(
                                'user_actived' => $user_activated
                            )
                        ));
                        break;

                }
                break;

            case 'checkPhoneVerified':
                if (!isset($_POST['phone'])) {
                    _error(404);
                }
                $phone = standardizePhone($_POST['phone']);
                if (!valid_phone($phone)) {
                    throw new Exception(__("Please enter a valid mobile number"));
                }
                $users = null;
                $is_phone_verified = false;
                if (isset($_POST['is_signin']) && $_POST['is_signin']) {
                    $info = $user->check_phone($phone, true);
                    if (is_numeric($info['user_id'])) {
                        $is_phone_verified = true;

                        $users['user_id'] = $info['user_id'];
                        $users['user_name'] = $info['user_name'];
                        $users['user_email'] = $info['user_email'];
                        //$users['session_token'] = $session_token;
                        $users['user_fullname'] = $info['user_fullname'];
                        $users['user_picture'] = $info['user_picture'];
                        $users['is_manager'] = $user->is_manage($info['user_id']);
                        /* set cookies */
                        $users['user_token'] = $user->set_cookies_via_sms($info['user_id']);
                    }
                } else {
                    $is_phone_verified = $user->check_phone($phone);
                }

                return_json(array(
                    'code' => 200,
                    'message' => __("OK"),
                    'data' => array(
                        'is_phone_verified' => $is_phone_verified,
                        'user_info' => $users,
                        'user_id' => $users['user_id'],
                        'user_name' => $users['user_name'],
                        'user_email' => $users['user_email'],
                        'user_fullname' => $users['user_fullname'],
                        'user_picture' => $users['user_picture'],
                        'is_manager' => $users['is_manager'],
                        'user_token' => $users['user_token']

                    )
                ));
                break;

            case 'getListGroup':
                $action = "get_list_group";
                include_once(APIBO_PATH . 'bo_get_list_group.php');
                break;

            case 'managePoint':
                $users['is_manager'] = $user->is_manage($_POST['user_id']);
                include_once(DAO_PATH . 'dao_school.php');
                $schoolDao = new SchoolDAO();
                //$school = $schoolDao->getSchoolByClass($group['group_id']);
                if(isset($_POST['school_username'])) {
                    $school = getSchoolDataByUsername($_POST['school_username'], SCHOOL_INFO);
                }else if(isset($_POST['school_id'])) {
                    $school = getSchoolData($_POST['school_id'], SCHOOL_INFO);
                    $_POST['school_username'] = $school['page_name'];
                }

                include_once(DAO_PATH . 'dao_user_manage.php');
                $userManage = new UserManageDAO();
                $listObjectManages = $userManage->getRoleUserApi($_POST['user_id']);
                $role = 1; //1 : phụ huynh hoặc học sinh, 2: hiệu trưởng, 3: giáo viên,
                foreach ($listObjectManages as $objectManage) {
                    if ($objectManage['object_type'] == 3) {
                        $role = 3;
                    }
                }

                foreach ($listObjectManages as $objectManage) {
                    if ($objectManage['object_type'] == 2 && $objectManage['object_id'] == $school['page_id']) {
                        $role = 2;
                    }
                }
                switch ($role) {
                    case 1:
                        include_once(APIBO_PATH . 'child/bochild_point.php');
                        break;
                    case 2:
                        include_once(APIBO_PATH . 'school/bo_point.php');
                        break;
                    case 3:
                        include_once(APIBO_PATH . 'class/boclass_point.php');
                        break;
                }
                break;

            default:
                _api_error(400);
        }

    } catch (Exception $e) {
        _api_error(0, $e->getMessage());
    } finally {
        $db->autocommit(true);
    }

} else {
    _api_error(400);
}


?>