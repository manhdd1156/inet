<?php
/**
 * Quản lý các chức năng chính của nhan vien NOGA
 * 
 * @package ConIu v1
 * @author QuanND
 * MODIFY GRADE LEVEL OF SCHOOL BY MANHDD 03/04/2021
 */

// set override_shutdown
$override_shutdown = true;

// fetch bootstrap
require('bootstrap.php');

// Kiểm tra user login rồi hay chưa
user_access();
// check admin logged in
$nogaRole = $user->_data['user_group'];

if ((!$user->_is_admin) && ($nogaRole != USER_NOGA_MANAGE_ALL) && ($nogaRole != USER_NOGA_MANAGE_CITY)
    && ($nogaRole != USER_NOGA_MANAGE_SCHOOL) && ($nogaRole != USER_MOD)){
    _error(__('System Message'), __("You don't have the right permission to access this"));
}

include_once(DAO_PATH.'dao_school.php');
/* ADD START - ManhDD 03/04/2021 */
include_once(DAO_PATH.'dao_subject.php');
/* ADD END - ManhDD 03/04/2021 */
include_once(DAO_PATH.'dao_class.php');
include_once(DAO_PATH.'dao_teacher.php');
include_once(DAO_PATH.'dao_child.php');
include_once(DAO_PATH.'dao_user.php');
include_once(DAO_PATH.'dao_foetus_development.php');
include_once(DAO_PATH.'dao_foetus_knowledge.php');
include_once(DAO_PATH.'dao_child_development.php');
include_once(DAO_PATH.'dao_noga_notification.php');
include_once(DAO_PATH.'dao_childmonth.php');
include_once(DAO_PATH . 'dao_statistic.php');
include_once(DAO_PATH . 'dao_review.php');
include_once(DAO_PATH . 'dao_region_manage.php');

$schoolDao = new SchoolDAO();
/* ADD START - ManhDD 03/04/2021 */
$subjectDao = new SubjectDAO();
/* ADD END - ManhDD 03/04/2021 */
$classDao = new ClassDAO();
$teacherDao = new TeacherDAO();
$childDao = new ChildDAO();
$userDao = new UserDAO();
$foetusDevelopmentDao = new FoetusDevelopmentDAO();
$childDevelopmentDao = new ChildDevelopmentDAO();
$nogaNotificationDao = new NogaNotificationDAO();
$childMonthDao = new ChildMonthDAO();
$statisticDao = new StatisticDAO();
$reviewDao = new ReviewDAO();
$regionManageDao = new RegionManageDAO();

// page content
switch ($_GET['view']) {
	case '':
		// page header
		page_header(__("NOGA Management")." &rsaquo; ".__("Dashboard"));
        $schools = null;
        if (($nogaRole == USER_NOGA_MANAGE_ALL) || $user->_is_admin) {
            $schools = $schoolDao->getAllSchools();
        } elseif ($nogaRole == USER_NOGA_MANAGE_CITY) {
            $nogaManages = $userDao->getNogaManage($user->_data['user_id']);
            $schools = $schoolDao->getSchoolInCity($nogaManages[0]['city_id']);
        } else {
            $schools = $schoolDao->getSchoolOfNOGAUser($user->_data['user_id']);
        }

        $insights['schools'] = $schools;
        // assign variables
        $smarty->assign('insights', $insights);
		break;
    case 'schools':
        switch ($_GET['sub_view']) {
            case '':
                page_header(__("NOGA Management") . " &rsaquo; " . __("School list"));
                $schools = null;
                if (($nogaRole == USER_NOGA_MANAGE_ALL) || $user->_is_admin) {
                    $schools = $schoolDao->getAllSchools();
                } elseif ($nogaRole == USER_NOGA_MANAGE_CITY) {
                    $nogaManages = $userDao->getNogaManage($user->_data['user_id']);
                    $schools = $schoolDao->getSchoolInCity($nogaManages[0]['city_id']);
                } else {
                    $schools = $schoolDao->getSchoolOfNOGAUser($user->_data['user_id']);
                }
                $smarty->assign('nogaRole', $nogaRole);
                $smarty->assign('rows', $schools);
                break;
            case 'addsystemschool':
                // page header
                page_header(__("NOGA Management")." &rsaquo; ".__("School")." &rsaquo; ".__("Add New system school"));
                break;

            case 'listsystemschool':
                // page header
                page_header(__("NOGA Management")." &rsaquo; ".__("System School")." &rsaquo; ".__("List system school"));
                $results = $schoolDao->getSystemSchools();

                $smarty->assign('results', $results);
                break;

            case 'editsystemschool':
                if(!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }
                $data = $schoolDao->getSystemSchoolsById($_GET['id']);
                // page header
                page_header(__("NOGA Management")." &rsaquo; ".__("System School")." &rsaquo; ".__("List system school"));

                $smarty->assign('data', $data);
                break;

            case 'edit':
                switch ($_GET['p4']) {
                    case 'basic':
                        if(!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                            _error(404);
                        }
                        //$data = $schoolDao->getSchoolById($_GET['id']);
                        $data = getSchoolData($_GET['id'], SCHOOL_INFO);
                        if (is_null($data)) {
                            _error(404);
                        }
                        $systemSchools = $schoolDao->getSystemSchools();
                        //$dataCon = $schoolDao->getConfiguration($_GET['id']);
                        $dataCon = getSchoolData($_GET['id'], SCHOOL_CONFIG);

                        //Lấy ra thông tin của quản lý
                        include_once(DAO_PATH.'dao_user.php');
                        $manager = $userDao->getUsers([$data['page_admin']]);
                        $smarty->assign('managers', $manager);

                        //$smarty->assign('cities', $cities);
                        $smarty->assign('systemSchools', $systemSchools);
                        $smarty->assign('data', $data);
                        $smarty->assign('dataCon', $dataCon);
                        // page header
                        page_header(__("NOGA Management")." &rsaquo; ".__("School")." &rsaquo; ".$data['page_title']);

                        break;

                    case 'services':
                        if(!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                            _error(404);
                        }
                        $data = $schoolDao->getSchoolById($_GET['id']);
                        if (is_null($data)) {
                            _error(404);
                        }
                        //$dataCon = $schoolDao->getConfiguration($_GET['id']);
                        $dataCon = getSchoolData($_GET['id'], SCHOOL_CONFIG);
                        //Lấy ra thông tin của quản lý
                        include_once(DAO_PATH.'dao_user.php');
                        $manager = $userDao->getUsers([$data['page_admin']]);
                        $smarty->assign('managers', $manager);

                        //$smarty->assign('cities', $cities);
                        $smarty->assign('data', $data);
                        $smarty->assign('dataCon', $dataCon);
                        break;

                    case 'addmission':
                        if(!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                            _error(404);
                        }
                        //$data = $schoolDao->getSchoolById($_GET['id']);
                        $data = getSchoolData($_GET['id'], SCHOOL_INFO);
                        if (is_null($data)) {
                            _error(404);
                        }
                        //$dataCon = $schoolDao->getConfiguration($_GET['id']);
                        $dataCon = getSchoolData($_GET['id'], SCHOOL_CONFIG);

                        //$smarty->assign('cities', $cities);
                        $smarty->assign('data', $data);
                        $smarty->assign('dataCon', $dataCon);
                        break;

                    case 'info':
                        if(!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                            _error(404);
                        }
                        //$data = $schoolDao->getSchoolById($_GET['id']);
                        $data = getSchoolData($_GET['id'], SCHOOL_INFO);
                        if (is_null($data)) {
                            _error(404);
                        }
                        //$dataCon = $schoolDao->getConfiguration($_GET['id']);
                        $dataCon = getSchoolData($_GET['id'], SCHOOL_CONFIG);

                        //$smarty->assign('cities', $cities);
                        $smarty->assign('data', $data);
                        $smarty->assign('dataCon', $dataCon);
                        break;

                    case 'image':

                        break;
                    default:
                        _error(404);
                        break;
                }
                // valid inputs
//                if(!isset($_GET['id']) || !is_numeric($_GET['id'])) {
//                    _error(404);
//                }
//                $data = $schoolDao->getSchoolById($_GET['id']);
//                if (is_null($data)) {
//                    _error(404);
//                }
//
//                //Lấy ra thông tin của quản lý
//                include_once(DAO_PATH.'dao_user.php');
//                $manager = $userDao->getUsers([$data['page_admin']]);
//                $smarty->assign('managers', $manager);
//
//                //$smarty->assign('cities', $cities);
//                $smarty->assign('data', $data);
//                // page header
//                page_header(__("NOGA Management")." &rsaquo; ".__("School")." &rsaquo; ".$data['page_title']);
                break;
            case 'add':
                // page header
                page_header(__("NOGA Management")." &rsaquo; ".__("School")." &rsaquo; ".__("Add New"));
                $systemSchools = $schoolDao->getSystemSchools();
                $smarty->assign('systemSchools', $systemSchools);
                break;
            case 'viewuser':
                include_once(DAO_PATH.'dao_parent.php');

                $parentDao = new ParentDAO();
                include_once(DAO_PATH.'dao_child.php');

                $childDao = new ChildDAO();
                page_header(__("NOGA Management")." &rsaquo; ".__("School")." &rsaquo; ".__("View user"));
                if(!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }
                //$data = $schoolDao->getSchoolById($_GET['id']);
                $data = getSchoolData($_GET['id'], SCHOOL_INFO);
                if (is_null($data)) {
                    _error(404);
                }
                $smarty->assign('data', $data);
//                $results = array();
//                $parents = array();
//                $parents = $parentDao->getParentOfSchoolForNoga($_GET['id']);
//                $countLogin = 0;
//                $countNoLogin = 0;
//                foreach ($parents as $parent) {
//                    $children = $childDao->getChildren($parent['user_id']);
//                    $parent['children'] = $children;
//                    $results[] = $parent;
//                    if(!is_empty($parent['user_last_login'])) {
//                        $countLogin++;
//                    } else {
//                        $countNoLogin++;
//                    }
//                }
//
//                $smarty->assign('results', $results);
//                $smarty->assign('countLogin', $countLogin);
//                $smarty->assign('countNoLogin', $countNoLogin);
//                //$classes = $classDao->getClassesOfSchool($_GET['id']);
//                $classes = getSchoolData($_GET['id'], SCHOOL_CLASSES);
//                $smarty->assign('classes', $classes);
//                $smarty->assign('data', $data);
//$classes = $classDao->getClassNamesOfSchool($school['page_id']);
                //Memcache - Lấy ra các lớp của 1 trường
                $classes = getSchoolData($_GET['id'], SCHOOL_CLASSES);
                $smarty->assign('classes', $classes);

                //Lấy điều kiện search trước đó ra khỏi session
                $condition = $_SESSION[SESSION_KEY_SEARCH_CHILDREN_NOGA];
                $keyword = isset($condition)? $condition['keyword']: '';
                $classId = isset($condition)? $condition['class_id']: '';
                $page = isset($condition)? $condition['page']: 1;
                if($page == '') {
                    $page = 1;
                }
                $result = array();
                $totalChildren = $childDao->searchCount($keyword, $_GET['id'], $classId, STATUS_ACTIVE);
                $children = $childDao->searchPaging($keyword, $_GET['id'], $classId, $page, PAGING_LIMIT, STATUS_ACTIVE, 1);

                $result['total'] = $totalChildren;
                $result['page_count'] = ceil(($totalChildren + 0.0)/PAGING_LIMIT);
                $result['children'] = $children;
                $result['page'] = $page;
                $result['class_id'] = $classId;
                $result['keyword'] = $keyword;
                $username = $data['page_name'];
                $school = $data;
                $smarty->assign('result', $result);
                $smarty->assign('username', $username);
                $smarty->assign('school', $school);
//                echo "<pre>";
//                print_r($result);
//                echo "</pre>";
//                die();
                break;
            case 'module':
                page_header(__("NOGA Management")." &rsaquo; ".__("School")." &rsaquo; ".__("Module"));
                if(!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }

                // Lấy cấu hình chi tiết những module của trường
                $data = getSchoolData($_GET['id'], SCHOOL_CONFIG);
                $schoolInfo = getSchoolData($_GET['id'], SCHOOL_INFO);

                $smarty->assign('school', $data);
                $smarty->assign('defaultModule', $defaultModule);
                $smarty->assign('moduleName', $moduleName);
                $smarty->assign('schoolInfo', $schoolInfo);
                break;
            default:
                _error(404);
                break;
        }

        $smarty->assign('configure', $_GET['p4']);
        break;
    /* ADD START - ManhDD 03/04/2021 */
    case 'subjects':
        switch ($_GET['sub_view']) {
            case '':
                page_header(__("NOGA Management") . " &rsaquo; " . __("Subject list"));
                $subjects = null;
                $subjects = $subjectDao->getAllSubjects();
                $smarty->assign('nogaRole', $nogaRole);
                $smarty->assign('rows', $subjects);
                break;
            case 'edit':
                if(!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }
                //$data = $schoolDao->getSchoolById($_GET['id']);
                $data = getSubjectData($_GET['id'], SUBJECT_INFO);
                if (is_null($data)) {
                    _error(404);
                }
                $smarty->assign('data', $data);
//                        $smarty->assign('dataCon', $dataCon);
                // page header
                page_header(__("NOGA Management")." &rsaquo; ".__("Subject")." &rsaquo; ".$data['subject_name']);

                break;
            case 'add':
                // page header
                page_header(__("NOGA Management")." &rsaquo; ".__("School")." &rsaquo; ".__("Add New"));
                $systemSchools = $schoolDao->getSystemSchools();
                $smarty->assign('systemSchools', $systemSchools);
                break;
            default:
                _error(404);
                break;
        }
        break;
    /* ADD END - ManhDD 03/04/2021 */
    case 'notifications':
        // page header

        switch ($_GET['sub_view']) {
            case 'immediately':

                page_header(__("NOGA Management")." &rsaquo; ".__("Notifications") ." &rsaquo; ".__("Notify immediately"));
                break;

            case 'schedule':

                page_header(__("NOGA Management")." &rsaquo; ".__("Notifications") ." &rsaquo; ".__("Appointment"));
                break;

            case 'list':
                page_header(__("NOGA Management")." &rsaquo; ".__("Notifications") ." &rsaquo; ".__("Lists"));
                $notifications = $nogaNotificationDao->getAllNotification();
                // assign variables
                $smarty->assign('rows', $notifications);

                break;

            case 'edit':
                page_header(__("NOGA Management")." &rsaquo; ".__("Notifications") ." &rsaquo; ".__("Edit"));
                $notification = $nogaNotificationDao->getNotification($_GET['id']);
                if (is_null($notification)) {
                    _error(404);
                }
                // assign variables
                $smarty->assign('data', $notification);

                break;

            default:
                _error(404);
                break;

        }
        break;

    case 'statistics':
        switch ($_GET['sub_view']) {
            case 'usersonline':
                page_header(__("NOGA Management")." &rsaquo; ".__("Statistics") ." &rsaquo; ".__("Users onnline"));

                // Lấy danh sách user online ngày hôm nay
                $begin = date('d/m/Y');
                $useronline = getUserOnline($begin, $begin);
                $smarty->assign('useronline', $useronline);
                $smarty->assign('begin', $begin);
//                echo "<pre>";
//                print_r($useronline);
//                echo "</pre>";
//                die('qqq');
                break;
            case 'schools':
                page_header(__("NOGA Management")." &rsaquo; ".__("Statistics") ." &rsaquo; ".__("School"));

                $schools = $statisticDao->getAllSchoolStatisticsForNoga();

                $beginDate = date($system['date_format'], mktime(0, 0, 0, date("m"), 1, date("Y")));
                $fromDate = date('01/m/Y');
                $toDate =  date('d/m/Y');
                $result = array();
                $schoolIds = array();
                foreach ($schools as $row) {
                    $schoolIds[] = $row['page_id'];
                }
                $schoolIds[] = 0;
                foreach ($schoolIds as $schoolId) {
                    $statistic = getStatisticsInteractive($schoolId, ALL, $fromDate, $toDate);
                    foreach ($statistic_keys as $key) {
                        $result[$key]['school_views'] = $result[$key]['school_views'] + $statistic[$key]['school_views'];
                        $result[$key]['parent_views'] = $result[$key]['parent_views'] + $statistic[$key]['parent_views'];
                        $result[$key]['school_createds'] = $result[$key]['school_createds'] + $statistic[$key]['school_createds'];
                        $result[$key]['parent_createds'] = $result[$key]['parent_createds'] + $statistic[$key]['parent_createds'];
                    }
                }

                $smarty->assign('rows', $schools);
                $smarty->assign('keys', $statistic_keys);
                $smarty->assign('statistic', $result);
                $smarty->assign('begin', $beginDate);
                break;

            case 'schooldetail':
                $school = getSchoolData($_GET['id'], SCHOOL_INFO);
                $smarty->assign('school', $school);

                $beginDate = date($system['date_format'], mktime(0, 0, 0, date("m"), 1, date("Y")));
                $statistic_for_noga = $statisticDao->getSchoolStatisticInThisMonth($_GET['id']);

                /* ---------- LẤY THỐNG KÊ LƯỢT TƯƠNG TÁC  --------- */
                $fromDate = date('01/m/Y');
                $toDate =  date('d/m/Y');
                $statistic = getStatisticsInteractive($school['page_id'], ALL, $fromDate, $toDate);
//                echo "<pre>";
//                print_r($statistic);
//                echo "</pre>";
//                die('qqq');
                $statistic_for_school = getSchoolStatistics($school['page_id'], ALL, $fromDate, $toDate);
                /* ---------- END - LẤY THỐNG KÊ LƯỢT TƯƠNG TÁC --------- */

                /* ---------- LẤY THÔNG TIN ĐÁNH GIÁ TRƯỜNG, GIÁO VIÊN --------- */
                $reviews = $reviewDao->getReviewInfo($school['page_id']);
                /* ---------- END - LẤY THÔNG TIN ĐÁNH GIÁ TRƯỜNG, GIÁO VIÊN --------- */

                $smarty->assign('begin', $beginDate);
                $smarty->assign('statistic_for_noga', $statistic_for_noga);
                $smarty->assign('statistic_for_school', $statistic_for_school);
                $smarty->assign('statistic', $statistic);
                $smarty->assign('reviews', $reviews);
                $smarty->assign('keys', $statistic_keys);
                page_header(__("NOGA Management")." &rsaquo; ".__("Statistics") ." &rsaquo; ".__("School")." &rsaquo; ". $school['page_title']);
                break;

            case 'reviewschool':
                $school = getSchoolData($_GET['id'], SCHOOL_INFO);
                $smarty->assign('school', $school);

                $reviews = $reviewDao->getSchoolReview($school['page_id']);
                $smarty->assign('rows', $reviews);
                page_header(__("NOGA Management")." &rsaquo; ".__("School Reviews") . " &rsaquo; ". $school['page_title']);
                break;

            case 'reviewteachers':
                $school = getSchoolData($_GET['id'], SCHOOL_INFO);
                $smarty->assign('school', $school);

                //Lấy ra danh sách đánh giá trường
                $reviews = $reviewDao->getTeacherReviewInSchool($school['page_id']);
                $smarty->assign('rows', $reviews);
                page_header(__("School Management")." &rsaquo; ".__("Teacher Reviews"). " &rsaquo; ". $school['page_title']);
                break;

            case 'reviewteacher':
                if(!isset($_GET['p4']) || !is_numeric($_GET['p4'])) {
                    _error(404);
                }
                $result = array();

                $class = getClassData($_GET['id'], CLASS_INFO);
                if(is_null($class)) {
                    _error(404);
                }
                $result['class'] = $class;

                $school = getSchoolData($class['school_id'], SCHOOL_INFO);
                $smarty->assign('school', $school);

                $teacher = getTeacherData($_GET['p4'], TEACHER_INFO);
                if(is_null($class)) {
                    _error(404);
                }
                $result['teacher'] = $teacher;

                //Lấy ra danh sách đánh giá trường
                $reviews = $reviewDao->getTeacherReviewDetail($_GET['p4'], $_GET['id']);
                $result['reviews'] = $reviews;
                $smarty->assign('rows', $result);
                page_header(__("School Management")." &rsaquo; ".__("Teacher Reviews") ." &rsaquo; ". $teacher['user_firstname'] . " " . $teacher['user_lastname']);
                break;

            case 'topics':
                page_header(__("NOGA Management"). " &rsaquo; " . __("Statistics") . " &rsaquo; " . __("Groups"). "&comma; " . __("topics"));

                $toDate =  date('d/m/Y');
                $fromDate = date('01/m/Y');

                $topics = $statisticDao->getTopicInteractive($fromDate, $toDate);
                $smarty->assign('begin', $fromDate);
                $smarty->assign('rows', $topics);

                break;
        }
        break;

    case 'users':
        // get content
        switch ($_GET['sub_view']) {
            case '':
                global $system;
                // page header
                page_header(__("Admin")." &rsaquo; ".__("Users"));

                // get data
                $get_rows = $db->query("SELECT * FROM users") or _error(SQL_ERROR);
                if($get_rows->num_rows > 0) {
                    while($row = $get_rows->fetch_assoc()) {
                        $row['user_picture'] = User::get_picture($row['user_picture'], $row['user_gender']);
                        $rows[] = $row;
                    }
                }

                // assign variables
                $smarty->assign('rows', $rows);

//                //Lấy điều kiện search trước đó ra khỏi session
//                $condition = $_SESSION[SESSION_KEY_SEARCH_USER];
//                $keyword = isset($condition)? $condition['keyword']: '';
//                $page = isset($condition)? $condition['page']: 1;
//
//                $limit = PAGING_LIMIT;
//                if($keyword) {
//                    $strSql = sprintf('SELECT count(user_id) AS cnt FROM users WHERE user_name LIKE %1$s OR user_fullname LIKE %1$s OR user_email LIKE %1$s', secure($keyword));
//                    $get_rows_count = $db->query($strSql) or _error(SQL_ERROR);
//
//                    $strSql = sprintf('SELECT * FROM users WHERE user_name LIKE %1$s OR user_fullname LIKE %1$s OR user_email LIKE %1$s
//                    ORDER BY user_id LIMIT 0, %2$s', secure($keyword), $limit);
//
//                    $get_rows_user = $db->query($strSql) or _error(SQL_ERROR);
//                } else {
//                    $get_rows_count = $db->query("SELECT count(user_id) AS cnt FROM users") or _error(SQL_ERROR);
//
//                    $strSql = sprintf("SELECT * FROM users ORDER BY user_id ASC LIMIT %s, %s", ($page - 1)* $limit, $limit);
//                    $get_rows_user = $db->query($strSql) or _error(SQL_ERROR);
//                }
//                $count_user = $get_rows_count->fetch_assoc()['cnt'];
//
//                $users = array();
//                if($get_rows_user->num_rows > 0) {
//                    while ($user = $get_rows_user->fetch_assoc()) {
//                        $user['user_picture'] = User::get_picture($user['user_picture'], $user['user_gender']);
////                        $user_picture = $system['system_url'].'//'.$user['user_picture'];
////                        $user['user_picture'] = $user_picture;
//                        $users[] = $user;
//                    }
//                }
//
//                // echo "<pre>"; print_r($users); die;
//                $result = array();
//                $result['total'] = $count_user;
//                $result['page_count'] = ceil(($count_user + 0.0)/PAGING_LIMIT);
//                $result['users'] = $users;
//                $result['page'] = $page;
//                $result['keyword'] = $keyword;
//
//                $smarty->assign('result', $result);
                break;
            case 'userregion':
                // page header
                page_header(__("Admin")." &rsaquo; ".__("Regional management account"));
                // Lấy danh sách quản lý trong bảng ci_region_manage

                $result = $regionManageDao->getAllUserRegionManageByNogaProvider();

                $smarty->assign('result', $result);
                $smarty->assign('cities', $cities);
                break;

            case 'admins':
                // page header
                page_header(__("Admin")." &rsaquo; ".__("Users")." &rsaquo; ".__("Admins"));

                // get data
                $get_rows = $db->query("SELECT * FROM users WHERE user_group = '1'") or _error(SQL_ERROR);
                if($get_rows->num_rows > 0) {
                    while($row = $get_rows->fetch_assoc()) {
                        $row['user_picture'] = User::get_picture($row['user_picture'], $row['user_gender']);
                        $rows[] = $row;
                    }
                }

                // assign variables
                $smarty->assign('rows', $rows);
                break;

            case 'moderators':
                // page header
                page_header(__("Admin")." &rsaquo; ".__("Users")." &rsaquo; ".__("Moderators"));

                // get data
                $get_rows = $db->query("SELECT * FROM users WHERE user_group = '2'") or _error(SQL_ERROR);
                if($get_rows->num_rows > 0) {
                    while($row = $get_rows->fetch_assoc()) {
                        $row['user_picture'] = User::get_picture($row['user_picture'], $row['user_gender']);
                        $rows[] = $row;
                    }
                }

                // assign variables
                $smarty->assign('rows', $rows);
                break;

            case 'online':
                // page header
                page_header(__("Admin")." &rsaquo; ".__("Users")." &rsaquo; ".__("Online"));

                // get data
                $get_rows = $db->query("SELECT users.* FROM users_online INNER JOIN users ON users_online.user_id = users.user_id") or _error(SQL_ERROR);
                if($get_rows->num_rows > 0) {
                    while($row = $get_rows->fetch_assoc()) {
                        $row['user_picture'] = User::get_picture($row['user_picture'], $row['user_gender']);
                        $rows[] = $row;
                    }
                }

                // assign variables
                $smarty->assign('rows', $rows);
                break;

            case 'banned':
                // page header
                page_header(__("Admin")." &rsaquo; ".__("Users")." &rsaquo; ".__("Banned"));

                // get data
                $get_rows = $db->query("SELECT * FROM users WHERE user_banned = '1'") or _error(SQL_ERROR);
                if($get_rows->num_rows > 0) {
                    while($row = $get_rows->fetch_assoc()) {
                        $row['user_picture'] = User::get_picture($row['user_picture'], $row['user_gender']);
                        $rows[] = $row;
                    }
                }

                // assign variables
                $smarty->assign('rows', $rows);
                break;

            case 'edit':
                // valid inputs
                if(!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }

                // get data
                $get_data = $db->query(sprintf("SELECT * FROM users WHERE user_id = %s", secure($_GET['id'], 'int') )) or _error(SQL_ERROR);
                if($get_data->num_rows == 0) {
                    _error(404);
                }
                $data = $get_data->fetch_assoc();
                $data['user_picture'] = User::get_picture($data['user_picture'], $data['user_gender']);
                /* get user's friends */
                $data['friends'] = count($user->get_friends_ids($data['user_id']));
                $data['followings'] = count($user->get_followings_ids($data['user_id']));
                $data['followers'] = count($user->get_followers_ids($data['user_id']));
                /* parse birthdate */
                $data['user_birthdate_parsed'] = date_parse($data['user_birthdate']);

                // assign variables
                $smarty->assign('data', $data);

                // page header
                page_header(__("Admin")." &rsaquo; ".__("Users")." &rsaquo; ".$data['user_firstname'] . " " . $data['user_lastname']);
                break;

            default:
                _error(404);
                break;
        }
        break;
    case 'foetusdevelopments':
        switch ($_GET['sub_view']) {
            case '':
                page_header(__("Noga")." &rsaquo; ".__("Foetus development"));
                $foetus_info = $foetusDevelopmentDao->getFoetusInfoForNoga();
                $smarty->assign('results', $foetus_info);
                break;

            case 'add':
                page_header(__("Noga")." &rsaquo; ".__("Add information about your foetus's development"));
                break;

            case 'edit':
                page_header(__("Noga")." &rsaquo; ".__("Update information about your foetus's development"));
                if(!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }
                $data = $foetusDevelopmentDao->getFoetusInfoDetail($_GET['id']);
                $smarty->assign('data', $data);
                break;

            case 'push':
                page_header(__("Noga")." &rsaquo; ".__("Add information about your foetus's development"));
                break;

            default:
                _error(404);
                break;
        }
        break;

    case 'childdevelopments':
        switch ($_GET['sub_view']) {
            case '':
                page_header(__("Noga")." &rsaquo; ".__("Child development"));

                $child_development = $childDevelopmentDao->getChildDevelopmentForNoga();

                $smarty->assign('results', $child_development);
                break;
            case 'add':
                page_header(__("Noga")." &rsaquo; ".__("Add information about your child's development"));
                break;
            case 'edit':
                page_header(__("Noga")." &rsaquo; ".__("Update information about your child's development"));

                if(!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }

                $data = $childDevelopmentDao->getChildDevelopmentDetail($_GET['id']);

                $smarty->assign('data', $data);
                break;
            case 'push':
                page_header(__("Noga")." &rsaquo; ".__("Add information about your foetus's development"));
                break;
            default:
                _error(404);
                break;
        }
        break;
    case 'childmonths':
        switch ($_GET['sub_view']) {
            case '':
                page_header(__("Noga")." &rsaquo; ".__("Month age information"));

                $childMonths = $childMonthDao->getAllChildMonth();

                $smarty->assign('childMonths', $childMonths);
                break;
            case 'add':
                page_header(__("Noga")." &rsaquo; ".__("Add New"));
                break;
            case 'edit':
                page_header(__("Noga")." &rsaquo; ".__("Edit"));

                if(!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }

                $data = $childMonthDao->getChildMonthDetail($_GET['id']);

                $smarty->assign('data', $data);
                break;
            default:
                _error(404);
                break;
        }
        break;
    case 'pregnancys':
        switch ($_GET['sub_view']) {
            case '':
                page_header(__("Noga")." &rsaquo; ".__("Pregnancy information"));

                $pregnancys = $childMonthDao->getAllPregnancys();

                $smarty->assign('pregnancys', $pregnancys);
                break;
            case 'add':
                page_header(__("Noga")." &rsaquo; ".__("Add New"));
                break;
            case 'edit':
                page_header(__("Noga")." &rsaquo; ".__("Edit"));

                if(!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                    _error(404);
                }

                $data = $childMonthDao->getPregnancyDetail($_GET['id']);

                $smarty->assign('data', $data);
                break;
            default:
                _error(404);
                break;
        }
        break;
    case 'usernews':
        // get content
        switch ($_GET['sub_view']) {
            case '':
                // page header
                page_header(__("Noga")." &rsaquo; ".__("New users"));
                break;
            default:
                _error(404);
                break;
        }
        break;
    case 'manageandteachers':
        // get content
        switch ($_GET['sub_view']) {
            case '':
                // page header
                page_header(__("Noga")." &rsaquo; ".__("Manage and teacher"));
                break;
            default:
                _error(404);
                break;
        }
        break;
    case 'pages':
        // get content
        switch ($_GET['sub_view']) {
            case '':
                // page header
                page_header(__("Noga")." &rsaquo; ".__("Pages"));

                // get data
                $get_rows = $db->query("SELECT * FROM pages") or _error(SQL_ERROR);
                if($get_rows->num_rows > 0) {
                    while($row = $get_rows->fetch_assoc()) {
                        $row['page_picture'] = User::get_picture($row['page_picture'], 'page');
                        $rows[] = $row;
                    }
                }

                // assign variables
                $smarty->assign('results', $rows);
                break;
            default:
                _error(404);
                break;
        }
        break;
	default:
		_error(404);
}

// assign variables
$smarty->assign('view', $_GET['view']);
$smarty->assign('sub_view', $_GET['sub_view']);

// page footer
page_footer("noga");

?>