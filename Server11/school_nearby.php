<?php
/**
 * Chức năng tìm trường quanh đây
 * 
 * @package ConIu v1
 * @author ConIu
 */

// set override_shutdown
$override_shutdown = true;

// fetch bootstrap
require('bootstrap.php');

// Kiểm tra user login rồi hay chưa
user_access();

// check username
if(is_empty($_GET['username']) || !valid_username($_GET['username'])) {
    _error(404);
}

include_once(DAO_PATH.'dao_school.php');
include_once(DAO_PATH.'dao_class.php');
include_once(DAO_PATH.'dao_class_level.php');
include_once(DAO_PATH.'dao_teacher.php');
include_once(DAO_PATH.'dao_child.php');
include_once(DAO_PATH.'dao_event.php');
include_once(DAO_PATH.'dao_medicine.php');
include_once(DAO_PATH.'dao_tuition.php');
include_once(DAO_PATH.'dao_attendance.php');
include_once(DAO_PATH.'dao_service.php');
include_once(DAO_PATH.'dao_report.php');

$schoolDao = new SchoolDAO();
$classDao = new ClassDAO();
$classLevelDao = new ClassLevelDAO();
$teacherDao = new TeacherDAO();
$childDao = new ChildDAO();
$eventDao = new EventDAO();
$medicineDao = new MedicineDAO();
$tuitionDao = new TuitionDAO();
$attendanceDao = new AttendanceDAO();
$serviceDao = new ServiceDAO();
$reportDao = new ReportDao();

//Lấy ra thông tin trường, bao gồm cả role_id của user hiện tại
$school = $schoolDao->getSchoolByUsername($_GET['username'], $user->_data['user_id']);
// Lấy danh sách user có trong bảng ci_permission
// page content
switch ($_GET['view']) {
	case '':
		// page header
		page_header(__("School Management")." &rsaquo; ".__("Dashboard"));

        $insights['child_cnt'] = $school['male_count'] + $school['female_count'];

        $today = date($system['date_format']);
        $medicines = $medicineDao->getMedicineOnDate($school['page_id'], $today);
        $insights['medicines'] = $medicines;

        $events = $eventDao->getEvents($school['page_id'],  $school['event_count_on_daskboard']);
        $insights['events'] = $events;

    	// assign variables
		$smarty->assign('insights', $insights);
		break;

	case 'settings':
		// page header
		page_header(__("School Management")." &rsaquo; ".__("Settings"));

		break;

	default:
		_error(404);
}

// assign variables
$smarty->assign('school', $school);
$smarty->assign('username', $_GET['username']);
$smarty->assign('view', $_GET['view']);
$smarty->assign('sub_view', $_GET['sub_view']);

// page footer
page_footer("school");

?>