<?php
/**
 * started
 * 
 * @package CINet v1.1
 * @author NOGA Co., Ltd
 */

// fetch bootstrap
require('bootstrap.php');

// user access
if(!$user->_logged_in) {
    user_login();
}

try {

	/* check if getted started */
	if(!$system['getting_started'] || $user->_data['user_started']
    ) {
		redirect();
	}

	/* get finished */
	if(isset($_GET['finished'])) {
		/* update user */
        $db->query(sprintf("UPDATE users SET user_started = '1' WHERE user_id = %s", secure($user->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
		redirect();
	}

	//Inet - Thêm trạng giá trị step để hiển thị màn hình tương ứng
	$step = 1;
	if (isset($_GET['step']) && ($_GET['step'] == 2 || $_GET['step'] == 3) ) {
	    $step = $_GET['step'];
	}
    $smarty->assign('step', $step);

    //parse birthdate
    //$user->_data['user_birthdate_parsed'] = date_parse($user->_data['user_birthdate']);
    $user->_data['user_birthdate'] = !is_empty($user->_data['user_birthdate']) ? toSysDate($user->_data['user_birthdate']) : "";
    //parse date of issue
    $user->_data['date_of_issue'] = toSysDate($user->_data['date_of_issue']);

	// get new people
	//$new_people = $user->get_new_people(0, true);
	/* assign variables */
	//$smarty->assign('new_people', $new_people);

} catch (Exception $e) {
	_error(__("Error"), $e->getMessage());
}

// page header
page_header($system['system_title']." &rsaquo; ".__("Getting Started"));

// page footer
page_footer("started");

?>