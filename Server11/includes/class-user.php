<?php
/**
 * class -> user
 * 
 * @package CINet v1.1
 * @author NOGA Co., Ltd
 */

class User {

	public $_logged_in = false;
    public $_is_admin = false;
    public $_data = array();

	private $_cookie_user_id = "c_user";
    private $_cookie_user_token = "xs";
    private $_cookie_user_referrer = "ref";


    /* ------------------------------- */
    /* __construct */
    /* ------------------------------- */


    /**
     * __construct
     *
     * @throws Exception
     */
    public function __construct($user_id = 0, $session_token = null) {
        global $db, $bootstrap_calling;

        $query = "";
        /* CI - mobile query lấy ra user từ user_id và session_token */
        if ($user_id > 0 && !is_null($session_token)) {
            $query = $db->query(sprintf("SELECT users.* FROM users 
                INNER JOIN users_sessions ON users.user_id = users_sessions.user_id
                WHERE users_sessions.user_id = %s AND users_sessions.session_token = %s",
                secure($user_id, 'int'), secure($session_token))) or _error(SQL_ERROR_THROWEN);

        } else if(isset($_COOKIE[$this->_cookie_user_id]) && isset($_COOKIE[$this->_cookie_user_token])) {
            $query = $db->query(sprintf("SELECT users.* FROM users 
                INNER JOIN users_sessions ON users.user_id = users_sessions.user_id 
                WHERE users_sessions.user_id = %s AND users_sessions.session_token = %s",
                secure($_COOKIE[$this->_cookie_user_id], 'int'), secure($_COOKIE[$this->_cookie_user_token]) )) or _error(SQL_ERROR_THROWEN);
        }
        if($query->num_rows > 0) {
            $this->_data = $query->fetch_assoc();
            $this->_logged_in = true;
            // Lưu ý: thêm "user_group" == 4 vào để cho nhân viên NOGA quản lý trường và người dùng
            $this->_is_admin = (($this->_data['user_group'] == 1) || ($this->_data['user_group'] == USER_MOD))? true: false;
            /* active session */
            $this->_data['active_session'] = $_COOKIE[$this->_cookie_user_token];
            /* get user picture */
            $this->_data['user_picture'] = $this->get_picture($this->_data['user_picture'], $this->_data['user_gender']);
            $this->_data['user_picture_100'] = get_picture_size($this->_data['user_picture'], $this->_data['user_gender'], 100);

            $lastest_load_time = 0; //Thời gian load gần nhất
            $current_time = microtime(true);
            if (isset( $_SESSION['lastest_load_time'] ) ) {
                $lastest_load_time = $_SESSION['lastest_load_time'];
            }
            //Nếu không phải đang làm trong phần quản lý trường thì load các thông tin dưới
            //Hoặc thời gian đã quá thời gian NEW_USER_RELOAD_INTERVAL
            if (($bootstrap_calling != BOOTSTRAP_CALLING_SCHOOL) || (($current_time - $lastest_load_time) > NEW_USER_RELOAD_INTERVAL)) {
                /* get all friends ids */
                $friends_ids = $this->get_friends_ids($this->_data['user_id']);
                $this->_data['friends_ids'] = $friends_ids;

                /* get all followings ids */
                $followings_ids = $this->get_followings_ids($this->_data['user_id']);
                $this->_data['followings_ids'] = $followings_ids;

                /* get all friend requests ids */
                $friend_requests_ids = $this->get_friend_requests_ids();
                $this->_data['friend_requests_ids'] = $friend_requests_ids;

                /* get all friend requests sent ids */
                $friend_requests_sent_ids = $this->get_friend_requests_sent_ids();
                $this->_data['friend_requests_sent_ids'] = $friend_requests_sent_ids;

                /* get friend requests */
                $friend_requests = $this->get_friend_requests();
                $this->_data['friend_requests'] = $friend_requests;
                $this->_data['friend_requests_count'] = count($this->_data['friend_requests']);

                /* get friend requests sent */
                $friend_requests_sent = $this->get_friend_requests_sent();
                $this->_data['friend_requests_sent'] = $friend_requests_sent;
                $this->_data['friend_requests_sent_count'] = count($this->_data['friend_requests_sent']);

                /* get search log */
                //$this->_data['search_log'] = $this->get_search_log();
                /* get new people */
                $new_people = $this->get_new_people(0, true);
                $this->_data['new_people'] = $new_people;

                if ($bootstrap_calling == BOOTSTRAP_CALLING_SCHOOL) {
                    $_SESSION['friends_ids'] = $friends_ids;
                    $_SESSION['followings_ids'] = $followings_ids;
                    $_SESSION['friend_requests_ids'] = $friend_requests_ids;
                    $_SESSION['friend_requests_sent'] = $friend_requests_sent;
                    $_SESSION['friend_requests'] = $friend_requests;
                    $_SESSION['friend_requests_sent_ids'] = $friend_requests_sent_ids;
                    $_SESSION['new_people'] = $new_people;
                    //Cập nhật thời gian vừa load dữ liệu vào session
                    $_SESSION['lastest_load_time'] = $current_time;
                }
            } else if ($bootstrap_calling == BOOTSTRAP_CALLING_SCHOOL) {
                //Khi chưa đủ thời gian thì lấy trong session ra
                $this->_data['friends_ids'] = $_SESSION['friends_ids'];
                /* get all followings ids */
                $this->_data['followings_ids'] = $_SESSION['followings_ids'];
                /* get all friend requests ids */
                $this->_data['friend_requests_ids'] = $_SESSION['friend_requests_ids'];
                /* get all friend requests sent ids */
                $this->_data['friend_requests_sent_ids'] = $_SESSION['friend_requests_sent_ids'];
                /* get friend requests */
                $this->_data['friend_requests'] = $_SESSION['friend_requests'];
                $this->_data['friend_requests_count'] = count($this->_data['friend_requests']);
                /* get friend requests sent */
                $this->_data['friend_requests_sent'] = $_SESSION['friend_requests_sent'];
                $this->_data['friend_requests_sent_count'] = count($this->_data['friend_requests_sent']);
                /* get new people */
                $this->_data['new_people'] = $_SESSION['new_people'];
            }

            /* get conversations */
            //$this->_data['conversations'] = $this->get_conversations();
            /* get notifications */
            $this->_data['notifications'] = $this->get_notifications();
            /* get package */
            /*$this->_data['can_boost_posts'] = false;
            $this->_data['can_boost_pages'] = false;
            if($system['packages_enabled'] && ($this->_is_admin || $this->_data['user_subscribed'])) {
                if($this->_is_admin || ($this->_data['boost_posts_enabled'] && ($this->_data['user_boosted_posts'] < $this->_data['boost_posts'])) ) {
                    $this->_data['can_boost_posts'] = true;
                }
                if($this->_is_admin || ($this->_data['boost_pages_enabled'] && ($this->_data['user_boosted_pages'] < $this->_data['boost_pages'])) ) {
                    $this->_data['can_boost_pages'] = true;
                }
            }*/
        }
    }

    /* ------------------------------- */
    /* Get Picture */
    /* ------------------------------- */

    /**
     * get_picture
     *
     * @param string $picture
     * @param string $type
     * @return string
     */

    static function get_picture($picture, $type) {
        global $system;
        if($picture == "") {
            switch ($type) {
                case MALE:
                    $picture = $system['system_url'].'/content/themes/'.$system['theme'].'/images/small/blank_profile_male.jpg';
                    break;

                case FEMALE:
                    $picture = $system['system_url'].'/content/themes/'.$system['theme'].'/images/small/blank_profile_female.jpg';
                    break;

                case 'school':
                    $picture = $system['system_url'].'/content/themes/'.$system['theme'].'/images/small/blank_page.png';
                    break;

                case 'class':
                    $picture = $system['system_url'].'/content/themes/'.$system['theme'].'/images/small/blank_group.png';
                    break;

                case 'page':
                    $picture = $system['system_url'].'/content/themes/'.$system['theme'].'/images/small/blank_page.png';
                    break;

                case 'group':
                    $picture = $system['system_url'].'/content/themes/'.$system['theme'].'/images/small/blank_group.png';
                    break;

                case 'game':
                    $picture = $system['system_url'].'/content/themes/'.$system['theme'].'/images/small/blank_game.png';
                    break;
            }
        } else {
            $picture = $system['system_uploads'].'/'.$picture;
        }
        return $picture;
    }


    /* ------------------------------- */
    /* Get Countries */
    /* ------------------------------- */

    /**
     * get_countries
     *
     * @return array
     */
    public function get_countries() {
        global $db, $system;
        $countries = array();
        $get_countries = $db->query("SELECT * FROM system_countries") or _error(SQL_ERROR_THROWEN);
        if($get_countries->num_rows > 0) {
            while($country = $get_countries->fetch_assoc()) {
                $countries[] = $country;
            }
        }
        return $countries;
    }


    /* ------------------------------- */
    /* Get Ids */
    /* ------------------------------- */

    /**
     * get_friends_ids
     *
     * @param integer $user_id
     * @return array
     */
    public function get_friends_ids($user_id) {
        global $db;
        $friends = array();
        $get_friends = $db->query(sprintf('SELECT users.user_id FROM friends INNER JOIN users ON (friends.user_one_id = users.user_id AND friends.user_one_id != %1$s) OR (friends.user_two_id = users.user_id AND friends.user_two_id != %1$s) WHERE status = 1 AND (user_one_id = %1$s OR user_two_id = %1$s)', secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        if($get_friends->num_rows > 0) {
            while($friend = $get_friends->fetch_assoc()) {
                $friends[] = $friend['user_id'];
            }
        }
        return $friends;
    }


    /**
     * get_followings_ids
     *
     * @param integer $user_id
     * @return array
     */
    public function get_followings_ids($user_id) {
        global $db;
        $followings = array();
        $get_followings = $db->query(sprintf("SELECT following_id FROM followings WHERE user_id = %s", secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        if($get_followings->num_rows > 0) {
            while($following = $get_followings->fetch_assoc()) {
                $followings[] = $following['following_id'];
            }
        }
        return $followings;
    }


    /**
     * get_followers_ids
     *
     * @param integer $user_id
     * @return array
     */
    public function get_followers_ids($user_id) {
        global $db;
        $followers = array();
        $get_followers = $db->query(sprintf("SELECT user_id FROM followings WHERE following_id = %s", secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        if($get_followers->num_rows > 0) {
            while($follower = $get_followers->fetch_assoc()) {
                $followers[] = $follower['user_id'];
            }
        }
        return $followers;
    }


    /**
     * get_friend_requests_ids
     *
     * @return array
     */
    public function get_friend_requests_ids() {
        global $db;
        $requests = array();
        $get_requests = $db->query(sprintf("SELECT user_one_id FROM friends WHERE status = 0 AND user_two_id = %s", secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
        if($get_requests->num_rows > 0) {
            while($request = $get_requests->fetch_assoc()) {
                $requests[] = $request['user_one_id'];
            }
        }
        return $requests;
    }


    /**
     * get_friend_requests_sent_ids
     *
     * @return array
     */
    public function get_friend_requests_sent_ids() {
        global $db;
        $requests = array();
        $get_requests = $db->query(sprintf("SELECT user_two_id FROM friends WHERE status = 0 AND user_one_id = %s", secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
        if($get_requests->num_rows > 0) {
            while($request = $get_requests->fetch_assoc()) {
                $requests[] = $request['user_two_id'];
            }
        }
        return $requests;
    }


    /**
     * get_pages_ids
     *
     * @return array
     */
    public function get_pages_ids() {
        global $db;
        $pages = array();
        if($this->_logged_in) {
            $get_pages = $db->query(sprintf("SELECT page_id FROM pages_likes WHERE user_id = %s", secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
            if($get_pages->num_rows > 0) {
                while($page = $get_pages->fetch_assoc()) {
                    $pages[] = $page['page_id'];
                }
            }
        }
        return $pages;
    }


    /**
     * get_groups_ids
     *
     * @param boolean $only_approved
     * @return array
     */
    public function get_groups_ids($only_approved = false) {
        global $db;
        $groups = array();
        if($only_approved) {
            $get_groups = $db->query(sprintf("SELECT group_id FROM groups_members WHERE approved = '1' AND user_id = %s", secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
        } else {
            $get_groups = $db->query(sprintf("SELECT group_id FROM groups_members WHERE user_id = %s", secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
        }

        if($get_groups->num_rows > 0) {
            while($group = $get_groups->fetch_assoc()) {
                $groups[] = $group['group_id'];
            }
        }
        return $groups;
    }


    /**
     * get_events_ids
     *
     * @return array
     */
    public function get_events_ids() {
        global $db;
        $events = array();
        $get_events = $db->query(sprintf("SELECT event_id FROM events_members WHERE user_id = %s", secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
        if($get_events->num_rows > 0) {
            while($event = $get_events->fetch_assoc()) {
                $events[] = $event['event_id'];
            }
        }
        return $events;
    }



    /* ------------------------------- */
    /* Get Users */
    /* ------------------------------- */


    /**
     * get_friends
     *
     * @param integer $user_id
     * @param integer $offset
     * @return array
     */
    public function get_friends($user_id, $offset = 0) {
        global $db, $system;
        $friends = array();
        $offset *= $system['min_results_even'];
        /* get the target user's privacy */
        $get_privacy = $db->query(sprintf("SELECT user_privacy_friends FROM users WHERE user_id = %s", secure($user_id, 'int') )) or _error(SQL_ERROR_THROWEN);
        $privacy = $get_privacy->fetch_assoc();
        /* check the target user's privacy  */
        if(!$this->_check_privacy($privacy['user_privacy_friends'], $user_id)) {
            return $friends;
        }
        $get_friends = $db->query(sprintf('SELECT users.user_id, users.user_name, users.user_fullname, users.user_firstname, users.user_lastname, users.user_gender, users.user_picture FROM friends INNER JOIN users ON (friends.user_one_id = users.user_id AND friends.user_one_id != %1$s) OR (friends.user_two_id = users.user_id AND friends.user_two_id != %1$s) WHERE status = 1 AND (user_one_id = %1$s OR user_two_id = %1$s) LIMIT %2$s, %3$s', secure($user_id, 'int'), secure($offset, 'int', false), secure($system['min_results_even'], 'int', false) )) or _error(SQL_ERROR_THROWEN);
        if($get_friends->num_rows > 0) {
            while($friend = $get_friends->fetch_assoc()) {
                $friend['user_picture'] = $this->get_picture($friend['user_picture'], $friend['user_gender']);
                /* get the connection between the viewer & the target */
                $friend['connection'] = $this->connection($friend['user_id']);
                $friends[] = $friend;
            }
        }
        return $friends;
    }


    /**
     * get_followings
     *
     * @param integer $user_id
     * @param integer $offset
     * @return array
     */
    public function get_followings($user_id, $offset = 0) {
        global $db, $system;
        $followings = array();
        $offset *= $system['min_results_even'];
        $get_followings = $db->query(sprintf('SELECT users.user_id, users.user_name, users.user_fullname, users.user_firstname, users.user_lastname, users.user_gender, users.user_picture FROM followings INNER JOIN users ON (followings.following_id = users.user_id) WHERE followings.user_id = %s LIMIT %s, %s', secure($user_id, 'int'), secure($offset, 'int', false), secure($system['min_results_even'], 'int', false) )) or _error(SQL_ERROR_THROWEN);
        if($get_followings->num_rows > 0) {
            while($following = $get_followings->fetch_assoc()) {
                $following['user_picture'] = $this->get_picture($following['user_picture'], $following['user_gender']);
                /* get the connection between the viewer & the target */
                $following['connection'] = $this->connection($following['user_id'], false);
                $followings[] = $following;
            }
        }
        return $followings;
    }


    /**
     * get_followers
     *
     * @param integer $user_id
     * @param integer $offset
     * @return array
     */
    public function get_followers($user_id, $offset = 0) {
        global $db, $system;
        $followers = array();
        $offset *= $system['min_results_even'];
        $get_followers = $db->query(sprintf('SELECT users.user_id, users.user_name, users.user_fullname, users.user_firstname, users.user_lastname, users.user_gender, users.user_picture FROM followings INNER JOIN users ON (followings.user_id = users.user_id) WHERE followings.following_id = %s LIMIT %s, %s', secure($user_id, 'int'), secure($offset, 'int', false), secure($system['min_results_even'], 'int', false) )) or _error(SQL_ERROR_THROWEN);
        if($get_followers->num_rows > 0) {
            while($follower = $get_followers->fetch_assoc()) {
                $follower['user_picture'] = $this->get_picture($follower['user_picture'], $follower['user_gender']);
                /* get the connection between the viewer & the target */
                $follower['connection'] = $this->connection($follower['user_id'], false);
                $followers[] = $follower;
            }
        }
        return $followers;
    }


    /**
     * get_blocked
     *
     * @param integer $offset
     * @return array
     */
    public function get_blocked($offset = 0) {
        global $db, $system;
        $blocks = array();
        $offset *= $system['max_results'];
        $get_blocks = $db->query(sprintf('SELECT users.user_id, users.user_name, users.user_fullname, users.user_firstname, users.user_lastname, users.user_gender, users.user_picture FROM users_blocks INNER JOIN users ON users_blocks.blocked_id = users.user_id WHERE users_blocks.user_id = %s LIMIT %s, %s', secure($this->_data['user_id'], 'int'), secure($offset, 'int', false), secure($system['max_results'], 'int', false) )) or _error(SQL_ERROR_THROWEN);
        if($get_blocks->num_rows > 0) {
            while($block = $get_blocks->fetch_assoc()) {
                $block['user_picture'] = $this->get_picture($block['user_picture'], $block['user_gender']);
                $block['connection'] = 'blocked';
                $blocks[] = $block;
            }
        }
        return $blocks;
    }


    /**
     * get_friend_requests
     *
     * @param integer $offset
     * @param integer $last_request_id
     * @return array
     */
    public function get_friend_requests($offset = 0, $last_request_id = null) {
        global $db, $system;
        $requests = array();
        $offset *= $system['max_results'];
        if($last_request_id !== null) {
            $get_requests = $db->query(sprintf("SELECT friends.id, friends.user_one_id as user_id, users.user_name, users.user_fullname, users.user_firstname, users.user_lastname, users.user_gender, users.user_picture FROM friends INNER JOIN users ON friends.user_one_id = users.user_id WHERE friends.status = 0 AND friends.user_two_id = %s AND friends.id > %s ORDER BY friends.id DESC", secure($this->_data['user_id'], 'int'), secure($last_request_id, 'int') )) or _error(SQL_ERROR_THROWEN);
        } else {
            $get_requests = $db->query(sprintf("SELECT friends.id, friends.user_one_id as user_id, users.user_name, users.user_fullname, users.user_firstname, users.user_lastname, users.user_gender, users.user_picture FROM friends INNER JOIN users ON friends.user_one_id = users.user_id WHERE friends.status = 0 AND friends.user_two_id = %s ORDER BY friends.id DESC LIMIT %s, %s", secure($this->_data['user_id'], 'int'), secure($offset, 'int', false), secure($system['max_results'], 'int', false) )) or _error(SQL_ERROR_THROWEN);
        }
        if($get_requests->num_rows > 0) {
            while($request = $get_requests->fetch_assoc()) {
                $request['user_picture'] = $this->get_picture($request['user_picture'], $request['user_gender']);
                $request['mutual_friends_count'] = $this->get_mutual_friends_count($request['user_id']);
                $requests[] = $request;
            }
        }
        return $requests;
    }


    /**
     * get_friend_requests_sent
     *
     * @param integer $offset
     * @return array
     */
    public function get_friend_requests_sent($offset = 0) {
        global $db, $system;
        $requests = array();
        $offset *= $system['max_results'];
        $get_requests = $db->query(sprintf("SELECT friends.user_two_id as user_id, users.user_name, users.user_fullname, users.user_firstname, users.user_lastname, users.user_gender, users.user_picture FROM friends INNER JOIN users ON friends.user_two_id = users.user_id WHERE friends.status = 0 AND friends.user_one_id = %s LIMIT %s, %s", secure($this->_data['user_id'], 'int'), secure($offset, 'int', false), secure($system['max_results'], 'int', false) )) or _error(SQL_ERROR_THROWEN);
        if($get_requests->num_rows > 0) {
            while($request = $get_requests->fetch_assoc()) {
                $request['user_picture'] = $this->get_picture($request['user_picture'], $request['user_gender']);
                $request['mutual_friends_count'] = $this->get_mutual_friends_count($request['user_id']);
                $requests[] = $request;
            }
        }
        return $requests;
    }


    /**
     * get_mutual_friends
     *
     * @param integer $user_two_id
     * @param integer $offset
     * @return array
     */
    public function get_mutual_friends($user_two_id, $offset = 0) {
        global $db, $system;
        $mutual_friends = array();
        $offset *= $system['max_results'];
        $mutual_friends = array_intersect($this->_data['friends_ids'], $this->get_friends_ids($user_two_id));
        /* if there is no mutual friends -> return empty array */
        if(count($mutual_friends) == 0) {
            return $mutual_friends;
        }
        /* slice the mutual friends array with system max results */
        $mutual_friends = array_slice($mutual_friends, $offset, $system['max_results']);
        /* if there is no more mutual friends to show -> return empty array */
        if(count($mutual_friends) == 0) {
            return $mutual_friends;
        }
        /* make a list from mutual friends */
        $mutual_friends_list = implode(',',$mutual_friends);
        /* get the user data of each mutual friend */
        $mutual_friends = array();
        $get_mutual_friends = $db->query("SELECT * FROM users WHERE user_id IN ($mutual_friends_list)") or _error(SQL_ERROR_THROWEN);
        if($get_mutual_friends->num_rows > 0) {
            while($mutual_friend = $get_mutual_friends->fetch_assoc()) {
                $mutual_friend['user_picture'] = $this->get_picture($mutual_friend['user_picture'], $mutual_friend['user_gender']);
                $mutual_friend['mutual_friends_count'] = $this->get_mutual_friends_count($mutual_friend['user_id']);
                $mutual_friends[] = $mutual_friend;
            }
        }
        return $mutual_friends;
    }


    /**
     * get_mutual_friends_count
     *
     * @param integer $user_two_id
     * @return integer
     */
    public function get_mutual_friends_count($user_two_id) {
        return count(array_intersect($this->_data['friends_ids'], $this->get_friends_ids($user_two_id)));
    }


    /**
     * get_new_people
     *
     * @param integer $offset
     * @param boolean $random
     * @return array
     */
    public function get_new_people($offset = 0, $random = false) {
        global $db, $system;
        $old_people_ids = array();
        $offset *= $system['min_results'];
        /* merge (friends, followings, friend requests & friend requests sent) and get the unique ids  */
        $old_people_ids = array_unique(array_merge($this->_data['friends_ids'], $this->_data['followings_ids'], $this->_data['friend_requests_ids'], $this->_data['friend_requests_sent_ids']));
        /* add the viewer to this list */
        $old_people_ids[] = $this->_data['user_id'];
        /* make a list from old people */
        $old_people_ids_list = implode(',',$old_people_ids);
        /* get users data not in old people list */
        $new_people = array();
        /* prepare where statement */
        $where = ($system['activation_enabled'])? "WHERE user_banned = '0' AND user_activated = '1' AND user_id NOT IN (%s)" : "WHERE user_banned = '0' AND user_id NOT IN (%s)";
        if($random) {
            $get_new_people = $db->query(sprintf("SELECT * FROM users ".$where." ORDER BY RAND() LIMIT %s, %s", $old_people_ids_list, secure($offset, 'int', false), secure($system['min_results'], 'int', false) )) or _error(SQL_ERROR_THROWEN);
        } else {
            $get_new_people = $db->query(sprintf("SELECT * FROM users ".$where." LIMIT %s, %s", $old_people_ids_list, secure($offset, 'int', false), secure($system['min_results'], 'int', false) )) or _error(SQL_ERROR_THROWEN);
        }
        if($get_new_people->num_rows > 0) {
            while($user = $get_new_people->fetch_assoc()) {
                $user['user_picture'] = $this->get_picture($user['user_picture'], $user['user_gender']);
                $user['mutual_friends_count'] = $this->get_mutual_friends_count($user['user_id']);
                $new_people[] = $user;
            }
        }
        return $new_people;
    }


    /**
     * get_pro_members
     *
     * @return array
     */
    public function get_pro_members() {
        global $db;
        $pro_members = array();
        $get_pro_members = $db->query("SELECT * FROM users WHERE user_subscribed = '1' ORDER BY RAND() LIMIT 3") or _error(SQL_ERROR_THROWEN);
        if($get_pro_members->num_rows > 0) {
            while($user = $get_pro_members->fetch_assoc()) {
                $user['user_picture'] = $this->get_picture($user['user_picture'], $user['user_gender']);
                $user['mutual_friends_count'] = $this->get_mutual_friends_count($user['user_id']);
                $pro_members[] = $user;
            }
        }
        return $pro_members;
    }


    /**
     * get_users
     *
     * @param string $query
     * @param array $skipped_array
     * @return array
     */
    public function get_users($query, $skipped_array = array()) {
        global $db, $system;
        $users = array();
        if(count($skipped_array) > 0) {
            /* make a skipped list (including the viewer) */
            $skipped_list = implode(',', $skipped_array);
            /* get users */
            $get_users = $db->query(sprintf("SELECT user_id, user_name, user_fullname, user_firstname, user_lastname, user_gender, user_picture FROM users WHERE user_id NOT IN (%s) AND (user_firstname LIKE %s OR user_lastname LIKE %s) ORDER BY user_firstname ASC LIMIT %s", $skipped_list, secure($query, 'search'), secure($query, 'search'), secure($system['min_results'], 'int', false) )) or _error(SQL_ERROR_THROWEN);
        } else {
            /* get users */
            $get_users = $db->query(sprintf("SELECT user_id, user_name, user_fullname, user_firstname, user_lastname, user_gender, user_picture FROM users WHERE user_firstname LIKE %s OR user_lastname LIKE %s ORDER BY user_firstname ASC LIMIT %s", secure($query, 'search'), secure($query, 'search'), secure($system['min_results'], 'int', false) )) or _error(SQL_ERROR_THROWEN);
        }
        if($get_users->num_rows > 0) {
            while($user = $get_users->fetch_assoc()) {
                $user['user_picture'] = $this->get_picture($user['user_picture'], $user['user_gender']);
                $users[] = $user;
            }
        }
        return $users;
    }



    /* ------------------------------- */
    /* Search */
    /* ------------------------------- */

    /**
     * search_quick
     * 
     * @param string $query
     * @return array
     */
    public function search_quick($query) {
        global $db, $system, $user;
        $results = array();
        /* search users */
        $get_users = $db->query(sprintf('SELECT user_id, user_name, user_fullname, user_firstname, user_lastname, user_gender, user_picture, user_subscribed, user_verified FROM users WHERE user_name LIKE %1$s OR user_firstname LIKE %1$s OR user_lastname LIKE %1$s LIMIT %2$s', secure($query, 'search'), secure($system['min_results'], 'int', false) )) or _error(SQL_ERROR_THROWEN);
        if($get_users->num_rows > 0) {
            while($user = $get_users->fetch_assoc()) {

                /* Coniu - Kiểm tra nếu chưa bị banned hoặc block thì mới trả về */
                $is_blocked = $this->blocked($user['user_id']);
                if ($is_blocked) {
                    continue;
                }
                $is_baned = $this->banned($user['user_id']);
                if ($is_baned) {
                    continue;
                }
                /* END - Coniu */

                $user['user_picture'] = $this->get_picture($user['user_picture'], $user['user_gender']);
                /* get the connection between the viewer & the target */
                $user['connection'] = $this->connection($user['user_id']);
                $user['sort'] = $user['user_firstname'];
                $user['type'] = 'user';
                $results[] = $user;
            }
        }
        /* search pages */
        $get_pages = $db->query(sprintf('SELECT * FROM pages WHERE page_name LIKE %1$s OR page_title LIKE %1$s', secure($query, 'search'))) or _error(SQL_ERROR_THROWEN);
        if($get_pages->num_rows > 0) {
            while($page = $get_pages->fetch_assoc()) {
                $page['page_picture'] = $this->get_picture($page['page_picture'], 'page');
                /* check if the viewer liked the page */
                $page['i_like'] = false;
                if($this->_logged_in) {
                    $get_likes = $db->query(sprintf("SELECT * FROM pages_likes WHERE page_id = %s AND user_id = %s", secure($page['page_id'], 'int'), secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                    if($get_likes->num_rows > 0) {
                        $page['i_like'] = true;
                    }
                }
                $page['sort'] = $page['page_title'];
                $page['type'] = 'page';
                // Kiểm tra xem page có phải page trường không
                $strSql = sprintf("SELECT school_id FROM ci_school_configuration WHERE school_id = %s", secure($page['page_id'], 'int'));
                $get_school_id = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
                if($get_school_id->num_rows > 0) {
                    $is_member = false;
                    // Kiểm tra xem user hiện tại đã like page chưa
                    if($page['i_like']) {
                        $is_member = true;
                    } else {
                        // Kiểm tra xem user hiện tại có phải là thành viên của trường không
                        // 1. Kiểm tra xem có phải giáo viên trong trường không
                        $strSql = sprintf("SELECT user_id FROM ci_teacher WHERE school_id = %s AND user_id = %s", secure($page['page_id'], 'int'), secure($this->_data['user_id'], 'int'));
                        $get_teacher_id = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
                        if($get_teacher_id->num_rows > 0) {
                            $is_member = true;
                        } else {
                            // 2. Kiểm tra xem user hiện tại có phải là phụ huynh trong trường không
                            $strSql = sprintf('SELECT UM.user_id FROM ci_user_manage UM INNER JOIN ci_school_child SC ON UM.object_type = %s AND UM.object_id = SC.child_id
                    AND SC.school_id = %s AND SC.status = %s AND UM.user_id = %s', MANAGE_CHILD, secure($page['page_id'], 'int'), secure(STATUS_ACTIVE, 'int'), secure($this->_data['user_id'], 'int'));
                            $get_parent_id = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
                            if($get_parent_id->num_rows > 0) {
                                $is_member = true;
                            }
                        }
                    }
                    if($is_member) {
                        $results[] = $page;
                    }
                } else {
                    $results[] = $page;
                }
            }
        }
        /* search groups */
        $get_groups = $db->query(sprintf('SELECT * FROM groups WHERE group_privacy != "secret" AND (group_name LIKE %1$s OR group_title LIKE %1$s) LIMIT %2$s', secure($query, 'search'), secure($system['min_results'], 'int', false) )) or _error(SQL_ERROR_THROWEN);
        if($get_groups->num_rows > 0) {
            while($group = $get_groups->fetch_assoc()) {
                /* Coniu - Tăng thành viên nhóm */
                $group['group_members'] = increase_members_groups($group['group_name'], $group['group_members']);

                $group['group_picture'] = $this->get_picture($group['group_picture'], 'group');
                /* check if the viewer joined the group */
                $group['i_joined'] = $this->check_group_membership($this->_data['user_id'], $group['group_id']);
                $group['sort'] = $group['group_title'];
                $group['type'] = 'group';

                // Kiểm tra có phải nhóm lớp không
                if($group['class_level_id'] > 0) {
                    $is_member = false;
                    // Nếu là group của lớp thì kiểm tra xem user hiện tại có phải là thành viên của group không.
                    if($group['i_joined']) {
                        // Là thành viên của nhóm
                        $is_member = true;
                    } else {
                        // 1. Kiểm tra xem có phải giáo viên trong trường không
                        // Lấy id của trường
                        $school_id = 0;
                        $strSql = sprintf("SELECT school_id FROM ci_class_level WHERE class_level_id = %s", secure($group['class_level_id'], 'int'));
                        $get_school_id = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
                        if($get_school_id->num_rows > 0) {
                            $school_id = $get_school_id->fetch_assoc()['school_id'];
                        }
                        if($school_id > 0) {
                            $strSql = sprintf("SELECT user_id FROM ci_teacher WHERE school_id = %s AND user_id = %s", secure($school_id, 'int'), secure($this->_data['user_id'], 'int'));
                            $get_teacher_id = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
                            if($get_teacher_id->num_rows > 0) {
                                $is_member = true;
                            } else {
                                // Kiểm  tra xem có phải là phụ huynh của lớp không
                                $strSql = sprintf('SELECT UM.user_id FROM ci_user_manage UM INNER JOIN ci_class_child CC ON UM.object_type = %s
                            AND UM.object_id = CC.child_id AND CC.class_id = %s AND CC.status = %s AND UM.user_id = %s', MANAGE_CHILD, secure($group['group_id'], 'int'), secure(STATUS_ACTIVE, 'int'), secure($this->_data['user_id'], 'int'));
                                $get_users = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
                                if($get_users->num_rows > 0) {
                                    $is_member = true;
                                }
                            }
                        }
                    }
                    if($is_member) {
                        $results[] = $group;
                    }
                } else {
                    $results[] = $group;
                }
            }
        }
        /* search events */
        /*$get_events = $db->query(sprintf('SELECT * FROM events WHERE event_privacy != "secret" AND event_title LIKE %1$s LIMIT %2$s', secure($query, 'search'), secure($system['min_results'], 'int', false) )) or _error(SQL_ERROR_THROWEN);
        if($get_events->num_rows > 0) {
            while($event = $get_events->fetch_assoc()) {
                $event['event_picture'] = $this->get_picture($event['event_cover'], 'event');
                // check if the viewer joined the event
                $event['i_joined'] = $this->check_event_membership($this->_data['user_id'], $event['event_id']);
                $event['sort'] = $event['event_title'];
                $event['type'] = 'event';
                $results[] = $event;
            }
        }*/

        function sort_results($a, $b){
            return strcmp($a["sort"], $b["sort"]);
        }
        usort($results, sort_results);
        return $results;
    }

    /**
     * search
     * 
     * @param string $query
     * @return array
     */
    public function search($query, $offset = 0) {
        global $db, $system;
        $results = array();
        $offset *= $system['max_results'];
        /* search posts */
        $posts = $this->get_posts( array('query' => $query) );
        if(count($posts) > 0) {
            $results['posts'] = $posts;
        }
        /* search users */
        $get_users = $db->query(sprintf('SELECT user_id, user_name, user_fullname, user_firstname, user_lastname, user_gender, user_picture FROM users WHERE user_name LIKE %1$s OR user_firstname LIKE %1$s OR user_lastname LIKE %1$s ORDER BY user_firstname ASC LIMIT %2$s, %3$s', secure($query, 'search'), secure($offset, 'int', false), secure($system['max_results'], 'int', false) )) or _error(SQL_ERROR_THROWEN);
        if($get_users->num_rows > 0) {
            while($user = $get_users->fetch_assoc()) {

                /* Coniu - Kiểm tra nếu chưa bị banned hoặc block thì mới trả về */
                $is_blocked = $this->blocked($user['user_id']);
                if ($is_blocked) {
                    continue;
                }
                $is_baned = $this->banned($user['user_id']);
                if ($is_baned) {
                    continue;
                }
                /* END - Coniu */

                $user['user_picture'] = $this->get_picture($user['user_picture'], $user['user_gender']);
                /* get the connection between the viewer & the target */
                $user['connection'] = $this->connection($user['user_id']);
                $results['users'][] = $user;
            }
        }
        /* search pages */
        $get_pages = $db->query(sprintf('SELECT * FROM pages WHERE page_name LIKE %1$s OR page_title LIKE %1$s ORDER BY page_title ASC', secure($query, 'search'))) or _error(SQL_ERROR_THROWEN);
        if($get_pages->num_rows > 0) {
            while($page = $get_pages->fetch_assoc()) {
                $page['page_picture'] = $this->get_picture($page['page_picture'], 'page');
                /* check if the viewer liked the page */
                $page['i_like'] = false;
                if($this->_logged_in) {
                    $get_likes = $db->query(sprintf("SELECT * FROM pages_likes WHERE page_id = %s AND user_id = %s", secure($page['page_id'], 'int'), secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                    if($get_likes->num_rows > 0) {
                        $page['i_like'] = true;
                    }
                }
                // Kiểm tra xem page có phải page trường không
                $strSql = sprintf("SELECT school_id FROM ci_school_configuration WHERE school_id = %s", secure($page['page_id'], 'int'));
                $get_school_id = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
                if($get_school_id->num_rows > 0) {
                    $is_member = false;
                    // Kiểm tra xem user hiện tại đã like page chưa
                    if($page['i_like']) {
                        $is_member = true;
                    } else {
                        // Kiểm tra xem user hiện tại có phải là thành viên của trường không
                        // 1. Kiểm tra xem có phải giáo viên trong trường không
                        $strSql = sprintf("SELECT user_id FROM ci_teacher WHERE school_id = %s AND user_id = %s", secure($page['page_id'], 'int'), secure($this->_data['user_id'], 'int'));
                        $get_teacher_id = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
                        if($get_teacher_id->num_rows > 0) {
                            $is_member = true;
                        } else {
                            // 2. Kiểm tra xem user hiện tại có phải là phụ huynh trong trường không
                            $strSql = sprintf('SELECT UM.user_id FROM ci_user_manage UM INNER JOIN ci_school_child SC ON UM.object_type = %s AND UM.object_id = SC.child_id
                    AND SC.school_id = %s AND SC.status = %s AND UM.user_id = %s', MANAGE_CHILD, secure($page['page_id'], 'int'), secure(STATUS_ACTIVE, 'int'), secure($this->_data['user_id'], 'int'));
                            $get_parent_id = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
                            if($get_parent_id->num_rows > 0) {
                                $is_member = true;
                            }
                        }
                    }
                    if($is_member) {
                        $results['pages'][] = $page;
                    }
                } else {
                    $results['pages'][] = $page;
                }
            }
        }
        /* search groups */
        $get_groups = $db->query(sprintf('SELECT * FROM groups WHERE group_privacy != "secret" AND (group_name LIKE %1$s OR group_title LIKE %1$s) ORDER BY group_title ASC LIMIT %2$s, %3$s', secure($query, 'search'), secure($offset, 'int', false), secure($system['max_results'], 'int', false) )) or _error(SQL_ERROR_THROWEN);
        if($get_groups->num_rows > 0) {
            while($group = $get_groups->fetch_assoc()) {
                /* Coniu - Tăng thành viên nhóm */
                $group['group_members'] = increase_members_groups($group['group_name'], $group['group_members']);

                $group['group_picture'] = $this->get_picture($group['group_picture'], 'group');
                /* check if the viewer joined the group */
                $group['i_joined'] = $this->check_group_membership($this->_data['user_id'], $group['group_id']);

                // Kiểm tra có phải nhóm lớp không
                if($group['class_level_id'] > 0) {
                    $is_member = false;
                    // Nếu là group của lớp thì kiểm tra xem user hiện tại có phải là thành viên của group không.
                    if($group['i_joined']) {
                        // Là thành viên của nhóm
                        $is_member = true;
                    } else {
                        // 1. Kiểm tra xem có phải giáo viên trong trường không
                        // Lấy id của trường
                        $school_id = 0;
                        $strSql = sprintf("SELECT school_id FROM ci_class_level WHERE class_level_id = %s", secure($group['class_level_id'], 'int'));
                        $get_school_id = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
                        if($get_school_id->num_rows > 0) {
                            $school_id = $get_school_id->fetch_assoc()['school_id'];
                        }
                        if($school_id > 0) {
                            $strSql = sprintf("SELECT user_id FROM ci_teacher WHERE school_id = %s AND user_id = %s", secure($school_id, 'int'), secure($this->_data['user_id'], 'int'));
                            $get_teacher_id = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
                            if($get_teacher_id->num_rows > 0) {
                                $is_member = true;
                            } else {
                                // Kiểm  tra xem có phải là phụ huynh của lớp không
                                $strSql = sprintf('SELECT UM.user_id FROM ci_user_manage UM INNER JOIN ci_class_child CC ON UM.object_type = %s
                            AND UM.object_id = CC.child_id AND CC.class_id = %s AND CC.status = %s AND UM.user_id = %s', MANAGE_CHILD, secure($group['group_id'], 'int'), secure(STATUS_ACTIVE, 'int'), secure($this->_data['user_id'], 'int'));
                                $get_users = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
                                if($get_users->num_rows > 0) {
                                    $is_member = true;
                                }
                            }
                        }
                    }
                    if($is_member) {
                        $results['groups'][] = $group;
                    }
                } else {
                    $results['groups'][] = $group;
                }
            }
        }
        /* search events */
        /*$get_events = $db->query(sprintf('SELECT * FROM events WHERE event_privacy != "secret" AND event_title LIKE %1$s ORDER BY event_title ASC LIMIT %2$s, %3$s', secure($query, 'search'), secure($offset, 'int', false), secure($system['max_results'], 'int', false) )) or _error(SQL_ERROR_THROWEN);
        if($get_events->num_rows > 0) {
            while($event = $get_events->fetch_assoc()) {
                $event['event_picture'] = $this->get_picture($event['event_cover'], 'event');
                //check if the viewer joined the event
                $event['i_joined'] = $this->check_event_membership($this->_data['user_id'], $event['event_id']);
                $results['events'][] = $event;
            }
        }*/
        return $results;
    }


    /**
     * search_users
     *
     * @param string $query
     * @param string $gender
     * @param string $relationship
     * @param string $status
     * @return array
     */
    public function search_users($query, $gender, $relationship, $status, $offset = 0) {
        global $db, $system;
        $results = array();
        $offset *= $system['max_results'];
        /* validate gender */
        if(!in_array($gender, array('any', 'male', 'female', 'other'))) {
            return $results;
        }
        /* validate relationship */
        /*if(!in_array($relationship, array('any','single', 'relationship', 'married', "complicated", 'separated', 'divorced', 'widowed'))) {
            return $results;
        }*/
        /* validate status */
        /*if(!in_array($status, array('any', 'online', 'offline'))) {
            return $results;
        }*/
        /* prepare where statement */
        $where = "";
        /* gender */
        $where .= ($gender != "any")? " AND users.user_gender = '$gender'": "";
        /* relationship */
        //$where .= ($relationship != "any")? " AND users.user_relationship = '$relationship'": "";
        /* status */
        /*if($status == "online") {
            $where .= " AND users_online.last_seen IS NOT NULL";
        } elseif ($status == "offline") {
            $where .= " AND users_online.last_seen IS NULL";
        }*/
        /* get users */
        if($query) {
            $get_users = $db->query(sprintf('SELECT users.*, users_online.last_seen FROM users LEFT JOIN users_online ON users.user_id = users_online.user_id WHERE (users.user_name LIKE %1$s OR users.user_firstname LIKE %1$s OR users.user_lastname LIKE %1$s OR users.user_fullname LIKE %1$s)'.$where.' ORDER BY user_firstname ASC LIMIT %2$s', secure($query, 'search'), secure($system['max_results'], 'int', false) )) or _error(SQL_ERROR_THROWEN);
        } else {
            $get_users = $db->query(sprintf("SELECT users.*, users_online.last_seen FROM users LEFT JOIN users_online ON users.user_id = users_online.user_id WHERE 1 = 1 ".$where." ORDER BY user_firstname ASC LIMIT %s", secure($system['max_results'], 'int', false) )) or _error(SQL_ERROR_THROWEN);
        }
        if($get_users->num_rows > 0) {
            while($user = $get_users->fetch_assoc()) {
                $user['user_picture'] = $this->get_picture($user['user_picture'], $user['user_gender']);
                /* get the connection between the viewer & the target */
                $user['connection'] = $this->connection($user['user_id']);
                $results[] = $user;
            }
        }
        return $results;
    }


    /**
     * get_search_log
     *
     * @return array
     */
    public function get_search_log() {
        global $db, $system;
        $results = array();
        $get_search_log = $db->query(sprintf("SELECT users_searches.log_id, users_searches.node_type, users.user_id, user_name, user_fullname, user_firstname, user_lastname, user_gender, user_picture, user_subscribed, user_verified, pages.*, groups.*, events.* FROM users_searches LEFT JOIN users ON users_searches.node_type = 'user' AND users_searches.node_id = users.user_id LEFT JOIN pages ON users_searches.node_type = 'page' AND users_searches.node_id = pages.page_id LEFT JOIN groups ON users_searches.node_type = 'group' AND users_searches.node_id = groups.group_id LEFT JOIN events ON users_searches.node_type = 'event' AND users_searches.node_id = events.event_id WHERE users_searches.user_id = %s ORDER BY users_searches.log_id DESC LIMIT %s", secure($this->_data['user_id'], 'int'), secure($system['min_results'], 'int', false) )) or _error(SQL_ERROR_THROWEN);
        if($get_search_log->num_rows > 0) {
            while($result = $get_search_log->fetch_assoc()) {
                switch ($result['node_type']) {
                    case 'user':
                        $result['user_picture'] = $this->get_picture($result['user_picture'], $result['user_gender']);
                        /* get the connection between the viewer & the target */
                        $result['connection'] = $this->connection($result['user_id']);
                        break;

                    case 'page':
                        $result['page_picture'] = $this->get_picture($result['page_picture'], 'page');
                        /* check if the viewer liked the page */
                        $result['i_like'] = false;
                        $get_likes = $db->query(sprintf("SELECT * FROM pages_likes WHERE page_id = %s AND user_id = %s", secure($result['page_id'], 'int'), secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                        if($get_likes->num_rows > 0) {
                            $result['i_like'] = true;
                        }
                        break;

                    case 'group':
                        $result['group_picture'] = $this->get_picture($result['group_picture'], 'group');
                        /* check if the viewer joined the group */
                        $result['i_joined'] = $this->check_group_membership($this->_data['user_id'], $result['group_id']);
                        break;

                    case 'event':
                        $result['event_picture'] = $this->get_picture($result['event_cover'], 'event');
                        /* check if the viewer joined the event */
                        $result['i_joined'] = $this->check_event_membership($this->_data['user_id'], $result['event_id']);
                        break;
                }
                $result['type'] = $result['node_type'];
                $results[] = $result;
            }
        }
        return $results;
    }


    /**
     * set_search_log
     *
     * @param integer $node_id
     * @param string $node_type
     * @return void
     */
    public function set_search_log($node_id, $node_type) {
        global $db, $date;
        $db->query(sprintf("INSERT INTO users_searches (user_id, node_id, node_type, time) VALUES (%s, %s, %s, %s)", secure($this->_data['user_id'], 'int'), secure($node_id, 'int'), secure($node_type), secure($date) ));
    }


    /**
     * clear_search_log
     *
     * @return void
     */
    public function clear_search_log() {
        global $db, $system;
        $db->query(sprintf("DELETE FROM users_searches WHERE user_id = %s", secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
    }



    /* ------------------------------- */
    /* User & Connections */
    /* ------------------------------- */

    /**
     * connect
     *
     * @param string $do
     * @param integer $id
     * @param integer $uid
     * @return void
     */
    public function connect($do, $id, $uid = null) {
        global $db;
        switch ($do) {
            case 'block':
                /* check blocking */
                if($this->blocked($id)) {
                    throw new Exception(__("You have already blocked this user before!"));
                }
                /* remove any friendship */
                $this->connect('friend-remove', $id);
                /* delete the target from viewer's followings */
                $this->connect('unfollow', $id);
                /* delete the viewer from target's followings */
                $db->query(sprintf("DELETE FROM followings WHERE user_id = %s AND following_id = %s", secure($id, 'int'), secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                /* block the user */
                $db->query(sprintf("INSERT INTO users_blocks (user_id, blocked_id) VALUES (%s, %s)", secure($this->_data['user_id'], 'int'), secure($id, 'int') )) or _error(SQL_ERROR_THROWEN);
                break;

            case 'unblock':
                /* check blocking */
                if(!$this->blocked($id)) return;
                /* unblock the user */
                $db->query(sprintf("DELETE FROM users_blocks WHERE user_id = %s AND blocked_id = %s", secure($this->_data['user_id'], 'int'), secure($id, 'int') )) or _error(SQL_ERROR_THROWEN);
                break;

            case 'friend-accept':
                /* check if there is a friend request from the target to the viewer */
                $check = $db->query(sprintf("SELECT * FROM friends WHERE user_one_id = %s AND user_two_id = %s AND status = 0", secure($id, 'int'), secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                /* if no -> return */
                if($check->num_rows == 0) return;
                /* add the target as a friend */
                $db->query(sprintf("UPDATE friends SET status = 1 WHERE user_one_id = %s AND user_two_id = %s", secure($id, 'int'), secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                /* post new notification */
                $this->post_notification_for_social( array('to_user_id'=>$id, 'action'=>'friend_accept', 'node_url'=>$this->_data['user_name']) );
                /* follow */
                $this->_follow($id);

                /* CI - Firebase thêm bạn bè và conversations*/
                $args = array(
                    'user_id' => $this->_data['user_id'],
                    'action' => ADD_FRIEND,
                    'user_one_id' => $id,
                    'user_two_id' => $this->_data['user_id']
                );
                insertBackgroundFirebase($args);
                /* END-CI */
                break;

            case 'friend-decline':
                /* check if there is a friend request from the target to the viewer */
                $check = $db->query(sprintf("SELECT * FROM friends WHERE user_one_id = %s AND user_two_id = %s AND status = 0", secure($id, 'int'), secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                /* if no -> return */
                if($check->num_rows == 0) return;
                /* decline this friend request */
                $db->query(sprintf("UPDATE friends SET status = -1 WHERE user_one_id = %s AND user_two_id = %s", secure($id, 'int'), secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                /* unfollow */
                $this->_unfollow($id);
                break;

            case 'friend-add':
                /* check blocking */
                if($this->blocked($id)) {
                    _error(403);
                }
                /* check if there is any relation between the viewer & the target */
                $check = $db->query(sprintf('SELECT * FROM friends WHERE (user_one_id = %1$s AND user_two_id = %2$s) OR (user_one_id = %2$s AND user_two_id = %1$s)', secure($this->_data['user_id'], 'int'),  secure($id, 'int') )) or _error(SQL_ERROR_THROWEN);
                /* if yes -> return */
                if($check->num_rows > 0) return;
                /* add the friend request */
                $db->query(sprintf("INSERT INTO friends (user_one_id, user_two_id, status) VALUES (%s, %s, '0')", secure($this->_data['user_id'], 'int'),  secure($id, 'int') )) or _error(SQL_ERROR_THROWEN);
                /* update requests counter +1 */
                $db->query(sprintf("UPDATE users SET user_live_requests_counter = user_live_requests_counter + 1 WHERE user_id = %s", secure($id, 'int') )) or _error(SQL_ERROR_THROWEN);
                /* post new notification */
                $this->post_notification_for_social( array('to_user_id'=>$id, 'action'=>'friend_add', 'node_url'=>$this->_data['user_name']) );
                /* follow */
                $this->_follow($id);
                break;

            case 'friend-cancel':
                /* check if there is a request from the viewer to the target */
                $check = $db->query(sprintf("SELECT * FROM friends WHERE user_one_id = %s AND user_two_id = %s AND status = 0", secure($this->_data['user_id'], 'int'),  secure($id, 'int') )) or _error(SQL_ERROR_THROWEN);
                /* if no -> return */
                if($check->num_rows == 0) return;
                /* delete the friend request */
                $db->query(sprintf("DELETE FROM friends WHERE user_one_id = %s AND user_two_id = %s", secure($this->_data['user_id'], 'int'), secure($id, 'int') )) or _error(SQL_ERROR_THROWEN);
                /* update requests counter -1 */
                $db->query(sprintf("UPDATE users SET user_live_requests_counter = IF(user_live_requests_counter=0,0,user_live_requests_counter-1), user_live_notifications_counter = IF(user_live_notifications_counter=0,0,user_live_notifications_counter-1) WHERE user_id = %s", secure($id, 'int') )) or _error(SQL_ERROR_THROWEN);
                /* unfollow */
                $this->_unfollow($id);
                break;

            case 'friend-remove':
                /* check if there is any relation between me & him */
                $check = $db->query(sprintf('SELECT * FROM friends WHERE (user_one_id = %1$s AND user_two_id = %2$s AND status = 1) OR (user_one_id = %2$s AND user_two_id = %1$s AND status = 1)', secure($this->_data['user_id'], 'int'),  secure($id, 'int') )) or _error(SQL_ERROR_THROWEN);
                /* if no -> return */
                if($check->num_rows == 0) return;
                /* delete this friend */
                $db->query(sprintf('DELETE FROM friends WHERE (user_one_id = %1$s AND user_two_id = %2$s AND status = 1) OR (user_one_id = %2$s AND user_two_id = %1$s AND status = 1)', secure($this->_data['user_id'], 'int'),  secure($id, 'int') )) or _error(SQL_ERROR_THROWEN);
                break;

            case 'follow':
                $this->_follow($id);
                break;

            case 'unfollow':
                $this->_unfollow($id);
                break;

            case 'page-like':
                /* check if the viewer already liked this page */
                $check = $db->query(sprintf("SELECT * FROM pages_likes WHERE user_id = %s AND page_id = %s", secure($this->_data['user_id'], 'int'),  secure($id, 'int') )) or _error(SQL_ERROR_THROWEN);
                /* if yes -> return */
                if($check->num_rows > 0) return;
                /* if no -> like this page */
                $db->query(sprintf("INSERT INTO pages_likes (user_id, page_id) VALUES (%s, %s)", secure($this->_data['user_id'], 'int'),  secure($id, 'int') )) or _error(SQL_ERROR_THROWEN);
                /* update likes counter +1 */
                $db->query(sprintf("UPDATE pages SET page_likes = page_likes + 1  WHERE page_id = %s", secure($id, 'int') )) or _error(SQL_ERROR_THROWEN);
                break;
            case 'page-unlike':
                /* check if the viewer already liked this page */
                $check = $db->query(sprintf("SELECT * FROM pages_likes WHERE user_id = %s AND page_id = %s", secure($this->_data['user_id'], 'int'),  secure($id, 'int') )) or _error(SQL_ERROR_THROWEN);
                /* if no -> return */
                if($check->num_rows == 0) return;
                /* if yes -> unlike this page */
                $db->query(sprintf("DELETE FROM pages_likes WHERE user_id = %s AND page_id = %s", secure($this->_data['user_id'], 'int'),  secure($id, 'int') )) or _error(SQL_ERROR_THROWEN);
                /* update likes counter -1 */
                $db->query(sprintf("UPDATE pages SET page_likes = IF(page_likes=0,0,page_likes-1) WHERE page_id = %s", secure($id, 'int') )) or _error(SQL_ERROR_THROWEN);
                break;

            case 'page-invite':
                if($uid == null) {
                    _error(400);
                }
                /* check if the viewer liked the page */
                $check_viewer = $db->query(sprintf("SELECT pages.* FROM pages INNER JOIN pages_likes ON pages.page_id = pages_likes.page_id WHERE pages_likes.user_id = %s AND pages_likes.page_id = %s", secure($this->_data['user_id'], 'int'), secure($id, 'int') )) or _error(SQL_ERROR_THROWEN);
                /* if no -> return */
                if($check_viewer->num_rows == 0) return;
                /* check if the target already liked this page */
                $check_target = $db->query(sprintf("SELECT * FROM pages_likes WHERE user_id = %s AND page_id = %s", secure($uid, 'int'),  secure($id, 'int') )) or _error(SQL_ERROR_THROWEN);
                /* if yes -> return */
                if($check_target->num_rows > 0) return;
                /* check if the viewer already invited to the viewer to this page */
                $check_target = $db->query(sprintf("SELECT * FROM pages_invites WHERE page_id = %s AND user_id = %s AND from_user_id = %s", secure($id, 'int'), secure($uid, 'int'), secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                /* if yes -> return */
                if($check_target->num_rows > 0) return;
                /* if no -> invite to this page */
                /* get page */
                $page = $check_viewer->fetch_assoc();
                /* insert invitation */
                $db->query(sprintf("INSERT INTO pages_invites (page_id, user_id, from_user_id) VALUES (%s, %s, %s)", secure($id, 'int'), secure($uid, 'int'), secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                /* send notification (page invitation) to the target user */
                $this->post_notification_for_social( array('to_user_id'=>$uid, 'action'=>'page_invitation', 'node_type'=>$page['page_title'], 'node_url'=>$page['page_name']) );
                break;

            case 'page-admin-addation':
                if($uid == null) {
                    _error(400);
                }
                /* check if the target already a page member */
                $check = $db->query(sprintf("SELECT * FROM pages_likes WHERE user_id = %s AND page_id = %s", secure($uid, 'int'),  secure($id, 'int') )) or _error(SQL_ERROR_THROWEN);
                /* if no -> return */
                if($check->num_rows == 0) return;
                /* check if the target already a page admin */
                $check = $db->query(sprintf("SELECT * FROM pages_admins WHERE user_id = %s AND page_id = %s", secure($uid, 'int'),  secure($id, 'int') )) or _error(SQL_ERROR_THROWEN);
                /* if yes -> return */
                if($check->num_rows > 0) return;
                /* get page */
                $get_page = $db->query(sprintf("SELECT * FROM pages WHERE page_id = %s", secure($id, 'int') )) or _error(SQL_ERROR_THROWEN);
                $page = $get_page->fetch_assoc();
                /* check if the viewer is page admin */
                if(!$this->check_page_adminship($this->_data['user_id'], $page['page_id']) && $this->_data['user_id'] != $page['page_admin']) {
                    _error(400);
                }
                /* add the target as page admin */
                $db->query(sprintf("INSERT INTO pages_admins (user_id, page_id) VALUES (%s, %s)", secure($uid, 'int'),  secure($id, 'int') )) or _error(SQL_ERROR_THROWEN);
                /* send notification (page admin addation) to the target user */
                $this->post_notification_for_social( array('to_user_id'=>$uid, 'action'=>'page_admin_addation', 'node_type'=>$page['page_title'], 'node_url'=>$page['page_name']) );
                break;

            case 'page-admin-remove':
                if($uid == null) {
                    _error(400);
                }
                /* check if the target already a page admin */
                $check = $db->query(sprintf("SELECT * FROM pages_admins WHERE user_id = %s AND page_id = %s", secure($uid, 'int'),  secure($id, 'int') )) or _error(SQL_ERROR_THROWEN);
                /* if no -> return */
                if($check->num_rows == 0) return;
                /* get page */
                $get_page = $db->query(sprintf("SELECT * FROM pages WHERE page_id = %s", secure($id, 'int') )) or _error(SQL_ERROR_THROWEN);
                $page = $get_page->fetch_assoc();
                /* check if the viewer is page admin */
                if(!$this->check_page_adminship($this->_data['user_id'], $page['page_id'])) {
                    _error(400);
                }
                /* check if the target is the super page admin */
                if($uid == $page['page_admin']) {
                    throw new Exception(__("You can not remove page super admin"));
                }
                /* remove the target as page admin */
                $db->query(sprintf("DELETE FROM pages_admins WHERE user_id = %s AND page_id = %s", secure($uid, 'int'), secure($id, 'int') )) or _error(SQL_ERROR_THROWEN);
                /* delete notification (page admin addation) to the target user */
                $this->delete_notification($uid, 'page_admin_addation', $page['page_title'], $page['page_name']);
                break;

            case 'page-member-remove':
                if($uid == null) {
                    _error(400);
                }
                /* check if the target already a page member */
                $check = $db->query(sprintf("SELECT * FROM pages_likes WHERE user_id = %s AND page_id = %s", secure($uid, 'int'),  secure($id, 'int') )) or _error(SQL_ERROR_THROWEN);
                /* if no -> return */
                if($check->num_rows == 0) return;
                /* get page */
                $get_page = $db->query(sprintf("SELECT * FROM pages WHERE page_id = %s", secure($id, 'int') )) or _error(SQL_ERROR_THROWEN);
                $page = $get_page->fetch_assoc();
                /* check if the target is the super page admin */
                if($uid == $page['page_admin']) {
                    throw new Exception(__("You can not remove page super admin"));
                }
                /* remove the target as page admin */
                $db->query(sprintf("DELETE FROM pages_admins WHERE user_id = %s AND page_id = %s", secure($uid, 'int'), secure($id, 'int') )) or _error(SQL_ERROR_THROWEN);
                /* remove the target as page member */
                $db->query(sprintf("DELETE FROM pages_likes WHERE user_id = %s AND page_id = %s", secure($uid, 'int'), secure($id, 'int') )) or _error(SQL_ERROR_THROWEN);
                /* update members counter -1 */
                $db->query(sprintf("UPDATE pages SET page_likes = IF(page_likes=0,0,page_likes-1) WHERE page_id = %s", secure($id, 'int') )) or _error(SQL_ERROR_THROWEN);
                break;

            case 'group-join':
                /* check if the viewer already joined (approved||pending) this group */
                $check = $db->query(sprintf("SELECT * FROM groups_members WHERE user_id = %s AND group_id = %s", secure($this->_data['user_id'], 'int'),  secure($id, 'int') )) or _error(SQL_ERROR_THROWEN);
                /* if yes -> return */
                if($check->num_rows > 0) return;
                /* if no -> join this group */
                /* get group */
                $get_group = $db->query(sprintf("SELECT * FROM groups WHERE group_id = %s", secure($id, 'int') )) or _error(SQL_ERROR_THROWEN);
                $group = $get_group->fetch_assoc();
                /* check approved */
                $approved = 0;
                if($group['group_privacy'] == 'public') {
                    /* the group is public */
                    $approved = '1';
                } elseif($this->_data['user_id'] == $group['group_admin']) {
                    /* the group admin join his group */
                    $approved = '1';
                }
                $db->query(sprintf("INSERT INTO groups_members (user_id, group_id, approved) VALUES (%s, %s, %s)", secure($this->_data['user_id'], 'int'),  secure($id, 'int'), secure($approved) )) or _error(SQL_ERROR_THROWEN);
                if($approved) {
                    /* update members counter +1 */
                    $db->query(sprintf("UPDATE groups SET group_members = group_members + 1  WHERE group_id = %s", secure($id, 'int') )) or _error(SQL_ERROR_THROWEN);
                } else {
                    /* send notification (pending request) to group admin  */
                    $this->post_notification_for_social( array('to_user_id'=>$group['group_admin'], 'action'=>'group_join', 'node_type'=>$group['group_title'], 'node_url'=>$group['group_name']) );
                }
                break;

            case 'group-leave':
                /* check if the viewer already joined (approved||pending) this group */
                $check = $db->query(sprintf("SELECT groups_members.approved, groups.* FROM groups_members INNER JOIN groups ON groups_members.group_id = groups.group_id WHERE groups_members.user_id = %s AND groups_members.group_id = %s", secure($this->_data['user_id'], 'int'),  secure($id, 'int') )) or _error(SQL_ERROR_THROWEN);
                /* if no -> return */
                if($check->num_rows == 0) return;
                /* if yes -> leave this group */
                $group = $check->fetch_assoc();
                $db->query(sprintf("DELETE FROM groups_members WHERE user_id = %s AND group_id = %s", secure($this->_data['user_id'], 'int'),  secure($id, 'int') )) or _error(SQL_ERROR_THROWEN);
                /* update members counter -1 */
                if($group['approved']) {
                    $db->query(sprintf("UPDATE groups SET group_members = IF(group_members=0,0,group_members-1) WHERE group_id = %s", secure($id, 'int') )) or _error(SQL_ERROR_THROWEN);
                } else {
                    /* delete notification (pending request) that sent to group admin */
                    $this->delete_notification($group['group_admin'], 'group_join', $group['group_title'], $group['group_name']);
                }
                break;

            case 'group-invite':
                if($uid == null) {
                    _error(400);
                }
                /* check if the viewer is group member (approved) */
                $check_viewer = $db->query(sprintf("SELECT groups.* FROM groups INNER JOIN groups_members ON groups.group_id = groups_members.group_id WHERE groups_members.approved = '1' AND groups_members.user_id = %s AND groups_members.group_id = %s", secure($this->_data['user_id'], 'int'), secure($id, 'int') )) or _error(SQL_ERROR_THROWEN);
                /* if no -> return */
                if($check_viewer->num_rows == 0) return;
                /* check if the target already joined (approved||pending) this group */
                $check_target = $db->query(sprintf("SELECT * FROM groups_members WHERE user_id = %s AND group_id = %s", secure($uid, 'int'),  secure($id, 'int') )) or _error(SQL_ERROR_THROWEN);
                /* if yes -> return */
                if($check_target->num_rows > 0) return;
                /* if no -> join this group */
                /* get group */
                $group = $check_viewer->fetch_assoc();
                /* check approved */
                $approved = 0;
                if($group['group_privacy'] == 'public') {
                    /* the group is public */
                    $approved = 1;
                } elseif($this->_data['user_id'] == $group['group_admin']) {
                    /* the group admin want to add a user */
                    $approved = 1;
                } elseif($uid == $group['group_admin']) {
                    /* the viewer invite group admin to join his group */
                    $approved = 1;
                }
                /* add the target user as group member */
                $db->query(sprintf("INSERT INTO groups_members (user_id, group_id, approved) VALUES (%s, %s, %s)", secure($uid, 'int'),  secure($id, 'int'), secure($approved) )) or _error(SQL_ERROR_THROWEN);
                if($approved) {
                    /* update members counter +1 */
                    $db->query(sprintf("UPDATE groups SET group_members = group_members + 1  WHERE group_id = %s", secure($id, 'int') )) or _error(SQL_ERROR_THROWEN);
                    /* send notification (group addition) to the target user */
                    $this->post_notification_for_social( array('to_user_id'=>$uid, 'action'=>'group_add', 'node_type'=>$group['group_title'], 'node_url'=>$group['group_name']) );
                } else {
                    /* send notification (group addition) to the target user */
                    if($group['group_privacy'] != 'secret') {
                        $this->post_notification_for_social( array('to_user_id'=>$uid, 'action'=>'group_add', 'node_type'=>$group['group_title'], 'node_url'=>$group['group_name']) );
                    }
                    /* send notification (pending request) to group admin [from the target]  */
                    $this->post_notification_for_social( array('to_user_id'=>$group['group_admin'], 'from_user_id'=>$uid, 'action'=>'group_join', 'node_type'=>$group['group_title'], 'node_url'=>$group['group_name']) );
                }
                break;

            case 'group-accept':
                if($uid == null) {
                    _error(400);
                }
                /* check if the target has pending request */
                $check = $db->query(sprintf("SELECT groups.* FROM groups INNER JOIN groups_members ON groups.group_id = groups_members.group_id WHERE groups_members.approved = '0' AND groups_members.user_id = %s AND groups_members.group_id = %s", secure($uid, 'int'), secure($id, 'int') )) or _error(SQL_ERROR_THROWEN);
                /* if no -> return */
                if($check->num_rows == 0) return;
                $group = $check->fetch_assoc();
                /* check if the viewer is group admin */
                if(!$this->check_group_adminship($this->_data['user_id'], $group['group_id'])) {
                    _error(400);
                }
                /* update request */
                $db->query(sprintf("UPDATE groups_members SET approved = '1' WHERE user_id = %s AND group_id = %s", secure($uid, 'int'), secure($id, 'int') )) or _error(SQL_ERROR_THROWEN);
                /* update members counter +1 */
                $db->query(sprintf("UPDATE groups SET group_members = group_members + 1  WHERE group_id = %s", secure($id, 'int') )) or _error(SQL_ERROR_THROWEN);
                /* send notification (group acceptance) to the target user */
                $this->post_notification_for_social( array('to_user_id'=>$uid, 'action'=>'group_accept', 'node_type'=>$group['group_title'], 'node_url'=>$group['group_name']) );
                break;

            case 'group-decline':
                if($uid == null) {
                    _error(400);
                }
                /* check if the target has pending request */
                $check = $db->query(sprintf("SELECT groups.* FROM groups INNER JOIN groups_members ON groups.group_id = groups_members.group_id WHERE groups_members.approved = '0' AND groups_members.user_id = %s AND groups_members.group_id = %s", secure($uid, 'int'), secure($id, 'int') )) or _error(SQL_ERROR_THROWEN);
                /* if no -> return */
                if($check->num_rows == 0) return;
                $group = $check->fetch_assoc();
                /* check if the viewer is group admin */
                if(!$this->check_group_adminship($this->_data['user_id'], $group['group_id'])) {
                    _error(400);
                }
                /* delete request */
                $db->query(sprintf("DELETE FROM groups_members WHERE user_id = %s AND group_id = %s", secure($uid, 'int'), secure($id, 'int') )) or _error(SQL_ERROR_THROWEN);
                break;

            case 'group-admin-addation':
                if($uid == null) {
                    _error(400);
                }
                /* check if the target already a group member */
                $check = $db->query(sprintf("SELECT * FROM groups_members WHERE user_id = %s AND group_id = %s", secure($uid, 'int'),  secure($id, 'int') )) or _error(SQL_ERROR_THROWEN);
                /* if no -> return */
                if($check->num_rows == 0) return;
                /* check if the target already a group admin */
                $check = $db->query(sprintf("SELECT * FROM groups_admins WHERE user_id = %s AND group_id = %s", secure($uid, 'int'),  secure($id, 'int') )) or _error(SQL_ERROR_THROWEN);
                /* if yes -> return */
                if($check->num_rows > 0) return;
                /* get group */
                $get_group = $db->query(sprintf("SELECT * FROM groups WHERE group_id = %s", secure($id, 'int') )) or _error(SQL_ERROR_THROWEN);
                $group = $get_group->fetch_assoc();
                /* check if the viewer is group admin */
                if(!$this->check_group_adminship($this->_data['user_id'], $group['group_id']) && $this->_data['user_id'] != $group['group_admin']) {
                    _error(400);
                }
                /* add the target as group admin */
                $db->query(sprintf("INSERT INTO groups_admins (user_id, group_id) VALUES (%s, %s)", secure($uid, 'int'),  secure($id, 'int') )) or _error(SQL_ERROR_THROWEN);
                /* send notification (group admin addation) to the target user */
                $this->post_notification_for_social( array('to_user_id'=>$uid, 'action'=>'group_admin_addation', 'node_type'=>$group['group_title'], 'node_url'=>$group['group_name']) );
                break;

            case 'group-admin-remove':
                if($uid == null) {
                    _error(400);
                }
                /* check if the target already a group admin */
                $check = $db->query(sprintf("SELECT * FROM groups_admins WHERE user_id = %s AND group_id = %s", secure($uid, 'int'),  secure($id, 'int') )) or _error(SQL_ERROR_THROWEN);
                /* if no -> return */
                if($check->num_rows == 0) return;
                /* get group */
                $get_group = $db->query(sprintf("SELECT * FROM groups WHERE group_id = %s", secure($id, 'int') )) or _error(SQL_ERROR_THROWEN);
                $group = $get_group->fetch_assoc();
                /* check if the viewer is group admin */
                if(!$this->check_group_adminship($this->_data['user_id'], $group['group_id'])) {
                    _error(400);
                }
                /* check if the target is the super group admin */
                if($uid == $group['group_admin']) {
                    throw new Exception(__("You can not remove group super admin"));
                }
                /* remove the target as group admin */
                $db->query(sprintf("DELETE FROM groups_admins WHERE user_id = %s AND group_id = %s", secure($uid, 'int'), secure($id, 'int') )) or _error(SQL_ERROR_THROWEN);
                /* delete notification (group admin addation) to the target user */
                $this->delete_notification($uid, 'group_admin_addation', $group['group_title'], $group['group_name']);
                break;

            case 'group-member-remove':
                if($uid == null) {
                    _error(400);
                }
                /* check if the target already a group member */
                $check = $db->query(sprintf("SELECT * FROM groups_members WHERE user_id = %s AND group_id = %s", secure($uid, 'int'),  secure($id, 'int') )) or _error(SQL_ERROR_THROWEN);
                /* if no -> return */
                if($check->num_rows == 0) return;
                /* get group */
                $get_group = $db->query(sprintf("SELECT * FROM groups WHERE group_id = %s", secure($id, 'int') )) or _error(SQL_ERROR_THROWEN);
                $group = $get_group->fetch_assoc();
                /* check if the target is the super group admin */
                if($uid == $group['group_admin']) {
                    throw new Exception(__("You can not remove group super admin"));
                }
                /* remove the target as group admin */
                $db->query(sprintf("DELETE FROM groups_admins WHERE user_id = %s AND group_id = %s", secure($uid, 'int'), secure($id, 'int') )) or _error(SQL_ERROR_THROWEN);
                /* remove the target as group member */
                $db->query(sprintf("DELETE FROM groups_members WHERE user_id = %s AND group_id = %s", secure($uid, 'int'), secure($id, 'int') )) or _error(SQL_ERROR_THROWEN);
                /* update members counter -1 */
                $db->query(sprintf("UPDATE groups SET group_members = IF(group_members=0,0,group_members-1) WHERE group_id = %s", secure($id, 'int') )) or _error(SQL_ERROR_THROWEN);
                break;
        }
    }

    /*
     * CI - Giới hạn kết bạn khi friends > 500 || request > 50
     *
     * */
    public function check_friend_add($user_id) {
        global $db;

        $get_request = $db->query(sprintf("SELECT COUNT(id) AS request_count FROM friends
                    WHERE user_one_id = %s AND status = 0", secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        if($get_request->fetch_assoc()['request_count'] > 50)  return false;

        $get_friends = $db->query(sprintf('SELECT COUNT(id) AS friends_count FROM friends
                    WHERE (user_one_id = %1$s OR user_two_id = %1$s) AND status = 1', secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        if($get_friends->fetch_assoc()['friends_count'] > 500)  return false;

        return true;
    }

    /**
     * connection
     *
     * @param integer $user_id
     * @param boolean $friendship
     * @return string
     */
    public function connection($user_id, $friendship = true) {
        /* check which type of connection (friendship|follow) connections to get */
        if($friendship) {
            /* check if there is a logged user */
            if(!$this->_logged_in) {
                /* no logged user */
                return "add";
            }
            /* check if the viewer is the target */
            if($user_id == $this->_data['user_id']) {
                return "me";
            }
            /* check if the viewer & the target are friends */
            if(in_array($user_id, $this->_data['friends_ids'])) {
                return "remove";
            }
            /* check if the target sent a request to the viewer */
            if(in_array($user_id, $this->_data['friend_requests_ids'])) {
                return "request";
            }
            /* check if the viewer sent a request to the target */
            if(in_array($user_id, $this->_data['friend_requests_sent_ids'])) {
                return "cancel";
            }
            /* there is no relation between the viewer & the target */
            return "add";
        } else {
            /* check if there is a logged user */
            if(!$this->_logged_in) {
                /* no logged user */
                return "follow";
            }
            /* check if the viewer is the target */
            if($user_id == $this->_data['user_id']) {
                return "me";
            }
            if(in_array($user_id, $this->_data['followings_ids'])) {
                /* the viewer follow the target */
                return "unfollow";
            } else {
                /* the viewer not follow the target */
                return "follow";
            }
        }
    }


    /**
     * banned
     *
     * @param integer $user_id
     * @return boolean
     */
    public function banned($user_id) {
        global $db;
        $check = $db->query(sprintf("SELECT %s FROM users WHERE user_banned = '1' AND user_id = %s", PARAM_PROFILE, secure($user_id, 'int') )) or _error(SQL_ERROR_THROWEN);
        if($check->num_rows > 0) {
            return true;
        }
        return false;
    }


    /**
     * blocked
     *
     * @param integer $user_id
     * @return boolean
     */
    public function blocked($user_id) {
        global $db;
        /* check if there is any blocking between the viewer & the target */
        if($this->_logged_in) {
            $check = $db->query(sprintf('SELECT * FROM users_blocks WHERE (user_id = %1$s AND blocked_id = %2$s) OR (user_id = %2$s AND blocked_id = %1$s)', secure($this->_data['user_id'], 'int'),  secure($user_id, 'int') )) or _error(SQL_ERROR_THROWEN);
            if($check->num_rows > 0) {
                return true;
            }
        }
        return false;
    }


    /**
     * delete_user
     *
     * @param integer $user_id
     * @return void
     */
    public function delete_user($user_id) {
        global $db;
        /* (check&get) user */
        $get_user = $db->query(sprintf("SELECT user_group FROM users WHERE user_id = %s", secure($user_id, 'int') )) or _error(SQL_ERROR_THROWEN);
        if($get_user->num_rows == 0) {
            _error(403);
        }
        $user = $get_user->fetch_assoc();
        // delete user
        $can_delete = false;
        /* target is (admin|moderator) */
        if($user['user_group'] < 3) {
            throw new Exception(__("You can not delete admin/morderator accounts"));
        }
        /* viewer is (admin|moderator) */
        if($this->_data['user_group'] < 3) {
            $can_delete = true;
        }
        /* viewer is the target */
        if($this->_data['user_id'] == $user_id) {
            $can_delete = true;
        }
        /* delete the user */
        if($can_delete) {
            /* delete the user */
            $db->query(sprintf("DELETE FROM users WHERE user_id = %s", secure($user_id, 'int') )) or _error(SQL_ERROR_THROWEN);

            /* CI - Firebase */
            $args = array(
                'user_id' => $user_id,
                'action' => DELETE_USER
            );
            insertBackgroundFirebase($args);
            /* END - Firebase */

            /* CI - delete trong bảng followings - Taila */
            $db->query(sprintf("DELETE FROM followings WHERE user_id = %s OR following_id = %s", secure($user_id, 'int'), secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);

            /* CI - delete trong bảng friends - Taila */
            $db->query(sprintf("DELETE FROM friends WHERE user_one_id = %s OR user_two_id = %s", secure($user_id, 'int'), secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        }
    }


    /**
     * _follow
     *
     * @param integer $user_id
     * @return void
     */
    private function _follow($user_id) {
        global $db;
        /* check blocking */
        if($this->blocked($user_id)) {
            _error(403);
        }
        /* check if the viewer already follow the target */
        $check = $db->query(sprintf("SELECT * FROM followings WHERE user_id = %s AND following_id = %s", secure($this->_data['user_id'], 'int'),  secure($user_id, 'int') )) or _error(SQL_ERROR_THROWEN);
        /* if yes -> return */
        if($check->num_rows > 0) return;
        /* add as following */
        $db->query(sprintf("INSERT INTO followings (user_id, following_id) VALUES (%s, %s)", secure($this->_data['user_id'], 'int'),  secure($user_id, 'int') )) or _error(SQL_ERROR_THROWEN);

        /* CI - bỏ thông báo khi theo dõi */
        /* post new notification */
        // $this->post_notification($user_id, 'follow');
    }


    /**
     * _unfollow
     *
     * @param integer $user_id
     * @return void
     */
    private function _unfollow($user_id) {
        global $db;
        /* check if the viewer already follow the target */
        $check = $db->query(sprintf("SELECT * FROM followings WHERE user_id = %s AND following_id = %s", secure($this->_data['user_id'], 'int'),  secure($user_id, 'int') )) or _error(SQL_ERROR_THROWEN);
        /* if no -> return */
        if($check->num_rows == 0) return;
        /* delete from viewer's followings */
        $db->query(sprintf("DELETE FROM followings WHERE user_id = %s AND following_id = %s", secure($this->_data['user_id'], 'int'), secure($user_id, 'int') )) or _error(SQL_ERROR_THROWEN);
        /* delete notification */
        $this->delete_notification($user_id, 'follow');
    }



    /* ------------------------------- */
    /* Live */
    /* ------------------------------- */

    /**
     * live_counters_reset
     *
     * @param string $counter
     * @return void
     */
    public function live_counters_reset($counter) {
        global $db;
        if($counter == "friend_requests") {
            $db->query(sprintf("UPDATE users SET user_live_requests_counter = 0 WHERE user_id = %s", secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
        } elseif($counter == "messages") {
            $db->query(sprintf("UPDATE users SET user_live_messages_counter = 0 WHERE user_id = %s", secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
        } elseif($counter == "notifications") {
            $db->query(sprintf("UPDATE users SET user_live_notifications_counter = 0 WHERE user_id = %s", secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
            $db->query(sprintf("UPDATE notifications SET seen = '1' WHERE to_user_id = %s", secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
        }
    }



    /* ------------------------------- */
    /* Notifications */
    /* ------------------------------- */

    /**
     * get_notifications
     * 
     * @param integer $offset
     * @param integer $last_notification_id
     * @return array
     */
    public function get_notifications($offset = 0, $last_notification_id = null) {
        global $db, $system;
        $offset *= $system['max_results'];
        $notifications = array();

        if($last_notification_id !== null) {
            $get_notifications = $db->query(sprintf("SELECT notifications.*, users.user_id, users.user_name, users.user_fullname, users.user_firstname, users.user_lastname, users.user_gender, users.user_picture FROM notifications LEFT JOIN users ON notifications.from_user_id = users.user_id WHERE (notifications.to_user_id = %s OR notifications.to_user_id = '0') AND notifications.notification_id > %s ORDER BY notifications.notification_id DESC", secure($this->_data['user_id'], 'int'), secure($last_notification_id, 'int') )) or _error(SQL_ERROR_THROWEN);
        } else {
            $get_notifications = $db->query(sprintf("SELECT notifications.*, users.user_id, users.user_name, users.user_fullname, users.user_firstname, users.user_lastname, users.user_gender, users.user_picture FROM notifications LEFT JOIN users ON notifications.from_user_id = users.user_id WHERE notifications.to_user_id = %s OR notifications.to_user_id = '0' ORDER BY notifications.notification_id DESC LIMIT %s, %s", secure($this->_data['user_id'], 'int'), secure($offset, 'int', false), secure($system['max_results'], 'int', false) )) or _error(SQL_ERROR_THROWEN);
        }

        if($get_notifications->num_rows > 0) {
            while($notification = $get_notifications->fetch_assoc()) {
                $notification['user_picture'] = $this->get_picture($notification['user_picture'], $notification['user_gender']);
                $notification['user_fullname'] = convertText4Web($notification['user_fullname']);
                $notification['notify_id'] = ($notification['notify_id'])? "?notify_id=".$notification['notify_id'] : "";
                $notification['extra1'] = convertText4Web($notification['extra1']);
                $notification['extra2'] = convertText4Web($notification['extra2']);
                $notification['extra3'] = convertText4Web($notification['extra3']);

                /* parse notification */
                switch ($notification['action']) {
                    case 'friend_add':
                        $notification['icon'] = "fa-user-plus";
                        $notification['url'] = $system['system_url'].'/'.$notification['user_name'];
                        $notification['message'] = __("send you a friend request");
                        break;

                    case 'friend_accept':
                        $notification['icon'] = "fa-user-plus";
                        $notification['url'] = $system['system_url'].'/'.$notification['user_name'];
                        $notification['message'] = __("accepted your friend request");
                        break;

                    case 'follow':
                        $notification['icon'] = "fa-rss";
                        $notification['url'] = $system['system_url'].'/'.$notification['user_name'];
                        $notification['message'] = __("now following you");
                        break;

                    case 'like':
                        $notification['icon'] = "fa-thumbs-up";
                        if($notification['node_type'] == "post") {
                            $notification['url'] = $system['system_url'].'/posts/'.$notification['node_url'];
                            $notification['message'] = __("likes your post");

                        } elseif ($notification['node_type'] == "photo") {
                            $notification['url'] = $system['system_url'].'/photos/'.$notification['node_url'];
                            $notification['message'] = __("likes your photo");

                        } elseif ($notification['node_type'] == "post_comment") {
                            $notification['url'] = $system['system_url'].'/posts/'.$notification['node_url'].$notification['notify_id'];
                            $notification['message'] = __("likes your comment");

                        } elseif ($notification['node_type'] == "photo_comment") {
                            $notification['url'] = $system['system_url'].'/photos/'.$notification['node_url'].$notification['notify_id'];
                            $notification['message'] = __("likes your comment");

                        } elseif ($notification['node_type'] == "post_reply") {
                            $notification['url'] = $system['system_url'].'/posts/'.$notification['node_url'].$notification['notify_id'];
                            $notification['message'] = __("likes your reply");

                        } elseif ($notification['node_type'] == "photo_reply") {
                            $notification['url'] = $system['system_url'].'/photos/'.$notification['node_url'].$notification['notify_id'];
                            $notification['message'] = __("likes your reply");
                        }
                        break;

                    case 'comment':
                        $notification['icon'] = "fa-comment";
                        if($notification['node_type'] == "post") {
                            $notification['url'] = $system['system_url'].'/posts/'.$notification['node_url'].$notification['notify_id'];
                            $notification['message'] = __("commented on your post");

                        } elseif ($notification['node_type'] == "photo") {
                            $notification['url'] = $system['system_url'].'/photos/'.$notification['node_url'].$notification['notify_id'];
                            $notification['message'] = __("commented on your photo");
                        }
                        break;

                    case 'reply':
                        $notification['icon'] = "fa-comment";
                        if($notification['node_type'] == "post") {
                            $notification['url'] = $system['system_url'].'/posts/'.$notification['node_url'].$notification['notify_id'];

                        } elseif ($notification['node_type'] == "photo") {
                            $notification['url'] = $system['system_url'].'/photos/'.$notification['node_url'].$notification['notify_id'];
                        }
                        $notification['message'] = __("replied to your comment");
                        break;

                    case 'share':
                        $notification['icon'] = "fa-share";
                        $notification['url'] = $system['system_url'].'/posts/'.$notification['node_url'];
                        $notification['message'] = __("shared your post");
                        break;

                    case 'vote':
                        $notification['icon'] = "fa-check-circle";
                        $notification['url'] = $system['system_url'].'/posts/'.$notification['node_url'];
                        $notification['message'] = __("voted on your poll");
                        break;

                    case 'mention':
                        $notification['icon'] = "fa-comment";
                        switch ($notification['node_type']) {
                            case 'post':
                                $notification['url'] = $system['system_url'].'/posts/'.$notification['node_url'];
                                $notification['message'] = __("mentioned you in a post");
                                break;

                            case 'comment_post':
                                $notification['url'] = $system['system_url'].'/posts/'.$notification['node_url'].$notification['notify_id'];
                                $notification['message'] = __("mentioned you in a comment");
                                break;

                            case 'comment_photo':
                                $notification['url'] = $system['system_url'].'/photos/'.$notification['node_url'].$notification['notify_id'];
                                $notification['message'] = __("mentioned you in a comment");
                                break;

                            case 'reply_post':
                                $notification['url'] = $system['system_url'].'/posts/'.$notification['node_url'].$notification['notify_id'];
                                $notification['message'] = __("mentioned you in a reply");
                                break;

                            case 'reply_photo':
                                $notification['url'] = $system['system_url'].'/photos/'.$notification['node_url'].$notification['notify_id'];
                                $notification['message'] = __("mentioned you in a reply");
                                break;
                        }
                        break;

                    case 'profile_visit':
                        $notification['icon'] = "fa-eye";
                        $notification['url'] = $system['system_url'].'/'.$notification['user_name'];
                        $notification['message'] = __("visited your profile");
                        break;

                    case 'wall':
                        $notification['icon'] = "fa-comment";
                        $notification['url'] = $system['system_url'].'/posts/'.$notification['node_url'];
                        $notification['message'] = __("posted on your wall");
                        break;

                    case 'page_invitation':
                        $notification['icon'] = "fa-flag";
                        $notification['url'] = $system['system_url'].'/pages/'.$notification['node_url'];
                        $notification['message'] = __("invite you to like a page")." '".html_entity_decode($notification['node_type'], ENT_QUOTES)."'";
                        break;

                    case 'page_admin_addation':
                        $notification['icon'] = "fa-users";
                        $notification['url'] = $system['system_url'].'/pages/'.$notification['node_url'];
                        $notification['message'] = __("invite you to like a page")." '".html_entity_decode($notification['node_type'], ENT_QUOTES)."'";
                        if ($system['language']['code'] == "vi_VN") {
                            $notification['message'] = __("added you as admin to") . " " . __("page") . " '".html_entity_decode($notification['node_type'], ENT_QUOTES)."' ";
                        } else {
                            $notification['message'] = __("added you as admin to")." '".html_entity_decode($notification['node_type'], ENT_QUOTES)."' ".__("page");
                        }
                        break;

                    case 'group_join':
                        $notification['icon'] = "fa-users";
                        $notification['url'] = $system['system_url'].'/groups/'.$notification['node_url'].'/settings/requests';
                        $notification['message'] = __("asked to join your group")." '".html_entity_decode($notification['node_type'], ENT_QUOTES)."'";
                        break;

                    case 'group_add':
                        $notification['icon'] = "fa-users";
                        $notification['url'] = $system['system_url'].'/groups/'.$notification['node_url'];
                        if ($system['language']['code'] == "vi_VN") {
                            $notification['message'] = __("added you to") . " " . __("group") . " '".html_entity_decode($notification['node_type'], ENT_QUOTES)."' ";
                        } else {
                            $notification['message'] = __("added you to")." '".html_entity_decode($notification['node_type'], ENT_QUOTES)."' ".__("group");
                        }
                        break;

                    case 'group_accept':
                        $notification['icon'] = "fa-users";
                        $notification['url'] = $system['system_url'].'/groups/'.$notification['node_url'];
                        if ($system['language']['code'] == "vi_VN") {
                            $notification['message'] = __("accepted your request to join") . " " . __("group") . " '".html_entity_decode($notification['node_type'], ENT_QUOTES)."' ";
                        } else {
                            $notification['message'] = __("accepted your request to join")." '".html_entity_decode($notification['node_type'], ENT_QUOTES)."' ".__("group");
                        }
                        break;

                    case 'group_admin_addation':
                        $notification['icon'] = "fa-users";
                        $notification['url'] = $system['system_url'].'/groups/'.$notification['node_url'];
                        if ($system['language']['code'] == "vi_VN") {
                            $notification['message'] = __("added you as admin to") . " " . __("group") . " '".html_entity_decode($notification['node_type'], ENT_QUOTES)."' ";
                        } else {
                            $notification['message'] = __("added you as admin to")." '".html_entity_decode($notification['node_type'], ENT_QUOTES)."' ".__("group");
                        }
                        break;

                    case NOTIFICATION_POST_IN_PAGE:
                        //$notification['ci'] = "1";
                        $notification['user_fullname'] = $notification['extra1'];
                        $notification['user_picture'] = $notification['extra2'];
                        $notification['icon'] = "fa-flag";
                        $notification['url'] = $system['system_url'].'/posts/'.$notification['node_url'];
                        $notification['message'] = $notification['extra1'] . ' ' .__("has a new post");
                        break;

                    case NOTIFICATION_POST_IN_GROUP:
                        //$notification['ci'] = "1";
                        $notification['icon'] = "fa-users";
                        $notification['url'] = $system['system_url']. '/posts/'.$notification['node_url'];
                        $notification['message'] = $notification['extra1'] . ' ' . __("posted in") . ' ' . $notification['extra2'];
                        break;

                    case NOTIFICATION_CONIU:
                        $notification['ci'] = "1";
                        $notification['user_fullname'] = 'Coniu';
                        $notification['user_picture'] = $system['system_url']. '/content/themes/inet/images/favicon.png';
                        $notification['icon'] = "fa-bell";
                        $notification['url'] = $notification['node_url'];
                        $notification['message'] = $notification['extra1'];
                        break;

                    //ConIu - BEGIN - Xử lý các trường hợp notification của nghiệp vụ quản lý
                    case NOTIFICATION_ASSIGN_CLASS:
                    case NOTIFICATION_NEW_CLASS:
                    case NOTIFICATION_UPDATE_CLASS:
                        $notification['ci'] = "1";
                        $notification['icon'] = "fa-graduation-cap";
                        $notification['url'] = $system['system_url'] . '/' . $notification['user_name'];
                        $notification['message'] = getNotificationMessage($notification);
                        break;
                    case NOTIFICATION_NEW_CLASSLEVEL:
                    case NOTIFICATION_UPDATE_CLASSLEVEL:
                        $notification['ci'] = "1";
                        $notification['icon'] = "fa-tree";
                        $notification['url'] = $system['system_url'] . '/' . $notification['user_name'];
                        $notification['message'] = getNotificationMessage($notification);
                        break;
                    case NOTIFICATION_NEW_CHILD:
                        $notification['ci'] = "1";
                        $notification['icon'] = "fa-child";
                        $notification['url'] = $system['system_url'] . '/' . $notification['user_name'];
                        $notification['message'] = getNotificationMessage($notification);
                        break;
                    case NOTIFICATION_UPDATE_CHILD_TEACHER:
                    case NOTIFICATION_NEW_CHILD_TEACHER:
                        $notification['ci'] = "1";
                        $notification['icon'] = "fa-child";
                        $notification['url'] = $system['system_url'] . '/' . $notification['user_name'];
                        $notification['message'] = getNotificationMessage($notification);
                        break;
                    case NOTIFICATION_CONFIRM_CHILD_TEACHER:
                    case NOTIFICATION_UNCONFIRM_CHILD_TEACHER:
                        $notification['ci'] = "1";
                        $notification['icon'] = "fa-child";
                        $notification['url'] = $system['system_url'] . '/' . $notification['user_name'];
                        $notification['message'] = getNotificationMessage($notification);
                        break;
                    case NOTIFICATION_NEW_TEACHER:
                        $notification['ci'] = "1";
                        $notification['icon'] = "fa-graduation-cap";
                        $notification['url'] = $system['system_url'] . '/' . $notification['user_name'];
                        $notification['message'] = getNotificationMessage($notification);
                        break;
                    case NOTIFICATION_REGISTER_EVENT_CHILD:
                    case NOTIFICATION_CANCEL_REGISTRATION_EVENT_CHILD:
                    case NOTIFICATION_REGISTER_EVENT_PARENT:
                    case NOTIFICATION_CANCEL_REGISTRATION_EVENT_PARENT:
                    case NOTIFICATION_NEW_EVENT_SCHOOL:
                    case NOTIFICATION_NEW_EVENT_CLASS_LEVEL:
                    case NOTIFICATION_NEW_EVENT_CLASS:
                    case NOTIFICATION_UPDATE_EVENT_SCHOOL:
                    case NOTIFICATION_UPDATE_EVENT_CLASS_LEVEL:
                    case NOTIFICATION_UPDATE_EVENT_CLASS:
                    case NOTIFICATION_CANCEL_EVENT_SCHOOL:
                    case NOTIFICATION_CANCEL_EVENT_CLASS_LEVEL:
                    case NOTIFICATION_CANCEL_EVENT_CLASS:
                        $notification['ci'] = "1";
                        $notification['icon'] = "fa-bell";
                        $notification['url'] = $system['system_url'] . '/' . $notification['user_name'];
                        $notification['message'] = getNotificationMessage($notification);
                        break;
                    case NOTIFICATION_NEW_MEDICINE:
                    case NOTIFICATION_UPDATE_MEDICINE:
                    case NOTIFICATION_USE_MEDICINE:
                    case NOTIFICATION_CONFIRM_MEDICINE:
                    case NOTIFICATION_CANCEL_MEDICINE:
                        $notification['ci'] = "1";
                        $notification['icon'] = "fa-medkit";
                        $notification['url'] = $system['system_url'] . '/' . $notification['user_name'];
                        $notification['message'] = getNotificationMessage($notification);
                        break;
                    case NOTIFICATION_NEW_TUITION:
                    case NOTIFICATION_UPDATE_TUITION:
                    case NOTIFICATION_CONFIRM_TUITION:
                    case NOTIFICATION_CONFIRM_TUITION_SCHOOL:
                    case NOTIFICATION_UNCONFIRM_TUITION:
                        $notification['ci'] = "1";
                        $notification['icon'] = "fas fa-money-bill-alt";
                        $notification['url'] = $system['system_url'] . '/' . $notification['user_name'];
                        $notification['message'] = getNotificationMessage($notification);
                        break;
                    case NOTIFICATION_ATTENDANCE_RESIGN:
                        $notification['ci'] = "1";
                        $notification['icon'] = "fa-tasks";
                        $notification['url'] = $system['system_url'] . '/' . $notification['user_name'];
                        $notification['message'] = getNotificationMessage($notification);
                        break;
                    case NOTIFICATION_ATTENDANCE_CONFIRM:
                        $notification['ci'] = "1";
                        $notification['icon'] = "fa-tasks";
                        $notification['url'] = $system['system_url'] . '/' . $notification['user_name'];
                        $notification['message'] = getNotificationMessage($notification);
                        if ($system['is_mobile']) {
                            include_once(DAO_PATH.'dao_attendance.php');
                            $attendanceDao = new AttendanceDAO();
                            $attendanceDetail = $attendanceDao->getAttendanceDetailById($notification['node_url']);
                            if (!is_null($attendanceDetail))
                                $notification['child_id'] = $attendanceDetail['child_id'];
                        }
                        break;

                    case NOTIFICATION_NEW_REPORT:
                    case NOTIFICATION_NEW_REPORT_TEMPLATE:
                    case NOTIFICATION_UPDATE_REPORT:
                    case NOTIFICATION_NEW_POINT:
                        $notification['ci'] = "1";
                        $notification['icon'] = "fa-book";
                        $notification['url'] = $system['system_url'] . '/' . $notification['user_name'];
                        $notification['message'] = getNotificationMessage($notification);
                        break;


                    case NOTIFICATION_NEW_FEEDBACK:
                        include_once(DAO_PATH.'dao_feedback.php');
                        $feedbackDao = new FeedbackDAO();
                        $feedback = $feedbackDao->getFeedback($notification['node_url']);
                        if (!is_null($feedback)) {
                            if ($feedback['is_incognito']) {
                                $notification['user_fullname'] = 'Ẩn danh';
                                $notification['user_picture'] = $this->get_picture('', 'male');
                            }

                            $notification['ci'] = "1";
                            $notification['icon'] = "fa-envelope";
                            $notification['url'] = $system['system_url'] . '/' . $notification['user_name'];
                            $notification['message'] = getNotificationMessage($notification);
                        }
                        break;

                    case NOTIFICATION_CONFIRM_FEEDBACK:
                        $notification['ci'] = "1";
                        $notification['icon'] = "fa-envelope";
                        $notification['url'] = $system['system_url'] . '/' . $notification['user_name'];
                        $notification['message'] = getNotificationMessage($notification);
                        break;

                    case NOTIFICATION_SERVICE_REGISTER_UNCOUNTBASED:
                    case NOTIFICATION_SERVICE_REGISTER_COUNTBASED:
                    case NOTIFICATION_SERVICE_CANCEL_UNCOUNTBASED:
                    case NOTIFICATION_SERVICE_CANCEL_COUNTBASED:
                    case NOTIFICATION_SERVICE_REGISTER_COUNTBASED_MOBILE:
                        $notification['ci'] = "1";
                        $notification['icon'] = "fa-life-ring";
                        $notification['url'] = $system['system_url'] . '/' . $notification['user_name'];
                        $notification['message'] = getNotificationMessage($notification);
                        break;

                    case NOTIFICATION_UPDATE_PICKER:
                    case NOTIFICATION_CONFIRM_PICKER:
                        $notification['ci'] = "1";
                        $notification['icon'] = "fa-info-circle";
                        $notification['url'] = $system['system_url'] . '/' . $notification['user_name'];
                        $notification['message'] = getNotificationMessage($notification);
                        break;

                    case NOTIFICATION_NEW_SCHEDULE:
                    case NOTIFICATION_UPDATE_SCHEDULE:
                        $notification['ci'] = "1";
                        $notification['icon'] = "fa-calendar-alt";
                        $notification['url'] = $system['system_url'] . '/' . $notification['user_name'];
                        $notification['message'] = getNotificationMessage($notification);
                        break;
                    case NOTIFICATION_NEW_MENU:
                    case NOTIFICATION_UPDATE_MENU:
                        $notification['ci'] = "1";
                        $notification['icon'] = "fa-calendar-alt";
                        $notification['url'] = $system['system_url'] . '/' . $notification['user_name'];
                        $notification['message'] = getNotificationMessage($notification);
                        break;
//                    sign_in
                    case NOTIFICATION_UPDATE_ROLE:
                        $notification['ci'] = "1";
                        $notification['icon'] = "fa-unlock-alt";
                        $notification['url'] = $system['system_url'] . '/' . $notification['user_name'];
                        $notification['message'] = getNotificationMessage($notification);
                        break;

                    case NOTIFICATION_ADD_LATEPICKUP_CLASS:
                        $notification['ci'] = "1";
                        $notification['icon'] = "fa-unlock-alt";
                        $notification['url'] = $system['system_url'] . '/' . $notification['user_name'];
                        $notification['message'] = getNotificationMessage($notification);
                        break;
                    case NOTIFICATION_REMOVE_LATEPICKUP_CLASS:
                        $notification['ci'] = "1";
                        $notification['icon'] = "fa-unlock-alt";
                        $notification['url'] = $system['system_url'] . '/' . $notification['user_name'];
                        $notification['message'] = getNotificationMessage($notification);
                        break;
                    case NOTIFICATION_REGISTER_LATEPICKUP:
                        $notification['ci'] = "1";
                        $notification['icon'] = "fa-unlock-alt";
                        $notification['url'] = $system['system_url'] . '/' . $notification['user_name'];
                        $notification['message'] = getNotificationMessage($notification);
                        break;
                    case NOTIFICATION_UPDATE_LATEPICKUP_INFO:
                        $notification['ci'] = "1";
                        $notification['icon'] = "fa-unlock-alt";
                        $notification['url'] = $system['system_url'] . '/' . $notification['user_name'];
                        $notification['message'] = getNotificationMessage($notification);
                        break;
                    case NOTIFICATION_ASSIGN_LATEPICKUP_CLASS:
                        $notification['ci'] = "1";
                        $notification['icon'] = "fa-unlock-alt";
                        $notification['url'] = $system['system_url'] . '/' . $notification['user_name'];
                        $notification['message'] = getNotificationMessage($notification);
                        break;
                    case NOTIFICATION_CHILD_PICKEDUP:
                        $notification['ci'] = "1";
                        $notification['icon'] = "fa-unlock-alt";
                        $notification['url'] = $system['system_url'] . '/' . $notification['user_name'];
                        $notification['message'] = getNotificationMessage($notification);
                        break;

                    case NOTIFICATION_FOETUS_DEVELOPMENT:
                        $notification['ci'] = "1";
                        $notification['user_fullname'] = 'Coniu';
                        $notification['user_picture'] = $notification['extra3'];
                        $notification['icon'] = "fa-line-chart";
                        $notification['message'] = getNotificationMessage($notification);

                        include_once(DAO_PATH.'dao_foetus_development.php');
                        $foetusDevelopmentDao = new FoetusDevelopmentDAO();
                        $data = $foetusDevelopmentDao->getFoetusInfoDetail($notification['node_url']);
                        if(!is_null($data)) {
                            if($data['content_type'] == 2) {
                                $notification['is_link'] = true;
                                $notification['url'] = $data['link'];
                            }
                        }
                        break;

                    case NOTIFICATION_CHILD_DEVELOPMENT:
                        $notification['ci'] = "1";
                        $notification['user_fullname'] = 'Coniu';
                        $notification['user_picture'] = $notification['extra3'];
                        $notification['icon'] = "fa-line-chart";
                        $notification['message'] = getNotificationMessage($notification);

                        include_once(DAO_PATH.'dao_child_development.php');
                        $childDevelopmentDao = new ChildDevelopmentDAO();
                        $data = $childDevelopmentDao->getChildDevelopmentDetail($notification['node_url']);
                        if(!is_null($data)) {
                            if($data['content_type'] == 2) {
                                $notification['is_link'] = true;
                                $notification['url'] = $data['link'];
                            }
                        }
                        break;

                    case NOTIFICATION_UPDATE_CHILD_SCHOOL:
                    case NOTIFICATION_NEW_CHILD_EXIST:
                        $notification['ci'] = "1";
                        $notification['icon'] = "fa-child";
                        $notification['url'] = $system['system_url'] . '/' . $notification['user_name'];
                        $notification['message'] = getNotificationMessage($notification);
                        break;
                    case NOTIFICATION_NEW_CHILD_HEALTH:
                    case NOTIFICATION_UPDATE_CHILD_HEALTH:
                        $notification['ci'] = "1";
                        $notification['icon'] = "fa-heart";
                        $notification['url'] = $system['system_url'] . '/' . $notification['user_name'];
                        $notification['message'] = getNotificationMessage($notification);
                        break;
                    case NOTIFICATION_NEW_CHILD_EDIT_BY_TEACHER:
                        $notification['ci'] = "1";
                        $notification['user_fullname'] = 'Coniu';
                        $notification['user_picture'] = $system['system_url']. '/content/themes/inet/images/favicon.png';
                        $notification['icon'] = "fa-child";
                        $notification['url'] = $system['system_url'] . '/' . $notification['user_name'];
                        $notification['message'] = getNotificationMessage($notification);
                        break;

                    case NOTIFICATION_LEAVE_SCHOOL_BY_PARENT:
                        $notification['ci'] = "1";
                        $notification['icon'] = "fa-minus-circle";
                        $notification['url'] = $system['system_url'] . '/' . $notification['user_name'];
                        $notification['message'] = getNotificationMessage($notification);
                        break;

                    case NOTIFICATION_MUST_UPDATE_INFORMATION:
                        $notification['user_fullname'] = 'Coniu';
                        $notification['user_picture'] = $system['system_url']. '/content/themes/inet/images/favicon.png';
                        $notification['icon'] = "fa-user";
                        $notification['url'] = $system['system_url'] . '/settings/profile';
                        $notification['message'] = getNotificationMessage($notification);
                        break;

                    case NOTIFICATION_CONIU_INFO:
                        $notification['user_fullname'] = 'Coniu';
                        $notification['user_picture'] = $notification['extra3'];
                        $notification['icon'] = "fa-info-circle";
                        $notification['message'] = getNotificationMessage($notification);
                        $notification['is_link'] = true;
                        $notification['url'] = $system['system_url'] . '/notification/' . $notification['node_url'];
                        break;

                    case NOTIFICATION_CONIU_CONGRATULATIONS:
                        $notification['user_fullname'] = 'Coniu';
                        $notification['user_picture'] = $notification['extra3'];
                        $notification['icon'] = "fa-gift";
                        $notification['message'] = getNotificationMessage($notification);
                        $notification['is_link'] = true;
                        $notification['url'] = $system['system_url'] . '/notification/' . $notification['node_url'];
                        break;

                    case NOTIFICATION_REMIND_INPUT_CHILD_HEALTH:
                        $notification['user_fullname'] = 'Coniu';
                        $notification['user_picture'] = $system['system_url']. '/content/themes/inet/images/favicon.png';
                        $notification['icon'] = "fa-edit";
                        $notification['message'] = getNotificationMessage($notification);
                        $notification['url'] = $system['system_url'] . '/childinfo/' . $notification['extra1'] . '/health';
                        break;

                    case NOTIFICATION_REMIND_INPUT_FOETUS_INFO:
                        $notification['user_fullname'] = 'Coniu';
                        $notification['user_picture'] = $system['system_url']. '/content/themes/inet/images/favicon.png';
                        $notification['icon'] = "fa-edit";
                        $notification['message'] = getNotificationMessage($notification);
                        $notification['url'] = $system['system_url'] . '/childinfo/' . $notification['extra1'] . '/health';
                        break;

                    case NOTIFICATION_INFORMATION_INTERESTED:
                        $notification['user_fullname'] = 'Coniu';
                        $notification['user_picture'] = $notification['extra3'];
                        $notification['icon'] = "fa-search";
                        $notification['message'] = getNotificationMessage($notification);
                        $notification['is_link'] = true;
                        $notification['url'] = $notification['extra2'];
                        break;
                    case NOTIFICATION_UPDATE_INFO:
                        $notification['user_fullname'] = 'Coniu';
                        $notification['user_picture'] = $notification['extra3'];
                        $notification['icon'] = "fa-info";
                        $notification['message'] = getNotificationMessage($notification);
                        $notification['is_link'] = true;
                        //$notification['url'] = $notification['extra2'];
                        $notification['url'] = $system['system_url'] . '/updateinfo';
                        break;
                    case NOTIFICATION_ADD_DIARY_SCHOOL:
                    case NOTIFICATION_EDIT_CAPTION_DIARY_SCHOOL:
                    case NOTIFICATION_DELETE_PHOTO_DIARY_SCHOOL:
                    case NOTIFICATION_ADD_PHOTO_DIARY_SCHOOL:
                        $notification['ci'] = "1";
                        $notification['icon'] = "fa-image";
                        $notification['url'] = $system['system_url'] . '/' . $notification['user_name'];
                        $notification['message'] = getNotificationMessage($notification);
                        break;
                    case NOTIFICATION_UPDATE_ATTENDANCE:
                    case NOTIFICATION_UPDATE_ATTENDANCE_BACK:
                    case NOTIFICATION_NEW_ATTENDANCE:
                    case NOTIFICATION_NEW_ATTENDANCE_BACK:
                    case NOTIFICATION_REMIND_ATTENDANCE:
                        $notification['ci'] = "1";
                        $notification['icon'] = "fa-tasks";
                        $notification['url'] = $system['system_url'] . '/' . $notification['user_name'];
                        $notification['message'] = getNotificationMessage($notification);
                        break;
                    case NOTIFICATION_REMIND_SCHEDULE:
                        $notification['ci'] = "1";
                        $notification['icon'] = "fa-calendar-alt";
                        $notification['url'] = $system['system_url'] . '/' . $notification['user_name'];
                        $notification['message'] = getNotificationMessage($notification);
                        break;
                    case NOTIFICATION_REMIND_MENU:
                        $notification['ci'] = "1";
                        $notification['icon'] = "fa-utensils";
                        $notification['url'] = $system['system_url'] . '/' . $notification['user_name'];
                        $notification['message'] = getNotificationMessage($notification);
                        break;
                    case NOTIFICATION_REMIND_REPORT:
                        $notification['ci'] = "1";
                        $notification['icon'] = "fa-book";
                        $notification['url'] = $system['system_url'] . '/' . $notification['user_name'];
                        $notification['message'] = getNotificationMessage($notification);
                        break;

                    //ConIu - END - Xử lý các trường hợp notification của nghiệp vụ quản lý
                }
                $notification['full_message'] = $notification['user_fullname'] . " " . $notification['message'];
                $notifications[] = $notification;
            }
        }
        return $notifications;
    }


    /**
     * post_notification
     *
     * @param integer $to_user_id
     * @param string $action
     * @return void
     */
    public function post_notification($to_user_id, $action, $node_type = '', $node_url = '', $extra1 = '', $extra2 = '', $extra3 = '') {
        global $db, $date;
        /* if the viewer is the target */
        if($this->_data['user_id'] == $to_user_id) {
            return;
        }

        /* insert notification */
        $db->query(sprintf("INSERT INTO notifications (to_user_id, from_user_id, action, node_type, node_url, time, extra1, extra2, extra3) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)", secure($to_user_id, 'int'), secure($this->_data['user_id'], 'int'), secure($action), secure($node_type), secure($node_url), secure($date), secure($extra1), secure($extra2), secure($extra3) )) or _error(SQL_ERROR_THROWEN);

        /* update notifications counter +1 */
        $db->query(sprintf("UPDATE users SET user_live_notifications_counter = user_live_notifications_counter + 1 WHERE user_id = %s", secure($to_user_id, 'int') )) or _error(SQL_ERROR_THROWEN);

    }


    /**
     * Gửi thông báo cho danh sách user
     *
     * @param $toUserIds
     * @param $action
     * @param string $node_type
     * @param string $node_url
     * @param string $extra1
     * @param string $extra2
     * @param string $extra3
     */
    public function post_notifications($toUserIds, $action, $node_type = '', $node_url = '', $extra1 = '', $extra2 = '', $extra3 = '') {
        $toUserIds = array_unique($toUserIds); //Bỏ trùng lặp
        foreach ($toUserIds as $userId) {
            $this->post_notification($userId, $action, $node_type, $node_url, $extra1, $extra2, $extra3);
        }
        $this->insertBackgroundNotifications($toUserIds, $action, $node_type, $node_url, $extra1, $extra2, $extra3);
    }

    /**
     * CI - mobile insert thông báo vào bảng ci_background_notifications
     *
     * @param array $args
     * @return void
     */
    public function insertNotice(array $args = array()) {
        global $db;

        $strSql = sprintf("INSERT INTO ci_background_notifications (type, count_notifications, message, notification_id, to_user_id, from_user_id, 
        action, node_type, node_url, time, extra1, extra2, extra3, user_name, user_fullname, user_picture, device_token)
                            VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
            secure($args['type']), secure($args['count_notifications'], 'int'), secure($args['message']), secure($args['notification_id'], 'int'),
            secure($args['to_user_id'], 'int'), secure($args['from_user_id'], 'int'), secure($args['action']), secure($args['node_type']),
            secure($args['node_url']), secure($args['time']), secure($args['extra1']), secure($args['extra2']), secure($args['extra3']), secure($args['user_name']), secure($args['user_fullname']),
            secure($args['user_picture']), secure($args['device_token']));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }


    /**
     * Hàm insert thông báo vào db để chạy ngầm gửi thông báo
     *
     * @param $toUserIds
     * @param $action
     * @param string $node_type
     * @param string $node_url
     * @param string $extra1
     * @param string $extra2
     * @param string $extra3
     */
    public function insertBackgroundNotifications($toUserIds, $action, $node_type = '', $node_url = '', $extra1 = '', $extra2 = '', $extra3 = '', $from_user_id = 0) {
        global $db, $date;

        $fromUserId = $from_user_id;
        if ($from_user_id == 0) {
            $fromUserId = $this->_data['user_id'];
        }
        $toUserIds = array_diff($toUserIds, array($fromUserId));

        $strCon = implode(',', $toUserIds);
        /* get device tokens */
        $getDeviceTokens = $db->query(sprintf("SELECT device_token, user_id FROM users_sessions          
              WHERE user_id IN (%s) AND device_token IS NOT NULL AND device_token != '' ", $strCon )) or _error(SQL_ERROR_THROWEN);

        $deviceTokens = array();
        if ($getDeviceTokens->num_rows > 0) {
            $i = 1; $strValues = "";
            while ($deviceToken = $getDeviceTokens->fetch_assoc()) {
                $strValues .= ";". $deviceToken['device_token'];
                $i++;

                if ($i == 100) {
                    $deviceTokens[] = trim($strValues, ";");
                    $i = 1; $strValues = "";
                }
            }
            if (!is_empty($strValues)) {
                $deviceTokens[] = trim($strValues, ";");
            }
        }

        $notification['time'] = $date;
        $notification['type'] = 'notification';

        $notification['to_user_id'] = 0;
        $notification['count_notifications'] = 0;

        $notification['from_user_id'] = $this->_data['user_id'];
        $notification['action'] = $action;
        $notification['node_type'] = $node_type;
        $notification['node_url'] = $node_url;
        $notification['extra1'] = convertText4Web($extra1);
        $notification['extra2'] = convertText4Web($extra2);
        $notification['extra3'] = convertText4Web($extra3);

        $notification['user_name'] = $this->_data['user_name'];
        $notification['user_fullname'] = convertText4Web($this->_data['user_fullname']);
        $notification['user_picture'] = $this->_data['user_picture'];

        /* parse notification */
        switch ($notification['action']) {
            case 'friend':
                $notification['message'] = __("accepted your friend request");
                break;

            case 'follow':
                $notification['message'] = __("now following you");
                break;

            case 'like':
                if ($notification['node_type'] == "post") {
                    $notification['message'] = __("likes your post");

                } elseif ($notification['node_type'] == "post_comment") {
                    $notification['message'] = __("likes your comment");

                } elseif ($notification['node_type'] == "photo") {
                    $notification['message'] = __("likes your photo");

                } elseif ($notification['node_type'] == "photo_comment") {
                    $notification['message'] = __("likes your comment");
                }
                break;

            case 'comment':
                if ($notification['node_type'] == "post") {
                    $notification['message'] = __("commented on your post");

                } elseif ($notification['node_type'] == "photo") {
                    $notification['message'] = __("commented on your photo");
                }
                break;

            case 'share':
                $notification['message'] = __("shared your post");
                break;

            case 'mention':
                $notification['icon'] = "fa-comment";
                if ($notification['node_type'] == "post") {
                    $notification['message'] = __("Mentioned you in a post");

                } elseif ($notification['node_type'] == "comment") {
                    $notification['message'] = __("mentioned you in a comment");

                } elseif ($notification['node_type'] == "photo") {
                    $notification['message'] = __("mentioned you in a comment");
                }
                break;

            case 'profile_visit':
                $notification['message'] = __("visited your profile");
                break;

            case 'wall':
                $notification['message'] = __("posted on your wall");
                break;

            case NOTIFICATION_POST_IN_PAGE:
                $notification['user_fullname'] = $notification['extra1'];
                $notification['user_picture'] = $notification['extra2'];
                $notification['message'] = __("has a new post");
                break;

            case NOTIFICATION_POST_IN_GROUP:
                $notification['message'] = $notification['user_fullname'] . ' ' . __("posted in") . ' ' . $notification['extra2'];
                break;
        }


        foreach ($deviceTokens as $deviceToken) {
            $strSql = sprintf("INSERT INTO ci_background_notifications (type, count_notifications, message, to_user_id, from_user_id, 
                action, node_type, node_url, time, extra1, extra2, extra3, user_name, user_fullname, user_picture, device_token)
                            VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
                secure($notification['type']), secure($notification['count_notifications'], 'int'), secure($notification['message']), secure($notification['to_user_id'], 'int'), secure($notification['from_user_id'], 'int'), secure($notification['action']), secure($notification['node_type']),
                secure($notification['node_url']), secure($notification['time']), secure($notification['extra1']), secure($notification['extra2']), secure($notification['extra3']), secure($notification['user_name']), secure($notification['user_fullname']),
                secure($notification['user_picture']), secure($deviceToken) );

            $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        }

    }


    /**
     * post_notification
     *
     * @param integer $to_user_id
     * @param string $action
     * @param string $node_type
     * @param string $node_url
     * @param string $notify_id
     * @return void
     */
    public function post_notification_for_social($args = array()) {
        global $db, $date, $system;
        /* initialize arguments */
        $to_user_id = !isset($args['to_user_id']) ? _error(400) : $args['to_user_id'];
        $from_user_id = !isset($args['from_user_id']) ? $this->_data['user_id'] : $args['from_user_id'];
        $action = !isset($args['action']) ? _error(400) : $args['action'];
        $node_type = !isset($args['node_type']) ? '' : $args['node_type'];
        $node_url = !isset($args['node_url']) ? '' : $args['node_url'];
        $notify_id = !isset($args['notify_id']) ? '' : $args['notify_id'];

        /* if the viewer is the target */
        if($this->_data['user_id'] == $to_user_id) {
            return;
        }
        /* get receiver user */
        $receivers = $this->get_friends_info($to_user_id);
        if (is_null($receivers)) {
            return;
        }

        $notification = array();
        /* insert notification */
        $db->query(sprintf("INSERT INTO notifications (to_user_id, from_user_id, action, node_type, node_url, notify_id, time) VALUES (%s, %s, %s, %s, %s, %s, %s)", secure($to_user_id, 'int'), secure($from_user_id, 'int'), secure($action), secure($node_type), secure($node_url), secure($notify_id), secure($date) )) or _error(SQL_ERROR_THROWEN);
        $notification['notification_id'] = $db->insert_id;

        /* update notifications counter +1 */
        $db->query(sprintf("UPDATE users SET user_live_notifications_counter = user_live_notifications_counter + 1 WHERE user_id = %s", secure($to_user_id, 'int') )) or _error(SQL_ERROR_THROWEN);

        $notification['time'] = $date;
        $notification['type'] = 'notification';

        foreach ($receivers as $receiver) {
            if (!is_empty($receiver['device_token'])) {
                $notification['device_token'] = $receiver['device_token'];
                $notification['count_notifications'] = $receiver['user_live_notifications_counter'] + 1;

                $notification['to_user_id'] = $to_user_id;
                $notification['from_user_id'] = $from_user_id;
                $notification['action'] = $action;
                $notification['node_type'] = $node_type;
                $notification['node_url'] = $node_url;

                $notification['user_name'] = $this->_data['user_name'];
                $notification['user_fullname'] = html2text($this->_data['user_fullname']);
                $notification['user_picture'] = $this->_data['user_picture'];

                /* parse notification */
                switch ($notification['action']) {
                    case 'friend_add':
                        $notification['message'] = __("send you a friend request");
                        break;

                    case 'friend_accept':
                        $notification['message'] = __("accepted your friend request");
                        break;

                    case 'follow':
                        $notification['message'] = __("now following you");
                        break;

                    case 'like':
                        if($node_type == "post") {
                            $notification['message'] = __("likes your post");

                        } elseif ($node_type == "photo") {
                            $notification['message'] = __("likes your photo");

                        } elseif ($node_type == "post_comment") {
                            $notification['message'] = __("likes your comment");

                        } elseif ($node_type == "photo_comment") {
                            $notification['message'] = __("likes your comment");

                        } elseif ($node_type == "post_reply") {
                            $notification['message'] = __("likes your reply");

                        } elseif ($node_type == "photo_reply") {
                            $notification['message'] = __("likes your reply");
                        }
                        break;

                    case 'comment':
                        if($node_type == "post") {
                            $notification['message'] = __("commented on your post");

                        } elseif ($node_type == "photo") {
                            $notification['message'] = __("commented on your photo");
                        }
                        break;

                    case 'reply':
                        if($node_type == "post") {
                            $notification['url'] = $system['system_url'].'/posts/'.$node_url.$notify_id;

                        } elseif ($node_type == "photo") {
                            $notification['url'] = $system['system_url'].'/photos/'.$node_url.$notify_id;
                        }
                        $notification['message'] = __("replied to your comment");
                        break;

                    case 'share':
                        $notification['message'] = __("shared your post");
                        break;

                    case 'mention':
                        switch ($node_type) {
                            case 'post':
                                $notification['message'] = __("mentioned you in a post");
                                break;

                            case 'comment_post':
                                $notification['message'] = __("mentioned you in a comment");
                                break;

                            case 'comment_photo':
                                $notification['message'] = __("mentioned you in a comment");
                                break;

                            case 'reply_post':
                                $notification['message'] = __("mentioned you in a reply");
                                break;

                            case 'reply_photo':
                                $notification['message'] = __("mentioned you in a reply");
                                break;
                        }
                        break;

                    case 'profile_visit':
                        $notification['message'] = __("visited your profile");
                        break;

                    case 'wall':
                        $notification['message'] = __("posted on your wall");
                        break;

                }
                $this->insertNotice($notification);
            }
        }

    }


    /**
     * delete_notification
     *
     * @param integer $to_user_id
     * @param string $action
     * @return void
     */
    public function delete_notification($to_user_id, $action, $node_type = '', $node_url = '') {
        global $db;
        /* delete notification */
        $db->query(sprintf("DELETE FROM notifications WHERE to_user_id = %s AND from_user_id = %s AND action = %s AND node_type = %s AND node_url = %s", secure($to_user_id, 'int'), secure($this->_data['user_id'], 'int'), secure($action), secure($node_type), secure($node_url) )) or _error(SQL_ERROR_THROWEN);
        /* update notifications counter -1 */
        $db->query(sprintf("UPDATE users SET user_live_notifications_counter = IF(user_live_notifications_counter=0,0,user_live_notifications_counter-1) WHERE user_id = %s", secure($to_user_id, 'int') )) or _error(SQL_ERROR_THROWEN);
    }



    /* ------------------------------- */
    /* Chat */
    /* ------------------------------- */

    /**
     * user_online
     * 
     * @param integer $user_id
     * @return boolean
     */
    public function user_online($user_id) {
        global $db;
        /* first -> check if the target user enable the chat */
        $get_user_status = $db->query(sprintf("SELECT * FROM users WHERE users.user_chat_enabled = '1' AND users.user_id = %s", secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        if($get_user_status->num_rows == 0) {
            /* if no > return false */
            return false;
        }
        /* second -> check if the target user is friend to the viewer */
        if(!in_array($user_id, $this->_data['friends_ids'])) {
            /* if no > return false */
            return false;
        }
        /* third > check if the target user is online */
        $check_user_online = $db->query(sprintf("SELECT * FROM users_online WHERE users_online.user_id = %s", secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        if($check_user_online->num_rows == 0) {
            /* if no > return false */
            return false;
        } else {
            /* if yes > return false */
            return true;
        }
    }


    /**
     * get_online_friends
     * 
     * @return array
     */
    public function get_online_friends() {
        global $db, $system, $date;
        /* check if the viewer is already online */
        $check = $db->query(sprintf("SELECT * FROM users_online WHERE user_id = %s", secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
        if($check->num_rows == 0) {
            /* if no -> insert into online table */
            $db->query(sprintf("INSERT INTO users_online (user_id) VALUES (%s)", secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
        } else {
            /* if yes -> update it's last seen time */
            $db->query(sprintf("UPDATE users_online SET last_seen = NOW() WHERE user_id = %s", secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
        }
        /* remove any user not seen in last the $system['offline_time'] seconds */
        $get_not_seen = $db->query(sprintf("SELECT * FROM users_online WHERE last_seen < SUBTIME(NOW(),'0 0:0:%s')", secure($system['offline_time'], 'int', false) )) or _error(SQL_ERROR_THROWEN);
        while($not_seen = $get_not_seen->fetch_assoc()) {
            /* update last login time */
            $db->query(sprintf("UPDATE users SET user_last_login = %s WHERE user_id = %s", secure($date), secure($not_seen['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
            /* remove this user from online table */
            $db->query(sprintf("DELETE FROM users_online WHERE user_id = %s", secure($not_seen['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
        }
        /* get online friends */
        $online_friends = array();
        /* check if the viewer has friends */
        if(count($this->_data['friends_ids']) == 0) {
            return $online_friends;
        }
        /* make a list from viewer's friends */
        $friends_list = implode(',', $this->_data['friends_ids']);
        $get_online_friends = $db->query(sprintf("SELECT users.user_id, users.user_name, users.user_fullname, users.user_gender, users.user_picture, users_online.last_seen FROM users_online INNER JOIN users ON users_online.user_id = users.user_id WHERE users_online.user_id IN (%s) AND users.user_chat_enabled = '1'", $friends_list )) or _error(SQL_ERROR_THROWEN);
        if($get_online_friends->num_rows > 0) {
            while($online_friend = $get_online_friends->fetch_assoc()) {
                $online_friend['user_picture'] = $this->get_picture($online_friend['user_picture'], $online_friend['user_gender']);
                $online_friend['user_is_online'] = '1';
                $online_friends[] = $online_friend;
            }
        }
        return $online_friends;
    }


    /**
     * get_offline_friends
     * 
     * @return array
     */
    public function get_offline_friends() {
        global $db, $system;
        /* get offline friends */
        $offline_friends = array();
        /* check if the viewer has friends */
        if(count($this->_data['friends_ids']) == 0) {
            return $offline_friends;
        }
        /* make a list from viewer's friends */
        $friends_list = implode(',', $this->_data['friends_ids']);
        $get_offline_friends = $db->query(sprintf("SELECT users.user_id, users.user_name, users.user_fullname, users.user_gender, users.user_picture, users.user_last_login FROM users LEFT JOIN users_online ON users.user_id = users_online.user_id WHERE users.user_chat_enabled = '1' AND users.user_id IN (%s) AND users_online.user_id IS NULL LIMIT %s", $friends_list, secure($system['max_results'], 'int', false) )) or _error(SQL_ERROR_THROWEN);
        if($get_offline_friends->num_rows > 0) {
            while($offline_friend = $get_offline_friends->fetch_assoc()) {
                $offline_friend['user_picture'] = $this->get_picture($offline_friend['user_picture'], $offline_friend['user_gender']);
                $offline_friend['user_is_online'] = '0';
                $offline_friends[] = $offline_friend;
            }
        }
        return $offline_friends;
    }
    

    /**
     * get_conversations_new
     * 
     * @return array
     */
    public function get_conversations_new() {
        global $db;
        $conversations = array();
        if(!empty($_SESSION['chat_boxes_opened'])) {
            /* make list from opened chat boxes keys (conversations ids) */
            $chat_boxes_opened_list = implode(',',$_SESSION['chat_boxes_opened']);
            $get_conversations = $db->query(sprintf("SELECT conversation_id FROM conversations_users WHERE user_id = %s AND seen = '0' AND deleted = '0' AND conversation_id NOT IN (%s)", secure($this->_data['user_id'], 'int'), $chat_boxes_opened_list )) or _error(SQL_ERROR_THROWEN);
        } else {
            $get_conversations = $db->query(sprintf("SELECT conversation_id FROM conversations_users WHERE user_id = %s AND seen = '0' AND deleted = '0'", secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
        }
        if($get_conversations->num_rows > 0) {
            while($conversation = $get_conversations->fetch_assoc()) {
                $db->query(sprintf("UPDATE conversations_users SET seen = '1' WHERE conversation_id = %s AND user_id = %s", secure($conversation['conversation_id'], 'int'), secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                $conversations[] = $this->get_conversation($conversation['conversation_id']);
            }
        }
        return $conversations;
    }

    
    /**
     * get_conversations
     * 
     * @param integer $offset
     * @return array
     */
    public function get_conversations($offset = 0) {
        global $db, $system;
        $conversations = array();
        $offset *= $system['max_results'];
        $get_conversations = $db->query(sprintf("SELECT conversations.conversation_id FROM conversations INNER JOIN conversations_messages ON conversations.last_message_id = conversations_messages.message_id INNER JOIN conversations_users ON conversations.conversation_id = conversations_users.conversation_id WHERE conversations_users.deleted = '0' AND conversations_users.user_id = %s ORDER BY conversations_messages.time DESC LIMIT %s, %s", secure($this->_data['user_id'], 'int'), secure($offset, 'int', false), secure($system['max_results'], 'int', false) )) or _error(SQL_ERROR_THROWEN);
        if($get_conversations->num_rows > 0) {
            while($conversation = $get_conversations->fetch_assoc()) {
                $conversation = $this->get_conversation($conversation['conversation_id']);
                if($conversation) {
                    $conversations[] = $conversation;
                }
            }
        }
        return $conversations;
    }


    /**
     * get_conversation
     * 
     * @param integer $conversation_id
     * @return array
     */
    public function get_conversation($conversation_id) {
        global $db;
        $conversation = array();
        $get_conversation = $db->query(sprintf("SELECT conversations.conversation_id, conversations.last_message_id, conversations_messages.message, conversations_messages.image, conversations_messages.time, conversations_users.seen FROM conversations INNER JOIN conversations_messages ON conversations.last_message_id = conversations_messages.message_id INNER JOIN conversations_users ON conversations.conversation_id = conversations_users.conversation_id WHERE conversations_users.deleted = '0' AND conversations_users.user_id = %s AND conversations.conversation_id = %s", secure($this->_data['user_id'], 'int'), secure($conversation_id, 'int') )) or _error(SQL_ERROR_THROWEN);
        if($get_conversation->num_rows == 0) {
            return false;
        }
        $conversation = $get_conversation->fetch_assoc();
        /* get recipients */
        $get_recipients = $db->query(sprintf("SELECT users.user_id, users.user_name, users.user_fullname, users.user_gender, users.user_picture FROM conversations_users INNER JOIN users ON conversations_users.user_id = users.user_id WHERE conversations_users.conversation_id = %s AND conversations_users.user_id != %s", secure($conversation['conversation_id'], 'int'), secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
        $recipients_num = $get_recipients->num_rows;
        if($recipients_num == 0) {
            return false;
        }
        $i = 1;
        while($recipient = $get_recipients->fetch_assoc()) {
            $recipient['user_picture'] = $this->get_picture($recipient['user_picture'], $recipient['user_gender']);
            $conversation['recipients'][] = $recipient;
            
            $conversation['name_list'] .= $recipient['user_fullname'];
            $conversation['ids'] .= $recipient['user_id'];
            if($i < $recipients_num) {
                $conversation['name_list'] .= ", ";
                $conversation['ids'] .= "_";
            }
            $i++;
        }
        /* prepare conversation with multiple_recipients */
        if($recipients_num > 1) {
            /* multiple recipients */
            $conversation['multiple_recipients'] = true;
            $conversation['picture_left'] = $conversation['recipients'][0]['user_picture'];
            $conversation['picture_right'] = $conversation['recipients'][1]['user_picture'];
            if($recipients_num > 2) {
                $conversation['name'] = get_firstname($conversation['recipients'][0]['user_fullname']).", ".get_firstname($conversation['recipients'][1]['user_fullname'])." & ".($recipients_num - 2)." ".__("more");
            } else {
                $conversation['name'] = get_firstname($conversation['recipients'][0]['user_fullname'])." & ".get_firstname($conversation['recipients'][1]['user_fullname']);
            }
        } else {
            /* one recipient */
            $conversation['multiple_recipients'] = false;
            $conversation['picture'] = $conversation['recipients'][0]['user_picture'];
            $conversation['name'] = html_entity_decode($conversation['recipients'][0]['user_fullname']);
            $conversation['name_html'] = popover($conversation['recipients'][0]['user_id'], $conversation['recipients'][0]['user_name'], $conversation['recipients'][0]['user_fullname']);
        }
        /* get total number of messages */
        $get_messages = $db->query(sprintf("SELECT * FROM conversations_messages WHERE conversation_id = %s", secure($conversation_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        $conversation['total_messages'] = $get_messages->num_rows;
        /* decode_emoji */
        $conversation['message'] = decode_emoji($conversation['message']);
        /* return */
        return $conversation;
    }


    /**
     * get_mutual_conversation
     * 
     * @param array $recipients
     * @return integer
     */
    public function get_mutual_conversation($recipients) {
        global $db;
        $recipients_array = (array)$recipients;
        $recipients_array[] = $this->_data['user_id'];
        $recipients_list = implode(',', $recipients_array);
        $get_mutual_conversations = $db->query(sprintf('SELECT conversation_id FROM conversations_users WHERE user_id IN (%s) GROUP BY conversation_id HAVING COUNT(conversation_id) = %s', $recipients_list, count($recipients_array) )) or _error(SQL_ERROR_THROWEN);
        if($get_mutual_conversations->num_rows == 0) {
            return false;
        }
        while($mutual_conversation = $get_mutual_conversations->fetch_assoc()) {
            /* get recipients */
            $get_recipients = $db->query(sprintf("SELECT * FROM conversations_users WHERE conversation_id = %s", secure($mutual_conversation['conversation_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
            if($get_recipients->num_rows == count($recipients_array)) {
                return $mutual_conversation['conversation_id'];
            }
        }
    }


    /**
     * get_conversation_messages
     * 
     * @param integer $conversation_id
     * @param integer $offset
     * @param integer $last_message_id
     * @return array
     */
    public function get_conversation_messages($conversation_id, $offset = 0, $last_message_id = null) {
        global $db, $system;
        /* check if user authorized */
        $check_conversation = $db->query(sprintf("SELECT * FROM conversations_users WHERE conversations_users.conversation_id = %s AND conversations_users.user_id = %s", secure($conversation_id, 'int'), secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
        if($check_conversation->num_rows == 0) {
            throw new Exception(__("You are not authorized to view this"));
        }
        $offset *= $system['max_results'];
        $messages = array();
        if($last_message_id !== null) {
            /* get all messages after the last_message_id */
            $get_messages = $db->query(sprintf("SELECT conversations_messages.message_id, conversations_messages.message, conversations_messages.image, conversations_messages.time, users.user_id, users.user_name, users.user_fullname, users.user_gender, users.user_picture FROM conversations_messages INNER JOIN users ON conversations_messages.user_id = users.user_id WHERE conversations_messages.conversation_id = %s AND conversations_messages.message_id > %s", secure($conversation_id, 'int'), secure($last_message_id, 'int') )) or _error(SQL_ERROR_THROWEN);
        } else {
            $get_messages = $db->query(sprintf("SELECT * FROM ( SELECT conversations_messages.message_id, conversations_messages.message, conversations_messages.image, conversations_messages.time, users.user_id, users.user_name, users.user_fullname, users.user_gender, users.user_picture FROM conversations_messages INNER JOIN users ON conversations_messages.user_id = users.user_id WHERE conversations_messages.conversation_id = %s ORDER BY conversations_messages.message_id DESC LIMIT %s,%s ) messages ORDER BY messages.message_id ASC", secure($conversation_id, 'int'), secure($offset, 'int', false), secure($system['max_results'], 'int', false) )) or _error(SQL_ERROR_THROWEN);
        }
        while($message = $get_messages->fetch_assoc()) {
            $message['user_picture'] = $this->get_picture($message['user_picture'], $message['user_gender']);
            /* decode html entity */
            //$message['message'] = html_entity_decode($message['message'], ENT_QUOTES);
            /* decode urls */
            $message['message'] = decode_urls($message['message']);
            /* decode_emoji */
            $message['message'] = decode_emoji($message['message']);
            /* return */
            $messages[] = $message;
        }
        return $messages;
    }


    /**
     * post_conversation_message
     * 
     * @param string $message
     * @param string $image
     * @param integer $conversation_id
     * @param array $recipients
     * @return void
     */
    public function post_conversation_message($message, $image, $conversation_id = null, $recipients = null) {
        global $db, $date, $system; //ConIu - thêm biến $system
        /* check if posting the message to (new || existed) conversation */
        if($conversation_id == null) {
            /* [1] post the message to -> a new conversation */
            /* [first] check previous conversation between (me & recipients) */
            $mutual_conversation = $this->get_mutual_conversation($recipients);
            if(!$mutual_conversation) {
                /* [1] there is no conversation between me and the recipients -> start new one */
                /* insert conversation */
                $db->query("INSERT INTO conversations (last_message_id) VALUES ('0')") or _error(SQL_ERROR_THROWEN);
                $conversation_id = $db->insert_id;
                /* insert the sender (me) */
                $db->query(sprintf("INSERT INTO conversations_users (conversation_id, user_id, seen) VALUES (%s, %s, '1')", secure($conversation_id, 'int'), secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                /* insert recipients */
                foreach($recipients as $recipient) {
                    $db->query(sprintf("INSERT INTO conversations_users (conversation_id, user_id) VALUES (%s, %s)", secure($conversation_id, 'int'), secure($recipient, 'int') )) or _error(SQL_ERROR_THROWEN);
                }
            } else {
                /* [2] there is a conversation between me and the recipients */
                /* set the conversation_id */
                $conversation_id = $mutual_conversation;
            }
        } else {
            /* [2] post the message to -> existed conversation */
            /* check if user authorized */
            $check_conversation = $db->query(sprintf("SELECT * FROM conversations_users WHERE conversation_id = %s AND user_id = %s", secure($conversation_id, 'int'), secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
            if($check_conversation->num_rows == 0) {
                throw new Exception(__("You are not authorized to do this"));
            }
            /* update sender me as seen and not deleted if any */
            $db->query(sprintf("UPDATE conversations_users SET seen = '1', deleted = '0' WHERE conversation_id = %s AND user_id = %s", secure($conversation_id, 'int'), secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
            /* update recipients as not seen */
            $db->query(sprintf("UPDATE conversations_users SET seen = '0' WHERE conversation_id = %s AND user_id != %s", secure($conversation_id, 'int'), secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
        }
        /* insert message */
        $image = ($image != '')? $image : '';
        $db->query(sprintf("INSERT INTO conversations_messages (conversation_id, user_id, message, image, time) VALUES (%s, %s, %s, %s, %s)", secure($conversation_id, 'int'), secure($this->_data['user_id'], 'int'), secure($message), secure($image), secure($date) )) or _error(SQL_ERROR_THROWEN);
        $message_id = $db->insert_id;
        /* update the conversation with last message id */
        $db->query(sprintf("UPDATE conversations SET last_message_id = %s WHERE conversation_id = %s", secure($message_id, 'int'), secure($conversation_id, 'int') )) or _error(SQL_ERROR_THROWEN);
        /* update sender (me) with last message id */
        $db->query(sprintf("UPDATE users SET user_live_messages_lastid = %s WHERE user_id = %s", secure($message_id, 'int'), secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
        /* get conversation */
        $conversation = $this->get_conversation($conversation_id);
        /* update the only offline recipient messages counter & all with last message id */
        /* get current online users */
        $online_users = array();
        $get_online_users = $db->query("SELECT user_id FROM users_online") or _error(SQL_ERROR_THROWEN);
        if($get_online_users->num_rows > 0) {
            while($online_user = $get_online_users->fetch_assoc()) {
                $online_users[] = $online_user['user_id'];
            }
        }
        foreach($conversation['recipients'] as $recipient) {
            if($system['chat_enabled'] && in_array($recipient['user_id'], $online_users)) {
                $db->query(sprintf("UPDATE users SET user_live_messages_lastid = %s WHERE user_id = %s", secure($message_id, 'int'), secure($recipient['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
            } else {
                $db->query(sprintf("UPDATE users SET user_live_messages_lastid = %s, user_live_messages_counter = user_live_messages_counter + 1 WHERE user_id = %s", secure($message_id, 'int'), secure($recipient['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
            }
        }
        /* return with converstaion */
        return $conversation;       
    }


    /**
     * delete_conversation
     * 
     * @param integer $conversation_id
     * @return void
     */
    public function delete_conversation($conversation_id) {
        global $db;
        /* check if user authorized */
        $check_conversation = $db->query(sprintf("SELECT * FROM conversations_users WHERE conversation_id = %s AND user_id = %s", secure($conversation_id, 'int'), secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
        if($check_conversation->num_rows == 0) {
            throw new Exception(__("You are not authorized to do this"));
        }
        /* update converstaion as deleted */
        $db->query(sprintf("UPDATE conversations_users SET deleted = '1' WHERE conversation_id = %s AND user_id = %s", secure($conversation_id, 'int'), secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
    }


    /* ------------------------------- */
    /* Emoji & Stickers */
    /* ------------------------------- */

    /**
     * get_emojis
     *
     * @return array
     */
    public function get_emojis() {
        global $db;
        $emojis = array();
        $get_emojis = $db->query("SELECT * FROM emojis") or _error(SQL_ERROR_THROWEN);
        if($get_emojis->num_rows > 0) {
            while($emoji = $get_emojis->fetch_assoc()) {
                $emojis[] = $emoji;
            }
        }
        return $emojis;
    }


    /**
     * decode_emoji
     *
     * @param string $text
     * @return string
     */
    public function decode_emoji($text) {
        global $db;
        $get_emojis = $db->query("SELECT * FROM emojis") or _error(SQL_ERROR_THROWEN);
        if($get_emojis->num_rows > 0) {
            while($emoji = $get_emojis->fetch_assoc()) {
                $replacement = '<i class="twa twa-xlg twa-'.$emoji['class'].'"></i>';
                $pattern = preg_quote($emoji['pattern'], '/');
                $text = preg_replace('/(^|\s)'.$pattern.'/', $replacement, $text);
            }
        }
        return $text;
    }


    /**
     * get_stickers
     *
     * @return array
     */
    public function get_stickers() {
        global $db;
        $stickers = array();
        $get_stickers = $db->query("SELECT * FROM stickers") or _error(SQL_ERROR_THROWEN);
        if($get_stickers->num_rows > 0) {
            while($sticker = $get_stickers->fetch_assoc()) {
                $stickers[] = $sticker;
            }
        }
        return $stickers;
    }


    /**
     * decode_stickers
     *
     * @param string $text
     * @return string
     */
    public function decode_stickers($text) {
        global $db, $system;
        $get_stickers = $db->query("SELECT * FROM stickers") or _error(SQL_ERROR_THROWEN);
        if($get_stickers->num_rows > 0) {
            while($sticker = $get_stickers->fetch_assoc()) {
                $replacement = '<img class="" src="'.$system['system_uploads'].'/'.$sticker['image'].'"></i>';
                $text = preg_replace('/(^|\s):STK-'.$sticker['sticker_id'].':/', $replacement, $text);
            }
        }
        return $text;
    }


    /* ------------------------------- */
    /* @Mentions */
    /* ------------------------------- */

    /**
     * get_mentions
     * 
     * @param array $matches
     * @return string
     */
    public function get_mentions($matches) {
        global $db;
        $get_user = $db->query(sprintf("SELECT user_id, user_name, user_fullname, user_firstname, user_lastname FROM users WHERE user_name = %s", secure($matches[1]) )) or _error(SQL_ERROR_THROWEN);
        if($get_user->num_rows > 0) {
            $user = $get_user->fetch_assoc();
            $replacement = popover($user['user_id'], $user['user_name'], $user['user_fullname']);
        }else {
            $replacement = $matches[0];
        }
        return $replacement;
    }


    /**
     * post_mentions
     *
     * @param string $text
     * @param integer $node_url
     * @param string $node_type
     * @param string $notify_id
     * @param array $excluded_ids
     * @return void
     */
    public function post_mentions($text, $node_url, $node_type = 'post', $notify_id = '', $excluded_ids = array()) {
        global $db;
        $where_query = "";
        if($excluded_ids) {
            $excluded_list = implode(',',$excluded_ids);
            $where_query = " user_id NOT IN ($excluded_list) AND ";
        }
        $done = array();
        if(preg_match_all('/\[([a-z0-9._]+)\]/', $text, $matches)) {
            foreach ($matches[1] as $username) {
                if($this->_data['user_name'] != $username && !in_array($username, $done)) {
                    $get_user = $db->query(sprintf("SELECT user_id FROM users WHERE ".$where_query." user_name = %s", secure($username) )) or _error(SQL_ERROR_THROWEN);
                    if($get_user->num_rows > 0) {
                        $_user = $get_user->fetch_assoc();
                        $this->post_notification_for_social( array('to_user_id'=>$_user['user_id'], 'action'=>'mention', 'node_type'=>$node_type, 'node_url'=>$node_url, 'notify_id'=>$notify_id) );
                        $done[] = $username;
                    }
                }
            }
        }
    }



    /* ------------------------------- */
    /* Publisher */
    /* ------------------------------- */

    /**
     * publisher
     * 
     * @param array $args
     * @return array
     */
    public function publisher(array $args = array()) {
        global $db, $system, $date, $not_receive_page_ids, $not_receive_group_ids;
        $post = array();

        /* default */
        $post['user_id'] = $this->_data['user_id'];
        $post['user_type'] = "user";
        $post['in_wall'] = 0;
        $post['wall_id'] = null;
        $post['in_group'] = 0;
        $post['group_id'] = null;
        $post['in_event'] = 0;
        $post['event_id'] = null;
        $post['school_id'] = 0; //Coniu
        $notify_from_page = 0; //Thông báo từ page, group?
        $notify_from_group = 0; //Thông báo từ page, group?

        $post['author_id'] = $this->_data['user_id'];
        $post['post_author_picture'] = $this->_data['user_picture'];
        $post['post_author_url'] = $system['system_url'].'/'.$this->_data['user_name'];
        $post['post_author_name'] = $this->_data['user_fullname'];
        $post['post_author_verified'] = $this->_data['user_verified'];


        /* check the user_type */
        if($args['handle'] == "user") {
            /* check if system allow wall posts */
            if(!$system['wall_posts_enabled']) {
                _error(400);
            }
            /* check if the user is valid & the viewer can post on his wall */
            $check_user = $db->query(sprintf("SELECT * FROM users WHERE user_id = %s",secure($args['id'], 'int'))) or _error(SQL_ERROR_THROWEN);
            if($check_user->num_rows == 0) {
                _error(400);
            }
            $_user = $check_user->fetch_assoc();
            if($_user['user_privacy_wall'] == 'me' || ($_user['user_privacy_wall'] == 'friends' && !in_array($args['id'], $this->_data['friends_ids'])) ) {
                _error(400);
            }
            $post['in_wall'] = 1;
            $post['wall_id'] = $args['id'];
            $post['wall_username'] = $_user['user_name'];
            $post['wall_fullname'] = $_user['user_fullname'];

        } elseif ($args['handle'] == "page") {
            /* check if the page is valid */
            $check_page = $db->query(sprintf("SELECT * FROM pages WHERE page_id = %s", secure($args['id'], 'int') )) or _error(SQL_ERROR_THROWEN);
            if($check_page->num_rows == 0) {
                _error(400);
            }
            $_page = $check_page->fetch_assoc();

            // Lấy danh sách id quản lý của trường - Taila - Thêm danh sách giáo viên của trường để mọi giáo viên đều có thể đăng lên page.
            $teachers = getSchoolData($args['id'], SCHOOL_TEACHERS);
            $teacherIds = array_keys($teachers);

            /* check if the viewer is the admin */
            if((!$this->check_page_adminship($this->_data['user_id'], $args['id'])) && (!in_array($this->_data['user_id'], $teacherIds))) {
                _error(400);
            }

            //Coniu - Begin - Lưu thêm trường school_id nếu là post lên tường của trường
            if ($_page['page_category'] == SCHOOL_CATEGORY_ID) {
                $post['school_id'] = $_page['page_id'];
            }
            //Có gửi thông báo đến user trong page hay không?
            if (!in_array($_page['page_id'], $not_receive_page_ids)) {
                $notify_from_page = 1;
            }
            //Coniu - End - Lưu thêm trường school_id nếu là post lên tường của trường

            $post['user_id'] = $_page['page_id'];
            $post['user_type'] = "page";
            $post['post_author_picture'] = $this->get_picture($_page['page_picture'], "page");
            $post['post_author_url'] = $system['system_url'].'/pages/'.$_page['page_name'];
            $post['post_author_name'] = $_page['page_title'];
            $post['post_author_verified'] = $this->_data['page_verified'];

        } elseif ($args['handle'] == "group") {
            /* check if the group is valid & the viewer is a member */
            //Coniu - Begin - Lưu thêm trường school_id nếu là post lên tường của lớp
            //$check_group = $db->query(sprintf("SELECT * FROM groups WHERE group_id = %s", secure($args['id'], 'int') )) or _error(SQL_ERROR_THROWEN);

            $check_group = $db->query(sprintf("SELECT G.*, CL.school_id FROM groups G 
                INNER JOIN groups_members M ON G.group_id = M.group_id 
                LEFT JOIN ci_class_level CL ON (G.class_level_id > 0) AND (G.class_level_id = CL.class_level_id)
                WHERE M.approved = '1' AND M.user_id = %s AND M.group_id = %s",
                secure($this->_data['user_id'], 'int'), secure($args['id'], 'int') )) or _error(SQL_ERROR_THROWEN);

            //Coniu - End - Lưu thêm trường school_id nếu là post lên tường của lớp

            if($check_group->num_rows == 0) {
                // Kiểm tra xem user hiện tại có pải admin của group không?
                $i_admin = $this->check_group_adminship($this->_data['user_id'], $args['id']);
                if(!$i_admin) {
                    _error(400);
                }
            }
            $_group = $check_group->fetch_assoc();

            //Coniu - Begin - Lưu thêm trường school_id nếu là post lên tường của trường
            if ($_group['class_level_id'] > 0) {
                $post['school_id'] = $_group['school_id'];

                /* CI - kiểm tra user có được viết lên tường của lớp hay không? */
                $school = getSchoolData($_group['school_id'], SCHOOL_DATA);
                if (is_null($school)) {
                    _error(__("Error"), __("Class does not belong to an existing school"));
                }

                //Nếu chưa phải thành viên nhóm (ko phải phụ huynh hay giao viên) và không phải admin trường thì báo lỗi
                $_group['i_joined'] = $this->check_group_membership($this->_data['user_id'], $_group['group_id']);
                if ((!$_group['i_joined']) && ($this->_data['user_id'] != $school['page_admin'])) {
                    _error(404);
                }

                $teachers = getClassData($_group['group_id'], CLASS_TEACHERS);

                //Kiểm tra vai trò của người dùng hiện tại với hệ thống
                $role_id = PERMISSION_NONE;
                if ($this->_data['user_id'] == $school['page_admin']) {
                    $role_id = PERMISSION_ALL;
                } else {
                    foreach($teachers as $teacher) {
                        if ($this->_data['user_id'] == $teacher['user_id']) {
                            $role_id = PERMISSION_MANAGE;
                            break;
                        }
                    }
                }

                if (!$school['class_allow_post']
                    && ($_group['group_admin'] != $this->_data['user_id'])
                    && ($role_id == PERMISSION_NONE)) {
                    throw new Exception(__("You have no permission to do this"));
                }
            }
            /* CI - END */

            //Có gửi thông báo đến user trong page hay không?
            if (!in_array($args['id'], $not_receive_group_ids)) {
                $notify_from_group = 1;
            }

            $post['in_group'] = 1;
            $post['group_id'] = $args['id'];
            $post['group_title'] = $_group['group_title'];

        } elseif ($args['handle'] == "event") {
            /* check if the event is valid & the viewer is event member */
            $check_event = $db->query(sprintf("SELECT events.* FROM events INNER JOIN events_members ON events.event_id = events_members.event_id WHERE events_members.user_id = %s AND events_members.event_id = %s", secure($this->_data['user_id'], 'int'), secure($args['id'], 'int') )) or _error(SQL_ERROR_THROWEN);
            $_event = $check_event->fetch_assoc();

            $post['in_event'] = 1;
            $post['event_id'] = $args['id'];
        }

        /* prepare post data */
        $post['text'] = $args['message'];
        $post['time'] = $date;
        $post['location'] = (!is_empty($args['location']) && valid_location($args['location']))? $args['location']: '';
        $post['privacy'] = $args['privacy'];
        $post['likes'] = 0;
        $post['comments'] = 0;
        $post['shares'] = 0;

        /* post feeling */
        $post['feeling_action'] = '';
        $post['feeling_value'] = '';
        $post['feeling_icon'] = '';
        if(!is_empty($args['feeling_action']) && !is_empty($args['feeling_value'])) {
            if($args['feeling_action'] != "Feeling") {
                $_feeling_icon = get_feeling_icon($args['feeling_action'], get_feelings());
            } else {
                $_feeling_icon = get_feeling_icon($args['feeling_value'], get_feelings_types());
            }
            if($_feeling_icon) {
                $post['feeling_action'] = $args['feeling_action'];
                $post['feeling_value'] = $args['feeling_value'];
                $post['feeling_icon'] = $_feeling_icon;
            }
        }

        /* prepare post type */
        if($args['link']) {
            if($args['link']->source_type == "link") {
                $post['post_type'] = 'link';
            } else {
                $post['post_type'] = 'media';
            }
        } elseif ($args['poll_options']) {
            $post['post_type'] = 'poll';
        } elseif ($args['product']) {
            $post['post_type'] = 'product';
            $post['privacy'] = "public";
        } elseif ($args['video']) {
            $post['post_type'] = 'video';
        } elseif ($args['audio']) {
            $post['post_type'] = 'audio';
        } elseif ($args['file']) {
            $post['post_type'] = 'file';
        } elseif(count($args['photos']) > 0) {
            if(!is_empty($args['album'])) {
                $post['post_type'] = 'album';
            } else {
                $post['post_type'] = 'photos';
            }
        } else {
            if($post['location'] != '') {
                $post['post_type'] = 'map';
            } else {
                $post['post_type'] = '';
            }
        }

        /* insert the post */
        //Coniu - Lưu thêm trường school_id
        //$db->query(sprintf("INSERT INTO posts (user_id, user_type, in_wall, wall_id, in_group, group_id, post_type, time, location, privacy, text) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", secure($post['user_id'], 'int'), secure($post['user_type']), secure($post['in_wall'], 'int'), secure($post['wall_id'], 'int'), secure($post['in_group']), secure($post['group_id'], 'int'), secure($post['post_type']), secure($post['time']), secure($post['location']), secure($post['privacy']), secure($post['text']) )) or _error(SQL_ERROR_THROWEN);
        $db->query(sprintf("INSERT INTO posts (user_id, user_type, in_wall, wall_id, in_group, group_id, post_type, time, location, privacy, text, school_id)
                            VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", secure($post['user_id'], 'int'), secure($post['user_type']),
                            secure($post['in_wall'], 'int'), secure($post['wall_id'], 'int'), secure($post['in_group']), secure($post['group_id'], 'int'),
                            secure($post['post_type']), secure($post['time']), secure($post['location']), secure($post['privacy']), secure($post['text']), secure($post['school_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
        $post['post_id'] = $db->insert_id;

        switch ($post['post_type']) {
            case 'link':
                $db->query(sprintf("INSERT INTO posts_links (post_id, source_url, source_host, source_title, source_text, source_thumbnail) VALUES (%s, %s, %s, %s, %s, %s)", secure($post['post_id'], 'int'), secure($args['link']->source_url), secure($args['link']->source_host), secure($args['link']->source_title), secure($args['link']->source_text), secure($args['link']->source_thumbnail) )) or _error(SQL_ERROR_THROWEN);
                $post['link']['link_id'] = $db->insert_id;
                $post['link']['post_id'] = $post['post_id'];
                $post['link']['source_url'] = $args['link']->source_url;
                $post['link']['source_host'] = $args['link']->source_host;
                $post['link']['source_title'] = $args['link']->source_title;
                $post['link']['source_text'] = $args['link']->source_text;
                $post['link']['source_thumbnail'] = $args['link']->source_thumbnail;
                break;

            case 'poll':
                /* insert poll */
                $db->query(sprintf("INSERT INTO posts_polls (post_id) VALUES (%s)", secure($post['post_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                $post['poll']['poll_id'] = $db->insert_id;
                $post['poll']['post_id'] = $post['post_id'];
                $post['poll']['votes'] = '0';
                /* insert poll options */
                foreach($args['poll_options'] as $option) {
                    $db->query(sprintf("INSERT INTO posts_polls_options (poll_id, text) VALUES (%s, %s)", secure($post['poll']['poll_id'], 'int'), secure($option) )) or _error(SQL_ERROR_THROWEN);
                    $poll_option['option_id'] = $db->insert_id;
                    $poll_option['text'] = $option;
                    $poll_option['votes'] = 0;
                    $poll_option['checked'] = false;
                    $post['poll']['options'][] = $poll_option;
                }
                break;

            case 'product':
                $args['product']->location = (!is_empty($args['product']->location) && valid_location($args['product']->location))? $args['product']->location: '';
                $db->query(sprintf("INSERT INTO posts_products (post_id, name, price, category_id, location) VALUES (%s, %s, %s, %s, %s)", secure($post['post_id'], 'int'), secure($args['product']->name), secure($args['product']->price), secure($args['product']->category_id, 'int'), secure($args['product']->location) )) or _error(SQL_ERROR_THROWEN);
                $post['product']['product_id'] = $db->insert_id;
                $post['product']['post_id'] = $post['post_id'];
                $post['product']['name'] = $args['product']->name;
                $post['product']['price'] = $args['product']->price;
                $post['product']['available'] = true;
                /* photos */
                if(count($args['photos']) > 0) {
                    foreach ($args['photos'] as $photo) {
                        $db->query(sprintf("INSERT INTO posts_photos (post_id, source) VALUES (%s, %s)", secure($post['post_id'], 'int'), secure($photo) )) or _error(SQL_ERROR_THROWEN);
                        $post_photo['photo_id'] = $db->insert_id;
                        $post_photo['post_id'] = $post['post_id'];
                        $post_photo['source'] = $photo;
                        $post_photo['likes'] = 0;
                        $post_photo['comments'] = 0;
                        $post['photos'][] = $post_photo;
                    }
                    $post['photos_num'] = count($post['photos']);
                }
                break;

            case 'video':
                $db->query(sprintf("INSERT INTO posts_videos (post_id, source) VALUES (%s, %s)", secure($post['post_id'], 'int'), secure($args['video']->source) )) or _error(SQL_ERROR_THROWEN);
                $post['video']['source'] = $args['video']->source;
                break;

            case 'audio':
                $db->query(sprintf("INSERT INTO posts_audios (post_id, source) VALUES (%s, %s)", secure($post['post_id'], 'int'), secure($args['audio']->source) )) or _error(SQL_ERROR_THROWEN);
                $post['audio']['source'] = $args['audio']->source;
                break;

            case 'file':
                $db->query(sprintf("INSERT INTO posts_files (post_id, source) VALUES (%s, %s)", secure($post['post_id'], 'int'), secure($args['file']->source) )) or _error(SQL_ERROR_THROWEN);
                $post['file']['source'] = $args['file']->source;
                break;

            case 'media':
                $db->query(sprintf("INSERT INTO posts_media (post_id, source_url, source_provider, source_type, source_title, source_text, source_html) VALUES (%s, %s, %s, %s, %s, %s, %s)", secure($post['post_id'], 'int'), secure($args['link']->source_url), secure($args['link']->source_provider), secure($args['link']->source_type), secure($args['link']->source_title), secure($args['link']->source_text), secure($args['link']->source_html) )) or _error(SQL_ERROR_THROWEN);
                $post['media']['media_id'] = $db->insert_id;
                $post['media']['post_id'] = $post['post_id'];
                $post['media']['source_url'] = $args['link']->source_url;
                $post['media']['source_type'] = $args['link']->source_type;
                $post['media']['source_provider'] = $args['link']->source_provider;
                $post['media']['source_title'] = $args['link']->source_title;
                $post['media']['source_text'] = $args['link']->source_text;
                $post['media']['source_html'] = $args['link']->source_html;
                break;

            case 'photos':
                if($args['handle'] == "page") {
                    /* check for page timeline album (public by default) */
                    if(!$_page['page_album_timeline']) {
                        /* create new page timeline album */
                        $db->query(sprintf("INSERT INTO posts_photos_albums (user_id, user_type, title) VALUES (%s, 'page', 'Timeline Photos')", secure($_page['page_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                        $_page['page_album_timeline'] = $db->insert_id;
                        /* update page */
                        $db->query(sprintf("UPDATE pages SET page_album_timeline = %s WHERE page_id = %s", secure($_page['page_album_timeline'], 'int'), secure($_page['page_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                    }
                    $album_id = $_page['page_album_timeline'];
                } elseif ($args['handle'] == "group") {
                    /* check for group timeline album */
                    if(!$_group['group_album_timeline']) {
                        /* create new group timeline album */
                        $db->query(sprintf("INSERT INTO posts_photos_albums (user_id, user_type, in_group, group_id, title, privacy) VALUES (%s, %s, %s, %s, 'Timeline Photos', 'custom')", secure($post['user_id'], 'int'), secure($post['user_type']), secure($post['in_group']), secure($post['group_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                        $_group['group_album_timeline'] = $db->insert_id;
                        /* update group */
                        $db->query(sprintf("UPDATE groups SET group_album_timeline = %s WHERE group_id = %s", secure($_group['group_album_timeline'], 'int'), secure($_group['group_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                    }
                    $album_id = $_group['group_album_timeline'];
                } elseif ($args['handle'] == "event") {
                    /* check for event timeline album */
                    if(!$_event['event_album_timeline']) {
                        /* create new event timeline album */
                        $db->query(sprintf("INSERT INTO posts_photos_albums (user_id, user_type, in_event, event_id, title, privacy) VALUES (%s, %s, %s, %s, 'Timeline Photos', 'custom')", secure($post['user_id'], 'int'), secure($post['user_type']), secure($post['in_event']), secure($post['event_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                        $_event['event_album_timeline'] = $db->insert_id;
                        /* update event */
                        $db->query(sprintf("UPDATE events SET event_album_timeline = %s WHERE event_id = %s", secure($_event['event_album_timeline'], 'int'), secure($_event['event_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                    }
                    $album_id = $_event['event_album_timeline'];
                } else {
                    /* check for timeline album */
                    if(!$this->_data['user_album_timeline']) {
                        /* create new timeline album (public by default) */
                        $db->query(sprintf("INSERT INTO posts_photos_albums (user_id, user_type, title) VALUES (%s, 'user', 'Timeline Photos')", secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                        $this->_data['user_album_timeline'] = $db->insert_id;
                        /* update user */
                        $db->query(sprintf("UPDATE users SET user_album_timeline = %s WHERE user_id = %s", secure($this->_data['user_album_timeline'], 'int'), secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                    }
                    $album_id = $this->_data['user_album_timeline'];
                }
                foreach ($args['photos'] as $photo) {
                    $db->query(sprintf("INSERT INTO posts_photos (post_id, album_id, source) VALUES (%s, %s, %s)", secure($post['post_id'], 'int'), secure($album_id, 'int'), secure($photo) )) or _error(SQL_ERROR_THROWEN);
                    $post_photo['photo_id'] = $db->insert_id;
                    $post_photo['post_id'] = $post['post_id'];
                    $post_photo['source'] = $photo;
                    $post_photo['likes'] = 0;
                    $post_photo['comments'] = 0;
                    $post['photos'][] = $post_photo;
                }
                $post['photos_num'] = count($post['photos']);
                break;

            case 'album':
                /* create new album */
                $db->query(sprintf("INSERT INTO posts_photos_albums (user_id, user_type, in_group, group_id, title, privacy) VALUES (%s, %s, %s, %s, %s, %s)", secure($post['user_id'], 'int'), secure($post['user_type']), secure($post['in_group']), secure($post['group_id'], 'int'), secure($args['album']), secure($post['privacy']) )) or _error(SQL_ERROR_THROWEN);
                $album_id = $db->insert_id;
                foreach ($args['photos'] as $photo) {
                    $db->query(sprintf("INSERT INTO posts_photos (post_id, album_id, source) VALUES (%s, %s, %s)", secure($post['post_id'], 'int'), secure($album_id, 'int'), secure($photo) )) or _error(SQL_ERROR_THROWEN);
                    $post_photo['photo_id'] = $db->insert_id;
                    $post_photo['post_id'] = $post['post_id'];
                    $post_photo['source'] = $photo;
                    $post_photo['likes'] = 0;
                    $post_photo['comments'] = 0;
                    $post['photos'][] = $post_photo;
                }
                $post['album']['album_id'] = $album_id;
                $post['album']['title'] = $args['album'];
                $post['photos_num'] = count($post['photos']);
                /* get album path */
                if($post['in_group']) {
                    $post['album']['path'] = 'groups/'.$_group['group_name'];
                } elseif ($post['in_event']) {
                    $post['album']['path'] = 'events/'.$_event['event_id'];
                } elseif ($post['user_type'] == "user") {
                    $post['album']['path'] = $this->_data['user_name'];
                } elseif ($post['user_type'] == "page") {
                    $post['album']['path'] = 'pages/'.$_page['page_name'];
                }
                break;
        }

        /* post mention notifications */
        $this->post_mentions($args['message'], $post['post_id']);

        /* post wall notifications */
        if($post['in_wall']) {
            $this->post_notification_for_social( array('to_user_id'=>$post['wall_id'], 'action'=>'wall', 'node_type'=>'post', 'node_url'=>$post['post_id']) );
        }

        /* post in page notifications */
        if($notify_from_page) {
            $strSql = sprintf("SELECT user_id FROM pages_likes WHERE page_id = %s", secure($post['user_id'], 'int'));
            $get_users = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
            if($get_users->num_rows > 0) {
                $userIds = array();
                while($get_user = $get_users->fetch_assoc()) {
                    $userIds[] = $get_user['user_id'];
                }
                $this->post_notifications($userIds, NOTIFICATION_POST_IN_PAGE, $post['user_type'], $post['post_id'], $post['post_author_name'], $post['post_author_picture'] );
            }
        }
        /* post in group notifications */
        if($notify_from_group) {
            $strSql = sprintf("SELECT user_id FROM groups_members WHERE group_id = %s AND approved = '1'", secure($post['group_id'], 'int'));
            $get_users = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
            if($get_users->num_rows > 0) {
                $userIds = array();
                while($get_user = $get_users->fetch_assoc()) {
                    $userIds[] = $get_user['user_id'];
                }
                $this->post_notifications($userIds, NOTIFICATION_POST_IN_GROUP, $post['user_type'], $post['post_id'], $this->_data['user_fullname'], $post['group_title']);
            }
        }

//        /* parse text */
//        $post['text_plain'] = htmlentities($post['text'], ENT_QUOTES, 'utf-8');
//        $post['text'] = $this->_parse($post['text_plain']);
//
//        /* user can manage the post */
//        $post['manage_post'] = true;

        $post = $this->get_post($post['post_id'], false, true);

        // return
        return $post;
    }


    /**
     * scraper
     *
     * @param string $url
     * @return array
     */
    public function scraper($url) {
        $url_parsed = parse_url($url);
        if(!isset($url_parsed["scheme"]) ) {
            $url = "http://".$url;
        }
        $url = ger_origenal_url($url);
        $advanced = true;
        if($advanced) {
            require_once(ABSPATH.'includes/libs/Embed/v3/autoloader.php');
            $embed = Embed\Embed::create($url);
        } else {
            require_once(ABSPATH.'includes/libs/Embed/v1/autoloader.php');
            $config = [
                'image' => [
                    'class' => 'Embed\\ImageInfo\\Sngine'
                ]
            ];
            $embed = Embed\Embed::create($url, $config);
        }
        if($embed) {
            $return = array();
            $return['source_url'] = $url;
            $return['source_title'] = $embed->title;
            $return['source_text'] = $embed->description;
            $return['source_type'] = $embed->type;
            if($return['source_type'] == "link") {
                $return['source_host'] = $url_parsed['host'];
                $return['source_thumbnail'] = $embed->image;
            } else {
                $return['source_html'] = $embed->code;
                $return['source_provider'] = $embed->providerName;
            }
            return $return;
        } else {
            return false;
        }
    }


    /**
     * parse
     *
     * @param string $text
     * @param boolean $nl2br
     * @param boolean $mention
     * @return string
     */
    private function _parse($text, $nl2br = true, $mention = true) {
        /* decode urls */
        $text = decode_urls($text);
        /* decode emoji */
        $text = $this->decode_emoji($text);
        /* decode stickers */
        /* CI - comment decode stickers */
        //$text = $this->decode_stickers($text);
        /* decode #hashtag */
        $text = decode_hashtag($text);
        /* decode @mention */
        if($mention) {
            $text = decode_mention($text);
        }
        /* censored words */
        $text = censored_words($text);
        /* nl2br */
        if($nl2br) {
            $text = nl2br($text);
        }
        return $text;
    }

    /**
     * parse event
     *
     * @param string $text
     * @param boolean $nl2br
     * @param boolean $mention
     * @return string
     */
    private function _parse_event($text, $nl2br = true, $mention = true) {
        /* decode urls */
        //$text = decode_urls($text);
        /* decode emoji */
        $text = $this->decode_emoji($text);
        /* decode stickers */
        /* CI - comment decode stickers */
        //$text = $this->decode_stickers($text);
        /* decode #hashtag */
        //$text = decode_hashtag($text);
        /* decode @mention */
        if($mention) {
            $text = decode_mention($text);
        }
        /* censored words */
        $text = censored_words($text);
        /* nl2br */
//        if($nl2br) {
//            $text = nl2br($text);
//        }
        $text = html_entity_decode($text, ENT_QUOTES);
        return $text;
    }


    /* ------------------------------- */
    /* Posts */
    /* ------------------------------- */

    /**
     * get_posts
     *
     * @param array $args
     * @return array
     */
    public function get_posts($args = array()) {
        global $db, $system;
        /* initialize vars */
        $posts = array();
        /* validate arguments */
        $get = !isset($args['get']) ? 'newsfeed' : $args['get'];
        $filter = !isset($args['filter']) ? 'all' : $args['filter'];
        $valid['filter'] = array('all', '', 'photos', 'video', 'audio', 'file', 'poll', 'product', 'article', 'map');
        if(!in_array($filter, $valid['filter'])) {
            _error(400);
        }
        $last_post_id = !isset($args['last_post_id']) ? null : $args['last_post_id'];
        if(isset($args['last_post']) && !is_numeric($args['last_post'])) {
            _error(400);
        }
        $offset = !isset($args['offset']) ? 0 : $args['offset'];
        $offset *= $system['max_results'];
        if(isset($args['query'])) {
            if(is_empty($args['query'])) {
                return $posts;
            } else {
                $query = secure($args['query'], 'search', false);
            }
        }
        $order_query = "ORDER BY posts.post_id DESC";
        $where_query = "";
        /* get postsc */
        switch ($get) {
            case 'newsfeed':
                if(!$this->_logged_in && $query) {
                    $where_query .= "WHERE (";
                    $where_query .= "(posts.text LIKE $query)";
                    /* get only public posts [except: group posts & event posts & wall posts] */
                    $where_query .= " AND (posts.in_group = '0' AND posts.in_event = '0' AND posts.in_wall = '0' AND posts.privacy = 'public')";
                    $where_query .= ")";
                } else {
                    /* get viewer user's newsfeed */
                    $where_query .= "WHERE (";
                    /* get viewer posts */
                    $me = $this->_data['user_id'];
                    $where_query .= "(posts.user_id = $me AND posts.user_type = 'user')";
                    /* get posts from friends still followed */
                    $friends_ids = array_intersect($this->_data['friends_ids'], $this->_data['followings_ids']);
                    if($friends_ids) {
                        $friends_list = implode(',',$friends_ids);
                        /* viewer friends posts -> authors */
                        $where_query .= " OR (posts.user_id IN ($friends_list) AND posts.user_type = 'user' AND posts.privacy = 'friends' AND posts.in_group = '0')";
                        /* viewer friends posts -> their wall posts */
                        $where_query .= " OR (posts.in_wall = '1' AND posts.wall_id IN ($friends_list) AND posts.user_type = 'user' AND posts.privacy = 'friends')";
                    }
                    /* get posts from followings */
                    if($this->_data['followings_ids']) {
                        $followings_list = implode(',',$this->_data['followings_ids']);
                        /* viewer followings posts -> authors */
                        $where_query .= " OR (posts.user_id IN ($followings_list) AND posts.user_type = 'user' AND posts.privacy = 'public' AND posts.in_group = '0')";
                        /* viewer followings posts -> their wall posts */
                        $where_query .= " OR (posts.in_wall = '1' AND posts.wall_id IN ($followings_list) AND posts.user_type = 'user' AND posts.privacy = 'public')";
                    }
                    /* get pages posts */
                    $pages_ids = $this->get_pages_ids();
                    if($pages_ids) {
                        $pages_list = implode(',',$pages_ids);
                        $where_query .= " OR (posts.user_id IN ($pages_list) AND posts.user_type = 'page')";
                    }
                    /* get groups (approved only) posts & exclude the viewer posts */
                    $groups_ids = $this->get_groups_ids(true);
                    if($groups_ids) {
                        $groups_list = implode(',',$groups_ids);
                        $where_query .= " OR (posts.group_id IN ($groups_list) AND posts.in_group = '1' AND posts.user_id != $me)";
                    }
                    /* get events posts & exclude the viewer posts */
                    /* CI - comment event*/
                    /*$events_ids = $this->get_events_ids();
                    if($events_ids) {
                        $events_list = implode(',',$events_ids);
                        $where_query .= " OR (posts.event_id IN ($events_list) AND posts.in_event = '1' AND posts.user_id != $me)";
                    }*/
                    $where_query .= ")";
                    if($query) {
                        $where_query .= " AND (posts.text LIKE $query)";
                    }
                }
                break;

            case 'posts_profile':
                if(isset($args['id']) && !is_numeric($args['id'])) {
                    _error(400);
                }
                $id = $args['id'];
                /* get target user's posts */
                /* check if there is a viewer user */
                if($this->_logged_in) {
                    /* check if the target user is the viewer */
                    if($id == $this->_data['user_id']) {
                        /* get all posts */
                        $where_query .= "WHERE (";
                        /* get all target posts */
                        $where_query .= "(posts.user_id = $id AND posts.user_type = 'user')";
                        /* get taget wall posts */
                        $where_query .= " OR (posts.wall_id = $id AND posts.in_wall = '1')";
                        $where_query .= ")";
                    } else {
                        /* check if the viewer & the target user are friends */
                        if(in_array($id, $this->_data['friends_ids'])) {
                            $where_query .= "WHERE (";
                            /* get all target posts [except: group posts] */
                            $where_query .= "(posts.user_id = $id AND posts.user_type = 'user' AND posts.in_group = '0' AND posts.privacy != 'me' )";
                            /* get taget wall posts */
                            $where_query .= " OR (posts.wall_id = $id AND posts.in_wall = '1')";
                            $where_query .= ")";
                        } else {
                            /* get only public posts [except: wall posts & group posts] */
                            $where_query .= "WHERE (posts.user_id = $id AND posts.user_type = 'user' AND posts.in_group = '0' AND posts.in_wall = '0' AND posts.privacy = 'public')";
                        }
                    }
                } else {
                    /* get only public posts [except: wall posts & group posts] */
                    $where_query .= "WHERE (posts.user_id = $id AND posts.user_type = 'user' AND posts.in_group = '0' AND posts.in_wall = '0' AND posts.privacy = 'public')";
                }
                break;

            case 'posts_page':
                if(isset($args['id']) && !is_numeric($args['id'])) {
                    _error(400);
                }
                $id = $args['id'];
                $where_query .= "WHERE (posts.user_id = $id AND posts.user_type = 'page')";
                break;

            case 'posts_group':
                if(isset($args['id']) && !is_numeric($args['id'])) {
                    _error(400);
                }
                $id = $args['id'];
                $where_query .= "WHERE (posts.group_id = $id AND posts.in_group = '1')";
                break;

            case 'posts_event':
                if(isset($args['id']) && !is_numeric($args['id'])) {
                    _error(400);
                }
                $id = $args['id'];
                $where_query .= "WHERE (posts.event_id = $id AND posts.in_event = '1')";
                break;

            case 'saved':
                $id = $this->_data['user_id'];
                $where_query .= "INNER JOIN posts_saved ON posts.post_id = posts_saved.post_id WHERE (posts_saved.user_id = $id)";
                $order_query = "ORDER BY posts_saved.time DESC";
                break;

            default:
                _error(400);
                break;
        }
        /* get his hidden posts to exclude from newsfeed */
        $hidden_posts = $this->_get_hidden_posts($this->_data['user_id']);
        if(count($hidden_posts) > 0) {
            $hidden_posts_list = implode(',',$hidden_posts);
            $where_query .= " AND (posts.post_id NOT IN ($hidden_posts_list))";
        }
        /* filter posts */
        if($filter != "all") {
            $where_query .= " AND (posts.post_type = '$filter')";
        }
        /* get posts */
        if($last_post_id != null && $get != 'saved') {
            $get_posts = $db->query(sprintf("SELECT * FROM (SELECT posts.post_id FROM posts ".$where_query.") posts WHERE posts.post_id > %s ORDER BY posts.post_id DESC", secure($last_post_id, 'int') )) or _error(SQL_ERROR_THROWEN);
        } else {
            $get_posts = $db->query(sprintf("SELECT posts.post_id FROM posts ".$where_query." ".$order_query." LIMIT %s, %s", secure($offset, 'int', false), secure($system['max_results'], 'int', false) )) or _error(SQL_ERROR_THROWEN);
        }
        if($get_posts->num_rows > 0) {
            while($post = $get_posts->fetch_assoc()) {
                $post = $this->get_post($post['post_id'], true, true); /* $full_details = true, $pass_privacy_check = true */
                if($post) {
                    $posts[] = $post;
                }
            }
        }
        return $posts;
    }


    /**
     * get_post
     *
     * @param integer $post_id
     * @param boolean $full_details
     * @param boolean $pass_privacy_check
     * @return array
     */
    public function get_post($post_id, $full_details = true, $pass_privacy_check = false) {
        global $db, $system;

        $post = $this->_check_post($post_id, $pass_privacy_check);
        if(!$post) {
            return false;
        }

        /* parse text */
        $post['text_plain'] = $post['text'];
        if($post['post_type'] == 'ci_event') {
            if(isset($system['is_mobile']) && $system['is_mobile']) {
                //$post['text'] = html_entity_decode($post['text_plain'], ENT_QUOTES);
                //$code = html_entity_decode($post['text_plain'], ENT_QUOTES);
                //$code = str_replace("\r\n",'', $code);
                $post['text'] = $this->_parse($post['text_plain']);
                $post['text'] = addMaxWidthForImgTag($post['text']);
            } else {
                $post['text'] = $this->_parse_event($post['text_plain']);
            }
        } else {
            $post['text'] = $this->_parse($post['text_plain']);
        }

        /* og-meta tags */
        $post['og_title'] = $post['post_author_name'];
        $post['og_title'] .= (!is_empty($post['text_plain']))? " - ".$post['text_plain'] : "";
        $post['og_description'] = $post['text_plain'];

        /* post type */
        if($post['post_type'] == 'album' || $post['post_type'] == 'photos' || $post['post_type'] == 'profile_picture' || $post['post_type'] == 'profile_cover' || $post['post_type'] == 'page_picture' || $post['post_type'] == 'page_cover' || $post['post_type'] == 'group_picture' || $post['post_type'] == 'group_cover' || $post['post_type'] == 'event_cover') {
            /* get photos */
            $get_photos = $db->query(sprintf("SELECT * FROM posts_photos WHERE post_id = %s ORDER BY photo_id DESC", secure($post['post_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
            $post['photos_num'] = $get_photos->num_rows;
            /* check if photos has been deleted */
            if($post['photos_num'] == 0) {
                return false;
            }
            while($post_photo = $get_photos->fetch_assoc()) {
                if ($system['is_mobile'] && !is_empty( $post_photo['source'])) {
                    $post_photo['source'] = $system['system_uploads'] . '/' . $post_photo['source'];
                }
                $post['photos'][] = $post_photo;
            }
            if($post['post_type'] == 'album') {
                $post['album'] = $this->get_album($post['photos'][0]['album_id'], false);
                if(!$post['album']) {
                    return false;
                }
                /* og-meta tags */
                $post['og_title'] = $post['album']['title'];
                $post['og_image'] = $post['album']['cover'];
            } else {
                /* og-meta tags */
                $post['og_image'] = $system['system_uploads'].'/'.$post['photos'][0]['source'];
            }

        } elseif ($post['post_type'] == 'media') {
            /* get media */
            $get_media = $db->query(sprintf("SELECT * FROM posts_media WHERE post_id = %s", secure($post['post_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
            /* check if media has been deleted */
            if($get_media->num_rows == 0) {
                return false;
            }
            $post['media'] = $get_media->fetch_assoc();

        } elseif ($post['post_type'] == 'link') {
            /* get link */
            $get_link = $db->query(sprintf("SELECT * FROM posts_links WHERE post_id = %s", secure($post['post_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
            /* check if link has been deleted */
            if($get_link->num_rows == 0) {
                return false;
            }
            $post['link'] = $get_link->fetch_assoc();

        } elseif ($post['post_type'] == 'poll') {
            /* get poll */
            $get_poll = $db->query(sprintf("SELECT * FROM posts_polls WHERE post_id = %s", secure($post['post_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
            /* check if video has been deleted */
            if($get_poll->num_rows == 0) {
                return false;
            }
            $post['poll'] = $get_poll->fetch_assoc();
            /* get poll options */
            $get_poll_options = $db->query(sprintf("SELECT option_id, text FROM posts_polls_options WHERE poll_id = %s", secure($post['poll']['poll_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
            if($get_poll_options->num_rows == 0) {
                return false;
            }
            while($poll_option = $get_poll_options->fetch_assoc()) {
                /* get option votes */
                $get_option_votes = $db->query(sprintf("SELECT * FROM users_polls_options WHERE option_id = %s", secure($poll_option['option_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                $poll_option['votes'] = $get_option_votes->num_rows;
                /* check if viewer voted */
                $poll_option['checked'] = false;
                if($this->_logged_in) {
                    $check = $db->query(sprintf("SELECT * FROM users_polls_options WHERE user_id = %s AND option_id = %s", secure($this->_data['user_id'], 'int'), secure($poll_option['option_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                    if($check->num_rows > 0) {
                        $poll_option['checked'] = true;
                    }
                }
                $post['poll']['options'][] = $poll_option;
            }

        } elseif ($post['post_type'] == 'product') {
            /* get product */
            $get_product = $db->query(sprintf("SELECT * FROM posts_products WHERE post_id = %s", secure($post['post_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
            /* check if link has been deleted */
            if($get_product->num_rows == 0) {
                return false;
            }
            $post['product'] = $get_product->fetch_assoc();
            /* get photos */
            $get_photos = $db->query(sprintf("SELECT * FROM posts_photos WHERE post_id = %s ORDER BY photo_id DESC", secure($post['post_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
            $post['photos_num'] = $get_photos->num_rows;
            /* check if photos has been deleted */
            if($post['photos_num'] > 0) {
                while($post_photo = $get_photos->fetch_assoc()) {
                    $post['photos'][] = $post_photo;
                }
            }

        } elseif ($post['post_type'] == 'article') {
            /* get article */
            $get_article = $db->query(sprintf("SELECT * FROM posts_articles WHERE post_id = %s", secure($post['post_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
            /* check if article has been deleted */
            if($get_article->num_rows == 0) {
                return false;
            }
            $post['article'] = $get_article->fetch_assoc();
            $post['article']['parsed_cover'] = $this->get_picture($post['article']['cover'], 'article');
            $post['article']['title_url'] = get_url_text($post['article']['title']);
            $post['article']['parsed_text'] = htmlspecialchars_decode($post['article']['text'], ENT_QUOTES);
            $post['article']['text_snippet'] = get_snippet_text($post['article']['text']);
            $tags = (!is_empty($post['article']['tags']))? explode(',', $post['article']['tags']): array();
            $post['article']['parsed_tags'] = array_map('get_tag', $tags);

        } elseif ($post['post_type'] == 'video') {
            /* get video */
            $get_video = $db->query(sprintf("SELECT * FROM posts_videos WHERE post_id = %s", secure($post['post_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
            /* check if video has been deleted */
            if($get_video->num_rows == 0) {
                return false;
            }
            $post['video'] = $get_video->fetch_assoc();
            if ($system['is_mobile'] && !is_empty($post['video']['source'])) {
                $post['video']['source'] = $system['system_uploads'] . '/' . $post['video']['source'];
            }

        } elseif ($post['post_type'] == 'audio') {
            /* get audio */
            $get_audio = $db->query(sprintf("SELECT * FROM posts_audios WHERE post_id = %s", secure($post['post_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
            /* check if audio has been deleted */
            if($get_audio->num_rows == 0) {
                return false;
            }
            $post['audio'] = $get_audio->fetch_assoc();
            if ($system['is_mobile'] && !is_empty($post['audio']['source'])) {
                $post['audio']['source'] = $system['system_uploads'] . '/' . $post['audio']['source'];
            }

        } elseif ($post['post_type'] == 'file') {
            /* get file */
            $get_file = $db->query(sprintf("SELECT * FROM posts_files WHERE post_id = %s", secure($post['post_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
            /* check if file has been deleted */
            if($get_file->num_rows == 0) {
                return false;
            }
            $post['file'] = $get_file->fetch_assoc();
            if ($system['is_mobile'] && !is_empty($post['file']['source'])) {
                $post['file']['source'] = $system['system_uploads'] . '/' . $post['file']['source'];
            }

        } elseif ($post['post_type'] == 'shared') {
            /* get origin post */
            $post['origin'] = $this->get_post($post['origin_id'], false);
            /* check if origin post has been deleted */
            if(!$post['origin']) {
                return false;
            }
        }

        /* post feeling */
        if(!is_empty($post['feeling_action']) && !is_empty($post['feeling_value'])) {
            if($post['feeling_action'] != "Feeling") {
                $_feeling_icon = get_feeling_icon($post['feeling_action'], get_feelings());
            } else {
                $_feeling_icon = get_feeling_icon($post['feeling_value'], get_feelings_types());
            }
            if($_feeling_icon) {
                $post['feeling_icon'] = $_feeling_icon;
            }
        }

        /* check if get full post details */
        if($full_details) {
            /* get post comments */
            if($post['comments'] > 0) {
                $post['post_comments'] = $this->get_comments($post['post_id'], 0, true, true, $post);
            }
        }

        return $post;
    }


    /**
     * get_boosted_post
     *
     * @return array
     */
    public function get_boosted_post() {
        global $db, $system;
        $get_random_post = $db->query("SELECT post_id FROM posts WHERE boosted = '1' ORDER BY RAND() LIMIT 1") or _error(SQL_ERROR_THROWEN);
        if($get_random_post->num_rows == 0) {
            return false;
        }
        $random_post = $get_random_post->fetch_assoc();
        return $this->get_post($random_post['post_id'], true, true);
    }


    /**
     * who_likes
     *
     * @param array $args
     * @return array
     */
    public function who_likes($args = []) {
        global $db, $system;
        /* initialize arguments */
        $post_id = !isset($args['post_id']) ? null : $args['post_id'];
        $photo_id = !isset($args['photo_id']) ? null : $args['photo_id'];
        $comment_id = !isset($args['comment_id']) ? null : $args['comment_id'];
        $offset = !isset($args['offset']) ? 0 : $args['offset'];
        /* initialize vars */
        $users = array();
        $offset *= $system['max_results'];
        if($post_id != null) {
            /* get users who like the post */
            $get_users = $db->query(sprintf('SELECT users.user_id, users.user_name, users.user_fullname, users.user_firstname, users.user_lastname, users.user_gender, users.user_picture FROM posts_likes INNER JOIN users ON (posts_likes.user_id = users.user_id) WHERE posts_likes.post_id = %s LIMIT %s, %s', secure($post_id, 'int'), secure($offset, 'int', false), secure($system['max_results'], 'int', false) )) or _error(SQL_ERROR_THROWEN);
        } elseif ($photo_id != null) {
            /* get users who like the photo */
            $get_users = $db->query(sprintf('SELECT users.user_id, users.user_name, users.user_fullname, users.user_firstname, users.user_lastname, users.user_gender, users.user_picture FROM posts_photos_likes INNER JOIN users ON (posts_photos_likes.user_id = users.user_id) WHERE posts_photos_likes.photo_id = %s LIMIT %s, %s', secure($photo_id, 'int'), secure($offset, 'int', false), secure($system['max_results'], 'int', false) )) or _error(SQL_ERROR_THROWEN);
        } else {
            /* get users who like the comment */
            $get_users = $db->query(sprintf('SELECT users.user_id, users.user_name, users.user_fullname, users.user_firstname, users.user_lastname, users.user_gender, users.user_picture FROM posts_comments_likes INNER JOIN users ON (posts_comments_likes.user_id = users.user_id) WHERE posts_comments_likes.comment_id = %s LIMIT %s, %s', secure($comment_id, 'int'), secure($offset, 'int', false), secure($system['max_results'], 'int', false) )) or _error(SQL_ERROR_THROWEN);
        }
        if($get_users->num_rows > 0) {
            while($_user = $get_users->fetch_assoc()) {
                $_user['user_picture'] = $this->get_picture($_user['user_picture'], $_user['user_gender']);
                /* get the connection between the viewer & the target */
                $_user['connection'] = $this->connection($_user['user_id']);
                /* get mutual friends count */
                $_user['mutual_friends_count'] = $this->get_mutual_friends_count($_user['user_id']);
                $users[] = $_user;
            }
        }
        return $users;
    }


    /**
     * who_shares
     *
     * @param integer $post_id
     * @param integer $offset
     * @return array
     */
    public function who_shares($post_id, $offset = 0) {
        global $db, $system;
        $posts = array();
        $offset *= $system['max_results'];
        $get_posts = $db->query(sprintf('SELECT posts.post_id FROM posts INNER JOIN users ON posts.user_id = users.user_id WHERE posts.post_type = "shared" AND posts.origin_id = %s LIMIT %s, %s', secure($post_id, 'int'), secure($offset, 'int', false), secure($system['max_results'], 'int', false) )) or _error(SQL_ERROR_THROWEN);
        if($get_posts->num_rows > 0) {
            while($post = $get_posts->fetch_assoc()) {
                $post = $this->get_post($post['post_id']);
                if($post) {
                    $posts[] = $post;
                }
            }
        }
        return $posts;
    }


    /**
     * who_votes
     *
     * @param integer $post_id
     * @param integer $offset
     * @return array
     */
    public function who_votes($option_id, $offset = 0) {
        global $db, $system;
        $voters = array();
        $offset *= $system['max_results'];
        $get_voters = $db->query(sprintf("SELECT users.user_id, users.user_name, users.user_fullname, users.user_firstname, users.user_lastname, users.user_picture, users.user_gender FROM posts_polls_options_users INNER JOIN users ON posts_polls_options_users.user_id = users.user_id WHERE option_id = %s LIMIT %s, %s", secure($option_id, 'int'), secure($offset, 'int', false), secure($system['max_results'], 'int', false) )) or _error(SQL_ERROR_THROWEN);
        while($voter = $get_voters->fetch_assoc()) {
            $voter['user_picture'] = $this->get_picture($voter['user_picture'], $voter['user_gender']);
            /* get the connection between the viewer & the target */
            $voter['connection'] = $this->connection($voter['user_id']);
            $voters[] = $voter;
        }
        return $voters;
    }


    /**
     * _get_hidden_posts
     *
     * @param integer $user_id
     * @return array
     */
    private function _get_hidden_posts($user_id) {
        global $db;
        $hidden_posts = array();
        $get_hidden_posts = $db->query(sprintf("SELECT post_id FROM posts_hidden WHERE user_id = %s", secure($user_id, 'int') )) or _error(SQL_ERROR_THROWEN);
        if($get_hidden_posts->num_rows > 0) {
            while($hidden_post = $get_hidden_posts->fetch_assoc()) {
                $hidden_posts[] = $hidden_post['post_id'];
            }
        }
        return $hidden_posts;
    }


    /**
     * _check_post
     *
     * @param integer $id
     * @param boolean $pass_privacy_check
     * @param boolean $full_details
     * @return array|false
     */
    private function _check_post($id, $pass_privacy_check = false, $full_details = true) {
        global $db, $system;

        /* get post */
        //$get_post = $db->query(sprintf("SELECT posts.*, users.user_name, users.user_firstname, users.user_lastname, users.user_gender, users.user_picture, users.user_picture_id, users.user_cover_id, users.user_verified, users.user_subscribed, users.user_pinned_post, pages.*, groups.*, events.* FROM posts LEFT JOIN users ON posts.user_id = users.user_id AND posts.user_type = 'user' LEFT JOIN pages ON posts.user_id = pages.page_id AND posts.user_type = 'page' LEFT JOIN groups ON posts.in_group = '1' AND posts.group_id = groups.group_id LEFT JOIN events ON posts.in_event = '1' AND posts.event_id = events.event_id WHERE NOT (users.user_name <=> NULL AND pages.page_name <=> NULL) AND posts.post_id = %s", secure($id, 'int') )) or _error(SQL_ERROR_THROWEN);
        $get_post = $db->query(sprintf("SELECT PO.*, U.user_name, U.user_fullname, U.user_firstname, U.user_lastname, U.user_gender, U.user_picture, U.user_cover, U.user_picture_id, U.user_cover_id, U.user_verified, U.user_pinned_post, PA.*, G.*, SC.school_allow_comment, SC.class_allow_comment FROM posts PO
          LEFT JOIN users U ON PO.user_id = U.user_id AND PO.user_type = 'user' 
          LEFT JOIN pages PA ON PO.user_id = PA.page_id AND PO.user_type = 'page' 
          LEFT JOIN groups G ON PO.in_group = '1' AND PO.group_id = G.group_id
          LEFT JOIN ci_school_configuration SC ON PO.school_id > 0 AND PO.school_id = SC.school_id
          WHERE NOT (U.user_name <=> NULL AND PA.page_name <=> NULL) AND PO.post_id = %s", secure($id, 'int') )) or _error(SQL_ERROR_THROWEN);

        if($get_post->num_rows == 0) {
            return false;
        }
        $post = $get_post->fetch_assoc();
        /* check if the page has been deleted */
        if($post['user_type'] == "page" && !$post['page_admin']) {
            return false;
        }

        /* get the author */
        $post['author_id'] = ($post['user_type'] == "page")? $post['page_admin'] : $post['user_id'];
        $post['is_page_admin'] = $this->check_page_adminship($this->_data['user_id'], $post['page_id']);
        $post['is_group_admin'] = $this->check_group_adminship($this->_data['user_id'], $post['group_id']);
        $post['is_event_admin'] = ($this->_logged_in && $this->_data['user_id'] == $post['event_admin'])? true : false;

        /* check the post author type */
        if($post['user_type'] == "user") {
            /* user */
            $post['post_author_picture'] = $this->get_picture($post['user_picture'], $post['user_gender']);
            $post['post_author_url'] = $system['system_url'].'/'.$post['user_name'];
            $post['post_author_name'] = $post['user_fullname'];
            $post['post_author_verified'] = $post['user_verified'];
            $post['pinned'] = ( (!$post['in_group'] && !$post['in_event'] && $post['post_id'] == $post['user_pinned_post'] ) || ($post['in_group'] && $post['post_id'] == $post['group_pinned_post'] ) || ($post['in_event'] && $post['post_id'] == $post['event_pinned_post'] ) )? true : false;
            //Coniu - Begin - build lên giá trị allow_comment
            $post['allow_comment'] = (($post['school_id'] > 0) && $post['in_group'] && $post['post_id'] && $post['class_allow_comment']) || $post['is_group_admin'] || ($post['school_id'] == 0);
        } else {
            /* page */
            $post['post_author_picture'] = $this->get_picture($post['page_picture'], "page");
            $post['post_author_url'] = $system['system_url'].'/pages/'.$post['page_name'];
            $post['post_author_name'] = $post['page_title'];
            $post['post_author_verified'] = $post['page_verified'];
            $post['pinned'] = ($post['post_id'] == $post['page_pinned_post'])? true : false;
            //Coniu - Begin - build lên giá trị allow_comment
            $post['allow_comment'] = (($post['school_id'] > 0) && $post['post_id'] && $post['school_allow_comment']) || $post['is_page_admin'] || ($post['school_id'] == 0);
        }

        /* check if viewer can manage post [Edit|Pin|Delete] */
        $post['manage_post'] = false;
        if($this->_logged_in) {
            /* viewer is (admins|moderators)] */
            if($this->_data['user_group'] < 3) {
                $post['manage_post'] = true;
            }
            /* viewer is the author of post || page admin */
            if($this->_data['user_id'] == $post['author_id']) {
                $post['manage_post'] = true;
            }
            /* viewer is the admin of the page of the post */
            if($post['user_type'] == "page" && $post['is_page_admin']) {
                $post['manage_post'] = true;
            }
            /* viewer is the admin of the group of the post */
            if($post['in_group'] && $post['is_group_admin']) {
                $post['manage_post'] = true;
            }
            /* viewer is the admin of the event of the post */
            if($post['in_event'] && $post['is_event_admin']) {
                $post['manage_post'] = true;
            }
        }

        /* full details */
        if($full_details) {
            /* check if wall post */
            if($post['in_wall']) {
                $get_wall_user = $db->query(sprintf("SELECT user_fullname, user_firstname, user_lastname, user_name FROM users WHERE user_id = %s", secure($post['wall_id'] ,'int') )) or _error(SQL_ERROR_THROWEN);
                if($get_wall_user->num_rows == 0) {
                    return false;
                }
                $wall_user = $get_wall_user->fetch_assoc();
                $post['wall_username'] = $wall_user['user_name'];
                $post['wall_fullname'] = $wall_user['user_fullname'];
            }

            /* check if viewer [liked|saved] this post */
            $post['i_save'] = false;
            $post['i_like'] = false;
            if($this->_logged_in) {
                /* save */
                $check_save = $db->query(sprintf("SELECT * FROM posts_saved WHERE user_id = %s AND post_id = %s", secure($this->_data['user_id'], 'int'), secure($post['post_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                if($check_save->num_rows > 0) {
                    $post['i_save'] = true;
                }
                /* like */
                $check_like = $db->query(sprintf("SELECT * FROM posts_likes WHERE user_id = %s AND post_id = %s", secure($this->_data['user_id'], 'int'), secure($post['post_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                if($check_like->num_rows > 0) {
                    $post['i_like'] = true;
                }
            }
        }

        /* check privacy */
        /* if post in group & (the group is public || the viewer approved member of this group) => pass privacy check */
        if( $post['in_group'] && ($post['group_privacy'] == 'public' || $this->check_group_membership($this->_data['user_id'], $post['group_id']) == 'approved') ) {
            $pass_privacy_check = true;
        }
        /* if post in event & (the event is public || the viewer member of this event) => pass privacy check */
        if( $post['in_event'] && ($post['event_privacy'] == 'public' || $this->check_event_membership($this->_data['user_id'], $post['event_id']))) {
            $pass_privacy_check = true;
        }
        if($pass_privacy_check || $this->_check_privacy($post['privacy'], $post['author_id'])) {
            return $post;
        }
        return false;
    }

    /**
     * check_post
     *
     * @param integer $id
     * @param boolean $pass_privacy_check
     * @param boolean $full_details
     * @return array|false
     */
    public function check_post($id, $pass_privacy_check = false, $full_details = true) {
        $post = $this->_check_post($id, $pass_privacy_check, $full_details);
        return $post;
    }


    /**
     * _check_privacy
     *
     * @param string $privacy
     * @param integer $author_id
     * @return boolean
     */
    private function _check_privacy($privacy, $author_id) {
        if($privacy == 'public') {
            return true;
        }
        if($this->_logged_in) {
            /* check if the viewer is the system admin */
            if($this->_data['user_group'] < 3) {
                return true;
            }
            /* check if the viewer is the target */
            if($author_id == $this->_data['user_id']) {
                return true;
            }
            /* check if the viewer and the target are friends */
            if($privacy == 'friends' && in_array($author_id, $this->_data['friends_ids'])) {
                return true;
            }
        }
        return false;
    }


    /* ------------------------------- */
    /* Comments & Replies */
    /* ------------------------------- */

    /**
     * get_comments
     *
     * @param integer $node_id
     * @param integer $offset
     * @param boolean $is_post
     * @param boolean $pass_privacy_check
     * @param array $post
     * @return array
     */
    public function get_comments($node_id, $offset = 0, $is_post = true, $pass_privacy_check = true, $post = array()) {
        global $db, $system;
        $comments = array();
        $offset *= $system['min_results'];
        /* get comments */
        if($is_post) {
            /* get post comments */
            if(!$pass_privacy_check) {
                /* (check|get) post */
                $post = $this->_check_post($node_id, false);
                if(!$post) {
                    return false;
                }
            }
            /* get post comments */
            $get_comments = $db->query(sprintf("SELECT * FROM (SELECT posts_comments.*, users.user_name, users.user_fullname, users.user_firstname, users.user_lastname, users.user_gender, users.user_picture, users.user_verified, pages.* FROM posts_comments LEFT JOIN users ON posts_comments.user_id = users.user_id AND posts_comments.user_type = 'user' LEFT JOIN pages ON posts_comments.user_id = pages.page_id AND posts_comments.user_type = 'page' WHERE NOT (users.user_name <=> NULL AND pages.page_name <=> NULL) AND posts_comments.node_type = 'post' AND posts_comments.node_id = %s ORDER BY posts_comments.comment_id DESC LIMIT %s, %s) comments ORDER BY comments.comment_id ASC", secure($node_id, 'int'), secure($offset, 'int', false), secure($system['min_results'], 'int', false) )) or _error(SQL_ERROR_THROWEN);
        } else {
            /* get photo comments */
            /* check privacy */
            if(!$pass_privacy_check) {
                /* (check|get) photo */
                $photo = $this->get_photo($node_id);
                if(!$photo) {
                    _error(403);
                }
                $post = $photo['post'];
            }
            /* get photo comments */
            $get_comments = $db->query(sprintf("SELECT * FROM (SELECT posts_comments.*, users.user_name, users.user_fullname, users.user_firstname, users.user_lastname, users.user_gender, users.user_picture, users.user_verified, pages.* FROM posts_comments LEFT JOIN users ON posts_comments.user_id = users.user_id AND posts_comments.user_type = 'user' LEFT JOIN pages ON posts_comments.user_id = pages.page_id AND posts_comments.user_type = 'page' WHERE NOT (users.user_name <=> NULL AND pages.page_name <=> NULL) AND posts_comments.node_type = 'photo' AND posts_comments.node_id = %s ORDER BY posts_comments.comment_id DESC LIMIT %s, %s) comments ORDER BY comments.comment_id ASC", secure($node_id, 'int'), secure($offset, 'int', false), secure($system['min_results'], 'int', false) )) or _error(SQL_ERROR_THROWEN);
        }
        if($get_comments->num_rows == 0) {
            return $comments;
        }
        while($comment = $get_comments->fetch_assoc()) {
            /* get replies */
            if($comment['replies'] > 0) {
                $comment['comment_replies'] = $this->get_replies($comment['comment_id']);
            }
            /* parse text */
            $comment['text_plain'] = $comment['text'];
            $comment['text'] = $this->_parse($comment['text']);
            /* get the comment author */
            if($comment['user_type'] == "user") {
                /* user type */
                $comment['author_id'] = $comment['user_id'];
                $comment['author_picture'] = $this->get_picture($comment['user_picture'], $comment['user_gender']);
                $comment['author_url'] = $system['system_url'].'/'.$comment['user_name'];
                $comment['author_name'] = $comment['user_fullname'];
                $comment['author_verified'] = $comment['user_verified'];
            } else {
                /* page type */
                $comment['author_id'] = $comment['page_admin'];
                $comment['author_picture'] = $this->get_picture($comment['page_picture'], "page");
                $comment['author_url'] = $system['system_url'].'/pages/'.$comment['page_name'];
                $comment['author_name'] = $comment['page_title'];
                $comment['author_verified'] = $comment['page_verified'];
            }
            /* check if viewer user likes this comment */
            if($this->_logged_in && $comment['likes'] > 0) {
                $check_like = $db->query(sprintf("SELECT * FROM posts_comments_likes WHERE user_id = %s AND comment_id = %s", secure($this->_data['user_id'], 'int'), secure($comment['comment_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                $comment['i_like'] = ($check_like->num_rows > 0)? true: false;
            }
            /* check if viewer can manage comment [Edit|Delete] */
            $comment['edit_comment'] = false;
            $comment['delete_comment'] = false;
            if($this->_logged_in) {
                /* viewer is (admins|moderators)] */
                if($this->_data['user_group'] < 3) {
                    $comment['edit_comment'] = true;
                    $comment['delete_comment'] = true;
                }
                /* viewer is the author of comment */
                if($this->_data['user_id'] == $comment['author_id']) {
                    $comment['edit_comment'] = true;
                    $comment['delete_comment'] = true;
                }
                /* viewer is the author of post || page admin */
                if($this->_data['user_id'] == $post['author_id']) {
                    $comment['delete_comment'] = true;
                }
                /* viewer is the admin of the group of the post */
                if($post['in_group'] && $post['is_group_admin']) {
                    $comment['delete_comment'] = true;
                }
                /* viewer is the admin of the event of the post */
                if($post['in_group'] && $post['is_event_admin']) {
                    $comment['delete_comment'] = true;
                }
            }
            $comments[] = $comment;
        }
        return $comments;
    }


    /**
     * get_replies
     *
     * @param integer $comment_id
     * @param integer $offset
     * @return array
     */
    public function get_replies($comment_id, $offset = 0, $pass_check = true) {
        global $db, $system;
        $replies = array();
        $offset *= $system['min_results'];
        if(!$pass_check) {
            $comment = $this->get_comment($comment_id);
            if(!$comment) {
                _error(403);
            }
        }
        /* get replies */
        $get_replies = $db->query(sprintf("SELECT * FROM (SELECT posts_comments.*, users.user_name, users.user_fullname, users.user_firstname, users.user_lastname, users.user_gender, users.user_picture, users.user_verified, pages.* FROM posts_comments LEFT JOIN users ON posts_comments.user_id = users.user_id AND posts_comments.user_type = 'user' LEFT JOIN pages ON posts_comments.user_id = pages.page_id AND posts_comments.user_type = 'page' WHERE NOT (users.user_name <=> NULL AND pages.page_name <=> NULL) AND posts_comments.node_type = 'comment' AND posts_comments.node_id = %s ORDER BY posts_comments.comment_id DESC LIMIT %s, %s) comments ORDER BY comments.comment_id ASC", secure($comment_id, 'int'), secure($offset, 'int', false), secure($system['min_results'], 'int', false) )) or _error(SQL_ERROR_THROWEN);
        if($get_replies->num_rows == 0) {
            return $replies;
        }
        while($reply = $get_replies->fetch_assoc()) {
            /* parse text */
            $reply['text_plain'] = $reply['text'];
            $reply['text'] = $this->_parse($reply['text']);
            /* get the reply author */
            if($reply['user_type'] == "user") {
                /* user type */
                $reply['author_id'] = $reply['user_id'];
                $reply['author_picture'] = $this->get_picture($reply['user_picture'], $reply['user_gender']);
                $reply['author_url'] = $system['system_url'].'/'.$reply['user_name'];
                $reply['author_user_name'] = $reply['user_name'];
                $reply['author_name'] = $reply['user_fullname'];
                $reply['author_verified'] = $reply['user_verified'];
            } else {
                /* page type */
                $reply['author_id'] = $reply['page_admin'];
                $reply['author_picture'] = $this->get_picture($reply['page_picture'], "page");
                $reply['author_url'] = $system['system_url'].'/pages/'.$reply['page_name'];
                $reply['author_name'] = $reply['page_title'];
                $reply['author_verified'] = $reply['page_verified'];
            }
            /* check if viewer user likes this reply */
            if($this->_logged_in && $reply['likes'] > 0) {
                $check_like = $db->query(sprintf("SELECT * FROM posts_comments_likes WHERE user_id = %s AND comment_id = %s", secure($this->_data['user_id'], 'int'), secure($reply['comment_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                $reply['i_like'] = ($check_like->num_rows > 0)? true: false;
            }
            /* check if viewer can manage comment [Edit|Delete] */
            $reply['edit_comment'] = false;
            $reply['delete_comment'] = false;
            if($this->_logged_in) {
                /* viewer is (admins|moderators)] */
                if($this->_data['user_group'] < 3) {
                    $reply['edit_comment'] = true;
                    $reply['delete_comment'] = true;
                }
                /* viewer is the author of comment */
                if($this->_data['user_id'] == $reply['author_id']) {
                    $reply['edit_comment'] = true;
                    $reply['delete_comment'] = true;
                }
            }
            $replies[] = $reply;
        }
        return $replies;
    }


    /**
     * comment
     *
     * @param string $handle
     * @param integer $node_id
     * @param string $message
     * @param string $photo
     * @return array
     */
    public function comment($handle, $node_id, $message, $photo) {
        global $db, $system, $date;
        $comment = array();

        /* default */
        $comment['node_id'] = $node_id;
        $comment['node_type'] = $handle;
        $comment['text'] = $message;
        $comment['image'] = $photo;
        $comment['time'] = $date;
        $comment['likes'] = 0;
        $comment['replies'] = 0;

        /* check the handle */
        switch ($handle) {
            case 'post':
                /* (check|get) post */
                $post = $this->_check_post($node_id, false, false);
                if(!$post) {
                    _error(403);
                }
                break;

            case 'photo':
                /* (check|get) photo */
                $photo = $this->get_photo($node_id);
                if(!$photo) {
                    _error(403);
                }
                $post = $photo['post'];
                break;

            case 'comment':
                /* (check|get) comment */
                $parent_comment = $this->get_comment($node_id, false);
                if(!$parent_comment) {
                    //_error(403);
                    throw new Exception(__("You have no permission to do this"));
                }
                $post = $parent_comment['post'];
                break;
        }

        /* check if there is any blocking between the viewer & the target */
        if($this->blocked($post['author_id']) || ($handle == "comment" && $this->blocked($parent_comment['author_id'])) ) {
            //_error(403);
            throw new Exception(__("You have no permission to do this"));
        }

        /* check if the viewer is page admin of the target post */
        if(!$post['is_page_admin']) {
            $comment['user_id'] = $this->_data['user_id'];
            $comment['user_type'] = "user";
            $comment['author_picture'] = $this->_data['user_picture'];
            $comment['author_url'] = $system['system_url'].'/'.$this->_data['user_name'];
            $comment['author_user_name'] = $this->_data['user_name'];
            $comment['author_name'] = $this->_data['user_fullname'];
            $comment['author_verified'] = $this->_data['user_verified'];
        } else {
            $comment['user_id'] = $post['page_id'];
            $comment['user_type'] = "page";
            $comment['author_picture'] = $this->get_picture($post['page_picture'], "page");
            $comment['author_url'] = $system['system_url'].'/pages/'.$post['page_name'];
            $comment['author_name'] = $post['page_title'];
            $comment['author_verified'] = $post['page_verified'];
        }

        /* insert the comment */
        $db->query(sprintf("INSERT INTO posts_comments (node_id, node_type, user_id, user_type, text, image, time) VALUES (%s, %s, %s, %s, %s, %s, %s)", secure($comment['node_id'], 'int'), secure($comment['node_type']), secure($comment['user_id'], 'int'), secure($comment['user_type']), secure($comment['text']), secure($comment['image']), secure($comment['time']) )) or _error(SQL_ERROR_THROWEN);
        $comment['comment_id'] = $db->insert_id;
        /* update (post|photo|comment) (comments|replies) counter */
        switch ($handle) {
            case 'post':
                $db->query(sprintf("UPDATE posts SET comments = comments + 1 WHERE post_id = %s", secure($node_id, 'int') )) or _error(SQL_ERROR_THROWEN);
                break;

            case 'photo':
                $db->query(sprintf("UPDATE posts_photos SET comments = comments + 1 WHERE photo_id = %s", secure($node_id, 'int') )) or _error(SQL_ERROR_THROWEN);
                break;

            case 'comment':
                $db->query(sprintf("UPDATE posts_comments SET replies = replies + 1 WHERE comment_id = %s", secure($node_id, 'int') )) or _error(SQL_ERROR_THROWEN);
                $db->query(sprintf("UPDATE posts SET comments = comments + 1 WHERE post_id = %s", secure($post['post_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                break;
        }

        /* post notification */
        if($handle == "comment") {
            $this->post_notification_for_social( array('to_user_id'=>$parent_comment['author_id'], 'action'=>'reply', 'node_type'=>$parent_comment['node_type'], 'node_url'=>$parent_comment['node_id'], 'notify_id'=>'comment_'.$comment['comment_id']) );
            if($post['author_id'] != $parent_comment['author_id']) {
                $this->post_notification_for_social( array('to_user_id'=>$post['author_id'], 'action'=>'comment', 'node_type'=>$parent_comment['node_type'], 'node_url'=>$parent_comment['node_id'], 'notify_id'=>'comment_'.$comment['comment_id']) );
            }
        } else {
            $this->post_notification_for_social( array('to_user_id'=>$post['author_id'], 'action'=>'comment', 'node_type'=>$handle, 'node_url'=>$node_id, 'notify_id'=>'comment_'.$comment['comment_id']) );
        }

        /* post mention notifications if any */
        if($handle == "comment") {
            $this->post_mentions($comment['text'], $parent_comment['node_id'], "reply_".$parent_comment['node_type'], 'comment_'.$comment['comment_id'], array($post['author_id'], $parent_comment['author_id']));
        } else {
            $this->post_mentions($comment['text'], $node_id, "comment_".$handle, 'comment_'.$comment['comment_id'], array($post['author_id']));
        }

        /* parse text */
        $comment['text_plain'] = htmlentities($comment['text'], ENT_QUOTES, 'utf-8');
        $comment['text'] = $this->_parse($comment['text_plain']);

        /* check if viewer can manage comment [Edit|Delete] */
        $comment['edit_comment'] = true;
        $comment['delete_comment'] = true;

        /* return */
        return $comment;
    }


    /**
     * get_comment
     *
     * @param integer $comment_id
     * @return array|false
     */
    public function get_comment($comment_id, $recursive = true) {
        global $db;
        /* get comment */
        $get_comment = $db->query(sprintf("SELECT posts_comments.*, pages.page_admin FROM posts_comments LEFT JOIN pages ON posts_comments.user_type = 'page' AND posts_comments.user_id = pages.page_id WHERE posts_comments.comment_id = %s", secure($comment_id, 'int') )) or _error(SQL_ERROR_THROWEN);
        if($get_comment->num_rows == 0) {
            return false;
        }
        $comment = $get_comment->fetch_assoc();
        /* check if the page has been deleted */
        if($comment['user_type'] == "page" && !$comment['page_admin']) {
            return false;
        }
        /* get the author */
        $comment['author_id'] = ($comment['user_type'] == "page")? $comment['page_admin'] : $comment['user_id'];
        /* get post */
        switch ($comment['node_type']) {
            case 'post':
                $post = $this->_check_post($comment['node_id'], false, false);
                /* check if the post has been deleted */
                if(!$post) {
                    return false;
                }
                $comment['post'] = $post;
                break;

            case 'photo':
                /* (check|get) photo */
                $photo = $this->get_photo($comment['node_id']);
                if(!$photo) {
                    return false;
                }
                $comment['post'] = $photo['post'];
                break;

            case 'comment':
                if(!$recursive) {
                    return false;
                }
                /* (check|get) comment */
                $parent_comment = $this->get_comment($comment['node_id'], false);
                if(!$parent_comment) {
                    return false;
                }
                $comment['parent_comment'] = $parent_comment;
                $comment['post'] = $parent_comment['post'];
                break;
        }
        /* check if viewer user likes this comment */
        if($this->_logged_in && $comment['likes'] > 0) {
            $check_like = $db->query(sprintf("SELECT * FROM posts_comments_likes WHERE user_id = %s AND comment_id = %s", secure($this->_data['user_id'], 'int'), secure($comment['comment_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
            $comment['i_like'] = ($check_like->num_rows > 0)? true: false;
        }
        return $comment;
    }


    /**
     * delete_comment
     *
     * @param integer $comment_id
     * @return void
     */
    public function delete_comment($comment_id) {
        global $db;
        /* (check|get) comment */
        $comment = $this->get_comment($comment_id);
        if(!$comment) {
            _error(403);
        }
        /* check if viewer can manage comment [Delete] */
        $comment['delete_comment'] = false;
        if($this->_logged_in) {
            /* viewer is (admins|moderators)] */
            if($this->_data['user_group'] < 3) {
                $comment['delete_comment'] = true;
            }
            /* viewer is the author of comment */
            if($this->_data['user_id'] == $comment['author_id']) {
                $comment['delete_comment'] = true;
            }
            /* viewer is the author of post || page admin */
            if($this->_data['user_id'] == $comment['post']['author_id']) {
                $comment['delete_comment'] = true;
            }
            /* viewer is the admin of the group of the post */
            if($comment['post']['in_group'] && $comment['post']['is_group_admin']) {
                $comment['delete_comment'] = true;
            }
            /* viewer is the admin of the event of the post */
            if($comment['post']['in_event'] && $comment['post']['is_event_admin']) {
                $comment['delete_comment'] = true;
            }
        }
        /* delete the comment */
        if($comment['delete_comment']) {
            /* delete the comment */
            $db->query(sprintf("DELETE FROM posts_comments WHERE comment_id = %s", secure($comment_id, 'int') )) or _error(SQL_ERROR_THROWEN);
            /* get comment_count */
            $countComment = $this->get_comments_count($comment_id);
            /* update comments counter */
            switch ($comment['node_type']) {
                case 'post':
                    $db->query(sprintf("UPDATE posts SET comments = IF(comments=0,0,comments- %s) WHERE post_id = %s", secure($countComment, 'int'), secure($comment['node_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                    break;

                case 'photo':
                    $db->query(sprintf("UPDATE posts_photos SET comments = IF(comments=0,0,comments-%s) WHERE photo_id = %s", secure($countComment, 'int'), secure($comment['node_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                    break;

                case 'comment':
                    $db->query(sprintf("UPDATE posts_comments SET replies = IF(replies=0,0,replies-%s) WHERE comment_id = %s", secure($countComment, 'int'), secure($comment['node_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                    break;
            }
        }
    }


    /**
     * edit_comment
     *
     * @param integer $comment_id
     * @param string $message
     * @param string $photo
     * @return array
     */
    public function edit_comment($comment_id, $message, $photo) {
        global $db, $system;
        /* (check|get) comment */
        $comment = $this->get_comment($comment_id);
        if(!$comment) {
            _error(403);
        }
        /* check if viewer can manage comment [Edit] */
        $comment['edit_comment'] = false;
        if($this->_logged_in) {
            /* viewer is (admins|moderators)] */
            if($this->_data['user_group'] < 3) {
                $comment['edit_comment'] = true;
            }
            /* viewer is the author of comment */
            if($this->_data['user_id'] == $comment['author_id']) {
                $comment['edit_comment'] = true;
            }
        }
        if(!$comment['edit_comment']) {
            _error(400);
        }
        /* update comment */
        $comment['text'] = $message;
        $comment['image'] = (!is_empty($comment['image']))? $comment['image'] : $photo;
        $db->query(sprintf("UPDATE posts_comments SET text = %s, image = %s WHERE comment_id = %s", secure($comment['text']), secure($comment['image']), secure($comment_id, 'int') )) or _error(SQL_ERROR_THROWEN);
        /* post mention notifications if any */
        if($comment['node_type'] == "comment") {
            $this->post_mentions($comment['text'], $comment['parent_comment']['node_id'], "reply_".$comment['parent_comment']['node_type'], 'comment_'.$comment['comment_id'], array($comment['post'], $comment['parent_comment']['author_id']));
        } else {
            $this->post_mentions($comment['text'], $comment['node_id'], "comment_".$comment['node_type'], 'comment_'.$comment['comment_id'], array($comment['post']['author_id']));
        }
        /* parse text */
        $comment['text_plain'] = htmlentities($comment['text'], ENT_QUOTES, 'utf-8');
        $comment['text'] = $this->_parse($comment['text_plain']);
        /* return */
        return $comment;
    }


    /**
     * like_comment
     *
     * @param integer $comment_id
     * @return void
     */
    public function like_comment($comment_id) {
        global $db;
        /* (check|get) comment */
        $comment = $this->get_comment($comment_id);
        if(!$comment) {
            _error(403);
        }
        /* check blocking */
        if($this->blocked($comment['author_id'])) {
            _error(403);
        }
        /* like the comment */
        if(!$comment['i_like']) {
            $db->query(sprintf("INSERT INTO posts_comments_likes (user_id, comment_id) VALUES (%s, %s)", secure($this->_data['user_id'], 'int'), secure($comment_id, 'int') )) or _error(SQL_ERROR_THROWEN);
            /* update comment likes counter */
            $db->query(sprintf("UPDATE posts_comments SET likes = likes + 1 WHERE comment_id = %s", secure($comment_id, 'int') )) or _error(SQL_ERROR_THROWEN);
            /* post notification */
            switch ($comment['node_type']) {
                case 'post':
                    $this->post_notification_for_social( array('to_user_id'=>$comment['author_id'], 'action'=>'like', 'node_type'=>'post_comment', 'node_url'=>$comment['node_id'], 'notify_id'=>'comment_'.$comment_id) );
                    break;

                case 'photo':
                    $this->post_notification_for_social( array('to_user_id'=>$comment['author_id'], 'action'=>'like', 'node_type'=>'photo_comment', 'node_url'=>$comment['node_id'], 'notify_id'=>'comment_'.$comment_id) );
                    break;

                case 'comment':
                    $_node_type = ($comment['parent_comment']['node_type'] == "post")? "post_reply": "photo_reply";
                    $_node_url = $comment['parent_comment']['node_id'];
                    $this->post_notification_for_social( array('to_user_id'=>$comment['author_id'], 'action'=>'like', 'node_type'=>$_node_type, 'node_url'=>$_node_url, 'notify_id'=>'comment_'.$comment_id) );
                    break;
            }
        }
    }


    /**
     * unlike_comment
     *
     * @param integer $comment_id
     * @return void
     */
    public function unlike_comment($comment_id) {
        global $db;
        /* (check|get) comment */
        $comment = $this->get_comment($comment_id);
        if(!$comment) {
            _error(403);
        }
        /* check blocking */
        if($this->blocked($comment['author_id'])) {
            _error(403);
        }
        /* unlike the comment */
        if($comment['i_like']) {
            $db->query(sprintf("DELETE FROM posts_comments_likes WHERE user_id = %s AND comment_id = %s", secure($this->_data['user_id'], 'int'), secure($comment_id, 'int') )) or _error(SQL_ERROR_THROWEN);
            /* update comment likes counter */
            $db->query(sprintf("UPDATE posts_comments SET likes = IF(likes=0,0,likes-1) WHERE comment_id = %s", secure($comment_id, 'int') )) or _error(SQL_ERROR_THROWEN);
            /* delete notification */
            switch ($comment['node_type']) {
                case 'post':
                    $this->delete_notification($comment['author_id'], 'like', 'post_comment', $comment['node_id']);
                    break;

                case 'photo':
                    $this->delete_notification($comment['author_id'], 'like', 'photo_comment', $comment['node_id']);
                    break;

                case 'comment':
                    $_node_type = ($comment['parent_comment']['node_type'] == "post")? "post_reply": "photo_reply";
                    $_node_url = $comment['parent_comment']['node_id'];
                    $this->delete_notification($comment['author_id'], 'like', $_node_type, $_node_url);
                    break;
            }
        }
    }


    /**
     * get_comments_count
     *
     * @param integer $comment_id
     * @param integer $offset
     * @return integer
     */
    public function get_comments_count($comment_id) {
        global $db;

        /* get replies */
        $get_replies = $db->query(sprintf("SELECT COUNT(comments.comment_id) as cnt FROM (SELECT posts_comments.comment_id FROM posts_comments 
            LEFT JOIN users ON posts_comments.user_id = users.user_id AND posts_comments.user_type = 'user' 
            LEFT JOIN pages ON posts_comments.user_id = pages.page_id AND posts_comments.user_type = 'page' 
            WHERE NOT (users.user_name <=> NULL AND pages.page_name <=> NULL) AND posts_comments.node_type = 'comment' AND posts_comments.node_id = %s ) comments", secure($comment_id, 'int') )) or _error(SQL_ERROR_THROWEN);

        if($get_replies->num_rows > 0) {
            return $get_replies->fetch_assoc()['cnt'] + 1;
        }
        return 1;

    }



    /* ------------------------------- */
    /* Photos & Albums */
    /* ------------------------------- */

    /**
     * get_photos
     * 
     * @param integer $id
     * @param string $type
     * @param integer $offset
     * @return array
     */
    /* CI - Mobile thêm biến $is_mobile để check và hiển thị source của image */
    public function get_photos($id, $type = 'user', $offset = 0, $pass_check = true) {
        global $db, $system;
        $photos = array();
        switch ($type) {
            case 'album':
                $offset *= $system['max_results_even'];
                if($pass_check) {
                    $get_photos = $db->query(sprintf("SELECT * FROM posts_photos WHERE album_id = %s ORDER BY photo_id DESC LIMIT %s, %s", secure($id, 'int'), secure($offset, 'int', false), secure($system['max_results_even'], 'int', false) )) or _error(SQL_ERROR_THROWEN);
                    if($get_photos->num_rows > 0) {
                        while($photo = $get_photos->fetch_assoc()) {
                        	if ($system['is_mobile'] && !is_empty($photo['source'])) {
                                $photo['source'] = $system['system_uploads'] . '/' . $photo['source'];
                            }
                            $photos[] = $photo;
                        }
                    }
                } else {
                    $album = $this->get_album($id, false);
                    if(!$album) {
                        return $photos;
                    }
                    $get_photos = $db->query(sprintf("SELECT * FROM posts_photos WHERE album_id = %s ORDER BY photo_id DESC LIMIT %s, %s", secure($id, 'int'), secure($offset, 'int', false), secure($system['max_results_even'], 'int', false) )) or _error(SQL_ERROR_THROWEN);
                    if($get_photos->num_rows > 0) {
                        while($photo = $get_photos->fetch_assoc()) {
                            if ($system['is_mobile'] && !is_empty($photo['source'])) {
                                $photo['source'] = $system['system_uploads'] . '/' . $photo['source'];
                            }
                            $photo['manage'] = $album['manage_album'];
                            $photos[] = $photo;
                        }
                    }
                }
                break;
            case 'user':
                $offset *= $system['min_results_even'];
                /* get the target user's privacy */
                $get_privacy = $db->query(sprintf("SELECT user_privacy_photos FROM users WHERE user_id = %s", secure($id, 'int') )) or _error(SQL_ERROR_THROWEN);
                $privacy = $get_privacy->fetch_assoc();
                /* check the target user's privacy  */
                if(!$this->_check_privacy($privacy['user_privacy_photos'], $id)) {
                    return $photos;
                }
                $get_photos = $db->query(sprintf("SELECT posts_photos.photo_id, posts_photos.source, posts.privacy FROM posts_photos INNER JOIN posts ON posts_photos.post_id = posts.post_id WHERE posts.user_id = %s AND user_type = 'user' ORDER BY posts_photos.photo_id DESC LIMIT %s, %s", secure($id, 'int'), secure($offset, 'int', false), secure($system['min_results_even'], 'int', false) )) or _error(SQL_ERROR_THROWEN);
                if($get_photos->num_rows > 0) {
                    while($photo = $get_photos->fetch_assoc()) {
                        /* check the photo privacy */
                        if($this->_check_privacy($photo['privacy'], $id)) {
                            if ($system['is_mobile'] && !is_empty($photo['source'])) {
                                $photo['source'] = $system['system_uploads'] . '/' . $photo['source'];
                            }
                            $photos[] = $photo;
                        }
                    }
                }
                break;

            case 'page':
                $offset *= $system['min_results_even'];
                $get_photos = $db->query(sprintf("SELECT posts_photos.photo_id, posts_photos.source FROM posts_photos INNER JOIN posts ON posts_photos.post_id = posts.post_id WHERE posts.user_id = %s AND user_type = 'page' ORDER BY posts_photos.photo_id DESC LIMIT %s, %s", secure($id, 'int'), secure($offset, 'int', false), secure($system['min_results_even'], 'int', false) )) or _error(SQL_ERROR_THROWEN);
                if($get_photos->num_rows > 0) {
                    while($photo = $get_photos->fetch_assoc()) {
                        if ($system['is_mobile'] && !is_empty($photo['source'])) {
                            $photo['source'] = $system['system_uploads'] . '/' . $photo['source'];
                        }
                        $photos[] = $photo;
                    }
                }
                break;

            case 'group':
                $offset *= $system['min_results_even'];
                if(!$pass_check) {
                    /* check if the viewer is group member (approved) */
                    $check_group = $db->query(sprintf("SELECT groups.* FROM groups INNER JOIN groups_members ON groups.group_id = groups_members.group_id WHERE groups_members.approved = '1' AND groups_members.user_id = %s AND groups_members.group_id = %s", secure($this->_data['user_id'], 'int'), secure($id, 'int') )) or _error(SQL_ERROR_THROWEN);
                    /* if no -> return */
                    if($check_group->num_rows == 0) {
                        return $photos;
                    }
                }
                $get_photos = $db->query(sprintf("SELECT posts_photos.photo_id, posts_photos.source FROM posts_photos INNER JOIN posts ON posts_photos.post_id = posts.post_id WHERE posts.group_id = %s AND in_group = '1' ORDER BY posts_photos.photo_id DESC LIMIT %s, %s", secure($id, 'int'), secure($offset, 'int', false), secure($system['min_results_even'], 'int', false) )) or _error(SQL_ERROR_THROWEN);
                if($get_photos->num_rows > 0) {
                    while($photo = $get_photos->fetch_assoc()) {
                        if ($system['is_mobile'] && !is_empty($photo['source'])) {
                            $photo['source'] = $system['system_uploads'] . '/' . $photo['source'];
                        }
                        $photos[] = $photo;
                    }
                }
                break;

            case 'event':
                $offset *= $system['min_results_even'];
                if(!$pass_check) {
                    /* check if the viewer is event member (approved) */
                    $check_event = $db->query(sprintf("SELECT events.* FROM events INNER JOIN events_members ON events.event_id = events_members.event_id WHERE events_members.user_id = %s AND events_members.event_id = %s", secure($this->_data['user_id'], 'int'), secure($id, 'int') )) or _error(SQL_ERROR_THROWEN);
                    /* if no -> return */
                    if($check_event->num_rows == 0) {
                        _error(403);
                    }
                }
                $get_photos = $db->query(sprintf("SELECT posts_photos.photo_id, posts_photos.source FROM posts_photos INNER JOIN posts ON posts_photos.post_id = posts.post_id WHERE posts.event_id = %s AND in_event = '1' ORDER BY posts_photos.photo_id DESC LIMIT %s, %s", secure($id, 'int'), secure($offset, 'int', false), secure($system['min_results_even'], 'int', false) )) or _error(SQL_ERROR_THROWEN);
                if($get_photos->num_rows > 0) {
                    while($photo = $get_photos->fetch_assoc()) {
                        $photos[] = $photo;
                    }
                }
                break;
        }
        return $photos;
    }


    /**
     * get_photo
     *
     * @param integer $photo_id
     * @param boolean $full_details
     * @param boolean $get_gallery
     * @param string $context
     * @return array
     */
    public function get_photo($photo_id, $full_details = false, $get_gallery = false, $context = 'photos') {
        global $db;

        /* get photo */
        $get_photo = $db->query(sprintf("SELECT * FROM posts_photos WHERE photo_id = %s", secure($photo_id, 'int') )) or _error(SQL_ERROR_THROWEN);
        if($get_photo->num_rows == 0) {
            return false;
        }
        $photo = $get_photo->fetch_assoc();

        /* get post */
        $post = $this->_check_post($photo['post_id'], false, $full_details);
        if(!$post) {
            return false;
        }

        /* check if photo can be deleted */
        if($post['in_group']) {
            /* check if (cover|profile) photo */
            $photo['can_delete'] = ( ($photo_id == $post['group_picture_id']) OR ($photo_id == $post['group_cover_id']) )? false : true;
        } elseif ($post['in_event']) {
            /* check if (cover) photo */
            $photo['can_delete'] = ( ($photo_id == $post['event_cover_id']) )? false : true;
        } elseif ($post['user_type'] == "user") {
            /* check if (cover|profile) photo */
            $photo['can_delete'] = ( ($photo_id == $post['user_picture_id']) OR ($photo_id == $post['user_cover_id']) )? false : true;
        } elseif ($post['user_type'] == "page") {
            /* check if (cover|profile) photo */
            $photo['can_delete'] = ( ($photo_id == $post['page_picture_id']) OR ($photo_id == $post['page_cover_id']) )? false : true;
        }

        /* check photo type [single|mutiple] */
        $check_single = $db->query(sprintf("SELECT * FROM posts_photos WHERE post_id = %s", secure($photo['post_id'], 'int') ))  or _error(SQL_ERROR_THROWEN);
        $photo['is_single'] = ($check_single->num_rows > 1)? false : true;

        /* get full details */
        if($full_details) {
            if($photo['is_single']) {
                /* single photo => get (likes|comments) of post */

                /* check if viewer likes this post */
                if($this->_logged_in && $post['likes'] > 0) {
                    $check_like = $db->query(sprintf("SELECT * FROM posts_likes WHERE user_id = %s AND post_id = %s", secure($this->_data['user_id'], 'int'), secure($post['post_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                    $photo['i_like'] = ($check_like->num_rows > 0)? true: false;
                }

                /* get post comments */
                if($post['comments'] > 0) {
                    $post['post_comments'] = $this->get_comments($post['post_id'], 0, true, true, $post);
                }

            } else {
                /* mutiple photo => get (likes|comments) of photo */

                /* check if viewer user likes this photo */
                if($this->_logged_in && $photo['likes'] > 0) {
                    $check_like = $db->query(sprintf("SELECT * FROM posts_photos_likes WHERE user_id = %s AND photo_id = %s", secure($this->_data['user_id'], 'int'), secure($photo['photo_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                    $photo['i_like'] = ($check_like->num_rows > 0)? true: false;
                }

                /* get post comments */
                if($photo['comments'] > 0) {
                    $photo['photo_comments'] = $this->get_comments($photo['photo_id'], 0, false, true, $post);
                }

            }
        }

        /* get gallery */
        if($get_gallery) {
            switch ($context) {
                case 'post':
                    $get_post_photos = $db->query(sprintf("SELECT photo_id, source FROM posts_photos WHERE post_id = %s", secure($post['post_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                    while($post_photo = $get_post_photos->fetch_assoc()) {
                        $post_photos[$post_photo['photo_id']] = $post_photo;
                    }
                    $photo['next'] = $post_photos[get_array_key($post_photos, $photo['photo_id'], -1)];
                    $photo['prev'] =  $post_photos[get_array_key($post_photos, $photo['photo_id'], 1)];
                    break;

                case 'album':
                    $get_album_photos = $db->query(sprintf("SELECT photo_id, source FROM posts_photos WHERE album_id = %s", secure($photo['album_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                    while($album_photo = $get_album_photos->fetch_assoc()) {
                        $album_photos[$album_photo['photo_id']] = $album_photo;
                    }
                    $photo['next'] = $album_photos[get_array_key($album_photos, $photo['photo_id'], -1)];
                    $photo['prev'] =  $album_photos[get_array_key($album_photos, $photo['photo_id'], 1)];
                    break;

                case 'photos':
                    if($post['in_group']) {
                        $get_target_photos = $db->query(sprintf("SELECT posts_photos.photo_id, posts_photos.source FROM posts INNER JOIN posts_photos ON posts.post_id = posts_photos.post_id WHERE posts.in_group = '1' AND posts.group_id = %s", secure($post['group_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                    } elseif ($post['in_event']) {
                        $get_target_photos = $db->query(sprintf("SELECT posts_photos.photo_id, posts_photos.source FROM posts INNER JOIN posts_photos ON posts.post_id = posts_photos.post_id WHERE posts.in_event = '1' AND posts.event_id = %s", secure($post['event_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                    } elseif ($post['user_type'] == "page") {
                        $get_target_photos = $db->query(sprintf("SELECT posts_photos.photo_id, posts_photos.source FROM posts INNER JOIN posts_photos ON posts.post_id = posts_photos.post_id WHERE posts.user_type = 'page' AND posts.user_id = %s", secure($post['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                    } elseif ($post['user_type'] == "user") {
                        $get_target_photos = $db->query(sprintf("SELECT posts_photos.photo_id, posts_photos.source FROM posts INNER JOIN posts_photos ON posts.post_id = posts_photos.post_id WHERE posts.user_type = 'user' AND posts.user_id = %s", secure($post['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                    }
                    while($target_photo = $get_target_photos->fetch_assoc()) {
                        $target_photos[$target_photo['photo_id']] = $target_photo;
                    }
                    $photo['next'] = $target_photos[get_array_key($target_photos, $photo['photo_id'], -1)];
                    $photo['prev'] =  $target_photos[get_array_key($target_photos, $photo['photo_id'], 1)];
                    break;
            }
        }

        /* return post array with photo */
        $photo['post'] = $post;
        return $photo;
    }

    /**
     * delete_photo
     * 
     * @param integer $photo_id
     * @return void
     */
    public function delete_photo($photo_id) {
        global $db, $system;
        /* (check|get) photo */
        $photo = $this->get_photo($photo_id);
        if(!$photo) {
            _error(403);
        }
        $post = $photo['post'];
        /* check if viewer can manage post */
        if(!$post['manage_post']) {
            _error(403);
        }
        /* check if photo can be deleted */
        if(!$photo['can_delete']) {
            throw new Exception(__("This photo can't be deleted"));
        }
        /* delete the photo */
        $db->query(sprintf("DELETE FROM posts_photos WHERE photo_id = %s", secure($photo_id, 'int') )) or _error(SQL_ERROR_THROWEN);
    }


    /**
     * get_albums
     * 
     * @param integer $user_id
     * @param string $type
     * @param integer $offset
     * @return array
     */
    public function get_albums($id, $type = 'user', $offset = 0) {
        global $db, $system;
        /* initialize vars */
        $albums = array();
        $offset *= $system['min_results_even'];
        $valid_types = array('user', 'page', 'group');
        if(!in_array($type, $valid_types)) {
            return $albums;
        }
        if($type == "group") {
            $get_albums = $db->query(sprintf("SELECT album_id FROM posts_photos_albums WHERE in_group = '1' AND group_id = %s LIMIT %s, %s", secure($id, 'int'), secure($offset, 'int', false), secure($system['min_results_even'], 'int', false) )) or _error(SQL_ERROR_THROWEN);
            
        } else {
            $get_albums = $db->query(sprintf("SELECT album_id FROM posts_photos_albums WHERE user_id = %s AND user_type = %s AND in_group = '0' LIMIT %s, %s", secure($id, 'int'), secure($type), secure($offset, 'int', false), secure($system['min_results_even'], 'int', false) )) or _error(SQL_ERROR_THROWEN);
        }
        if($get_albums->num_rows > 0) {
            while($album = $get_albums->fetch_assoc()) {
                $album = $this->get_album($album['album_id'], false); /* $full_details = false */
                if($album) {
                    $albums[] = $album;
                }                    
            }
        }
        return $albums;
    }


    /**
     * get_album
     * 
     * @param integer $album_id
     * @param boolean $full_details
     * @return array
     */
    /* CI-mobile thêm biến $offset để xử lý cho app*/
    public function get_album($album_id, $full_details = true, $offset = 0) {
        global $db, $system;
        $get_album = $db->query(sprintf("SELECT posts_photos_albums.*, users.user_name, users.user_album_pictures, users.user_album_covers, users.user_album_timeline, pages.page_id, pages.page_name, pages.page_admin, pages.page_album_pictures, pages.page_album_covers, pages.page_album_timeline, groups.group_name, groups.group_admin, groups.group_album_pictures, groups.group_album_covers, groups.group_album_timeline FROM posts_photos_albums LEFT JOIN users ON posts_photos_albums.user_id = users.user_id AND posts_photos_albums.user_type = 'user' LEFT JOIN pages ON posts_photos_albums.user_id = pages.page_id AND posts_photos_albums.user_type = 'page' LEFT JOIN groups ON posts_photos_albums.in_group = '1' AND posts_photos_albums.group_id = groups.group_id WHERE NOT (users.user_name <=> NULL AND pages.page_name <=> NULL) AND posts_photos_albums.album_id = %s", secure($album_id, 'int') )) or _error(SQL_ERROR_THROWEN);
        if($get_album->num_rows == 0) {
            return false;
        }
        $album = $get_album->fetch_assoc();
        /* get the author */
        $album['author_id'] = ($album['user_type'] == "page")? $album['page_admin'] : $album['user_id'];
        /* check the album privacy  */
        /* if post in group & (the group is public || the viewer approved member of this group) => pass privacy check */
        $pass_privacy_check = false;
        if( $album['in_group'] && ($album['group_privacy'] == 'public' || $this->check_group_membership($this->_data['user_id'], $album['group_id']) == 'approved') ) {
            $pass_privacy_check = true;
        }
        /* if post in event & (the event is public || the viewer member of this event) => pass privacy check */
        /*if( $album['in_event'] && ($album['event_privacy'] == 'public' || $this->check_event_membership($this->_data['user_id'], $album['event_id']))) {
            $pass_privacy_check = true;
        }*/
        if(!$pass_privacy_check) {
            if(!$this->_check_privacy($album['privacy'], $album['author_id'])) {
                return false;
            }
        }
        /* get album path */
        if($album['in_group']) {
            $album['path'] = 'groups/'.$album['group_name'];
            /* check if (cover|profile|timeline) album */
            $album['can_delete'] = ( ($album_id == $album['group_album_pictures']) OR ($album_id == $album['group_album_covers']) OR ($album_id == $album['group_album_timeline']) )? false : true;
        } elseif ($album['in_event']) {
            $album['path'] = 'events/'.$album['event_id'];
            /* check if (cover|profile|timeline) album */
            $album['can_delete'] = ( ($album_id == $album['event_album_pictures']) OR ($album_id == $album['event_album_covers']) OR ($album_id == $album['event_album_timeline']) )? false : true;
        } elseif ($album['user_type'] == "user") {
            $album['path'] = $album['user_name'];
            /* check if (cover|profile|timeline) album */
            $album['can_delete'] = ( ($album_id == $album['user_album_pictures']) OR ($album_id == $album['user_album_covers']) OR ($album_id == $album['user_album_timeline']) )? false : true;
        } elseif ($album['user_type'] == "page") {
            $album['path'] = 'pages/'.$album['page_name'];
            /* check if (cover|profile|timeline) album */
            $album['can_delete'] = ( ($album_id == $album['user_album_timeline']) OR ($album_id == $album['page_album_covers']) OR ($album_id == $album['page_album_timeline']) )? false : true;
        }
        /* get album cover photo */
        $get_cover = $db->query(sprintf("SELECT source FROM posts_photos WHERE album_id = %s ORDER BY photo_id DESC", secure($album_id, 'int') )) or _error(SQL_ERROR_THROWEN);
        if($get_cover->num_rows == 0) {
            $album['cover'] = $system['system_url'].'/content/themes/'.$system['theme'].'/images/blank_album.jpg';
        } else {
            $cover = $get_cover->fetch_assoc();
            $album['cover'] = $system['system_uploads'].'/'.$cover['source'];
        }
        $album['photos_count'] = $get_cover->num_rows;
        /* check if viewer can manage album [Edit|Update|Delete] */
        $album['is_page_admin'] = ($this->_logged_in && $this->_data['user_id'] == $album['page_admin'])? true : false;
        $album['is_group_admin'] = ($this->_logged_in && $this->_data['user_id'] == $album['group_admin'])? true : false;
        //$album['is_event_admin'] = ($this->_logged_in && $this->_data['user_id'] == $album['event_admin'])? true : false;
        /* check if viewer can manage album [Edit|Update|Delete] */
        $album['manage_album'] = false;
        if($this->_logged_in) {
            /* viewer is (admins|moderators)] */
            if($this->_data['user_group'] < 3) {
                $album['manage_album'] = true;
            }
            /* viewer is the author of post || page admin */
            if($this->_data['user_id'] == $album['author_id']) {
                $album['manage_album'] = true;
            }
            /* viewer is the admin of the group of the post */
            if($album['in_group'] && $album['is_group_admin']) {
                $album['manage_album'] = true;
            }
            /* viewer is the admin of the event of the post */
            /*if($album['in_event'] && $album['is_event_admin']) {
                $album['manage_album'] = true;
            }*/
        }
        /* get album photos */
        if($full_details) {
            $album['photos'] = $this->get_photos($album_id, 'album', $offset);

        }
        return $album;
    }


    /**
     * delete_album
     * 
     * @param integer $album_id
     * @return array
     */
    public function delete_album($album_id) {
        global $db, $system;
        /* (check|get) album */
        $album = $this->get_album($album_id, false);
        if(!$album) {
            _error(403);
        }
        /* check if viewer can manage album */
        if(!$album['manage_album']) {
            _error(403);
        }
        /* check if album can be deleted */
        if(!$album['can_delete']) {
            throw new Exception(__("This album can't be deleted"));
        }
        /* delete the album */
        $db->query(sprintf("DELETE FROM posts_photos_albums WHERE album_id = %s", secure($album_id, 'int') )) or _error(SQL_ERROR_THROWEN);
        /* delete all album photos */
        $db->query(sprintf("DELETE FROM posts_photos WHERE album_id = %s", secure($album_id, 'int') )) or _error(SQL_ERROR_THROWEN);
        /* retrun path */
        $path = $system['system_url']."/".$album['path']."/albums";
        return $path;
    }


    /**
     * edit_album
     * 
     * @param integer $album_id
     * @param string $title
     * @return void
     */
    public function edit_album($album_id, $title) {
        global $db;
        /* (check|get) album */
        $album = $this->get_album($album_id, false);
        if(!$album) {
            _error(400);
        }
        /* check if viewer can manage album */
        if(!$album['manage_album']) {
            _error(400);
        }
        /* validate all fields */
        if(is_empty($title)) {
            throw new Exception(__("You must fill in all of the fields"));
        }
        /* edit the album */
        $db->query(sprintf("UPDATE posts_photos_albums SET title = %s WHERE album_id = %s", secure($title), secure($album_id, 'int') )) or _error(SQL_ERROR_THROWEN);
    }


    /**
     * add_photos
     *
     * @param array $args
     * @return array
     */
    public function add_album_photos($args = array()) {
        global $db, $system, $date;
        /* (check|get) album */
        $album = $this->get_album($args['album_id'], false);
        if(!$album) {
            _error(400);
        }
        /* check if viewer can manage album */
        if(!$album['manage_album']) {
            _error(400);
        }
        /* check user_id */
        $user_id = ($album['user_type'] == "page")? $album['page_id'] : $album['user_id'];
        /* check post location */
        $args['location'] = (!is_empty($args['location']) && valid_location($args['location']))? $args['location']: '';
        /* check privacy */
        if($album['in_group'] || $album['in_event']) {
            $args['privacy'] = 'custom';
        } elseif ($album['user_type'] == "page") {
            $args['privacy'] = 'public';
        } else {
            if(!in_array($args['privacy'], array('me', 'friends', 'public'))) {
                $args['privacy'] = 'public';
            }
        }
        /* insert the post */
        $db->query(sprintf("INSERT INTO posts (user_id, user_type, in_group, group_id, post_type, time, location, privacy, text) VALUES (%s, %s, %s, %s, 'album', %s, %s, %s, %s)", secure($user_id, 'int'), secure($album['user_type']), secure($album['in_group']), secure($album['group_id'], 'int'), secure($date), secure($args['location']), secure($args['privacy']), secure($args['message']) )) or _error(SQL_ERROR_THROWEN);
        $post_id = $db->insert_id;
        /* insert new photos */
        foreach ($args['photos'] as $photo) {
            $db->query(sprintf("INSERT INTO posts_photos (post_id, album_id, source) VALUES (%s, %s, %s)", secure($post_id, 'int'), secure($args['album_id'], 'int'), secure($photo) )) or _error(SQL_ERROR_THROWEN);
        }
        /* post mention notifications */
        $this->post_mentions($args['message'], $post_id);
        return $post_id;
    }

    /**
     * get post_id from album_id
     *
     * @param array $args
     * @return int
     */
    public function getPostFromAlbumId($album_id) {
        global $db ;

        $get_post_id = $db->query(sprintf("SELECT DISTINCT post_id FROM posts_photos WHERE album_id = %s", secure($album_id, 'int') )) or _error(SQL_ERROR_THROWEN);
        if($get_post_id->num_rows > 0) {
             return $get_post_id->fetch_assoc();
        }
        return 0;
    }



    /* ------------------------------- */
    /* Post Reactions */
    /* ------------------------------- */

    /**
     * share
     * 
     * @param integer $post_id
     *@return void
     */
    public function share($post_id) {
        global $db, $date;
        /* check if the viewer can share the post */
        $post = $this->_check_post($post_id, true);
        if(!$post || $post['privacy'] != 'public') {
            _error(403);
        }
        /* CI - thêm TH không check blocking đối với bài đăng của page */
        /* check blocking */
        if($this->blocked($post['author_id']) && $post['user_type'] != 'page') {
            _error(403);
        }

        // share post
        /* share the origin post */
        if($post['post_type'] == "shared") {
            $origin = $this->_check_post($post['origin_id'], true);
            if(!$origin || $origin['privacy'] != 'public') {
                _error(403);
            }
            $post_id = $origin['post_id'];
            $author_id = $origin['author_id'];
        } else {
            $post_id = $post['post_id'];
            $author_id = $post['author_id'];
        }
        /* insert the new shared post */
        $db->query(sprintf("INSERT INTO posts (user_id, user_type, post_type, origin_id, time, privacy) VALUES (%s, 'user', 'shared', %s, %s, 'public')", secure($this->_data['user_id'], 'int'), secure($post_id, 'int'), secure($date) )) or _error(SQL_ERROR_THROWEN);
        /* update the origin post shares counter */
        $db->query(sprintf("UPDATE posts SET shares = shares + 1 WHERE post_id = %s", secure($post_id, 'int') )) or _error(SQL_ERROR_THROWEN);
        /* post notification */
        //$this->post_notification($author_id, 'share', 'post', $post_id);
        $this->post_notification_for_social( array('to_user_id'=>$author_id, 'action'=>'share', 'node_type'=>'post', 'node_url'=>$post_id) );
    }


    /**
     * delete_post
     * 
     * @param integer $post_id
     * @return void
     */
    public function delete_post($post_id) {
        global $db;
        /* (check|get) post */
        $post = $this->_check_post($post_id, true);
        if(!$post) {
            _error(403);
        }
        /* delete the post */
        if(!$post['manage_post']) {
            _error(403);
        }
        $db->query(sprintf("DELETE FROM posts WHERE post_id = %s", secure($post_id, 'int') )) or _error(SQL_ERROR_THROWEN);
    }


    /**
     * edit_post
     * 
     * @param integer $post_id
     * @param string $message
     * @return array
     */
    public function edit_post($post_id, $message) {
        global $db, $system;
        /* (check|get) post */
        $post = $this->_check_post($post_id, true);
        if(!$post) {
            _error(403);
        }
        /* check if viewer can edit post */
        if(!$post['manage_post']) {
            _error(403);
        }
        /* check if viewer can edit post */
        if(!$post['manage_post'] && $system['is_mobile']) {
            _api_error(403);
        } elseif (!$post['manage_post']) {
            _error(403);
        }
        /* update post */
        $db->query(sprintf("UPDATE posts SET text = %s WHERE post_id = %s", secure($message), secure($post_id, 'int') )) or _error(SQL_ERROR_THROWEN);
        /* post mention notifications */
        $this->post_mentions($message, $post_id);
        /* parse text */
        $post['text_plain'] = htmlentities($message, ENT_QUOTES, 'utf-8');
        $post['text'] = $this->_parse($post['text_plain']);
        /* return */
        return $post;
    }


    /**
     * edit_privacy
     * 
     * @param integer $post_id
     * @param string $privacy
     */
    public function edit_privacy($post_id, $privacy) {
        global $db, $system;
        /* (check|get) post */
        $post = $this->_check_post($post_id, true);
        if(!$post) {
            _error(403);
        }
        /* check if viewer can edit privacy */
        if($post['manage_post'] && $post['user_type'] == 'user' && !$post['in_group']) {
            /* update privacy */
            $db->query(sprintf("UPDATE posts SET privacy = %s WHERE post_id = %s", secure($privacy), secure($post_id, 'int') )) or _error(SQL_ERROR_THROWEN);
        } else {
            _error(403);
        }
    }


    /**
     * save_post
     * 
     * @param integer $post_id
     * @return array
     */
    public function save_post($post_id) {
        global $db, $date;
        /* (check|get) post */
        $post = $this->_check_post($post_id);
        if(!$post) {
            _error(403);
        }
        /* save post */
        if(!$post['i_save']) {
            $db->query(sprintf("INSERT INTO posts_saved (post_id, user_id, time) VALUES (%s, %s, %s)", secure($post_id, 'int'), secure($this->_data['user_id'], 'int'), secure($date) )) or _error(SQL_ERROR_THROWEN);
        }
    }


    /**
     * unsave_post
     * 
     * @param integer $post_id
     * @return array
     */
    public function unsave_post($post_id) {
        global $db;
        /* (check|get) post */
        $post = $this->_check_post($post_id);
        if(!$post) {
            _error(403);
        }
        /* unsave post */
        if($post['i_save']) {
            $db->query(sprintf("DELETE FROM posts_saved WHERE post_id = %s AND user_id = %s", secure($post_id, 'int'), secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
        }
    }


    /**
     * pin_post
     * 
     * @param integer $post_id
     * @return array
     */
    public function pin_post($post_id) {
        global $db, $date;
        /* (check|get) post */
        $post = $this->_check_post($post_id);
        if(!$post) {
            _error(403);
        }
        /* check if viewer can edit post */
        if(!$post['manage_post']) {
            _error(403);
        }
        /* pin post */
        if(!$post['pinned']) {
            /* check the post author type */
            if($post['user_type'] == "user") {
                /* user */
                if($post['in_group']) {
                    /* update group */
                    $db->query(sprintf("UPDATE groups SET group_pinned_post = %s WHERE group_id = %s", secure($post_id, 'int'), secure($post['group_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                } elseif ($post['in_event']) {
                    /* update event */
                    $db->query(sprintf("UPDATE events SET event_pinned_post = %s WHERE event_id = %s", secure($post_id, 'int'), secure($post['event_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                } else {
                    /* update user */
                    $db->query(sprintf("UPDATE users SET user_pinned_post = %s WHERE user_id = %s", secure($post_id, 'int'), secure($post['author_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                }
            } else {
                /* update page */
                $db->query(sprintf("UPDATE pages SET page_pinned_post = %s WHERE page_id = %s", secure($post_id, 'int'), secure($post['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
            }
        }
    }


    /**
     * unpin_post
     *
     * @param integer $post_id
     * @return array
     */
    public function unpin_post($post_id) {
        global $db, $date;
        /* (check|get) post */
        $post = $this->_check_post($post_id);
        if(!$post) {
            _error(403);
        }
        /* check if viewer can edit post */
        if(!$post['manage_post']) {
            _error(403);
        }

        /* pin post */
        if($post['pinned']) {
            /* check the post author type */
            if($post['user_type'] == "user") {
                /* user */
                if($post['in_group']) {
                    /* update group */
                    $db->query(sprintf("UPDATE groups SET group_pinned_post = '0' WHERE group_id = %s", secure($post['group_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                } elseif($post['in_event']) {
                    /* update event */
                    $db->query(sprintf("UPDATE events SET event_pinned_post = '0' WHERE event_id = %s", secure($post['event_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                } else {
                    /* update user */
                    $db->query(sprintf("UPDATE users SET user_pinned_post = '0' WHERE user_id = %s", secure($post['author_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                }
            } else {
                /* update page */
                $db->query(sprintf("UPDATE pages SET page_pinned_post = '0' WHERE page_id = %s", secure($post['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
            }
        }
    }


    /**
     * like_post
     * 
     * @param integer $post_id
     * @return void
     */
    public function like_post($post_id) {
        global $db;
        /* (check|get) post */
        $post = $this->_check_post($post_id);
        if(!$post) {
            _error(403);
        }
        /* CI - thêm TH không check blocking đối với bài đăng của page */
        /* check blocking */
        if($this->blocked($post['author_id']) && $post['user_type'] != 'page') {
            _error(403);
        }

        /* like the post */
        if(!$post['i_like']) {
            $db->query(sprintf("INSERT INTO posts_likes (user_id, post_id) VALUES (%s, %s)", secure($this->_data['user_id'], 'int'), secure($post_id, 'int') )) or _error(SQL_ERROR_THROWEN);
            /* update post likes counter */
            $db->query(sprintf("UPDATE posts SET likes = likes + 1 WHERE post_id = %s", secure($post_id, 'int') )) or _error(SQL_ERROR_THROWEN);
            /* post notification */
            //$this->post_notification($post['author_id'], 'like', 'post', $post_id);
            $this->post_notification_for_social( array('to_user_id'=>$post['author_id'], 'action'=>'like', 'node_type'=>'post', 'node_url'=>$post_id) );
        }
    }


    /**
     * unlike_post
     * 
     * @param integer $post_id
     * @return void
     */
    public function unlike_post($post_id) {
        global $db;
        /* (check|get) post */
        $post = $this->_check_post($post_id);
        if(!$post) {
            _error(403);
        }
        /* CI - thêm TH không check blocking đối với bài đăng của page */
        /* check blocking */
        if($this->blocked($post['author_id']) && $post['user_type'] != 'page') {
            _error(403);
        }

        /* unlike the post */
        if($post['i_like']) {
            $db->query(sprintf("DELETE FROM posts_likes WHERE user_id = %s AND post_id = %s", secure($this->_data['user_id'], 'int'), secure($post_id, 'int') )) or _error(SQL_ERROR_THROWEN);
            /* update post likes counter */
            $db->query(sprintf("UPDATE posts SET likes = IF(likes=0,0,likes-1) WHERE post_id = %s", secure($post_id, 'int') )) or _error(SQL_ERROR_THROWEN);
            /* delete notification */
            $this->delete_notification($post['author_id'], 'like', 'post', $post_id);
        }
    }


    /**
     * hide_post
     * 
     * @param integer $post_id
     * @return void
     */
    public function hide_post($post_id) {
        global $db;
        /* (check|get) post */
        $post = $this->_check_post($post_id);
        if(!$post) {
            _error(403);
        }
        /* CI - thêm TH không check blocking đối với bài đăng của page */
        /* check blocking */
        if($this->blocked($post['author_id']) && $post['user_type'] != 'page') {
            _error(403);
        }
        /* hide the post */
        $db->query(sprintf("INSERT INTO posts_hidden (user_id, post_id) VALUES (%s, %s)", secure($this->_data['user_id'], 'int'), secure($post_id, 'int') )) or _error(SQL_ERROR_THROWEN);
    }


    /**
     * unhide_post
     * 
     * @param integer $post_id
     * @return void
     */
    public function unhide_post($post_id) {
        global $db;
        /* (check|get) post */
        $post = $this->_check_post($post_id);
        if(!$post) {
            _error(403);
        }
        /* unhide the post */
        $db->query(sprintf("DELETE FROM posts_hidden WHERE user_id = %s AND post_id = %s", secure($this->_data['user_id'], 'int'), secure($post_id, 'int') )) or _error(SQL_ERROR_THROWEN);
    }


    /**
     * like_photo
     * 
     * @param integer $photo_id
     * @return void
     */
    public function like_photo($photo_id) {
        global $db;
        /* (check|get) photo */
        $photo = $this->get_photo($photo_id);
        if(!$photo) {
            _error(403);
        }
        $post = $photo['post'];

        /* CI - thêm TH không check blocking đối với bài đăng của page */
        /* check blocking */
        if($this->blocked($post['author_id']) && $post['user_type'] != 'page') {
            _error(403);
        }

        /* like the photo */
        $db->query(sprintf("INSERT INTO posts_photos_likes (user_id, photo_id) VALUES (%s, %s)", secure($this->_data['user_id'], 'int'), secure($photo_id, 'int') )) or _error(SQL_ERROR_THROWEN);
        /* update photo likes counter */
        $db->query(sprintf("UPDATE posts_photos SET likes = likes + 1 WHERE photo_id = %s", secure($photo_id, 'int') )) or _error(SQL_ERROR_THROWEN);
        /* post notification */
        //$this->post_notification($post['author_id'], 'like', 'photo', $photo_id);
        $this->post_notification_for_social( array('to_user_id'=>$post['author_id'], 'action'=>'like', 'node_type'=>'photo', 'node_url'=>$photo_id) );
    }


    /**
     * unlike_photo
     * 
     * @param integer $photo_id
     * @return void
     */
    public function unlike_photo($photo_id) {
        global $db;
        /* (check|get) photo */
        $photo = $this->get_photo($photo_id);
        if(!$photo) {
            _error(403);
        }
        $post = $photo['post'];
        /* unlike the photo */
        $db->query(sprintf("DELETE FROM posts_photos_likes WHERE user_id = %s AND photo_id = %s", secure($this->_data['user_id'], 'int'), secure($photo_id, 'int') )) or _error(SQL_ERROR_THROWEN);
        /* update photo likes counter */
        $db->query(sprintf("UPDATE posts_photos SET likes = IF(likes=0,0,likes-1) WHERE photo_id = %s", secure($photo_id, 'int') )) or _error(SQL_ERROR_THROWEN);
        /* delete notification */
        $this->delete_notification($post['author_id'], 'like', 'photo', $photo_id);
    }


    /* ------------------------------- */
    /* Reports */
    /* ------------------------------- */

    /**
     * report
     *
     * @param integer $id
     * @param string $handle
     * @return void
     */
    public function report($id, $handle) {
        global $db, $date;
        switch ($handle) {
            case 'user':
                /* check the user */
                $check = $db->query(sprintf("SELECT * FROM users WHERE user_id = %s", secure($id, 'int') )) or _error(SQL_ERROR_THROWEN);
                break;

            case 'page':
                /* check the page */
                $check = $db->query(sprintf("SELECT * FROM pages WHERE page_id = %s", secure($id, 'int') )) or _error(SQL_ERROR_THROWEN);
                break;

            case 'group':
                /* check the group */
                $check = $db->query(sprintf("SELECT * FROM groups WHERE group_id = %s", secure($id, 'int') )) or _error(SQL_ERROR_THROWEN);
                break;

            case 'event':
                /* check the event */
                $check = $db->query(sprintf("SELECT * FROM events WHERE event_id = %s", secure($id, 'int') )) or _error(SQL_ERROR_THROWEN);
                break;

            case 'post':
                /* check the post */
                $check = $db->query(sprintf("SELECT * FROM posts WHERE post_id = %s", secure($id, 'int') )) or _error(SQL_ERROR_THROWEN);
                break;

            case 'comment':
                /* check the comment */
                $check = $db->query(sprintf("SELECT * FROM posts_comments WHERE comment_id = %s", secure($id, 'int') )) or _error(SQL_ERROR_THROWEN);
                break;

            default:
                _error(403);
                break;
        }
        /* check node */
        if($check->num_rows == 0) {
            _error(403);
        }
        /* check old reports */
        $check = $db->query(sprintf("SELECT * FROM reports WHERE user_id = %s AND node_id = %s AND node_type = %s", secure($this->_data['user_id'], 'int'), secure($id, 'int'), secure($handle) )) or _error(SQL_ERROR_THROWEN);
        if($check->num_rows > 0) {
            switch ($handle) {
                case 'user':
                    throw new Exception(__("You have already reported this user!"));
                    break;

                case 'page':
                    throw new Exception(__("You have already reported this page!"));
                    break;

                case 'group':
                    throw new Exception(__("You have already reported this group!"));
                    break;

                case 'post':
                    throw new Exception(__("You have already reported this post!"));
                    break;

                case 'comment':
                    throw new Exception(__("You have already reported this comment!"));
                    break;

                default:
                    throw new Exception(__("You have already reported this!"));
                    break;
            }

        }
        /* report */
        $db->query(sprintf("INSERT INTO reports (user_id, node_id, node_type, time) VALUES (%s, %s, %s, %s)", secure($this->_data['user_id'], 'int'), secure($id, 'int'), secure($handle), secure($date) )) or _error(SQL_ERROR_THROWEN);
    }

    /* ------------------------------- */
    /* Affiliates */
    /* ------------------------------- */

    /**
     * init_affiliates
     *
     * @return void
     */
    public function init_affiliates() {
        if(!$this->_logged_in && isset($_GET['ref'])) {
            $expire = time()+2592000;
            setcookie($this->_cookie_user_referrer, $_GET['ref'], $expire, '/');
        }
    }


    /**
     * affiliates_balance
     *
     * @param string $type
     * @param integer $referee_id
     * @param integer $referrer_id
     * @return void
     */
    private function process_affiliates($type, $referee_id, $referrer_id = null) {
        global $db, $system;
        if(!$system['affiliates_enabled'] || $system['affiliate_type'] != $type || !isset($_COOKIE[$this->_cookie_user_referrer])) {
            return;
        }
        $get_referrer = $db->query(sprintf("SELECT user_id, user_name, user_affiliate_balance FROM users WHERE user_name = %s", secure($_COOKIE[$this->_cookie_user_referrer]) )) or _error(SQL_ERROR_THROWEN);
        if($get_referrer->num_rows == 0) {
            return;
        }
        $referrer = $get_referrer->fetch_assoc();
        if($referrer['user_id'] == $referee_id || $referrer['user_id'] == $referrer_id) {
            return;
        }
        /* update referrer balance */
        $balance = $referrer['user_affiliate_balance'] + $system['affiliates_per_user'];
        $db->query(sprintf("UPDATE users SET user_affiliate_balance = %s WHERE user_id = %s", secure($balance), secure($referrer['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
        /* update referee */
        $db->query(sprintf("UPDATE users SET user_referrer_id = %s WHERE user_id = %s", secure($referrer['user_id'], 'int'), secure($referee_id, 'int') )) or _error(SQL_ERROR_THROWEN);
    }


    /**
     * get_affiliates
     *
     * @param integer $user_id
     * @param integer $offset
     * @return array
     */
    public function get_affiliates($user_id, $offset = 0) {
        global $db, $system;
        $affiliates = array();
        $offset *= $system['max_results'];
        $get_affiliates = $db->query(sprintf('SELECT user_id, user_name, user_fullname, user_firstname, user_lastname, user_gender, user_picture FROM users WHERE user_referrer_id = %s LIMIT %s, %s', secure($user_id, 'int'), secure($offset, 'int', false), secure($system['max_results'], 'int', false) )) or _error(SQL_ERROR_THROWEN);
        if($get_affiliates->num_rows > 0) {
            while($affiliate = $get_affiliates->fetch_assoc()) {
                $affiliate['user_picture'] = $this->get_picture($affiliate['user_picture'], $affiliate['user_gender']);
                /* get the connection between the viewer & the target */
                $affiliate['connection'] = $this->connection($affiliate['user_id'], false);
                $affiliates[] = $affiliate;
            }
        }
        return $affiliates;
    }

    /* ------------------------------- */
    /* Custom Fields */
    /* ------------------------------- */

    /**
     * get_custom_fields
     *
     * @param array $args
     * @return array
     */
    public function get_custom_fields($args = []) {
        global $db, $system;
        $fields = array();
        /* prepare "for" [user|page|group|event] - default -> user */
        $args['for'] = (isset($args['for']))? $args['for']: "user";
        if(!in_array($args['for'], array('user', 'page', 'group', 'event'))) {
            _error(400);
        }
        /* prepare "get" [registration|profile|settings] - default -> registration */
        $args['get'] = (isset($args['get']))? $args['get']: "registration";
        if(!in_array($args['get'], array('registration', 'profile', 'settings'))) {
            _error(400);
        }
        /* prepare where_query */
        $where_query = "WHERE field_for = '".$args['for']."'";
        if($args['get'] == "registration") {
            $where_query .= " AND in_registration = '1'";
        } elseif ($args['get'] == "profile") {
            $where_query .= " AND in_profile = '1'";
        }
        $get_fields = $db->query("SELECT * FROM custom_fields ".$where_query." ORDER BY field_order ASC") or _error(SQL_ERROR_THROWEN);
        if($get_fields->num_rows > 0) {
            while($field = $get_fields->fetch_assoc()) {
                if($field['type'] == "selectbox") {
                    $field['options'] = explode(PHP_EOL, $field['select_options']);
                }
                if($args['get'] == "registration") {
                    /* no value neeeded */
                    $fields[] = $field;
                } else {
                    /* valid node_id */
                    if(!isset($args['node_id'])) {
                        _error(400);
                    }
                    /* get the custom field value */
                    $get_field_value = $db->query(sprintf("SELECT value FROM custom_fields_values WHERE field_id = %s AND node_id = %s AND node_type = %s", secure($field['field_id'], 'int'), secure($args['node_id'], 'int'), secure($args['for']) )) or _error(SQL_ERROR_THROWEN);
                    $field_value = $get_field_value->fetch_assoc();
                    if($field['type'] == "selectbox") {
                        $field['value'] = $field['options'][$field_value['value']];
                    } else {
                        $field['value'] = $field_value['value'];
                    }
                    if($args['get'] == "profile" && is_empty($field['value'])) {
                        continue;
                    }
                    $fields[$field['place']][] = $field;
                }
            }
        }
        return $fields;
    }


    /**
     * set_custom_fields
     *
     * @param array $input_fields
     * @param string $for
     * @param string $set
     * @param integer $node_id
     * @return array
     */
    public function set_custom_fields($input_fields, $for = "user", $set = "registration", $node_id = null) {
        global $db, $system;
        $custom_fields = array();
        /* prepare "for" [user|page|group|event] - default -> user */
        if(!in_array($for, array('user', 'page', 'group', 'event'))) {
            _error(400);
        }
        /* prepare "set" [registration|settings] - default -> registration */
        if(!in_array($set, array('registration', 'settings'))) {
            _error(400);
        }
        /* prepare where_query */
        $where_query = " AND field_for = '".$for."'";
        if($set == "registration") {
            $where_query .= " AND in_registration = '1'";
        }
        /* get & set input_fields */
        $prefix = "fld_";
        $prefix_len = strlen($prefix);
        foreach($input_fields as $key => $value) {
            if(strpos($key, $prefix) === false) {
                continue;
            }
            $field_id = substr($key, $prefix_len);
            $get_field = $db->query(sprintf("SELECT * FROM custom_fields WHERE field_id = %s".$where_query, secure($field_id, 'int') )) or _error(SQL_ERROR_THROWEN);
            if($get_field->num_rows == 0) {
                continue;
            }
            $field = $get_field->fetch_assoc();
            /* valid field */
            if($field['mandatory']) {
                if($field['type'] != "selectbox" && is_empty($value)) {
                    throw new Exception(__("You must enter")." ".$field['label']);
                }
                if($field['type'] == "selectbox" && $value == "none") {
                    throw new Exception(__("You must select")." ".$field['label']);
                }
            }
            if(strlen($value) > $field['length']) {
                throw new Exception(__("The maximum value for")." ".$field['label']." ".__("is")." ".$field['length']);
            }
            /* (insert|update) node custom fields */
            if($set == "registration") {
                /* insert query */
                $custom_fields[$field['field_id']] = $value;
            } else {
                /* valid node_id */
                if($node_id == null) {
                    _error(400);
                }
                $insert_field = $db->query(sprintf("INSERT INTO custom_fields_values (value, field_id, node_id, node_type) VALUES (%s, %s, %s, %s)", secure($value), secure($field['field_id'], 'int'), secure($node_id, 'int'), secure($for) ));
                if(!$insert_field) {
                    /* update if already exist */
                    $db->query(sprintf("UPDATE custom_fields_values SET value = %s WHERE field_id = %s AND node_id = %s AND node_type = %s", secure($value), secure($field['field_id'], 'int'), secure($node_id, 'int'), secure($for) )) or _error(SQL_ERROR_THROWEN);
                }
            }
        }
        if($set == "registration") {
            return $custom_fields;
        }
        return;
    }


    /* ------------------------------- */
    /* Pages */
    /* ------------------------------- */

    /**
     * get_pages
     *
     * @param array $args
     * @return array
     */
    public function get_pages($args = []) {
        global $db, $system;
        /* initialize arguments */
        $user_id = !isset($args['user_id']) ? null : $args['user_id'];
        $offset = !isset($args['offset']) ? 0 : $args['offset'];
        $promoted = !isset($args['promoted']) ? false : true;
        $suggested = !isset($args['suggested']) ? false : true;
        $random = !isset($args['random']) ? false : true;
        $boosted = !isset($args['boosted']) ? false : true;
        $managed = !isset($args['managed']) ? false : true;
        $results = !isset($args['results']) ? $system['max_results_even'] : $args['results'];
        /* initialize vars */
        $pages = array();
        $offset *= $results;
        /* get suggested pages */
        if($promoted) {
            $get_pages = $db->query("SELECT * FROM pages WHERE page_boosted = '1' ORDER BY RAND() LIMIT 3") or _error(SQL_ERROR_THROWEN);
        } elseif($suggested) {
            $pages_ids = $this->get_pages_ids();
            $random_statement = ($random) ? "ORDER BY RAND()" : "";
            if(count($pages_ids) > 0) {
                /* make a list from liked pages */
                $pages_list = implode(',',$pages_ids);
                $get_pages = $db->query(sprintf("SELECT * FROM pages WHERE page_id NOT IN (%s) ".$random_statement." LIMIT %s, %s", $pages_list, secure($offset, 'int', false), secure($results, 'int', false) )) or _error(SQL_ERROR_THROWEN);
            } else {
                $get_pages = $db->query(sprintf("SELECT * FROM pages ".$random_statement." LIMIT %s, %s", secure($offset, 'int', false), secure($results, 'int', false) )) or _error(SQL_ERROR_THROWEN);
            }
            /* get the "viewer" boosted pages */
        } elseif($boosted) {
            $get_pages = $db->query(sprintf("SELECT * FROM pages WHERE page_boosted = '1' AND page_boosted_by = %s LIMIT %s, %s", secure($this->_data['user_id'], 'int'), secure($offset, 'int', false), secure($results, 'int', false) )) or _error(SQL_ERROR_THROWEN);
            /* get the "taget" all pages who admin */
        } elseif($managed) {
            $get_pages = $db->query(sprintf("SELECT pages.* FROM pages_admins INNER JOIN pages ON pages_admins.page_id = pages.page_id WHERE pages_admins.user_id = %s ORDER BY page_id DESC", secure($user_id, 'int') )) or _error(SQL_ERROR_THROWEN);
            /* get the "viewer" pages who admin */
        } elseif($user_id == null) {
            $get_pages = $db->query(sprintf("SELECT pages.* FROM pages_admins INNER JOIN pages ON pages_admins.page_id = pages.page_id WHERE pages_admins.user_id = %s ORDER BY page_id DESC LIMIT %s, %s", secure($this->_data['user_id'], 'int'), secure($offset, 'int', false), secure($results, 'int', false) )) or _error(SQL_ERROR_THROWEN);
            /* get the "target" liked pages*/
        } else {
            /* get the target user's privacy */
            $get_privacy = $db->query(sprintf("SELECT user_privacy_pages FROM users WHERE user_id = %s", secure($user_id, 'int') )) or _error(SQL_ERROR_THROWEN);
            $privacy = $get_privacy->fetch_assoc();
            /* check the target user's privacy  */
            if(!$this->_check_privacy($privacy['user_privacy_pages'], $user_id)) {
                return $pages;
            }
            $get_pages = $db->query(sprintf("SELECT pages.* FROM pages INNER JOIN pages_likes ON pages.page_id = pages_likes.page_id WHERE pages_likes.user_id = %s LIMIT %s, %s", secure($user_id, 'int'), secure($offset, 'int', false), secure($results, 'int', false) )) or _error(SQL_ERROR_THROWEN);
        }
        if($get_pages->num_rows > 0) {
            while($page = $get_pages->fetch_assoc()) {
                $page['page_picture'] = $this->get_picture($page['page_picture'], 'page');
                /* check if the viewer liked the page */
                $page['i_like'] = false;
                if($this->_logged_in) {
                    $get_likes = $db->query(sprintf("SELECT * FROM pages_likes WHERE page_id = %s AND user_id = %s", secure($page['page_id'], 'int'), secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                    if($get_likes->num_rows > 0) {
                        $page['i_like'] = true;
                    }
                }
                $pages[] = $page;
            }
        }
        return $pages;
    }


    /**
     * get_pages_categories
     *
     * @return array
     */
    public function get_pages_categories() {
        global $db;
        $categories = array();
        $get_categories = $db->query("SELECT * FROM pages_categories") or _error(SQL_ERROR_THROWEN);
        if($get_categories->num_rows > 0) {
            while($category = $get_categories->fetch_assoc()) {
                //ConIu - Thêm điều kiện IF
                if ($category['category_id'] != SCHOOL_CATEGORY_ID) {
                    $categories[] = $category;
                }
            }
        }
        return $categories;
    }


    /**
     * create_page
     *
     * @param array $args
     * @return void
     */
    public function create_page($args = []) {
        global $db, $system, $date;
        /* check if pages enabled */
        if($this->_data['user_group'] >= 3 && !$system['pages_enabled']) {
            throw new Exception(__("This feature has been disabled by the admin"));
        }
        /* validate title */
        if(is_empty($args['title'])) {
            throw new Exception(__("You must enter a title for your page"));
        }
        if(strlen($args['title']) < 3) {
            throw new Exception(__("Page title must be at least 3 characters long. Please try another"));
        }
        /* validate username */
        if(is_empty($args['username'])) {
            throw new Exception(__("You must enter a username for your page"));
        }
        if(!valid_username($args['username'])) {
            throw new Exception(__("Please enter a valid username (a-z0-9_.) with minimum 3 characters long"));
        }
        if(reserved_username($args['username'])) {
            throw new Exception(__("You can't use")." ".$args['username']." ".__("as username"));
        }
        if($this->check_username($args['username'], 'page')) {
            throw new Exception(__("Sorry, it looks like this username")." ".$args['username']." ".__("belongs to an existing page"));
        }
        /* validate category */
        if(is_empty($args['category'])) {
            throw new Exception(__("You must select valid category for your page"));
        }
        $check = $db->query(sprintf("SELECT * FROM pages_categories WHERE category_id = %s", secure($args['category'], 'int') )) or _error(SQL_ERROR_THROWEN);
        if($check->num_rows == 0) {
            throw new Exception(__("You must select valid category for your page"));
        }
        /* set custom fields */
        //$custom_fields = $this->set_custom_fields($args, "page");
        /* insert new page */
        $db->query(sprintf("INSERT INTO pages (page_admin, page_category, page_name, page_title, page_description, page_date) VALUES (%s, %s, %s, %s, %s, %s)", secure($this->_data['user_id'], 'int'), secure($args['category'], 'int'), secure($args['username']), secure($args['title']), secure($args['description']), secure($date) )) or _error(SQL_ERROR_THROWEN);
        /* get page_id */
        $page_id = $db->insert_id;
        /* insert custom fields values */
        /*if($custom_fields) {
            foreach($custom_fields as $field_id => $value) {
                $db->query(sprintf("INSERT INTO custom_fields_values (value, field_id, node_id, node_type) VALUES (%s, %s, %s, 'page')", secure($value), secure($field_id, 'int'), secure($page_id, 'int') ));
            }
        }*/
        /* like the page */
        $this->connect("page-like", $page_id);
        /* page admin addation */
        $this->connect("page-admin-addation", $page_id, $this->_data['user_id']);
    }


    /**
     * edit_page
     *
     * @param integer $page_id
     * @param string $edit
     * @param array $args
     * @return string
     */
    public function edit_page($page_id, $edit, $args) {
        global $db, $system;
        /* check if pages enabled */
        if($this->_data['user_group'] >= 3 && !$system['pages_enabled']) {
            throw new Exception(__("This feature has been disabled by the admin"));
        }
        /* (check&get) page */
        $get_page = $db->query(sprintf("SELECT * FROM pages WHERE page_id = %s", secure($page_id, 'int') )) or _error(SQL_ERROR_THROWEN);
        if($get_page->num_rows == 0) {
            _error(403);
        }
        $page = $get_page->fetch_assoc();
        /* check permission */
        $can_edit = false;
        /* viewer is (admin|moderator) */
        if($this->_data['user_group'] < 3) {
            $can_edit = true;
            /* viewer is the admin of page */
        } elseif($this->check_page_adminship($this->_data['user_id'], $page_id)) {
            $can_edit = true;
        }
        if(!$can_edit) {
            _error(403);
        }
        /* edit page */
        switch ($edit) {
            case 'settings':
                /* validate title */
                if(is_empty($args['title'])) {
                    throw new Exception(__("You must enter a title for your page"));
                }
                if(strlen($args['title']) < 3) {
                    throw new Exception(__("Page title must be at least 3 characters long. Please try another"));
                }
                /* validate username */
                if(strtolower($args['username']) != strtolower($page['page_name'])) {
                    if(is_empty($args['username'])) {
                        throw new Exception(__("You must enter a username for your page"));
                    }
                    if(!valid_username($args['username'])) {
                        throw new Exception(__("Please enter a valid username (a-z0-9_.) with minimum 3 characters long"));
                    }
                    if(reserved_username($args['username'])) {
                        throw new Exception(__("You can't use")." ".$args['username']." ".__("as username"));
                    }
                    if($this->check_username($args['username'], 'page')) {
                        throw new Exception(__("Sorry, it looks like this username")." ".$args['username']." ".__("belongs to an existing page"));
                    }
                    /* set new page name */
                    $page['page_name'] = $args['username'];
                }
                /* validate category */
                if(is_empty($args['category'])) {
                    throw new Exception(__("You must select valid category for your page"));
                }
                $check = $db->query(sprintf("SELECT * FROM pages_categories WHERE category_id = %s", secure($args['category'], 'int') )) or _error(SQL_ERROR_THROWEN);
                if($check->num_rows == 0) {
                    throw new Exception(__("You must select valid category for your page"));
                }
                /* edit from admin panel */
                if($this->_data['user_group'] >= 3 && !isset($args['page_verified'])) {
                    $args['page_verified'] = $page['page_verified'];
                }
                if($this->_data['user_group'] >= 3 && !isset($args['page_description'])) {
                    $args['page_description'] = $page['page_description'];
                }
                /* update page */
                $db->query(sprintf("UPDATE pages SET page_category = %s, page_name = %s, page_title = %s, page_description = %s, page_verified = %s WHERE page_id = %s", secure($args['category'], 'int'), secure($args['username']), secure($args['title']), secure($args['page_description']), secure($args['page_verified']), secure($page_id, 'int') )) or _error(SQL_ERROR_THROWEN);
                break;

            case 'info':
                /* validate website */
//                if(!is_empty($args['website']) && !valid_url($args['website'])) {
//                    throw new Exception(__("Please enter a valid website URL"));
//                }
                /* set custom fields */
                $custom_fields = $this->set_custom_fields($args, "page" ,"settings", $page_id);
                /* update page */
                $db->query(sprintf("UPDATE pages SET page_company = %s, page_phone = %s, page_website = %s, page_location = %s, page_description = %s WHERE page_id = %s", secure($args['company']), secure($args['phone']), secure($args['website']), secure($args['location']), secure($args['description']), secure($page_id, 'int') )) or _error(SQL_ERROR_THROWEN);
                break;

            case 'action':
                /* validate action color */
                if(!in_array($args['action_color'], array('default', 'primary', 'success', 'info', 'warning', 'danger'))) {
                    throw new Exception(__("Please select a valid action button color"));
                }
                /* validate action URL */
                if(!is_empty($args['action_url']) && !valid_url($args['action_url'])) {
                    throw new Exception(__("Please enter a valid action button URL"));
                }
                /* update page */
                $db->query(sprintf("UPDATE pages SET page_action_text = %s, page_action_color = %s, page_action_url = %s WHERE page_id = %s", secure($args['action_text']), secure($args['action_color']), secure($args['action_url']), secure($page_id, 'int') )) or _error(SQL_ERROR_THROWEN);
                break;

            case 'social':
                /* validate facebook */
                if(!is_empty($args['facebook']) && !valid_url($args['facebook'])) {
                    throw new Exception(__("Please enter a valid Facebook Profile URL"));
                }
                /* validate twitter */
                if(!is_empty($args['twitter']) && !valid_url($args['twitter'])) {
                    throw new Exception(__("Please enter a valid Twitter Profile URL"));
                }
                /* validate google */
                if(!is_empty($args['google']) && !valid_url($args['google'])) {
                    throw new Exception(__("Please enter a valid Google+ Profile URL"));
                }
                /* validate youtube */
                if(!is_empty($args['youtube']) && !valid_url($args['youtube'])) {
                    throw new Exception(__("Please enter a valid YouTube Profile URL"));
                }
                /* validate instagram */
                if(!is_empty($args['instagram']) && !valid_url($args['instagram'])) {
                    throw new Exception(__("Please enter a valid Instagram Profile URL"));
                }
                /* validate linkedin */
                if(!is_empty($args['linkedin']) && !valid_url($args['linkedin'])) {
                    throw new Exception(__("Please enter a valid Linkedin Profile URL"));
                }
                /* validate vkontakte */
                if(!is_empty($args['vkontakte']) && !valid_url($args['vkontakte'])) {
                    throw new Exception(__("Please enter a valid Vkontakte Profile URL"));
                }
                /* update page */
                $db->query(sprintf("UPDATE pages SET page_social_facebook = %s, page_social_twitter = %s, page_social_google = %s, page_social_youtube = %s, page_social_instagram = %s, page_social_linkedin = %s, page_social_vkontakte = %s WHERE page_id = %s", secure($args['facebook']), secure($args['twitter']), secure($args['google']), secure($args['youtube']), secure($args['instagram']), secure($args['linkedin']), secure($args['vkontakte']), secure($page_id, 'int') )) or _error(SQL_ERROR_THROWEN);
                break;
        }
        return $page['page_name'];
    }


    /**
     * delete_page
     *
     * @param integer $page_id
     * @return void
     */
    public function delete_page($page_id) {
        global $db, $system;
        /* check if pages enabled */
        if($this->_data['user_group'] >= 3 && !$system['pages_enabled']) {
            throw new Exception(__("This feature has been disabled by the admin"));
        }
        /* (check&get) page */
        $get_page = $db->query(sprintf("SELECT * FROM pages WHERE page_id = %s", secure($page_id, 'int') )) or _error(SQL_ERROR_THROWEN);
        if($get_page->num_rows == 0) {
            _error(403);
        }
        $page = $get_page->fetch_assoc();
        // delete page
        $can_delete = false;
        /* viewer is (admin|moderator) */
        if($this->_data['user_group'] < 3) {
            $can_delete = true;
        }
        /* viewer is the admin of page */
        if($this->_data['user_id'] == $page['page_admin']) {
            $can_delete = true;
        }
        /* delete the page */
        if($can_delete) {
            /* delete the page */
            $db->query(sprintf("DELETE FROM pages WHERE page_id = %s", secure($page_id, 'int') )) or _error(SQL_ERROR_THROWEN);
        }
    }

    /**
     * get_page_admins_ids
     *
     * @param integer $page_id
     * @return array
     */
    public function get_page_admins_ids($page_id) {
        global $db;
        $admins = array();
        $get_admins = $db->query(sprintf("SELECT user_id FROM pages_admins WHERE page_id = %s", secure($page_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        if($get_admins->num_rows > 0) {
            while($admin = $get_admins->fetch_assoc()) {
                $admins[] = $admin['user_id'];
            }
        }
        $get_page_admin = $db->query(sprintf("SELECT page_admin FROM pages WHERE page_id = %s", secure($page_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        if($get_page_admin->num_rows > 0) {
            while($page_admin = $get_page_admin->fetch_assoc()) {
                $admins[] = $page_admin['page_admin'];
            }
        }
        // Lấy danh sách hiệu trưởng của trường
        $schoolConfig = getSchoolData($page_id, SCHOOL_CONFIG);
        $principalIds = array();
        if($schoolConfig['principal'] != '') {
            $principalIds = explode(',', $schoolConfig['principal']);
        }
        $admins = array_merge($principalIds, $admins);
        $admins = array_unique($admins);

        return $admins;
    }

    /**
     * Lấy danh sách hiệu trưởng và page_admin
     *
     * @param $page_id
     * @return array
     * @throws Exception
     */
    public function get_page_admins_ids_by_school($page_id) {
        global $db;
        $admins = array();
        $get_page_admin = $db->query(sprintf("SELECT page_admin FROM pages WHERE page_id = %s", secure($page_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        if($get_page_admin->num_rows > 0) {
            while($page_admin = $get_page_admin->fetch_assoc()) {
                $admins[] = $page_admin['page_admin'];
            }
        }
        // Lấy danh sách hiệu trưởng của trường
        $schoolConfig = getSchoolData($page_id, SCHOOL_CONFIG);
        $principalIds = array();
        if($schoolConfig['principal'] != '') {
            $principalIds = explode(',', $schoolConfig['principal']);
        }
        $admins = array_merge($principalIds, $admins);
        $admins = array_unique($admins);

        return $admins;
    }

    /**
     * get_page_admins
     *
     * @param integer $page_id
     * @param integer $offset
     * @return array
     */
    // Viết lại hàm này luôn - Taila
//    public function get_page_admins($page_id, $offset = 0) {
//        global $db, $system;
//        $admins = array();
//        if($offset == 0) {
//            // Lấy thêm page_admin trong bảng page và hiệu trưởng
//            $get_page_admin = $db->query(sprintf("SELECT users.user_id, users.user_name, users.user_fullname, users.user_firstname, users.user_lastname, users.user_gender, users.user_picture FROM users INNER JOIN pages ON pages.page_admin = users.user_id WHERE pages.page_id = %s", secure($page_id, 'int'))) or _error(SQL_ERROR_THROWEN);
//            if($get_page_admin->num_rows > 0) {
//                while($admin = $get_page_admin->fetch_assoc()) {
//                    $admin['user_picture'] = $this->get_picture($admin['user_picture'], $admin['user_gender']);
//                    /* get the connection */
//                    $admin['i_admin'] = true;
//                    $admin['connection'] = 'page_manage';
//                    $admin['node_id'] = $page_id;
//                    $admins[] = $admin;
//                }
//            }
//
//            $schoolConfig = getSchoolData($page_id, SCHOOL_CONFIG);
//            $strCon = '';
//            if($schoolConfig['principal'] != '') {
//                $strCon = $schoolConfig['principal'];
//                $get_admins = $db->query(sprintf("SELECT user_id, user_name, user_fullname, user_firstname, user_lastname, user_gender, user_picture FROM users WHERE user_id IN (%s)", $strCon)) or _error(SQL_ERROR_THROWEN);
//                if($get_admins->num_rows > 0) {
//                    while($admin = $get_admins->fetch_assoc()) {
//                        $admin['user_picture'] = $this->get_picture($admin['user_picture'], $admin['user_gender']);
//                        /* get the connection */
//                        $admin['i_admin'] = true;
//                        $admin['connection'] = 'page_manage';
//                        $admin['node_id'] = $page_id;
//                        $admins[] = $admin;
//                    }
//                }
//            }
//        }
//
//        // lấy danh sách admin trong bảng pages_admins
//        $offset *= $system['max_results_even'];
//        $get_admins = $db->query(sprintf("SELECT users.user_id, users.user_name, users.user_fullname, users.user_firstname, users.user_lastname, users.user_gender, users.user_picture FROM pages_admins INNER JOIN users ON pages_admins.user_id = users.user_id WHERE pages_admins.page_id = %s LIMIT %s, %s", secure($page_id, 'int'), secure($offset, 'int', false), secure($system['max_results_even'], 'int', false) )) or _error(SQL_ERROR_THROWEN);
//        if($get_admins->num_rows > 0) {
//            while($admin = $get_admins->fetch_assoc()) {
//                $admin['user_picture'] = $this->get_picture($admin['user_picture'], $admin['user_gender']);
//                /* get the connection */
//                $admin['i_admin'] = true;
//                $admin['connection'] = 'page_manage';
//                $admin['node_id'] = $page_id;
//                $admins[] = $admin;
//            }
//        }
//
//        // Xử lý bỏ những user trùng
//        $admin_ids = $this->get_page_admins_ids($page_id);
//        $adminsLast = array();
//        foreach ($admins as $admin) {
//            if(in_array($admin['user_id'], $admin_ids)) {
//                $adminsLast[] = $admin;
//                $admin_ids = array_diff($admin_ids, [$admin['user_id']]);
//            }
//        }
//
//        return $admins;
//    }
    public function get_page_admins($page_id, $offset = 0) {
        global $db, $system;

        // Lấy danh sách adminIds - Cái này có sẵn
        $adminIds = $this->get_page_admins_ids($page_id);
        $strCon = '';
        if(count($adminIds) > 0) {
            $strCon = implode(',', $adminIds);
        }
        $offset *= $system['max_results_even'];
        $get_admins = $db->query(sprintf("SELECT user_id, user_name, user_fullname, user_firstname, user_lastname, user_gender, user_picture FROM users WHERE user_id IN (%s) LIMIT %s, %s", $strCon, secure($offset, 'int', false), secure($system['max_results_even'], 'int', false) )) or _error(SQL_ERROR_THROWEN);
        if($get_admins->num_rows > 0) {
            while($admin = $get_admins->fetch_assoc()) {
                $admin['user_picture'] = $this->get_picture($admin['user_picture'], $admin['user_gender']);
                /* get the connection */
                $admin['i_admin'] = true;
                $admin['connection'] = 'page_manage';
                $admin['node_id'] = $page_id;
                $admins[] = $admin;
            }
        }
        return $admins;
    }

    /**
     * get_page_members
     *
     * @param integer $page_id
     * @param integer $offset
     * @return array
     */
    public function get_page_members($page_id, $offset = 0) {
        global $db, $system;
        $members = array();
        $offset *= $system['max_results_even'];
        $get_members = $db->query(sprintf("SELECT users.user_id, users.user_name, users.user_fullname, users.user_firstname, users.user_lastname, users.user_gender, users.user_picture FROM pages_likes INNER JOIN users ON pages_likes.user_id = users.user_id WHERE pages_likes.page_id = %s LIMIT %s, %s", secure($page_id, 'int'), secure($offset, 'int', false), secure($system['max_results_even'], 'int', false) )) or _error(SQL_ERROR_THROWEN);
        if($get_members->num_rows > 0) {
            /* get page admins ids */
            $page_admins_ids = $this->get_page_admins_ids($page_id);
            while($member = $get_members->fetch_assoc()) {
                $member['user_picture'] = $this->get_picture($member['user_picture'], $member['user_gender']);
                /* get the connection */
                $member['i_admin'] = in_array($member['user_id'], $page_admins_ids);
                $member['connection'] = 'page_manage';
                $member['node_id'] = $page_id;
                $members[] = $member;
            }
        }
        return $members;
    }


    /**
     * get_page_invites
     *
     * @param integer $page_id
     * @param integer $offset
     * @return array
     */
    public function get_page_invites($page_id, $offset = 0) {
        global $db, $system;
        $friends = array();
        $offset *= $system['max_results_even'];
        if($this->_data['friends_ids']) {
            /* get page members */
            $members = array();
            $get_members = $db->query(sprintf("SELECT user_id FROM pages_likes WHERE page_id = %s", secure($page_id, 'int'))) or _error(SQL_ERROR_THROWEN);
            if($get_members->num_rows > 0) {
                while($member = $get_members->fetch_assoc()) {
                    $members[] = $member['user_id'];
                }
            }
            /*  */
            $potential_invites = array_diff($this->_data['friends_ids'], $members);
            if($potential_invites) {
                /* get already invited friends */
                $invited_friends = array();
                $get_invited_friends = $db->query(sprintf("SELECT user_id FROM pages_invites WHERE page_id = %s AND from_user_id = %s", secure($page_id, 'int'), secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                if($get_invited_friends->num_rows > 0) {
                    while($invited_friend = $get_invited_friends->fetch_assoc()) {
                        $invited_friends[] = $invited_friend['user_id'];
                    }
                }
                $invites_list = implode(',',array_diff($potential_invites, $invited_friends));
                if($invites_list) {
                    $get_friends = $db->query(sprintf("SELECT user_id, user_name, user_fullname, user_gender, user_picture FROM users WHERE user_id IN ($invites_list) LIMIT %s, %s", secure($offset, 'int', false), secure($system['max_results_even'], 'int', false) )) or _error(SQL_ERROR_THROWEN);
                    while($friend = $get_friends->fetch_assoc()) {
                        $friend['user_picture'] = $this->get_picture($friend['user_picture'], $friend['user_gender']);
                        $friend['connection'] = 'page_invite';
                        $friend['node_id'] = $page_id;
                        $friends[] = $friend;
                    }
                }
            }
        }
        return $friends;
    }

    /**
     * check_page_adminship
     *
     * @param integer $user_id
     * @param integer $page_id
     * @return boolean
     */
    public function check_page_adminship($user_id, $page_id) {
        if($this->_logged_in) {
            /* get page admins ids */
            $page_admins_ids = $this->get_page_admins_ids($page_id);
            if(in_array($user_id, $page_admins_ids)) {
                return true;
            }
        }
        return false;
    }


    /**
     * check_page_membership
     *
     * @param integer $user_id
     * @param integer $page_id
     * @return boolean
     */
    public function check_page_membership($user_id, $page_id) {
        global $db;
        if($this->_logged_in) {
            $get_likes = $db->query(sprintf("SELECT * FROM pages_likes WHERE user_id = %s AND page_id = %s", secure($user_id, 'int'), secure($page_id, 'int') )) or _error(SQL_ERROR_THROWEN);
            if($get_likes->num_rows > 0) {
                return true;
            }
        }
        return false;
    }

    /* ------------------------------- */
    /* Groups */
    /* ------------------------------- */

    /**
     * get_groups
     * 
     * @param array $args
     * @return array
     */
    public function get_groups(array $args = array()) {
        global $db, $system;
        /* initialize arguments */
        $user_id = !isset($args['user_id']) ? null : $args['user_id'];
        $offset = !isset($args['offset']) ? 0 : $args['offset'];
        $suggested = !isset($args['suggested']) ? false : true;
        $random = !isset($args['random']) ? false : true;
        $managed = !isset($args['managed']) ? false : true;
        $results = !isset($args['results']) ? $system['max_results_even'] : $args['results'];
        /* initialize vars */
        $groups = array();
        $offset *= $results;
        /* get suggested groups */
        if($suggested) {
            $where_statement = "";
            /* make a list from joined groups (approved|pending) */
            $groups_ids = $this->get_groups_ids();
            if($groups_ids) {
                $groups_list = implode(',',$groups_ids);
                $where_statement .= "AND group_id NOT IN (".$groups_list.") ";
            }
            $sort_statement = ($random) ? "ORDER BY RAND()" : "ORDER BY group_id DESC";
            //$get_groups = $db->query(sprintf("SELECT * FROM groups WHERE group_privacy != 'secret' ".$where_statement.$sort_statement." LIMIT %s, %s", secure($offset, 'int', false), secure($results, 'int', false) )) or _error(SQL_ERROR_THROWEN);
            $get_groups = $db->query(sprintf("SELECT * FROM groups WHERE group_privacy = 'public' ".$where_statement.$sort_statement." LIMIT %s, %s", secure($offset, 'int', false), secure($results, 'int', false) )) or _error(SQL_ERROR_THROWEN);
            /* get the "taget" all groups who admin */
        } elseif($managed) {
            $get_groups = $db->query(sprintf("SELECT groups.* FROM groups_admins INNER JOIN groups ON groups_admins.group_id = groups.group_id WHERE groups_admins.user_id = %s ORDER BY group_id DESC", secure($user_id, 'int') )) or _error(SQL_ERROR_THROWEN);
            /* get the "viewer" groups who admin */
        } elseif($user_id == null) {
            $get_groups = $db->query(sprintf("SELECT groups.* FROM groups_admins INNER JOIN groups ON groups_admins.group_id = groups.group_id WHERE groups_admins.user_id = %s ORDER BY group_id DESC LIMIT %s, %s", secure($this->_data['user_id'], 'int'), secure($offset, 'int', false), secure($results, 'int', false) )) or _error(SQL_ERROR_THROWEN);
            /* get the "target" groups*/
        } else {
            /* get the target user's privacy */
            $get_privacy = $db->query(sprintf("SELECT user_privacy_groups FROM users WHERE user_id = %s", secure($user_id, 'int') )) or _error(SQL_ERROR_THROWEN);
            $privacy = $get_privacy->fetch_assoc();
            /* check the target user's privacy  */
            if(!$this->_check_privacy($privacy['user_privacy_groups'], $user_id)) {
                return $groups;
            }
            /* if the viewer not the target exclude secret groups */
            $where_statement = ($this->_data['user_id'] == $user_id)? "": "AND groups.group_privacy != 'secret'";
            $get_groups = $db->query(sprintf("SELECT groups.* FROM groups INNER JOIN groups_members ON groups.group_id = groups_members.group_id WHERE groups_members.approved = '1' AND groups_members.user_id = %s ".$where_statement." ORDER BY group_id DESC LIMIT %s, %s", secure($user_id, 'int'), secure($offset, 'int', false), secure($results, 'int', false) )) or _error(SQL_ERROR_THROWEN);
        }
        if($get_groups->num_rows > 0) {
            while($group = $get_groups->fetch_assoc()) {
                $group['group_picture'] = $this->get_picture($group['group_picture'], 'group');
                /* check if the viewer joined the group */
                $group['i_joined'] = $this->check_group_membership($this->_data['user_id'], $group['group_id']);;
                $groups[] = $group;
            }
        }
        return $groups;
    }

    /**
     * get_groups_categories
     *
     * @return array
     */
    public function get_groups_categories() {
        global $db;
        $categories = array();
        $get_categories = $db->query("SELECT * FROM groups_categories ORDER BY category_id ASC") or _error(SQL_ERROR_THROWEN);
        if($get_categories->num_rows > 0) {
            while($category = $get_categories->fetch_assoc()) {
                //ConIu - Thêm điều kiện IF
                if ($category['category_id'] != CLASS_CATEGORY_ID && $category['category_id'] != CONIU_CATEGORY_ID) {
                    $categories[] = $category;
                }
            }
        }
        return $categories;
    }


    /**
     * get_groups_categories
     *
     * @return array
     */
    public function get_groups_by_category($category_id) {
        global $db;

        $groups = array();
        $get_groups = $db->query(sprintf("SELECT G.group_id, G.group_privacy, G.group_admin, G.group_name, G.group_title, G.group_picture, G.group_members, C.category_name FROM groups G
                      INNER JOIN groups_categories C ON G.group_privacy != 'secret' AND G.group_category = %s AND G.group_category = C.category_id", secure($category_id, 'int') )) or _error(SQL_ERROR_THROWEN);

        if($get_groups->num_rows > 0) {
            while($group = $get_groups->fetch_assoc()) {
                /* Coniu - Tăng thành viên nhóm */
                $group['group_members'] = increase_members_groups($group['group_name'], $group['group_members']);
                $group['group_picture'] = $this->get_picture($group['group_picture'], 'group');
                /* check if the viewer joined the group */
                $group['i_joined'] = $this->check_group_membership($this->_data['user_id'], $group['group_id']);;
                $groups[] = $group;
            }
        }
        return $groups;
    }

    /**
     *
     * @return array
     */
    public function get_group($group_name) {
        global $db;

        // [1] get main group info
        $get_group = $db->query(sprintf("SELECT G.*, CL.school_id, GC.category_name as group_category_name FROM groups G 
                 LEFT JOIN ci_class_level CL ON (G.class_level_id > 0) AND (G.class_level_id = CL.class_level_id)
                 LEFT JOIN groups_categories GC ON G.group_category = GC.category_id
                 WHERE G.group_name = %s", secure($group_name) )) or _error(SQL_ERROR_THROWEN);

        if($get_group->num_rows == 0) {
            _api_error(404);
        }
        $group = $get_group->fetch_assoc();

        /* Coniu - Tăng thành viên nhóm */
        $group['group_members'] = increase_members_groups($group['group_name'], $group['group_members']);

        /* get group picture */
        $group['group_picture_default'] = ($group['group_picture'])? false : true;
        $group['group_picture'] = $this->get_picture($group['group_picture'], 'group');
        /* check group category */
        $group['group_category_name'] = (!$group['group_category_name'])? __('N/A'): $group['group_category_name']; /* in case deleted by system admin */
        /* get the connection */
        $group['i_admin'] = $this->check_group_adminship($this->_data['user_id'], $group['group_id']);
        $group['i_joined'] = $this->check_group_membership($this->_data['user_id'], $group['group_id']);

        /* get group requests */
        if($group['group_privacy'] != "public") {
            $group['total_requests'] = $this->get_group_requests_total($group['group_id']);
        }

        /* check group privacy */
        if($group['group_privacy'] == "secret") {
            if($group['i_joined'] != "approved" && $group['group_admin'] != $this->_data['user_id']) {
                _api_error(404);
            }
        }

        if($group['group_privacy'] == "closed") {
            if($group['i_joined'] != "approved" && !$group['i_admin']) {
                if($group['group_members'] > 0) {
                    $group['members'] = $this->get_group_members($group['group_id']);
                }
            }
        } else {
            /* get invites */
            $group['invites'] = $this->get_group_invites($group['group_id']);

            /* get photos */
            $group['photos'] = $this->get_photos($group['group_id'], 'group');

            /* get pinned post */
            $group['pinned_post'] = $this->get_post($group['group_pinned_post']);


            /* get posts */
            $group['posts'] = $this->get_posts( array('get' => 'posts_group', 'id' => $group['group_id']) );
        }

        return $group;
    }



    /**
     * create_group
     *
     * @param array $args
     * @return void
     */
    public function create_group($args = []) {
        global $db, $system, $date;
        /* check if groups enabled */
        if($this->_data['user_group'] >= 3 && !$system['groups_enabled']) {
            throw new Exception(__("This feature has been disabled by the admin"));
        }
        /* validate title */
        if(is_empty($args['title'])) {
            throw new Exception(__("You must enter a name for your group"));
        }
        if(strlen($args['title']) < 3) {
            throw new Exception(__("Group name must be at least 3 characters long. Please try another"));
        }
        /* validate username */
        if(is_empty($args['username'])) {
            throw new Exception(__("You must enter a web address for your group"));
        }
        if(!valid_username($args['username'])) {
            throw new Exception(__("Please enter a valid web address (a-z0-9_.) with minimum 3 characters long"));
        }
        if(reserved_username($args['username'])) {
            throw new Exception(__("You can't use")." ".$args['username']." ".__("as web address"));
        }
        if($this->check_username($args['username'], 'group')) {
            throw new Exception(__("Sorry, it looks like this web address")." ".$args['username']." ".__("belongs to an existing group"));
        }
        /* validate category */
        if(is_empty($args['category'])) {
            throw new Exception(__("You must select valid category for your group"));
        }
        $check = $db->query(sprintf("SELECT * FROM groups_categories WHERE category_id = %s", secure($args['category'], 'int') )) or _error(SQL_ERROR_THROWEN);
        if($check->num_rows == 0) {
            throw new Exception(__("You must select valid category for your group"));
        }
        /* validate privacy */
        if(!in_array($args['privacy'], array('secret','closed','public'))) {
            throw new Exception(__("You must select a valid privacy for your group"));
        }
        /* set custom fields */
        $custom_fields = $this->set_custom_fields($args, "group");
        /* insert new group */
        $db->query(sprintf("INSERT INTO groups (group_privacy, group_admin, group_name, group_category, group_title, group_description, group_date) VALUES (%s, %s, %s, %s, %s, %s, %s)", secure($args['privacy']), secure($this->_data['user_id'], 'int'), secure($args['username']), secure($args['category']), secure($args['title']), secure($args['description']), secure($date) )) or _error(SQL_ERROR_THROWEN);
        /* get group_id */
        $group_id = $db->insert_id;
        /* insert custom fields values */
        if($custom_fields) {
            foreach($custom_fields as $field_id => $value) {
                $db->query(sprintf("INSERT INTO custom_fields_values (value, field_id, node_id, node_type) VALUES (%s, %s, %s, 'group')", secure($value), secure($field_id, 'int'), secure($group_id, 'int') ));
            }
        }
        /* join the group */
        $this->connect("group-join", $group_id);
        /* group admin addation */
        $this->connect("group-admin-addation", $group_id, $this->_data['user_id']);
    }


    /**
     * edit_group
     *
     * @param integer $group_id
     * @param array $args
     * @return void
     */
    public function edit_group($group_id, $args) {
        global $db, $system;
        /* check if groups enabled */
        if($this->_data['user_group'] >= 3 && !$system['groups_enabled']) {
            throw new Exception(__("This feature has been disabled by the admin"));
        }
        /* (check&get) group */
        $get_group = $db->query(sprintf("SELECT * FROM groups WHERE group_id = %s", secure($group_id, 'int') )) or _error(SQL_ERROR_THROWEN);
        if($get_group->num_rows == 0) {
            _error(403);
        }
        $group = $get_group->fetch_assoc();
        /* check permission */
        $can_edit = false;
        /* viewer is (admin|moderator) */
        if($this->_data['user_group'] < 3) {
            $can_edit = true;
            /* viewer is the admin of group */
        } elseif($this->check_group_adminship($this->_data['user_id'], $group_id)) {
            $can_edit = true;
        }
        if(!$can_edit) {
            _error(403);
        }
        /* validate title */
        if(is_empty($args['title'])) {
            throw new Exception(__("You must enter a name for your group"));
        }
        if(strlen($args['title']) < 3) {
            throw new Exception(__("Group name must be at least 3 characters long. Please try another"));
        }
        /* validate username */
        if(strtolower($args['username']) != strtolower($group['group_name'])) {
            if(is_empty($args['username'])) {
                throw new Exception(__("You must enter a web address for your group"));
            }
            if(!valid_username($args['username'])) {
                throw new Exception(__("Please enter a valid web address (a-z0-9_.) with minimum 3 characters long"));
            }
            if(reserved_username($args['username'])) {
                throw new Exception(__("You can't use")." ".$args['username']." ".__("as web address"));
            }
            if($this->check_username($args['username'], 'group')) {
                throw new Exception(__("Sorry, it looks like this web address")." ".$args['username']." ".__("belongs to an existing group"));
            }
        }
        /* validate privacy */
        if(!in_array($args['privacy'], array('secret','closed','public'))) {
            throw new Exception(__("You must select a valid privacy for your group"));
        }
        /* validate category */
        if(is_empty($args['category'])) {
            throw new Exception(__("You must select valid category for your group"));
        }
        $check = $db->query(sprintf("SELECT * FROM groups_categories WHERE category_id = %s", secure($args['category'], 'int') )) or _error(SQL_ERROR_THROWEN);
        if($check->num_rows == 0) {
            throw new Exception(__("You must select valid category for your group"));
        }
        /* set custom fields */
        $custom_fields = $this->set_custom_fields($args, "group" ,"settings", $group_id);
        /* update the group */
        $db->query(sprintf("UPDATE groups SET group_privacy = %s, group_category = %s, group_name = %s, group_title = %s, group_description = %s WHERE group_id = %s", secure($args['privacy']), secure($args['category']), secure($args['username']), secure($args['title']), secure($args['description']), secure($group_id, 'int') )) or _error(SQL_ERROR_THROWEN);
    }


    /**
     * delete_group
     *
     * @param integer $group_id
     * @return void
     */
    public function delete_group($group_id) {
        global $db, $system;
        /* check if groups enabled */
        if($this->_data['user_group'] >= 3 && !$system['groups_enabled']) {
            throw new Exception(__("This feature has been disabled by the admin"));
        }
        /* (check&get) group */
        $get_group = $db->query(sprintf("SELECT * FROM groups WHERE group_id = %s", secure($group_id, 'int') )) or _error(SQL_ERROR_THROWEN);
        if($get_group->num_rows == 0) {
            _error(403);
        }
        $group = $get_group->fetch_assoc();
        // delete group
        $can_delete = false;
        /* viewer is (admin|moderator) */
        if($this->_data['user_group'] < 3) {
            $can_delete = true;
        }
        /* viewer is the super admin of group */
        if($this->_data['user_id'] == $group['group_admin']) {
            $can_delete = true;
        }
        /* check if viewer can delete the group */
        if(!$can_delete) {
            _error(403);
        }
        /* delete the group */
        $db->query(sprintf("DELETE FROM groups WHERE group_id = %s", secure($group_id, 'int') )) or _error(SQL_ERROR_THROWEN);
        /* delete the group members */
        $db->query(sprintf("DELETE FROM groups_members WHERE group_id = %s", secure($group_id, 'int') )) or _error(SQL_ERROR_THROWEN);
        /* delete the group admins */
        $db->query(sprintf("DELETE FROM groups_admins WHERE group_id = %s", secure($group_id, 'int') )) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * get_group_admins_ids
     *
     * @param integer $group_id
     * @return array
     */
    public function get_group_admins_ids($group_id) {
        global $db;
        $admins = array();
        $get_admins = $db->query(sprintf("SELECT user_id FROM groups_admins WHERE group_id = %s", secure($group_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        if($get_admins->num_rows > 0) {
            while($admin = $get_admins->fetch_assoc()) {
                $admins[] = $admin['user_id'];
            }
        }

        // Lấy group_admin trong bảng group
        $group_admin = $db->query(sprintf("SELECT group_admin FROM groups WHERE group_id = %s", secure($group_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        if($group_admin->num_rows > 0) {
            while($admin = $group_admin->fetch_assoc()) {
                $admins[] = $admin['group_admin'];
            }
        }
        // Lấy danh sách hiệu trưởng của trường
        $class = getClassData($group_id, CLASS_INFO);
        $schoolConfig = getSchoolData($class['school_id'], SCHOOL_CONFIG);
        $principals = array();
        if($schoolConfig['principal'] != '') {
            $principals = explode(',', $schoolConfig['principal']);
        }

        // Lấy danh sách giáo viên của lớp
        $teachers = getClassData($group_id, CLASS_TEACHERS);
        $teacherIds = array_keys($teachers);

        // Lấy danh sách những người được phân quyền quản lý group của trường
        $strSql = sprintf("SELECT UR.user_id FROM ci_user_role UR 
                  INNER JOIN ci_role_permission RP ON RP.role_id = UR.role_id
                  WHERE UR.school_id = %s AND RP.value = %s AND RP.module = %s", secure($class['school_id'], 'int'), PERM_EDIT, secure("managegroups"));
        $get_user_ids = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $userIds = array();
        if($get_user_ids->num_rows > 0) {
            while($userId = $get_user_ids->fetch_assoc()['user_id']) {
                $userIds[] = $userId;
            }
        }
        $admins = array_merge($principals, $admins, $teacherIds, $userIds);
        // page_admin
        $schoolInfo = getSchoolData($class['school_id'], SCHOOL_INFO);
        $page_admin = $schoolInfo['page_admin'];
        $admins[] = $page_admin;
        $admins = array_unique($admins);
        return $admins;
    }

    /**
     * Lấy danh sách những admin thêm vào từ bên quản lý
     *
     * @param $group_id
     * @return array
     * @throws Exception
     */
    public function get_group_admins_ids_by_school($group_id) {
        global $db;
        $admins = array();

        // Lấy group_admin trong bảng group
        $group_admin = $db->query(sprintf("SELECT group_admin FROM groups WHERE group_id = %s", secure($group_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        if($group_admin->num_rows > 0) {
            while($admin = $group_admin->fetch_assoc()) {
                $admins[] = $admin['group_admin'];
            }
        }
        // Lấy danh sách hiệu trưởng của trường
        $class = getClassData($group_id, CLASS_INFO);
        $schoolConfig = getSchoolData($class['school_id'], SCHOOL_CONFIG);
        $principals = array();
        if($schoolConfig['principal'] != '') {
            $principals = explode(',', $schoolConfig['principal']);
        }

        // Lấy danh sách giáo viên của lớp
        $teachers = getClassData($group_id, CLASS_TEACHERS);
        $teacherIds = array_keys($teachers);

        // Lấy danh sách những người được phân quyền quản lý group của trường
        $strSql = sprintf("SELECT UR.user_id FROM ci_user_role UR 
                  INNER JOIN ci_role_permission RP ON RP.role_id = UR.role_id
                  WHERE UR.school_id = %s AND RP.value = %s AND RP.module = %s", secure($class['school_id'], 'int'), PERM_EDIT, secure("managegroups"));
        $get_user_ids = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $userIds = array();
        if($get_user_ids->num_rows > 0) {
            while($userId = $get_user_ids->fetch_assoc()['user_id']) {
                $userIds[] = $userId;
            }
        }
        $admins = array_merge($principals, $admins, $teacherIds, $userIds);
        // page_admin
        $schoolInfo = getSchoolData($class['school_id'], SCHOOL_INFO);
        $page_admin = $schoolInfo['page_admin'];
        $admins[] = $page_admin;
        $admins = array_unique($admins);
        return $admins;
    }

    /**
     * get_group_admins
     *
     * @param integer $group_id
     * @param integer $offset
     * @return array
     */

    // Comment đoạn này, viết hàm khác - Taila
//    public function get_group_admins($group_id, $offset = 0) {
//        global $db, $system;
//        $admins = array();
//
//        if($offset == 0) {
//            // Lấy danh sách hiệu trưởng + giáo viên quản lý lớp + group_admin
//            $class = getClassData($group_id, CLASS_INFO);
//            $school = getSchoolData($class['school_id'], SCHOOL_CONFIG);
//            $strCon = '';
//            if($school['principal'] != '') {
//                $strCon = $school['principal'];
//            }
//            if($strCon != '') {
//                $get_admins = $db->query(sprintf("SELECT user_id, user_name, user_fullname, user_firstname, user_lastname, user_gender, user_picture FROM users WHERE user_id IN (%s)", $strCon)) or _error(SQL_ERROR_THROWEN);
//                if($get_admins->num_rows > 0) {
//                    while($admin = $get_admins->fetch_assoc()) {
//                        $admin['user_picture'] = $this->get_picture($admin['user_picture'], $admin['user_gender']);
//                        /* get the connection */
//                        $admin['i_admin'] = true;
//                        $admin['connection'] = 'group_manage';
//                        $admin['node_id'] = $group_id;
//                        $admins[] = $admin;
//                    }
//                }
//            }
//
//            // Lấy danh sách giáo viên quản lý lớp
//            $teachers = getClassData($group_id, CLASS_TEACHERS);
//            if(count($teachers) > 0) {
//                foreach ($teachers as $admin) {
//                    $admin['user_picture'] = $this->get_picture($admin['user_picture'], $admin['user_gender']);
//                    /* get the connection */
//                    $admin['i_admin'] = true;
//                    $admin['connection'] = 'group_manage';
//                    $admin['node_id'] = $group_id;
//                    $admins[] = $admin;
//                }
//            }
//
//            // Lấy page_admin - còn tiếp - Nhưng nghĩ ra cách khác nên để tạm đây, nếu làm theo cách cũ thì viết tiếp - TaiLa
//        }
//        $offset *= $system['max_results_even'];
//        $get_admins = $db->query(sprintf("SELECT users.user_id, users.user_name, users.user_fullname, users.user_firstname, users.user_lastname, users.user_gender, users.user_picture FROM groups_admins INNER JOIN users ON groups_admins.user_id = users.user_id WHERE groups_admins.group_id = %s LIMIT %s, %s", secure($group_id, 'int'), secure($offset, 'int', false), secure($system['max_results_even'], 'int', false) )) or _error(SQL_ERROR_THROWEN);
//        if($get_admins->num_rows > 0) {
//            while($admin = $get_admins->fetch_assoc()) {
//                $admin['user_picture'] = $this->get_picture($admin['user_picture'], $admin['user_gender']);
//                /* get the connection */
//                $admin['i_admin'] = true;
//                $admin['connection'] = 'group_manage';
//                $admin['node_id'] = $group_id;
//                $admins[] = $admin;
//            }
//        }
//        return $admins;
//    }
//
    // Viết hàm này thay cho đoạn trên - Taila
    public function get_group_admins($group_id, $offset = 0) {
        global $db, $system;

        // Lấy danh sách adminIds - Cái này có sẵn
        $adminIds = $this->get_group_admins_ids($group_id);
        $strCon = '';
        if(count($adminIds) > 0) {
            $strCon = implode(',', $adminIds);
        }
        $offset *= $system['max_results_even'];
        $get_admins = $db->query(sprintf("SELECT user_id, user_name, user_fullname, user_firstname, user_lastname, user_gender, user_picture FROM users WHERE user_id IN (%s) LIMIT %s, %s", $strCon, secure($offset, 'int', false), secure($system['max_results_even'], 'int', false) )) or _error(SQL_ERROR_THROWEN);
        if($get_admins->num_rows > 0) {
            while($admin = $get_admins->fetch_assoc()) {
                $admin['user_picture'] = $this->get_picture($admin['user_picture'], $admin['user_gender']);
                /* get the connection */
                $admin['i_admin'] = true;
                $admin['connection'] = 'group_manage';
                $admin['node_id'] = $group_id;
                $admins[] = $admin;
            }
        }
        return $admins;
    }

    /**
     * get_group_members
     *
     * @param integer $group_id
     * @param integer $offset
     * @param boolean $manage
     * @return array
     */
    public function get_group_members($group_id, $offset = 0, $manage = false) {
        global $db, $system;
        $members = array();
        $offset *= $system['max_results_even'];
        $get_members = $db->query(sprintf("SELECT users.user_id, users.user_name, users.user_fullname, users.user_firstname, users.user_lastname, users.user_gender, users.user_picture FROM groups_members INNER JOIN users ON groups_members.user_id = users.user_id WHERE groups_members.approved = '1' AND groups_members.group_id = %s LIMIT %s, %s", secure($group_id, 'int'), secure($offset, 'int', false), secure($system['max_results_even'], 'int', false) )) or _error(SQL_ERROR_THROWEN);
        if($get_members->num_rows > 0) {
            /* get group admins ids */
            $group_admins_ids = $this->get_group_admins_ids($group_id);
            while($member = $get_members->fetch_assoc()) {
                $member['user_picture'] = $this->get_picture($member['user_picture'], $member['user_gender']);
                if($manage) {
                    /* get the connection */
                    $member['i_admin'] = in_array($member['user_id'], $group_admins_ids);
                    $member['connection'] = 'group_manage';
                    $member['node_id'] = $group_id;
                } else {
                    /* get the connection between the viewer & the target */
                    $member['connection'] = $this->connection($member['user_id']);
                }
                $members[] = $member;
            }
        }
        return $members;
    }


    /**
     * get_group_invites
     *
     * @param integer $group_id
     * @param integer $offset
     * @return array
     */
    public function get_group_invites($group_id, $offset = 0) {
        global $db, $system;
        $friends = array();
        $offset *= $system['max_results_even'];
        if($this->_data['friends_ids']) {
            $get_members = $db->query(sprintf("SELECT user_id FROM groups_members WHERE group_id = %s", secure($group_id, 'int'))) or _error(SQL_ERROR_THROWEN);
            if($get_members->num_rows > 0) {
                while($member = $get_members->fetch_assoc()) {
                    $members[] = $member['user_id'];
                }
                $invites_list = implode(',',array_diff($this->_data['friends_ids'], $members));
                if($invites_list) {
                    $get_friends = $db->query(sprintf("SELECT user_id, user_name, user_fullname, user_firstname, user_lastname, user_gender, user_picture FROM users WHERE user_id IN ($invites_list) LIMIT %s, %s", secure($offset, 'int', false), secure($system['max_results_even'], 'int', false) )) or _error(SQL_ERROR_THROWEN);
                    while($friend = $get_friends->fetch_assoc()) {
                        $friend['user_picture'] = $this->get_picture($friend['user_picture'], $friend['user_gender']);
                        $friend['connection'] = 'group_invite';
                        $friend['node_id'] = $group_id;
                        $friends[] = $friend;
                    }
                }
            }
        }
        return $friends;
    }


    /**
     * get_group_requests
     *
     * @param integer $group_id
     * @param integer $offset
     * @return array
     */
    public function get_group_requests($group_id, $offset = 0) {
        global $db, $system;
        $requests = array();
        $offset *= $system['max_results'];
        $get_requests = $db->query(sprintf("SELECT users.user_id, users.user_name, users.user_fullname, users.user_firstname, users.user_lastname, users.user_gender, users.user_picture FROM users INNER JOIN groups_members ON users.user_id = groups_members.user_id WHERE groups_members.approved = '0' AND groups_members.group_id = %s LIMIT %s, %s", secure($group_id, 'int'), secure($offset, 'int', false), secure($system['max_results'], 'int', false) )) or _error(SQL_ERROR_THROWEN);
        if($get_requests->num_rows > 0) {
            while($request = $get_requests->fetch_assoc()) {
                $request['user_picture'] = $this->get_picture($request['user_picture'], $request['user_gender']);
                $request['connection'] = 'group_request';
                $request['node_id'] = $group_id;
                $requests[] = $request;
            }
        }
        return $requests;
    }


    /**
     * get_group_requests_total
     *
     * @param integer $group_id
     * @return integer
     */
    public function get_group_requests_total($group_id) {
        global $db, $system;
        $get_requests = $db->query(sprintf("SELECT  * FROM groups_members WHERE approved = '0' AND group_id = %s", secure($group_id, 'int') )) or _error(SQL_ERROR_THROWEN);
        return $get_requests->num_rows;
    }


    /**
     * check_group_adminship
     *
     * @param integer $user_id
     * @param integer $group_id
     * @return boolean
     */
    public function check_group_adminship($user_id, $group_id) {
        if($this->_logged_in) {
            /* get group admins ids */
            $group_admins_ids = $this->get_group_admins_ids($group_id);
            if(in_array($user_id, $group_admins_ids)) {
                return true;
            }
        }
        return false;
    }


    /**
     * check_group_membership
     *
     * @param integer $user_id
     * @param integer $group_id
     * @return mixed
     */
    public function check_group_membership($user_id, $group_id) {
        global $db;
        if($this->_logged_in) {
            $get_membership = $db->query(sprintf("SELECT * FROM groups_members WHERE user_id = %s AND group_id = %s", secure($user_id, 'int'), secure($group_id, 'int') )) or _error(SQL_ERROR_THROWEN);
            if($get_membership->num_rows > 0) {
                $membership = $get_membership->fetch_assoc();
                return ($membership['approved'] == '1')? "approved": "pending";
            }
        }
        return false;
    }


    /* ------------------------------- */
    /* Popovers */
    /* ------------------------------- */

    /**
     * popover
     * 
     * @param integer $id
     * @param string $type
     * @return array
     */
    public function popover($id, $type) {
        global $db;
        $profile = array();
        /* check the type to get */
        if($type == "user") {
            /* get user info */
            $get_profile = $db->query(sprintf("SELECT %s FROM users WHERE user_id = %s", PARAM_PROFILE, secure($id, 'int'))) or _error(SQL_ERROR_THROWEN);
            if($get_profile->num_rows > 0) {
                $profile = $get_profile->fetch_assoc();
                /* get profile picture */
                $profile['user_picture'] = $this->get_picture($profile['user_picture'], $profile['user_gender']);
                /* get followers count */
                $profile['followers_count'] = count($this->get_followers_ids($profile['user_id']));
                /* get mutual friends count between the viewer and the target */
                if($this->_logged_in && $this->_data['user_id'] != $profile['user_id']) {
                    $profile['mutual_friends_count'] = $this->get_mutual_friends_count($profile['user_id']);
                }
                /* get the connection between the viewer & the target */
                if($profile['user_id'] != $this->_data['user_id']) {
                    $profile['we_friends'] = (in_array($profile['user_id'], $this->_data['friends_ids']))? true: false;
                    $profile['he_request'] = (in_array($profile['user_id'], $this->_data['friend_requests_ids']))? true: false;
                    $profile['i_request'] = (in_array($profile['user_id'], $this->_data['friend_requests_sent_ids']))? true: false;
                    $profile['i_follow'] = (in_array($profile['user_id'], $this->_data['followings_ids']))? true: false;
                }
            }
        } else {
            /* get page info */
            $get_profile = $db->query(sprintf("SELECT * FROM pages WHERE page_id = %s", secure($id, 'int') )) or _error(SQL_ERROR_THROWEN);
            if($get_profile->num_rows > 0) {
                $profile = $get_profile->fetch_assoc();
                $profile['page_picture'] = get_picture($profile['page_picture'], "page");
                /* get the connection between the viewer & the target */
                $get_likes = $db->query(sprintf("SELECT * FROM pages_likes WHERE page_id = %s AND user_id = %s", secure($id, 'int'), secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                if($get_likes->num_rows > 0) {
                    $profile['i_like'] = true;
                } else {
                    $profile['i_like'] = false;
                }
            }
        }
        return $profile;
    }



    /* ------------------------------- */
    /* Settings */
    /* ------------------------------- */

    /**
     * settings
     * 
     * @param array $args
     * @return void
     */
    public function settings(array $args = array()) {
        global $db, $system;

        switch ($args['edit']) {
            case 'username':
                
                /* validate username */
                if(strtolower($args['username']) != strtolower($this->_data['user_name'])) {
                    if(is_numeric($args['username'])) {
                        throw new Exception(__("Usernames can't only contain numeric characters"));
                    }
                    if(!valid_username($args['username'], "user")) {
                        throw new Exception(__("Please enter a valid username (a-z0-9_.) with minimum 3 characters long"));
                    }
                    if(reserved_username($args['username'])) {
                        throw new Exception(__("You can't use")." ".$args['username']." ".__("as username"));
                    }
                    if($this->check_username($args['username'])) {
                        throw new Exception(__("Sorry, it looks like")." ".$args['username']." ".__("belongs to an existing account"));
                    }
                    /* update user */
                    $db->query(sprintf("UPDATE users SET user_name = %s WHERE user_id = %s", secure($args['username']), secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);

                    /* CI - Firebase */
                    $arr = array(
                        'user_id' => $this->_data['user_id'],
                        'action' => UPDATE_USER,
                        'user_name' => $args['username']
                    );
                    insertBackgroundFirebase($arr);
                    /* END - Firebase */
                }
                break;

            case 'email':
                /* validate email */
                if($args['email'] != $this->_data['user_email']) {
                    if(!valid_email($args['email'])) {
                        throw new Exception(__("Please enter a valid email address"));
                    }
                    if($this->check_email($args['email'])) {
                        throw new Exception(__("Sorry, it looks like")." ".$args['email']." ".__("belongs to an existing account"));
                    }
                    /* generate activation key */
                    $activation_key = get_hash_token();
                    /* update user */
                    //$db->query(sprintf("UPDATE users SET user_email = %s, user_activation_key = %s, user_activated = '0' WHERE user_id = %s", secure($args['email']), secure($activation_key), secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                    $db->query(sprintf("UPDATE users SET user_email_activation = %s, user_activation_key = %s WHERE user_id = %s", secure($args['email']), secure($activation_key), secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);

                    // send activation key
                    /* prepare activation email */
                    $subject = __("Just one more step to get started on")." ".$system['system_title'];
                    $body  = __("Hi")." ".ucwords(convertText4Web($this->_data['user_fullname'])).",";
                    $body .= "\r\n\r\n".__("To complete the sign-up process, please follow this link:");
                    $body .= "\r\n\r\n".$system['system_url']."/activation/".$this->_data['user_id']."/".$activation_key;
                    $body .= "\r\n\r\n".__("Welcome to")." ".$system['system_title'];
                    $body .= "\r\n\r".$system['system_title']." ".__("Team");
                    /* send email */
                    if(!_email($args['email'], $subject, $body)) {
                        throw new Exception(__("New activation email could not be sent"));
                    }
                }
                break;

            case 'phone':
                /* validate phone */
                if($args['phone'] != $this->_data['user_phone_signin']) {
                    if($this->check_phone_setting($args['phone'])) {
                        throw new Exception(__("Sorry, it looks like")." ".$args['phone']." ".__("belongs to an existing account"));
                    }
                    /* update user */
                    $db->query(sprintf("UPDATE users SET user_phone = %s, user_phone_signin = %s WHERE user_id = %s", secure($args['phone']), secure($args['phone']), secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                }
                break;

            case 'password':
                /* validate all fields */
                if(is_empty($args['current']) || is_empty($args['new']) || is_empty($args['confirm'])) {
                    throw new Exception(__("You must fill in all of the fields"));
                }
                /* validate current password (MD5 check for versions < v2.5) */
                if(md5($args['current']) != $this->_data['user_password'] && !password_verify($args['current'], $this->_data['user_password'])) {
                    throw new Exception(__("Your current password is incorrect"));
                }
                /* validate new password */
                if($args['new'] != $args['confirm']) {
                    throw new Exception(__("Your passwords do not match"));
                }
                if(strlen($args['new']) < 6) {
                    throw new Exception(__("Password must be at least 6 characters long. Please try another"));
                }
                /* update user */
                $db->query(sprintf("UPDATE users SET user_password = %s WHERE user_id = %s", secure(_password_hash($args['new'])), secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                break;

            case 'social_account':
                if(is_empty($args['new']) || is_empty($args['confirm'])) {
                    throw new Exception(__("You must fill in all of the fields"));
                }
                /* validate new password */
                if($args['new'] != $args['confirm']) {
                    throw new Exception(__("Your passwords do not match"));
                }
                if(strlen($args['new']) < 6) {
                    throw new Exception(__("Your password must be at least 6 characters long. Please try another"));
                }

                /* update user */
                $db->query(sprintf("UPDATE users SET user_password = %s, user_changed_pass = '1' WHERE user_id = %s", secure(_password_hash($args['new'])), secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);

                break;

            case 'basic':
                /* validate firstname */
                if(is_empty($args['firstname'])) {
                    throw new Exception(__("You must enter first name"));
                }
                if(!valid_name($args['firstname'])) {
                    throw new Exception(__("First name contains invalid characters"));
                }
                /*if(strlen($args['firstname']) < 3) {
                    throw new Exception(__("First name must be at least 3 characters long. Please try another"));
                }*/
                /* validate lastname */
                if(is_empty($args['lastname'])) {
                    throw new Exception(__("You must enter last name"));
                }
                if(!valid_name($args['lastname'])) {
                    throw new Exception(__("Last name contains invalid characters"));
                }
                /*if(strlen($args['lastname']) < 3) {
                    throw new Exception(__("Last name must be at least 3 characters long. Please try another"));
                }*/
                /* validate gender */
                if(!in_array($args['gender'], array('male', 'female'))) {
                    throw new Exception(__("Please select a valid gender"));
                }

                if(!in_array($args['birth_month'], range(1, 12))) {
                    throw new Exception(__("Please select a valid birth month"));
                }
                if(!in_array($args['birth_day'], range(1, 31))) {
                    throw new Exception(__("Please select a valid birth day"));
                }
                if(!in_array($args['birth_year'], range(1905, 2015))) {
                    throw new Exception(__("Please select a valid birth year"));
                }
                $args['birth_date'] = $args['birth_year'].'-'.$args['birth_month'].'-'.$args['birth_day'];

                /* CI - Kiểm tra người dùng  lớn hơn 14 tuổi */
                $interval = date_diff(date_create(date("Y-m-d")), date_create($args['birth_date']));
                $yearold = $interval->format('%Y');;
                if ($yearold < 14) {
                    throw new Exception(__("14 tuổi"));
                }

                $full_name = $args['lastname'] . " " . $args['firstname'];
                /* CI - update user (thêm user_fullname) */
                $db->query(sprintf("UPDATE users SET user_fullname = %s, user_firstname = %s, user_lastname = %s, user_gender = %s, user_birthdate = %s WHERE user_id = %s", secure($full_name), secure($args['firstname']), secure($args['lastname']), secure($args['gender']), secure($args['birth_date']), secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);

                /* CI - Firebase */
                $arr = array(
                    'user_id' => $this->_data['user_id'],
                    'action' => UPDATE_USER,
                    'user_fullname' => $full_name
                );
                insertBackgroundFirebase($arr);
                /* END - Firebase */

                break;

            case 'work':
                /* validate work website */
                if(!is_empty($args['work_url'])) {
                    if(!valid_url($args['work_url'])) {
                        throw new Exception(__("Please enter a valid work website"));
                    }
                } else {
                    $args['work_url'] = 'null';
                }

                /* update user */
                $db->query(sprintf("UPDATE users SET user_work_title = %s, user_work_place = %s, user_work_url = %s WHERE user_id = %s", secure($args['work_title']), secure($args['work_place']), secure($args['work_url']), secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                break;

            case 'location':
                /* update user */
                $db->query(sprintf("UPDATE users SET user_current_city = %s, user_hometown = %s WHERE user_id = %s", secure($args['city']), secure($args['hometown']), secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                break;

            case 'education':
                /* update user */
                $db->query(sprintf("UPDATE users SET user_edu_major = %s, user_edu_school = %s, user_edu_class = %s WHERE user_id = %s", secure($args['edu_major']), secure($args['edu_school']), secure($args['edu_class']), secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                break;

            case 'privacy':
                $args['privacy_chat'] = ($args['privacy_chat'] == 0)? 0 : 1;
                $privacy = array('me', 'friends', 'public');
                //if(!in_array($args['privacy_wall'], $privacy) || !in_array($args['privacy_birthdate'], $privacy) || !in_array($args['privacy_relationship'], $privacy) || !in_array($args['privacy_basic'], $privacy) || !in_array($args['privacy_work'], $privacy) || !in_array($args['privacy_location'], $privacy) || !in_array($args['privacy_education'], $privacy) || !in_array($args['privacy_other'], $privacy) || !in_array($args['privacy_friends'], $privacy) || !in_array($args['privacy_photos'], $privacy) || !in_array($args['privacy_pages'], $privacy) || !in_array($args['privacy_groups'], $privacy) || !in_array($args['privacy_events'], $privacy)) {
                if(!in_array($args['privacy_wall'], $privacy) || !in_array($args['privacy_birthdate'], $privacy) || !in_array($args['privacy_relationship'], $privacy) || !in_array($args['privacy_basic'], $privacy) || !in_array($args['privacy_work'], $privacy) || !in_array($args['privacy_location'], $privacy) || !in_array($args['privacy_education'], $privacy) || !in_array($args['privacy_other'], $privacy) || !in_array($args['privacy_friends'], $privacy) || !in_array($args['privacy_photos'], $privacy) || !in_array($args['privacy_pages'], $privacy) || !in_array($args['privacy_groups'], $privacy) ) {
                    _error(400);
                }
                /* update user */
                //$db->query(sprintf("UPDATE users SET user_chat_enabled = %s, user_privacy_wall = %s, user_privacy_birthdate = %s, user_privacy_relationship = %s, user_privacy_basic = %s, user_privacy_work = %s, user_privacy_location = %s, user_privacy_education = %s, user_privacy_other = %s, user_privacy_friends = %s, user_privacy_photos = %s, user_privacy_pages = %s, user_privacy_groups = %s, user_privacy_events = %s WHERE user_id = %s", secure($args['privacy_chat']), secure($args['privacy_wall']), secure($args['privacy_birthdate']), secure($args['privacy_relationship']), secure($args['privacy_basic']), secure($args['privacy_work']), secure($args['privacy_location']), secure($args['privacy_education']), secure($args['privacy_other']), secure($args['privacy_friends']), secure($args['privacy_photos']), secure($args['privacy_pages']), secure($args['privacy_groups']), secure($args['privacy_events']), secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                $db->query(sprintf("UPDATE users SET user_chat_enabled = %s, user_privacy_wall = %s, user_privacy_birthdate = %s, user_privacy_relationship = %s, user_privacy_basic = %s, user_privacy_work = %s, user_privacy_location = %s, user_privacy_education = %s, user_privacy_other = %s, user_privacy_friends = %s, user_privacy_photos = %s, user_privacy_pages = %s, user_privacy_groups = %s WHERE user_id = %s", secure($args['privacy_chat']), secure($args['privacy_wall']), secure($args['privacy_birthdate']), secure($args['privacy_relationship']), secure($args['privacy_basic']), secure($args['privacy_work']), secure($args['privacy_location']), secure($args['privacy_education']), secure($args['privacy_other']), secure($args['privacy_friends']), secure($args['privacy_photos']), secure($args['privacy_pages']), secure($args['privacy_groups']), secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                break;

            case 'chat':
                $args['privacy_chat'] = ($args['privacy_chat'] == 0)? 0 : 1;
                /* update user */
                $db->query(sprintf("UPDATE users SET user_chat_enabled = %s WHERE user_id = %s", secure($args['privacy_chat']), secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                break;

            case 'started_profile':
                if(is_empty($args['identification_card_number'])) {
                    throw new Exception(__("Please enter identification card number"));
                }
                if(is_empty($args['date_of_issue'])) {
                    throw new Exception(__("Please enter date of issue"));
                }
                if(is_empty($args['place_of_issue'])) {
                    throw new Exception(__("Please enter place of issue"));
                }
                $args['date_of_issue'] = toDBDate($args['date_of_issue']);
                $args['birth_date'] = isset($args['birth_date']) ? toDBDate($args['birth_date']) : 'null';

                /* CI - Kiểm tra người dùng  lớn hơn 14 tuổi */
                $interval = date_diff(date_create(date("Y-m-d")), date_create($args['birth_date']));
                $yearold = $interval->format('%Y');
                if ($yearold < 14) {
                    throw new Exception(__("Bạn không thể sử dụng Coniu nếu chưa đủ 14 tuổi"));
                }
                if(isset($args['password'])) {
                    if (strlen($args['password']) < 6) {
                        throw new Exception(__("Your password must be at least 6 characters long. Please try another"));
                    }
                    /* update user */
                    $db->query(sprintf("UPDATE users SET user_id_card_number = %s, date_of_issue = %s, place_of_issue = %s, user_birthdate = %s, user_password = %s, user_changed_pass = '1', user_started = '1' WHERE user_id = %s", secure($args['identification_card_number']), secure($args['date_of_issue']), secure($args['place_of_issue']), secure($args['birth_date']), secure(_password_hash($args['password'])), secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                } else {
                    /* update user */
                    $db->query(sprintf("UPDATE users SET user_id_card_number = %s, date_of_issue = %s, place_of_issue = %s, user_birthdate = %s, user_started = '1' WHERE user_id = %s", secure($args['identification_card_number']), secure($args['date_of_issue']), secure($args['place_of_issue']), secure($args['birth_date']), secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                }
                break;

            case 'started_profile_sms':
                /* validate firstname */
                if(is_empty($args['firstname'])) {
                    throw new Exception(__("You must enter first name"));
                }
                if(!valid_name($args['firstname'])) {
                    throw new Exception(__("First name contains invalid characters"));
                }

                /* validate lastname */
                if(is_empty($args['lastname'])) {
                    throw new Exception(__("You must enter last name"));
                }
                if(!valid_name($args['lastname'])) {
                    throw new Exception(__("Last name contains invalid characters"));
                }

                if(is_empty($args['identification_card_number'])) {
                    throw new Exception(__("Please enter identification card number"));
                }

                if(is_empty($args['date_of_issue'])) {
                    throw new Exception(__("Please enter date of issue"));
                }
                if(is_empty($args['place_of_issue'])) {
                    throw new Exception(__("Please enter place of issue"));
                }
                $args['date_of_issue'] = toDBDate($args['date_of_issue']);
                $args['birth_date'] = isset($args['birth_date']) ? toDBDate($args['birth_date']) : 'null';

                /* CI - Kiểm tra người dùng  lớn hơn 14 tuổi */
                $interval = date_diff(date_create(date("Y-m-d")), date_create($args['birth_date']));
                $yearold = $interval->format('%Y');
                if ($yearold < 14) {
                    throw new Exception(__("Bạn không thể sử dụng Coniu nếu chưa đủ 14 tuổi"));
                }

                $full_name = $args['lastname'] . " " . $args['firstname'];

                /* update user */
                $db->query(sprintf("UPDATE users SET user_id_card_number = %s, date_of_issue = %s, place_of_issue = %s, user_birthdate = %s WHERE user_id = %s", secure($full_name), secure($args['firstname']), secure($args['lastname']), secure($args['identification_card_number']), secure($args['date_of_issue']), secure($args['place_of_issue']), secure($args['birth_date']), secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                break;

            case 'started_account':
                $set_query = '';
                if (isset($args['password'])) {
                    /* validate new password */
                    if(strlen($args['password']) < 6) {
                        throw new Exception(__("Your password must be at least 6 characters long. Please try another"));
                    }
                    $set_query .= " , user_password = ". secure(_password_hash($args['password']));
                    $set_query .= " , user_changed_pass = '1'";
                }
                $set_query .= " , user_started = '1'";
                $set_query = preg_replace('/, /', '', $set_query, 1);

                $db->query(sprintf("UPDATE users SET " .$set_query. " WHERE user_id = %s", secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                break;

            /* ---------- CI - API update account ---------- */
            case 'account':

                /* validate username */
                if(strtolower($args['username']) != strtolower($this->_data['user_name'])) {
                    if(is_empty($args['username']) || !valid_username($args['username'], "user")) {
                        throw new Exception(__("Please enter a valid username (a-z0-9_.)"));
                    }
                    if($this->check_username($args['username'])) {
                        throw new Exception(__("Sorry, it looks like") ." ". $args['username'] ." ". __("belongs to an existing account"));
                    }
                    /* update user */
                    $db->query(sprintf("UPDATE users SET user_name = %s WHERE user_id = %s", secure($args['username']), secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                }

                /* validate email */
                if($args['email'] != $this->_data['user_email']) {
                    if(!valid_email($args['email'])) {
                        throw new Exception(__("Please enter a valid email address"));
                    }
                    if($this->check_email($args['email'])) {
                        throw new Exception(__("Sorry, it looks like")." ".$args['email']." ".__("belongs to an existing account"));
                    }
                    /* generate activation key */
                    $activation_key = get_hash_token();

                    /* update user */
                    $db->query(sprintf("UPDATE users SET user_email = %s, user_activation_key = %s, user_activated = '0' WHERE user_id = %s", secure($args['email']), secure($activation_key), secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);

                    // send activation email
                    /* prepare activation email */
                    $subject = __("Just one more step to get started on")." ".$system['system_title'];
                    $body  = __("Hi")." ".ucwords(convertText4Web($this->_data['user_fullname'])).",";
                    $body .= "\r\n\r\n".__("To complete the sign-up process, please follow this link:");
                    $body .= "\r\n\r\n".$system['system_url']."/activation/".$this->_data['user_id']."/".$activation_key;
                    $body .= "\r\n\r\n".__("Welcome to")." ".$system['system_title'];
                    $body .= "\r\n\r".$system['system_title']." ".__("Team");
                    /* send email */
                    if(!_email($args['email'], $subject, $body)) {
                        //throw new Exception(__("Activation email could not be sent. But you can login now"));
                    }
                }

                /* CI - Firebase */
                $arr = array(
                    'user_id' => $this->_data['user_id'],
                    'action' => UPDATE_USER,
                    'user_name' => $args['username'],
                    'user_email' => $args['email']
                );
                insertBackgroundFirebase($arr);
                /* END - Firebase */

                break;

            /* CI - API update profile*/
            case 'profile':
                /* validate firstname */
                if(is_empty($args['firstname'])) {
                    throw new Exception(__("You must enter first name"));
                }
                if(!valid_name($args['firstname'])) {
                    throw new Exception(__("First name contains invalid characters"));
                }
                /*if(strlen($args['firstname']) < 3) {
                    throw new Exception(__("First name must be at least 3 characters long. Please try another"));
                }*/
                /* validate lastname */
                if(is_empty($args['lastname'])) {
                    throw new Exception(__("You must enter last name"));
                }
                if(!valid_name($args['lastname'])) {
                    throw new Exception(__("Last name contains invalid characters"));
                }
                /*if(strlen($args['lastname']) < 3) {
                    throw new Exception(__("Last name must be at least 3 characters long. Please try another"));
                }*/

                /* validate birthdate */
                if (!is_empty($args['birth_day'])) {
                    $args['birth_day'] = toDBDate($args['birth_day']);
                } else {
                    $args['birth_day'] = 'null';
                }

                if(is_empty($args['identification_card_number'])) {
                    throw new Exception(__("Please enter identification card number"));
                }
                if(is_empty($args['date_of_issue'])) {
                    throw new Exception(__("Please enter date of issue"));
                }
                if(is_empty($args['place_of_issue'])) {
                    throw new Exception(__("Please enter place of issue"));
                }

                /* validate gender */
                if(!in_array($args['gender'], array('male', 'female'))) {
                    throw new Exception(__("Please select a valid gender"));
                }

                if (!isset($args['avatar'])) {
                    $args['avatar'] = !is_empty($this->_data['user_picture']) ? $this->_data['user_picture'] : "null";
                }

                $full_name = $args['lastname'] . " " . $args['firstname'];
                $args['date_of_issue'] = toDBDate($args['date_of_issue']);
                /* update user */
                $db->query(sprintf("UPDATE users SET user_id_card_number = %s,  date_of_issue = %s,  place_of_issue = %s, user_fullname = %s, user_firstname = %s, user_lastname = %s, user_gender = %s, user_picture = %s, user_birthdate = %s, city_id = %s,
                    user_work_title = %s, user_work_place = %s, user_current_city = %s, user_hometown = %s,
                    user_edu_major = %s, user_edu_school = %s, user_edu_class = %s WHERE user_id = %s",
                    secure($args['identification_card_number']), secure($args['date_of_issue']), secure($args['place_of_issue']), secure($full_name), secure($args['firstname']), secure($args['lastname']), secure($args['gender']), secure($args['avatar']), secure($args['birth_day']), secure($args['city_id'], 'int'),
                    secure($args['work_title']), secure($args['work_place']),
                    secure($args['city']), secure($args['hometown']),
                    secure($args['edu_major']), secure($args['edu_school']), secure($args['edu_class']),
                    secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);

                /* CI - Firebase */
                $arr = array(
                    'user_id' => $this->_data['user_id'],
                    'action' => UPDATE_USER,
                    'user_fullname' => $full_name,
                    'user_picture' => get_picture($args['avatar'], $args['gender'])
                );

                insertBackgroundFirebase($arr);
                /* END - Firebase */
                break;

            /* CI - API update info*/
            case 'info':

                /* update work */
                $db->query(sprintf("UPDATE users SET user_work_title = %s, user_work_place = %s WHERE user_id = %s", secure($args['work_title']), secure($args['work_place']), secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);

                /* update place */
                $db->query(sprintf("UPDATE users SET user_current_city = %s, user_hometown = %s WHERE user_id = %s", secure($args['city']), secure($args['hometown']), secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);

                /* update edu */
                $db->query(sprintf("UPDATE users SET user_edu_major = %s, user_edu_school = %s, user_edu_class = %s WHERE user_id = %s", secure($args['edu_major']), secure($args['edu_school']), secure($args['edu_class']), secure($this->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);

                break;

            /* ---------- CI - END -----------*/

        }
    }



    /* ------------------------------- */
    /* Announcements */
    /* ------------------------------- */

    /**
     * announcements
     * 
     * @param array $place
     * @return array
     */
    public function announcements() {
        global $db, $date, $system;
        $announcements = array();
        $get_announcement = $db->query(sprintf('SELECT * FROM announcements WHERE start_date <= %1$s AND end_date >= %1$s', secure($date))) or _error(SQL_ERROR_THROWEN);
        if($get_announcement->num_rows > 0) {
            while($announcement = $get_announcement->fetch_assoc()) {
                $check = $db->query(sprintf("SELECT * FROM announcements_users WHERE announcement_id = %s AND user_id = %s", secure($announcement['announcement_id'], 'int'), secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                if($check->num_rows == 0) {
                    /* CI - mobile không cần decode */
                    if (isset($system['is_mobile']) && $system['is_mobile']) {
                        $code = html_entity_decode($announcement['code'], ENT_QUOTES);
                        $code = str_replace("\r\n",'', $code);
                        $announcement['code'] = strip_tags($code, '<div>');
                    } else {
                        $announcement['code'] = html_entity_decode($announcement['code'], ENT_QUOTES);
                    }
                    $announcements[] = $announcement;
                }
            }
        }
        return $announcements;
    }


    /**
     * hide_announcement
     * 
     * @param integer $id
     * @return void
     */
    public function hide_announcement($id) {
        global $db, $system;
        /* check announcement */
        $check = $db->query(sprintf("SELECT * FROM announcements WHERE announcement_id = %s", secure($id, 'int') )) or _error(SQL_ERROR_THROWEN);
        if($check->num_rows == 0) {
            _error(403);
        }
        /* hide announcement */
        $db->query(sprintf("INSERT INTO announcements_users (announcement_id, user_id) VALUES (%s, %s)", secure($id, 'int'), secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
    }



    /* ------------------------------- */
    /* Ads */
    /* ------------------------------- */

    /**
     * ads
     *
     * @param string $place
     * @return array
     */
    public function ads($place) {
        global $db;
        $ads = array();
        $get_ads = $db->query(sprintf("SELECT * FROM ads_system WHERE place = %s", secure($place) )) or _error(SQL_ERROR_THROWEN);
        if($get_ads->num_rows > 0) {
            while($ads_unit = $get_ads->fetch_assoc()) {
                $ads_unit['code'] = html_entity_decode($ads_unit['code'], ENT_QUOTES);
                $ads[] = $ads_unit;
            }
        }
        return $ads;
    }


    /**
     * ads_campaigns
     *
     * @param string $place
     * @return array
     */
    public function ads_campaigns($place = 'sidebar') {
        global $db, $system, $date;
        $campaigns = array();
        /* check if ads enabled */
        if(!$system['ads_enabled']) {
            return $campaigns;
        }
        /* get active campaigns */
        $get_campaigns = $db->query(sprintf("SELECT ads_campaigns.*, pages.page_name, groups.group_name, users.user_wallet_balance FROM ads_campaigns INNER JOIN users ON ads_campaigns.campaign_user_id = users.user_id LEFT JOIN pages ON ads_campaigns.ads_type = 'page' AND ads_campaigns.ads_page = pages.page_id LEFT JOIN groups ON ads_campaigns.ads_type = 'group' AND ads_campaigns.ads_group = groups.group_id WHERE ads_campaigns.campaign_is_active = '1' AND ads_campaigns.ads_placement = %s ORDER BY RAND()", secure($place) )) or _error(SQL_ERROR_THROWEN);
        if($get_campaigns->num_rows > 0) {
            while($campaign = $get_campaigns->fetch_assoc()) {
                // check if viewer get 1 valid campaign
                if(count($campaigns) >= 1) {
                    break;
                }

                // update campaign
                /* [1] -> campaign expired */
                if(strtotime($campaign['campaign_end_date']) <= strtotime($date)) {
                    $db->query(sprintf("UPDATE ads_campaigns SET campaign_is_active = '0' WHERE campaign_id = %s", secure($campaign['campaign_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                    continue;
                }
                $remining = $campaign['campaign_budget'] - $campaign['campaign_spend']; // campaign remining (budget - spend)
                /* [2] -> campaign remining = 0 (spend == budget) */
                if($remining == 0) {
                    $db->query(sprintf("UPDATE ads_campaigns SET campaign_is_active = '0' WHERE campaign_id = %s", secure($campaign['campaign_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                    continue;
                }
                /* [3] -> campaign remining > campaign's author wallet credit */
                if($remining > $campaign['user_wallet_balance']) {
                    $db->query(sprintf("UPDATE ads_campaigns SET campaign_is_active = '0' WHERE campaign_id = %s", secure($campaign['campaign_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                    continue;
                }
                /* [4] -> campaign remining < cost per click */
                if($remining < $system['ads_cost_click'] && $campaign['campaign_bidding'] == "click") {
                    $db->query(sprintf("UPDATE ads_campaigns SET campaign_is_active = '0' WHERE campaign_id = %s", secure($campaign['campaign_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                    continue;
                }
                /* [5] -> campaign remining < cost per view */
                if($remining < $system['ads_cost_view'] && $campaign['campaign_bidding'] == "view") {
                    $db->query(sprintf("UPDATE ads_campaigns SET campaign_is_active = '0' WHERE campaign_id = %s", secure($campaign['campaign_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                    continue;
                }

                // check if "viewer" is campaign target audience
                /* if viewer is campaign author */
                if($this->_data['user_id'] == $campaign['campaign_user_id']) {
                    continue;
                }
                /* check viewer country */
                $campaign['audience_countries'] = ($campaign['audience_countries'])? explode(',', $campaign['audience_countries']) : array();
                if($campaign['audience_countries'] && !in_array($this->_data['user_country'], $campaign['audience_countries'])) {
                    continue;
                }
                /* check viewer gender */
                if($campaign['audience_gender'] != "all" && $this->_data['user_gender'] != $campaign['audience_gender']) {
                    continue;
                }
                /* check viewer relationship */
                if($campaign['audience_relationship'] != "all" && $this->_data['user_relationship'] != $campaign['audience_relationship']) {
                    continue;
                }

                // prepare ads URL
                switch ($campaign['ads_type']) {
                    case 'page':
                        $campaign['ads_url'] = $system['system_url'].'/pages/'.$campaign['page_name'];
                        break;

                    case 'group':
                        $campaign['ads_url'] = $system['system_url'].'/groups/'.$campaign['group_name'];
                        break;

                    case 'event':
                        $campaign['ads_url'] = $system['system_url'].'/events/'.$campaign['ads_event'];
                        break;
                }

                // update campaign if bidding = view
                if($campaign['campaign_bidding'] == "view") {
                    /* update campaign spend & views */
                    $db->query(sprintf("UPDATE ads_campaigns SET campaign_views = campaign_views + 1, campaign_spend = campaign_spend + %s WHERE campaign_id = %s", secure($system['ads_cost_view']), secure($campaign['campaign_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                    /* decrease campaign author wallet balance */
                    $db->query(sprintf("UPDATE users SET user_wallet_balance = IF(user_wallet_balance=0,0,user_wallet_balance-%s) WHERE user_id = %s", secure($system['ads_cost_view']), secure($campaign['campaign_user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                }

                // get campaigns
                $campaigns[] = $campaign;
            }
        }
        return $campaigns;
    }


    /**
     * get_campaigns
     *
     * @return array
     */
    public function get_campaigns() {
        global $db;
        $campaigns = array();
        $get_campaigns = $db->query(sprintf("SELECT * FROM ads_campaigns WHERE campaign_user_id = %s ORDER BY campaign_id DESC", secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
        if($get_campaigns->num_rows > 0) {
            while($campaign = $get_campaigns->fetch_assoc()) {
                $campaigns[] = $campaign;
            }
        }
        return $campaigns;
    }


    /**
     * get_campaign
     *
     * @param integer $campaign_id
     * @return array
     */
    public function get_campaign($campaign_id) {
        global $db;
        $get_campaign = $db->query(sprintf("SELECT * FROM ads_campaigns WHERE campaign_id = %s", secure($campaign_id, 'int') )) or _error(SQL_ERROR_THROWEN);
        if($get_campaign->num_rows == 0) {
            return false;
        }
        $campaign = $get_campaign->fetch_assoc();
        /* get audience countries array */
        $campaign['audience_countries'] = ($campaign['audience_countries'])? explode(',', $campaign['audience_countries']) : array();
        /* get campaign potential reach */
        $campaign['campaign_potential_reach'] = $this->campaign_potential_reach($campaign['audience_countries'], $campaign['audience_gender'], $campaign['audience_relationship']);
        return $campaign;
    }


    /**
     * create_campaign
     *
     * @param array $args
     * @return void
     */
    public function create_campaign($args = []) {
        global $db, $system, $date;
        /* check if ads enabled */
        if(!$system['ads_enabled']) {
            throw new Exception(__("This feature has been disabled by the admin"));
        }
        /* validate campaign title */
        if(is_empty($args['campaign_title'])) {
            throw new Exception(__("You have to enter the campaign title"));
        }
        /* validate campaign start & end dates (UTC) */
        if(is_empty($args['campaign_start_date'])) {
            throw new Exception(__("You have to enter the campaign start date"));
        }
        if(is_empty($args['campaign_end_date'])) {
            throw new Exception(__("You have to enter the campaign end date"));
        }
        if(strtotime($args['campaign_start_date']) >= strtotime($args['campaign_end_date'])) {
            throw new Exception(__("Campaign end date must be after the start date"));
        }
        if(strtotime($args['campaign_end_date']) <= strtotime($date)) {
            throw new Exception(__("Campaign end date must be after today datetime"));
        }
        /* validate campaign budget */
        if(is_empty($args['campaign_budget']) || !is_numeric($args['campaign_budget'])) {
            throw new Exception(__("You must enter valid budget"));
        }
        if($args['campaign_budget'] < max($system['ads_cost_click'], $system['ads_cost_view'])) {
            throw new Exception(__("The minimum budget must be")." ".$system['system_currency_symbol'].max($system['ads_cost_click'], $system['ads_cost_view'])." ");
        }
        if($this->_data['user_wallet_balance'] < $args['campaign_budget']) {
            throw new Exception(__("Your current wallet credit is")." ".$system['system_currency_symbol'].$this->_data['user_wallet_balance']." ".__("You need to")." <a href='".$system['system_url']."/wallet'>".__("Replenish wallet credit")."</a>");
        }
        /* validate campaign bidding */
        if(!in_array($args['campaign_bidding'], array('click','view'))) {
            throw new Exception(__("You have to select a valid bidding"));
        }
        /* validate audience countries */
        $args['audience_countries'] = (isset($args['audience_countries']) && is_array($args['audience_countries']))? implode(',',$args['audience_countries']) : "";
        /* validate audience gender */
        if(!in_array($args['audience_gender'], array('all', 'male', 'female', 'other'))) {
            throw new Exception(__("You have to select a valid gender"));
        }
        /* validate audience relationship */
        if(!in_array($args['audience_relationship'], array('all','single', 'relationship', 'married', "complicated", 'separated', 'divorced', 'widowed'))) {
            throw new Exception(__("You have to select a valid relationship"));
        }
        /* validate ads type */
        switch ($args['ads_type']) {
            case 'url':
                if(is_empty($args['ads_url']) || !valid_url($args['ads_url'])) {
                    throw new Exception(__("You have to enter a valid URL for your ads"));
                }
                $args['ads_page'] = 'null';
                $args['ads_group'] = 'null';
                $args['ads_event'] = 'null';
                break;

            case 'page':
                if($args['ads_page'] == "none") {
                    throw new Exception(__("You have to select one for your pages for your ads"));
                }
                $args['ads_url'] = 'null';
                $args['ads_group'] = 'null';
                $args['ads_event'] = 'null';
                break;

            case 'group':
                if($args['ads_group'] == "none") {
                    throw new Exception(__("You have to select one for your groups for your ads"));
                }
                $args['ads_url'] = 'null';
                $args['ads_page'] = 'null';
                $args['ads_event'] = 'null';
                break;

            case 'event':
                if($args['ads_event'] == "none") {
                    throw new Exception(__("You have to select one for your events for your ads"));
                }
                $args['ads_url'] = 'null';
                $args['ads_page'] = 'null';
                $args['ads_group'] = 'null';
                break;

            default:
                throw new Exception(__("You have to select a valid ads type"));
                break;
        }
        /* validate ads placement */
        if(!in_array($args['ads_placement'], array('newsfeed','sidebar'))) {
            throw new Exception(__("You have to select a valid ads placement"));
        }
        /* validate ads image */
        if(is_empty($args['ads_image'])) {
            throw new Exception(__("You have to upload an image for your ads"));
        }
        /* insert new campain */
        $db->query(sprintf("INSERT INTO ads_campaigns (
            campaign_user_id, 
            campaign_title, 
            campaign_start_date, 
            campaign_end_date, 
            campaign_budget, 
            campaign_bidding, 
            audience_countries, 
            audience_gender, 
            audience_relationship, 
            ads_title, 
            ads_description, 
            ads_type, 
            ads_url, 
            ads_page, 
            ads_group, 
            ads_event, 
            ads_placement, 
            ads_image, 
            campaign_created_date) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
            secure($this->_data['user_id'], 'int'),
            secure($args['campaign_title']),
            secure($args['campaign_start_date'], 'datetime'),
            secure($args['campaign_end_date'], 'datetime'),
            secure($args['campaign_budget']),
            secure($args['campaign_bidding']),
            secure($args['audience_countries']),
            secure($args['audience_gender']),
            secure($args['audience_relationship']),
            secure($args['ads_title']),
            secure($args['ads_description']),
            secure($args['ads_type']),
            secure($args['ads_url']),
            secure($args['ads_page']),
            secure($args['ads_group']),
            secure($args['ads_event']),
            secure($args['ads_placement']),
            secure($args['ads_image']),
            secure($date) )) or _error(SQL_ERROR_THROWEN);
    }


    /**
     * edit_campaign
     *
     * @param integer $campaign_id
     * @param array $args
     * @return void
     */
    public function edit_campaign($campaign_id, $args) {
        global $db, $system, $date;
        /* check if ads enabled */
        if($this->_data['user_group'] >= 3 && !$system['ads_enabled']) {
            throw new Exception(__("This feature has been disabled by the admin"));
        }
        /* (check&get) campaign */
        $get_campaign = $db->query(sprintf("SELECT ads_campaigns.*, users.user_wallet_balance FROM ads_campaigns INNER JOIN users ON ads_campaigns.campaign_user_id = users.user_id WHERE ads_campaigns.campaign_id = %s", secure($campaign_id, 'int') )) or _error(SQL_ERROR_THROWEN);
        if($get_campaign->num_rows == 0) {
            _error(403);
        }
        $campaign = $get_campaign->fetch_assoc();
        /* check permission */
        $can_edit = false;
        /* viewer is (admin|moderator) */
        if($this->_data['user_group'] < 3) {
            $can_edit = true;
            /* viewer is the creator of campaign */
        } elseif($this->_data['user_id'] == $campaign['campaign_user_id']) {
            $can_edit = true;
        }
        /* edit the campaign */
        if(!$can_edit) {
            _error(403);
        }
        /* validate campaign title */
        if(is_empty($args['campaign_title'])) {
            throw new Exception(__("You have to enter the campaign title"));
        }
        /* validate campaign start & end dates */
        if(is_empty($args['campaign_start_date'])) {
            throw new Exception(__("You have to enter the campaign start date"));
        }
        if(is_empty($args['campaign_end_date'])) {
            throw new Exception(__("You have to enter the campaign end date"));
        }
        if(strtotime($args['campaign_start_date']) > strtotime($args['campaign_end_date'])) {
            throw new Exception(__("Campaign end date must be after the start date"));
        }
        if(strtotime($args['campaign_end_date']) <= strtotime($date)) {
            throw new Exception(__("Campaign end date must be after today datetime"));
        }
        /* validate campaign budget */
        if(is_empty($args['campaign_budget']) || !is_numeric($args['campaign_budget'])) {
            throw new Exception(__("You must enter valid budget"));
        }
        $remining = $args['campaign_budget'] - $campaign['campaign_spend']; // campaign remining (budget(new) - spend)
        if($remining == 0) {
            throw new Exception(__("The campaign total spend reached the campaign budget already, increase the campaign budget to resume the campaign"));
        }
        if($remining > $campaign['user_wallet_balance']) {
            throw new Exception(__("The remining spend is larger than current wallet credit")." ".$system['system_currency_symbol'].$campaign['user_wallet_balance']." ".__("You need to")." ".__("Replenish wallet credit"));
        }
        /* validate campaign bidding */
        if(!in_array($args['campaign_bidding'], array('click','view'))) {
            throw new Exception(__("You have to select a valid bidding"));
        }
        if($args['campaign_bidding'] == "click") {
            if($remining < $system['ads_cost_click']) {
                throw new Exception(__("The cost per click is larger than your campaign remining spend")." ".$system['system_currency_symbol'].$remining." ".__("increase the campaign budget to resume the campaign"));
            }
        }
        if($args['campaign_bidding'] == "view") {
            if($remining < $system['ads_cost_view']) {
                throw new Exception(__("The cost per view is larger than your campaign remining spend")." ".$system['system_currency_symbol'].$remining." ".__("increase the campaign budget to resume the campaign"));
            }
        }
        /* validate audience countries */
        $args['audience_countries'] = (isset($args['audience_countries']) && is_array($args['audience_countries']))? implode(',',$args['audience_countries']) : "";
        /* validate audience gender */
        if(!in_array($args['audience_gender'], array('all', 'male', 'female', 'other'))) {
            throw new Exception(__("You have to select a valid gender"));
        }
        /* validate audience relationship */
        if(!in_array($args['audience_relationship'], array('all','single', 'relationship', 'married', "complicated", 'separated', 'divorced', 'widowed'))) {
            throw new Exception(__("You have to select a valid relationship"));
        }
        /* validate ads type */
        switch ($args['ads_type']) {
            case 'url':
                if(is_empty($args['ads_url']) || !valid_url($args['ads_url'])) {
                    throw new Exception(__("You have to enter a valid URL for your ads"));
                }
                $args['ads_page'] = 'null';
                $args['ads_group'] = 'null';
                $args['ads_event'] = 'null';
                break;

            case 'page':
                if($args['ads_page'] == "none") {
                    throw new Exception(__("You have to select one for your pages for your ads"));
                }
                $args['ads_url'] = 'null';
                $args['ads_group'] = 'null';
                $args['ads_event'] = 'null';
                break;

            case 'group':
                if($args['ads_group'] == "none") {
                    throw new Exception(__("You have to select one for your groups for your ads"));
                }
                $args['ads_url'] = 'null';
                $args['ads_page'] = 'null';
                $args['ads_event'] = 'null';
                break;

            case 'event':
                if($args['ads_event'] == "none") {
                    throw new Exception(__("You have to select one for your events for your ads"));
                }
                $args['ads_url'] = 'null';
                $args['ads_page'] = 'null';
                $args['ads_group'] = 'null';
                break;

            default:
                throw new Exception(__("You have to select a valid ads type"));
                break;
        }
        /* validate ads placement */
        if(!in_array($args['ads_placement'], array('newsfeed','sidebar'))) {
            throw new Exception(__("You have to select a valid ads placement"));
        }
        /* validate ads image */
        if(is_empty($args['ads_image'])) {
            throw new Exception(__("You have to upload an image for your ads"));
        }
        /* update the campain */
        $db->query(sprintf("UPDATE ads_campaigns SET 
            campaign_title = %s, 
            campaign_start_date = %s, 
            campaign_end_date = %s, 
            campaign_budget = %s, 
            campaign_bidding = %s, 
            audience_countries = %s, 
            audience_gender = %s, 
            audience_relationship = %s, 
            ads_title = %s, 
            ads_description = %s, 
            ads_type = %s, 
            ads_url = %s, 
            ads_page = %s, 
            ads_group = %s, 
            ads_event = %s, 
            ads_placement = %s, 
            ads_image = %s, 
            campaign_is_active = '1' WHERE campaign_id = %s",
            secure($args['campaign_title']),
            secure($args['campaign_start_date'], 'datetime'),
            secure($args['campaign_end_date'], 'datetime'),
            secure($args['campaign_budget']),
            secure($args['campaign_bidding']),
            secure($args['audience_countries']),
            secure($args['audience_gender']),
            secure($args['audience_relationship']),
            secure($args['ads_title']),
            secure($args['ads_description']),
            secure($args['ads_type']),
            secure($args['ads_url']),
            secure($args['ads_page']),
            secure($args['ads_group']),
            secure($args['ads_event']),
            secure($args['ads_placement']),
            secure($args['ads_image']),
            secure($campaign_id, 'int') )) or _error(SQL_ERROR_THROWEN);
    }


    /**
     * delete_campaign
     *
     * @param integer $campaign_id
     * @return void
     */
    public function delete_campaign($campaign_id) {
        global $db, $system;
        /* check if ads enabled */
        if($this->_data['user_group'] >= 3 && !$system['ads_enabled']) {
            throw new Exception(__("This feature has been disabled by the admin"));
        }
        /* (check&get) campaign */
        $get_campaign = $db->query(sprintf("SELECT * FROM ads_campaigns WHERE campaign_id = %s", secure($campaign_id, 'int') )) or _error(SQL_ERROR_THROWEN);
        if($get_campaign->num_rows == 0) {
            _error(403);
        }
        $campaign = $get_campaign->fetch_assoc();
        // delete campaign
        $can_delete = false;
        /* viewer is (admin|moderator) */
        if($this->_data['user_group'] < 3) {
            $can_delete = true;
            /* viewer is the creator of campaign */
        } elseif($this->_data['user_id'] == $campaign['campaign_user_id']) {
            $can_delete = true;
        }
        /* delete the campaign */
        if(!$can_delete) {
            _error(403);
        }
        $db->query(sprintf("DELETE FROM ads_campaigns WHERE campaign_id = %s", secure($campaign_id, 'int') )) or _error(SQL_ERROR_THROWEN);
    }


    /**
     * update_campaign_status
     *
     * @param integer $campaign_id
     * @param boolean $is_active
     * @return void
     */
    public function update_campaign_status($campaign_id, $is_active) {
        global $db, $system, $date;
        /* check if ads enabled */
        if($this->_data['user_group'] >= 3 && !$system['ads_enabled']) {
            throw new Exception(__("This feature has been disabled by the admin"));
        }
        /* (check&get) campaign */
        $get_campaign = $db->query(sprintf("SELECT ads_campaigns.*, users.user_wallet_balance FROM ads_campaigns INNER JOIN users ON ads_campaigns.campaign_user_id = users.user_id WHERE ads_campaigns.campaign_id = %s", secure($campaign_id, 'int') )) or _error(SQL_ERROR_THROWEN);
        if($get_campaign->num_rows == 0) {
            _error(403);
        }
        $campaign = $get_campaign->fetch_assoc();
        // change campaign status
        $can_edit = false;
        /* viewer is (admin|moderator) */
        if($this->_data['user_group'] < 3) {
            $can_edit = true;
            /* viewer is the creator of campaign */
        } elseif($this->_data['user_id'] == $campaign['campaign_user_id']) {
            $can_edit = true;
        }
        /* change campaign status */
        if(!$can_edit) {
            _error(403);
        }
        $is_active = ($is_active)? '1' : '0';
        if($is_active) {
            /* validate campaign */
            if(strtotime($campaign['campaign_end_date']) <= strtotime($date)) {
                throw new Exception(__("Campaign end date must be after today datetime"));
            }
            $remining = $campaign['campaign_budget'] - $campaign['campaign_spend']; // campaign remining (budget - spend)
            if($remining == 0) {
                throw new Exception(__("The campaign total spend reached the campaign budget already, increase the campaign budget to resume the campaign"));
            }
            if($remining > $campaign['user_wallet_balance']) {
                throw new Exception(__("The remining spend is larger than current wallet credit")." ".$system['system_currency_symbol'].$campaign['user_wallet_balance']." ".__("You need to")." ".__("Replenish wallet credit"));
            }
            if($campaign['campaign_bidding'] == "click") {
                if($remining < $system['ads_cost_click']) {
                    throw new Exception(__("The cost per click is larger than your campaign remining spend")." ".$system['system_currency_symbol'].$remining." ".__("increase the campaign budget to resume the campaign"));
                }
            }
            if($campaign['campaign_bidding'] == "view") {
                if($remining < $system['ads_cost_view']) {
                    throw new Exception(__("The cost per view is larger than your campaign remining spend")." ".$system['system_currency_symbol'].$remining." ".__("increase the campaign budget to resume the campaign"));
                }
            }
        }
        $db->query(sprintf("UPDATE ads_campaigns SET campaign_is_active = %s WHERE campaign_id = %s", secure($is_active), secure($campaign_id, 'int') )) or _error(SQL_ERROR_THROWEN);
    }


    /**
     * update_campaign_bidding
     *
     * @param integer $campaign_id
     * @return void
     */
    public function update_campaign_bidding($campaign_id) {
        global $db, $system, $date;
        /* check if ads enabled */
        if(!$system['ads_enabled']) {
            return;
        }
        /* (check&get) campaign */
        $get_campaign = $db->query(sprintf("SELECT ads_campaigns.*, users.user_wallet_balance FROM ads_campaigns INNER JOIN users ON ads_campaigns.campaign_user_id = users.user_id WHERE ads_campaigns.campaign_id = %s", secure($campaign_id, 'int') )) or _error(SQL_ERROR_THROWEN);
        if($get_campaign->num_rows == 0) {
            _error(403);
        }
        $campaign = $get_campaign->fetch_assoc();
        // update campaign if bidding = click
        if($campaign['campaign_bidding'] == "click") {
            /* update campaign spend & clicks */
            $db->query(sprintf("UPDATE ads_campaigns SET campaign_clicks = campaign_clicks + 1, campaign_spend = campaign_spend + %s WHERE campaign_id = %s", secure($system['ads_cost_click']), secure($campaign['campaign_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
            /* decrease campaign author wallet balance */
            $db->query(sprintf("UPDATE users SET user_wallet_balance = IF(user_wallet_balance=0,0,user_wallet_balance-%s) WHERE user_id = %s", secure($system['ads_cost_click']), secure($campaign['campaign_user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
        }
    }


    /**
     * campaign_estimated_reach
     *
     * @param array $counties
     * @param string $gender
     * @param string $relationship
     * @return integer
     */
    public function campaign_potential_reach($counties = [], $gender = 'all', $relationship = 'all') {
        global $db, $system;
        $results = 0;
        /* validate gender */
        if(!in_array($gender, array('all', 'male', 'female', 'other'))) {
            return $results;
        }
        /* validate relationship */
        if(!in_array($relationship, array('all','single', 'relationship', 'married', "complicated", 'separated', 'divorced', 'widowed'))) {
            return $results;
        }
        /* prepare where statement */
        $where = "";
        /* validate countries */
        if($counties) {
            $counties_list = implode(',',$counties);
            $where .= " WHERE user_country IN ($counties_list)";
        }
        /* gender */
        if($gender != "all") {
            if($where) {
                $where .= " AND user_gender = '$gender'";
            } else {
                $where .= " WHERE user_gender = '$gender'";
            }
        }
        /* relationship */
        if($relationship != "all") {
            if($where) {
                $where .= " AND user_relationship = '$relationship'";
            } else {
                $where .= " WHERE user_relationship = '$relationship'";
            }
        }
        /* get users */
        $get_users = $db->query("SELECT * FROM users".$where) or _error(SQL_ERROR_THROWEN);
        $results = $get_users->num_rows;
        return $results;
    }



    /* ------------------------------- */
    /* Widgets */
    /* ------------------------------- */

    /**
     * widget
     *
     * @param array $place
     * @return array
     */
    public function widgets($place) {
        global $db;
        $widgets = array();
        $get_widgets = $db->query(sprintf("SELECT * FROM widgets WHERE place = %s", secure($place) )) or _error(SQL_ERROR_THROWEN);
        if($get_widgets->num_rows > 0) {
            while($widget = $get_widgets->fetch_assoc()) {
                $widget['code'] = html_entity_decode($widget['code'], ENT_QUOTES);
                $widgets[] = $widget;
            }
        }
        return $widgets;
    }


    /* ------------------------------- */
    /* Games */
    /* ------------------------------- */

    /**
     * get_games
     * 
     * @param integer $offset
     * @return array
     */
    public function get_games($offset = 0) {
        global $db, $system;
        $games = array();
        $offset *= $system['max_results'];
        $get_games = $db->query(sprintf('SELECT * FROM games LIMIT %s, %s', secure($offset, 'int', false), secure($system['max_results'], 'int', false) )) or _error(SQL_ERROR_THROWEN);
        if($get_games->num_rows > 0) {
            while($game = $get_games->fetch_assoc()) {
                $game['thumbnail'] = $this->get_picture($game['thumbnail'], 'game');
                $games[] = $game;
            }
        }
        return $games;
    }


    /**
     * get_game
     * 
     * @param integer $game_id
     * @return array
     */
    public function get_game($game_id) {
        global $db;
        $get_game = $db->query(sprintf('SELECT * FROM games WHERE game_id = %s', secure($game_id, 'int') )) or _error(SQL_ERROR_THROWEN);
        if($get_game->num_rows == 0) {
            return false;
        }
        $game = $get_game->fetch_assoc();
        $game['thumbnail'] = $this->get_picture($game['thumbnail'], 'game');
        return $game;
    }



    /* ------------------------------- */
    /* User Sign (in|up|out) */
    /* ------------------------------- */

    /**
     * sign_up
     *
     * @param array $args
     * @return integer
     */
    public function sign_up($args = []) {
        global $db, $system, $date;
        /* check invitation code */
        if(!$system['registration_enabled'] && $system['invitation_enabled']) {
            if(!$this->check_invitation_code($args['invitation_code'])) {
                throw new Exception(__("The invitation code you entered is invalid or expired"));
            }
        }

        /* check IP */
        $this->_check_ip();

        if(is_empty($args['first_name']) || is_empty($args['last_name']) || is_empty($args['password'])) {
            throw new Exception(__("You must fill in all of the fields"));
        }

        //Coniu - trim các dữ liệu truyền vào
        $full_name = ucwords(trim($args['last_name']." ".$args['first_name']));
        $email = trim($args['email']);

        //Coniu - Hệ thống tự gen ra username cho user
        $username = is_empty($args['username'])? "" : trim($args['username']);
        if (is_empty($username)) {
            $username = generateUsername($full_name);
        } else {
            if (!valid_username($username, "user")) {
                throw new Exception(__("Please enter a valid username (a-z0-9_.)"));
            }
            if(reserved_username($username)) {
                throw new Exception(__("You can't use")." ". $username ." ".__("as username"));
            }
            if ($this->check_username($username)) {
                throw new Exception(__("Sorry, it looks like") . " " . $username . " " . __("belongs to an existing account"));
            }
        }

        if(valid_email($email)) {
            if($this->check_email($email)) {
                throw new Exception(__("Sorry, it looks like")." ".$email." ".__("belongs to an existing account"));
            }
        } else {
            throw new Exception(__("Please enter a valid email address"));
        }

        if(strlen($args['password']) < 6) {
            throw new Exception(__("Your password must be at least 6 characters long. Please try another"));
        }
        if(!valid_name($args['first_name'])) {
            throw new Exception(__("Your first name contains invalid characters"));
        }

        if(!valid_name($args['last_name'])) {
            throw new Exception(__("Your last name contains invalid characters"));
        }

        if(!in_array($args['gender'], array('male', 'female'))) {
            throw new Exception(__("Please select a valid gender"));
        }

        $args['birth_date'] = 'null';

        /* check reCAPTCHA */
        if($system['reCAPTCHA_enabled']) {
            require_once(ABSPATH.'includes/libs/ReCaptcha/autoload.php');
            $recaptcha = new \ReCaptcha\ReCaptcha($system['reCAPTCHA_secret_key']);
            $resp = $recaptcha->verify($args['g-recaptcha-response'], get_user_ip());
            if (!$resp->isSuccess()) {
                throw new Exception(__("The secuirty check is incorrect. Please try again"));
            }
        }
        /* generate activation_key */
        $activation_key = ($system['activation_type'] == "email")? get_hash_token(): get_hash_key();
        /* register user */
        $db->query(sprintf("INSERT INTO users (user_name, user_email, user_password, user_fullname, user_firstname, user_lastname, user_gender, user_birthdate, user_registered, user_activation_key) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", secure($username), secure($email), secure(_password_hash($args['password'])), secure(ucwords($full_name)),secure(ucwords($args['first_name'])), secure(ucwords($args['last_name'])), secure($args['gender']), secure($args['birth_date']), secure($date), secure($activation_key) )) or _error(SQL_ERROR_THROWEN);
        /* get user_id */
        $user_id = $db->insert_id;

        /* CI - Firebase */
        $args = array(
            'user_id' => $user_id,
            'action' => CREATE_USER,
            'user_email' => $email,
            'user_fullname' => $full_name,
            'user_name' => $username,
            'user_picture' => get_picture('', $args['gender']),
            'is_online' => 0,
            'count_chat' => 0,
            'last_login' => date(DB_DATETIME_FORMAT_DEFAULT),
            'provider' => 'Coniu'
        );
        insertBackgroundFirebase($args);
        /* END-CI */

        /* send activation */
        if($system['activation_enabled']) {
            /* prepare activation email */
            $subject = __("Just one more step to get started on")." ".$system['system_title'];
            $body  = __("Hi")." ".$full_name.",";
            $body .= "\r\n\r\n".__("To complete the sign-up process, please follow this link:");
            $body .= "\r\n\r\n".$system['system_url']."/activation/".$user_id."/".$activation_key;
            $body .= "\r\n\r\n".__("Welcome to")." ".$system['system_title'];
            $body .= "\r\n\r".$system['system_title']." ".__("Team");
            /* send email */
            if(!_email($email, $subject, $body)) {
                throw new Exception(__("Activation email could not be sent. But you can login now"));
            }
        }

        /* set cookies */
        $this->_set_cookies($user_id);

        return $user_id; //Coniu
	}


    /**
     * sign_up_via_sms
     *
     * @param array $args
     * @return array
     */
    public function sign_up_via_sms($args = []) {
        global $db, $date;

        /* check IP */
        $this->_check_ip();

        if(is_empty($args['first_name']) || is_empty($args['last_name']) || is_empty($args['birth_date'])) {
            throw new Exception(__("You must fill in all of the fields"));
        }

        //Coniu - trim các dữ liệu truyền vào
        $full_name = ucwords(trim($args['last_name']." ".$args['first_name']));
        $phone = standardizePhone($args['phone']);

        //Coniu - Hệ thống tự gen ra username cho user
        $username = is_empty($args['username'])? "" : trim($args['username']);
        if (is_empty($username)) {
            $username = generateUsername($full_name);
        } else {
            if (!valid_username($username, "user")) {
                throw new Exception(__("Please enter a valid username (a-z0-9_.)"));
            }
            if(reserved_username($username)) {
                throw new Exception(__("You can't use")." ". $username ." ".__("as username"));
            }
            if ($this->check_username($username)) {
                throw new Exception(__("Sorry, it looks like") . " " . $username . " " . __("belongs to an existing account"));
            }
        }

        if (valid_phone($phone)) {
            if($this->check_phone($phone)) {
                throw new Exception(__("Sorry, it looks like")." ".$phone." ".__("belongs to an existing account"));
            }
        } else {
            throw new Exception(__("Invalid mobile number"));
        }

        if(!valid_name($args['first_name'])) {
            throw new Exception(__("Your first name contains invalid characters"));
        }

        if(!valid_name($args['last_name'])) {
            throw new Exception(__("Your last name contains invalid characters"));
        }

        $args['gender'] = isset($args['gender']) ? $args['gender'] : 'male';
        $args['birth_date'] = isset($args['birth_date']) ? toDBDate($args['birth_date']) : 'null';

        if(is_empty($args['identification_card_number'])) {
            throw new Exception(__("Please enter identification card number"));
        }
        if(is_empty($args['date_of_issue'])) {
            throw new Exception(__("Please enter date of issue"));
        }
        if(is_empty($args['place_of_issue'])) {
            throw new Exception(__("Please enter place of issue"));
        }
        $args['date_of_issue'] = toDBDate($args['date_of_issue']);

        /* CI - Kiểm tra người dùng  lớn hơn 14 tuổi */
        $interval = date_diff(date_create(date("Y-m-d")), date_create($args['birth_date']));
        $yearold = $interval->format('%Y');
        if ($yearold < 14) {
            throw new Exception(__("Bạn không thể sử dụng Coniu nếu chưa đủ 14 tuổi"));
        }

        if(strlen($args['password']) < 6) {
            throw new Exception(__("Your password must be at least 6 characters long. Please try another"));
        }

        $user_picture = !empty($args['avatar']) ? $args['avatar'] : "null";
        /* register user */
        $db->query(sprintf("INSERT INTO users (user_name, user_email, user_phone, user_phone_signin, user_password, user_fullname, user_firstname, user_lastname, user_gender, user_picture, user_id_card_number, date_of_issue, place_of_issue, user_birthdate, user_registered, user_activated, user_started, user_changed_pass) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, '1', '1', '1')", secure($username), secure('null'), secure($phone), secure($phone), secure(_password_hash($args['password'])), secure(ucwords($full_name)),secure(ucwords($args['first_name'])), secure(ucwords($args['last_name'])), secure($args['gender']), secure($user_picture), secure($args['identification_card_number']), secure($args['date_of_issue']), secure($args['place_of_issue']), secure($args['birth_date']), secure($date) )) or _error(SQL_ERROR_THROWEN);
        /* get user_id */
        $user_id = $db->insert_id;

        /* CI - Firebase */
        $args = array(
            'user_id' => $user_id,
            'action' => CREATE_USER,
            'user_email' => "",
            'user_fullname' => $full_name,
            'user_name' => $username,
            'user_picture' => ($user_picture == "null") ? get_picture('', $args['gender']) : get_picture($user_picture, $args['gender']),
            'is_online' => 0,
            'count_chat' => 0,
            'last_login' => date(DB_DATETIME_FORMAT_DEFAULT),
            'provider' => 'Coniu'
        );
        insertBackgroundFirebase($args);
        /* END-CI */

        /* set cookies */
        $user_token = $this->_set_cookies($user_id);

        $user = array();
        $user['user_id'] = $user_id;
        $user['user_name'] = $username;
        $user['user_email'] = "";
        $user['user_token'] = $user_token;
        $user['user_fullname'] = $full_name;
        $user['user_picture'] = $user_picture;

        return $user;
    }

    /**
     * set_cookies_via_sms
     *
     * @param integer $user_id
     * @param boolean $remember
     * @param string $path
     * @return string
     */
    public function set_cookies_via_sms($user_id, $remember = false, $path = '/') {
        global $db, $date;
        /* generate new token */
        $session_token = get_hash_token();
        /* set cookies */
        if($remember) {
            $expire = time()+2592000;
            setcookie($this->_cookie_user_id, $user_id, $expire, $path);
            setcookie($this->_cookie_user_token, $session_token, $expire, $path);
        } else {
            setcookie($this->_cookie_user_id, $user_id, 0, $path);
            setcookie($this->_cookie_user_token, $session_token, 0, $path);
        }

        /* CI - Chỉ giữ session của 5 phiên đăng nhập gần nhất */
        $db->query(sprintf('DELETE FROM users_sessions WHERE user_id = %1$s AND session_id NOT IN (SELECT * FROM (SELECT session_id FROM users_sessions WHERE user_id = %1$s ORDER BY session_id DESC LIMIT 4) sid) ', secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        /* END - CI */

        /* insert user token */
        $db->query(sprintf("INSERT INTO users_sessions (session_token, session_date, user_id, user_browser, user_os, user_ip) VALUES (%s, %s, %s, %s, %s, %s)", secure($session_token), secure($date), secure($user_id, 'int'), secure(get_user_browser()), secure(get_user_os()), secure(get_user_ip()) )) or _error(SQL_ERROR_THROWEN);
        /* update last login time */
        $db->query(sprintf("UPDATE users SET user_last_login = %s WHERE user_id = %s", secure($date), secure($user_id, 'int') )) or _error(SQL_ERROR);

        return $session_token;
    }


    /**
     * sign_in
     *
     * @param string $username_email
     * @param string $password
     * @param boolean $remember
     *
     * @return void
     */
    public function sign_in($username_email, $password, $remember = false) {
        global $db, $system;
        /* valid inputs */
        if(is_empty($username_email) || is_empty($password)) {
            throw new Exception(__("You must fill in all of the fields"));
        }
        /* check if username or email */
        if(valid_email($username_email)) {
            $user = $this->check_email($username_email, true);
            if($user === false) {
                throw new Exception(__("The email you entered does not belong to any account"));
            }
            $field = "user_email";
        } else {
            if(!valid_username($username_email)) {
                throw new Exception(__("Please enter a valid email address or username"));
            }
            $user = $this->check_username($username_email, 'user', true);
            if($user === false) {
                throw new Exception(__("The username you entered does not belong to any account"));
            }
            $field = "user_name";
        }
        /* check password */
        if(md5($password) == $user['user_password']) {
            /* validate current password (MD5 check for versions < v2.5) */
            $user['user_password'] = _password_hash($password);
            /* update user password hash from MD5 */
            $db->query(sprintf("UPDATE users SET user_password = %s WHERE user_id = %s", secure($user['user_password']), secure($user['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
        }
        if(!password_verify($password, $user['user_password'])) {
            throw new Exception("<p>".__("Please re-enter your password")."</p><p>".__("The password you entered is incorrect").". ".__("If you forgot your password?")." <a href='".$system['system_url']."/reset'>".__("Request a new one")."</a></p>");
        }
        /* set cookies */
        $this->_set_cookies($user['user_id'], $remember);
        return $user['user_id'];
    }


    /**
     * sign_out
     *
     * @return void
     */
    public function sign_out() {
        global $db, $date;
        /* delete the session */
        $db->query(sprintf("DELETE FROM users_sessions WHERE session_token = %s AND user_id = %s", secure($_COOKIE[$this->_cookie_user_token]), secure($_COOKIE[$this->_cookie_user_id], 'int') )) or _error(SQL_ERROR_THROWEN);
        /* destroy the session */
        session_destroy();
        /* unset the cookies */
        unset($_COOKIE[$this->_cookie_user_id]);
        unset($_COOKIE[$this->_cookie_user_token]);
        setcookie($this->_cookie_user_id, NULL, -1, '/');
        setcookie($this->_cookie_user_token, NULL, -1, '/');
    }

    /**
     * _set_cookies
     * 
     * @param integer $user_id
     * @param boolean $remember
     * @param string $path
     * @return string
     */
    private function _set_cookies($user_id, $remember = false, $path = '/') {
        global $db, $date;
        /* generate new token */
        $session_token = get_hash_token();
        /* set cookies */
        if($remember) {
            $expire = time()+2592000;
            setcookie($this->_cookie_user_id, $user_id, $expire, $path);
            setcookie($this->_cookie_user_token, $session_token, $expire, $path);
        } else {
            setcookie($this->_cookie_user_id, $user_id, 0, $path);
            setcookie($this->_cookie_user_token, $session_token, 0, $path);
        }
        header('HTTP/1.1 200 OK');

        /* CI - Chỉ giữ session của 5 phiên đăng nhập gần nhất */
        $db->query(sprintf('DELETE FROM users_sessions WHERE user_id = %1$s AND session_id NOT IN (SELECT * FROM (SELECT session_id FROM users_sessions WHERE user_id = %1$s ORDER BY session_id DESC LIMIT 4) sid) ', secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        /* END - CI */

        /* insert user token */
        $db->query(sprintf("INSERT INTO users_sessions (session_token, session_date, user_id, user_browser, user_os, user_ip) VALUES (%s, %s, %s, %s, %s, %s)", secure($session_token), secure($date), secure($user_id, 'int'), secure(get_user_browser()), secure(get_user_os()), secure(get_user_ip()) )) or _error(SQL_ERROR_THROWEN);
        /* update last login time */
        $db->query(sprintf("UPDATE users SET user_last_login = %s WHERE user_id = %s", secure($date), secure($user_id, 'int') )) or _error(SQL_ERROR);

        return $session_token;
    }


    /**
     * _check_ip
     *
     * @return void
     */
    private function _check_ip() {
        global $db, $system;
        if($system['max_accounts'] > 0) {
            $check = $db->query(sprintf("SELECT user_ip, COUNT(*) FROM users_sessions WHERE user_ip = %s GROUP BY user_id", secure(get_user_ip()) )) or _error(SQL_ERROR_THROWEN);
            if($check->num_rows >= $system['max_accounts']) {
                throw new Exception(__("You have reached the maximum number of account for your IP"));
            }
        }
    }



    /* ------------------------------- */
    /* Socail Login */
    /* ------------------------------- */

    /**
     * socail_login
     * 
     * @param string $provider
     * @param object $user_profile
     * 
     * @return void
     */
    public function socail_login($provider, $user_profile) {
        global $db, $system_page_ids, $system_group_ids;

        include_once(DAO_PATH.'dao_user.php');
        $userDao = new UserDAO();

        unset($_SESSION['social_id']);
        switch ($provider) {
            case 'facebook':
                $social_id = "facebook_id";
                $social_connected = "facebook_connected";
                break;
            
            case 'twitter':
                $social_id = "twitter_id";
                $social_connected = "twitter_connected";
                break;

            case 'google':
                $social_id = "google_id";
                $social_connected = "google_connected";
                break;

            case 'linkedin':
                $social_id = "linkedin_id";
                $social_connected = "linkedin_connected";
                break;

            case 'vkontakte':
                $social_id = "vkontakte_id";
                $social_connected = "vkontakte_connected";
                break;
        }

        /* check if user connected or not */
        $check_user = $db->query(sprintf("SELECT user_id FROM users WHERE $social_id = %s", secure($user_profile->identifier) )) or _error(SQL_ERROR_THROWEN);

        if($check_user->num_rows > 0) {
            /* social account connected and just signing-in */
            $user = $check_user->fetch_assoc();
            /* signout if user logged-in */
            if($this->_logged_in) {
                $this->sign_out();
            }

            /* CI - Cho user like các page và join các group của hệ thống */
            $userDao->addUsersLikeSomePages($system_page_ids, [$user['user_id']]);
            $userDao->addUserToSomeGroups($system_group_ids, [$user['user_id']]);
            /* END - CI */

            /* set cookies */
            $this->_set_cookies($user['user_id'], true);
            redirect();
        } else {
            // Check xem email đã được đăng ký chưa, nếu chưa thì đăng ký và đăng nhập luôn
            $check_email = "";
            if (!is_empty($user_profile->email)) {
                $check_email = $db->query(sprintf("SELECT user_id FROM users WHERE user_email = %s", secure($user_profile->email) )) or _error(SQL_ERROR_THROWEN);
            }
            if($check_email->num_rows > 0) {
                $user = $check_email->fetch_assoc();
                /* signout if user logged-in */
                if($this->_logged_in) {
                    $this->sign_out();
                }
                /* Update info connecting social account */
                $db->query(sprintf("UPDATE users SET $social_connected = '1', $social_id = %s WHERE user_id = %s", secure($user_profile->identifier), secure($user['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);

                /* CI - Cho user like các page và join các group của hệ thống */
                $userDao->addUsersLikeSomePages($system_page_ids, [$user['user_id']]);
                $userDao->addUserToSomeGroups($system_group_ids, [$user['user_id']]);
                /* END - CI */

                /* set cookies */
                $this->_set_cookies($user['user_id'], true);
                redirect();
            } else {
                if($this->_logged_in) {
                    /* [1] connecting social account */
                    $db->query(sprintf("UPDATE users SET $social_connected = '1', $social_id = %s WHERE user_id = %s", secure($user_profile->identifier), secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                    redirect('/settings/linked');
                } else {
                    
                    /* user cloud be connecting his social account or signing-up */
                    $_SESSION['social_id'] = $user_profile->identifier;
                    /* generate password */
                    $password = rand(100000, 999999);
                    /* register user */
                    $full_name = $user_profile->displayName;
                    $first_name = $user_profile->firstName;
                    $last_name = $user_profile->lastName;
                    $email = $user_profile->email;
                    $gender = $user_profile->gender;
                    $avatar = $user_profile->photoURL;

                    //Coniu - Hệ thống tự gen ra username cho user
                    $username = generateUsername($full_name);

                    $user_id = $this->socail_register($full_name, $first_name, $last_name, $username, $email, $password, $gender, $avatar, $provider);

                    /* CI - Cho user like các page và join các group của hệ thống */
                    $userDao->addUsersLikeSomePages($system_page_ids, [$user_id]);
                    $userDao->addUserToSomeGroups($system_group_ids, [$user_id]);
                    /* END - CI */

                    redirect();
                }
            }
            /* CI - END */
        }
    }


    /**
     * socail_register
     *
     * @param string $first_name
     * @param string $last_name
     * @param string $username
     * @param string $email
     * @param string $password
     * @param string $gender
     * @param string $avatar
     * @param string $provider
     * @param string $invitation_code
     * @return integer
     */
    public function socail_register($full_name, $first_name, $last_name, $username, $email, $password, $gender, $avatar, $provider, $invitation_code = "") {
        global $db, $system, $date;
        switch ($provider) {
            case 'facebook':
                $social_id = "facebook_id";
                $social_connected = "facebook_connected";
                break;

            case 'twitter':
                $social_id = "twitter_id";
                $social_connected = "twitter_connected";
                break;

            case 'google':
                $social_id = "google_id";
                $social_connected = "google_connected";
                break;

            case 'instagram':
                $social_id = "instagram_id";
                $social_connected = "instagram_connected";
                break;

            case 'linkedin':
                $social_id = "linkedin_id";
                $social_connected = "linkedin_connected";
                break;

            case 'vkontakte':
                $social_id = "vkontakte_id";
                $social_connected = "vkontakte_connected";
                break;

            default:
                _error(400);
                break;
        }
        /* check invitation code */
        if(!$system['registration_enabled'] && $system['invitation_enabled']) {
            if(!$this->check_invitation_code($invitation_code)) {
                throw new Exception(__("The invitation code you entered is invalid or expired"));
            }
        }
        /* check IP */
        $this->_check_ip();
        if(is_empty($first_name) || is_empty($last_name) || is_empty($username) || is_empty($password)) {
            throw new Exception(__("You must fill in all of the fields"));
        }
        if(!valid_username($username, "user")) {
            throw new Exception(__("Please enter a valid username (a-z0-9_.) with minimum 3 characters long"));
        }
        if(reserved_username($username)) {
            throw new Exception(__("You can't use")." ".$username." ".__("as username"));
        }
        if($this->check_username($username)) {
            throw new Exception(__("Sorry, it looks like")." <".$username." ".__("belongs to an existing account"));
        }
        if (!is_empty($email)) {
            if(!valid_email($email)) {
                throw new Exception(__("Please enter a valid email address"));
            }
            if($this->check_email($email)) {
                throw new Exception(__("Sorry, it looks like")." ".$email." ".__("belongs to an existing account"));
            }
        }

        if(strlen($password) < 6) {
            throw new Exception(__("Your password must be at least 6 characters long. Please try another"));
        }
        if(!valid_name($first_name)) {
            throw new Exception(__("Your first name contains invalid characters"));
        }
        /*if(strlen($first_name) < 3) {
            throw new Exception(__("Your first name must be at least 3 characters long. Please try another"));
        }*/
        if(!valid_name($last_name)) {
            throw new Exception(__("Your last name contains invalid characters"));
        }
        /*if(strlen($last_name) < 3) {
            throw new Exception(__("Your last name must be at least 3 characters long. Please try another"));
        }*/
        /*if(!in_array($gender, array('male', 'female'))) {
            throw new Exception(__("Please select a valid gender"));
        }*/
        $gender = ($gender == "male")? "male" : "female";

        /* save avatar */
        $image_new_name = '';
        /* check & create uploads dir */
        $depth = '../../..';
        $user_started = 0;
        /*if ($system['is_mobile']) {
            $depth = '';
            $user_started = 1;
        }*/

        if (!is_empty($avatar)) {
            $folder = 'photos';
            if(!file_exists($depth.$system['uploads_directory'].'/'.$folder)) {
                @mkdir($depth.$system['uploads_directory'].'/'.$folder, 0777, true);
            }
            if(!file_exists($depth.$system['uploads_directory'].'/'.$folder.'/' . date('Y'))) {
                @mkdir($depth.$system['uploads_directory'].'/'.$folder.'/' . date('Y'), 0777, true);
            }
            if(!file_exists($system['uploads_directory'].'/'.$folder.'/' . date('Y') . '/' . date('m'))) {
                @mkdir($depth.$system['uploads_directory'].'/'.$folder.'/' . date('Y') . '/' . date('m'), 0777, true);
            }

            /* prepare new file name */
            $directory = $folder.'/'. date('Y') . '/' . date('m') . '/';
            $prefix = $system['uploads_prefix'].'_'.md5(time()*rand(1, 9999));

            /* save avatar */
            $arrContextOptions=array(
                "ssl"=>array(
                    "verify_peer"=>false,
                    "verify_peer_name"=>false,
                ),
            );
            $profile_picture = @file_get_contents($avatar, false, stream_context_create($arrContextOptions));
            // Use @ to silent the error if user doesn't have any profile picture uploaded

            $image_new_name = $directory.$prefix.'.jpg';
            $path = ABSPATH.$system['uploads_directory'].$system["system_uploads_directory"].'/'.$image_new_name;
            file_put_contents($path, $profile_picture);
        }

        /* register user */
        $full_name = ucwords($full_name);
        if(!isset($email) || $email == '' || is_null($email)) {
            $email = 'null';
        }
        $db->query(sprintf("INSERT INTO users (user_name, user_email, user_password, user_fullname, user_firstname, user_lastname, user_gender, user_registered, user_activated, user_started, user_picture, $social_id, $social_connected) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, '1', %s, %s, %s, '1')",
            secure($username), secure($email), secure(_password_hash($password)), secure($full_name), secure(ucwords($first_name)), secure(ucwords($last_name)), secure($gender), secure($date), secure($user_started, 'int'), secure($image_new_name), secure($_SESSION['social_id']) )) or _error(SQL_ERROR_THROWEN);

        /* get user_id */
        $user_id = $db->insert_id;

        /* CI - Firebase */
        $user_pic = get_picture($image_new_name, $gender);
        $args = array(
            'user_id' => $user_id,
            'action' => CREATE_USER,
            'user_email' => $email,
            'user_fullname' => $full_name,
            'user_name' => $username,
            'user_picture' => $user_pic,
            'is_online' => 0,
            'count_chat' => 0,
            'last_login' => date(DB_DATETIME_FORMAT_DEFAULT),
            'provider' => $provider
        );
        insertBackgroundFirebase($args);
        /* END-CI */

        /* set cookies */
        $this->_set_cookies($user_id);
        return $user_id;
    }

    /* ------------------------------- */
    /* Invitations */
    /* ------------------------------- */

    /**
     * get_invitation_code
     *
     * @return string
     */
    public function get_invitation_code() {
        global $db, $system, $date;
        if(!$system['invitation_enabled']) {
            throw new Exception(__("Please enable the invitations system first to get new invitation code"));
        }
        $code = get_hash_key();
        $db->query(sprintf("INSERT INTO invitation_codes (code, date) VALUES (%s, %s)", secure($code), secure($date) )) or _error(SQL_ERROR_THROWEN);
        return $code;
    }


    /**
     * check_invitation_code
     *
     * @param string $code
     * @return boolean
     *
     */
    public function check_invitation_code($code) {
        global $db;
        $query = $db->query(sprintf("SELECT * FROM invitation_codes WHERE code = %s AND valid = '1'", secure($code) )) or _error(SQL_ERROR_THROWEN);
        if($query->num_rows > 0) {
            return true;
        }
        return false;
    }


    /**
     * update_invitation_code
     *
     * @param string $code
     * @return void
     */
    public function update_invitation_code($code) {
        global $db;
        $db->query(sprintf("UPDATE invitation_codes SET valid = '0' WHERE code = %s", secure($code) )) or _error(SQL_ERROR_THROWEN);
    }


    /**
     * send_invitation_code
     *
     * @param string $code
     * @param string $email
     * @return void
     */
    public function send_invitation_code($code, $email) {
        global $db, $system;
        /* check invitation code */
        if(!$this->check_invitation_code($code)) {
            throw new Exception(__("This invitation code is expired"));
        }
        /* check email */
        if(!valid_email($email)) {
            throw new Exception(__("Please enter a valid email address"));
        }
        if($this->check_email($email)) {
            throw new Exception(__("Sorry, it looks like")." ".$email." ".__("belongs to an existing account"));
        }
        /* prepare invitation email */
        $subject = __("Invitation to join")." ".$system['system_title'];
        $body  = __("Hi").",";
        $body .= "\r\n\r\n".__("We happy to invite you to our website")." ".$system['system_title'];
        $body .= "\r\n\r\n".__("To complete the sign-up process, please follow this link:");
        $body .= "\r\n\r\n".$system['system_url']."/signup/";
        $body .= "\r\n\r\n".__("Your invitation code").": ".$code;
        $body .= "\r\n\r".$system['system_title']." ".__("Team");
        /* send email */
        if(!_email($email, $subject, $body)) {
            throw new Exception(__("Invitation email could not be sent"));
        }
    }

    /* ------------------------------- */
    /* Password */
    /* ------------------------------- */

    /**
     * forget_password
     * 
     * @param string $email
     * @param string $recaptcha_response
     * @return void
     */
    public function forget_password($email, $recaptcha_response) {
        global $db, $system;
        if(!valid_email($email)) {
            throw new Exception(__("Please enter a valid email address"));
        }
        if(!$this->check_email($email)) {
            throw new Exception(__("Sorry, it looks like")." ".$email." ".__("doesn't belong to any account"));
        }
        /* CI- check reCAPTCHA đối với web */
        if($system['reCAPTCHA_enabled'] && !$system['is_mobile']) {
            require_once(ABSPATH.'includes/libs/ReCaptcha/autoload.php');
        	$recaptcha = new \ReCaptcha\ReCaptcha($system['reCAPTCHA_secret_key']);
            $resp = $recaptcha->verify($recaptcha_response, get_user_ip());
            if (!$resp->isSuccess()) {
                throw new Exception(__("The secuirty check is incorrect. Please try again"));
            }
        }
        /* generate reset key */
        $reset_key = get_hash_key(6);
        /* update user */
        $db->query(sprintf("UPDATE users SET user_reset_key = %s, user_reseted = '1' WHERE user_email = %s", secure($reset_key), secure($email) )) or _error(SQL_ERROR_THROWEN);
        /* send reset email */
        /* prepare reset email */
        $subject = __("Forget password activation key!");
        $body  = __("Hi")." ".$email.",";
        $body .= "\r\n\r\n".__("To complete the reset password process, please copy this token:");
        $body .= "\r\n\r\n".__("Token:")." ".$reset_key;
        $body .= "\r\n\r".$system['system_title']." ".__("Team");
        /* send email */
        /*Ci-mobile thay đổi giá trị $args['email'] = $email*/
        if(!_email($email, $subject, $body)) {
            throw new Exception(__("Activation key email could not be sent!"));
        }
    }


    /**
     * forget_password_confirm
     * 
     * @param string $email
     * @param string $reset_key
     * @return void
     */
    public function forget_password_confirm($email, $reset_key) {
        global $db;
        if(!valid_email($email)) {
        	throw new Exception(__("Invalid email, please try again"));
        }
        /* check reset key */
        $check_key = $db->query(sprintf("SELECT user_reset_key FROM users WHERE user_email = %s AND user_reset_key = %s AND user_reseted = '1'", secure($email), secure($reset_key))) or _error(SQL_ERROR_THROWEN);
        if($check_key->num_rows == 0) {
        	throw new Exception(__("Invalid code, please try again."));
        }
    }


    /**
     * forget_password_reset
     * 
     * @param string $email
     * @param string $reset_key
     * @param string $password
     * @param string $confirm
     * @return void
     */
    public function forget_password_reset($email, $reset_key, $password, $confirm) {
        global $db;
        if(!valid_email($email)) {
        	throw new Exception(__("Invalid email, please try again"));
        }
        /* check reset key */
        $check_key = $db->query(sprintf("SELECT user_reset_key FROM users WHERE user_email = %s AND user_reset_key = %s AND user_reseted = '1'", secure($email), secure($reset_key))) or _error(SQL_ERROR_THROWEN);
        if($check_key->num_rows == 0) {
        	throw new Exception(__("Invalid code, please try again."));
        }
        /* check password length */
        if(strlen($password) < 6) {
            throw new Exception(__("Your password must be at least 6 characters long. Please try another"));
        }
        /* check password confirm */
        if($password !== $confirm) {
            throw new Exception(__("Your passwords do not match. Please try another"));
        }
        /* update user password */
        $db->query(sprintf("UPDATE users SET user_password = %s, user_reseted = '0' WHERE user_email = %s", secure(_password_hash($password)), secure($email) )) or _error(SQL_ERROR_THROWEN);
    }



    /* ------------------------------- */
    /* Activation Email */
    /* ------------------------------- */

    /**
     * activation_email_resend
     *
     * @return void
     */
    public function activation_email_resend() {
        global $db, $system;
        /* generate user activation key */
        $activation_key = get_hash_token();
        /* update user */
        $db->query(sprintf("UPDATE users SET user_activation_key = %s WHERE user_id = %s", secure($activation_key), secure($this->_data['user_id']) )) or _error(SQL_ERROR_THROWEN);
        // resend activation email
        /* prepare activation email */
        $subject = __("Just one more step to get started on")." ".$system['system_title'];
        $body  = __("Hi")." ".ucwords(convertText4Web($this->_data['user_fullname'])).",";
        $body .= "\r\n\r\n".__("To complete the sign-up process, please follow this link:");
        $body .= "\r\n\r\n".$system['system_url']."/activation/".$this->_data['user_id']."/".$activation_key;
        $body .= "\r\n\r\n".__("Welcome to")." ".$system['system_title'];
        $body .= "\r\n\r".$system['system_title']." ".__("Team");
        /* send email */
        $email_address = is_empty($this->_data['user_email_activation']) ? $this->_data['user_email'] : $this->_data['user_email_activation'];
        if(!_email($email_address, $subject, $body)) {
            throw new Exception(__("Activation email could not be sent"));
        }
    }


    /**
     * activation_email_reset
     *
     * @param string $email
     * @return void
     */
    public function activation_email_reset($email) {
        global $db, $system;
        if(!valid_email($email)) {
            throw new Exception(__("Invalid email, please try again"));
        }
        if($this->check_email($email)) {
            throw new Exception(__("Sorry, it looks like")." ".$email." ".__("belongs to an existing account"));
        }
        /* generate user activation key */
        $activation_key = get_hash_token();
        /* update user */
        $db->query(sprintf("UPDATE users SET user_email = %s, user_activation_key = %s WHERE user_id = %s", secure($email), secure($activation_key), secure($this->_data['user_id']) )) or _error(SQL_ERROR_THROWEN);
        // send activation email
        /* prepare activation email */
        $subject = __("Just one more step to get started on")." ".$system['system_title'];
        $body  = __("Hi")." ".ucwords(convertText4Web($this->_data['user_fullname'])).",";
        $body .= "\r\n\r\n".__("To complete the sign-up process, please follow this link:");
        $body .= "\r\n\r\n".$system['system_url']."/activation/".$this->_data['user_id']."/".$activation_key;
        $body .= "\r\n\r\n".__("Welcome to")." ".$system['system_title'];
        $body .= "\r\n\r".$system['system_title']." ".__("Team");
        /* send email */
        if(!_email($email, $subject, $body)) {
            throw new Exception(__("Activation email could not be sent. But you can login now"));
        }
    }


    /**
     * activation_email
     *
     * @param integer $id
     * @param string $token
     * @return void
     */
    public function activation_email($id, $token) {
        global $db;
        if($this->_logged_in && !$this->_data['user_activated']) {
            if($this->_data['user_id'] != $id && $this->_data['user_activation_key'] != $token) {
                _error(404);
            }

            /* activate user */
            $db->query(sprintf("UPDATE users SET user_activated = '1' WHERE user_id = %s", secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
            /* redirect */
            //redirect();

        } else if($this->_logged_in && $this->_data['user_activated'] && !is_empty($this->_data['user_email_activation'])) {
            if($this->_data['user_id'] != $id && $this->_data['user_activation_key'] != $token) {
                _error(404);
            }

            /* activate user */
            $db->query(sprintf("UPDATE users SET user_email = %s, user_email_activation = %s WHERE user_id = %s", secure($this->_data['user_email_activation']), secure("null"), secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);

            /* CI - Firebase */
            $arr = array(
                'user_id' => $this->_data['user_id'],
                'action' => UPDATE_USER,
                'user_email' => $this->_data['user_email_activation']
            );
            insertBackgroundFirebase($arr);
            /* END - Firebase */

            /* redirect */
            redirect('/settings');

        } else {
            $check_user = $db->query(sprintf("SELECT user_id, user_email, user_email_activation FROM users WHERE user_id = %s AND user_activation_key = %s", secure($id, 'int'), secure($token) )) or _error(SQL_ERROR_THROWEN);
            if($check_user->num_rows == 0) {
                _error(404);
            }
            $_user = $check_user->fetch_assoc();
            if ($_user['user_activated']) {
                if (is_empty($_user['user_email_activation'])) {
                    _error(404);
                }
                $db->query(sprintf("UPDATE users SET user_email = %s, user_email_activation = %s WHERE user_id = %s", secure($_user['user_email_activation']), secure("null"), secure($_user['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);

                /* CI - Firebase */
                $arr = array(
                    'user_id' => $_user['user_id'],
                    'action' => UPDATE_USER,
                    'user_email' => $_user['user_email_activation']
                );
                insertBackgroundFirebase($arr);
                /* END - Firebase */

            } else {
                /* activate user */
                $db->query(sprintf("UPDATE users SET user_activated = '1' WHERE user_id = %s", secure($_user['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
            }

            /* set cookies */
            $this->_set_cookies($_user['user_id']);
        }
    }

    /**
     * check_email_verified
     *
     * @param integer $user_id
     * @return array
     *
     */
    public function check_email_verified() {
        global $db;
        $return = array();
        $return['is_verified'] = false;
        $return['email'] = '';
        $query = $db->query(sprintf("SELECT user_email FROM users WHERE user_id = %s AND (user_email_activation IS NULL OR user_email_activation = '') ", secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
        if($query->num_rows > 0) {
            $return['email'] = $query->fetch_assoc()['user_email'];
            $return['is_verified'] = true;
        }
        return $return;
    }



    /* ------------------------------- */
    /* Security Checks */
    /* ------------------------------- */

    /**
     * check_email
     *
     * @param string $email
     * @param boolean $return_info
     * @return boolean|array
     *
     */
    public function check_email($email, $return_info = false) {
        global $db;
        $query = $db->query(sprintf("SELECT * FROM users WHERE user_email = %s", secure($email) )) or _error(SQL_ERROR_THROWEN);
        if($query->num_rows > 0) {
            if($return_info) {
                $info = $query->fetch_assoc();
                return $info;
            }
            return true;
        }
        return false;
    }

    /**
     * check_phone
     *
     * @param string $phone
     * @param boolean $return_info
     * @return boolean|array
     *
     */
    public function check_phone($phone, $return_info = false) {
        global $db;

        $arrPhone = getArrayPhone($phone);
        $strCon = implode(',', $arrPhone);
        $strCon = trim($strCon, ",");

        $query = $db->query(sprintf("SELECT * FROM users WHERE user_phone_signin IN (%s)", $strCon )) or _error(SQL_ERROR_THROWEN);
        if($query->num_rows > 0) {
            if($return_info) {
                $info = $query->fetch_assoc();
                return $info;
            }
            return true;
        }
        return false;
    }


    /**
     * check_phone
     *
     * @param string $phone
     * @param boolean $return_info
     * @return boolean|array
     *
     */
    public function check_phone_setting($phone) {
        global $db;

        $arrPhone = getArrayPhone($phone);
        $strCon = implode(',', $arrPhone);
        $strCon = trim($strCon, ",");

        $query = $db->query(sprintf("SELECT * FROM users WHERE user_id != %s AND user_phone_signin IN (%s)", secure($this->_data['user_id'], 'int'),  $strCon )) or _error(SQL_ERROR_THROWEN);
        if($query->num_rows > 0) {
            return true;
        }
        return false;
    }

    /**
     * check_username
     *
     * @param string $username
     * @param string $type
     * @param boolean $return_info
     * @return boolean|array
     */
    public function check_username($username, $type = 'user', $return_info = false) {
        global $db;
        /* check type (user|page|group) */
        switch ($type) {
            case 'page':
                $query = $db->query(sprintf("SELECT * FROM pages WHERE page_name = %s", secure($username) )) or _error(SQL_ERROR_THROWEN);
                break;

            case 'group':
                $query = $db->query(sprintf("SELECT * FROM groups WHERE group_name = %s", secure($username) )) or _error(SQL_ERROR_THROWEN);
                break;

            default:
                $query = $db->query(sprintf("SELECT * FROM users WHERE user_name = %s", secure($username) )) or _error(SQL_ERROR_THROWEN);
                break;
        }
        if($query->num_rows > 0) {
            if($return_info) {
                $info = $query->fetch_assoc();
                return $info;
            }
            return true;
        }
        return false;
    }


    /**
     * check_username_and_phone
     *
     * @param string $username
     * @param string $type
     * @param boolean $return_info
     * @return boolean|array
     */
    public function check_username_phone($username_phone, $return_info = false) {
        global $db;
        $query = $db->query(sprintf("SELECT * FROM users WHERE user_name = %s", secure($username_phone) )) or _error(SQL_ERROR_THROWEN);

        if($query->num_rows > 0) {
            if($return_info) {
                $info = $query->fetch_assoc();
                return $info;
            }
            return true;
        }
        return false;
    }

# --- BEGIN API CONIU ---

    /* ------------------------------- */
    /* API User Sign (in|up|out) */
    /* ------------------------------- */

    /**
     * sign_up
     *
     * @param string $full_name
     * @param string $username
     * @param string $email
     * @param string $password
     * @param string $gender
     * @return $user
     */
    public function api_sign_up($args = []) {
        global $db, $system, $date;
        if(is_empty($args['first_name']) || is_empty($args['last_name']) || is_empty($args['password'])) {
            throw new Exception(__("You must fill in all of the fields"));
        }

        //Coniu - trim các dữ liệu truyền vào
        $full_name = ucwords(trim($args['last_name']." ".$args['first_name']));
        $email = trim($args['email']);
        $device_token = is_empty($args['device_token'])? 'null' : $args['device_token'];

        //Coniu - Hệ thống tự gen ra username cho user
        $username = is_empty($args['username'])? "" : trim($args['username']);

        if (is_empty($username)) {
            $username = generateUsername($full_name);
        } else {
            if(!valid_username($username, "user")) {
                throw new Exception(__("Please enter a valid username (a-z0-9_.) with minimum 3 characters long"));
            }
            if(reserved_username($username)) {
                throw new Exception(__("You can't use")." ".$username." ".__("as username"));
            }
            if($this->check_username($username)) {
                throw new Exception(__("Sorry, it looks like")." ".$username." ".__("belongs to an existing account"));
            }
        }

        if(valid_email($email)) {
            if($this->check_email($args['email'])) {
                throw new Exception(__("Sorry, it looks like")." ".$email." ".__("belongs to an existing account"));
            }
        } else {
            throw new Exception(__("Please enter a valid email address or mobile number"));
        }

        if(strlen($args['password']) < 6) {
            throw new Exception(__("Your password must be at least 6 characters long. Please try another"));
        }
        if(!valid_name($args['first_name'])) {
            throw new Exception(__("Your first name contains invalid characters"));
        }

        if(!valid_name($args['last_name'])) {
            throw new Exception(__("Your last name contains invalid characters"));
        }

        if(!in_array($args['gender'], array('male', 'female'))) {
            throw new Exception(__("Please select a valid gender"));
        }
        $args['birth_date'] = 'null';

        $picture = get_picture('', $args['gender']);

        /* generate activation_key */
        $activation_key = ($system['activation_type'] == "email")? get_hash_token(): get_hash_key();
        /* register user */
        $db->query(sprintf("INSERT INTO users (user_name, user_email, user_password, user_fullname, user_firstname, user_lastname, user_gender, user_birthdate, user_registered, user_activation_key) 
                            VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
                            secure($username), secure($email), secure(_password_hash($args['password'])), secure($full_name), secure(ucwords($args['first_name'])), secure(ucwords($args['last_name'])), secure($args['gender']), secure($args['birth_date']), secure($date), secure($activation_key) )) or _error(SQL_ERROR_THROWEN);
        /* get user_id */
        $user_id = $db->insert_id;

        /* CI - Firebase */
        $args = array(
            'user_id' => $user_id,
            'action' => CREATE_USER,
            'user_email' => $email,
            'user_fullname' => $full_name,
            'user_name' => $username,
            'user_picture' => $picture,
            'is_online' => 0,
            'count_chat' => 0,
            'last_login' => date(DB_DATETIME_FORMAT_DEFAULT),
            'provider' => 'inet'
        );
        insertBackgroundFirebase($args);
        /* END-CI */

        /* send activation */
        if($system['activation_enabled']) {
            $subject = __("Just one more step to get started on")." ".$system['system_title'];
            $body  = __("Hi")." ".$full_name.",";
            $body .= "\r\n\r\n".__("To complete the sign-up process, please follow this link:");
            $body .= "\r\n\r\n".$system['system_url']."/activation/".$user_id."/".$activation_key;
            $body .= "\r\n\r\n".__("Welcome to")." ".$system['system_title'];
            $body .= "\r\n\r".$system['system_title']." "."Team";
            /* send email */
            if(!_email($email, $subject, $body)) {
                throw new Exception(__("Activation email could not be sent. But you can login now"));
            }
        }

        /* CI - Chỉ giữ session của 5 phiên đăng nhập gần nhất */
        $db->query(sprintf('DELETE FROM users_sessions WHERE user_id = %1$s AND session_id NOT IN (SELECT * FROM (SELECT session_id FROM users_sessions WHERE user_id = %1$s ORDER BY session_id DESC LIMIT 4) sid) ', secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        /* END - CI */

        /* generate new token */
        $session_token = get_hash_token();
        /* insert user token */
        $db->query(sprintf("INSERT INTO users_sessions (session_token, device_token, session_date, user_id, user_browser, user_os, user_ip) VALUES (%s, %s, %s, %s, %s, %s, %s)", secure($session_token), secure($device_token), secure($date), secure($user_id, 'int'), secure(get_user_browser()), secure(get_user_os()), secure(get_user_ip()) )) or _error(SQL_ERROR_THROWEN);
        /* update last login time */
        $db->query(sprintf("UPDATE users SET user_last_login = %s WHERE user_id = %s", secure($date), secure($user_id, 'int') )) or _error(SQL_ERROR);

        $user = array();
        $user['user_id'] = $user_id;
        $user['user_name'] = $username;
        $user['user_email'] = $email;
        $user['user_token'] = $session_token;
        $user['user_fullname'] = $full_name;
        $user['user_picture'] = $picture;

        return $user;
    }


    /**
     * API sign_in
     *
     * @param string $username_email
     * @param string $password
     * @param boolean $remember
     *
     * @return $user
     */
    public function api_signin($username_email, $password, $device_token = "") {
        global $db, $date,$system;
        /* valid inputs */
        if(is_empty($username_email) || is_empty($password)) {
            throw new Exception(__("You must fill in all of the fields"));
        }

        /* check if username or email */
        if(valid_email($username_email)) {
            $user = $this->check_email($username_email, true);
            if($user === false) {
                throw new Exception(__("The email you entered does not belong to any account"));
            }
            $field = "user_email";
        } else if(valid_username($username_email, "user")) {
            $user = $this->check_username($username_email, 'user', true);
            if($user === false) {
                throw new Exception(__("The username you entered does not belong to any account"));
            }
            $field = "user_name";
        } else {
            throw new Exception(__("Please enter a valid email address or mobile number"));
        }

        /* check password */
        if(md5($password) == $user['user_password']) {
            /* validate current password (MD5 check for versions < v2.5) */
            $user['user_password'] = _password_hash($password);
            /* update user password hash from MD5 */
            $db->query(sprintf("UPDATE users SET user_password = %s WHERE user_id = %s", secure($user['user_password']), secure($user['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
        }
        if(!password_verify($password, $user['user_password'])) {
            throw new Exception(__("The password you entered is incorrect"));
        }

        /* CI - Chỉ giữ session của 5 phiên đăng nhập gần nhất */
        $db->query(sprintf('DELETE FROM users_sessions WHERE user_id = %1$s AND session_id NOT IN (SELECT * FROM (SELECT session_id FROM users_sessions WHERE user_id = %1$s ORDER BY session_id DESC LIMIT 4) sid) ', secure($user['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
        /* END - CI */

        /* generate new token */
        $session_token = get_hash_token();
        /* insert user token */
        $db->query(sprintf("INSERT INTO users_sessions (session_token, device_token, session_date, user_id, user_browser, user_os, user_ip) VALUES (%s, %s, %s, %s, %s, %s, %s)", secure($session_token), secure($device_token), secure($date), secure($user['user_id'], 'int'), secure(get_user_browser()), secure(get_user_os()), secure(get_user_ip()) )) or _error(SQL_ERROR_THROWEN);

        /* update last login time */
        $db->query(sprintf("UPDATE users SET user_last_login = %s WHERE user_id = %s", secure($date), secure($user['user_id'], 'int') )) or _error(SQL_ERROR);

        $get_user = $db->query(sprintf('SELECT user_id FROM ci_user_manage WHERE user_id = %1$s AND status = %2$s AND object_type != 1 UNION SELECT user_id FROM ci_teacher WHERE user_id = %1$s AND status = %2$s', secure($user['user_id'], 'int'), secure(STATUS_ACTIVE, 'int'))) or _error(SQL_ERROR_THROWEN);

        $users = array();
        $users['user_id'] = $user['user_id'];
        $users['user_name'] = $user['user_name'];
        $users['user_email'] = $user['user_email'];
        //$users['session_token'] = $session_token;
        $users['user_token'] = $session_token;
        $users['user_fname'] = $user['user_firstname'];
        $users['user_lname'] = $user['user_lastname'];
        $users['user_fullname'] = $user['user_fullname'];
        $users['user_picture'] = $system['system_uploads']."/".$user['user_picture'];
        $users['is_manager'] = ($get_user->num_rows > 0);

        return $users;
    }
    
    public function api_sign_out($user_id, $session_token) {
        global $db;

        /* delete the session */
        $db->query(sprintf("DELETE FROM users_sessions WHERE session_token = %s AND user_id = %s", secure($session_token), secure($user_id) )) or _error(SQL_ERROR_THROWEN);
    }


    /**
     * API check User registed social
     *
     * @param $facebookId
     * @param $googleId
     * @return $user
     */

    public function social_signin($user_profile = array())
    {
        global $db, $date;
        switch ($user_profile['provider']) {
            case 'facebook':
                $social_id = "facebook_id";
                $social_connected = "facebook_connected";
                break;

            case 'twitter':
                $social_id = "twitter_id";
                $social_connected = "twitter_connected";
                break;

            case 'google':
                $social_id = "google_id";
                $social_connected = "google_connected";
                break;

            case 'linkedin':
                $social_id = "linkedin_id";
                $social_connected = "linkedin_connected";
                break;

            case 'vkontakte':
                $social_id = "vkontakte_id";
                $social_connected = "vkontakte_connected";
                break;
        }
        $user = null;
        /* check if user connected or not */
        $check_user = $db->query(sprintf("SELECT user_id, user_name, user_email, user_fullname, user_firstname, user_lastname, user_gender, user_picture FROM users 
            WHERE $social_id = %s", secure($user_profile['social_id']) )) or _error(SQL_ERROR_THROWEN);
        if($check_user->num_rows > 0) {
            /* social account connected and just signing-in */
            $user = $check_user->fetch_assoc();
            /* if user logged-in */
            $user['user_picture'] =  get_picture($user['user_picture'], $user['user_gender']);
            $user['connected'] = 1;

            $get_user_manager = $db->query(sprintf('SELECT user_id FROM ci_user_manage WHERE user_id = %1$s AND status = %2$s AND object_type != 1 UNION SELECT user_id FROM ci_teacher WHERE user_id = %1$s AND status = %2$s', secure($user['user_id'], 'int'), secure(STATUS_ACTIVE, 'int'))) or _error(SQL_ERROR_THROWEN);
            $user['is_manager'] = ($get_user_manager->num_rows > 0);

        } else {
            $get_user_email = '';
            if (!is_empty($user_profile['email'])) {
                $get_user_email = $db->query(sprintf("SELECT user_id FROM users WHERE user_email = %s", secure($user_profile['email']))) or _error(SQL_ERROR_THROWEN);
            }

            // Kiểm tra nếu đã đăng ký bằng email này thì update thông tin social connect
            if($get_user_email->num_rows > 0) {
                $connected = 1;
                $user['user_id'] = $get_user_email->fetch_assoc()['user_id'];
                $db->query(sprintf("UPDATE users SET $social_connected = '1', $social_id = %s, device_token = %s WHERE user_id = %s", secure($user_profile['social_id']), secure($user_profile['device_token']), secure($user['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);

            } else {
                $connected = 0;
                /* CI - Đăng ký account cho user khi đăng nhập lần đầu qua social */
                $_SESSION['social_id'] = $user_profile['social_id'];
                /* generate password */
                $password = rand(100000, 999999);
                /* register user */
                $first_name = $user_profile['first_name'];
                $last_name = $user_profile['last_name'];
                $email = $user_profile['email'];
                $gender = $user_profile['gender'];
                $avatar = $user_profile['avatar'];
                $provider = $user_profile['provider'];

                //Coniu - Hệ thống tự gen ra username cho user
                $full_name = $last_name . " " . $first_name;
                $username = generateUsername($full_name);

                $user['user_id'] = $this->socail_register($full_name, $first_name, $last_name, $username, $email, $password, $gender, $avatar, $provider);
            }

            // get user
            $get_user = $db->query(sprintf("SELECT user_id, user_name, user_email, user_fullname, user_firstname, user_lastname, user_gender, user_picture FROM users WHERE $social_id = %s", secure($user_profile['social_id']) )) or _error(SQL_ERROR_THROWEN);

            if ($get_user->num_rows == 0) {
                throw new Exception(__("There is something that went wrong!"));
            }
            $user = $get_user->fetch_assoc();
            $user['user_picture'] =  get_picture($user['user_picture'], $user['user_gender']);
            $user['connected'] = $connected;

            $get_user = $db->query(sprintf('SELECT user_id FROM ci_user_manage WHERE user_id = %1$s AND status = %2$s AND object_type != 1 UNION SELECT user_id FROM ci_teacher WHERE user_id = %1$s AND status = %2$s', secure($user['user_id'], 'int'), secure(STATUS_ACTIVE, 'int'))) or _error(SQL_ERROR_THROWEN);
            $user['is_manager'] = ($get_user->num_rows > 0);
            /* CI - END */
        }

        /* CI - Chỉ giữ session của 5 phiên đăng nhập gần nhất */
        $db->query(sprintf('DELETE FROM users_sessions WHERE user_id = %1$s AND session_id NOT IN (SELECT * FROM (SELECT session_id FROM users_sessions WHERE user_id = %1$s ORDER BY session_id DESC LIMIT 4) sid) ', secure($user['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
        /* END - CI */

        /* device token */
        $device_token = is_empty($user_profile['device_token']) ? 'null': $user_profile['device_token'];
        /* generate new token */
        $session_token = get_hash_token();
        $user['user_token'] = $session_token;
        /* insert user token */
        $db->query(sprintf("INSERT INTO users_sessions (session_token, device_token, session_date, user_id, user_browser, user_os, user_ip) VALUES (%s, %s, %s, %s, %s, %s, %s)",
            secure($session_token), secure($device_token), secure($date), secure($user['user_id'], 'int'), secure(get_user_browser()), secure(get_user_os()), secure(get_user_ip()) )) or _error(SQL_ERROR_THROWEN);
        /* update last login time */
        $db->query(sprintf("UPDATE users SET user_last_login = %s WHERE user_id = %s", secure($date), secure($user['user_id'], 'int') )) or _error(SQL_ERROR);
        return $user;
    }

    /**
     * check User registed social in DB
     *
     * @param $field
     * @param $social_id
     * @return boolean
     */

    public function check_social($field, $social_id)
    {
        global $db;
        $query = $db->query(sprintf("SELECT user_id FROM users WHERE " . $field . " = %s", secure($social_id))) or _error(SQL_ERROR_THROWEN);
        if ($query->num_rows > 0) {
            return true;
        }
        return false;
    }


    /**
     * API get_friend_requests
     *
     * @param integer $offset
     * @param integer $last_request_id
     * @return array
     */
    public function api_get_friend_requests() {
        global $db;
        $requests = array();
        $get_requests = $db->query(sprintf("SELECT friends.id, friends.user_one_id as user_id, users.user_name, users.user_fullname, users.user_firstname, users.user_lastname, users.user_gender, users.user_picture FROM friends INNER JOIN users ON friends.user_one_id = users.user_id WHERE friends.status = 0 AND friends.user_two_id = %s ORDER BY friends.id", secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);

        if($get_requests->num_rows > 0) {
            while($request = $get_requests->fetch_assoc()) {
                $request['user_picture'] = $this->get_picture($request['user_picture'], $request['user_gender']);
                $request['mutual_friends_count'] = $this->get_mutual_friends_count($request['user_id']);
                $requests[] = $request;
            }
        }
        return $requests;
    }

    /* ------------------------------- */
    /* Posts Gets */
    /* ------------------------------- */

    /**
     * API get_posts
     *
     * @param array $args
     * @return array
     */
    public function api_get_posts(array $args = array()) {
        global $db, $system;
        /* initialize vars */
        $posts = array();
        /* validate arguments */
        $get = !isset($args['get']) ? 'newsfeed' : $args['get'];
        $filter = !isset($args['filter']) ? 'all' : $args['filter'];
        $valid['filter'] = array('all', '', 'photos', 'video', 'audio', 'file', 'poll', 'product', 'article', 'map');
        if(!in_array($filter, $valid['filter'])) {
            _api_error(400);
        }
        $last_post_id = !isset($args['last_post_id']) ? null : $args['last_post_id'];
        if(isset($args['last_post']) && !is_numeric($args['last_post'])) {
            _api_error(400);
        }
        $offset = !isset($args['offset']) ? 0 : $args['offset'];
        $offset *= $system['max_results'];
        if(isset($args['query'])) {
            if(is_empty($args['query'])) {
                return $posts;
            } else {
                $query = secure($args['query'], 'search', false);
            }
        }
        $order_query = "ORDER BY posts.post_id DESC";
        $where_query = "";
        /* get postsc */
        switch ($get) {
            case 'newsfeed':
                if(!$this->_logged_in && $query) {
                    $where_query .= "WHERE (";
                    $where_query .= "(posts.text LIKE $query)";
                    /* get only public posts [except: group posts & event posts & wall posts] */
                    $where_query .= " AND (posts.in_group = '0' AND posts.in_event = '0' AND posts.in_wall = '0' AND posts.privacy = 'public')";
                    $where_query .= ")";
                } else {
                    /* get viewer user's newsfeed */
                    $where_query .= "WHERE (";
                    /* get viewer posts */
                    $me = $this->_data['user_id'];
                    $where_query .= "(posts.user_id = $me AND posts.user_type = 'user')";
                    /* get posts from friends still followed */
                    $friends_ids = array_intersect($this->_data['friends_ids'], $this->_data['followings_ids']);
                    if($friends_ids) {
                        $friends_list = implode(',',$friends_ids);
                        /* viewer friends posts -> authors */
                        $where_query .= " OR (posts.user_id IN ($friends_list) AND posts.user_type = 'user' AND posts.privacy = 'friends' AND posts.in_group = '0')";
                        /* viewer friends posts -> their wall posts */
                        $where_query .= " OR (posts.in_wall = '1' AND posts.wall_id IN ($friends_list) AND posts.user_type = 'user' AND posts.privacy = 'friends')";
                    }
                    /* get posts from followings */
                    if($this->_data['followings_ids']) {
                        $followings_list = implode(',',$this->_data['followings_ids']);
                        /* viewer followings posts -> authors */
                        $where_query .= " OR (posts.user_id IN ($followings_list) AND posts.user_type = 'user' AND posts.privacy = 'public' AND posts.in_group = '0')";
                        /* viewer followings posts -> their wall posts */
                        $where_query .= " OR (posts.in_wall = '1' AND posts.wall_id IN ($followings_list) AND posts.user_type = 'user' AND posts.privacy = 'public')";
                    }
                    /* get pages posts */
                    $pages_ids = $this->get_pages_ids();
                    if($pages_ids) {
                        $pages_list = implode(',',$pages_ids);
                        $where_query .= " OR (posts.user_id IN ($pages_list) AND posts.user_type = 'page')";
                    }
                    /* get groups (approved only) posts & exclude the viewer posts */
                    $groups_ids = $this->get_groups_ids(true);
                    if($groups_ids) {
                        $groups_list = implode(',',$groups_ids);
                        $where_query .= " OR (posts.group_id IN ($groups_list) AND posts.in_group = '1' AND posts.user_id != $me)";
                    }
                    /* get events posts & exclude the viewer posts */
                    /*$events_ids = $this->get_events_ids();
                    if($events_ids) {
                        $events_list = implode(',',$events_ids);
                        $where_query .= " OR (posts.event_id IN ($events_list) AND posts.in_event = '1' AND posts.user_id != $me)";
                    }*/
                    $where_query .= ")";
                    if($query) {
                        $where_query .= " AND (posts.text LIKE $query)";
                    }
                }
                break;

            case 'posts_profile':
                if(isset($args['id']) && !is_numeric($args['id'])) {
                    _error(400);
                }
                $id = $args['id'];
                /* get target user's posts */
                /* check if there is a viewer user */
                if($this->_logged_in) {
                    /* check if the target user is the viewer */
                    if($id == $this->_data['user_id']) {
                        /* get all posts */
                        $where_query .= "WHERE (";
                        /* get all target posts */
                        $where_query .= "(posts.user_id = $id AND posts.user_type = 'user')";
                        /* get taget wall posts */
                        $where_query .= " OR (posts.wall_id = $id AND posts.in_wall = '1')";
                        $where_query .= ")";
                    } else {
                        /* check if the viewer & the target user are friends */
                        if(in_array($id, $this->_data['friends_ids'])) {
                            $where_query .= "WHERE (";
                            /* get all target posts [except: group posts] */
                            $where_query .= "(posts.user_id = $id AND posts.user_type = 'user' AND posts.in_group = '0' AND posts.privacy != 'me' )";
                            /* get taget wall posts */
                            $where_query .= " OR (posts.wall_id = $id AND posts.in_wall = '1')";
                            $where_query .= ")";
                        } else {
                            /* get only public posts [except: wall posts & group posts] */
                            $where_query .= "WHERE (posts.user_id = $id AND posts.user_type = 'user' AND posts.in_group = '0' AND posts.in_wall = '0' AND posts.privacy = 'public')";
                        }
                    }
                } else {
                    /* get only public posts [except: wall posts & group posts] */
                    $where_query .= "WHERE (posts.user_id = $id AND posts.user_type = 'user' AND posts.in_group = '0' AND posts.in_wall = '0' AND posts.privacy = 'public')";
                }
                break;

            case 'posts_page':
                if(isset($args['id']) && !is_numeric($args['id'])) {
                    _error(400);
                }
                $id = $args['id'];
                $where_query .= "WHERE (posts.user_id = $id AND posts.user_type = 'page')";
                break;

            case 'posts_group':
                if(isset($args['id']) && !is_numeric($args['id'])) {
                    _error(400);
                }
                $id = $args['id'];
                $where_query .= "WHERE (posts.group_id = $id AND posts.in_group = '1')";
                break;

            case 'posts_event':
                if(isset($args['id']) && !is_numeric($args['id'])) {
                    _error(400);
                }
                $id = $args['id'];
                $where_query .= "WHERE (posts.event_id = $id AND posts.in_event = '1')";
                break;

            case 'saved':
                $id = $this->_data['user_id'];
                $where_query .= "INNER JOIN posts_saved ON posts.post_id = posts_saved.post_id WHERE (posts_saved.user_id = $id)";
                $order_query = "ORDER BY posts_saved.time DESC";
                break;

            default:
                _error(400);
                break;
        }
        /* get his hidden posts to exclude from newsfeed */
        $hidden_posts = $this->_get_hidden_posts($this->_data['user_id']);
        if(count($hidden_posts) > 0) {
            $hidden_posts_list = implode(',',$hidden_posts);
            $where_query .= " AND (posts.post_id NOT IN ($hidden_posts_list))";
        }
        /* filter posts */
        if($filter != "all") {
            $where_query .= " AND (posts.post_type = '$filter')";
        }
        /* get posts */
        if($last_post_id != null && $get != 'saved') {
            $get_posts = $db->query(sprintf("SELECT * FROM (SELECT posts.post_id FROM posts ".$where_query.") posts WHERE posts.post_id > %s ORDER BY posts.post_id DESC", secure($last_post_id, 'int') )) or _error(SQL_ERROR_THROWEN);
        } else {
            $get_posts = $db->query(sprintf("SELECT posts.post_id FROM posts ".$where_query." ".$order_query." LIMIT %s, %s", secure($offset, 'int', false), secure($system['max_results'], 'int', false) )) or _error(SQL_ERROR_THROWEN);
        }
        if($get_posts->num_rows > 0) {
            while($post = $get_posts->fetch_assoc()) {
                $post = $this->get_post($post['post_id'], true, true); /* $full_details = true, $pass_privacy_check = true */
                if($post) {
                    $posts[] = $post;
                }
            }
        }
        return $posts;
    }


    /**
     * API get_post
     *
     * @param integer $post_id
     * @param boolean $isMobile
     * @param integer $offset
     * @param boolean $full_details
     * @param boolean $pass_privacy_check
     * @return array
     */
    //public function api_get_post($post_id, $full_details = true, $pass_privacy_check = false) {
    public function api_get_post(array $args = array()) {
        global $db, $system;

        $post_id = $args['post_id'];
        $offset = isset($args['offset']) ? $args['offset'] : 0;
        $full_details = isset($args['full_details']) ? $args['full_details'] : true;
        $pass_privacy_check = isset($args['pass_privacy_check']) ? $args['pass_privacy_check'] : false;

        $post = $this->_check_post($post_id, $pass_privacy_check);
        if(!$post) {
            return false;
        }

        // return full link for html
        if (!is_empty($post['user_picture']))
            $post['user_picture'] = $system['system_uploads'] . '/' . $post['user_picture'];
        if (!is_empty($post['user_cover']))
            $post['user_cover'] = $system['system_uploads'] . '/' . $post['user_cover'];
        if (!is_empty($post['page_picture']))
            $post['page_picture'] = $system['system_uploads'] . '/' . $post['page_picture'];
        if (!is_empty($post['page_cover']))
            $post['page_cover'] = $system['system_uploads'] . '/' . $post['page_cover'];
        if (!is_empty($post['group_picture']))
            $post['group_picture'] = $system['system_uploads'] . '/' . $post['group_picture'];
        if (!is_empty($post['group_cover']))
            $post['group_cover'] = $system['system_uploads'] . '/' . $post['group_cover'];

        /* post type */
        if($post['post_type'] == 'album' || $post['post_type'] == 'photos' || $post['post_type'] == 'profile_picture' || $post['post_type'] == 'profile_cover' || $post['post_type'] == 'page_picture' || $post['post_type'] == 'page_cover' || $post['post_type'] == 'group_picture' || $post['post_type'] == 'group_cover') {
            /* get photos */
            $get_photos = $db->query(sprintf("SELECT * FROM posts_photos WHERE post_id = %s ORDER BY photo_id DESC", secure($post['post_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
            $post['photos_num'] = $get_photos->num_rows;
            /* check if photos has been deleted */
            if($post['photos_num'] == 0) {
                return false;
            }
            while($post_photo = $get_photos->fetch_assoc()) {
                $post_photo['source'] = $system['system_uploads'] . '/' . $post_photo['source'];
                $post['photos'][] = $post_photo;
            }
            if($post['post_type'] == 'album') {
                $post['album'] = $this->get_album($post['photos'][0]['album_id'], false);
                if(!$post['album']) {
                    return false;
                }
            }

        } elseif ($post['post_type'] == 'media') {
            /* get media */
            $get_media = $db->query(sprintf("SELECT * FROM posts_media WHERE post_id = %s", secure($post['post_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
            /* check if media has been deleted */
            if($get_media->num_rows == 0) {
                return false;
            }
            $post['media'] = $get_media->fetch_assoc();

        } elseif ($post['post_type'] == 'link') {
            /* get link */
            $get_link = $db->query(sprintf("SELECT * FROM posts_links WHERE post_id = %s", secure($post['post_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
            /* check if link has been deleted */
            if($get_link->num_rows == 0) {
                return false;
            }
            $post['link'] = $get_link->fetch_assoc();

        } elseif ($post['post_type'] == 'video') {
            /* get video */
            $get_video = $db->query(sprintf("SELECT * FROM posts_videos WHERE post_id = %s", secure($post['post_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
            /* check if video has been deleted */
            if($get_video->num_rows == 0) {
                return false;
            }
            $post['video'] = $get_video->fetch_assoc();
            $post['video']['source'] = $system['system_uploads'] . '/' . $post['video']['source'];

        } elseif ($post['post_type'] == 'audio') {
            /* get audio */
            $get_audio = $db->query(sprintf("SELECT * FROM posts_audios WHERE post_id = %s", secure($post['post_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
            /* check if audio has been deleted */
            if($get_audio->num_rows == 0) {
                return false;
            }
            $post['audio'] = $get_audio->fetch_assoc();
            $post['audio']['source'] = $system['system_uploads'] . '/' . $post['audio']['source'];

        } elseif ($post['post_type'] == 'file') {
            /* get file */
            $get_file = $db->query(sprintf("SELECT * FROM posts_files WHERE post_id = %s", secure($post['post_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
            /* check if file has been deleted */
            if($get_file->num_rows == 0) {
                return false;
            }
            $post['file'] = $get_file->fetch_assoc();
            $post['file']['source'] = $system['system_uploads'] . '/' . $post['file']['source'];

        } elseif ($post['post_type'] == 'shared') {
            /* get origin post */
            $post['origin'] = $this->api_get_post(array('post_id' => $post['origin_id'], 'full_details' => false));
            /* check if origin post has been deleted */
            if(!$post['origin']) {
                return false;
            }
        }

        /* check if get full post details */
        if($full_details) {
            /* get post comments */
            if ($post['comments'] > 0) {
                $post['post_comments'] = $this->api_get_comments($post['post_id'], $offset, true, true, $post);
            }
        }
        if (isset($post['text']))
            $post['text'] = censored_words($post['text']);

        return $post;
    }


    /**
     * API get_comments
     *
     * @param integer $node_id
     * @param integer $offset
     * @param boolean $is_post
     * @param boolean $pass_privacy_check
     * @param array $post
     * @return array
     */
    public function api_get_comments($node_id, $offset = 0, $is_post = true, $pass_privacy_check = true, $post = array()) {
        global $db, $system;
        $comments = array();
        $offset *= $system['min_results'];
        /* get comments */
        if($is_post) {
            /* get post comments */
            if(!$pass_privacy_check) {
                /* (check|get) post */
                $post = $this->_check_post($node_id, false);
                if(!$post) {
                    return false;
                }
            }
            /* get post comments */
            $get_comments = $db->query(sprintf("SELECT * FROM (SELECT posts_comments.*, users.user_name, users.user_fullname, users.user_firstname, users.user_lastname, users.user_gender, users.user_picture, users.user_verified, pages.* FROM posts_comments LEFT JOIN users ON posts_comments.user_id = users.user_id AND posts_comments.user_type = 'user' LEFT JOIN pages ON posts_comments.user_id = pages.page_id AND posts_comments.user_type = 'page' WHERE NOT (users.user_name <=> NULL AND pages.page_name <=> NULL) AND posts_comments.node_type = 'post' AND posts_comments.node_id = %s ORDER BY posts_comments.comment_id DESC LIMIT %s, %s) comments ORDER BY comments.comment_id ASC", secure($node_id, 'int'), secure($offset, 'int', false), secure($system['min_results'], 'int', false) )) or _error(SQL_ERROR_THROWEN);
        } else {
            /* get photo comments */
            /* check privacy */
            if(!$pass_privacy_check) {
                /* (check|get) photo */
                $photo = $this->get_photo($node_id);
                if(!$photo) {
                    _error(403);
                }
                $post = $photo['post'];
            }
            /* get photo comments */
            $get_comments = $db->query(sprintf("SELECT * FROM (SELECT posts_comments.*, users.user_name, users.user_fullname, users.user_firstname, users.user_lastname, users.user_gender, users.user_picture, users.user_verified, pages.* FROM posts_comments LEFT JOIN users ON posts_comments.user_id = users.user_id AND posts_comments.user_type = 'user' LEFT JOIN pages ON posts_comments.user_id = pages.page_id AND posts_comments.user_type = 'page' WHERE NOT (users.user_name <=> NULL AND pages.page_name <=> NULL) AND posts_comments.node_type = 'photo' AND posts_comments.node_id = %s ORDER BY posts_comments.comment_id DESC LIMIT %s, %s) comments ORDER BY comments.comment_id ASC", secure($node_id, 'int'), secure($offset, 'int', false), secure($system['min_results'], 'int', false) )) or _error(SQL_ERROR_THROWEN);
        }
        if($get_comments->num_rows == 0) {
            return $comments;
        }
        while($comment = $get_comments->fetch_assoc()) {
            /* get the comment author */
            if($comment['user_type'] == "user") {
                /* user type */
                $comment['author_id'] = $comment['user_id'];
                $comment['author_picture'] = $this->get_picture($comment['user_picture'], $comment['user_gender']);
                $comment['author_url'] = $system['system_url'].'/'.$comment['user_name'];
                $comment['author_name'] = $comment['user_fullname'];
                $comment['author_verified'] = $comment['user_verified'];
            } else {
                /* page type */
                $comment['author_id'] = $comment['page_admin'];
                $comment['author_picture'] = $this->get_picture($comment['page_picture'], "page");
                $comment['author_url'] = $system['system_url'].'/pages/'.$comment['page_name'];
                $comment['author_name'] = $comment['page_title'];
                $comment['author_verified'] = $comment['page_verified'];
            }
            /* check if viewer user likes this comment */
            if($this->_logged_in && $comment['likes'] > 0) {
                $check_like = $db->query(sprintf("SELECT * FROM posts_comments_likes WHERE user_id = %s AND comment_id = %s", secure($this->_data['user_id'], 'int'), secure($comment['comment_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                $comment['i_like'] = ($check_like->num_rows > 0)? true: false;
            }
            /* return full link for mobile */
            $comment['user_picture'] = $system['system_uploads'] . '/' . $comment['user_picture'];
            /* check if viewer can manage comment [Edit|Delete] */
            $comment['edit_comment'] = false;
            $comment['delete_comment'] = false;
            if($this->_logged_in) {
                /* viewer is (admins|moderators)] */
                if($this->_data['user_group'] < 3) {
                    $comment['edit_comment'] = true;
                    $comment['delete_comment'] = true;
                }
                /* viewer is the author of comment */
                if($this->_data['user_id'] == $comment['author_id']) {
                    $comment['edit_comment'] = true;
                    $comment['delete_comment'] = true;
                }
                /* viewer is the author of post || page admin */
                if($this->_data['user_id'] == $post['author_id']) {
                    $comment['delete_comment'] = true;
                }
                /* viewer is the admin of the group of the post */
                if($post['in_group'] && $post['is_group_admin']) {
                    $comment['delete_comment'] = true;
                }
            }
            if (isset($comment['text']))
                $comment['text'] = censored_words($comment['text']);
            if (!is_empty($comment['image']))
                $comment['image'] = $system['system_uploads'] . '/' . $comment['image'];
            $comments[] = $comment;
        }
        return $comments;
    }


    /**
     * API get_photo
     *
     * @param integer $photo_id
     * @param boolean $full_details
     * @param boolean $full_details
     * @param string $context
     * @return array
     */
    public function api_get_photo($photo_id, $offset = 0, $full_details = false, $get_gallery = false, $context = 'photos') {
        global $db, $system;

        /* get photo */
        $get_photo = $db->query(sprintf("SELECT * FROM posts_photos WHERE photo_id = %s", secure($photo_id, 'int') )) or _error(SQL_ERROR_THROWEN);
        if($get_photo->num_rows == 0) {
            return false;
        }
        $photo = $get_photo->fetch_assoc();

        /* get post */
        $post = $this->_check_post($photo['post_id']);
        /* check if the post has been deleted */
        if(!$post) {
            return false;
        }

        /* check if photo can be deleted */
        if($post['in_group']) {
            /* check if (cover|profile) photo */
            $photo['can_delete'] = ( ($photo_id == $post['group_picture_id']) OR ($photo_id == $post['group_cover_id']) )? false : true;
        } elseif ($post['user_type'] == "user") {
            /* check if (cover|profile) photo */
            $photo['can_delete'] = ( ($photo_id == $post['user_picture_id']) OR ($photo_id == $post['user_cover_id']) )? false : true;
        } elseif ($post['user_type'] == "page") {
            /* check if (cover|profile) photo */
            $photo['can_delete'] = ( ($photo_id == $post['page_picture_id']) OR ($photo_id == $post['page_cover_id']) )? false : true;
        }

        $photo['source'] = $system['system_uploads'] . '/' . $photo['source'];

        /* check photo type [single|mutiple] */
        $check_single = $db->query(sprintf("SELECT * FROM posts_photos WHERE post_id = %s", secure($photo['post_id'], 'int') ))  or _error(SQL_ERROR_THROWEN);
        $photo['is_single'] = ($check_single->num_rows > 1)? false : true;

        /* get full details */
        if($full_details) {
            if($photo['is_single']) {
                /* get (likes|comments) of post */

                /* check if viewer likes this post */
                if($this->_logged_in && $post['likes'] > 0) {
                    $check_like = $db->query(sprintf("SELECT * FROM posts_likes WHERE user_id = %s AND post_id = %s", secure($this->_data['user_id'], 'int'), secure($post['post_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                    $photo['i_like'] = ($check_like->num_rows > 0)? true: false;
                }

                /* get post comments */
                if($post['comments'] > 0) {
                    $post['post_comments'] = $this->api_get_comments($post['post_id'], $offset, true, true, $post);
                }

            } else {
                /* single photo */
                /* get (likes|comments) of photo */

                /* check if viewer user likes this photo */
                if($this->_logged_in && $photo['likes'] > 0) {
                    $check_like = $db->query(sprintf("SELECT * FROM posts_photos_likes WHERE user_id = %s AND photo_id = %s", secure($this->_data['user_id'], 'int'), secure($photo['photo_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                    $photo['i_like'] = ($check_like->num_rows > 0)? true: false;
                }

                /* get post comments */
                if($photo['comments'] > 0) {
                    $photo['photo_comments'] = $this->api_get_comments($photo['photo_id'], $offset, false, true, $post);
                }

            }
        }

        /* get gallery */
        if($get_gallery) {
            switch ($context) {
                case 'post':
                    $get_post_photos = $db->query(sprintf("SELECT photo_id, source FROM posts_photos WHERE post_id = %s", secure($post['post_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                    while($post_photo = $get_post_photos->fetch_assoc()) {
                        $post_photos[$post_photo['photo_id']] = $post_photo;
                    }
                    $photo['next'] = $post_photos[get_array_key($post_photos, $photo['photo_id'], -1)];
                    $photo['prev'] =  $post_photos[get_array_key($post_photos, $photo['photo_id'], 1)];
                    break;

                case 'album':
                    $get_album_photos = $db->query(sprintf("SELECT photo_id, source FROM posts_photos WHERE album_id = %s", secure($photo['album_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                    while($album_photo = $get_album_photos->fetch_assoc()) {
                        $album_photos[$album_photo['photo_id']] = $album_photo;
                    }
                    $photo['next'] = $album_photos[get_array_key($album_photos, $photo['photo_id'], -1)];
                    $photo['prev'] =  $album_photos[get_array_key($album_photos, $photo['photo_id'], 1)];
                    break;

                case 'photos':
                    $get_target_photos = $db->query(sprintf("SELECT posts_photos.photo_id, posts_photos.source FROM posts INNER JOIN posts_photos ON posts.post_id = posts_photos.post_id WHERE posts.user_id = %s AND posts.user_type = %s", secure($post['user_id'], 'int'), secure($post['user_type']) )) or _error(SQL_ERROR_THROWEN);
                    while($target_photo = $get_target_photos->fetch_assoc()) {
                        $target_photos[$target_photo['photo_id']] = $target_photo;
                    }
                    $photo['next'] = $target_photos[get_array_key($target_photos, $photo['photo_id'], -1)];
                    $photo['prev'] =  $target_photos[get_array_key($target_photos, $photo['photo_id'], 1)];
                    break;
            }
        }

        /* return post array with photo */
        //$photo['post'] = $post;
        return $photo;
    }


    /* ------------------------------- */
    /* API Publisher */
    /* ------------------------------- */

    /**
     * publisher
     *
     * @param array $args
     * @return array
     */
    public function api_publisher(array $args = array())
    {
        global $db, $system, $date, $not_receive_page_ids, $not_receive_group_ids;
        $post = array();

        /* default */
        $post['user_id'] = $this->_data['user_id'];
        $post['user_type'] = "user";
        $post['in_wall'] = 0;
        $post['wall_id'] = null;
        $post['in_group'] = 0;
        $post['group_id'] = null;
        $post['school_id'] = 0; //Coniu
        $notify_from_page = 0; //Thông báo từ page, group?
        $notify_from_group = 0; //Thông báo từ page, group?

        $post['post_author_picture'] = $this->_data['user_picture'];
        $post['post_author_url'] = $system['system_url'].'/'.$this->_data['user_name'];
        $post['post_author_name'] = $this->_data['user_fullname'];
        $post['post_author_verified'] = $this->_data['user_verified'];

        /* check the user_type */
        if($args['handle'] == "user") {
            /* check if system allow wall posts */
            if(!$system['wall_posts_enabled']) {
                _api_error(400);
            }
            /* check if the user is valid & the viewer can post on his wall */
            $check_user = $db->query(sprintf("SELECT user_name, user_fullname, user_firstname, user_lastname, user_privacy_wall FROM users WHERE user_id = %s",secure($args['id'], 'int'))) or _error(SQL_ERROR_THROWEN);
            if($check_user->num_rows == 0) {
                _api_error(400);
            }
            $_user = $check_user->fetch_assoc();
            if($_user['user_privacy_wall'] == 'me' || ($_user['user_privacy_wall'] == 'friends' && !in_array($args['id'], $this->_data['friends_ids'])) ) {
                _api_error(400);
            }
            $post['in_wall'] = 1;
            $post['wall_id'] = $args['id'];
            $post['wall_username'] = $_user['user_name'];
            $post['wall_fullname'] = $_user['user_fullname'];

        } elseif ($args['handle'] == "page") {
            /* check if the page is valid & the viewer is the admin */
            // $check_page = $db->query(sprintf("SELECT * FROM pages WHERE page_id = %s AND page_admin = %s", secure($args['id'], 'int'), secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
            // Bỏ check page_admin vì check i_admin ở dưới rồi
            $check_page = $db->query(sprintf("SELECT * FROM pages WHERE page_id = %s", secure($args['id'], 'int') )) or _error(SQL_ERROR_THROWEN);
            $i_admin = $this->check_page_adminship($this->_data['user_id'], $args['id']);
            $i_teacher = false;
            $teachers = getSchoolData($args['id'], SCHOOL_TEACHERS);
            $teacherIds = array_keys($teachers);
            if(in_array($this->_data['user_id'], $teacherIds)) {
                $i_teacher = true;
            }
            if($check_page->num_rows == 0 && !$i_admin && !$i_teacher) {
                _api_error(400);
            }
            $_page = $check_page->fetch_assoc();

            //Có gửi thông báo đến user trong page hay không?
            if (!in_array($_page['page_id'], $not_receive_page_ids)) {
                $notify_from_page = 1;
            }

            //Coniu - Begin - Lưu thêm trường school_id nếu là post lên tường của trường
            if ($_page['page_category'] == SCHOOL_CATEGORY_ID) {
                $post['school_id'] = $_page['page_id'];
            }
            //Coniu - End - Lưu thêm trường school_id nếu là post lên tường của trường

            $post['user_id'] = $_page['page_id'];
            $post['user_type'] = "page";
            $post['post_author_picture'] = $this->get_picture($_page['page_picture'], "page");
            $post['post_author_url'] = $system['system_url'].'/pages/'.$_page['page_name'];
            $post['post_author_name'] = $_page['page_title'];
            $post['post_author_verified'] = $this->_data['page_verified'];

        } elseif ($args['handle'] == "group") {
            /* check if the group is valid & the viewer is a member */
            //Coniu - Begin - Lưu thêm trường school_id nếu là post lên tường của lớp
            //$check_group = $db->query(sprintf("SELECT * FROM groups WHERE group_id = %s", secure($args['id'], 'int') )) or _error(SQL_ERROR_THROWEN);
            $check_group = $db->query(sprintf("SELECT G.*, CL.school_id FROM groups G LEFT JOIN ci_class_level CL ON (G.class_level_id > 0) AND (G.class_level_id = CL.class_level_id)
                                                WHERE G.group_id = %s", secure($args['id'], 'int') )) or _error(SQL_ERROR_THROWEN);
            //Coniu - End - Lưu thêm trường school_id nếu là post lên tường của lớp
            $i_admin = $this->check_group_adminship($this->_data['user_id'], $args['id']);
            if($check_group->num_rows == 0 && !$i_admin) {
                _api_error(400);
            }
            $_group = $check_group->fetch_assoc();

            //Coniu - Begin - Lưu thêm trường school_id nếu là post lên tường của trường
            if ($_group['class_level_id'] > 0) {
                $post['school_id'] = $_group['school_id'];

                /* CI - kiểm tra user có được viết lên tường của lớp hay không? */
                //Lấy ra thông tin trường
                $school = getSchoolData($_group['school_id'], SCHOOL_DATA);

                if (is_null($school)) {
                    _api_error(__("Error"), __("Class does not belong to an existing school"));
                }

                //Nếu chưa phải thành viên nhóm (ko phải phụ huynh hay giao viên) và không phải admin trường thì báo lỗi
                $_group['i_joined'] = false;
                $check_membership = $db->query(sprintf("SELECT * FROM groups_members WHERE group_id = %s AND user_id = %s", secure($_group['group_id'], 'int'), secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR);
                if($check_membership->num_rows > 0) {
                    $_group['i_joined'] = true;
                }

                if ((!$_group['i_joined']) && ($this->_data['user_id'] != $school['page_admin'])) {
                    _api_error(403);
                }

                $teachers = getClassData($_group['group_id'], CLASS_TEACHERS);

                //Kiểm tra vai trò của người dùng hiện tại với hệ thống
                $role_id = PERMISSION_NONE;
                if ($this->_data['user_id'] == $school['page_admin']) {
                    $role_id = PERMISSION_ALL;
                } else {
                    foreach($teachers as $teacher) {
                        if ($this->_data['user_id'] == $teacher['user_id']) {
                            $role_id = PERMISSION_MANAGE;
                            break;
                        }
                    }
                }

                if (!$school['class_allow_post']
                    && ($_group['group_admin'] != $this->_data['user_id'])
                    && ($role_id == PERMISSION_NONE)) {
                    _api_error(403);
                }
                /* CI - END */
            }

            //Có gửi thông báo đến user trong page hay không?
            if (!in_array($args['id'], $not_receive_group_ids)) {
                $notify_from_group = 1;
            }

            $post['in_group'] = 1;
            $post['group_id'] = $args['id'];
            $post['group_title'] = $_group['group_title'];
            $post['group_name'] = $_group['group_name'];
            $post['group_picture'] = $this->get_picture($_group['group_picture'], "group");
        }

        /* prepare post data */
        $post['text'] = $args['message'];
        $post['time'] = $date;
        $post['location'] = (!is_empty($args['location']) && valid_location($args['location']))? $args['location']: '';
        $post['privacy'] = $args['privacy'];
        $post['likes'] = 0;
        $post['comments'] = 0;
        $post['shares'] = 0;

        /* prepare post type */
        if(count($args['photos']) > 0) {
            if(!is_empty($args['album'])) {
                $post['post_type'] = 'album';
            } else {
                $post['post_type'] = 'photos';
            }

        } elseif ($args['video']) {
            $post['post_type'] = 'video';
        } elseif ($args['audio']) {
            $post['post_type'] = 'audio';
        } elseif ($args['file']) {
            $post['post_type'] = 'file';
        } elseif ($args['link']) {
            if($args['link']['source_type'] == "link") {
                $post['post_type'] = 'link';
            } else {
                $post['post_type'] = 'media';
            }
        } else {
            if($post['location'] != '') {
                $post['post_type'] = 'map';
            } else {
                $post['post_type'] = '';
            }
        }
        /* insert the post */
        //Coniu - Lưu thêm trường school_id
        //$db->query(sprintf("INSERT INTO posts (user_id, user_type, in_wall, wall_id, in_group, group_id, post_type, time, location, privacy, text) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", secure($post['user_id'], 'int'), secure($post['user_type']), secure($post['in_wall'], 'int'), secure($post['wall_id'], 'int'), secure($post['in_group']), secure($post['group_id'], 'int'), secure($post['post_type']), secure($post['time']), secure($post['location']), secure($post['privacy']), secure($post['text']) )) or _error(SQL_ERROR_THROWEN);
        $db->query(sprintf("INSERT INTO posts (user_id, user_type, in_wall, wall_id, in_group, group_id, post_type, time, location, privacy, text, school_id)
                            VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", secure($post['user_id'], 'int'), secure($post['user_type']),
            secure($post['in_wall'], 'int'), secure($post['wall_id'], 'int'), secure($post['in_group']), secure($post['group_id'], 'int'),
            secure($post['post_type']), secure($post['time']), secure($post['location']), secure($post['privacy']), secure($post['text']), secure($post['school_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
        $post['post_id'] = $db->insert_id;

        switch ($post['post_type']) {
            case 'album':
                /* create new album */
                $db->query(sprintf("INSERT INTO posts_photos_albums (user_id, user_type, in_group, group_id, title, privacy) VALUES (%s, %s, %s, %s, %s, %s)", secure($post['user_id'], 'int'), secure($post['user_type']), secure($post['in_group']), secure($post['group_id'], 'int'), secure($args['album']), secure($post['privacy']) )) or _error(SQL_ERROR_THROWEN);
                $album_id = $db->insert_id;
                foreach ($args['photos'] as $photo) {
                    $db->query(sprintf("INSERT INTO posts_photos (post_id, album_id, source) VALUES (%s, %s, %s)", secure($post['post_id'], 'int'), secure($album_id, 'int'), secure($photo) )) or _error(SQL_ERROR_THROWEN);
                }
                break;
            case 'photos':
                if($args['handle'] == "page") {
                    /* check for page timeline album */
                    if(!$_page['page_album_timeline']) {
                        /* create new page timeline album */
                        $db->query(sprintf("INSERT INTO posts_photos_albums (user_id, user_type, title, privacy) VALUES (%s, 'page', 'Timeline Photos', 'public')", secure($_page['page_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                        $_page['page_album_timeline'] = $db->insert_id;
                        /* update page */
                        $db->query(sprintf("UPDATE pages SET page_album_timeline = %s WHERE page_id = %s", secure($_page['page_album_timeline'], 'int'), secure($_page['page_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                    }
                    $album_id = $_page['page_album_timeline'];
                } elseif ($args['handle'] == "group") {
                    /* check for group timeline album */
                    if(!$_group['group_album_timeline']) {
                        /* create new group timeline album */
                        $db->query(sprintf("INSERT INTO posts_photos_albums (user_id, user_type, title, privacy) VALUES (%s, 'group', 'Timeline Photos', 'public')", secure($_group['group_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                        $_group['group_album_timeline'] = $db->insert_id;
                        /* update page */
                        $db->query(sprintf("UPDATE groups SET group_album_timeline = %s WHERE group_id = %s", secure($_group['group_album_timeline'], 'int'), secure($_group['group_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                    }
                    $album_id = $_group['group_album_timeline'];
                } else {
                    /* check for timeline album */
                    if(!$this->_data['user_album_timeline']) {
                        /* create new timeline album */
                        $db->query(sprintf("INSERT INTO posts_photos_albums (user_id, user_type, title, privacy) VALUES (%s, 'user', 'Timeline Photos', 'public')", secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                        $this->_data['user_album_timeline'] = $db->insert_id;
                        /* update user */
                        $db->query(sprintf("UPDATE users SET user_album_timeline = %s WHERE user_id = %s", secure($this->_data['user_album_timeline'], 'int'), secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                    }
                    $album_id = $this->_data['user_album_timeline'];
                }
                foreach ($args['photos'] as $photo) {
                    $db->query(sprintf("INSERT INTO posts_photos (post_id, album_id, source) VALUES (%s, %s, %s)", secure($post['post_id'], 'int'), secure($album_id, 'int'), secure($photo) )) or _error(SQL_ERROR_THROWEN);
                }
                break;

            case 'video':
                $db->query(sprintf("INSERT INTO posts_videos (post_id, source) VALUES (%s, %s)", secure($post['post_id'], 'int'), secure($args['video']) )) or _error(SQL_ERROR_THROWEN);
                break;

            case 'audio':
                $db->query(sprintf("INSERT INTO posts_audios (post_id, source) VALUES (%s, %s)", secure($post['post_id'], 'int'), secure($args['audio']) )) or _error(SQL_ERROR_THROWEN);
                break;

            case 'file':
                $db->query(sprintf("INSERT INTO posts_files (post_id, source) VALUES (%s, %s)", secure($post['post_id'], 'int'), secure($args['file']) )) or _error(SQL_ERROR_THROWEN);
                break;

            case 'media':
                $db->query(sprintf("INSERT INTO posts_media (post_id, source_url, source_provider, source_type, source_title, source_text, source_html, source_thumbnail) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)", secure($post['post_id'], 'int'), secure($args['link']['source_url']), secure($args['link']['source_provider']), secure($args['link']['source_type']), secure($args['link']['source_title']), secure($args['link']['source_text']), secure($args['link']['source_html']), secure($args['link']['source_thumbnail']) )) or _error(SQL_ERROR_THROWEN);
                break;

            case 'link':
                $db->query(sprintf("INSERT INTO posts_links (post_id, source_url, source_host, source_title, source_text, source_thumbnail) VALUES (%s, %s, %s, %s, %s, %s)", secure($post['post_id'], 'int'), secure($args['link']['source_url']), secure($args['link']['source_host']), secure($args['link']['source_title']), secure($args['link']['source_text']), secure($args['link']['source_thumbnail']) )) or _error(SQL_ERROR_THROWEN);
                break;
        }

        /* post mention notifications */
        $this->post_mentions($args['message'], $post['post_id']);

        /* post wall notifications */
        if($post['in_wall']) {
            //$this->post_notification($post['wall_id'], 'wall', 'post', $post['post_id']);
            $this->post_notification_for_social( array('to_user_id'=>$post['wall_id'], 'action'=>'wall', 'node_type'=>'post', 'node_url'=>$post['post_id']) );
        }

        /* post in page notifications */
        if($notify_from_page) {
            $strSql = sprintf("SELECT user_id FROM pages_likes WHERE page_id = %s", secure($post['user_id'], 'int'));
            $get_users = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
            if($get_users->num_rows > 0) {
                $userIds = array();
                while($get_user = $get_users->fetch_assoc()) {
                    $userIds[] = $get_user['user_id'];
                }
                $this->post_notifications($userIds, NOTIFICATION_POST_IN_PAGE, $post['user_type'], $post['post_id'], $post['post_author_name'], $post['post_author_picture'] );
            }
        }
        /* post in group notifications */
        if($notify_from_group) {
            $strSql = sprintf("SELECT user_id FROM groups_members WHERE group_id = %s", secure($post['group_id'], 'int'));
            $get_users = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
            if($get_users->num_rows > 0) {
                $userIds = array();
                while($get_user = $get_users->fetch_assoc()) {
                    $userIds[] = $get_user['user_id'];
                }
                $this->post_notifications($userIds, NOTIFICATION_POST_IN_GROUP, $post['user_type'], $post['post_id'], $this->_data['user_fullname'], $post['group_title']);
            }
        }

        $result = $this->api_get_post(array(
                'post_id' => $post['post_id'],
                'offset' => 0,
                'full_details' => true
            )
        );

        // return
        return $result;
    }


    /**
     * API comment
     *
     * @param string $handle
     * @param integer $node_id
     * @param string $message
     * @param string $photo
     * @return array
     */
    public function api_comment($handle, $node_id, $message, $photo = '') {
        global $db, $system, $date;
        $comment = array();

        /* default */
        $comment['node_id'] = $node_id;
        $comment['node_type'] = $handle;
        $comment['text'] = $message;
        $comment['image'] = $photo;
        $comment['time'] = $date;
        $comment['likes'] = 0;
        $comment['replies'] = 0;

        $comment['user_id'] = $this->_data['user_id'];
        $comment['user_type'] = "user";
        $comment['author_picture'] = $this->_data['user_picture'];
        $comment['author_url'] = $system['system_url'].'/'.$this->_data['user_name'];
        $comment['author_name'] = $this->_data['user_fullname'];
        $comment['author_verified'] = $this->_data['user_verified'];

        /* check the handle */
        switch ($handle) {
            case 'post':
                /* (check|get) post */
                $post = $this->_check_post($node_id, false, false);
                if(!$post) {
                    _api_error(403);
                }
                break;

            case 'photo':
                /* (check|get) photo */
                $photo = $this->get_photo($node_id);
                if(!$photo) {
                    _api_error(403);
                }
                $post = $photo['post'];
                break;

            case 'comment':
                /* (check|get) comment */
                $parent_comment = $this->get_comment($node_id, false);
                if(!$parent_comment) {
                    _api_error(403);
                }
                $post = $parent_comment['post'];
                break;
        }
        /* check if there is any blocking between the viewer & the target */
        if($this->blocked($post['author_id']) || ($handle == "comment" && $this->blocked($parent_comment['author_id'])) ) {
            _api_error(403);
        }
        /*if(!$post['allow_comment']) {
            _api_error(403);
        }*/

        /* check if the viewer is page admin of the target post */
        if(!$post['is_page_admin']) {
            $comment['user_id'] = $this->_data['user_id'];
            $comment['user_type'] = "user";
            $comment['author_picture'] = $this->_data['user_picture'];
            $comment['author_url'] = $system['system_url'].'/'.$this->_data['user_name'];
            $comment['author_user_name'] = $this->_data['user_name'];
            //$comment['author_name'] = $this->_data['user_firstname']." ".$this->_data['user_lastname'];
            $comment['author_name'] = $this->_data['user_fullname'];
            $comment['author_verified'] = $this->_data['user_verified'];
        } else {
            $comment['user_id'] = $post['page_id'];
            $comment['user_type'] = "page";
            $comment['author_picture'] = $this->get_picture($post['page_picture'], "page");
            $comment['author_url'] = $system['system_url'].'/pages/'.$post['page_name'];
            $comment['author_name'] = $post['page_title'];
            $comment['author_verified'] = $post['page_verified'];
        }

        /* insert the comment */
        $db->query(sprintf("INSERT INTO posts_comments (node_id, node_type, user_id, user_type, text, image, time) VALUES (%s, %s, %s, %s, %s, %s, %s)", secure($comment['node_id'], 'int'), secure($comment['node_type']), secure($comment['user_id'], 'int'), secure($comment['user_type']), secure($comment['text']), secure($comment['image']), secure($comment['time']) )) or _error(SQL_ERROR_THROWEN);
        $comment['comment_id'] = $db->insert_id;
        /* update (post|photo|comment) (comments|replies) counter */
        switch ($handle) {
            case 'post':
                $db->query(sprintf("UPDATE posts SET comments = comments + 1 WHERE post_id = %s", secure($node_id, 'int') )) or _error(SQL_ERROR_THROWEN);
                break;

            case 'photo':
                $db->query(sprintf("UPDATE posts_photos SET comments = comments + 1 WHERE photo_id = %s", secure($node_id, 'int') )) or _error(SQL_ERROR_THROWEN);
                break;

            case 'comment':
                $db->query(sprintf("UPDATE posts_comments SET replies = replies + 1 WHERE comment_id = %s", secure($node_id, 'int') )) or _error(SQL_ERROR_THROWEN);
                $db->query(sprintf("UPDATE posts SET comments = comments + 1 WHERE post_id = %s", secure($post['post_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                break;
        }

        /* post notification */
        if($handle == "comment") {
            $this->post_notification_for_social( array('to_user_id'=>$parent_comment['author_id'], 'action'=>'reply', 'node_type'=>$parent_comment['node_type'], 'node_url'=>$parent_comment['node_id'], 'notify_id'=>'comment_'.$comment['comment_id']) );
            if($post['author_id'] != $parent_comment['author_id']) {
                $this->post_notification_for_social( array('to_user_id'=>$post['author_id'], 'action'=>'comment', 'node_type'=>$parent_comment['node_type'], 'node_url'=>$parent_comment['node_id'], 'notify_id'=>'comment_'.$comment['comment_id']) );
            }
        } else {
            $this->post_notification_for_social( array('to_user_id'=>$post['author_id'], 'action'=>'comment', 'node_type'=>$handle, 'node_url'=>$node_id, 'notify_id'=>'comment_'.$comment['comment_id']) );
        }

        /* post mention notifications if any */
        if($handle == "comment") {
            $this->post_mentions($comment['text'], $parent_comment['node_id'], "reply_".$parent_comment['node_type'], 'comment_'.$comment['comment_id'], array($post['author_id'], $parent_comment['author_id']));
        } else {
            $this->post_mentions($comment['text'], $node_id, "comment_".$handle, 'comment_'.$comment['comment_id'], array($post['author_id']));
        }

        /* parse text */
        $comment['text_plain'] = htmlentities($comment['text'], ENT_QUOTES, 'utf-8');
        $comment['text'] = $this->_parse($comment['text_plain']);

        /* check if viewer can manage comment [Edit|Delete] */
        $comment['edit_comment'] = true;
        $comment['delete_comment'] = true;

        /* return */
        return $comment;
    }


    // Check user has registed by Facebook, Google
    /**
     * check_social_connect
     *
     * @param int $id
     * @param string $type
     * @return boolean
     */
    public function check_social_connect($id , $type = null)
    {
        global $db;
        /* check type (facebook|google) */
        switch ($type) {
            case 'facebook':
                $query = $db->query(sprintf("SELECT user_id FROM users WHERE facebook_id = %s", secure($id))) or _error(SQL_ERROR_THROWEN);
                break;
            case 'google':
                $query = $db->query(sprintf("SELECT user_id FROM users WHERE google_id = %s", secure($id))) or _error(SQL_ERROR_THROWEN);
                break;
            default:
                return false;
                break;
        }
        if ($query->num_rows > 0) {
            return false;
        }
        return true;
    }


    public function pushNotice(array $args = array())
    {
        /* initialize arguments */
        $device_token =  $args['device_token'];
        $message = isset($args['message']) ? $args['message'] : '';
        $badge = isset($args['badge']) ? $args['badge'] : 0;

        $data = array(
            'type' => 'chat',
            'friend_id' => $args['friend_id'],
            'conversation_id' => $args['conversation_id'],
            'badge' => $badge
        );

        push_notification($device_token, $data, $message, $badge);
    }


    /**
     * get_notification_count_from_userid
     *
     * @return array
     */
    public function get_friends_info($user_id) {
        global $db;
        $result = null;
        /* get notifications_count */
        $friends_info = $db->query(sprintf("SELECT S.session_token, S.device_token, U.user_live_notifications_counter FROM users U
            LEFT JOIN users_sessions S ON U.user_id = S.user_id
            WHERE U.user_id = %s", secure($user_id, 'int') )) or _error(SQL_ERROR_THROWEN);

        if($friends_info->num_rows > 0) {
            while($user = $friends_info->fetch_assoc()) {
                $result[] = $user;
            }
        }

        return $result;
    }

    /**
     * get_profile
     *
     * @return array
     */
    public function get_profile($args) {
        global $db, $system;

        // [1] get main profile info
        if (!is_empty($args['user_name']) && valid_username($args['user_name'], "user")) {
            $get_profile = $db->query(sprintf("SELECT %s FROM users WHERE user_name = %s", PARAM_PROFILE, secure($args['user_name']))) or _error(SQL_ERROR_THROWEN);
        } else if (!is_empty($args['user_id']) && $args['user_id'] > 0) {
            $get_profile = $db->query(sprintf("SELECT %s FROM users WHERE user_id = %s", PARAM_PROFILE, secure($args['user_id']))) or _error(SQL_ERROR_THROWEN);
        } else
            _api_error(400);

        if ($get_profile->num_rows == 0) {
            _api_error(404);
        }
        $profile = $get_profile->fetch_assoc();
        /* check if banned by the system */
        if ($this->banned($profile['user_id'])) {
            _api_error(404);
        }
        /* check if blocked by the viewer */
        if ($this->blocked($profile['user_id'])) {
            _api_error(404);
        }
        /* check username case */
        /*if (strtolower($_POST['username']) == strtolower($profile['user_name']) && $_POST['username'] != $profile['user_name']) {
            _redirect('/' . $profile['user_name']);
        }*/
        $profile['date_of_issue'] = toSysDate($profile['date_of_issue']);
        $profile['user_email_activation'] = !is_empty($profile['user_email_activation']) ? $profile['user_email_activation'] : "";
        $profile['allow_user_post_on_wall'] = ($system['allow_user_post_on_wall']) ? true : false;
        /* get profile picture */
        $profile['user_picture_default'] = ($profile['user_picture']) ? false : true;
        $profile['user_picture'] = get_picture($profile['user_picture'], $profile['user_gender']);
        if (!is_empty($profile['user_cover']))
            $profile['user_cover'] = $system['system_uploads'] . '/' . $profile['user_cover'];

        $profile['is_admin'] = ($profile['user_id'] == $this->_data['user_id']) ? true : false;
        /* get the connection &  mutual friends */
        if ($this->_logged_in && $profile['user_id'] != $this->_data['user_id']) {
            /* get the connection */
            if (isset($args['reload']) && $args['reload']) {
                /* get all friends ids */
                $friends_ids = $this->get_friends_ids($this->_data['user_id']);
                /* get all followings ids */
                $followings_ids = $this->get_followings_ids($this->_data['user_id']);
                /* get all friend requests ids */
                $friend_requests_ids = $this->get_friend_requests_ids();
                /* get all friend requests sent ids */
                $friend_requests_sent_ids = $this->get_friend_requests_sent_ids();
            } else {
                $friends_ids = $this->_data['friends_ids'];
                $followings_ids = $this->_data['followings_ids'];
                $friend_requests_ids = $this->_data['friend_requests_ids'];
                $friend_requests_sent_ids = $this->_data['friend_requests_sent_ids'];
            }

            $profile['we_friends'] = (in_array($profile['user_id'], $friends_ids)) ? true : false;
            $profile['he_request'] = (in_array($profile['user_id'], $friend_requests_ids)) ? true : false;
            $profile['i_request'] = (in_array($profile['user_id'], $friend_requests_sent_ids)) ? true : false;
            $profile['i_follow'] = (in_array($profile['user_id'], $followings_ids)) ? true : false;
            /* get mutual friends */
            $profile['mutual_friends_count'] = $this->get_mutual_friends_count($profile['user_id']);
            $profile['mutual_friends'] = $this->get_mutual_friends($profile['user_id']);
        }

        return $profile;
    }

    /* ------------------------------- */
    /* API Notifications */
    /* ------------------------------- */

    /**
     * API get_notifications
     *
     * @param string $notification_id
     * @return array
     */
    public function get_notification_api($notification_id) {
        global $db, $system;

        $notification = array();
        $get_notifications = $db->query(sprintf("SELECT notifications.*, users.user_id, users.user_name, users.user_fullname, users.user_gender, users.user_picture FROM notifications INNER JOIN users ON notifications.from_user_id = users.user_id WHERE notifications.notification_id = %s", secure($notification_id, 'int') )) or _error(SQL_ERROR_THROWEN);

        if($get_notifications->num_rows > 0) {
            $notification = $get_notifications->fetch_assoc();
            $notification['type'] = 'notification';
            $notification['user_picture'] = $this->get_picture($notification['user_picture'], $notification['user_gender']);
            /* parse notification */
            switch ($notification['action']) {
                case 'friend':
                    $notification['icon'] = "fa-user-plus";
                    $notification['message'] = __("accepted your friend request");
                    break;

                case 'follow':
                    $notification['icon'] = "fa-rss";
                    $notification['message'] = __("now following you");
                    break;

                case 'like':
                    $notification['icon'] = "fa-thumbs-o-up";
                    if($notification['node_type'] == "post") {
                        $notification['message'] = __("likes your post");

                    } elseif ($notification['node_type'] == "post_comment") {
                        $notification['message'] = __("likes your comment");

                    } elseif ($notification['node_type'] == "photo") {
                        $notification['message'] = __("likes your photo");

                    } elseif ($notification['node_type'] == "photo_comment") {
                        $notification['message'] = __("likes your comment");
                    }
                    break;

                case 'comment':
                    $notification['icon'] = "fa-comment";
                    if($notification['node_type'] == "post") {
                        $notification['message'] = __("commented on your post");

                    } elseif ($notification['node_type'] == "photo") {
                        $notification['message'] = __("commented on your photo");
                    }
                    break;

                case 'share':
                    $notification['icon'] = "fa-share";
                    $notification['message'] = __("shared your post");
                    break;

                case 'mention':
                    $notification['icon'] = "fa-comment";
                    if($notification['node_type'] == "post") {
                        $notification['message'] = __("Mentioned you in a post");

                    } elseif ($notification['node_type'] == "comment") {
                        $notification['message'] = __("mentioned you in a comment");

                    } elseif ($notification['node_type'] == "photo") {
                        $notification['message'] = __("mentioned you in a comment");
                    }
                    break;

                case 'profile_visit':
                    $notification['icon'] = "fa-eye";
                    $notification['message'] = __("visited your profile");
                    break;

                case 'wall':
                    $notification['icon'] = "fa-comment";
                    $notification['message'] = __("posted on your wall");
                    break;

                //ConIu - BEGIN - Xử lý các trường hợp notification của nghiệp vụ quản lý
                case NOTIFICATION_ASSIGN_CLASS:
                case NOTIFICATION_NEW_CLASS:
                case NOTIFICATION_UPDATE_CLASS:
                    $notification['ci'] = "1";
                    $notification['icon'] = "fa-graduation-cap";
                    $notification['message'] = getNotificationMessage($notification);
                    break;
                case NOTIFICATION_NEW_CLASSLEVEL:
                case NOTIFICATION_UPDATE_CLASSLEVEL:
                    $notification['ci'] = "1";
                    $notification['icon'] = "fa-tree";
                    $notification['message'] = getNotificationMessage($notification);
                    break;
                case NOTIFICATION_NEW_CHILD:
                    $notification['ci'] = "1";
                    $notification['icon'] = "fa-child";
                    $notification['message'] = getNotificationMessage($notification);
                    break;
                case NOTIFICATION_UPDATE_CHILD_TEACHER:
                    $notification['ci'] = "1";
                    $notification['icon'] = "fa-child";
                    $notification['message'] = getNotificationMessage($notification);
                    break;
                case NOTIFICATION_CONFIRM_CHILD_TEACHER:
                case NOTIFICATION_UNCONFIRM_CHILD_TEACHER:
                    $notification['ci'] = "1";
                    $notification['icon'] = "fa-child";
                    $notification['message'] = getNotificationMessage($notification);
                    break;
                case NOTIFICATION_NEW_TEACHER:
                    $notification['ci'] = "1";
                    $notification['icon'] = "fa-graduation-cap";
                    $notification['message'] = getNotificationMessage($notification);
                    break;

                case NOTIFICATION_SCHOOL:
                case NOTIFICATION_CLASS_LEVEL:
                case NOTIFICATION_CLASS:
                    $notification['ci'] = "1";
                    $notification['icon'] = "fa-globe";
                    $notification['message'] = getNotificationMessage($notification);
                    break;
                case NOTIFICATION_NEW_EVENT_SCHOOL:
                case NOTIFICATION_NEW_EVENT_CLASS_LEVEL:
                case NOTIFICATION_NEW_EVENT_CLASS:
                case NOTIFICATION_CANCEL_EVENT_SCHOOL:
                case NOTIFICATION_CANCEL_EVENT_CLASS_LEVEL:
                case NOTIFICATION_CANCEL_EVENT_CLASS:
                    $notification['ci'] = "1";
                    $notification['icon'] = "fa-bell";
                    $notification['message'] = getNotificationMessage($notification);
                    break;
                case NOTIFICATION_NEW_MEDICINE:
                case NOTIFICATION_UPDATE_MEDICINE:
                case NOTIFICATION_USE_MEDICINE:
                case NOTIFICATION_CONFIRM_MEDICINE:
                case NOTIFICATION_CANCEL_MEDICINE:
                    $notification['ci'] = "1";
                    $notification['icon'] = "fa-medkit";
                    $notification['message'] = getNotificationMessage($notification);
                    break;
                case NOTIFICATION_NEW_TUITION:
                case NOTIFICATION_UPDATE_TUITION:
                case NOTIFICATION_CONFIRM_TUITION:
                case NOTIFICATION_UNCONFIRM_TUITION:
                    $notification['ci'] = "1";
                    $notification['icon'] = "fas fa-money-bill-alt";
                    $notification['message'] = getNotificationMessage($notification);
                    break;
                case NOTIFICATION_ATTENDANCE_RESIGN:
                case NOTIFICATION_ATTENDANCE_CONFIRM:
                    $notification['ci'] = "1";
                    $notification['icon'] = "fa-tasks";
                    $notification['message'] = getNotificationMessage($notification);
                    break;
                case NOTIFICATION_NEW_REPORT:
                case NOTIFICATION_NEW_REPORT_TEMPLATE:
                case NOTIFICATION_UPDATE_REPORT:
                case NOTIFICATION_NEW_POINT:
                    $notification['ci'] = "1";
                    $notification['icon'] = "fa-book";
                    $notification['message'] = getNotificationMessage($notification);
                    break;
                //ConIu - END - Xử lý các trường hợp notification của nghiệp vụ quản lý
            }
        }
        return $notification;
    }


    /* ------------------------------- */
    /* Search */
    /* ------------------------------- */

    /**
     * API search_quick
     *
     * @param string $query
     * @return array
     */
    public function api_search_detail($args = array()) {
        global $db, $system;
        $results = array();
        $offset = !isset($args['offset']) ? 0 : $args['offset'];
        $offset *= $system['max_results'];
        $type = !isset($args['type']) ? 'posts' : $args['type'];
        $query = $args['query'];

        /* search posts */
        if ($type == 'posts') {
            $posts = $this->get_posts( array('query' => $query, 'offset' => $offset) );
            if(count($posts) > 0) {
                $results['posts'] = $posts;
            }
        } elseif ($type == 'users') {
            /* search users */
            //$get_users = $db->query(sprintf('SELECT user_id, user_name, user_firstname, user_lastname, user_gender, user_picture FROM users WHERE user_name LIKE %1$s OR user_firstname LIKE %1$s OR user_lastname LIKE %1$s ORDER BY user_firstname ASC LIMIT %2$s, %3$s', secure($query, 'search'), secure($offset, 'int', false), secure($system['max_results'], 'int', false) )) or _error(SQL_ERROR_THROWEN);
            $get_users = $db->query(sprintf('SELECT user_id, user_name, user_fullname, user_gender, user_picture FROM users WHERE user_name LIKE %1$s OR user_fullname LIKE %1$s ORDER BY user_fullname ASC LIMIT %2$s, %3$s', secure($query, 'search'), secure($offset, 'int', false), secure($system['max_results'], 'int', false) )) or _error(SQL_ERROR_THROWEN);
            if($get_users->num_rows > 0) {
                while($user = $get_users->fetch_assoc()) {
                    $user['user_picture'] = $this->get_picture($user['user_picture'], $user['user_gender']);
                    /* get the connection between the viewer & the target */
                    $user['connection'] = $this->connection($user['user_id']);
                    $results['users'][] = $user;
                }
            }
        } elseif ($type == 'pages') {
            /* search pages */
            $get_pages = $db->query(sprintf('SELECT * FROM pages WHERE page_name LIKE %1$s OR page_title LIKE %1$s ORDER BY page_title ASC', secure($query, 'search'))) or _error(SQL_ERROR_THROWEN);
            if($get_pages->num_rows > 0) {
                while($page = $get_pages->fetch_assoc()) {
                    $page['page_picture'] = $this->get_picture($page['page_picture'], 'page');
                    /* check if the viewer liked the page */
                    $page['i_like'] = false;
                    if($this->_logged_in) {
                        $get_likes = $db->query(sprintf("SELECT * FROM pages_likes WHERE page_id = %s AND user_id = %s", secure($page['page_id'], 'int'), secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                        if($get_likes->num_rows > 0) {
                            $page['i_like'] = true;
                        }
                    }
                    // Kiểm tra xem page có phải page trường không
                    $strSql = sprintf("SELECT school_id FROM ci_school_configuration WHERE school_id = %s", secure($page['page_id'], 'int'));
                    $get_school_id = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
                    if($get_school_id->num_rows > 0) {
                        $is_member = false;
                        // Kiểm tra xem user hiện tại đã like page chưa
                        if($page['i_like']) {
                            $is_member = true;
                        } else {
                            // Kiểm tra xem user hiện tại có phải là thành viên của trường không
                            // 1. Kiểm tra xem có phải giáo viên trong trường không
                            $strSql = sprintf("SELECT user_id FROM ci_teacher WHERE school_id = %s AND user_id = %s", secure($page['page_id'], 'int'), secure($this->_data['user_id'], 'int'));
                            $get_teacher_id = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
                            if($get_teacher_id->num_rows > 0) {
                                $is_member = true;
                            } else {
                                // 2. Kiểm tra xem user hiện tại có phải là phụ huynh trong trường không
                                $strSql = sprintf('SELECT UM.user_id FROM ci_user_manage UM INNER JOIN ci_school_child SC ON UM.object_type = %s AND UM.object_id = SC.child_id
                    AND SC.school_id = %s AND SC.status = %s AND UM.user_id = %s', MANAGE_CHILD, secure($page['page_id'], 'int'), secure(STATUS_ACTIVE, 'int'), secure($this->_data['user_id'], 'int'));
                                $get_parent_id = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
                                if($get_parent_id->num_rows > 0) {
                                    $is_member = true;
                                }
                            }
                        }
                        if($is_member) {
                            $results['pages'][] = $page;
                        }
                    } else {
                        $results['pages'][] = $page;
                    }
                }
            }
        } elseif ($type == 'groups') {
            /* search groups */
            $get_groups = $db->query(sprintf('SELECT * FROM groups WHERE group_privacy != "secret" AND (group_name LIKE %1$s OR group_title LIKE %1$s) ORDER BY group_title ASC LIMIT %2$s, %3$s', secure($query, 'search'), secure($offset, 'int', false), secure($system['max_results'], 'int', false) )) or _error(SQL_ERROR_THROWEN);
            if($get_groups->num_rows > 0) {
                while($group = $get_groups->fetch_assoc()) {
                    $group['group_picture'] = $this->get_picture($group['group_picture'], 'group');
                    /* check if the viewer joined the group */
                    $group['i_joined'] = $this->check_group_membership($this->_data['user_id'], $group['group_id']);

                    // Kiểm tra có phải nhóm lớp không
                    if($group['class_level_id'] > 0) {
                        $is_member = false;
                        // Nếu là group của lớp thì kiểm tra xem user hiện tại có phải là thành viên của group không.
                        if($group['i_joined']) {
                            // Là thành viên của nhóm
                            $is_member = true;
                        } else {
                            // 1. Kiểm tra xem có phải giáo viên trong trường không
                            // Lấy id của trường
                            $school_id = 0;
                            $strSql = sprintf("SELECT school_id FROM ci_class_level WHERE class_level_id = %s", secure($group['class_level_id'], 'int'));
                            $get_school_id = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
                            if($get_school_id->num_rows > 0) {
                                $school_id = $get_school_id->fetch_assoc()['school_id'];
                            }
                            if($school_id > 0) {
                                $strSql = sprintf("SELECT user_id FROM ci_teacher WHERE school_id = %s AND user_id = %s", secure($school_id, 'int'), secure($this->_data['user_id'], 'int'));
                                $get_teacher_id = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
                                if($get_teacher_id->num_rows > 0) {
                                    $is_member = true;
                                } else {
                                    // Kiểm  tra xem có phải là phụ huynh của lớp không
                                    $strSql = sprintf('SELECT UM.user_id FROM ci_user_manage UM INNER JOIN ci_class_child CC ON UM.object_type = %s
                            AND UM.object_id = CC.child_id AND CC.class_id = %s AND CC.status = %s AND UM.user_id = %s', MANAGE_CHILD, secure($group['group_id'], 'int'), secure(STATUS_ACTIVE, 'int'), secure($this->_data['user_id'], 'int'));
                                    $get_users = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
                                    if($get_users->num_rows > 0) {
                                        $is_member = true;
                                    }
                                }
                            }
                        }
                        if($is_member) {
                            $results['groups'][] = $group;
                        }
                    } else {
                        $results['groups'][] = $group;
                    }
                }
            }
        } elseif ($type == 'search_in_pages') {
            if(!isset($args['id']) || $args['id'] < 0) {
                _error(400);
            }
            $id = $args['id'];
            $query = secure($args['query'], 'search', false);

            $order_query = "ORDER BY posts.post_id DESC";
            $where_query = "WHERE posts.user_id = $id AND posts.user_type = 'page' AND posts.text LIKE $query";
            /* get his hidden posts to exclude from newsfeed */
            $hidden_posts = $this->_get_hidden_posts($this->_data['user_id']);
            if(count($hidden_posts) > 0) {
                $hidden_posts_list = implode(',',$hidden_posts);
                $where_query .= " AND (posts.post_id NOT IN ($hidden_posts_list))";
            }
            $get_in_pages = $db->query(sprintf("SELECT posts.post_id FROM posts ".$where_query." ".$order_query." LIMIT %s, %s", secure($offset, 'int', false), secure($system['max_results'], 'int', false) )) or _error(SQL_ERROR_THROWEN);

            if($get_in_pages->num_rows > 0) {
                while($post = $get_in_pages->fetch_assoc()) {
                    $post = $this->get_post($post['post_id'], true, true); /* $full_details = true, $pass_privacy_check = true */
                    if($post) {
                        $results['posts'][] = $post;
                    }
                }
            }
        } elseif ($type == 'search_in_groups') {
            if(!isset($args['id']) || $args['id'] < 0) {
                _error(400);
            }
            $id = $args['id'];
            $query = secure($args['query'], 'search', false);

            $order_query = "ORDER BY posts.post_id DESC";
            $where_query = "WHERE posts.group_id = $id AND posts.in_group = '1' AND posts.text LIKE $query";
            /* get his hidden posts to exclude from newsfeed */
            $hidden_posts = $this->_get_hidden_posts($this->_data['user_id']);
            if(count($hidden_posts) > 0) {
                $hidden_posts_list = implode(',',$hidden_posts);
                $where_query .= " AND (posts.post_id NOT IN ($hidden_posts_list))";
            }
            $get_in_pages = $db->query(sprintf("SELECT posts.post_id FROM posts ".$where_query." ".$order_query." LIMIT %s, %s", secure($offset, 'int', false), secure($system['max_results'], 'int', false) )) or _error(SQL_ERROR_THROWEN);

            if($get_in_pages->num_rows > 0) {
                while($post = $get_in_pages->fetch_assoc()) {
                    $post = $this->get_post($post['post_id'], true, true); /* $full_details = true, $pass_privacy_check = true */
                    if($post) {
                        $results['posts'][] = $post;
                    }
                }
            }

        }

        return $results;
    }

    /* ------------------------------- */
    /* API GET VIDEOS */
    /* ------------------------------- */

    /**
     * get_videos
     *
     * @param integer $id
     * @param string $type
     * @param integer $offset
     * @return array
     */
    /* CI - Mobile thêm biến $is_mobile để check và hiển thị source của image */
    public function get_videos($id, $type = 'user', $offset = 0) {

        $videos = array();
        switch ($type) {
            case 'user':
                // get data
                $posts = $this->get_posts( array('get' => 'posts_profile', 'filter' => 'video', 'id' => $id, 'offset' => $offset) );
                foreach ($posts as $post) {
                    $video = $post['video'];
                    $video['privacy'] = $post['privacy'];
                    $videos[] = $video;
                }
                break;

            case 'page':
                // get data
                $posts = $this->get_posts( array('get' => 'posts_page', 'filter' => 'video', 'id' => $id, 'offset' => $offset) );
                foreach ($posts as $post) {
                    $video = $post['video'];
                    $video['privacy'] = $post['privacy'];
                    $videos[] = $video;
                }
                break;

            case 'group':
                // get data
                $posts = $this->get_posts( array('get' => 'posts_group', 'filter' => 'video', 'id' => $id, 'offset' => $offset) );
                foreach ($posts as $post) {
                    $video = $post['video'];
                    $video['privacy'] = $post['privacy'];
                    $videos[] = $video;
                }
                break;
        }
        return $videos;
    }

    /**
     * GET PAGES AND GROUP NOGA
     *
     * @param array $args
     * @return array
     */
    public function api_get_pages() {
        global $db, $system_page_ids, $system_group_ids, $nogaPages, $nogaGroups;
        $result = array();
        $pages = array();
        $groups = array();
        /* get pages of Noga */
        $get_pages = "";
        $get_groups = "";
        $pageIds = implode(',', $system_page_ids);
        $get_pages = $db->query(sprintf("SELECT * FROM pages WHERE page_id IN (%s)", $pageIds )) or _error(SQL_ERROR_THROWEN);

        if($get_pages->num_rows > 0) {
            while($page = $get_pages->fetch_assoc()) {
                $page['page_title_short'] = $nogaPages[$page['page_id']];
                $page['page_picture'] = $this->get_picture($page['page_picture'], 'page');
                /* check if the viewer liked the page */
                $page['i_like'] = false;
                if($this->_logged_in) {
                    $get_likes = $db->query(sprintf("SELECT * FROM pages_likes WHERE page_id = %s AND user_id = %s", secure($page['page_id'], 'int'), secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                    if($get_likes->num_rows > 0) {
                        $page['i_like'] = true;
                    }
                }
                $pages[] = $page;
            }
        }

        /* get groups of Noga */
        $groupIds = implode(',', $system_group_ids);

        //ConIu - Không suggest các classes
        $strSql = sprintf("SELECT * FROM groups WHERE class_level_id = 0 AND group_id IN (%s)", $groupIds);
        $get_groups = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        if($get_groups->num_rows > 0) {
            while($group = $get_groups->fetch_assoc()) {
                $group['group_title_short'] = $nogaGroups[$group['group_id']];
                $group['group_picture'] = $this->get_picture($group['group_picture'], 'group');
                /* check if the viewer joined the group */
                $group['i_joined'] = false;
                if($this->_logged_in) {
                    $check_membership = $db->query(sprintf("SELECT * FROM groups_members WHERE group_id = %s AND user_id = %s", secure($group['group_id'], 'int'), secure($this->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                    if($check_membership->num_rows > 0) {
                        $group['i_joined'] = true;
                    }
                }
                $groups[] = $group;
            }
        }

        $result = array(
          'pages' => $pages,
          'groups' => $groups
        );
        return $result;
    }


    /**
     * GET PAGES AND GROUP NOGA
     *
     * @param array $args
     * @return boolean
     */
    public function is_manage($user_id) {
        global $db;
        $get_user = $db->query(sprintf('SELECT user_id FROM ci_user_manage WHERE user_id = %1$s AND status = %2$s AND object_type != 1 UNION SELECT user_id FROM ci_teacher WHERE user_id = %1$s AND status = %2$s', secure($user_id, 'int'), secure(STATUS_ACTIVE, 'int'))) or _error(SQL_ERROR_THROWEN);
        $is_manage = ($get_user->num_rows > 0);
        return $is_manage;
    }

    # --- END API CONIU ---
}

?>