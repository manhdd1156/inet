<?php
/**
 * ajax -> data -> upload
 *
 * @package CINet v1.1
 * @author NOGA Co., Ltd
 */

// fetch bootstrap
require('../../../bootstrap.php');

// fetch image class
require('../../class-image.php');

// check AJAX Request
is_ajax();

// check secret
if ($_SESSION['secret'] != $_POST['secret']) {
    _error(403);
}

// check user logged in
/*if(!$user->_logged_in) {
    modal(LOGIN);
}*/

// check type
if (!isset($_POST["type"])) {
    _error(403);
}

// check handle
if (!isset($_POST["handle"])) {
    _error(403);
}

// check multiple
if (!isset($_POST["multiple"])) {
    _error(403);
}

// upload
try {

    switch ($_POST["type"]) {
        case 'photos':
            // check photo upload enabled
            if ($_POST['handle'] == 'publisher' && !$system['photos_enabled']) {
                modal(MESSAGE, __("Not Allowed"), __("This feature has been disabled"));
            }

            // get allowed file size
            if ($_POST['handle'] == 'picture-user' || $_POST['handle'] == 'picture-page' || $_POST['handle'] == 'picture-group') {
                $max_allowed_size = $system['max_avatar_size'] * 1024;
            } elseif ($_POST['handle'] == 'cover-user' || $_POST['handle'] == 'cover-page' || $_POST['handle'] == 'cover-group') {
                $max_allowed_size = $system['max_cover_size'] * 1024;
            } else {
                $max_allowed_size = $system['max_photo_size'] * 1024;
            }

            /* check & create uploads dir */
            $depth = '../../../';
            $folder = 'photos';
            if (!file_exists($depth . $system['system_uploads_directory'] . '/' . $folder)) {
                @mkdir($depth . $system['system_uploads_directory'] . '/' . $folder, 0777, true);
            }
            if (!file_exists($depth . $system['system_uploads_directory'] . '/' . $folder . '/' . date('Y'))) {
                @mkdir($depth . $system['system_uploads_directory'] . '/' . $folder . '/' . date('Y'), 0777, true);
            }
            if (!file_exists($system['system_uploads_directory'] . '/' . $folder . '/' . date('Y') . '/' . date('m'))) {
                @mkdir($depth . $system['system_uploads_directory'] . '/' . $folder . '/' . date('Y') . '/' . date('m'), 0777, true);
            }

            // valid inputs
            if (!isset($_FILES["file"]) || $_FILES["file"]["error"] != UPLOAD_ERR_OK) {
                modal(MESSAGE, __("Upload Error"), __("Something wrong with upload! Is 'upload_max_filesize' set correctly?"));
            }

            // check file size
            if ($_FILES["file"]["size"] > $max_allowed_size) {
                modal(MESSAGE, __("Upload Error"), __("The file size is so big"));
            }

            /* prepare new file name */
            $directory = $folder . '/' . date('Y') . '/' . date('m') . '/';
            $prefix = $system['uploads_prefix'] . '_' . md5(time() * rand(1, 9999));
            $image = new Image($_FILES["file"]["tmp_name"]);
            $image_tmp_name = $directory . $prefix . '_tmp' . $image->_img_ext;
            $image_new_name = $directory . $prefix . $image->_img_ext;
            $path_tmp = $depth . $system['system_uploads_directory'] . '/' . $image_tmp_name;
            $path_new = $depth . $system['system_uploads_directory'] . '/' . $image_new_name;

            /* check if the file uploaded successfully */
            if (!@move_uploaded_file($_FILES['file']['tmp_name'], $path_tmp)) {
                modal(MESSAGE, __("Upload Error"), __("Sorry, can not upload the file"));
            }

            /* save the new image */
            $image->save($path_new, $path_tmp);

            /* delete the tmp image */
            unlink($path_tmp);

            // return the file new name & exit
            return_json(array("file" => $image_new_name));
    }

} catch (Exception $e) {
    modal(ERROR, __("Error"), $e->getMessage());
}

?>