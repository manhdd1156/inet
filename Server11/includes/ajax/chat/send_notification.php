<?php
/**
 * ajax -> chat -> post
 * 
 * @package CINet v1.1
 * @author NOGA Co., Ltd
 */

// fetch bootstrap
require('../../../bootstrap.php');

// check AJAX Request
is_ajax();

// check user logged in
if(!$user->_logged_in) {
    modal(LOGIN);
}

// check username
if (!is_numeric($_POST['friend_id']) || !is_numeric($_POST['count_chat'])) {
    _api_error(400);
}
if (!isset($_POST['conversation_id']) || is_null($_POST['conversation_id'])) {
    _api_error(400);
}

// post message
try {

    $args = array();
    $to_users = $user->get_friends_info($_POST['friend_id']);
    if (is_null($to_users)) {
        return;
    }

    foreach ($to_users as $to_user) {
        if (!is_empty($to_user['device_token'])) {
            $args['device_token'] = $to_user['device_token'];
            $message = isset($_POST['message']) ? $_POST['message'] : '';
            $user_fullname = isset($_POST['user_fullname']) ? convertText4Web($_POST['user_fullname']): '';
            //$user_fullname = $user->_data['user_fullname'];
            $count_chat = isset($_POST['count_chat']) ? $_POST['count_chat'] : 0;

            $args['friend_id'] = $user->_data['user_id'];
            $args['conversation_id'] = $_POST['conversation_id'];
            $args['message'] = $user_fullname . ': ' . $message;
            $args['badge'] = $count_chat + $to_user['user_live_notifications_counter'];

            $user->pushNotice($args);
        }
    }


	
} catch (Exception $e) {
	modal(ERROR, __("Error"), $e->getMessage());
}

?>