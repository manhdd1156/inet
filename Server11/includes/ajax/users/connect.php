<?php
/**
 * ajax -> users -> connect
 * 
 * @package CINet v1.1
 * @author NOGA Co., Ltd
 */

// fetch bootstrap
require('../../../bootstrap.php');
// check AJAX Request
is_ajax();

// check user logged in
if(!$user->_logged_in) {
    modal(LOGIN);
}

// check user activated
if($system['activation_enabled'] && !$user->_data['user_activated']) {
	modal(MESSAGE, __("Not Activated"), __("Before you can interact with other users, you need to confirm your email address"));
}

// valid inputs
//$valid['do'] = array('block', 'unblock', 'friend-accept', 'friend-decline', 'friend-add', 'friend-cancel', 'friend-remove', 'follow', 'unfollow', 'like', 'unlike', 'group-join', 'group-leave', 'group-invite', 'group-accept', 'group-decline');

if(!in_array($_POST['do'], array('block', 'unblock', 'friend-accept', 'friend-decline', 'friend-add', 'friend-cancel', 'friend-remove', 'follow', 'unfollow', 'page-like', 'page-unlike', 'page-boost', 'page-unboost', 'page-invite', 'page-admin-addation', 'page-admin-remove', 'page-member-remove', 'group-join', 'group-leave', 'group-invite', 'group-accept', 'group-decline', 'group-admin-addation', 'group-admin-remove', 'group-member-remove', 'event-go', 'event-ungo', 'event-interest', 'event-uninterest', 'event-invite'))) {
    _error(400);
}
/* check id */
if(!isset($_POST['id']) || !is_numeric($_POST['id'])) {
	_error(400);
}

/* check uid */
if(isset($_POST['uid']) && !is_numeric($_POST['uid'])) {
    _error(400);
}

// connect api
try {
    $db->autocommit(false);
    $db->begin_transaction();
	// connect user
    $_POST['uid'] = ($_POST['uid'] == '0')? null: $_POST['uid'];
    $user->connect($_POST['do'], $_POST['id'], $_POST['uid']);

    $db->commit();
	// return & exit
    if($_POST['do'] == 'page-admin-addation' || $_POST['do'] == 'page-admin-remove' || $_POST['do'] == 'page-member-remove') {
        return_json( array('success' => true, 'message' => __("Success")));
    } else {
        return_json();
    }

} catch (Exception $e) {
    $db->rollback();
	modal(ERROR, __("Error"), $e->getMessage());
} finally {
    $db->autocommit(true);
}
?>