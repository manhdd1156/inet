<?php
/**
 * ajax -> users -> started
 * 
 * @package CINet v1.1
 * @author NOGA Co., Ltd
 */

// fetch bootstrap
require('../../../bootstrap.php');

// check AJAX Request
is_ajax();

// check user logged in
if(!$user->_logged_in) {
	modal(LOGIN);
}

// check user activated
if($system['activation_enabled'] && !$user->_data['user_activated']) {
	modal(MESSAGE, __("Not Activated"), __("Before you can interact with other users, you need to confirm your email address"));
}

// started
try {
    $db->autocommit(false);
    $db->begin_transaction();

    $args = array();
    switch ($_GET['edit']) {
        case 'profile':
            $args['edit'] = 'started_profile';
            $args['city'] = $_POST['city'];

            /* CI - thêm phone và city_id */
            $args['identification_card_number'] = $_POST['identification_card_number'];
            $args['date_of_issue'] = $_POST['date_of_issue'];
            $args['place_of_issue'] = $_POST['place_of_issue'];

            $args['birth_date'] = $_POST['birth_date'];
            if (isset($_POST['password'])) {
                $args['password'] = $_POST['password'];
            }
            /* END - CI */

            $user->settings($args);
            return_json(array('callback' => 'window.location = "'. $system['system_url'] . "/started/finished" . '";'));

            break;

        case 'account':
            $args['edit'] = 'started_account';
            //Cập nhật thông tin tài khoản
            if (!isset($_POST['password'])) {
                _error(404);
            }
            $args['password'] = $_POST['password'];
            $user->settings($args);
            $db->commit();

            return_json(array('callback' => 'window.location = "'. $system['system_url'] . "/started/finished" . '";'));
            break;
    }

} catch (Exception $e) {
    $db->rollback();
	return_json( array('error' => true, 'message' => $e->getMessage()) );
} finally {
    $db->autocommit(true);
}

?>