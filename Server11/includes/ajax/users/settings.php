<?php
/**
 * ajax -> users -> settings
 *
 * @package CINet v1.1
 * @author NOGA Co., Ltd
 */

// fetch bootstrap
require('../../../bootstrap.php');

// check AJAX Request
is_ajax();

// user access
user_access(true);

// settings
try {
    $db->autocommit(false);
    $db->begin_transaction();
    $valid = array(
        'username', 'email', 'basic', 'phone'
    );
    $classDao = null;
    $schoolDao = null;
    if (in_array($_GET['edit'], $valid)) {
        include_once(DAO_PATH . 'dao_class.php');
        $classDao = new ClassDAO();
        include_once(DAO_PATH . 'dao_teacher.php');
        $teacherDao = new TeacherDAO();
    }

    switch ($_GET['edit']) {
        case 'username':
            /* valid inputs */
            if (!isset($_POST['username'])) {
                _error(400);
            }
            $args = array();
            $args['edit'] = $_GET['edit'];
            $args['username'] = $_POST['username'];
            $user->settings($args);
            $db->commit();

            /* ---------- UPDATE - MEMCACHE ---------- */
            //1.Cập nhật thông tin giáo viên
            $classes = $classDao->getClasses($user->_data['user_id']);
            // Lấy thông tin trường
            $schoolIds = $teacherDao->getSchoolIdOfUserId($user->_data['user_id']);
            if (count($schoolIds) > 0) {
                updateTeacherData($user->_data['user_id'], TEACHER_INFO);

            }


            /* ---------- END - MEMCACHE ---------- */

            return_json(array('success' => true, 'message' => __("Done, Your username has been updated")));
            break;

        case 'email':
            /* valid inputs */
            if (!isset($_POST['email'])) {
                _error(400);
            }
            $args = array();
            $args['edit'] = $_GET['edit'];
            $args['email'] = $_POST['email'];
            $user->settings($args);
            $db->commit();

            /* ---------- UPDATE - MEMCACHE ---------- */
            //1.Cập nhật thông giáo viên
            $classes = $classDao->getClasses($user->_data['user_id']);

            // Lấy thông tin trường
            $schoolIds = $teacherDao->getSchoolIdOfUserId($user->_data['user_id']);
            if (count($schoolIds) > 0) {
                updateTeacherData($user->_data['user_id'], TEACHER_INFO);

            }
            /* ---------- END - MEMCACHE ---------- */

            return_json(array('success' => true, 'message' => __("Done, Your email has been updated")));
            break;

        case 'password':
            /* valid inputs */
            if (!isset($_POST['current']) || !isset($_POST['new']) || !isset($_POST['confirm'])) {
                _error(400);
            }
            $args = array();
            $args['edit'] = $_GET['edit'];
            $args['current'] = $_POST['current'];
            $args['new'] = $_POST['new'];
            $args['confirm'] = $_POST['confirm'];
            $user->settings($args);

            $db->commit();
            return_json(array('success' => true, 'message' => __("Done, Your password has been updated")));
            break;

        case 'phone':
            /* valid inputs */
            $phone = standardizePhone($_POST['phone']);
            if (valid_phone($phone)) {
                throw new Exception(__("Please enter a valid mobile number"));
            }

            $user->settings($_POST);
            $db->commit();

            /* ---------- UPDATE - MEMCACHE ---------- */
            //1.Cập nhật thông giáo viên
            $classes = $classDao->getClasses($user->_data['user_id']);

            // Lấy thông tin trường
            $schoolIds = $teacherDao->getSchoolIdOfUserId($user->_data['user_id']);
            if (count($schoolIds) > 0) {
                updateTeacherData($user->_data['user_id'], TEACHER_INFO);

            }
            /* ---------- END - MEMCACHE ---------- */

            return_json(array('success' => true, 'message' => __("Your phone has been updated")));
            break;

        case 'basic':
            /* valid inputs */
            if (!isset($_POST['firstname']) || !isset($_POST['lastname']) || !isset($_POST['gender']) || !isset($_POST['birth_month']) || !isset($_POST['birth_day']) || !isset($_POST['birth_year'])) {
                _error(400);
            }
            $args = array();
            $args['edit'] = $_GET['edit'];
            $args['firstname'] = $_POST['firstname'];
            $args['lastname'] = $_POST['lastname'];
            $args['gender'] = $_POST['gender'];

            /* CI - Thêm user_phone và city_id */
            $args['city_id'] = $_POST['city_id'];
            /* END - CI */

            $args['birth_month'] = $_POST['birth_month'];
            $args['birth_day'] = $_POST['birth_day'];
            $args['birth_year'] = $_POST['birth_year'];
            $user->settings($args);
            $db->commit();

            /* ---------- UPDATE - MEMCACHE ---------- */
            //1.Cập nhật thông giáo viên
            $classes = $classDao->getClasses($user->_data['user_id']);
            if (!empty($classes)) {
                updateTeacherData($user->_data['user_id'], TEACHER_INFO);
            }

            $schoolIds = $teacherDao->getSchoolIdOfUserId($user->_data['user_id']);
            if (count($schoolIds) > 0) {
                updateTeacherData($user->_data['user_id'], TEACHER_INFO);

            }
            /* ---------- END - MEMCACHE ---------- */

            return_json(array('success' => true, 'message' => __("Done, Your profile info has been updated")));
            break;

        case 'work':
            /* valid inputs */
            if (!isset($_POST['work_title']) || !isset($_POST['work_place'])) {
                _error(400);
            }
            $args = array();
            $args['edit'] = $_GET['edit'];
            $args['work_title'] = $_POST['work_title'];
            $args['work_place'] = $_POST['work_place'];
            $args['work_url'] = isset($_POST['work_url']) ? $_POST['work_url'] : "";
            $user->settings($args);
            $db->commit();
            return_json(array('success' => true, 'message' => __("Done, Your profile info has been updated")));
            break;

        case 'location':
            /* valid inputs */
            if (!isset($_POST['city']) || !isset($_POST['hometown'])) {
                _error(400);
            }
            $args = array();
            $args['edit'] = $_GET['edit'];
            $args['city'] = $_POST['city'];
            $args['hometown'] = $_POST['hometown'];
            $user->settings($args);
            $db->commit();
            return_json(array('success' => true, 'message' => __("Done, Your profile info has been updated")));
            break;

        case 'education':
            /* valid inputs */
            if (!isset($_POST['edu_major']) || !isset($_POST['edu_school']) || !isset($_POST['edu_class'])) {
                _error(400);
            }
            $args = array();
            $args['edit'] = $_GET['edit'];
            $args['edu_major'] = $_POST['edu_major'];
            $args['edu_school'] = $_POST['edu_school'];
            $args['edu_class'] = $_POST['edu_class'];
            $user->settings($args);
            $db->commit();
            return_json(array('success' => true, 'message' => __("Done, Your profile info has been updated")));
            break;

        case 'privacy':
            $_POST['edit'] = $_GET['edit'];
            $user->settings($_POST);
            $db->commit();
            return_json(array('success' => true, 'message' => __("Done, Your privacy settings have been updated")));
            break;

        case 'chat':
            /* valid inputs */
            if (!isset($_POST['privacy_chat'])) {
                _error(400);
            }
            $args = array();
            $args['edit'] = $_GET['edit'];
            $args['privacy_chat'] = $_POST['privacy_chat'];
            $user->settings($args);
            $db->commit();
            return_json(array('success' => true, 'message' => __("Done, Your privacy settings have been updated")));
            break;

        default:
            _error(400);
            break;
    }

} catch (Exception $e) {
    $db->rollback();
    return_json(array('error' => true, 'message' => $e->getMessage()));
} finally {
    $db->autocommit(true);
}

?>