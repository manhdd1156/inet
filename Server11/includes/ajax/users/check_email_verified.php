<?php
/**
 * ajax -> users -> settings
 *
 * @package CINet v1.1
 * @author NOGA Co., Ltd
 */

// fetch bootstrap
require('../../../bootstrap.php');

// check AJAX Request
is_ajax();

// user access
user_access(true);

// settings
try {
    $results = $user->check_email_verified();
    $return['is_verified'] = $results['is_verified'];
    $return['email'] = $results['email'];
    return_json($return);

} catch (Exception $e) {
    return_json(array('error' => true, 'message' => $e->getMessage()));
}

?>