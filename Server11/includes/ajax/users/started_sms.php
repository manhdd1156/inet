<?php
/**
 * ajax -> users -> started
 * 
 * @package CINet v1.1
 * @author NOGA Co., Ltd
 */

// fetch bootstrap
require('../../../bootstrap.php');

// check AJAX Request
is_ajax();

// started
try {
    $db->autocommit(false);
    $db->begin_transaction();

    $args = array();
    switch ($_GET['do']) {

        case 'register':
            $args['last_name'] = $_POST['last_name'];
            $args['first_name'] = $_POST['first_name'];
            $args['avatar'] = $_POST['avatar'];
            $args['phone'] = $_SESSION['phone'];

            $args['identification_card_number'] = $_POST['identification_card_number'];
            $args['date_of_issue'] = $_POST['date_of_issue'];
            $args['place_of_issue'] = $_POST['place_of_issue'];

            $args['birth_date'] = $_POST['birth_date'];
            $args['password'] = $_POST['password'];

            $user_info = $user->sign_up_via_sms($args);

            /* CI - Cho user like các page và join các group của hệ thống */
            include_once(DAO_PATH.'dao_user.php');
            $userDao = new UserDAO();

            $userDao->addUsersLikeSomePages($system_page_ids, [$user_info['user_id']]);
            $userDao->addUserToSomeGroups($system_group_ids, [$user_info['user_id']]);
            /* END - CI */
            break;
    }

    $db->commit();
    return_json( array('callback' => 'window.location.reload();') );

} catch (Exception $e) {
    $db->rollback();
	return_json( array('error' => true, 'message' => $e->getMessage()) );
} finally {
    $db->autocommit(true);
}

?>