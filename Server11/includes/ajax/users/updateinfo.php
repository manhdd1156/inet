<?php
/**
 * ajax -> users -> update Email and Phone
 *
 * @package CINet v1.1
 * @author NOGA Co., Ltd
 */

// fetch bootstrap
require('../../../bootstrap.php');

// check AJAX Request
is_ajax();

// check user logged in
if(!$user->_logged_in) {
    modal(LOGIN);
}

// check user activated
if($system['activation_enabled'] && !$user->_data['user_activated']) {
    modal(MESSAGE, __("Not Activated"), __("Before you can interact with other users, you need to confirm your email address"));
}

// started
try {
    $db->autocommit(false);
    $db->begin_transaction();

    $args = array();
    $args['edit'] = 'emailandphone';
    $args['user_phone'] = standardizePhone($_POST['user_phone']); //ConIu - Them vao 2 truong moi
    $args['user_email'] = $_POST['user_email'];
    $user->settings($args);
    $db->commit();

    //return_json( array('success' => true, 'message' => __("Done, Your info has been updated")) );
    return_json(array('callback' => 'window.location = "'. $system['system_url'] . '";'));

} catch (Exception $e) {
    $db->rollback();
    return_json( array('error' => true, 'message' => $e->getMessage()) );
} finally {
    $db->autocommit(true);
}

?>