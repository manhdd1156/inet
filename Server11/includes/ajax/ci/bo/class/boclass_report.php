<?php
/**
 * Package: ajax/ci/bo/class
 *
 * @package ConIu
 * @author TaiLA
 */

// fetch bootstrap
require('../../../../../bootstrap.php');

// check AJAX Request
is_ajax();
check_login();

// initialize the return array
$return = array();
$return['callback'] = 'window.location.replace(response.path);';

include_once(DAO_PATH.'dao_school.php');
include_once(DAO_PATH.'dao_class.php');
include_once(DAO_PATH.'dao_child.php');
include_once(DAO_PATH.'dao_parent.php');
include_once(DAO_PATH.'dao_user.php');
include_once(DAO_PATH.'dao_report.php');
include_once(DAO_PATH.'dao_mail.php');

$schoolDao = new SchoolDAO();
$classDao = new ClassDAO();
$childDao = new ChildDAO();
$parentDao = new ParentDAO();
$userDao = new UserDAO();
$reportDao = new ReportDAO();
$mailDao = new MailDAO();

//Lấy ra thông tin lớp
//$class = $classDao->getClassByUsername($_POST['username'], $user->_data['user_id']);
$class = getClassDataByUsername($_POST['username'], CLASS_INFO);
if (is_null($class)) {
    _error(403);
}
$smarty->assign('username', $_POST['username']);
// Lấy ra thông tin trường
//$school = $schoolDao->getSchoolById($class['school_id']);
$school = getSchoolData($class['school_id'], SCHOOL_INFO);
// Lấy cấu hình thông báo của những user quản lý trường
//$notify_settings = $schoolDao->getUserNotificationSettingOfSchool($class['school_id']);
$notify_settings = getSchoolData($class['school_id'], SCHOOL_NOTIFICATIONS);
try {
    $return = array();
    $db->autocommit(false);
    switch ($_POST['do']) {
        case 'add':
            $db->begin_transaction();
            if(is_null($_POST['child'])) {
                throw new Exception(__("You have not picked a student yet"));
            }
            $args = array();
            $args['report_name'] = trim($_POST['title']);
            $args['school_id'] = $class['school_id'];
            $args['child'] = $_POST['child'];
            $args['is_notified'] = (isset($_POST['notify_immediately']) && $_POST['notify_immediately'] == 'on')? 1: 0;
            $args['status'] = 1;
            $args['date'] = 'null';
            if($args['is_notified'] == 1) {
                $args['date'] = $date;
            }

            $file_name = "";
            // Upload file đính kèm
            if(file_exists($_FILES['file']['tmp_name'])) {
                // check file upload enabled
                if(!$system['file_enabled']) {
                    throw new Exception(__("This feature has been disabled"));
                }

                // valid inputs
                if(!isset($_FILES["file"]) || $_FILES["file"]["error"] != UPLOAD_ERR_OK) {
                    throw new Exception(__("Something wrong with upload! Is 'upload_max_filesize' set correctly?"));
                }

                // check file size
                $max_allowed_size = $system['max_file_size'] * 1024;
                if($_FILES["file"]["size"] > $max_allowed_size) {
                    throw new Exception(__("The file size is so big"));
                }

                // check file extesnion
                $extension = get_extension($_FILES['file']['name']);
                if(!valid_extension($extension, $system['file_report_extensions'])) {
                    throw new Exception(__("The file type is not valid or not supported"));
                }

                /* check & create uploads dir */
                $depth = '../../../../../';
                $folder = 'reports/'. $class['school_id'];
                if(!file_exists($depth.$system['system_uploads_directory'].'/'.$folder)) {
                    @mkdir($depth.$system['system_uploads_directory'].'/'.$folder, 0777, true);
                }
                if(!file_exists($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y'))) {
                    @mkdir($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y'), 0777, true);
                }
                if(!file_exists($system['system_uploads_directory'].'/'.$folder.'/' . date('Y') . '/' . date('m'))) {
                    @mkdir($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y') . '/' . date('m'), 0777, true);
                }

                /* prepare new file name */
                $directory = $folder.'/'. date('Y') . '/' . date('m') . '/';
                $prefix = $system['uploads_prefix'].'_'.md5(time()*rand(1, 9999));
                $file_name = $directory.$prefix.'.'.$extension;
                $path = $depth.$system['system_uploads_directory'].'/'.$file_name;

                /* check if the file uploaded successfully */
                if(!@move_uploaded_file($_FILES['file']['tmp_name'], $path)) {
                    throw new Exception(__("Sorry, can not upload the file"));
                }
            }

            $args['source_file'] = $file_name;
            $args['file_name'] = $_FILES['file']['name'];

            // Lấy id category
            $categoryIds = isset($_POST['category_ids'])? $_POST['category_ids']:array();
            if(count($categoryIds) == 0 && is_empty($args['source_file'])) {
                throw new Exception(__("You must select report template"));
            }
            // 1. Insert vào bảng ci_report
            if(is_empty($args['source_file']) && (count($_POST['category_ids']) == 0)){
                throw new Exception(__("You must have an attachment or contact book content"));
            }
            $reportIds = $reportDao->insertReportClass($args);

            foreach ($reportIds as $reportId) {
                // Tăng lượt thêm mới -TaiLA
                addInteractive($class['school_id'], 'report', 'school_view', $reportId, 1);

            }
            // 2. Inser vào bảng ci_report_category

            // Lặp mảng categoryIds lấy nội dung gửi lên tương ứng
            $categorys = array();
            $category = array();
            if(count($categoryIds) > 0) {
                foreach ($categoryIds as $id) {
                    $category['multi_content'] = isset($_POST['suggest_' . $id]) ? $_POST['suggest_' . $id] : array();
                    $category['report_category_content'] = $_POST['content_' . $id];
                    $category['report_category_name'] = $_POST['category_name_' . $id];

                    // Lấy toàn bộ gợi ý của category
                    $categoryTemp = $reportDao->getReportTemplateCategoryDetail($id);
                    $suggests = $categoryTemp['suggests'];
                    $category['template_multi_content'] = array();
                    foreach ($suggests as $row) {
                        $category['template_multi_content'][] = convertText4Web($row);
                    }
//                    $category['template_multi_content'] = convertText4Web($category['template_multi_content']);
                    $categorys[] = $category;
                }
                for($i = 0; $i < count($reportIds); $i++) {
                    $reportDao->insertReportDetail($reportIds[$i], $categorys);
                }
            }

            //3. Gửi email và thông báo đến phụ huynh
            if ($args['is_notified']) {
                $argEmail = array();
                if (!is_empty($file_name)) {
                    $argEmail['file_attachment'] = $system['system_uploads_directory'].'/'.$file_name;
                    $argEmail['file_attachment_name'] = $args['file_name'];
                }

                $content = "";
                $isOther = false;
                for($i = 0; $i < count($cates); $i ++) {
                    $content = $content . "- <b>" . $cates[$i] . "-</b>: " . $contents[$i] . "<br/><br/>";
                }
                $smarty->assign('title', $args['report_name']);
                $smarty->assign('content', $content);
                $smarty->assign('class_name', convertText4Web($class['group_title']));

                $argEmail['action'] = "create_report";
                $argEmail['subject'] = $args['report_name'];
                $argEmail['school_id'] = $args['school_id'];
                $argEmail['user_id'] = $user->_data['user_id'];


                $receivers = "";
                foreach ($args['child'] as $child_id) {
                    $parent_email = array();
                    //$child = $childDao->getChild($child_id);
                    $child = getChildData($child_id, CHILD_INFO);
                    if (!is_null($child)) {
                        if (!is_empty($child['parent_email'])) {
                            $parent_email[] = $child['parent_email'];

                        }
                        //$parents = $childDao->getParent($child_id);
                        $parents = getChildData($child_id, CHILD_PARENTS);
                        if (!is_null($parents)) {
                            foreach ($parents as $parent) {
                                $reports = $reportDao->getChildReport($child_id);
                                foreach ($reports as $report) {
                                    if(in_array($report['report_id'], $reportIds)) {
                                        //thông báo về cho phụ huynh
                                        $userDao->postNotifications($parent['user_id'], NOTIFICATION_NEW_REPORT, NOTIFICATION_NODE_TYPE_CHILD,
                                            $report['report_id'], convertText4Web($_POST['title']), $child_id, convertText4Web($child['child_name']));
                                    }
                                }

                                // lấy danh sách email
                                if (!is_empty($parent['user_email'])) {
                                    $parent_email[] = $parent['user_email'];
                                }
                            }
                        }

                        $parent_email = array_unique($parent_email);
                        $emai_str = implode(",", $parent_email);
                        $receivers = $receivers . ',' . $emai_str;

                        $smarty->assign('child_name', convertText4Web($child['child_name']));
                        $argEmail['content'] = $smarty->fetch("ci/email_templates/create_report.tpl");
                        $argEmail['receivers'] = $emai_str;

                        $argEmail['delete_after_sending'] = 1;
                        // $mailDao->insertEmail($argEmail);
                    }
                }
                // Thông báo đến quản lý trường
//                $userManagerIds = getUserIdsManagerReceiveNotify($class['school_id'], 'reports', $school['page_admin']);
                $userManagerIds = getUserIdsReceiveNotifyOfSchoolForClass($notify_settings, 'reports', $class['school_id'], $school['page_admin']);
                $children_report = '';
                foreach ($reportIds as $reportId) {
                    $report = $reportDao->getReportById($reportId);
                    $child = getChildData($report['child_id'], CHILD_INFO);
                    $children_report .= ' ' . $child['child_name'] . ',';
//                    if (!is_null($child)) {
//                        $userDao->postNotifications($userManagerIds, NOTIFICATION_NEW_REPORT, NOTIFICATION_NODE_TYPE_SCHOOL,
//                            $reportId, convertText4Web($_POST['title']), $school['page_name'], convertText4Web($child['child_name']));
//                    }
                }
                $children_report = trim($children_report, ' ,');
                if($children_report != '') {
                    $userDao->postNotifications($userManagerIds, NOTIFICATION_NEW_REPORT, NOTIFICATION_NODE_TYPE_SCHOOL,
                        $school['page_id'], convertText4Web($_POST['title']), $school['page_name'], convertText4Web($children_report));
                }

                /* Coniu - Tương tác trường */
                setIsAddNew($class['school_id'], 'report_created', $reportIds);
                /* Coniu - END */
            }

            $db->commit();

            //return_json(array('callback' => 'window.location = "'.$system['system_url'].'/class/'.$class['group_name'].'/reports";'));
            return_json( array('success' => true, 'message' => __("Contact book has been created")) );
            break;

        case 'edit':
            $data = $reportDao->getReportById($_POST['report_id']);
            if (is_null($data)) {
                _error(404);
            }
            $args = array();
            $args['report_id'] = $_POST['report_id'];
            $args['is_notified'] = (isset($_POST['notify_immediately']) && $_POST['notify_immediately'] == 'on')? 1: 0;
            $args['report_name'] = trim($_POST['title']);

            $args['date'] = 'null';
            if($args['is_notified'] == 1) {
                $args['date'] = $date;
            }

            $args['source_file'] = $data['source_file_path'];
            $args['file_name'] = convertText4Web($data['file_name']);
            if(!$_POST['is_file']) {
                $args['source_file'] = "";
                $args['file_name'] = "";
            }
            $file_name = "";
            // Upload file đính kèm
            if(file_exists($_FILES['file']['tmp_name'])) {
                // check file upload enabled
                if(!$system['file_enabled']) {
                    throw new Exception(__("This feature has been disabled"));
                }

                // valid inputs
                if(!isset($_FILES["file"]) || $_FILES["file"]["error"] != UPLOAD_ERR_OK) {
                    throw new Exception(__("Something wrong with upload! Is 'upload_max_filesize' set correctly?"));
                }

                // check file size
                $max_allowed_size = $system['max_file_size'] * 1024;
                if($_FILES["file"]["size"] > $max_allowed_size) {
                    throw new Exception(__("The file size is so big"));
                }

                // check file extesnion
                $extension = get_extension($_FILES['file']['name']);
                if(!valid_extension($extension, $system['file_report_extensions'])) {
                    throw new Exception(__("The file type is not valid or not supported"));
                }

                /* check & create uploads dir */
                $depth = '../../../../../';
                $folder = 'reports/'. $class['school_id'];
                if(!file_exists($depth.$system['system_uploads_directory'].'/'.$folder)) {
                    @mkdir($depth.$system['system_uploads_directory'].'/'.$folder, 0777, true);
                }
                if(!file_exists($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y'))) {
                    @mkdir($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y'), 0777, true);
                }
                if(!file_exists($system['system_uploads_directory'].'/'.$folder.'/' . date('Y') . '/' . date('m'))) {
                    @mkdir($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y') . '/' . date('m'), 0777, true);
                }

                /* prepare new file name */
                $directory = $folder.'/'. date('Y') . '/' . date('m') . '/';
                $prefix = $system['uploads_prefix'].'_'.md5(time()*rand(1, 9999));
                $file_name = $directory.$prefix.'.'.$extension;
                $path = $depth.$system['system_uploads_directory'].'/'.$file_name;

                /* check if the file uploaded successfully */
                if(!@move_uploaded_file($_FILES['file']['tmp_name'], $path)) {
                    throw new Exception(__("Sorry, can not upload the file"));
                }
                $args['source_file'] = $file_name;
                $args['file_name'] = $_FILES['file']['name'];
            }

            if($_POST['report_template_id'] == null) {
                if(is_empty($args['source_file']) && !isset($_POST['report_category_ids'])){
                    throw new Exception(__("You must have an attachment or contact book content"));
                }
            } else {
                if(is_empty($args['source_file']) && !isset($_POST['category_ids'])){
                    throw new Exception(__("You must have an attachment or contact book content"));
                }
            }

            // 1. Cập nhật bảng ci_report
            $reportDao->updateReport($args);

            // Nếu không chọn mẫu khác thì update category cũ
            if($_POST['report_template_id'] == null) {
                // 2. cập nhật bảng ci_report_category
                // Lấy id category
                $categoryIds = array();
                $categoryIds = $_POST['report_category_ids'];
                // Lặp mảng categoryIds lấy nội dung gửi lên tương ứng
                $category = array();
                if(count($categoryIds) > 0) {
                    foreach ($categoryIds as $id) {
                        $category['report_category_id'] = $id;
                        $category['multi_content'] = isset($_POST['report_suggest_' . $id]) ? $_POST['report_suggest_' . $id] : array();
                        $category['report_category_content'] = $_POST['report_content_' . $id];
//                    $categorys[] = $category;
                        // Update report category
                        $reportDao->updateReportCategory($category);
                    }
                }
            } else {
                // Nếu chọn mẫu khác thì insert category mới
                //3. Xóa report category cũ
                $reportDao->deleteReportDetail($args['report_id']);

                //4. Thêm report category mới
                // Lấy id category
                $categoryIds = array();
                $categoryIds = $_POST['category_ids'];
                // Lặp mảng categoryIds lấy nội dung gửi lên tương ứng
                $categorys = array();
                $category = array();
                if(count($categoryIds) > 0) {
                    foreach ($categoryIds as $k => $id) {
                        $category['multi_content'] = isset($_POST['suggest_' . $id]) ? $_POST['suggest_' . $id] : array();
                        $category['report_category_content'] = $_POST['content_' . $id];
                        $category['report_category_name'] = $_POST['category_name_' . $id];
                        // Lấy toàn bộ gợi ý của category
                        $categoryTemp = $reportDao->getReportTemplateCategoryDetail($id);
                        $suggests = $categoryTemp['suggests'];
                        $category['template_multi_content'] = array();
                        foreach ($suggests as $row) {
                            $category['template_multi_content'][] = convertText4Web($row);
                        }
                        $categorys[] = $category;
                    }
                    // Insert chi tiết sổ liên lạc (bảng ci_report_category)
                    $reportDao->insertReportDetail($_POST['report_id'], $categorys);
                }
            }

            // Gửi email và thông báo đến phụ huynh
            if ($args['is_notified']) {
                $argEmail = array();
                if (!is_empty($file_name)) {
                    $argEmail['file_attachment'] = $system['system_uploads_directory'].'/'.$file_name;
                    $argEmail['file_attachment_name'] = $args['file_name'];
                } else {
                    $argEmail['file_attachment'] = $system['system_uploads_directory'].'/' . $args['source_file'];
                    $argEmail['file_attachment_name'] = $args['file_name'];
                }

                $content = "";
                $isOther = false;
                for($i = 0; $i < count($cates); $i ++) {
                    $content = $content . "- <b>" . $cates[$i] . "-</b>: " . $contents[$i] . "<br/><br/>";
                }
                $smarty->assign('title', $args['report_name']);
                $smarty->assign('content', $content);
                $smarty->assign('class_name', convertText4Web($class['group_title']));

                $argEmail['action'] = "create_report";
                $argEmail['subject'] = $args['report_name'];
                $argEmail['school_id'] = $args['school_id'];
                $argEmail['user_id'] = $user->_data['user_id'];


                $receivers = "";
                $parent_email = array();
                //$child = $childDao->getChild($data['child_id']);
                $child = getChildData($data['child_id'], CHILD_INFO);
                if (!is_null($child)) {
                    if (!is_empty($child['parent_email'])) {
                        $parent_email[] = $child['parent_email'];

                    }
                    //$parents = $childDao->getParent($child['child_id']);
                    $parents = getChildData($child['child_id'], CHILD_PARENTS);
                    if (!is_null($parents)) {
                        $parentIds = array_keys($parents);
                        //thông báo về cho phụ huynh
                        $userDao->postNotifications($parentIds, NOTIFICATION_UPDATE_REPORT, NOTIFICATION_NODE_TYPE_CHILD,
                            $_POST['report_id'], convertText4Web($_POST['title']), $child['child_id'], convertText4Web($child['child_name']));
                        foreach ($parents as $parent) {
                            // lấy danh sách email
                            if (!is_empty($parent['user_email'])) {
                                $parent_email[] = $parent['user_email'];
                            }
                        }
                    }

                    $parent_email = array_unique($parent_email);
                    $emai_str = implode(",", $parent_email);
                    $receivers = $receivers . ',' . $emai_str;

                    $smarty->assign('child_name', convertText4Web($child['child_name']));
                    $argEmail['content'] = $smarty->fetch("ci/email_templates/create_report.tpl");
                    $argEmail['receivers'] = $emai_str;

                    $argEmail['delete_after_sending'] = 1;
                    // $mailDao->insertEmail($argEmail);
                }

                // Thông báo đến quản lý trường
                $school = getSchoolData($class['school_id'], SCHOOL_INFO);
                $userManagerIds = getUserIdsReceiveNotifyOfSchoolForClass($notify_settings, 'reports', $class['school_id'], $school['page_admin']);
                $child = getChildData($data['child_id'], CHILD_INFO);
                if (!is_null($child)) {
                    $userDao->postNotifications($userManagerIds, NOTIFICATION_UPDATE_REPORT, NOTIFICATION_NODE_TYPE_SCHOOL,
                        $data['report_id'], convertText4Web($_POST['title']), $school['page_name'], convertText4Web($child['child_name']));
                }
            }

            $db->commit();
            return_json(array('callback' => 'window.location = "'.$system['system_url'].'/class/'.$class['group_name'].'/reports";'));
            break;

        case 'notify':
            $db->begin_transaction();
            //Lấy ra thông tin thông báo
            $data = $reportDao->getReportById($_POST['id']);

            if (is_null($data)) {
                _error(404);
            }
            $data['file_name'] = convertText4Web($data['file_name']);

            /*if ($data['created_user_id'] != $user->_data['user_id']) {
                _error(403);
            }*/

            if (!$data['is_notified']) {
                $argEmail = array();
                if (!is_empty($data['source_file'])) {
                    $argEmail['file_attachment'] = $system['system_uploads_directory'].'/'.$data['source_file_path'];
                    $argEmail['file_attachment_name'] = $data['file_name'];
                }

                $content = "";
                for($i = 0; $i < count($data['detail']); $i++) {
                    $content = $content . "- <b>" . $data['detail'][$i]['report_detaill_name'] . "-</b>: " . $data['detail'][$i]['report_detail_content'] . "<br/><br/>";
                }
                $smarty->assign('title', $data['report_name']);
                $smarty->assign('content', $content);
                $smarty->assign('class_name', convertText4Web($class['group_title']));

                $argEmail['action'] = "create_report";
                $argEmail['subject'] = $data['report_name'];
                $argEmail['school_id'] = $data['school_id'];
                $argEmail['user_id'] = $user->_data['user_id'];


                $receivers = "";
                $parent_email = array();
                //$child = $childDao->getChild($data['child_id']);
                $child = getChildData($data['child_id'], CHILD_INFO);
                if (!is_null($child)) {
                    if (!is_empty($child['parent_email'])) {
                        $parent_email[] = $child['parent_email'];
                    }

                    $parents = getChildData($data['child_id'], CHILD_PARENTS);
                    if (!is_null($parents)) {
                        $parentIds = array_keys($parents);
                        //thông báo về cho phụ huynh
                        $userDao->postNotifications($parentIds, NOTIFICATION_NEW_REPORT, NOTIFICATION_NODE_TYPE_CHILD,
                            $_POST['id'], convertText4Web($data['report_name']), $child['child_id'], convertText4Web($child['child_name']));
                        foreach ($parents as $parent) {

                            // lấy danh sách email
                            if (!is_empty($parent['user_email'])) {
                                $parent_email[] = $parent['user_email'];
                            }
                        }
                    }

                    $parent_email = array_unique($parent_email);
                    $emai_str = implode(",", $parent_email);
                    $receivers = $receivers . ',' . $emai_str;

                    $smarty->assign('child_name', convertText4Web($child['child_name']));
                    $argEmail['content'] = $smarty->fetch("ci/email_templates/create_report.tpl");
                    $argEmail['receivers'] = $emai_str;

                    $argEmail['delete_after_sending'] = 1;
                    //$mailDao->insertEmail($argEmail);
                }
                // Thông báo đến quản lý trường
                $school = getSchoolData($class['school_id'], SCHOOL_INFO);
                $userManagerIds = getUserIdsReceiveNotifyOfSchoolForClass($notify_settings, 'reports', $class['school_id'], $school['page_admin']);
                $report = $reportDao->getReportById($data['report_id']);
                $child = getChildData($report['child_id'], CHILD_INFO);
                if (!is_null($child)) {
                    $userDao->postNotifications($userManagerIds, NOTIFICATION_NEW_REPORT, NOTIFICATION_NODE_TYPE_SCHOOL,
                        $data['report_id'], convertText4Web($data['report_name']), $school['page_name'], convertText4Web($child['child_name']));
                }

                /* Coniu - Tương tác trường */
                setIsAddNew($class['school_id'], 'report_created', [$data['report_id']]);
                /* Coniu - END */
            }

            // Cập nhật trạng thái đã gửi thông báo.
            $reportDao->updateStatusToNotified($_POST['id']);

            $db->commit();
            return_json(array('callback' => 'window.location = "'.$system['system_url'].'/class/'.$_POST['username'].'/reports";'));
            break;

        case 'notify_all':
            // Thông báo tất cả những sổ liên lạc chưa thông báo
            $db->begin_transaction();

            // Lấy ra tất cả những sổ liên lạc của lớp chưa được thông báo
            $noNotifyReports = $reportDao->getClassReportNoNotify($class['group_id']);

            foreach ($noNotifyReports as $data) {
                $data['file_name'] = convertText4Web($data['file_name']);
                if (!$data['is_notified']) {
                    $argEmail = array();
                    if (!is_empty($data['source_file'])) {
                        $argEmail['file_attachment'] = $system['system_uploads_directory'].'/'.$data['source_file_path'];
                        $argEmail['file_attachment_name'] = $data['file_name'];
                    }

                    $content = "";
                    for($i = 0; $i < count($data['detail']); $i++) {
                        $content = $content . "- <b>" . $data['detail'][$i]['report_detaill_name'] . "-</b>: " . $data['detail'][$i]['report_detail_content'] . "<br/><br/>";
                    }
                    $smarty->assign('title', $data['report_name']);
                    $smarty->assign('content', $content);
                    $smarty->assign('class_name', convertText4Web($class['group_title']));

                    $argEmail['action'] = "create_report";
                    $argEmail['subject'] = $data['report_name'];
                    $argEmail['school_id'] = $data['school_id'];
                    $argEmail['user_id'] = $user->_data['user_id'];


                    $receivers = "";
                    $parent_email = array();
                    //$child = $childDao->getChild($data['child_id']);
                    $child = getChildData($data['child_id'], CHILD_INFO);
                    if (!is_null($child)) {
                        if (!is_empty($child['parent_email'])) {
                            $parent_email[] = $child['parent_email'];
                        }

                        $parents = getChildData($data['child_id'], CHILD_PARENTS);
                        if (!is_null($child)) {
                            $parentIds = array_keys($parents);
                            //thông báo về cho phụ huynh
                            $userDao->postNotifications($parentIds, NOTIFICATION_NEW_REPORT, NOTIFICATION_NODE_TYPE_CHILD,
                                $data['report_id'], convertText4Web($data['report_name']), $child['child_id'], convertText4Web($child['child_name']));
                            foreach ($parents as $parent) {
                                // lấy danh sách email
                                if (!is_empty($parent['user_email'])) {
                                    $parent_email[] = $parent['user_email'];
                                }
                            }
                        }

                        $parent_email = array_unique($parent_email);
                        $emai_str = implode(",", $parent_email);
                        $receivers = $receivers . ',' . $emai_str;

                        $smarty->assign('child_name', convertText4Web($child['child_name']));
                        $argEmail['content'] = $smarty->fetch("ci/email_templates/create_report.tpl");
                        $argEmail['receivers'] = $emai_str;

                        $argEmail['delete_after_sending'] = 1;
                        //$mailDao->insertEmail($argEmail);
                    }

                    // Thông báo đến quản lý trường
                    $school = getSchoolData($class['school_id'], SCHOOL_INFO);
                    $userManagerIds = getUserIdsManagerReceiveNotify($class['school_id'], 'reports', $school['page_admin']);
//                    $report = $reportDao->getReportById($data['report_id']);
                    $child = getChildData($report['child_id'], CHILD_INFO);
                    if (!is_null($child)) {
                        $userDao->postNotifications($userManagerIds, NOTIFICATION_NEW_REPORT, NOTIFICATION_NODE_TYPE_SCHOOL,
                            $data['report_id'], convertText4Web($data['report_name']), $school['page_name'], convertText4Web($data['child_name']));
                    }

                    /* Coniu - Tương tác trường */
                    setIsAddNew($class['school_id'], 'report_created', [$data['report_id']]);
                    /* Coniu - END */
                }
                // Cập nhật trạng thái đã gửi thông báo.
                $reportDao->updateStatusToNotified($data['report_id']);
            }

            $db->commit();
            return_json(array('callback' => 'window.location = "'.$system['system_url'].'/class/'.$_POST['username'].'/reports";'));
            break;

        case 'template_detail':
            if (!isset($_POST['template_id']) || !is_numeric($_POST['template_id'])) {
                _error(404);
            }
            $results = $reportDao->getReportTemplate($_POST['template_id'], 1);
            $smarty->assign('results', $results);

            $return['results'] = $smarty->fetch("ci/ajax.reporttemplatedetail.tpl");
            //$return['no_data'] = (count($results['children']) == 0);

            return_json($return);
            break;

        case 'edit_temp':
            $db->begin_transaction();
            if(!isset($_POST['report_template_id'])) {
                _error(404);
            }
            $args = array();
            $args['report_template_id'] = $_POST['report_template_id'];
            $args['template_name'] = trim($_POST['template_name']);
            $args['level'] = CLASS_LEVEL;
            $args['class_id'] = $class['group_id'];
            $args['class_level_id'] = 0;
            $args['school_id'] = $class['school_id'];

            // 1. Update bảng ci_report_template
            $reportDao->updateReportTemplate($args);

            // 2. Update bảng ci_report_template_detail
            // Xóa toàn bộ chi tiết mẫu cũ
            $reportDao->deleteReportTemplateDetail($_POST['report_template_id']);

            // Lấy dữ liệu mới
            $categoryIds = isset($_POST['category_ids'])?$_POST['category_ids']:array();

            // Lặp mảng $categoryIds lấy nội dung tương ứng
            $contents = array();
            foreach ($categoryIds as $id) {
                $contents[] = $_POST['content_'.$id];
            }

            // Insert vào bảng ci_report_template_detail
            $reportDao->insertReportTemplateDetail($_POST['report_template_id'], $categoryIds, $contents);

            $db->commit();
            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/class/' . $_POST['username'] . '/reports/listtemp";'));
            break;
        case 'add_temp':
            $db->begin_transaction();
            $args = array();
            $args['template_name'] = trim($_POST['template_name']);
            $args['class_id'] = $class['group_id'];
            $args['school_id'] = $class['school_id'];
            $args['class_level_id'] = 0;
            $args['level'] = CLASS_LEVEL;

            // 1. Insert vào bảng ci_report_template
            $reportTemplateId = $reportDao->insertReportTemplate($args);

            $categoryIds = isset($_POST['category_ids'])?$_POST['category_ids']:array();
            // Lặp mảng $categoryIds lấy nội dung tương ứng
            $contents = array();
            foreach ($categoryIds as $id) {
                $contents[] = $_POST['content_'.$id];
            }

            // 2. Insert vào bảng ci_report_template_detail
            $reportDao->insertReportTemplateDetail($reportTemplateId, $categoryIds, $contents);

            $db->commit();
            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/class/' . $_POST['username'] . '/reports/listtemp";'));
            break;
        case 'cate_detail':
            if(!isset($_POST['report_template_category_id']) || !is_numeric($_POST['report_template_category_id'])) {
                _error(404);
            }

            $results = $reportDao->getReportTemplateCategoryDetail($_POST['report_template_category_id']);
            $smarty->assign('results', $results);

            $return['results'] = $smarty->fetch("ci/class/ajax.class.categorydetail.tpl");
            //$return['no_data'] = (count($results['children']) == 0);

            return_json($return);
            break;
        case 'delete_report':
            $db->begin_transaction();
            if(!$_POST['id'] || !is_numeric($_POST['id'])) {
                _error(404);
            }
            $reportDao->deleteReport($_POST['id']);

            $db->commit();
            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/class/' . $_POST['username'] . '/reports";'));
            break;

        case 'delete_template':
            $db->begin_transaction();
            if(!$_POST['id'] || !is_numeric($_POST['id'])) {
                _error(404);
            }
            // Xóa trong bảng ci_report_template
            $reportDao->deleteReportTemplate($_POST['id']);

            // Xóa trong bảng ci_report_template_detail
            $reportDao->deleteReportTemplateDetail($_POST['id']);

            $db->commit();
            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/class/' . $_POST['username'] . '/reports/listtemp";'));
            break;

        case 'child_search':
            /*if(!isset($_POST['child_id']) || !is_numeric($_POST['child_id'])) {
                _error(404);
            }*/
            if (is_numeric($_POST['child_id'])) {
                $results = $reportDao->getClassReportOfChild($class['group_id'], $_POST['child_id']);
            } else {
                $results = $reportDao->getClassReport($class['group_id']);
            }

            $countNotNotify = 0;
            foreach ($results as $row) {
                if(!$row['is_notified']) {
                    $countNotNotify = $countNotNotify + 1;
                }
            }
            $smarty->assign('countNotNotify', $countNotNotify);

            $smarty->assign('results', $results);
            $return['results'] = $smarty->fetch("ci/class/ajax.reportchild.tpl");
            $return['no_data'] = (count($results) == 0);
            return_json($return);
            break;
        case 'delete_all_select':
            $db->begin_transaction();
            $ids = isset($_POST['ids'])? $_POST['ids']: array();
            if(count($ids) > 0) {
                foreach ($ids as $id) {
                    $reportDao->deleteReport($id);
                }
            }
            $db->commit();
            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/class/' . $_POST['username'] . '/reports";'));
            break;

        case 'notify_all_select':
            $db->begin_transaction();
            $ids = isset($_POST['ids'])? $_POST['ids']: array();

            if(count($ids) > 0) {
                foreach ($ids as $id) {
                    $report = $reportDao->getReportById($id);
                    //$parents = $childDao->getParent($child['child_id']);
                    $child = getChildData($report['child_id'], CHILD_INFO);
                    $parents = getChildData($report['child_id'], CHILD_PARENTS);
                    if (!is_null($parents)) {
                        $parentIds = array_keys($parents);
                        //thông báo về cho phụ huynh
                        $userDao->postNotifications($parentIds, NOTIFICATION_NEW_REPORT, NOTIFICATION_NODE_TYPE_CHILD,
                            $id, convertText4Web($report['report_name']), $child['child_id'], convertText4Web($child['child_name']));
                    }
                }
            }

            // Đổi trạng thái thành đã thông báo
            $reportDao->updateNotifiedForReports($ids);

            $db->commit();
            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/class/' . $_POST['username'] . '/reports";'));
            break;

        default:
            _error(400);
            break;
    }
}   catch (Exception $e) {
    $db->rollback();
    return_json( array('error' => true, 'message' => $e->getMessage()) );
} finally {
    $db->autocommit(true);
}

?>