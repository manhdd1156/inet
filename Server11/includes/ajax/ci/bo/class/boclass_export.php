<?php
/**
 * Created by PhpStorm.
 * User: TaiLA
 * Date: 5/9/17
 * Time: 11:21 PM
 */

/**
 * Package: ajax/ci/bo/class
 *
 * @package ConIu
 * @author QuanND
 */

// fetch bootstrap
require('../../../../../bootstrap.php');
// check AJAX Request
is_ajax();
check_login();

// user access
if(!$system['system_public']) {
    user_access();
}
// initialize the return array
$return = array();
$return['callback'] = 'window.location.replace(response.path);';

// check username
if(is_empty($_POST['username']) || !valid_username($_POST['username'])) {
    _error(404);
}

include_once(DAO_PATH.'dao_school.php');
include_once(DAO_PATH.'dao_class.php');
include_once(DAO_PATH.'dao_child.php');
include_once(DAO_PATH.'dao_parent.php');
include_once(DAO_PATH.'dao_user.php');
include_once(DAO_PATH.'dao_service.php');
include_once(DAO_PATH.'dao_teacher.php');
$schoolDao = new SchoolDAO();
$classDao = new ClassDAO();
$childDao = new ChildDAO();
$parentDao = new ParentDAO();
$userDao = new UserDAO();
$serviceDao = new ServiceDAO();
$teacherDao = new TeacherDAO();

//Lấy ra thông tin lớp
//$class = $classDao->getClassByUsername($_POST['username'], $user->_data['user_id']);
$class = getClassDataByUsername($_POST['username'], CLASS_INFO);
if (is_null($class)) {
    _error(403);
}

$smarty->assign('username', $_POST['username']);
//$school = $schoolDao->getSchoolByClass($class['group_id']);
$school = getSchoolData($class['school_id'], SCHOOL_INFO);
try {
    $return = array();
    $db->autocommit(false);
    switch ($_POST['do']) {
        case 'export_newfeed':
            if ($_POST['begin'] == "" || $_POST['end'] == "") {
                _error(400);
            }

            $begin = $_POST['begin'];
            $begin_db = toDBDate($_POST['begin']);
            $end = $_POST['end'];
            $end_db = toDBDate($_POST['end']);
            $begin_str = strtotime($begin_db);
            $end_str = strtotime($end_db);
            // include PHPExcel class
            include_once(ABSPATH . 'includes/libs/PHPExcel/PHPExcel/IOFactory.php');
            include_once(ABSPATH . 'includes/libs/PHPExcel/PHPExcel.php');
            $objPHPExcel = new PHPExcel();
            $objPHPExcel = PHPExcel_IOFactory::load("../../../../../" . "/content/templates/newfeed.xlsx");
            /* get photos */
            $group['photos'] = $user->get_photos($class['group_id'], 'group');
            /* get pinned post */
            $pinned_post = $user->get_post($class['group_pinned_post']);
            $smarty->assign('pinned_post', $pinned_post);
            /* get posts */
            $posts = $user->get_posts(array('get' => 'posts_group', 'id' => $class['group_id']));

            // Lấy những post theo ngày được chọn
            $postSelect = array();
            foreach ($posts as $row) {
                $time = toSysDate($row['time']);
                $time = toDBDate($time);
                $time = strtotime($time);
                if($time >= $begin_str && $time <= $end_str) {
                    $postSelect[] = $row;
                }
            }

            // Lấy những post của giáo viên post
            // Lấy danh sách giáo viên
            $teachers = array();
            //$teachers = $teacherDao->getTeacherOfClass($class['group_id']);
            $teachers = getClassData($class['group_id'], CLASS_TEACHERS);
            $teacherIds = array_keys($teachers);

            if(count($postSelect) <= 0) {
                _error(400);
            }
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'BÁO CÁO HOẠT ĐỘNG CỦA LỚP: ' . mb_strtoupper(convertText4Web($class['group_title']), 'UTF-8'));
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A2', convertText4Web($school['page_title']));
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A3', 'Từ ngày: ' . $begin .'     '. 'Đến ngày: ' . $end);
            // Lặp lấy thông tin post
            $i = 5;
            $postLast = array();
            foreach ($postSelect as $k => $post) {
                if(($post['post_type'] == 'photos' || $post['post_type'] == 'album' || is_empty($post['post_type'])) && in_array($post['author_id'],$teacherIds)){
                    $postLast[] = $post;
                }
            }
            foreach ($postLast as $k => $post) {
                if($k <= 100) {
                    $objPHPExcel->getActiveSheet()->getRowDimension($i)->setRowHeight(20);
                    $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A' . $i . ':I' . $i);
                    $objPHPExcel->getActiveSheet()->getStyle('A' . $i)
                        ->applyFromArray(
                            styleArray('Times new Roman', true, false, 12,
                                PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                                PHPExcel_Style_Alignment::VERTICAL_CENTER, true
                            )
                        );
                    if($post['post_type'] == 'album') {
                        $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue('A' . $i, $k + 1 . ' - ' . convertText4Web($post['post_author_name']) . ' đã thêm ảnh vào album: ' .  convertText4Web($post['album']['title']));
                    } else {
                        $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue('A' . $i, $k + 1 . ' - ' . convertText4Web($post['post_author_name']) . ' đã thêm bài viết vào: ' .  convertText4Web($class['group_title']));
                    }
                    $i++;
                    $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A' . $i . ':I' . $i);
                    $objPHPExcel->getActiveSheet()->getStyle('A' . $i)
                        ->applyFromArray(
                            styleArray('Times new Roman', false, false, 12,
                                PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                                PHPExcel_Style_Alignment::VERTICAL_TOP, true
                            )
                        );
                    $height = getHeightCell(convertText4Web($post['text_plain']));
                    $objPHPExcel->getActiveSheet()->getRowDimension($i)->setRowHeight($height);
                    $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A' . $i, convertText4Web($post['text_plain']));
                    $i++;
                    $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A' . $i . ':C' . $i);
                    $objPHPExcel->setActiveSheetIndex(0)->mergeCells('D' . $i . ':F' . $i);
                    $objPHPExcel->setActiveSheetIndex(0)->mergeCells('G' . $i . ':I' . $i);
                    $objPHPExcel->getActiveSheet()->getStyle('A' . $i . ':G' . $i)
                        ->applyFromArray(
                            styleArray('Times new Roman', false, false, 12,
                                PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                                PHPExcel_Style_Alignment::VERTICAL_TOP, true
                            )
                        );
                    if(isset($post['photos']) && $post['photos_num'] > 0) {
                        foreach ($post['photos'] as $p => $photo) {
                            if ($p <= 2) {
                                $objDrawing = new PHPExcel_Worksheet_Drawing();
                                $objDrawing->setName('Thumb');
                                $objDrawing->setDescription('Thumbnail Image');
                                $path = '../../../../../' . 'content/uploads/' . $photo['source'];
                                $objDrawing->setPath($path);
                                $objDrawing->setOffsetX(10);                       //setOffsetX works properly
                                $objDrawing->setOffsetY(10);                       //setOffsetY works properly
                                $objDrawing->setWidth(120);                 //set width, height
                                $objDrawing->setHeight(120);

                                if ($p == 0) {
                                    $objDrawing->setCoordinates('A' . $i);
                                } elseif ($p == 1) {
                                    $objDrawing->setCoordinates('D' . $i);
                                } else {
                                    $objDrawing->setCoordinates('G' . $i);
                                }
                                $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
                                $objPHPExcel->getActiveSheet()->getRowDimension($i)->setRowHeight(105);
                            }
                        }
                    } else {
                        //Xóa dòng trống lưu ảnh, giảm I đi 1 giá trị
                        $objPHPExcel->getActiveSheet()->getRowDimension($i)->setRowHeight(0);
                    }
                    $i = $i + 2;
                }
            }

            // Lưu file và download về client
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            ob_start();
            $file_name = 'Baocao' . $class['group_title'] . '_' . $school['page_title'] . '.xlsx';
            $file_name = convertText4Web($file_name);
            $file_name = convert_vi_to_en($file_name);
            $file_name = str_replace(" ",'_', $file_name);

            $objWriter->save("php://output");

            $xlsData = ob_get_contents();
            ob_end_clean();

            return_json(array(
                'file_name' => $file_name,
                'file' => "data:application/vnd.ms-excel;base64,".base64_encode($xlsData)
            ));

            break;

        default:
            _error(400);
            break;
    }
} catch (Exception $e) {
    $db->rollback();
    return_json( array('error' => true, 'message' => $e->getMessage()) );
} finally {
    $db->autocommit(true);
}

?>