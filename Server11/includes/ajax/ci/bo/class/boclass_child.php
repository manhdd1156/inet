<?php
/**
 * Package: ajax/ci/bo
 * 
 * @package ConIu
 * @author QuanND
 */

// fetch bootstrap
require('../../../../../bootstrap.php');
require(ABSPATH.'includes/class-image.php');

// check AJAX Request
is_ajax();
check_login();

// initialize the return array
$return = array();
$return['callback'] = 'window.location.replace(response.path);';

include_once(DAO_PATH . 'dao_child.php');
include_once(DAO_PATH . 'dao_parent.php');
include_once(DAO_PATH . 'dao_school.php');
include_once(DAO_PATH . 'dao_subject.php');
include_once(DAO_PATH . 'dao_class.php');
include_once(DAO_PATH . 'dao_teacher.php');
include_once(DAO_PATH . 'dao_user.php');
include_once(DAO_PATH . 'dao_mail.php');
include_once(DAO_PATH . 'dao_journal.php');

$journalDao = new JournalDAO();
$childDao = new ChildDAO();
$parentDao = new ParentDAO();
$schoolDao = new SchoolDAO();
$classDao = new ClassDAO();
$subjectDao = new SubjectDAO();
$teacherDao = new TeacherDAO();
$userDao = new UserDAO();
$mailDao = new MailDAO();

//Lấy ra thông tin lớp
//$class = $classDao->getClassByUsername($_POST['username'], $user->_data['user_id']);
$class = getClassDataByUsername($_POST['username'], CLASS_INFO);

// Lấy cấu hình thông báo của trường
$notify_settings = getSchoolData($class['school_id'], SCHOOL_NOTIFICATIONS);
if (is_null($class)) {
    _error(403);
}

try {
    $args = array();
    $args['user_id'] = $user->_data['user_id'];
    $args['child_id'] = $_POST['child_id'];
    $args['child_parent_id'] = $_POST['child_parent_id'];
    $args['child_code'] = isset($_POST['child_code'])? trim($_POST['child_code']): "";
    $args['first_name'] = trim($_POST['first_name']);
    $args['last_name'] = trim($_POST['last_name']);
    $args['child_name'] = $args['last_name']." ".$args['first_name'];
    $args['name_for_sort'] = convert_to_en_4sort($args['first_name'].$args['last_name']);
    $args['description'] = isset($_POST['description'])? trim($_POST['description']) : "";
    $args['gender'] = $_POST['gender'];
    $args['parent_phone'] = standardizePhone($_POST['parent_phone']);
    $args['parent_phone_dad'] = standardizePhone($_POST['parent_phone_dad']);
    /*if (!validatePhone($args['parent_phone'])) {
        return_json( array('error' => true, 'message' => __("The phone number you entered is incorrect")) );
    }*/
    $args['parent_email'] = isset($_POST['parent_email'])? trim($_POST['parent_email']) : "";
    $args['parent_name'] = (isset($_POST['parent_name']) && $_POST['parent_name'] != '')? trim($_POST['parent_name']) : '';
    $args['parent_name_dad'] = (isset($_POST['parent_name_dad']) && $_POST['parent_name_dad'] != '')? trim($_POST['parent_name_dad']) : '';

    // Đoạn dưới xử lý phụ huynh nào sẽ dùng để tạo tài khoản cho phụ huynh
    if($args['parent_name'] == '' && $args['parent_name_dad'] == '') {
        $args['parent_name'] = __("Parent of")." ".$args['child_name'];
        $args['parent_name_creat_account'] = $args['parent_name'];
        $args['parent_phone_creat_account'] = $args['parent_phone'];
    } else {
        if($args['parent_name'] != '') {
            $args['parent_name_creat_account'] = $args['parent_name'];
            $args['parent_phone_creat_account'] = $args['parent_phone'];
        } else {
            $args['parent_name_creat_account'] = $args['parent_name_dad'];
            $args['parent_phone_creat_account'] = $args['parent_phone_dad'];
        }
    }
    $args['parent_job'] = trim($_POST['parent_job']);
    $args['parent_job_dad'] = trim($_POST['parent_job_dad']);

    $args['address'] = isset($_POST['address'])? trim($_POST['address']) : "";
    $args['birthday'] = isset($_POST['birthday'])? trim($_POST['birthday']) : "";
    $args['begin_at'] = $_POST['begin_at'];
    $args['school_id'] = $class['school_id'];
    $args['class_id'] = $class['group_id'];

    $db->autocommit(false);

    switch ($_POST['do']) {
        case 'edit':
            /**
             * Hàm này xử lý khi giáo viên edit thông tin trẻ
             */

            if(isset($_POST['begin_at']) && !validateDate($_POST['begin_at'])) {
                throw new Exception(__("You must enter study start date"));
            }
            $birthday = toDBDate($args['birthday']);
            $beginAt = toDBDate($args['begin_at']);
            if(strtotime($birthday) > strtotime($beginAt)) {
                throw new Exception(__("Begin at not before birthdate"));
            }

            //Validate điều kiện tạo tài khoản phụ huynh
            $create_parent_account = $_POST['create_parent_account'];
            if ($create_parent_account && !is_empty($args['parent_email']) && !valid_email($args['parent_email'])) {
                throw new Exception(__("To create parent account, you must enter correct parent email."));
            }

            // Lấy thông tin trường
            //$school = $schoolDao->getSchoolById($class['school_id']);
            $school = getSchoolData($class['school_id'], SCHOOL_DATA);

            $oldChild = getChildData($_POST['child_id'], CHILD_INFO);
            if (is_null($oldChild)) {
                _error(404);
            }

            // Lấy thông tin trẻ
            $child = $childDao->getChildByParent($_POST['child_parent_id']);
            if (is_null($child)) {
                _error(404);
            }
            $db->begin_transaction();

            //1. Cập nhật thông tin trẻ trong bảng ci_child
            $args['child_id'] = $_POST['child_id'];
            $childDao->editChild($args);

            // Nếu trẻ đang đi học ở trường thì mới cập nhật thông tin trong bảng ci_child_parent
            if($oldChild['status']) {
                // 1.1 Cập nhật thông tin trẻ trong bảng ci_child_parent
                $args['child_parent_id'] = $_POST['child_parent_id'];
                $childDao->editChildBySchool($args);
            }

            $class_id = $class['group_id'];

            //3. Cập nhật thông tin trẻ trong trường (cập nhật bảng ci_school_child)
            $argsCC['begin_at'] = isset($_POST['begin_at'])? trim($_POST['begin_at']) : "";
            $argsCC['class_id'] = $class_id;
            $argsCC['child_id'] = $_POST['child_id'];
            $argsCC['school_id'] = $school['page_id'];

            $childDao->updateSchoolChild($argsCC);

            // Lấy danh sách phụ huynh
            $parents = getChildData($_POST['child_id'], CHILD_PARENTS);
            $oldParentIds = array_keys($parents);
            $newParentIds = (isset($_POST['user_id']) && (count($_POST['user_id']) > 0))? $_POST['user_id']: array();

            // bỏ những user trùng khi gửi lên
            $newParentIds = array_unique($newParentIds);

            // Xóa danh sách cha mẹ cũ đi
            $deletedParentIds = array_diff($oldParentIds, $newParentIds);
            if (count($deletedParentIds) > 0) {
                // Không được xóa phụ huynh admin của trẻ
                if(in_array($child['child_admin'], $deletedParentIds)) {
                    throw new Exception(__("You do not have the right to remove a parent who administers a student"));
                }
                if($oldChild['status']) {
                    //Xóa cha mẹ khỏi lớp cũ nếu trẻ đang đi học
                    $classDao->deleteUserFromClass($oldChild['class_id'], $deletedParentIds);
                }
                //Xóa danh sách cha mẹ trong danh sách đc quản lý (bảng ci_user_manage)
                $parentDao->deleteParentList($_POST['child_id'], $deletedParentIds);

                // Nếu trẻ đang đi học thì mới cập nhật thông tin phụ huynh trong bảng ci_parent_manage
                if($oldChild['status']) {
                    // Xóa danh sách cha mẹ trong bảng ci_parent_manage
                    $parentDao->deleteParentListByParent($_POST['child_parent_id'], $deletedParentIds);
                }
                if($oldChild['status']) {
                    //Xóa danh sách cha mẹ like page trường (nếu trẻ đang học tại trường)
                    $schoolDao->deleteUserLikeSchool($school['page_id'], $deletedParentIds); //Xem xét có thể để cha mẹ like page của trường, ko cần thiết phải xóa
                }
            }

            //3.1. Tự động tạo tài khoản phụ huynh
            if ($create_parent_account) {
                $argsParent = array();
                $argsParent['full_name'] = $args['parent_name_creat_account'];
                $argsParent['first_name'] = split_name($argsParent['full_name'])[0];
                $argsParent['last_name'] = split_name($argsParent['full_name'])[1];
                $argsParent['username'] = generateUsername($argsParent['full_name']);
                $argsParent['user_phone'] = standardizePhone($args['parent_phone_creat_account']);
                /*if (!validatePhone($args['user_phone'])) {
                    return_json( array('error' => true, 'message' => __("The phone number you entered is incorrect")) );
                }*/
                $argsParent['email'] = ($args['parent_email'] != '') ? $args['parent_email'] : 'null';
                $argsParent['password'] = (is_empty($argsParent['user_phone'])? "coniu.vn": $argsParent['user_phone']);
                $argsParent['gender'] = FEMALE;

                $parentId = $userDao->createUser($argsParent);
                $newParentIds[] = $parentId;
                // Set child_admin cho trẻ
                $childDao->setChildAdmin($_POST['child_parent_id'], $parentId);
                if($argsParent['email'] != 'null') {
                    //Gửi mail cho phụ huynh của trẻ.
                    $argEmail = array();
                    $argEmail['action'] = "create_parent_account";
                    $argEmail['receivers'] = $argsParent['email'];
                    $argEmail['subject'] = "[".convertText4Web($school['page_title'])."]".__("Use Inet application to connect student's parents");

                    //Tạo nên nội dung email
                    $smarty->assign('full_name', $argsParent['full_name']);
                    $smarty->assign('child_name', $args['child_name']);
                    $smarty->assign('username', $argsParent['email']);
                    $smarty->assign('password', $argsParent['password']);
                    $argEmail['content'] = $smarty->fetch("ci/email_templates/create_parent_account.tpl");

                    $argEmail['delete_after_sending'] = 1;
                    $argEmail['school_id'] = $school['page_id'];
                    $argEmail['user_id'] = $user->_data['user_id'];

                    $mailDao->insertEmail($argEmail);
                }
            }

            // Thêm danh sách phụ huynh mới
            $addedParentIds = array_diff($newParentIds, $oldParentIds);
            if (count($addedParentIds) > 0) {
                //3.2. Cho phụ huynh like các page của hệ thống, join các group của hệ thống
                $userDao->addUsersLikeSomePages($system_page_ids, $addedParentIds);
                $userDao->addUserToSomeGroups($system_group_ids, $addedParentIds);

                //4. Thêm cha mẹ vào nhóm của lớp (bao gồm việc tăng thành viên nhóm)
                $classDao->addUserToClass($class_id, $addedParentIds);

                //5. Thêm cha mẹ cho trẻ & người tạo ra thông tin trẻ vào danh sách người có thể quản lý
                $parentDao->addParentList($_POST['child_id'], $addedParentIds);

                // Nếu trẻ đang đi học tại trường thì mới cập nhật thông tin trong bảng ci_parent_manage
                if($oldChild['status']) {
                    //5.1 Thêm cha mẹ quản lý trẻ trong bảng ci_parent_manage
                    $childDao->createParentManageInfo($addedParentIds, $_POST['child_parent_id'], $_POST['child_id'], $school['page_id']);
                }
                //7. Trẻ đang đi học và chưa có phụ huynh thì cập nhật child_admin(ci_child_parent)
                if (count($oldParentIds) == 0 && $oldChild['status']) {
                    $childAdmin = $addedParentIds[0];
                    $childParentId = $oldChild['child_parent_id'];
                    $childDao->updateChildForParent($childParentId, $childAdmin);
                }
                //6. Cho cha mẹ like trang của trường (tăng số like)
                $likeCnt = $schoolDao->addUsersLikeSchool($school['page_id'], $addedParentIds);
                $school['page_likes'] = $school['page_likes'] + $likeCnt;
            }

            //Cập nhật số lượng học sinh theo giới tính của trường
            if ($oldChild['gender'] != $args['gender']) {
                //Giảm giới tính cũ
                $schoolDao->updateGenderCount($school['page_id'], $oldChild['gender'], -1);
                //Tăng giới tính mới
                $schoolDao->updateGenderCount($school['page_id'], $args['gender'], 1);

                //Cập nhật thông tin trường
                if ($oldChild['gender'] == MALE) {
                    $school['male_count'] = $school['male_count'] - 1;
                    $school['female_count'] = $school['female_count'] + 1;
                } else {
                    $school['female_count'] = $school['female_count'] - 1;
                    $school['male_count'] = $school['male_count'] + 1;
                }
            }

            // Thông báo cho phụ huynh biết trẻ được sửa
            $userDao->postNotifications($newParentIds, NOTIFICATION_UPDATE_CHILD_SCHOOL, NOTIFICATION_NODE_TYPE_CHILD,
                $args['child_parent_id'], convertText4Web($args['child_name']), $args['child_parent_id'], convertText4Web($school['page_title']));

            /* ---------- UPDATE - MEMCACHE ---------- */
            //1. Cập nhật thông tin của trường
            $info_update = array(
                'page_likes' => $school['page_likes']
            );
            $config_update = array(
                'male_count' => $school['male_count'],
                'female_count' => $school['female_count']
            );
            updateSchoolData($school['page_id'], SCHOOL_INFO, $info_update);
            updateSchoolData($school['page_id'], SCHOOL_CONFIG, $config_update);

            //2. Cập nhật thông tin lớp
            if ($class_id > 0) {
                updateClassData($class_id, CLASS_INFO);
            }
            //3.Cập nhật thông tin trẻ
            updateChildData($_POST['child_id'], CHILD_INFO);
            updateChildData($_POST['child_id'], CHILD_PARENTS);

            //4. Cập nhật tin tin quản lý trẻ
            foreach ($addedParentIds as $parentId) {
                updateUserManageData($parentId, USER_MANAGE_CHILDREN);
            }
            foreach ($deletedParentIds as $parentId) {
                updateUserManageData($parentId, USER_MANAGE_CHILDREN);
            }
            /* ---------- END - MEMCACHE ---------- */

            // Check xem trẻ đã được sửa trước đó (có trong bảng ci_child_edit_history) chưa
            $issetEditHistory = $childDao->checkEditHistory($_POST['child_id'], $school['page_id']);

            $argc = array();
            $argc['edit_by'] = EDIT_BY_TEACHER;
            $argc['child_id'] = $_POST['child_id'];
            $argc['school_id'] = $school['page_id'];

            if($issetEditHistory) {
                // Cập nhật
                $childDao->updateChildEditHistory($argc);
            } else {
                // Thêm trẻ vào bảng ci_child_edit_history
                $childDao->insertChildEditHistory($argc);
            }
            $db->commit();
            return_json( array('success' => true, 'message' => __("Done, Child info have been updated")) );
            break;

        case 'add':
            //$school = $schoolDao->getSchoolByClass($class['group_id']);
            $school = getSchoolData($class['school_id'], SCHOOL_DATA);
            /**
             * Hàm này xử lý khi trường tạo ra thông tin một trẻ. Công việc bao gồm
             * 1. Tạo thông tin trẻ trong bảng ci_child
             * 2. Cập nhật danh sách lớp (đưa trẻ vào lớp - bảng ci_class_child)
             * 2.2 nhập thông tin của trẻ vào bảng điểm ci_point_
             * 2.3 Nhập thông tin của trẻ vào bảng hạnh kiểm ci_conduct
             * 3. Cập nhật thông tin trẻ trong trường (cập nhật bảng ci_school_child)
             * 3.5. Tự động tạo tài khoản phụ huynh
             * 4. Thêm cha mẹ vào nhóm của lớp (bao gồm việc tăng
             * 5. Thêm cha mẹ cho trẻ
             * 6. Cho cha mẹ like trang của trường
             * 7. Cho cha mẹ thành bạn của giáo viên (bao gồm cả quản lý trường)
             * 8. Tăng số lượng trẻ của trường
             */

            if(isset($_POST['begin_at']) && !validateDate($_POST['begin_at'])) {
                throw new Exception(__("You must enter study start date"));
            }
            $birthday = toDBDate($args['birthday']);
            $beginAt = toDBDate($args['begin_at']);
            if(strtotime($birthday) > strtotime($beginAt)) {
                throw new Exception(__("Begin at not before birthdate"));
            }

            //Validate điều kiện tạo tài khoản phụ huynh
            $create_parent_account = $_POST['create_parent_account'];
            if ($create_parent_account && !is_empty($args['parent_email']) && !valid_email($args['parent_email'])) {
                throw new Exception(__("To create parent account, you must enter correct parent email or parent email is empty."));
            }
            $class_id = is_numeric($_POST['class_id']) ? $_POST['class_id'] : 0;
            $db->begin_transaction();

            $args['code_auto'] = isset($_POST['code_auto'])? $_POST['code_auto']: 0;
            //Sinh mã trẻ nếu cần
            if ($args['code_auto']) {
                $args['child_code'] = generateCode($args['child_name']);
            }
            $childInfo = $childDao->getStatusChildInSchool($args['child_code'], $school['page_id']);
            // Nếu trẻ chưa có trong hệ thống thì tạo mới thông tin trẻ trong trường
            if ($childInfo['status'] == 0) {
                $args['child_parent_id'] = 0;

                //1. Tạo thông tin trẻ trong bảng ci_child
                $lastId = $childDao->createChild($args);

                //2. Cập nhật danh sách lớp (đưa trẻ vào lớp - bảng ci_class_child)
                // Nếu ko chọn class thì thêm trẻ vào trường, bỏ qua bước này
                $argsCC['class_id'] = $class_id;
                $argsCC['begin_at'] = isset($_POST['begin_at']) ? trim($_POST['begin_at']) : "";
                $argsCC['child_id'] = $lastId;
                if ($class_id > 0) {
                    $childDao->addChildToClass($argsCC);
                    //2.2 nhập thông tin của trẻ vào bảng điểm ci_point_
                    $subjectOfClass = $subjectDao->getSubjectsByClassId($class_id,$school['page_id']);
                    if(count($subjectOfClass)>0) {
                        foreach($subjectOfClass as $subject) {
                            $childDao->addChildPoints($subject['subject_id'],$args['child_code'],$class_id,$subject['school_year']);
                        }
                    }
                    //2.3 Nhập thông tin của trẻ vào bảng hạnh kiểm ci_conduct
                    $school_year = date("Y", strtotime(str_replace('/', '-', $_POST['begin_at']))).'-'.(date("Y", strtotime(str_replace('/', '-', $_POST['begin_at'])))+1);
                    $childDao->addChildConduct($class_id,$argsCC['child_id'],$school_year);
                }

                //3. Cập nhật thông tin trẻ trong trường (cập nhật bảng ci_school_child)
                $argsCC['school_id'] = $class['school_id'];
                $argsCC['child_parent_id'] = 0;
                $childDao->addChildToSchool($argsCC);

                $parentIds = (isset($_POST['user_id']) ? $_POST['user_id'] : array());
                // bỏ những user trùng khi gửi lên
                $parentIds = array_unique($parentIds);
                //3.1. Tự động tạo tài khoản phụ huynh
                if ($create_parent_account) {
                    $argsParent = array();
                    $argsParent['full_name'] = $args['parent_name_creat_account'];
                    $argsParent['first_name'] = split_name($argsParent['full_name'])[0];
                    $argsParent['last_name'] = split_name($argsParent['full_name'])[1];
                    $argsParent['username'] = generateUsername($argsParent['full_name']);
                    $argsParent['user_phone'] = standardizePhone($args['parent_phone_creat_account']);
                    /*if (!validatePhone($args['user_phone'])) {
                        return_json( array('error' => true, 'message' => __("The phone number you entered is incorrect")) );
                    }*/
                    $argsParent['email'] = ($args['parent_email'] != '') ? $args['parent_email'] : 'null';
                    $argsParent['password'] = (is_empty($argsParent['user_phone']) ? "coniu.vn" : $argsParent['user_phone']);
                    $argsParent['gender'] = FEMALE;

                    $parentId = $userDao->createUser($argsParent);
                    $parentIds[] = $parentId;

                    if($argsParent['email'] != 'null') {
                        //Gửi mail cho phụ huynh của trẻ.
                        $argEmail = array();
                        $argEmail['action'] = "create_parent_account";
                        $argEmail['receivers'] = $argsParent['email'];
                        $argEmail['subject'] = "[" . convertText4Web($school['page_title']) . "]" . __("Use Inet application to connect student's parents");

                        //Tạo nên nội dung email
                        $smarty->assign('full_name', $argsParent['full_name']);
                        $smarty->assign('child_name', $args['child_name']);
                        $smarty->assign('username', $argsParent['email']);
                        $smarty->assign('password', $argsParent['password']);
                        $argEmail['content'] = $smarty->fetch("ci/email_templates/create_parent_account.tpl");

                        $argEmail['delete_after_sending'] = 1;
                        $argEmail['school_id'] = $class['school_id'];
                        $argEmail['user_id'] = $user->_data['user_id'];

                        $mailDao->insertEmail($argEmail);
                    }
                }
                if (count($parentIds) > 0) {
                    //3.2. Cho phụ huynh like các page của hệ thống, join các group của hệ thống
                    $userDao->addUsersLikeSomePages($system_page_ids, $parentIds);
                    $userDao->addUserToSomeGroups($system_group_ids, $parentIds);

                    //4. Thêm cha mẹ vào nhóm của lớp (bao gồm việc tăng thành viên nhóm)
                    // Nếu chưa chọn lớp thì bỏ qua
                    if ($class_id > 0) {
                        $classDao->addUserToClass($class_id, $parentIds);
                    }

                    //5. Thêm cha mẹ cho trẻ
                    $parentDao->addParentList($lastId, $parentIds);

                    //6. Cho cha mẹ like trang của trường (tăng số like)
                    $likeCnt = $schoolDao->addUsersLikeSchool($class['school_id'], $parentIds);
                    $school['page_likes'] = $school['page_likes'] + $likeCnt;

                    //7. Cho cha mẹ thành bạn của giáo viên (bao gồm cả quản lý trường)
                    // Nếu chưa chọn lớp thì bỏ qua
                    // Tạm thời không cho kết bạn giữa giáo viên, phụ huynh
                    /*if ($class_id > 0) {
                        $teacherIds = $teacherDao->getTeacherIDOfClass($class_id);
                    }
                    $teacherIds[] = $user->_data['user_id']; //User hiện tại là quản lý của trường

                    $userDao->becomeFriends(array_unique($teacherIds), $parentIds);*/

                    //7. Tạo thông tin trẻ cho phụ huynh
                    $args['child_id'] = $lastId;
                    $args['child_admin'] = $parentIds[0];
                    $args['school_id'] = $school['page_id'];
                    $childParentId = $childDao->createChildForParent($args);

                    //7.1. Gắn trẻ được quản lý cho phụ huynh
                    $childDao->createParentManageInfo($parentIds, $childParentId, $lastId, $school['page_id']);
                } else {
                    //7. Tạo thông tin trẻ cho phụ huynh (ci_child_parent)
                    $args['child_id'] = $lastId;
                    $child['child_admin'] = 0;
                    $args['school_id'] = $school['page_id'];
                    $childParentId = $childDao->createChildForParent($args);
                }
                //8. Tăng số lượng trẻ của trường
                $schoolDao->updateGenderCount($class['school_id'], $args['gender'], 1);
                if ($args['gender'] == MALE) {
                    $school['male_count'] = $school['male_count'] + 1;
                } else {
                    $school['female_count'] = $school['female_count'] + 1;
                }

                // Thông báo cho quản lý trường biết
//                $userManagerIds = getUserIdsManagerReceiveNotify($class['school_id'], 'children', $school['page_admin']);
                $userManagerIds = getUserIdsReceiveNotifyOfSchoolForClass($notify_settings, 'children', $class['school_id'], $school['page_admin']);
                $userDao->postNotifications($userManagerIds, NOTIFICATION_NEW_CHILD_TEACHER, NOTIFICATION_NODE_TYPE_SCHOOL,
                    $args['child_id'], convertText4Web($args['child_name']), $school['page_name'], convertText4Web($class['group_title']));
            } else {
                throw new Exception(__("Student is existing in system, check student code"));
            }

            $argc = array();
            $argc['edit_by'] = EDIT_BY_TEACHER;
            $argc['child_id'] = $lastId;
            $argc['school_id'] = $school['page_id'];

            // Thêm trẻ vào bảng ci_child_edit_history
            $childDao->insertChildEditHistory($argc);
            $db->commit();

            /* ---------- UPDATE - MEMCACHE ---------- */
            //1. Cập nhật thông tin của trường
            $info_update = array(
                'page_likes' => $school['page_likes']
            );
            $config_update = array(
                'male_count' => $school['male_count'],
                'female_count' => $school['female_count']
            );

            updateSchoolData($school['page_id'], SCHOOL_INFO, $info_update);
            updateSchoolData($school['page_id'], SCHOOL_CONFIG, $config_update);
            addSchoolData($school['page_id'], SCHOOL_CHILDREN, $lastId);

            //2. Cập nhật thông tin trẻ và lớp
            addClassData($class_id, CLASS_CHILDREN, $lastId);
            updateClassData($class_id, CLASS_INFO);

            //4. Cập nhật tin tin quản lý trẻ
            foreach ($parentIds as $parentId) {
                updateUserManageData($parentId, USER_MANAGE_CHILDREN);
            }
            /* ---------- END - MEMCACHE ---------- */


            return_json( array('success' => true, 'message' => __("Done, Child info has been created")) );
            break;

        case 'import':
            // Get school
            //$school = $schoolDao->getSchoolByClass($class['group_id']);
            $school = getSchoolData($class['school_id'], SCHOOL_DATA);

            include_once(ABSPATH . 'includes/libs/PHPExcel/PHPExcel/IOFactory.php');
            include_once(ABSPATH . 'includes/ajax/ci/utilities.php');

            $file_name = $_FILES['file']['name'];
            $inputFileName = $_FILES['file']['tmp_name'];

            $results = array();
            $newChildIds = array();
            $new_child_list = array();
            //Lấy ra class_id của lớp
            $class_id = $_POST['class_id'];
            $sheet = readChildInfoInExcelFile($inputFileName);

            if ($sheet['error'] == 0) {
                foreach ($sheet['child_list'] as $child) { //Duyệt danh sách trẻ
                    if ($child['error'] == 0) {
                        try {
                            if ($childDao->isCreatedInClass($class_id, $child['name_for_sort'], $child['parent_email'], $child['birthday'], $child['parent_phone'])) {
                                $child['error'] = 1;
                                $child['message'] = __("Student is existing in class");

                                $new_child_list[] = $child;
                                continue;
                            }

                            $db->autocommit(false);
                            $db->begin_transaction();

                            $childInfo = $childDao->getStatusChildInSchool($child['child_code'], $school['page_id']);
                            // Nếu trẻ chưa có trong hệ thống thì tạo mới thông tin trẻ trong trường
                            if ($childInfo['status'] == 0) {
                                $child['child_parent_id'] = 0;
                                //1. Tạo thông tin trẻ trong bảng ci_child
                                $lastId = $childDao->createChild($child);
                                // Danh sách trẻ đã được thêm
                                $newChildIds[] = $lastId;

                                //2. Cập nhật danh sách lớp (đưa trẻ vào lớp - bảng ci_class_child)
                                $child['class_id'] = $class_id;
                                $child['child_id'] = $lastId;
                                $childDao->addChildToClass($child);

                                // ADD START MANHDD 03/06/2021
                                //2.2 nhập thông tin của trẻ vào bảng điểm ci_point_
                                $subjectOfClass = $subjectDao->getSubjectsByClassId($class_id,$class['school_id']);
                                if(count($subjectOfClass)>0) {
                                    foreach($subjectOfClass as $subject) {
                                        $childDao->addChildPoints($subject['subject_id'],$child['child_code'],$class_id,$subject['school_year']);
                                    }
                                }
                                //2.3 Nhập thông tin của trẻ vào bảng hạnh kiểm ci_conduct
                                $school_year = date("Y", strtotime(str_replace('/', '-', $_POST['begin_at']))).'-'.(date("Y", strtotime(str_replace('/', '-', $_POST['begin_at'])))+1);
                                $childDao->addChildConduct($class_id,$child['child_id'],$school_year);
                                // ADD END MANHDD 03/06/2021

                                //3. Cập nhật thông tin trẻ trong trường (cập nhật bảng ci_school_child)
                                $child['school_id'] = $class['school_id'];
                                $childDao->addChildToSchool($child);

                                $parentIds = array();
                                //3.1. Tự động tạo tài khoản phụ huynh
                                if ($child['create_parent_account'] && is_empty($child['create_account_result'])) {
                                    //Trường hợp có chọn tạo tài khoản phụ huynh & định dạng email đúng.
                                    try {
                                        $argsParent = array();
                                        //$argsParent['full_name'] = __("Parent of") . " " . $child['child_name'];
                                        if($child['parent_name'] == '' && $child['parent_name_dad'] == '') {
                                            $argsParent['parent_name'] = __("Parent of")." ".$child['child_name'];
                                            $argsParent['parent_name_creat_account'] = $argsParent['parent_name'];
                                            $argsParent['parent_phone_creat_account'] = $child['parent_phone'];
                                        } else {
                                            if($child['parent_name'] != '') {
                                                $argsParent['parent_name_creat_account'] = $child['parent_name'];
                                                $argsParent['parent_phone_creat_account'] = $child['parent_phone'];
                                            } else {
                                                $argsParent['parent_name_creat_account'] = $child['parent_name_dad'];
                                                $argsParent['parent_phone_creat_account'] = $child['parent_phone_dad'];
                                            }
                                        }
                                        $argsParent['full_name'] = $argsParent['parent_name_creat_account'];
                                        $argsParent['first_name'] = split_name($argsParent['full_name'])[0];
                                        $argsParent['last_name'] = split_name($argsParent['full_name'])[1];
                                        $argsParent['username'] = generateUsername($argsParent['full_name']);
                                        $argsParent['user_phone'] = standardizePhone($argsParent['parent_phone_creat_account']);
                                        $argsParent['email'] = ($child['parent_email'] != '') ? $child['parent_email'] : 'null';
                                        $argsParent['password'] = (is_empty($argsParent['user_phone']) ? "coniu.vn" : $argsParent['user_phone']);
                                        $argsParent['gender'] = FEMALE;

                                        $parentId = $userDao->createUser($argsParent);
                                        $parentIds[] = $parentId;
                                        if($argsParent['email'] != 'null') {
                                            //Gửi mail cho phụ huynh của trẻ.
                                            $argEmail = array();
                                            $argEmail['action'] = "create_parent_account";
                                            $argEmail['receivers'] = $argsParent['email'];
                                            $argEmail['subject'] = "[" . convertText4Web($school['page_title']) . "]" . __("Use Inet application to connect student's parents");

                                            //Tạo nên nội dung email
                                            $smarty->assign('full_name', $argsParent['full_name']);
                                            $smarty->assign('child_name', $child['child_name']);
                                            $smarty->assign('username', $argsParent['email']);
                                            $smarty->assign('password', $argsParent['password']);
                                            $argEmail['content'] = $smarty->fetch("ci/email_templates/create_parent_account.tpl");

                                            $argEmail['delete_after_sending'] = 1;
                                            $argEmail['school_id'] = $class['school_id'];
                                            $argEmail['user_id'] = $user->_data['user_id'];
                                            $mailDao->insertEmail($argEmail);
                                        }
                                        $child['create_account_result'] = __("Create parent account successfully");
                                    } catch (Exception $e) {
                                        $child['create_account_result'] = __("Can not create parent account, parent email may be existing");
                                    }
                                } else if ($child['create_parent_account'] == 0) {
                                    $child['create_account_result'] = __("Not create parent account");
                                }
                                if (count($parentIds) > 0) {
                                    //3.2. Cho phụ huynh like các page của hệ thống, join các group của hệ thống
                                    $userDao->addUsersLikeSomePages($system_page_ids, $parentIds);
                                    $userDao->addUserToSomeGroups($system_group_ids, $parentIds);

                                    //4. Thêm cha mẹ vào nhóm của lớp (bao gồm việc tăng thành viên nhóm)
                                    $classDao->addUserToClass($class_id, $parentIds);

                                    //5. Thêm cha mẹ cho trẻ
                                    $parentDao->addParentList($lastId, $parentIds);

                                    //6. Cho cha mẹ like trang của trường (tăng số like)
                                    $likeCnt = $schoolDao->addUsersLikeSchool($class['school_id'], $parentIds);
                                    $school['page_likes'] = $school['page_likes'] + $likeCnt;

                                    //7. Cho cha mẹ thành bạn của giáo viên (bao gồm cả quản lý trường)
                                    // Tạm thời không cho kết bạn giữa giáo viên, phụ huynh
                                    /*$teacherIds = $teacherDao->getTeacherIDOfClass($class_id);
                                    $teacherIds[] = $user->_data['user_id']; //User hiện tại là quản lý của trường
                                    $userDao->becomeFriends($teacherIds, $parentIds);*/

                                    //7. Tạo thông tin trẻ cho phụ huynh (ci_child_parent)
                                    $child['child_admin'] = $parentIds[0];
                                    $childParentId = $childDao->createChildForParent($child);

                                    //7.1. Gắn trẻ được quản lý cho phụ huynh (ci_parent_manage)
                                    $childDao->createParentManageInfo($parentIds, $childParentId, $child['child_id'], $school['page_id']);
                                } else {
                                    //7. Tạo thông tin trẻ cho phụ huynh (ci_child_parent)
                                    $child['child_admin'] = 0;
                                    $childParentId = $childDao->createChildForParent($child);
                                }
                                //8. Tăng số lượng trẻ của trường
                                $schoolDao->updateGenderCount($class['school_id'], $child['gender'], 1);
                                if ($child['gender'] == MALE) {
                                    $school['male_count'] = $school['male_count'] + 1;
                                } else {
                                    $school['female_count'] = $school['female_count'] + 1;
                                }
                                $db->commit();

                            } else {
                                $child['error'] = 1;
                                $child['message'] = __("Student is existing in system, check student code");
                            }
                        } catch (Exception $e) {
                            $db->rollback();
                            $child['error'] = 1;
                            //$child['message'] = __("Can not create child");
                            $child['message'] = $e->getMessage();
                        } finally {
                            $db->autocommit(true);
                        }
                    }
                    $new_child_list[] = $child;
                }

                /* ---------- UPDATE - MEMCACHE ---------- */
                //1. Cập nhật thông tin của trường
                $info_update = array(
                    'page_likes' => $school['page_likes']
                );
                $config_update = array(
                    'male_count' => $school['male_count'],
                    'female_count' => $school['female_count']
                );
                updateSchoolData($school['page_id'], SCHOOL_INFO, $info_update);
                updateSchoolData($school['page_id'], SCHOOL_CONFIG, $config_update);
                updateSchoolData($school['page_id'], SCHOOL_CHILDREN);

                //2. Cập nhật thông tin lớp
                updateClassData($class_id, CLASS_INFO);
                updateClassData($class_id, CLASS_CHILDREN);

                //3. Cập nhật thông tin trẻ
                foreach ($newChildIds as $childId) {
                    updateChildData($childId);
                }

                /* ---------- END - MEMCACHE ---------- */
            }
            $sheet['child_list'] = $new_child_list;
            $smarty->assign("sheet", $sheet);
            $results['results'] = $smarty->fetch("ci/class/ajax.children.importresult.tpl");
            return_json($results);
            break;
        case 'addexisting':
            /**
             * Hàm này xử lý khi trường tạo ra thông tin một trẻ. Công việc bao gồm
             * 1. Tạo thông tin trẻ trong bảng ci_child
             * 2. Cập nhật danh sách lớp (đưa trẻ vào lớp - bảng ci_class_child)
             * 2.2 nhập thông tin của trẻ vào bảng điểm ci_point_
             * 2.3 Nhập thông tin của trẻ vào bảng hạnh kiểm ci_conduct
             * 3. Cập nhật thông tin trẻ trong trường (cập nhật bảng ci_school_child)
             * 3.5. Tự động tạo tài khoản phụ huynh
             * 4. Thêm cha mẹ vào nhóm của lớp (bao gồm việc tăng
             * 5. Thêm cha mẹ cho trẻ
             * 6. Cho cha mẹ like trang của trường
             * 7. Cho cha mẹ thành bạn của giáo viên (bao gồm cả quản lý trường)
             * 8. Tăng số lượng trẻ của trường
             */
            //Validate điều kiện tạo tài khoản phụ huynh
            $create_parent_account = $_POST['create_parent_account'];
            if ($create_parent_account && !is_empty($args['parent_email']) && !valid_email($args['parent_email'])) {
                throw new Exception(__("To create parent account, you must enter correct parent email or parent email is empty."));
            }
            $class_id = $class['group_id'];
            $db->begin_transaction();

            //$args['code_auto'] = isset($_POST['code_auto'])? $_POST['code_auto']: 0;
            $args['child_parent_id'] = $_POST['child_parent_id'];
            //Sinh mã trẻ nếu cần
            /*if ($args['code_auto']) {
                $args['child_code'] = generateCode($args['child_name']);
            }*/

            //1. Tạo thông tin trẻ trong bảng ci_child
            $lastId = $childDao->createChild($args);

            //2. Cập nhật danh sách lớp (đưa trẻ vào lớp - bảng ci_class_child)
            // Nếu ko chọn class thì thêm trẻ vào trường, bỏ qua bước này
            $argsCC['class_id'] = $class_id;
            $argsCC['begin_at'] = isset($_POST['begin_at'])? trim($_POST['begin_at']) : "";
            $argsCC['child_id'] = $lastId;
            if ($class_id > 0) {
                $childDao->addChildToClass($argsCC);
                // ADD START MANHDD 03/06/2021
                //2.2 nhập thông tin của trẻ vào bảng điểm ci_point_
                $subjectOfClass = $subjectDao->getSubjectsByClassId($class_id,$class['school_id']);
                if(count($subjectOfClass)>0) {
                    foreach($subjectOfClass as $subject) {
                        $childDao->addChildPoints($subject['subject_id'],$args['child_code'],$class_id,$subject['school_year']);
                    }
                }
                //2.3 Nhập thông tin của trẻ vào bảng hạnh kiểm ci_conduct
                $school_year = date("Y", strtotime(str_replace('/', '-', $_POST['begin_at']))).'-'.(date("Y", strtotime(str_replace('/', '-', $_POST['begin_at'])))+1);
                $childDao->addChildConduct($class_id,$argsCC['child_id'],$school_year);
                // ADD END MANHDD 03/06/2021
            }

            //3. Cập nhật thông tin trẻ trong trường (cập nhật bảng ci_school_child)
            $school = getSchoolData($class['school_id'], SCHOOL_DATA);

            $argsCC['school_id'] = $school['page_id'];
            $argsCC['child_parent_id'] = $_POST['child_parent_id'];
            $childDao->addChildToSchool($argsCC);

            $parentIds = (isset($_POST['user_id'])? $_POST['user_id'] : array());

            // bỏ user_id trùng
            $parentIds = array_unique($parentIds);

            //3.1. Tự động tạo tài khoản phụ huynh
            if ($create_parent_account) {
                $argsParent = array();
                $argsParent['full_name'] = $args['parent_name_creat_account'];
                $argsParent['first_name'] = split_name($argsParent['full_name'])[0];
                $argsParent['last_name'] = split_name($argsParent['full_name'])[1];
                $argsParent['username'] = generateUsername($argsParent['full_name']);
                $argsParent['user_phone'] = standardizePhone($args['parent_phone_creat_account']);
                /*if (!validatePhone($args['user_phone'])) {
                    return_json( array('error' => true, 'message' => __("The phone number you entered is incorrect")) );
                }*/
                $argsParent['email'] = ($args['parent_email'] != '') ? $args['parent_email'] : 'null';
                $argsParent['password'] = (is_empty($argsParent['user_phone'])? "coniu.vn": $argsParent['user_phone']);
                $argsParent['gender'] = FEMALE;

                $parentId = $userDao->createUser($argsParent);
                $parentIds[] = $parentId;
                if($argsParent['email'] != 'null') {
                    //Gửi mail cho phụ huynh của trẻ.
                    $argEmail = array();
                    $argEmail['action'] = "create_parent_account";
                    $argEmail['receivers'] = $argsParent['email'];
                    $argEmail['subject'] = "[".convertText4Web($school['page_title'])."]".__("Use Inet application to connect student's parents");

                    //Tạo nên nội dung email
                    $smarty->assign('full_name', $argsParent['full_name']);
                    $smarty->assign('child_name', $args['child_name']);
                    $smarty->assign('username', $argsParent['email']);
                    $smarty->assign('password', $argsParent['password']);
                    $argEmail['content'] = $smarty->fetch("ci/email_templates/create_parent_account.tpl");

                    $argEmail['delete_after_sending'] = 1;
                    $argEmail['school_id'] = $school['page_id'];
                    $argEmail['user_id'] = $user->_data['user_id'];

                    $mailDao->insertEmail($argEmail);
                }
            }
            if (count($parentIds) > 0){
                //3.2. Cho phụ huynh like các page của hệ thống, join các group của hệ thống
                $userDao->addUsersLikeSomePages($system_page_ids, $parentIds);
                $userDao->addUserToSomeGroups($system_group_ids, $parentIds);

                //4. Thêm cha mẹ vào nhóm của lớp (bao gồm việc tăng thành viên nhóm)
                // Nếu chưa chọn lớp thì bỏ qua
                if ($class_id > 0) {
                    $classDao->addUserToClass($class_id, $parentIds);
                }

                //5. Thêm cha mẹ cho trẻ
                $parentDao->addParentList($lastId, $parentIds);

                //6. Cho cha mẹ like trang của trường (tăng số like)
                $likeCnt = $schoolDao->addUsersLikeSchool($school['page_id'], $parentIds);
                $school['page_likes'] = $school['page_likes'] + $likeCnt;

                //7. Cho cha mẹ thành bạn của giáo viên (bao gồm cả quản lý trường)
                // Nếu chưa chọn lớp thì bỏ qua
                // Tạm thời không cho kết bạn giữa giáo viên, phụ huynh
                /*if ($class_id > 0) {
                    $teacherIds = $teacherDao->getTeacherIDOfClass($class_id);
                }
                $teacherIds[] = $user->_data['user_id']; //User hiện tại là quản lý của trường
                $userDao->becomeFriends($teacherIds, $parentIds);*/

                //7. Gắn trẻ được quản lý cho phụ huynh (ci_parent_manage)
                $parents = $childDao->getParentManageChild($_POST['child_parent_id']);
                $oldParentIds = array();
                foreach ($parents as $parent) {
                    $oldParentIds[] = $parent['user_id'];
                }
                $addedParentIds = array_diff($parentIds, $oldParentIds);

                $childDao->createParentManageInfo($addedParentIds, $_POST['child_parent_id'], $lastId, $school['page_id']);
                $childDao->updateParentManageInfo($oldParentIds, $_POST['child_parent_id'], $lastId, $school['page_id']);
            }
            //8. Tăng số lượng trẻ của trường
            $schoolDao->updateGenderCount($school['page_id'], $args['gender'], 1);

            if ($args['gender'] == MALE) {
                $school['male_count'] = $school['male_count'] + 1;
            } else {
                $school['female_count'] = $school['female_count'] + 1;
            }

            // Thông báo cho phụ huynh biết trẻ được thêm vào trường
            $userDao->postNotifications($parentIds, NOTIFICATION_NEW_CHILD_EXIST, NOTIFICATION_NODE_TYPE_CHILD,
                $lastId, convertText4Web($args['child_name']), $lastId, convertText4Web($school['page_title']));

            /* ---------- UPDATE - MEMCACHE ---------- */
            //1. Cập nhật thông tin của trường
            $info_update = array(
                'page_likes' => $school['page_likes']
            );
            $config_update = array(
                'male_count' => $school['male_count'],
                'female_count' => $school['female_count']
            );

            updateSchoolData($school['page_id'], SCHOOL_INFO, $info_update);
            updateSchoolData($school['page_id'], SCHOOL_CONFIG, $config_update);
            addSchoolData($school['page_id'], SCHOOL_CHILDREN, $lastId);

            //2. Cập nhật thông tin trẻ và lớp
            addClassData($class_id, CLASS_CHILDREN, $lastId);
            updateClassData($class_id, CLASS_INFO);

            //3. Cập nhật tin tin quản lý trẻ
            foreach ($parentIds as $parentId) {
                updateUserManageData($parentId, USER_MANAGE_CHILDREN);
            }
            /* ---------- END - MEMCACHE ---------- */

            $argc = array();
            $argc['edit_by'] = EDIT_BY_TEACHER;
            $argc['child_id'] = $lastId;
            $argc['school_id'] = $school['page_id'];

            // Thêm trẻ vào bảng ci_child_edit_history
            $childDao->insertChildEditHistory($argc);
            $db->commit();
            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/class/' . $_POST['username'] . '/children' . '";'));
            break;
        //Xác nhận user là phụ huynh của trẻ
        case 'approve':
            $school = getSchoolData($class['school_id'], SCHOOL_DATA);

            $db->begin_transaction();
            //1. Lấy ra thông tin cũ của trẻ để kiểm tra
            $oldChild = getChildData($_POST['child_id']);
            if (is_null($oldChild)) {
                _error(404);
            }
            //1. Lấy ra thông tin cũ của trẻ để kiểm tra
            $oldParents = getChildData($_POST['child_id'], CHILD_PARENTS);
            $oldParentIds = array_keys($oldParents);

            //1. Thêm cha mẹ cho trẻ
            $parentDao->addParentList($_POST['child_id'], [$_POST['parent_id']]);

            //2. Thêm cha mẹ vào nhóm của lớp (bao gồm việc tăng thành viên nhóm)
            $classDao->addUserToClass($class['group_id'], [$_POST['parent_id']]);
            //3. Cho cha mẹ like trang của trường (tăng số like)
            $likeCnt = $schoolDao->addUsersLikeSchool($class['school_id'], [$_POST['parent_id']]);
            $school['page_likes'] = $school['page_likes'] + $likeCnt;

            //4. Cho cha mẹ thành bạn của giáo viên (bao gồm cả quản lý trường)
            //$teacherIds = $teacherDao->getTeacherIDOfClass($class['group_id']);
            /*$teacherIds = array_keys(getClassData($class['group_id'], CLASS_TEACHERS));*/
            //Lấy ra admin của trường
            /*$schoolInfo = getSchoolData($class['school_id'], SCHOOL_INFO);*/
            //$teacherIds[] = $schoolDao->getAdminId($class['school_id']);
            // Tạm thời không cho kết bạn giữa giáo viên, phụ huynh
            /*$teacherIds[] = $schoolInfo['page_admin'];
            $userDao->becomeFriends(array_unique($teacherIds), [$_POST['parent_id']]);*/

            //7. Tạo thông tin trẻ cho phụ huynh (ci_child_parent)
            if (count($oldParentIds) == 0) {
                $childAdmin = $_POST['parent_id'];
                $childParentId = $oldChild['child_parent_id'];
                $childParentId = $childDao->updateChildForParent($childParentId, $childAdmin);
            }

            //7.1. Gắn trẻ được quản lý cho phụ huynh (ci_parent_manage)
            $childDao->createParentManageInfo([$_POST['parent_id']], $oldChild['child_parent_id'], $_POST['child_id'], $school['page_id']);
            $db->commit();

            /* ---------- UPDATE - MEMCACHE ---------- */
            //1. Cập nhật thông tin của trường
            $info_update = array(
                'page_likes' => $school['page_likes']
            );
            updateSchoolData($school['page_id'], SCHOOL_INFO, $info_update);
            //2. Cập nhật thông tin lớp
            updateClassData($class['group_id']);
            //3. Cập nhật thông tin trẻ
            updateChildData($_POST['child_id']);
            /* ---------- END - MEMCACHE ---------- */

            return_json(array());
            break;

        //Xác nhận thông tin người đón trẻ
        case 'confirm':
            if(!isset($_POST['child_id']) || !is_numeric($_POST['child_id'])) {
                _error(404);
            }
            $status = PICKER_CONFIRMED;
            // update trạng thái đã trả trẻ
            $childDao->updateChildInformationConfirm($_POST['child_id'], $status);
            //$child = $childDao->getChild($_POST['child_id']);
            $child = getChildData($_POST['child_id'], CHILD_INFO);
            //$parentIds = $parentDao->getParentIds($_POST['child_id']);
            $parents = getChildData($_POST['child_id'], CHILD_PARENTS);
            // Thông báo đến phụ huynh
            if (!is_null($child) && !is_null($parents)) {
                $parentIds = array_keys($parents);
                $userDao->postNotifications($parentIds, NOTIFICATION_CONFIRM_PICKER, NOTIFICATION_NODE_TYPE_CHILD, $_POST['child_id'], '', '', convertText4Web($child['child_name']));
            }
            $db->commit();

            /* ---------- UPDATE - MEMCACHE ---------- */
            //1. Cập nhật thông tin của trường
            updateChildData($_POST['child_id'], CHILD_PICKER_INFO);
            /* ---------- END - MEMCACHE ---------- */

            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/class/' . $_POST['username'] . '/children/detail/' . $_POST['child_id'] . '";'));
            break;
        case 'delete_edit':
            // Xóa trẻ khỏi danh sách edit
            $db->begin_transaction();
            $childDao->deleteChildEditOfTeacher($class['school_id'],$_POST['child_id']);
            $db->commit();
            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/class/' . $_POST['username'] . '/children/listchildedit' . '";'));
            break;

        case 'edit_growth':
            // Thay đổi thông tin chiều cao, cân nặng của trẻ
            if(!isset($_POST['child_growth_id']) || !is_numeric($_POST['child_growth_id'])) {
                _error(404);
            }
            $db->begin_transaction();
            // Lấy child_parent_id
            $child_parent_id = $childDao->getChildParentIdFromChildId($_POST['child_id']);

            $child_growth = $childDao->getChildGrowthById($_POST['child_growth_id']);
            $args = array();
            $args['recorded_at'] = $_POST['recorded_at'];
            // Lấy thông tin chiều cao cân nặng của trẻ
            $growth = $childDao->getChildGrowth($child_growth['child_parent_id']);
            $growth_recorded = array();
            foreach ($growth as $row) {
                $growth_recorded[] = $row['recorded_at'];
            }
            if(in_array($args['recorded_at'], $growth_recorded) && ($args['recorded_at'] != $child_growth['recorded_at'])) {
                throw new Exception(__("Selected date existed"));
            }
            $args['height'] = $_POST['height'];
            $args['weight'] = $_POST['weight'];
            $args['nutriture_status'] = $_POST['nutriture_status'];
            $args['heart'] = $_POST['heart'];
            $args['blood_pressure'] = $_POST['blood_pressure'];
            $args['ear'] = $_POST['ear'];
            $args['eye'] = $_POST['eye'];
            $args['nose'] = $_POST['nose'];
            $args['description'] = $_POST['description'];
            $args['child_growth_id'] = $_POST['child_growth_id'];

            $args['source_file'] = $child_growth['source_file_path'];
            $args['file_name'] = convertText4Web($child_growth['file_name']);

            $file_name = "";
            // Upload file đính kèm
            if (file_exists($_FILES['file']['tmp_name'])) {
                // check file upload enabled
                if (!$system['file_enabled']) {
                    modal(MESSAGE, __("Not Allowed"), __("This feature has been disabled"));
                }

                // valid inputs
                if (!isset($_FILES["file"]) || $_FILES["file"]["error"] != UPLOAD_ERR_OK) {
                    modal(MESSAGE, __("Upload Error"), __("Something wrong with upload! Is 'upload_max_filesize' set correctly?"));
                }

                // check file size
                $max_allowed_size = $system['max_file_size'] * 1024;
                if ($_FILES["file"]["size"] > $max_allowed_size) {
                    modal(MESSAGE, __("Upload Error"), __("The file size is so big"));
                }

                // check file extesnion
                $extension = get_extension($_FILES['file']['name']);
                if (!valid_extension($extension, $system['file_medicine_extensions'])) {
                    //modal(MESSAGE, __("Upload Error"), __("The file type is not valid or not supported"));
                    return_json(array('error' => true, 'message' => __("The file type is not valid or not supported")));
                }

                /* check & create uploads dir */
                $depth = '../../../../../';
                $folder = 'growths';
                if (!file_exists($depth . $system['system_uploads_directory'] . '/' . $folder)) {
                    @mkdir($depth . $system['system_uploads_directory'] . '/' . $folder, 0777, true);
                }
                if (!file_exists($depth . $system['system_uploads_directory'] . '/' . $folder . '/' . date('Y'))) {
                    @mkdir($depth . $system['system_uploads_directory'] . '/' . $folder . '/' . date('Y'), 0777, true);
                }
                if (!file_exists($system['system_uploads_directory'] . '/' . $folder . '/' . date('Y') . '/' . date('m'))) {
                    @mkdir($depth . $system['system_uploads_directory'] . '/' . $folder . '/' . date('Y') . '/' . date('m'), 0777, true);
                }

                /* prepare new file name */
                $directory = $folder . '/' . date('Y') . '/' . date('m') . '/';
                $prefix = $system['uploads_prefix'] . '_' . md5(time() * rand(1, 9999));

                $image = new Image($_FILES["file"]["tmp_name"]);

                $file_name = $directory . $prefix . $image->_img_ext;
                $file_name_tmp = $directory . $prefix . '_tmp' . $image->_img_ext;

                $path = $depth . $system['system_uploads_directory'] . '/' . $file_name;
                $path_tmp = $depth . $system['system_uploads_directory'] . '/' . $file_name_tmp;

                /* check if the file uploaded successfully */
                if (!@move_uploaded_file($_FILES['file']['tmp_name'], $path_tmp)) {
                    modal(MESSAGE, __("Upload Error"), __("Sorry, can not upload the file"));
                }

                /* save the new image */
                $image->save($path, $path_tmp);

                /* delete the tmp image */
                unlink($path_tmp);
                $args['source_file'] = $file_name;
                $args['file_name'] = $_FILES['file']['name'];

            }

            $childDao->updateChildGrowth($args);

            // Lấy thông tin chi tiết trẻ
            $child = getChildData($_POST['child_id'], CHILD_INFO);

            // Lấy danh sách phụ huynh của trẻ
            $parents = getChildData($_POST['child_id'], CHILD_PARENTS);
            $parentIds = array();
            if(count($parents) > 0) {
                foreach ($parents as $parent) {
                    $parentIds[] = $parent['user_id'];
                }

                // Thông báo cho phụ huynh có thông tin sức khỏe mới
                $userDao->postNotifications($parentIds, NOTIFICATION_UPDATE_CHILD_HEALTH, NOTIFICATION_NODE_TYPE_CHILD,
                    $child['birthday'], $_POST['recorded_at'], $child_parent_id, convertText4Web($child['child_name']));
            }

            $db->commit();

            if(isset($_POST['is_module']) && $_POST['is_module'] == 1) {
                return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/class/' . $_POST['username'] . '/healths' . '";'));
            } else {
                return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/class/' . $_POST['username'] . '/children/health/'. $_POST['child_id'] . '";'));
            }
            break;

        case 'add_growth':
            // Thêm thông tin chiều cao cân nặng của trẻ trong lớp
            if (!isset($_POST['child_id']) || !is_numeric($_POST['child_id'])) {
                _api_error(404);
            }
            $db->begin_transaction();

            // Lấy child_parent_id
            $child_parent_id = $childDao->getChildParentIdFromChildId($_POST['child_id']);

            $file_name = "";
            // Upload file đính kèm
            if (file_exists($_FILES['file']['tmp_name'])) {
                // check file upload enabled
                if (!$system['file_enabled']) {
                    _api_error(0, __("This feature has been disabled"));
                }

                // valid inputs
                if (!isset($_FILES["file"]) || $_FILES["file"]["error"] != UPLOAD_ERR_OK) {
                    _api_error(0, __("Something wrong with upload! Is 'upload_max_filesize' set correctly?"));
                }

                // check file size
                $max_allowed_size = $system['max_file_size'] * 1024;
                if ($_FILES["file"]["size"] > $max_allowed_size) {
                    _api_error(0, __("The file size is so big"));
                }

                // check file extesnion
                $extension = get_extension($_FILES['file']['name']);
                if (!valid_extension($extension, $system['file_medicine_extensions'])) {
                    //modal(MESSAGE, __("Upload Error"), __("The file type is not valid or not supported"));
                    _api_error(0, __("The file type is not valid or not supported") );
                }

                /* check & create uploads dir */
                $depth = '../../../../../';
                $folder = 'growths';
                if (!file_exists($depth . $system['system_uploads_directory'] . '/' . $folder)) {
                    @mkdir($depth . $system['system_uploads_directory'] . '/' . $folder, 0777, true);
                }
                if (!file_exists($depth . $system['system_uploads_directory'] . '/' . $folder . '/' . date('Y'))) {
                    @mkdir($depth . $system['system_uploads_directory'] . '/' . $folder . '/' . date('Y'), 0777, true);
                }
                if (!file_exists($system['system_uploads_directory'] . '/' . $folder . '/' . date('Y') . '/' . date('m'))) {
                    @mkdir($depth . $system['system_uploads_directory'] . '/' . $folder . '/' . date('Y') . '/' . date('m'), 0777, true);
                }

                /* prepare new file name */
                $directory = $folder . '/' . date('Y') . '/' . date('m') . '/';
                $prefix = $system['uploads_prefix'] . '_' . md5(time() * rand(1, 9999));

                $image = new Image($_FILES["file"]["tmp_name"]);

                $file_name = $directory . $prefix . $image->_img_ext;
                $file_name_tmp = $directory . $prefix . '_tmp' . $image->_img_ext;

                $path = $depth . $system['system_uploads_directory'] . '/' . $file_name;
                $path_tmp = $depth . $system['system_uploads_directory'] . '/' . $file_name_tmp;

                /* check if the file uploaded successfully */
                if (!@move_uploaded_file($_FILES['file']['tmp_name'], $path_tmp)) {
                    throw new Exception (__("Sorry, can not upload the file"));
                }

                /* save the new image */
                $image->save($path, $path_tmp);

                /* delete the tmp image */
                unlink($path_tmp);
                $argsGR['source_file'] = $file_name;
                $argsGR['file_name'] = $_FILES['file']['name'];

            }

            $argsGR['child_parent_id'] = $child_parent_id;
            $argsGR['height'] = $_POST['height'];
            $argsGR['weight'] = $_POST['weight'];
            $argsGR['nutriture_status'] = $_POST['nutriture_status'];
            $argsGR['heart'] = $_POST['heart'];
            $argsGR['blood_pressure'] = $_POST['blood_pressure'];
            $argsGR['ear'] = $_POST['ear'];
            $argsGR['eye'] = $_POST['eye'];
            $argsGR['nose'] = $_POST['nose'];
            $argsGR['description'] = $_POST['description'];
            $argsGR['is_parent'] = 0;


            // Lấy thông tin chiều cao cân nặng của trẻ
            $growth = $childDao->getChildGrowth($child_parent_id);
            $growth_recorded = array();
            foreach ($growth as $row) {
                $growth_recorded[] = $row['recorded_at'];
            }
            // $date_now = date('d/m/Y');
            if(!isset($_POST['recorded_at']) || is_null($_POST['recorded_at'])) {
                throw new Exception(__("Data record date can not be empty"));
            }
            $recorded_at = $_POST['recorded_at'];
            $argsGR['recorded_at'] = $recorded_at;
            if(in_array($recorded_at, $growth_recorded)) {
                $growth_now = $childDao->getChildGrowthByDate($argsGR['recorded_at'], $child_parent_id);
                if(!isset($argsGR['source_file'])) {
                    $argsGR['source_file'] = $growth_now['source_file_path'];
                    $argsGR['file_name'] = $growth_now['file_name'];
                }
                $childDao->updateChildGrowthDateNow($argsGR);
            } else {
                $insertIdGR = $childDao->insertChildGrowth($argsGR);
            }

            // Lấy thông tin trẻ
            $child = getChildData($_POST['child_id'], CHILD_INFO);

            // Lấy danh sách phụ huynh của trẻ
            $parents = getChildData($_POST['child_id'], CHILD_PARENTS);
            $parentIds = array();
            if(count($parents) > 0) {
                foreach ($parents as $parent) {
                    $parentIds[] = $parent['user_id'];
                }

                // Thông báo cho phụ huynh có thông tin sức khỏe mới
                if(isset($updateIdGR) && !isset($insertIdGR)) {
                    $userDao->postNotifications($parentIds, NOTIFICATION_UPDATE_CHILD_HEALTH, NOTIFICATION_NODE_TYPE_CHILD,
                        $child['birthday'], $_POST['recorded_at'], $child_parent_id, convertText4Web($child['child_name']));
                } else if(!isset($updateIdGR) && isset($insertIdGR)) {
                    $userDao->postNotifications($parentIds, NOTIFICATION_NEW_CHILD_HEALTH, NOTIFICATION_NODE_TYPE_CHILD,
                        $child['birthday'], $_POST['recorded_at'], $child_parent_id, convertText4Web($child['child_name']));
                }
            }

            $db->commit();
            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/class/' . $_POST['username'] . '/children/health/'. $_POST['child_id'] . '";'));
            break;
        case 'add_health_index':
            // Thêm thông tin chiều cao cân nặng của tất cả trẻ trong lớp
            if (!isset($_POST['recorded_at'])) {
                throw new Exception(__("You must enter correct recorded_at"));
            }
            $childIds = isset($_POST['childIds'])?$_POST['childIds']:array();
//            $height = isset($_POST['height'])?$_POST['height']:array();
//            $weight = isset($_POST['weight'])?$_POST['weight']:array();
//            $nutritureSstatus = isset($_POST['nutriture_status'])?$_POST['nutriture_status']:array();
//            $heart = isset($_POST['heart'])?$_POST['heart']:array();
//            $bloodPressure = isset($_POST['blood_pressure'])?$_POST['blood_pressure']:array();
//            $ear = isset($_POST['ear'])?$_POST['ear']:array();
//            $eye = isset($_POST['eye'])?$_POST['eye']:array();
//            $description = isset($_POST['description'])?$_POST['description']:array();
            if(count($childIds) > 0) {
                foreach ($childIds as $childId) {
                    if(isset($_POST['height_'.$childId]) && is_numeric($_POST['height_'.$childId]) && isset($_POST['weight_'.$childId]) && is_numeric($_POST['weight_'.$childId])) {
                        // Lấy child_parent_id
                        $child_parent_id = $childDao->getChildParentIdFromChildId($childId);

                        $file_name = "";
                        // Upload file đính kèm
                        if (file_exists($_FILES['file_'.$childId]['tmp_name'])) {
                            // check file upload enabled
                            if (!$system['file_enabled']) {
                                _api_error(0, __("This feature has been disabled"));
                            }

                            // valid inputs
                            if (!isset($_FILES['file_'.$childId]) || $_FILES['file_'.$childId]["error"] != UPLOAD_ERR_OK) {
                                _api_error(0, __("Something wrong with upload! Is 'upload_max_filesize' set correctly?"));
                            }

                            // check file size
                            $max_allowed_size = $system['max_file_size'] * 1024;
                            if ($_FILES['file_'.$childId]["size"] > $max_allowed_size) {
                                _api_error(0, __("The file size is so big"));
                            }

                            // check file extesnion
                            $extension = get_extension($_FILES['file_'.$childId]['name']);
                            if (!valid_extension($extension, $system['file_medicine_extensions'])) {
                                //modal(MESSAGE, __("Upload Error"), __("The file type is not valid or not supported"));
                                _api_error(0, __("The file type is not valid or not supported") );
                            }

                            /* check & create uploads dir */
                            $depth = '../../../../../';
                            $folder = 'growths';
                            if (!file_exists($depth . $system['system_uploads_directory'] . '/' . $folder)) {
                                @mkdir($depth . $system['system_uploads_directory'] . '/' . $folder, 0777, true);
                            }
                            if (!file_exists($depth . $system['system_uploads_directory'] . '/' . $folder . '/' . date('Y'))) {
                                @mkdir($depth . $system['system_uploads_directory'] . '/' . $folder . '/' . date('Y'), 0777, true);
                            }
                            if (!file_exists($system['system_uploads_directory'] . '/' . $folder . '/' . date('Y') . '/' . date('m'))) {
                                @mkdir($depth . $system['system_uploads_directory'] . '/' . $folder . '/' . date('Y') . '/' . date('m'), 0777, true);
                            }

                            /* prepare new file name */
                            $directory = $folder . '/' . date('Y') . '/' . date('m') . '/';
                            $prefix = $system['uploads_prefix'] . '_' . md5(time() * rand(1, 9999));

                            $image = new Image($_FILES['file_'.$childId]["tmp_name"]);

                            $file_name = $directory . $prefix . $image->_img_ext;
                            $file_name_tmp = $directory . $prefix . '_tmp' . $image->_img_ext;

                            $path = $depth . $system['system_uploads_directory'] . '/' . $file_name;
                            $path_tmp = $depth . $system['system_uploads_directory'] . '/' . $file_name_tmp;

                            /* check if the file uploaded successfully */
                            if (!@move_uploaded_file($_FILES['file_'.$childId]['tmp_name'], $path_tmp)) {
                                throw new Exception (__("Sorry, can not upload the file"));
                            }

                            /* save the new image */
                            $image->save($path, $path_tmp);

                            /* delete the tmp image */
                            unlink($path_tmp);
                            $argsGR['source_file'] = $file_name;
                            $argsGR['file_name'] = $_FILES['file_'.$childId]['name'];

                        }

                        $argsGR['child_parent_id'] = $child_parent_id;
                        $argsGR['height'] = $_POST['height_'.$childId];
                        $argsGR['weight'] = $_POST['weight_'.$childId];
                        $argsGR['nutriture_status'] = $_POST['nutriture_status_'.$childId];
                        $argsGR['heart'] = $_POST['heart_'.$childId];
                        $argsGR['blood_pressure'] = $_POST['blood_pressure_'.$childId];
                        $argsGR['ear'] = $_POST['ear_'.$childId];
                        $argsGR['eye'] = $_POST['eye_'.$childId];
                        $argsGR['nose'] = $_POST['nose_'.$childId];
                        $argsGR['description'] = $_POST['description_'.$childId];
                        $argsGR['is_parent'] = 0;


                        // Lấy thông tin chiều cao cân nặng của trẻ
                        $growth = $childDao->getChildGrowth($child_parent_id);
                        $growth_recorded = array();
                        foreach ($growth as $row) {
                            $growth_recorded[] = $row['recorded_at'];
                        }
                        // $date_now = date('d/m/Y');
                        if(!isset($_POST['recorded_at']) || is_null($_POST['recorded_at'])) {
                            throw new Exception(__("Data record date can not be empty"));
                        }
                        $recorded_at = $_POST['recorded_at'];
                        $argsGR['recorded_at'] = $recorded_at;
                        if(in_array($recorded_at, $growth_recorded)) {
                            $growth_now = $childDao->getChildGrowthByDate($argsGR['recorded_at'], $child_parent_id);
                            if(!isset($argsGR['source_file'])) {
                                $argsGR['source_file'] = $growth_now['source_file_path'];
                                $argsGR['file_name'] = $growth_now['file_name'];
                            }
                            $childDao->updateChildGrowthDateNow($argsGR);
                        } else {
                            $insertIdGR = $childDao->insertChildGrowth($argsGR);
                        }

                        // Lấy thông tin chi tiết trẻ
                        $child = getChildData($childId, CHILD_INFO);

                        // Lấy danh sách phụ huynh của trẻ
                        $parents = getChildData($childId, CHILD_PARENTS);
                        $parentIds = array();
                        if(count($parents) > 0) {
                            foreach ($parents as $parent) {
                                $parentIds[] = $parent['user_id'];
                            }

                            // Thông báo cho phụ huynh có thông tin sức khỏe mới
                            if(isset($updateIdGR) && !isset($insertIdGR)) {
                                $userDao->postNotifications($parentIds, NOTIFICATION_UPDATE_CHILD_HEALTH, NOTIFICATION_NODE_TYPE_CHILD,
                                    $child['birthday'], $_POST['recorded_at'], $child_parent_id, convertText4Web($child['child_name']));
                            } else if(!isset($updateIdGR) && isset($insertIdGR)) {
                                $userDao->postNotifications($parentIds, NOTIFICATION_NEW_CHILD_HEALTH, NOTIFICATION_NODE_TYPE_CHILD,
                                    $child['birthday'], $_POST['recorded_at'], $child_parent_id, convertText4Web($child['child_name']));
                            }
                        }
                    }
                }
            }
            $db->begin_transaction();
            $db->commit();
            return_json( array('success' => true, 'message' => __("Done, Child info has been created")) );
            break;
        case 'delete_growth':
            if(!isset($_POST['child_id']) || !is_numeric($_POST['child_id'])) {
                _error(404);
            }
            if(!isset($_POST['id']) || !is_numeric($_POST['id'])) {
                _error(404);
            }
            $module = $_POST['module'];

            $child_parent_id = $childDao->getChildParentIdFromChildId($_POST['child_id']);
            $db->begin_transaction();

            $childDao->deleteChildGrowth($child_parent_id, $_POST['id']);

            $db->commit();
            if($module) {
                return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/class/' . $_POST['username'] . '/healths'. '";'));
            } else {
                return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/class/' . $_POST['username'] . '/children/health/'. $_POST['child_id'] . '";'));
            }
            break;
        case 'search_chart':
            $db->begin_transaction();
            if($_POST['begin'] == '' || !validateDate($_POST['begin']) ) {
                $begin = null;
            } else {
                $begin = $_POST['begin'];
            }
            if($_POST['end'] == '' || !validateDate($_POST['end'])) {
                $end = null;
            } else {
                $end = $_POST['end'];
            }

            // Lấy ngày hiện tại
            $dateNow = date('Y-m-d');

            $child_parent_id = $childDao->getChildParentIdFromChildId($_POST['child_id']);
            // Lấy thông tin trẻ
//            $child = $childDao->getChildByParent($child_parent_id);
            $child = getChildData($_POST['child_id'], CHILD_INFO);

            // Lấy ngày sinh nhật của trẻ
            $birthday = toDBDate($child['birthday']);

            // Khởi tạo mảng lưu chỉ số sức khỏe của trẻ
            $childGrowth = array();

            if(is_null($begin) || is_null($end)) {
                // Lấy toàn bộ thông tin sức khỏe của trẻ
                $childGrowth = $childDao->getChildGrowthForChart($child['child_parent_id']);

                // Tính xem trẻ được bao nhiêu ngày tuổi
                $day_age_now = (strtotime($dateNow) - strtotime($birthday)) / (60*60*24);

                // đoạn này xử lý để lấy ngày bắt đầu (đặt tạm a, b, c; bí tên quá)
                $a = $day_age_now % 30;
                $b = $day_age_now - $a;
                $c = $b - 30*10;
                $dayGet = (int)$c;
                $day_age_max = $day_age_now + 65;
                if($day_age_now >= 300) {
                    $day_age_min = $dayGet;
                } else {
                    $day_age_min = 0;
                }

                // Lấy dữ liệu người dùng nhập vào vẽ biểu đồ
                $heightForChartChartJS = getDataJsonForChartChartJS($childGrowth, $day_age_min, $day_age_max, $birthday, 'height');
                $weightForChartChartJS = getDataJsonForChartChartJS($childGrowth, $day_age_min, $day_age_max, $birthday, 'weight');
                if($child['gender'] == 'male') {
                    // Dữ liệu chuẩn
                    $hightStandardArray = getHightDataForChart($hightStandardJsonMale);
                    $weightStandardArray = getHightDataForChart($weightStandardJsonMale);
                } else {
                    $hightStandardArray = getHightDataForChart($hightStandardJsonFeMale);
                    $weightStandardArray = getHightDataForChart($weightStandardJsonFeMale);

                }
                // 1. Chiều cao
                // Lấy 2 ngày gần nhất của $begin_day có dữ liệu, nội suy chuẩn của ngày bắt đầu
                $hightStandardArray[$day_age_min]['highest'] = getStandardDataBeginAndEnd($hightStandardArray, $day_age_min, 'highest');

                $hightStandardArray[$day_age_min]['lowest'] = getStandardDataBeginAndEnd($hightStandardArray, $day_age_min, 'lowest');

                // Lấy 2 ngày gần nhất của $end_day có dữ liệu, nội suy chuẩn của ngày kết thúc
                $hightStandardArray[$day_age_max]['highest'] = getStandardDataBeginAndEnd($hightStandardArray, $day_age_max, 'highest');

                $hightStandardArray[$day_age_max]['lowest'] = getStandardDataBeginAndEnd($hightStandardArray, $day_age_max, 'lowest');

                // dỮ LIỆU đường cao nhất (thư viện charts.js)
                $highestForChartChartsJS = getStandardDataForChartChartJS($hightStandardArray, $day_age_min, $day_age_max, 'highest');

                // Dữ liệu đường thấp nhất (thư viện charts.js)
                $lowestForChartChartsJS = getStandardDataForChartChartJS($hightStandardArray, $day_age_min, $day_age_max, 'lowest');

                // 2. Cân nặng
                // Lấy 2 ngày gần nhất của $begin_day có dữ liệu, nội suy chuẩn của ngày bắt đầu
                $weightStandardArray[$day_age_min]['highest'] = getStandardDataBeginAndEnd($weightStandardArray, $day_age_min, 'highest');

                $weightStandardArray[$day_age_min]['lowest'] = getStandardDataBeginAndEnd($weightStandardArray, $day_age_min, 'lowest');

                // Lấy 2 ngày gần nhất của $end_day có dữ liệu, nội suy chuẩn của ngày kết thúc
                $weightStandardArray[$day_age_max]['highest'] = getStandardDataBeginAndEnd($weightStandardArray, $day_age_max, 'highest');

                $weightStandardArray[$day_age_max]['lowest'] = getStandardDataBeginAndEnd($weightStandardArray, $day_age_max, 'lowest');

                // Dữ liệu đường cao nhất (charts.js)
                $highestWeightForChartChartJS = getStandardDataForChartChartJS($weightStandardArray, $day_age_min, $day_age_max, 'highest');

                // Dữ liệu đường thấp nhất (charts.js)
                $lowestWeightForChartChartJS = getStandardDataForChartChartJS($weightStandardArray, $day_age_min, $day_age_max, 'lowest');

                // 3. BMI (charts.js)
                $bmiForChartChartJS = getDataJsonForChartChartJS($childGrowth, $day_age_min, $day_age_max, $birthday, 'bmi');

                // Get label
                $label = '';
                for($i = $day_age_min; $i <= $day_age_max; $i++) {
                    if($i%30 == 0) {
                        $month = $i/30;
                        $label .= ' ' .$month. ',';
                    } else {
                        $label .= ' ,';
                    }
                }
                $smarty->assign('label', $label);
            } else {
                // Lấy dữ liệu nhập và của trẻ
                $childGrowth = $childDao->getChildGrowthSearchForChartBeginEnd($child['child_parent_id'], $begin, $end);

                // Lấy ngày bắt đầu và ngày kết thúc
                $begin = toDBDate($begin);
                $end = toDBDate($end);

                $end_day = (strtotime($end) - strtotime($birthday)) / (60*60*24);
                $end_day = $end_day / 30;
                $end_day = (int)$end_day;
                $end_day = ($end_day + 1) * 30;

                $begin_day = (strtotime($begin) - strtotime($birthday)) / (60*60*24);
                $begin_day = $begin_day / 30;
                $begin_day = (int)$begin_day;
                $begin_day = $begin_day * 30;
                if($begin_day < 0) {
                    throw new Exception(__("You must select day begin after birthday"));
                }

                // Dữ liệu chuẩn người dùng nhập vào vẽ biểu đồ chiều cao (thư viện charts.js)
                $heightForChartChartJS = getDataJsonForChartChartJS($childGrowth, $begin_day, $end_day, $birthday, 'height');

                // Lấy dữ liệu người dùng nhập vào vẽ biểu đồ cân nặng (charts.js)
                $weightForChartChartJS = getDataJsonForChartChartJS($childGrowth, $begin_day, $end_day, $birthday, 'weight');

                if($child['gender'] == 'male') {
                    // Dữ liệu chuẩn
                    $hightStandardArray = getHightDataForChart($hightStandardJsonMale);
                    $weightStandardArray = getHightDataForChart($weightStandardJsonMale);
                } else {
                    $hightStandardArray = getHightDataForChart($hightStandardJsonFeMale);
                    $weightStandardArray = getHightDataForChart($weightStandardJsonFeMale);
                }

                // 1. Chiều cao
                // Lấy 2 ngày gần nhất của $begin_day có dữ liệu, nội suy chuẩn của ngày bắt đầu
                $hightStandardArray[$begin_day]['highest'] = getStandardDataBeginAndEnd($hightStandardArray, $begin_day, 'highest');

                $hightStandardArray[$begin_day]['lowest'] = getStandardDataBeginAndEnd($hightStandardArray, $begin_day, 'lowest');

                // Lấy 2 ngày gần nhất của $end_day có dữ liệu, nội suy chuẩn của ngày kết thúc
                $hightStandardArray[$end_day]['highest'] = getStandardDataBeginAndEnd($hightStandardArray, $end_day, 'highest');

                $hightStandardArray[$end_day]['lowest'] = getStandardDataBeginAndEnd($hightStandardArray, $end_day, 'lowest');

                // dỮ LIỆU đường cao nhất (thư viện charts.js)
                $highestForChartChartsJS = getStandardDataForChartChartJS($hightStandardArray, $begin_day, $end_day, 'highest');

                // Dữ liệu đường thấp nhất (thư viện charts.js)
                $lowestForChartChartsJS = getStandardDataForChartChartJS($hightStandardArray, $begin_day, $end_day, 'lowest');

                // 2. Cân nặng
                // Lấy 2 ngày gần nhất của $begin_day có dữ liệu, nội suy chuẩn của ngày bắt đầu
                $weightStandardArray[$begin_day]['highest'] = getStandardDataBeginAndEnd($weightStandardArray, $begin_day, 'highest');

                $weightStandardArray[$begin_day]['lowest'] = getStandardDataBeginAndEnd($weightStandardArray, $begin_day, 'lowest');

                // Lấy 2 ngày gần nhất của $end_day có dữ liệu, nội suy chuẩn của ngày kết thúc
                $weightStandardArray[$end_day]['highest'] = getStandardDataBeginAndEnd($weightStandardArray, $end_day, 'highest');

                $weightStandardArray[$end_day]['lowest'] = getStandardDataBeginAndEnd($weightStandardArray, $end_day, 'lowest');

                // Dữ liệu đường cao nhất (charts.js)
                $highestWeightForChartChartJS = getStandardDataForChartChartJS($weightStandardArray, $begin_day, $end_day, 'highest');

                // Dữ liệu đường thấp nhất (charts.js)
                $lowestWeightForChartChartJS = getStandardDataForChartChartJS($weightStandardArray, $begin_day, $end_day, 'lowest');

                // 3. BMI (charts.js)
                $bmiForChartChartJS = getDataJsonForChartChartJS($childGrowth, $begin_day, $end_day, $birthday, 'bmi');
                // Get label
                $label = '';
                for($i = $begin_day; $i <= $end_day; $i++) {
                    if($i%30 == 0) {
                        $month = $i/30;
                        $label .= ' ' .$month. ',';
                    } else {
                        $label .= ' ,';
                    }
                }
                $smarty->assign('label', $label);
            }

            // Chiều cao for charts.js
            $smarty->assign('highestForChartChartJS', $highestForChartChartsJS);
            $smarty->assign('lowestForChartChartJS', $lowestForChartChartsJS);
            $smarty->assign('heightForChartChartJS', $heightForChartChartJS);

            //Cân nặng cho charts.js
            $smarty->assign('highestWeightForChartChartJS', $highestWeightForChartChartJS);
            $smarty->assign('lowestWeightForChartChartJS', $lowestWeightForChartChartJS);
            $smarty->assign('weightForChartChartJS', $weightForChartChartJS);

            // BMI (charts.js)
            $smarty->assign('bmiForChartChartJS', $bmiForChartChartJS);

            $smarty->assign('username', $class['group_name']);
            $smarty->assign('growth', array_reverse($childGrowth));
            $smarty->assign('child', $child);

            $return['results'] = $smarty->fetch("ci/class/ajax.class.chartsearch.tpl");
            //$return['lowestForChart'] = $smarty->fetch("ci/childinfo/ajax.child.chartsearch.tpl");

            return_json($return);
            break;
        case 'add_photo':
            // Thêm album ảnh nhật ký của trẻ
            if (!isset($_POST['child_id']) || !is_numeric($_POST['child_id'])) {
                _error(404);
            }
            $reload = (isset($_POST['reload']) && $_POST['reload'] == 1)?1:0;

            $db->begin_transaction();
            $child_parent_id = $childDao->getChildParentIdFromChildId($_POST['child_id']);
            $caption = isset($_POST['caption']) ? $_POST['caption'] : 'null';
            // get allowed file size
            $max_allowed_size = $system['max_photo_size'] * 1024;

            /* check & create uploads dir */
            $depth = '../../../../../';
            $folder = 'children';
            if(!file_exists($depth.$system['system_uploads_directory'].'/'.$folder)) {
                @mkdir($depth.$system['system_uploads_directory'].'/'.$folder, 0777, true);
            }
            if(!file_exists($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y'))) {
                @mkdir($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y'), 0777, true);
            }
            if(!file_exists($system['system_uploads_directory'].'/'.$folder.'/' . date('Y') . '/' . date('m'))) {
                @mkdir($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y') . '/' . date('m'), 0777, true);
            }

            $files = array();
            foreach ($_FILES['file'] as $key => $val) {
                for ($i = 0; $i < count($val); $i++) {
                    $files[$i][$key] = $val[$i];
                }
            }

            $return_files = array();
            $files_num = count($files);

            foreach ($files as $file) {

                // valid inputs
                if (!isset($file) || $file["error"] != UPLOAD_ERR_OK) {
                    if ($files_num > 1) {
                        continue;
                    } else {
//                        modal(MESSAGE, __("Upload Error"), __("Something wrong with upload! Is 'upload_max_filesize' set correctly?"));
                        return_json( array('error' => true, 'message' => __("You must add at least one image")) );
                    }
                }

                // check file extesnion
                $extension = get_extension($file['name']);
                if(!valid_extension($extension, $system['file_medicine_extensions'])) {
                    // modal(MESSAGE, __("Upload Error"), __("The file type is not valid or not supported"));
                    return_json( array('error' => true, 'message' => __("The file type is not valid or not supported")) );
                }

                // check file size
                if ($file["size"] > $max_allowed_size) {
                    if ($files_num > 1) {
                        continue;
                    } else {
                        //modal(MESSAGE, __("Upload Error"), __("The file size is so big"));
                        return_json( array('error' => true, 'message' => __("The file size is so big")) );
                    }
                }

                /* prepare new file name */
                $directory = $folder . '/' . date('Y') . '/' . date('m') . '/';
                $prefix = $system['uploads_prefix'] . '_' . md5(time() * rand(1, 9999));


                $image = new Image($file["tmp_name"]);

                $image_tmp_name = $directory . $prefix . '_tmp' . $image->_img_ext;
                $image_new_name = $directory . $prefix . $image->_img_ext;
                $path_tmp = $depth . $system['system_uploads_directory'] . '/' . $image_tmp_name;
                $path_new = $depth . $system['system_uploads_directory'] . '/' . $image_new_name;

                /* check if the file uploaded successfully */
                if (!@move_uploaded_file($file['tmp_name'], $path_tmp)) {
                    if ($files_num > 1) {
                        continue;
                    } else {
                        //modal(MESSAGE, __("Upload Error"), __("Sorry, can not upload the file"));
                        return_json( array('error' => true, 'message' => __("Sorry, can not upload the file")) );
                    }
                }

                /* save the new image */
                $image->save($path_new, $path_tmp);

                /* delete the tmp image */
                unlink($path_tmp);

                /* return */
                $return_files[] = $image_new_name;
            }
            //Thêm album nhật ký
            $isParent = 0;
            $journalAlbumId = $journalDao->insertJournalAlbum($child_parent_id, $caption, $isParent);
            // Thêm ảnh vào album
            $journalDao->insertJournal($journalAlbumId, $return_files);

            // Thông báo đến phụ huynh
            // Lấy danh sách phụ huynh của trẻ
            $parents = getChildData($_POST['child_id'], CHILD_PARENTS);
            $child = getChildData($_POST['child_id'], CHILD_INFO);
            $parentIds = array();
            if(count($parents) > 0) {
                foreach ($parents as $parent) {
                    $parentIds[] = $parent['user_id'];
                }

                // Thông báo cho phụ huynh có nhật ký mới
                $userDao->postNotifications($parentIds, NOTIFICATION_ADD_DIARY_SCHOOL, NOTIFICATION_NODE_TYPE_CHILD,
                    $journalAlbumId, convertText4Web($caption), $child_parent_id, convertText4Web($child['child_name']));
            }

            $db->commit();
            if($reload) {
                $module = $_POST['module'];
                if($module) {
                    return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/class/' . $_POST['username'] . '/diarys/add' . '";'));
                } else {
                    return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/class/' . $_POST['username'] . '/children/adddiary' . '";'));
                }
            } else {
                return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/class/' . $_POST['username'] . '/children/journal/' . $_POST['child_id'] . '";'));
            }
            break;
        case 'delete_photo':
            if (!isset($_POST['child_journal_id']) || !is_numeric($_POST['child_journal_id'])) {
                _error(404);
            }
            if (!isset($_POST['child_id']) || !is_numeric($_POST['child_id'])) {
                _error(404);
            }
            $db->begin_transaction();
            $journal_id = $journalDao->getJournalAlbumIdFromJournalId($_POST['child_journal_id']);
            $journal = $journalDao->getJournalById($journal_id);

            $journalDao->deleteJournal($_POST['child_journal_id']);

            // Thông báo đến phụ huynh
            // Lấy danh sách phụ huynh của trẻ
            $parents = getChildData($_POST['child_id'], CHILD_PARENTS);
            $child = getChildData($_POST['child_id'], CHILD_INFO);
            $parentIds = array();
            if(count($parents) > 0) {
                foreach ($parents as $parent) {
                    $parentIds[] = $parent['user_id'];
                }

                // Thông báo cho phụ huynh xóa ảnh trong album nhật ký
                $userDao->postNotifications($parentIds, NOTIFICATION_DELETE_PHOTO_DIARY_SCHOOL, NOTIFICATION_NODE_TYPE_CHILD,
                    $journal_id, convertText4Web($journal['caption']), $child['child_parent_id'], '');
            }

            $db->commit();
            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/class/' . $_POST['username'] . '/children/journal/' . $_POST['child_id'] . '";'));
            break;
        case 'delete_album':
            if (!isset($_POST['child_journal_album_id']) || !is_numeric($_POST['child_journal_album_id'])) {
                _api_error(404);
            }
            if (!isset($_POST['child_id']) || !is_numeric($_POST['child_id'])) {
                _api_error(404);
            }
            $db->begin_transaction();
            $journalDao->deleteJournalAlbum($_POST['child_journal_album_id']);
            $db->commit();
            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/class/' . $_POST['username'] . '/children/journal/' . $_POST['child_id'] . '";'));
            break;
        case 'edit_caption':
            if (!isset($_POST['child_journal_album_id']) || !is_numeric($_POST['child_journal_album_id'])) {
                _error(404);
            }
            if (!isset($_POST['child_id']) || !is_numeric($_POST['child_id'])) {
                _error(404);
            }
            $db->begin_transaction();
            $journal = $journalDao->getJournalById($_POST['child_journal_album_id']);
            $oldCaption = $journal['caption'];
            $journalDao->editCaptionAlbum($_POST['child_journal_album_id'], $_POST['caption']);

            // Thông báo đến phụ huynh
            // Lấy danh sách phụ huynh của trẻ
            $parents = getChildData($_POST['child_id'], CHILD_PARENTS);
            $child = getChildData($_POST['child_id'], CHILD_INFO);
            $parentIds = array();
            if(count($parents) > 0) {
                foreach ($parents as $parent) {
                    $parentIds[] = $parent['user_id'];
                }

                // Thông báo cho phụ huynh thêm ảnh trong album nhật ký
                $userDao->postNotifications($parentIds, NOTIFICATION_EDIT_CAPTION_DIARY_SCHOOL, NOTIFICATION_NODE_TYPE_CHILD,
                    $_POST['child_journal_album_id'], convertText4Web($oldCaption), $child['child_parent_id'], '');
            }

            $db->commit();
            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/class/' . $_POST['username'] . '/children/journal/' . $_POST['child_id'] . '";'));
            break;
        case 'add_photo_journal':
            // valid inputs
            if(!isset($_POST['child_journal_album_id']) || !is_numeric($_POST['child_journal_album_id'])) {
                _error(400);
            }
            $db->begin_transaction();

            // get allowed file size
            $max_allowed_size = $system['max_photo_size'] * 1024;

            /* check & create uploads dir */
            $depth = '../../../../../';
            $folder = 'children';
            if(!file_exists($depth.$system['system_uploads_directory'].'/'.$folder)) {
                @mkdir($depth.$system['system_uploads_directory'].'/'.$folder, 0777, true);
            }
            if(!file_exists($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y'))) {
                @mkdir($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y'), 0777, true);
            }
            if(!file_exists($system['system_uploads_directory'].'/'.$folder.'/' . date('Y') . '/' . date('m'))) {
                @mkdir($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y') . '/' . date('m'), 0777, true);
            }

            $files = array();
            foreach ($_FILES['file'] as $key => $val) {
                for ($i = 0; $i < count($val); $i++) {
                    $files[$i][$key] = $val[$i];
                }
            }

            $return_files = array();
            $files_num = count($files);

            foreach ($files as $file) {

                // valid inputs
                if (!isset($file) || $file["error"] != UPLOAD_ERR_OK) {
                    if ($files_num > 1) {
                        continue;
                    } else {
//                        modal(MESSAGE, __("Upload Error"), __("Something wrong with upload! Is 'upload_max_filesize' set correctly?"));
                        return_json( array('error' => true, 'message' => __("You must add at least one image")) );
                    }
                }

                // check file extesnion
                $extension = get_extension($file['name']);
                if(!valid_extension($extension, $system['file_medicine_extensions'])) {
                    // modal(MESSAGE, __("Upload Error"), __("The file type is not valid or not supported"));
                    return_json( array('error' => true, 'message' => __("The file type is not valid or not supported")) );
                }

                // check file size
                if ($file["size"] > $max_allowed_size) {
                    if ($files_num > 1) {
                        continue;
                    } else {
                        //modal(MESSAGE, __("Upload Error"), __("The file size is so big"));
                        return_json( array('error' => true, 'message' => __("The file size is so big")) );
                    }
                }

                /* prepare new file name */
                $directory = $folder . '/' . date('Y') . '/' . date('m') . '/';
                $prefix = $system['uploads_prefix'] . '_' . md5(time() * rand(1, 9999));


                $image = new Image($file["tmp_name"]);

                $image_tmp_name = $directory . $prefix . '_tmp' . $image->_img_ext;
                $image_new_name = $directory . $prefix . $image->_img_ext;
                $path_tmp = $depth . $system['system_uploads_directory'] . '/' . $image_tmp_name;
                $path_new = $depth . $system['system_uploads_directory'] . '/' . $image_new_name;

                /* check if the file uploaded successfully */
                if (!@move_uploaded_file($file['tmp_name'], $path_tmp)) {
                    if ($files_num > 1) {
                        continue;
                    } else {
                        //modal(MESSAGE, __("Upload Error"), __("Sorry, can not upload the file"));
                        return_json( array('error' => true, 'message' => __("Sorry, can not upload the file")) );
                    }
                }

                /* save the new image */
                $image->save($path_new, $path_tmp);

                /* delete the tmp image */
                unlink($path_tmp);

                /* return */
                $return_files[] = $image_new_name;
            }

            // Thêm ảnh vào album
            $journalDao->addPhotoForAlbum($_POST['child_journal_album_id'], $return_files);

            $journal = $journalDao->getJournalById($_POST['child_journal_album_id']);
            // Thông báo đến phụ huynh
            // Lấy danh sách phụ huynh của trẻ
            $parents = getChildData($_POST['child_id'], CHILD_PARENTS);
            $child = getChildData($_POST['child_id'], CHILD_INFO);
            $parentIds = array();
            if(count($parents) > 0) {
                foreach ($parents as $parent) {
                    $parentIds[] = $parent['user_id'];
                }

                // Thông báo cho phụ huynh thêm ảnh trong album nhật ký
                $userDao->postNotifications($parentIds, NOTIFICATION_ADD_PHOTO_DIARY_SCHOOL, NOTIFICATION_NODE_TYPE_CHILD,
                    $_POST['child_journal_album_id'], convertText4Web($journal['caption']), $child['child_parent_id'], '');
            }
//            $post_id = $user->add_album_photos($inputs);
            $db->commit();
            if($_POST['diary_module']) {
                return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/class/' . $_POST['username'] . '/diarys' . '";'));
            } else {
                return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/class/' . $_POST['username'] . '/children/journal/' . $_POST['child_id'] . '";'));
            }
            break;
        case 'search_diary':
            if(!isset($_POST['child_id']) || !is_numeric(($_POST['child_id']))) {
                _error(404);
            }
            $child_parent_id = $childDao->getChildParentIdFromChildId($_POST['child_id']);
            $results = $journalDao->getAllJournalChildForSchoolOfYear($child_parent_id, $_POST['year']);

            $child = getChildData($_POST['child_id'], CHILD_INFO);
            $smarty->assign('results', $results);
            $smarty->assign('child_parent_id', $child_parent_id);
            $smarty->assign('child', $child);
            $smarty->assign('username', $_POST['username']);
            $return['results'] = $smarty->fetch("ci/ajax.class.journal.list.tpl");
            $return['no_data'] = (count($results) == 0);
            return_json($return);
            break;
        case 'search_diary_child':
            // Hàm search ở màn hình góc nhật ký, không phải màn hình riêng từng trẻ
            if(!isset($_POST['child_id']) || !is_numeric($_POST['child_id'])) {
                throw new Exception(__("You have not picked a student yet"));
            }
            $isNewSearch = isset($_POST['is_new'])? $_POST['is_new']: 0;
            $condition = array();

            if($isNewSearch) { // Nếu là lần search mới
                // Đưa vào session để lần sau search lại
                $condition['class_id'] = $class['group_id'];
                $condition['child_id'] = $_POST['child_id'];
                $condition['year'] = $_POST['year'];
                $_SESSION[SESSION_KEY_SEARCH_DIARY_CLASS] = $condition;
            }
            $child_parent_id = $childDao->getChildParentIdFromChildId($_POST['child_id']);
            $child = getChildData($_POST['child_id'], CHILD_INFO);
            $results = array();

            $results = $journalDao->getAllJournalChildForSchoolOfYear($child_parent_id, $_POST['year']);
            $smarty->assign('results', $results);
            $smarty->assign('child_parent_id', $child_parent_id);
            $smarty->assign('child', $child);
            $smarty->assign('username', $_POST['username']);
            $smarty->assign('is_module', 1);
            $return['results'] = $smarty->fetch("ci/ajax.class.journal.list.tpl");
            $return['no_data'] = (count($results) == 0);
            return_json($return);
            break;
        case 'search_health_child':
            // Hàm search ở màn hình thông tin sức khỏe, không phải màn hình riêng từng trẻ
            if(!isset($_POST['child_id']) || !is_numeric($_POST['child_id'])) {
                throw new Exception(__("You have not picked a student yet"));
            }
            $isNewSearch = isset($_POST['is_new'])? $_POST['is_new']: 0;
            $condition = array();

            if($isNewSearch) { // Nếu là lần search mới
                // Đưa vào session để lần sau search lại
                $condition['class_id'] = $class['group_id'];
                $condition['child_id'] = $_POST['child_id'];
                $condition['begin'] = $_POST['begin'];
                $condition['end'] = $_POST['end'];
                $_SESSION[SESSION_KEY_SEARCH_CHILDREN_HEALTH_CLASS] = $condition;
            }
            $child_parent_id = $childDao->getChildParentIdFromChildId($_POST['child_id']);
            $child = getChildData($_POST['child_id'], CHILD_INFO);


            if($_POST['begin'] == '' || !validateDate($_POST['begin']) ) {
                $begin = null;
            } else {
                $begin = $_POST['begin'];
            }
            if($_POST['end'] == '' || !validateDate($_POST['end'])) {
                $end = null;
            } else {
                $end = $_POST['end'];
            }

            // Lấy ngày hiện tại
            $dateNow = date('Y-m-d');

            $child_parent_id = $childDao->getChildParentIdFromChildId($_POST['child_id']);
            // Lấy thông tin trẻ
            // $child = $childDao->getChildByParent($child_parent_id);
            $child = getChildData($_POST['child_id'], CHILD_INFO);

            // Lấy ngày sinh nhật của trẻ
            $birthday = toDBDate($child['birthday']);

            // Khởi tạo mảng lưu chỉ số sức khỏe của trẻ
            $childGrowth = array();

            if(is_null($begin) || is_null($end)) {
                // Lấy toàn bộ thông tin sức khỏe của trẻ
                $childGrowth = $childDao->getChildGrowthForChart($child['child_parent_id']);

                // Tính xem trẻ được bao nhiêu ngày tuổi
                $day_age_now = (strtotime($dateNow) - strtotime($birthday)) / (60*60*24);

                // đoạn này xử lý để lấy ngày bắt đầu (đặt tạm a, b, c; bí tên quá)
                $a = $day_age_now % 30;
                $b = $day_age_now - $a;
                $c = $b - 30*10;
                $dayGet = (int)$c;
                $day_age_max = $day_age_now + 65;
                if($day_age_now >= 300) {
                    $day_age_min = $dayGet;
                } else {
                    $day_age_min = 0;
                }

                // Lấy dữ liệu người dùng nhập vào vẽ biểu đồ
                $heightForChartChartJS = getDataJsonForChartChartJS($childGrowth, $day_age_min, $day_age_max, $birthday, 'height');
                $weightForChartChartJS = getDataJsonForChartChartJS($childGrowth, $day_age_min, $day_age_max, $birthday, 'weight');
                if($child['gender'] == 'male') {
                    // Dữ liệu chuẩn
                    $hightStandardArray = getHightDataForChart($hightStandardJsonMale);
                    $weightStandardArray = getHightDataForChart($weightStandardJsonMale);
                } else {
                    $hightStandardArray = getHightDataForChart($hightStandardJsonFeMale);
                    $weightStandardArray = getHightDataForChart($weightStandardJsonFeMale);

                }
                // 1. Chiều cao
                // Lấy 2 ngày gần nhất của $begin_day có dữ liệu, nội suy chuẩn của ngày bắt đầu
                $hightStandardArray[$day_age_min]['highest'] = getStandardDataBeginAndEnd($hightStandardArray, $day_age_min, 'highest');

                $hightStandardArray[$day_age_min]['lowest'] = getStandardDataBeginAndEnd($hightStandardArray, $day_age_min, 'lowest');

                // Lấy 2 ngày gần nhất của $end_day có dữ liệu, nội suy chuẩn của ngày kết thúc
                $hightStandardArray[$day_age_max]['highest'] = getStandardDataBeginAndEnd($hightStandardArray, $day_age_max, 'highest');

                $hightStandardArray[$day_age_max]['lowest'] = getStandardDataBeginAndEnd($hightStandardArray, $day_age_max, 'lowest');

                // dỮ LIỆU đường cao nhất (thư viện charts.js)
                $highestForChartChartsJS = getStandardDataForChartChartJS($hightStandardArray, $day_age_min, $day_age_max, 'highest');

                // Dữ liệu đường thấp nhất (thư viện charts.js)
                $lowestForChartChartsJS = getStandardDataForChartChartJS($hightStandardArray, $day_age_min, $day_age_max, 'lowest');

                // 2. Cân nặng
                // Lấy 2 ngày gần nhất của $begin_day có dữ liệu, nội suy chuẩn của ngày bắt đầu
                $weightStandardArray[$day_age_min]['highest'] = getStandardDataBeginAndEnd($weightStandardArray, $day_age_min, 'highest');

                $weightStandardArray[$day_age_min]['lowest'] = getStandardDataBeginAndEnd($weightStandardArray, $day_age_min, 'lowest');

                // Lấy 2 ngày gần nhất của $end_day có dữ liệu, nội suy chuẩn của ngày kết thúc
                $weightStandardArray[$day_age_max]['highest'] = getStandardDataBeginAndEnd($weightStandardArray, $day_age_max, 'highest');

                $weightStandardArray[$day_age_max]['lowest'] = getStandardDataBeginAndEnd($weightStandardArray, $day_age_max, 'lowest');

                // Dữ liệu đường cao nhất (charts.js)
                $highestWeightForChartChartJS = getStandardDataForChartChartJS($weightStandardArray, $day_age_min, $day_age_max, 'highest');

                // Dữ liệu đường thấp nhất (charts.js)
                $lowestWeightForChartChartJS = getStandardDataForChartChartJS($weightStandardArray, $day_age_min, $day_age_max, 'lowest');

                // 3. BMI (charts.js)
                $bmiForChartChartJS = getDataJsonForChartChartJS($childGrowth, $day_age_min, $day_age_max, $birthday, 'bmi');

                // Get label
                $label = '';
                for($i = $day_age_min; $i <= $day_age_max; $i++) {
                    if($i%30 == 0) {
                        $month = $i/30;
                        $label .= ' ' .$month. ',';
                    } else {
                        $label .= ' ,';
                    }
                }
                $smarty->assign('label', $label);
            } else {
                // Lấy dữ liệu nhập và của trẻ
                $childGrowth = $childDao->getChildGrowthSearchForChartBeginEnd($child['child_parent_id'], $begin, $end);

                // Lấy ngày bắt đầu và ngày kết thúc
                $begin = toDBDate($begin);
                $end = toDBDate($end);

                $end_day = (strtotime($end) - strtotime($birthday)) / (60*60*24);
                $end_day = $end_day / 30;
                $end_day = (int)$end_day;
                $end_day = ($end_day + 1) * 30;

                $begin_day = (strtotime($begin) - strtotime($birthday)) / (60*60*24);
                $begin_day = $begin_day / 30;
                $begin_day = (int)$begin_day;
                $begin_day = $begin_day * 30;
                if($begin_day < 0) {
                    throw new Exception(__("You must select day begin after birthday"));
                }

                // Dữ liệu chuẩn người dùng nhập vào vẽ biểu đồ chiều cao (thư viện charts.js)
                $heightForChartChartJS = getDataJsonForChartChartJS($childGrowth, $begin_day, $end_day, $birthday, 'height');

                // Lấy dữ liệu người dùng nhập vào vẽ biểu đồ cân nặng (charts.js)
                $weightForChartChartJS = getDataJsonForChartChartJS($childGrowth, $begin_day, $end_day, $birthday, 'weight');

                if($child['gender'] == 'male') {
                    // Dữ liệu chuẩn
                    $hightStandardArray = getHightDataForChart($hightStandardJsonMale);
                    $weightStandardArray = getHightDataForChart($weightStandardJsonMale);
                } else {
                    $hightStandardArray = getHightDataForChart($hightStandardJsonFeMale);
                    $weightStandardArray = getHightDataForChart($weightStandardJsonFeMale);
                }

                // 1. Chiều cao
                // Lấy 2 ngày gần nhất của $begin_day có dữ liệu, nội suy chuẩn của ngày bắt đầu
                $hightStandardArray[$begin_day]['highest'] = getStandardDataBeginAndEnd($hightStandardArray, $begin_day, 'highest');

                $hightStandardArray[$begin_day]['lowest'] = getStandardDataBeginAndEnd($hightStandardArray, $begin_day, 'lowest');

                // Lấy 2 ngày gần nhất của $end_day có dữ liệu, nội suy chuẩn của ngày kết thúc
                $hightStandardArray[$end_day]['highest'] = getStandardDataBeginAndEnd($hightStandardArray, $end_day, 'highest');

                $hightStandardArray[$end_day]['lowest'] = getStandardDataBeginAndEnd($hightStandardArray, $end_day, 'lowest');

                // dỮ LIỆU đường cao nhất (thư viện charts.js)
                $highestForChartChartsJS = getStandardDataForChartChartJS($hightStandardArray, $begin_day, $end_day, 'highest');

                // Dữ liệu đường thấp nhất (thư viện charts.js)
                $lowestForChartChartsJS = getStandardDataForChartChartJS($hightStandardArray, $begin_day, $end_day, 'lowest');

                // 2. Cân nặng
                // Lấy 2 ngày gần nhất của $begin_day có dữ liệu, nội suy chuẩn của ngày bắt đầu
                $weightStandardArray[$begin_day]['highest'] = getStandardDataBeginAndEnd($weightStandardArray, $begin_day, 'highest');

                $weightStandardArray[$begin_day]['lowest'] = getStandardDataBeginAndEnd($weightStandardArray, $begin_day, 'lowest');

                // Lấy 2 ngày gần nhất của $end_day có dữ liệu, nội suy chuẩn của ngày kết thúc
                $weightStandardArray[$end_day]['highest'] = getStandardDataBeginAndEnd($weightStandardArray, $end_day, 'highest');

                $weightStandardArray[$end_day]['lowest'] = getStandardDataBeginAndEnd($weightStandardArray, $end_day, 'lowest');

                // Dữ liệu đường cao nhất (charts.js)
                $highestWeightForChartChartJS = getStandardDataForChartChartJS($weightStandardArray, $begin_day, $end_day, 'highest');

                // Dữ liệu đường thấp nhất (charts.js)
                $lowestWeightForChartChartJS = getStandardDataForChartChartJS($weightStandardArray, $begin_day, $end_day, 'lowest');

                // 3. BMI (charts.js)
                $bmiForChartChartJS = getDataJsonForChartChartJS($childGrowth, $begin_day, $end_day, $birthday, 'bmi');
                // Get label
                $label = '';
                for($i = $begin_day; $i <= $end_day; $i++) {
                    if($i%30 == 0) {
                        $month = $i/30;
                        $label .= ' ' .$month. ',';
                    } else {
                        $label .= ' ,';
                    }
                }
                $smarty->assign('label', $label);
            }

            // Chiều cao for charts.js
            $smarty->assign('highestForChartChartJS', $highestForChartChartsJS);
            $smarty->assign('lowestForChartChartJS', $lowestForChartChartsJS);
            $smarty->assign('heightForChartChartJS', $heightForChartChartJS);

            //Cân nặng cho charts.js
            $smarty->assign('highestWeightForChartChartJS', $highestWeightForChartChartJS);
            $smarty->assign('lowestWeightForChartChartJS', $lowestWeightForChartChartJS);
            $smarty->assign('weightForChartChartJS', $weightForChartChartJS);

            // BMI (charts.js)
            $smarty->assign('bmiForChartChartJS', $bmiForChartChartJS);

            $smarty->assign('username', $class['group_name']);
            $smarty->assign('growth', array_reverse($childGrowth));

            $smarty->assign('child_parent_id', $child_parent_id);
            $smarty->assign('child', $child);
            $smarty->assign('username', $_POST['username']);
            $smarty->assign('is_module', 1);

            $return['results'] = $smarty->fetch("ci/class/ajax.class.chartsearch.tpl");
            return_json($return);
            break;
        default:
            _error(400);
            break;
    }

	// return & exit
	return_json($return);
	
} catch (Exception $e) {
    $db->rollback();
	return_json( array('error' => true, 'message' => $e->getMessage()) );
} finally {
    $db->autocommit(true);
}

?>