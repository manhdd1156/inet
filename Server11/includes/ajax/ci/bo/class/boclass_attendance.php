<?php
/**
 * Package: ajax/ci/bo
 * 
 * @package ConIu
 * @author QuanND
 */

// fetch bootstrap
require('../../../../../bootstrap.php');

// check AJAX Request
is_ajax();
check_login();

include_once(DAO_PATH . 'dao_school.php');
include_once(DAO_PATH . 'dao_attendance.php');
include_once(DAO_PATH . 'dao_parent.php');
include_once(DAO_PATH . 'dao_teacher.php');
include_once(DAO_PATH . 'dao_user.php');
include_once(DAO_PATH . 'dao_class.php');
include_once(DAO_PATH . 'dao_child.php');
$schoolDao = new SchoolDAO();
$attendanceDao = new AttendanceDAO();
$parentDao = new ParentDAO();
$teacherDao = new TeacherDAO();
$userDao = new UserDAO();
$classDao = new ClassDAO();
$childDao = new ChildDAO();

//Lấy ra thông tin lớp
//$class = $classDao->getClassByUsername($_POST['username'], $user->_data['user_id']);
$class = getClassDataByUsername($_POST['username'], CLASS_INFO);
if (is_null($class)) {
    _error(403);
}
$smarty->assign('username', $_POST['username']);

//$schoolConfig = $schoolDao->getConfiguration($class['school_id']);
$schoolConfig = getSchoolData($class['school_id'], SCHOOL_CONFIG);
//$notify_settings = $schoolDao->getUserNotificationSettingOfSchool($class['school_id']);
$notify_settings = getSchoolData($class['school_id'], SCHOOL_NOTIFICATIONS);

try {
    $return = array();
    $db->autocommit(false);
    switch ($_POST['do']) {
        case 'rollup':
            //Xử lý nghiệp vụ khi cô giáo điểm danh lớp
            $args = array();
            $args['attendance_date'] = $_POST['attendance_date'];
            $attendanceDate = toDBDate($args['attendance_date']);
            $dateNow = toDBDate(toSysDate($date));
            if (($schoolConfig['allow_teacher_rolls_days_before'] == 0) && (strtotime($attendanceDate) < strtotime($dateNow))) {
                throw new Exception(__("You do not have permission rollup dates in the past, please contact the school administration"));
            }

            $args['class_id'] = $class['group_id'];

            //Kiểm tra xem lớp đã có bản ghi điểm danh chưa
            $attendanceId = $attendanceDao->getAttendanceId($args['class_id'], $args['attendance_date']);
            //$attendanceId = isset($_POST['attendance_id'])? $_POST['attendance_id']: 0;

            $allChildIds = $_POST['allChildIds'];
            $allStatus = array();
            foreach ($allChildIds as $child_id) {
                $allStatus[] = $_POST['rollup_' . $child_id];
            }
            $allReasons = $_POST['allReasons'];
            $absenceCount = 0;
            $presentCount = 0;
            foreach ($allStatus as $status) {
                if (($status == ATTENDANCE_ABSENCE) || ($status == ATTENDANCE_ABSENCE_NO_REASON)) {
                    $absenceCount++;
                } else {
                    //ATTENDANCE_PRESENT, ATTENDANCE_COME_LATE, ATTENDANCE_EARLY_LEAVE
                    $presentCount++;
                }
            }

            $args['absence_count'] = $absenceCount;
            $args['present_count'] = $presentCount;
            $args['is_checked'] = 1;

            $db->begin_transaction();
            if ($attendanceId > 0) {
                // Lấy trạng thái điểm danh của cả lớp theo attendance_id
                $attendance = $attendanceDao->getAttendance($attendanceId, $school['page_id'], $class['group_id']);

                $attendanceChildIds = array();
                // Lặp danh sách điểm danh chi tiết, đưa vào mảng để tiện so sánh
                $childrenAtt = array();
                foreach ($attendance['detail'] as $row) {
                    if(isset($row['attendance_detail_id']) && $row['attendance_detail_id'] > 0) {
                        $childrenAtt[$row['child_id']]['status'] = $row['status'];
                        $childrenAtt[$row['child_id']]['reason'] = $row['reason'];
                        $childrenAtt[$row['child_id']]['is_parent'] = $row['is_parent'];
                        $childrenAtt[$row['child_id']]['feedback'] = $row['feedback'];
                        $childrenAtt[$row['child_id']]['attendance_detail_id'] = $row['attendance_detail_id'];
                    } else {
                        $childrenAtt[$row['child_id']]['status'] = '';
                        $childrenAtt[$row['child_id']]['reason'] = '';
                        $childrenAtt[$row['child_id']]['is_parent'] = '';
                        $childrenAtt[$row['child_id']]['feedback'] = '';
                        $childrenAtt[$row['child_id']]['attendance_detail_id'] = '';
                    }

                    // Lấy danh sách id những trẻ đã điểm danh rồi
                    $attendanceChildIds[] = $row['child_id'];
                }

                // Lấy danh sách những trẻ có trong lớp nhưng chưa được điểm danh trước đó
                $childIdsNew = array_diff($allChildIds, $attendanceChildIds);
                foreach ($childIdsNew as $childId) {
                    $childrenAtt[$childId]['status'] = '';
                    $childrenAtt[$childId]['reason'] = '';
                    $childrenAtt[$childId]['is_parent'] = '';
                    $childrenAtt[$childId]['feedback'] = '';
                    $childrenAtt[$childId]['attendance_detail_id'] = '';
                }

                //print_r($childrenAtt); die;
                // Lặp danh sách id trẻ và trạng thái điểm danh
                foreach ($childrenAtt as $childId => $att) {
                    // kiểm tra nếu trạng thái khác thì thông báo update, nếu không có trạng thái điểm danh trước đó thì thông báo điểm danh mới
                    if(in_array($childId, $allChildIds) && $att['status'] != '') {
                        // trẻ đã điểm danh trước đó - update
                        // Lặp danh sách id gửi lên

                        $parents = getChildData($childId, CHILD_PARENTS);
                        foreach ($allChildIds as $k => $id) {
                            // Nếu phụ huynh xin nghỉ mà chưa xác nhận
                            if($id == $childId && $att['is_parent'] == 1 && $att['feedback'] == 0) {
                                // Cập nhật trạng thái đã xác nhận và gửi thông báo về cho phụ huynh
                                $attendanceDao->updateAttendanceDetailStatus($att['attendance_detail_id']);
                                $parentIds = array_keys($parents);
                                $userDao->postNotifications($parentIds, NOTIFICATION_ATTENDANCE_CONFIRM, NOTIFICATION_NODE_TYPE_CHILD, $att['attendance_detail_id'], '', $childId);
                            }

                            // Nếu thay đổi trạng thái điểm danh thì gửi thông báo update điểm danh
                            if($id == $childId && ($allStatus[$k] != $att['status'] || $allReasons[$k] != $att['reason'])) {
                                // Cập nhật chi tiết điểm danh
                                $attendanceDao->updateChildAttendanceDetail($attendanceId, $childId, $allStatus[$k], $allReasons[$k]);

                                // Thông báo cập nhật điểm danh
                                //$child = $childDao->getChild($_POST['child_id']);
                                $child = getChildData($childId, CHILD_INFO);

                                if (!is_null($child) && !is_null($parents)) {
                                    foreach ($parents as $parent) {
                                        $userDao->postNotifications($parent['user_id'], NOTIFICATION_UPDATE_ATTENDANCE, NOTIFICATION_NODE_TYPE_CHILD,
                                            $allStatus[$k], $args['attendance_date'], $childId, convertText4Web($child['child_name']));
                                    }
                                }
                            }
                        }
                    } else {
                        // Lấy trạng thái điểm danh của trẻ
                        if(in_array($childId, $allChildIds)) {
                            foreach ($allChildIds as $k => $allChildId) {
                                if($allChildId == $childId) {
                                    $status = $allStatus[$k];
                                    $reason = $allReasons[$k];
                                }
                            }

                            // insert thông tin điểm danh của trẻ vào bảng ci_attendance_detail
                            $attendanceDao->saveAttendanceDetailChild($attendanceId, $childId, $status, $reason);

                            // Thông báo đến phụ huynh trẻ được điểm danh mới
                            //$child = $childDao->getChild($_POST['child_id']);
                            $child = getChildData($childId, CHILD_INFO);
                            //$parents = array();
                            //$parents = $parentDao->getParent($_POST['child_id']);
                            $parents = getChildData($childId, CHILD_PARENTS);

                            if (!is_null($child) && !is_null($parents)) {
                                $parentIds = array_keys($parents);
                                $userDao->postNotifications($parentIds, NOTIFICATION_NEW_ATTENDANCE, NOTIFICATION_NODE_TYPE_CHILD,
                                    $status, $args['attendance_date'], $childId, convertText4Web($child['child_name']));
                            }
                        }
                    }
                }
                $attendanceDao->updateAttendance($args);
            } else {
                $attendanceId = $attendanceDao->insertAttendance($args);

                foreach ($allChildIds as $k => $childId) {
                    // Lặp danh sách trẻ, gửi thông báo về cho phụ huynh
                    // Thông báo điểm danh
                    //$child = $childDao->getChild($_POST['child_id']);
                    $child = getChildData($childId, CHILD_INFO);
                    //$parents = array();
                    //$parents = $parentDao->getParent($_POST['child_id']);
                    $parents = getChildData($childId, CHILD_PARENTS);

                    if (!is_null($child) && !is_null($parents)) {
                        $parentIds = array_keys($parents);
                        $userDao->postNotifications($parentIds, NOTIFICATION_NEW_ATTENDANCE, NOTIFICATION_NODE_TYPE_CHILD,
                            $allStatus[$k], $args['attendance_date'], $childId, convertText4Web($child['child_name']));
                    }
                }

                // Insert điểm danh của cả lớp
                //$allResignTime = $_POST['allResignTime'];
                //$allIsParent = $_POST['allIsParent'];
                //$allRecordUserId = $_POST['allRecordUserId'];
                //$allStartDate = $_POST['allStartDate'];
                //$allEndDate = $_POST['allEndDate'];
//                foreach ($allChildIds as $child_id) {
//                    $allFeedback[] = $_POST['feedback_' . $child_id];
//                }
                $attendanceDao->saveAttendanceDetail($attendanceId, $allChildIds, $allStatus, $allReasons);

                // TaiLA  - Tạo hàm lưu thông tin điểm danh chi tiết mới
                //$attendanceDao->saveAttendanceDetailNew($attendanceId, $allChildIds, $allStatus, $allReasons, $allResignTime, $allIsParent, $allRecordUserId, $allStartDate, $allEndDate, $allFeedback);
            }
            //$attendanceDao->saveAttendanceDetail($attendanceId, $presentChildIds, $absentChildIds, $absentReasons);
            $db->commit();
            return_json(array('callback' => 'window.location = "'.$system['system_url'].'/class/'.$_POST['username'].'/attendance";'));
            break;

        case 'save_rollup_back': //Xử lý nghiệp vụ khi lưu ĐIỂM DANH về NGÀY CỦA MỘT LỚP
            $args = array();
            $args['attendance_date'] = $_POST['attendance_date'];
            $attendanceDate = toDBDate($args['attendance_date']);
            $dateNow = toDBDate(toSysDate($date));

            $args['class_id'] = $class['group_id'];

            //Kiểm tra xem lớp đã có bản ghi điểm danh về chưa
            $attendanceBackId = $attendanceDao->getAttendanceBackId($args['class_id'], $args['attendance_date']);

            $allChildIds = $_POST['allChildBackIds'];

            $allStatus = array();
            foreach ($allChildIds as $child_id) {
                if(isset($_POST['status_' . $child_id])) {
                    $allStatus[] = 1;
                } else {
                    $allStatus[] = 0;
                }

            }
            //echo "<pre>"; print_r($allStatus); die;
            //$allStatus = $_POST['allStatus'];
            $allCameBackTimes = $_POST['cameback_time'];
            $allCameBackNotes = $_POST['cameback_note'];
            // echo "<pre>"; print_r($allStatus); die;
            $cameBackCount = 0;
            $notCameBackCount = 0;
            foreach ($allStatus as $status) {
                if ($status == 1) {
                    $cameBackCount++;
                } else {
                    //ATTENDANCE_PRESENT, ATTENDANCE_COME_LATE, ATTENDANCE_EARLY_LEAVE
                    $notCameBackCount++;
                }
            }

            $args['cameback_count'] = $cameBackCount;
            $args['not_cameback_count'] = $notCameBackCount;

            $db->begin_transaction();
            if ($attendanceBackId > 0) {
                // Lấy trạng thái điểm danh về của cả lớp theo attendance_back_id
                $attendance = $attendanceDao->getAttendanceBack($attendanceBackId, $class['school_id']);

                // Lặp danh sách điểm danh chi tiết, đưa vào mảng để tiện so sánh
                $childrenAtt = array();
                $childCameBack = array();
                foreach ($attendance['detail'] as $row) {
                    if(isset($row['attendance_back_detail_id']) && $row['attendance_back_detail_id'] > 0) {
                        $childrenAtt[$row['child_id']] = $row['status'];
                    } else {
                        $childrenAtt[$row['child_id']] = '';
                    }
                    $childCameBack[] = $row['child_id'];
                }

                //echo "<pre>"; print_r($allChildIds); print_r($childCameBack); die;

                foreach ($allChildIds as $k => $childId) {
                    if(in_array($childId, $childCameBack)) {
                        if($allStatus[$k] == 0) {
                            // Gửi thông báo update trẻ chưa về
                        } else {
                            // Không gửi thông báo gì
                        }
                    } else {
                        $status = $allStatus[$k];
                        if($status == 1) {
                            // Thông báo đến phụ huynh trẻ được điểm danh về
                            //$child = $childDao->getChild($_POST['child_id']);
                            $child = getChildData($childId, CHILD_INFO);
                            //$parents = array();
                            //$parents = $parentDao->getParent($_POST['child_id']);
                            $parents = getChildData($childId, CHILD_PARENTS);

                            if (!is_null($child) && !is_null($parents)) {
                                $parentIds = array();
                                foreach ($parents as $parent) {
                                    $parentIds[] = $parent['user_id'];
                                }
                                $userDao->postNotifications($parentIds, NOTIFICATION_NEW_ATTENDANCE_BACK, NOTIFICATION_NODE_TYPE_CHILD,
                                    $status, $allCameBackTimes[$k], $childId, convertText4Web($child['child_name']));
                            }
                        }
                    }
                }

                $attendanceDao->updateAttendanceBack($args);
                $attendanceId = $attendanceBackId;
            } else {
                $attendanceId = $attendanceDao->insertAttendanceBack($args);
                foreach ($allChildIds as $k => $childId) {
                    if($allStatus[$k] == 1) {
                        // Lặp danh sách trẻ, gửi thông báo về cho phụ huynh
                        // Thông báo điểm danh
                        //$child = $childDao->getChild($_POST['child_id']);
                        $child = getChildData($childId, CHILD_INFO);
                        //$parents = array();
                        //$parents = $parentDao->getParent($_POST['child_id']);
                        $parents = getChildData($childId, CHILD_PARENTS);
                        if (!is_null($child) && !is_null($parents)) {
                            $parentIds = array();
                            foreach ($parents as $parent) {
                                $parentIds[] = $parent['user_id'];
                            }
                            $userDao->postNotifications($parentIds, NOTIFICATION_NEW_ATTENDANCE_BACK, NOTIFICATION_NODE_TYPE_CHILD,
                                $allStatus[$k], $allCameBackTimes[$k], $childId, convertText4Web($child['child_name']));
                        }
                    }
                }
            }
            $attendanceDao->saveAttendanceBackDetail($attendanceId, $allChildIds, $allStatus, $allCameBackTimes, $allCameBackNotes);

//            echo "<pre>";
//            print_r($allCameBackTimes); die;
            $db->commit();
            return_json( array('success' => true, 'message' => __("Class attendance have been updated")) );
            break;

        case 'update_child':
            if(!isset($_POST['child_id']) || !is_numeric($_POST['child_id'])) {
                _error(404);
            }

            $attendanceIds = isset($_POST['attendanceIds'])? $_POST['attendanceIds']: array();
            $childId = $_POST['child_id'];
            $allStatus = $_POST['allStatus'];
            $allReasons = $_POST['allReasons'];
            $allOldStatus = $_POST['allOldStatus'];

            if (count($attendanceIds) > 0) {
                $db->begin_transaction();

                $attendanceDao->updateAttendanceOfChild($childId, $attendanceIds, $allStatus, $allOldStatus, $allReasons);

                $db->commit();
                return_json( array('success' => true, 'message' => __("Done, Student attendance have been updated")) );
            }

            break;

        case 'list':
            $results = $attendanceDao->getAttendanceDetail($class['group_id'], $_POST['attendance_date']);

            foreach ($results['detail'] as $k => $row) {
                // Lấy ngày trẻ bắt đầu đi học trong lớp
                $begin_at = $childDao->getChildBeginAtInClass($row['child_id'], $class['group_id']);
                // lấy ngày đầu tháng hiện tại
                $firstDayofMonthNow = date('Y-m-01');
                $attendanceDate = toDBDate($_POST['attendance_date']);
                if((strtotime($begin_at) - strtotime($firstDayofMonthNow)) > 0 && (strtotime($firstDayofMonthNow) - strtotime($attendanceDate)) > 0) {
                    unset($results['detail'][$k]);
//                    $att[] = $row;
                }
            }
            // Lấy thông tin điểm danh về của lớp
            $data_cameback = $attendanceDao->getAttendanceCameBackdetail($class['group_id'], $_POST['attendance_date']);
            for($i = 0; $i < count($data_cameback['detail']); $i++) {
                $data_cameback['detail'][$i]['status'] = $results['detail'][$i]['status'];
            }

            // Lấy giờ phút hiện tại
            $hour = date('H:i');
            $return['disableSave'] = (count($results) == 0);
            if (count($results) > 0) {
                /* assign variables */
                $smarty->assign('data', $results);
                $smarty->assign('hour', $hour);
                $smarty->assign('data_cameback', $data_cameback);
                $smarty->assign('schoolConfig', $schoolConfig);
                /* return */
                $return['results'] = $smarty->fetch("ci/class/ajax.class.attendancelist.tpl");
            }

            return_json($return);
            break;

        case 'search':
            $end = $_POST['toDate'];
            $begin = $_POST['fromDate'];

            //Lấy ra điểm danh của một lớp
            $results = $attendanceDao->getFullAttendanceOfClass($class['group_id'], $begin, $end);

            //Tạo ra danh sách các ngày để build lên header của bảng.
            $fromDate = strtotime(toDBDate($begin));
            $toDate = strtotime(toDBDate($end));

            $displayDates = array();
            while ($fromDate <= $toDate) {
                $parts = explode("-",date("Y-m-d", $fromDate));
                $displayDates[] = $parts[2];
                $fromDate = strtotime("+1 day", $fromDate);
            }
            $smarty->assign('dates', $displayDates);

            //Mãng 2 chiều lưu bảng điểm danh toàn lớp
            $newResults = array();

            //Băm danh sách kết quả thành mãng 2 chiều theo từng cháu
            $lastChildId = -1;
            $aRow = array(); //Dòng lưu thông tin điểm danh 1 trẻ (chỉ những ngày có điểm danh).
            foreach ($results as $result) {
                //Nếu child_id thay đổi thì bắt đầu một dòng mới.
                if ($lastChildId != $result['child_id']) {

                    //Nếu trước đó là một dòng (không phải là dòng đầu tiên), thì xử lý bổ xung dòng đã xong trước đó.
                    if ($lastChildId > 0) {
                        $isFirstCell = true; //Check có phải cell đầu dòng hay không
                        $newRow = array(); //Dòng lưu thông tin điểm danh 1 trẻ (bao gồm những ngày không điểm danh).
                        $newCells = array();
                        $lastDate = strtotime(toDBDate($begin));
                        foreach ($aRow as $cell) {
                            //Xét ngày đầu hàng là ngày nghỉ thì thêm thành phần vào
                            while ($isFirstCell && ($lastDate < strtotime($cell['attendance_date']))) {
                                $newCells[] = array("is_checked" => 0);
                                $lastDate = strtotime("+1 day", $lastDate);
                            }

                            //Xét thành phần giữa hàng, nếu có ngày nghỉ thì chèn thêm vào
                            while ((!$isFirstCell) && (strtotime("+1 day", $lastDate) < strtotime($cell['attendance_date']))) {
                                $newCells[] = array("is_checked" => 0);
                                $lastDate = strtotime("+1 day", $lastDate);
                            }

                            $newCells[] = $cell;
                            $newRow['child_name'] = $cell['child_name'];
                            $newRow['child_id'] = $cell['child_id'];
                            $newRow['child_status'] = $cell['child_status'];
                            $isFirstCell = false;
                            $lastDate = strtotime($cell['attendance_date']);
                        }
                        //Xét cuối hàng, nếu những ngày cuối hàng là ngày nghỉ thì thêm ô trống
                        while ($lastDate < strtotime(toDBDate($end))) {
                            $newCells[] = array("is_checked" => 0);
                            $lastDate = strtotime("+1 day", $lastDate);
                        }
                        $newRow['cells'] = $newCells;
                        $newResults[] = $newRow;
                    }
                    $aRow = array();
                }

                $aRow[] = $result;
                $lastChildId = $result['child_id'];
            }

            $twoLastRows = array(); //Biến này lưu tổng kết số lượng đi/nghỉ theo ngày của lớp.

            //Xử lý hàng cuối cùng (cháu cuối cùng).
            if ($lastChildId > 0) {
                $isFirstCell = true;
                $newRow = array(); //Dòng lưu thông tin điểm danh 1 trẻ (bao gồm những ngày không điểm danh).
                $newCells = array();
                $lastDate = strtotime(toDBDate($begin));
                foreach ($aRow as $cell) {
                    //Xét ngày đầu hàng là ngày nghỉ thì thêm thành phần vào
                    while ($isFirstCell && ($lastDate < strtotime($cell['attendance_date']))) {
                        $newCells[] = array("is_checked" => 0);
                        $lastDate = strtotime("+1 day", $lastDate);

                        $twoLastRows[] = array("is_checked" => 0);
                    }

                    //Xét thành phần giữa hàng, nếu có ngày nghỉ thì chèn thêm vào
                    while ((!$isFirstCell) && (strtotime("+1 day", $lastDate) < strtotime($cell['attendance_date']))) {
                        $newCells[] = array("is_checked" => 0);
                        $lastDate = strtotime("+1 day", $lastDate);

                        $twoLastRows[] = array("is_checked" => 0);
                    }

                    $newCells[] = $cell;
                    $newRow['child_name'] = $cell['child_name'];
                    $newRow['child_id'] = $cell['child_id'];
                    $newRow['child_status'] = $cell['child_status'];

                    $isFirstCell = false;
                    $lastDate = strtotime($cell['attendance_date']);

                    $twoLastRows[] = array("attendance_id" => $cell['attendance_id'], "is_checked" => 1, "present_count" => $cell['present_count'], "absence_count" => $cell['absence_count']);
                }
                //Xét cuối hàng, nếu những ngày cuối hàng là ngày nghỉ thì thêm ô trống
                while ($lastDate < strtotime(toDBDate($end))) {
                    $newCells[] = array("is_checked" => 0);
                    $lastDate = strtotime("+1 day", $lastDate);

                    $twoLastRows[] = array("is_checked" => 0);
                }
                $newRow['cells'] = $newCells;
                $newResults[] = $newRow;
            }

            $smarty->assign('rows', $newResults);
            $smarty->assign('last_rows', $twoLastRows);
            $smarty->assign('schoolConfig', $schoolConfig);
            $return['results'] = $smarty->fetch("ci/class/ajax.class.attendancesearch.tpl");
            return_json($return);
            break;

        case 'child':
            $data = array();
            $attendances = $attendanceDao->getAttendanceChild($_POST['child_id'], $_POST['fromDate'], $_POST['toDate']);
            if (count($attendances) > 0) {
                $data['attendance'] = $attendances;
                $smarty->assign('data', $data);
                $smarty->assign('schoolConfig', $schoolConfig);
                /* return */
                $return['results'] = $smarty->fetch("ci/class/ajax.class.attendance.child.tpl");
                $return['no_data'] = (count($data['attendance']['attendance']) <= 0);
            }
            return_json($return);
            break;

        case 'confirm':
            $db->begin_transaction();
            //Cập nhật trại thái
            $attendanceDao->updateAttendanceDetailStatus($_POST['attendance_detail_id']);
            //Thông báo phụ huynh
            //$parentIds = $parentDao->getParentIds($_POST['child_id']);
            $parents = getChildData($_POST['child_id'], CHILD_PARENTS);
            $parentIds = array_keys($parents);
            $userDao->postNotifications($parentIds, NOTIFICATION_ATTENDANCE_CONFIRM, NOTIFICATION_NODE_TYPE_CHILD, $_POST['attendance_detail_id'], '', $_POST['child_id']);

            $db->commit();

            return_json($return);
            //return_json(array('callback' => 'window.location = "'.$system['system_url'].'/class/'.$_POST['username'].'/attendance";'));
            break;
        case 'change_attendance_status':
            $db->begin_transaction();
            if($_POST['attendance_status'] != $_POST['old_status']) {
                // Cập nhật trạng thái điểm danh
                $attendanceDao->updateChildAttendanceStatus($_POST['id'], $_POST['child_id'], $_POST['attendance_status'], $_POST['old_status']);
            }

            $db->commit();
            return_json($return);
            break;
        default:
            _error(400);
            break;
    }

} catch (Exception $e) {
    $db->rollback();
    return_json( array('error' => true, 'message' => $e->getMessage()) );
} finally {
    $db->autocommit(true);
}

?>