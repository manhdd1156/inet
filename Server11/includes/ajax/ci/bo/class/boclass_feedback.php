<?php
/**
* Package: ajax/ci/bo/class
*
 * @package ConIu
* @author QuanND
*/

// fetch bootstrap
require('../../../../../bootstrap.php');

// check AJAX Request
is_ajax();
check_login();

// initialize the return array
$return = array();
$return['callback'] = 'window.location.replace(response.path);';

include_once(DAO_PATH.'dao_class.php');
include_once(DAO_PATH.'dao_user.php');
include_once(DAO_PATH.'dao_feedback.php');
$classDao = new ClassDAO();
$userDao = new UserDAO();
$feedbackDao = new FeedbackDAO();

//Lấy ra thông tin lớp
//$class = $classDao->getClassByUsername($_POST['username'], $user->_data['user_id']);
$class = getClassDataByUsername($_POST['username'], CLASS_INFO);
if (is_null($class)) {
    _error(403);
}
$smarty->assign('username', $_POST['username']);

try {
    $return = array();
    $db->autocommit(false);
    switch ($_POST['do']) {
        case 'confirm':
            $return = array();

            if(!isset($_POST['feedback_id']) || !is_numeric($_POST['feedback_id'])) {
                _error(404);
            }
            $feedback = $feedbackDao->getFeedback($_POST['feedback_id']);
            if (is_null($feedback)) {
                _error(404);
            }
            // 1.Update trạng thái
            $feedbackDao->updateStatusToConfirmed($_POST['feedback_id']);
            // 2. Thông báo đến phụ huynh
            //Thông báo giáo viên về góp ý của phụ huynh
            $uId[] = $feedback['created_user_id'];
            $userDao->postNotifications($uId, NOTIFICATION_CONFIRM_FEEDBACK, NOTIFICATION_NODE_TYPE_CHILD, $_POST['feedback_id']);
            $db->commit();

            break;

        default:
            _error(400);
            break;
    }
} catch (Exception $e) {
    $db->rollback();
    return_json( array('error' => true, 'message' => $e->getMessage()) );
} finally {
    $db->autocommit(true);
}

?>