<?php
/**
* Package: ajax/ci/bo/class
*
 * @package ConIu
* @author QuanND
*/

// fetch bootstrap
require('../../../../../bootstrap.php');

// check AJAX Request
is_ajax();
check_login();

// initialize the return array
$return = array();
$return['callback'] = 'window.location.replace(response.path);';

include_once(DAO_PATH.'dao_school.php');
include_once(DAO_PATH.'dao_class.php');
include_once(DAO_PATH.'dao_child.php');
include_once(DAO_PATH.'dao_parent.php');
include_once(DAO_PATH.'dao_user.php');
include_once(DAO_PATH.'dao_service.php');
include_once(DAO_PATH.'dao_attendance.php');
$schoolDao = new SchoolDAO();
$classDao = new ClassDAO();
$childDao = new ChildDAO();
$parentDao = new ParentDAO();
$userDao = new UserDAO();
$serviceDao = new ServiceDAO();
$attendanceDao = new AttendanceDAO();

//Lấy ra thông tin lớp
//$class = $classDao->getClassByUsername($_POST['username'], $user->_data['user_id']);
$class = getClassDataByUsername($_POST['username'], CLASS_INFO);
if (is_null($class)) {
    _error(403);
}
$smarty->assign('username', $_POST['username']);
// Lấy ra thông tin trường
//$school = $schoolDao->getSchoolById($class['school_id']);
$school = getSchoolData($class['school_id'], SCHOOL_INFO);
// Lấy cấu hình thông báo của những user quản lý trường
//$notify_settings = $schoolDao->getUserNotificationSettingOfSchool($class['school_id']);
$notify_settings = getSchoolData($class['school_id'], SCHOOL_NOTIFICATIONS);

try {
    $return = array();
    $db->autocommit(false);
    switch ($_POST['do']) {
        case 'child_list4record':
            $return = array();

            //Validate thông tin đầu vào
            $inputCorrect = true;
            if (!isset($_POST['service_id']) || ($_POST['service_id'] <= 0)) {
                $inputCorrect = false;
            }
            if (!isset($_POST['using_at']) || ($_POST['using_at'] == '')) {
                $inputCorrect = false;
            }
            if ($inputCorrect) {
                $results = $serviceDao->getServiceChild4Record($class['school_id'], $_POST['service_id'], $_POST['using_at'], $class['group_id']);
                $absentChildIds = $attendanceDao->getAbsentChildIds($class['school_id'], $_POST['using_at'], $class['group_id']);
                $smarty->assign('username', $_POST['username']);
                $smarty->assign('service_id', $_POST['service_id']);
                $smarty->assign('absent_child_ids', $absentChildIds); //Truyền thêm danh sách ID những trẻ nghỉ học để check.
                $smarty->assign('results', $results);
                $return['results'] = $smarty->fetch("ci/class/ajax.service.childlist4record.tpl");
                $return['disableSave'] = (count($results['children']) == 0);
            } else {
                $return['results'] = __("No data or incorrect input data");
                $return['disableSave'] = true;
            }
            return_json($return);

            break;

        case 'record':
            $registerChildIds = isset($_POST['childIds'])? $_POST['childIds']: array(); //ID của trẻ sử dụng dịch vụ
            //$allChildIds = $_POST['allChildIds']; //ID của tất cả trẻ trong danh sách
            $oldChildIds = isset($_POST['oldChildIds'])? $_POST['oldChildIds']: array(); //ID của trẻ đã sử dụng dịch vụ ở lần ghi trước

            //Danh sách trẻ sử dụng dịch vụ mới (so với lần lưu trước)
            $newChildIds = array_diff($registerChildIds, $oldChildIds);
            //Danh sách trẻ bỏ sử dụng dịch vụ (do lần trước ghi nhầm)
            $cancelChildIds = array_diff($oldChildIds, $registerChildIds);

            if ((count($newChildIds) + count($cancelChildIds)) > 0) {
                $db->begin_transaction();
                $serviceDao->recordServiceUsage($_POST['service_id'], $newChildIds, $cancelChildIds, $_POST['using_at']);
                $service = $serviceDao->getServiceById($_POST['service_id']);
                // Thông báo cho quản lý trường và phụ huynh
                // Phụ huynh
                foreach ($newChildIds as $childId) {
                    // lấy thông tin trẻ
                    //$child = $childDao->getChild($childId);
                    $child = getChildData($childId, CHILD_INFO);
                    // Lấy danh sách id phụ huynh
                    //$parentIds = $parentDao->getParentIds($childId);
                    $parents = getChildData($childId, CHILD_PARENTS);
                    if (!is_null($child) && !is_null($parents)) {
                        $parentIds = array_keys($parents);
                        $userDao->postNotifications($parentIds, NOTIFICATION_SERVICE_REGISTER_COUNTBASED, NOTIFICATION_NODE_TYPE_CHILD,
                            $_POST['service_id'], convertText4Web($service['service_name']), $child['child_id'], convertText4Web($child['child_name']));
                        // Quản lý trường
                        // Lấy id của những user quản lý được nhận thông báo
                        $userIds = getUserIdsReceiveNotifyOfSchoolForClass($notify_settings, 'services', $class['school_id'], $school['page_admin']);

                        $userDao->postNotifications($userIds, NOTIFICATION_SERVICE_REGISTER_COUNTBASED, NOTIFICATION_NODE_TYPE_SCHOOL,
                            $_POST['service_id'], convertText4Web($service['service_name']), $school['page_name'], convertText4Web($child['child_name']));
                    }
                }
                foreach ($cancelChildIds as $childId) {
                    // lấy thông tin trẻ
                    //$child = $childDao->getChild($childId);
                    $child = getChildData($childId, CHILD_INFO);
                    // Lấy danh sách id phụ huynh
                    //$parentIds = $parentDao->getParentIds($childId);
                    $parents = getChildData($childId, CHILD_PARENTS);
                    if (!is_null($child) && !is_null($parents)) {
                        $parentIds = array_keys($parents);
                        $userDao->postNotifications($parentIds, NOTIFICATION_SERVICE_CANCEL_COUNTBASED, NOTIFICATION_NODE_TYPE_CHILD,
                            $_POST['service_id'], convertText4Web($service['service_name']), $child['child_id'], convertText4Web($child['child_name']));
                        // Quản lý trường
                        // Lấy id của những user quản lý được nhận thông báo
                        $userIds = getUserIdsReceiveNotifyOfSchoolForClass($notify_settings, 'services', $class['school_id'], $school['page_admin']);

                        $userDao->postNotifications($userIds, NOTIFICATION_SERVICE_CANCEL_COUNTBASED, NOTIFICATION_NODE_TYPE_SCHOOL,
                            $_POST['service_id'], convertText4Web($service['service_name']), $school['page_name'], convertText4Web($child['child_name']));
                    }
                }

                $db->commit();
                return_json(array('success' => true, 'message' => __("Done, Service usage info have been updated")));

            } else {
                return_json(array('success' => true, 'message' => __("Service usage info has no change")));
            }

            break;

        case 'history':
            $return = array();
            $serviceId = (isset($_POST['service_id']) && ($_POST['service_id'] > 0))? $_POST['service_id']: 0;
            $childId = (isset($_POST['child_id']) && ($_POST['child_id'] > 0))? $_POST['child_id']: 0;
            $smarty->assign('serviceId', $serviceId);
            $smarty->assign('childId', $childId);
            $begin = isset($_POST['begin'])? $_POST['begin']: (date($system['date_format'], mktime(0, 0, 0, date("m"), 1, date("Y"))));
            $end = isset($_POST['end'])? $_POST['end']: (toSysDate($date));

            if (($childId != 0) || ($serviceId != 0)) {
                //Lấy thông tin sử dụng dịch vụ theo THÁNG và ĐIỂM DANH của một trẻ
                if ($serviceId == 0) {
                    $notCountBasedServices = $serviceDao->getAllNotCountBasedServiceAChild($class['school_id'], $_POST['child_id'], $begin, $end);
                    $smarty->assign('ncb_services', $notCountBasedServices);
                }

                //Lấy ra thông tin sử dụng dịch vụ theo SỐ LẦN của cả lớp
                if ($childId == 0) {
                    $history = $serviceDao->getCountBasedServiceAClass($class['group_id'], $serviceId, $begin, $end);
                } else {
                    $history = $serviceDao->getCountBasedServiceAChild($class['school_id'], $_POST['child_id'], $begin, $end, $serviceId);
                }
                $smarty->assign('history', $history);
                $return['results'] = $smarty->fetch("ci/class/ajax.service.history.tpl");
            } else {
                $return['results'] = __("No data or incorrect input data");
            }
            return_json($return);

            break;
        default:
            _error(400);
            break;
    }
} catch (Exception $e) {
    $db->rollback();
    return_json( array('error' => true, 'message' => $e->getMessage()) );
} finally {
    $db->autocommit(true);
}

?>