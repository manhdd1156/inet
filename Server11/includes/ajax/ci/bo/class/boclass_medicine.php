<?php
/**
 * Package: ajax/ci/bo
 * 
 * @package ConIu
 * @author QuanND
 */

// fetch bootstrap
require('../../../../../bootstrap.php');

// fetch image class
require(ABSPATH . 'includes/class-image.php');

// check AJAX Request
is_ajax();
check_login();

include_once(DAO_PATH . 'dao_school.php');
include_once(DAO_PATH . 'dao_medicine.php');
include_once(DAO_PATH . 'dao_parent.php');
include_once(DAO_PATH . 'dao_teacher.php');
include_once(DAO_PATH . 'dao_user.php');
include_once(DAO_PATH . 'dao_class.php');
include_once(DAO_PATH . 'dao_child.php');

$schoolDao = new SchoolDAO();
$medicineDao = new MedicineDAO();
$parentDao = new ParentDAO();
$teacherDao = new TeacherDAO();
$userDao = new UserDAO();
$classDao = new ClassDAO();
$childDao = new ChildDAO();

//Lấy ra thông tin lớp
//$class = $classDao->getClassByUsername($_POST['username'], $user->_data['user_id']);
$class = getClassDataByUsername($_POST['username'], CLASS_INFO);
if (is_null($class)) {
    _error(403);
}
// Lấy ra thông tin trường
//$school = $schoolDao->getSchoolById($class['school_id']);
$school = getSchoolData($class['school_id'], SCHOOL_INFO);
// Lấy cấu hình thông báo của những user quản lý trường
//$notify_settings = $schoolDao->getUserNotificationSettingOfSchool($class['school_id']);
$notify_settings = getSchoolData($class['school_id'], SCHOOL_NOTIFICATIONS);
try {
    $return = array();
    $db->autocommit(false);
    switch ($_POST['do']) {
        case 'edit':
            // valid inputs
            if(!isset($_POST['medicine_id']) || !is_numeric($_POST['medicine_id'])) {
                _error(400);
            }
            $data = $medicineDao->getMedicine($_POST['medicine_id'], false);
            if (is_null($data)) {
                _error(404);
            }
            $db->begin_transaction();

            $args = array();
            $args['medicine_id'] = $_POST['medicine_id'];
            $args['child_id'] = $_POST['child_id'];
            $args['medicine_list'] = $_POST['medicine_list'];
            $args['begin'] = $_POST['begin'];
            $args['end'] = $_POST['end'];
            $args['guide'] = $_POST['guide'];
            $args['time_per_day'] = $_POST['time_per_day'];
            $args['status'] = MEDICINE_STATUS_CONFIRMED;
            $args['source_file'] = $data['source_file_path'];
            $args['file_name'] = convertText4Web($data['file_name']);

            $args['source_file'] = $data['source_file_path'];
            $args['file_name'] = convertText4Web($data['file_name']);
            if(!$_POST['is_file']) {
                $args['source_file'] = "";
                $args['file_name'] = "";
            }

            $file_name = "";
            // Upload file đính kèm
            if(file_exists($_FILES['file']['tmp_name'])) {
                // check file upload enabled
                if(!$system['file_enabled']) {
                    throw new Exception(__("This feature has been disabled"));
                }

                // valid inputs
                if(!isset($_FILES["file"]) || $_FILES["file"]["error"] != UPLOAD_ERR_OK) {
                    throw new Exception(__("Something wrong with upload! Is 'upload_max_filesize' set correctly?"));
                }

                // check file size
                $max_allowed_size = $system['max_file_size'] * 1024;
                if($_FILES["file"]["size"] > $max_allowed_size) {
                    throw new Exception(__("The file size is so big"));
                }

                // check file extesnion
                $extension = get_extension($_FILES['file']['name']);
                if(!valid_extension($extension, $system['file_medicine_extensions'])) {
                    // modal(MESSAGE, __("Upload Error"), __("The file type is not valid or not supported"));
                    return_json( array('error' => true, 'message' => __("The file type is not valid or not supported")) );
                }

                /* check & create uploads dir */
                $depth = '../../../../../';
                $folder = 'medicines/'. $class['school_id'];
                if(!file_exists($depth.$system['system_uploads_directory'].'/'.$folder)) {
                    @mkdir($depth.$system['system_uploads_directory'].'/'.$folder, 0777, true);
                }
                if(!file_exists($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y'))) {
                    @mkdir($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y'), 0777, true);
                }
                if(!file_exists($system['system_uploads_directory'].'/'.$folder.'/' . date('Y') . '/' . date('m'))) {
                    @mkdir($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y') . '/' . date('m'), 0777, true);
                }

                $directory = $folder.'/'. date('Y') . '/' . date('m') . '/';
                $prefix = $system['uploads_prefix'].'_'.md5(time()*rand(1, 9999));

                $image = new Image($_FILES["file"]["tmp_name"]);
                $file_name = $directory.$prefix.$image->_img_ext;
                $file_name_tmp = $directory.$prefix.'_tmp'.$image->_img_ext;

                $path = $depth.$system['system_uploads_directory'].'/'.$file_name;
                $path_tmp = $depth.$system['system_uploads_directory'].'/'.$file_name_tmp;

                /* check if the file uploaded successfully */
                if(!@move_uploaded_file($_FILES['file']['tmp_name'], $path_tmp)) {
                    throw new Exception(__("Sorry, can not upload the file"));
                }
                /* save the new image */
                $image->save($path, $path_tmp);

                /* delete the tmp image */
                unlink($path_tmp);

                $args['source_file'] = $file_name;
                $args['file_name'] = $_FILES['file']['name'];

            }
            //Cập nhật thông tin vào hệ thống
            $medicineDao->updateMedicine($args);

            //Thông báo nhà trường và phụ huynh
            // Lấy danh sách phụ huynh của trẻ
            $child = getChildData($_POST['child_id'], CHILD_INFO);
            //$parentIds = $parentDao->getParentIds($args['child_id']);
            $parents = getChildData($_POST['child_id'], CHILD_PARENTS);
            if (!is_null($child) && !is_null($parents)) {
                $parentIds = array_keys($parents);
                $userDao->postNotifications($parentIds, NOTIFICATION_UPDATE_MEDICINE, NOTIFICATION_NODE_TYPE_CHILD,
                    $_POST['medicine_id'], convertText4Web($_POST['medicine_list']), $_POST['child_id'], convertText4Web($child['child_name']));

                // Lấy id của những user quản lý được nhận thông báo
                $userIds = getUserIdsReceiveNotifyOfSchoolForClass($notify_settings, 'medicines', $class['school_id'], $school['page_admin']);

                $userDao->postNotifications($userIds, NOTIFICATION_UPDATE_MEDICINE, NOTIFICATION_NODE_TYPE_SCHOOL,
                    $_POST['medicine_id'], convertText4Web($_POST['medicine_list']), $school['page_name'], convertText4Web($child['child_name']));
            }

            $db->commit();
            return_json( array('success' => true, 'message' => __("Done, Medicine info have been updated")) );
            break;

        case 'add':
            $db->begin_transaction();

            $args = array();
            $args['child_id'] = $_POST['child_id'];
            $args['medicine_list'] = $_POST['medicine_list'];
            $args['begin'] = $_POST['begin'];
            $args['end'] = $_POST['end'];
            $args['guide'] = $_POST['guide'];
            $args['time_per_day'] = $_POST['time_per_day'];
            $args['created_user_id'] = $user->_data['user_id'];
            $args['status'] = MEDICINE_STATUS_CONFIRMED; //Do giáo viên tạo ra thì chuyển trạng thái xác nhận luôn
            $file_name = "";
            // Upload file đính kèm
            if(file_exists($_FILES['file']['tmp_name'])) {
                // check file upload enabled
                if (!$system['file_enabled']) {
                    throw new Exception(__("This feature has been disabled"));
                }

                // valid inputs
                if (!isset($_FILES["file"]) || $_FILES["file"]["error"] != UPLOAD_ERR_OK) {
                    throw new Exception(__("Something wrong with upload! Is 'upload_max_filesize' set correctly?"));
                }

                // check file size
                $max_allowed_size = $system['max_file_size'] * 1024;
                if ($_FILES["file"]["size"] > $max_allowed_size) {
                    throw new Exception(__("The file size is so big"));
                }

                // check file extesnion
                $extension = get_extension($_FILES['file']['name']);
                if (!valid_extension($extension, $system['file_medicine_extensions'])) {
                    // modal(MESSAGE, __("Upload Error"), __("The file type is not valid or not supported"));
                    return_json(array('error' => true, 'message' => __("The file type is not valid or not supported")));
                }

                /* check & create uploads dir */
                $depth = '../../../../../';
                $folder = 'medicines/' . $class['school_id'];
                if (!file_exists($depth . $system['system_uploads_directory'] . '/' . $folder)) {
                    @mkdir($depth . $system['system_uploads_directory'] . '/' . $folder, 0777, true);
                }
                if (!file_exists($depth . $system['system_uploads_directory'] . '/' . $folder . '/' . date('Y'))) {
                    @mkdir($depth . $system['system_uploads_directory'] . '/' . $folder . '/' . date('Y'), 0777, true);
                }
                if (!file_exists($system['system_uploads_directory'] . '/' . $folder . '/' . date('Y') . '/' . date('m'))) {
                    @mkdir($depth . $system['system_uploads_directory'] . '/' . $folder . '/' . date('Y') . '/' . date('m'), 0777, true);
                }


//                /* prepare new file name */
//                $directory = $folder.'/'. date('Y') . '/' . date('m') . '/';
//                $prefix = $system['uploads_prefix'].'_'.md5(time()*rand(1, 9999));
//                $file_name = $directory.$prefix.'.'.$extension;
//                $path = $depth.$system['system_uploads_directory'].'/'.$file_name;
//
//                /* check if the file uploaded successfully */
//                if(!@move_uploaded_file($_FILES['file']['tmp_name'], $path)) {
//                    modal(MESSAGE, __("Upload Error"), __("Sorry, can not upload the file"));
//                }

                /* prepare new file name */
                $directory = $folder . '/' . date('Y') . '/' . date('m') . '/';
                $prefix = $system['uploads_prefix'] . '_' . md5(time() * rand(1, 9999));

                $image = new Image($_FILES["file"]["tmp_name"]);
                $file_name = $directory . $prefix . $image->_img_ext;
                $file_name_tmp = $directory . $prefix . '_tmp' . $image->_img_ext;

                $path = $depth . $system['system_uploads_directory'] . '/' . $file_name;
                $path_tmp = $depth . $system['system_uploads_directory'] . '/' . $file_name_tmp;

                /* check if the file uploaded successfully */
                if (!@move_uploaded_file($_FILES['file']['tmp_name'], $path_tmp)) {
                    throw new Exception(__("Sorry, can not upload the file"));
                }
                /* save the new image */
                $image->save($path, $path_tmp);

                /* delete the tmp image */
                unlink($path_tmp);
            }

            $args['source_file'] = $file_name;
            $args['file_name'] = $_FILES['file']['name'];
            //Nhập thông tin vào hệ thống
            $medicineId = $medicineDao->insertMedicine($args);

            // Tăng lượt tương tác thêm mới đơn thuốc - TaiLA
            addInteractive($class['school_id'], 'medicine', 'school_view', $medicineId, 1);

            //Thông báo cô giáo và phụ huynh
            // Lấy danh sách phụ huynh của trẻ
            $child = getChildData($_POST['child_id'], CHILD_INFO);
            //$parentIds = $parentDao->getParentIds($args['child_id']);
            $parents = getChildData($_POST['child_id'], CHILD_PARENTS);
            if (!is_null($child) && !is_null($parents)) {
                $parentIds = array_keys($parents);

                $userDao->postNotifications($parentIds, NOTIFICATION_NEW_MEDICINE, NOTIFICATION_NODE_TYPE_CHILD,
                    $medicineId, convertText4Web($_POST['medicine_list']), $_POST['child_id'], convertText4Web($child['child_name']));

                // Lấy id của những user quản lý được nhận thông báo
                $userIds = getUserIdsReceiveNotifyOfSchoolForClass($notify_settings, 'medicines', $class['school_id'], $school['page_admin']);
                $userDao->postNotifications($userIds, NOTIFICATION_NEW_MEDICINE, NOTIFICATION_NODE_TYPE_SCHOOL,
                    $medicineId, convertText4Web($_POST['medicine_list']), $school['page_name'], convertText4Web($child['child_name']));
            }

            $db->commit();
            return_json( array('success' => true, 'message' => __("Done, Medicine info have been created")));
            break;

        case 'delete':
            if(!isset($_POST['id']) || !is_numeric($_POST['id'])) {
                _error(400);
            }
            $db->begin_transaction();

            $medicineDao->deleteMedicine($_POST['id']);

            $db->commit();
            return_json(array('callback' => 'window.location = "'.$system['system_url'].'/class/'.$_POST['username'].'/medicines/'.$_POST['screen'].'";'));
            break;

        case 'medicate':
            $db->begin_transaction();
            //Thêm lần uống thuốc vào hệ thống
            $medicineDao->addMedicateTime($_POST['id'], $_POST['max']);
            //Cập nhật trạng thái của lần gửi thuốc
            $medicineDao->updateMedicineStatus(MEDICINE_STATUS_CONFIRMED, $_POST['id']);

            $medicine = $medicineDao->getMedicine($_POST['id']);
            $child = getChildData($_POST['child_id'], CHILD_INFO);
            //$parentIds = $parentDao->getParentIds($args['child_id']);
            $parents = getChildData($_POST['child_id'], CHILD_PARENTS);
            if (!is_null($child) && !is_null($parents)) {
                $parentIds = array_keys($parents);
                $userDao->postNotifications($parentIds, NOTIFICATION_USE_MEDICINE, NOTIFICATION_NODE_TYPE_CHILD,
                    $_POST['id'], convertText4Web($_POST['medicine_list']), $_POST['child_id'], convertText4Web($child['child_name']));

                // Lấy id của những user quản lý được nhận thông báo
                $userIds = getUserIdsReceiveNotifyOfSchoolForClass($notify_settings, 'medicines', $class['school_id'], $school['page_admin']);

                $userDao->postNotifications($userIds, NOTIFICATION_USE_MEDICINE, NOTIFICATION_NODE_TYPE_SCHOOL,
                    $_POST['id'], convertText4Web($_POST['medicine_list']), $school['page_name'], convertText4Web($child['child_name']));
            }

            $db->commit();
            return_json(array('callback' => 'window.location = "'.$system['system_url'].'/class/'.$_POST['username'].'/medicines/'.$_POST['screen'].'";'));
            break;

        case 'confirm':
            $db->begin_transaction();
            //Cập nhật trạng thái của lần gửi thuốc
            $medicineDao->updateMedicineStatus(MEDICINE_STATUS_CONFIRMED, $_POST['id']);

            $medicine = $medicineDao->getMedicine($_POST['id']);

            $child = getChildData($_POST['child_id'], CHILD_INFO);
            //$parentIds = array();
            //$parentIds = $parentDao->getParentIds($args['child_id']);
            $parents = getChildData($_POST['child_id'], CHILD_PARENTS);
            if (!is_null($child) && !is_null($parents)) {
                $parentIds = array_keys($parents);
                $userDao->postNotifications($parentIds, NOTIFICATION_CONFIRM_MEDICINE, NOTIFICATION_NODE_TYPE_CHILD,
                        $_POST['id'], convertText4Web($medicine['medicine_list']), $child['child_id'], convertText4Web($child['child_name']));

                // Lấy id của những user quản lý được nhận thông báo
                $userIds = getUserIdsReceiveNotifyOfSchoolForClass($notify_settings, 'medicines', $class['school_id'], $school['page_admin']);

                $userDao->postNotifications($userIds, NOTIFICATION_CONFIRM_MEDICINE, NOTIFICATION_NODE_TYPE_SCHOOL,
                    $_POST['id'], convertText4Web($_POST['medicine_list']), $school['page_name'], convertText4Web($child['child_name']));
            }

            $db->commit();
            if (isset($_POST['screen']) && ($_POST['screen'] == 'dashboard')) {
                return_json(array('callback' => 'window.location = "'.$system['system_url'].'/class/'.$_POST['username'].'";'));
            } else {
                return_json(array('callback' => 'window.location = "'.$system['system_url'].'/class/'.$_POST['username'].'/medicines/'.$_POST['screen'].'";'));
            }
            break;

        case 'confirm_all_today':
            $db->begin_transaction();

            // Lấy tất cả danh sách thuốc chưa xác nhận của lớp
            $today = date($system['date_format']);
            $medicineIds = $medicineDao->getClassMedicineIdsOnDateNoConfirm($class['group_id'], $today);
            if(count($medicineIds) > 0) {
                //Cập nhật trạng thái của lần gửi thuốc
                $medicineDao->updateAllMedicineStatus(MEDICINE_STATUS_CONFIRMED, $medicineIds);
                foreach ($medicineIds as $medicineId) {
                    $medicine = $medicineDao->getMedicine($medicineId);

                    //Thông báo quản lý trường và phụ huynh
                    $child = getChildData($medicine['child_id'], CHILD_INFO);
                    //$parentIds = $parentDao->getParentIds($args['child_id']);
                    $parents = getChildData($medicine['child_id'], CHILD_PARENTS);
                    if (!is_null($child) && !is_null($parents)) {
                        $parentIds = array_keys($parents);
                        $userDao->postNotifications($parentIds, NOTIFICATION_CONFIRM_MEDICINE, NOTIFICATION_NODE_TYPE_CHILD,
                            $medicine['medicine_id'], convertText4Web($medicine['medicine_list']), $child['child_id'], convertText4Web($child['child_name']));

                        // Lấy id của những user quản lý được nhận thông báo
                        $userIds = getUserIdsReceiveNotifyOfSchoolForClass($notify_settings, 'medicines', $class['school_id'], $school['page_admin']);

                        $userDao->postNotifications($userIds, NOTIFICATION_CONFIRM_MEDICINE, NOTIFICATION_NODE_TYPE_SCHOOL,
                            $medicine['medicine_id'], convertText4Web($medicine['medicine_list']), $school['page_name'], convertText4Web($child['child_name']));
                    }
                }

                $db->commit();
            }
            if (isset($_POST['screen']) && ($_POST['screen'] == 'dashboard')) {
                return_json(array('callback' => 'window.location = "'.$system['system_url'].'/class/'.$_POST['username'].'";'));
            } else {
                return_json(array('callback' => 'window.location = "'.$system['system_url'].'/class/'.$_POST['username'].'/medicines/'.'";'));
            }
            break;


        case 'confirm_all':
            $db->begin_transaction();

            // Lấy tất cả danh sách thuốc chưa xác nhận của lớp
            $medicineIds = $medicineDao->getClassAllMedicineIdsNoConfirm($class['group_id']);
            //Cập nhật trạng thái của lần gửi thuốc
            $medicineDao->updateAllMedicineStatus(MEDICINE_STATUS_CONFIRMED, $medicineIds);
            foreach ($medicineIds as $medicineId) {
                $medicine = $medicineDao->getMedicine($medicineId);

                //Thông báo quản lý trường và phụ huynh
                $child = getChildData($medicine['child_id'], CHILD_INFO);
                //$parentIds = $parentDao->getParentIds($args['child_id']);
                $parents = getChildData($medicine['child_id'], CHILD_PARENTS);
                if (!is_null($child) && !is_null($parents)) {
                    $parentIds = array_keys($parents);
                    $userDao->postNotifications($parentIds, NOTIFICATION_CONFIRM_MEDICINE, NOTIFICATION_NODE_TYPE_CHILD,
                        $medicine['medicine_id'], convertText4Web($medicine['medicine_list']), $child['child_id'], convertText4Web($child['child_name']));

                    // Lấy id của những user quản lý được nhận thông báo
                    $userIds = getUserIdsReceiveNotifyOfSchoolForClass($notify_settings, 'medicines', $class['school_id'], $school['page_admin']);

                    $userDao->postNotifications($userIds, NOTIFICATION_CONFIRM_MEDICINE, NOTIFICATION_NODE_TYPE_SCHOOL,
                        $medicine['medicine_id'], convertText4Web($medicine['medicine_list']), $school['page_name'], convertText4Web($child['child_name']));
                }
            }

            $db->commit();
            if (isset($_POST['screen']) && ($_POST['screen'] == 'dashboard')) {
                return_json(array('callback' => 'window.location = "'.$system['system_url'].'/class/'.$_POST['username'].'";'));
            } else {
                return_json(array('callback' => 'window.location = "'.$system['system_url'].'/class/'.$_POST['username'].'/medicines/all'.'";'));
            }
            break;

        case 'cancel':
            $db->begin_transaction();
            //Cập nhật trạng thái của lần gửi thuốc
            $medicineDao->updateMedicineStatus(MEDICINE_STATUS_CANCEL, $_POST['id']);

            $medicine = $medicineDao->getMedicine($_POST['id']);

            //Thông báo quản lý trường và phụ huynh
            $child = getChildData($_POST['child_id'], CHILD_INFO);
            //$parentIds = $parentDao->getParentIds($args['child_id']);
            $parents = getChildData($_POST['child_id'], CHILD_PARENTS);
            if (!is_null($child) && !empty($parents)) {
                $parentIds = array_keys($parents);
                $userDao->postNotifications($parentIds, NOTIFICATION_CANCEL_MEDICINE, NOTIFICATION_NODE_TYPE_CHILD,
                    $_POST['id'], convertText4Web($medicine['medicine_list']), $child['child_id'], convertText4Web($child['child_name']));

                // Lấy id của những user quản lý được nhận thông báo
                $userIds = getUserIdsReceiveNotifyOfSchoolForClass($notify_settings, 'medicines', $class['school_id'], $school['page_admin']);

                $userDao->postNotifications($userIds, NOTIFICATION_CANCEL_MEDICINE, NOTIFICATION_NODE_TYPE_SCHOOL,
                    $_POST['id'], convertText4Web($_POST['medicine_list']), $school['page_name'], convertText4Web($child['child_name']));
            }

            $db->commit();
            return_json(array('callback' => 'window.location = "'.$system['system_url'].'/class/'.$_POST['username'].'/medicines/'.$_POST['screen'].'";'));
            break;

        case 'detail':
            if(!isset($_POST['id']) || !is_numeric($_POST['id'])) {
                _error(404);
            }

            $details = $medicineDao->getMedicineDetail($_POST['id']);
            $results = "<br/>".__("Medicated").":";
            foreach ($details as $detail) {
                $results = $results.'<br/> - '.$detail['usage_date'].' | '.__("Medication time").' '.$detail['time_on_day'].' | '.$detail['created_at'].' | '.$detail['user_firstname']. ' ' .$detail['user_lastname'];
                $results = $results.' <a href="#" class="far fa-comments fa-lg js_chat-start" data-name="'.$detail['user_firstname']. ' ' . $detail['user_lastname'].'" data-uid="'.$detail['created_user_id'].'"></a>';
            }
            $return['results'] = $results;

            return_json($return);
            break;
        default:
            _error(400);
            break;
    }

} catch (Exception $e) {
    $db->rollback();
    return_json( array('error' => true, 'message' => $e->getMessage()) );
} finally {
    $db->autocommit(true);
}

?>