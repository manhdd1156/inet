<?php
/**
 * Package: ajax/ci/bo/class
 *
 * @package ConIu
 * @author TaiLA
 */

// fetch bootstrap
require('../../../../../bootstrap.php');

// check AJAX Request
is_ajax();
check_login();

// initialize the return array
$return = array();
$return['callback'] = 'window.location.replace(response.path);';

include_once(DAO_PATH . 'dao_school.php');
include_once(DAO_PATH . 'dao_class.php');
include_once(DAO_PATH . 'dao_point.php');
include_once(DAO_PATH . 'dao_subject.php');
include_once(DAO_PATH . 'dao_child.php');
include_once(DAO_PATH . 'dao_parent.php');
include_once(DAO_PATH . 'dao_user.php');
include_once(DAO_PATH . 'dao_report.php');
include_once(DAO_PATH . 'dao_conduct.php');
include_once(DAO_PATH . 'dao_mail.php');

$schoolDao = new SchoolDAO();
$classDao = new ClassDAO();
$pointDao = new PointDAO();
$subjectDao = new SubjectDAO();
$conductDao = new ConductDAO();
$childDao = new ChildDAO();
$parentDao = new ParentDAO();
$userDao = new UserDAO();
$reportDao = new ReportDAO();
$mailDao = new MailDAO();

//Lấy ra thông tin lớp
//$class = $classDao->getClassByUsername($_POST['username'], $user->_data['user_id']);
$class = getClassDataByUsername($_POST['username'], CLASS_INFO);
if (is_null($class)) {
    _error(403);
}
$smarty->assign('username', $_POST['username']);
// Lấy ra thông tin trường
//$school = $schoolDao->getSchoolById($class['school_id']);
$school = getSchoolData($class['school_id'], SCHOOL_INFO);
// Lấy cấu hình thông báo của những user quản lý trường
//$notify_settings = $schoolDao->getUserNotificationSettingOfSchool($class['school_id']);
$notify_settings = getSchoolData($class['school_id'], SCHOOL_NOTIFICATIONS);
try {
    $return = array();
    $db->autocommit(false);
    switch ($_POST['do']) {
        case 'list_subject_import' :
            $school_year = $_POST['school_year'];
            $search_with = $_POST['search_with'];
            $semesters = $_POST['semester'];
            if ($search_with == 'search_with_subject' || $search_with == 'importManual') {
//                    $subjects = $pointDao->getClassLevelSubject($school['page_id'], $class_level['gov_class_level'], $school_year);


                // Lấy chi tiết gov_class_level
                $class_level = getClassLevelData($class['class_level_id'], CLASS_LEVEL_INFO);
                //Lấy danh sách môn hiện có của khối
                $classLevelSubjects = $subjectDao->getSubjectsByClassLevel($school['page_id'], $class_level['gov_class_level'], $school_year);
                $subjectCurrentOfClassLevel = array();
                // nếu là giáo viên chủ nhiệm thì sẽ xem được tất cả các môn
                if(canView_class($user->_data['user_id'],$class['group_id'])) {
                    $subjects = $classLevelSubjects;
                }else {
                    $subjects = $subjectDao->getSubjectByTeacher($user->_data['user_id'],$class['group_id'], $semesters, $school_year);
                }
                // Lọc lấy id môn
                foreach ($classLevelSubjects as $classLevelSubject) {
                    $subjectCurrentOfClassLevel[] = $classLevelSubject['subject_id'];
                }
                $lastSubjects = array();

                $results = "<option value=''>" . __("Select subject") . "...</option>";
                $idx = 1;
                foreach ($subjects as $subject) {
                    if (!in_array($subject['subject_id'], $subjectCurrentOfClassLevel)) {
                        continue;
                    }
                    $results = $results . '<option value="' . $subject['subject_id'] . '">' . $idx . " - " . $subject['subject_name'] . '</option>';
                    $idx++;
                }
            } else {

            }
            $return['results'] = $results;

            return_json($return);
            break;
        case 'search_with_subject':

            //Lấy ra class_id của lớp
            $class_id = $class['group_id'];
            $school_year = $_POST['school_year'];

            $subject_id = $_POST['subject_id'];
            $semester = $_POST['semester'];
            $is_last_semester = $_POST['is_last_semester'];

            // Lấy school_config
            $schoolConfig = getSchoolData($school['page_id'], SCHOOL_CONFIG);
            // Lấy chi tiết gov_class_level
            $class_level = getClassLevelData($class['class_level_id'], CLASS_LEVEL_INFO);
            // Lấy danh sách điểm của trẻ trong năm học
            $children_point = $pointDao->getChildrenPointsBySubjectId($class_id, $school_year, $subject_id);


            if (count($children_point) > 0) {
                $children_point_last = array();
                // flag đánh dấu đã đủ điểm các môn hay chưa
                $flag_enough_point = true;
                // trạng thái fail/pass hay re-exam hay chưa đủ điểm ( Waiting.. )
                $status_result = 'fail';
                $subject_key = array();
                $children_point_last = array();
                // lưu số cột mỗi đầu điểm
                $countColumnMaxPoint_hk1 = 2;
                $countColumnMaxPoint_hk2 = 2;
                $countColumnMaxPoint_gk1 = 1;
                $countColumnMaxPoint_gk2 = 1;
                if ($schoolConfig['score_fomula'] == "km") {
                    // Mặc định cột điểm của khmer là 3
                    $countColumnMaxPoint_hk1 = 3;
                    $countColumnMaxPoint_hk2 = 3;
                    foreach ($children_point as $child) {
                        // Lấy child_name
                        $child_detail = $childDao->getChildByCode($child['child_code']);
                        $child_id = $childDao->getChildIdByCode($child['child_code']);
                        // Lấy số buổi nghỉ có phép và không phép của học sinh
                        $child_absent = $childDao->getAbsentsChild($child_id, $class_id);

                        // Lấy danh sách điểm của trẻ trong năm học
                        $each_child_points = $pointDao->getChildrenPointsByStudentCode($class_id, $school_year, $child['child_code']);

//                    $children_point_last = array();
                        // Lưu điểm của các môn thi lại
                        $children_subject_reexams = array();
                        // flag đánh dấu đã đủ điểm các môn hay chưa
                        $child_enough_point = true;
                        // trạng thái fail/pass hay re-exam hay chưa đủ điểm ( Waiting )
                        $status = '';
                        // điểm các tháng của 2 kỳ
                        $child_m11_point = $child_m12_point = $child_m13_point = $child_m21_point = $child_m22_point = $child_m23_point = 0;
                        // điểm trung bình các tháng của 2 kỳ
                        $child_avM1_point = $child_avM2_point = 0;
                        // điểm trung bình của cuối kỳ
                        $child_avd1_point = 0;
                        $child_avd2_point = 0;
                        $child_final_semester1 = $child_final_semester2 = 0;
                        // điểm trung bình năm

                        $child_avYear_point = 0;
                        if (count($each_child_points) > 0) {
//                        $child_id = $childDao->getChildIdByCode($children_point[0]['child_code']);
                            // Lấy số buổi nghỉ có phép và không phép của học sinh
//                        $child_absent = $childDao->getAbsentsChild($child_id, $class_id);
                            foreach ($each_child_points as $each) {
                                if (!isset($each['hs11']) || !isset($each['hs12']) || !isset($each['hs13']) || !isset($each['gk11'])
                                    || !isset($each['hs21']) || !isset($each['hs22']) || !isset($each['hs23']) || !isset($each['gk21'])) {
                                    $flag_enough_point = false;
                                }
                                // tạm thời tính tổng các đầu điểm
                                // kỳ 1
                                $child_m11_point += (int)$each['hs11'];
                                $child_m12_point += (int)$each['hs12'];
                                $child_m13_point += (int)$each['hs13'];
                                $child_avd1_point += (int)$each['gk11'];
                                // kỳ 2
                                $child_m21_point += (int)$each['hs21'];
                                $child_m22_point += (int)$each['hs22'];
                                $child_m23_point += (int)$each['hs23'];
                                $child_avd2_point += (int)$each['gk21'];

                                // lấy tên môn học
                                $subject_detail = $subjectDao->getSubjectById($each['subject_id']);
                                $each['subject_name'] = $subject_detail['subject_name'];
                                if (isset($subject_detail['re_exam']) && $subject_detail['re_exam']) { // nếu môn đó là môn thi lại thì lưu vào array
                                    $subject_reexam['name'] = $subject_detail['subject_name'];
                                    $subject_reexam['point'] = $each['re_exam'];
                                    $children_subject_reexams[] = $subject_reexam;
                                }
                            }
                        }
//                        $child['subject_reexam']= $subject_detail['re_exam'];
                        // định nghĩa số bị chia của từng khối
                        if ($class_level['gov_class_level'] == '7' || $class_level['gov_class_level'] == '8') {
                            $dividend = 15;
                        } else if ($class_level['gov_class_level'] == '9') {
                            $dividend = 11.4;
                        } else if ($class_level['gov_class_level'] == '10') {
                            $dividend = 15.26;
                        } else if ($class_level['gov_class_level'] == '11') {
                            $dividend = 16.5;
                        } else if ($class_level['gov_class_level'] == '12') {
                            $dividend = 14.5;
                        }
                        // tính điểm theo công thức từng khối
                        // kỳ 1
                        $child_m11_point /= $dividend;
                        $child_m12_point /= $dividend;
                        $child_m13_point /= $dividend;
                        $child_avd1_point /= $dividend;
                        $child_avM1_point = ($child_m11_point + $child_m12_point + $child_m13_point) / 3;
                        $child_final_semester1 = ($child_avd1_point + $child_avM1_point) / 2;
                        // kỳ 2
                        $child_m21_point /= $dividend;
                        $child_m22_point /= $dividend;
                        $child_m23_point /= $dividend;
                        $child_avd2_point /= $dividend;
                        $child_avM2_point = ($child_m21_point + $child_m22_point + $child_m23_point) / 3;
                        $child_final_semester2 = ($child_avd2_point + $child_avM2_point) / 2;
                        // cả năm
                        $child_avYear_point = ($child_final_semester1 + $child_final_semester2) / 2;
                        // lưu lại vào array

                        //xét điều kiện để đánh giá pass hay fail
                        if ($child_avYear_point < 25) { // dưới 25 điểm thì fail chặt
                            $status = 'Fail';
                        } else { // nếu trên 25 điểm thì xét các điều kiện khác
                            $status = 'Pass';
                            if ((isset($child_absent['absent_true']) && (int)$child_absent['absent_true'] >= 55)
                                || (isset($child_absent['absent_false']) && (int)$child_absent['absent_false'] >= 30)
                                || (((int)$child_absent['absent_true'] + (int)$child_absent['absent_false']) >= 55)) {
                                if ($class_level['gov_class_level'] == '7' || $class_level['gov_class_level'] == '8'
                                    || $class_level['gov_class_level'] == '10' || $class_level['gov_class_level'] == '11') {
                                    $status = 'Re-exam';
                                } else if ($class_level['gov_class_level'] == '9' || $class_level['gov_class_level'] == '12') {
                                    $status = 'Pass';
                                }
                            }
                        }
                        // xét đến đánh giá điểm thi lại
                        if ($status == 'Re-exam') {
                            $result_exam = 0;
                            foreach ($children_subject_reexams as $subject_reexam) {
                                if (!isset($subject_reexam['point'])) {
                                    $child_enough_point = false;
                                } else {
                                    $result_exam += (int)$subject_reexam['point'];
                                }
                            }
                            if ($class_level['gov_class_level'] == '7' || $class_level['gov_class_level'] == '8'
                                || $class_level['gov_class_level'] == '11') {
                                $result_exam /= 4;
                            } else if ($class_level['gov_class_level'] == '10') {
                                $result_exam /= 4;
                            }
                            // nếu điểm thi lại trên 25 điểm thì pass còn lại fail
                            if ($child_enough_point && $result_exam >= 25) {
                                $status = 'Pass';
                            } else if ($child_enough_point && $result_exam < 25) {
                                $status = 'Fail';
                            } else { // chuaw du diem
                                $status = 'Re-exam';
                            }
                        }
//                        $children_point_last[] = $child;
                        if (!$child_enough_point && $status != 'Re-exam') {
                            $status = 'Waiting..';
                        }
                        $child['status'] = $status;
                        $child['without_permission'] = $child_absent['absent_true'];
                        $child['with_permission'] = $child_absent['absent_false'];
                        $child['child_enough_point'] = $child_enough_point;
                        $child['x1'] = number_format($child_avM1_point, 2);
                        $child['x2'] = number_format($child_avM2_point, 2);;
                        $child['e1'] = number_format($child_final_semester1, 2);;
                        $child['e2'] = number_format($child_final_semester2, 2);;
                        $child['y'] = number_format($child_avYear_point, 2);;

                        $child['child_firstname'] = $child_detail['first_name'];
                        $child['child_lastname'] = $child_detail['last_name'];
                        $child['child_id'] = $child_id;
                        $children_point_last[] = $child;
                    }


//                    $subject_key = array();
                    if ($schoolConfig['grade'] == 1) {
//                        if (!$is_last_semester) {
//                            $subject_key[] = 'gk' . $semester . 'nx';
//                            $subject_key[] = 'gk' . $semester . 'nl';
//                            $subject_key[] = 'gk' . $semester . 'pc';
//                            $subject_key[] = 'gk' . $semester . 'kt';
//                            $subject_key[] = 'gk' . $semester . 'xl';
//                        } else {
//                            $subject_key[] = 'ck' . $semester . 'nx';
//                            $subject_key[] = 'ck' . $semester . 'nl';
//                            $subject_key[] = 'ck' . $semester . 'pc';
//                            $subject_key[] = 'ck' . $semester . 'kt';
//                            $subject_key[] = 'ck' . $semester . 'xl';
//                        }
                    } elseif ($schoolConfig['grade'] == 2) {
                        if ($semester == 0) {
                            $subject_key[] = 'hs11';
                            $subject_key[] = 'hs12';
                            $subject_key[] = 'hs13';
                            $subject_key[] = 'gk11';
                            $subject_key[] = 'hs21';
                            $subject_key[] = 'hs22';
                            $subject_key[] = 'hs23';
                            $subject_key[] = 'gk21';
                            $subject_key[] = 'x1';
                            $subject_key[] = 'x2';
                            $subject_key[] = 'e1';
                            $subject_key[] = 'e2';
                            $subject_key[] = 'y';
                            $subject_key[] = 're_exam';
                            $subject_key[] = 'without_permission';
                            $subject_key[] = 'with_permission';
                            $subject_key[] = 'status';
                        } else {
                            $subject_key[] = 'hs' . $semester . '1';
                            $subject_key[] = 'hs' . $semester . '2';
                            $subject_key[] = 'hs' . $semester . '3';
                            $subject_key[] = 'gk' . $semester . '1';
                        }
                    }
                    // mặc định sinh ra 3 cột Month và 1 cột last
                    $smarty->assign("column_hk", 3);
                    $smarty->assign("column_gk", 1);
                } else if ($schoolConfig['score_fomula'] == "vn") {
//                    foreach ($children_point as $child) {

                    // điều kiện điểm của môn 1: kém, 2 yếu, 3 trung bình, 4 tiên tiến, 5 giỏi
                    $point_avg1_condition1 = 5;//hk1
                    $point_avg2_condition1 = 5;//hk2
                    $point_avgYearAll_condition1 = 5;// cả năm
                    // điều kiện điểm của môn toán hoặc văn  1: kém, 2 yếu, 3 trung bình, 4 tiên tiến, 5 giỏi
                    $point_avg1_condition2 = 5; //hk1
                    $point_avg2_condition2 = 5; //hk2
                    $point_avgYearAll_condition2 = 5; // cả năm
                    // trạng thái
                    $status_point1 = '';
                    $status_point2 = '';
                    $status_pointAll = '';
                    // flag đánh dấu học sinh đã thi lại hay chưa
                    $flag_reexam = false;
                    // trạng thái lên lớp hay thi lại hay chưa đủ điểm...
                    $status_result = 'fail';

                    // điểm trung bình các môn 2 kỳ
                    $child_TBM1_point = $child_TBM2_point = 0;
                    // điểm trung bình năm
                    $child_avYearAll_point = 0;
                    // TÍnh điểm mỗi môn
                    if ($semester != 0) {
                        $countColumnMaxPoint_hk = 2;
                        $countColumnMaxPoint_gk = 1;
                        foreach ($children_point as $child) {
                            $child_detail = $childDao->getChildByCode($child['child_code']);
                            $child_id = $childDao->getChildIdByCode($child['child_code']);
                            if (!isset($child['hs' . $semester . '1']) || !isset($child['hs' . $semester . '2'])
                                || !isset($child['gk' . $semester . '1']) || !isset($child['ck' . $semester])) {
                                $flag_enough_point = false;
                            }
                            // điểm hệ số 2 kỳ
                            $child_hk_point = 0;
                            $child_TBM_current_point = 0;
                            // đếm số điểm đã có
                            $count_hk_point = 0;
                            //Tính điểm trung bình môn
                            for ($i = 1; $i <= 6; $i++) {
                                // điểm hệ số 1
                                if (isset($child['hs' . $semester . $i])) {
                                    $child_hk_point += (int)$child['hs' . $semester . $i];
                                    $count_hk_point++;
                                    // lưu số cột đã có điểm của hệ số 1
                                    if ($i > $countColumnMaxPoint_hk) {
                                        $countColumnMaxPoint_hk = $i;
                                    }
                                }
                                // điểm hệ số 2
                                if (isset($child['gk' . $semester . $i])) {
                                    $child_hk_point += (int)$child['gk' . $semester . $i] * 2;
                                    $count_hk_point += 2;
                                    // lưu số cột đã có điểm
                                    $countColumnMaxPoint_gk = $i;
                                }
                            }
                            //điểm cuối kì

                            if (isset($child['ck' . $semester])) {
                                $child_hk_point += (int)$child['ck' . $semester] * 3;
                                $count_hk_point += 3;
                            }
                            // Điểm trung bình học kỳ
                            if ($count_hk_point > 0) {
                                $child_TBM_current_point = $child_hk_point / $count_hk_point;
                                $child['tb_hk'] = number_format($child_TBM_current_point,2);
                            }
                            $child['child_firstname'] = $child_detail['first_name'];
                            $child['child_lastname'] = $child_detail['last_name'];
                            $child['child_id'] = $child_id;
                            $children_point_last[] = $child;
                        }
                        // setup subject_key
                        for ($i = 1; $i <= $countColumnMaxPoint_hk; $i++) {
                            $subject_key[] = 'hs' . $semester . $i;
                        }
                        for ($i = 1; $i <= $countColumnMaxPoint_gk; $i++) {
                            $subject_key[] = 'gk' . $semester . $i;
                        }
                        $subject_key[] = 'ck' . $semester;
                        $subject_key[] = 'tb_hk';

                        $smarty->assign("column_hk", $countColumnMaxPoint_hk);
                        $smarty->assign("column_gk", $countColumnMaxPoint_gk);
                        $smarty->assign("rows", $children_point_last);
                        $smarty->assign("semester", $semester);
                        $smarty->assign("grade", $schoolConfig['grade']);
                        $smarty->assign("score_fomula", $schoolConfig['score_fomula']);
                        $smarty->assign("subject_key", $subject_key);
                        $smarty->assign("username", $school['page_name']);
                        $smarty->assign("search_with", $_POST['do']);
                        $results['results'] = $smarty->fetch("ci/class/ajax.points.searchresult.tpl");
                        return_json($results);
                    } else if ($semester == 0) {
                        // lưu số cột mỗi đầu điểm

                        foreach ($children_point as $child) {
                            // Lấy child_name
                            $child_detail = $childDao->getChildByCode($child['child_code']);
                            $child_id = $childDao->getChildIdByCode($child['child_code']);
                            // Lấy số buổi nghỉ có phép và không phép của học sinh
                            // flag đánh dấu đã đủ điểm các môn hay chưa
                            $flag_enough_point = true;
                            // trạng thái fail/pass hay re-exam hay chưa đủ điểm ( Waiting.. )
                            $status_result = 'fail';

                            if (!isset($child['hs11']) || !isset($child['hs12']) || !isset($child['gk11'])
                                || !isset($child['hs21']) || !isset($child['hs22']) || !isset($child['gk21'])
                                || !isset($child['ck1']) || !isset($child['ck2'])) {
                                $flag_enough_point = false;
                            }
                                // điểm hệ số 2 kỳ
                                $child_hk1_point = $child_hk2_point = 0;
                                $child_TBM1_current_point = $child_TBM2_current_point = 0;
                                // đếm số điểm đã có
                                $count_hk1_point = 0;
                                $count_hk2_point = 0;
                                //Tính điểm trung bình môn

                                for ($i = 1; $i <= 6; $i++) {
                                    // điểm hệ số 1
                                    if (isset($child['hs1' . $i])) {
                                        $child_hk1_point += (int)$child['hs1' . $i];
                                        $count_hk1_point++;
                                        // lưu số cột đã có điểm của hệ số 1 kỳ 1
                                        if ($i > $countColumnMaxPoint_hk1) {
                                            $countColumnMaxPoint_hk1 = $i;
                                        }
                                    }
                                    if (isset($child['hs2' . $i])) {
                                        $child_hk2_point += (int)$child['hs2' . $i];
                                        $count_hk2_point++;
                                        // lưu số cột đã có điểm của hệ số 1 kỳ 2
                                        if ($i > $countColumnMaxPoint_hk2) {
                                            $countColumnMaxPoint_hk2 = $i;
                                        }
                                    }
                                    // điểm hệ số 2
                                    if (isset($child['gk1' . $i])) {
                                        $child_hk1_point += (int)$child['gk1' . $i] * 2;
                                        $count_hk1_point += 2;
                                        // lưu số cột đã có điểm
                                        if ($i > $countColumnMaxPoint_gk1) {
                                            $countColumnMaxPoint_gk1 = $i;
                                        }

                                    }
                                    if (isset($child['gk2' . $i])) {
                                        $child_hk2_point += (int)$child['gk2' . $i] * 2;
                                        $count_hk2_point += 2;
                                        // lưu số cột đã có điểm
                                        if ($i > $countColumnMaxPoint_gk2) {
                                            $countColumnMaxPoint_gk2 = $i;
                                        }
                                    }
                                }
                                //điểm cuối kì

                                if (isset($child['ck1'])) {
                                    $child_hk1_point += (int)$child['ck1'] * 3;
                                    $count_hk1_point += 3;
                                }
                                if (isset($child['ck2'])) {
                                    // Nếu có điểm thi lại thì điểm tổng kết sẽ tính dựa theo điểm thi lại
                                    if(isset($child['re_exam'])) {
                                        $child_hk2_point += (int)$child['re_exam'] * 3;
                                        $flag_reexam = true;
                                    }else {
                                        $child_hk2_point += (int)$child['ck2'] * 3;
                                    }
                                    $count_hk2_point += 3;
                                }
                                if ($count_hk1_point > 0) {
                                    $child_TBM1_current_point = $child_hk1_point / $count_hk1_point;
                                        $child['tb_hk1'] = number_format($child_TBM1_current_point,2);
                                }
                                if ($count_hk2_point > 0) {
                                    $child_TBM2_current_point = $child_hk2_point / $count_hk2_point;
                                    $child['tb_hk2'] = number_format($child_TBM2_current_point,2);
                                }
                                // Điểm tổng kết
                                $child_avYear_point = ($child_TBM1_current_point + $child_TBM2_current_point * 2) / 3;

                            if (!is_infinite($child_avYear_point)) {
                                $child['tb_year'] = number_format($child_avYear_point, 2);
                            } else {
                                $child['tb_year'] = null;
                            }
                            $child['child_firstname'] = $child_detail['first_name'];
                            $child['child_lastname'] = $child_detail['last_name'];
                            $child['child_id'] = $child_id;
                            $children_point_last[] = $child;

                        }
                        // setup subject_key
                        for ($i = 1; $i <= $countColumnMaxPoint_hk1; $i++) {
                            $subject_key[] = 'hs1' . $i;
                        }
                        for ($i = 1; $i <= $countColumnMaxPoint_gk1; $i++) {
                            $subject_key[] = 'gk1' . $i;
                        }
                        $subject_key[] = 'ck1';
                        $subject_key[] = 'tb_hk1';
                        for ($i = 1; $i <= $countColumnMaxPoint_hk2; $i++) {
                            $subject_key[] = 'hs2' . $i;
                        }
                        for ($i = 1; $i <= $countColumnMaxPoint_gk2; $i++) {
                            $subject_key[] = 'gk2' . $i;
                        }
                        $subject_key[] = 'ck2';
                        $subject_key[] = 'tb_hk2';
                        $subject_key[] = 'tb_year';
                        $subject_key[] = 're_exam';
                    }
                }
                $smarty->assign("column_hk1", $countColumnMaxPoint_hk1);
                $smarty->assign("column_hk2", $countColumnMaxPoint_hk2);
                $smarty->assign("column_gk1", $countColumnMaxPoint_gk1);
                $smarty->assign("column_gk2", $countColumnMaxPoint_gk2);
                $smarty->assign("rows", $children_point_last);
                $smarty->assign("semester", $semester);
                $smarty->assign("grade", $schoolConfig['grade']);
                $smarty->assign("subject_key", $subject_key);
                $smarty->assign("username", $_POST['username']);
                $smarty->assign("search_with", $_POST['do']);
                $smarty->assign("score_fomula", $schoolConfig['score_fomula']);
            }
            $results['results'] = $smarty->fetch("ci/class/ajax.points.searchresult.tpl");
            return_json($results);
            break;
        case 'search_with_student':
            global $user;
            //Lấy ra class_id của lớp
            $class_id = $class['group_id'];
            $school_year = $_POST['school_year'];

            // danh sách các môn mà giáo viên đc assign
            $subjects = $subjectDao->getSubjectByTeacher($user->_data['user_id'],$class_id, 0, $school_year);
            $subject_ids = array();
            if (count($subjects) > 0) {
                foreach ($subjects as $subject) {
                    $subject_ids[] = $subject['subject_id'];
                }
            }
            // Lấy school_config
            $studentId = $_POST['student_id'];
            $schoolConfig = getSchoolData($school['page_id'], SCHOOL_CONFIG);
            // Lấy chi tiết gov_class_level
            $class_level = getClassLevelData($class['class_level_id'], CLASS_LEVEL_INFO);
            // Lấy danh sách điểm của trẻ trong năm học
            $children_point = $pointDao->getChildrenPointsByStudentId($class_id, $school_year, $studentId);
            // Lấy child_name
            if (count($children_point) > 0) {

                $children_point_last = array();
                // Lưu các điểm đã được tính toán
                $children_point_avgs = array();
                // Lưu điểm của các môn thi lại
                $children_subject_reexams = array();
                // flag đánh dấu đã đủ điểm các môn hay chưa
                $child_enough_point = true;
                // Lấy số buổi nghỉ có phép và không phép của học sinh
                $child_absent = $childDao->getAbsentsChild($studentId, $class_id);
                // lưu số cột mỗi đầu điểm
                $countColumnMaxPoint_hk1 = 2;
                $countColumnMaxPoint_hk2 = 2;
                $countColumnMaxPoint_gk1 = 1;
                $countColumnMaxPoint_gk2 = 1;
                // trạng thái fail/pass hay re-exam hay chưa đủ điểm ( Waiting.. )
                $status_result = '';
                if ($schoolConfig['score_fomula'] == "km") {
                    // Mặc định cột điểm của khmer là 3
                    $countColumnMaxPoint_hk1 = 3;
                    $countColumnMaxPoint_hk2 = 3;
                    // điểm các tháng của 2 kỳ
                    $child_m11_point = $child_m12_point = $child_m13_point = $child_m21_point = $child_m22_point = $child_m23_point = 0;
                    // điểm trung bình các tháng của 2 kỳ
                    $child_avM1_point = $child_avM2_point = 0;
                    // điểm trung bình của cuối kỳ
                    $child_avd1_point = 0;
                    $child_avd2_point = 0;
                    $child_final_semester1 = $child_final_semester2 = 0;
                    // điểm trung bình năm
                    $child_avYear_point = 0;

//                    $child_id = $childDao->getChildIdByCode($children_point[0]['child_code']);
//                    // Lấy số buổi nghỉ có phép và không phép của học sinh
//                    $child_absent = $childDao->getAbsentsChild($child_id, $class_id);
                    // Lấy số môn hiện tại của 1 khối
                    $classLevelSubjects = $subjectDao->getSubjectsByClassLevel($school['page_id'], $class_level['gov_class_level'], $school_year);
                    $subjectCurrentOfClassLevel = array();
                    // Lọc lấy id môn
                    foreach ($classLevelSubjects as $classLevelSubject) {
                        $subjectCurrentOfClassLevel[] = $classLevelSubject['subject_id'];
                    }
                    foreach ($children_point as $child) {
                        if (!in_array($child['subject_id'], $subjectCurrentOfClassLevel)) {
                            continue;
                        }
                        // Nếu bất kì điểm nào thiếu sẽ đánh dấu là chưa đủ điểm
                        if (!isset($each['hs11']) || !isset($each['hs12']) || !isset($each['hs13']) || !isset($each['gk11'])
                            || !isset($each['hs21']) || !isset($each['hs22']) || !isset($each['hs23']) || !isset($each['gk21'])) {
                            $flag_enough_point = false;
                        }
                        // tạm thời tính tổng các đầu điểm
                        // kỳ 1
                        $child_m11_point += (int)$child['hs11'];
                        $child_m12_point += (int)$child['hs12'];
                        $child_m13_point += (int)$child['hs13'];
                        $child_avd1_point += (int)$child['gk11'];
                        // kỳ 2
                        $child_m21_point += (int)$child['hs21'];
                        $child_m22_point += (int)$child['hs22'];
                        $child_m23_point += (int)$child['hs23'];
                        $child_avd2_point += (int)$child['gk21'];

                        // lấy tên môn học
                        $subject_detail = $subjectDao->getSubjectById($child['subject_id']);
                        $child['subject_name'] = $subject_detail['subject_name'];
                        if (isset($subject_detail['re_exam']) && $subject_detail['re_exam']) { // nếu môn đó là môn thi lại thì lưu vào array
                            $subject_reexam['name'] = $subject_detail['subject_name'];
                            $subject_reexam['point'] = $child['re_exam'];
                            $children_subject_reexams[] = $subject_reexam;
                        }
//                        $child['subject_reexam']= $subject_detail['re_exam'];
                        if (array_key_exists($child['subject_id'], $subject_ids)) {
                            $children_point_last[] = $child;
                        }
                    }


                    // định nghĩa số bị chia của từng khối
                    if ($class_level['gov_class_level'] == '7' || $class_level['gov_class_level'] == '8') {
                        $dividend = 15;
                    } else if ($class_level['gov_class_level'] == '9') {
                        $dividend = 11.4;
                    } else if ($class_level['gov_class_level'] == '10') {
                        $dividend = 15.26;
                    } else if ($class_level['gov_class_level'] == '11') {
                        $dividend = 16.5;
                    } else if ($class_level['gov_class_level'] == '12') {
                        $dividend = 14.5;
                    }
                    // tính điểm theo công thức từng khối
                    // kỳ 1
                    $child_m11_point /= $dividend;
                    $child_m12_point /= $dividend;
                    $child_m13_point /= $dividend;
                    $child_avd1_point /= $dividend;
                    $child_avM1_point = ($child_m11_point + $child_m12_point + $child_m13_point) / 3;
                    $child_final_semester1 = ($child_avd1_point + $child_avM1_point) / 2;
                    // kỳ 2
                    $child_m21_point /= $dividend;
                    $child_m22_point /= $dividend;
                    $child_m23_point /= $dividend;
                    $child_avd2_point /= $dividend;
                    $child_avM2_point = ($child_m21_point + $child_m22_point + $child_m23_point) / 3;
                    $child_final_semester2 = ($child_avd2_point + $child_avM2_point) / 2;
                    // cả năm
                    $child_avYear_point = ($child_final_semester1 + $child_final_semester2) / 2;
                    // lưu lại vào array
                    $children_point_avgs['a1'] = $child_m11_point;
                    $children_point_avgs['b1'] = $child_m12_point;
                    $children_point_avgs['c1'] = $child_m13_point;
                    $children_point_avgs['d1'] = $child_avd1_point;
                    $children_point_avgs['x1'] = $child_avM1_point;
                    $children_point_avgs['e1'] = $child_final_semester1;
                    $children_point_avgs['a2'] = $child_m21_point;
                    $children_point_avgs['b2'] = $child_m22_point;
                    $children_point_avgs['c2'] = $child_m23_point;
                    $children_point_avgs['d2'] = $child_avd2_point;
                    $children_point_avgs['x2'] = $child_avM2_point;
                    $children_point_avgs['e2'] = $child_final_semester2;
                    $children_point_avgs['y'] = $child_avYear_point;

                    //xét điều kiện để đánh giá pass hay fail
                    if ($child_avYear_point < 25) { // dưới 25 điểm thì fail chặt
                        $status_result = 'Fail';
                    } else { // nếu trên 25 điểm thì xét các điều kiện khác
                        $status_result = 'Pass';
                        if ((isset($child_absent['absent_true']) && (int)$child_absent['absent_true'] >= 55)
                            || (isset($child_absent['absent_false']) && (int)$child_absent['absent_false'] >= 30)
                            || (((int)$child_absent['absent_true'] + (int)$child_absent['absent_false']) >= 55)) {
                            if ($class_level['gov_class_level'] == '7' || $class_level['gov_class_level'] == '8'
                                || $class_level['gov_class_level'] == '10' || $class_level['gov_class_level'] == '11') {
                                $status_result = 'Re-exam';
                            } else if ($class_level['gov_class_level'] == '9' || $class_level['gov_class_level'] == '12') {
                                $status_result = 'Pass';
                            }
                        }
                    }
                    // xét đến đánh giá điểm thi lại
                    if ($status_result == 'Re-exam') {
                        $result_exam = 0;
                        foreach ($children_subject_reexams as $subject_reexam) {
                            if (!isset($subject_reexam['point'])) {
                                $child_enough_point = false;
                            } else {
                                $result_exam += (int)$subject_reexam['point'];
                            }
                        }
                        if ($class_level['gov_class_level'] == '7' || $class_level['gov_class_level'] == '8'
                            || $class_level['gov_class_level'] == '11') {
                            $result_exam /= 4;
                        } else if ($class_level['gov_class_level'] == '10') {
                            $result_exam /= 4;
                        }
                        // nếu điểm thi lại trên 25 điểm thì pass còn lại fail
                        if ($child_enough_point && $result_exam >= 25) {
                            $status_result = 'Pass';
                        } else if ($child_enough_point && $result_exam < 25) {
                            $status_result = 'Fail';
                        } else { // chuaw du diem
                            $status_result = 'Re-exam';
                        }
                    }
                    if (!$child_enough_point && $status_result != 'Re-exam') {
                        $status_result = 'Waiting..';
                    }
                    $points_key = array();
                    if ($schoolConfig['grade'] == 1) { // chưa làm cho cấp 1
//                        if (!$is_last_semester) {
//                            $points_key[] = 'gk' . $semester . 'nx';
//                            $points_key[] = 'gk' . $semester . 'nl';
//                            $points_key[] = 'gk' . $semester . 'pc';
//                            $points_key[] = 'gk' . $semester . 'kt';
//                            $points_key[] = 'gk' . $semester . 'xl';
//                        } else {
//                            $points_key[] = 'ck' . $semester . 'nx';
//                            $points_key[] = 'ck' . $semester . 'nl';
//                            $points_key[] = 'ck' . $semester . 'pc';
//                            $points_key[] = 'ck' . $semester . 'kt';
//                            $points_key[] = 'ck' . $semester . 'xl';
//                        }
                    } elseif ($schoolConfig['grade'] == 2) {
                        $subject_key[] = 'hs11';
                        $subject_key[] = 'hs12';
                        $subject_key[] = 'hs13';
                        $subject_key[] = 'gk11';
                        $subject_key[] = 'hs21';
                        $subject_key[] = 'hs22';
                        $subject_key[] = 'hs23';
                        $subject_key[] = 'gk21';
                    }
                    $smarty->assign("children_point_avgs", $children_point_avgs);
                    $smarty->assign("children_subject_reexams", $children_subject_reexams);
                    $smarty->assign("result_exam", $result_exam);
                } else if ($schoolConfig['score_fomula'] == "vn") {
                    // điều kiện điểm của môn 1: kém, 2 yếu, 3 trung bình, 4 tiên tiến, 5 giỏi
                    $point_avg1_condition1 = 5;//hk1
                    $point_avg2_condition1 = 5;//hk2
                    $point_avgYearAll_condition1 = 5;// cả năm
                    // điều kiện điểm của môn toán hoặc văn  1: kém, 2 yếu, 3 trung bình, 4 tiên tiến, 5 giỏi
                    $point_avg1_condition2 = 5; //hk1
                    $point_avg2_condition2 = 5; //hk2
                    $point_avgYearAll_condition2 = 5; // cả năm
                    // trạng thái
                    $status_point1 = '';
                    $status_point2 = '';
                    $status_pointAll = '';
                    // flag đánh dấu học sinh đã thi lại hay chưa
                    $flag_reexam = false;
                    // trạng thái lên lớp hay thi lại hay chưa đủ điểm...
                    $status_result = 'fail';

                    // điểm trung bình các môn 2 kỳ
                    $child_TBM1_point = $child_TBM2_point = 0;
                    // điểm trung bình năm
                    $child_avYearAll_point = 0;
                    // TÍnh điểm mỗi môn
                    foreach ($children_point as $each) {
                        if (!isset($each['hs11']) || !isset($each['hs12']) || !isset($each['gk11'])
                            || !isset($each['hs21']) || !isset($each['hs22']) || !isset($each['gk21'])
                            || !isset($each['ck1']) || !isset($each['ck2'])) {
                            $flag_enough_point = false;
                        }
                        // điểm hệ số 2 kỳ
                        $child_hk1_point = $child_hk2_point = 0;
                        $child_TBM1_current_point = $child_TBM2_current_point = 0;
                        // đếm số điểm đã có
                        $count_hk1_point = 0;
                        $count_hk2_point = 0;
                        //Tính điểm trung bình môn

                        for ($i = 1; $i <= 6; $i++) {
                            // điểm hệ số 1
                            if (isset($each['hs1' . $i])) {
                                $child_hk1_point += (int)$each['hs1' . $i];
                                $count_hk1_point++;
                                // lưu số cột đã có điểm của hệ số 1 kỳ 1
                                if ($i > $countColumnMaxPoint_hk1) {
                                    $countColumnMaxPoint_hk1 = $i;
                                }
                            }
                            if (isset($each['hs2' . $i])) {
                                $child_hk2_point += (int)$each['hs2' . $i];
                                $count_hk2_point++;
                                // lưu số cột đã có điểm của hệ số 1 kỳ 2
                                if ($i > $countColumnMaxPoint_hk2) {
                                    $countColumnMaxPoint_hk2 = $i;
                                }
                            }
                            // điểm hệ số 2
                            if (isset($each['gk1' . $i])) {
                                $child_hk1_point += (int)$each['gk1' . $i] * 2;
                                $count_hk1_point += 2;
                                // lưu số cột đã có điểm
                                $countColumnMaxPoint_gk1 = $i;
                            }
                            if (isset($each['gk2' . $i])) {
                                $child_hk2_point += (int)$each['gk2' . $i] * 2;
                                $count_hk2_point += 2;
                                // lưu số cột đã có điểm
                                $countColumnMaxPoint_gk2 = $i;
                            }
                        }
                        //điểm cuối kì

                        if (isset($each['ck1'])) {
                            $child_hk1_point += (int)$each['ck1'] * 3;
                            $count_hk1_point += 3;
                        }
                        if (isset($each['ck2'])) {
                            // Nếu có điểm thi lại thì điểm tổng kết sẽ tính dựa theo điểm thi lại
                            if(isset($each['re_exam'])) {
                                $child_hk2_point += (int)$each['re_exam'] * 3;
                                $flag_reexam = true;
                            }else {
                                $child_hk2_point += (int)$each['ck2'] * 3;
                            }
                            $count_hk2_point += 3;
                        }
                        if ($count_hk1_point > 0) {
                            $child_TBM1_current_point = $child_hk1_point / $count_hk1_point;
                            $each['tb_hk1'] = number_format($child_TBM1_current_point,2);
                        }
                        if ($count_hk2_point > 0) {
                            $child_TBM2_current_point = $child_hk2_point / $count_hk2_point;
                            $each['tb_hk2'] = number_format($child_TBM2_current_point,2);
                        }
                        // Lấy tên môn học
                        $subject_detail = $subjectDao->getSubjectById($each['subject_id']);
                        $each['subject_name'] = $subject_detail['subject_name'];

                        // Điểm tổng kết
                        $child_avYear_point = ($child_TBM1_current_point + $child_TBM2_current_point * 2) / 3;

                        $each['tb_year'] = number_format($child_avYear_point,2);
                        // xét xem điều kiện các môn ở mức nào
                        //học kỳ 1
                        if ($child_TBM1_current_point < 2 && $point_avg1_condition1 > 1) {
                            $point_avg1_condition1 = 1;
                        } else if ($child_TBM1_current_point < 3.5 && $point_avg1_condition1 > 2) {
                            $point_avg1_condition1 = 2;
                        } else if ($child_TBM1_current_point < 5 && $point_avg1_condition1 > 3) {
                            $point_avg1_condition1 = 3;
                        } else if ($child_TBM1_current_point < 6.5 && $point_avg1_condition1 > 4) {
                            $point_avg1_condition1 = 4;
                        }
                        //học kỳ 2
                        if ($child_TBM2_current_point < 2 && $point_avg2_condition1 > 1) {
                            $point_avg2_condition1 = 1;
                        } else if ($child_TBM2_current_point < 3.5 && $point_avg2_condition1 > 2) {
                            $point_avg2_condition1 = 2;
                        } else if ($child_TBM2_current_point < 5 && $point_avg2_condition1 > 3) {
                            $point_avg2_condition1 = 3;
                        } else if ($child_TBM2_current_point < 6.5 && $point_avg2_condition1 > 4) {
                            $point_avg2_condition1 = 4;
                        }
                        // cả năm
                        if ($child_avYear_point < 2 && $point_avgYearAll_condition1 > 1) {
                            $point_avgYearAll_condition1 = 1;
                        } else if ($child_avYear_point < 3.5 && $point_avgYearAll_condition1 > 2) {
                            $point_avgYearAll_condition1 = 2;
                        } else if ($child_avYear_point < 5 && $point_avgYearAll_condition1 > 3) {
                            $point_avgYearAll_condition1 = 3;
                        } else if ($child_avYear_point < 6.5 && $point_avgYearAll_condition1 > 4) {
                            $point_avgYearAll_condition1 = 4;
                        }
                        // xét xem điều kiện của môn toán văn
                        // lấy tên môn học
                        $subject_detail = $subjectDao->getSubjectById($each['subject_id']);
                        $each['subject_name'] = $subject_detail['subject_name'];
                        if (convert_vi_to_en(strtolower(html_entity_decode($each['subject_name']))) == "toan" || convert_vi_to_en(strtolower(html_entity_decode($each['subject_name']))) == "van") {
                            //học kỳ 1
                            if ($child_TBM1_current_point < 2 && $point_avg1_condition2 > 1) {
                                $point_avg1_condition2 = 1;
                            } else if ($child_TBM1_current_point < 3.5 && $point_avg1_condition2 > 2) {
                                $point_avg1_condition2 = 2;
                            } else if ($child_TBM1_current_point < 5 && $point_avg1_condition2 > 3) {
                                $point_avg1_condition2 = 3;
                            } else if ($child_TBM1_current_point < 6.5 && $point_avg1_condition2 > 4) {
                                $point_avg1_condition2 = 4;
                            }
                            // học kỳ 2
                            if ($child_TBM2_current_point < 2 && $point_avg2_condition2 > 1) {
                                $point_avg2_condition2 = 1;
                            } else if ($child_TBM2_current_point < 3.5 && $point_avg2_condition2 > 2) {
                                $point_avg2_condition2 = 2;
                            } else if ($child_TBM2_current_point < 5 && $point_avg2_condition2 > 3) {
                                $point_avg2_condition2 = 3;
                            } else if ($child_TBM2_current_point < 6.5 && $point_avg2_condition2 > 4) {
                                $point_avg2_condition2 = 4;
                            }
                            // cả năm
                            if ($child_avYear_point < 2 && $point_avgYearAll_condition2 > 1) {
                                $point_avgYearAll_condition2 = 1;
                            } else if ($child_avYear_point < 3.5 && $point_avgYearAll_condition2 > 2) {
                                $point_avgYearAll_condition2 = 2;
                            } else if ($child_avYear_point < 5 && $point_avgYearAll_condition2 > 3) {
                                $point_avgYearAll_condition2 = 3;
                            } else if ($child_avYear_point < 6.5 && $point_avgYearAll_condition2 > 4) {
                                $point_avgYearAll_condition2 = 4;
                            }
                        }
                        // Cộng vào điểm trung bình các môn
                        $child_TBM1_point += $child_TBM1_current_point;
                        $child_TBM2_point += $child_TBM2_current_point;
                        $child_avYearAll_point += $child_avYear_point;
                        $children_point_last[] = $each;
                    }
                    // điểm trung bình các môn
                    $child_TBM1_point /= count($children_point);
                    $child_TBM2_point /= count($children_point);
                    $child_avYearAll_point /= count($children_point);

                    //xét điều kiện để đánh giá pass hay fail
                    //học kỳ 1
                    if ($child_TBM1_point >= 8 && $point_avg1_condition1 == 5 && $point_avg1_condition2 == 5) { // trên 8 điểm thì đánh giá loại giỏi
                        $status_point1 = 5;
                    } else if ($child_TBM1_point >= 8 && ($point_avg1_condition1 == 3 || $point_avg1_condition2 == 3)) {
                        $status_point1 = 4;
                    } else if ($child_TBM1_point >= 8 && ($point_avg1_condition1 == 2 || $point_avg1_condition2 == 2)) {
                        $status_point1 = 3;
                    } else if ($child_TBM1_point >= 6.5 && $point_avg1_condition1 >= 4 && $point_avg1_condition2 >= 4) {  // trên 6.5 điểm thì đánh giá loại tiên tiến
                        $status_point1 = 4;
                    } else if ($child_TBM1_point >= 6.5 && ($point_avg1_condition1 == 2 || $point_avg1_condition2 == 2)) {  // trên 6.5 điểm thì đánh giá loại tiên tiến
                        $status_point1 = 3;
                    } else if ($child_TBM1_point >= 6.5 && ($point_avg1_condition1 == 1 || $point_avg1_condition2 == 1)) {  // trên 6.5 điểm thì đánh giá loại tiên tiến
                        $status_point1 = 2;
                    } else if ($child_TBM1_point >= 5 && $point_avg1_condition1 >= 3 && $point_avg1_condition2 >= 3) {  // trên 5 điểm thì đánh giá loại trung bình
                        $status_point1 = 3;
                    } else if ($child_TBM1_point >= 3.5 && $point_avg1_condition1 >= 2 && $point_avg1_condition2 >= 2) {  // trên 3.5 điểm thì đánh giá loại yếu
                        $status_point1 = 2;
                    } else if ($child_TBM1_point < 3.5 && $point_avg1_condition1 >= 1 && $point_avg1_condition2 >= 1) {
                        $status_point1 = 1;
                    }
                    // học kỳ 2
                    if ($child_TBM2_point >= 8 && $point_avg2_condition1 == 5 && $point_avg2_condition2 == 5) { // trên 8 điểm thì đánh giá loại giỏi
                        $status_point2 = 5;
                    } else if ($child_TBM2_point >= 8 && ($point_avg2_condition1 == 3 || $point_avg2_condition2 == 3)) {
                        $status_point2 = 4;
                    } else if ($child_TBM2_point >= 8 && ($point_avg2_condition1 == 2 || $point_avg2_condition2 == 2)) {
                        $status_point2 = 3;
                    } else if ($child_TBM2_point >= 6.5 && $point_avg2_condition1 >= 4 && $point_avg2_condition2 >= 4) {  // trên 6.5 điểm thì đánh giá loại tiên tiến
                        $status_point2 = 4;
                    } else if ($child_TBM2_point >= 6.5 && ($point_avg2_condition1 == 2 || $point_avg2_condition2 == 2)) {  // trên 6.5 điểm thì đánh giá loại tiên tiến
                        $status_point2 = 3;
                    } else if ($child_TBM2_point >= 6.5 && ($point_avg2_condition1 == 1 || $point_avg2_condition2 == 1)) {  // trên 6.5 điểm thì đánh giá loại tiên tiến
                        $status_point2 = 2;
                    } else if ($child_TBM2_point >= 5 && $point_avg2_condition1 >= 3 && $point_avg2_condition2 >= 3) {  // trên 5 điểm thì đánh giá loại trung bình
                        $status_point2 = 3;
                    } else if ($child_TBM2_point >= 3.5 && $point_avg2_condition1 >= 2 && $point_avg2_condition2 >= 2) {  // trên 3.5 điểm thì đánh giá loại yếu
                        $status_point2 = 2;
                    } else if ($child_TBM2_point < 3.5 && $point_avg2_condition1 >= 1 && $point_avg2_condition2 >= 1) {
                        $status_point2 = 1;
                    }
                    // cả năm
                    if ($child_avYearAll_point >= 8 && $point_avgYearAll_condition1 == 5 && $point_avgYearAll_condition2 == 5) { // trên 8 điểm thì đánh giá loại giỏi
                        $status_pointAll = 5;
                    } else if ($child_avYearAll_point >= 8 && ($point_avgYearAll_condition1 == 3 || $point_avgYearAll_condition2 == 3)) {
                        $status_pointAll = 4;
                    } else if ($child_avYearAll_point >= 8 && ($point_avgYearAll_condition1 == 2 || $point_avgYearAll_condition2 == 2)) {
                        $status_pointAll = 3;
                    } else if ($child_avYearAll_point >= 6.5 && $point_avgYearAll_condition1 >= 4 && $point_avgYearAll_condition2 >= 4) {  // trên 6.5 điểm thì đánh giá loại tiên tiến
                        $status_pointAll = 4;
                    } else if ($child_avYearAll_point >= 6.5 && ($point_avgYearAll_condition1 == 2 || $point_avgYearAll_condition2 == 2)) {  // trên 6.5 điểm thì đánh giá loại tiên tiến
                        $status_pointAll = 3;
                    } else if ($child_avYearAll_point >= 6.5 && ($point_avgYearAll_condition1 == 1 || $point_avgYearAll_condition2 == 1)) {  // trên 6.5 điểm thì đánh giá loại tiên tiến
                        $status_pointAll = 2;
                    } else if ($child_avYearAll_point >= 5 && $point_avgYearAll_condition1 >= 3 && $point_avgYearAll_condition2 >= 3) {  // trên 5 điểm thì đánh giá loại trung bình
                        $status_pointAll = 3;
                    } else if ($child_avYearAll_point >= 3.5 && $point_avgYearAll_condition1 >= 2 && $point_avgYearAll_condition2 >= 2) {  // trên 3.5 điểm thì đánh giá loại yếu
                        $status_pointAll = 2;
                    } else if ($child_avYearAll_point < 3.5 && $point_avgYearAll_condition1 >= 1 && $point_avgYearAll_condition2 >= 1) {
                        $status_pointAll = 1;
                    }

                    // Lấy hạnh kiểm của học sinh
                    $conduct = $conductDao->getConductOfChildren($class_id, $studentId, $school_year);
                    if (!isset($conduct) || !isset($conduct['ck'])) {
                        $flag_enough_point = false;
                    } else if ($child_absent['absent_true'] + $child_absent['absent_false'] <= 45
                        && $status_pointAll >= 3 && $conduct['ck'] != "Weak") {
                        $status_result = "pass";
                    }


                    if (!$flag_enough_point) {
                        $status_result = "waiting..";
                    }
                    // setup subject_key
                    for ($i = 1; $i <= $countColumnMaxPoint_hk1; $i++) {
                        $subject_key[] = 'hs1' . $i;
                    }
                    for ($i = 1; $i <= $countColumnMaxPoint_gk1; $i++) {
                        $subject_key[] = 'gk1' . $i;
                    }
                    $subject_key[] = 'ck1';
                    $subject_key[] = 'tb_hk1';
                    for ($i = 1; $i <= $countColumnMaxPoint_hk2; $i++) {
                        $subject_key[] = 'hs2' . $i;
                    }
                    for ($i = 1; $i <= $countColumnMaxPoint_gk2; $i++) {
                        $subject_key[] = 'gk2' . $i;
                    }
                    $subject_key[] = 'ck2';
                    $subject_key[] = 'tb_hk2';
                    $subject_key[] = 're_exam';


                    $smarty->assign("tb_total_hk1", number_format($child_TBM1_point,2));
                    $smarty->assign("tb_total_hk2", number_format($child_TBM2_point,2));
                    $smarty->assign("tb_total_year", number_format($child_avYearAll_point,2));
                }
                $smarty->assign("column_hk1", $countColumnMaxPoint_hk1);
                $smarty->assign("column_hk2", $countColumnMaxPoint_hk2);
                $smarty->assign("column_gk1", $countColumnMaxPoint_gk1);
                $smarty->assign("column_gk2", $countColumnMaxPoint_gk2);
                $smarty->assign("rows", $children_point_last);
                $smarty->assign("grade", $schoolConfig['grade']);
                $smarty->assign("subject_key", $subject_key);
                $smarty->assign("username", $_POST['username']);
                $smarty->assign("search_with", $_POST['do']);
                $smarty->assign("child_absent", $child_absent);
                $smarty->assign("status", $status_result);
                $smarty->assign("score_fomula", $schoolConfig['score_fomula']);

//                $smarty->assign("semester", $semester);
//            $smarty->assign("child_enough_point", $child_enough_point);
//            $smarty->assign("children_point_avgs", $children_point_avgs);
//            $smarty->assign("children_subject_reexams", $children_subject_reexams);
//            $smarty->assign("result_exam", $result_exam);
                $results['results'] = $smarty->fetch("ci/class/ajax.points.searchresult.tpl");
                return_json($results);
            }
            break;
        case 'showDataImport' :

            //Lấy ra class_id của lớp
            $class_id = $class['group_id'];
            $school_year = $_POST['school_year'];

            $subject_id = $_POST['subject_id'];
            $semester = $_POST['semester'];
            $is_last_semester = $_POST['is_last_semester'];

            // Lấy school_config
            $schoolConfig = getSchoolData($school['page_id'], SCHOOL_CONFIG);
            // Lấy chi tiết gov_class_level
            $class_level = getClassLevelData($class['class_level_id'], CLASS_LEVEL_INFO);
            // Lấy danh sách điểm của trẻ trong năm học
            $children_point = $pointDao->getChildrenPointsBySubjectId($class_id, $school_year, $subject_id);
            $countColumnMaxPoint_hk1 = $countColumnMaxPoint_hk2 = 2;
            $countColumnMaxPoint_gk1 = $countColumnMaxPoint_gk2 = 1;
            // danh sách các môn mà giáo viên đc assign
            $subjects = $subjectDao->getSubjectByTeacher($user->_data['user_id'],$class_id, $semester, $school_year);
            $subject_ids = array();
            if (count($subjects) > 0) {
                foreach ($subjects as $subject) {
                    $subject_ids[] = $subject['subject_id'];
                }
            }
            $children_point_last = array();
            if (count($children_point) > 0) {

                foreach ($children_point as $child) {
                    // Lấy child_name
                    $child_detail = $childDao->getChildByCode($child['child_code']);
                    $child_id = $childDao->getChildIdByCode($child['child_code']);
                    // Lấy số buổi nghỉ có phép và không phép của học sinh
                    $child_absent = $childDao->getAbsentsChild($child_id, $class_id);
                    // lấy tên môn học và đánh dấu là môn thi lại
                    $subject_detail = $subjectDao->getSubjectById($child['subject_id']);
                    //đếm số cột các điểm đã có
                    if ($semester != 0) {
                        for ($i = 1; $i <= 6; $i++) {
                            // điểm hệ số 1
                            if (isset($child['hs' . $semester . $i])) {
                                // lưu số cột đã có điểm của hệ số 1
                                if ($i > $countColumnMaxPoint_hk1) {
                                    $countColumnMaxPoint_hk1 = $i;
                                }
                            }
                            // điểm hệ số 2
                            if (isset($child['gk' . $semester . $i])) {
                                // lưu số cột đã có điểm
                                $countColumnMaxPoint_gk1 = $i;
                            }
                        }
                    } else {
                        for ($i = 1; $i <= 6; $i++) {
                            // điểm hệ số 1
                            if (isset($child['hs1' . $i])) {
                                // lưu số cột đã có điểm của hệ số 1
                                if ($i > $countColumnMaxPoint_hk1) {
                                    $countColumnMaxPoint_hk1 = $i;
                                }
                            }
                            if (isset($child['hs2' . $i])) {
                                // lưu số cột đã có điểm của hệ số 1
                                if ($i > $countColumnMaxPoint_hk2) {
                                    $countColumnMaxPoint_hk2 = $i;
                                }
                            }
                            // điểm hệ số 2
                            if (isset($child['gk1' . $i])) {
                                // lưu số cột đã có điểm
                                $countColumnMaxPoint_gk1 = $i;
                            }
                            if (isset($child['gk2' . $i])) {
                                // lưu số cột đã có điểm
                                $countColumnMaxPoint_gk2 = $i;
                            }
                        }
                    }
                    if($schoolConfig['score_fomula'] == 'vn') {
                        $child['is_reexam'] = 1;
                    }else {
                        $child['is_reexam'] = $subject_detail['re_exam'];
                    }
                    $child['child_firstname'] = $child_detail['first_name'];
                    $child['child_lastname'] = $child_detail['last_name'];
                    $child['child_id'] = $child_id;
                    if (in_array($child['subject_id'], $subject_ids)) {
                        $children_point_last[] = $child;
                    }
                }
            }

            if ($schoolConfig['score_fomula'] != 'vn') {
                $countColumnMaxPoint_hk1 = 3;
                $countColumnMaxPoint_gk1 = 1;
                $countColumnMaxPoint_hk2 = 3;
                $countColumnMaxPoint_gk2 = 1;
            }
            $subject_key = array();
            if ($semester == 0) {
                for ($i = 1; $i <= $countColumnMaxPoint_hk1; $i++) {
                    $subject_key[] = 'hs1' . $i;
                }
                for ($i = 1; $i <= $countColumnMaxPoint_gk1; $i++) {
                    $subject_key[] = 'gk1' . $i;
                }
                if($schoolConfig['score_fomula']=='vn') {
                    $subject_key[] = 'ck1';
                }
                for ($i = 1; $i <= $countColumnMaxPoint_hk2; $i++) {
                    $subject_key[] = 'hs2' . $i;
                }
                for ($i = 1; $i <= $countColumnMaxPoint_gk2; $i++) {
                    $subject_key[] = 'gk2' . $i;
                }
                if($schoolConfig['score_fomula']=='vn') {
                    $subject_key[] = 'ck2';
                }
                $subject_key[] = 're_exam';
            } else {
                for ($i = 1; $i <= $countColumnMaxPoint_hk1; $i++) {
                    $subject_key[] = 'hs' . $semester . $i;
                }
                for ($i = 1; $i <= $countColumnMaxPoint_gk1; $i++) {
                    $subject_key[] = 'gk' . $semester . $i;
                }
                if($schoolConfig['score_fomula']=='vn') {
                    $subject_key[] = 'ck'. $semester;
                }
            }
            $smarty->assign("column_hk1", $countColumnMaxPoint_hk1);
            $smarty->assign("column_gk1", $countColumnMaxPoint_gk1);
            $smarty->assign("column_hk2", $countColumnMaxPoint_hk2);
            $smarty->assign("column_gk2", $countColumnMaxPoint_gk2);
            $smarty->assign("score_fomula", $schoolConfig['score_fomula']);
            $smarty->assign("rows", $children_point_last);
            $smarty->assign("semester", $semester);
            $smarty->assign("grade", $schoolConfig['grade']);
            $smarty->assign("subject_key", $subject_key);
            $smarty->assign("username", $_POST['username']);
            $smarty->assign("search_with", $_POST['do']);
            $results['results'] = $smarty->fetch("ci/class/ajax.points.searchresult.tpl");
            return_json($results);
            break;
        case 'importManual':
            $results = array();
            $newChildIds = array();
            $new_child_list = array();
            $sheet = array();
            $sheet['error'] = 0;

            // Lấy school_config
            $schoolConfig = getSchoolData($school['page_id'], SCHOOL_CONFIG);


            //Lấy ra thông số từ form gửi lên
            $class_id = $class['group_id'];
            $subject_id = $_POST['subject_id'];
            $semester = $_POST['semester'];
            $is_last_semester = $_POST['is_last_semester'];
            $school_year = $_POST['school_year'];
            $childs_points = $_POST['childs_points'];

            // Lấy danh sách điểm của trẻ trong năm học
            $children_point = $pointDao->getChildrenPointsBySubjectId($class_id, $school_year, $subject_id);

//            $children_codes = array();
//            if (count($children_point) > 0) {
//                foreach ($children_point as $child) {
//                    $children_codes[] = $child['child_code'];
//                }
//            }

//            if ($sheet['error'] == 0) {
            foreach ($childs_points as $child) { // Duyệt danh sách trẻ
                $child['error'] = 0;
                // Kiểm tra xem trẻ có tồn tại trong lớp không
                $is_child = $childDao->checkExistChildByCodeAndClassId($child['child_code'], $class_id);
                if (!$is_child) {
                    $child['error'] = 1;
                    $child['message'] = __("Student not in class");
                    $new_child_list[] = $child;
                } else {
                    if ($child['error'] == 0) {
                        // Kiểm tra xem trẻ đã được tạo bản ghi điểm trước đó chưa
                        $is_create_point = $pointDao->checkCreatedPoint($subject_id, $class_id, $child['child_code'], $school_year);

                        if ($is_create_point) {
                            // Kiểm tra xem điểm gửi lên có khác gì điểm trước đó không, nếu khác thì update, không thì thôi (tạm thời bỏ qua bước này, update luôn
                            $db->autocommit(false);
                            $db->begin_transaction();
                            $pointDao->updateChildPoint($class_id, $subject_id, $school_year, $child);
                            $db->commit();
                            $child['message'] = __("Update point success");
                            $child['child_fullname'] = $child['child_lastname'] . ' ' . $child['child_firstname'];

                            //thông báo về cho phụ huynh
                            $childId = $childDao->getChildIdByCode($child['child_code']);
                            $child['child_id'] = $childId;
                            $parents = getChildData($child['child_id'], CHILD_PARENTS);
                            if (!is_null($parents)) {
                                $parentIds = array_keys($parents);

                                $subject_detail = $subjectDao->getSubjectById($subject_id);
                                $userDao->postNotifications($parentIds, NOTIFICATION_NEW_POINT, NOTIFICATION_NODE_TYPE_CHILD,
                                    $user->_data['user_id'], convertText4Web($subject_detail['subject_name']), $child['child_id'], convertText4Web($child['child_fullname']));
                                // đồng thời thông báo cho học sinh
                                // lấy user của học sinh theo child id
                                $userOfChild = $childDao->getUserOfChildByChildId($child['child_id']);
                                $userDao->postNotifications($userOfChild['user_id'], NOTIFICATION_NEW_POINT, NOTIFICATION_NODE_TYPE_CHILD,
                                    $user->_data['user_id'], convertText4Web($subject_detail['subject_name']), $child['child_id'], convertText4Web($child['child_fullname']));

                            }
                            $new_child_list[] = $child;
                        } else {
                            // Insert bản ghi điểm mới
                            try {
                                $new_child_list[] = $child;
                                $db->autocommit(false);
                                $db->begin_transaction();

                                $pointDao->createChildPoint($class_id, $subject_id, $school_year, $child);
                                $db->commit();
                            } catch (Exception $e) {
                                $db->rollback();
                                $child['error'] = 1;
                                $child['message'] = $e->getMessage();
                            } finally {
                                $db->autocommit(true);
                            }
                        }
                    }
                }
            }


            $sheet['child_list'] = $new_child_list;
            //echo "<pre>"; print_r($sheet); die('vvv');
            $smarty->assign("sheet", $sheet);
            $results['results'] = $smarty->fetch("ci/class/ajax.points.importresult.tpl");
            return_json($results);
            break;
        case 'show_comment_child':
            if (!isset($_POST['child_id']) || !is_numeric($_POST['child_id'])) {
                _error(404);
            }

            // Get school_config
            $schoolConfig = getSchoolData($school['page_id'], SCHOOL_CONFIG);

            $school_year = $_POST['school_year'];
            $is_last_semester = $_POST['is_last_semester'];
            // Lấy chi tiết nhận xét của trẻ theo điều kiện gửi lên
            $child_comment = $pointDao->getChildComment($_POST['child_id'], $class['group_id'], $school_year, $_POST['semester'], $is_last_semester, $schoolConfig['grade']);

            $smarty->assign('results', $child_comment);
            $smarty->assign('username', $_POST['username']);
            $smarty->assign('grade', $schoolConfig['grade']);

            $return['results'] = $smarty->fetch("ci/class/ajax.class.point.comment.tpl");
            return_json($return);
            break;

        case 'comment':
            if (!isset($_POST['student_id']) || !is_numeric($_POST['student_id'])) {
                _error(404);
            }

            $db->begin_transaction();

            $student_id = $_POST['student_id'];
            $school_year = $_POST['school_year'];
            $is_last_semester = $_POST['is_last_semester'];

            // Get school_config
            $schoolConfig = getSchoolData($school['page_id'], SCHOOL_CONFIG);

            // Kiểm tra xem trẻ đã được thêm  nhận xét trước đó chưa
            $is_child_comment = $pointDao->checkChildComment($student_id, $class['group_id'], $school_year, $schoolConfig['grade']);

            if ($is_child_comment) {
                // Update child comment
                $pointDao->updateChildComment($student_id, $class['group_id'], $school_year, $_POST['semester'], $is_last_semester, $schoolConfig['grade'], $_POST['child_comment']);
            } else {
                // insert child comment
                $pointDao->createChildComment($student_id, $class['group_id'], $school_year, $_POST['semester'], $is_last_semester, $schoolConfig['grade'], $_POST['child_comment']);
            }

            $db->commit();

            return_json(array('success' => true, 'message' => __("Success")));
            break;
        case 'add':
            $db->begin_transaction();
            if (is_null($_POST['child'])) {
                throw new Exception(__("You have not picked a student yet"));
            }
            $args = array();
            $args['report_name'] = trim($_POST['title']);
            $args['school_id'] = $class['school_id'];
            $args['child'] = $_POST['child'];
            $args['is_notified'] = (isset($_POST['notify_immediately']) && $_POST['notify_immediately'] == 'on') ? 1 : 0;
            $args['status'] = 1;
            $args['date'] = 'null';
            if ($args['is_notified'] == 1) {
                $args['date'] = $date;
            }

            $file_name = "";
            // Upload file đính kèm
            if (file_exists($_FILES['file']['tmp_name'])) {
                // check file upload enabled
                if (!$system['file_enabled']) {
                    throw new Exception(__("This feature has been disabled"));
                }

                // valid inputs
                if (!isset($_FILES["file"]) || $_FILES["file"]["error"] != UPLOAD_ERR_OK) {
                    throw new Exception(__("Something wrong with upload! Is 'upload_max_filesize' set correctly?"));
                }

                // check file size
                $max_allowed_size = $system['max_file_size'] * 1024;
                if ($_FILES["file"]["size"] > $max_allowed_size) {
                    throw new Exception(__("The file size is so big"));
                }

                // check file extesnion
                $extension = get_extension($_FILES['file']['name']);
                if (!valid_extension($extension, $system['file_report_extensions'])) {
                    throw new Exception(__("The file type is not valid or not supported"));
                }

                /* check & create uploads dir */
                $depth = '../../../../../';
                $folder = 'reports/' . $class['school_id'];
                if (!file_exists($depth . $system['system_uploads_directory'] . '/' . $folder)) {
                    @mkdir($depth . $system['system_uploads_directory'] . '/' . $folder, 0777, true);
                }
                if (!file_exists($depth . $system['system_uploads_directory'] . '/' . $folder . '/' . date('Y'))) {
                    @mkdir($depth . $system['system_uploads_directory'] . '/' . $folder . '/' . date('Y'), 0777, true);
                }
                if (!file_exists($system['system_uploads_directory'] . '/' . $folder . '/' . date('Y') . '/' . date('m'))) {
                    @mkdir($depth . $system['system_uploads_directory'] . '/' . $folder . '/' . date('Y') . '/' . date('m'), 0777, true);
                }

                /* prepare new file name */
                $directory = $folder . '/' . date('Y') . '/' . date('m') . '/';
                $prefix = $system['uploads_prefix'] . '_' . md5(time() * rand(1, 9999));
                $file_name = $directory . $prefix . '.' . $extension;
                $path = $depth . $system['system_uploads_directory'] . '/' . $file_name;

                /* check if the file uploaded successfully */
                if (!@move_uploaded_file($_FILES['file']['tmp_name'], $path)) {
                    throw new Exception(__("Sorry, can not upload the file"));
                }
            }

            $args['source_file'] = $file_name;
            $args['file_name'] = $_FILES['file']['name'];

            // Lấy id category
            $categoryIds = isset($_POST['category_ids']) ? $_POST['category_ids'] : array();
            if (count($categoryIds) == 0 && is_empty($args['source_file'])) {
                throw new Exception(__("You must select report template"));
            }
            // 1. Insert vào bảng ci_report
            if (is_empty($args['source_file']) && (count($_POST['category_ids']) == 0)) {
                throw new Exception(__("You must have an attachment or contact book content"));
            }
            $reportIds = $reportDao->insertReportClass($args);

            foreach ($reportIds as $reportId) {
                // Tăng lượt thêm mới -TaiLA
                addInteractive($class['school_id'], 'report', 'school_view', $reportId, 1);

            }
            // 2. Inser vào bảng ci_report_category

            // Lặp mảng categoryIds lấy nội dung gửi lên tương ứng
            $categorys = array();
            $category = array();
            if (count($categoryIds) > 0) {
                foreach ($categoryIds as $id) {
                    $category['multi_content'] = isset($_POST['suggest_' . $id]) ? $_POST['suggest_' . $id] : array();
                    $category['report_category_content'] = $_POST['content_' . $id];
                    $category['report_category_name'] = $_POST['category_name_' . $id];

                    // Lấy toàn bộ gợi ý của category
                    $categoryTemp = $reportDao->getReportTemplateCategoryDetail($id);
                    $suggests = $categoryTemp['suggests'];
                    $category['template_multi_content'] = array();
                    foreach ($suggests as $row) {
                        $category['template_multi_content'][] = convertText4Web($row);
                    }
//                    $category['template_multi_content'] = convertText4Web($category['template_multi_content']);
                    $categorys[] = $category;
                }
                for ($i = 0; $i < count($reportIds); $i++) {
                    $reportDao->insertReportDetail($reportIds[$i], $categorys);
                }
            }

            //3. Gửi email và thông báo đến phụ huynh
            if ($args['is_notified']) {
                $argEmail = array();
                if (!is_empty($file_name)) {
                    $argEmail['file_attachment'] = $system['system_uploads_directory'] . '/' . $file_name;
                    $argEmail['file_attachment_name'] = $args['file_name'];
                }

                $content = "";
                $isOther = false;
                for ($i = 0; $i < count($cates); $i++) {
                    $content = $content . "- <b>" . $cates[$i] . "-</b>: " . $contents[$i] . "<br/><br/>";
                }
                $smarty->assign('title', $args['report_name']);
                $smarty->assign('content', $content);
                $smarty->assign('class_name', convertText4Web($class['group_title']));

                $argEmail['action'] = "create_report";
                $argEmail['subject'] = $args['report_name'];
                $argEmail['school_id'] = $args['school_id'];
                $argEmail['user_id'] = $user->_data['user_id'];


                $receivers = "";
                foreach ($args['child'] as $child_id) {
                    $parent_email = array();
                    //$child = $childDao->getChild($child_id);
                    $child = getChildData($child_id, CHILD_INFO);
                    if (!is_null($child)) {
                        if (!is_empty($child['parent_email'])) {
                            $parent_email[] = $child['parent_email'];

                        }
                        //$parents = $childDao->getParent($child_id);
                        $parents = getChildData($child_id, CHILD_PARENTS);
                        if (!is_null($parents)) {
                            foreach ($parents as $parent) {
                                $reports = $reportDao->getChildReport($child_id);
                                foreach ($reports as $report) {
                                    if (in_array($report['report_id'], $reportIds)) {
                                        //thông báo về cho phụ huynh
                                        $userDao->postNotifications($parent['user_id'], NOTIFICATION_NEW_REPORT, NOTIFICATION_NODE_TYPE_CHILD,
                                            $report['report_id'], convertText4Web($_POST['title']), $child_id, convertText4Web($child['child_name']));
                                    }
                                }

                                // lấy danh sách email
                                if (!is_empty($parent['user_email'])) {
                                    $parent_email[] = $parent['user_email'];
                                }
                            }
                        }

                        $parent_email = array_unique($parent_email);
                        $emai_str = implode(",", $parent_email);
                        $receivers = $receivers . ',' . $emai_str;

                        $smarty->assign('child_name', convertText4Web($child['child_name']));
                        $argEmail['content'] = $smarty->fetch("ci/email_templates/create_report.tpl");
                        $argEmail['receivers'] = $emai_str;

                        $argEmail['delete_after_sending'] = 1;
                        // $mailDao->insertEmail($argEmail);
                    }
                }
                // Thông báo đến quản lý trường
//                $userManagerIds = getUserIdsManagerReceiveNotify($class['school_id'], 'reports', $school['page_admin']);
                $userManagerIds = getUserIdsReceiveNotifyOfSchoolForClass($notify_settings, 'reports', $class['school_id'], $school['page_admin']);
                $children_report = '';
                foreach ($reportIds as $reportId) {
                    $report = $reportDao->getReportById($reportId);
                    $child = getChildData($report['child_id'], CHILD_INFO);
                    $children_report .= ' ' . $child['child_name'] . ',';
//                    if (!is_null($child)) {
//                        $userDao->postNotifications($userManagerIds, NOTIFICATION_NEW_REPORT, NOTIFICATION_NODE_TYPE_SCHOOL,
//                            $reportId, convertText4Web($_POST['title']), $school['page_name'], convertText4Web($child['child_name']));
//                    }
                }
                $children_report = trim($children_report, ' ,');
                if ($children_report != '') {
                    $userDao->postNotifications($userManagerIds, NOTIFICATION_NEW_REPORT, NOTIFICATION_NODE_TYPE_SCHOOL,
                        $school['page_id'], convertText4Web($_POST['title']), $school['page_name'], convertText4Web($children_report));
                }

                /* Coniu - Tương tác trường */
                setIsAddNew($class['school_id'], 'report_created', $reportIds);
                /* Coniu - END */
            }

            $db->commit();

            //return_json(array('callback' => 'window.location = "'.$system['system_url'].'/class/'.$class['group_name'].'/reports";'));
            return_json(array('success' => true, 'message' => __("Contact book has been created")));
            break;

        case 'edit':
            $data = $reportDao->getReportById($_POST['report_id']);
            if (is_null($data)) {
                _error(404);
            }
            $args = array();
            $args['report_id'] = $_POST['report_id'];
            $args['is_notified'] = (isset($_POST['notify_immediately']) && $_POST['notify_immediately'] == 'on') ? 1 : 0;
            $args['report_name'] = trim($_POST['title']);

            $args['date'] = 'null';
            if ($args['is_notified'] == 1) {
                $args['date'] = $date;
            }

            $args['source_file'] = $data['source_file_path'];
            $args['file_name'] = convertText4Web($data['file_name']);
            if (!$_POST['is_file']) {
                $args['source_file'] = "";
                $args['file_name'] = "";
            }
            $file_name = "";
            // Upload file đính kèm
            if (file_exists($_FILES['file']['tmp_name'])) {
                // check file upload enabled
                if (!$system['file_enabled']) {
                    throw new Exception(__("This feature has been disabled"));
                }

                // valid inputs
                if (!isset($_FILES["file"]) || $_FILES["file"]["error"] != UPLOAD_ERR_OK) {
                    throw new Exception(__("Something wrong with upload! Is 'upload_max_filesize' set correctly?"));
                }

                // check file size
                $max_allowed_size = $system['max_file_size'] * 1024;
                if ($_FILES["file"]["size"] > $max_allowed_size) {
                    throw new Exception(__("The file size is so big"));
                }

                // check file extesnion
                $extension = get_extension($_FILES['file']['name']);
                if (!valid_extension($extension, $system['file_report_extensions'])) {
                    throw new Exception(__("The file type is not valid or not supported"));
                }

                /* check & create uploads dir */
                $depth = '../../../../../';
                $folder = 'reports/' . $class['school_id'];
                if (!file_exists($depth . $system['system_uploads_directory'] . '/' . $folder)) {
                    @mkdir($depth . $system['system_uploads_directory'] . '/' . $folder, 0777, true);
                }
                if (!file_exists($depth . $system['system_uploads_directory'] . '/' . $folder . '/' . date('Y'))) {
                    @mkdir($depth . $system['system_uploads_directory'] . '/' . $folder . '/' . date('Y'), 0777, true);
                }
                if (!file_exists($system['system_uploads_directory'] . '/' . $folder . '/' . date('Y') . '/' . date('m'))) {
                    @mkdir($depth . $system['system_uploads_directory'] . '/' . $folder . '/' . date('Y') . '/' . date('m'), 0777, true);
                }

                /* prepare new file name */
                $directory = $folder . '/' . date('Y') . '/' . date('m') . '/';
                $prefix = $system['uploads_prefix'] . '_' . md5(time() * rand(1, 9999));
                $file_name = $directory . $prefix . '.' . $extension;
                $path = $depth . $system['system_uploads_directory'] . '/' . $file_name;

                /* check if the file uploaded successfully */
                if (!@move_uploaded_file($_FILES['file']['tmp_name'], $path)) {
                    throw new Exception(__("Sorry, can not upload the file"));
                }
                $args['source_file'] = $file_name;
                $args['file_name'] = $_FILES['file']['name'];
            }

            if ($_POST['report_template_id'] == null) {
                if (is_empty($args['source_file']) && !isset($_POST['report_category_ids'])) {
                    throw new Exception(__("You must have an attachment or contact book content"));
                }
            } else {
                if (is_empty($args['source_file']) && !isset($_POST['category_ids'])) {
                    throw new Exception(__("You must have an attachment or contact book content"));
                }
            }

            // 1. Cập nhật bảng ci_report
            $reportDao->updateReport($args);

            // Nếu không chọn mẫu khác thì update category cũ
            if ($_POST['report_template_id'] == null) {
                // 2. cập nhật bảng ci_report_category
                // Lấy id category
                $categoryIds = array();
                $categoryIds = $_POST['report_category_ids'];
                // Lặp mảng categoryIds lấy nội dung gửi lên tương ứng
                $category = array();
                if (count($categoryIds) > 0) {
                    foreach ($categoryIds as $id) {
                        $category['report_category_id'] = $id;
                        $category['multi_content'] = isset($_POST['report_suggest_' . $id]) ? $_POST['report_suggest_' . $id] : array();
                        $category['report_category_content'] = $_POST['report_content_' . $id];
//                    $categorys[] = $category;
                        // Update report category
                        $reportDao->updateReportCategory($category);
                    }
                }
            } else {
                // Nếu chọn mẫu khác thì insert category mới
                //3. Xóa report category cũ
                $reportDao->deleteReportDetail($args['report_id']);

                //4. Thêm report category mới
                // Lấy id category
                $categoryIds = array();
                $categoryIds = $_POST['category_ids'];
                // Lặp mảng categoryIds lấy nội dung gửi lên tương ứng
                $categorys = array();
                $category = array();
                if (count($categoryIds) > 0) {
                    foreach ($categoryIds as $k => $id) {
                        $category['multi_content'] = isset($_POST['suggest_' . $id]) ? $_POST['suggest_' . $id] : array();
                        $category['report_category_content'] = $_POST['content_' . $id];
                        $category['report_category_name'] = $_POST['category_name_' . $id];
                        // Lấy toàn bộ gợi ý của category
                        $categoryTemp = $reportDao->getReportTemplateCategoryDetail($id);
                        $suggests = $categoryTemp['suggests'];
                        $category['template_multi_content'] = array();
                        foreach ($suggests as $row) {
                            $category['template_multi_content'][] = convertText4Web($row);
                        }
                        $categorys[] = $category;
                    }
                    // Insert chi tiết sổ liên lạc (bảng ci_report_category)
                    $reportDao->insertReportDetail($_POST['report_id'], $categorys);
                }
            }

            // Gửi email và thông báo đến phụ huynh
            if ($args['is_notified']) {
                $argEmail = array();
                if (!is_empty($file_name)) {
                    $argEmail['file_attachment'] = $system['system_uploads_directory'] . '/' . $file_name;
                    $argEmail['file_attachment_name'] = $args['file_name'];
                } else {
                    $argEmail['file_attachment'] = $system['system_uploads_directory'] . '/' . $args['source_file'];
                    $argEmail['file_attachment_name'] = $args['file_name'];
                }

                $content = "";
                $isOther = false;
                for ($i = 0; $i < count($cates); $i++) {
                    $content = $content . "- <b>" . $cates[$i] . "-</b>: " . $contents[$i] . "<br/><br/>";
                }
                $smarty->assign('title', $args['report_name']);
                $smarty->assign('content', $content);
                $smarty->assign('class_name', convertText4Web($class['group_title']));

                $argEmail['action'] = "create_report";
                $argEmail['subject'] = $args['report_name'];
                $argEmail['school_id'] = $args['school_id'];
                $argEmail['user_id'] = $user->_data['user_id'];


                $receivers = "";
                $parent_email = array();
                //$child = $childDao->getChild($data['child_id']);
                $child = getChildData($data['child_id'], CHILD_INFO);
                if (!is_null($child)) {
                    if (!is_empty($child['parent_email'])) {
                        $parent_email[] = $child['parent_email'];

                    }
                    //$parents = $childDao->getParent($child['child_id']);
                    $parents = getChildData($child['child_id'], CHILD_PARENTS);
                    if (!is_null($parents)) {
                        $parentIds = array_keys($parents);
                        //thông báo về cho phụ huynh
                        $userDao->postNotifications($parentIds, NOTIFICATION_UPDATE_REPORT, NOTIFICATION_NODE_TYPE_CHILD,
                            $_POST['report_id'], convertText4Web($_POST['title']), $child['child_id'], convertText4Web($child['child_name']));
                        foreach ($parents as $parent) {
                            // lấy danh sách email
                            if (!is_empty($parent['user_email'])) {
                                $parent_email[] = $parent['user_email'];
                            }
                        }
                    }

                    $parent_email = array_unique($parent_email);
                    $emai_str = implode(",", $parent_email);
                    $receivers = $receivers . ',' . $emai_str;

                    $smarty->assign('child_name', convertText4Web($child['child_name']));
                    $argEmail['content'] = $smarty->fetch("ci/email_templates/create_report.tpl");
                    $argEmail['receivers'] = $emai_str;

                    $argEmail['delete_after_sending'] = 1;
                    // $mailDao->insertEmail($argEmail);
                }

                // Thông báo đến quản lý trường
                $school = getSchoolData($class['school_id'], SCHOOL_INFO);
                $userManagerIds = getUserIdsReceiveNotifyOfSchoolForClass($notify_settings, 'reports', $class['school_id'], $school['page_admin']);
                $child = getChildData($data['child_id'], CHILD_INFO);
                if (!is_null($child)) {
                    $userDao->postNotifications($userManagerIds, NOTIFICATION_UPDATE_REPORT, NOTIFICATION_NODE_TYPE_SCHOOL,
                        $data['report_id'], convertText4Web($_POST['title']), $school['page_name'], convertText4Web($child['child_name']));
                }
            }

            $db->commit();
            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/class/' . $class['group_name'] . '/reports";'));
            break;

        case 'notify':
            $db->begin_transaction();
            //Lấy ra thông tin thông báo
            $data = $reportDao->getReportById($_POST['id']);

            if (is_null($data)) {
                _error(404);
            }
            $data['file_name'] = convertText4Web($data['file_name']);

            /*if ($data['created_user_id'] != $user->_data['user_id']) {
                _error(403);
            }*/

            if (!$data['is_notified']) {
                $argEmail = array();
                if (!is_empty($data['source_file'])) {
                    $argEmail['file_attachment'] = $system['system_uploads_directory'] . '/' . $data['source_file_path'];
                    $argEmail['file_attachment_name'] = $data['file_name'];
                }

                $content = "";
                for ($i = 0; $i < count($data['detail']); $i++) {
                    $content = $content . "- <b>" . $data['detail'][$i]['report_detaill_name'] . "-</b>: " . $data['detail'][$i]['report_detail_content'] . "<br/><br/>";
                }
                $smarty->assign('title', $data['report_name']);
                $smarty->assign('content', $content);
                $smarty->assign('class_name', convertText4Web($class['group_title']));

                $argEmail['action'] = "create_report";
                $argEmail['subject'] = $data['report_name'];
                $argEmail['school_id'] = $data['school_id'];
                $argEmail['user_id'] = $user->_data['user_id'];


                $receivers = "";
                $parent_email = array();
                //$child = $childDao->getChild($data['child_id']);
                $child = getChildData($data['child_id'], CHILD_INFO);
                if (!is_null($child)) {
                    if (!is_empty($child['parent_email'])) {
                        $parent_email[] = $child['parent_email'];
                    }

                    $parents = getChildData($data['child_id'], CHILD_PARENTS);
                    if (!is_null($parents)) {
                        $parentIds = array_keys($parents);
                        //thông báo về cho phụ huynh
                        $userDao->postNotifications($parentIds, NOTIFICATION_NEW_REPORT, NOTIFICATION_NODE_TYPE_CHILD,
                            $_POST['id'], convertText4Web($data['report_name']), $child['child_id'], convertText4Web($child['child_name']));
                        foreach ($parents as $parent) {

                            // lấy danh sách email
                            if (!is_empty($parent['user_email'])) {
                                $parent_email[] = $parent['user_email'];
                            }
                        }
                    }

                    $parent_email = array_unique($parent_email);
                    $emai_str = implode(",", $parent_email);
                    $receivers = $receivers . ',' . $emai_str;

                    $smarty->assign('child_name', convertText4Web($child['child_name']));
                    $argEmail['content'] = $smarty->fetch("ci/email_templates/create_report.tpl");
                    $argEmail['receivers'] = $emai_str;

                    $argEmail['delete_after_sending'] = 1;
                    //$mailDao->insertEmail($argEmail);
                }
                // Thông báo đến quản lý trường
                $school = getSchoolData($class['school_id'], SCHOOL_INFO);
                $userManagerIds = getUserIdsReceiveNotifyOfSchoolForClass($notify_settings, 'reports', $class['school_id'], $school['page_admin']);
                $report = $reportDao->getReportById($data['report_id']);
                $child = getChildData($report['child_id'], CHILD_INFO);
                if (!is_null($child)) {
                    $userDao->postNotifications($userManagerIds, NOTIFICATION_NEW_REPORT, NOTIFICATION_NODE_TYPE_SCHOOL,
                        $data['report_id'], convertText4Web($data['report_name']), $school['page_name'], convertText4Web($child['child_name']));
                }

                /* Coniu - Tương tác trường */
                setIsAddNew($class['school_id'], 'report_created', [$data['report_id']]);
                /* Coniu - END */
            }

            // Cập nhật trạng thái đã gửi thông báo.
            $reportDao->updateStatusToNotified($_POST['id']);

            $db->commit();
            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/class/' . $_POST['username'] . '/reports";'));
            break;

        case 'notify_all':
            // Thông báo tất cả những sổ liên lạc chưa thông báo
            $db->begin_transaction();

            // Lấy ra tất cả những sổ liên lạc của lớp chưa được thông báo
            $noNotifyReports = $reportDao->getClassReportNoNotify($class['group_id']);

            foreach ($noNotifyReports as $data) {
                $data['file_name'] = convertText4Web($data['file_name']);
                if (!$data['is_notified']) {
                    $argEmail = array();
                    if (!is_empty($data['source_file'])) {
                        $argEmail['file_attachment'] = $system['system_uploads_directory'] . '/' . $data['source_file_path'];
                        $argEmail['file_attachment_name'] = $data['file_name'];
                    }

                    $content = "";
                    for ($i = 0; $i < count($data['detail']); $i++) {
                        $content = $content . "- <b>" . $data['detail'][$i]['report_detaill_name'] . "-</b>: " . $data['detail'][$i]['report_detail_content'] . "<br/><br/>";
                    }
                    $smarty->assign('title', $data['report_name']);
                    $smarty->assign('content', $content);
                    $smarty->assign('class_name', convertText4Web($class['group_title']));

                    $argEmail['action'] = "create_report";
                    $argEmail['subject'] = $data['report_name'];
                    $argEmail['school_id'] = $data['school_id'];
                    $argEmail['user_id'] = $user->_data['user_id'];


                    $receivers = "";
                    $parent_email = array();
                    //$child = $childDao->getChild($data['child_id']);
                    $child = getChildData($data['child_id'], CHILD_INFO);
                    if (!is_null($child)) {
                        if (!is_empty($child['parent_email'])) {
                            $parent_email[] = $child['parent_email'];
                        }

                        $parents = getChildData($data['child_id'], CHILD_PARENTS);
                        if (!is_null($child)) {
                            $parentIds = array_keys($parents);
                            //thông báo về cho phụ huynh
                            $userDao->postNotifications($parentIds, NOTIFICATION_NEW_REPORT, NOTIFICATION_NODE_TYPE_CHILD,
                                $data['report_id'], convertText4Web($data['report_name']), $child['child_id'], convertText4Web($child['child_name']));
                            foreach ($parents as $parent) {
                                // lấy danh sách email
                                if (!is_empty($parent['user_email'])) {
                                    $parent_email[] = $parent['user_email'];
                                }
                            }
                        }

                        $parent_email = array_unique($parent_email);
                        $emai_str = implode(",", $parent_email);
                        $receivers = $receivers . ',' . $emai_str;

                        $smarty->assign('child_name', convertText4Web($child['child_name']));
                        $argEmail['content'] = $smarty->fetch("ci/email_templates/create_report.tpl");
                        $argEmail['receivers'] = $emai_str;

                        $argEmail['delete_after_sending'] = 1;
                        //$mailDao->insertEmail($argEmail);
                    }

                    // Thông báo đến quản lý trường
                    $school = getSchoolData($class['school_id'], SCHOOL_INFO);
                    $userManagerIds = getUserIdsManagerReceiveNotify($class['school_id'], 'reports', $school['page_admin']);
//                    $report = $reportDao->getReportById($data['report_id']);
                    $child = getChildData($report['child_id'], CHILD_INFO);
                    if (!is_null($child)) {
                        $userDao->postNotifications($userManagerIds, NOTIFICATION_NEW_REPORT, NOTIFICATION_NODE_TYPE_SCHOOL,
                            $data['report_id'], convertText4Web($data['report_name']), $school['page_name'], convertText4Web($data['child_name']));
                    }

                    /* Coniu - Tương tác trường */
                    setIsAddNew($class['school_id'], 'report_created', [$data['report_id']]);
                    /* Coniu - END */
                }
                // Cập nhật trạng thái đã gửi thông báo.
                $reportDao->updateStatusToNotified($data['report_id']);
            }

            $db->commit();
            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/class/' . $_POST['username'] . '/reports";'));
            break;

        case 'template_detail':
            if (!isset($_POST['template_id']) || !is_numeric($_POST['template_id'])) {
                _error(404);
            }
            $results = $reportDao->getReportTemplate($_POST['template_id'], 1);
            $smarty->assign('results', $results);

            $return['results'] = $smarty->fetch("ci/ajax.reporttemplatedetail.tpl");
            //$return['no_data'] = (count($results['children']) == 0);

            return_json($return);
            break;

        case 'edit_temp':
            $db->begin_transaction();
            if (!isset($_POST['report_template_id'])) {
                _error(404);
            }
            $args = array();
            $args['report_template_id'] = $_POST['report_template_id'];
            $args['template_name'] = trim($_POST['template_name']);
            $args['level'] = CLASS_LEVEL;
            $args['class_id'] = $class['group_id'];
            $args['class_level_id'] = 0;
            $args['school_id'] = $class['school_id'];

            // 1. Update bảng ci_report_template
            $reportDao->updateReportTemplate($args);

            // 2. Update bảng ci_report_template_detail
            // Xóa toàn bộ chi tiết mẫu cũ
            $reportDao->deleteReportTemplateDetail($_POST['report_template_id']);

            // Lấy dữ liệu mới
            $categoryIds = isset($_POST['category_ids']) ? $_POST['category_ids'] : array();

            // Lặp mảng $categoryIds lấy nội dung tương ứng
            $contents = array();
            foreach ($categoryIds as $id) {
                $contents[] = $_POST['content_' . $id];
            }

            // Insert vào bảng ci_report_template_detail
            $reportDao->insertReportTemplateDetail($_POST['report_template_id'], $categoryIds, $contents);

            $db->commit();
            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/class/' . $_POST['username'] . '/reports/listtemp";'));
            break;
        case 'add_temp':
            $db->begin_transaction();
            $args = array();
            $args['template_name'] = trim($_POST['template_name']);
            $args['class_id'] = $class['group_id'];
            $args['school_id'] = $class['school_id'];
            $args['class_level_id'] = 0;
            $args['level'] = CLASS_LEVEL;

            // 1. Insert vào bảng ci_report_template
            $reportTemplateId = $reportDao->insertReportTemplate($args);

            $categoryIds = isset($_POST['category_ids']) ? $_POST['category_ids'] : array();
            // Lặp mảng $categoryIds lấy nội dung tương ứng
            $contents = array();
            foreach ($categoryIds as $id) {
                $contents[] = $_POST['content_' . $id];
            }

            // 2. Insert vào bảng ci_report_template_detail
            $reportDao->insertReportTemplateDetail($reportTemplateId, $categoryIds, $contents);

            $db->commit();
            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/class/' . $_POST['username'] . '/reports/listtemp";'));
            break;
        case 'cate_detail':
            if (!isset($_POST['report_template_category_id']) || !is_numeric($_POST['report_template_category_id'])) {
                _error(404);
            }

            $results = $reportDao->getReportTemplateCategoryDetail($_POST['report_template_category_id']);
            $smarty->assign('results', $results);

            $return['results'] = $smarty->fetch("ci/class/ajax.class.categorydetail.tpl");
            //$return['no_data'] = (count($results['children']) == 0);

            return_json($return);
            break;
        case 'delete_report':
            $db->begin_transaction();
            if (!$_POST['id'] || !is_numeric($_POST['id'])) {
                _error(404);
            }
            $reportDao->deleteReport($_POST['id']);

            $db->commit();
            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/class/' . $_POST['username'] . '/reports";'));
            break;

        case 'delete_template':
            $db->begin_transaction();
            if (!$_POST['id'] || !is_numeric($_POST['id'])) {
                _error(404);
            }
            // Xóa trong bảng ci_report_template
            $reportDao->deleteReportTemplate($_POST['id']);

            // Xóa trong bảng ci_report_template_detail
            $reportDao->deleteReportTemplateDetail($_POST['id']);

            $db->commit();
            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/class/' . $_POST['username'] . '/reports/listtemp";'));
            break;

        case 'child_search':
            /*if(!isset($_POST['child_id']) || !is_numeric($_POST['child_id'])) {
                _error(404);
            }*/
            if (is_numeric($_POST['child_id'])) {
                $results = $reportDao->getClassReportOfChild($class['group_id'], $_POST['child_id']);
            } else {
                $results = $reportDao->getClassReport($class['group_id']);
            }

            $countNotNotify = 0;
            foreach ($results as $row) {
                if (!$row['is_notified']) {
                    $countNotNotify = $countNotNotify + 1;
                }
            }
            $smarty->assign('countNotNotify', $countNotNotify);

            $smarty->assign('results', $results);
            $return['results'] = $smarty->fetch("ci/class/ajax.reportchild.tpl");
            $return['no_data'] = (count($results) == 0);
            return_json($return);
            break;
        case 'delete_all_select':
            $db->begin_transaction();
            $ids = isset($_POST['ids']) ? $_POST['ids'] : array();
            if (count($ids) > 0) {
                foreach ($ids as $id) {
                    $reportDao->deleteReport($id);
                }
            }
            $db->commit();
            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/class/' . $_POST['username'] . '/reports";'));
            break;

        case 'notify_all_select':
            $db->begin_transaction();
            $ids = isset($_POST['ids']) ? $_POST['ids'] : array();

            if (count($ids) > 0) {
                foreach ($ids as $id) {
                    $report = $reportDao->getReportById($id);
                    //$parents = $childDao->getParent($child['child_id']);
                    $child = getChildData($report['child_id'], CHILD_INFO);
                    $parents = getChildData($report['child_id'], CHILD_PARENTS);
                    if (!is_null($parents)) {
                        $parentIds = array_keys($parents);
                        //thông báo về cho phụ huynh
                        $userDao->postNotifications($parentIds, NOTIFICATION_NEW_REPORT, NOTIFICATION_NODE_TYPE_CHILD,
                            $id, convertText4Web($report['report_name']), $child['child_id'], convertText4Web($child['child_name']));
                    }
                }
            }

            // Đổi trạng thái thành đã thông báo
            $reportDao->updateNotifiedForReports($ids);

            $db->commit();
            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/class/' . $_POST['username'] . '/reports";'));
            break;

        default:
            _error(400);
            break;
    }
} catch (Exception $e) {
    $db->rollback();
    return_json(array('error' => true, 'message' => $e->getMessage()));
} finally {
    $db->autocommit(true);
}

?>