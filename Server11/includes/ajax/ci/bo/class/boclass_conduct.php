<?php
/**
 * Package: ajax/ci/bo/class
 *
 * @package Inet
 * @author ManhDD
 */

// fetch bootstrap
require('../../../../../bootstrap.php');
// check AJAX Request
is_ajax();
check_login();

// user access
if(!$system['system_public']) {
    user_access();
}
// initialize the return array
$return = array();
$return['callback'] = 'window.location.replace(response.path);';

// check username
if(is_empty($_POST['username']) || !valid_username($_POST['username'])) {
    _error(404);
}

include_once(DAO_PATH.'dao_school.php');
include_once(DAO_PATH.'dao_class.php');
include_once(DAO_PATH.'dao_child.php');
include_once(DAO_PATH.'dao_parent.php');
include_once(DAO_PATH.'dao_conduct.php');
include_once(DAO_PATH.'dao_user.php');
include_once(DAO_PATH.'dao_service.php');
include_once(DAO_PATH.'dao_teacher.php');
$schoolDao = new SchoolDAO();
$classDao = new ClassDAO();
$childDao = new ChildDAO();
$conductDao = new ConductDAO();
$parentDao = new ParentDAO();
$userDao = new UserDAO();
$serviceDao = new ServiceDAO();
$teacherDao = new TeacherDAO();

//Lấy ra thông tin lớp
//$class = $classDao->getClassByUsername($_POST['username'], $user->_data['user_id']);
$class = getClassDataByUsername($_POST['username'], CLASS_INFO);
if (is_null($class)) {
    _error(403);
}

$smarty->assign('username', $_POST['username']);
//$school = $schoolDao->getSchoolByClass($class['group_id']);
$school = getSchoolData($class['school_id'], SCHOOL_INFO);
try {
    $return = array();
    $db->autocommit(false);
    switch ($_POST['do']) {
        case 'edit':
            $db->begin_transaction();
            $args = array();
            $args['conduct_id'] = $_POST['conduct_id'];
            $args['conduct_semester1'] = $_POST['conduct_id1'];
            $args['conduct_semester2'] = $_POST['conduct_id2'];
            $args['conduct_semester3'] = $_POST['conduct_id3'];

            $conductDao->editConduct($args);
            $db->commit();
            //return_json(array('callback' => 'window.location = "'.$system['system_url'].'/class/'.$class['group_name'].'/reports";'));
            return_json( array('success' => true, 'message' => __("Conduct has been updated")) );
            break;
        default:
            _error(400);
            break;
    }
} catch (Exception $e) {
    $db->rollback();
    return_json( array('error' => true, 'message' => $e->getMessage()) );
} finally {
    $db->autocommit(true);
}

?>