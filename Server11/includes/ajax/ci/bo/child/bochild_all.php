<?php
/**
 * Package: ajax/ci/bo
 * 
 * @package ConIu
 * @author QuanND
 */

// fetch bootstrap
require('../../../../../bootstrap.php');

// fetch image class
require(ABSPATH . 'includes/class-image.php');

// check AJAX Request
is_ajax();
check_login();

// initialize the return array
$return = array();
$return['callback'] = 'window.location.replace(response.path);';

include_once(DAO_PATH . 'dao_child.php');
include_once(DAO_PATH . 'dao_parent.php');
include_once(DAO_PATH . 'dao_school.php');
include_once(DAO_PATH . 'dao_class.php');
include_once(DAO_PATH . 'dao_teacher.php');
include_once(DAO_PATH . 'dao_user.php');
include_once(DAO_PATH . 'dao_event.php');
include_once(DAO_PATH . 'dao_tuition.php');
include_once(DAO_PATH . 'dao_attendance.php');
include_once(DAO_PATH . 'dao_service.php');
include_once(DAO_PATH . 'dao_report.php');
include_once(DAO_PATH . 'dao_feedback.php');
include_once(DAO_PATH . 'dao_user_manage.php');
include_once(DAO_PATH . 'dao_pickup.php');

$childDao = new ChildDAO();
$parentDao = new ParentDAO();
$schoolDao = new SchoolDAO();
$classDao = new ClassDAO();
$teacherDao = new TeacherDAO();
$userDao = new UserDAO();
$eventDao = new EventDAO();
$tuitionDao = new TuitionDAO();
$attendanceDao = new AttendanceDAO();
$serviceDao = new ServiceDAO();
$reportDao = new ReportDAO();
$feedbackDao = new FeedbackDAO();
$userManDao = new UserManageDAO();
$pickupDao = new PickupDAO();

// valid inputs
if(!isset($_POST['child_id']) || !is_numeric($_POST['child_id'])) {
    _error(400);
}

$child = getChildDataById($_POST['child_id'], CHILD_INFO);

//Lấy ra thông tin lớp
if (is_null($child)) {
    _error(403);
}

try {
    $db->autocommit(false);

    switch ($_POST['do']) {
        case 'change_child':
            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/child/' . $_POST['child_id'] . '";'));
            break;
        case 'reg_event':
            /**
             * Xứ lý khi user đăng ký/hủy tham gia sự kiện
             */
            $db->begin_transaction();

            $childId = (isset($_POST['childId']) && ($_POST['childId'] > 0)) ? $_POST['childId'] : 0;
            $oldChildId = (isset($_POST['old_child_id']) && ($_POST['old_child_id'] > 0)) ? $_POST['old_child_id'] : 0;

            $parentId = (isset($_POST['parent_id']) && ($_POST['parent_id'] > 0)) ? $_POST['parent_id'] : 0;
            $oldParentId = (isset($_POST['old_parent_id']) && ($_POST['old_parent_id'] > 0)) ? $_POST['old_parent_id'] : 0;

            //Đăng ký cho trẻ
            if (($childId > 0) && ($oldChildId == 0)) {
                $eventDao->insertParticipants($_POST['event_id'], PARTICIPANT_TYPE_CHILD, [$childId]);
                //Thông báo cho Quản lý và Giáo viên lớp
                // tắt
//                 notifySchoolManagerAndTearcherOfView($childId, NOTIFICATION_REGISTER_EVENT_CHILD, $_POST['event_id'], $_POST['event_name'], '', 'events');
            } elseif (($childId == 0) && ($oldChildId > 0)) {
                //Huỷ đăng ký trẻ
                $eventDao->deleteParticipants($_POST['event_id'], PARTICIPANT_TYPE_CHILD, [$oldChildId]);
                //Thông báo cho Quản lý và Giáo viên lớp
                // tắt
//                 notifySchoolManagerAndTearcherOfView($oldChildId, NOTIFICATION_CANCEL_REGISTRATION_EVENT_CHILD, $_POST['event_id'], $_POST['event_name'], '', 'events');
            }

            if (($parentId > 0) && ($oldParentId == 0)) {
                //Đăng ký phụ huynh
                $eventDao->insertParticipants($_POST['event_id'], PARTICIPANT_TYPE_PARENT, [$user->_data['user_id']]);

                //Thông báo cho Quản lý và Giáo viên lớp
                // Tắt
//                notifySchoolManagerAndTearcherOfView($_POST['child_id'], NOTIFICATION_REGISTER_EVENT_PARENT, $_POST['event_id'],
//                    $_POST['event_name'], convertText4Web($user->_data['user_fullname']), 'events');
            } else if (($parentId == 0) && ($oldParentId > 0)) {
                //Huỷ đăng ký phụ huynh
                $eventDao->deleteParticipants($_POST['event_id'], PARTICIPANT_TYPE_PARENT, [$user->_data['user_id']]);

                // Thông báo cho Quản lý và Giáo viên lớp
                // Tắt
//                notifySchoolManagerAndTearcherOfView($_POST['child_id'], NOTIFICATION_CANCEL_REGISTRATION_EVENT_PARENT, $_POST['event_id'],
//                    $_POST['event_name'], convertText4Web($user->_data['user_fullname']), 'events');
            }
            $db->commit();
            return_json(array('success' => true, 'message' => __("Registration info have been updated")));
            break;

        case 'tuition':
            $db->begin_transaction();

            $tuition = $tuitionDao->getTuitionChildById($_POST['id']);
            if (is_null($tuition) || $tuition['child_id'] != $_POST['child_id']) {
                _error(404);
            }
            //Cập nhật trạng thái của học phí
            $result = $tuitionDao->updateTuitionChildStatus($_POST['id'], TUITION_CHILD_PARENT_PAID);

            if ($result > 0) {
                //$child = $childDao->getChild($_POST['child_id']);
                $child = getChildData($_POST['child_id'], CHILD_INFO);
                //Thông báo cho Quản lý và Giáo viên lớp
                notifySchoolManagerOfView($_POST['child_id'], NOTIFICATION_CONFIRM_TUITION, $tuition['tuition_id'], $tuition['month'], convertText4Web($child['child_name']), 'tuitions');
//            $school = $schoolDao->getSchoolById($tuition['school_id']);
//
//            $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'tuitions', $school['page_admin']);
//            $userDao->postNotifications($userManagerIds, NOTIFICATION_CONFIRM_TUITION, NOTIFICATION_NODE_TYPE_SCHOOL, $tuition['tuition_id'], $tuition['month'], $school['page_name'], convertText4Web($child['child_name']));
            }
            $db->commit();
            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/child/' . $_POST['child_id'] . '/tuitions";'));
            break;

        case 'attendance':
            $data = array();
            $attendances = $attendanceDao->getAttendanceChild($_POST['child_id'], $_POST['fromDate'], $_POST['toDate']);
            if (count($attendances) > 0) {
                $data['attendance'] = $attendances;
                /* assign variables */
                $smarty->assign('data', $data);
                /* return */
                $return['results'] = $smarty->fetch("ci/child/ajax.child.attendance.tpl");
            }
            return_json($return);
            break;

        case 'service_history':
            $return = array();

            $serviceId = (isset($_POST['service_id']) && ($_POST['service_id'] > 0)) ? $_POST['service_id'] : 0;
            $begin = isset($_POST['begin']) ? $_POST['begin'] : (date($system['date_format'], mktime(0, 0, 0, date("m"), 1, date("Y"))));
            $end = isset($_POST['end']) ? $_POST['end'] : (toSysDate($date));

            $cbs_usage = $serviceDao->getCountBasedServiceAChild($_POST['school_id'], $_POST['child_id'], $begin, $end, $serviceId);
            $smarty->assign('cbs_usage', $cbs_usage);

            $return['results'] = $smarty->fetch("ci/child/ajax.service.history.tpl");
            return_json($return);
            break;

        case 'resign':
            $db->begin_transaction();
            //Xử lý sự kiện khi phụ huynh xin nghỉ cho con
            //Xử lý sự kiện khi phụ huynh xin nghỉ cho con
            $args = array();
            $atds = array();

            //1. Lấy ra thông tin lớp của trẻ
            //memcache??
            //$class = $classDao->getClassOfChild($_POST['child_id']);
            $class = getChildData($_POST['child_id'], CHILD_CLASS);
            $args['class_id'] = $class['group_id'];

            $start_date = toDBDate($_POST['start_date']);
            $end_date = toDBDate($_POST['end_date']);

            $atds['start_date'] = $start_date;
            $atds['end_date'] = $end_date;
            $dbDate = toDBDate(toSysDate($date));

            $sent = false;
            $detail_id = 0;
            if (strtotime($start_date) < strtotime($dbDate)) {
                throw new Exception(__("The started date for asking leave is not the date in the past"));
            }
            while (strtotime($start_date) <= strtotime($end_date)) {
                $args['attendance_date'] = toSysDate($start_date);
                //2. Kiểm tra xem đã có bản ghi
                $attendance = $attendanceDao->getAttendanceByClassNDate($args['class_id'], $args['attendance_date']);
                $atds['attendance_id'] = 0;
                if (is_null($attendance)) {
                    $args['absence_count'] = 0;
                    $args['present_count'] = 0;
                    $args['is_checked'] = 0;
                    //Lấy ra danh sách ID của tất cả trẻ trong lớp
                    $atds['attendance_id'] = $attendanceDao->insertAttendance($args);
                } else {
                    $atds['attendance_id'] = $attendance['attendance_id'];
                }
                $atds['child_id'] = $_POST['child_id'];
                $atds['reason'] = $_POST['reason'];
                $atds['isParent'] = 1;

                $result = $attendanceDao->saveAbsentChild($atds);

                $start_date = date("Y-m-d", strtotime("+1 day", strtotime($start_date)));

                if (!$sent && $result['is_updated']) {
                    $detail_id = $result['attendance_detail_id'];
                    $sent = true;
                }
            }

            if ($detail_id > 0) {
                //Thông báo giáo viên về việc xin nghỉ của con
                notifySchoolManagerAndTearcherOfView($_POST['child_id'], NOTIFICATION_ATTENDANCE_RESIGN,
                    $detail_id, '', '', 'attendance');
            }

            $db->commit();

            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/child/' . $_POST['child_id'] . '/attendance";'));
            break;

        case 'reg':
            $using_at = strtotime(toDBDate($_POST['using_at']));
            $date_now = strtotime(date('Y-m-d'));
            if ($using_at < $date_now) {
                throw new Exception(__("You can not register service in the past"));
            }
            $ncbServiceIds = isset($_POST['ncbServiceIds']) ? $_POST['ncbServiceIds'] : array();
            $cbServiceIds = isset($_POST['cbServiceIds']) ? $_POST['cbServiceIds'] : array();

            if ((count($ncbServiceIds) + count($cbServiceIds)) > 0) {
                $db->begin_transaction();
                if (count($ncbServiceIds) > 0) {
                    //Đăng ký dịch vụ THÁNG và theo ĐIỂM DANH
                    $serviceDao->registerNotCountBasedServiceForChild($_POST['child_id'], $ncbServiceIds);

                    foreach ($ncbServiceIds as $id) {
                        //memcache???
                        $service = $serviceDao->getServiceById($id);
                        //Thông báo cho Quản lý và Giáo viên lớp
                        notifySchoolManagerAndTearcherOfView($_POST['child_id'], NOTIFICATION_SERVICE_REGISTER_UNCOUNTBASED,
                            $id, convertText4Web($service['service_name']), '', 'services');
                    }
                }

                if (count($cbServiceIds) > 0) {
                    //Đăng ký dịch vụ theo SỐ LẦN
                    $serviceDao->registerCountBasedServiceForChild($_POST['child_id'], $cbServiceIds, $_POST['using_at']);

                    foreach ($cbServiceIds as $id) {
                        //memcache???
                        $service = $serviceDao->getServiceById($id);
                        //Thông báo cho Quản lý và Giáo viên lớp
                        notifySchoolManagerAndTearcherOfView($_POST['child_id'], NOTIFICATION_SERVICE_REGISTER_COUNTBASED,
                            $id, convertText4Web($service['service_name']), '', 'services');
                    }
                }

                $db->commit();
            }
            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/child/' . $_POST['child_id'] . '/services/reg";'));

            break;
        case 'service_list4record':
            $return = array();
            //Validate thông tin đầu vào
            $inputCorrect = true;
            if (!isset($_POST['using_at']) || ($_POST['using_at'] == '')) {
                $inputCorrect = false;
                _error(404);
            }
            if ($inputCorrect) {
                $cb_services = $serviceDao->getCountBasedServiceAChild4Register($_POST['school_id'], $_POST['child_id'], $_POST['using_at']);
                // đếm số dịch vụ chưa đã đăng ký
                $service_not_reg = 0;
                foreach ($cb_services as $service) {
                    if (!(isset($service['service_usage_id']) && ($service['service_usage_id'] > 0))) {
                        $service_not_reg = $service_not_reg + 1;
                    }
                }
                $smarty->assign('cb_services', $cb_services);
                $return['results'] = $smarty->fetch("ci/child/ajax.child.servicelist4record.tpl");
                $return['disableSave'] = ((count($cb_services) == 0) || ($service_not_reg == 0));
            } else {
                $return['results'] = __("No data or incorrect input data");
                $return['disableSave'] = true;
            }
            return_json($return);
            break;

        case 'pickup_history':
            $return = array();
            if (!isset($_POST['from_date']) || !isset($_POST['to_date'])) {
                modal(MESSAGE, __("Error"), __("The requested URL was not found on this server. That's all we know"));
            }

            //$class = $classDao->getClassOfChild($_POST['child_id']);
            $class = getChildData($_POST['child_id'], CHILD_CLASS);
            if (is_null($class)) {
                modal(MESSAGE, __("Error"), __("The class does not exist, it should be deleted."));
            }

            $total = 0;
            $results = $pickupDao->getChildPickupHistory($class['school_id'], $_POST['child_id'], toDBDate($_POST['from_date']), toDBDate($_POST['to_date']), $total);

            $smarty->assign('results', $results);
            $smarty->assign('total', $total);

            $return['results'] = $smarty->fetch("ci/child/ajax.child.pickup.tpl");
            return_json($return);
            break;

        case 'feedback':
            $return = array();
            //Validate thông tin đầu vào
            if (!isset($_POST['child_id']) || ($_POST['child_id'] <= 0)) {
                _error(404);
            }
            if ($_POST['feedback_level'] != CLASS_LEVEL && $_POST['feedback_level'] != SCHOOL_LEVEL) {
                _error(404);
            }
            //1. Lấy data để insert vào bảng ci_feedback
            // Lấy ra thông tin lớp của trẻ
            //$class = $classDao->getClassOfChild($_POST['child_id']);
            $child = getChildData($_POST['child_id'], CHILD_INFO);

            $args['child_id'] = $_POST['child_id'];
            $args['class_id'] = $child['class_id'];
            $args['school_id'] = $child['school_id'];
            $args['name'] = convertText4Web($user->_data['user_fullname']);
            $args['email'] = convertText4Web($user->_data['user_email']);
            $args['phone'] = $user->_data['user_phone'];
            $args['content'] = $_POST['content'];
            $args['level'] = $_POST['feedback_level'];
            $args['confirm'] = 0;
            $args['is_incognito'] = (isset($_POST['incognito']) && $_POST['incognito'] == 'on') ? 0 : 1;
            $args['user_id'] = $user->_data['user_id'];

            $feedback_id = $feedbackDao->insertFeedback($args);
            // Tăng lượt tương tác - TaiLA
            addInteractive($child['school_id'], 'pickup', 'parent_view', $feedback_id, 2);

            /*if ($args['level'] == CLASS_LEVEL) {
                //Thông báo giáo viên về góp ý của phụ huynh
                $teacherIds = $teacherDao->getTeacherIDOfClass($args['class_id']);
                $userDao->postNotifications($teacherIds, NOTIFICATION_NEW_FEEDBACK, NOTIFICATION_NODE_TYPE_CLASS, $feedback_id);
            } else*/
            if ($args['level'] == SCHOOL_LEVEL) {
                //$school = $schoolDao->getSchoolById($args['school_id']);
                $school = getSchoolData($args['school_id'], SCHOOL_INFO);
                //Notify trạng thái cho các quản lý trường
                notifySchoolManagerOfView($_POST['child_id'], NOTIFICATION_NEW_FEEDBACK, $feedback_id, '', convertText4Web($school['page_title']), 'feedback');

                //$userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'feedback', $school['page_admin']);
                //$userDao->postNotifications($userManagerIds, NOTIFICATION_NEW_FEEDBACK, NOTIFICATION_NODE_TYPE_SCHOOL, $feedback_id, '', $school['page_name'], convertText4Web($school['page_title']));
            }
            $db->commit();
            /*return_json(array(
                    'success' => true,
                    'message' => __("Góp ý đã được gửi đi, xin chờ phản hồi từ phía nhà trường."))
            );*/
            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/child/' . $_POST['child_id'] . '/feedback/list";'));
            break;

        case 'edit_info':
            // valid inputs
            if (!isset($_POST['child_info_id']) || !is_numeric($_POST['child_info_id'])) {
                _error(404);
            }
            if (!isset($_POST['child_id']) || !is_numeric($_POST['child_id'])) {
                _error(404);
            }
            //1. Lấy ra thông tin lớp của trẻ
            //$class = $classDao->getClassOfChild($_POST['child_id']);
            $class = getChildData($_POST['child_id'], CHILD_CLASS);

            //$data = $childDao->getChildInformation($_POST['child_id']);
            //$data = getChildData($_POST['child_id'], CHILD_PICKER_INFO);

            // Lấy thông tin chi tiết người đón trẻ
            $data = $childDao->getChildInformationDetail($_POST['child_info_id']);

            $db->begin_transaction();
            $args = array();
            $args['child_id'] = $_POST['child_id'];
            $args['child_info_id'] = $_POST['child_info_id'];
            $args['picker_name'] = $_POST['picker_name'];
            $args['picker_relation'] = $_POST['picker_relation'];
            $args['picker_phone'] = $_POST['phone'];
            $args['picker_address'] = $_POST['address'];
            $args['source_file'] = $data['source_file'];
            $args['file_name'] = convertText4Web($data['picker_file_name']);


            $file_name = "";
            // Upload file đính kèm
            if (file_exists($_FILES['file']['tmp_name'])) {
                // check file upload enabled
                if (!$system['file_enabled']) {
                    modal(MESSAGE, __("Not Allowed"), __("This feature has been disabled"));
                }

                // valid inputs
                if (!isset($_FILES["file"]) || $_FILES["file"]["error"] != UPLOAD_ERR_OK) {
                    modal(MESSAGE, __("Upload Error"), __("Something wrong with upload! Is 'upload_max_filesize' set correctly?"));
                }

                // check file size
                $max_allowed_size = $system['max_file_size'] * 1024;
                if ($_FILES["file"]["size"] > $max_allowed_size) {
                    modal(MESSAGE, __("Upload Error"), __("The file size is so big"));
                }

                // check file extesnion
                $extension = get_extension($_FILES['file']['name']);
                if (!valid_extension($extension, $system['file_medicine_extensions'])) {
                    //modal(MESSAGE, __("Upload Error"), __("The file type is not valid or not supported"));
                    return_json(array('error' => true, 'message' => __("The file type is not valid or not supported")));
                }

                /* check & create uploads dir */
                $depth = '../../../../../';
                $folder = 'informations/' . $class['school_id'];
                if (!file_exists($depth . $system['system_uploads_directory'] . '/' . $folder)) {
                    @mkdir($depth . $system['system_uploads_directory'] . '/' . $folder, 0777, true);
                }
                if (!file_exists($depth . $system['system_uploads_directory'] . '/' . $folder . '/' . date('Y'))) {
                    @mkdir($depth . $system['system_uploads_directory'] . '/' . $folder . '/' . date('Y'), 0777, true);
                }
                if (!file_exists($system['system_uploads_directory'] . '/' . $folder . '/' . date('Y') . '/' . date('m'))) {
                    @mkdir($depth . $system['system_uploads_directory'] . '/' . $folder . '/' . date('Y') . '/' . date('m'), 0777, true);
                }

                /* prepare new file name */
                $directory = $folder . '/' . date('Y') . '/' . date('m') . '/';
                $prefix = $system['uploads_prefix'] . '_' . md5(time() * rand(1, 9999));

                $image = new Image($_FILES["file"]["tmp_name"]);

                $file_name = $directory . $prefix . $image->_img_ext;
                $file_name_tmp = $directory . $prefix . '_tmp' . $image->_img_ext;

                $path = $depth . $system['system_uploads_directory'] . '/' . $file_name;
                $path_tmp = $depth . $system['system_uploads_directory'] . '/' . $file_name_tmp;

                /* check if the file uploaded successfully */
                if (!@move_uploaded_file($_FILES['file']['tmp_name'], $path_tmp)) {
                    modal(MESSAGE, __("Upload Error"), __("Sorry, can not upload the file"));
                }

                /* save the new image */
                $image->save($path, $path_tmp);

                /* delete the tmp image */
                unlink($path_tmp);
                $args['source_file'] = $file_name;
                $args['file_name'] = $_FILES['file']['name'];

            }

            $childDao->updateChildInformation($args);

            //$child = $childDao->getChild($args['child_id']);
//            $child = getChildData($args['child_id'], CHILD_INFO);
//            if(!is_null($child)) {
//                notifySchoolManagerAndTearcherOfView($args['child_id'], NOTIFICATION_UPDATE_PICKER, $args['child_id'], '', convertText4Web($child['child_name']), 'children');
//            }
            $db->commit();

            /* ---------- UPDATE - MEMCACHE ---------- */
            //1.Cập nhật thông tin trẻ
            updateChildData($args['child_id'], CHILD_PICKER_INFO);
            /* ---------- END - MEMCACHE ---------- */

            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/child/' . $_POST['child_id'] . '/informations";'));
            break;

        case 'add_info':
            // valid inputs
            if (!isset($_POST['child_id']) || !is_numeric($_POST['child_id'])) {
                _error(404);
            }
            //1. Lấy ra thông tin lớp của trẻ
            //$class = $classDao->getClassOfChild($_POST['child_id']);
            $class = getChildData($_POST['child_id'], CHILD_CLASS);

            //$data = $childDao->getChildInformation($_POST['child_id']);
            $data = getChildData($_POST['child_id'], CHILD_PICKER_INFO);

            $db->begin_transaction();
            $args = array();
            $args['child_id'] = $_POST['child_id'];
            $args['picker_name'] = $_POST['picker_name'];
            $args['picker_relation'] = $_POST['picker_relation'];
            $args['picker_phone'] = $_POST['phone'];
            $args['picker_address'] = $_POST['address'];
            $args['source_file'] = $data['source_file'];
            $args['file_name'] = convertText4Web($data['picker_file_name']);


            $file_name = "";
            // Upload file đính kèm
            if (file_exists($_FILES['file']['tmp_name'])) {
                // check file upload enabled
                if (!$system['file_enabled']) {
                    modal(MESSAGE, __("Not Allowed"), __("This feature has been disabled"));
                }

                // valid inputs
                if (!isset($_FILES["file"]) || $_FILES["file"]["error"] != UPLOAD_ERR_OK) {
                    modal(MESSAGE, __("Upload Error"), __("Something wrong with upload! Is 'upload_max_filesize' set correctly?"));
                }

                // check file size
                $max_allowed_size = $system['max_file_size'] * 1024;
                if ($_FILES["file"]["size"] > $max_allowed_size) {
                    modal(MESSAGE, __("Upload Error"), __("The file size is so big"));
                }

                // check file extesnion
                $extension = get_extension($_FILES['file']['name']);
                if (!valid_extension($extension, $system['file_medicine_extensions'])) {
                    //modal(MESSAGE, __("Upload Error"), __("The file type is not valid or not supported"));
                    return_json(array('error' => true, 'message' => __("The file type is not valid or not supported")));
                }

                /* check & create uploads dir */
                $depth = '../../../../../';
                $folder = 'informations/' . $class['school_id'];
                if (!file_exists($depth . $system['system_uploads_directory'] . '/' . $folder)) {
                    @mkdir($depth . $system['system_uploads_directory'] . '/' . $folder, 0777, true);
                }
                if (!file_exists($depth . $system['system_uploads_directory'] . '/' . $folder . '/' . date('Y'))) {
                    @mkdir($depth . $system['system_uploads_directory'] . '/' . $folder . '/' . date('Y'), 0777, true);
                }
                if (!file_exists($system['system_uploads_directory'] . '/' . $folder . '/' . date('Y') . '/' . date('m'))) {
                    @mkdir($depth . $system['system_uploads_directory'] . '/' . $folder . '/' . date('Y') . '/' . date('m'), 0777, true);
                }

                /* prepare new file name */
                $directory = $folder . '/' . date('Y') . '/' . date('m') . '/';
                $prefix = $system['uploads_prefix'] . '_' . md5(time() * rand(1, 9999));

                $image = new Image($_FILES["file"]["tmp_name"]);

                $file_name = $directory . $prefix . $image->_img_ext;
                $file_name_tmp = $directory . $prefix . '_tmp' . $image->_img_ext;

                $path = $depth . $system['system_uploads_directory'] . '/' . $file_name;
                $path_tmp = $depth . $system['system_uploads_directory'] . '/' . $file_name_tmp;

                /* check if the file uploaded successfully */
                if (!@move_uploaded_file($_FILES['file']['tmp_name'], $path_tmp)) {
                    modal(MESSAGE, __("Upload Error"), __("Sorry, can not upload the file"));
                }

                /* save the new image */
                $image->save($path, $path_tmp);

                /* delete the tmp image */
                unlink($path_tmp);
                $args['source_file'] = $file_name;
                $args['file_name'] = $_FILES['file']['name'];

            }
            // Insert vào bảng ci_child_info
            $childDao->insertChildInfomation($args);

            //$child = $childDao->getChild($args['child_id']);
            //$child = getChildData($args['child_id'], CHILD_INFO);
//            if(!is_null($child)) {
//                notifySchoolManagerAndTearcherOfView($args['child_id'], NOTIFICATION_UPDATE_PICKER, $args['child_id'], '', convertText4Web($child['child_name']), 'children');
//            }
            $db->commit();

            /* ---------- UPDATE - MEMCACHE ---------- */
            //1.Cập nhật thông tin trẻ
            updateChildData($args['child_id'], CHILD_PICKER_INFO);
            /* ---------- END - MEMCACHE ---------- */

            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/child/' . $_POST['child_id'] . '/informations";'));
            break;

        case 'delete_info':
            if (!isset($_POST['child_info_id']) || !is_numeric($_POST['child_info_id'])) {
                _error(404);
            }
            $db->begin_transaction();
            $childDao->deleteChildInformation($_POST['child_info_id']);
            $db->commit();

            /* ---------- UPDATE - MEMCACHE ---------- */
            //1.Cập nhật thông tin trẻ
            updateChildData($_POST['child_id'], CHILD_PICKER_INFO);
            /* ---------- END - MEMCACHE ---------- */

            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/child/' . $_POST['child_id'] . '/informations";'));
            break;

        case 'register_pickup':
            // Lấy ra thông tin lớp của trẻ
            //$class = $classDao->getClassOfChild($_POST['child_id']);
            $class = getChildData($_POST['child_id'], CHILD_CLASS);
            if (is_null($class)) {
                _error("Error", "The class does not exist, it should be deleted.");
            }
            $args = array();
            $args['child_id'] = $_POST['child_id'];
            $args['class_id'] = $class['group_id'];
            $args['school_id'] = $class['school_id'];
            $args['registered_date'] = date('Y-m-d');
            $args['service_ids'] = isset($_POST['serviceIds']) ? $_POST['serviceIds'] : array();
            //$args['description'] = $_POST['description'];
            $args['type'] = 2;  // Phụ huynh đăng ký type = 2

            $template = $pickupDao->getTemplate($args['school_id']);
            if (is_null($template)) {
                throw new Exception(__("The school has not establish the late pickup configuration"));
            }
            if (strtotime(date('H:i:s')) > strtotime($template['begin_pickup_time'])) {
                throw new Exception(__("Đã quá thời gian đăng ký, vui lòng liên hệ quản lý trường"));
            }

            $oldServiceIds = array();
            // Kiểm tra xem đã có bản ghi đăng ký chưa
            $registered = $pickupDao->getChildRegisteredByParent($args['child_id'], $args['school_id'], $args['registered_date']);
            if (is_null($registered)) {
                $args['pickup_register_id'] = $pickupDao->insertPickupRegister($args);
            } else {
                $args['pickup_register_id'] = $registered['pickup_register_id'];
                $pickupDao->updatePickupRegister($args);
            }

            /*//Thông báo cho giáo viên lớp
            //$teachers = $teacherDao->getTeacherIdClassBySchoolAndChild($args['school_id'], $args['child_id']);
            $teachers = getClassData($class['group_id'], CLASS_TEACHERS);
            //$childName = $childDao->getChildName($args['child_id']);
            $child = getChildData($args['child_id'], CHILD_INFO);
            $childName = $child['child_name'];
            foreach ($teachers as $teacher) {
                $userDao->postNotifications($teacher['teacher_id'], NOTIFICATION_REGISTER_LATEPICKUP, NOTIFICATION_NODE_TYPE_CLASS,
                    $args['pickup_register_id'], toSysDate($args['registered_date']), $teacher['class_name'], convertText4Web($childName));
            }*/

            $db->commit();
            return_json(array('success' => true, 'message' => __("Đăng ký đón muộn thành công")));
            break;

        case 'leave_school':
            /* check password */
            if(!isset($_POST['user_password'])) {
                _api_error(404);
            }

            $checkPass = $userDao->comparePassword($user->_data['user_id'], $_POST['user_password']);
            if(!$checkPass) {
                throw new Exception(__("The password you entered is incorrect"));
            } else {

                $db->begin_transaction();

                $end_at = date('d/m/Y');
                //1. Cập nhật thông tin trẻ trong lớp (chuyển trạng thái Inactive - bảng ci_class_child và ci_school_child)
                $childDao->leaveChildInSchool($child['child_id'], $child['class_id'], $end_at);

                //2. Xóa cha mẹ khỏi lớp (nhưng vẫn giữ like page của trường)
                //$parentIds = $parentDao->getParentIds($_POST['child_id']);
                $parents = getChildData($child['child_id'], CHILD_PARENTS);
                $parentIds = array_keys($parents);
                if (count($parentIds) > 0) {
                    if ($child['class_id'] > 0) {
                        $classDao->deleteUserFromClass($child['class_id'], $parentIds);
                    }
                }
                //2.1. Xóa thông tin trẻ trong ci_parent_manage
                $childDao->deleteParentManageInfo($child['child_id'], $child['child_parent_id']);

                //3. Giảm số lượng trẻ của trường (giảm giới tính trẻ)
                $schoolDao->updateGenderCount($child['school_id'], $child['gender'], -1);

                $school = getSchoolData($child['school_id'], SCHOOL_DATA);
                if ($child['gender'] == MALE) {
                    $school['male_count'] = $school['male_count'] - 1;
                } else {
                    $school['female_count'] = $school['female_count'] - 1;
                }

                //4.Hủy các dịch vụ trẻ đã đăng ký
                $serviceDao->updateServiceForChildLeave($child['child_id'], $end_at);

                //5. Cập nhật quyết toán học phí.
                //$attendanceCount = $_POST['attendance_count'];
                //$prePaidTuition = $_POST['pre_paid_tuition'];
                //$totalAmount = $_POST['total_amount'];
                //$tuition4LDetails = getTuition4LeaveData();
                //$tuitionDao->insertTuition4Leave($school['page_id'], $_POST['child_id'], $totalAmount, $attendanceCount, $prePaidTuition, $tuition4LDetails);

                //6.Thông báo cho quản lý trường phụ huynh đã cho trẻ nghỉ học
                $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'children', $school['page_admin']);

                if(count($userManagerIds) > 0) {
                    $userDao->postNotifications($userManagerIds, NOTIFICATION_LEAVE_SCHOOL_BY_PARENT, NOTIFICATION_NODE_TYPE_SCHOOL,
                        '', convertText4Web($school['page_title']), $school['page_name'], $child['child_name'], $user->_data['user_id']);
                }

                /* ---------- UPDATE - MEMCACHE ---------- */
                //1.Cập nhật thông tin trường
                $config_update = array(
                    'male_count' => $school['male_count'],
                    'female_count' => $school['female_count']
                );
                updateSchoolData($school['page_id'], SCHOOL_CONFIG, $config_update);
                deleteSchoolData($school['page_id'], SCHOOL_CHILDREN, $child['child_id']);

                //2. Cập nhật thông tin lớp cũ
                updateClassData($child['class_id'], CLASS_INFO);
                deleteClassData($child['class_id'], CLASS_CHILDREN, $child['child_id']);

                //3.Xóa thông tin trẻ
                deleteChildData($child['child_id']);
                /* ---------- END - MEMCACHE ---------- */

                $db->commit();
            }
            return_json(array('callback' => 'window.location = "'. $system['system_url'] . '";'));
            break;

        default:
            _error(400);
            break;
    }

    // return & exit
    return_json($return);

} catch (Exception $e) {
    $db->rollback();
	return_json( array('error' => true, 'message' => $e->getMessage()) );
} finally {
    $db->autocommit(true);
}
?>