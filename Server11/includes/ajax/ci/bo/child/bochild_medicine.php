<?php
/**
 * Package: ajax/ci/bo
 * 
 * @package ConIu
 * @author QuanND
 */

// fetch bootstrap
require('../../../../../bootstrap.php');

// fetch image class
require(ABSPATH . 'includes/class-image.php');

// check AJAX Request
is_ajax();
check_login();

include_once(DAO_PATH . 'dao_school.php');
include_once(DAO_PATH . 'dao_medicine.php');
include_once(DAO_PATH . 'dao_parent.php');
include_once(DAO_PATH . 'dao_teacher.php');
include_once(DAO_PATH . 'dao_user.php');
include_once(DAO_PATH . 'dao_class.php');
include_once(DAO_PATH . 'dao_child.php');
$schoolDao = new SchoolDAO();
$medicineDao = new MedicineDAO();
$parentDao = new ParentDAO();
$teacherDao = new TeacherDAO();
$userDao = new UserDAO();
$classDao = new ClassDAO();
$childDao = new ChildDAO();

//Lấy ra thông tin lớp
$child = getChildDataById($_POST['child_id'], CHILD_INFO);
if (is_null($child)) {
    _error(403);
}
$class = getChildData($_POST['child_id'], CHILD_CLASS);

try {
    $return = array();
    $db->autocommit(false);
    switch ($_POST['do']) {
        case 'edit':
            // valid inputs
            if(!isset($_POST['medicine_id']) || !is_numeric($_POST['medicine_id'])) {
                _error(400);
            }
            $data = $medicineDao->getMedicine($_POST['medicine_id'], false);
            if (is_null($data)) {
                _error(404);
            }
//            $begin = strtotime(toDBDate($_POST['begin']));
//            $date_now = strtotime(date('Y-m-d'));
//            if ($begin < $date_now) {
//                throw new Exception(__("You can not sent medicine in the past"));
//            }
            $db->begin_transaction();

            $args = array();
            $args['medicine_id'] = $_POST['medicine_id'];
            $args['child_id'] = $_POST['child_id'];
            $args['medicine_list'] = $_POST['medicine_list'];
            $args['begin'] = $_POST['begin'];
            $args['end'] = $_POST['end'];
            $args['guide'] = $_POST['guide'];
            $args['time_per_day'] = $_POST['time_per_day'];
            $args['status'] = MEDICINE_STATUS_NEW;
            $args['source_file'] = $data['source_file_path'];
            $args['file_name'] = convertText4Web($data['file_name']);

            $args['source_file'] = $data['source_file_path'];
            $args['file_name'] = convertText4Web($data['file_name']);
            if(!$_POST['is_file']) {
                $args['source_file'] = "";
                $args['file_name'] = "";
            }

            $file_name = "";
            // Upload file đính kèm
            if(file_exists($_FILES['file']['tmp_name'])) {
                // check file upload enabled
                if(!$system['file_enabled']) {
                    throw new Exception(__("This feature has been disabled"));
                }

                // valid inputs
                if(!isset($_FILES["file"]) || $_FILES["file"]["error"] != UPLOAD_ERR_OK) {
                    throw new Exception(__("Something wrong with upload! Is 'upload_max_filesize' set correctly?"));
                }

                // check file size
                $max_allowed_size = $system['max_file_size'] * 1024;
                if($_FILES["file"]["size"] > $max_allowed_size) {
                    throw new Exception(__("The file size is so big"));
                }

                // check file extesnion
                $extension = get_extension($_FILES['file']['name']);
                if(!valid_extension($extension, $system['file_medicine_extensions'])) {
                    //modal(MESSAGE, __("Upload Error"), __("The file type is not valid or not supported"));
                    return_json( array('error' => true, 'message' => __("The file type is not valid or not supported")) );
                }

                /* check & create uploads dir */
                $depth = '../../../../../';
                $folder = 'medicines/'. $class['school_id'];
                if(!file_exists($depth.$system['system_uploads_directory'].'/'.$folder)) {
                    @mkdir($depth.$system['system_uploads_directory'].'/'.$folder, 0777, true);
                }
                if(!file_exists($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y'))) {
                    @mkdir($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y'), 0777, true);
                }
                if(!file_exists($system['system_uploads_directory'].'/'.$folder.'/' . date('Y') . '/' . date('m'))) {
                    @mkdir($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y') . '/' . date('m'), 0777, true);
                }

                /* prepare new file name */
                $directory = $folder.'/'. date('Y') . '/' . date('m') . '/';
                $prefix = $system['uploads_prefix'].'_'.md5(time()*rand(1, 9999));

                $image = new Image($_FILES["file"]["tmp_name"]);
                $file_name = $directory.$prefix.$image->_img_ext;
                $file_name_tmp = $directory.$prefix.'_tmp'.$image->_img_ext;

                $path = $depth.$system['system_uploads_directory'].'/'.$file_name;
                $path_tmp = $depth.$system['system_uploads_directory'].'/'.$file_name_tmp;

                /* check if the file uploaded successfully */
                if(!@move_uploaded_file($_FILES['file']['tmp_name'], $path_tmp)) {
                    throw new Exception(__("Sorry, can not upload the file"));
                }
                /* save the new image */
                $image->save($path, $path_tmp);

                /* delete the tmp image */
                unlink($path_tmp);
                $args['source_file'] = $file_name;
                $args['file_name'] = $_FILES['file']['name'];

            }
            //Cập nhật thông tin vào hệ thống
            $medicineDao->updateMedicine($args);

            //Thông báo quản lý trường, cô giáo và phụ huynh
            notifySchoolManagerAndTearcherOfView($_POST['child_id'], NOTIFICATION_NEW_MEDICINE,
                $args['medicine_id'], convertText4Web($_POST['medicine_list']), convertText4Web($child['child_name']), 'medicines');

            $db->commit();
            return_json(array('callback' => 'window.location = "'.$system['system_url'].'/child/'.$_POST['child_id'].'/medicines";'));
            break;

        case 'add':
//            $begin = strtotime(toDBDate($_POST['begin']));
//            $date_now = strtotime(date('Y-m-d'));
//            if ($begin < $date_now) {
//                throw new Exception(__("You can not sent medicine in the past"));
//            }
            $db->begin_transaction();

            $args = array();
            $args['child_id'] = $_POST['child_id'];
            $args['medicine_list'] = $_POST['medicine_list'];
            $args['begin'] = $_POST['begin'];
            $args['end'] = $_POST['end'];
            $args['guide'] = $_POST['guide'];
            $args['time_per_day'] = $_POST['time_per_day'];
            $args['created_user_id'] = $user->_data['user_id'];
            $args['status'] = MEDICINE_STATUS_NEW; //Do cha mẹ tạo ra
            //Nhập thông tin vào hệ thống

            $file_name = "";
            // Upload file đính kèm
            if(file_exists($_FILES['file']['tmp_name'])) {
                // check file upload enabled
                if(!$system['file_enabled']) {
                    throw new Exception(__("This feature has been disabled"));
                }

                // valid inputs
                if(!isset($_FILES["file"]) || $_FILES["file"]["error"] != UPLOAD_ERR_OK) {
                    throw new Exception(__("Something wrong with upload! Is 'upload_max_filesize' set correctly?"));
                }

                // check file size
                $max_allowed_size = $system['max_file_size'] * 1024;
                if($_FILES["file"]["size"] > $max_allowed_size) {
                    throw new Exception(__("The file size is so big"));
                }

                // check file extesnion
                $extension = get_extension($_FILES['file']['name']);
                if(!valid_extension($extension, $system['file_medicine_extensions'])) {
                    // modal(MESSAGE, __("Upload Error"), __("The file type is not valid or not supported"));
                    return_json( array('error' => true, 'message' => __("The file type is not valid or not supported")) );
                }

                /* check & create uploads dir */
                $depth = '../../../../../';
                $folder = 'medicines/'. $class['school_id'];
                if(!file_exists($depth.$system['system_uploads_directory'].'/'.$folder)) {
                    @mkdir($depth.$system['system_uploads_directory'].'/'.$folder, 0777, true);
                }
                if(!file_exists($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y'))) {
                    @mkdir($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y'), 0777, true);
                }
                if(!file_exists($system['system_uploads_directory'].'/'.$folder.'/' . date('Y') . '/' . date('m'))) {
                    @mkdir($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y') . '/' . date('m'), 0777, true);
                }

                /* prepare new file name */
                $directory = $folder.'/'. date('Y') . '/' . date('m') . '/';
                $prefix = $system['uploads_prefix'].'_'.md5(time()*rand(1, 9999));

                $image = new Image($_FILES["file"]["tmp_name"]);
                $file_name = $directory.$prefix.$image->_img_ext;
                $file_name_tmp = $directory.$prefix.'_tmp'.$image->_img_ext;

                $path = $depth.$system['system_uploads_directory'].'/'.$file_name;
                $path_tmp = $depth.$system['system_uploads_directory'].'/'.$file_name_tmp;

                /* check if the file uploaded successfully */
                if(!@move_uploaded_file($_FILES['file']['tmp_name'], $path_tmp)) {
                    throw new Exception(__("Sorry, can not upload the file"));
                }
                /* save the new image */
                $image->save($path, $path_tmp);

                /* delete the tmp image */
                unlink($path_tmp);
            }

            $args['source_file'] = $file_name;
            $args['file_name'] = $_FILES['file']['name'];

            $medicineId = $medicineDao->insertMedicine($args);
            // Gửi thông báo cho quẩn lý trường và giáo viên
            notifySchoolManagerAndTearcherOfView($_POST['child_id'], NOTIFICATION_NEW_MEDICINE,
                $medicineId, convertText4Web($_POST['medicine_list']), convertText4Web($child['child_name']), 'medicines');

            // Tăng lượt tương tác - TaiLA
            addInteractive($child['school_id'], 'medicine', 'parent_view', $medicineId, 2);

            $db->commit();
            return_json(array('callback' => 'window.location = "'.$system['system_url'].'/child/'.$_POST['child_id'].'/medicines";'));
            break;

        case 'delete':
            if(!isset($_POST['id']) || !is_numeric($_POST['id'])) {
                _error(400);
            }
            $db->begin_transaction();
            $medicineDao->deleteMedicine($_POST['id']);

            $db->commit();
            return_json(array('callback' => 'window.location = "'.$system['system_url'].'/child/'.$_POST['child_id'].'/medicines";'));
            break;

        case 'cancel':
            $db->begin_transaction();
            //Cập nhật trạng thái của lần gửi thuốc
            $medicineDao->updateMedicineStatus(MEDICINE_STATUS_CANCEL, $_POST['id']);

            // Gửi thông báo cho quẩn lý trường và giáo viên
            notifySchoolManagerAndTearcherOfView($_POST['child_id'], NOTIFICATION_CANCEL_MEDICINE,
                $_POST['id'], convertText4Web($_POST['medicine_list']), '', 'medicines');

            $db->commit();
            return_json(array('callback' => 'window.location = "'.$system['system_url'].'/child/'.$_POST['child_id'].'/medicines";'));
            break;
        case 'detail':
            if(!isset($_POST['id']) || !is_numeric($_POST['id'])) {
                _error(404);
            }

            $details = $medicineDao->getMedicineDetail($_POST['id']);
            $results = "<br/>".__("Medicated").":";
            foreach ($details as $detail) {
                $results = $results.'<br/> - '.$detail['usage_date'].' | '.__("Medication time").' '.$detail['time_on_day'].' | '.$detail['created_at'].' | '.$detail['user_fullname'];
                $results = $results.' <a href="#" class="far fa-comments fa-lg js_chat-start" data-name="'.$detail['user_fullname'].'" data-uid="'.$detail['created_user_id'].'"></a>';
            }
            $return['results'] = $results;

            return_json($return);
            break;

        default:
            _error(400);
            break;
    }

} catch (Exception $e) {
    $db->rollback();
    return_json( array('error' => true, 'message' => $e->getMessage()) );
} finally {
    $db->autocommit(true);
}

?>