<?php
/**
 * Package: ajax/ci/bo
 *
 * @package ConIu
 * @author TaiLA
 */

// fetch bootstrap
require('../../../../../bootstrap.php');
require(ABSPATH.'includes/class-image.php');

// check AJAX Request
is_ajax();
check_login();

try {
    $return = array();
    $return['callback'] = 'window.location.replace(response.path);';

    include_once(DAO_PATH.'dao_child.php');
    include_once(DAO_PATH.'dao_user.php');
    include_once(DAO_PATH.'dao_parent.php');

    $childDao = new ChildDAO();
    $userDao = new UserDAO();
    $parentDao = new ParentDAO();

    $db->autocommit(false);
    switch ($_POST['do']) {
        case 'edit_growth':
            // Thay đổi thông tin chiều cao, cân nặng của trẻ
            if(!isset($_POST['child_growth_id']) || !is_numeric($_POST['child_growth_id'])) {
                _error(404);
            }
            $db->begin_transaction();
            $child_growth = $childDao->getChildGrowthById($_POST['child_growth_id']);
            $args = array();
            $args['recorded_at'] = $_POST['recorded_at'];
            // Lấy thông tin chiều cao cân nặng của trẻ
            $growth = $childDao->getChildGrowth($_POST['child_parent_id']);
            $growth_recorded = array();
            foreach ($growth as $row) {
                $growth_recorded[] = $row['recorded_at'];
            }
            if(in_array($args['recorded_at'], $growth_recorded) && ($args['recorded_at'] != $child_growth['recorded_at'])) {
                throw new Exception(__("Selected date existed"));
            }
            $args['height'] = $_POST['height'];
            $args['weight'] = $_POST['weight'];
            $args['nutriture_status'] = $_POST['nutriture_status'];
            $args['heart'] = $_POST['heart'];
            $args['blood_pressure'] = $_POST['blood_pressure'];
            $args['ear'] = $_POST['ear'];
            $args['eye'] = $_POST['eye'];
            $args['nose'] = $_POST['nose'];
            $args['description'] = $_POST['description'];
            $args['child_growth_id'] = $_POST['child_growth_id'];

            $args['source_file'] = $child_growth['source_file_path'];
            $args['file_name'] = convertText4Web($child_growth['file_name']);

            $file_name = "";
            // Upload file đính kèm
            if (file_exists($_FILES['file']['tmp_name'])) {
                // check file upload enabled
                if (!$system['file_enabled']) {
                    modal(MESSAGE, __("Not Allowed"), __("This feature has been disabled"));
                }

                // valid inputs
                if (!isset($_FILES["file"]) || $_FILES["file"]["error"] != UPLOAD_ERR_OK) {
                    modal(MESSAGE, __("Upload Error"), __("Something wrong with upload! Is 'upload_max_filesize' set correctly?"));
                }

                // check file size
                $max_allowed_size = $system['max_file_size'] * 1024;
                if ($_FILES["file"]["size"] > $max_allowed_size) {
                    modal(MESSAGE, __("Upload Error"), __("The file size is so big"));
                }

                // check file extesnion
                $extension = get_extension($_FILES['file']['name']);
                if (!valid_extension($extension, $system['file_medicine_extensions'])) {
                    //modal(MESSAGE, __("Upload Error"), __("The file type is not valid or not supported"));
                    return_json(array('error' => true, 'message' => __("The file type is not valid or not supported")));
                }

                /* check & create uploads dir */
                $depth = '../../../../../';
                $folder = 'growths';
                if (!file_exists($depth . $system['system_uploads_directory'] . '/' . $folder)) {
                    @mkdir($depth . $system['system_uploads_directory'] . '/' . $folder, 0777, true);
                }
                if (!file_exists($depth . $system['system_uploads_directory'] . '/' . $folder . '/' . date('Y'))) {
                    @mkdir($depth . $system['system_uploads_directory'] . '/' . $folder . '/' . date('Y'), 0777, true);
                }
                if (!file_exists($system['system_uploads_directory'] . '/' . $folder . '/' . date('Y') . '/' . date('m'))) {
                    @mkdir($depth . $system['system_uploads_directory'] . '/' . $folder . '/' . date('Y') . '/' . date('m'), 0777, true);
                }

                /* prepare new file name */
                $directory = $folder . '/' . date('Y') . '/' . date('m') . '/';
                $prefix = $system['uploads_prefix'] . '_' . md5(time() * rand(1, 9999));

                $image = new Image($_FILES["file"]["tmp_name"]);

                $file_name = $directory . $prefix . $image->_img_ext;
                $file_name_tmp = $directory . $prefix . '_tmp' . $image->_img_ext;

                $path = $depth . $system['system_uploads_directory'] . '/' . $file_name;
                $path_tmp = $depth . $system['system_uploads_directory'] . '/' . $file_name_tmp;

                /* check if the file uploaded successfully */
                if (!@move_uploaded_file($_FILES['file']['tmp_name'], $path_tmp)) {
                    modal(MESSAGE, __("Upload Error"), __("Sorry, can not upload the file"));
                }

                /* save the new image */
                $image->save($path, $path_tmp);

                /* delete the tmp image */
                unlink($path_tmp);
                $args['source_file'] = $file_name;
                $args['file_name'] = $_FILES['file']['name'];

            }

            $childDao->updateChildGrowth($args);

            $db->commit();
            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/childinfo/' . $_POST['child_parent_id'] . '/health";'));
            break;
        case 'add_growth':
            // Thêm thông tin chiều cao, cân nặng của trẻ
            $db->begin_transaction();
            $child_growth = $childDao->getChildGrowthById($_POST['child_growth_id']);
            $args = array();
            $args['recorded_at'] = $_POST['recorded_at'];
            // Lấy thông tin chiều cao cân nặng của trẻ
            $growth = $childDao->getChildGrowth($_POST['child_parent_id']);
            $growth_recorded = array();
            foreach ($growth as $row) {
                $growth_recorded[] = $row['recorded_at'];
            }
            if(in_array($args['recorded_at'], $growth_recorded) && ($args['recorded_at'] != $child_growth['recorded_at'])) {
                throw new Exception(__("Selected date existed"));
            }
            $args['height'] = $_POST['height'];
            $args['weight'] = $_POST['weight'];
            $args['nutriture_status'] = $_POST['nutriture_status'];
            $args['heart'] = $_POST['heart'];
            $args['blood_pressure'] = $_POST['blood_pressure'];
            $args['ear'] = $_POST['ear'];
            $args['eye'] = $_POST['eye'];
            $args['nose'] = $_POST['nose'];
            $args['description'] = $_POST['description'];
            $args['child_parent_id'] = $_POST['child_parent_id'];
            $args['is_parent'] = 1;

            $args['source_file'] = $child_growth['source_file_path'];
            $args['file_name'] = convertText4Web($child_growth['file_name']);

            $file_name = "";
            // Upload file đính kèm
            if (file_exists($_FILES['file']['tmp_name'])) {
                // check file upload enabled
                if (!$system['file_enabled']) {
                    modal(MESSAGE, __("Not Allowed"), __("This feature has been disabled"));
                }

                // valid inputs
                if (!isset($_FILES["file"]) || $_FILES["file"]["error"] != UPLOAD_ERR_OK) {
                    modal(MESSAGE, __("Upload Error"), __("Something wrong with upload! Is 'upload_max_filesize' set correctly?"));
                }

                // check file size
                $max_allowed_size = $system['max_file_size'] * 1024;
                if ($_FILES["file"]["size"] > $max_allowed_size) {
                    modal(MESSAGE, __("Upload Error"), __("The file size is so big"));
                }

                // check file extesnion
                $extension = get_extension($_FILES['file']['name']);
                if (!valid_extension($extension, $system['file_medicine_extensions'])) {
                    //modal(MESSAGE, __("Upload Error"), __("The file type is not valid or not supported"));
                    return_json(array('error' => true, 'message' => __("The file type is not valid or not supported")));
                }

                /* check & create uploads dir */
                $depth = '../../../../../';
                $folder = 'growths';
                if (!file_exists($depth . $system['system_uploads_directory'] . '/' . $folder)) {
                    @mkdir($depth . $system['system_uploads_directory'] . '/' . $folder, 0777, true);
                }
                if (!file_exists($depth . $system['system_uploads_directory'] . '/' . $folder . '/' . date('Y'))) {
                    @mkdir($depth . $system['system_uploads_directory'] . '/' . $folder . '/' . date('Y'), 0777, true);
                }
                if (!file_exists($system['system_uploads_directory'] . '/' . $folder . '/' . date('Y') . '/' . date('m'))) {
                    @mkdir($depth . $system['system_uploads_directory'] . '/' . $folder . '/' . date('Y') . '/' . date('m'), 0777, true);
                }

                /* prepare new file name */
                $directory = $folder . '/' . date('Y') . '/' . date('m') . '/';
                $prefix = $system['uploads_prefix'] . '_' . md5(time() * rand(1, 9999));

                $image = new Image($_FILES["file"]["tmp_name"]);

                $file_name = $directory . $prefix . $image->_img_ext;
                $file_name_tmp = $directory . $prefix . '_tmp' . $image->_img_ext;

                $path = $depth . $system['system_uploads_directory'] . '/' . $file_name;
                $path_tmp = $depth . $system['system_uploads_directory'] . '/' . $file_name_tmp;

                /* check if the file uploaded successfully */
                if (!@move_uploaded_file($_FILES['file']['tmp_name'], $path_tmp)) {
                    modal(MESSAGE, __("Upload Error"), __("Sorry, can not upload the file"));
                }

                /* save the new image */
                $image->save($path, $path_tmp);

                /* delete the tmp image */
                unlink($path_tmp);
                $args['source_file'] = $file_name;
                $args['file_name'] = $_FILES['file']['name'];
            }

            $growthId = $childDao->insertChildGrowth($args);
            // Lấy thông tin trẻ
            $child = $childDao->getChildByParent($_POST['child_parent_id']);
            // Tăng lượt tương tác - TaiLA
            addInteractive($child['school_id'], 'health', 'parent_view', $growthId, 2);
            $db->commit();
            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/childinfo/' . $_POST['child_parent_id'] . '/health";'));
            break;
        case 'add_foetus':
            // Thêm thông tin sức khỏe thai nhi
            if(!isset($_POST['child_parent_id']) || !is_numeric($_POST['child_parent_id'])) {
                _error(404);
            }
            $db->begin_transaction();
            $args = array();

            $args['child_parent_id'] = $_POST['child_parent_id'];
            $args['recorded_at'] = $_POST['recorded_at'];
            $args['pregnant_week'] = $_POST['pregnant_week'];
            $args['due_date_of_childbearing'] = $_POST['due_date_of_childbearing'];
            $args['femus_length'] = $_POST['femus_length'];
            $args['from_head_to_hips_length'] = $_POST['from_head_to_hips_length'];
            $args['weight'] = $_POST['weight'];
//            $args['biparietal_diameter'] = $_POST['biparietal_diameter'];
//            $args['transverse_abdominal_diameter'] = $_POST['transverse_abdominal_diameter'];
//            $args['nuchal_translucency'] = $_POST['nuchal_translucency'];
            $args['fetal_heart'] = $_POST['fetal_heart'];
//            $args['placenta'] = $_POST['placenta'];
//            $args['umbilical_cord'] = $_POST['umbilical_cord'];
//            $args['amniotic_fluid'] = $_POST['amniotic_fluid'];
            $args['hospital'] = $_POST['hospital'];
            $args['hospital_address'] = $_POST['hospital_address'];
            $args['doctor_name'] = $_POST['doctor_name'];
            $args['doctor_phone'] = $_POST['doctor_phone'];
            $args['description'] = $_POST['description'];

            // Thêm thông tin trẻ vào bảng ci_child_foetus_growth
            $child_foetus_growth_id = $childDao->createChildFoetusGrowth($args);

            // Lấy thông tin trẻ
            $child = $childDao->getChildByParent($_POST['child_parent_id']);
            // Tăng lượt tương tác - TaiLA
            addInteractive($child['school_id'], 'health', 'parent_view', $child_foetus_growth_id, 2);

            $db->commit();
            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/childinfo/' . $_POST['child_parent_id'] . '/health";'));
            break;
        case 'edit_foetus':
            // Thay đổi thông tin sức khỏe thai nhi
            if(!isset($_POST['child_foetus_growth_id']) || !is_numeric($_POST['child_foetus_growth_id'])) {
                _error(404);
            }
            $db->begin_transaction();
            $args = array();

            $args['child_foetus_growth_id'] = $_POST['child_foetus_growth_id'];
            $args['recorded_at'] = $_POST['recorded_at'];
            $args['pregnant_week'] = $_POST['pregnant_week'];
            $args['due_date_of_childbearing'] = $_POST['due_date_of_childbearing'];
            $args['femus_length'] = $_POST['femus_length'];
            $args['from_head_to_hips_length'] = $_POST['from_head_to_hips_length'];
            $args['weight'] = $_POST['weight'];
//            $args['biparietal_diameter'] = $_POST['biparietal_diameter'];
//            $args['transverse_abdominal_diameter'] = $_POST['transverse_abdominal_diameter'];
//            $args['nuchal_translucency'] = $_POST['nuchal_translucency'];
            $args['fetal_heart'] = $_POST['fetal_heart'];
//            $args['placenta'] = $_POST['placenta'];
//            $args['umbilical_cord'] = $_POST['umbilical_cord'];
//            $args['amniotic_fluid'] = $_POST['amniotic_fluid'];
            $args['hospital'] = $_POST['hospital'];
            $args['hospital_address'] = $_POST['hospital_address'];
            $args['doctor_name'] = $_POST['doctor_name'];
            $args['doctor_phone'] = $_POST['doctor_phone'];
            $args['description'] = $_POST['description'];

            // Thêm thông tin trẻ vào bảng ci_child_foetus_growth
            $childDao->updateChildFoetusGrowth($args);

            $db->commit();
            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/childinfo/' . $_POST['child_parent_id'] . '/health";'));
            break;
        case 'delete_growth':
            if(!isset($_POST['child_parent_id']) || !is_numeric($_POST['child_parent_id'])) {
                _error(404);
            }
            if(!isset($_POST['id']) || !is_numeric($_POST['id'])) {
                _error(404);
            }

            $db->begin_transaction();

            $childDao->deleteChildGrowth($_POST['child_parent_id'], $_POST['id']);

            $db->commit();
            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/childinfo/' . $_POST['child_parent_id'] . '/health";'));
            break;
        case 'delete_foetus':
            if(!isset($_POST['child_parent_id']) || !is_numeric($_POST['child_parent_id'])) {
                _error(404);
            }
            if(!isset($_POST['id']) || !is_numeric($_POST['id'])) {
                _error(404);
            }

            $db->begin_transaction();

            $childDao->deleteChildFoetusGrowth($_POST['child_parent_id'], $_POST['id']);

            $db->commit();
            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/childinfo/' . $_POST['child_parent_id'] . '/health";'));
            break;
        case 'search_chart':
            $db->begin_transaction();
            if($_POST['begin'] == '' || !validateDate($_POST['begin']) ) {
                $begin = null;
            } else {
                $begin = $_POST['begin'];
            }
            if($_POST['end'] == '' || !validateDate($_POST['end'])) {
                $end = null;
            } else {
                $end = $_POST['end'];
            }

            // Lấy ngày hiện tại
            $dateNow = date('Y-m-d');

            // Lấy thông tin trẻ
            $child = $childDao->getChildByParent($_POST['child_parent_id']);

            // Lấy ngày sinh nhật của trẻ
            $birthday = toDBDate($child['birthday']);

            // Khởi tạo mảng lưu chỉ số sức khỏe của trẻ
            $childGrowth = array();

            if(is_null($begin) || is_null($end)) {
                // Lấy toàn bộ thông tin sức khỏe của trẻ
                $childGrowth = $childDao->getChildGrowthForChart($child['child_parent_id']);

                // Tính xem trẻ được bao nhiêu ngày tuổi
                $day_age_now = (strtotime($dateNow) - strtotime($birthday)) / (60*60*24);

                // đoạn này xử lý để lấy ngày bắt đầu (đặt tạm a, b, c; bí tên quá)
                $a = $day_age_now % 30;
                $b = $day_age_now - $a;
                $c = $b - 30*10;
                $dayGet = (int)$c;
                $day_age_max = $day_age_now + 65;
                if($day_age_now >= 300) {
                    $day_age_min = $dayGet;
                } else {
                    $day_age_min = 0;
                }

                // Dữ liệu chuẩn người dùng nhập vào vẽ biểu đồ chiều cao (thư viện charts.js)
                $heightForChartChartJS = getDataJsonForChartChartJS($childGrowth, $day_age_min, $day_age_max, $birthday, 'height');

                // Lấy dữ liệu người dùng nhập vào vẽ biểu đồ cân nặng (charts.js)
                $weightForChartChartJS = getDataJsonForChartChartJS($childGrowth, $day_age_min, $day_age_max, $birthday, 'weight');

                if($child['gender'] == 'male') {
                    // Dữ liệu chuẩn
                    $hightStandardArray = getHightDataForChart($hightStandardJsonMale);
                    $weightStandardArray = getHightDataForChart($weightStandardJsonMale);
                } else {
                    $hightStandardArray = getHightDataForChart($hightStandardJsonFeMale);
                    $weightStandardArray = getHightDataForChart($weightStandardJsonFeMale);

                }
                // 1. Chiều cao
                // Lấy 2 ngày gần nhất của $day_age_min có dữ liệu, nội suy chuẩn của ngày bắt đầu
                $hightStandardArray[$day_age_min]['highest'] = getStandardDataBeginAndEnd($hightStandardArray, $day_age_min, 'highest');

                $hightStandardArray[$day_age_min]['lowest'] = getStandardDataBeginAndEnd($hightStandardArray, $day_age_min, 'lowest');

                // Lấy 2 ngày gần nhất của $end_day có dữ liệu, nội suy chuẩn của ngày kết thúc
                $hightStandardArray[$day_age_max]['highest'] = getStandardDataBeginAndEnd($hightStandardArray, $day_age_max, 'highest');

                $hightStandardArray[$day_age_max]['lowest'] = getStandardDataBeginAndEnd($hightStandardArray, $day_age_max, 'lowest');

                // dỮ LIỆU đường cao nhất (thư viện charts.js)
                $highestForChartChartsJS = getStandardDataForChartChartJS($hightStandardArray, $day_age_min, $day_age_max, 'highest');

                // Dữ liệu đường thấp nhất (thư viện charts.js)
                $lowestForChartChartsJS = getStandardDataForChartChartJS($hightStandardArray, $day_age_min, $day_age_max, 'lowest');

                // 2. Cân nặng
                // Lấy 2 ngày gần nhất của $begin_day có dữ liệu, nội suy chuẩn của ngày bắt đầu
                $weightStandardArray[$day_age_min]['highest'] = getStandardDataBeginAndEnd($weightStandardArray, $day_age_min, 'highest');

                $weightStandardArray[$day_age_min]['lowest'] = getStandardDataBeginAndEnd($weightStandardArray, $day_age_min, 'lowest');

                // Lấy 2 ngày gần nhất của $end_day có dữ liệu, nội suy chuẩn của ngày kết thúc
                $weightStandardArray[$day_age_max]['highest'] = getStandardDataBeginAndEnd($weightStandardArray, $day_age_max, 'highest');

                $weightStandardArray[$day_age_max]['lowest'] = getStandardDataBeginAndEnd($weightStandardArray, $day_age_max, 'lowest');

                // Dữ liệu đường cao nhất (charts.js)
                $highestWeightForChartChartJS = getStandardDataForChartChartJS($weightStandardArray, $day_age_min, $day_age_max, 'highest');

                // Dữ liệu đường thấp nhất (charts.js)
                $lowestWeightForChartChartJS = getStandardDataForChartChartJS($weightStandardArray, $day_age_min, $day_age_max, 'lowest');

                // 3. BMI (charts.js)
                $bmiForChartChartJS = getDataJsonForChartChartJS($childGrowth, $day_age_min, $day_age_max, $birthday, 'bmi');

                // GEt label
                $label = '';
                for($i = $day_age_min; $i <= $day_age_max; $i++) {
                    if($i%30 == 0) {
                        $month = $i/30;
                        $label .= ' ' .$month. ',';
                    } else {
                        $label .= ' ,';
                    }
                }
                $smarty->assign('label', $label);
            } else {
                // Lấy dữ liệu nhập và của trẻ
                $childGrowth = $childDao->getChildGrowthSearchForChartBeginEnd($child['child_parent_id'], $begin, $end);

                // Lấy ngày bắt đầu và ngày kết thúc
                $begin = toDBDate($begin);
                $end = toDBDate($end);

                $end_day = (strtotime($end) - strtotime($birthday)) / (60*60*24);
                $end_day = $end_day / 30;
                $end_day = (int)$end_day;
                $end_day = ($end_day + 1) * 30;

                $begin_day = (strtotime($begin) - strtotime($birthday)) / (60*60*24);
                $begin_day = $begin_day / 30;
                $begin_day = (int)$begin_day;
                $begin_day = $begin_day * 30;
                if($begin_day < 0) {
                    throw new Exception(__("You must select day begin after birthday"));
                }

                // Dữ liệu chuẩn người dùng nhập vào vẽ biểu đồ chiều cao (thư viện charts.js)
                $heightForChartChartJS = getDataJsonForChartChartJS($childGrowth, $begin_day, $end_day, $birthday, 'height');

                // Lấy dữ liệu người dùng nhập vào vẽ biểu đồ cân nặng (charts.js)
                $weightForChartChartJS = getDataJsonForChartChartJS($childGrowth, $begin_day, $end_day, $birthday, 'weight');

                if($child['gender'] == 'male') {
                    // Dữ liệu chuẩn
                    $hightStandardArray = getHightDataForChart($hightStandardJsonMale);
                    $weightStandardArray = getHightDataForChart($weightStandardJsonMale);
                } else {
                    $hightStandardArray = getHightDataForChart($hightStandardJsonFeMale);
                    $weightStandardArray = getHightDataForChart($weightStandardJsonFeMale);
                }

                // 1. Chiều cao
                // Lấy 2 ngày gần nhất của $begin_day có dữ liệu, nội suy chuẩn của ngày bắt đầu
                $hightStandardArray[$begin_day]['highest'] = getStandardDataBeginAndEnd($hightStandardArray, $begin_day, 'highest');

                $hightStandardArray[$begin_day]['lowest'] = getStandardDataBeginAndEnd($hightStandardArray, $begin_day, 'lowest');

                // Lấy 2 ngày gần nhất của $end_day có dữ liệu, nội suy chuẩn của ngày kết thúc
                $hightStandardArray[$end_day]['highest'] = getStandardDataBeginAndEnd($hightStandardArray, $end_day, 'highest');

                $hightStandardArray[$end_day]['lowest'] = getStandardDataBeginAndEnd($hightStandardArray, $end_day, 'lowest');

                // dỮ LIỆU đường cao nhất (thư viện charts.js)
                $highestForChartChartsJS = getStandardDataForChartChartJS($hightStandardArray, $begin_day, $end_day, 'highest');

                // Dữ liệu đường thấp nhất (thư viện charts.js)
                $lowestForChartChartsJS = getStandardDataForChartChartJS($hightStandardArray, $begin_day, $end_day, 'lowest');

                // 2. Cân nặng
                // Lấy 2 ngày gần nhất của $begin_day có dữ liệu, nội suy chuẩn của ngày bắt đầu
                $weightStandardArray[$begin_day]['highest'] = getStandardDataBeginAndEnd($weightStandardArray, $begin_day, 'highest');

                $weightStandardArray[$begin_day]['lowest'] = getStandardDataBeginAndEnd($weightStandardArray, $begin_day, 'lowest');

                // Lấy 2 ngày gần nhất của $end_day có dữ liệu, nội suy chuẩn của ngày kết thúc
                $weightStandardArray[$end_day]['highest'] = getStandardDataBeginAndEnd($weightStandardArray, $end_day, 'highest');

                $weightStandardArray[$end_day]['lowest'] = getStandardDataBeginAndEnd($weightStandardArray, $end_day, 'lowest');

                // Dữ liệu đường cao nhất (charts.js)
                $highestWeightForChartChartJS = getStandardDataForChartChartJS($weightStandardArray, $begin_day, $end_day, 'highest');

                // Dữ liệu đường thấp nhất (charts.js)
                $lowestWeightForChartChartJS = getStandardDataForChartChartJS($weightStandardArray, $begin_day, $end_day, 'lowest');

                // 3. BMI (charts.js)
                $bmiForChartChartJS = getDataJsonForChartChartJS($childGrowth, $begin_day, $end_day, $birthday, 'bmi');

                $label = '';
                for($i = $begin_day; $i <= $end_day; $i++) {
                    if($i%30 == 0) {
                        $month = $i/30;
                        $label .= ' ' .$month. ',';
                    } else {
                        $label .= ' ,';
                    }
                }
                $smarty->assign('label', $label);
            }
            // Chiều cao for charts.js
            $smarty->assign('highestForChartChartJS', $highestForChartChartsJS);
            $smarty->assign('lowestForChartChartJS', $lowestForChartChartsJS);
            $smarty->assign('heightForChartChartJS', $heightForChartChartJS);

            //Cân nặng cho charts.js
            $smarty->assign('highestWeightForChartChartJS', $highestWeightForChartChartJS);
            $smarty->assign('lowestWeightForChartChartJS', $lowestWeightForChartChartJS);
            $smarty->assign('weightForChartChartJS', $weightForChartChartJS);

            // BMI (charts.js)
            $smarty->assign('bmiForChartChartJS', $bmiForChartChartJS);


            $smarty->assign('child', $child);
            $smarty->assign('growth', array_reverse($childGrowth));

            $return['results'] = $smarty->fetch("ci/childinfo/ajax.child.chartsearch.tpl");

            return_json($return);
            break;
        default:
            _error(400);
            break;
    }
}
catch (Exception $e) {
    $db->rollback();
    return_json( array('error' => true, 'message' => $e->getMessage()) );
} finally {
    $db->autocommit(true);
}
?>