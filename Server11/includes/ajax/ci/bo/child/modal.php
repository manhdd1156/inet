<?php
/**
 * ajax -> albums -> modal
 *
 * @package CINet v1.1
 * @author NOGA Co., Ltd
 */

// fetch bootstrap
require('../../../../../bootstrap.php');
include_once(DAO_PATH.'dao_journal.php');
$journalDao = new JournalDAO();
// check AJAX Request
is_ajax();

// check user logged in
if(!$user->_logged_in) {
    modal(LOGIN);
}

// check user activated
if($system['activation_enabled'] && !$user->_data['user_activated']) {
    modal(MESSAGE, __("Not Activated"), __("Before you can interact with other users, you need to confirm your email address"));
}

// valid inputs
if(!isset($_GET['id']) || !is_numeric($_GET['id'])) {
    _error(400);
}

try {

    // initialize the return array
    $return = array();

    // get journal
    $journal = $journalDao->getJournalById($_GET['id']);
    /* assign variables */
    $smarty->assign('journal', $journal);

    switch ($_GET['do']) {
        case 'add_photos':
            /* return */
            $return['add_photos'] = $smarty->fetch("ajax.journal.album.add_photos.tpl");
            $return['callback'] = "$('#modal').modal('show'); $('.modal-content:last').html(response.add_photos);";
            break;
        default:
            _error(400);
            break;
    }

    // return & exit
    return_json($return);

} catch (Exception $e) {
    modal(ERROR, __("Error"), $e->getMessage());
}


?>