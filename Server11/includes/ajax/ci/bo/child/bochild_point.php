<?php
/**
 * Package: ajax/ci/bo
 *
 * @package ConIu
 * @author QuanND
 */

// fetch bootstrap
require('../../../../../bootstrap.php');

// fetch image class
require(ABSPATH . 'includes/class-image.php');

// check AJAX Request
is_ajax();
check_login();

include_once(DAO_PATH . 'dao_school.php');
include_once(DAO_PATH . 'dao_medicine.php');
include_once(DAO_PATH . 'dao_subject.php');
include_once(DAO_PATH . 'dao_point.php');
include_once(DAO_PATH . 'dao_parent.php');
include_once(DAO_PATH . 'dao_teacher.php');
include_once(DAO_PATH . 'dao_user.php');
include_once(DAO_PATH . 'dao_class.php');
include_once(DAO_PATH . 'dao_conduct.php');
include_once(DAO_PATH . 'dao_child.php');
$schoolDao = new SchoolDAO();
$medicineDao = new MedicineDAO();
$subjectDao = new SubjectDAO();
$pointDao = new PointDAO();
$parentDao = new ParentDAO();
$teacherDao = new TeacherDAO();
$userDao = new UserDAO();
$classDao = new ClassDAO();
$conductDao = new ConductDAO();
$childDao = new ChildDAO();

//Lấy ra thông tin lớp
$child = getChildDataById($_POST['child_id'], CHILD_INFO);
// nếu là học sinh
if (!$child) {
    $child = getChildDataById($_POST['child_id'], CHILD_INFO, "itself");
}
if (is_null($child)) {
    _error(403);
}
$class = getChildData($_POST['child_id'], CHILD_CLASS);

try {
    $return = array();
    $db->autocommit(false);
    switch ($_POST['do']) {
        case 'list':

            //Lấy ra class_id của lớp
            $class_id = $class['group_id'];
            $school_year = $_POST['school_year'];

            // Lấy school_config
            $studentId = $_POST['child_id'];
            $schoolConfig = getSchoolData($_POST['school_id'], SCHOOL_CONFIG);
            // Lấy chi tiết gov_class_level
            $class_level = getClassLevelData($class['class_level_id'], CLASS_LEVEL_INFO);
            // Lấy danh sách điểm của trẻ trong năm học
            $children_point = $pointDao->getChildrenPointsByStudentId($class_id, $school_year, $studentId);
            // Lấy child_name
            if (count($children_point) > 0) {
// flag đánh dấu đã đủ điểm các môn hay chưa
                $flag_enough_point = true;
                // trạng thái fail/pass hay re-exam hay chưa đủ điểm ( Waiting.. )
                $status_result = 'fail';
                $subject_key = array();
                $children_point_last = array();
                // lưu số cột mỗi đầu điểm
                $countColumnMaxPoint_hk1 = 2;
                $countColumnMaxPoint_hk2 = 2;
                $countColumnMaxPoint_gk1 = 1;
                $countColumnMaxPoint_gk2 = 1;

                $child_id = $childDao->getChildIdByCode($children_point[0]['child_code']);
                // Lấy số buổi nghỉ có phép và không phép của học sinh
                $child_absent = $childDao->getAbsentsChild($child_id, $class_id);

                if ($schoolConfig['score_fomula'] == "km") {
                    // Mặc định cột điểm của khmer là 3
                    $countColumnMaxPoint_hk1 = 3;
                    $countColumnMaxPoint_hk2 = 3;
                    // Lưu các điểm đã được tính toán
                    $children_point_avgs = array();
                    // Lưu điểm của các môn thi lại
                    $children_subject_reexams = array();
                    // flag đánh dấu đã đủ điểm các môn hay chưa
                    $child_enough_point = true;
                    // trạng thái fail/pass hay re-exam hay chưa đủ điểm ( Waiting.. )
                    $status_result = '';
                    // điểm các tháng của 2 kỳ
                    $child_m11_point = $child_m12_point = $child_m13_point = $child_m21_point = $child_m22_point = $child_m23_point = 0;
                    // điểm trung bình các tháng của 2 kỳ
                    $child_avM1_point = $child_avM2_point = 0;
                    // điểm trung bình của cuối kỳ
                    $child_avd1_point = 0;
                    $child_avd2_point = 0;
                    $child_final_semester1 = $child_final_semester2 = 0;
                    // điểm trung bình năm
                    $child_avYear_point = 0;


                    foreach ($children_point as $child) {
                        // Nếu bất kì điểm nào thiếu sẽ đánh dấu là chưa đủ điểm
//                    if (!isset($child['m11']) || !isset($child['m12']) || !isset($child['m13']) || !isset($child['d1'])
//                        || !isset($child['m21']) || !isset($child['m22']) || !isset($child['m23']) || !isset($child['d2'])) {
//                        $child_enough_point = false;
//                    }
                        if (!isset($each['hs11']) || !isset($each['hs12']) || !isset($each['hs13']) || !isset($each['gk11'])
                            || !isset($each['hs21']) || !isset($each['hs22']) || !isset($each['hs23']) || !isset($each['gk21'])) {
                            $flag_enough_point = false;
                        }
                        // tạm thời tính tổng các đầu điểm
                        // kỳ 1
                        $child_m11_point += (int)$child['hs11'];
                        $child_m12_point += (int)$child['hs12'];
                        $child_m13_point += (int)$child['hs13'];
                        $child_avd1_point += (int)$child['gk11'];
                        // kỳ 2
                        $child_m21_point += (int)$child['hs21'];
                        $child_m22_point += (int)$child['hs22'];
                        $child_m23_point += (int)$child['hs23'];
                        $child_avd2_point += (int)$child['gk21'];

                        // lấy tên môn học
                        $subject_detail = $subjectDao->getSubjectById($child['subject_id']);
                        $child['subject_name'] = $subject_detail['subject_name'];
                        if (isset($subject_detail['re_exam']) && $subject_detail['re_exam']) { // nếu môn đó là môn thi lại thì lưu vào array
                            $subject_reexam['name'] = $subject_detail['subject_name'];
                            $subject_reexam['point'] = $child['re_exam'];
                            $children_subject_reexams[] = $subject_reexam;
                        }
//                        $child['subject_reexam']= $subject_detail['re_exam'];

                    }

                    // định nghĩa số bị chia của từng khối
                    if ($class_level['gov_class_level'] == '7' || $class_level['gov_class_level'] == '8') {
                        $dividend = 15;
                    } else if ($class_level['gov_class_level'] == '9') {
                        $dividend = 11.4;
                    } else if ($class_level['gov_class_level'] == '10') {
                        $dividend = 15.26;
                    } else if ($class_level['gov_class_level'] == '11') {
                        $dividend = 16.5;
                    } else if ($class_level['gov_class_level'] == '12') {
                        $dividend = 14.5;
                    }
                    // tính điểm theo công thức từng khối
                    // kỳ 1
                    $child_m11_point /= $dividend;
                    $child_m12_point /= $dividend;
                    $child_m13_point /= $dividend;
                    $child_avd1_point /= $dividend;
                    $child_avM1_point = ($child_m11_point + $child_m12_point + $child_m13_point) / 3;
                    $child_final_semester1 = ($child_avd1_point + $child_avM1_point) / 2;
                    // kỳ 2
                    $child_m21_point /= $dividend;
                    $child_m22_point /= $dividend;
                    $child_m23_point /= $dividend;
                    $child_avd2_point /= $dividend;
                    $child_avM2_point = ($child_m21_point + $child_m22_point + $child_m23_point) / 3;
                    $child_final_semester2 = ($child_avd2_point + $child_avM2_point) / 2;
                    // cả năm
                    $child_avYear_point = ($child_final_semester1 + $child_final_semester2) / 2;
                    // lưu lại vào array
                    $children_point_avgs['a1'] = $child_m11_point;
                    $children_point_avgs['b1'] = $child_m12_point;
                    $children_point_avgs['c1'] = $child_m13_point;
                    $children_point_avgs['d1'] = $child_avd1_point;
                    $children_point_avgs['x1'] = $child_avM1_point;
                    $children_point_avgs['e1'] = $child_final_semester1;
                    $children_point_avgs['a2'] = $child_m21_point;
                    $children_point_avgs['b2'] = $child_m22_point;
                    $children_point_avgs['c2'] = $child_m23_point;
                    $children_point_avgs['d2'] = $child_avd2_point;
                    $children_point_avgs['x2'] = $child_avM2_point;
                    $children_point_avgs['e2'] = $child_final_semester2;
                    $children_point_avgs['y'] = $child_avYear_point;
                    //xét điều kiện để đánh giá pass hay fail
                    if ($child_avYear_point < 25) { // dưới 25 điểm thì fail chặt
                        $status_result = 'Fail';
                    } else { // nếu trên 25 điểm thì xét các điều kiện khác
                        $status_result = 'Pass';
                        if ((isset($child_absent['absent_true']) && (int)$child_absent['absent_true'] >= 55)
                            || (isset($child_absent['absent_false']) && (int)$child_absent['absent_false'] >= 30)
                            || (((int)$child_absent['absent_true'] + (int)$child_absent['absent_false']) >= 55)) {
                            if ($class_level['gov_class_level'] == '7' || $class_level['gov_class_level'] == '8'
                                || $class_level['gov_class_level'] == '10' || $class_level['gov_class_level'] == '11') {
                                $status_result = 'Re-exam';
                            } else if ($class_level['gov_class_level'] == '9' || $class_level['gov_class_level'] == '12') {
                                $status_result = 'Pass';
                            }
                        }
                    }
                    // xét đến đánh giá điểm thi lại
                    if ($status_result == 'Re-exam') {
                        $result_exam = 0;
                        foreach ($children_subject_reexams as $subject_reexam) {
                            if (!isset($subject_reexam['point'])) {
                                $child_enough_point = false;
                            } else {
                                $result_exam += (int)$subject_reexam['point'];
                            }
                        }
                        if ($class_level['gov_class_level'] == '7' || $class_level['gov_class_level'] == '8'
                            || $class_level['gov_class_level'] == '11') {
                            $result_exam /= 4;
                        } else if ($class_level['gov_class_level'] == '10') {
                            $result_exam /= 4;
                        }
                        // nếu điểm thi lại trên 25 điểm thì pass còn lại fail
                        if ($child_enough_point && $result_exam >= 25) {
                            $status_result = 'Pass';
                        } else if ($child_enough_point && $result_exam < 25) {
                            $status_result = 'Fail';
                        } else { // chuaw du diem
                            $status_result = 'Re-exam';
                        }
                    }
                    if (!$child_enough_point && $status_result != 'Re-exam') {
                        $status_result = __('Waiting..');
                    }
                    $points_key = array();
                    if ($schoolConfig['grade'] == 1) { // chưa làm cho cấp 1
//                if (!$is_last_semester) {
//                    $points_key[] = 'gk' . $semester . 'nx';
//                    $points_key[] = 'gk' . $semester . 'nl';
//                    $points_key[] = 'gk' . $semester . 'pc';
//                    $points_key[] = 'gk' . $semester . 'kt';
//                    $points_key[] = 'gk' . $semester . 'xl';
//                } else {
//                    $points_key[] = 'ck' . $semester . 'nx';
//                    $points_key[] = 'ck' . $semester . 'nl';
//                    $points_key[] = 'ck' . $semester . 'pc';
//                    $points_key[] = 'ck' . $semester . 'kt';
//                    $points_key[] = 'ck' . $semester . 'xl';
//                }
                    } elseif ($schoolConfig['grade'] == 2) {
                        $subject_key[] = 'hs11';
                        $subject_key[] = 'hs12';
                        $subject_key[] = 'hs13';
                        $subject_key[] = 'gk11';
                        $subject_key[] = 'hs21';
                        $subject_key[] = 'hs22';
                        $subject_key[] = 'hs23';
                        $subject_key[] = 'gk21';
                    }
                    $smarty->assign("children_point_avgs", $children_point_avgs);
                    $smarty->assign("children_subject_reexams", $children_subject_reexams);
                    $smarty->assign("result_exam", $result_exam);
                } else if ($schoolConfig['score_fomula'] == "vn") {
                    // điều kiện điểm của môn 1: kém, 2 yếu, 3 trung bình, 4 tiên tiến, 5 giỏi
                    $point_avg1_condition1 = 5;//hk1
                    $point_avg2_condition1 = 5;//hk2
                    $point_avgYearAll_condition1 = 5;// cả năm
                    // điều kiện điểm của môn toán hoặc văn  1: kém, 2 yếu, 3 trung bình, 4 tiên tiến, 5 giỏi
                    $point_avg1_condition2 = 5; //hk1
                    $point_avg2_condition2 = 5; //hk2
                    $point_avgYearAll_condition2 = 5; // cả năm
                    // trạng thái
                    $status_point1 = '';
                    $status_point2 = '';
                    $status_pointAll = '';
                    // flag đánh dấu học sinh đã thi lại hay chưa
                    $flag_reexam = false;
                    // trạng thái lên lớp hay thi lại hay chưa đủ điểm...
                    $status_result = 'fail';

                    // điểm trung bình các môn 2 kỳ
                    $child_TBM1_point = $child_TBM2_point = 0;
                    // điểm trung bình năm
                    $child_avYearAll_point = 0;
                    // TÍnh điểm mỗi môn
                    if (count($children_point) > 0) {
                        foreach ($children_point as $each) {
                            if (!isset($each['hs11']) || !isset($each['hs12']) || !isset($each['gk11'])
                                || !isset($each['hs21']) || !isset($each['hs22']) || !isset($each['gk21'])
                                || !isset($each['ck1']) || !isset($each['ck2'])) {
                                $flag_enough_point = false;
                            }
                            // điểm hệ số 2 kỳ
                            $child_hk1_point = $child_hk2_point = 0;
                            $child_TBM1_current_point = $child_TBM2_current_point = 0;
                            // đếm số điểm đã có
                            $count_hk1_point = 0;
                            $count_hk2_point = 0;
                            //Tính điểm trung bình môn

                            for ($i = 1; $i <= 6; $i++) {
                                // điểm hệ số 1
                                if (isset($each['hs1' . $i])) {
                                    $child_hk1_point += (int)$each['hs1' . $i];
                                    $count_hk1_point++;
                                    // lưu số cột đã có điểm của hệ số 1 kỳ 1
                                    if ($i > $countColumnMaxPoint_hk1) {
                                        $countColumnMaxPoint_hk1 = $i;
                                    }
                                }
                                if (isset($each['hs2' . $i])) {
                                    $child_hk2_point += (int)$each['hs2' . $i];
                                    $count_hk2_point++;
                                    // lưu số cột đã có điểm của hệ số 1 kỳ 2
                                    if ($i > $countColumnMaxPoint_hk2) {
                                        $countColumnMaxPoint_hk2 = $i;
                                    }
                                }
                                // điểm hệ số 2
                                if (isset($each['gk1' . $i])) {
                                    $child_hk1_point += (int)$each['gk1' . $i] * 2;
                                    $count_hk1_point += 2;
                                    // lưu số cột đã có điểm
                                    $countColumnMaxPoint_gk1 = $i;
                                }
                                if (isset($each['gk2' . $i])) {
                                    $child_hk2_point += (int)$each['gk2' . $i] * 2;
                                    $count_hk2_point += 2;
                                    // lưu số cột đã có điểm
                                    $countColumnMaxPoint_gk2 = $i;
                                }
                            }
                            //điểm cuối kì

                            if (isset($each['ck1'])) {
                                $child_hk1_point += (int)$each['ck1'] * 3;
                                $count_hk1_point += 3;
                            }
                            if (isset($each['ck2'])) {
                                // Nếu có điểm thi lại thì điểm tổng kết sẽ tính dựa theo điểm thi lại
                                if(isset($each['re_exam'])) {
                                    $child_hk2_point += (int)$each['re_exam'] * 3;
                                    $flag_reexam = true;
                                }else {
                                    $child_hk2_point += (int)$each['ck2'] * 3;
                                }
                                $count_hk2_point += 3;
                            }
                            if ($count_hk1_point > 0) {
                                $child_TBM1_current_point = $child_hk1_point / $count_hk1_point;
                                $each['tb_hk1'] = number_format($child_TBM1_current_point, 2);
                            }
                            if ($count_hk2_point > 0) {
                                $child_TBM2_current_point = $child_hk2_point / $count_hk2_point;
                                $each['tb_hk2'] = number_format($child_TBM2_current_point, 2);
                            }
                            // Lấy tên môn học
                            $subject_detail = $subjectDao->getSubjectById($each['subject_id']);
                            $each['subject_name'] = $subject_detail['subject_name'];
                            // Nếu có điểm thi lại thì điểm tổng kết sẽ tính dựa theo điểm thi lại
                            if (isset($each['re_exam'])) {
                                $flag_reexam = true;
                            }
                            $child_avYear_point = ($child_TBM1_current_point + $child_TBM2_current_point * 2) / 3;

                            $each['tb_year'] = number_format($child_avYear_point, 2);
                            // xét xem điều kiện các môn ở mức nào
                            //học kỳ 1
                            if ($child_TBM1_current_point < 2 && $point_avg1_condition1 > 1) {
                                $point_avg1_condition1 = 1;
                            } else if ($child_TBM1_current_point < 3.5 && $point_avg1_condition1 > 2) {
                                $point_avg1_condition1 = 2;
                            } else if ($child_TBM1_current_point < 5 && $point_avg1_condition1 > 3) {
                                $point_avg1_condition1 = 3;
                            } else if ($child_TBM1_current_point < 6.5 && $point_avg1_condition1 > 4) {
                                $point_avg1_condition1 = 4;
                            }
                            //học kỳ 2
                            if ($child_TBM2_current_point < 2 && $point_avg2_condition1 > 1) {
                                $point_avg2_condition1 = 1;
                            } else if ($child_TBM2_current_point < 3.5 && $point_avg2_condition1 > 2) {
                                $point_avg2_condition1 = 2;
                            } else if ($child_TBM2_current_point < 5 && $point_avg2_condition1 > 3) {
                                $point_avg2_condition1 = 3;
                            } else if ($child_TBM2_current_point < 6.5 && $point_avg2_condition1 > 4) {
                                $point_avg2_condition1 = 4;
                            }
                            // cả năm
                            if ($child_avYear_point < 2 && $point_avgYearAll_condition1 > 1) {
                                $point_avgYearAll_condition1 = 1;
                            } else if ($child_avYear_point < 3.5 && $point_avgYearAll_condition1 > 2) {
                                $point_avgYearAll_condition1 = 2;
                            } else if ($child_avYear_point < 5 && $point_avgYearAll_condition1 > 3) {
                                $point_avgYearAll_condition1 = 3;
                            } else if ($child_avYear_point < 6.5 && $point_avgYearAll_condition1 > 4) {
                                $point_avgYearAll_condition1 = 4;
                            }
                            // xét xem điều kiện của môn toán văn
                            // lấy tên môn học
                            $subject_detail = $subjectDao->getSubjectById($each['subject_id']);
                            $each['subject_name'] = $subject_detail['subject_name'];
                            if (convert_vi_to_en(strtolower(html_entity_decode($each['subject_name']))) == "toan" || convert_vi_to_en(strtolower(html_entity_decode($each['subject_name']))) == "van") {
                                //học kỳ 1
                                if ($child_TBM1_current_point < 2 && $point_avg1_condition2 > 1) {
                                    $point_avg1_condition2 = 1;
                                } else if ($child_TBM1_current_point < 3.5 && $point_avg1_condition2 > 2) {
                                    $point_avg1_condition2 = 2;
                                } else if ($child_TBM1_current_point < 5 && $point_avg1_condition2 > 3) {
                                    $point_avg1_condition2 = 3;
                                } else if ($child_TBM1_current_point < 6.5 && $point_avg1_condition2 > 4) {
                                    $point_avg1_condition2 = 4;
                                }
                                // học kỳ 2
                                if ($child_TBM2_current_point < 2 && $point_avg2_condition2 > 1) {
                                    $point_avg2_condition2 = 1;
                                } else if ($child_TBM2_current_point < 3.5 && $point_avg2_condition2 > 2) {
                                    $point_avg2_condition2 = 2;
                                } else if ($child_TBM2_current_point < 5 && $point_avg2_condition2 > 3) {
                                    $point_avg2_condition2 = 3;
                                } else if ($child_TBM2_current_point < 6.5 && $point_avg2_condition2 > 4) {
                                    $point_avg2_condition2 = 4;
                                }
                                // cả năm
                                if ($child_avYear_point < 2 && $point_avgYearAll_condition2 > 1) {
                                    $point_avgYearAll_condition2 = 1;
                                } else if ($child_avYear_point < 3.5 && $point_avgYearAll_condition2 > 2) {
                                    $point_avgYearAll_condition2 = 2;
                                } else if ($child_avYear_point < 5 && $point_avgYearAll_condition2 > 3) {
                                    $point_avgYearAll_condition2 = 3;
                                } else if ($child_avYear_point < 6.5 && $point_avgYearAll_condition2 > 4) {
                                    $point_avgYearAll_condition2 = 4;
                                }
                            }
                            // Cộng vào điểm trung bình các môn
                            $child_TBM1_point += $child_TBM1_current_point;
                            $child_TBM2_point += $child_TBM2_current_point;
                            $child_avYearAll_point += $child_avYear_point;
                            $children_point_last[] = $each;
                        }
                        // điểm trung bình các môn
                        $child_TBM1_point /= count($children_point);
                        $child_TBM2_point /= count($children_point);
                        $child_avYearAll_point /= count($children_point);

                        //xét điều kiện để đánh giá pass hay fail
                        //học kỳ 1
                        if ($child_TBM1_point >= 8 && $point_avg1_condition1 == 5 && $point_avg1_condition2 == 5) { // trên 8 điểm thì đánh giá loại giỏi
                            $status_point1 = 5;
                        } else if ($child_TBM1_point >= 8 && ($point_avg1_condition1 == 3 || $point_avg1_condition2 == 3)) {
                            $status_point1 = 4;
                        } else if ($child_TBM1_point >= 8 && ($point_avg1_condition1 == 2 || $point_avg1_condition2 == 2)) {
                            $status_point1 = 3;
                        } else if ($child_TBM1_point >= 6.5 && $point_avg1_condition1 >= 4 && $point_avg1_condition2 >= 4) {  // trên 6.5 điểm thì đánh giá loại tiên tiến
                            $status_point1 = 4;
                        } else if ($child_TBM1_point >= 6.5 && ($point_avg1_condition1 == 2 || $point_avg1_condition2 == 2)) {  // trên 6.5 điểm thì đánh giá loại tiên tiến
                            $status_point1 = 3;
                        } else if ($child_TBM1_point >= 6.5 && ($point_avg1_condition1 == 1 || $point_avg1_condition2 == 1)) {  // trên 6.5 điểm thì đánh giá loại tiên tiến
                            $status_point1 = 2;
                        } else if ($child_TBM1_point >= 5 && $point_avg1_condition1 >= 3 && $point_avg1_condition2 >= 3) {  // trên 5 điểm thì đánh giá loại trung bình
                            $status_point1 = 3;
                        } else if ($child_TBM1_point >= 3.5 && $point_avg1_condition1 >= 2 && $point_avg1_condition2 >= 2) {  // trên 3.5 điểm thì đánh giá loại yếu
                            $status_point1 = 2;
                        } else if ($child_TBM1_point < 3.5 && $point_avg1_condition1 >= 1 && $point_avg1_condition2 >= 1) {
                            $status_point1 = 1;
                        }
                        // học kỳ 2
                        if ($child_TBM2_point >= 8 && $point_avg2_condition1 == 5 && $point_avg2_condition2 == 5) { // trên 8 điểm thì đánh giá loại giỏi
                            $status_point2 = 5;
                        } else if ($child_TBM2_point >= 8 && ($point_avg2_condition1 == 3 || $point_avg2_condition2 == 3)) {
                            $status_point2 = 4;
                        } else if ($child_TBM2_point >= 8 && ($point_avg2_condition1 == 2 || $point_avg2_condition2 == 2)) {
                            $status_point2 = 3;
                        } else if ($child_TBM2_point >= 6.5 && $point_avg2_condition1 >= 4 && $point_avg2_condition2 >= 4) {  // trên 6.5 điểm thì đánh giá loại tiên tiến
                            $status_point2 = 4;
                        } else if ($child_TBM2_point >= 6.5 && ($point_avg2_condition1 == 2 || $point_avg2_condition2 == 2)) {  // trên 6.5 điểm thì đánh giá loại tiên tiến
                            $status_point2 = 3;
                        } else if ($child_TBM2_point >= 6.5 && ($point_avg2_condition1 == 1 || $point_avg2_condition2 == 1)) {  // trên 6.5 điểm thì đánh giá loại tiên tiến
                            $status_point2 = 2;
                        } else if ($child_TBM2_point >= 5 && $point_avg2_condition1 >= 3 && $point_avg2_condition2 >= 3) {  // trên 5 điểm thì đánh giá loại trung bình
                            $status_point2 = 3;
                        } else if ($child_TBM2_point >= 3.5 && $point_avg2_condition1 >= 2 && $point_avg2_condition2 >= 2) {  // trên 3.5 điểm thì đánh giá loại yếu
                            $status_point2 = 2;
                        } else if ($child_TBM2_point < 3.5 && $point_avg2_condition1 >= 1 && $point_avg2_condition2 >= 1) {
                            $status_point2 = 1;
                        }
                        // cả năm
                        if ($child_avYearAll_point >= 8 && $point_avgYearAll_condition1 == 5 && $point_avgYearAll_condition2 == 5) { // trên 8 điểm thì đánh giá loại giỏi
                            $status_pointAll = 5;
                        } else if ($child_avYearAll_point >= 8 && ($point_avgYearAll_condition1 == 3 || $point_avgYearAll_condition2 == 3)) {
                            $status_pointAll = 4;
                        } else if ($child_avYearAll_point >= 8 && ($point_avgYearAll_condition1 == 2 || $point_avgYearAll_condition2 == 2)) {
                            $status_pointAll = 3;
                        } else if ($child_avYearAll_point >= 6.5 && $point_avgYearAll_condition1 >= 4 && $point_avgYearAll_condition2 >= 4) {  // trên 6.5 điểm thì đánh giá loại tiên tiến
                            $status_pointAll = 4;
                        } else if ($child_avYearAll_point >= 6.5 && ($point_avgYearAll_condition1 == 2 || $point_avgYearAll_condition2 == 2)) {  // trên 6.5 điểm thì đánh giá loại tiên tiến
                            $status_pointAll = 3;
                        } else if ($child_avYearAll_point >= 6.5 && ($point_avgYearAll_condition1 == 1 || $point_avgYearAll_condition2 == 1)) {  // trên 6.5 điểm thì đánh giá loại tiên tiến
                            $status_pointAll = 2;
                        } else if ($child_avYearAll_point >= 5 && $point_avgYearAll_condition1 >= 3 && $point_avgYearAll_condition2 >= 3) {  // trên 5 điểm thì đánh giá loại trung bình
                            $status_pointAll = 3;
                        } else if ($child_avYearAll_point >= 3.5 && $point_avgYearAll_condition1 >= 2 && $point_avgYearAll_condition2 >= 2) {  // trên 3.5 điểm thì đánh giá loại yếu
                            $status_pointAll = 2;
                        } else if ($child_avYearAll_point < 3.5 && $point_avgYearAll_condition1 >= 1 && $point_avgYearAll_condition2 >= 1) {
                            $status_pointAll = 1;
                        }

                        // Lấy hạnh kiểm của học sinh
                        $conduct = $conductDao->getConductOfChildren($class_id, $child_id, $school_year);
                        if (!isset($conduct) || !isset($conduct['ck'])) {
                            $flag_enough_point = false;
                        } else if ($child_absent['absent_true'] + $child_absent['absent_false'] <= 45
                            && $status_pointAll >= 3 && $conduct['ck'] != "Weak") {
                            $status_result = "pass";
                        }


                    }
                    if (!$flag_enough_point) {
                        $status_result = "waiting..";
                    }
                    // setup subject_key
                    for ($i = 1; $i <= $countColumnMaxPoint_hk1; $i++) {
                        $subject_key[] = 'hs1' . $i;
                    }
                    for ($i = 1; $i <= $countColumnMaxPoint_gk1; $i++) {
                        $subject_key[] = 'gk1' . $i;
                    }
                    $subject_key[] = 'ck1';
                    $subject_key[] = 'tb_hk1';
                    for ($i = 1; $i <= $countColumnMaxPoint_hk2; $i++) {
                        $subject_key[] = 'hs2' . $i;
                    }
                    for ($i = 1; $i <= $countColumnMaxPoint_gk2; $i++) {
                        $subject_key[] = 'gk2' . $i;
                    }
                    $subject_key[] = 'ck2';
                    $subject_key[] = 'tb_hk2';
                    $subject_key[] = 're_exam';


                    $smarty->assign("tb_total_hk1", $child_TBM1_point);
                    $smarty->assign("tb_total_hk2", $child_TBM2_point);
                    $smarty->assign("tb_total_year", $child_avYearAll_point);
                }
                $smarty->assign("column_hk1", $countColumnMaxPoint_hk1);
                $smarty->assign("column_hk2", $countColumnMaxPoint_hk2);
                $smarty->assign("column_gk1", $countColumnMaxPoint_gk1);
                $smarty->assign("column_gk2", $countColumnMaxPoint_gk2);
                $smarty->assign("rows", $children_point_last);
                $smarty->assign("grade", $schoolConfig['grade']);
                $smarty->assign("subject_key", $subject_key);
                $smarty->assign("status", $status_result);
                $smarty->assign("child_absent", $child_absent);
                $smarty->assign("children_subject_reexams", $children_subject_reexams);
                $smarty->assign("score_fomula", $schoolConfig['score_fomula']);
                $results['results'] = $smarty->fetch("ci/child/ajax.points.searchresult.tpl");
                return_json($results);
            }
            break;
        case 'show_comment_child':
            if (!isset($_POST['child_id']) || !is_numeric($_POST['child_id'])) {
                _error(404);
            }

            // Get school_config
            $schoolConfig = getSchoolData($_POST['school_id'], SCHOOL_CONFIG);
            $class_id = $_POST['class_id'];
            $school_year = $_POST['school_year'];
//            $is_last_semester = $_POST['is_last_semester'];
            // Lấy chi tiết nhận xét của trẻ theo điều kiện gửi lên
            $child_comment = $pointDao->getChildComment($_POST['child_id'], $class_id, $school_year, $_POST['semester'], $is_last_semester, $schoolConfig['grade']);

            $smarty->assign('results', $child_comment);
            $smarty->assign('username', $_POST['username']);
            $smarty->assign('grade', $schoolConfig['grade']);

            $return['results'] = $smarty->fetch("ci/child/ajax.child.point.comment.tpl");
            return_json($return);
            break;
        default:
            _error(400);
            break;
    }

} catch (Exception $e) {
    $db->rollback();
    return_json(array('error' => true, 'message' => $e->getMessage()));
} finally {
    $db->autocommit(true);
}

?>