<?php
/**
 * Package: ajax/ci/bo
 *
 * @package ConIu
 * @author TaiLA
 */

// fetch bootstrap
require('../../../../../bootstrap.php');
require(ABSPATH.'includes/class-image.php');

// check AJAX Request
is_ajax();
check_login();

try {
    $return = array();
    $return['callback'] = 'window.location.replace(response.path);';

    include_once(DAO_PATH.'dao_child.php');
    include_once(DAO_PATH.'dao_user.php');
    include_once(DAO_PATH.'dao_parent.php');
    include_once(DAO_PATH.'dao_school.php');
    include_once(DAO_PATH.'dao_class.php');
    include_once(DAO_PATH.'dao_teacher.php');

    $childDao = new ChildDAO();
    $userDao = new UserDAO();
    $parentDao = new ParentDAO();
    $schoolDao = new SchoolDAO();
    $classDao = new ClassDAO();
    $teacherDao = new TeacherDAO();

    $args = array();
    $args['user_id'] = $user->_data['user_id'];
    $args['first_name'] = trim($_POST['first_name']);
    $args['last_name'] = trim($_POST['last_name']);
    $args['child_name'] = $args['last_name']." ".$args['first_name'];
    $args['child_nickname'] = trim($_POST['nickname']);
    $args['name_for_sort'] = convert_to_en_4sort($args['first_name'].$args['last_name']);
    $args['description'] = isset($_POST['description'])? trim($_POST['description']) : "";
    $args['is_pregnant'] = isset($_POST['child_pregnant']) ? 1:0;
    if($args['is_pregnant']) {
        $args['pregnant_week'] = isset($_POST['pregnant_week']) ? $_POST['pregnant_week'] : 'null';
        $args['due_date_of_childbearing'] = !is_empty($_POST['due_date_of_childbearing']) ? trim($_POST['due_date_of_childbearing']): "null";
        $args['birthday'] = "null";
        $args['gender'] = "null";
    } else {
        $args['gender'] = $_POST['gender'];
        $args['birthday'] = !is_empty($_POST['birthday'])? trim($_POST['birthday']) : "null";
        $args['pregnant_week'] = 'null';
        $args['due_date_of_childbearing'] = 'null';
        $args['foetus_begin_date'] = 'null';
    }
    /*if (!validatePhone($args['parent_phone'])) {
        return_json( array('error' => true, 'message' => __("The phone number you entered is incorrect")) );
    }*/

    $db->autocommit(false);
    switch ($_POST['do']) {
        case 'add':
            /**
             * Hàm này xử lý khi phụ huynh tạo ra thông tin một trẻ. Công việc bao gồm
             * 1. Tạo thông tin trẻ trong bảng ci_child_parent
             * 2. Thêm cha mẹ cho trẻ
             */
            // Lấy thông tin user
            $userNow = $userDao->getUserAllByUsername($user->_data['user_name']);

            $db->begin_transaction();
            $args['child_code'] = generateCode($args['child_name']);
            $args['parent_email'] = $userNow['user_email'];
            $args['parent_phone'] = $userNow['user_phone'];
            $args['parent_name'] = $userNow['user_fullname'];
            $args['address'] = null;
            //1. Tạo thông tin trẻ trong bảng ci_child
            $lastId = $childDao->createChildByParent($args);

            //2. Thêm cha mẹ cho trẻ
            $parentIds = array();
            $parentIds[] = $user->_data['user_id'];
            $parentDao->addParentListForParent($lastId, $parentIds);

            $db->commit();
//            return_json(array('success' => true, 'message' => __("Done, Child info has been created")));
            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/childinfo/' . $lastId . '/childdetail' . '";'));
            break;
        case 'edit':
            /**
             * Hàm này xử lý khi phụ huynh edit thông tin của trẻ
             * 1. Cập nhật thông tin trẻ trong bảng ci_parent_child
             * 2. Cập nhật thông tin trẻ trong bảng ci_child nếu trẻ đang đi học
             * 3. Cập nhật thông tin trẻ trong trường (cập nhật bảng ci_school_child)
             * 4. Cập nhật cha mẹ vào nhóm của lớp (bao gồm việc tăng/giảm member)
             * 5. Cập nhật danh sách cha mẹ của trẻ
             * 6. Cập nhật danh sách cha mẹ like trang của trường (bao gồm cả việc thay đổi số like).
             */
            if (!is_numeric($_POST['child_parent_id'])) {
                _error(404);
            }
            $db->begin_transaction();

            // Lấy ra thông tin cũ của trẻ để kiểm tra
            $oldChild = $childDao->getChildByParent($_POST['child_parent_id']);
            if (is_null($oldChild)) {
                _error(404);
            }

            $class = null;
            if($oldChild['school_id'] > 0) {
                // Lấy class_id của trẻ
                $class = $classDao->getClassOfChild($oldChild['child_id'], $oldChild['school_id']);
            }

            //1. Cập nhật thông tin trẻ trong bảng ci_parent_child
            $args['child_parent_id'] = $_POST['child_parent_id'];
            $args['parent_email'] = trim($_POST['parent_email']);
            $args['parent_phone'] = trim($_POST['parent_phone']);
            $args['parent_name'] = trim($_POST['parent_name']);
            $args['parent_job'] = trim($_POST['parent_job']);
            $args['parent_phone_dad'] = trim($_POST['parent_phone_dad']);
            $args['parent_name_dad'] = trim($_POST['parent_name_dad']);
            $args['parent_job_dad'] = trim($_POST['parent_job_dad']);
            $args['address'] = $_POST['address'];
            $args['blood_type'] = $_POST['blood_type'];
            $args['hobby'] = $_POST['hobby'];
            $args['allergy'] = $_POST['allergy'];

            $args['child_picture'] = $oldChild['child_picture_path'];
            if(!$_POST['is_file']) {
                $args['child_picture'] = "null";
            }
            $file_name = "";
            // Upload file đính kèm
            if(file_exists($_FILES['file']['tmp_name'])) {
                // check file upload enabled
                if(!$system['file_enabled']) {
                    throw new Exception(__("This feature has been disabled"));
                }

                // valid inputs
                if(!isset($_FILES["file"]) || $_FILES["file"]["error"] != UPLOAD_ERR_OK) {
                    throw new Exception(__("Something wrong with upload! Is 'upload_max_filesize' set correctly?"));
                }

                // check file size
                $max_allowed_size = $system['max_file_size'] * 1024;
                if($_FILES["file"]["size"] > $max_allowed_size) {
                    throw new Exception(__("The file size is so big"));
                }

                // check file extesnion
                $extension = get_extension($_FILES['file']['name']);
                if(!valid_extension($extension, $system['file_medicine_extensions'])) {
                    throw new Exception(__("The file type is not valid or not supported"));
                    //modal(MESSAGE, __("Error"), __("The requested URL was not found on this server. That's all we know"));
                }

                /* check & create uploads dir */
                $depth = '../../../../../';
                // Tạo thư mục lưu ảnh đại diện của trẻ
                $folder = 'pictures';
                if(!file_exists($depth.$system['system_uploads_directory'].'/'.$folder)) {
                    @mkdir($depth.$system['system_uploads_directory'].'/'.$folder, 0777, true);
                }
                if(!file_exists($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y'))) {
                    @mkdir($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y'), 0777, true);
                }
                if(!file_exists($system['system_uploads_directory'].'/'.$folder.'/' . date('Y') . '/' . date('m'))) {
                    @mkdir($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y') . '/' . date('m'), 0777, true);
                }

                /* prepare new file name */
                $directory = $folder.'/'. date('Y') . '/' . date('m') . '/';
                $prefix = $system['uploads_prefix'].'_'.md5(time()*rand(1, 9999));

                $image = new Image($_FILES["file"]["tmp_name"]);

                $file_name = $directory . $prefix . $image->_img_ext;
                $file_name_tmp = $directory . $prefix . '_tmp' . $image->_img_ext;

                $path = $depth . $system['system_uploads_directory'] . '/' . $file_name;
                $path_tmp = $depth . $system['system_uploads_directory'] . '/' . $file_name_tmp;

                /* check if the file uploaded successfully */
                if(!@move_uploaded_file($_FILES['file']['tmp_name'], $path_tmp)) {
                    throw new Exception(__("Sorry, can not upload the file"));
                }
                /* save the new image */
                $image->save($path, $path_tmp);

                /* delete the tmp image */
                unlink($path_tmp);
                $args['child_picture'] = $file_name;
            }

            if($oldChild['school_id'] != 0 && (($_POST['first_name'] != convertText4Web($oldChild['first_name'])) || ($_POST['last_name'] != convertText4Web($oldChild['last_name'])) || ($_POST['gender'] != $oldChild['gender']) || ($_POST['birthday'] != $oldChild['birthday']))) {
                throw new Exception(__("When your student is in school, you can not change your name, gender and birthday, please contact the school"));
            }

            $childDao->editChildByParent($args);

            $oldParentIds = $parentDao->getParentIdsByParent($_POST['child_parent_id']);
            $newParentIds = (isset($_POST['user_id']) && (count($_POST['user_id']) > 0))? $_POST['user_id']: array();

            // bỏ những user_id trùng
            $newParentIds = array_unique($newParentIds);

            //2. Thay đổi thông tin trẻ trong bảng ci_child nếu trẻ đang đi học
            $args['child_id'] = $_POST['child_id'];
            if($oldChild['school_id'] != 0) {
                $childDao->editChildByParentOfCiChild($args);
            }

            if(empty($newParentIds)) {
                throw new Exception(__("Student must have parent"));
            }

            // 3.Xóa danh sách cha mẹ cũ đi
            $deletedParentIds = array_diff($oldParentIds, $newParentIds);

            if((count($deletedParentIds) == 1) && ($user->_data['user_id'] == $deletedParentIds[0]) && ($deletedParentIds[0] == $oldChild['child_admin'])) {
                throw new Exception(__("You can not remove the admin's account"));
            }
            if((count($deletedParentIds) > 1) && ($user->_data['user_id'] != $oldChild['child_admin'])) {
                throw new Exception(__("You do not have the right to delete parents"));
            }
            if(in_array($user->_data['user_id'], $deletedParentIds) && ($user->_data['user_id'] == $oldChild['child_admin'])) {
                throw new Exception(__("You do not have the right to remove a parent who administers a student"));
            }

            if (count($deletedParentIds) > 0) {
                //Xóa danh sách cha mẹ trong danh sách đc quản lý
                $parentDao->deleteParentListByParent($_POST['child_parent_id'], $deletedParentIds);
                if(($oldChild['school_id'] > 0) && $class['group_id'] > 0) {
                    // Xóa cha mẹ khỏi bảng ci_user_manage
                    $parentDao->deleteParentList($_POST['child_id'], $deletedParentIds);
                    //Xóa cha mẹ khỏi lớp cũ
                    $classDao->deleteUserFromClass($class['group_id'], $deletedParentIds);
                    //Xóa danh sách cha mẹ like page trường
                    $schoolDao->deleteUserLikeSchool($oldChild['school_id'], $deletedParentIds); //Xem xét có thể để cha mẹ like page của trường, ko cần thiết phải xóa
                }
            }

            // Thêm danh sách phụ huynh mới vào
            $addedParentIds = array_diff($newParentIds, $oldParentIds);
            if ($oldChild['school_id'] > 0) {
                if (count($addedParentIds) > 0) {
                    //3.2. Cho phụ huynh like các page của hệ thống, join các group của hệ thống
                    $userDao->addUsersLikeSomePages($system_page_ids, $addedParentIds);
                    $userDao->addUserToSomeGroups($system_group_ids, $addedParentIds);

                    //4. Thêm cha mẹ vào nhóm của lớp (bao gồm việc tăng thành viên nhóm)
                    // Nếu chưa chọn lớp thì bỏ qua
                    if (!is_null($class) && $class['group_id'] > 0) {
                        $classDao->addUserToClass($class['group_id'], $addedParentIds);
                    }

                    $school = getSchoolData($oldChild['school_id'], SCHOOL_DATA);
//                    //6. Cho cha mẹ like trang của trường (tăng số like)
//                    $schoolDao->addUsersLikeSchool($oldChild['school_id'], $addedParentIds);
                    //6. Cho cha mẹ like trang của trường (tăng số like)
                    $likeCnt = $schoolDao->addUsersLikeSchool($school['page_id'], $addedParentIds);

                    $school['page_likes'] = $school['page_likes'] + $likeCnt;

                    // Thêm cha mẹ quản lý trẻ trong bảng ci_user_manage
                    $parentDao->addParentList($_POST['child_id'], $addedParentIds);
                    // Thêm cha mẹ quản lý trẻ vào bảng ci_parent_manage
                    $childDao->createParentManageInfo($addedParentIds, $_POST['child_parent_id'], $_POST['child_id'], $oldChild['school_id']);
                }
                /* ---------- UPDATE - MEMCACHE ---------- */
                //1. Cập nhật thông tin trường
                updateSchoolData($oldChild['school_id'], SCHOOL_INFO);
                //2. Cập nhật thông tin lớp
                if ($class['group_id'] > 0) {
                    updateClassData($class['group_id'], CLASS_INFO);
                }
                //3.Cập nhật thông tin trẻ
                updateChildData($_POST['child_id'], CHILD_INFO);
                updateChildData($_POST['child_id'], CHILD_PARENTS);

                //4. Cập nhật tin tin quản lý trẻ
                foreach ($addedParentIds as $parentId) {
                    updateUserManageData($parentId, USER_MANAGE_CHILDREN);
                }
                foreach ($deletedParentIds as $parentId) {
                    updateUserManageData($parentId, USER_MANAGE_CHILDREN);
                }
                /* ---------- END - MEMCACHE ---------- */
            } else {
                if(count($addedParentIds) > 0) {
                    //5. Thêm cha mẹ cho trẻ & người tạo ra thông tin trẻ vào danh sách người có thể quản lý (bảng ci_parent_manage), không có child_id và school_id
                    $parentDao->addParentListForParent($_POST['child_parent_id'], $addedParentIds);
                }
            }
            $db->commit();
            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/childinfo/' . $_POST['child_parent_id'] . '/childdetail' . '";'));
            break;
        case 'delete':
            if (!isset($_POST['child_parent_id']) || !is_numeric($_POST['child_parent_id'])) {
                _error(404);
            }
            $db->begin_transaction();
            // Lấy thông tin trẻ để kiểm tra
            $child = $childDao->getChildByParent($_POST['child_parent_id']);

            if($child['school_id'] != 0) {
                modal(ERROR, __("Error"), (__("It is impossible to remove when student is under the management of the school")));
            } else {
                // Xóa phụ huynh của trẻ trong bảng ci_parent_mangage
                $childDao->deleteAllParentOfChildForParent($_POST['child_parent_id']);
                // Xóa trẻ trong bảng ci_child_parent
                $childDao->deleteChildForParent($_POST['child_parent_id']);
            }
            $db->commit();
            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '";'));
            break;
        default:
            _error(400);
            break;
    }
}
catch (Exception $e) {
    $db->rollback();
    return_json( array('error' => true, 'message' => $e->getMessage()) );
} finally {
    $db->autocommit(true);
}
?>