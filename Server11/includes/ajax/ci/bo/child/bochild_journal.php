<?php
/**
 * Package: ajax/ci/bo
 *
 * @package ConIu
 * @author TaiLA
 */

// fetch bootstrap
require('../../../../../bootstrap.php');
require(ABSPATH.'includes/class-image.php');

// check AJAX Request
is_ajax();
check_login();

try {
    $return = array();
    $return['callback'] = 'window.location.replace(response.path);';

    include_once(DAO_PATH.'dao_child.php');
    include_once(DAO_PATH.'dao_journal.php');
    include_once(DAO_PATH.'dao_user.php');

    $childDao = new ChildDAO();
    $journalDao = new JournalDAO();
    $userDao = new UserDAO();

    $db->autocommit(false);
    switch ($_POST['do']) {
        case 'add':
            /**
             * Hàm này xử lý khi phụ huynh thêm thông tin nhật ký cho trẻ
             */
            if(!isset($_POST['child_parent_id']) || !is_numeric($_POST['child_parent_id'])) {
                _error(404);
            }

            $db->begin_transaction();

            $child_parent_id = $_POST['child_parent_id'];
            $caption = isset($_POST['caption']) ? $_POST['caption'] : 'null';
            // get allowed file size
            $max_allowed_size = $system['max_photo_size'] * 1024;

            /* check & create uploads dir */
            $depth = '../../../../../';
            $folder = 'children';
            if(!file_exists($depth.$system['system_uploads_directory'].'/'.$folder)) {
                @mkdir($depth.$system['system_uploads_directory'].'/'.$folder, 0777, true);
            }
            if(!file_exists($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y'))) {
                @mkdir($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y'), 0777, true);
            }
            if(!file_exists($system['system_uploads_directory'].'/'.$folder.'/' . date('Y') . '/' . date('m'))) {
                @mkdir($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y') . '/' . date('m'), 0777, true);
            }

            $files = array();
            foreach ($_FILES['file'] as $key => $val) {
                for ($i = 0; $i < count($val); $i++) {
                    $files[$i][$key] = $val[$i];
                }
            }

            $return_files = array();
            $files_num = count($files);

            foreach ($files as $file) {

                // valid inputs
                if (!isset($file) || $file["error"] != UPLOAD_ERR_OK) {
                    if ($files_num > 1) {
                        continue;
                    } else {
//                        modal(MESSAGE, __("Upload Error"), __("Something wrong with upload! Is 'upload_max_filesize' set correctly?"));
                        return_json( array('error' => true, 'message' => __("You must add at least one image")) );
                    }
                }

                // check file size
                if ($file["size"] > $max_allowed_size) {
                    if ($files_num > 1) {
                        continue;
                    } else {
                        //modal(MESSAGE, __("Upload Error"), __("The file size is so big"));
                        return_json( array('error' => true, 'message' => __("The file size is so big")) );
                    }
                }

                /* prepare new file name */
                $directory = $folder . '/' . date('Y') . '/' . date('m') . '/';
                $prefix = $system['uploads_prefix'] . '_' . md5(time() * rand(1, 9999));


                $image = new Image($file["tmp_name"]);

                $image_tmp_name = $directory . $prefix . '_tmp' . $image->_img_ext;
                $image_new_name = $directory . $prefix . $image->_img_ext;
                $path_tmp = $depth . $system['system_uploads_directory'] . '/' . $image_tmp_name;
                $path_new = $depth . $system['system_uploads_directory'] . '/' . $image_new_name;

                /* check if the file uploaded successfully */
                if (!@move_uploaded_file($file['tmp_name'], $path_tmp)) {
                    if ($files_num > 1) {
                        continue;
                    } else {
                        //modal(MESSAGE, __("Upload Error"), __("Sorry, can not upload the file"));
                        return_json( array('error' => true, 'message' => __("Sorry, can not upload the file")) );
                    }
                }

                /* save the new image */
                $image->save($path_new, $path_tmp);

                /* delete the tmp image */
                unlink($path_tmp);

                /* return */
                $return_files[] = $image_new_name;
            }
            //Thêm album nhật ký
            $isParent = 1;
            $journalAlbumId = $journalDao->insertJournalAlbum($child_parent_id, $caption, $isParent);
            // Thêm ảnh vào album
            $journalDao->insertJournal($journalAlbumId, $return_files);

            // Lấy thông tin trẻ
            $child = $childDao->getChildByParent($_POST['child_parent_id']);
            // Tăng lượt tương tác - TaiLA
            addInteractive($child['school_id'], 'diary', 'parent_view', $journalAlbumId, 2);

            $db->commit();
            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/childinfo/' . $_POST['child_parent_id'] . '/journals";'));
            break;
        case 'edit':
            if (!is_numeric($_POST['child_parent_id'])) {
                _error(404);
            }
            $db->begin_transaction();

            // Lấy thông tin nhật ký
            $journal = $journalDao->getJournalById($_POST['child_journal_id']);
            $args['child_parent_id'] = $_POST['child_parent_id'];
            $args['caption'] = (isset($_POST['caption']) && $_POST['caption'] != null) ? $_POST['caption'] : 'null';
            $args['child_journal_id'] = $_POST['child_journal_id'];

            $args['source_file'] = $journal['source_file_path'];
            if(!$_POST['is_file']) {
                $args['source_file'] = null;
            }
            $file_name = "";
            // Upload file đính kèm
            if(file_exists($_FILES['file']['tmp_name'])) {
                // check file upload enabled
                if(!$system['file_enabled']) {
                    modal(MESSAGE, __("Not Allowed"), __("This feature has been disabled"));
                }

                // valid inputs
                if(!isset($_FILES["file"]) || $_FILES["file"]["error"] != UPLOAD_ERR_OK) {
//                    modal(MESSAGE, __("Upload Error"), __("Something wrong with upload! Is 'upload_max_filesize' set correctly?"));
                    return_json( array('error' => true, 'message' => __("Something wrong with upload! Is 'upload_max_filesize' set correctly?")) );
                }

                // check file size
                $max_allowed_size = $system['max_file_size'] * 1024;
                if($_FILES["file"]["size"] > $max_allowed_size) {
//                    modal(MESSAGE, __("Upload Error"), __("The file size is so big"));
                    return_json( array('error' => true, 'message' => __("The file size is so big")) );
                }

                // check file extesnion
                $extension = get_extension($_FILES['file']['name']);
                if(!valid_extension($extension, $system['file_medicine_extensions'])) {
                    return_json( array('error' => true, 'message' => __("The file type is not valid or not supported")) );
                }

                /* check & create uploads dir */
                $depth = '../../../../../';
                // Tạo thư mục lưu ảnh nhật ký của trẻ
                $folder = 'children';
                if(!file_exists($depth.$system['system_uploads_directory'].'/'.$folder)) {
                    @mkdir($depth.$system['system_uploads_directory'].'/'.$folder, 0777, true);
                }
                if(!file_exists($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y'))) {
                    @mkdir($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y'), 0777, true);
                }
                if(!file_exists($system['system_uploads_directory'].'/'.$folder.'/' . date('Y') . '/' . date('m'))) {
                    @mkdir($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y') . '/' . date('m'), 0777, true);
                }

                /* prepare new file name */
                $directory = $folder.'/'. date('Y') . '/' . date('m') . '/';
                $prefix = $system['uploads_prefix'].'_'.md5(time()*rand(1, 9999));

                $image = new Image($_FILES["file"]["tmp_name"]);

                $file_name = $directory . $prefix . $image->_img_ext;
                $file_name_tmp = $directory . $prefix . '_tmp' . $image->_img_ext;

                $path = $depth . $system['system_uploads_directory'] . '/' . $file_name;
                $path_tmp = $depth . $system['system_uploads_directory'] . '/' . $file_name_tmp;

                /* check if the file uploaded successfully */
                if(!@move_uploaded_file($_FILES['file']['tmp_name'], $path_tmp)) {
//                    modal(MESSAGE, __("Upload Error"), __("Sorry, can not upload the file"));
                    return_json( array('error' => true, 'message' => __("Sorry, can not upload the file")) );
                }
                /* save the new image */
                $image->save($path, $path_tmp);

                /* delete the tmp image */
                unlink($path_tmp);
                $args['source_file'] = $file_name;
            }

            $journalDao->updateJournal($args);

            $db->commit();
            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/childinfo/' . $_POST['child_parent_id'] . '/journals";'));
            break;
        case 'delete':
            if (!isset($_POST['child_journal_id']) || !is_numeric($_POST['child_journal_id'])) {
                _api_error(404);
            }
            if (!isset($_POST['child_parent_id']) || !is_numeric($_POST['child_parent_id'])) {
                _api_error(404);
            }
            $db->begin_transaction();
            $journalDao->deleteJournal($_POST['child_journal_id']);
            $db->commit();
            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/childinfo/' . $_POST['child_parent_id'] . '/journals";'));
            break;
        case 'delete_album':
            if (!isset($_POST['child_journal_album_id']) || !is_numeric($_POST['child_journal_album_id'])) {
                _api_error(404);
            }
            if (!isset($_POST['child_parent_id']) || !is_numeric($_POST['child_parent_id'])) {
                _api_error(404);
            }
            $db->begin_transaction();
            $journalDao->deleteJournalAlbum($_POST['child_journal_album_id']);
            $db->commit();
            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/childinfo/' . $_POST['child_parent_id'] . '/journals";'));
            break;
        case 'edit_caption':
            if (!isset($_POST['child_journal_album_id']) || !is_numeric($_POST['child_journal_album_id'])) {
                _api_error(404);
            }
            if (!isset($_POST['child_parent_id']) || !is_numeric($_POST['child_parent_id'])) {
                _api_error(404);
            }

            $db->begin_transaction();
            $journalDao->editCaptionAlbum($_POST['child_journal_album_id'], $_POST['caption']);
            $db->commit();
            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/childinfo/' . $_POST['child_parent_id'] . '/journals";'));
            break;
        case 'search':
            if(!isset($_POST['child_parent_id']) || !is_numeric(($_POST['child_parent_id']))) {
                _error(404);
            }

            $results = $journalDao->getAllJournalChildOfYear($_POST['child_parent_id'], $_POST['year']);

            $smarty->assign('results', $results);
            $smarty->assign('child_parent_id', $_POST['child_parent_id']);
            $return['results'] = $smarty->fetch("ci/ajax.journallist.tpl");
            $return['no_data'] = (count($results) == 0);
            return_json($return);

            break;
        case 'add_photo':
            // valid inputs
            if(!isset($_POST['child_journal_album_id']) || !is_numeric($_POST['child_journal_album_id'])) {
                _error(400);
            }
            $db->begin_transaction();

            // get allowed file size
            $max_allowed_size = $system['max_photo_size'] * 1024;

            /* check & create uploads dir */
            $depth = '../../../../../';
            $folder = 'children';
            if(!file_exists($depth.$system['system_uploads_directory'].'/'.$folder)) {
                @mkdir($depth.$system['system_uploads_directory'].'/'.$folder, 0777, true);
            }
            if(!file_exists($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y'))) {
                @mkdir($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y'), 0777, true);
            }
            if(!file_exists($system['system_uploads_directory'].'/'.$folder.'/' . date('Y') . '/' . date('m'))) {
                @mkdir($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y') . '/' . date('m'), 0777, true);
            }

            $files = array();
            foreach ($_FILES['file'] as $key => $val) {
                for ($i = 0; $i < count($val); $i++) {
                    $files[$i][$key] = $val[$i];
                }
            }

            $return_files = array();
            $files_num = count($files);

            foreach ($files as $file) {

                // valid inputs
                if (!isset($file) || $file["error"] != UPLOAD_ERR_OK) {
                    if ($files_num > 1) {
                        continue;
                    } else {
//                        modal(MESSAGE, __("Upload Error"), __("Something wrong with upload! Is 'upload_max_filesize' set correctly?"));
                        return_json( array('error' => true, 'message' => __("You must add at least one image")) );
                    }
                }

                // check file size
                if ($file["size"] > $max_allowed_size) {
                    if ($files_num > 1) {
                        continue;
                    } else {
                        //modal(MESSAGE, __("Upload Error"), __("The file size is so big"));
                        return_json( array('error' => true, 'message' => __("The file size is so big")) );
                    }
                }

                /* prepare new file name */
                $directory = $folder . '/' . date('Y') . '/' . date('m') . '/';
                $prefix = $system['uploads_prefix'] . '_' . md5(time() * rand(1, 9999));


                $image = new Image($file["tmp_name"]);

                $image_tmp_name = $directory . $prefix . '_tmp' . $image->_img_ext;
                $image_new_name = $directory . $prefix . $image->_img_ext;
                $path_tmp = $depth . $system['system_uploads_directory'] . '/' . $image_tmp_name;
                $path_new = $depth . $system['system_uploads_directory'] . '/' . $image_new_name;

                /* check if the file uploaded successfully */
                if (!@move_uploaded_file($file['tmp_name'], $path_tmp)) {
                    if ($files_num > 1) {
                        continue;
                    } else {
                        //modal(MESSAGE, __("Upload Error"), __("Sorry, can not upload the file"));
                        return_json( array('error' => true, 'message' => __("Sorry, can not upload the file")) );
                    }
                }

                /* save the new image */
                $image->save($path_new, $path_tmp);

                /* delete the tmp image */
                unlink($path_tmp);

                /* return */
                $return_files[] = $image_new_name;
            }

            // Thêm ảnh vào album
            $journalDao->addPhotoForAlbum($_POST['child_journal_album_id'], $return_files);
//            $post_id = $user->add_album_photos($inputs);
            $db->commit();
            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/childinfo/' . $_POST['child_parent_id'] . '/journals";'));
            break;
        default:
            _error(403);
            break;
    }
}
catch (Exception $e) {
    $db->rollback();
    return_json( array('error' => true, 'message' => $e->getMessage()) );
} finally {
    $db->autocommit(true);
}
?>