<?php
/**
 * Package: ajax/ci/bo
 *
 * @package ConIu
 * @author TaiLA
 */

// fetch bootstrap
require('../../../../../bootstrap.php');

// check AJAX Request
is_ajax();
check_login();

try {
    $return = array();
    $return['callback'] = 'window.location.replace(response.path);';

    include_once(DAO_PATH.'dao_child_development.php');

    $childDevelopmentDao = new ChildDevelopmentDAO();

    $db->autocommit(false);
    switch ($_POST['do']) {
        case 'vaccinationed':
            if (!isset($_POST['child_development_id']) || !is_numeric($_POST['child_development_id'])) {
                _api_error(404);
            }

            if (!isset($_POST['child_parent_id']) || !is_numeric($_POST['child_parent_id'])) {
                _api_error(404);
            }

            $db->begin_transaction();
            $childDevelopmentDao->insertChildVaccinationed($_POST['child_development_id'], $_POST['child_parent_id']);
            $db->commit();

            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/childinfo/' . $_POST['child_parent_id'] . '/childdevelopments";'));
            break;
        case 'cancel_vaccination':
            if (!isset($_POST['child_development_id']) || !is_numeric($_POST['child_development_id'])) {
                _api_error(404);
            }

            if (!isset($_POST['child_parent_id']) || !is_numeric($_POST['child_parent_id'])) {
                _api_error(404);
            }

            $db->begin_transaction();
            $childDevelopmentDao->deleteChildVaccination($_POST['child_development_id'], $_POST['child_parent_id']);
            $db->commit();

            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/childinfo/' . $_POST['child_parent_id'] . '/childdevelopments";'));
            break;
        default:
            _error(400);
            break;
    }
}
catch (Exception $e) {
    $db->rollback();
    return_json( array('error' => true, 'message' => $e->getMessage()) );
} finally {
    $db->autocommit(true);
}
?>