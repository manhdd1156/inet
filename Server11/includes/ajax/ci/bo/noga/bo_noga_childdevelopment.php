<?php
/**
 * Package: ajax/ci/bo
 *
 * @package ConIu
 * @author TaiLA
 */

// fetch bootstrap
require('../../../../../bootstrap.php');

// check AJAX Request
user_access();
$nogaRole = $user->_data['user_group'];
if ((!$user->_is_admin) && ($nogaRole != USER_NOGA_MANAGE_ALL) && ($nogaRole != USER_NOGA_MANAGE_CITY)
    && ($nogaRole != USER_NOGA_MANAGE_SCHOOL) && ($nogaRole != USER_MOD)){
    _error(__('System Message'), __("You don't have the right permission to access this"));
}
include_once(DAO_PATH.'dao_user.php');
$userDao = new UserDAO();

include_once(DAO_PATH.'dao_child_development.php');
$childDevelopmentDao = new ChildDevelopmentDAO();

include_once(DAO_PATH.'dao_user_manage.php');
$userManDao = new UserManageDAO();

try {

    $db->autocommit(false);
    switch ($_POST['do']) {
        case 'add':
            $db->begin_transaction();

            //Insert vào bảng ci_child_development
            $args['type'] = $_POST['type'];
            $args['month_push'] = $_POST['month_push'];
            $args['day_push'] = $_POST['day_push'];
            $args['time_push'] = $_POST['time_push'];
            $args['is_notice_before'] = (isset($_POST['is_notice_before']) && $_POST['is_notice_before'] == 'on') ? 1 : 0;
            $args['notice_before_days'] = $_POST['notice_before_days'];
            $args['is_reminder_before'] = (isset($_POST['is_reminder_before']) && $_POST['is_reminder_before'] == 'on') ? 1 : 0;
            $args['reminder_before_days'] = $_POST['reminder_before_days'];
            $args['title'] = $_POST['title'];
            $args['is_development'] = $_POST['is_development'];
            if(file_exists($_FILES['file']['tmp_name'])) {
                $args['image'] = upload($_FILES['file'], 'images', 'foetus_and_child');
            }
            $args['content_type'] = $_POST['content_type'];
            if($args['content_type'] == 1) {
                $args['content'] = $_POST['content_text'];
                $args['link'] = '';
            } else if($args['content_type'] == 2) {
                $args['content'] = '';
                $args['link'] = $_POST['link'];
            } else {
                $args['content'] = $_POST['content_text'];
                $args['link'] = $_POST['link'];
            }
            $childDevelopmentDao->insertChildDevelopment($args);
            $db->commit();
            //return_json(array('success' => true, 'message' => __("Done, Foetus development information has been created")));
            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/noga/childdevelopments";'));

            break;
        case 'push_notify':
            include_once(DAO_PATH . 'dao_parent.php');
            $parentDao = new ParentDAO();
            $hourSys = date('H');

            $db->begin_transaction();
            $childDevelopment = $childDevelopmentDao->getChildDevelopmentForNoga();

            foreach ($childDevelopment as $row) {
                $extra1 = convertText4Web($row['title']);
                if (!is_empty($row['image'])) {
                    $extra3 = $system['system_uploads'] . '/' . $row['image'];
                } else {
                    $extra3 = $system['system_url']. '/content/themes/inet/images/favicon.png';
                }

                $hourPush = date('H', strtotime($row['time_push']));
                if($hourPush == $hourSys) {
                    $daysPush = getDayForPushNotification($row['month_push'],$row['day_push'], $row['is_notice_before'], $row['notice_before_days'], $row['is_reminder_before'], $row['reminder_before_days']);
                    foreach ($daysPush as $dayPush) {
                        $childParentIds = getChildIdsPushNotifyChildDevelopment($dayPush);
                        foreach ($childParentIds as $childParentId) {
                            $parentIds = $parentDao->getParentIdsByParent($childParentId);
                            foreach ($parentIds as $parentId) {
                                $userDao->postNotifications( $parentId, NOTIFICATION_CHILD_DEVELOPMENT, NOTIFICATION_NODE_TYPE_CHILD, $row['child_development_id'], $extra1, $childParentId, $extra3, 1);
                                //$userDao->postNotifications($parentId, NOTIFICATION_CHILD_DEVELOPMENT, $type, $node_url, $extra1, $extra2, $extra3);
                            }
                        }
                    }
                }
            }

            $db->commit();
            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/noga/childdevelopments/push";'));
            //return_json(array('success' => true, 'message' => __("Done, Schedule have been updated")));
            break;

        case 'edit':
            $db->begin_transaction();
            if(!isset($_POST['child_development_id']) || !is_numeric($_POST['child_development_id'])) {
                _error(404);
            }
            $args = array();
            $args['child_development_id'] = $_POST['child_development_id'];
            $args['type'] = $_POST['type'];
            $args['month_push'] = $_POST['month_push'];
            $args['day_push'] = $_POST['day_push'];
            $args['time_push'] = $_POST['time_push'];
            $args['is_notice_before'] = (isset($_POST['is_notice_before']) && $_POST['is_notice_before'] == 'on') ? 1 : 0;
            $args['notice_before_days'] = $_POST['notice_before_days'];
            $args['is_reminder_before'] = (isset($_POST['is_reminder_before']) && $_POST['is_reminder_before'] == 'on') ? 1 : 0;
            $args['reminder_before_days'] = $_POST['reminder_before_days'];
            $args['title'] = $_POST['title'];
            $args['is_development'] = $_POST['is_development'];
            if($_POST['is_file']) {
                $args['image'] = $_POST['old_src'];
            } else {
                $args['image'] = "";
            }
            if(file_exists($_FILES['file']['tmp_name'])) {
                $args['image'] = upload($_FILES['file'], 'images', 'foetus_and_child');
            }
            $args['content_type'] = $_POST['content_type'];
            if($args['content_type'] == 1) {
                $args['content'] = $_POST['content_text'];
                $args['link'] = '';
            } else if($args['content_type'] == 2) {
                $args['content'] = '';
                $args['link'] = $_POST['link'];
            } else {
                $args['content'] = $_POST['content_text'];
                $args['link'] = $_POST['link'];
            }

            $childDevelopmentDao->updateChildDevelopment($args);
            $db->commit();

            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/noga/childdevelopments";'));
            break;
        case 'delete':
            if(!isset($_POST['id']) || !is_numeric($_POST['id'])) {
                _error(404);
            }
            $db->begin_transaction();

            $childDevelopmentDao->deleteChildDevelopment($_POST['id']);

            $db->commit();

            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/noga/childdevelopments";'));
            break;
        case 'search':
            if(!isset($_POST['type'])) {
                _error(404);
            }
            if($_POST['type'] == 0) {
                $child_development = $childDevelopmentDao->getChildDevelopmentForNoga();
            } else {
                $child_development = $childDevelopmentDao->getChildDevelopmentByType($_POST['type']);
            }

            /* assign variables */
            $smarty->assign('results', $child_development);
            /* return */
            $return['results'] = $smarty->fetch("ci/noga/ajax.noga.childdevelopment.list.tpl");
            return_json($return);
            break;
        default:
            _error(400);
            break;
    }

} catch (Exception $e) {
    $db->rollback();
    return_json( array('error' => true, 'message' => $e->getMessage()) );
} finally {
    $db->autocommit(true);
}

/**
 * Lấy ra ngày gửi thông báo từ ngày + thời gian thông báo trước + thời gian nhắc lại
 *
 * @return array
 */
function getDayForPushNotification($month_push, $day_push, $is_notice_before, $notice_before_days, $is_reminder_before, $reminder_before_days) {
    $days = array();
    $day_push = (($month_push - 1) * 30) + $day_push;
    if ($is_notice_before) {
        $days[] = $day_push - $notice_before_days;
    }
    if ($is_reminder_before) {
        $days[] = $day_push - $reminder_before_days;
    }
    if (!$is_notice_before && !$is_reminder_before) {
        $days[] = $day_push;
    }
    $days = array_unique($days);
    return $days;
}

?>