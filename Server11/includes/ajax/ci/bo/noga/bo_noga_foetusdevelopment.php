<?php
/**
 * Package: ajax/ci/bo
 *
 * @package ConIu
 * @author TaiLA
 */

// fetch bootstrap
require('../../../../../bootstrap.php');

// check AJAX Request
user_access();
$nogaRole = $user->_data['user_group'];
if ((!$user->_is_admin) && ($nogaRole != USER_NOGA_MANAGE_ALL) && ($nogaRole != USER_NOGA_MANAGE_CITY)
    && ($nogaRole != USER_NOGA_MANAGE_SCHOOL) && ($nogaRole != USER_MOD)){
    _error(__('System Message'), __("You don't have the right permission to access this"));
}
include_once(DAO_PATH.'dao_user.php');
$userDao = new UserDAO();

include_once(DAO_PATH.'dao_foetus_development.php');
$foetusDevelopmentDao = new FoetusDevelopmentDAO();

include_once(DAO_PATH.'dao_user_manage.php');
$userManDao = new UserManageDAO();

try {
    include_once(DAO_PATH.'dao_school.php');
    $schoolDao = new SchoolDAO();
    $return = array();

    $db->autocommit(false);
    switch ($_POST['do']) {
        case 'add':
            $db->begin_transaction();

            // Insert vào bảng ci_foetus_info
            $args['type'] = $_POST['type'];
            $args['day_push'] = $_POST['day_push'];
            $args['time'] = $_POST['hour'];
            $args['is_notice_before'] = (isset($_POST['is_notice_before']) && $_POST['is_notice_before'] == 'on') ? 1 : 0;
            $args['notice_before_days'] = $_POST['notice_before_days'];
            $args['is_reminder_before'] = (isset($_POST['is_reminder_before']) && $_POST['is_reminder_before'] == 'on') ? 1 : 0;
            $args['reminder_before_days'] = $_POST['reminder_before_days'];
            $args['title'] = $_POST['title'];
            $args['is_development'] = $_POST['is_development'];
            if(file_exists($_FILES['file']['tmp_name'])) {
                $args['image'] = upload($_FILES['file'], 'images', 'foetus_and_child');
            }

            $args['content_type'] = $_POST['content_type'];
            if($args['content_type'] == 1) {
                $args['content'] = $_POST['content_text'];
                $args['link'] = '';
            } else if($args['content_type'] == 2) {
                $args['content'] = '';
                $args['link'] = $_POST['link'];
            } else {
                $args['content'] = $_POST['content_text'];
                $args['link'] = $_POST['link'];
            }

            $foetusDevelopmentDao->insertFoetusInfo($args);

            $db->commit();
            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/noga/foetusdevelopments";'));
            //return_json(array('success' => true, 'message' => __("Done, Foetus development information has been created")));
            break;

        case 'push_notify':
            include_once(DAO_PATH . 'dao_parent.php');
            $parentDao = new ParentDAO();
            $hourSys = date('H');

            $db->begin_transaction();
            $foetusDevelopment = $foetusDevelopmentDao->getFoetusInfoForNoga();
            foreach ($foetusDevelopment as $row) {
                $extra1 = convertText4Web($row['title']);
                if (!is_empty($row['image'])) {
                    $extra3 = $system['system_uploads'] . '/' . $row['image'];
                } else {
                    $extra3 = $system['system_url']. '/content/themes/inet/images/favicon.png';
                }
                //$fromUserId = $notification['created_user_id'];

                $hourPush = date('H', strtotime($row['time']));
                if($hourPush == $hourSys) {
                    $daysPush = getDayForPushNotification($row['day_push'], $row['is_notice_before'], $row['notice_before_days'], $row['is_reminder_before'], $row['reminder_before_days']);
                    foreach ($daysPush as $dayPush) {
                        $childParentIds = getChildIdsPushNotify($dayPush);
                        foreach ($childParentIds as $childParentId) {
                            $parentIds = $parentDao->getParentIdsByParent($childParentId);
                            $userDao->postNotifications($parentIds, NOTIFICATION_FOETUS_DEVELOPMENT, NOTIFICATION_NODE_TYPE_CHILD, $row['foetus_info_id'], $extra1, $childParentId, $extra3, 1);
                        }
                    }
                }
            }

            $db->commit();
            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/noga/foetusdevelopments/push";'));
            //return_json(array('success' => true, 'message' => __("Done, Schedule have been updated")));
            break;

        case 'edit':
            $db->begin_transaction();
            if(!isset($_POST['foetus_info_id']) || !is_numeric($_POST['foetus_info_id'])) {
                _error(404);
            }
            $args = array();
            $args['foetus_info_id'] = $_POST['foetus_info_id'];
            $args['type'] = $_POST['type'];
            $args['day_push'] = $_POST['day_push'];
            $args['time'] = $_POST['hour'];
            $args['is_notice_before'] = (isset($_POST['is_notice_before']) && $_POST['is_notice_before'] == 'on') ? 1 : 0;
            $args['notice_before_days'] = $_POST['notice_before_days'];
            $args['is_reminder_before'] = (isset($_POST['is_reminder_before']) && $_POST['is_reminder_before'] == 'on') ? 1 : 0;
            $args['reminder_before_days'] = $_POST['reminder_before_days'];
            $args['title'] = $_POST['title'];
            $args['is_development'] = $_POST['is_development'];
            if($_POST['is_file']) {
                $args['image'] = $_POST['old_src'];
            } else {
                $args['image'] = "";
            }
            if(file_exists($_FILES['file']['tmp_name'])) {
                $args['image'] = upload($_FILES['file'], 'images', 'foetus_and_child');
            }

            $args['content_type'] = $_POST['content_type'];
            if($args['content_type'] == 1) {
                $args['content'] = $_POST['content_text'];
                $args['link'] = '';
            } else if($args['content_type'] == 2) {
                $args['content'] = '';
                $args['link'] = $_POST['link'];
            } else {
                $args['content'] = $_POST['content_text'];
                $args['link'] = $_POST['link'];
            }
            $foetusDevelopmentDao->updateFoetusInfo($args);
            $db->commit();

            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/noga/foetusdevelopments";'));
            break;

        case 'delete':
            if(!isset($_POST['id']) || !is_numeric($_POST['id'])) {
                _error(404);
            }
            $db->begin_transaction();

            $foetusDevelopmentDao->deleteFoetusInfo($_POST['id']);

            $db->commit();

            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/noga/foetusdevelopments";'));
            break;
        case 'search':
            if(!isset($_POST['type'])) {
                _error(404);
            }
            if($_POST['type'] == 0) {
                $foetus_info = $foetusDevelopmentDao->getFoetusInfoForNoga();
            } else {
                $foetus_info = $foetusDevelopmentDao->getFoetusInfoByType($_POST['type']);
            }

            /* assign variables */
            $smarty->assign('results', $foetus_info);
            /* return */
            $return['results'] = $smarty->fetch("ci/noga/ajax.noga.foetusdevelopment.list.tpl");
            return_json($return);
            break;
        default:
            _error(400);
            break;
    }

} catch (Exception $e) {
    $db->rollback();
    return_json( array('error' => true, 'message' => $e->getMessage()) );
} finally {
    $db->autocommit(true);
}


/**
 * Lấy ra ngày gửi thông báo từ ngày + thời gian thông báo trước + thời gian nhắc lại
 *
 * @return array
 */
function getDayForPushNotification($day_push, $is_notice_before, $notice_before_days, $is_reminder_before, $reminder_before_days) {
    $days = array();
    if ($is_notice_before) {
        $days[] = $day_push - $notice_before_days;
    }
    if ($is_reminder_before) {
        $days[] = $day_push - $reminder_before_days;
    }
    if (!$is_notice_before && !$is_reminder_before) {
        $days[] = $day_push;
    }
    $days = array_unique($days);
    return $days;
}

?>