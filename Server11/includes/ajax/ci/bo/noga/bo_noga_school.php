<?php
/**
 * Package: ajax/ci/bo
 * 
 * @package ConIu
 * @author QuanND
 */

// fetch bootstrap
require('../../../../../bootstrap.php');

// check AJAX Request
user_access();
$nogaRole = $user->_data['user_group'];
if ((!$user->_is_admin) && ($nogaRole != USER_NOGA_MANAGE_ALL) && ($nogaRole != USER_NOGA_MANAGE_CITY)
            && ($nogaRole != USER_NOGA_MANAGE_SCHOOL) && ($nogaRole != USER_MOD)){
    _error(__('System Message'), __("You don't have the right permission to access this"));
}
include_once(DAO_PATH.'dao_user.php');
$userDao = new UserDAO();
include_once(DAO_PATH.'dao_class_level.php');
$classLevelDao = new ClassLevelDAO();
include_once(DAO_PATH.'dao_user_manage.php');
$userManDao = new UserManageDAO();
include_once(DAO_PATH.'dao_child.php');
$childDao = new ChildDAO();
include_once(DAO_PATH.'dao_teacher.php');
$teacherDao = new TeacherDAO();

try {

    include_once(DAO_PATH.'dao_school.php');
    $schoolDao = new SchoolDAO();
    $return = array();

    $db->autocommit(false);
    switch ($_POST['do']) {
        case 'search_manager':
            // Danh sách user đã chọn thì ko chọn nữa
            $userIds = $_POST['user_ids'];
            $userIds[] = $user->_data['user_id']; //Tính cả user hiện tại cũng ko được chọn

            // get results
            $results = $userDao->searchUser($_POST['query'], $userIds);
            if (count($results) > 0) {
                /* assign variables */
                $smarty->assign('results', $results);
                /* return */
                $return['results'] = $smarty->fetch("ci/noga/ajax.noga.search4manager.tpl");
            }
            return_json($return);
            break;
        case 'add_manager':
            $user_ids = [$_POST['user_id']];
            $results = $userDao->getUsers($user_ids);
            if (count($results) > 0) {
                /* assign variables */
                $smarty->assign('results', $results);
                /* return */
                $return['results'] = $smarty->fetch("ci/noga/ajax.noga.managerlist.tpl");
                $return['phone'] = $results[0]['user_phone'];
                $return['email'] = $results[0]['user_email'];
            } else {
                $return['results'] = __("No manager");
            }
            return_json($return);
            break;

        case 'addsystemschool':
            $db->begin_transaction();
            $args = array();
            $args['created_user_id'] = $user->_data['user_id'];
            $args['system_title'] = $_POST['system_title'];
            $args['system_username'] = $_POST['system_username'];
            $args['system_description'] = $_POST['system_description'];
            $schoolDao->insertSystemSchools($args);
            $db->commit();
            return_json(array('callback' => 'window.location = "'.$system['system_url'].'/noga/schools/listsystemschool' . '";'
            ));
            break;

        case 'editsystemschool':
            $db->begin_transaction();
            $args = array();
            $args['system_school_id'] = $_POST['system_school_id'];
            $args['system_title'] = $_POST['system_title'];
            $args['system_username'] = $_POST['system_username'];
            $args['system_description'] = $_POST['system_description'];

            // edit system school
            $schoolDao->editSystemSchools($args);
            $db->commit();
            return_json(array('callback' => 'window.location = "'.$system['system_url'].'/noga/schools/listsystemschool' . '";'
            ));
            break;

        case 'add':
            $db->begin_transaction();

            $args = array();
            // Kiểm tra có gửi lên thông tin
            //Kiểm tra xem có được set người quản lý không
            if(isset($_POST['user_ids']) && (count($_POST['user_ids']) > 0)) {
                // Trường hợp có gửi lên user_id
                $args['page_admin'] = $_POST['user_ids'][0];
            } else {
                // Tạo user mới làm quản lý
                $user_arr = array();
                //UPDATE MANHDD 25/06/2021
//                $user_arr['first_name'] = 'Inet';
                $user_arr['first_name'] = $_POST['username'];
//                $user_arr['last_name'] = 'User';
                $user_arr['password'] = '123456';
                $user_arr['gender'] = 'female';
//                $user_arr['full_name'] = $user_arr['last_name']." ".$user_arr['first_name'];
                $user_arr['full_name'] =  $user_arr['first_name'];
                //UPDATE MANHDD 25/06/2021
                // Nếu không gửi lên user_id, kiểm tra xem có gửi lên search-manager không
                if(isset($_POST['search-manager']) && trim($_POST['search-manager'] != null)) {
                    // Kiểm tra xem có phải là 1 email hay không?
                    if(valid_email($_POST['search-manager'])) {
                        // Tạo tài khoản quản lý với $_POST['search-manager'] là email
                        $user_arr['email'] = trim($_POST['search-manager']);
                        $user_arr['username'] = generateUsername($user_arr['full_name']);
                    } else {
                        // Tạo tài khoản quản lý với $_POST['search-manager'] là username
                        $user_arr['email'] = '';
                        $user_arr['username'] = trim($_POST['search-manager']);
                    }
                } else {
//                    $user_arr['email'] = generateEmail();
                    $user_arr['email'] = $_POST['email'];
//                    $user_arr['username'] = generateUsername($user_arr['full_name']);
                    $user_arr['username'] = generateUsername('user');
                }

                // 1. Tạo user mới
                $userId = $userDao->createUser($user_arr);
                $args['page_admin'] = $userId;
            }
            // $args['page_admin'] = (isset($_POST['user_ids']) && (count($_POST['user_ids']) > 0))? $_POST['user_ids'][0]: $user->_data['user_id'];
            $args['page_category'] = SCHOOL_CATEGORY_ID;
            $args['page_name'] = (isset($_POST['username']) && $_POST['username'] != null) ? $_POST['username'] : ('school' . time());
            $args['page_title'] = $_POST['title'];
            $args['score_fomula'] = $_POST['score_fomula'];
            $args['page_description'] = $_POST['description'];
            $args['city_id'] = $_POST['city_id'];
            $args['telephone'] = $_POST['telephone'];
            $args['website'] = $_POST['website'];
            $args['email'] = $_POST['email'];
            $args['address'] = $_POST['address'];

            // Insert vào pages
            $args['page_id'] = $schoolDao->createSchoolNoga($args);

            //Nếu assign người quản lý trường luôn thì insert vào user_manager
            if ($user->_data['user_id'] != $args['page_admin']) {
                $argsUM = array();
                $argsUM['user_id'] = $args['page_admin'];
                $argsUM['object_id'] = $args['page_id'];
                $argsUM['object_type'] = MANAGE_SCHOOL;
                $argsUM['status'] = STATUS_ACTIVE;  //Active luôn trạng thái quản lý
                $argsUM['role_id'] = PERMISSION_ALL; //Người tạo ra thì có toàn quyền
                //Đưa vào bảng đối tượng quản lý
                $userManDao->insertUserManage($argsUM);

                $teacherIds = array();
                $teacherIds[] = $args['page_admin'];
                // Cho admin là giáo viên của trường
                $teacherDao->addTeacherToSchool($args['page_id'], $teacherIds);
                // Cho quản lý vào danh sách quản lý page, bảng pages_admins
//                $admins = array();
//                $admins[] = $args['page_admin'];
//                $userDao->addAdminsToPagesAdmins($args['page_id'], $admins);

                // Memcache - cập nhật danh sách giáo viên
                updateUserManageData($args['page_admin'], USER_MANAGE_SCHOOL);
                addSchoolData($args['page_id'], SCHOOL_TEACHERS, $args['page_admin']);

                //2.Cập nhật số lượng giáo viên của trường
                $teacherCnt = 1;
                $array_update = array(
                    'teacher_count' => $teacherCnt
                );
                updateSchoolData($args['page_admin'], SCHOOL_CONFIG, $array_update);
                /* ---------- END - MEMCACHE ---------- */
            }

            //Cho user được xét làm Quản trị like page luôn
            //if ($args['user_id'] != $user->_data['user_id']) {
                $schoolDao->addUsersLikeSchool($args['page_id'], [$args['page_admin']]);
            //}

            //Cập nhật người quản lý của trường.
            //Nếu user chỉ có quyền quản lý một số trường (ko quản lý tất cả, không quản lý một địa bàn) thì mới cập nhật
            if ($nogaRole == USER_NOGA_MANAGE_SCHOOL) {
                $userDao->insertNogaManage($user->_data['user_id'], 1, NOGA_MANAGE_TYPE_SCHOOL, $args['page_id']);
            }
            error_log('tao truong - $_POST[\'grade\'] :'.$_POST['grade']);
            //Thêm các khối mặc định
            $grades = array();
            if($_POST['grade'] == 0) {
                $grades = $defaultGrades;
            } else if($_POST['grade'] == 1) {
                $grades = $defaultGrades1;
            } else if($_POST['grade'] == 2) {
                $grades = $defaultGrades2;
            }else {
                $grades = $defaultGrades3;
            }
            foreach($grades as $grade) {
                $classLevelDao->insertDefaultCL($grade['name'], $args['page_id'], $grade['description'], $grade['gov_class_level']);
                //Memcache - Cập nhật thông tin khối
                updateClassLevelData($classLevelId, CLASS_LEVEL_INFO);
            }

            // Thêm các môn học mặc định đối với trường cấp 1 và cấp 2
        // DELETE START - MANHDD
        //admin sẽ không tự đông thêm môn vào nữa, mà để cho hiệu trưởng tự thêm môn học vào khối
//            $subjects = array();
//            if($_POST['grade'] == 1 || $_POST['grade'] == 2) {
//                $subjects = $default_subjects[$_POST['grade']];
//
//                // Thêm vào bảng ci_class_level_subject
//                $schoolDao->createClassLevelSubjectDefault($args['page_id'], $subjects, $school_year);
//            }
        // DELETE END - MANHDD
            //Tạo một profile
            $agrsPro = array();
            $agrsPro['inside_system_school'] = $_POST['inside_system_school'];
            $agrsPro['school_type'] = $_POST['school_type'];
            $agrsPro['district_slug'] = $_POST['district_slug'];
            $agrsPro['grade'] = $_POST['grade'];
            $districtName = "";
            foreach ($cities as $city) {
                if($city['city_id'] == $_POST['city_id']) {
                    foreach ($city['district'] as $district)
                    if($_POST['district_slug'] == $district['district_slug']) {
                        $districtName = $district['district_name'];
                    }
                }
            }
            $agrsPro['longtitude'] = $_POST['longtitude'];
            $agrsPro['latitude'] = $_POST['latitude'];
            $agrsPro['direct'] = $_POST['direct'];
            $agrsPro['short_description'] = $_POST['short_description'];
            $citySlug = "";
            $cityName = "";
            foreach ($cities as $city) {
                if($city['city_id'] == $_POST['city_id']) {
                    $citySlug = $city['city_slug'];
                    $cityName = $city['city_name'];
                }
            }
            $agrsPro['city_slug'] = $citySlug;
            $agrsPro['city_name'] = $cityName;
            $agrsPro['district_name'] = $districtName;
            $schoolDao->createSchoolProfile($args['page_id'], $agrsPro);

            // sửa module mặc định theo khối
            $argsEdit = array();
            foreach ($moduleName as $module => $value) {
                $argsEdit[$module] = $defaultModule[$_POST['grade']][$module] ? 1 : 0 ;
            }
            if(isset($argsEdit['points']) && $argsEdit['points']) {
                $argsEdit['score_fomula'] = $args['score_fomula'];
            }
            // update module vào db
            $schoolDao->updateSchoolModules($args['page_id'], $argsEdit);

            // Cho admin làm hiệu trưởng trường
            if ($user->_data['user_id'] != $args['page_admin']) {
                // Cập nhật danh sách hiệu trưởng trong bảng ci_school_configuration
                $schoolDao->updateSchoolPrincipal($args['page_id'], $teacherIds);
            }
            $db->commit();

            /* CI - Memcached */
            // Lưu thông tin trường vào memcached
            updateSchoolData($args['page_id'], SCHOOL_INFO);
            updateSchoolData($args['page_id'], SCHOOL_CONFIG);
			updateSchoolData($args['page_id'], SCHOOL_CLASS_LEVELS);
            updateUserManageData($args['page_admin'], USER_MANAGE_SCHOOL);
            /* END - Memcached */

            return_json(array(
                'callback' => 'window.location = "'.$system['system_url'].'/noga/schools/edit/' . $args['page_id'] . '/services' . '";'
                ));
            break;

        case  'basic':
            $db->begin_transaction();
            if(!isset($_POST['page_id']) || !is_numeric($_POST['page_id'])) {
                _error(400);
            }
            $args = array();

            $school = null;
            if (($nogaRole == USER_NOGA_MANAGE_CITY) || ($nogaRole == USER_NOGA_MANAGE_SCHOOL)) {
                $school = $schoolDao->getSchoolNoga($_POST['page_id'], $user->_data['user_id']);
            } else {
                //$school = $schoolDao->getSchoolById($_POST['page_id']);
                $school = getSchoolData($_POST['page_id'], SCHOOL_INFO);
            }
            if (is_null($school)) {
                _error(403);
            }
            //Kiểm tra xem có được set người quản lý không
            if(isset($_POST['user_ids']) && (count($_POST['user_ids']) > 0)) {
                // Trường hợp có gửi lên user_id
                $args['page_admin'] = $_POST['user_ids'][0];
            } else {
                // Tạo user mới làm quản lý
                $user_arr = array();
                $user_arr['first_name'] = 'Inet';
                $user_arr['last_name'] = 'User';
                $user_arr['password'] = '123456';
                $user_arr['gender'] = 'female';
                $user_arr['full_name'] = $user_arr['last_name']." ".$user_arr['first_name'];
                // Nếu không gửi lên user_id, kiểm tra xem có gửi lên search-manager không
                if(isset($_POST['search-manager']) && trim($_POST['search-manager'] != null)) {
                    // Kiểm tra xem có phải là 1 email hay không?
                    if(valid_email($_POST['search-manager'])) {
                        // Tạo tài khoản quản lý với $_POST['search-manager'] là email
                        $user_arr['email'] = trim($_POST['search-manager']);
                        $user_arr['username'] = generateUsername($user_arr['full_name']);
                    } else {
                        // Tạo tài khoản quản lý với $_POST['search-manager'] là username
                        $user_arr['email'] = '';
                        $user_arr['username'] = trim($_POST['search-manager']);
                    }
                } else {
                    $user_arr['email'] = generateEmail();
                    $user_arr['username'] = generateUsername($user_arr['full_name']);
                }

                // 1. Tạo user mới
                $userId = $userDao->createUser($user_arr);
                $args['page_admin'] = $userId;
            }

            // $args['page_admin'] = (isset($_POST['user_ids']) && (count($_POST['user_ids']) > 0))? $_POST['user_ids'][0]: $user->_data['user_id'];
            $args['page_title'] = $_POST['title'];
            $args['page_name'] = (isset($_POST['username']) && $_POST['username'] != null) ? $_POST['username'] : ('school' . time());
            $args['page_description'] = $_POST['description'];
            $args['city_id'] = $_POST['city_id'];
            $args['telephone'] = $_POST['telephone'];
            $args['website'] = $_POST['website'];
            $args['email'] = $_POST['email'];
            $args['address'] = $_POST['address'];
            $args['page_id'] = $_POST['page_id'];
            $args['username_old'] = $school['page_name'];
            $schoolDao->editSchoolNoga($args);

            //Nếu thay đổi page_admin thì cập nhật bảng quản lý user_manage
            if ($school['page_admin'] != $args['page_admin']) {
                //Xóa thông tin người quản lý cũ
                $userManDao->deleteSchoolManager($_POST['page_id']);

                $argsUM = array();
                $argsUM['user_id'] = $args['page_admin'];
                $argsUM['object_id'] = $args['page_id'];
                $argsUM['object_type'] = MANAGE_SCHOOL;
                $argsUM['status'] = STATUS_ACTIVE;  //Active luôn trạng thái quản lý
                $argsUM['role_id'] = PERMISSION_ALL; //Người tạo ra thì có toàn quyền
                //Đưa vào bảng đối tượng quản lý
                $userManDao->insertUserManage($argsUM);

                /* CI - Memcached */
                $teacherIds = array();
                $teacherIds[] = $args['page_admin'];

                // Lấy sanh sách giáo viên của trường
                $teachers = getSchoolData($_POST['page_id'], SCHOOL_TEACHERS);
                $teachersId = array_keys($teachers);
                if(!in_array($args['page_admin'], $teachersId)) {
                    // Cho admin là giáo viên của trường
                    $teacherDao->addTeacherToSchool($args['page_id'], $teacherIds);
                }
                // Lấy danh sách hiệu trường của trường
                $schoolCon = getSchoolData($args['page_id'], SCHOOL_CONFIG);
                $oldPrincipals = array();
                if (!is_empty($schoolCon['principal'])) {
                    $oldPrincipals = explode(',', $schoolCon['principal']);
                }
                $newPrincipals = array();
                foreach ($oldPrincipals as $oldPrincipal) {
                    if($oldPrincipal != $school['page_admin']) {
                        $newPrincipals[] = $oldPrincipal;
                    }
                }
                $newPrincipals[] = $args['page_admin'];
                // Cập nhật danh sách hiệu trưởng trong bảng ci_school_configuration
                $schoolDao->updateSchoolPrincipal($school['page_id'], $newPrincipals);

                // Memcache - cập nhật danh sách giáo viên
                updateUserManageData($args['page_admin'], USER_MANAGE_SCHOOL);
                addSchoolData($args['page_id'], SCHOOL_TEACHERS, $args['page_admin']);
            }
            //Cho user được xét làm quản trị like page luôn
            if ($args['page_admin'] != $user->_data['user_id']) {
                $schoolDao->addUsersLikeSchool($args['page_id'], [$args['page_admin']]);
            }

            // update school profile
            $agrsPro = array();
            $agrsPro['inside_system_school'] = $_POST['inside_system_school'];
            $agrsPro['school_type'] = $_POST['school_type'];
            $agrsPro['district_slug'] = $_POST['district_slug'];
            $agrsPro['grade'] = $_POST['grade'];
            $districtName = "";
            foreach ($cities as $city) {
                if($city['city_id'] == $_POST['city_id']) {
                    foreach ($city['district'] as $district)
                        if($_POST['district_slug'] == $district['district_slug']) {
                            $districtName = $district['district_name'];
                        }
                }
            }
            $agrsPro['longtitude'] = $_POST['longtitude'];
            $agrsPro['latitude'] = $_POST['latitude'];
            $agrsPro['direct'] = $_POST['direct'];
            $agrsPro['short_description'] = $_POST['short_description'];
            $citySlug = "";
            $cityName = "";
            foreach ($cities as $city) {
                if($city['city_id'] == $_POST['city_id']) {
                    $citySlug = $city['city_slug'];
                    $cityName = $city['city_name'];
                }
            }
            $agrsPro['city_slug'] = $citySlug;
            $agrsPro['city_name'] = $cityName;
            $agrsPro['district_name'] = $districtName;
            $schoolDao->updateSchoolProfile($args['page_id'], $agrsPro);
            $db->commit();

            /* CI - Memcached */
            unset($args['username_old']);
            // Lưu thông tin trường vào memcached
            //1.Cập nhật thông tin trường
            updateSchoolData($args['page_id']);
            //1.Cập nhật thông tin user quản lý
            updateUserManageData($school['page_admin'], USER_MANAGE_SCHOOL);
            /* END - Memcached */

            return_json(array('callback' => 'window.location = "'.$system['system_url'].'/noga/schools/edit/'. $_POST['page_id']. '/services' . '";'));
            break;

        case 'services':
            $db->begin_transaction();

            $argsFa = array();
            if(!isset($_POST['page_id']) || !is_numeric($_POST['page_id'])) {
                _error(404);
            }
            if(isset($_POST['facilities'])) {
                $facilities = array();
                $facilities = $_POST['facilities'];
                $argsFa = array();
                $argsFa['school_id'] = $_POST['page_id'];
                $argsFa['pool'] = isset($facilities[0]) ? $facilities[0] : 0;
                $argsFa['outdoor'] = isset($facilities[1]) ? $facilities[1] : 0;
                $argsFa['indoor'] = isset($facilities[2]) ? $facilities[2] : 0;
                $argsFa['library'] = isset($facilities[3]) ? $facilities[3] : 0;
                $argsFa['viewcamera'] = isset($facilities[4]) ? $facilities[4]: 0;
                $argsFa['note'] = $_POST['facility_note'];

                // Update facility vào school config
                $schoolDao->updateSchoolConfigNogaFacility($argsFa);
            }
            if(isset($_POST['services'])) {
                $services = array();
                $services = $_POST['services'];
                $argsSe = array();
                $argsSe['school_id'] = $_POST['page_id'];
                $argsSe['breakfast'] = isset($services[0]) ? $services[0] : 0;
                $argsSe['belated'] = isset($services[1]) ? $services[1] : 0;
                $argsSe['saturday'] = isset($services[2]) ? $services[2] : 0;
                $argsSe['bus'] = isset($services[3]) ? $services[3] : 0;
                $argsSe['note'] = $_POST['service_note'];

                // Update facility vào school config
                $schoolDao->updateSchoolConfigNogaServices($argsSe);
            }

            $db->commit();
            return_json(array('callback' => 'window.location = "'.$system['system_url'].'/noga/schools/edit/'. $_POST['page_id']. '/addmission' . '";'));
            break;

        case 'addmission':
            $db->begin_transaction();

            $args = array();
            if(!isset($_POST['page_id']) || !is_numeric($_POST['page_id'])) {
                _error(404);
            }
            $args['school_id'] = $_POST['page_id'];
            $args['age_begin'] = $_POST['age_begin'];
            $args['age_end'] = $_POST['age_end'];
            $args['tuition_begin'] = $_POST['tuition_begin'];
            $args['tuition_end'] = $_POST['tuition_end'];
            $args['tuition_note'] = $_POST['tuition_note'];
            $args['addmission'] = $_POST['addmission'];
            $args['addmission_note'] = $_POST['addmission_note'];
            $schoolDao->updateSchoolConfigNogaAddmission($args);
            $db->commit();
            return_json(array('callback' => 'window.location = "'.$system['system_url'].'/noga/schools/edit/'. $_POST['page_id'] . '/info'.'";'));
            break;

        case 'info':
            $db->begin_transaction();

            $args = array();
            if(!isset($_POST['page_id']) || !is_numeric($_POST['page_id'])) {
                _error(404);
            }
            $args['school_id'] = $_POST['page_id'];
            $args['leader'] = $_POST['leader'];
            $args['method'] = $_POST['method'];
            $args['teacher'] = $_POST['teacher'];
            $args['nutrition'] = $_POST['nutrition'];
            $schoolDao->updateSchoolConfigNogaInfo($args);
            $db->commit();
            return_json(array('callback' => 'window.location = "'.$system['system_url'].'/noga/schools'.'";'));
            break;

        case 'district_list':
            if (!isset($_POST['city_id'])) {
                _error(404);
            }

            //$districts = $childDao->getChildrenOfClass($_POST['city_slug']);
            $results = "";
            $results = $results . '<option value = "0">' . __('Select district') . '</option>';
            if($_POST['city_id'] != 0) {
                foreach ($cities as $city) {
                    if($city['city_id'] == $_POST['city_id']) {
                        $districts = $city['district'];
                        if(count($districts) > 0) {
                            foreach ($districts as $district) {
                                $results = $results . '<option value="' . $district['district_slug'] . '">' . $district['district_name'] . '</option>';
                            }
                        }
                    }
                }
            }
            $return['results'] = $results;

            return_json($return);
            break;

        case 'view_user':
            include_once(DAO_PATH.'dao_parent.php');
            $parentDao = new ParentDAO();
            include_once(DAO_PATH.'dao_child.php');
            $childDao = new ChildDAO();
            if (!isset($_POST['page_id']) || !is_numeric($_POST['page_id'])) {
                _error(404);
            }
            $results = array();
            $parents = array();
            if (!isset($_POST['class_id']) || $_POST['class_id'] == "") {
                $parents = $parentDao->getParentOfSchoolForNoga($_POST['page_id']);
            } else {
                $parents = $parentDao->getParentOfClassForNoga($_POST['class_id']);
            }
            foreach ($parents as $parent) {
                $children = $childDao->getChildren($parent['user_id']);
                $parent['children'] = $children;
                $results[] = $parent;
                if(!is_empty($parent['user_last_login'])) {
                    $countLogin++;
                } else {
                    $countNoLogin++;
                }
            }
            $smarty->assign('results', $results);
            $smarty->assign('countLogin', $countLogin);
            $smarty->assign('countNoLogin', $countNoLogin);
            $return['results'] = $smarty->fetch("ci/noga/ajax.parentlist.tpl");
            return_json($return);
            break;
        case 'export':
            $reportTemplate = ABSPATH."content/templates/user_lists.xlsx";
            include_once(ABSPATH . 'includes/libs/PHPExcel/PHPExcel/IOFactory.php');
            include_once(ABSPATH . 'includes/libs/PHPExcel/PHPExcel.php');

            $objPHPExcel = PHPExcel_IOFactory::load($reportTemplate);

            $all_border_style = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('rgb' => '333333'))));

            // Get school
            $school = getSchoolData($_POST['school_id'], SCHOOL_INFO);

            $condition = $_SESSION[SESSION_KEY_SEARCH_CHILDREN_NOGA];
            $result = $childDao->search($condition['keyword'], $school['page_id'], $condition['class_id']);
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', mb_strtoupper(convertText4Web($school['page_title']),"UTF-8"));

            if($condition['class_id'] != null){
                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('E2', convertText4Web($result['children'][0]['group_title']));
            } else {
                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A2', convertText4Web($school['page_title']));
            }

            $rowNumber = 5;
            $idx = 1;
            foreach ($result['children'] as $row) {
                if (count($row['parent']) == 0) {
                    $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A' . $rowNumber, $idx++)
                        ->setCellValue('B' . $rowNumber, convertText4Web($row['child_name']))
                        ->setCellValue('C' . $rowNumber, $row['parent_phone'])
                        ->setCellValue('D' . $rowNumber, __("No parent"))
                        ->setCellValue('E' . $rowNumber, __("No parent"))
                        ->setCellValue('F' . $rowNumber, __("No parent"))
                        ->setCellValue('G' . $rowNumber, __("No parent"))
                        ->setCellValue('H' . $rowNumber, __("No parent"))
                        ->setCellValue('I' . $rowNumber, __("No parent"));
                    $rowNumber++;
                } elseif(count($row['parent']) == 1) {
                    foreach ($row['parent'] as $_user) {
                        $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue('A' . $rowNumber, $idx++)
                            ->setCellValue('B' . $rowNumber, convertText4Web($row['child_name']))
                            ->setCellValue('C' . $rowNumber, $row['parent_phone'])
                            ->setCellValue('D' . $rowNumber, convertText4Web($_user['user_fullname']))
                            ->setCellValue('E' . $rowNumber, $_user['user_name'])
                            ->setCellValue('F' . $rowNumber, $_user['user_email'])
                            ->setCellValue('G' . $rowNumber, $_user['user_phone'])
                            ->setCellValue('H' . $rowNumber, $_user['user_last_login'])
                            ->setCellValue('I' . $rowNumber, $_user['user_last_active']);
                        $rowNumber++;
                    }
                } else {
                    $rowMerge = $rowNumber + count($row['parent']) -1;
                    $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A' . $rowNumber . ':A' . $rowMerge);
                    $objPHPExcel->setActiveSheetIndex(0)->mergeCells('B' . $rowNumber . ':B' . $rowMerge);
                    $objPHPExcel->setActiveSheetIndex(0)->mergeCells('C' . $rowNumber . ':C' . $rowMerge);
                    $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A' . $rowNumber, $idx++)
                        ->setCellValue('B' . $rowNumber, convertText4Web($row['child_name']))
                        ->setCellValue('C' . $rowNumber, $row['parent_phone']);
                    foreach ($row['parent'] as $_user) {
                        $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue('D' . $rowNumber, convertText4Web($_user['user_fullname']))
                            ->setCellValue('E' . $rowNumber, $_user['user_name'])
                            ->setCellValue('F' . $rowNumber, $_user['user_email'])
                            ->setCellValue('G' . $rowNumber, $_user['user_phone'])
                            ->setCellValue('H' . $rowNumber, $_user['user_last_login'])
                            ->setCellValue('I' . $rowNumber, $_user['user_last_active']);
                        $rowNumber++;
                    }
                }
            }
            $rowEnd = $rowNumber - 1;
            $objPHPExcel->getActiveSheet()->getStyle('A5:I' . $rowEnd)->applyFromArray($all_border_style);
            $objPHPExcel->getActiveSheet()->getStyle('A5:A' . $rowEnd)->applyFromArray(
                styleArray('Times new Roman', true, false, 11, PHPExcel_Style_Alignment::HORIZONTAL_CENTER, PHPExcel_Style_Alignment::VERTICAL_CENTER, true));
            $objPHPExcel->getActiveSheet()->getStyle('B5:I' . $rowEnd)->applyFromArray(
                styleArray('Times new Roman', false, false, 11, PHPExcel_Style_Alignment::HORIZONTAL_LEFT, PHPExcel_Style_Alignment::VERTICAL_CENTER, true));
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            ob_start();
            if($condition['class_id'] != null) {
                $group_name = str_replace('/', '_', $result['children'][0]['group_title']);
                $file_name = convertText4Web('danhsachtre_'.$group_name.'.xlsx');
            } else {
                $school_name = str_replace('/', '_', $school['page_title']);
                $file_name = convertText4Web('danhsachtre_'.$school_name.'.xlsx');
            }
            $file_name = convert_vi_to_en($file_name);
            $file_name = str_replace(" ",'_', $file_name);

            $objWriter->save("php://output");

            $xlsData = ob_get_contents();
            ob_end_clean();

            return_json(array(
                'file_name' => $file_name,
                'file' => "data:application/vnd.ms-excel;base64,".base64_encode($xlsData)
            ));
            break;
        case 'school_search':
            $nogaRole = $_POST['nogaRole'];
            $school_status = $_POST['school_status'];
            $city_id = $_POST['city_id'];
            $district_slug = $_POST['district_slug'];

            $return = array();
            $schools = null;
            if (($nogaRole == USER_NOGA_MANAGE_ALL) || $user->_is_admin) {
                $schools = $schoolDao->getAllSchoolConiuByClause($school_status, $city_id, $district_slug);
            } elseif ($nogaRole == USER_NOGA_MANAGE_CITY) {
                $nogaManages = $userDao->getNogaManage($user->_data['user_id']);
                $schools = $schoolDao->getSchoolInCity($nogaManages[0]['city_id']);
            } else {
                $schools = $schoolDao->getSchoolOfNOGAUser($user->_data['user_id']);
            }
            $smarty->assign('rows', $schools);
            $return['results'] = $smarty->fetch("ci/noga/ajax.noga.schoollist.tpl");

            return_json($return);
            break;
        case 'change_status':
            $db->begin_transaction();
            if(!isset($_POST['id']) || !is_numeric($_POST['id'])) {
                _error(404);
            }
            $schoolId = $_POST['id'];
            $school_status = $_POST['school_status'];
            // Update status
            $schoolDao->changeSchoolStatus($schoolId, $_POST['school_status']);
            $db->commit();

            // Update MEMCACHE
            updateSchoolData($_POST['id'], SCHOOL_CONFIG, array('school_status' => $_POST['school_status']));
            $classes = getSchoolData($_POST['id'], SCHOOL_CLASSES);
            foreach ($classes as $class) {
                updateClassData($class['group_id'], CLASS_INFO);
            }

            // END MEMCACHE
            return_json();
            break;
        case 'edit_module':
            $db->begin_transaction();
            if(!isset($_POST['school_id']) || !is_numeric($_POST['school_id'])) {
                _error(404);
            }
            $schoolId = $_POST['school_id'];
            $args = array();
            foreach ($moduleName as $module => $value) {
                if(isset($_POST[$module]) && $_POST[$module] == 1) {
                    $args[$module] = 1;
                } else {
                    $args[$module] = 0;
                }
            }
            //ADD START MANHDD 1/6/2021
            // THêm phần chọn công thức tính điểm theo nước nào
            $args['score_fomula'] = $_POST['score_fomula'];
            //ADD END MANHDD 1/6/2021
            // update module vào db
            $schoolDao->updateSchoolModules($schoolId, $args);
            $db->commit();

            // Update MEMCACHE
            updateSchoolData($schoolId, SCHOOL_CONFIG);
            return_json();
            break;
        default:
            _error(400);
            break;
    }

} catch (Exception $e) {
    $db->rollback();
	return_json( array('error' => true, 'message' => $e->getMessage()) );
} finally {
    $db->autocommit(true);
}

?>