<?php
/**
 * Package: ajax/ci/bo
 *
 * @package ConIu
 * @author QuanND
 */

// fetch bootstrap
require('../../../../../bootstrap.php');

// check AJAX Request
user_access();
$nogaRole = $user->_data['user_group'];
if ((!$user->_is_admin) && ($nogaRole != USER_NOGA_MANAGE_ALL) && ($nogaRole != USER_NOGA_MANAGE_CITY)
    && ($nogaRole != USER_NOGA_MANAGE_SCHOOL) && ($nogaRole != USER_MOD)){
    _error(__('System Message'), __("You don't have the right permission to access this"));
}
include_once(DAO_PATH.'dao_user.php');
$userDao = new UserDAO();
include_once(DAO_PATH.'dao_class_level.php');
$classLevelDao = new ClassLevelDAO();
include_once(DAO_PATH.'dao_user_manage.php');
$userManDao = new UserManageDAO();
include_once(DAO_PATH.'dao_child.php');
$childDao = new ChildDAO();
include_once(DAO_PATH.'dao_teacher.php');
$teacherDao = new TeacherDAO();
include_once(DAO_PATH.'dao_region_manage.php');
$regionManageDao = new RegionManageDAO();

try {
    include_once(DAO_PATH.'dao_school.php');
    $schoolDao = new SchoolDAO();
    $return = array();

    $db->autocommit(false);
    switch ($_POST['do']) {
        case 'add_region_user':
            $db->begin_transaction();

            $args = array();
            // Kiểm tra có gửi lên thông tin
            // Kiểm tra xem có được set người quản lý không
            if(isset($_POST['user_ids']) && (count($_POST['user_ids']) > 0)) {
                // Trường hợp có gửi lên user_id
                $args['user_id'] = $_POST['user_ids'][0];
            } else {
                // Tạo user mới làm quản lý
                $user_arr = array();
                $user_arr['first_name'] = 'Inet';
                $user_arr['last_name'] = 'User';
                $user_arr['password'] = '123456';
                $user_arr['gender'] = 'female';
                $user_arr['full_name'] = $user_arr['last_name']." ".$user_arr['first_name'];
                // Nếu không gửi lên user_id, kiểm tra xem có gửi lên search-manager không
                if(isset($_POST['search-manager']) && trim($_POST['search-manager'] != null)) {
                    // Kiểm tra xem có phải là 1 email hay không?
                    if(valid_email($_POST['search-manager'])) {
                        // Tạo tài khoản quản lý với $_POST['search-manager'] là email
                        $user_arr['email'] = trim($_POST['search-manager']);
                        $user_arr['username'] = generateUsername($user_arr['full_name']);
                    } else {
                        // Tạo tài khoản quản lý với $_POST['search-manager'] là username
                        $user_arr['email'] = '';
                        $user_arr['username'] = trim($_POST['search-manager']);
                    }
                } else {
                    $user_arr['email'] = generateEmail();
                    $user_arr['username'] = generateUsername($user_arr['full_name']);
                }

                // 1. Tạo user mới
                $userId = $userDao->createUser($user_arr);
                $args['user_id'] = $userId;
            }

            // Lấy các giá trị post lên khác
            $args['city_id'] = $_POST['city_id'];
            $args['district_slug'] = $_POST['district_slug'];
            $args['provider'] = 1; // Tài khoản do Noga cấp
            $args['grade'] = 'null';
            $args['role'] = 1;

            // Thêm vào bảng ci_region_manage
            $regionManageDao->createRegionManage($args);

            $db->commit();
            return_json();
            break;

        case 'delete':
            $db->begin_transaction();

            $region_manage_id = $_POST['id'];

            // Delete user quản lý dùng
            $regionManageDao->deleteRegionManage($region_manage_id);

            $db->commit();
            return_json();
            break;
        default:
            _error(400);
            break;
    }

} catch (Exception $e) {
    $db->rollback();
    return_json( array('error' => true, 'message' => $e->getMessage()) );
} finally {
    $db->autocommit(true);
}

?>