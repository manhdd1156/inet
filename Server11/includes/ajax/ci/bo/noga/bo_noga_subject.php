<?php
/**
 * Package: ajax/ci/bo
 * 
 * @package ConIu
 * @author ManhDD
 * ADD NEW BY MANHDD 03/04/2021
 */

// fetch bootstrap
require('../../../../../bootstrap.php');

// check AJAX Request
user_access();
$nogaRole = $user->_data['user_group'];
if ((!$user->_is_admin) && ($nogaRole != USER_NOGA_MANAGE_ALL) && ($nogaRole != USER_NOGA_MANAGE_CITY)
            && ($nogaRole != USER_NOGA_MANAGE_SCHOOL) && ($nogaRole != USER_MOD)){
    _error(__('System Message'), __("You don't have the right permission to access this"));
}
include_once(DAO_PATH.'dao_user.php');
$userDao = new UserDAO();
//include_once(DAO_PATH.'dao_class_level.php');
//$classLevelDao = new ClassLevelDAO();
//include_once(DAO_PATH.'dao_user_manage.php');
//$userManDao = new UserManageDAO();
//include_once(DAO_PATH.'dao_child.php');
//$childDao = new ChildDAO();
//include_once(DAO_PATH.'dao_teacher.php');
//$teacherDao = new TeacherDAO();

try {

    include_once(DAO_PATH.'dao_subject.php');
    $subjectDao = new SubjectDAO();
    $return = array();

    $db->autocommit(false);
    switch ($_POST['do']) {
        case 'add':
            $db->begin_transaction();

            $args = array();
            $args['subject_name'] = $_POST['subject_name'];
            $args['re_exam'] = (isset($_POST['re_exam']) && $_POST['re_exam'] != null) ? 1 : 0;

            // Kiểm tra đã tồn tại bản ghi chưa
            $subject_exist = $subjectDao->getSubjectByName($args['subject_name']);
            if(isset($subject_exist))  throw new Exception(__("The record already exists. Please try again later"));
            // Insert vào pages
            $args['subject_id'] = $subjectDao->createSubjectNoga($args);

            $db->commit();

            updateSubjectData($args['subject_id'], SCHOOL_SUBJECTS);
            /* END - Memcached */
            return_json(array('callback' => 'window.location = "'.$system['system_url'].'/noga/subjects'.'";'));
            break;
        case 'delete':

//            return_json(array('callback' => 'window.location = "'.$system['system_url'].'/noga/subjects'.'";'));
//            if(confirm('Are you sure you want to delete this?')) {
//                echo('test');
//                echo('test');
//            }
            $db->begin_transaction();
            if(!isset($_POST['subject_id']) || !is_numeric($_POST['subject_id'])) {
                _error(400);
            }
            $subjectDao->deleteSubjectNoga($_POST['subject_id']);

            $db->commit();
            return_json(array('success' => true, 'message' => __("Success")));
            break;
        case  'edit':
            $db->begin_transaction();
            if(!isset($_POST['subject_id']) || !is_numeric($_POST['subject_id'])) {
                _error(400);
            }
            $args = array();

            $subject = null;
            if (($nogaRole == USER_NOGA_MANAGE_CITY) || ($nogaRole == USER_NOGA_MANAGE_SCHOOL)) {
                $subject = $subjectDao->getSubjectNoga($_POST['subject_id']);
            } else {
                //$school = $schoolDao->getSchoolById($_POST['page_id']);
                $subject = getSubjectData($_POST['subject_id'], SUBJECT_INFO);
            }
            if (is_null($subject)) {
                _error(403);
            }
            $args['subject_id'] = $_POST['subject_id'];
            $args['subject_name'] = $_POST['subject_name'];
            $args['re_exam'] = (isset($_POST['re_exam']) && $_POST['re_exam'] != null) ? 1 : 0;

            $subjectDao->editSubjectNoga($args);

            $db->commit();

            updateSubjectData($args['subject_id']);
//            return_json(array('callback' => 'window.location = "'.$system['system_url'].'/noga/subjects'.'";'));
            return_json(array('success' => true, 'message' => __("Success")));

            break;
        default:
            _error(400);
            break;
    }

} catch (Exception $e) {
    $db->rollback();
	return_json( array('error' => true, 'message' => $e->getMessage()) );
} finally {
    $db->autocommit(true);
}

?>