<?php
/**
 * Package: ajax/ci/bo
 * 
 * @package ConIu
 * @author QuanND
 */

// fetch bootstrap
require('../../../../../bootstrap.php');

// check AJAX Request
user_access();
if(!$user->_logged_in || !$user->_is_admin) {
    _error(__('System Message'), __("You don't have the right permission to access this"));
}
include_once(DAO_PATH.'dao_noga_notification.php');
$nogaNotificationDao = new NogaNotificationDAO();

try {

    $db->autocommit(false);
    switch ($_POST['do']) {
        case 'add':
            if (!isset($_POST['cb_type_user']) && !isset($_POST['cb_area']) && !isset($_POST['cb_gender']) && !isset($_POST['cb_age'])) {
                throw new Exception(__("Phải chọn đối tượng gửi thông báo"));
            }
            $args['time'] = $_POST['push_time'];
            $args['type_user'] = isset($_POST['cb_type_user']) ? $_POST['type_user'] : 0;

            //$cb_area = $_POST['cb_area'];
            $args['city_id'] = isset($_POST['cb_area']) ? $_POST['city_id'] : 0;
            $args['district_slug'] = isset($_POST['cb_area']) ? $_POST['district_slug'] : '';

            //$cb_gender = $_POST['cb_gender'];
            $args['gender'] = isset($_POST['cb_gender']) ? $_POST['gender'] : 0;

            //$cb_age = $_POST['cb_age'];
            $args['from_age'] = isset($_POST['cb_age']) ? $_POST['from_age'] : 0;
            $args['to_age'] = isset($_POST['cb_age'])? $_POST['to_age'] : 0;

            $args['is_repeat'] = (isset($_POST['is_repeat']) && $_POST['is_repeat'] == 'on') ? 1 : 0;
            $args['repeat_time'] = ($args['is_repeat']) ? $_POST['repeat_time'] : 0;

            $args['type'] = is_empty($_POST['link']) ? 0 : 1;
            $args['title'] = $_POST['title'];
            $args['content'] = $_POST['content'];
            $args['link'] = $_POST['link'];
            $args['description'] = $_POST['description'];

            $nogaNotificationDao->createNotification($args);
            $db->commit();
            return_json(array('success' => true, 'message' => __("Done, Notification has been created")));
            break;

        case 'edit':
            if (!isset($_POST['cb_type_user']) && !isset($_POST['cb_area']) && !isset($_POST['cb_gender']) && !isset($_POST['cb_age'])) {
                throw new Exception(__("Phải chọn đối tượng gửi thông báo"));
            }
            $notification = $nogaNotificationDao->getNotification($_POST['notification_id']);
            if (is_null($notification)) {
                _error(404);
            }
            $args['noga_notification_id'] = $_POST['notification_id'];
            $args['time'] = $_POST['push_time'];
            $args['type_user'] = isset($_POST['cb_type_user']) ? $_POST['type_user'] : 0;

            //$cb_area = $_POST['cb_area'];
            $args['city_id'] = isset($_POST['cb_area']) ? $_POST['city_id'] : 0;
            $args['district_slug'] = isset($_POST['cb_area']) ? $_POST['district_slug'] : '';

            //$cb_gender = $_POST['cb_gender'];
            $args['gender'] = isset($_POST['cb_gender']) ? $_POST['gender'] : 0;

            //$cb_age = $_POST['cb_age'];
            $args['from_age'] = isset($_POST['cb_age']) ? $_POST['from_age'] : 0;
            $args['to_age'] = isset($_POST['cb_age'])? $_POST['to_age'] : 0;

            $args['is_repeat'] = (isset($_POST['is_repeat']) && $_POST['is_repeat'] == 'on') ? 1 : 0;
            $args['repeat_time'] = ($args['is_repeat']) ? $_POST['repeat_time'] : 0;

            $args['type'] = is_empty($_POST['link']) ? 0 : 1;
            $args['title'] = $_POST['title'];
            $args['content'] = $_POST['content'];
            $args['link'] = $_POST['link'];
            $args['description'] = $_POST['description'];

            $nogaNotificationDao->updateNotification($args);
            $db->commit();
            return_json(array('success' => true, 'message' => __("Done, Notification has been updated")));
            break;

        case 'delete':
            $nogaNotificationDao->deleteNotification($_POST['notification_id']);
            $db->commit();
            return_json(array('callback' => 'window.location = "'.$system['system_url'].'/noga/notifications/list'.'";'));
            break;

        default:
            _error(400);
            break;
    }

} catch (Exception $e) {
    $db->rollback();
	return_json( array('error' => true, 'message' => $e->getMessage()) );
} finally {
    $db->autocommit(true);
}

?>