<?php
/**
 * Package: ajax/ci/bo
 *
 * @package ConIu
 * @author TaiLA
 */

// fetch bootstrap
require('../../../../../bootstrap.php');

// check AJAX Request
user_access();
$nogaRole = $user->_data['user_group'];
if ((!$user->_is_admin) && ($nogaRole != USER_NOGA_MANAGE_ALL) && ($nogaRole != USER_NOGA_MANAGE_CITY)
    && ($nogaRole != USER_NOGA_MANAGE_SCHOOL) && ($nogaRole != USER_MOD)){
    _error(__('System Message'), __("You don't have the right permission to access this"));
}
include_once(DAO_PATH.'dao_user.php');
$userDao = new UserDAO();

include_once(DAO_PATH.'dao_foetus_knowledge.php');
$foetusKnowledgeDao = new FoetusKnowledgeDAO();

include_once(DAO_PATH.'dao_user_manage.php');
$userManDao = new UserManageDAO();

try {

    $db->autocommit(false);
    switch ($_POST['do']) {
        case 'add':
            $db->begin_transaction();

            // 2. Insert vào bảng ci_foetus_info
            $titles = array();
            $titles = $_POST['title'];

            $links = array();
            $links = $_POST['links'];

            $foetusKnowledgeDao->insertFoetusKnowledge($titles, $links);

            $db->commit();

            return_json(array('success' => true, 'message' => __("Done, Foetus development information has been created")));

            break;
        case 'edit':
            $db->begin_transaction();
            if(!isset($_POST['foetus_knowledge_id']) || !is_numeric($_POST['foetus_knowledge_id'])) {
                _error(404);
            }
            $args = array();
            $args['foetus_knowledge_id'] = $_POST['foetus_knowledge_id'];
            $args['title'] = $_POST['title'];
            $args['link'] = $_POST['link'];

            $foetusKnowledgeDao->updateFoetusKnowledge($args);
            $db->commit();

            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/noga/foetusknowledges";'));
            break;
        case 'delete':
            if(!isset($_POST['id']) || !is_numeric($_POST['id'])) {
                _error(404);
            }
            $db->begin_transaction();

            $foetusKnowledgeDao->deleteFoetusKnowledge($_POST['id']);

            $db->commit();

            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/noga/foetusknowledges";'));
            break;
        default:
            _error(400);
            break;
    }

} catch (Exception $e) {
    $db->rollback();
    return_json( array('error' => true, 'message' => $e->getMessage()) );
} finally {
    $db->autocommit(true);
}

?>