<?php
/**
 * Package: ajax/ci/bo
 *
 * @package ConIu
 * @author TaiLA
 */

// fetch bootstrap
require('../../../../../bootstrap.php');

// check AJAX Request
user_access();
$nogaRole = $user->_data['user_group'];
if ((!$user->_is_admin) && ($nogaRole != USER_NOGA_MANAGE_ALL) && ($nogaRole != USER_NOGA_MANAGE_CITY)
    && ($nogaRole != USER_NOGA_MANAGE_SCHOOL) && ($nogaRole != USER_MOD)){
    _error(__('System Message'), __("You don't have the right permission to access this"));
}
include_once(DAO_PATH.'dao_user.php');
$userDao = new UserDAO();
try {
    $return = array();

    $db->autocommit(false);
    switch ($_POST['do']) {
        case 'search':
            // Tìm kiếm page có bài đăng trong khoảng thời gian
            $fromDate = $_POST['from_date'];
            $toDate = $_POST['to_date'];
            if(!validateDate($fromDate) || !validateDate($toDate)) {
                throw new Exception(__("Vui lòng nhập thời gian đúng định dạnh"));
            }

            $fromDate = toDBDate($fromDate);
            $toDate = toDBDate($toDate);
            $strSql = sprintf("SELECT P.page_id, P.page_name, P.page_title, page_picture, page_likes, PO.time FROM pages P INNER JOIN posts PO ON
                    P.page_id = PO.user_id AND PO.user_type = 'page'
                    WHERE DATE(PO.time) >= %s AND DATE(PO.time) <= %s 
                    ORDER BY PO.time DESC", secure($fromDate), secure($toDate));

            $get_rows = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
            $results = array();
            if($get_rows->num_rows > 0) {
                $pageIds = array();
                while($row = $get_rows->fetch_assoc()) {
                    if(!in_array($row['page_id'], $pageIds)) {
                        $row['page_picture'] = User::get_picture($row['page_picture'], 'page');
                        $row['time'] = toSysDatetime($row['time']);
                        $results[] = $row;
                        $pageIds[] = $row['page_id'];
                    }
                }
            }
            /* assign variables */
            $smarty->assign('results', $results);
            /* return */
            $return['results'] = $smarty->fetch("ci/noga/ajax.noga.page.list.tpl");

            return_json($return);
            break;
        default:
            _error(400);
            break;
    }

} catch (Exception $e) {
    $db->rollback();
    return_json( array('error' => true, 'message' => $e->getMessage()) );
} finally {
    $db->autocommit(true);
}

?>