<?php
/**
 * Package: ajax/ci/bo
 *
 * @package ConIu
 * @author QuanND
 */

// fetch bootstrap
require('../../../../../bootstrap.php');

// check AJAX Request
user_access();
$nogaRole = $user->_data['user_group'];
if ((!$user->_is_admin) && ($nogaRole != USER_NOGA_MANAGE_ALL) && ($nogaRole != USER_NOGA_MANAGE_CITY)
    && ($nogaRole != USER_NOGA_MANAGE_SCHOOL) && ($nogaRole != USER_MOD)){
    _error(__('System Message'), __("You don't have the right permission to access this"));
}
include_once(DAO_PATH.'dao_user.php');
$userDao = new UserDAO();
include_once(DAO_PATH.'dao_school.php');
$schoolDao = new SchoolDAO();
include_once(DAO_PATH.'dao_role.php');
$roleDao = new RoleDAO();
include_once(DAO_PATH.'dao_teacher.php');
$teacherDao = new TeacherDAO();
try {
    $return = array();

    $db->autocommit(false);
    switch ($_POST['do']) {
        case 'search_user_new':
            // Tìm kiếm user đăng ký mới theo thời gian nhập vào
            $fromDate = $_POST['from_date'];
            $toDate = $_POST['to_date'];
            if(!validateDate($fromDate) || !validateDate($toDate)) {
                throw new Exception(__("Vui lòng nhập thời gian đúng định dạnh"));
            }
            // Search user mới đăng ký (theo fromDate và toDate)
            $results = $userDao->getUserRegistered($fromDate, $toDate);
            if (count($results) > 0) {
                /* assign variables */
                $smarty->assign('results', $results);
                /* return */
                $return['results'] = $smarty->fetch("ci/noga/ajax.noga.usernews.list.tpl");
            }
            return_json($return);
            break;
        case 'search_role_user':
            // Tìm kiếm user quản lý và giáo viên
            $school_role = $_POST['school_role'];
            $school_status = $_POST['school_status'];

            // Lấy danh sách trường theo status
            $schools = $schoolDao->getAllSchoolsUsingInet($school_status);

            $results = array();
            // Lặp danh sách trường, lấy ra thông tin quản lý hoặc giáo viên
            foreach ($schools as $school) {
                $manageIds = array();
                if($school_role == 1) {
                    // Lấy danh sách quản lý
                    $manageIds = $roleDao->getAllUserManageSchool($school['page_id']);

                    // Lấy danh sách hiệu trưởng
                    $principals = array();
                    if(!is_empty($school['principal'])) {
                        $principals = explode(',', $school['principal']);
                        foreach ($principals as $row) {
                            $manageIds[] = $row;
                        }
                    }
                    // Thêm tài khoản admin vào danh sách
                    $manageIds[] = $school['page_admin'];
                } else {
                    // Lấy danh sách giáo viên
                    $manageIds = $teacherDao->getTeacherIDOfSchool($school['page_id']);
                }
                $manageIds = array_unique($manageIds);
                $users = array();
                if(count($manageIds) > 0) {
                    // Lấy danh sách user
                    $users = $userDao->getUsersFullDetail($manageIds);
                }
                $school['users'] = $users;
                if(!(count($manageIds) == 1 && (in_array(416, $manageIds) || in_array(417, $manageIds)))) {
                    $results[] = $school;
                }
            }
            if (count($results) > 0) {
                /* assign variables */
                $smarty->assign('results', $results);
                /* return */
                $return['results'] = $smarty->fetch("ci/noga/ajax.noga.manageandteacher.list.tpl");
            }
            return_json($return);
            break;
        case 'export':
            $reportTemplate = ABSPATH."content/templates/manage_and_teacher_A4.xlsx";
            include_once(ABSPATH . 'includes/libs/PHPExcel/PHPExcel/IOFactory.php');
            include_once(ABSPATH . 'includes/libs/PHPExcel/PHPExcel.php');

            $objPHPExcel = PHPExcel_IOFactory::load($reportTemplate);

            $all_border_style = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('rgb' => '333333'))));

            // Tìm kiếm user quản lý và giáo viên
            $school_role = $_POST['school_role'];
            $school_status = $_POST['school_status'];

            // Lấy danh sách trường theo status
            $schools = $schoolDao->getAllSchoolsUsingInet($school_status);

            $results = array();
            // Lặp danh sách trường, lấy ra thông tin quản lý hoặc giáo viên
            foreach ($schools as $school) {
                $manageIds = array();
                if($school_role == 1) {
                    // Lấy danh sách quản lý
                    $manageIds = $roleDao->getAllUserManageSchool($school['page_id']);

                    // Lấy danh sách hiệu trưởng
                    $principals = array();
                    if(!is_empty($school['principal'])) {
                        $principals = explode(',', $school['principal']);
                        foreach ($principals as $row) {
                            $manageIds[] = $row;
                        }
                    }
                    // Thêm tài khoản admin vào danh sách
                    $manageIds[] = $school['page_admin'];
                } else {
                    // Lấy danh sách giáo viên
                    $manageIds = $teacherDao->getTeacherIDOfSchool($school['page_id']);
                }
                $manageIds = array_unique($manageIds);
                $users = array();
                if(count($manageIds) > 0) {
                    // Lấy danh sách user
                    $users = $userDao->getUsersFullDetail($manageIds);
                }
                $school['users'] = $users;
                if(!(count($manageIds) == 1 && (in_array(416, $manageIds) || in_array(417, $manageIds)))) {
                    $results[] = $school;
                }
            }
            if($school_role == 1) {
                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A1', mb_strtoupper(convertText4Web(__("Manage list")),"UTF-8"));
            } else {
                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A1', mb_strtoupper(convertText4Web(__("Teacher list")),"UTF-8"));
            }
            // content
            $rowNumber = 4;
            foreach ($results as $school) {
                $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A' . $rowNumber . ':F' . $rowNumber);
                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $rowNumber, convertText4Web($school['page_title']) . ' (' . count($school['users']) . ')');
                $objPHPExcel->getActiveSheet()->getStyle('A' . $rowNumber)->applyFromArray(
                    styleArray('Times new Roman', true, false, 11, PHPExcel_Style_Alignment::HORIZONTAL_LEFT, PHPExcel_Style_Alignment::VERTICAL_CENTER, true));
                $rowNumber++;
                if(count($school['users']) > 0) {
                    $idx = 1;
                    foreach ($school['users'] as $row) {
                        if($row['user_gender'] == 'male') {
                            $gender = __("Male");
                        } else $gender = __("Female");
                        $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue('A' . $rowNumber, $idx)
                            ->setCellValue('B' . $rowNumber, convertText4Web($row['user_fullname']))
                            ->setCellValue('C' . $rowNumber, convertText4Web($row['user_email']))
                            ->setCellValue('D' . $rowNumber, convertText4Web($row['user_phone']))
                            ->setCellValue('E' . $rowNumber, convertText4Web($row['user_birthdate']))
                            ->setCellValue('F' . $rowNumber, convertText4Web($gender));
                        $objPHPExcel->getActiveSheet()->getStyle('A' . $rowNumber)->applyFromArray(
                            styleArray('Times new Roman', true, false, 11, PHPExcel_Style_Alignment::HORIZONTAL_CENTER, PHPExcel_Style_Alignment::VERTICAL_CENTER, true));
                        $rowNumber++;
                        $idx++;
                    }
                }
                $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A' . $rowNumber . ':F' . $rowNumber);
                $rowNumber++;
            }
            $rowEnd = $rowNumber - 1;
            $objPHPExcel->getActiveSheet()->getStyle('A4:F' . $rowEnd)->applyFromArray($all_border_style);
            $objPHPExcel->getActiveSheet()->getStyle('B4:F' . $rowEnd)->applyFromArray(
                styleArray('Times new Roman', false, false, 11, PHPExcel_Style_Alignment::HORIZONTAL_LEFT, PHPExcel_Style_Alignment::VERTICAL_CENTER, true));
            $objPHPExcel->getActiveSheet()->getStyle('E4:F' . $rowEnd)->applyFromArray(
                styleArray('Times new Roman', false, false, 11, PHPExcel_Style_Alignment::HORIZONTAL_CENTER, PHPExcel_Style_Alignment::VERTICAL_CENTER, true));
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            ob_start();

            $file_name = convertText4Web('danh_sach_quan_ly_hoac_giao_vien.xlsx');

            $file_name = convert_vi_to_en($file_name);
            $file_name = str_replace(" ",'_', $file_name);

            $objWriter->save("php://output");

            $xlsData = ob_get_contents();
            ob_end_clean();

            return_json(array(
                'file_name' => $file_name,
                'file' => "data:application/vnd.ms-excel;base64,".base64_encode($xlsData)
            ));
            break;
        case 'export_user_new':
            $reportTemplate = ABSPATH."content/templates/user_new_A4.xlsx";
            include_once(ABSPATH . 'includes/libs/PHPExcel/PHPExcel/IOFactory.php');
            include_once(ABSPATH . 'includes/libs/PHPExcel/PHPExcel.php');

            $objPHPExcel = PHPExcel_IOFactory::load($reportTemplate);

            $all_border_style = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('rgb' => '333333'))));

            // Tìm kiếm user đăng ký mới theo thời gian nhập vào
            $fromDate = $_POST['from_date'];
            $toDate = $_POST['to_date'];
            if(!validateDate($fromDate) || !validateDate($toDate)) {
                throw new Exception(__("Vui lòng nhập thời gian đúng định dạnh"));
            }
            // Search user mới đăng ký (theo fromDate và toDate)
            $results = $userDao->getUserRegistered($fromDate, $toDate);
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'Danh sách user mới' . ' (' . count($results) . ')');

            // content
            $rowNumber = 4;
            $idx = 1;
            foreach ($results as $row) {
                if($row['user_gender'] == 'male') {
                    $gender = __("Male");
                } else $gender = __("Female");
                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $rowNumber, $idx)
                    ->setCellValue('B' . $rowNumber, convertText4Web($row['user_fullname']))
                    ->setCellValue('C' . $rowNumber, $row['user_name'])
                    ->setCellValue('D' . $rowNumber, convertText4Web($row['user_email']))
                    ->setCellValue('E' . $rowNumber, convertText4Web($row['user_phone']))
                    ->setCellValue('F' . $rowNumber, convertText4Web($row['user_birthdate']))
                    ->setCellValue('G' . $rowNumber, convertText4Web($gender))
                    ->setCellValue('H' . $rowNumber, convertText4Web($row['user_registered']))
                    ->setCellValue('I' . $rowNumber, convertText4Web($row['user_last_login']))
                    ->setCellValue('J' . $rowNumber, convertText4Web($row['user_last_active']));
                $objPHPExcel->getActiveSheet()->getStyle('A' . $rowNumber)->applyFromArray(
                    styleArray('Times new Roman', true, false, 11, PHPExcel_Style_Alignment::HORIZONTAL_CENTER, PHPExcel_Style_Alignment::VERTICAL_CENTER, true));
                $rowNumber++;
                $idx++;
            }
            $rowEnd = $rowNumber - 1;
            $objPHPExcel->getActiveSheet()->getStyle('A4:J' . $rowEnd)->applyFromArray($all_border_style);
            $objPHPExcel->getActiveSheet()->getStyle('A4:A' . $rowEnd)->applyFromArray(
                styleArray('Times new Roman', true, false, 11, PHPExcel_Style_Alignment::HORIZONTAL_CENTER, PHPExcel_Style_Alignment::VERTICAL_CENTER, true));
            $objPHPExcel->getActiveSheet()->getStyle('B4:J' . $rowEnd)->applyFromArray(
                styleArray('Times new Roman', false, false, 11, PHPExcel_Style_Alignment::HORIZONTAL_LEFT, PHPExcel_Style_Alignment::VERTICAL_CENTER, true));
            $objPHPExcel->getActiveSheet()->getStyle('F4:J' . $rowEnd)->applyFromArray(
                styleArray('Times new Roman', false, false, 11, PHPExcel_Style_Alignment::HORIZONTAL_CENTER, PHPExcel_Style_Alignment::VERTICAL_CENTER, true));
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            ob_start();

            $file_name = convertText4Web('danh_sach_user_moi.xlsx');

            $file_name = convert_vi_to_en($file_name);
            $file_name = str_replace(" ",'_', $file_name);

            $objWriter->save("php://output");

            $xlsData = ob_get_contents();
            ob_end_clean();

            return_json(array(
                'file_name' => $file_name,
                'file' => "data:application/vnd.ms-excel;base64,".base64_encode($xlsData)
            ));
            break;
        default:
            _error(400);
            break;
    }

} catch (Exception $e) {
    $db->rollback();
    return_json( array('error' => true, 'message' => $e->getMessage()) );
} finally {
    $db->autocommit(true);
}

?>