<?php
/**
 * Package: ajax/ci/bo
 * 
 * @package ConIu
 * @author QuanND
 */

// fetch bootstrap
require('../../../../../bootstrap.php');

// check AJAX Request
user_access();
$nogaRole = $user->_data['user_group'];
if ((!$user->_is_admin) && ($nogaRole != USER_NOGA_MANAGE_ALL) && ($nogaRole != USER_NOGA_MANAGE_CITY)
            && ($nogaRole != USER_NOGA_MANAGE_SCHOOL) && ($nogaRole != USER_MOD)){
    _error(__('System Message'), __("You don't have the right permission to access this"));
}
include_once(DAO_PATH.'dao_statistic.php');
$statisticDao = new StatisticDAO();

try {
    include_once(DAO_PATH.'dao_school.php');
    $schoolDao = new SchoolDAO();
    $return = array();

    $db->autocommit(false);
    switch ($_POST['do']) {
        case 'search_interactive_school':
            $schoolId = $_POST['school_id'];
            $fromDate = $_POST['fromDate'];
            $toDate = $_POST['toDate'];

            // get results
            //$statistic_for_noga = $statisticDao->getSchoolInteractive($schoolId, $fromDate, $toDate);

            $statistic = getStatisticsInteractive($schoolId, ALL, $fromDate, $toDate);

            /* assign variables */
            $smarty->assign('statistic_for_noga', $statistic_for_noga);
            $smarty->assign('statistic', $statistic);
            $smarty->assign('keys', $statistic_keys);
            /* return */
            $return['result_noga'] = $smarty->fetch("ci/noga/ajax.noga.statistic.school.tpl");

//            $statistic_for_school = getSchoolStatistics($schoolId, ALL, $fromDate, $toDate);
//            /* assign variables */
//            $smarty->assign('statistic_for_school', $statistic_for_school);
            /* return */
            $return['result_school'] = $smarty->fetch("ci/noga/ajax.noga.statistic.parent.tpl");
            return_json($return);
            break;

        case 'search_interactive_topic':
            $fromDate = $_POST['fromDate'];
            $toDate = $_POST['toDate'];

            // get results
            $results = $statisticDao->getTopicInteractive($fromDate, $toDate);
            /* assign variables */
            $smarty->assign('rows', $results);
            /* return */
            $return['results'] = $smarty->fetch("ci/noga/ajax.noga.statistic.topic.tpl");
            return_json($return);
            break;
        case 'search_interactive':
            $fromDate = $_POST['fromDate'];
            $toDate = $_POST['toDate'];

            $schools = $statisticDao->getAllSchoolStatisticsForNoga();
            $result = array();
            $schoolIds = array();
            foreach ($schools as $row) {
                $schoolIds[] = $row['page_id'];
            }
            $schoolIds[] = 0;
            foreach ($schoolIds as $schoolId) {
                $statistic = getStatisticsInteractive($schoolId, ALL, $fromDate, $toDate);
                foreach ($statistic_keys as $key) {
                    $result[$key]['school_views'] = $result[$key]['school_views'] + $statistic[$key]['school_views'];
                    $result[$key]['parent_views'] = $result[$key]['parent_views'] + $statistic[$key]['parent_views'];
                    $result[$key]['school_createds'] = $result[$key]['school_createds'] + $statistic[$key]['school_createds'];
                    $result[$key]['parent_createds'] = $result[$key]['parent_createds'] + $statistic[$key]['parent_createds'];
                }
            }

            /* assign variables */
            $smarty->assign('statistic', $result);
            $smarty->assign('keys', $statistic_keys);
            /* return */
            $return['result_noga'] = $smarty->fetch("ci/noga/ajax.noga.statistic.school.tpl");

//            $statistic_for_school = getSchoolStatistics($schoolId, ALL, $fromDate, $toDate);
//            /* assign variables */
//            $smarty->assign('statistic_for_school', $statistic_for_school);
            /* return */
            $return['result_school'] = $smarty->fetch("ci/noga/ajax.noga.statistic.parent.tpl");
            return_json($return);
            break;
        case 'search_useronline':
            $fromDate = $_POST['fromDate'];
            $toDate = $_POST['toDate'];

            $useronline = getUserOnline($fromDate, $toDate);
            /* assign variables */
            $smarty->assign('useronline', $useronline);
            /* return */
            $return['result'] = $smarty->fetch("ci/noga/ajax.noga.statistic.useronline.tpl");

            return_json($return);
            break;

        default:
            _error(400);
            break;
    }

} catch (Exception $e) {
    $db->rollback();
	return_json( array('error' => true, 'message' => $e->getMessage()) );
} finally {
    $db->autocommit(true);
}

?>