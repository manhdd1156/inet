<?php
/**
 * Package: ajax/ci/bo
 *
 * @package Inet
 * @author TaiLA
 */

// fetch bootstrap
require('../../../../../bootstrap.php');

// check AJAX Request
user_access();
$nogaRole = $user->_data['user_group'];
if ((!$user->_is_admin) && ($nogaRole != USER_NOGA_MANAGE_ALL) && ($nogaRole != USER_NOGA_MANAGE_CITY)
    && ($nogaRole != USER_NOGA_MANAGE_SCHOOL) && ($nogaRole != USER_MOD)){
    _error(__('System Message'), __("You don't have the right permission to access this"));
}
include_once(DAO_PATH.'dao_user.php');
$userDao = new UserDAO();

include_once(DAO_PATH.'dao_childmonth.php');
$childMonthDao = new ChildMonthDAO();

include_once(DAO_PATH.'dao_user_manage.php');
$userManDao = new UserManageDAO();

try {

    $db->autocommit(false);
    switch ($_POST['do']) {
        case 'add':
            $db->begin_transaction();

            // 2. Insert vào bảng ci_foetus_info
            $months = array();
            $months = $_POST['months'];

            $links = array();
            $links = $_POST['links'];

            $titles = array();
            $titles = $_POST['titles'];

            $childMonthDao->insertChildMonth($months, $links, $titles);

            $db->commit();

            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/noga/childmonths";'));

            break;
        case 'edit':
            $db->begin_transaction();
            if(!isset($_POST['child_based_on_month_id']) || !is_numeric($_POST['child_based_on_month_id'])) {
                _error(404);
            }
            $args = array();
            $args['child_based_on_month_id'] = $_POST['child_based_on_month_id'];
            $args['month'] = $_POST['month'];
            $args['link'] = $_POST['link'];
            $args['title'] = $_POST['title'];

            $childMonthDao->updateChildMonth($args);
            $db->commit();

            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/noga/childmonths";'));
            break;
        case 'delete':
            if(!isset($_POST['id']) || !is_numeric($_POST['id'])) {
                _error(404);
            }
            $db->begin_transaction();

            $childMonthDao->deleteChildMonth($_POST['id']);

            $db->commit();

            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/noga/childmonths";'));
            break;
        default:
            _error(400);
            break;
    }

} catch (Exception $e) {
    $db->rollback();
    return_json( array('error' => true, 'message' => $e->getMessage()) );
} finally {
    $db->autocommit(true);
}

?>