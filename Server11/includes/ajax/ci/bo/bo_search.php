<?php
/**
 * ajax -> data -> search
 * 
 * @package CINet v1.1
 * @author NOGA Co., Ltd
 */

// fetch bootstrap
require('../../../../bootstrap.php');

// check AJAX Request
is_ajax();

// check user logged in
if(!$user->_logged_in) {
    modal(LOGIN);
}

// check user activated
if($system['activation_enabled'] && !$user->_data['user_activated']) {
	modal(MESSAGE, __("Not Activated"), __("Before you can interact with other users, you need to confirm your email address"));
}

// valid inputs
//if(!isset($_POST['query']) && ($_POST['func'] != 'searchchild')) {
//	_error(400);
//}
// search
try {

	// initialize the return array
	$return = array();
    switch ($_POST['func']) {
        case 'user':
            include_once(DAO_PATH.'dao_user.php');
            $userDao = new UserDAO();
            // Danh sách user đã chọn thì ko chọn nữa
            $userIds = isset($_POST['user_ids']) ? $_POST['user_ids'] : array();
            $userIds = array_unique($userIds);
            $userIds[] = 0; //Set = 0 để có thể chọn quản lý làm gv
            //$userIds[] = $user->_data['user_id']; //Tính cả user hiện tại cũng ko được chọn

            // get results
            $results = $userDao->searchUser($_POST['query'], $userIds);
            if (count($results) > 0) {
                /* assign variables */
                $smarty->assign('results', $results);
                /* return */
                $return['results'] = $smarty->fetch("ci/ajax.search4teacher.tpl");
            }
            break;

        case 'parent':
            include_once(DAO_PATH.'dao_user.php');
            $userDao = new UserDAO();

            // Danh sách user đã chọn thì ko chọn nữa
            $userIds = isset($_POST['user_ids']) ? $_POST['user_ids'] : array();
            $userIds = array_unique($userIds);
            $userIds[] = 0; //Set bangwf 0 ddeer có thể chọn quản lý trường làm phụ huynh
            // $userIds[] = $user->_data['user_id']; //Tính cả user hiện tại cũng ko được chọn

            // get results
            $results = $userDao->searchUser($_POST['query'], $userIds);
            if (count($results) > 0) {
                /* assign variables */
                $smarty->assign('results', $results);
                /* return */
                $return['results'] = $smarty->fetch("ci/ajax.search4parent.tpl");
            }
            break;

        case 'teacher':
            include_once(DAO_PATH.'dao_teacher.php');
            $teacherDao = new TeacherDAO();

            // Danh sách user đã chọn thì ko chọn nữa
            $teacherIds = isset($_POST['user_ids']) ? $_POST['user_ids'] : array();
            $teacherIds = array_unique($teacherIds);
            $teacherIds[] = 0; //Set = 0 để có thể chọn quản lý làm gv
            //$teacherIds[] = $user->_data['user_id']; //Tính cả user hiện tại cũng ko được chọn

            $results = $teacherDao->searchTeacher($_POST['school_username'], $_POST['query'], $teacherIds);
            if (count($results) > 0) {
                /* assign variables */
                $smarty->assign('results', $results);
                /* return */
                $return['results'] = $smarty->fetch("ci/school/ajax.searchteacher.tpl");
            }
            break;
        case 'classlevel':
            include_once(DAO_PATH.'dao_class_level.php');
            $classLevelDao = new ClassLevelDAO();
            error_log('bo_search.php - 95 :$_POST[\'classlevel_ids\'] : '.$_POST['classlevel_ids']);
            // Danh sách khối đã chọn thì ko chọn nữa
            $classlevelIds = isset($_POST['classlevel_ids']) ? $_POST['classlevel_ids'] : array();
            $classlevelIds = array_unique($classlevelIds);
            error_log('bo_search.php - 99 :$classlevelIds : '.json_encode($classlevelIds));
            error_log('bo_search.php - 100 :$_POST[\'school_id\'] : '.$_POST['school_id'].';$_POST[\'query\']: '.$_POST['query']);
//            $results = $teacherDao->searchTeacher($_POST['school_username'], $_POST['query'], $teacherIds);
            $results = $classLevelDao->searchClassLevel($_POST['school_id'], $_POST['query'], $classlevelIds);
            error_log('bo_search.php - 103 :$results : '.json_encode($results));
            if (count($results) > 0) {
                /* assign variables */
                $smarty->assign('results', $results);
                /* return */
                $return['results'] = $smarty->fetch("ci/school/ajax.searchclasslevel.tpl");
            }
            break;
        case 'searchchild':
            include_once(DAO_PATH.'dao_child.php');
            $childDao = new ChildDAO();

            $school = getSchoolDataByUsername($_POST['school_username'], SCHOOL_DATA);
            $smarty->assign('school', $school);
            $smarty->assign('username', $_POST['school_username']);

            $isNewSearch = isset($_POST['is_new'])? $_POST['is_new']: 0;
            $condition = array();
            if ($isNewSearch) { //Nếu là lần search mới.
                //Đưa vào session để sau search lại.
                $condition['keyword'] = $_POST['keyword'];
                $condition['class_id'] = $_POST['class_id'];
                $condition['child_month'] = $_POST['child_month'];
                $condition['page'] = 1;
                $condition['school_id'] = $_POST['school_id'];
                $_SESSION[SESSION_KEY_SEARCH_CHILDREN] = $condition;
            } else {
                //Nếu click vào paging.
                $condition = $_SESSION[SESSION_KEY_SEARCH_CHILDREN];
                $condition['page'] = $_POST['page'];
                $_SESSION[SESSION_KEY_SEARCH_CHILDREN] = $condition;
            }

            $totalChildren = $childDao->searchCountHaveMonth($condition['keyword'], $_POST['school_id'], $condition['class_id'], $condition['child_month'], STATUS_ACTIVE);
            $children = $childDao->searchPagingHaveMonth($condition['keyword'], $_POST['school_id'], $condition['class_id'], $condition['child_month'], $condition['page'], PAGING_LIMIT);

            $result = array();
            $result['total'] = $totalChildren;
            $result['page_count'] = ceil(($totalChildren + 0.0)/PAGING_LIMIT);
            $result['children'] = $children;
            $result['page'] = $condition['page'];
            $result['class_id'] = $condition['class_id'];
            $result['child_month'] = $condition['child_month'];

            $smarty->assign('result', $result);
            $smarty->assign('canEdit', canEdit($_POST['school_username'], 'children'));

            $return['results'] = $smarty->fetch("ci/school/ajax.school.children.list.tpl");

            break;
        case 'searchchildstatistics':
            include_once(DAO_PATH.'dao_child.php');
            $childDao = new ChildDAO();

            $school = getSchoolDataByUsername($_POST['school_username'], SCHOOL_INFO);
            $smarty->assign('school', $school);
            $smarty->assign('username', $_POST['school_username']);

            $isNewSearch = (isset($_POST['is_new']) && $_POST['is_new'] == 1)? $_POST['is_new']: 0;
            $condition = array();
            if ($isNewSearch) { //Nếu là lần search mới.
                //Đưa vào session để sau search lại.
                $condition['class_id'] = $_POST['class_id'];
                $condition['page'] = 1;
                $condition['school_id'] = $_POST['school_id'];
                $_SESSION[SESSION_KEY_SEARCH_CHILDREN_STATISTICS] = $condition;
            } else {
                //Nếu click vào paging.
                $condition = $_SESSION[SESSION_KEY_SEARCH_CHILDREN_STATISTICS];
                $condition['page'] = $_POST['page'];
                $_SESSION[SESSION_KEY_SEARCH_CHILDREN_STATISTICS] = $condition;
            }

            $totalChildren = $childDao->searchCount('', $_POST['school_id'], $condition['class_id'], STATUS_ACTIVE);
            $children = $childDao->searchPaging('', $_POST['school_id'], $condition['class_id'], $condition['page'], PAGING_LIMIT, STATUS_ACTIVE);

            $childrenLast = array();
            foreach ($children as $row) {
                foreach ($row['parent'] as $k => $value) {
                    if (isset($value['suggest']) && $value['suggest'] == 1) {
                        unset($row['parent'][$k]);
                    }
                }
                $childrenLast[] = $row;
            }
            $childrenAll = array();
            if($condition['class_id'] > 0) {
                $childrenAll = getClassData($condition['class_id'], CLASS_CHILDREN);
            } else {
                $childrenAll = getSchoolData($school['page_id'], SCHOOL_CHILDREN);
            }
            $countParentActive = 0;
            foreach ($childrenAll as $child) {
                $parents = getChildData($child['child_id'], CHILD_PARENTS);
                if(count($parents) > 0) {
                    foreach ($parents as $parent) {
                        if($parent['user_last_active'] != '') {
                            $countParentActive++;
                            break;
                        }
                    }
                }
            }

            $result = array();
            $result['total'] = $totalChildren;
            $result['page_count'] = ceil(($totalChildren + 0.0)/PAGING_LIMIT);
            $result['children'] = $childrenLast;
            $result['page'] = $condition['page'];
            $result['class_id'] = $condition['class_id'];

            $smarty->assign('result', $result);
            $smarty->assign('countParentActive', $countParentActive);
            $smarty->assign('canView', canView($_POST['school_username'], 'loginstatistics'));

            $return['results'] = $smarty->fetch("ci/school/ajax.school.children.statistics.list.tpl");

            break;
        case 'searchchildleave':
            include_once(DAO_PATH.'dao_child.php');
            $childDao = new ChildDAO();
            include_once(DAO_PATH.'dao_tuition.php');
            $tuitionDao = new TuitionDAO();

            $school = getSchoolDataByUsername($_POST['school_username'], SCHOOL_INFO);
            $smarty->assign('school', $school);
            $smarty->assign('username', $_POST['school_username']);

            $isNewSearch = isset($_POST['is_new'])? $_POST['is_new']: 0;
            $condition = array();
            if ($isNewSearch) { //Nếu là lần search mới.
                //Đưa vào session để sau search lại.
                $condition['page'] = 1;
                $condition['school_id'] = $_POST['school_id'];
                $condition['class_id'] = $_POST['class_id'];
                $condition['keyword'] = $_POST['keyword'];
                $_SESSION[SESSION_KEY_SEARCH_CHILDREN_LEAVE] = $condition;
            } else {
                //Nếu click vào paging.
                $condition = $_SESSION[SESSION_KEY_SEARCH_CHILDREN_LEAVE];
                $condition['page'] = $_POST['page'];
//                $condition['school_id'] = $_POST['school_id'];
                $_SESSION[SESSION_KEY_SEARCH_CHILDREN_LEAVE] = $condition;
            }

            $result = array();
            $totalChildren = $childDao->searchCount($condition['keyword'], $school['page_id'], $condition['class_id'], STATUS_INACTIVE);
            //$totalChildren = getSchoolData($school['page_id'], SCHOOL_CHILDREN);

            $result['total'] = $totalChildren;
            $result['page_count'] = ceil(($totalChildren + 0.0)/PAGING_LIMIT);
            $result['class_id'] = $condition['class_id'];
            $children = $childDao->searchPaging($condition['keyword'], $school['page_id'], $condition['class_id'], $condition['page'], PAGING_LIMIT, STATUS_INACTIVE);
            // Lặp danh sách trẻ, kiểm tra xem trẻ đã có thông tin thanh toán học phí thôi học hay chưa
//        echo "<pre>";
//            print_r($children);
//        echo "</pre>";
//
//            die('qqq');
            $childs = array(); // Biến trung gian, đặt tạm tên như vậy (sai tiếng anh)
            foreach($children as $child) {
                $data = $tuitionDao->getLeaveTuitionDetail($school['page_id'], $child['child_id']);
                if(empty($data)) {
                    $child['is_leave_fee'] = 0;
                } else {
                    $child['is_leave_fee'] = 1;
                }
                $childs[] = $child;
            }
            $result['children'] = $childs;
            $result['page'] = $condition['page'];
            $smarty->assign('result', $result);
            $smarty->assign('canEdit', canEdit($_POST['school_username'], 'children'));
            $return['results'] = $smarty->fetch("ci/school/ajax.child.leaveschoollist.tpl");

            break;
        case 'searchchildnoga':
            include_once(DAO_PATH.'dao_child.php');
            $childDao = new ChildDAO();

            $school = getSchoolData($_POST['school_id'], SCHOOL_INFO);
            $smarty->assign('school', $school);
            $smarty->assign('username', $_POST['school_username']);

            $isNewSearch = isset($_POST['is_new'])? $_POST['is_new']: 0;
            $condition = array();
            if ($isNewSearch) { //Nếu là lần search mới.
                //Đưa vào session để sau search lại.
                $condition['keyword'] = $_POST['keyword'];
                $condition['class_id'] = $_POST['class_id'];
                $condition['page'] = 1;
                $_SESSION[SESSION_KEY_SEARCH_CHILDREN_NOGA] = $condition;
            } else {
                //Nếu click vào paging.
                $condition = $_SESSION[SESSION_KEY_SEARCH_CHILDREN_NOGA];
                $condition['page'] = $_POST['page'];
                $_SESSION[SESSION_KEY_SEARCH_CHILDREN_NOGA] = $condition;
            }

            $totalChildren = $childDao->searchCount($condition['keyword'], $_POST['school_id'], $condition['class_id'], STATUS_ACTIVE);
            $children = $childDao->searchPaging($condition['keyword'], $_POST['school_id'], $condition['class_id'], $condition['page'], PAGING_LIMIT, STATUS_ACTIVE, 1);

            $result = array();
            $result['total'] = $totalChildren;
            $result['page_count'] = ceil(($totalChildren + 0.0)/PAGING_LIMIT);
            $result['children'] = $children;
            $result['page'] = $condition['page'];
            $result['class_id'] = $condition['class_id'];

            $smarty->assign('result', $result);
//            $smarty->assign('canEdit', canEdit($_POST['school_username'], 'children'));

            $return['results'] = $smarty->fetch("ci/noga/ajax.noga.children.list.tpl");

            break;

        case 'searchhealthchild':
            include_once(DAO_PATH.'dao_child.php');
            $childDao = new ChildDAO();

            $school = getSchoolDataByUsername($_POST['school_username'], SCHOOL_INFO);
            $smarty->assign('school', $school);
            $smarty->assign('username', $_POST['school_username']);

            $condition = array();
            $condition['keyword'] = $_POST['keyword'];
            $condition['class_id'] = $_POST['class_id'];
            $children = $childDao->search($condition['keyword'], $_POST['school_id'], $condition['class_id']);

            $smarty->assign('children', $children);
            $smarty->assign('canEdit', canEdit($_POST['school_username'], 'children'));

            $return['results'] = $smarty->fetch("ci/school/ajax.school.children.health.list.tpl");

            break;

        case 'searchchildexist':
            include_once(DAO_PATH.'dao_child.php');
            $childDao = new ChildDAO();

            $school = getSchoolDataByUsername($_POST['school_username'], SCHOOL_CONFIG);
            
            $smarty->assign('school', $school);
            $smarty->assign('username', $_POST['school_username']);

            $result = array();
            $childInfo = $childDao->getStatusChildInSchool($_POST['childcode'], $school['school_id']);

            if ($childInfo['status'] == 0) {
                $return['results'] = "<div class='col-sm-9 col-sm-offset-3'><strong style='color: red'>" . __("Không tìm thấy thông tin trẻ") . "</strong></div>";
            } elseif ($childInfo['status'] == 2) {
                $return['results'] = "<div class='col-sm-9 col-sm-offset-3'><strong style='color: red'>" . __("Trẻ đã được thêm vào trường") . "</strong></div>";
            } elseif ($childInfo['status'] == 3) {
                $return['results'] = "<div class='col-sm-9 col-sm-offset-3'><strong style='color: red'>" . __("Trẻ đang được học ở trường khác") . "</strong></div>";
            } elseif ($childInfo['status'] == 1) {
                $child = $childDao->getChildByCode($_POST['childcode']);
                $parent = $childDao->getParentManageChild($child['child_parent_id']);
                $classes = getSchoolData($school['school_id'], SCHOOL_CLASSES);
                $smarty->assign('child', $child);
                $smarty->assign('parent', $parent);
                $smarty->assign('classes', $classes);
                $return['results'] = $smarty->fetch("ci/school/ajax.child.addexisting.tpl");
            }
            break;

        case 'searchchildexistclass':
            include_once(DAO_PATH.'dao_child.php');
            $childDao = new ChildDAO();

            $school = getSchoolDataByUsername($_POST['school_username'], SCHOOL_CONFIG);

            $class = getClassData($_POST['class_id'], CLASS_INFO);

            $smarty->assign('school', $school);
            $smarty->assign('class', $class);
            $smarty->assign('username', $class['group_name']);

            $result = array();
            $childInfo = $childDao->getStatusChildInSchool($_POST['childcode'], $school['school_id']);

            // Kiểm tra xem có phải là lớp gửi lên không
            $is_class = isset($_POST['is_class'])?$_POST['is_class']:0;
            $class_id = isset($_POST['class_id'])?$_POST['class_id']:0;

            if ($childInfo['status'] == 0) {
                $return['results'] = "<div class='col-sm-9 col-sm-offset-3'><strong style='color: red'>" . __("Không tìm thấy thông tin trẻ") . "</strong></div>";
            } elseif ($childInfo['status'] == 2) {
                $return['results'] = "<div class='col-sm-9 col-sm-offset-3'><strong style='color: red'>" . __("Trẻ đã được thêm vào trường") . "</strong></div>";
            } elseif ($childInfo['status'] == 3) {
                $return['results'] = "<div class='col-sm-9 col-sm-offset-3'><strong style='color: red'>" . __("Trẻ đang được học ở trường khác") . "</strong></div>";
            } elseif ($childInfo['status'] == 1) {
                $child = $childDao->getChildByCode($_POST['childcode']);
                $parent = $childDao->getParentManageChild($child['child_parent_id']);
                $classes = getSchoolData($school['school_id'], SCHOOL_CLASSES);
                $smarty->assign('child', $child);
                $smarty->assign('parent', $parent);
                $smarty->assign('classes', $classes);
                $smarty->assign('is_class', $is_class);
                $smarty->assign('class_id', $class_id);
                $return['results'] = $smarty->fetch("ci/class/ajax.child.addexisting.tpl");
            }
            break;

        case 'searchchildpaging':
            include_once(DAO_PATH.'dao_child.php');
            $childDao = new ChildDAO();
//            include_once (DAO_PATH.'dao_school.php');
//            $schoolDao = new SchoolDAO();
//            $school = $schoolDao->getSchoolByUsername($_POST['school_username'], $user->_data['user_id']);
            //$school = getSchool($_POST['school_username']);
            $school = getSchoolDataByUsername($_POST['school_username'], SCHOOL_INFO);
            $smarty->assign('school', $school);
            $smarty->assign('username', $_POST['school_username']);

            $result = array();
            //$totalChildren = $childDao->searchCount($_POST['keyword'], $_POST['school_id'], $_POST['class_id']);
            //$result['page_count'] = ceil(($totalChildren + 0.0)/PAGING_LIMIT);
            //$result['total'] = $totalChildren;

            //$result = $childDao->search($_POST['keyword'], $_POST['school_id'], $_POST['class_id']);
            $children = $childDao->searchPaging($_POST['keyword'], $_POST['school_id'], $_POST['class_id'], $_POST['page'], PAGING_LIMIT);
            $result['children'] = $children;
            $result['page'] = $_POST['page'];
            $result['class_id'] = $_POST['class_id'];
            $result['page_count'] = $_POST['page_count'];
            $result['total'] = $_POST['total'];
            $smarty->assign('result', $result);
            $smarty->assign('canEdit', canEdit($_POST['school_username'], 'children'));
            $return['results'] = $smarty->fetch("ci/school/ajax.school.children.list.tpl");

            break;
        case 'change_object':
            if($_POST['type'] == 'childinfo') {
                return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/' . $_POST['type'] . '/' . $_POST['object'] . '/childdetail' . '";'));
            }
            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/' . $_POST['type'] . '/' . $_POST['object'] . '";'));
            break;
        case 'searchuser':

            $isNewSearch = isset($_POST['is_new'])? $_POST['is_new']: 0;
            $condition = array();
            if ($isNewSearch) { //Nếu là lần search mới.
                //Đưa vào session để sau search lại.
                $condition['keyword'] = $_POST['keyword'];
                $condition['page'] = 1;
                $_SESSION[SESSION_KEY_SEARCH_USER] = $condition;
            } else {
                //Nếu click vào paging.
                $condition = $_SESSION[SESSION_KEY_SEARCH_USER];
                $condition['page'] = $_POST['page'];
                $_SESSION[SESSION_KEY_SEARCH_USER] = $condition;
            }

            $keyword = $condition['keyword'];
            $page= $condition['page'];
            $limit = PAGING_LIMIT;
            if($keyword) {
                $strSql = sprintf('SELECT count(user_id) AS cnt FROM users WHERE user_name LIKE %1$s OR user_fullname LIKE %1$s OR user_email LIKE %1$s', secure($keyword));
                $get_rows_count = $db->query($strSql) or _error(SQL_ERROR);

                $strSql = sprintf('SELECT * FROM users WHERE user_name LIKE %1$s OR user_fullname LIKE %1$s OR user_email LIKE %1$s
                    ORDER BY user_id LIMIT 0, %2$s', secure($keyword), $limit);

                $get_rows_user = $db->query($strSql) or _error(SQL_ERROR);
            } else {
                $get_rows_count = $db->query("SELECT count(user_id) AS cnt FROM users") or _error(SQL_ERROR);

                $strSql = sprintf("SELECT * FROM users ORDER BY user_id ASC LIMIT %s, %s", ($page - 1)* $limit, $limit);
                $get_rows_user = $db->query($strSql) or _error(SQL_ERROR);
            }
            $count_user = $get_rows_count->fetch_assoc()['cnt'];

            $users = array();
            if($get_rows_user->num_rows > 0) {
                while ($user = $get_rows_user->fetch_assoc()) {
                    $users[] = $user;
                }
            }

            // echo "<pre>"; print_r($users); die;
            $result = array();
            $result['total'] = $count_user;
            $result['page_count'] = ceil(($count_user + 0.0)/PAGING_LIMIT);
            $result['users'] = $users;
            $result['page'] = $page;
            $result['keyword'] = $keyword;

            $smarty->assign('result', $result);
            return_json($return);
            $return['results'] = $smarty->fetch("ci/noga/ajax.noga.user.list.tpl");
            break;

    }
	// return & exit
	return_json($return);

} catch (Exception $e) {
	modal(ERROR, __("Error"), $e->getMessage());
}

?>