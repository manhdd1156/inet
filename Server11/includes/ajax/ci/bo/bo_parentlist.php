<?php
/**
 * ajax -> data -> search
 * 
 * @package CINet v1.1
 * @author NOGA Co., Ltd
 */

// fetch bootstrap
require('../../../../bootstrap.php');

// check AJAX Request
is_ajax();

// check user logged in
if(!$user->_logged_in) {
    modal(LOGIN);
}

// search
try {

	// initialize the return array
	$return = array();
    include_once(DAO_PATH.'dao_user.php');
    $userDao = new UserDAO();
    $user_ids = count($_POST['user_ids']) ? $_POST['user_ids'] : array();
    $user_id = $_POST['user_id'];
    switch ($_POST['func']) {
        case 'add':
            $user_ids[] = $_POST['user_id'];
            break;
        case 'remove':
            $user_ids = array_diff($user_ids, [$_POST['user_id']]);
            break;
    }
    $results = $userDao->getUsers($user_ids);

    if (count($results) > 0) {
        // assign variables
        $smarty->assign('child_admin', $_POST['child_admin']);
        /* assign variables */
        $smarty->assign('results', $results);
        /* return */
        sleep(0.5);
        $return['results'] = $smarty->fetch("ci/ajax.parentlist.tpl");
        $return['phone'] = $results[0]['user_phone'];
        $return['email'] = $results[0]['user_email'];
        $return['no_parent'] = 0;
    } else {
        $return['results'] = __("No parent");
        $return['no_parent'] = 1;
    }
	// return & exit
	return_json($return);

} catch (Exception $e) {
	modal(ERROR, __("Error"), $e->getMessage());
}

?>