<?php
/**
* Package: ajax/ci/bo/class
*
 * @package ConIu
* @author QuanND
*/

// fetch bootstrap
require('../../../../../bootstrap.php');

// check AJAX Request
is_ajax();
check_login();

// initialize the return array
$return = array();

include_once(DAO_PATH.'dao_school.php');
include_once(DAO_PATH.'dao_class.php');
include_once(DAO_PATH.'dao_parent.php');
include_once(DAO_PATH.'dao_child.php');
include_once(DAO_PATH.'dao_user.php');
include_once(DAO_PATH.'dao_pickup.php');
$schoolDao = new SchoolDAO();
$classDao = new ClassDAO();
$parentDao = new ParentDAO();
$childDao = new ChildDAO();
$userDao = new UserDAO();
$pickupDao = new PickupDAO();

//Lấy ra thông tin lớp
//$class = $classDao->getClassByUsername($_POST['username'], $user->_data['user_id']);
$schoolId = $schoolDao->getSchoolIdByUsername($_POST['school_username']);
$school = getSchoolData($schoolId, SCHOOL_DATA);

try {
    $return = array();
    $db->autocommit(false);
    switch ($_POST['do']) {
        case 'save':
            $pickup = $pickupDao->getPickup($_POST['pickup_id']);
            if (is_null($pickup)) {
                throw new Exception(__("Late pickup class information is incorrect, please check again"));
            }
            //Xử lý nghiệp vụ khi cô giáo lưu thông tin đón muộn
            $args = array();
            $args['pickup_date'] = $pickup['pickup_time'];
            $args['using_at'] = $pickup['pickup_time'];
            $args['school_id'] = $school['page_id'];
            $args['pickup_id'] = $pickup['pickup_id'];
            //Giáo viên ghi sử dụng dịch vụ type = 0
            $args['type'] = 0;
            $childIds = $_POST['child'];
            $pickupTimes = $_POST['pickup_time'];
            //$serviceFees = $_POST['service_fee'];
            $pickupFees = $_POST['pickup_fee'];
            $totalChilds = $_POST['total_child'];
            $notes = $_POST['pickup_note'];
            $total = 0;

            $db->begin_transaction();
            for ($idx = 0; $idx < count($childIds); $idx++) {

                $args['child_id'] = $childIds[$idx];
                $latePickupFee = $pickupFees[$idx];
                $pickupTime = $pickupTimes[$idx];
                $totalAmount = $totalChilds[$idx];
                $pickupFee = 0;
                $totalChild = 0;

                if (is_empty($pickupTime)) {
                    $args['status'] = 0;
                    if ($latePickupFee >= 0 && $totalAmount >= $latePickupFee) {
                        $totalChild = $totalAmount - $latePickupFee;
                    }
                } else {
                    $args['status'] = 1;
                    $pickupFee = $pickupDao->getPickupFee($args['school_id'], $pickupTime);
                    $totalChild = $totalAmount + $pickupFee - $latePickupFee;
                    //$total = $total + $totalChild;
                }

                // Lấy dịch vụ sử dụng
                $serviceIds = isset($_POST['service_' . $childIds[$idx]]) ? $_POST['service_' . $childIds[$idx]] : array();
                // Lấy dịch vụ đã sử dụng
                $serviceUsedIds = $pickupDao->getIdServiceUsage($args['pickup_id'], $args['child_id']);
                //$serviceUsedIds = isset($_POST['service_used_' . $childIds[$idx]]) ? $_POST['service_used_' . $childIds[$idx]] : array();

                // Lấy ra dịch vụ mới đăng ký
                $args['recordServiceIds'] = $serviceIds;
                $args['deleteServiceIds'] = array_values(array_diff($serviceUsedIds, $serviceIds));

                // Xóa dịch vụ ghi nhầm
                if (!empty($args['deleteServiceIds'])) {
                    $deleteServicePrice = $pickupDao->deletePickupServiceUsage($args);
                    $totalChild = $totalChild - $deleteServicePrice;
                }

                // Ghi sử dụng dịch vụ mới
                if (!empty($args['recordServiceIds'])) {
                    $recordServicePrice = $pickupDao->recordPickupServiceUsage($args);
                    $totalChild = $totalChild + $recordServicePrice;
                }

                $args['pickup_time'] = $pickupTime;
                $args['pickup_fee'] = $pickupFee;
                $args['description'] = $notes[$idx];
                $args['total_child'] = $totalChild;
                $total = $total + $totalChild;

                // Update các thông tin chung của lần đón muộn (ci_pickup_child)
                $pickupDao->updatePickupChildApi($args);
            }
            // Update các thông tin chung của lần đón muộn (ci_pickup)
            $pickupDao->updateTotalPickup($args['pickup_id'], $total);

            $db->commit();
            if ($_POST['callback'] == 'manage') {
                return_json(array('callback' => 'window.location = "'.$system['system_url'].'/school/'.$_POST['school_username']. '/pickupteacher/manage' . '";'));
            } else {
                return_json(array('callback' => 'window.location = "'.$system['system_url'].'/school/'.$_POST['school_username']. '/pickupteacher/detail/' . $pickup['pickup_id'] . '";'));
            }
            break;

        case 'pickup':

            $pickup = $pickupDao->getPickup($_POST['pickup_id']);
            if (is_null($pickup)) {
                throw new Exception(__("Thông tin lớp đón muộn không đúng"));
            }

            $totalChild = 0;
            //Xử lý nghiệp vụ khi trả trẻ
            $args = array();
            $args['pickup_at'] = is_empty($_POST['time']) ? date('H:i') : $_POST['time'];
            $args['using_at'] = $pickup['pickup_time'];
            $args['school_id'] = $school['page_id'];
            $args['pickup_id'] = $_POST['pickup_id'];
            $args['child_id'] = $_POST['child_id'];
            $serviceIdList = $_POST['serviceIdList'];
            $args['pickup_note'] = $_POST['note'];
            $args['recordServiceIds'] = $serviceIdList;
            $serviceFeeList = $_POST['serviceFeeList'];
            $args['type'] = 0;

            // Lấy ra thông tin trẻ đã có (tiền đón muộn + tiền sử dụng dịch vụ)
            $childFee = $pickupDao->getPickupChildFee($args['pickup_id'], $args['child_id']);
            $latePickupFee = isset($childFee['late_pickup_fee']) ? $childFee['late_pickup_fee'] : 0;
            $totalAmount = isset($childFee['total_amount']) ? $childFee['total_amount'] : 0;
            // Tính tiền đón muộn theo thời gian trả trẻ
            $pickupFee = $pickupDao->getPickupFee($args['school_id'], $args['pickup_at']);
            $totalChild = $totalChild + $pickupFee;
            // Ghi sử dụng dịch vụ mới
            if (!empty($serviceIdList)) {
                $recordServicePrice = $pickupDao->recordPickupServiceUsage($args);
                $totalChild = $totalChild + $recordServicePrice;
            }

            // Khởi tạo các giá trị truyền vào
            $args['late_pickup_fee'] = $pickupFee;
            $args['total_child'] = $totalChild;
            $total = $pickup['total'] + $args['total_child'];
            $args['total'] = ($total > 0) ? $total : 0;

            $db->begin_transaction();
            $pickupDao->recordPickupOfChild($args);

            // Thông báo cho phụ huynh khi trẻ được trả
            $child = getChildData($args['child_id'], CHILD_INFO);
            $parents = getChildData($args['child_id'], CHILD_PARENTS);
            $parentIds = array_keys($parents);
            $userDao->postNotifications($parentIds, NOTIFICATION_CHILD_PICKEDUP, NOTIFICATION_NODE_TYPE_CHILD,
                $args['pickup_id'], $args['pickup_at'], $args['child_id'], convertText4Web($child['child_name']));
            $db->commit();
            if ($_POST['callback'] == 'manage') {
                return_json(array('callback' => 'window.location = "'.$system['system_url'].'/school/'.$_POST['school_username']. '/pickupteacher/manage' . '";'));
            } else {
                return_json(array('callback' => 'window.location = "'.$system['system_url'].'/school/'.$_POST['school_username']. '/pickupteacher/detail/' . $_POST['pickup_id'] . '";'));
            }
            break;

        case 'get_pickup_fee':

            $late_pickup_fee = $pickupDao->getPickupFee($school['page_id'], $_POST['pickup_time']);

            $return['results'] = $late_pickup_fee;
            return_json($return);
            break;

        case 'cancel':
            //Xử lý nghiệp vụ khi cô giáo hủy 1 trẻ khỏi lớp trông muộn
            $args = array();
            $pickup = $pickupDao->getPickup($_POST['pickup_id']);
            if (is_null($pickup)) {
                throw new Exception(__("Late pickup class information is incorrect, please check again"));
            }

            $args['school_id'] = $school['page_id'];
            $args['pickup_id'] = $_POST['pickup_id'];
            $args['child_id'] = $_POST['child_id'];
            $args['remove_child'][] = $_POST['child_id'];

            $args['deduction'] = $pickupDao->getTotalAmountPickupChild($args);
            $args['count_remove'] = 1;

            $db->begin_transaction();
            $pickupDao->deletePickupChild($args);

            $db->commit();

            if ($_POST['callback'] == 'manage') {
                return_json(array('callback' => 'window.location = "'.$system['system_url'].'/school/'.$_POST['school_username']. '/pickupteacher/manage' . '";'));
            } else {
                return_json(array('callback' => 'window.location = "'.$system['system_url'].'/school/'.$_POST['school_username']. '/pickupteacher/detail/' . $_POST['pickup_id'] . '";'));
            }
            //return_json(array('success' => true, 'message' => __("Remove child from the late pickup class successfully")));
            break;

        case 'get_class_assign':
            $args = array();
            $args['school_id'] = $school['page_id'];
            $args['begin'] = toDBDate($_POST['begin']);
            $monday = date("Y-m-d", strtotime("monday this week", strtotime($args['begin'])));

            $day['begin'] = date('d/m/Y');
            $day['mon'] = toSysDate($monday);
            $day['tue'] = date('d/m/Y', strtotime("+1 day", strtotime($monday)));
            $day['wed'] = date('d/m/Y', strtotime("+2 day", strtotime($monday)));
            $day['thu'] = date('d/m/Y', strtotime("+3 day", strtotime($monday)));
            $day['fri'] = date('d/m/Y', strtotime("+4 day", strtotime($monday)));
            $day['sat'] = date('d/m/Y', strtotime("+5 day", strtotime($monday)));
            $day['sun'] = date('d/m/Y', strtotime("+6 day", strtotime($monday)));

            $classes = $pickupDao->getPickupClasses($args['school_id']);
            $assignInfo = $pickupDao->getAssignInWeek($args['school_id'], $monday);

            // assign variables
            $smarty->assign('classes', $classes);
            $smarty->assign('data', $assignInfo['teacher_list']);
            $smarty->assign('day', $day);
            $return['results'] = $smarty->fetch("ci/school/teacher_pickup/ajax.pickup.teacherassign.tpl");

            return_json($return);

            break;

        case 'list_child':
            $class_id = $_POST['class_id'];
            $pickup_class_id = $_POST['pickup_class_id'];

            $template = $pickupDao->getTemplate($school['page_id']);

            $child_count = 0;
            $childrenRegistered = array();
            $date = toDBDate($_POST['add_to_date']);
            $children = $pickupDao->getChildOfClass($class_id, $_POST['pickup_id'], $pickup_class_id, $child_count, $date);
            $disableSave = (count($children) == 0) ? true : false;

            $pickupIds = $pickupDao->getPickupIds($school['page_id'], $date);
            if (!is_null($pickupIds) && $pickup_class_id > 0) {
                $childrenRegistered = $pickupDao->getChildRegistered($class_id, $pickupIds, $pickup_class_id);
            }
            //print_r($childrenRegistered); die;

            $smarty->assign('child_count', $child_count);
            $smarty->assign('children', $children);
            $smarty->assign('childrenRegistered', $childrenRegistered);
            $smarty->assign('today', $_POST['add_to_date']);

            $return['results'] = $smarty->fetch("ci/school/teacher_pickup/ajax.pickupchild.tpl");
            $return['disableSave'] = $disableSave;

            return_json($return);

            break;

        case 'add_child':
            //Xử lý nghiệp vụ khi thêm trẻ vào lớp trông muộn
            $template = $pickupDao->getTemplate($school['page_id']);
            if (is_null($template)) {
                throw new Exception(__("The school has not establish the late pickup configuration"));
            }

            $pickup = $pickupDao->getPickup($_POST['pickup_id']);
            if (is_null($pickup)) {
                throw new Exception(__("Late pickup class information is incorrect, please check again"));
            }

            $args = array();
            $args['pickup_id'] = $pickup['pickup_id'];
            $args['pickup_time'] = $pickup['pickup_time'];
            $args['pickup_day'] = $pickup['pickup_day'];
            $args['class_id'] = $_POST['class_id'];
            $args['pickup_class_id'] = $_POST['pickup_class_id'];
            $args['school_id'] = $school['page_id'];
            $args['status'] = 0;
            $args['type'] = 0; // Giáo viên đăng ký type = 0

            //Lấy id của những trẻ thêm vào trông muộn
            $args['new_child'] = is_null($_POST['childIds'])? array(): $_POST['childIds'];
            //Lấy id của những trẻ đã có trong lớp
            $args['old_child'] = $pickupDao->getChildIdOfPickup($args['pickup_id'], $args['class_id']);
            $args['remove_child'] = array_diff($args['old_child'], $args['new_child']);

            $db->begin_transaction();
            if (count($args['remove_child']) > 0) {
                $args['deduction'] = $pickupDao->getTotalAmountPickupChild($args);
                $args['count_remove'] = count($args['remove_child']);
                $pickupDao->deletePickupChild($args);
            }

            // Thêm trẻ vào lớp đón muộn
            $args['add_child'] = array_diff($args['new_child'], $args['old_child']);
            if (count($args['add_child']) > 0) {
                $pickupDao->insertPickupChild($args);
            }

            $db->commit();
            return_json(array('success' => true, 'message' => __("Add child to the late pickup class successfully")));

            break;

        case 'search_pickup':
            if (validateDate($_POST['fromDate']) && validateDate($_POST['toDate'])) {
                $fromDate = $_POST['fromDate'];
                $toDate =  $_POST['toDate'];
                $pickups = $pickupDao->getPickupTeacherInMonth($school['page_id'], $user->_data['user_id'], toDBDate($fromDate), toDBDate($toDate));

                $smarty->assign('username', $school['page_name']);
                $smarty->assign('pickups', $pickups);
                $return['results'] = $smarty->fetch("ci/school/teacher_pickup/ajax.pickuplist.tpl");

                return_json($return);
            } else {
                modal(MESSAGE, __("Notification"), __("The time is incorrect, please check again"));
            }
            break;

        default:
            _error(400);
            break;
    }
} catch (Exception $e) {
    $db->rollback();
    return_json( array('error' => true, 'message' => $e->getMessage()) );
} finally {
    $db->autocommit(true);
}

?>