<?php
/**
 * Package: ajax/ci/bo
 *
 * @package ConIu
 * @author QuanND
 */

// fetch bootstrap
require('../../../../../bootstrap.php');

// check AJAX Request
is_ajax();
check_login();

if (is_empty($_POST['school_username']) || !valid_username($_POST['school_username'])) {
    _error(404);
}

include_once(DAO_PATH . 'dao_school.php');
include_once(DAO_PATH . 'dao_tuition.php');
include_once(DAO_PATH . 'dao_child.php');
include_once(DAO_PATH . 'dao_parent.php');
include_once(DAO_PATH . 'dao_user.php');
include_once(DAO_PATH . 'dao_attendance.php');
include_once(DAO_PATH . 'dao_class.php');
include_once(DAO_PATH . 'dao_service.php');

$schoolDao = new SchoolDAO();
$tuitionDao = new TuitionDAO();
$childDao = new ChildDAO();
$parentDao = new ParentDAO();
$userDao = new UserDAO();
$attendanceDao = new AttendanceDAO();
$classDao = new ClassDAO();
$serviceDao = new ServiceDAO();

//$school = getSchool($_POST['school_username']);
$school = getSchoolDataByUsername($_POST['school_username'], SCHOOL_DATA);
if (is_null($school)) {
    _error(403);
}
$canEdit = canEdit($_POST['school_username'], 'tuitions');
$objPHPExcel = null;
try {
    $return = array();
    $db->autocommit(false);
    switch ($_POST['do']) {
        case 'edit_fee':
            if (!$canEdit) {
                _error(403);
            }
            if (!isset($_POST['fee_id']) || !is_numeric($_POST['fee_id'])) {
                _error(400);
            }
            $tuitionDetail = $tuitionDao->getFee($_POST['fee_id']);
            if (is_null($tuitionDetail)) {
                _error(404);
            }
            $args = array();
            $args['fee_id'] = $_POST['fee_id'];
            $args['fee_name'] = $_POST['fee_name'];
            //$args['type'] = $_POST['type'];
            $args['unit_price'] = str_replace(',', '', $_POST['unit_price']);
            $args['level'] = (isset($_POST['level']) && ($_POST['level'] > 0))? $_POST['level']: CHILD_LEVEL; //Combobox bị disable sẽ không truyền được giá trị.
            $args['class_level_id'] = $_POST['class_level_id'];
            $args['class_id'] = $_POST['class_id'];
            $args['description'] = $_POST['description'];
            $args['status'] = (isset($_POST['status']) && ($_POST['status'] == 'on')) ? 1 : 0;
            $args['fee_change_id'] = $_POST['fee_change_id'];

            if ($args['level'] == CHILD_LEVEL) {
                $strRemandingChildIds = isset($_POST['remanding_child_ids'])? $_POST['remanding_child_ids']: '';
                $remandingChildIds = ($strRemandingChildIds == '')? array(): explode(",", $strRemandingChildIds);
                $childIds = isset($_POST['child_ids'])? $_POST['child_ids']: array();
                $allChildIds = array_merge($childIds, $remandingChildIds);
                $allChildIds = array_unique($allChildIds);
                $args['child_ids'] = $allChildIds;

                // Nếu áp dụng cho trẻ mà không có trẻ nào được chọn thì báo lỗi
                if (count($allChildIds) == 0) {
                    throw new Exception(__("No student have been selected yet"));
                }
            }

            $db->begin_transaction();
            $tuitionDao->updateFee($args);
            $db->commit();
            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/school/' . $_POST['school_username'] . '/fees";'));
            break;
        case 'add_fee':
            if (!$canEdit) {
                _error(403);
            }
            $args = array();

            $args['fee_name'] = $_POST['fee_name'];
            $args['type'] = $_POST['type'];
            $args['unit_price'] = $_POST['unit_price'];
            $args['unit_price'] = str_replace(',', '', $args['unit_price']);
            $args['level'] = $_POST['level'];
            $args['school_id'] = $school['page_id'];
            $args['class_level_id'] = $_POST['class_level_id'];
            $args['class_id'] = $_POST['class_id'];
            $args['description'] = $_POST['description'];
            $args['fee_change_id'] = $_POST['fee_change_id'];
            $args['status'] = (isset($_POST['status']) && ($_POST['status'] == 'on')) ? 1 : 0;

            if ($args['level'] == CHILD_LEVEL) {
                $strRemandingChildIds = isset($_POST['remanding_child_ids'])? $_POST['remanding_child_ids']: '';
                $remandingChildIds = ($strRemandingChildIds == '')? array(): explode(",", $strRemandingChildIds);
                $childIds = isset($_POST['child_ids'])? $_POST['child_ids']: array();
                $allChildIds = array_merge($childIds, $remandingChildIds);
                $allChildIds = array_unique($allChildIds);
                $args['child_ids'] = $allChildIds;

                // Nếu áp dụng cho trẻ mà không có trẻ nào được chọn thì báo lỗi
                if (count($allChildIds) == 0) {
                    throw new Exception(__("No student have been selected yet"));
                }
            }

            $db->begin_transaction();
            $tuitionDao->insertFee($args);

            $db->commit();
            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/school/' . $_POST['school_username'] . '/fees";'));
            break;
        case 'delete_fee':
            if (!$canEdit) {
                _error(403);
            }
            $db->begin_transaction();

            if (!isset($_POST['id']) || !is_numeric($_POST['id'])) {
                _error(400);
            }
            $tuitionDao->deleteFee($_POST['id']);
            $db->commit();

            return_json( array('success' => true, 'message' => __("Success")));
            break;
        case 'cancel_fee':
            if (!$canEdit) {
                _error(403);
            }
            $db->begin_transaction();

            if (!isset($_POST['id']) || !is_numeric($_POST['id'])) {
                _error(400);
            }
            $tuitionDao->InactiveFee($_POST['id']);
            $db->commit();

            return_json( array('success' => true, 'message' => __("Success")));
            break;
        case 'fee_list_child': //Hiển thị danh sách trẻ khi chọn lớp trên màn hình tạo/sửa loại phí.
            if (!isset($_POST['class_id']) || !is_numeric($_POST['class_id'])) {
                _error(404);
            }

            $strRemandingChildIds = isset($_POST['remanding_child_ids'])? $_POST['remanding_child_ids']: '';
            $remandingChildIds = ($strRemandingChildIds == '')? array(): explode(",", $strRemandingChildIds);
            $childIds = isset($_POST['child_ids'])? $_POST['child_ids']: array();
            $allChildIds = array_merge($childIds, $remandingChildIds);
            $allChildIds = array_unique($allChildIds);

            $currentClassChildIds = array();
            $newChildren = array();
            $children = getClassData($_POST['class_id'], CLASS_CHILDREN);
            foreach ($children as $child) {
                if (in_array($child['child_id'], $allChildIds)) {
                    $child['checked'] = 1;
                    $currentClassChildIds[] = $child['child_id'];
                } else {
                    $child['checked'] = 0;
                }
                $newChildren[] = $child;
            }

            $remandingChildIds = array_diff($allChildIds, $currentClassChildIds);

            $smarty->assign('children', $newChildren);
            $smarty->assign('remanding_child_ids', implode(",", $remandingChildIds));
            $childrenListHtml = $smarty->fetch("ci/school/school.fees.children.tpl");
            $return['results'] = $childrenListHtml;

            return_json($return);
            break;
        case 'confirm':
            if (!$canEdit) {
                _error(403);
            }
            $pay_amount = str_replace(',', '', $_POST['pay_amount']);
            $tuition = $tuitionDao->getTuition($_POST['tuition_id']);

            //Cập nhật thông tin hệ thống
            $result = $tuitionDao->confirmTuitionChild($_POST['tuition_id'], $_POST['id'], $pay_amount);

            if ($result == 1) { //Chỉ khi thực sự cập nhật mới thông báo
                //Thông báo với phụ huynh
                //$child = $childDao->getChild($_POST['child_id']);
                $child = getChildData($_POST['child_id'], CHILD_INFO);

                //$parents = $childDao->getParent($_POST['child_id']);
                $parents = getChildData($_POST['child_id'], CHILD_PARENTS);
                if (!is_null($child) && !is_null($parents)) {
                    $parentIds = array();
                    foreach ($parents as $parent) {
                        $parentIds[] = $parent['user_id'];
                    }

                    $userDao->postNotifications($parentIds, NOTIFICATION_CONFIRM_TUITION, NOTIFICATION_NODE_TYPE_CHILD,
                        $_POST['id'], $tuition['month'], $_POST['child_id'], convertText4Web($child['child_name']));
                    // Thông báo cho các quản lý trường khác thanh toán được xác nhận
                    $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'tuitions', $school['page_admin']);
                    $userDao->postNotifications($userManagerIds, NOTIFICATION_CONFIRM_TUITION_SCHOOL, NOTIFICATION_NODE_TYPE_SCHOOL,
                        $_POST['tuition_id'], $tuition['month'], $school['page_name'], convertText4Web($child['child_name']));
                }
            }

            $db->commit();
            return_json( array('success' => true, 'message' => __("Success")));
            //return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/school/' . $_POST['school_username'] . '/tuitions/detail/' . $_POST['tuition_id'] . '";'));
            break;
        case 'delchild':
            if (!$canEdit) {
                _error(403);
            }
            $db->begin_transaction();

            $tuitionChild = $tuitionDao->getTuitionChildOnly($_POST['tuition_child_id']);
            if (!is_null($tuitionChild)) {
                //Cập nhật thông tin học phí của lớp.
                $tuitionDao->updateTuitionWhenDeletingChild($tuitionChild);
                //Xóa học phí của trẻ.
                $tuitionDao->deleteTuitionChild($_POST['tuition_child_id']);
            }

            $db->commit();
            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/school/' . $_POST['school_username'] . '/tuitions/detail/' . $tuitionChild['tuition_id'] . '";'));
            break;
        case 'unconfirm':
            if (!$canEdit) {
                _error(403);
            }
            $db->begin_transaction();

            //Cập nhật thông tin hệ thống
            $result = $tuitionDao->updateTuitionChildStatus($_POST['id'], TUITION_CHILD_NOT_PAID);

            //Thông báo với phụ huynh
            if ($result > 0) {
                //$child = $childDao->getChild($_POST['child_id']);
                $child = getChildData($_POST['child_id'], CHILD_INFO);

                //$parents = $childDao->getParent($_POST['child_id']);
                $parents = getChildData($_POST['child_id'], CHILD_PARENTS);
                $parentIds = array_keys($parents);
                if (!is_null($child) && !is_null($parentIds)) {
                    $userDao->postNotifications($parentIds, NOTIFICATION_UNCONFIRM_TUITION, NOTIFICATION_NODE_TYPE_CHILD,
                            $_POST['id'], convertText4Web($_POST['child_amount']), $_POST['child_id'], convertText4Web($child['child_name']));
                    // Thông báo cho các quản lý trường khác thanh toán không được xác nhận
                    $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'tuitions', $school['page_admin']);
                    $userDao->postNotifications($userManagerIds, NOTIFICATION_UNCONFIRM_TUITION, NOTIFICATION_NODE_TYPE_SCHOOL,
                        $_POST['tuition_id'], convertText4Web($_POST['child_amount']), $school['page_name'], convertText4Web($child['child_name']));
                }
            }
            $db->commit();
            //return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/school/' . $_POST['school_username'] . '/tuitions/detail/' . $_POST['tuition_id'] . '";'));
            return_json( array('success' => true, 'message' => __("Success")));
            break;
        case 'newchild':
            if (!$canEdit) {
                _error(403);
            }

            $childIds = isset($_POST['child_id'])? $_POST['child_id']: array();
            $childPreMonths = $_POST['pre_month'];
            $childDeductionTotals = $_POST['total_child_deduction'];
            $childTotals = $_POST['child_total'];
            $childDebtAmounts = $_POST['debt_amount'];
            $descriptions = $_POST['detail_description'];

            if (count($childIds) > 0) {
                $db->begin_transaction();

                $tuitionDao->updateTotal($_POST['tuition_id'], str_replace(',', '', $_POST['class_total']));

                // Đưa danh sách học phí của trẻ thêm mới vào hệ thống
                $tuitionChildIds = array();
                for ($idx = 0; $idx < count($childIds); $idx++) {
                    $childId = $childIds[$idx];

                    $tuitionDetails = getTuitionDetailData($childId);

                    // Thêm mới tuition của tháng hiện tại (giả định)
                    $child = array();
                    $child['child_id'] = $childId;
                    $child['pre_month'] = $childPreMonths[$idx];
                    $child['total_amount'] = str_replace(',', '', $childTotals[$idx]);
                    $child['description'] = $descriptions[$idx];
                    $child['debt_amount'] = str_replace(',', '', $childDebtAmounts[$idx]);
                    $tuitionChildId = $tuitionDao->insertTuitionChild($_POST['tuition_id'], $child, $tuitionDetails);
                    $tuitionChildIds[] = $tuitionChildId;

                    //Thông báo cho phụ huynh học sinh.
                    if ($_POST['is_notified']) {
                        //Thông báo với phụ huynh
                        //$child = $childDao->getChild($childId);
                        $child = getChildData($childId, CHILD_INFO);

                        //$parentIds = $parentDao->getParentIds($childId);
                        $parents = getChildData($childId, CHILD_PARENTS);
                        if (!is_null($child) && !is_null($parents)) {
                            $parentIds = array_keys($parents);
                            $userDao->postNotifications($parentIds, NOTIFICATION_NEW_TUITION, NOTIFICATION_NODE_TYPE_CHILD, $tuitionChildId, convertText4Web($_POST['month']), $childId, convertText4Web($child['child_name']));
                        }
                    }
                }

                /* Coniu - Tương tác trường */
                setIsAddNew($school['page_id'], 'tuition_created', $tuitionChildIds);
                /* Coniu - END */
            }
            $db->commit();
            return_json(array('callback' => 'window.location = "'.$system['system_url'].'/school/'.$_POST['school_username'].'/tuitions/detail/'.$_POST['tuition_id'].'";'));
            break;
        case 'add_tuition':
            if (!$canEdit) {
                _error(403);
            }
            $args = array();
            $args['class_id'] = $_POST['class_id'];
            $args['school_id'] = $school['page_id'];
            $args['month'] = $_POST['month'];
            $args['day_of_month'] = $_POST['day_of_month'];
            $args['is_notified'] = (isset($_POST['notify_immediately']) && $_POST['notify_immediately'] == 'on') ? 1 : 0;
            $args['description'] = $_POST['description'];
            $args['total_amount'] = str_replace( ',', '', $_POST['total_amount']);

            $db->begin_transaction();
            //Nhập thông tin học phí cả lớp vào hệ thống
            $tuitionId = $tuitionDao->insertTuition($args);

            // Tăng lượt tương tác thêm mới - TaiLA
            addInteractive($school['page_id'], 'tuition', 'school_view', $tuitionId, 1);

            $childIds = $_POST['child_id'];
            $childPreMonths = $_POST['pre_month'];
            $childTotals = $_POST['child_total'];
            $childDebtAmounts = $_POST['debt_amount'];
            $descriptions = $_POST['detail_description'];

            //Đưa danh sách học phí của từng trẻ vào hệ thống
            $tuitionChildIds = array();
            for ($idx = 0; $idx < count($childIds); $idx++) {
                $childId = $childIds[$idx];

                $tuitionDetails = getTuitionDetailData($childId);

                // Thêm mới tuition của tháng hiện tại (giả định)
                $child = array();
                $child['child_id'] = $childId;
                $child['pre_month'] = $childPreMonths[$idx];
                $child['total_amount'] = str_replace(',', '', $childTotals[$idx]);
                $child['description'] = $descriptions[$idx];
                $child['debt_amount'] = str_replace(',', '', $childDebtAmounts[$idx]);
                $tuitionChildId = $tuitionDao->insertTuitionChild($tuitionId, $child, $tuitionDetails);
                $tuitionChildIds[] = $tuitionChildId;

                //Thông báo cho phụ huynh học sinh.
                if ($args['is_notified']) {
                    //Thông báo với phụ huynh
                    $child = getChildData($childId, CHILD_INFO);

                    //$parentIds = $parentDao->getParentIds($childId);
                    $parents = getChildData($childId, CHILD_PARENTS);
                    if (!is_null($child) && !is_null($parents)) {
                        $parentIds = array_keys($parents);
                        $userDao->postNotifications($parentIds, NOTIFICATION_NEW_TUITION, NOTIFICATION_NODE_TYPE_CHILD, $tuitionChildId, convertText4Web($_POST['amount']), $childId, convertText4Web($child['child_name']));
                    }
                    //Cập nhật thời gian thông báo cuối cùng
                    $tuitionDao->updateNotifyTime($tuitionChildId);
                }
            }
            if ($args['is_notified']) {
                // Thông báo cho các quản lý trường khác thanh toán được xác nhận
                //$class = $classDao->getClass($_POST['class_id']);
                $class = getClassData($_POST['class_id'], CLASS_INFO);
                $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'tuitions', $school['page_admin']);
                $userDao->postNotifications($userManagerIds, NOTIFICATION_NEW_TUITION, NOTIFICATION_NODE_TYPE_SCHOOL, $tuitionId, convertText4Web($class['group_title']), $school['page_name'], convertText4Web($_POST['month']));

                /* Coniu - Tương tác trường */
                setIsAddNew($school['page_id'], 'tuition_created', $tuitionChildIds);
                /* Coniu - END */
            }
            addInteractive($school['page_id'], 'tuition', 'school_view', $tuitionId, 1);
            $db->commit();
            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/school/' . $_POST['school_username'] . '/tuitions";'));

            break;
        case 'edit_tuition':
            if (!$canEdit) {
                _error(403);
            }
            $args = array();
            $args['tuition_id'] = $_POST['tuition_id'];
            $args['is_notified'] = (isset($_POST['notify_immediately']) && $_POST['notify_immediately'] == 'on') ? 1 : 0;
            $args['description'] = $_POST['description'];
            $args['total_amount'] = str_replace(',', '', $_POST['total_amount']);

            $db->begin_transaction();
            //Nhập thông tin học phí cả lớp vào hệ thống
            $tuitionDao->updateTuition($args);

            $childIds = $_POST['child_id'];
            $tuitionChildIds = $_POST['tuition_child_id'];
            $childTotals = $_POST['child_total'];
            $childOldTotals = $_POST['old_child_total'];
            $childDebtAmounts = $_POST['debt_amount'];
            $descriptions = $_POST['detail_description'];
            $oldTuitionChildIds = $_POST['old_tuition_child_id'];
            $deletedTuitionChildIds = array_diff($oldTuitionChildIds, $tuitionChildIds); //TuitionChildID của những trẻ bị xóa.
            if (count($deletedTuitionChildIds) > 0) {
                $tuitionDao->deleteTuitionChildren($deletedTuitionChildIds);
            }

            //Đưa danh sách học phí của từng trẻ vào hệ thống
            for ($idx = 0; $idx < count($childIds); $idx++) {
                $childId = $childIds[$idx];

                $tuitionDetails = getTuitionDetailData($childId);
                $tuitionDetailsTemp = array();
                foreach ($tuitionDetails as $tuitionDetail) {
                    if($tuitionDetail['unit_price_deduction'] == '') {
                        $tuitionDetail['unit_price_deduction'] = 0;
                    }
                    $tuitionDetailsTemp[] = $tuitionDetail;
                }

                // Thêm mới tuition của tháng hiện tại (giả định)
                $tuitionChild = array();
                $tuitionChild['tuition_child_id'] = $tuitionChildIds[$idx];
                $tuitionChild['total_amount'] = str_replace(',', '', $childTotals[$idx]);
                $tuitionChild['description'] = $descriptions[$idx];
                $tuitionChild['debt_amount'] = str_replace(',', '', $childDebtAmounts[$idx]);

                // Check toàn bộ các mục học phí của trẻ, nếu có thay đổi thì thông báo
                // * Lấy thông tin học phí cũ của trẻ (trước khi sửa)
                $tuitionDetailsOld = $tuitionDao->getTuitionDetail4DetailPub($tuitionChildIds[$idx]);
                // Xét các trường hợp gửi thông báo về cho phụ huynh
                // Mặc định không gửi
                $is_notify = false;
                if($args['is_notified']) {
                    if($_POST['is_notified_old'] != $args['is_notified']) {
                        $is_notify = true;
                    } else {
                        if(($tuitionDetailsTemp != $tuitionDetailsOld) || ($tuitionChild['total_amount'] != $childOldTotals[$idx])) {
                            $is_notify = true;
                        }
                    }
                }
                // Cập nhật thông tin học phí của trẻ
                $tuitionDao->updateTuitionChild($tuitionChild, $tuitionDetails);

                //Thông báo cho phụ huynh học sinh khi
                // a. Chuyển từ trạng thái KHÔNG->THÔNG BÁO: Thì thông báo tất
                // b. Số tiền học phí thay đổi hoặc các hạng mục thay đổi
                if ($is_notify) {
                    //Thông báo với phụ huynh
                    //$child = $childDao->getChild($childIds[$idx]);
                    $child = getChildData($childIds[$idx], CHILD_INFO);

                    //$parentIds = $parentDao->getParentIds($childIds[$idx]);
                    $parents = getChildData($childIds[$idx], CHILD_PARENTS);
                    if (!is_null($child) && !is_null($parents)) {
                        $parentIds = array_keys($parents);
                        $userDao->postNotifications($parentIds, NOTIFICATION_UPDATE_TUITION, NOTIFICATION_NODE_TYPE_CHILD,
                            $tuitionChildIds[$idx], '', $childIds[$idx], convertText4Web($child['child_name']));
                    }
                    //Cập nhật thời gian thông báo cuối cùng
                    $tuitionDao->updateNotifyTime($tuitionChild['tuition_child_id']);
                }
            }
            if ($args['is_notified'] && (($args['is_notified'] != $_POST['is_notified_old']))) {
                // Thông báo cho các quản lý trường khác thanh toán được xác nhận
                //$class = $classDao->getClass($_POST['class_id']);
                $class = getClassData($_POST['class_id'], CLASS_INFO);
                $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'tuitions', $school['page_admin']);
                $userDao->postNotifications($userManagerIds, NOTIFICATION_UPDATE_TUITION, NOTIFICATION_NODE_TYPE_SCHOOL,
                    $_POST['tuition_id'], convertText4Web($class['group_title']), $school['page_name'], convertText4Web($_POST['month']));
            }

            $db->commit();
            return_json(array('success' => true, 'message' => __("Done, Tuition info have been updated")));
            break;
        case 'notify':
            if (!$canEdit) {
                _error(403);
            }
            $db->begin_transaction();
            // Lấy chi tiết tuition
            $tuition = $tuitionDao->getTuition($_POST['id']);
            //Lấy ra danh sách tuition child
            $tuitionChilds = $tuitionDao->getTuitionChild($_POST['id']);

            //Cập nhật trạng thái thông báo của học phí
            $tuitionDao->updateNotifyStatus($_POST['id'], 1);

            //Thông báo tới phụ huynh
            $tuitionChildIds = array();
            foreach ($tuitionChilds as $tuitionChild) {

                $tuitionChildIds[] = $tuitionChild['tuition_child_id'];
                //Thông báo với phụ huynh
                //$child = $childDao->getChild($tuitionChild['child_id']);
                $child = getChildData($tuitionChild['child_id'], CHILD_INFO);

                //$parentIds = $parentDao->getParentIds($tuitionChild['child_id']);
                $parents = getChildData($tuitionChild['child_id'], CHILD_PARENTS);
                if (!is_null($child) && !is_null($parents)) {
                    $parentIds = array_keys($parents);
                    $userDao->postNotifications($parentIds, NOTIFICATION_NEW_TUITION, NOTIFICATION_NODE_TYPE_CHILD,
                        $tuitionChild['tuition_child_id'], '', $tuitionChild['child_id'], convertText4Web($child['child_name']));
                }
                //Cập nhật thời gian thông báo cuối cùng
                $tuitionDao->updateNotifyTime($tuitionChild['tuition_child_id']);
            }
            // Thông báo cho các quản lý trường khác thanh toán được xác nhận
            //$class = $classDao->getClass($tuition['class_id']);
            $class = getClassData($tuition['class_id'], CLASS_INFO);
            $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'tuitions', $school['page_admin']);
            $userDao->postNotifications($userManagerIds, NOTIFICATION_NEW_TUITION, NOTIFICATION_NODE_TYPE_SCHOOL,
                $_POST['id'], convertText4Web($class['group_title']), $school['page_name'], convertText4Web($tuition['month']));

            /* Coniu - Tương tác trường */
            setIsAddNew($school['page_id'], 'tuition_created', $tuitionChildIds);
            /* Coniu - END */

            $db->commit();
            return_json( array('success' => true, 'message' => __("Success")));
            break;
        case 'load_4leave':
            if(!isset($_POST['end_at']) || (toDBDate($_POST['end_at']) == '')) {
                _error(404);
            }

            $data = $tuitionDao->getTuition4Leave($school['page_id'], $_POST['child_id'], $_POST['end_at']);
            $smarty->assign('data', $data);

            $return['results'] = $smarty->fetch("ci/school/ajax.school.tuition.4leave.tpl");
            return_json($return);

            break;
        case 'class_load_4leave':
            if(!isset($_POST['class_end_at']) || (toDBDate($_POST['class_end_at']) == '')) {
                _error(404);
            }


            //$data = $classDao->getClass($_GET['id']);
            // Memcache - Lấy ra thông tin của 1 lớp
            $data = getClassData($_POST['class_id'], ALL);
            if (is_null($data)) {
                _error(404);
            }
            // assign variables
            $results = array();
            if(count($data['children']) > 0) {
                foreach ($data['children'] as $child) {
                    $pads = array();
                    $childPad = $childDao->getChild4Leave($child['child_id']);
                    if (is_null($child)) {
                        _error(404);
                    }
                    $dataPad = $tuitionDao->getTuition4Leave($school['page_id'], $child['child_id'], $childPad['end_at']);
                    $pads['child'] = $childPad;
                    $pads['data'] = $dataPad;
                    $results[] = $pads;
                }
            }

            $smarty->assign('data', $data['info']);
            $smarty->assign('results', $results);

            $return['results'] = $smarty->fetch("ci/school/ajax.school.graduate.tpl");
            return_json($return);

            break;
        case 'list_child': //Load thông tin học phí một lớp để tạo học phí tháng.
            if (!isset($_POST['class_id']) || !is_numeric($_POST['class_id'])) {
                _error(404);
            }

            $attCount = 0;
            if ((!isset($_POST['day_of_month'])) || ($_POST['day_of_month'] < 1)) {
                $attCount = workingDayInMonth($_POST['month']);
            } else {
                $attCount = $_POST['day_of_month'];
            }
            $results = $tuitionDao->getTuitionOfClassInMonth($school['page_id'], $_POST['class_id'], $_POST['month'], $attCount);

            $smarty->assign('results', $results);
            $smarty->assign('school', $school);
            $smarty->assign('attCount', $attCount);
            $smarty->assign('username', $_POST['school_username']);

            $return['results'] = $smarty->fetch("ci/school/ajax.school.tuition.children.tpl");
            $return['attCount'] = $attCount;
            $return['no_data'] = (count($results['children']) == 0);

            return_json($return);

            break;
        case 'reload_child_tuition':
            if (!isset($_POST['class_id']) || !is_numeric($_POST['class_id'])) {
                _error(404);
            }
            $child = $tuitionDao->reloadTuitionOfAChildInMonth($school['page_id'], $_POST['class_id'], $_POST['month'], $_POST['day_of_month'], $_POST['child_id']);
            if (is_null($child)) {
                $return['results'] = "";
            } else {
                $results = array();
                $results['month'] = $_POST['month'];

                $smarty->assign('childIdx', $_POST['child_idx']);
                $smarty->assign('results', $results);
                $smarty->assign('child', $child);
                $smarty->assign('attCount', $_POST['day_of_month']);

                $return['results'] = $smarty->fetch("ci/school/ajax.school.tuition.child.tpl");
            }
            return_json($return);

            break;
        case 'display_fee_2_add':
            if (!isset($_POST['child_id']) || !is_numeric($_POST['child_id'])) {
                _error(404);
            }

            $feeIdList = is_null($_POST['fee_id_list'])? array(): $_POST['fee_id_list'];
            $serviceIdList = is_null($_POST['service_id_list'])? array(): $_POST['service_id_list'];

            //Lấy ra danh sách tất cả loại phí của trường
            //TODO: sửa bởi memcache
            $tuitionDetails = $tuitionDao->getOnlyFees($school['page_id']); //Lấy tất cả ra xử trong chương trình cho đỡ DB server
            $feeList2Display = array();
            foreach ($tuitionDetails as $tuitionDetail) {
                if (!in_array($tuitionDetail['fee_id'], $feeIdList))
                $feeList2Display[] = $tuitionDetail;
            }
            $smarty->assign('fee_list', $feeList2Display);

            //TODO: sửa bởi memcache
            $services = $serviceDao->getActiveSchoolServices($school['page_id']);
            $serviceList2Display = array();
            foreach ($services as $tuitionDetail) {
                if (!in_array($tuitionDetail['service_id'], $serviceIdList)) {
                    $serviceList2Display[] = $tuitionDetail;
                }
            }

            //Kiểm tra đã có đón muộn chưa
            $hasPickup = (in_array('0', $serviceIdList));

            $smarty->assign('has_pickup', $hasPickup);
            $smarty->assign('service_list', $serviceList2Display);
            $smarty->assign('fee_id_list', implode(",", $feeIdList));
            $smarty->assign('service_id_list', implode(",", $serviceIdList));

            $smarty->assign('username', $_POST['school_username']);
            $smarty->assign('child_id', $_POST['child_id']);

            $popupHtml = $smarty->fetch("ci/school/ajax.school.tuition.child.popup.tpl");
            $return['results'] = $popupHtml;

            return_json($return);

            break;
        case 'service_usage':
            if (!isset($_POST['class_id']) || !is_numeric($_POST['class_id'])) {
                _error(404);
            }
            if (!isset($_POST['month']) || ($_POST['month'] == "")) {
                _error(404);
            }
            $children = $tuitionDao->getServiceOfClassInPreMonth($school['page_id'], $_POST['class_id'], $_POST['month']);
            //1. Tạo nên danh sách dịch vụ theo ĐIỂM DANH và theo LẦN
            $dailyHeaderServices = array();
            $cbHeaderServices  = array();
            $serviceIds = array();
            $childrenServices = array(); //Lưu mãng ID dịch vụ của tất cả các trẻ, dùng khi tạo ra bảng.
            foreach ($children as $child) {
                $childServiceIds = array(); //Lưu ID dịch vụ mà trẻ có dùng
                foreach ($child["daily_services"] as $service) {
                    if (!in_array($service["service_id"], $serviceIds)) {
                        $dailyHeaderServices[] = $service;
                        $serviceIds[] = $service["service_id"];
                    }
                    $childServiceIds[] = $service["service_id"];
                }
                foreach ($child["cbservices"] as $service) {
                    if (!in_array($service["service_id"], $serviceIds)) {
                        $cbHeaderServices[] = $service;
                        $serviceIds[] = $service["service_id"];
                    }
                    $childServiceIds[] = $service["service_id"];
                }
                $childrenServices[$child['child_id']] = $childServiceIds;
            }

            $smarty->assign('daily_services', $dailyHeaderServices);
            $smarty->assign('cbservices', $cbHeaderServices);
            $smarty->assign('children', $children);
            $smarty->assign('children_services', $childrenServices);
            $popupHtml = $smarty->fetch("ci/school/ajax.school.tuition.serviceusage.tpl");
            $return['results'] = $popupHtml;

            return_json($return);

            break;
        case 'add_fee_to_tuition':
            if (!isset($_POST['child_id']) || !is_numeric($_POST['child_id'])) {
                _error(404);
            }

            $existingFeeIds = (isset($_POST['fee_id_list']) && ($_POST['fee_id_list'] != ''))? explode(",", $_POST['fee_id_list']) : array();
            $existingServiceIds = (isset($_POST['service_id_list']) && ($_POST['service_id_list'] != ''))? explode(",", $_POST['service_id_list']) : array();

            $smarty->assign('child_id', $_POST['child_id']);

            $strFee = "";
            $selectedFeeIds = (isset($_POST['fee_ids']) && (count($_POST['fee_ids']) > 0))? $_POST['fee_ids'] : array();
            $changeAmount = 0;
            if (count($selectedFeeIds) > 0) {
                $tuitionDetails = $tuitionDao->getFeesInList($school['page_id'], $selectedFeeIds);
                $feeIdx = 1;
                foreach ($tuitionDetails as $tuitionDetail) {
                    $smarty->assign('fee', $tuitionDetail);
                    //$smarty->assign('idx', count($existingFeeIds) + $feeIdx++);
                    $strFee = $strFee.$smarty->fetch("ci/school/ajax.school.tuition.child.fee.tpl");
                    if ($tuitionDetail['type'] == FEE_TYPE_MONTHLY) {
                        $changeAmount = $changeAmount + $tuitionDetail['unit_price'];
                    }
                }
            }

            $strService = "";
            $selectedServiceIds = (isset($_POST['service_ids']) && (count($_POST['service_ids']) > 0))? $_POST['service_ids'] : array();
            if (count($selectedServiceIds) > 0) {
                $services = $serviceDao->getServicesInList($school['page_id'], $selectedServiceIds);
                $serviceIdx = 1;
                foreach($services as $tuitionDetail) {
                    $smarty->assign('service', $tuitionDetail);
                    //$smarty->assign('idx', count($existingServiceIds) + $serviceIdx++);
                    $strService = $strService.$smarty->fetch("ci/school/ajax.school.tuition.child.service.tpl");

                    if ($tuitionDetail['type'] == SERVICE_TYPE_MONTHLY) {
                        $changeAmount = $changeAmount + $tuitionDetail['fee'];
                    }
                }
            }
            if (isset($_POST['pickup']) && (strcmp($_POST['pickup'], "true") == 0)) {
                $strService = $strService . $smarty->fetch("ci/school/ajax.school.tuition.child.pickup.tpl");
            }
            $return['str_fee'] = $strFee;
            $return['str_service'] = $strService;
            $return['change_amount'] = $changeAmount;

            return_json($return);

            break;
        case 'remove':
            if (!$canEdit) {
                _error(403);
            }
            $db->begin_transaction();

            $tuitionDao->deleteTuition($_POST['id']);
            $db->commit();
            // return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/school/' . $_POST['school_username'] . '/tuitions";'));
            return_json( array('success' => true, 'message' => __("Success")));

            break;
        case 'export':
            $reportTemplate = ABSPATH."content/templates/tuition_a_child_".$school['tuition_child_report_template']."_".$system['language']['code'].".xlsx";
            include_once(ABSPATH . 'includes/libs/PHPExcel/PHPExcel/IOFactory.php');
            include_once(ABSPATH . 'includes/libs/PHPExcel/PHPExcel.php');

            // Lấy thông tin học phí
            $data = $tuitionDao->getTuition4ChildInMonth($_POST['id'], $_POST['child_id']);
            if (is_null($data)) {
                _error(404);
            }

            $objPHPExcel = PHPExcel_IOFactory::load($reportTemplate);

            processOneSheetContent($objPHPExcel, $data['tuition_child'], $school, $data, "Tuition");

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            ob_start();
            $month = str_replace('/', '_', $data['month']);
            $childName = convert_vi_to_en($data['tuition_child']["child_name"]);
            $childName = str_replace(" ", '_', $childName);
            $file_name = convertText4Web($childName . "_" . '_hocphi_' . $month . '.xlsx');
            $objWriter->save("php://output");

            $xlsData = ob_get_contents();
            ob_end_clean();
            return_json(array(
                'file_name' => $file_name,
                'file' => "data:application/vnd.ms-excel;base64," . base64_encode($xlsData)
            ));

            break;
        case 'export_all':
            $reportTemplate = ABSPATH."content/templates/tuition_whole_class_".$school['tuition_child_report_template']."_".$system['language']['code'].".xlsx";
            include_once(ABSPATH . 'includes/libs/PHPExcel/PHPExcel/IOFactory.php');
            include_once(ABSPATH . 'includes/libs/PHPExcel/PHPExcel.php');

            // Lấy thông tin học phí
            $data = $tuitionDao->getTuition($_POST['id']);
            if (is_null($data)) {
                _error(404);
            }

            $objPHPExcel = PHPExcel_IOFactory::load($reportTemplate);
            //Xử lý sheet cả lớp.
            processFirstSheetContent($objPHPExcel->getSheet(0), $school, $data);

            $childSheet = $objPHPExcel->getSheet(1);
            $childCnt = 1;
            foreach ($data['tuition_child'] as $child) {
                $sheetName = $childCnt . "_" . convert_vi_to_en(convertText4Web($child['child_name']));
                //Tạo một sheet học phí cho mỗi trẻ.
                $sheet = clone $childSheet;
                $sheet->setTitle($sheetName);
                $objPHPExcel->addSheet($sheet);
                processOneSheetContent($objPHPExcel, $child, $school, $data, $sheetName);

                unset($sheet);
                $childCnt++;
            }
            unset($childSheet);

            //Xóa sheet thứ 2 đi.
            $objPHPExcel->removeSheetByIndex(1);

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            ob_start();
            $month = str_replace('/', '_', $data['month']);
            $childName = convert_vi_to_en($data["group_title"]);
            $childName = str_replace(" ", '_', $childName);
            $file_name = convertText4Web($childName . "_" . $data['class_id'] . '_hocphi_' . $month . '.xlsx');
            $objWriter->save("php://output");

            $xlsData = ob_get_contents();
            ob_end_clean();
            return_json(array(
                'file_name' => $file_name,
                'file' => "data:application/vnd.ms-excel;base64," . base64_encode($xlsData)
            ));

            break;
        case 'summary_paid_search':
        	if($_POST['status'] != 2) {
        		$_POST['cashier_id'] = '';
        	}
            $children = $tuitionDao->getTuitionSummaryPaid($school['page_id'], $_POST['fromDate'], $_POST['toDate'], $_POST['cashier_id'], $_POST['status'], $_POST['class_id']);

            $smarty->assign('children', $children);
            $smarty->assign('school', $school);
            $smarty->assign('username', $_POST['school_username']);
            if($_POST['status'] == TUITION_CHILD_CONFIRMED) {
                // Đã thanh toán thì trả về file paid
                $return['results'] = $smarty->fetch("ci/school/ajax.school.tuition.summarypaid.tpl");
            } else {
                // Chưa thanh toán và đang chờ xác nhận thì trả về file not paid
                $return['results'] = $smarty->fetch("ci/school/ajax.school.tuition.summarynotpaid.tpl");
            }

            return_json($return);
            break;
        case 'export_summary':
            if($_POST['status'] == 2) {
                $reportTemplate = ABSPATH."content/templates/tuition_summary_paid.xlsx";
            } else {
                $reportTemplate = ABSPATH."content/templates/tuition_summary_not_paid.xlsx";
                $_POST['cashier_id'] = '';
            }
            include_once(ABSPATH . 'includes/libs/PHPExcel/PHPExcel/IOFactory.php');
            include_once(ABSPATH . 'includes/libs/PHPExcel/PHPExcel.php');

            // Lấy thông tin thanh toán học phí
            $children = $tuitionDao->getTuitionSummaryPaid($school['page_id'], $_POST['fromDate'], $_POST['toDate'], $_POST['cashier_id'], $_POST['status'], $_POST['class_id']);

            $objPHPExcel = PHPExcel_IOFactory::load($reportTemplate);


            $sheet = $objPHPExcel->getSheet(0);

            $all_border_style = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('rgb' => '333333'))));

            if($_POST['status'] == 2) {
                //Set thông tin trường/lớp và trẻ ở đầu bảng học phí.
                $sheet->setCellValue('A2', mb_strtoupper(convertText4Web(str_replace('/', '_', $school['page_title'])), "UTF-8"))
                    ->setCellValue('A3', __("From") . ' ' . $_POST['fromDate'] . ' ' . __("To") . ' ' . $_POST['toDate']);

                $firstRow = 6;
                $rowNumber = $firstRow;
                // Điền thông tin thanh toán học phí của trẻ
                $idx = 1;
                $classId = -1;
                $total = 0;
                $totalPaid = 0;
                $totalDebt = 0;
                foreach ($children as $child) {
                    if($classId != $child['class_id']) {
                        $sheet->mergeCells('A' . $rowNumber . ':H' . $rowNumber);
                        if($child['class_id'] > 0) {
                            $sheet->setCellValue('A' . $rowNumber, convertText4Web($child['group_title']));
                        } else {
                            $sheet->setCellValue('A' . $rowNumber, __("No class"));
                        }
                        $rowNumber++;
                    }
                    $sheet->setCellValue('A' . $rowNumber, $idx)
                        ->setCellValue('B' . $rowNumber, convertText4Web($child['child_name']))
                        ->setCellValue('C' . $rowNumber, $child['total_amount'])
                        ->setCellValue('D' . $rowNumber, $child['paid_amount'])
                        ->setCellValue('E' . $rowNumber, $child['total_amount'] - $child['paid_amount'])
                        ->setCellValue('F' . $rowNumber, $child['month'])
                        ->setCellValue('G' . $rowNumber, convertText4Web($child['user_fullname']))
                        ->setCellValue('H' . $rowNumber, $child['paid_at']);
                    $sheet->getStyle('F' . $rowNumber)->applyFromArray(styleArray('Times new Roman', false, false, 11, PHPExcel_Style_Alignment::HORIZONTAL_CENTER, PHPExcel_Style_Alignment::VERTICAL_CENTER, true));
                    $sheet->getStyle('A' . $rowNumber)->applyFromArray(styleArray('Times new Roman', true, false, 11, PHPExcel_Style_Alignment::HORIZONTAL_CENTER, PHPExcel_Style_Alignment::VERTICAL_CENTER, true));
                    $sheet->getStyle('H' . $rowNumber)->applyFromArray(styleArray('Times new Roman', false, false, 11, PHPExcel_Style_Alignment::HORIZONTAL_CENTER, PHPExcel_Style_Alignment::VERTICAL_CENTER, true));
                    $classId = $child['class_id'];
                    $rowNumber++;
                    $idx++;
                    $debt = $child['total_amount'] - $child['paid_amount'];
                    $total = $total + $child['total_amount'];
                    $totalPaid = $totalPaid + $child['paid_amount'];
                    $totalDebt = $totalDebt + $debt;
                }
                $sheet->mergeCells('A' . $rowNumber . ':B' . $rowNumber);
                $sheet->setCellValue('A' . $rowNumber, __("Total"))
                    ->setCellValue('C' . $rowNumber, $total)
                    ->setCellValue('D' . $rowNumber, $totalPaid)
                    ->setCellValue('E' . $rowNumber, $totalDebt);
                $rowNumber++;
                $endRow = $rowNumber - 1;
                //Set boarder cho toàn bộ bảng
                $sheet->getStyle('A6:' . 'H' . $endRow)->applyFromArray($all_border_style);
                $sheet->getStyle('B6:' . 'E' . $endRow)->applyFromArray(styleArray('Times new Roman', false, false, 11, PHPExcel_Style_Alignment::HORIZONTAL_GENERAL, PHPExcel_Style_Alignment::VERTICAL_CENTER, true));
                $sheet->getStyle('G6:' . 'G' . $endRow)->applyFromArray(styleArray('Times new Roman', false, false, 11, PHPExcel_Style_Alignment::HORIZONTAL_GENERAL, PHPExcel_Style_Alignment::VERTICAL_CENTER, true));
                $sheet->getStyle('A' . $endRow . ':E' . $endRow)->applyFromArray(styleArray('Times new Roman', true, false, 11, PHPExcel_Style_Alignment::HORIZONTAL_RIGHT, PHPExcel_Style_Alignment::VERTICAL_CENTER, true));

                // Ngày tháng
                $rowNumber = $rowNumber + 1;
                $sheet->mergeCells('A' . $rowNumber . ':H' . $rowNumber);
                $sheet->getStyle('A' . $rowNumber)->applyFromArray(styleArray('Times new Roman', false, true, 11, PHPExcel_Style_Alignment::HORIZONTAL_RIGHT, PHPExcel_Style_Alignment::VERTICAL_CENTER, true));
                $sheet->setCellValue('A' . $rowNumber, '..........., ' . __('day') . ' .... ' . __('month') . ' .... ' . __('year') . ' ...... ');

                // Người lập đơn
                $rowNumber++;
                $sheet->mergeCells('E' . $rowNumber . ':H' . $rowNumber);
                $sheet->getStyle('E' .$rowNumber)->applyFromArray(styleArray('Times new Roman', true, false, 11, PHPExcel_Style_Alignment::HORIZONTAL_CENTER, PHPExcel_Style_Alignment::VERTICAL_CENTER, true));
                $sheet->setCellValue('E' . $rowNumber, mb_strtoupper(__('Issuer')));
                unset($sheet);
                $teacher_name = '';
                if($_POST['cashier_id'] != '') {
                    $user = getTeacherData($_POST['cashier_id'], TEACHER_INFO);
                    $teacher_name = $user['user_fullname'] . '_';
                }
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
                ob_start();
                $file_name = convertText4Web(__("Summary tuition paid") . "_" . $teacher_name . $_POST['fromDate'] . "_" . $_POST["toDate"] . '.xlsx');
            } else {
                //Set thông tin trường/lớp và trẻ ở đầu bảng học phí.
                $sheet->setCellValue('A2', mb_strtoupper(convertText4Web(str_replace('/', '_', $school['page_title'])), "UTF-8"));
                if($_POST['status'] == TUITION_CHILD_NOT_PAID) {
                    $sheet->setCellValue('A1', mb_strtoupper(convertText4Web(__("DS trẻ chưa thanh toán học phí")), "UTF-8"));
                } else {
                    $sheet->setCellValue('A1', mb_strtoupper(convertText4Web(__("DS trẻ đang chờ xác nhận đã nộp học phí")), "UTF-8"));
                }

                $firstRow = 5;
                $rowNumber = $firstRow;
                // Điền thông tin thanh toán học phí của trẻ
                $idx = 1;
                $classId = -1;
                $total = 0;
                foreach ($children as $child) {
                    if($classId != $child['class_id']) {
                        $sheet->mergeCells('A' . $rowNumber . ':D' . $rowNumber);
                        if($child['class_id'] > 0) {
                            $sheet->setCellValue('A' . $rowNumber, convertText4Web($child['group_title']));
                        } else {
                            $sheet->setCellValue('A' . $rowNumber, __("No class"));
                        }
                        $rowNumber++;
                    }
                    $sheet->setCellValue('A' . $rowNumber, $idx)
                        ->setCellValue('B' . $rowNumber, convertText4Web($child['child_name']))
                        ->setCellValue('C' . $rowNumber, $child['total_amount'])
                        ->setCellValue('D' . $rowNumber, $child['month']);
                    $sheet->getStyle('D' . $rowNumber)->applyFromArray(styleArray('Times new Roman', false, false, 11, PHPExcel_Style_Alignment::HORIZONTAL_CENTER, PHPExcel_Style_Alignment::VERTICAL_CENTER, true));
                    $sheet->getStyle('A' . $rowNumber)->applyFromArray(styleArray('Times new Roman', true, false, 11, PHPExcel_Style_Alignment::HORIZONTAL_CENTER, PHPExcel_Style_Alignment::VERTICAL_CENTER, true));
                    $classId = $child['class_id'];
                    $rowNumber++;
                    $idx++;
                    $total = $total + $child['total_amount'];
                }
                $sheet->mergeCells('A' . $rowNumber . ':B' . $rowNumber);
                $sheet->setCellValue('A' . $rowNumber, __("Total"))
                    ->setCellValue('C' . $rowNumber, $total);
                $rowNumber++;
                $endRow = $rowNumber - 1;
                //Set boarder cho toàn bộ bảng
                $sheet->getStyle('A5:' . 'D' . $endRow)->applyFromArray($all_border_style);
                $sheet->getStyle('B5:' . 'C' . $endRow)->applyFromArray(styleArray('Times new Roman', false, false, 11, PHPExcel_Style_Alignment::HORIZONTAL_GENERAL, PHPExcel_Style_Alignment::VERTICAL_CENTER, true));
                $sheet->getStyle('A' . $endRow . ':D' . $endRow)->applyFromArray(styleArray('Times new Roman', true, false, 11, PHPExcel_Style_Alignment::HORIZONTAL_RIGHT, PHPExcel_Style_Alignment::VERTICAL_CENTER, true));

                // Ngày tháng
                $rowNumber = $rowNumber + 1;
                $sheet->mergeCells('A' . $rowNumber . ':D' . $rowNumber);
                $sheet->getStyle('A' . $rowNumber)->applyFromArray(styleArray('Times new Roman', false, true, 11, PHPExcel_Style_Alignment::HORIZONTAL_RIGHT, PHPExcel_Style_Alignment::VERTICAL_CENTER, true));
                $sheet->setCellValue('A' . $rowNumber, '..........., ' . __('day') . ' .... ' . __('month') . ' .... ' . __('year') . ' ...... ');

                // Người lập đơn
                $rowNumber++;
                $sheet->mergeCells('C' . $rowNumber . ':D' . $rowNumber);
                $sheet->getStyle('C' .$rowNumber)->applyFromArray(styleArray('Times new Roman', true, false, 11, PHPExcel_Style_Alignment::HORIZONTAL_CENTER, PHPExcel_Style_Alignment::VERTICAL_CENTER, true));
                $sheet->setCellValue('C' . $rowNumber, mb_strtoupper(__('Issuer')));
                unset($sheet);
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
                ob_start();
                if($_POST['status'] == TUITION_CHILD_NOT_PAID) {
                    $file_name = __("Danh sách chưa nộp học phí") . '.xlsx';
                } else {
                    $file_name = __("Danh sách đang chờ xác nhận") . '.xlsx';
                }
            }

            $objWriter->save("php://output");

            $xlsData = ob_get_contents();
            ob_end_clean();
            return_json(array(
                'file_name' => $file_name,
                'file' => "data:application/vnd.ms-excel;base64," . base64_encode($xlsData)
            ));
            break;
        default:
            _error(400);
            break;
    }
} catch (Exception $e) {
    $db->rollback();
    return_json(array('error' => true, 'message' => $e->getMessage()));
} finally {
    $db->autocommit(true);
    if (!is_null($objPHPExcel)) {
        $objPHPExcel->disconnectWorksheets();
        //$objPHPExcel->__destruct();
        unset($objPHPExcel);
    }
}

/**
 * Lấy thông tin học phí chi tiết của một trẻ trên màn hình.
 *
 * @param $childId
 * @return array
 */
function getTuitionDetailData($childId) {
    //Thông tin phí tính cho tháng này
    $feeIds = is_null($_POST['fee_id_'.$childId])? array(): $_POST['fee_id_'.$childId];
    $feeQuantities = is_null($_POST['fee_quantity_'.$childId])? array(): $_POST['fee_quantity_'.$childId];
    $feePrices = is_null($_POST['fee_unit_price_'.$childId])? array(): $_POST['fee_unit_price_'.$childId];
    $feeDeductionQties = is_null($_POST['fee_quantity_deduction_'.$childId])? array(): $_POST['fee_quantity_deduction_'.$childId];
    $feeDeductionPrices = is_null($_POST['fee_unit_price_deduction_'.$childId])? array(): $_POST['fee_unit_price_deduction_'.$childId];

    $tuitionDetails = array(); //Lưu các tuition_detail
    for ($idx = 0; $idx < count($feeIds); $idx++) {
        $tuitionDetail = array();
        $tuitionDetail['service_id'] = 0;
        $tuitionDetail['fee_id'] = $feeIds[$idx];
        $tuitionDetail['quantity'] = $feeQuantities[$idx];
        $tuitionDetail['unit_price'] = str_replace(',', '', $feePrices[$idx]);
        $tuitionDetail['unit_price_deduction'] = str_replace(',', '', $feeDeductionPrices[$idx]);
        $tuitionDetail['type'] = TUITION_DETAIL_FEE;
        $tuitionDetail['service_type'] = 0;
        $tuitionDetail['quantity_deduction'] = $feeDeductionQties[$idx];
        if (($tuitionDetail['quantity'] * $tuitionDetail['unit_price'] != 0) || ($tuitionDetail['quantity_deduction'] * $tuitionDetail['unit_price_deduction'] != 0)) {
            $tuitionDetails[] = $tuitionDetail;
        }
    }

    // Danh sách dịch vụ
    $usePickup = isset($_POST['pickup_'.$childId]) ? true : false;

    $serviceIds = is_null($_POST['service_id_'.$childId])? array(): $_POST['service_id_'.$childId];
    $serviceQuantities = is_null($_POST['service_quantity_'.$childId])? array(): $_POST['service_quantity_'.$childId];
    $servicePrices = is_null($_POST['service_unit_price_'.$childId])? array(): $_POST['service_unit_price_'.$childId];
    $serviceTypes = is_null($_POST['service_type_'.$childId])? array(): $_POST['service_type_'.$childId];
    $serviceDeductionQties = is_null($_POST['service_quantity_deduction_'.$childId])? array(): $_POST['service_quantity_deduction_'.$childId];
    $serviceDeductionPrices = is_null($_POST['service_unit_price_deduction_'.$childId])? array(): $_POST['service_unit_price_deduction_'.$childId];
    for ($idx = 0; $idx < count($serviceIds); $idx++) {
        $tuitionDetail = array();
        $tuitionDetail['service_id'] = $serviceIds[$idx];
        $tuitionDetail['fee_id'] = 0;
        $tuitionDetail['quantity'] = $serviceQuantities[$idx];
        $tuitionDetail['unit_price'] = str_replace(',', '', $servicePrices[$idx]);
        $tuitionDetail['unit_price_deduction'] = str_replace(',', '', $serviceDeductionPrices[$idx]);
        if ($serviceIds[$idx] > 0) {
            $tuitionDetail['type'] = TUITION_DETAIL_SERVICE;
            $tuitionDetail['service_type'] = $serviceTypes[$idx];
        } else if ($usePickup) {
            $tuitionDetail['type'] = LATE_PICKUP_FEE;
            $tuitionDetail['service_type'] = 0;
        }
        $tuitionDetail['quantity_deduction'] = $serviceDeductionQties[$idx];

        if (($tuitionDetail['quantity'] * $tuitionDetail['unit_price'] != 0) || ($tuitionDetail['quantity_deduction'] * $tuitionDetail['unit_price_deduction'] != 0)) {
            $tuitionDetails[] = $tuitionDetail;
        }
    }

    return $tuitionDetails;
}

/**
 * Hàm xử lý sheet danh sách cả lớp.
 *
 * @param $sheet
 * @param $school
 * @param $data
 */
function processFirstSheetContent($sheet, $school, $data)
{
    $COLUMN_OF_EACH_TYPE = 10;
    $START_ROW = 7;
    $daoTuition = new TuitionDAO();

    //1. Tạo nên danh sách các phí và dịch vụ (tổng hợp danh sách dịch vụ/phí có xuất hiện trong cả lớp), để tạo nên các CỘT
    $monthlyFeeColumns = array();
    $dailyFeeColumns = array();
    $feeIds = array();
    $serviceIds = array();
    //1.1 Xử lý với các loại phí trước (chưa xử lý phí cấp trẻ)
    foreach ($data['tuition_child'] as $child) { //Duyệt danh sách trẻ của lớp
        foreach ($child['tuition_detail'] as $detail) { //Duyệt danh sách phí
            if ($detail['type'] == TUITION_DETAIL_FEE && ($detail['level'] != CHILD_LEVEL)) {
                if (!in_array($detail['fee_id'], $feeIds)) { //Nếu chưa có trong danh sách dịch vụ
                    $column = array();
                    $column['fee_id'] = $detail['fee_id'];
                    $column['title'] = $detail['fee_name'];
                    if ($detail['fee_type'] == FEE_TYPE_MONTHLY) {
                        $monthlyFeeColumns[] = $column;
                    } else {
                        $dailyFeeColumns[] = $column;
                    }
                    $feeIds[] = $detail['fee_id'];
                }
            }
        }
    }
    $replacedPairs = array(); //Lưu cặp ID của phí và phí thay thế: ID => Replaced ID
    //1.2. Xử lý phí cấp TRẺ sau khi xử lý xong phí khác
    foreach ($data['tuition_child'] as $child) { //Duyệt danh sách trẻ của lớp
        foreach ($child['tuition_detail'] as $detail) { //Duyệt danh sách dịch vụ từng trẻ
            if (($detail['type'] == TUITION_DETAIL_FEE) && ($detail['level'] == CHILD_LEVEL)) {
                $replacedFees = $daoTuition->getReplacedFees($detail['fee_id']);
                $found = false;
                if (count($replacedFees) > 0) {
                    foreach ($replacedFees as $replacedFee) {
                        if (in_array($replacedFee['fee_id'], $feeIds)) { //Nếu tìm ra phí thay thế trong danh sách phí của lớp
                            //Lưu lại giá trị phí thay thế cho cột hiện tại
                            $replacedPairs[$detail['fee_id']] = $replacedFee['fee_id'];
                            $found = true;
                            break;
                        }
                    }
                }
                //Trường hợp phí cấp trẻ nhưng không thay thế cho phí nào khác HOẶC không tìm thấy trong danh sách phí của lớp => Thêm cột mới
                if ((!$found) && (!in_array($detail['fee_id'], $feeIds))) {
                    $column = array();
                    $column['fee_id'] = $detail['fee_id'];
                    $column['title'] = $detail['fee_name'];
                    if ($detail['fee_type'] == FEE_TYPE_MONTHLY) {
                        $monthlyFeeColumns[] = $column;
                    } else {
                        $dailyFeeColumns[] = $column;
                    }
                    $feeIds[] = $detail['fee_id'];
                }
            }
        }
    }

    //1.3. Xử lý với các Dịch vụ
    $monthlyServiceColumns = array();
    $dailyServiceColumns = array();
    $cbServiceColumns = array();
    foreach ($data['tuition_child'] as $child) { //Duyệt danh sách trẻ của lớp
        foreach ($child['tuition_detail'] as $detail) { //Duyệt danh sách dịch vụ từng trẻ
            if ($detail['type'] == TUITION_DETAIL_SERVICE) {
                if (!in_array($detail['service_id'], $serviceIds)) {
                    $column = array();
                    $column['service_id'] = $detail['service_id'];
                    $column['title'] = $detail['service_name'];
                    if ($detail['service_type'] == SERVICE_TYPE_MONTHLY) {
                        $monthlyServiceColumns[] = $column;
                    } else if ($detail['service_type'] == SERVICE_TYPE_DAILY) {
                        $dailyServiceColumns[] = $column;
                    } else {
                        $cbServiceColumns[] = $column;
                    }

                    $serviceIds[] = $detail['service_id'];
                }
            }
        }
    }

    $newColumns = array();
    //2. Điền thông tin vào các header các cột
    //2.1. Điền các phí/dịch vụ theo THÁNG
    $col = 3;
    foreach ($monthlyFeeColumns as $column) {
        $sheet->setCellValueByColumnAndRow($col, 5, convertText4Web($column['title']));
        $column['col'] = $col;
        $newColumns[TUITION_DETAIL_FEE . "_" . $column['fee_id']] = $column;
        $col = $col + 1;
    }
    foreach ($monthlyServiceColumns as $column) {
        $sheet->setCellValueByColumnAndRow($col, 5, convertText4Web($column['title']));
        $column['col'] = $col;
        $newColumns[TUITION_DETAIL_SERVICE . "_" . $column['service_id']] = $column;
        $col = $col + 1;
    }
    //2.2. Điền các phí/dịch vụ theo ĐIỂM DANH
    $col = 3 + $COLUMN_OF_EACH_TYPE;
    foreach ($dailyFeeColumns as $column) {
        $strFromCol = PHPExcel_Cell::stringFromColumnIndex($col);
        $strEndCol = PHPExcel_Cell::stringFromColumnIndex($col + 1);
        $sheet->mergeCells($strFromCol . 5 . ':' . $strEndCol . 5);

        $sheet->setCellValueByColumnAndRow($col, 5, convertText4Web($column['title']));
        $column['col'] = $col;
        $newColumns[TUITION_DETAIL_FEE . "_" . $column['fee_id']] = $column;
        $col = $col + 2;
    }
    foreach ($dailyServiceColumns as $column) {
        $strFromCol = PHPExcel_Cell::stringFromColumnIndex($col);
        $strEndCol = PHPExcel_Cell::stringFromColumnIndex($col + 1);
        $sheet->mergeCells($strFromCol . 5 . ':' . $strEndCol . 5);

        $sheet->setCellValueByColumnAndRow($col, 5, convertText4Web($column['title']));
        $column['col'] = $col;
        $newColumns[TUITION_DETAIL_SERVICE . "_" . $column['service_id']] = $column;
        $col = $col + 2;
    }
    //2.3. Điền các dịch vụ theo LẦN
    $col = 3 + $COLUMN_OF_EACH_TYPE * 3;
    foreach ($cbServiceColumns as $column) {
        $strFromCol = PHPExcel_Cell::stringFromColumnIndex($col);
        $strEndCol = PHPExcel_Cell::stringFromColumnIndex($col + 1);
        $sheet->mergeCells($strFromCol . 5 . ':' . $strEndCol . 5);

        $sheet->setCellValueByColumnAndRow($col, 5, convertText4Web($column['title']));
        $column['col'] = $col;
        $newColumns[TUITION_DETAIL_SERVICE . "_" . $column['service_id']] = $column;
        $col = $col + 2;
    }

    $childCnt = 1;
    $rowNumber = $START_ROW;
    foreach ($data['tuition_child'] as $child) { //Điền dữ liệu của từng trẻ một
        //Điền tên trẻ
        $sheetName = $childCnt . "_" . convert_vi_to_en(convertText4Web($child['child_name']));
        $sheet->setCellValue('A' . $rowNumber, $childCnt)
            ->setCellValue('B' . $rowNumber, convertText4Web($child['child_name']))
            ->setCellValue('C' . $rowNumber, $child['total_amount']);
        $sheet->getHyperlink('B' . $rowNumber)->setUrl("sheet://'" . $sheetName . "'!A1");
        $childCnt++;

        foreach ($child['tuition_detail'] as $detail) {
            if (($detail['type'] == TUITION_DETAIL_FEE) && ($detail['level'] != CHILD_LEVEL)) {
                $column = $newColumns[TUITION_DETAIL_FEE . "_" . $detail['fee_id']];
                if ($detail['fee_type'] == FEE_TYPE_MONTHLY) {
                    $sheet->setCellValueByColumnAndRow($column['col'], $rowNumber, ($detail['unit_price'] - $detail['unit_price_deduction']));
                } else {
                    $sheet->setCellValueByColumnAndRow($column['col'], $rowNumber, ($detail['quantity']."-".$detail['quantity_deduction']));
                    $sheet->setCellValueByColumnAndRow($column['col'] + 1, $rowNumber, ($detail['quantity']  * $detail['unit_price'] - $detail['quantity_deduction']  * $detail['unit_price_deduction']));
                }
            } else if (($detail['type'] == TUITION_DETAIL_FEE) && ($detail['level'] == CHILD_LEVEL)) {
                $feeId = $detail['fee_id'];
                $replacedId = $replacedPairs[$feeId];
                if (isset($replacedId) && ($replacedId > 0)) {
                    $feeId = $replacedId;
                }
                $column = $newColumns[TUITION_DETAIL_FEE . "_" . $feeId];
                if ($detail['fee_type'] == FEE_TYPE_MONTHLY) {
                    $sheet->setCellValueByColumnAndRow($column['col'], $rowNumber, $detail['unit_price']);
                } else {
                    $sheet->setCellValueByColumnAndRow($column['col'], $rowNumber, ($detail['quantity']."-".$detail['quantity_deduction']));
                    $sheet->setCellValueByColumnAndRow($column['col'] + 1, $rowNumber, ($detail['quantity']  * $detail['unit_price'] - $detail['quantity_deduction']  * $detail['unit_price_deduction']));
                }
            } else if ($detail['type'] == TUITION_DETAIL_SERVICE) {
                $column = $newColumns[TUITION_DETAIL_SERVICE . "_" . $detail['service_id']];
                if ($detail['service_type'] == SERVICE_TYPE_MONTHLY) {
                    $sheet->setCellValueByColumnAndRow($column['col'], $rowNumber, $detail['unit_price']);
                } else if ($detail['service_type'] == SERVICE_TYPE_DAILY) {
                    $sheet->setCellValueByColumnAndRow($column['col'], $rowNumber, ($detail['quantity']."-".$detail['quantity_deduction']));
                    $sheet->setCellValueByColumnAndRow($column['col'] + 1, $rowNumber, ($detail['quantity']  * $detail['unit_price'] - $detail['quantity_deduction']  * $detail['unit_price_deduction']));
                } else {
                    if ($detail['quantity_deduction'] == 0) {
                        $sheet->setCellValueByColumnAndRow($column['col'], $rowNumber, $detail['quantity']);
                    } else {
                        $sheet->setCellValueByColumnAndRow($column['col'], $rowNumber, ($detail['quantity']."-".$detail['quantity_deduction']));
                    }
                    $sheet->setCellValueByColumnAndRow($column['col'] + 1, $rowNumber, ($detail['quantity']  * $detail['unit_price'] - $detail['quantity_deduction']  * $detail['unit_price_deduction']));
                }
            } else if ($detail['type'] == LATE_PICKUP_FEE) {
                $sheet->setCellValueByColumnAndRow(($COLUMN_OF_EACH_TYPE * 5 + 3), $rowNumber, $detail['unit_price']);
            }
        }
        if ($child['debt_amount'] != 0) {
            $sheet->setCellValueByColumnAndRow(($COLUMN_OF_EACH_TYPE * 5 + 4), $rowNumber, $child['debt_amount']); //Nợ tháng trước
        }
        if (!is_empty($child['description'])) {
            $sheet->setCellValueByColumnAndRow(($COLUMN_OF_EACH_TYPE * 5 + 5), $rowNumber, convertText4Web($child['description']));
        }

        $rowNumber++;
    }

    //3. Xóa các cột bị thừa
    //3.1 Xóa cột dịch vụ THEO LẦN thừa
    $start = 2 + $COLUMN_OF_EACH_TYPE * 3 + count($cbServiceColumns) * 2;
    $end = 2 + $COLUMN_OF_EACH_TYPE * 5;
    for ($idx = $end; $idx > $start; $idx--) {
        $sheet->removeColumnByIndex($idx);
    }
    //3.2 Xóa cột phí/dịch vụ theo ĐIỂM DANH
    $start = 2 + $COLUMN_OF_EACH_TYPE + (count($dailyFeeColumns) + count($dailyServiceColumns)) * 2;
    $end = 2 + $COLUMN_OF_EACH_TYPE * 3;
    for ($idx = $end; $idx > $start; $idx--) {
        $sheet->removeColumnByIndex($idx);
    }

    //3.3 Xóa cột phí/dịch vụ theo ĐIỂM DANH
    $start = 2 + count($monthlyFeeColumns) + count($monthlyServiceColumns);
    $end = 2 + $COLUMN_OF_EACH_TYPE -1;
    for ($idx = $end; $idx > $start; $idx--) {
        $sheet->removeColumnByIndex($idx);
    }

    $lastColIdx = 7 + count($monthlyFeeColumns) + count($monthlyServiceColumns) + 2*(count($dailyFeeColumns) + count($dailyServiceColumns) + count($cbServiceColumns));
    $strEndCol = PHPExcel_Cell::stringFromColumnIndex($lastColIdx);
    $sheet->mergeCells('A1:' . $strEndCol . 1);
    $sheet->setCellValue('A1', mb_strtoupper(convertText4Web(str_replace('/', '_', $school['page_title'])), "UTF-8"))
        ->setCellValue('C2', convertText4Web($data['month']))->setCellValue('C3', convertText4Web($data['group_title']));
    $sheet->getStyle('A1')->applyFromArray(styleArray('Times new Roman', true, false, 16, PHPExcel_Style_Alignment::HORIZONTAL_CENTER, PHPExcel_Style_Alignment::VERTICAL_CENTER, true));

    //Total
    $sheet->mergeCells('A' . $rowNumber . ':B' . $rowNumber);
    $sheet->getStyle('A' . $rowNumber . ":C" . $rowNumber)->applyFromArray(styleArray('Times new Roman', true, false, 11, PHPExcel_Style_Alignment::HORIZONTAL_RIGHT, PHPExcel_Style_Alignment::VERTICAL_CENTER, true));
    $sheet->setCellValue('A' . $rowNumber, __('Total'))->setCellValue('C' . $rowNumber, $data['total_amount']);

    //Set boarder cho toàn bộ bảng
    $all_border_style = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('rgb' => '333333'))));
    $sheet->getStyle('A7:'.$strEndCol.$rowNumber)->applyFromArray($all_border_style);

    // Ngày tháng
    $rowNumber = $rowNumber + 2;
    $sheet->mergeCells('A' . $rowNumber . ':'. $strEndCol . $rowNumber);
    $sheet->getStyle('A' . $rowNumber)->applyFromArray(styleArray('Times new Roman', false, true, 11, PHPExcel_Style_Alignment::HORIZONTAL_RIGHT, PHPExcel_Style_Alignment::VERTICAL_CENTER, true));
    $sheet->setCellValue('A' . $rowNumber, '..........., ' . __('day') . ' .... ' . __('month') . ' .... ' . __('year') . ' ...... ');

    // Người lập đơn
    $rowNumber++;
    $strFromCol = PHPExcel_Cell::stringFromColumnIndex($lastColIdx - 3);
    $sheet->mergeCells($strFromCol . $rowNumber . ':'. $strEndCol . $rowNumber);
    $sheet->getStyle($strFromCol.$rowNumber)->applyFromArray(styleArray('Times new Roman', true, false, 11, PHPExcel_Style_Alignment::HORIZONTAL_CENTER, PHPExcel_Style_Alignment::VERTICAL_CENTER, true));
    $sheet->setCellValue($strFromCol . $rowNumber, mb_strtoupper(__('Issuer')));

    unset($sheet);
}

/**
 * Hàm xử lý một sheet học phí của 1 trẻ.
 *
 * @param $objPHPExcel
 * @param $child
 * @param $school
 * @param $data
 * @param $sheetName
 */
function processOneSheetContent($objPHPExcel, $child, $school, $data, $sheetName)
{
    //$objPHPExcel = new PHPExcel();
    $sheet = $objPHPExcel->getSheetByName($sheetName);

    $all_border_style = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('rgb' => '333333'))));

    // Lấy thông tin địa chỉ và số điện thoại
    if(!is_null($school['telephone'])) {
        // Lấy thông tin địa chỉ và số điện thoại
        $add_phone = '';
        if(!is_null($school['telephone'])) {
            $add_phone .= __('Address') . ': ' . convertText4Web($school['address']) . ' - ' . __('Telephone') . ': ' . $school['telephone'];
        } else {
            $add_phone .= convertText4Web($school['address']);
        }
    }

    //Set thông tin trường/lớp và trẻ ở đầu bảng học phí.
    $sheet->setCellValue('A1', mb_strtoupper(convertText4Web(str_replace('/', '_', $school['page_title'])), "UTF-8"))
        ->setCellValue('A2', $add_phone)
        ->setCellValue('E3', $data['month'])
        ->setCellValue('A4', __('Child') . ': ' . convertText4Web($child['child_name']))
        ->setCellValue('E4', __('Class') . ': ' . convertText4Web($data['group_title']));
    $sheet->getHyperlink('E3')->setUrl("sheet://'Summary'!A1"); //Trèn link vào tên lớp để quay về sheet Summary

    $sheet->getStyle('E4')->applyFromArray(
        styleArray('Times new Roman', true, false, 11, PHPExcel_Style_Alignment::HORIZONTAL_LEFT, PHPExcel_Style_Alignment::VERTICAL_CENTER, true));

    // Xử lý autofit row height dòng địa chỉ
    $sheet->setCellValue('X1', mb_strtoupper(convertText4Web(str_replace('/', '_', $school['page_title'])), "UTF-8"))
        ->setCellValue('X2', $add_phone)
        ->setCellValue('Y4', __('Class') . ': ' . convertText4Web($data['group_title']));
    $sheet->getStyle('X1')->applyFromArray(
        styleArray('Times new Roman', true, false, 12, PHPExcel_Style_Alignment::HORIZONTAL_LEFT, PHPExcel_Style_Alignment::VERTICAL_CENTER, true));
    $sheet->getStyle('X2')->applyFromArray(
        styleArray('Times new Roman', false, false, 11, PHPExcel_Style_Alignment::HORIZONTAL_LEFT, PHPExcel_Style_Alignment::VERTICAL_CENTER, true));
    $sheet->getStyle('Y4')->applyFromArray(
        styleArray('Times new Roman', true, false, 11, PHPExcel_Style_Alignment::HORIZONTAL_LEFT, PHPExcel_Style_Alignment::VERTICAL_CENTER, true));
    $sheet->getRowDimension(1)->setRowHeight(-1);
    $sheet->getRowDimension(2)->setRowHeight(-1);
    $sheet->getRowDimension(4)->setRowHeight(-1);
    // End xử lý autofit row height

    $firstRow = 9;
    $rowNumber = $firstRow;
    // Lặp lấy ra thông tin học phí ước tính của trẻ
    $idx = 1;
    foreach ($child['tuition_detail'] as $detail) {
        $amount = $detail['quantity'] * $detail['unit_price'] - $detail['quantity_deduction'] * $detail['unit_price_deduction'];
        if ($detail['type'] == TUITION_DETAIL_FEE) {
            if ($detail['fee_type'] == FEE_TYPE_MONTHLY) {
                $deduction = $detail['unit_price_deduction'];
            } else {
                $deduction = $detail['quantity_deduction'];
            }
            $deduction = $deduction == 0? "": $deduction;
            $sheet->setCellValue('A' . $rowNumber, $idx)
                ->setCellValue('B' . $rowNumber, convertText4Web($detail['fee_name']))
                ->setCellValue('C' . $rowNumber, $detail['quantity'])
                ->setCellValue('D' . $rowNumber, $detail['unit_price'])
                ->setCellValue('E' . $rowNumber, $deduction)
                ->setCellValue('F' . $rowNumber, $amount);
        } else if ($detail['type'] == TUITION_DETAIL_SERVICE) {
            //Thong tin dich vu
            if ($detail['service_type'] == SERVICE_TYPE_MONTHLY) {
                $deduction = $detail['unit_price_deduction'];
            } else {
                $deduction = $detail['quantity_deduction'];
            }
            $deduction = $deduction == 0? "": $deduction;
            $sheet->setCellValue('A' . $rowNumber, $idx)
                ->setCellValue('B' . $rowNumber, convertText4Web($detail['service_name']))
                ->setCellValue('C' . $rowNumber, $detail['quantity'])
                ->setCellValue('D' . $rowNumber, $detail['unit_price'])
                ->setCellValue('E' . $rowNumber, $deduction)
                ->setCellValue('F' . $rowNumber, $amount);
        } else {
            $sheet->mergeCells('B' . $rowNumber . ':E' . $rowNumber);
            $sheet->setCellValue('A' . $rowNumber, $idx)
                ->setCellValue('B' . $rowNumber, __('Late PickUp'))
                ->setCellValue('F' . $rowNumber, $detail['unit_price']);
        }
        $rowNumber++;
        $idx++;
    }

    //Nợ tháng trước
    if ($child['debt_amount'] != 0) {
        $sheet->mergeCells('A' . $rowNumber . ':E' . $rowNumber);
        $sheet->getStyle('A' . $rowNumber . ':F' . $rowNumber)->applyFromArray(styleArray('Times new Roman', true, false, 11, PHPExcel_Style_Alignment::HORIZONTAL_RIGHT, PHPExcel_Style_Alignment::VERTICAL_CENTER, true));
        $sheet->setCellValue('A' . $rowNumber, __('Debt amount of previous month')." ".$child['pre_month'])->setCellValue('F' . $rowNumber, $child['debt_amount']);
        $rowNumber++;
    }

    // Lấy tổng học phí của từng trẻ
    $sheet->mergeCells('A' . $rowNumber . ':E' . $rowNumber);
    $sheet->getStyle('A' . $rowNumber . ':F' . $rowNumber)->applyFromArray(styleArray('Times new Roman', true, false, 11, PHPExcel_Style_Alignment::HORIZONTAL_RIGHT, PHPExcel_Style_Alignment::VERTICAL_CENTER, true));
    $sheet->setCellValue('A' . $rowNumber, __('Total'))->setCellValue('F' . $rowNumber, $child['total_amount']);
    $rowNumber++;

    if (($child['status'] == TUITION_CHILD_CONFIRMED) && (($child['total_amount'] - $child['paid_amount']) != 0)) {
        // Số tiền đã thanh toán
        $sheet->mergeCells('A' . $rowNumber . ':E' . $rowNumber);
        $sheet->getStyle('A' . $rowNumber . ':F' . $rowNumber)->applyFromArray(styleArray('Times new Roman', true, false, 11, PHPExcel_Style_Alignment::HORIZONTAL_RIGHT, PHPExcel_Style_Alignment::VERTICAL_CENTER, true));
        $sheet->setCellValue('A' . $rowNumber, __('Paid'))->setCellValue('F' . $rowNumber, $child['paid_amount']);
        $rowNumber++;

        // Nợ học phí tháng này
        $sheet->mergeCells('A' . $rowNumber . ':E' . $rowNumber);
        $sheet->getStyle('A' . $rowNumber . ':F' . $rowNumber)->applyFromArray(styleArray('Times new Roman', true, false, 11, PHPExcel_Style_Alignment::HORIZONTAL_RIGHT, PHPExcel_Style_Alignment::VERTICAL_CENTER, true));
        $sheet->setCellValue('A' . $rowNumber, __('Debt amount of this month'))->setCellValue('F' . $rowNumber, ($child['total_amount'] - $child['paid_amount']));
        $rowNumber++;
    }
    // Căn giữa trường STT
    //$sheet->getStyle('A7:A' . ($rowNumber - 1))->applyFromArray(styleArray('Times new Roman', false, false, 11, PHPExcel_Style_Alignment::HORIZONTAL_CENTER, PHPExcel_Style_Alignment::VERTICAL_CENTER, true));
    $sheet->getStyle('A' . $firstRow . ':F' . ($rowNumber - 1))->applyFromArray($all_border_style);

    // Ngày nộp tiền
    $rowNumber++;
    $sheet->mergeCells('A' . $rowNumber . ':F' . $rowNumber);
    $sheet->getStyle('A' . $rowNumber . ':D' . $rowNumber)->applyFromArray(
        styleArray('Times new Roman', false, true, 11, PHPExcel_Style_Alignment::HORIZONTAL_RIGHT, PHPExcel_Style_Alignment::VERTICAL_CENTER, true)
    );
    $sheet->setCellValue('A' . $rowNumber, '..........., ' . __('day') . ' .... ' . __('month') . ' .... ' . __('year') . ' ...... ');

    // Thông tin chuyển khoản
    if(!is_null($school['bank_account'])) {
        $rowNumber++;
        $sheet->mergeCells('A' . $rowNumber . ':F' . $rowNumber);
        $sheet->getStyle('A' . $rowNumber)->applyFromArray(
            styleArray('Times new Roman', true, false, 10, PHPExcel_Style_Alignment::HORIZONTAL_LEFT, PHPExcel_Style_Alignment::VERTICAL_CENTER, true)
        );
        $sheet->setCellValue('A' . $rowNumber, convertText4Web($school['bank_account']));
        $sheet->setCellValue('X' . $rowNumber, convertText4Web($school['bank_account']));
        $sheet->getStyle('X' . $rowNumber)->applyFromArray(
            styleArray('Times new Roman', true, false, 10, PHPExcel_Style_Alignment::HORIZONTAL_LEFT, PHPExcel_Style_Alignment::VERTICAL_CENTER, true)
        );
    }
    // Đoạn dưới này set cho cả trường tên trường và địa chỉ, không có tài khoản ngân hàng vẫn để lại
    $widthCol = 53;
    if($school['tuition_child_report_template'] == "A4") {
        $widthCol = 100;
    }
    $widthColClass = 19.5;
    $sheet->getColumnDimension('X')->setWidth($widthCol);
    $sheet->getColumnDimension('Y')->setWidth($widthColClass);
    // Set autoFit row height Y.$rowNumber
    //$firstSheet->getRowDimension($rowNumber)->setRowHeight(-1);
    $sheet->getColumnDimension('X')->setVisible(false);
    $sheet->getColumnDimension('Y')->setVisible(false);
    $sheet->getRowDimension($rowNumber)->setRowHeight(-1);
    // Đến đây

    // Những người liên quan
    $rowNumber = $rowNumber + 2;
    $sheet->mergeCells('A' . $rowNumber . ':B' . $rowNumber);
    $sheet->mergeCells('C' . $rowNumber . ':D' . $rowNumber);
    $sheet->mergeCells('E' . $rowNumber . ':F' . $rowNumber);
    $sheet->getStyle('A' . $rowNumber . ':E' . $rowNumber)->applyFromArray(styleArray('Times new Roman', true, false, 11, PHPExcel_Style_Alignment::HORIZONTAL_CENTER, PHPExcel_Style_Alignment::VERTICAL_CENTER, true));
    $sheet->setCellValue('A' . $rowNumber, __('Payer'))
        ->setCellValue('C' . $rowNumber, __('Cashier'))
        ->setCellValue('E' . $rowNumber, __('Treasurer'));

    // Set chỉ in tới F và $rowNumber
    $sheet->getPageSetup()->setPrintArea('A1:F' . $rowNumber);
    unset($sheet);
}

?>