<?php
/**
 * Created by PhpStorm.
 * User: TaiLA
 * Date: 5/5/17
 * Time: 3:16 PM
 */
require('../../../../../bootstrap.php');

// check AJAX Request
is_ajax();
check_login();

if(is_empty($_POST['school_username']) || !valid_username($_POST['school_username'])) {
    _error(404);
}

include_once(DAO_PATH.'dao_school.php');
include_once(DAO_PATH.'dao_event.php');
include_once(DAO_PATH.'dao_parent.php');
include_once(DAO_PATH.'dao_teacher.php');
include_once(DAO_PATH.'dao_user.php');
include_once(DAO_PATH.'dao_class.php');
include_once (DAO_PATH.'dao_birthday.php');
include_once (DAO_PATH.'dao_child.php');
$schoolDao = new SchoolDAO();
$eventDao = new EventDAO();
$parentDao = new ParentDAO();
$teacherDao = new TeacherDAO();
$userDao = new UserDAO();
$classDao = new ClassDAO();
$birthdayDao = new BirthdayDao();
$childDao = new ChildDAO();

if (!canView($_POST['school_username'], 'birthdays')) {
    _error(403);
}
//$school = getSchool($_POST['school_username']);
$school = getSchoolDataByUsername($_POST['school_username'], SCHOOL_INFO);
if (is_null($school)) {
    _error(403);
}

try {
    $return = array();
    switch ($_POST['do']) {
        case 'child_search':
            $return = array();

            $begin = validateDate($_POST['fromDate'])? $_POST['fromDate']: "31/12/2999";
            $end = validateDate($_POST['toDate'])?$_POST['toDate']:"01/01/1970";

            $class_id = isset($_POST['class_id'])?$_POST['class_id']:null;
            $birthday = $birthdayDao->getChildBirthday($school['page_id'], $begin, $end, $class_id);
            $smarty->assign('rows', $birthday);
            $return['results'] = $smarty->fetch("ci/school/ajax.child.birthday.tpl");

            return_json($return);
            break;

        case 'parent_search':
            $begin = validateDate($_POST['fromDate'])? $_POST['fromDate']: "31/12/2999";
            $end = validateDate($_POST['toDate'])?$_POST['toDate']:"01/01/1970";

            $class_id = isset($_POST['class_id'])?$_POST['class_id']:null;

            $parents = $parentDao->getParentsBirthday($school['page_id'], $begin, $end, $class_id);

            $results = array();
            // Lặp danh sách phụ huynh, lấy danh sách trẻ tương ứng với mỗi phụ huynh
            foreach ($parents as $parent) {
                // Lấy danh sách trẻ phụ huynh quản lý
                $children = $childDao->getChildBySchoolAndParent($school['page_id'], $parent['user_id']);
                $parent['children'] = $children;
                $results[] = $parent;
            }

            $smarty->assign('rows', $results);
            $return['results'] = $smarty->fetch("ci/school/ajax.parent.birthday.tpl");

            return_json($return);
            break;
        case 'teacher_search':
            $begin = validateDate($_POST['fromDate'])? $_POST['fromDate']: "31/12/2999";
            $end = validateDate($_POST['toDate'])?$_POST['toDate']:"01/01/1970";

            $birthday = $teacherDao->getTeachersBirthday($school['page_id'], $begin, $end);

            $results = array();
            // Lặp danh sách giáo viên, lấy danh sách lớp mà giáo viên đó chủ nhiệm
            foreach ($birthday as $teacher) {
                $classes = $classDao->getClassesBySchoolAndTeacher($school['page_id'], $teacher['user_id']);
                $teacher['classes'] = $classes;
                $results[] = $teacher;
            }
            $smarty->assign('rows', $results);
            $return['results'] = $smarty->fetch("ci/school/ajax.teacher.birthday.tpl");

            return_json($return);
            break;
        default:
            _error(400);
            break;
    }

} catch (Exception $e) {
    return_json( array('error' => true, 'message' => $e->getMessage()) );
}

?>