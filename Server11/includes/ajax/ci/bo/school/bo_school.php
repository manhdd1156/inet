<?php
/**
 * Package: ajax/ci/bo
 * 
 * @package ConIu
 * @author QuanND
 */

// fetch bootstrap
require('../../../../../bootstrap.php');

// check AJAX Request
is_ajax();
check_login();

try {
    include_once(DAO_PATH . 'dao_school.php');
    $schoolDao = new SchoolDAO();

    $args = array();
    $args['page_admin'] = $user->_data['user_id'];
    $args['page_title'] = $_POST['title'];
    $args['page_name'] = $_POST['username'];
    $args['page_description'] = $_POST['description'];
    $args['city_id'] = $_POST['city_id'];
    $args['telephone'] = $_POST['telephone'];
    $args['website'] = $_POST['website'];
    $args['email'] = $_POST['email'];
    $args['address'] = $_POST['address'];

    $db->autocommit(false);
    switch ($_GET['do']) {
        case 'create':
            $db->begin_transaction();

            $args['page_id'] = $schoolDao->createSchool($args);

            include_once(DAO_PATH . 'dao_user_manage.php');
            $userManDao = new UserManageDAO();

            $argsUM['user_id'] = $user->_data['user_id'];
            $argsUM['object_id'] = $args['page_id'];
            $argsUM['object_type'] = MANAGE_SCHOOL;
            $argsUM['status'] = STATUS_ACTIVE;  //Active luôn trạng thái quản lý
            $argsUM['role_id'] = PERMISSION_ALL; //Người tạo ra thì có toàn quyền
            //Đưa vào bảng đối tượng quản lý
            $userManDao->insertUserManage($argsUM);

            //Thêm các khối mặc định
            include_once(DAO_PATH . 'dao_class_level.php');
            $classLevelDao = new ClassLevelDAO();
            foreach($defaultGrades as $grade) {
                $classLevelId = $classLevelDao->insertDefaultCL($grade['name'], $args['page_id'], $grade['description']);
                //Memcache - Cập nhật thông tin khối
                updateClassLevelData($classLevelId, CLASS_LEVEL_INFO);
            }

            //Tạo một profile mặc định
            $schoolDao->createSchoolProfile($args['page_id']);
            $db->commit();

            /* ---------- UPDATE - MEMCACHE ---------- */
            //1.Cập nhật thông tin trường
            updateSchoolData($args['page_id']);
            //2.Cập nhật thông tin quản lý
            updateSchoolData($argsUM['user_id']);
            /* ---------- END - MEMCACHE ---------- */

            return_json(array('callback' => 'window.location = "'.$system['system_url'].'/pages/'.$_POST['username'].'";'));
            break;

        case 'edit':
            /* check id */
            if(!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                _error(400);
            }
            //$school = $schoolDao->getSchool($_GET['id'], $user->_data['user_id']);
            $school = getSchoolData($_GET['id'], SCHOOL_INFO);
            if (is_null($school) || ($school['role_id'] != PERMISSION_ALL)) {
                // Nếu không phải là admin thì báo lỗi
                $i_admin = $user->check_page_adminship($user->_data['user_id'], $_GET['id']);
                if(!$i_admin) {
                    _error(403);
                }
            }

            $db->begin_transaction();
            /* edit page */
            $args['page_id'] = $_GET['id'];
            $args['username_old'] = $school['page_name'];
            $args['type'] = $_POST['school_type'];
            $args['district_slug'] = $_POST['district_slug'];
            $args['short_overview'] = $_POST['short_description'];
            $schoolDao->editSchool($args);
            $db->commit();

            /* ---------- UPDATE - MEMCACHE ---------- */
            //1.Cập nhật thông tin trường
            //unset($args['username_old']);
            updateSchoolData($args['page_id'], ALL);
            /* ---------- END - MEMCACHE ---------- */

            return_json(array('callback' => 'window.location = "'.$system['system_url'].'/pages/'.$_POST['username'] . '/settings/' .'";'));
            break;
        case 'editservice':
            $db->begin_transaction();

            $argsFa = array();
            /* check id */
            if(!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                _error(400);
            }
            //$school = $schoolDao->getSchool($_GET['id'], $user->_data['user_id']);
            $school = getSchoolData($_GET['id'], SCHOOL_INFO);
            if (is_null($school) || ($school['role_id'] != PERMISSION_ALL)) {
                // Nếu không phải là admin thì báo lỗi
                $i_admin = $user->check_page_adminship($user->_data['user_id'], $_GET['id']);
                if(!$i_admin) {
                    _error(403);
                }
            }
            $argsFa = array();
            if(isset($_POST['facilities'])) {
                $facilities = array();
                $facilities = $_POST['facilities'];
                $argsFa['pool'] = isset($facilities[0]) ? $facilities[0] : 0;
                $argsFa['outdoor'] = isset($facilities[1]) ? $facilities[1] : 0;
                $argsFa['indoor'] = isset($facilities[2]) ? $facilities[2] : 0;
                $argsFa['library'] = isset($facilities[3]) ? $facilities[3] : 0;
                $argsFa['viewcamera'] = isset($facilities[4]) ? $facilities[4]: 0;
            } else {
                $argsFa['pool'] = 0;
                $argsFa['outdoor'] = 0;
                $argsFa['indoor'] = 0;
                $argsFa['library'] = 0;
                $argsFa['viewcamera'] = 0;
            }

            $argsFa['school_id'] = $_GET['id'];
            $argsFa['note'] = $_POST['facility_note'];

            // Update facility vào school config
            $schoolDao->updateSchoolConfigNogaFacility($argsFa);

            $argsSe = array();
            if(isset($_POST['services'])) {
                $services = array();
                $services = $_POST['services'];
                $argsSe['bus'] = isset($services[0]) ? $services[0] : 0;
                $argsSe['breakfast'] = isset($services[1]) ? $services[1] : 0;
                $argsSe['belated'] = isset($services[2]) ? $services[2] : 0;
                $argsSe['saturday'] = isset($services[3]) ? $services[3] : 0;
            } else {
                $argsSe['breakfast'] = 0;
                $argsSe['belated'] = 0;
                $argsSe['saturday'] = 0;
                $argsSe['bus'] = 0;
            }
            $argsSe['school_id'] = $_GET['id'];
            $argsSe['note'] = $_POST['service_note'];

            // Update facility vào school config
            $schoolDao->updateSchoolConfigNogaServices($argsSe);

            $db->commit();
            /* ---------- UPDATE - MEMCACHE ---------- */
            //1.Cập nhật thông tin trường
            //unset($args['username_old']);
            updateSchoolData($_GET['id'], ALL);
            /* ---------- END - MEMCACHE ---------- */

            return_json(array('callback' => 'window.location = "'.$system['system_url'].'/pages/'.$_POST['username'] . '/settings/' .'";'));
            break;

        case 'editaddmission':
            $db->begin_transaction();

            $args = array();
            if(!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                _error(400);
            }
            // $school = $schoolDao->getSchool($_GET['id'], $user->_data['user_id']);
            $school = getSchoolData($_GET['id'], SCHOOL_INFO);
            if (is_null($school) || ($school['role_id'] != PERMISSION_ALL)) {
                // Nếu không phải là admin thì báo lỗi
                $i_admin = $user->check_page_adminship($user->_data['user_id'], $_GET['id']);
                if(!$i_admin) {
                    _error(403);
                }
            }
            $args['school_id'] = $_GET['id'];
            $args['age_begin'] = $_POST['age_begin'];
            $args['age_end'] = $_POST['age_end'];
            $args['tuition_begin'] = $_POST['tuition_begin'];
            $args['tuition_end'] = $_POST['tuition_end'];
            $args['tuition_note'] = $_POST['tuition_note'];
            $args['addmission'] = $_POST['addmission'];
            $args['addmission_note'] = $_POST['addmission_note'];
            $schoolDao->updateSchoolConfigNogaAddmission($args);
            $db->commit();
            /* ---------- UPDATE - MEMCACHE ---------- */
            //1.Cập nhật thông tin trường
            //unset($args['username_old']);
            updateSchoolData($_GET['id'], ALL);
            /* ---------- END - MEMCACHE ---------- */

            return_json(array('callback' => 'window.location = "'.$system['system_url'].'/pages/'.$_POST['username'] . '/settings/' .'";'));
            break;

        case 'editinfo':
            $db->begin_transaction();

            $args = array();
            if(!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                _error(400);
            }
            //$school = $schoolDao->getSchool($_GET['id'], $user->_data['user_id']);
            $school = getSchoolData($_GET['id'], SCHOOL_INFO);
            if (is_null($school) || ($school['role_id'] != PERMISSION_ALL)) {
                // Nếu không phải là admin thì báo lỗi
                $i_admin = $user->check_page_adminship($user->_data['user_id'], $_GET['id']);
                if(!$i_admin) {
                    _error(403);
                }
            }
            $args['school_id'] = $_GET['id'];
            $args['leader'] = $_POST['leader'];
            $args['method'] = $_POST['method'];
            $args['teacher'] = $_POST['teacher'];
            $args['nutrition'] = $_POST['nutrition'];
            $schoolDao->updateSchoolConfigNogaInfo($args);
            $db->commit();

            /* ---------- UPDATE - MEMCACHE ---------- */
            //1.Cập nhật thông tin trường
            //unset($args['username_old']);
            updateSchoolData($_GET['id'], ALL);
            /* ---------- END - MEMCACHE ---------- */

            return_json(array('callback' => 'window.location = "'.$system['system_url'].'/pages/'.$_POST['username'] . '/settings/' .'";'));
            break;

        default:
            _error(400);
            break;
    }

} catch (Exception $e) {
    $db->rollback();
	return_json( array('error' => true, 'message' => $e->getMessage()) );
} finally {
    $db->autocommit(true);
}

?>