<?php
/**
 * Package: ajax/ci/bo
 * 
 * @package ConIu
 * @author QuanND
 */

// fetch bootstrap
require('../../../../../bootstrap.php');

// check AJAX Request
is_ajax();
check_login();

if(is_empty($_POST['school_username']) || !valid_username($_POST['school_username'])) {
    _error(404);
}

include_once(DAO_PATH.'dao_school.php');
include_once(DAO_PATH.'dao_parent.php');
include_once(DAO_PATH.'dao_teacher.php');
include_once(DAO_PATH.'dao_user.php');
include_once(DAO_PATH.'dao_class.php');
include_once(DAO_PATH.'dao_child.php');
include_once(DAO_PATH.'dao_mail.php');
include_once(DAO_PATH.'dao_report.php');

$schoolDao = new SchoolDAO();
$parentDao = new ParentDAO();
$teacherDao = new TeacherDAO();
$userDao = new UserDAO();
$classDao = new ClassDAO();
$childDao = new ChildDAO();
$mailDao = new MailDAO();
$reportDao = new ReportDAO();

//$school = $schoolDao->getSchoolByUsername($_POST['school_username'], $user->_data['user_id']);
//$school = getSchool($_POST['school_username']);
$school = getSchoolDataByUsername($_POST['school_username'], SCHOOL_INFO);


try {
    $return = array();
    $db->autocommit(false);
    switch ($_POST['do']) {
        case 'list_child':
            if(!isset($_POST['class_id']) || !is_numeric($_POST['class_id'])) {
                _error(404);
            }

            $children = $childDao->getChildrenOfClassForReport($_POST['class_id']);
            $results = "";
            switch ($_POST['view']) {
                case 'add':
                    $results = $results.'<div class="col-sm-12"><input type="checkbox" id="select_all_child">' . ' ' . '<strong>' . __("Select all") . '</strong>'  . '</div>';
                    foreach ($children as $child) {
                        if(!is_null($child['report_new'])) {
                            $results = $results.'<div class="col-xs-12 col-sm-6"><input type="checkbox" class = "checkbox_child" name = "child[]" value="'.$child['child_id'].'"> <strong>'.$child['child_name'].'</strong> <br/> '.__('Latest').': '.toSysDate($child['report_new']).'</div> ';
                        } else {
                            $results = $results.'<div class="col-xs-12 col-sm-6"><input type="checkbox" name = "child[]" class = "checkbox_child" value="'.$child['child_id'].'"> <strong>'.$child['child_name'].'</strong> <br/>'.__("Latest").': '.__("No information").'</div> ';
                        }
                    }
                    break;
                case 'show':
                    foreach ($children as $child) {
                        $results = $results.'<option value="'.$child['child_id'].'">'.$child['child_name'].'</option>';
                    }
                    break;
            }

            $return['results'] = $results;
            return_json($return);
            break;
        case 'template_detail':
            if (!isset($_POST['template_id']) || !is_numeric($_POST['template_id'])) {
                _error(404);
            }
            $results = $reportDao->getReportTemplate($_POST['template_id'], 1);
            $smarty->assign('results', $results);

            $return['results'] = $smarty->fetch("ci/ajax.reporttemplatedetail.tpl");
            //$return['no_data'] = (count($results['children']) == 0);

            return_json($return);
            break;
        case 'edit_temp':
            if (!canEdit($_POST['school_username'], 'reports')) {
                _error(403);
            }
            $db->begin_transaction();
            if(!isset($_POST['report_template_id'])) {
                _error(404);
            }
            $args = array();
            $args['report_template_id'] = $_POST['report_template_id'];
            $args['template_name'] = trim($_POST['template_name']);
            $args['level'] = $_POST['level'];
            if($_POST['level'] == SCHOOL_LEVEL) {
                $args['class_id'] = 0;
                $args['class_level_id'] = 0;
            } else if ($_POST['level'] == CLASS_LEVEL_LEVEL) {
                $args['class_id'] = 0;
                $args['class_level_id'] = $_POST['class_level_id'];
            } else {
                $args['class_id'] = $_POST['class_id'];
                $args['class_level_id'] = 0;
            }
            $args['school_id'] = $school['page_id'];

            // 1. Update bảng ci_report_template
            $reportDao->updateReportTemplate($args);

            // 2. Update bảng ci_report_template_detail
            // Xóa toàn bộ chi tiết mẫu cũ
            $reportDao->deleteReportTemplateDetail($_POST['report_template_id']);

            // Lấy dữ liệu mới
            $categoryIds = isset($_POST['category_ids'])?$_POST['category_ids']:array();

            // Lặp mảng $categoryIds lấy nội dung tương ứng
            $contents = array();
            foreach ($categoryIds as $id) {
                $contents[] = $_POST['content_'.$id];
            }

            // Insert vào bảng ci_report_template_detail
            $reportDao->insertReportTemplateDetail($_POST['report_template_id'], $categoryIds, $contents);

            $db->commit();
            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/school/' . $_POST['school_username'] . '/reports/listtemp";'));
            break;
        case 'add_temp':
            if (!canEdit($_POST['school_username'], 'reports')) {
                _error(403);
            }
            $db->begin_transaction();
            $args = array();
            $args['template_name'] = trim($_POST['template_name']);
            $args['level'] = $_POST['level'];
            if($_POST['level'] == SCHOOL_LEVEL) {
                $args['class_id'] = 0;
                $args['class_level'] = 0;
            } else if ($_POST['level'] == CLASS_LEVEL_LEVEL) {
                $args['class_level_id'] = $_POST['class_level_id'];
                $args['class_id'] = 0;
            } else {
                $args['class_level_id'] = 0;
                $args['class_id'] = $_POST['class_id'];
            }
            $args['school_id'] = $school['page_id'];

            // 1. Insert vào bảng ci_report_template
            $reportTemplateId = $reportDao->insertReportTemplate($args);

            $categoryIds = isset($_POST['category_ids'])?$_POST['category_ids']:array();
            // Lặp mảng $categoryIds lấy nội dung tương ứng
            $contents = array();
            foreach ($categoryIds as $id) {
                $contents[] = $_POST['content_'.$id];
            }

            // 2. Insert vào bảng ci_report_template_detail
            $reportDao->insertReportTemplateDetail($reportTemplateId, $categoryIds, $contents);

            if($_POST['level'] == SCHOOL_LEVEL) {
                // Lấy danh sách lớp của trường
                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);

                if(count($classes) > 0) {
                    foreach ($classes as $class) {
                        // Lấy danh sách giáo viên của lớp
                        $teachers = getClassData($class['group_id'], CLASS_TEACHERS);
                        if(count($teachers) > 0) {
                            $teacherIds = array();
                            foreach ($teachers as $teacher) {
                                $teacherIds[] = $teacher['user_id'];
                            }
                            // Thông báo cho giáo viên có mẫu mới
                            $userDao->postNotifications($teacherIds, NOTIFICATION_NEW_REPORT_TEMPLATE, NOTIFICATION_NODE_TYPE_CLASS,
                                $reportTemplateId, convertText4Web($school['page_title']), $class['group_name'], $_POST['template_name']);
                        }
                    }
                }
            } else if($_POST['level'] == CLASS_LEVEL_LEVEL) {
                // Lấy chi tiết khối
                $class_level = getClassLevelData($_POST['class_level_id'], CLASS_LEVEL_INFO);
                // Lấy danh sách lớp của khối
                $classes = getClassLevelData($_POST['class_level_id'], CLASS_LEVEL_CLASSES);
                if(count($classes) > 0) {
                    foreach ($classes as $class) {
                        // Lấy danh sách giáo viên của lớp
                        $teachers = getClassData($class['group_id'], CLASS_TEACHERS);
                        if(count($teachers) > 0) {
                            $teacherIds = array();
                            foreach ($teachers as $teacher) {
                                $teacherIds[] = $teacher['user_id'];
                            }
                            // Thông báo cho giáo viên có mẫu mới
                            $userDao->postNotifications($teacherIds, NOTIFICATION_NEW_REPORT_TEMPLATE, NOTIFICATION_NODE_TYPE_CLASS,
                                $reportTemplateId, convertText4Web($school['page_title']), $class['group_name'], $_POST['template_name']);
                        }
                    }
                }
            } else {
                // Lấy chi tiết lớp
                $class = getClassData($_POST['class_id'], CLASS_INFO);

                // Lấy danh sách giáo viên của lớp
                $teachers = getClassData($_POST['class_id'], CLASS_TEACHERS);
                if(count($teachers) > 0) {
                    $teacherIds = array();
                    foreach ($teachers as $teacher) {
                        $teacherIds[] = $teacher['user_id'];
                    }
                    // Thông báo cho giáo viên có mẫu mới
                    $userDao->postNotifications($teacherIds, NOTIFICATION_NEW_REPORT_TEMPLATE, NOTIFICATION_NODE_TYPE_CLASS,
                        $reportTemplateId, convertText4Web($school['page_title']), $class['group_name'], $_POST['template_name']);
                }
            }

            $db->commit();
            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/school/' . $_POST['school_username'] . '/reports/listtemp";'));
            break;
        case 'add':
            if (!canEdit($_POST['school_username'], 'reports')) {
                _error(403);
            }
            try {
                $db->begin_transaction();
                if(is_null($_POST['child'])) {
                    throw new Exception(__("You have not picked a student yet"));
                }
                $args = array();
                $args['report_name'] = trim($_POST['title']);
                $args['school_id'] = $school['page_id'];
                $args['child'] = $_POST['child'];
                $args['is_notified'] = (isset($_POST['notify_immediately']) && $_POST['notify_immediately'] == 'on')? 1: 0;
                $args['status'] = 1;
                $args['date'] = 'null';
                if($args['is_notified'] == 1) {
                    $args['date'] = $date;
                }

                $file_name = "";
                // Upload file đính kèm
                if(file_exists($_FILES['file']['tmp_name'])) {
                    // check file upload enabled
                    if(!$system['file_enabled']) {
                        throw new Exception(__("This feature has been disabled"));
                    }

                    // valid inputs
                    if(!isset($_FILES["file"]) || $_FILES["file"]["error"] != UPLOAD_ERR_OK) {
                        throw new Exception(__("Something wrong with upload! Is 'upload_max_filesize' set correctly?"));
                    }

                    // check file size
                    $max_allowed_size = $system['max_file_size'] * 1024;
                    if($_FILES["file"]["size"] > $max_allowed_size) {
                        throw new Exception(__("The file size is so big"));
                    }

                    // check file extesnion
                    $extension = get_extension($_FILES['file']['name']);
                    if(!valid_extension($extension, $system['file_report_extensions'])) {
                        throw new Exception(__("The file type is not valid or not supported"));
                    }

                    /* check & create uploads dir */
                    $depth = '../../../../../';
                    $folder = 'reports/'. $school['page_id'];
                    if(!file_exists($depth.$system['system_uploads_directory'].'/'.$folder)) {
                        @mkdir($depth.$system['system_uploads_directory'].'/'.$folder, 0777, true);
                    }
                    if(!file_exists($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y'))) {
                        @mkdir($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y'), 0777, true);
                    }
                    if(!file_exists($system['system_uploads_directory'].'/'.$folder.'/' . date('Y') . '/' . date('m'))) {
                        @mkdir($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y') . '/' . date('m'), 0777, true);
                    }

                    /* prepare new file name */
                    $directory = $folder.'/'. date('Y') . '/' . date('m') . '/';
                    $prefix = $system['uploads_prefix'].'_'.md5(time()*rand(1, 9999));
                    $file_name = $directory.$prefix.'.'.$extension;
                    $path = $depth.$system['system_uploads_directory'].'/'.$file_name;

                    /* check if the file uploaded successfully */
                    if(!@move_uploaded_file($_FILES['file']['tmp_name'], $path)) {
                        throw new Exception(__("Sorry, can not upload the file"));
                    }
                }

                $args['source_file'] = $file_name;
                $args['file_name'] = $_FILES['file']['name'];

                // 1. Insert vào bảng ci_report
                if(is_empty($args['source_file']) && (count($_POST['category_ids']) == 0)){
                    throw new Exception(__("You must have an attachment or contact book content"));
                }
                $reportIds = $reportDao->insertReportClass($args);
                // TaiLA - Tăng số lượng thêm mới của trường
                foreach ($reportIds as $reportId) {
                    addInteractive($school['page_id'], 'report', 'school_view', $reportId, 1);
                }
                // 2. Inser vào bảng ci_report_category
                // Lấy id category
                $categoryIds = isset($_POST['category_ids'])?$_POST['category_ids']:array();
                if(count($categoryIds) == 0 && is_empty($args['source_file'])) {
                    throw new Exception(__("You must select report template"));
                }
                // Lặp mảng categoryIds lấy nội dung gửi lên tương ứng
                $categorys = array();
                $category = array();
                if(count($categoryIds) > 0) {
                    foreach ($categoryIds as $id) {
                        $category['multi_content'] = isset($_POST['suggest_' . $id]) ? $_POST['suggest_' . $id] : array();
                        $category['report_category_content'] = $_POST['content_' . $id];
                        $category['report_category_name'] = $_POST['category_name_' . $id];

                        // Lấy toàn bộ gợi ý của category
                        $categoryTemp = $reportDao->getReportTemplateCategoryDetail($id);
                        $suggests = $categoryTemp['suggests'];
                        $category['template_multi_content'] = array();
                        foreach ($suggests as $row) {
                            $category['template_multi_content'][] = convertText4Web($row);
                        }
//                    $category['template_multi_content'] = convertText4Web($category['template_multi_content']);
                        $categorys[] = $category;
                    }
                    for($i = 0; $i < count($reportIds); $i++) {
                        $reportDao->insertReportDetail($reportIds[$i], $categorys);
                    }
                }

                // Gửi email và thông báo đến phụ huynh
                if ($args['is_notified']) {
                    $argEmail = array();
                    if (!is_empty($file_name)) {
                        $argEmail['file_attachment'] = $system['system_uploads_directory'].'/'.$file_name;
                        $argEmail['file_attachment_name'] = $args['file_name'];
                    }

                    $content = "";
                    $isOther = false;
                    for($i = 0; $i < count($cates); $i ++) {
                        $content = $content . "- <b>" . $cates[$i] . "-</b>: " . $contents[$i] . "<br/><br/>";
                    }
                    $smarty->assign('title', $args['report_name']);
                    $smarty->assign('content', $content);
                    $smarty->assign('class_name', convertText4Web($class['group_title']));

                    $argEmail['action'] = "create_report";
                    $argEmail['subject'] = $args['report_name'];
                    $argEmail['school_id'] = $args['school_id'];
                    $argEmail['user_id'] = $user->_data['user_id'];


                    $receivers = "";
                    foreach ($args['child'] as $child_id) {
                        $parent_email = array();
                        //$child = $childDao->getChild($child_id);
                        $child = getChildData($child_id, CHILD_INFO);
                        if (!is_null($child)) {
                            if (!is_empty($child['parent_email'])) {
                                $parent_email[] = $child['parent_email'];

                            }
                            //$parents = $childDao->getParent($child_id);
                            $parents = getChildData($child_id, CHILD_PARENTS);
                            if (!is_null($parents)) {
                                foreach ($parents as $parent) {
                                    $reports = $reportDao->getChildReport($child_id);
                                    foreach ($reports as $report) {
                                        if(in_array($report['report_id'], $reportIds)) {
                                            //thông báo về cho phụ huynh
                                            $userDao->postNotifications($parent['user_id'], NOTIFICATION_NEW_REPORT, NOTIFICATION_NODE_TYPE_CHILD,
                                                $report['report_id'], convertText4Web($_POST['title']), $child_id, convertText4Web($child['child_name']));
                                        }
                                    }

                                    // lấy danh sách email
                                    if (!is_empty($parent['user_email'])) {
                                        $parent_email[] = $parent['user_email'];
                                    }
                                }
                            }

                            $parent_email = array_unique($parent_email);
                            $emai_str = implode(",", $parent_email);
                            $receivers = $receivers . ',' . $emai_str;

                            $smarty->assign('child_name', convertText4Web($child['child_name']));
                            $argEmail['content'] = $smarty->fetch("ci/email_templates/create_report.tpl");
                            $argEmail['receivers'] = $emai_str;

                            $argEmail['delete_after_sending'] = 1;
                            // $mailDao->insertEmail($argEmail);
                        }
                    }
                    // Thông báo cho các quản lý trường khác khi tạo báo cáo mới
                    //$class = $classDao->getClass($_POST['class_id']);
                    $class = getClassData($_POST['class_id'], CLASS_INFO);
                    if (!is_null($class)) {
                        $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'reports', $school['page_admin']);
                        $userDao->postNotifications($userManagerIds, NOTIFICATION_NEW_REPORT, NOTIFICATION_NODE_TYPE_SCHOOL,
                            $_POST['class_id'], convertText4Web($class['group_title']), $school['page_name'], '');
                    }

                    /* Coniu - Tương tác trường */
                    setIsAddNew($school['page_id'], 'report_created', $reportIds);
                    /* Coniu - END */
                }

                $db->commit();

                //return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/school/' . $_POST['school_username'] . '/reports";'));
                return_json( array('success' => true, 'message' => __("Contact book has been created")) );
            } catch (Exception $e) {
                $db->rollback();
                return_json( array('error' => true, 'message' => $e->getMessage()));
            } finally {
                $db->autocommit(true);
            }
            break;
        case 'edit':
            try {
                $data = $reportDao->getReportById($_POST['report_id']);
                if (is_null($data)) {
                    _error(404);
                }
                $args = array();
                $args['report_id'] = $_POST['report_id'];
                $args['is_notified'] = (isset($_POST['notify_immediately']) && $_POST['notify_immediately'] == 'on')? 1: 0;
                $args['report_name'] = trim($_POST['title']);

                $args['date'] = 'null';
                if($args['is_notified'] == 1) {
                    $args['date'] = $date;
                }

                $args['source_file'] = $data['source_file_path'];
                $args['file_name'] = convertText4Web($data['file_name']);
                if(!$_POST['is_file']) {
                    $args['source_file'] = "";
                    $args['file_name'] = "";
                }
                $file_name = "";
                // Upload file đính kèm
                if(file_exists($_FILES['file']['tmp_name'])) {
                    // check file upload enabled
                    if(!$system['file_enabled']) {
                        throw new Exception(__("This feature has been disabled"));
                    }

                    // valid inputs
                    if(!isset($_FILES["file"]) || $_FILES["file"]["error"] != UPLOAD_ERR_OK) {
                        throw new Exception(__("Something wrong with upload! Is 'upload_max_filesize' set correctly?"));
                    }

                    // check file size
                    $max_allowed_size = $system['max_file_size'] * 1024;
                    if($_FILES["file"]["size"] > $max_allowed_size) {
                        throw new Exception(__("The file size is so big"));
                    }

                    // check file extesnion
                    $extension = get_extension($_FILES['file']['name']);
                    if(!valid_extension($extension, $system['file_report_extensions'])) {
                        throw new Exception(__("The file type is not valid or not supported"));
                    }

                    /* check & create uploads dir */
                    $depth = '../../../../../';
                    $folder = 'reports/'. $school['page_id'];
                    if(!file_exists($depth.$system['system_uploads_directory'].'/'.$folder)) {
                        @mkdir($depth.$system['system_uploads_directory'].'/'.$folder, 0777, true);
                    }
                    if(!file_exists($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y'))) {
                        @mkdir($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y'), 0777, true);
                    }
                    if(!file_exists($system['system_uploads_directory'].'/'.$folder.'/' . date('Y') . '/' . date('m'))) {
                        @mkdir($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y') . '/' . date('m'), 0777, true);
                    }

                    /* prepare new file name */
                    $directory = $folder.'/'. date('Y') . '/' . date('m') . '/';
                    $prefix = $system['uploads_prefix'].'_'.md5(time()*rand(1, 9999));
                    $file_name = $directory.$prefix.'.'.$extension;
                    $path = $depth.$system['system_uploads_directory'].'/'.$file_name;

                    /* check if the file uploaded successfully */
                    if(!@move_uploaded_file($_FILES['file']['tmp_name'], $path)) {
                        throw new Exception(__("Sorry, can not upload the file"));
                    }
                    $args['source_file'] = $file_name;
                    $args['file_name'] = $_FILES['file']['name'];
                }

                if(is_empty($args['source_file']) && empty($_POST['report_category_ids'])){
                    throw new Exception(__("You must have an attachment or contact book content"));
                }

                // 1. Cập nhật bảng ci_report
                $reportDao->updateReport($args);

                // Nếu không chọn mẫu khác thì update category cũ
                if($_POST['report_template_id'] == null) {
                    // 2. cập nhật bảng ci_report_category
                    // Lấy id category
                    $categoryIds = array();
                    $categoryIds = $_POST['report_category_ids'];
                    // Lặp mảng categoryIds lấy nội dung gửi lên tương ứng
                    $category = array();
                    if(count($categoryIds) > 0) {
                        foreach ($categoryIds as $id) {
                            $category['report_category_id'] = $id;
                            $category['multi_content'] = isset($_POST['report_suggest_' . $id]) ? $_POST['report_suggest_' . $id] : array();
                            $category['report_category_content'] = $_POST['report_content_' . $id];
//                    $categorys[] = $category;
                            // Update report category
                            $reportDao->updateReportCategory($category);
                        }
                    }
                } else {
                    // Nếu chọn mẫu khác thì insert category mới
                    //3. Xóa report category cũ
                    $reportDao->deleteReportDetail($args['report_id']);

                    //4. Thêm report category mới
                    // Lấy id category
                    $categoryIds = array();
                    $categoryIds = $_POST['category_ids'];
                    // Lặp mảng categoryIds lấy nội dung gửi lên tương ứng
                    $categorys = array();
                    $category = array();
                    if(count($categoryIds) > 0) {
                        foreach ($categoryIds as $k => $id) {
                            $category['multi_content'] = isset($_POST['suggest_' . $id]) ? $_POST['suggest_' . $id] : array();
                            $category['report_category_content'] = $_POST['content_' . $id];
                            $category['report_category_name'] = $_POST['category_name_' . $id];
                            // Lấy toàn bộ gợi ý của category
                            $categoryTemp = $reportDao->getReportTemplateCategoryDetail($id);
                            $suggests = $categoryTemp['suggests'];
                            $category['template_multi_content'] = array();
                            foreach ($suggests as $row) {
                                $category['template_multi_content'][] = convertText4Web($row);
                            }
                            $categorys[] = $category;
                        }
                        // Insert chi tiết sổ liên lạc (bảng ci_report_category)
                        $reportDao->insertReportDetail($_POST['report_id'], $categorys);
                    }
                }

                // Gửi email và thông báo đến phụ huynh
                if ($args['is_notified']) {
                    $argEmail = array();
                    if (!is_empty($file_name)) {
                        $argEmail['file_attachment'] = $system['system_uploads_directory'].'/'.$file_name;
                        $argEmail['file_attachment_name'] = $args['file_name'];
                    } else {
                        $argEmail['file_attachment'] = $system['system_uploads_directory'].'/' . $args['source_file'];
                        $argEmail['file_attachment_name'] = $args['file_name'];
                    }

                    $content = "";
                    $isOther = false;
                    for($i = 0; $i < count($cates); $i ++) {
                        $content = $content . "- <b>" . $cates[$i] . "-</b>: " . $contents[$i] . "<br/><br/>";
                    }
                    $smarty->assign('title', $args['report_name']);
                    $smarty->assign('content', $content);
                    $smarty->assign('class_name', convertText4Web($class['group_title']));

                    $argEmail['action'] = "create_report";
                    $argEmail['subject'] = $args['report_name'];
                    $argEmail['school_id'] = $args['school_id'];
                    $argEmail['user_id'] = $user->_data['user_id'];


                    $receivers = "";
                    $parent_email = array();
                    //$child = $childDao->getChild($data['child_id']);
                    $child = getChildData($data['child_id'], CHILD_INFO);
                    if (!is_null($child)) {
                        if (!is_empty($child['parent_email'])) {
                            $parent_email[] = $child['parent_email'];
                        }
                        //$parents = $childDao->getParent($child['child_id']);
                        $parents = getChildData($child['child_id'], CHILD_PARENTS);
                        if (!is_null($parents)) {
                            $parentIds = array_keys($parents);
                            //thông báo về cho phụ huynh
                            $userDao->postNotifications($parentIds, NOTIFICATION_UPDATE_REPORT, NOTIFICATION_NODE_TYPE_CHILD,
                                $_POST['report_id'], convertText4Web($_POST['title']), $child['child_id'], convertText4Web($child['child_name']));
                            foreach ($parents as $parent) {
                                // lấy danh sách email
                                if (!is_empty($parent['user_email'])) {
                                    $parent_email[] = $parent['user_email'];
                                }
                            }
                        }

                        $parent_email = array_unique($parent_email);
                        $emai_str = implode(",", $parent_email);
                        $receivers = $receivers . ',' . $emai_str;

                        $smarty->assign('child_name', convertText4Web($child['child_name']));
                        $argEmail['content'] = $smarty->fetch("ci/email_templates/create_report.tpl");
                        $argEmail['receivers'] = $emai_str;

                        $argEmail['delete_after_sending'] = 1;
                        //$mailDao->insertEmail($argEmail);
                    }
                    // Thông báo cho các quản lý trường khác khi sửa sổ liên lạc
                    $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'reports', $school['page_admin']);
                    $userDao->postNotifications($userManagerIds, NOTIFICATION_UPDATE_REPORT, NOTIFICATION_NODE_TYPE_SCHOOL,
                        $_POST['report_id'], convertText4Web($_POST['title']), $school['page_name'], convertText4Web($child['child_name']));
                }
                $db->commit();
                return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/school/' . $_POST['school_username'] . '/reports";'));
            } catch (Exception $e) {
                $db->rollback();
                return_json( array('error' => true, 'message' => $e->getMessage()));
            } finally {
                $db->autocommit(true);
            }

            break;
        case 'search_report':
            try {
                if(!isset($_POST['level']) || !is_numeric($_POST['level'])) {
                    _error(404);
                }
                $result = array();
                $result_cate = array();
                switch ($_POST['level']) {
                    case SCHOOL_LEVEL:
                        $result = $reportDao->getSchoolReport($school['page_id']);
                        if($_POST['category'] != 0) {
                            // Lấy chi tiết category
                            $category = $reportDao->getReportTemplateCategoryDetail($_POST['category']);
                            $result_cate = $reportDao->getSchoolReportByCategoryName($school['page_id'], $category['category_name']);
                        }
                        break;
                    case CLASS_LEVEL:
                        if(!is_numeric($_POST['class_id'])) {
                            throw new Exception(__("You must choose a class"));
                        }
                        $result = $reportDao->getClassReport($_POST['class_id']);
                        if($_POST['category'] != 0) {
                            // Lấy chi tiết category
                            $category = $reportDao->getReportTemplateCategoryDetail($_POST['category']);
                            $result_cate = $reportDao->getClassReportByCategoryName($_POST['class_id'], $category['category_name']);
                        }

                        break;
                    case PARENT_LEVEL:
                        if(!is_numeric($_POST['child_id'])) {
                            throw new Exception(__("You must choose a student"));
                        }
                        $result = $reportDao->getClassReportOfChild($_POST['class_id'], $_POST['child_id']);
                        if($_POST['category'] != 0) {
                            // Lấy chi tiết category
                            $category = $reportDao->getReportTemplateCategoryDetail($_POST['category']);
                            $result_cate = $reportDao->getClassReportOfChildByCategoryName($_POST['class_id'], $_POST['child_id'], $category['category_name']);
                        }
                        break;

                    default:
                }

                $countNotNotify = 0;
                foreach ($result as $row) {
                    if(!$row['is_notified']) {
                        $countNotNotify = $countNotNotify + 1;
                    }
                }

                $smarty->assign('username', $_POST['school_username']);
                $smarty->assign('result', $result);
                $smarty->assign('result_cate', $result_cate);
                $smarty->assign('countNotNotify', $countNotNotify);
                $smarty->assign('canEdit', canEdit($_POST['school_username'], 'reports'));

                if($_POST['category'] == 0) {
                    $return['results'] = $smarty->fetch("ci/school/ajax.reportlist.tpl");
                } else {
                    $return['results'] = $smarty->fetch("ci/school/ajax.reportlistbycategory.tpl");
                }


                // return & exit
                return_json($return);
            } catch (Exception $e) {
                $db->rollback();
                return_json( array('error' => true, 'message' => $e->getMessage()));
            } finally {
                $db->autocommit(true);
            }
            break;
        case 'delete_report':
            $db->begin_transaction();
            if(!$_POST['id'] || !is_numeric($_POST['id'])) {
                _error(404);
            }
            $reportDao->deleteReport($_POST['id']);

            $db->commit();
            return_json( array('success' => true, 'message' => __("Success")));

            break;
        case 'delete_all_select':
            $db->begin_transaction();
            $ids = isset($_POST['ids'])? $_POST['ids']: array();
            if(count($ids) > 0) {
                foreach ($ids as $id) {
                    $reportDao->deleteReport($id);
                }
            }
            $db->commit();
            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/school/' . $_POST['school_username'] . '/reports";'));
            break;

        case 'notify_all_select':
            $db->begin_transaction();
            $ids = isset($_POST['ids'])? $_POST['ids']: array();

            if(count($ids) > 0) {
                foreach ($ids as $id) {
                    $report = $reportDao->getReportById($id);
                    //$parents = $childDao->getParent($child['child_id']);
                    $child = getChildData($report['child_id'], CHILD_INFO);
                    $parents = getChildData($report['child_id'], CHILD_PARENTS);
                    if (!is_null($parents)) {
                        $parentIds = array_keys($parents);
                        //thông báo về cho phụ huynh
                        $userDao->postNotifications($parentIds, NOTIFICATION_NEW_REPORT, NOTIFICATION_NODE_TYPE_CHILD,
                            $id, convertText4Web($report['report_name']), $child['child_id'], convertText4Web($child['child_name']));
                    }
                }
            }

            // Đổi trạng thái thành đã thông báo
            $reportDao->updateNotifiedForReports($ids);

            $db->commit();
            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/school/' . $_POST['school_username'] . '/reports";'));
            break;

        case 'delete_temp':
            $db->begin_transaction();
            if(!$_POST['id'] || !is_numeric($_POST['id'])) {
                _error(404);
            }
            // Xóa trong bảng ci_report_template
            $reportDao->deleteReportTemplate($_POST['id']);

            // Xóa trong bảng ci_report_template_detail
            $reportDao->deleteReportTemplateDetail($_POST['id']);

            $db->commit();
            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/school/' . $_POST['school_username'] . '/reports/listtemp";'));
            break;
        case 'notify':
            $db->begin_transaction();
            //Lấy ra thông tin thông báo
            $data = $reportDao->getReportById($_POST['id']);

            if (is_null($data)) {
                _error(404);
            }
            $data['file_name'] = convertText4Web($data['file_name']);

//            if ($data['created_user_id'] != $user->_data['user_id']) {
//                _error(403);
//            }

            if (!$data['is_notified']) {
                $argEmail = array();
                if (!is_empty($data['source_file'])) {
                    $argEmail['file_attachment'] = $system['system_uploads_directory'].'/'.$data['source_file_path'];
                    $argEmail['file_attachment_name'] = $data['file_name'];
                }

                $content = "";
                for($i = 0; $i < count($data['detail']); $i++) {
                    $content = $content . "- <b>" . $data['detail'][$i]['report_detaill_name'] . "-</b>: " . $data['detail'][$i]['report_detail_content'] . "<br/><br/>";
                }
                $smarty->assign('title', $data['report_name']);
                $smarty->assign('content', $content);
                $smarty->assign('class_name', convertText4Web($class['group_title']));

                $argEmail['action'] = "create_report";
                $argEmail['subject'] = $data['report_name'];
                $argEmail['school_id'] = $data['school_id'];
                $argEmail['user_id'] = $user->_data['user_id'];


                $receivers = "";
                $parent_email = array();
                //$child = $childDao->getChild($data['child_id']);
                $child = getChildData($data['child_id'], CHILD_INFO);
                if (!is_null($child)) {
                    if (!is_empty($child['parent_email'])) {
                        $parent_email[] = $child['parent_email'];

                    }
                    //$parents = $childDao->getParent($child['child_id']);
                    $parents = getChildData($child['child_id'], CHILD_PARENTS);
                    if (!is_null($parents)) {
                        $parentIds = array_keys($parents);
                        //thông báo về cho phụ huynh
                        $userDao->postNotifications($parentIds, NOTIFICATION_NEW_REPORT, NOTIFICATION_NODE_TYPE_CHILD,
                            $_POST['id'], convertText4Web($data['report_name']), $child['child_id'], convertText4Web($child['child_name']));

                        foreach ($parents as $parent) {
                            // lấy danh sách email
                            if (!is_empty($parent['user_email'])) {
                                $parent_email[] = $parent['user_email'];
                            }
                        }
                    }
                    $parent_email = array_unique($parent_email);
                    $emai_str = implode(",", $parent_email);
                    $receivers = $receivers . ',' . $emai_str;

                    $smarty->assign('child_name', convertText4Web($child['child_name']));
                    $argEmail['content'] = $smarty->fetch("ci/email_templates/create_report.tpl");
                    $argEmail['receivers'] = $emai_str;

                    $argEmail['delete_after_sending'] = 1;
                    //$mailDao->insertEmail($argEmail);

                }
                // Thông báo cho các quản lý trường khác khi sửa sổ liên lạc
                $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'reports', $school['page_admin']);
                $userDao->postNotifications($userManagerIds, NOTIFICATION_UPDATE_REPORT, NOTIFICATION_NODE_TYPE_SCHOOL,
                    $_POST['id'], convertText4Web($data['report_name']), $school['page_name'], convertText4Web($child['child_name']));

                /* Coniu - Tương tác trường */
                setIsAddNew($school['page_id'], 'report_created', [$_POST['id']]);
                /* Coniu - END */
            }

            // Cập nhật trạng thái đã gửi thông báo.
            $reportDao->updateStatusToNotified($_POST['id']);

            $db->commit();
            return_json( array('success' => true, 'message' => __("Success")));
            break;
        case 'add_cate':
            $db->begin_transaction();

            // Lấy dữ liệu truyền lên
            $categoryName = null;
            $categoryName = trim($_POST['category_name']);
            $suggests = array();
            $suggests = array_map("trim", $_POST['suggests']);
            $suggests = array_diff($suggests, array(""));
            // Lưu vào database
            $lastId = $reportDao->createReportTemplateCategory($categoryName, $suggests, $school['page_id']);

            $db->commit();
            return_json(array('callback' => 'window.location = "'.$system['system_url'].'/school/'.$_POST['school_username'].'/reports/listcate";'));
            break;
        case 'add_cate_temp':
            $db->begin_transaction();

            // Lấy dữ liệu truyền lên
            $categoryName = null;
            $categoryName = trim($_POST['category_name']);
            $suggests = array();
//            $suggests = $_POST['suggests'];
            $suggests = array_map("trim", $_POST['suggests']);
            $suggests = array_diff($suggests, array(""));
            $reportTemplateId = $_POST['report_template_id'];

            // Lưu vào database
            $lastId = $reportDao->createReportTemplateCategory($categoryName, $suggests, $school['page_id']);
            $category_new_pm = '';
            $category_new = $reportDao->getReportTemplateCategoryDetail($lastId);
            // Lấy số hạng mục của trường, chưa tối ưu, sửa sau
            $categorys = $reportDao->getAllCategoryOfSchool($school['page_id']);
            $count = count($categorys);
            $idx = $count;
            $smarty->assign('category_new', $category_new);
            $smarty->assign('username', $school['page_name']);
            $smarty->assign('idx', $idx);
            $smarty->assign('reportTemplateId', $reportTemplateId);
            $category_new_pm = $category_new_pm.$smarty->fetch("ci/school/ajax.school.addtemp.category.new.tpl");
            $return['category_new_pm'] = $category_new_pm;
            $db->commit();
            return_json($return);
//            return_json(array('callback' => 'window.location = "'.$system['system_url'].'/school/'.$_POST['school_username'].'/reports/addtemp";'));
            break;
        case 'edit_cate':
            $db->begin_transaction();
            // Lấy dữ liệu truyền lên
            $categoryId = $_POST['category_id'];
            $categoryName = null;
            $categoryName = trim($_POST['category_name']);
            $suggests = array();
//            $suggests = $_POST['suggests'];
            $suggests = array_map("trim", $_POST['suggests']);
            $suggests = array_diff($suggests, array(""));
            // Cập nhật db ci_report_template_category
            $reportDao->updateReportTemplateCategory($categoryId, $categoryName, $suggests);

            $db->commit();
            if($_POST['p5'] == 1) {
                return_json(array('callback' => 'window.location = "'.$system['system_url'].'/school/'.$_POST['school_username'].'/reports/listcate";'));
            } else {
                if(isset($_POST['p6'])) {
                    return_json(array('callback' => 'window.location = "'.$system['system_url'].'/school/'.$_POST['school_username'].'/reports/edittemp/' . $_POST['p6'] . '";'));
                }
                return_json(array('callback' => 'window.location = "'.$system['system_url'].'/school/'.$_POST['school_username'].'/reports/addtemp";'));
            }
            break;
        case 'edit_cate_temp':
            $db->begin_transaction();
            // Lấy dữ liệu truyền lên
            $categoryId = $_POST['category_id'];
            $categoryName = null;
            $categoryName = trim($_POST['category_name']);
            $suggests = array();
//            $suggests = $_POST['suggests'];
            $suggests = array_map("trim", $_POST['suggests']);
            $suggests = array_diff($suggests, array(""));
            // Cập nhật db ci_report_template_category
            $reportDao->updateReportTemplateCategory($categoryId, $categoryName, $suggests);

            $db->commit();

            $return['disableSave'] = $disableSave;
            return_json($return);
            break;
            // Cái case dưới chỉ để xử lý enter, không có tác dụng gì khác
        case 'edit_cate_in_temp':
            throw new Exception(__("Bạn vui lòng kích chuột vào thao tác cụ thể"));
            break;
        case 'delete_cate':
            $db->begin_transaction();

            if(!isset($_POST['id']) || !is_numeric($_POST['id'])) {
                _error(404);
            }

            // Delete hạng mục trong bảng ci_report_template_category
            $reportDao->deleteReportCategory($_POST['id']);

            $db->commit();
            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/school/' . $_POST['school_username'] . '/reports/listcate";'));
            break;
        case 'cate_detail':
            if(!isset($_POST['report_template_category_id']) || !is_numeric($_POST['report_template_category_id'])) {
                _error(404);
            }

            $results = $reportDao->getReportTemplateCategoryDetail($_POST['report_template_category_id']);
            $smarty->assign('results', $results);
            $smarty->assign('username', $school['page_name']);

            $return['results'] = $smarty->fetch("ci/school/ajax.school.categorydetail.tpl");
            //$return['no_data'] = (count($results['children']) == 0);

            return_json($return);
            break;
        default:
            _error(400);
            break;
    }

} catch (Exception $e) {
    $db->rollback();
    return_json( array('error' => true, 'message' => $e->getMessage()) );
} finally {
    $db->autocommit(true);
}

?>