<?php
/**
 * Package: ajax/ci/bo
 *
 * @package ConIu
 * @author QuanND
 */

// fetch bootstrap
require('../../../../../bootstrap.php');

// check AJAX Request
is_ajax();
check_login();

if (is_empty($_POST['school_username']) || !valid_username($_POST['school_username'])) {
    _error(404);
}

include_once(DAO_PATH . 'dao_school.php');
include_once(DAO_PATH . 'dao_parent.php');
include_once(DAO_PATH . 'dao_teacher.php');
include_once(DAO_PATH . 'dao_user.php');
include_once(DAO_PATH . 'dao_class.php');
include_once(DAO_PATH . 'dao_child.php');
include_once(DAO_PATH . 'dao_mail.php');
include_once(DAO_PATH . 'dao_report.php');
include_once(DAO_PATH . 'dao_conduct.php');

$schoolDao = new SchoolDAO();
$parentDao = new ParentDAO();
$teacherDao = new TeacherDAO();
$userDao = new UserDAO();
$classDao = new ClassDAO();
$childDao = new ChildDAO();
$mailDao = new MailDAO();
$reportDao = new ReportDAO();
$conductDao = new ConductDAO();

//$school = $schoolDao->getSchoolByUsername($_POST['school_username'], $user->_data['user_id']);
//$school = getSchool($_POST['school_username']);
$school = getSchoolDataByUsername($_POST['school_username'], SCHOOL_INFO);


try {
    global $db;
    $return = array();
    $db->autocommit(false);
    switch ($_POST['do']) {
        case 'search_conduct':
            try {
                $result = array();

                if (!is_numeric($_POST['class_id'])) {
                    throw new Exception(__("You must choose a class"));
                }
                $children = array();
                $childs = getClassData($_POST['class_id'], CLASS_CHILDREN);
                foreach ($childs as $child) {
                    //Lấy danh sách hạnh kiểm của học sinh
                    $conduct = $conductDao->getConductOfChildren($_POST['class_id'], $child['child_id'], $_POST['school_year']);
                    if (count($conduct) > 0) {
                        $child['conduct_id'] = $conduct['conduct_id'];
                        $child['conduct1'] = $conduct['hk1'];
                        $child['conduct2'] = $conduct['hk2'];
                        $child['conduct3'] = $conduct['ck'];
                        $children[] = $child;
                    }
                }
                $smarty->assign('username', $_POST['school_username']);
                $smarty->assign('result', $children);
                $smarty->assign('canEdit', canEdit($_POST['school_username'], 'conducts'));
                $return['results'] = $smarty->fetch("ci/school/ajax.conductlist.tpl");
                // return & exit
                return_json($return);
            } catch (Exception $e) {
                $db->rollback();
                return_json(array('error' => true, 'message' => $e->getMessage()));
            } finally {
                $db->autocommit(true);
            }
            break;
        case 'edit':
            global $default_conducts;
            if (!canEdit($_POST['school_username'], 'conducts')) {
                _error(403);
            }
            $db->begin_transaction();
            $args = array();
            $args['conduct_id'] = $_POST['conduct_id'];
            $args['conduct_semester1'] = $_POST['conduct_id1'];
            $args['conduct_semester2'] = $_POST['conduct_id2'];
            $args['conduct_semester3'] = $_POST['conduct_id3'];

            $conductDao->editConduct($args);
            $db->commit();
            return_json( array('success' => true, 'message' => __("Conduct has been updated")) );
            break;
        default:
            _error(400);
            break;
    }

} catch (Exception $e) {
    $db->rollback();
    return_json(array('error' => true, 'message' => $e->getMessage()));
} finally {
    $db->autocommit(true);
}

?>