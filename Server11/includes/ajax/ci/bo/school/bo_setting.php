<?php
/**
 * Package: ajax/ci/bo
 *
 * @package ConIu
 * @author QuanND
 */

// fetch bootstrap
require('../../../../../bootstrap.php');

// check AJAX Request
is_ajax();
check_login();

if(is_empty($_POST['school_username']) || !valid_username($_POST['school_username'])) {
    _error(404);
}

include_once(DAO_PATH . 'dao_school.php');
$schoolDao = new SchoolDAO();
//$school = getSchool($_POST['school_username']);
$school = getSchoolDataByUsername($_POST['school_username'], SCHOOL_INFO);

if (is_null($school)) {
    _error(403);
}

try {
    $return = array();
    $db->autocommit(false);
    switch ($_POST['do']) {
        case 'school':

            if (!canEdit($_POST['school_username'], 'settings')) {
                _error(403);
            }
            $db->begin_transaction();
            $school_allow_comment = (isset($_POST['school_allow_comment']) && $_POST['school_allow_comment'] == 'on')? 1: 0;
            $display_children_list = (isset($_POST['display_children_list']) && $_POST['display_children_list'] == 'on')? 1: 0;
            $display_parent_info_for_others = (isset($_POST['display_parent_info_for_others']) && $_POST['display_parent_info_for_others'] == 'on')? 1: 0;
            $allow_parent_register_service = (isset($_POST['allow_parent_register_service']) && $_POST['allow_parent_register_service'] == 'on')? 1: 0;
            $allow_teacher_edit_pickup_before = (isset($_POST['allow_teacher_edit_pickup_before']) && $_POST['allow_teacher_edit_pickup_before'] == 'on')? 1: 0;
            $school_allow_medicate = (isset($_POST['school_allow_medicate']) && $_POST['school_allow_medicate'] == 'on')? 1: 0;

            $schoolDao->updateSchoolConfig($school['page_id'], $school_allow_comment, $display_children_list, $display_parent_info_for_others, $allow_parent_register_service, $allow_teacher_edit_pickup_before, $school_allow_medicate);
            $db->commit();

            /* ---------- UPDATE - MEMCACHE ---------- */
            $array_update = array(
                'school_allow_comment' => $school_allow_comment,
                'display_children_list' => $display_children_list,
                'display_parent_info_for_others' => $display_parent_info_for_others,
                'allow_parent_register_service' => $allow_parent_register_service,
                'allow_teacher_edit_pickup_before' => $allow_teacher_edit_pickup_before,
                'school_allow_medicate' => $school_allow_medicate
            );
            updateSchoolData($school['page_id'], SCHOOL_CONFIG, $array_update);
            /* ---------- END - MEMCACHE ---------- */

            //unsetRelatedObjects();
            return_json( array('success' => true, 'message' => __("Done, School setting has been updated")) );
            break;

        case 'class':
            if (!canEdit($_POST['school_username'], 'settings')) {
                _error(403);
            }
            $db->begin_transaction();
            $class_allow_post = (isset($_POST['class_allow_post']) && $_POST['class_allow_post'] == 'on')? 1: 0;
            $allow_class_event = (isset($_POST['allow_class_event']) && $_POST['allow_class_event'] == 'on')? 1: 0;
            $class_allow_comment = (isset($_POST['class_allow_comment']) && $_POST['class_allow_comment'] == 'on')? 1: 0;
            $children_use_no_class = (isset($_POST['children_use_no_class']) && $_POST['children_use_no_class'] == 'on')? 1: 0;
            $schoolDao->updateClassConfig($school['page_id'], $class_allow_post, $allow_class_event, $class_allow_comment, $children_use_no_class);
            $db->commit();

            /* ---------- UPDATE - MEMCACHE ---------- */
            $array_update = array(
                'class_allow_post' => $class_allow_post,
                'allow_class_event' => $allow_class_event,
                'class_allow_comment' => $class_allow_comment,
                'children_use_no_class' => $children_use_no_class
            );
            updateSchoolData($school['page_id'], SCHOOL_CONFIG, $array_update);
            /* ---------- END - MEMCACHE ---------- */

            return_json( array('success' => true, 'message' => __("Done, School setting has been updated")) );
            break;

        case 'attendance':
            if (!canEdit($_POST['school_username'], 'settings')) {
                _error(403);
            }
            $db->begin_transaction();
            $allow_teacher_rolls_days_before = (isset($_POST['allow_teacher_rolls_days_before']) && $_POST['allow_teacher_rolls_days_before'] == 'on')? 1: 0;
            $attendance_allow_delete_old = (isset($_POST['attendance_allow_delete_old']) && $_POST['attendance_allow_delete_old'] == 'on')? 1: 0;
            $attendance_use_leave_early = (isset($_POST['attendance_use_leave_early']) && $_POST['attendance_use_leave_early'] == 'on')? 1: 0;
            $attendance_use_come_late = (isset($_POST['attendance_use_come_late']) && $_POST['attendance_use_come_late'] == 'on')? 1: 0;
            $attendance_absence_no_reason = (isset($_POST['attendance_absence_no_reason']) && $_POST['attendance_absence_no_reason'] == 'on')? 1: 0;
            $schoolDao->updateAttendanceConfig($school['page_id'], $allow_teacher_rolls_days_before, $attendance_allow_delete_old, $attendance_use_leave_early, $attendance_use_come_late, $attendance_absence_no_reason);
            $db->commit();

            /* ---------- UPDATE - MEMCACHE ---------- */
            $array_update = array(
                'allow_teacher_rolls_days_before' => $allow_teacher_rolls_days_before,
                'attendance_allow_delete_old' => $attendance_allow_delete_old,
                'attendance_use_leave_early' => $attendance_use_leave_early,
                'attendance_use_come_late' => $attendance_use_come_late,
                'attendance_absence_no_reason' => $attendance_absence_no_reason
            );
            updateSchoolData($school['page_id'], SCHOOL_CONFIG, $array_update);
            /* ---------- END - MEMCACHE ---------- */

            return_json( array('success' => true, 'message' => __("Done, School setting has been updated")) );
            break;

        case 'tuition':
            if (!canEdit($_POST['school_username'], 'settings')) {
                _error(403);
            }
            $db->begin_transaction();
            $allow_class_see_tuition = (isset($_POST['allow_class_see_tuition']) && $_POST['allow_class_see_tuition'] == 'on')? 1: 0;
            $tuition_view_attandance = (isset($_POST['tuition_view_attandance']) && $_POST['tuition_view_attandance'] == 'on')? 1: 0;
            $tuition_use_mdservice_deduction = (isset($_POST['tuition_use_mdservice_deduction']) && $_POST['tuition_use_mdservice_deduction'] == 'on')? 1: 0;

            $schoolDao->updateTuitionConfig($school['page_id'], $_POST['notification_tuition_interval'], $_POST['bank_account'], $allow_class_see_tuition, $tuition_view_attandance,
                                    $tuition_use_mdservice_deduction, $_POST['tuition_child_report_template']);
            $db->commit();

            /* ---------- UPDATE - MEMCACHE ---------- */
            $array_update = array(
                'notification_tuition_interval' => $_POST['notification_tuition_interval'],
                'bank_account' => $_POST['bank_account'],
                'allow_class_see_tuition' => $allow_class_see_tuition,
                'tuition_view_attandance' => $tuition_view_attandance,
                'tuition_use_mdservice_deduction' => $tuition_use_mdservice_deduction,
                'tuition_child_report_template' => $_POST['tuition_child_report_template']
            );
            updateSchoolData($school['page_id'], SCHOOL_CONFIG, $array_update);
            /* ---------- END - MEMCACHE ---------- */

            return_json( array('success' => true, 'message' => __("Done, School setting has been updated")) );
            break;

        case 'camera':
            if (!canEdit($_POST['school_username'], 'settings')) {
                _error(403);
            }
            $schoolCon = getSchoolDataByUsername($_POST['school_username'], SCHOOL_CONFIG);

            $db->begin_transaction();

            $args['camera_guide'] = $schoolCon['camera_guide'];
            if(!$_POST['is_file']) {
                $args['camera_guide'] = "";
            }
            $file_name = "";
            // Upload file đính kèm
            if(file_exists($_FILES['file']['tmp_name'])) {
                // check file upload enabled
                if(!$system['file_enabled']) {
                    throw new Exception(__("This feature has been disabled"));
                }

                // valid inputs
                if(!isset($_FILES["file"]) || $_FILES["file"]["error"] != UPLOAD_ERR_OK) {
                    throw new Exception(__("Something wrong with upload! Is 'upload_max_filesize' set correctly?"));
                }

                // check file size
                $max_allowed_size = $system['max_file_size'] * 1024;
                if($_FILES["file"]["size"] > $max_allowed_size) {
                    throw new Exception(__("The file size is so big"));
                }

                // check file extesnion
                $extension = get_extension($_FILES['file']['name']);
                if(!valid_extension($extension, $system['file_report_extensions'])) {
                    throw new Exception(__("The file type is not valid or not supported"));
                }

                /* check & create uploads dir */
                $depth = '../../../../../';
                $folder = 'cameras/'. $school['page_id'];
                if(!file_exists($depth.$system['system_uploads_directory'].'/'.$folder)) {
                    @mkdir($depth.$system['system_uploads_directory'].'/'.$folder, 0777, true);
                }
                if(!file_exists($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y'))) {
                    @mkdir($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y'), 0777, true);
                }
                if(!file_exists($system['system_uploads_directory'].'/'.$folder.'/' . date('Y') . '/' . date('m'))) {
                    @mkdir($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y') . '/' . date('m'), 0777, true);
                }

                /* prepare new file name */
                $directory = $folder.'/'. date('Y') . '/' . date('m') . '/';
                $prefix = $system['uploads_prefix'].'_'.md5(time()*rand(1, 9999));
                $file_name = $directory.$prefix.'.'.$extension;
                $path = $depth.$system['system_uploads_directory'].'/'.$file_name;

                /* check if the file uploaded successfully */
                if(!@move_uploaded_file($_FILES['file']['tmp_name'], $path)) {
                    throw new Exception(__("Sorry, can not upload the file"));
                }
                $args['camera_guide'] = $file_name;
            }
            $args['school_id'] = $school['page_id'];

            // update link camera
            $schoolDao->updateSchoolConfigCamera($args);

            $db->commit();

            /* ---------- UPDATE - MEMCACHE ---------- */
            $cameraUrl = "";
            if($args['camera_guide'] != '') {
                $cameraUrl = $system['system_uploads'] . '/' . $args['camera_guide'];
            }
            $array_update = array(
                'camera_guide' => $cameraUrl,
            );
            updateSchoolData($school['page_id'], SCHOOL_CONFIG, $array_update);
            /* ---------- END - MEMCACHE ---------- */

            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/school/' . $_POST['school_username'] . '/settings";'));
            break;

        case 'report':
            if (!canEdit($_POST['school_username'], 'settings')) {
                _error(403);
            }
            $db->begin_transaction();
            $report_interval = (is_empty($_POST['report_interval'])) ? 'null' : $_POST['report_interval'];

            $schoolDao->updateSchoolReportInterval($school['page_id'], $report_interval);
            $db->commit();

            /* ---------- UPDATE - MEMCACHE ---------- */
            $array_update = array(
                'report_cycle' => $_POST['report_interval']
            );
            updateSchoolData($school['page_id'], SCHOOL_CONFIG, $array_update);
            /* ---------- END - MEMCACHE ---------- */

            return_json( array('success' => true, 'message' => __("Done, School setting has been updated")) );
            break;

        case 'menu_display':
            $_SESSION['menu_status'] = $_POST['status'];
            break;

        case 'notification':
//            if (!canEdit($_POST['school_username'], 'settings')) {
//                _error(403);
//            }
            $db->begin_transaction();
            $args = array();
            $args['school_id'] = $school['page_id'];
            $teacher = array();
            $parent = array();
            foreach ($notifyModules as $module) {
                // $module_name = $module['module'];
                $teacher[] = isset($_POST[$module['module'] . "_teacher"]) ? $_POST[$module['module'] . "_teacher"] : 0;
                $parent[] = isset($_POST[$module['module'] . "_parent"]) ? $_POST[$module['module'] . "_parent"] : 0;
            }
            $is_notification_setting = $schoolDao->checkNotificationSetting($user->_data['user_id'], $school['page_id']);
            if($is_notification_setting) {
                $schoolDao->deleteNotificationSetting($user->_data['user_id'], $school['page_id']);
            }
            $schoolDao->insertNotificationSetting($user->_data['user_id'], $school['page_id'], $teacher, $parent);
            $db->commit();

            /* ---------- UPDATE - MEMCACHE ---------- */
            updateSchoolData($school['page_id'], SCHOOL_NOTIFICATIONS);
            /* ---------- END - MEMCACHE ---------- */

            return_json( array('success' => true, 'message' => __("Done, School setting has been updated")) );
            break;
        case 'update_step':
            // Cập nhật thông tin school_step
            $db->begin_transaction();

            if(!isset($_POST['school_username'])) {
                _error(404);
            }
            $school = getSchoolDataByUsername($_POST['school_username'], SCHOOL_DATA);

            if(!isset($_POST['step']) || !is_numeric($_POST['step'])) {
                _error(400);
            }
            $view = isset($_POST['view']) ? $_POST['view'] : '';
            $subview = isset($_POST['subview']) ? $_POST['subview'] : '';
            if(($school['school_step'] < $_POST['step']) || $_POST['step'] == 0) {
                $schoolIds = array();
                $schoolIds[] = $school['page_id'];
                $schoolDao->updateSchoolStep($schoolIds, $_POST['step']);
            }
            $db->commit();

            /* ---------- UPDATE - MEMCACHE ---------- */
            //1.Cập nhật thông tin trường
            //unset($args['username_old']);
            updateSchoolData($school['page_id'], SCHOOL_CONFIG);
            /* ---------- END - MEMCACHE ---------- */

            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/school/' . $_POST['school_username'] . '/' . $view . '/' . $subview . '";'));
            break;

        default:
            _error(400);
            break;
    }

} catch (Exception $e) {
    $db->rollback();
    return_json( array('error' => true, 'message' => $e->getMessage()) );
} finally {
    $db->autocommit(true);
}

?>