<?php
/**
 * Package: ajax/ci/bo
 *
 * @package ConIu
 * @author QuanND
 */
// fetch bootstrap
require('../../../../../bootstrap.php');

// check AJAX Request
is_ajax();
check_login();

if(is_empty($_POST['school_username']) || !valid_username($_POST['school_username'])) {
    _error(404);
}

include_once(DAO_PATH.'dao_school.php');
include_once(DAO_PATH.'dao_class.php');
include_once(DAO_PATH.'dao_role.php');
include_once(DAO_PATH.'dao_user.php');

$schoolDao = new SchoolDAO();
$classDao = new ClassDAO();
$roleDao = new RoleDao();
$userDao = new UserDao();

//$school = getSchool($_POST['school_username']);
$school = getSchoolDataByUsername($_POST['school_username'], SCHOOL_INFO);
if (is_null($school)) {
    _error(403);
}
if (!canEdit($_POST['school_username'], 'roles')) {
    _error(403);
}

try {
    $db->autocommit(false);
    switch ($_POST['do']) {
        case 'add':
            $db->begin_transaction();

            $args = array();
            $args['school_id'] = $school['page_id'];
            $args['role_name'] = $_POST['role_name'];
            $args['description'] = $_POST['description'];

            $permissions = array();
            foreach ($systemModules as $module) {
                if ($_POST[$module['module']] != PERM_NONE) {
                    $permission = array('module' => $module['module'], 'value' => $_POST[$module['module']]);
                    $permissions[] = $permission;
                }
            }
            $roleDao->insertRole($args, $permissions);

            $db->commit();
            return_json(array('callback' => 'window.location = "'.$system['system_url'].'/school/'.$_POST['school_username'].'/roles";'));
            break;

        case 'edit':
            $db->begin_transaction();

            // Lấy những user có quyền quản lý nhóm lớp cũ
            $oldManageUserIds = $roleDao->getUserIdsOfModule($school['page_id'], 'managegroups');

            $args = array();
            $args['role_id'] = $_POST['role_id'];
            $args['role_name'] = $_POST['role_name'];
            $args['description'] = $_POST['description'];

            $permissions = array();
            foreach ($systemModules as $module) {
                if ($_POST[$module['module']] != PERM_NONE) {
                    $permission = array('module' => $module['module'], 'value' => $_POST[$module['module']]);
                    $permissions[] = $permission;
                }
            }
            $roleDao->updateRole($args, $permissions);

            // Lấy user có quyền quản đối với module managegroups
            $newManageUserIds = $roleDao->getUserIdsOfModule($school['page_id'], 'managegroups');

            // Những user được thêm mới vào quyền quản lý group lớp
            $addManageUserIds = array_diff($newManageUserIds, $oldManageUserIds);

            // Những user bị loại khỏi quyền quản lý group lớp
            $unManageUserIds = array_diff($oldManageUserIds, $newManageUserIds);

// Kiểm tra xem user có phải hiệu trưởng không, nếu là hiệu trưởng thì không loại khỏi group lớp
            $schoolConfig = getSchoolData($school['page_id'], SCHOOL_CONFIG);
            $principals = array();
            if($schoolConfig['principal'] != '') {
                $principals = explode(',', $schoolConfig['principal']);
            }
            $unManageUserIds = array_diff($unManageUserIds, $principals);

            // Kiểm tra xem có phải là page_admin không, nếu là page_admin thì cũng không loại khỏi group
            $page_admins = array();
            $page_admins[] = $school['page_admin'];
            $unManageUserIds = array_diff($unManageUserIds, $page_admins);

            $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
            foreach ($classes as $class) {
                // Thêm những user mới vào group lớp
                if(count($newManageUserIds) > 0) {
                    $classDao->addUserToClass($class['group_id'], $newManageUserIds);
                }

                // Bỏ user khỏi group lớp
                // Kiểm tra xem có phải là giáo viên dạy lớp đó không, nếu là giáo viên thì cũng không bỏ ra khỏi group lớp
                $teachers = getClassData($class['group_id'], CLASS_TEACHERS);
                $teachersIds = array_keys($teachers);
                $unManageUserIds = array_diff($unManageUserIds, $teachersIds);
                $unManageUserIds = array_unique($unManageUserIds);
                if(count($unManageUserIds) > 0) {
                    $classDao->deleteUserFromClass($class['group_id'], $unManageUserIds);
                }
            }

            $userIds = $roleDao->getUserIdHaveRole($school['page_id'], $_POST['role_id']);

            /* ---------- UPDATE - MEMCACHE ---------- */
            //Cập nhật thông tin quản lý cho user
            foreach ($userIds as $userId) {
                updateUserManageData($userId, USER_MANAGE_SCHOOL);
            }
            /* ---------- END - MEMCACHE ---------- */

            $db->commit();

            //return_json( array('success' => true, 'message' => __("Role information have been updated")) );
            return_json(array('callback' => 'window.location = "'.$system['system_url'].'/school/'.$_POST['school_username'].'/roles";'));
            break;

        case 'delete':
            if(!isset($_POST['id']) || !is_numeric($_POST['id'])) {
                _error(400);
            }
            $db->begin_transaction();
            $roleDao->deleteRole($school['page_id'], $_POST['id']);
            $db->commit();

            return_json(array('callback' => 'window.location = "'.$system['system_url'].'/school/'.$_POST['school_username'].'/roles";'));
            break;

        case 'assignrole':
            $db->begin_transaction();

            $postUserIds = isset($_POST['userIds']) ? $_POST['userIds'] : array();

            // Lấy những user có quyền quản lý nhóm lớp cũ
            $oldManageUserIds = $roleDao->getUserIdsOfModule($school['page_id'], 'managegroups');
            // Lấy id những user đang có quyền với role hiện tại
            $userIds = $roleDao->getUserIdHaveRole($school['page_id'], $_POST['role_id']);

            $roleDao->insertUser2Role($school['page_id'], $_POST['role_id'], $postUserIds);

            // Lấy những user bị loại khỏi role
            $unassignUserIds = array_diff($userIds, $postUserIds);

            // Những user được thêm mới
            $newUserIds = array_diff($postUserIds, $userIds);

            // Lấy user có quyền quản đối với module managegroups
            $newManageUserIds = $roleDao->getUserIdsOfModule($school['page_id'], 'managegroups');

            // Những user được thêm mới vào quyền quản lý group lớp
            $addManageUserIds = array_diff($newManageUserIds, $oldManageUserIds);

            // Những user bị loại khỏi quyền quản lý group lớp
            $unManageUserIds = array_diff($oldManageUserIds, $newManageUserIds);

            // Kiểm tra xem user có phải hiệu trưởng không, nếu là hiệu trưởng thì không loại khỏi group lớp
            $schoolConfig = getSchoolData($school['page_id'], SCHOOL_CONFIG);
            $principals = array();
            if($schoolConfig['principal'] != '') {
                $principals = explode(',', $schoolConfig['principal']);
            }
            $unManageUserIds = array_diff($unManageUserIds, $principals);

            // Kiểm tra xem có phải là page_admin không, nếu là page_admin thì cũng không loại khỏi group
            $page_admins = array();
            $page_admins[] = $school['page_admin'];
            $unManageUserIds = array_diff($unManageUserIds, $page_admins);

            $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
            foreach ($classes as $class) {
                // Thêm những user mới vào group lớp
                if(count($postUserIds)) {
                    $classDao->addUserToClass($class['group_id'], $postUserIds);
                }

                // Bỏ user khỏi group lớp
                // Kiểm tra xem có phải là giáo viên dạy lớp đó không, nếu là giáo viên thì cũng không bỏ ra khỏi group lớp
                $teachers = getClassData($class['group_id'], CLASS_TEACHERS);
                $teachersIds = array_keys($teachers);
                $unManageUserIds = array_diff($unManageUserIds, $teachersIds);
                $unManageUserIds = array_unique($unManageUserIds);
                if(count($unManageUserIds) > 0) {
                    $classDao->deleteUserFromClass($class['group_id'], $unManageUserIds);
                }
            }

            // Thông báo cho người được phân quyền
            $userDao->postNotifications($newUserIds, NOTIFICATION_UPDATE_ROLE, NOTIFICATION_NODE_TYPE_SCHOOL,
                $_POST['role_id'], '', $school['page_name'], convertText4Web($school['page_title']));
            $db->commit();

            /* ---------- UPDATE - MEMCACHE ---------- */
            //Cập nhật thông tin quản lý cho user
            foreach ($postUserIds as $userId) {
                updateUserManageData($userId, USER_MANAGE_SCHOOL);
            }
            foreach ($unassignUserIds as $userId) {
                updateUserManageData($userId, USER_MANAGE_SCHOOL);
            }
            /* ---------- END - MEMCACHE ---------- */

            return_json(array('callback' => 'window.location = "'.$system['system_url'].'/school/'.$_POST['school_username'].'/roles";'));
            break;
        case 'assignprincipal':
            $db->begin_transaction();
            $oldPrincipals = array();
            // danh sách hiệu trưởng cũ
            $schoolConfig = getSchoolData($school['page_id'], SCHOOL_CONFIG);
            if (!is_empty($schoolConfig['principal'])) {
                $oldPrincipals = explode(',', $schoolConfig['principal']);
            }

            // Lấy id những user gửi lên
            $postUserIds = isset($_POST['userIds']) ? $_POST['userIds'] : array();
            $principals = implode(',', $postUserIds);

            // Những user bỏ phân quyền hiệu trưởng
            $unassignPrins = array_diff($oldPrincipals, $postUserIds);

            // Lấy user có quyền quản đối với module managegroups
            $newManageUserIds = $roleDao->getUserIdsOfModule($school['page_id'], 'managegroups');

            // Những user có quyền quản lý group thì không loại khỏi group
            $unassignPrins = array_diff($unassignPrins, $newManageUserIds);

            // Lấy danh sách các lớp của trường
            $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
            $classIds = array_keys($classes);

            // Những user thêm mới quyền hiệu trưởng
            $newPrins = array_diff($postUserIds, $oldPrincipals);

            foreach ($classIds as $classId) {
                // Kiểm tra nếu user là giáo viên của lớp thì không loại khỏi group
                $teachers = getClassData($classId, CLASS_TEACHERS);
                $teachersIds = array_keys($teachers);
                $unassignPrins = array_diff($unassignPrins, $teachersIds);
                // Loại user khỏi group
                if(count($unassignPrins) > 0) {
                    $classDao->deleteUserFromClass($classId, $unassignPrins);
                }

                // Thêm mới giáo viên vào group
                if(count($postUserIds) > 0) {
                    $classDao->addUserToClass($classId, $postUserIds);
                }
            }

            // Cập nhật danh sách hiệu trưởng trong bảng ci_school_configuration
            $schoolDao->updateSchoolPrincipal($school['page_id'], $postUserIds);

            // Thông báo cho người được phân làm hiệu trưởng
            $userDao->postNotifications($newPrins, NOTIFICATION_UPDATE_ROLE, NOTIFICATION_NODE_TYPE_SCHOOL,
                $school['page_id'], '', $school['page_name'], convertText4Web($school['page_title']));
            $db->commit();

            /* ---------- UPDATE - MEMCACHE ---------- */
            updateSchoolData($school['page_id'], SCHOOL_CONFIG, array('principal' => $principals));
            //Cập nhật thông tin quản lý cho user
            foreach ($postUserIds as $userId) {
                updateUserManageData($userId, USER_MANAGE_SCHOOL);
            }
            foreach ($unassignPrins as $userId) {
                updateUserManageData($userId, USER_MANAGE_SCHOOL);
            }
            /* ---------- END - MEMCACHE ---------- */

//            return_json(array('callback' => 'window.location = "'.$system['system_url'].'/school/'.$_POST['school_username'].'/roles";'));
            return_json(array('success' => true, 'message' => __("Done, principal list has been updated")));
            break;

        case 'default':
            $db->begin_transaction();

            $roleDao->insertDefaultRoles($school['page_id']);

            $db->commit();
            return_json(array('callback' => 'window.location = "'.$system['system_url'].'/school/'.$_POST['school_username'].'/roles";'));
            break;

        default:
            _error(400);
            break;
    }

} catch (Exception $e) {
    $db->rollback();
    return_json( array('error' => true, 'message' => $e->getMessage()) );
} finally {
    $db->autocommit(true);
}
?>