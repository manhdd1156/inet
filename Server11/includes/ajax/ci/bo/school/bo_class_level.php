<?php
/**
 * Package: ajax/ci/bo
 * 
 * @package ConIu
 * @author QuanND
 */

// fetch bootstrap
require('../../../../../bootstrap.php');

// check AJAX Request
is_ajax();
check_login();

//include_once(DAO_PATH . 'dao_school.php');
include_once(DAO_PATH . 'dao_class_level.php');
//include_once(DAO_PATH . 'dao_user.php');
include_once(DAO_PATH . 'dao_user.php');

//$schoolDao = new SchoolDAO();
$classLevelDao = new ClassLevelDAO();
//$userDao = new UserDAO();
$userDao = new UserDAO();

//$school = getSchool($_POST['school_username']);
$school = getSchoolDataByUsername($_POST['school_username'], SCHOOL_INFO);
if (is_null($school)) {
    _error(403);
}
if (!canEdit($_POST['school_username'], 'classlevels')) {
    _error(403);
}

$args = array();
$args['class_level_name'] = $_POST['class_level_name'];
$args['gov_class_level'] = $_POST['gov_class_level'];
$args['description'] = $_POST['description'];
$args['school_id'] = $school['page_id'];

try {
    $db->begin_transaction();
    switch ($_POST['do']) {
        case 'edit':
            // valid inputs
            if(!isset($_POST['class_level_id']) || !is_numeric($_POST['class_level_id'])) {
                _error(400);
            }
            $args['class_level_id'] = $_POST['class_level_id'];
            $classLevelDao->updateClassLevel($args);
            // Thông báo cho các quản lý trường khác khối lớp được cập nhật
            $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'classlevels', $school['page_admin']);
            $userDao->postNotifications($userManagerIds, NOTIFICATION_UPDATE_CLASSLEVEL, NOTIFICATION_NODE_TYPE_SCHOOL,
                $_POST['class_level_id'], convertText4Web($_POST['class_level_name']), $school['page_name'], convertText4Web($school['page_title']));

            $db->commit();

            /* ---------- UPDATE - MEMCACHE ---------- */
            //1. Cập nhật thông tin khối lớp
            $array_update = array(
                'class_level_name' => $args['class_level_name'],
                'description' => $args['description']
            );
            updateClassLevelData($args['class_level_id'], CLASS_LEVEL_INFO, $array_update);

            //2. Cập nhật thông tin lớp (trong khối)
            $classes = getClassLevelData($args['class_level_id'], CLASS_LEVEL_CLASSES);
            if(count($classes) > 0) {
                $classIds = array_keys($classes);
                foreach ($classIds as $classId){
                    updateClassData($classId, CLASS_INFO);
                }
            }
            /* ---------- END - MEMCACHE ---------- */

            //return_json( array('success' => true, 'message' => __("Done, Class level info have been updated")) );
            return_json(array('callback' => 'window.location = "'.$system['system_url'].'/school/'.$_POST['school_username'].'/classlevels";'));
            break;

        case 'add':
            $class_level_id = $classLevelDao->insertClassLevel($args);

            // Thông báo cho các quản lý trường khác khối lớp được cập nhật
            $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'classlevels', $school['page_admin']);
            $userDao->postNotifications($userManagerIds, NOTIFICATION_NEW_CLASSLEVEL, NOTIFICATION_NODE_TYPE_SCHOOL,
                $class_level_id, convertText4Web($_POST['class_level_name']), $school['page_name'], convertText4Web($school['page_title']));

            /* ---------- ADD - MEMCACHE ---------- */
            //1.Cập nhật thông tin khối lớp trong trường
            $array_add = array(
                'school_id' => $args['school_id'],
                'class_level_id' => $class_level_id,
                'class_level_name' => $args['class_level_name'],
                'description' => $args['description'],
                'cnt' => 0
            );
            addSchoolData($args['school_id'], SCHOOL_CLASS_LEVELS, $class_level_id);
            /* ---------- END - MEMCACHE ---------- */

            $db->commit();
            return_json(array('callback' => 'window.location = "'.$system['system_url'].'/school/'.$_POST['school_username'].'/classlevels";'));
            break;

        case 'delete':
            if(!isset($_POST['id']) || !is_numeric($_POST['id'])) {
                _error(400);
            }
            $classLevelDao->deleteClassLevel($_POST['id']);

            /* ---------- DELETE - MEMCACHE ---------- */
            //1.Xóa thông tin khối lớp trong trường
            deleteSchoolData($args['school_id'], SCHOOL_CLASS_LEVELS, $_POST['id']);
            //2.Xóa khối lớp
            deleteClassLevelData($_POST['id']);
            /* ---------- END - MEMCACHE ---------- */

            $db->commit();
            return_json(array('callback' => 'window.location = "'.$system['system_url'].'/school/'.$_POST['school_username'].'/classlevels";'));
            break;

        default:
            _error(400);
            break;
    }
} catch (Exception $e) {
    $db->rollback();
    return_json( array('error' => true, 'message' => $e->getMessage()) );
} finally {
    $db->autocommit(true);
}

?>