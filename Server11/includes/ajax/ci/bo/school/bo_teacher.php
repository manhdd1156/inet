<?php
/**
 * Package: ajax/ci/bo
 * 
 * @package ConIu
 * @author QuanND
 */

// fetch bootstrap
require('../../../../../bootstrap.php');

// check AJAX Request
is_ajax();
check_login();
if(is_empty($_POST['school_username']) || !valid_username($_POST['school_username'])) {
    _error(404);
}

include_once(DAO_PATH . 'dao_school.php');
include_once(DAO_PATH . 'dao_class.php');
include_once(DAO_PATH . 'dao_user.php');
include_once(DAO_PATH . 'dao_mail.php');
include_once(DAO_PATH . 'dao_teacher.php');
include_once(DAO_PATH . 'dao_role.php');
$schoolDao = new SchoolDAO();
$classDao = new ClassDAO();
$userDao = new UserDAO();
$mailDao = new MailDAO();
$roleDao = new RoleDAO();
$teacherDao = new TeacherDAO();

//$school = $schoolDao->getSchoolByUsername($_POST['school_username'], $user->_data['user_id']);
//$school = getSchool($_POST['school_username']);
$school = getSchoolDataByUsername($_POST['school_username'], SCHOOL_DATA);
if (!canEdit($_POST['school_username'], 'teachers')) {
    _error(403);
}

try {
    $db->autocommit(false);
    switch ($_POST['do']) {
        case 'edit':
            // valid inputs
            if(!isset($_POST['user_id']) || !is_numeric($_POST['user_id'])) {
                _error(400);
            }
            $db->begin_transaction();
            $args = array();
            $args['full_name'] = isset($_POST['full_name'])? trim($_POST['full_name']): "";
            //$args['username'] = isset($_POST['username'])? trim($_POST['username']): "";
            //$args['username'] = generateUsername($args['full_name']);
            $args['user_phone'] = standardizePhone($_POST['user_phone']);
            /*if (!validatePhone($args['user_phone'])) {
                return_json( array('error' => true, 'message' => __("The phone number you entered is incorrect")) );
            }*/
            $args['email'] = isset($_POST['email'])? trim($_POST['email']): "";
            $args['password'] = $_POST['password'];
            $args['gender'] = $_POST['gender'];
            $args['user_id'] = $_POST['user_id'];

            //$teacher_old = $teacherDao->getTeacher($_POST['user_id']);
            $teacher_old = getTeacherData($_POST['user_id'], TEACHER_INFO);
            if (is_null($teacher_old)) {
                _error(404);
            }
            //$args['username_old'] = $teacher_old['user_name'];
            $args['email_old'] = $teacher_old['user_email'];
            $userDao->editUser($args);

            //Cập nhật vai trò của giáo viên.
            if (isset($_POST['role_ids']) && (count($_POST['role_ids']) > 0)) {
                $roleDao->insertRole2User($school['page_id'], $_POST['role_ids'], $_POST['user_id']);
                //Memcache - Cập nhật vai trò cho giáo viên
                updateUserManageData($args['user_id'], USER_MANAGE_SCHOOL);
            }

            /* ---------- UPDATE - MEMCACHE ---------- */
            //1. Cập nhật thông tin cho giáo viên
            $array_update = array(
                'user_fullname' => $args['full_name'],
                'user_phone' => $args['user_phone'],
                'user_email' => $args['email'],
                'user_gender' => $args['gender']
            );
            updateTeacherData($args['user_id'], TEACHER_INFO, $array_update);
            /* ---------- END - MEMCACHE ---------- */

            $db->commit();
            //return_json( array('success' => true, 'message' => __("Done, Teacher info have been updated")) );
            return_json(array('callback' => 'window.location = "'.$system['system_url'].'/school/'.$_POST['school_username'].'/teachers";'));
            break;

        case 'assign':
            // valid inputs
            if(!isset($_POST['user_id']) || !is_numeric($_POST['user_id'])) {
                _error(400);
            }
            $db->begin_transaction();

            // Lấy danh sách lớp của trường
            $schoolClasses = getSchoolData($school['page_id'], SCHOOL_CLASSES);
            //Cập nhật vai trò của giáo viên.
            $roles = $roleDao->getRoleOfUser($school['page_id'], $_POST['user_id']);
            $roleIdsOld = array();
            foreach ($roles as $role) {
                if($role['user_id'] == $_POST['user_id']) {
                    $roleIdsOld[] = $role['role_id'];
                }
            }
            $is_notify = 0;
            if(count($_POST['role_ids']) > 0) {
                if(count($roleIdsOld) != count($_POST['role_ids'])) {
                    $is_notify = 1;
                } else {
                    if(count($roleIdsOld) != 0 && count($_POST['role_ids']) != 0) {
                        $newRole = array_diff($_POST['role_ids'], $roleIdsOld);
                        $deleteRole = array_diff($roleIdsOld, $_POST['role_ids']);

                        if(count($newRole) > 0 || count($deleteRole) > 0) {
                            $is_notify = 1;
                        }
                    }
                }
            }

            if (isset($_POST['role_ids']) && (count($_POST['role_ids']) > 0)) {
                $roleDao->insertRole2User($school['page_id'], $_POST['role_ids'], $_POST['user_id']);
            } else {
                $roleDao->deleteUserRole($school['page_id'], $_POST['user_id']);
            }
            // Kiểm tra giáo viên có quyền quản lý group không
            $userIds = $roleDao->getUserIdsOfModule($school['page_id'], 'managegroups');
            if(in_array($teacherId, $userIds)) {
                // Thêm giáo viên vào tất cả các nhóm lớp của trường
                $classesSchool = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                $classIdsSchool = array_keys($classesSchool);
                if(count($classIdsSchool) > 0) {
                    $classDao->addClassToUser($_POST['user_id'], $classIdsSchool);
                }
            }
            if($is_notify) {
                // Thông báo cho người được phân quyền
                $userDao->postNotifications($_POST['user_id'], NOTIFICATION_UPDATE_ROLE, NOTIFICATION_NODE_TYPE_SCHOOL, $_POST['user_id'], '', $school['page_name'], convertText4Web($school['page_title']));
            }
            // Lấy danh sách lớp cũ của giáo viên
            $classes = getTeacherData($_POST['user_id'], TEACHER_CLASSES);
            $classIds = array();
            foreach ($classes as $class) {
                if($class['school_id'] == $school['page_id']) {
                    $classIds[] = $class['group_id'];
                }
            }
            $postClassIds = $_POST['class_ids'];

            if(count($postClassIds) == 0) {
                $newClassIds = array();
                $deleteClassIds = $classIds;
            } else {
                if(count($classIds) == 0) {
                    $newClassIds = $postClassIds;
                    $deleteClassIds = array();
                } else {
                    // Danh sách lớp được thêm mới
                    $newClassIds = array_diff($postClassIds, $classIds);

                    // danh sách lớp bỏ đi
                    $deleteClassIds = array_diff($classIds, $postClassIds);
                }
            }

            // Cập nhật lớp mới của giáo viên
            $teacherDao->insertClassListForTeacher($_POST['user_id'], $newClassIds);
            //ADD START MANHDD 04/06/2021
            // Nếu tích chọn giáo viên chủ nhiệm thì sẽ cập nhật user thành giáo viên chủ nhiệm
            foreach (count($newClassIds)>0 ? $newClassIds : $postClassIds as $classId) {
                if(isset($_POST['homeroom_'.$classId])) {
                    $teacherDao->updateHomeroomTeacher($_POST['user_id'],$classId,'becomeHomeRoom');
                }else {
                    $teacherDao->updateHomeroomTeacher($_POST['user_id'],$classId,'removeHomeRoom');
                }
            }
            //ADD END MANHDD 04/06/2021
            // Thông báo giáo viên mới về việc được phân công lớp
            foreach ($newClassIds as $newClassId) {
                $userDao->postNotifications($_POST['user_id'], NOTIFICATION_ASSIGN_CLASS, NOTIFICATION_NODE_TYPE_CLASS, $newClassId);
            }

            // Thêm giáo viên vào nhóm
            $classDao->addClassToUser($_POST['user_id'], $newClassIds);

            // Bỏ những lớp cũ
            $teacherDao->deleteClassListForTeacher($_POST['user_id'], $deleteClassIds);

            // Loại giáo viên ra khỏi nhóm và giảm số lượng thành viên của nhóm
            $classDao->deleteClassFromUser($_POST['user_id'], $deleteClassIds);

            /* ---------- UPDATE - MEMCACHE ---------- */
            //1. Memcache - Cập nhật vai trò cho giáo viên
            updateUserManageData($_POST['user_id'], USER_MANAGE_CLASSES);

            //Memcache - Cập nhật vai trò cho giáo viên
            updateUserManageData($_POST['user_id'], USER_MANAGE_SCHOOL);

            // 2. Cập nhật danh sách giáo viên lớp
            foreach ($schoolClasses as $class) {
                updateClassData($class['group_id'], CLASS_TEACHERS);
            }

            // 3. Cập nhật danh sách lớp của giáo viên
            updateTeacherData($_POST['user_id'], TEACHER_CLASSES);
            /* ---------- END - MEMCACHE ---------- */

            $db->commit();
            //return_json( array('success' => true, 'message' => __("Done, Teacher info have been updated")) );
            return_json(array('callback' => 'window.location = "'.$system['system_url'].'/school/'.$_POST['school_username'].'/teachers";'));
            break;

        case 'addexisting':
            $db->begin_transaction();
            //Lấy ra ID của user từ username
//            if ($school['role_id'] < PERMISSION_MANAGE) {
//                _error(403);
//            }

            //Danh sách ID của giáo viên mới được chọn
            $teacherIds = $_POST['user_id'];
            //Lấy ra danh sách ID của giáo viên đã có trong trường
            //$oldTeacherIds = $teacherDao->getTeacherIDOfSchool($school['page_id']);
            $teachers = getSchoolData($school['page_id'], SCHOOL_TEACHERS);
            $oldTeacherIds = array_keys($teachers);
            //Bỏ đi những ID của người đã là giáo viên trước đó
            $newTeacherIds = array_diff($teacherIds, $oldTeacherIds);

            if (count($newTeacherIds) > 0) {
                //Đưa user mới chọn thành giáo viên của trường
                $teacherDao->addTeacherToSchool($school['page_id'], $newTeacherIds);

                //Giáo viên like page của trường & Tăng số lượng like của page.
                $schoolDao->addUsersLikeSchool($school['page_id'], $newTeacherIds);
                //Giáo viên kết bạn với quản lý trường (user hiện tại).
                $userDao->becomeFriends([$user->_data['user_id']], $newTeacherIds);
                //Cập nhật số lượng giáo viên của trường
                $schoolDao->updateTeacherCount($school['page_id'], count($newTeacherIds));


                /* ---------- UPDATE - MEMCACHE ---------- */
                //1.Đưa user mới chọn thành giáo viên của trường
                foreach ($newTeacherIds as $teacherId) {
                    updateUserManageData($teacherId, USER_MANAGE_SCHOOL);
                    addSchoolData($school['page_id'], SCHOOL_TEACHERS, $teacherId);
                }

                //2.Cập nhật số lượng giáo viên của trường
                $teacherCnt = $school['teacher_count'] + count($newTeacherIds);
                $array_update = array(
                    'teacher_count' => $teacherCnt
                );
                updateSchoolData($school['page_id'], SCHOOL_CONFIG, $array_update);
                /* ---------- END - MEMCACHE ---------- */

                //unset($_SESSION[$_POST['school_username']]);
            }

            // Nếu chọn lớp, thêm giáo viên vào lớp
            if(!isset($_POST['class_option'])) {
                if(isset($_POST['class_id']) && $_POST['class_id'] > 0) {
                    $oldTeacherIdsClass = $teacherDao->getTeacherIDOfClass($_POST['class_id']);
                    $newTeacherIdsClass = array_diff($teacherIds, $oldTeacherIdsClass);

                    if(count($newTeacherIdsClass) > 0) {
                        $teacherDao->insertTeacherList($_POST['class_id'], $newTeacherIdsClass);

                        //3. Them moi giao vien vao thanh vien nhom
                        $classDao->addUserToClass($_POST['class_id'], $newTeacherIdsClass);

                        //4. Thông báo giáo viên mới về việc được phân công lớp
                        $userDao->postNotifications($newTeacherIdsClass, NOTIFICATION_ASSIGN_CLASS, NOTIFICATION_NODE_TYPE_CLASS, $_POST['class_id']);
                        // update - memcache
                        // Cập nhật class
                        updateClassData($_POST['class_id'], CLASS_TEACHERS);

                        // Cập nhật đối tượng quản lý cho giáo viên
                        foreach ($newTeacherIdsClass as $teacherId) {
                            addTeacherData($teacherId, TEACHER_CLASSES, $_POST['class_id']);
                            addUserManageData($teacherId, USER_MANAGE_CLASSES, $_POST['class_id']);
                        }
                    }
                }
            } else {
                // Nếu thêm mới lớp
                $args = array();
                $args['title'] = $_POST['group_title'];
                $args['class_level_id'] = $_POST['class_level_id'];
                $args['user_id'] = $user->_data['user_id'];
                $args['school_id'] = $school['page_id'];
                //1. Tạo thông tin lớp trong hệ thống
                $lastId = $classDao->createClass($args);

                if (count($teacherIds) > 0) {
                    //2. Cập nhật danh sách giáo viên của lớp
                    $teacherDao->insertTeacherList($lastId, $teacherIds);
                    //3. Đưa tất cả giáo viên join vào group luôn
                    $classDao->addUserToClass($lastId, $teacherIds);

                    //4. Thông báo giáo viên mới về việc được phân công lớp
                    $userDao->postNotifications($teacherIds, NOTIFICATION_ASSIGN_CLASS, NOTIFICATION_NODE_TYPE_CLASS, $lastId);

                    //5. Các giáo viên trong 1 lớp sẽ thành bạn của nhau
                    $userDao->becomeFriendTogether($teacherIds);
                }

                //6. Thông báo cho các quản lý trường khác lớp được thêm mới
                $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'classes', $school['page_admin']);
                $userDao->postNotifications($userManagerIds, NOTIFICATION_NEW_CLASS, NOTIFICATION_NODE_TYPE_SCHOOL,
                    $lastId, convertText4Web($_POST['group_title']), $school['page_name'], convertText4Web($school['page_title']));
                //7. Cập nhật số lượng class của trường
                $schoolDao->updateClassCount($school['page_id'], 1);
                //unset($_SESSION[$_POST['school_username']]);

                //Cập nhật lại session Class Name
                //$classDao->refreshClassInSession($school['page_id']);

                /* ---------- CLASS - MEMCACHE ---------- */
                //1.Cập nhật thông tin trường
                updateSchoolData($school['page_id'], SCHOOL_CLASSES);
                updateSchoolData($school['page_id'], SCHOOL_CONFIG);

                //2.Cập nhật lại khối lớp
                updateClassLevelData($args['class_level_id']);

                //3.Cập nhật danh sách giáo viên lớp
                updateClassData($lastId, CLASS_TEACHERS);

                //4.Cập nhật thông tin quản lý cho giáo viên và thông tin lớp tương ứng
                foreach ($teacherIds as $teacherId) {
                    addTeacherData($teacherId, TEACHER_CLASSES, $lastId);
                    addUserManageData($teacherId, USER_MANAGE_CLASSES, $lastId);
                }
                /* ---------- CLASS - MEMCACHE ---------- */
            }

            $db->commit();
            return_json(array('callback' => 'window.location = "'.$system['system_url'].'/school/'.$_POST['school_username'].'/teachers";'));
            break;

        case 'add':
            /**
             * Tạo mới giáo viên của trường, bao gồm các việc:
             * 1.1. Tạo mới giáo viên
             * 2.1. Đưa giáo viên vào trường
             * 3.1. Giáo viên like page của trường & Tăng số lượng like của page.
             * 4.1. Giáo viên kết bạn với quản lý trường
             * 5.1. Cập nhật số lượng giáo viên của trường
             * 6.1. Cập nhật vai trò của giáo viên
             */
            $db->begin_transaction();
            $args = array();
            $args['first_name'] = trim($_POST['first_name']);
            $args['last_name'] = trim($_POST['last_name']);
            $args['full_name'] = $args['last_name']." ".$args['first_name'];
            //$args['username'] = isset($_POST['username'])? trim($_POST['username']): "";
            $args['username'] = generateUsername($args['full_name']);
            $args['user_phone'] = standardizePhone($_POST['phone']);
            /*if (!validatePhone($args['user_phone'])) {
                return_json( array('error' => true, 'message' => __("The phone number you entered is incorrect")) );
            }*/
            $args['email'] = trim($_POST['email']);
            $args['password'] = $_POST['password'];
            $args['gender'] = $_POST['gender'];
            //1. Tạo mới giáo viên
            $teacherId = $userDao->createUser($args);
            //1.1 Cho giáo viên like các page của hệ thống, join các group của hệ thống
            $userDao->addUsersLikeSomePages($system_page_ids, [$teacherId]);
            $userDao->addUserToSomeGroups($system_group_ids, [$teacherId]);
            //2. Đưa giáo viên vào trường
            $teacherDao->addTeacherToSchool($school['page_id'], [$teacherId]);
            //3. Giáo viên like page của trường & Tăng số lượng like của page.
            $schoolDao->addUsersLikeSchool($school['page_id'], [$teacherId]);
            //4. Giáo viên kết bạn với quản lý trường (user hiện tại).
            $userDao->becomeFriend($user->_data['user_id'], $teacherId);
            //5. Cập nhật số lượng giáo viên của trường
            $schoolDao->updateTeacherCount($school['page_id'], 1);
            //6. Cập nhật vai trò của giáo viên.
            if (isset($_POST['role_ids']) && (count($_POST['role_ids']) > 0)) {
                $roleDao->insertRole2User($school['page_id'], $_POST['role_ids'], $teacherId);
                // Kiểm tra giáo viên có quyền quản lý group không
                $userIds = $roleDao->getUserIdsOfModule($school['page_id'], 'managegroups');
                if(in_array($teacherId, $userIds)) {
                    // Thêm giáo viên vào tất cả các nhóm lớp của trường
                    $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                    $classIds = array_keys($classes);
                    if(count($classIds) > 0) {
                        $classDao->addClassToUser($teacherId, $classIds);
                    }
                }
                //Memcache - Cập nhật vai trò cho giáo viên
                updateUserManageData($teacherId, USER_MANAGE_SCHOOL);
            }
            //7. Gửi mail thông báo cho giáo viên
            $argEmail = array();
            $argEmail['action'] = "create_teacher_account";
            $argEmail['receivers'] = $args['email'];
            $argEmail['subject'] = "[".$school['page_title']."]".__("School manager creates a Inet account for you");

            //Tạo nên nội dung email
            $smarty->assign('full_name', $args['full_name']);
            $smarty->assign('username', $args['email']);
            $smarty->assign('password', $args['password']);
            $argEmail['content'] = $smarty->fetch("ci/email_templates/create_teacher_account.tpl");

            $argEmail['delete_after_sending'] = 1;
            $argEmail['school_id'] = $school['page_id'];
            $argEmail['user_id'] = $user->_data['user_id'];
            $mailDao->insertEmail($argEmail);
            // Nếu chọn lớp, thêm giáo viên vào lớp
            if(!isset($_POST['class_option'])) {
                if(isset($_POST['class_id']) && $_POST['class_id'] > 0) {
                    $newTeacherIds = array();
                    $newTeacherIds[] = $teacherId;
                    $teacherDao->insertTeacherList($_POST['class_id'], $newTeacherIds);
                    //3. Them moi giao vien vao thanh vien nhom
                    $classDao->addUserToClass($_POST['class_id'], $newTeacherIds);

                    //4. Thông báo giáo viên mới về việc được phân công lớp
                    $userDao->postNotifications($newTeacherIds, NOTIFICATION_ASSIGN_CLASS, NOTIFICATION_NODE_TYPE_CLASS, $_POST['class_id']);
                    // update - memcache
                    // Cập nhật class
                    updateClassData($_POST['class_id'], CLASS_TEACHERS);

                    // Cập nhật đối tượng quản lý cho giáo viên
                    foreach ($newTeacherIds as $teacherId) {
                        addTeacherData($teacherId, TEACHER_CLASSES, $_POST['class_id']);
                        addUserManageData($teacherId, USER_MANAGE_CLASSES, $_POST['class_id']);
                    }
                }
            } else {
                // Nếu thêm mới lớp
                $teacherIds = array();
                $teacherIds[] = $teacherId;
                $args = array();
                $args['title'] = $_POST['group_title'];
                $args['class_level_id'] = $_POST['class_level_id'];
                $args['user_id'] = $user->_data['user_id'];
                $args['school_id'] = $school['page_id'];
                //1. Tạo thông tin lớp trong hệ thống
                $lastId = $classDao->createClass($args);
                if (count($teacherIds) > 0) {
                    //2. Cập nhật danh sách giáo viên của lớp
                    $teacherDao->insertTeacherList($lastId, $teacherIds);
                    //3. Đưa tất cả giáo viên join vào group luôn
                    $classDao->addUserToClass($lastId, $teacherIds);

                    //4. Thông báo giáo viên mới về việc được phân công lớp
                    $userDao->postNotifications($teacherIds, NOTIFICATION_ASSIGN_CLASS, NOTIFICATION_NODE_TYPE_CLASS, $lastId);

                    //5. Các giáo viên trong 1 lớp sẽ thành bạn của nhau
                    $userDao->becomeFriendTogether($teacherIds);
                }

                //6. Thông báo cho các quản lý trường khác lớp được thêm mới
                $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'classes', $school['page_admin']);
                $userDao->postNotifications($userManagerIds, NOTIFICATION_NEW_CLASS, NOTIFICATION_NODE_TYPE_SCHOOL,
                    $lastId, convertText4Web($_POST['group_title']), $school['page_name'], convertText4Web($school['page_title']));
                //7. Cập nhật số lượng class của trường
                $schoolDao->updateClassCount($school['page_id'], 1);
                //unset($_SESSION[$_POST['school_username']]);
                //Cập nhật lại session Class Name
                //$classDao->refreshClassInSession($school['page_id']);

                /* ---------- CLASS - MEMCACHE ---------- */
                //1.Cập nhật thông tin trường
                updateSchoolData($school['page_id'], SCHOOL_CLASSES);
                updateSchoolData($school['page_id'], SCHOOL_CONFIG);

                //2.Cập nhật lại khối lớp
                updateClassLevelData($args['class_level_id']);

                //3.Cập nhật danh sách giáo viên lớp
                updateClassData($lastId, CLASS_TEACHERS);

                //4.Cập nhật thông tin quản lý cho giáo viên và thông tin lớp tương ứng
                foreach ($teacherIds as $teacherId) {
                    addTeacherData($teacherId, TEACHER_CLASSES, $lastId);
                    addUserManageData($teacherId, USER_MANAGE_CLASSES, $lastId);
                }
                /* ---------- CLASS - MEMCACHE ---------- */
            }


            /* ---------- UPDATE - MEMCACHE ---------- */
            //1.Đưa user mới chọn thành giáo viên của trường
            addSchoolData($school['page_id'], SCHOOL_TEACHERS, $teacherId);
            //Cập nhật số lượng giáo viên của trường
            $teacherCnt = $school['teacher_count'] + 1;
            $array_update = array(
                'teacher_count' => $teacherCnt
            );
            updateSchoolData($school['page_id'], SCHOOL_CONFIG, $array_update);
            /* ---------- END - MEMCACHE ---------- */
            $db->commit();
            return_json(array('callback' => 'window.location = "'.$system['system_url'].'/school/'.$_POST['school_username'].'/teachers";'));
            break;

        case 'unassign':
            if(!isset($_POST['id']) || !is_numeric($_POST['id'])) {
                _error(400);
            }
            $teacher = $teacherDao->getTeacher($_POST['id']);
            $db->begin_transaction();

            // Bỏ quản lý những lớp cũ
            $classes = getTeacherData($_POST['id'], TEACHER_CLASSES);
            $classIds = array();
            foreach ($classes as $class) {
                if($class['school_id'] == $school['page_id']) {
                    $classIds[] = $class['group_id'];
                }
            }

            // Bỏ quản lý những lớp cũ
            $teacherDao->deleteClassListForTeacher($_POST['id'], $classIds);

            // Loại giáo viên ra khỏi tất cả nhóm và giảm số lượng thành viên của nhóm
            $classesSchool = getSchoolData($school['page_id'], SCHOOL_CLASSES);
            $classIdsSchool = array_keys($classesSchool);
            $classDao->deleteClassFromUser($_POST['id'], $classIdsSchool);

            //Xóa giáo viên khỏi trường
            $teacherDao->unassignTeacher($school['page_id'], $_POST['id']);

            //Xóa việc like của giáo viên
            // $schoolDao->deleteUserLikeSchool($school['page_id'], [$_POST['id']]);

            //Unfriend của user bị xóa
            //$userDao->removeFriend($_POST['id']);

            //Cập nhật số lượng giáo viên của trường
            $schoolDao->updateTeacherCount($school['page_id'], -1);

            //Xoá tất cả vai trò của user trong trường.
            $roleDao->deleteUserRole($school['page_id'], $_POST['id']);

            $db->commit();

            /* ---------- UPDATE - MEMCACHE ---------- */
            //1.Xóa giáo viên khỏi trường
            deleteSchoolData($school['page_id'], SCHOOL_TEACHERS, $_POST['id']);

            //2.Cập nhật số lượng giáo viên của trường
            $teacherCnt = $school['teacher_count'] - 1;
            $array_update = array(
                'teacher_count' => $teacherCnt
            );
            updateSchoolData($school['page_id'], SCHOOL_CONFIG, $array_update);

            //3.Cập nhật vai trò cho giáo viên
            updateUserManageData($_POST['id'], USER_MANAGE_SCHOOL);

            //4. Memcache - Cập nhật vai trò cho giáo viên
            updateUserManageData($_POST['id'], USER_MANAGE_CLASSES);

            //5. Cập nhật danh sách giáo viên lớp
            foreach ($classIds as $classId) {
                updateClassData($classId, CLASS_TEACHERS);
            }

            // 3. Cập nhật danh sách lớp của giáo viên
            updateTeacherData($_POST['id'], TEACHER_CLASSES);
            /* ---------- END - MEMCACHE ---------- */

            return_json(array('callback' => 'window.location = "'.$system['system_url'].'/school/'.$_POST['school_username'].'/teachers";'));
            break;

        case 'delete':
            if(!isset($_POST['id']) || !is_numeric($_POST['id'])) {
                _error(400);
            }
            $teacher = $teacherDao->getTeacher($_POST['id']);
            if ($teacher['created_id'] == $user->_data['user_id']) {
                $db->begin_transaction();

                //Xóa giáo viên khỏi trường, khỏi bảng user và bảng followings
                $teacherDao->deleteTeacher($school['page_id'], $_POST['id']);

                //Xóa việc like của giáo viên
                $schoolDao->deleteUserLikeSchool($school['page_id'], [$_POST['id']]);

                //Unfriend của user bị xóa
                $userDao->removeFriend($_POST['id']);

                //Cập nhật số lượng giáo viên của trường
                $schoolDao->updateTeacherCount($school['page_id'], -1);

                //Xoá tất cả vai trò của user trong trường.
                $roleDao->deleteUserRole($school['page_id'], $_POST['id']);
                $db->commit();

                /* ---------- UPDATE - MEMCACHE ---------- */
                //1.Xóa giáo viên khỏi trường
                deleteSchoolData($school['page_id'], SCHOOL_TEACHERS, $_POST['id']);

                //2.Cập nhật số lượng giáo viên của trường
                $teacherCnt = $school['teacher_count'] - 1;
                $array_update = array(
                    'teacher_count' => $teacherCnt
                );
                updateSchoolData($school['page_id'], SCHOOL_CONFIG, $array_update);

                //3.Cập nhật vai trò cho giáo viên
                updateUserManageData($_POST['id'], USER_MANAGE_SCHOOL);

                //3.Cập nhật thông tin của giáo viên
                //updateTeacherData($_POST['id']);
                /* ---------- END - MEMCACHE ---------- */
            }
            return_json(array('callback' => 'window.location = "'.$system['system_url'].'/school/'.$_POST['school_username'].'/teachers";'));
            break;
        case 'add_teacher_new_class':
            $db->begin_transaction();

            /**
             * Tạo mới giáo viên của trường, bao gồm các việc:
             * 1.1. Tạo mới giáo viên
             * 2.1. Đưa giáo viên vào trường
             * 3.1. Giáo viên like page của trường & Tăng số lượng like của page.
             * 4.1. Giáo viên kết bạn với quản lý trường
             * 5.1. Cập nhật số lượng giáo viên của trường
             * 6.1. Cập nhật vai trò của giáo viên
             */
            $args = array();
            $args['full_name'] = isset($_POST['full_name'])? trim($_POST['full_name']): "";
            $args['first_name'] = split_name($args['full_name'])[0];
            $args['last_name'] = split_name($args['full_name'])[1];
            //$args['username'] = isset($_POST['username'])? trim($_POST['username']): "";
            $args['username'] = generateUsername($args['full_name']);
            $args['user_phone'] = standardizePhone($_POST['user_phone']);
            /*if (!validatePhone($args['user_phone'])) {
                return_json( array('error' => true, 'message' => __("The phone number you entered is incorrect")) );
            }*/
            $args['email'] = isset($_POST['email'])? trim($_POST['email']): "";
            $args['password'] = $_POST['password'];
            $args['gender'] = $_POST['gender'];

            //1. Tạo mới giáo viên
            $teacherId = $userDao->createUser($args);

            //1.1 Cho giáo viên like các page của hệ thống, join các group của hệ thống
            $userDao->addUsersLikeSomePages($system_page_ids, [$teacherId]);
            $userDao->addUserToSomeGroups($system_group_ids, [$teacherId]);

            //2. Đưa giáo viên vào trường
            $teacherDao->addTeacherToSchool($school['page_id'], [$teacherId]);

            //3. Giáo viên like page của trường & Tăng số lượng like của page.
            $schoolDao->addUsersLikeSchool($school['page_id'], [$teacherId]);

            //4. Giáo viên kết bạn với quản lý trường (user hiện tại).
            $userDao->becomeFriend($user->_data['user_id'], $teacherId);

            //5. Cập nhật số lượng giáo viên của trường
            $schoolDao->updateTeacherCount($school['page_id'], 1);

            //6. Cập nhật vai trò của giáo viên.
//            if (isset($_POST['role_ids']) && (count($_POST['role_ids']) > 0)) {
//                $roleDao->insertRole2User($school['page_id'], $_POST['role_ids'], $teacherId);
//                //Memcache - Cập nhật vai trò cho giáo viên
//                updateUserManageData($teacherId, USER_MANAGE_SCHOOL);
//            }

            //7. Gửi mail thông báo cho giáo viên
            $argEmail = array();
            $argEmail['action'] = "create_teacher_account";
            $argEmail['receivers'] = $args['email'];
            $argEmail['subject'] = "[".$school['page_title']."]".__("School manager creates a Inet account for you");

            //Tạo nên nội dung email
            $smarty->assign('full_name', $args['full_name']);
            $smarty->assign('username', $args['email']);
            $smarty->assign('password', $args['password']);
            $argEmail['content'] = $smarty->fetch("ci/email_templates/create_teacher_account.tpl");

            $argEmail['delete_after_sending'] = 1;
            $argEmail['school_id'] = $school['page_id'];
            $argEmail['user_id'] = $user->_data['user_id'];
            $mailDao->insertEmail($argEmail);

            /* ---------- UPDATE - MEMCACHE ---------- */
            //1.Đưa user mới chọn thành giáo viên của trường
            updateSchoolData($school['page_id'], SCHOOL_TEACHERS);

            //Cập nhật số lượng giáo viên của trường
            $teacherCnt = $school['teacher_count'] + 1;
            $array_update = array(
                'teacher_count' => $teacherCnt
            );
            updateSchoolData($school['page_id'], SCHOOL_CONFIG, $array_update);
            /* ---------- END - MEMCACHE ---------- */

            // initialize the return array
            $return = array();
            if(isset($_POST['user_ids']) && ($_POST['user_ids']) != '') {
                $user_ids = explode(",", $_POST['user_ids']);
            } else {
                $user_ids = array();
            }

            $user_ids[] = $teacherId;

            $results = $userDao->getUsers($user_ids);

            $return['no_data'] = (count($results) == 0);
            if (count($results) > 0) {
                /* assign variables */
                $smarty->assign('results', $results);
                /* return */
                $return['results'] = $smarty->fetch("ci/ajax.teacherlist.tpl");
                $return['phone'] = $results[0]['user_phone'];
                $return['email'] = $results[0]['user_email'];
            } else {
                $return['results'] = __("No teacher");
            }

            $db->commit();
            return_json($return);
//            return_json(array('callback' => 'window.location = "'.$system['system_url'].'/school/'.$_POST['school_username'].'/reports/addtemp";'));
            break;
        default:
            _error(400);
            break;
    }

} catch (Exception $e) {
    $db->rollback();
    return_json( array('error' => true, 'message' => $e->getMessage()) );
} finally {
    $db->autocommit(true);
}

?>