<?php
/**
 * Package: ajax/ci/bo
 * 
 * @package ConIu
 * @author QuanND
 */

// fetch bootstrap
require('../../../../../bootstrap.php');

// check AJAX Request
is_ajax();
check_login();

if(is_empty($_POST['school_username']) || !valid_username($_POST['school_username'])) {
    _error(404);
}

include_once(DAO_PATH . 'dao_school.php');
include_once(DAO_PATH . 'dao_event.php');
include_once(DAO_PATH . 'dao_parent.php');
include_once(DAO_PATH . 'dao_teacher.php');
include_once(DAO_PATH . 'dao_user.php');
include_once(DAO_PATH . 'dao_class.php');
include_once(DAO_PATH . 'dao_child.php');
$schoolDao = new SchoolDAO();
$eventDao = new EventDAO();
$parentDao = new ParentDAO();
$teacherDao = new TeacherDAO();
$userDao = new UserDAO();
$classDao = new ClassDAO();
$childDao = new ChildDAO();

//$school = getSchool($_POST['school_username']);
$school = getSchoolDataByUsername($_POST['school_username'], SCHOOL_INFO);
if (is_null($school)) {
    _error(403);
}
$canEdit = canEdit($_POST['school_username'], 'events');
try {
    $return = array();
    $db->autocommit(false);
    switch ($_POST['do']) {
        case 'edit':
            if (!$canEdit) {
                _error(403);
            }
            // valid inputs
            if(!isset($_POST['event_id']) || !is_numeric($_POST['event_id'])) {
                _error(400);
            }
            $db->begin_transaction();

            $args = array();
            $args['event_id'] = $_POST['event_id'];

            $args['event_name'] = $_POST['event_name'];
            // $args['location'] = $_POST['location'];
            // $args['price'] = (isset($_POST['price'])? $_POST['price']: 0);
            // $args['begin'] = $_POST['begin'];
            // $args['end'] = $_POST['end'];
            $args['level'] = $_POST['event_level'];
            $args['description'] = $_POST['description'];
            $args['post_on_wall'] = (isset($_POST['post_on_wall']) && $_POST['post_on_wall'] == 'on')? 1: 0;
            $args['post_ids'] = $_POST['post_ids'];
            $args['is_notified'] = (isset($_POST['notify_immediately']) && $_POST['notify_immediately'] == 'on')? 1: 0;
            $args['for_teacher'] = (isset($_POST['for_teacher']) && ($_POST['for_teacher'] == 'on'))? 1: 0;
            $args['school_id'] = $school['page_id'];
            if($args['for_teacher']) {
                $args['level'] = SCHOOL_LEVEL;
                $args['class_level_id'] = 0;
                $args['class_id'] = 0;
            } else {
                if($args['level'] == SCHOOL_LEVEL) {
                    $args['class_level_id'] = 0;
                    $args['class_id'] = 0;
                } elseif ($args['level'] == CLASS_LEVEL_LEVEL) {
                    $args['class_level_id'] = $_POST['class_level_id'];
                    $args['class_id'] = 0;
                } else {
                    $args['class_level_id'] = 0;
                    $args['class_id'] = $_POST['class_id'];
                }
            }
            $args['created_user_id'] = $user->_data['user_id'];
            $args['must_register'] = (isset($_POST['must_register']) && $_POST['must_register'] == 'on')? 1: 0;

            // Nếu phải đăng ký thì phải có deadline
            if($args['must_register']) {
                if(!validateDateTime($_POST['registration_deadline'])) {
                    throw new Exception(__("Please enter the registration deadline"));
                }
                $args['registration_deadline'] = $_POST['registration_deadline'];
            } else {
                $args['registration_deadline'] = '';
            }
            //Nếu không điền thông tin deadline thì hạn đăng ký là ngày bắt đâu sự kiện
//            if (($args['must_register'] == 1) && (!isset($_POST['registration_deadline']) || ($_POST['registration_deadline'] == ''))) {
//                $args['registration_deadline'] = $args['begin'];
//            } else {
//                $registationDB = toDBDate($args['registration_deadline']);
//                $dateNow = date('Y-m-d');
//                if(strtotime($registationDB) > strtotime($dateNow)) {
//                    throw new Exception(__("The registration deadline can not be the past day"));
//                }
//            }
            $args['for_parent'] = (isset($_POST['for_parent']) && ($_POST['for_parent'] == 'on') && ($args['must_register'] == 1) && ($args['level'] != TEACHER_LEVEL))? 1: 0;
            $args['for_child'] = (isset($_POST['for_child']) && ($_POST['for_child'] == 'on')  && ($args['must_register'] == 1) && ($args['level'] != TEACHER_LEVEL))? 1: 0;

            if ($args['for_teacher'] == 0 && $args['must_register'] == 1 && $args['for_parent'] == 0 && $args['for_child'] == 0) {
                throw new Exception(__("Phải chọn đối tượng tham gia trẻ, phụ huynh hay cả hai"));
            }

            //1. Cập nhật thông tin event
            $eventDao->updateEvent($args);
            $userDao->deletePosts($args['post_ids']);

            if ($args['is_notified']) {
                if($args['for_teacher']) {
                    // Thông báo cho giáo viên/nhân viên trong trường
                    $teachers = getSchoolData($school['page_id'], SCHOOL_TEACHERS);
                    $teacherIds = array_keys($teachers);
                    $teacherIds[] = $school['page_admin'];
                    $userDao->postNotifications($teacherIds, NOTIFICATION_UPDATE_EVENT_SCHOOL, NOTIFICATION_NODE_TYPE_SCHOOL, $_POST['event_id'], convertText4Web($_POST['event_name']), $school['page_name'], convertText4Web($school['page_title']));
                    // Chỉ thông báo luôn thì mới đăng lên tường
                    if ($args['post_on_wall']) {
                        $content = $args['event_name']."<br/><br/>".$args['description'];
                        $postId = $userDao->postOnPage($school, $content);
                        $eventDao->updatePostIds($postId, $eventId);
                    }
                } else {
                    switch ($args['level']) {
                        case SCHOOL_LEVEL: //Trường hợp sự kiện cấp trường thì
                            //2. Thông báo các đối tượng liên quan với trường hợp user click vào Save and Notify
                            // - Chỉ giáo viên dạy ít nhất một lớp mới nhận được
                            // - Tất cả phụ huynh đều nhận được.

                            // Lấy danh sách lớp của trường
                            $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                            if(count($classes) > 0) {
                                foreach ($classes as $class) {
                                    // Lấy danh sách giáo viên của lớp
                                    $teachers = getClassData($class['group_id'], CLASS_TEACHERS);
                                    $teacherIds = array_keys($teachers);

                                    if(count($teacherIds) > 0) {
                                        // Gửi thông báo về cho giáo viên lớp
                                        $userDao->postNotifications($teacherIds, NOTIFICATION_UPDATE_EVENT_SCHOOL, NOTIFICATION_NODE_TYPE_CLASS, $args['event_id'], convertText4Web($args['event_name']), $class['group_name']);
                                    }

                                    // Lấy danh sách trẻ trong lớp
                                    $children = getClassData($class['group_id'], CLASS_CHILDREN);
                                    foreach ($children as $child) {
                                        // Lấy danh sách phụ huynh của trẻ
                                        $parents = getChildData($child['child_id'], CHILD_PARENTS);
                                        $parentIds = array_keys($parents);

                                        if(count($parentIds) > 0) {
                                            // Gửi thông báo đến phụ huynh
                                            $userDao->postNotifications($parentIds, NOTIFICATION_UPDATE_EVENT_SCHOOL, NOTIFICATION_NODE_TYPE_CHILD, $args['event_id'], convertText4Web($args['event_name']), $child['child_id']);
                                        }
                                    }
                                }
                            }

                            // Thông báo cho các quản lý trường khác sự kiện được cập nhật
                            $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'events', $school['page_admin']);

                            if(count($userManagerIds) > 0) {
                                $userDao->postNotifications($userManagerIds, NOTIFICATION_UPDATE_EVENT_SCHOOL, NOTIFICATION_NODE_TYPE_SCHOOL,
                                    $_POST['event_id'], convertText4Web($_POST['event_name']), $school['page_name'], convertText4Web($school['page_title']));
                            }

                            //3.Post sự kiện lên tường của trường. Chỉ có sự kiện luôn thì mới đưa lên tường
                            if ($args['post_on_wall']) {
                                $content = $args['event_name'] . "<br/><br/>" . $args['description'];
                                $postId = $userDao->postOnPage($school, $content);
                                $eventDao->updatePostIds($postId, $args['event_id']);
                            }

                            break;
//                        case TEACHER_LEVEL: //Trường hợp sự kiện cấp giáo viên thì
//                            //2. Thông báo các đối tượng liên quan với trường hợp user click vào Save and Notify
//                            // - Tất cả giáo viên trong trường đều nhận được
//                            //$teacherIds = $teacherDao->getTeacherIDOfSchool($school['page_id']);
//                            $teachers = getSchoolData($school['page_id'], SCHOOL_TEACHERS);
//                            $teacherIds = array_keys($teachers);
//                            $userDao->postNotifications($teacherIds, NOTIFICATION_UPDATE_EVENT_SCHOOL, NOTIFICATION_NODE_TYPE_CLASS, $args['event_id'], convertText4Web($args['event_name']));
//                            // Thông báo cho các quản lý trường khác sự kiện được cập nhật
//                            $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'events', $school['page_admin']);
//                            $userDao->postNotifications($userManagerIds, NOTIFICATION_UPDATE_EVENT_SCHOOL, NOTIFICATION_NODE_TYPE_SCHOOL,
//                                $_POST['event_id'], convertText4Web($_POST['event_name']), $school['page_name'], convertText4Web($school['page_title']));
//
//                            break;
                        case CLASS_LEVEL_LEVEL:
                            //$classIds = $classDao->getClassIdOfLevel($_POST['class_level_id']);
                            $classes = getClassLevelData($_POST['class_level_id'], CLASS_LEVEL_CLASSES);
                            //2. Thông báo các đối tượng liên quan với trường hợp user click vào Save and Notify
                            // - Tất cả giáo viên trong khối đều nhận được
                            // - Tất cả phụ huynh của khối đều nhận được.

                            if(count($classes) > 0) {
                                foreach ($classes as $class) {
                                    $teachers = getClassData($class['group_id'], CLASS_TEACHERS);
                                    $teacherIds = array_keys($teachers);
                                    if(count($teacherIds) > 0) {
                                        // Gửi thông báo đến cho giáo viên
                                        $userDao->postNotifications($teacherIds, NOTIFICATION_UPDATE_EVENT_CLASS_LEVEL, NOTIFICATION_NODE_TYPE_CLASS, $args['event_id'], convertText4Web($args['event_name']), $class['group_name']);
                                    }

                                    // Lấy danh sách trẻ trong lớp
                                    $children = getClassData($class['group_id'], CLASS_CHILDREN);
                                    foreach ($children as $child) {
                                        // Lấy danh sách phụ huynh của trẻ
                                        $parents = getChildData($child['child_id'], CHILD_PARENTS);
                                        $parentIds = array_keys($parents);
                                        if(count($parentIds) > 0) {
                                            // THông báo đến phụ huynh của trẻ
                                            $userDao->postNotifications($parentIds, NOTIFICATION_UPDATE_EVENT_CLASS_LEVEL, NOTIFICATION_NODE_TYPE_CHILD, $args['event_id'], convertText4Web($args['event_name']), $child['child_id']);
                                        }
                                    }
                                }
                            }

                            // Thông báo cho các quản lý trường khác sự kiện được cập nhật
                            $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'events', $school['page_admin']);
                            if(count($userManagerIds) > 0) {
                                $userDao->postNotifications($userManagerIds, NOTIFICATION_UPDATE_EVENT_CLASS_LEVEL, NOTIFICATION_NODE_TYPE_SCHOOL,
                                    $_POST['event_id'], convertText4Web($_POST['event_name']), $school['page_name'], convertText4Web($school['page_title']));
                            }

                            //3.Post sự kiện lên tường của các lớp trong khối. Chỉ có thông báo luôn thì mới đưa lên tường
                            if ($args['post_on_wall']) {
                                $content = $args['event_name'] . "<br/><br/>" . $args['description'];
                                $postIds = "";
                                foreach ($classes as $class) {
                                    //$class = $classDao->getClass($classId);
                                    $postIds = $postIds . "," . $userDao->postOnGroup($class, $content, $school['page_id']);
                                }
                                $postIds = trim($postIds, ",");
                                $eventDao->updatePostIds($postIds, $args['event_id']);
                            }
                            break;
                        case CLASS_LEVEL:
                            //2. Thông báo các đối tượng liên quan với trường hợp user click vào Save and Notify
                            // - Tất cả giáo viên của lớp đều nhận được
                            // - Tất cả phụ huynh của lớp đều nhận được.
                            //$teacherIds = $teacherDao->getTeacherIDOfClass($_POST['class_id']);
                            $class = getClassData($_POST['class_id'], CLASS_INFO);
                            $teachers = getClassData($_POST['class_id'], CLASS_TEACHERS);

                            // Gửi thông báo cho giáo viên
                            $teacherIds = array_keys($teachers);
                            if(count($teacherIds) > 0) {
                                $userDao->postNotifications($teacherIds, NOTIFICATION_UPDATE_EVENT_CLASS, NOTIFICATION_NODE_TYPE_CLASS, $args['event_id'], convertText4Web($args['event_name']), $class['group_name']);
                            }

                            // Lấy danh sách trẻ trong lớp
                            $children = getClassData($_POST['class_id'], CLASS_CHILDREN);
                            if(count($children) > 0) {
                                foreach ($children as $child) {
                                    // Lấy danh sách phụ huynh của trẻ
                                    $parents = getChildData($child['child_id'], CHILD_PARENTS);
                                    $parentIds = array_keys($parents);

                                    if(count($parentIds) > 0) {
                                        $userDao->postNotifications($parentIds, NOTIFICATION_UPDATE_EVENT_CLASS, NOTIFICATION_NODE_TYPE_CHILD, $args['event_id'], convertText4Web($args['event_name']), $child['child_id']);
                                    }
                                }
                            }

                            // Thông báo cho các quản lý trường khác sự kiện được cập nhật
                            $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'events', $school['page_admin']);
                            $userDao->postNotifications($userManagerIds, NOTIFICATION_UPDATE_EVENT_CLASS, NOTIFICATION_NODE_TYPE_SCHOOL,
                                $_POST['event_id'], convertText4Web($_POST['event_name']), $school['page_name'], convertText4Web($school['page_title']));

                            //$class = $classDao->getClass($args['class_id']);
                            $class = getClassData($args['class_id'], CLASS_INFO);
                            //3.Post thông tin sự kiên lên tường của lớp. Chỉ có thông báo luôn thì mới đưa lên tường
                            if ($args['post_on_wall']) {
                                $content = $args['event_name'] . "<br/><br/>" . $args['description'];
                                $postId = $userDao->postOnGroup($class, $content, $school['page_id']);
                                $eventDao->updatePostIds($postId, $args['event_id']);
                            }

                            break;
                        default:
                            _error(400);
                            break;
                    }
                }
            }

            $db->commit();
            return_json(array('callback' => 'window.location = "'.$system['system_url'].'/school/'.$_POST['school_username'].'/events";'));
            break;

        case 'add':
            if (!$canEdit) {
                _error(403);
            }
            $db->begin_transaction();

            $args = array();
            $args['event_name'] = $_POST['event_name'];
//            $args['location'] = $_POST['location'];
//            $args['price'] = (isset($_POST['price'])? $_POST['price']: 0);
//            $args['begin'] = $_POST['begin'];
//            $args['end'] = $_POST['end'];
            $args['level'] = $_POST['event_level'];
            $args['description'] = $_POST['description'];
            $args['post_on_wall'] = (isset($_POST['post_on_wall']) && $_POST['post_on_wall'] == 'on')? 1: 0;
            $args['is_notified'] = (isset($_POST['notify_immediately']) && $_POST['notify_immediately'] == 'on')? 1: 0;
            $args['school_id'] = $school['page_id'];
            $args['for_teacher'] = (isset($_POST['for_teacher']) && $_POST['for_teacher'] == 'on')? 1: 0;
            if($args['for_teacher']) {
                $args['level'] = SCHOOL_LEVEL;
                $args['class_level_id'] = 0;
                $args['class_id'] = 0;
            } else {
                if($args['level'] == SCHOOL_LEVEL) {
                    $args['class_level_id'] = 0;
                    $args['class_id'] = 0;
                } elseif ($args['level'] == CLASS_LEVEL_LEVEL) {
                    $args['class_level_id'] = $_POST['class_level_id'];
                    $args['class_id'] = 0;
                } else {
                    $args['class_level_id'] = 0;
                    $args['class_id'] = $_POST['class_id'];
                }
            }
            $args['created_user_id'] = $user->_data['user_id'];
            $args['must_register'] = (isset($_POST['must_register']) && $_POST['must_register'] == 'on')? 1: 0;

            // Nếu phải đăng ký thì phải có deadline
            if($args['must_register']) {
                if(!validateDateTime($_POST['registration_deadline'])) {
                    throw new Exception(__("Please enter the registration deadline"));
                }
                $args['registration_deadline'] = $_POST['registration_deadline'];
            } else {
                $args['registration_deadline'] = '';
            }
            //Nếu không điền thông tin deadline thì hạn đăng ký là ngày bắt đâu sự kiện
//            if (($args['must_register'] == 1) && (!isset($_POST['registration_deadline']) || ($_POST['registration_deadline'] == ''))) {
//                $args['registration_deadline'] = $args['begin'];
//            } else {
//            }
            $args['for_parent'] = (isset($_POST['for_parent']) && ($_POST['for_parent'] == 'on') && ($args['must_register'] == 1) && (!$args['for_teacher']))? 1: 0;
            $args['for_child'] = (isset($_POST['for_child']) && ($_POST['for_child'] == 'on') && ($args['must_register'] == 1) && (!$args['for_teacher']))? 1: 0;
            if ($args['for_teacher'] == 0 && $args['must_register'] == 1 && $args['for_parent'] == 0 && $args['for_child'] == 0) {
                throw new Exception(__("Phải chọn đối tượng tham gia trẻ, phụ huynh hay cả hai"));
            }
            //1. Đưa thông báo vào hệ thống
            $eventId = $eventDao->insertEvent($args);

            // Tăng lượt tương tác thêm mới - TaiLA
            addInteractive($school['page_id'], 'event', 'school_view', $eventId, 1);
            if ($args['is_notified']) {
                if($args['for_teacher']) {
                    // Thông báo cho giáo viên/nhân viên trong trường
                    //$teacherIds = $teacherDao->getTeacherIDOfSchool($school['page_id']);
                    $teachers = getSchoolData($school['page_id'], SCHOOL_TEACHERS);
                    $teacherIds = array_keys($teachers);
                    $teacherIds[] = $school['page_admin'];
                    $userDao->postNotifications($teacherIds, NOTIFICATION_NEW_EVENT_SCHOOL, NOTIFICATION_NODE_TYPE_SCHOOL, $eventId, convertText4Web($_POST['event_name']), $school['page_name'], convertText4Web($school['page_title']));
                    // Chỉ thông báo luôn thì mới đăng lên tường
                    if ($args['post_on_wall']) {
                        $content = $args['event_name']."<br/><br/>".$args['description'];
                        $postId = $userDao->postOnPage($school, $content);
                        $eventDao->updatePostIds($postId, $eventId);
                    }
                } else {
                    switch ($args['level']) {
                        case SCHOOL_LEVEL: //Trường hợp cấp trường cấp trường thì
                            //2. Thông báo các đối tượng liên quan với trường hợp user click vào Save and Notify
                            // Nếu không phải sự kiện dành cho giáo viên thì thông báo về cho phụ huynh và giáo viên lớp
                            // - Tất cả giáo viên trong trường đều nhận được
                            // - Tất cả phụ huynh đều nhận được.

                            // Lấy danh sách lớp của trường
                            $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                            if(count($classes) > 0) {
                                foreach ($classes as $class) {
                                    // Lấy danh sách giáo viên của lớp
                                    $teachers = getClassData($class['group_id'], CLASS_TEACHERS);
                                    $teacherIds = array_keys($teachers);

                                    if(count($teacherIds) > 0) {
                                        // Gửi thông báo về cho giáo viên lớp
                                        $userDao->postNotifications($teacherIds, NOTIFICATION_NEW_EVENT_SCHOOL, NOTIFICATION_NODE_TYPE_CLASS, $eventId, convertText4Web($args['event_name']), $class['group_name']);
                                    }

                                    // Lấy danh sách trẻ trong lớp
                                    $children = getClassData($class['group_id'], CLASS_CHILDREN);
                                    foreach ($children as $child) {
                                        // Lấy danh sách phụ huynh của trẻ
                                        $parents = getChildData($child['child_id'], CHILD_PARENTS);
                                        $parentIds = array_keys($parents);

                                        if(count($parentIds) > 0) {
                                            // Gửi thông báo đến phụ huynh
                                            $userDao->postNotifications($parentIds, NOTIFICATION_NEW_EVENT_SCHOOL, NOTIFICATION_NODE_TYPE_CHILD, $eventId, convertText4Web($args['event_name']), $child['child_id']);
                                        }
                                    }
                                }
                            }

                            // Thông báo cho các quản lý trường khác sự kiện được cập nhật
                            $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'events', $school['page_admin']);
                            if(count($userManagerIds) > 0) {
                                $userDao->postNotifications($userManagerIds, NOTIFICATION_NEW_EVENT_SCHOOL, NOTIFICATION_NODE_TYPE_SCHOOL,
                                    $eventId, convertText4Web($_POST['event_name']), $school['page_name'], convertText4Web($school['page_title']));
                            }

                            //3.Post thông tin sự kiên lên tường của trường.
                            // Chỉ có thông báo luôn thì mới đưa lên tường
                            if ($args['post_on_wall']) {
                                $content = $args['event_name']."<br/><br/>".$args['description'];
                                $postId = $userDao->postOnPage($school, $content);
                                $eventDao->updatePostIds($postId, $eventId);
                            }

                            break;
                        case CLASS_LEVEL_LEVEL:
                            //$classIds = $classDao->getClassIdOfLevel($_POST['class_level_id']);
                            $classes = getClassLevelData($_POST['class_level_id'], CLASS_LEVEL_CLASSES);

                            if(count($classes) > 0) {
                                foreach ($classes as $class) {
                                    $teachers = getClassData($class['group_id'], CLASS_TEACHERS);
                                    $teacherIds = array_keys($teachers);
                                    if(count($teacherIds) > 0) {
                                        // Gửi thông báo đến cho giáo viên
                                        $userDao->postNotifications($teacherIds, NOTIFICATION_NEW_EVENT_CLASS_LEVEL, NOTIFICATION_NODE_TYPE_CLASS, $eventId, convertText4Web($args['event_name']), $class['group_name']);
                                    }

                                    // Lấy danh sách trẻ trong lớp
                                    $children = getClassData($class['group_id'], CLASS_CHILDREN);
                                    foreach ($children as $child) {
                                        // Lấy danh sách phụ huynh của trẻ
                                        $parents = getChildData($child['child_id'], CHILD_PARENTS);
                                        $parentIds = array_keys($parents);
                                        if(count($parentIds) > 0) {
                                            // THông báo đến phụ huynh của trẻ
                                            $userDao->postNotifications($parentIds, NOTIFICATION_NEW_EVENT_CLASS_LEVEL, NOTIFICATION_NODE_TYPE_CHILD, $eventId, convertText4Web($args['event_name']), $child['child_id']);
                                        }
                                    }
                                }
                            }

                            // Thông báo cho các quản lý trường khác sự kiện được cập nhật
                            $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'events', $school['page_admin']);
                            $userDao->postNotifications($userManagerIds, NOTIFICATION_NEW_EVENT_CLASS_LEVEL, NOTIFICATION_NODE_TYPE_SCHOOL,
                                $eventId, convertText4Web($_POST['event_name']), $school['page_name'], convertText4Web($school['page_title']));

                            //3.Post thông tin sự kiên lên tường của các lớp trong khối.
                            // Chỉ có thông báo luôn thì mới đưa lên tường
                            if ($args['post_on_wall']) {
                                $content = $args['event_name']."<br/><br/>".$args['description'];
                                $postIds = "";
                                foreach ($classes as $class) {
                                    //$class = $classDao->getClass($classId);
                                    $class = getClassData($class['group_id'], CLASS_INFO);
                                    $postIds = $postIds.",".$userDao->postOnGroup($class, $content, $school['page_id']);
                                }
                                $postIds = trim($postIds, ",");
                                $eventDao->updatePostIds($postIds, $eventId);
                            }
                            break;
                        case CLASS_LEVEL:
                            //2. Thông báo các đối tượng liên quan với trường hợp user click vào Save and Notify
                            // - Tất cả giáo viên của lớp đều nhận được
                            // - Tất cả phụ huynh của lớp đều nhận được.
                            //$teacherIds = $teacherDao->getTeacherIDOfClass($_POST['class_id']);
                            $class = getClassData($_POST['class_id'], CLASS_INFO);
                            $teachers = getClassData($_POST['class_id'], CLASS_TEACHERS);

                            // Gửi thông báo cho giáo viên
                            $teacherIds = array_keys($teachers);
                            if(count($teacherIds) > 0) {
                                $userDao->postNotifications($teacherIds, NOTIFICATION_NEW_EVENT_CLASS, NOTIFICATION_NODE_TYPE_CLASS, $eventId, convertText4Web($args['event_name']), $class['group_name']);
                            }

                            // Lấy danh sách trẻ trong lớp
                            $children = getClassData($_POST['class_id'], CLASS_CHILDREN);
                            if(count($children) > 0) {
                                foreach ($children as $child) {
                                    // Lấy danh sách phụ huynh của trẻ
                                    $parents = getChildData($child['child_id'], CHILD_PARENTS);
                                    $parentIds = array_keys($parents);

                                    if(count($parentIds) > 0) {
                                        $userDao->postNotifications($parentIds, NOTIFICATION_NEW_EVENT_CLASS, NOTIFICATION_NODE_TYPE_CHILD, $eventId, convertText4Web($args['event_name']), $child['child_id']);
                                    }
                                }
                            }

                            // Thông báo cho các quản lý trường khác sự kiện được thêm mới
                            $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'events', $school['page_admin']);
                            $userDao->postNotifications($userManagerIds, NOTIFICATION_NEW_EVENT_CLASS, NOTIFICATION_NODE_TYPE_SCHOOL,
                                $eventId, convertText4Web($_POST['event_name']), $school['page_name'], convertText4Web($school['page_title']));

                            //$class = $classDao->getClass($args['class_id']);
                            $class = getClassData($args['class_id'], CLASS_INFO);
                            //3.Post thông tin sự kiên lên tường của lớp. Chỉ có thông báo luôn thì mới đưa lên tường
                            if ($args['post_on_wall']) {
                                $content = $args['event_name']."<br/><br/>".$args['description'];
                                $postId = $userDao->postOnGroup($class, $content, $school['page_id']);
                                $eventDao->updatePostIds($postId, $eventId);
                            }

                            break;
                        default:
                            _error(400);
                            break;
                    }

                    /* Coniu - Tương tác trường */
                    $eventIds = array();
                    $eventIds[] = $eventId;
                    setIsAddNew($school['page_id'], 'event_created', $eventIds);
                    /* Coniu - END */
                }
            }
            addInteractive($school['page_id'], 'event', 'school_view', $eventId, 1);
            $db->commit();

            return_json(array('callback' => 'window.location = "'.$system['system_url'].'/school/'.$_POST['school_username'].'/events";'));
            break;

        case 'notify':
            if (!$canEdit) {
                _error(403);
            }
            $db->begin_transaction();
            //Lấy ra thông tin thông báo
            $event = $eventDao->getEvent($_POST['id']);

            if (!$event['is_notified']) {
                if($event['for_teacher']) {
                    // Thông báo cho giáo viên/nhân viên trong trường
                    //$teacherIds = $teacherDao->getTeacherIDOfSchool($school['page_id']);
                    $teachers = getSchoolData($school['page_id'], SCHOOL_TEACHERS);
                    $teacherIds = array_keys($teachers);
                    $teacherIds[] = $school['page_admin'];
                    $userDao->postNotifications($teacherIds, NOTIFICATION_NEW_EVENT_SCHOOL, NOTIFICATION_NODE_TYPE_SCHOOL, $_POST['id'], convertText4Web($event['event_name']), $school['page_name'], convertText4Web($school['page_title']));
                    // Chỉ thông báo luôn thì mới đăng lên tường
                    if ($event['post_on_wall']) {
                        $content = convertText4Web($event['event_name']."<br/><br/>".$event['description']);
                        $postId = $userDao->postOnPage($school, $content);
                        $eventDao->updatePostIds($postId, $event['event_id']);
                    }
                } else {
                    switch ($event['level']) {
                        case SCHOOL_LEVEL: //Trường hợp sự kiện cấp trường thì
                            //2. Thông báo các đối tượng liên quan với trường hợp user click vào Save and Notify
                            // - Tất cả giáo viên trong trường đều nhận được
                            // - Tất cả phụ huynh đều nhận được.
                            // Lấy danh sách lớp của trường
                            $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                            if(count($classes) > 0) {
                                foreach ($classes as $class) {
                                    // Lấy danh sách giáo viên của lớp
                                    $teachers = getClassData($class['group_id'], CLASS_TEACHERS);
                                    $teacherIds = array_keys($teachers);

                                    if(count($teacherIds) > 0) {
                                        // Gửi thông báo về cho giáo viên lớp
                                        $userDao->postNotifications($teacherIds, NOTIFICATION_NEW_EVENT_SCHOOL, NOTIFICATION_NODE_TYPE_CLASS, $event['event_id'], convertText4Web($args['event_name']), $class['group_name']);
                                    }

                                    // Lấy danh sách trẻ trong lớp
                                    $children = getClassData($class['group_id'], CLASS_CHILDREN);
                                    foreach ($children as $child) {
                                        // Lấy danh sách phụ huynh của trẻ
                                        $parents = getChildData($child['child_id'], CHILD_PARENTS);
                                        $parentIds = array_keys($parents);

                                        if(count($parentIds) > 0) {
                                            // Gửi thông báo đến phụ huynh
                                            $userDao->postNotifications($parentIds, NOTIFICATION_NEW_EVENT_SCHOOL, NOTIFICATION_NODE_TYPE_CHILD, $args['event_id'], convertText4Web($args['event_name']), $child['child_id']);
                                        }
                                    }
                                }
                            }

                            // Thông báo cho các quản lý trường khác sự kiện được cập nhật
                            $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'events', $school['page_admin']);
                            $userDao->postNotifications($userManagerIds, NOTIFICATION_NEW_EVENT_SCHOOL, NOTIFICATION_NODE_TYPE_SCHOOL,
                                $event['event_id'], convertText4Web($event['event_name']), $school['page_name'], convertText4Web($school['page_title']));

                            //3.Post sự kiện lên tường của trường. Chỉ có sự kiện luôn thì mới đưa lên tường
                            if ($event['post_on_wall']) {
                                $content = convertText4Web($event['event_name'] . "<br/><br/>" . $event['description']);
                                $postId = $userDao->postOnPage($school, $content);
                                $eventDao->updatePostIds($postId, $event['event_id']);
                            }

                            break;
                        case CLASS_LEVEL_LEVEL:
                            //$classIds = $classDao->getClassIdOfLevel($event['class_level_id']);
                            $classes = getClassLevelData($_POST['class_level_id'], CLASS_LEVEL_CLASSES);

                            if(count($classes) > 0) {
                                foreach ($classes as $class) {
                                    $teachers = getClassData($class['group_id'], CLASS_TEACHERS);
                                    $teacherIds = array_keys($teachers);
                                    if(count($teacherIds) > 0) {
                                        // Gửi thông báo đến cho giáo viên
                                        $userDao->postNotifications($teacherIds, NOTIFICATION_NEW_EVENT_CLASS_LEVEL, NOTIFICATION_NODE_TYPE_CLASS, $event['event_id'], convertText4Web($args['event_name']), $class['group_name']);
                                    }

                                    // Lấy danh sách trẻ trong lớp
                                    $children = getClassData($class['group_id'], CLASS_CHILDREN);
                                    foreach ($children as $child) {
                                        // Lấy danh sách phụ huynh của trẻ
                                        $parents = getChildData($child['child_id'], CHILD_PARENTS);
                                        $parentIds = array_keys($parents);
                                        if(count($parentIds) > 0) {
                                            // THông báo đến phụ huynh của trẻ
                                            $userDao->postNotifications($parentIds, NOTIFICATION_NEW_EVENT_CLASS_LEVEL, NOTIFICATION_NODE_TYPE_CHILD, $event['event_id'], convertText4Web($args['event_name']), $child['child_id']);
                                        }
                                    }
                                }
                            }

                            // Thông báo cho các quản lý trường khác sự kiện được cập nhật
                            $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'events', $school['page_admin']);
                            $userDao->postNotifications($userManagerIds, NOTIFICATION_NEW_EVENT_CLASS_LEVEL, NOTIFICATION_NODE_TYPE_SCHOOL,
                                $event['event_id'], convertText4Web($event['event_name']), $school['page_name'], convertText4Web($school['page_title']));

                            //3.Post sự kiện lên tường của các lớp trong khối. Chỉ có thông báo luôn thì mới đưa lên tường
                            if ($event['post_on_wall']) {
                                $content = $event['event_name'] . "<br/><br/>" . $event['description'];
                                $postIds = "";
                                foreach ($classes as $class) {
                                    //$class = $classDao->getClass($classId);
                                    $postIds = $postIds . "," . $userDao->postOnGroup($class, $content, $school['page_id']);
                                }
                                $postIds = trim($postIds, ",");
                                $eventDao->updatePostIds($postIds, $_POST['id']);
                            }
                            break;
                        case CLASS_LEVEL:
                            //2. Thông báo các đối tượng liên quan với trường hợp user click vào Save and Notify
                            // - Tất cả giáo viên của lớp đều nhận được
                            // - Tất cả phụ huynh của lớp đều nhận được.
                            //$teacherIds = $teacherDao->getTeacherIDOfClass($event['class_id']);
                            $class = getClassData($_POST['class_id'], CLASS_INFO);
                            $teachers = getClassData($_POST['class_id'], CLASS_TEACHERS);

                            // Gửi thông báo cho giáo viên
                            $teacherIds = array_keys($teachers);
                            if(count($teacherIds) > 0) {
                                $userDao->postNotifications($teacherIds, NOTIFICATION_NEW_EVENT_CLASS, NOTIFICATION_NODE_TYPE_CLASS, $event['event_id'], convertText4Web($args['event_name']), $class['group_name']);
                            }

                            // Lấy danh sách trẻ trong lớp
                            $children = getClassData($_POST['class_id'], CLASS_CHILDREN);
                            if(count($children) > 0) {
                                foreach ($children as $child) {
                                    // Lấy danh sách phụ huynh của trẻ
                                    $parents = getChildData($child['child_id'], CHILD_PARENTS);
                                    $parentIds = array_keys($parents);

                                    if(count($parentIds) > 0) {
                                        $userDao->postNotifications($parentIds, NOTIFICATION_NEW_EVENT_CLASS, NOTIFICATION_NODE_TYPE_CHILD, $event['event_id'], convertText4Web($args['event_name']), $child['child_id']);
                                    }
                                }
                            }

                            // Thông báo cho các quản lý trường khác sự kiện được cập nhật
                            $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'events', $school['page_admin']);
                            $userDao->postNotifications($userManagerIds, NOTIFICATION_NEW_EVENT_CLASS, NOTIFICATION_NODE_TYPE_SCHOOL,
                                $event['event_id'], convertText4Web($event['event_name']), $school['page_name'], convertText4Web($school['page_title']));

                            //$class = $classDao->getClass($event['class_id']);
                            $class = getClassData($event['class_id'], CLASS_INFO);
                            //3.Post thông tin sự kiên lên tường của lớp. Chỉ có thông báo luôn thì mới đưa lên tường
                            if ($event['post_on_wall']) {
                                $content = $event['event_name'] . "<br/><br/>" . $event['description'];
                                $postId = $userDao->postOnGroup($class, $content, $school['page_id']);
                                $eventDao->updatePostIds($postId, $_POST['id']);
                            }

                            break;
                        default:
                            _error(400);
                            break;
                    }

                    /* Coniu - Tương tác trường */
                    $eventIds = array();
                    $eventIds[] = $event['event_id'];
                    setIsAddNew($school['page_id'], 'event_created', $eventIds);
                    /* Coniu - END */
                }
            }
            // Cập nhật trạng thái đã gửi thông báo sự kiện.
            $eventDao->updateStatusToNotified($_POST['id']);

            $db->commit();
            return_json( array('success' => true, 'message' => __("Success")));
            break;

        case 'delete':
            if (!$canEdit) {
                _error(403);
            }
            if(!isset($_POST['id']) || !is_numeric($_POST['id'])) {
                _error(400);
            }
            $db->begin_transaction();

            $event = $eventDao->getEvent($_POST['id']);
            if (is_null($event)) {
                _error("Error", "The event does not exist, it should be deleted.");
            }

            //Xóa thông tin sự kiện. Trường hợp xóa không được sẽ báo lỗi và không chạy đoạn code dưới.
            $eventDao->deleteEvent($_POST['id'], true);
            //Xóa tất cả bài post liên quan đến sự kiện
            $userDao->deletePosts($event['post_ids']);

            $db->commit();
            return_json( array('success' => true, 'message' => __("Success")));
            break;

        case 'cancel':
            if (!$canEdit) {
                _error(403);
            }
            if(!isset($_POST['event_id']) || !is_numeric($_POST['event_id'])) {
                _error(400);
            }
            $db->begin_transaction();

            $event = $eventDao->getEvent($_POST['event_id']);
            if (is_null($event)) {
                _error("Error", "The event does not exist, it should be canceled.");
            }

            //Xóa thông tin sự kiện. Trường hợp xóa không được sẽ báo lỗi và không chạy đoạn code dưới.
            $eventDao->cancelEvent($_POST['event_id']);
            //Xóa tất cả bài post liên quan đến sự kiện
            $userDao->deletePosts($event['post_ids']);

            //Nếu sự kiện đã notify lúc tạo thì phải notify lúc hủy
            if ($event['is_notified']) {
                if($event['for_teacher']) {
                    // Thông báo cho giáo viên/nhân viên trong trường
                    //$teacherIds = $teacherDao->getTeacherIDOfSchool($school['page_id']);
                    $teachers = getSchoolData($school['page_id'], SCHOOL_TEACHERS);
                    $teacherIds = array_keys($teachers);
                    $teacherIds[] = $school['page_admin'];
                    $userDao->postNotifications($teacherIds, NOTIFICATION_CANCEL_EVENT_SCHOOL, NOTIFICATION_NODE_TYPE_SCHOOL, $_POST['event_id'], convertText4Web($event['event_name']), $school['page_name'], convertText4Web($school['page_title']));
                } else {
                    switch ($event['level']) {
                        case SCHOOL_LEVEL:
                            $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                            if(count($classes) > 0) {
                                foreach ($classes as $class) {
                                    // Lấy danh sách giáo viên của lớp
                                    $teachers = getClassData($class['group_id'], CLASS_TEACHERS);
                                    $teacherIds = array_keys($teachers);

                                    if(count($teacherIds) > 0) {
                                        // Gửi thông báo về cho giáo viên lớp
                                        $userDao->postNotifications($teacherIds, NOTIFICATION_CANCEL_EVENT_SCHOOL, NOTIFICATION_NODE_TYPE_CLASS, $event['event_id'], convertText4Web($event['event_name']), $class['group_name']);
                                    }

                                    // Lấy danh sách trẻ trong lớp
                                    $children = getClassData($class['group_id'], CLASS_CHILDREN);
                                    foreach ($children as $child) {
                                        // Lấy danh sách phụ huynh của trẻ
                                        $parents = getChildData($child['child_id'], CHILD_PARENTS);
                                        $parentIds = array_keys($parents);

                                        if(count($parentIds) > 0) {
                                            // Gửi thông báo đến phụ huynh
                                            $userDao->postNotifications($parentIds, NOTIFICATION_CANCEL_EVENT_SCHOOL, NOTIFICATION_NODE_TYPE_CHILD, $event['event_id'], convertText4Web($event['event_name']), $child['child_id']);
                                        }
                                    }
                                }
                            }
                            // Thông báo cho các quản lý trường khác sự kiện được cập nhật
                            $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'events', $school['page_admin']);
                            $userDao->postNotifications($userManagerIds, NOTIFICATION_CANCEL_EVENT_SCHOOL, NOTIFICATION_NODE_TYPE_SCHOOL,
                                $event['event_id'], convertText4Web($event['event_name']), $school['page_name'], convertText4Web($school['page_title']));

                            break;
                        case CLASS_LEVEL_LEVEL:
                            //$classIds = $classDao->getClassIdOfLevel($event['class_level_id']);
                            $classes = getClassLevelData($_POST['class_level_id'], CLASS_LEVEL_CLASSES);

                            if(count($classes) > 0) {
                                foreach ($classes as $class) {
                                    $teachers = getClassData($class['group_id'], CLASS_TEACHERS);
                                    $teacherIds = array_keys($teachers);
                                    if(count($teacherIds) > 0) {
                                        // Gửi thông báo đến cho giáo viên
                                        $userDao->postNotifications($teacherIds, NOTIFICATION_CANCEL_EVENT_CLASS_LEVEL, NOTIFICATION_NODE_TYPE_CLASS, $event['event_id'], convertText4Web($event['event_name']), $class['group_name']);
                                    }

                                    // Lấy danh sách trẻ trong lớp
                                    $children = getClassData($class['group_id'], CLASS_CHILDREN);
                                    foreach ($children as $child) {
                                        // Lấy danh sách phụ huynh của trẻ
                                        $parents = getChildData($child['child_id'], CHILD_PARENTS);
                                        $parentIds = array_keys($parents);
                                        if(count($parentIds) > 0) {
                                            // THông báo đến phụ huynh của trẻ
                                            $userDao->postNotifications($parentIds, NOTIFICATION_CANCEL_EVENT_CLASS_LEVEL, NOTIFICATION_NODE_TYPE_CHILD, $event['event_id'], convertText4Web($event['event_name']), $child['child_id']);
                                        }
                                    }
                                }
                            }
                            // Thông báo cho các quản lý trường khác sự kiện được cập nhật
                            $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'events', $school['page_admin']);
                            $userDao->postNotifications($userManagerIds, NOTIFICATION_CANCEL_EVENT_CLASS_LEVEL, NOTIFICATION_NODE_TYPE_SCHOOL,
                                $event['event_id'], convertText4Web($event['event_name']), $school['page_name'], convertText4Web($school['page_title']));

                            break;
                        case CLASS_LEVEL:
                            //$teacherIds = $teacherDao->getTeacherIDOfClass($event['class_id']);
                            $class = getClassData($_POST['class_id'], CLASS_INFO);
                            $teachers = getClassData($_POST['class_id'], CLASS_TEACHERS);

                            // Gửi thông báo cho giáo viên
                            $teacherIds = array_keys($teachers);
                            if(count($teacherIds) > 0) {
                                $userDao->postNotifications($teacherIds, NOTIFICATION_CANCEL_EVENT_CLASS, NOTIFICATION_NODE_TYPE_CLASS, $event['event_id'], convertText4Web($event['event_name']), $class['group_name']);
                            }

                            // Lấy danh sách trẻ trong lớp
                            $children = getClassData($_POST['class_id'], CLASS_CHILDREN);
                            if(count($children) > 0) {
                                foreach ($children as $child) {
                                    // Lấy danh sách phụ huynh của trẻ
                                    $parents = getChildData($child['child_id'], CHILD_PARENTS);
                                    $parentIds = array_keys($parents);

                                    if(count($parentIds) > 0) {
                                        $userDao->postNotifications($parentIds, NOTIFICATION_CANCEL_EVENT_CLASS, NOTIFICATION_NODE_TYPE_CHILD, $event['event_id'], convertText4Web($event['event_name']), $child['child_id']);
                                    }
                                }
                            }
                            // Thông báo cho các quản lý trường khác sự kiện được cập nhật
                            $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'events', $school['page_admin']);
                            $userDao->postNotifications($userManagerIds, NOTIFICATION_CANCEL_EVENT_CLASS, NOTIFICATION_NODE_TYPE_SCHOOL,
                                $event['event_id'], convertText4Web($event['event_name']), $school['page_name'], convertText4Web($school['page_title']));

                            break;
                        default:
                            _error(400);
                            break;
                    }
                }
            }

            $db->commit();
            return_json( array('success' => true, 'message' => __("Success")));
            break;
        case 'add_pp':
            if (!$canEdit) {
                _error(403);
            }
            //Cập nhật danh sách người tham gia sự kiện
            $childIds = isset($_POST['childIds'])? $_POST['childIds']: array();
            $oldChildIds = isset($_POST['oldChildIds'])? $_POST['oldChildIds']: array();
            $parentIds = isset($_POST['parentIds'])? $_POST['parentIds']: array();
            $oldParentIds = isset($_POST['oldParentIds'])? $_POST['oldParentIds']: array();
            $teacherIds = isset($_POST['teacherIds'])? $_POST['teacherIds']: array();
            $oldTeacherIds = isset($_POST['oldTeacherIds'])? $_POST['oldTeacherIds']: array();

            //Nếu danh sách không có thành viên nào thì bỏ qua
            //if ((count($childIds) + count($parentIds)) > 0) {
            $db->begin_transaction();
            $event = $eventDao->getEvent($_POST['event_id']);

            if ($event['level'] != TEACHER_LEVEL) {
                //$classTeacherIds = $teacherDao->getTeacherIDOfClass($_POST['class_id']); //Chỉ cần lấy teacher của trẻ đầu tiên, vì đăng ký theo từng lớp.
                //$class = $classDao->getClassInSchoolManager($school['page_id'], $_POST['class_id']);
                //Nếu sự kiện trẻ có thể tham gia
                if ($event['for_child']) {
                    //Xóa đối tượng đã bị loại bỏ
                    $deletedChildIds = array_diff($oldChildIds, $childIds);
                    if (count($deletedChildIds) > 0) {
                        $eventDao->deleteParticipants($_POST['event_id'], PARTICIPANT_TYPE_CHILD, $deletedChildIds);

                        //Thông báo tới phụ huynh có trẻ huỷ sử dụng dịch vụ.
                        $parents = $childDao->getListChildNParentId($deletedChildIds);
                        if(count($parents) > 0) {
                            foreach ($parents as $parent) {
                                $userDao->postNotifications($parent['parent_id'], NOTIFICATION_CANCEL_REGISTRATION_EVENT_CHILD, NOTIFICATION_NODE_TYPE_CHILD,
                                    $_POST['event_id'], convertText4Web($event['event_name']), $parent['child_id'], convertText4Web($parent['child_name']));
                            }
                        }
                        //Thông báo tới giáo viên của lớp. (tắt)
                        //foreach ($deletedChildIds as $id) {
//                            $teachers = $teacherDao->getTeacherIdClassBySchoolAndChild($school['page_id'], $id);
//                            $userDao->postNotifications($classTeacherIds, NOTIFICATION_CANCEL_REGISTRATION_EVENT_CHILD, NOTIFICATION_NODE_TYPE_CLASS,
//                                $_POST['event_id'], convertText4Web($event['event_name']), $class['group_name'], $_POST['child_name_'.$id]);
                        //}
                    }

                    $newChildIds = array_diff($childIds, $oldChildIds);
                    if (count($newChildIds) > 0) {
                        //Thêm những đối tượng mới danh sách đăng ký
                        $eventDao->insertParticipants($_POST['event_id'], PARTICIPANT_TYPE_CHILD, $newChildIds);

                        //Thông báo tới phụ huynh có trẻ đăng ký
                        $parents = $childDao->getListChildNParentId($newChildIds);
                        if(count($parents) > 0) {
                            foreach ($parents as $parent) {
                                $userDao->postNotifications($parent['parent_id'], NOTIFICATION_REGISTER_EVENT_CHILD, NOTIFICATION_NODE_TYPE_CHILD,
                                    $_POST['event_id'], convertText4Web($event['event_name']), $parent['child_id'], convertText4Web($parent['child_name']));
                            }
                        }
                        //Thông báo tới giáo viên của lớp. (tắt)
//                        foreach ($newChildIds as $id) {
//                            $teachers = $teacherDao->getTeacherIdClassBySchoolAndChild($school['page_id'], $id);
//                            $userDao->postNotifications($classTeacherIds, NOTIFICATION_REGISTER_EVENT_CHILD, NOTIFICATION_NODE_TYPE_CLASS,
//                                $_POST['event_id'], convertText4Web($event['event_name']), $class['group_name'], $_POST['child_name_'.$id]);
//                        }
                    }
                }

                //Nếu sự kiện mà phụ huynh có thể tham gia
                if ($event['for_parent']) {
                    //Xóa đối tượng đã bị loại bỏ
                    $deletedParentIds = array_diff($oldParentIds, $parentIds);
                    $deletedParentIds = array_unique($deletedParentIds);
                    if (count($deletedParentIds) > 0) {
                        $eventDao->deleteParticipants($_POST['event_id'], PARTICIPANT_TYPE_PARENT, $deletedParentIds);

                        foreach ($deletedParentIds as $id) {
                            //Thông báo cho phụ huynh
                            $userDao->postNotifications($id, NOTIFICATION_CANCEL_REGISTRATION_EVENT_PARENT, NOTIFICATION_NODE_TYPE_CHILD,
                                $_POST['event_id'], convertText4Web($event['event_name']), $_POST['child_id_of_parent_'.$id], __("you"));
                            //Thông báo cho giáo viên (tắt)
//                            $userDao->postNotifications($classTeacherIds, NOTIFICATION_CANCEL_REGISTRATION_EVENT_PARENT, NOTIFICATION_NODE_TYPE_CLASS,
//                                $_POST['event_id'], convertText4Web($event['event_name']), $class['group_name'], $_POST['parent_name_'.$id]);
                        }
                    }

                    //Thêm những đối tượng mới danh sách đăng ký
                    $newParentIds = array_diff($parentIds, $oldParentIds);
                    $newParentIds = array_unique($newParentIds);
                    if (count($newParentIds) > 0) {
                        $eventDao->insertParticipants($_POST['event_id'], PARTICIPANT_TYPE_PARENT, $newParentIds);

                        foreach ($newParentIds as $id) {
                            //Thông báo cho phụ huynh
                            $userDao->postNotifications($id, NOTIFICATION_REGISTER_EVENT_PARENT, NOTIFICATION_NODE_TYPE_CHILD,
                                $_POST['event_id'], convertText4Web($event['event_name']), $_POST['child_id_of_parent_'.$id], __("you"));

                            //Thông báo cho giáo viên (tắt)
//                            $userDao->postNotifications($classTeacherIds, NOTIFICATION_REGISTER_EVENT_PARENT, NOTIFICATION_NODE_TYPE_CLASS,
//                                $_POST['event_id'], convertText4Web($event['event_name']), $class['group_name'], $_POST['parent_name_'.$id]);
                        }
                    }
                }
            } else {
                //Xóa đối tượng đã bị loại bỏ
                $deletedTeacherIds = array_diff($oldTeacherIds, $teacherIds);
                $eventDao->deleteParticipants($_POST['event_id'], PARTICIPANT_TYPE_TEACHER, $deletedTeacherIds);

                //Thêm những đối tượng mới danh sách đăng ký
                $eventDao->insertParticipants($_POST['event_id'], PARTICIPANT_TYPE_TEACHER, $teacherIds);
            }

            $db->commit();
            //}
            //return_json( array('success' => true, 'message' => __("Participants have been updated")) );
            return_json(array('callback' => 'window.location = "'.$system['system_url'].'/school/'.$_POST['school_username'].'/events/participants/'.$_POST['event_id'] .'";'));
            break;

        case 'add_pp_for_employee':
            if (!$canEdit) {
                _error(403);
            }
            //Cập nhật danh sách người tham gia sự kiện
            $teacherIds = isset($_POST['teacherIds'])? $_POST['teacherIds']: array();
            $oldTeacherIds = isset($_POST['oldTeacherIds'])? $_POST['oldTeacherIds']: array();

            //Nếu danh sách không có thành viên nào thì bỏ qua
            //if ((count($childIds) + count($parentIds)) > 0) {
            $db->begin_transaction();

            //Xóa đối tượng đã bị loại bỏ
            $deletedTeacherIds = array_diff($oldTeacherIds, $teacherIds);
            $eventDao->deleteParticipants($_POST['event_id'], PARTICIPANT_TYPE_TEACHER, $deletedTeacherIds);

            //Thêm những đối tượng mới danh sách đăng ký
            $eventDao->insertParticipants($_POST['event_id'], PARTICIPANT_TYPE_TEACHER, $teacherIds);

            $db->commit();
            //}
            return_json( array('success' => true, 'message' => __("Participants have been updated")) );
            break;

        case 'search':
            if(!isset($_POST['event_id']) || !is_numeric($_POST['event_id'])) {
                _error(404);
            }
            //Lấy ra thông tin sự kiện
            $event = $eventDao->getEvent($_POST['event_id']);
            if (is_null($event)) {
                _error(404);
            }

            $childCount = 0;
            $parentCount = 0;
            $participants = $eventDao->getParticipantsOfClass($_POST['class_id'], $event['event_id'], $event['for_parent'], $childCount, $parentCount);
            $no_edit = ($event['can_register'] == 0) || (count($participants) <= 0);

            $smarty->assign('participants', $participants);
            $smarty->assign('event', $event);
            $smarty->assign('canEdit', $canEdit);
            $smarty->assign('childCount', $childCount);
            $smarty->assign('parentCount', $parentCount);
            $smarty->assign('username', $_POST['school_username']);
            $smarty->assign('no_edit', $no_edit);
            $return['results'] = $smarty->fetch("ci/school/ajax.eventparticipants.tpl");

            //Giá trị xác định xem có thể edit đc thông tin trên màn hình không
            $return['no_edit'] = $no_edit;
            return_json($return);
            //$event_id = $_POST['event_id'];
            //return_json(array('callback' => 'window.location = "'.$system['system_url'].'/school/'.$_POST['school_username'].'/events/participants/'.$_POST['event_id'].'/'.$_POST['class_id'].'";'));
            break;

        case 'reg_event':
            /**
             * Xứ lý khi user đăng ký/hủy tham gia sự kiện
             */
            $db->begin_transaction();

            $teacherId = (isset($_POST['teacherId']) && ($_POST['teacherId'] > 0)) ? $_POST['teacherId'] : 0;
            $oldTeacherId = (isset($_POST['old_teacher_id']) && ($_POST['old_teacher_id'] > 0)) ? $_POST['old_teacher_id'] : 0;

            if (($teacherId > 0) && ($oldTeacherId == 0)) {
                //Đăng ký nhân viên
                $eventDao->insertParticipants($_POST['event_id'], PARTICIPANT_TYPE_TEACHER, [$user->_data['user_id']]);

                //Thông báo cho Quản lý
                // Tắt
//                notifySchoolManagerAndTearcherOfView($_POST['child_id'], NOTIFICATION_REGISTER_EVENT_PARENT, $_POST['event_id'],
//                    $_POST['event_name'], convertText4Web($user->_data['user_fullname']), 'events');
            } else if (($teacherId == 0) && ($oldTeacherId > 0)) {
                //Huỷ đăng ký phụ huynh
                $eventDao->deleteParticipants($_POST['event_id'], PARTICIPANT_TYPE_TEACHER, [$user->_data['user_id']]);
                // Thông báo cho Quản lý
                // Tắt
//                notifySchoolManagerAndTearcherOfView($_POST['child_id'], NOTIFICATION_CANCEL_REGISTRATION_EVENT_PARENT, $_POST['event_id'],
//                    $_POST['event_name'], convertText4Web($user->_data['user_fullname']), 'events');
            }
            $db->commit();
            return_json(array('success' => true, 'message' => __("Registration info have been updated")));
            break;

        case 'reject':
            if (!$canEdit) {
                _error(403);
            }
            $db->begin_transaction();
            $eventDao->deleteParticipants($_POST['event_id'], $_POST['type'], [$_POST['pp_id']]);

            //$class = $classDao->getClassInSchoolManager($school['page_id'], $_POST['class_id']);
            $class = getClassData($_POST['class_id'], CLASS_INFO);
            //$classTeacherIds = $teacherDao->getTeacherIDOfClass($_POST['class_id']);
            $teachers = getClassData($_POST['class_id'], CLASS_TEACHERS);
            $classTeacherIds = array_keys($teachers);

            if ($_POST['type'] == PARTICIPANT_TYPE_PARENT) {
                //Thông báo cho phụ huynh
                $userDao->postNotifications($_POST['pp_id'], NOTIFICATION_CANCEL_REGISTRATION_EVENT_PARENT, NOTIFICATION_NODE_TYPE_CHILD, $_POST['event_id'], $_POST['event_name'], $_POST['child_id'], __("you"));
                //Thông báo cho giáo viên
                $userDao->postNotifications($classTeacherIds, NOTIFICATION_CANCEL_REGISTRATION_EVENT_PARENT, NOTIFICATION_NODE_TYPE_CLASS, $_POST['event_id'], $_POST['event_name'], $class['group_name'], $_POST['name']);
            } else if ($_POST['type'] == PARTICIPANT_TYPE_CHILD) {
                $parents = $childDao->getListChildNParentId([$_POST['pp_id']]);
                if(count($parents) > 0) {
                    foreach ($parents as $parent) {
                        $userDao->postNotifications($parent['parent_id'], NOTIFICATION_CANCEL_REGISTRATION_EVENT_CHILD, NOTIFICATION_NODE_TYPE_CHILD, $_POST['event_id'], $_POST['event_name'], $parent['child_id'], $_POST['name']);
                    }
                }
                //Thông báo tới giáo viên của lớp.
                $userDao->postNotifications($classTeacherIds, NOTIFICATION_CANCEL_REGISTRATION_EVENT_CHILD, NOTIFICATION_NODE_TYPE_CLASS,$_POST['event_id'], $_POST['event_name'], $class['group_name'], $_POST['name']);
            }

            $db->commit();
            return_json(array('callback' => 'window.location = "'.$system['system_url'].'/school/'.$_POST['school_username'].'/events/participants/'.$_POST['event_id'].'";'));
            //return_json(array('callback' => 'window.location.replace(response.path);'));
            break;
        default:
            _error(400);
            break;
    }

} catch (Exception $e) {
    $db->rollback();
    return_json( array('error' => true, 'message' => $e->getMessage()) );
} finally {
    $db->autocommit(true);
}

?>