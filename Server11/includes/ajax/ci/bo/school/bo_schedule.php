<?php
/**
 * Package: ajax/ci/bo
 *
 * @package ConIu
 * @author TaiLA
 */

// fetch bootstrap
require('../../../../../bootstrap.php');

// check AJAX Request
is_ajax();
check_login();

if (is_empty($_POST['school_username']) || !valid_username($_POST['school_username'])) {
    _error(404);
}

include_once(DAO_PATH . 'dao_school.php');
include_once(DAO_PATH . 'dao_schedule.php');
include_once(DAO_PATH . 'dao_child.php');
include_once(DAO_PATH . 'dao_parent.php');
include_once(DAO_PATH . 'dao_teacher.php');
include_once(DAO_PATH . 'dao_user.php');
include_once(DAO_PATH . 'dao_class_level.php');
include_once(DAO_PATH . 'dao_class.php');
include_once(DAO_PATH . 'dao_role.php');

$schoolDao = new SchoolDAO();
$scheduleDao = new ScheduleDAO();
$childDao = new ChildDAO();
$parentDao = new ParentDAO();
$teacherDao = new TeacherDAO();
$userDao = new UserDAO();
$classLevelDao = new ClassLevelDAO();
$classDao = new ClassDAO();
$roleDao = new RoleDAO();

//$school = getSchool($_POST['school_username']);
$school = getSchoolDataByUsername($_POST['school_username'], SCHOOL_INFO);
if (is_null($school)) {
    _error(403);
}

try {
    $return = array();
    $db->autocommit(false);
    switch ($_POST['do']) {
        case 'edit':
            if (!canEdit($_POST['school_username'], 'schedules')) {
                _error(403);
            }
            $db->begin_transaction();
            // 1. Edit schedule
            $args = array();
            $args['schedule_id'] = $_POST['schedule_id'];
            $args['schedule_name'] = $_POST['schedule_name'];
            $args['begin'] = $_POST['begin'];
            $args['description'] = $_POST['description'];
            $args['school_id'] = $school['page_id'];
            $args['class_level_id'] = $_POST['class_level_id'];
            $args['class_id'] = $_POST['class_id'];
            $args['is_category'] = (isset($_POST['use_category']) && $_POST['use_category'] == 'on') ? 1 : 0;
            $args['is_saturday'] = (isset($_POST['study_saturday']) && $_POST['study_saturday'] == 'on') ? 1 : 0;
            $args['is_notified'] = (isset($_POST['notify_immediately']) && $_POST['notify_immediately'] == 'on')? 1: 0;
            $level = $_POST['level'];
            if($level == 2 && $args['class_level_id'] == 0) {
                throw new Exception(__("You must select a class level"));
            } else if($level == 3 && $args['class_id'] == 0) {
                throw new Exception(__("You must select a class"));
            }
            $args['applied_for'] = 0;
            if($args['class_id'] > 0) {
                $args['applied_for'] = CLASS_LEVEL;
            }
            if(($args['class_id'] == 0) && $args['class_level_id'] > 0) {
                $args['applied_for'] = CLASS_LEVEL_LEVEL;
            }
            if($args['class_id'] == 0 && $args['class_level_id'] == 0) {
                $args['applied_for'] = SCHOOL_LEVEL;
            }
            $data = $scheduleDao->getScheduleDetailById($_POST['schedule_id']);
            $scheduleDao->updateSchedule($args, $data);

            // Xóa schedule_detail cũ
            $scheduleDao->deleteScheduleDetail($args['schedule_id']);

            // Insert schedule detail mới
            $starts = array();
            $starts = $_POST['start'];

            $categorys = array();
            if($args['is_category']) {
                $categorys = $_POST['category'];
            }
            if($args['is_category'] && (count($starts) != count($categorys))) {
                throw new Exception(__('Please enter the full information'));
            }
            $subject_detail_mon = array();
            $subject_detail_mon = $_POST['subject_detail_mon'];

            $subject_detail_tue = array();
            $subject_detail_tue = $_POST['subject_detail_tue'];

            $subject_detail_wed = array();
            $subject_detail_wed = $_POST['subject_detail_wed'];

            $subject_detail_thu = array();
            $subject_detail_thu = $_POST['subject_detail_thu'];

            $subject_detail_fri = array();
            $subject_detail_fri = $_POST['subject_detail_fri'];

            $subject_detail_sat = array();
            if($args['is_saturday']) {
                $subject_detail_sat = $_POST['subject_detail_sat'];
            }

            $scheduleDao->insertScheduleDetail($args['schedule_id'], $starts, $categorys, $subject_detail_mon, $subject_detail_tue, $subject_detail_wed, $subject_detail_thu, $subject_detail_fri, $subject_detail_sat);
            if($args['is_notified']) {
                if($args['applied_for'] == CLASS_LEVEL) {
                    $class = getClassData($args['class_id'], CLASS_INFO);
                    // Danh sách trẻ trong lớp
                    //$childrenIds = $childDao->getChildIdOfClass($args['class_id']);
                    $childrens = getClassData($args['class_id'], CLASS_CHILDREN);
                    if (!is_null($class) && !is_null($childrens)) {
                        $childrenIds = array_keys($childrens);
                        //Thông báo tới phụ huynh có trẻ trong lớp được tạo lịch học.
                        if (count($childrenIds) > 0) {
                            $parents = $childDao->getListChildNParentId($childrenIds);
                            if(count($parents) > 0) {
                                foreach ($parents as $parent) {
                                    $userDao->postNotifications($parent['parent_id'], NOTIFICATION_UPDATE_SCHEDULE, NOTIFICATION_NODE_TYPE_CHILD,
                                        $_POST['schedule_id'], convertText4Web($_POST['schedule_name']), $parent['child_id'], convertText4Web($class['group_title']));
                                }
                            }
                            //Thông báo tới giáo viên của lớp.
                            //$teachers = $teacherDao->getTeacherOfClass($class['group_id']);
                            $teachers = getClassData($class['group_id'], CLASS_TEACHERS);
                            if(count($teachers) > 0) {
                                $teacherIds = array_keys($teachers);
                                $userDao->postNotifications($teacherIds, NOTIFICATION_UPDATE_SCHEDULE, NOTIFICATION_NODE_TYPE_CLASS,
                                    $_POST['schedule_id'], convertText4Web($_POST['schedule_name']), $class['group_name'], convertText4Web($class['group_title']));
                            }
                            // Thông báo đến các quản lý trường khác
                            $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'schedules', $school['page_admin']);
                            $userDao->postNotifications($userManagerIds, NOTIFICATION_UPDATE_SCHEDULE, NOTIFICATION_NODE_TYPE_SCHOOL,
                                $_POST['schedule_id'], convertText4Web($_POST['schedule_name']), $school['page_name'], convertText4Web($class['group_title']));
                        }
                    }
                } elseif ($args['applied_for'] == CLASS_LEVEL_LEVEL) {
                    // Danh sách trẻ trong khối
                    $childrenIds = $childDao->getChildIdByClassLevel($args['class_level_id']);
                    //$class_level = $classLevelDao->getClassLevel($args['class_level_id']);
                    $class_level = getClassLevelData($args['class_level_id'], CLASS_LEVEL_INFO);

                    //Thông báo tới phụ huynh có trẻ trong lớp được tạo lịch học.
                    if (count($childrenIds) > 0) {
                        $parents = $childDao->getListChildNParentId($childrenIds);
                        if(count($parents) > 0) {
                            foreach ($parents as $parent) {
                                $userDao->postNotifications($parent['parent_id'], NOTIFICATION_UPDATE_SCHEDULE, NOTIFICATION_NODE_TYPE_CHILD,
                                    $_POST['schedule_id'], convertText4Web($_POST['schedule_name']), $parent['child_id'], convertText4Web($class_level['class_level_name']));
                            }
                        }
                        //Thông báo tới giáo viên của lớp.
                        //$classes = $classDao->getClassesOfLevel($args['class_level_id']);
                        $classes = getClassLevelData($args['class_level_id'], CLASS_LEVEL_CLASSES);
                        foreach ($classes as $class) {
                            //$teachers = $teacherDao->getTeacherOfClass($class['group_id']);
                            $teachers = getClassData($class['group_id'], CLASS_TEACHERS);
                            if (!is_null($teachers)) {
                                $teacherIds = array_keys($teachers);
                                $userDao->postNotifications($teacherIds, NOTIFICATION_UPDATE_SCHEDULE, NOTIFICATION_NODE_TYPE_CLASS,
                                    $_POST['schedule_id'], convertText4Web($_POST['schedule_name']), $class['group_name'], convertText4Web($class_level['class_level_name']));
                            }
                        }
                        // Thông báo đến các quản lý trường khác
                        $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'schedules', $school['page_admin']);
                        $userDao->postNotifications($userManagerIds, NOTIFICATION_UPDATE_SCHEDULE, NOTIFICATION_NODE_TYPE_SCHOOL,
                            $_POST['schedule_id'], convertText4Web($_POST['schedule_name']), $school['page_name'], convertText4Web($class_level['class_level_name']));
                    }
                } else {
                    // Danh sách trẻ trong trường
                    //$childrenIds = $childDao->getChildIdBySchool($args['school_id']);
                    $children = getSchoolData($args['school_id'], SCHOOL_CHILDREN);
                    $childrenIds = array_keys($children);

                    //Thông báo tới phụ huynh có trẻ trong lớp được tạo lịch học.
                    if (count($childrenIds) > 0) {
                        $parents = $childDao->getListChildNParentId($childrenIds);
                        if(count($parents) > 0) {
                            foreach ($parents as $parent) {
                                $userDao->postNotifications($parent['parent_id'], NOTIFICATION_UPDATE_SCHEDULE, NOTIFICATION_NODE_TYPE_CHILD,
                                    $_POST['schedule_id'], convertText4Web($_POST['schedule_name']), $parent['child_id'], convertText4Web($school['page_title']));
                            }
                        }
                        //Thông báo tới giáo viên của lớp.
                        // Lấy danh sách lớp
                        //$classes = $classDao->getClassesOfSchool($args['school_id']);
                        $classes = getSchoolData($args['school_id'], SCHOOL_CLASSES);
                        foreach ($classes as $class) {
                            //$teachers = $teacherDao->getTeacherOfClass($class['group_id']);
                            $teachers = getClassData($class['group_id'], CLASS_TEACHERS);
                            if(count($teachers) > 0) {
                                $teacherIds = array_keys($teachers);
                                $userDao->postNotifications($teacherIds, NOTIFICATION_UPDATE_SCHEDULE, NOTIFICATION_NODE_TYPE_CLASS,
                                    $_POST['schedule_id'], convertText4Web($_POST['schedule_name']), $class['group_name'], convertText4Web($school['page_title']));
                            }
                        }
                        // Thông báo đến các quản lý trường khác
                        $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'schedules', $school['page_admin']);
                        $userDao->postNotifications($userManagerIds, NOTIFICATION_UPDATE_SCHEDULE, NOTIFICATION_NODE_TYPE_SCHOOL,
                            $_POST['schedule_id'], convertText4Web($_POST['schedule_name']), $school['page_name'], convertText4Web($school['page_title']));
                    }
                }
            }

            $db->commit();
            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/school/' . $_POST['school_username'] . '/schedules";'));
            //return_json(array('success' => true, 'message' => __("Done, Schedule have been updated")));
            break;

        case 'add':
            if (!canEdit($_POST['school_username'], 'schedules')) {
                _error(403);
            }
            $db->begin_transaction();
            $args = array();
            $args['schedule_name'] = $_POST['schedule_name'];
            $args['begin'] = $_POST['begin'];
            $args['description'] = $_POST['description'];
            $args['school_id'] = $school['page_id'];
            $args['class_level_id'] = $_POST['class_level_id'];
            $args['class_id'] = $_POST['class_id'];
            $args['is_category'] = (isset($_POST['use_cate']) && $_POST['use_cate'] == 'on') ? 1 : 0;
            $args['is_saturday'] = (isset($_POST['study_saturday']) && $_POST['study_saturday'] == 'on') ? 1 : 0;
            $args['is_notified'] = (isset($_POST['notify_immediately']) && $_POST['notify_immediately'] == 'on')? 1: 0;
            $args['applied_for'] = 0;
            $level = $_POST['level'];
            if($level == 2 && $args['class_level_id'] == 0) {
                throw new Exception(__("You must select a class level"));
            } else if($level == 3 && $args['class_id'] == 0) {
                throw new Exception(__("You must select a class"));
            }
            if($args['class_id'] > 0) {
                $args['applied_for'] = CLASS_LEVEL;
            }
            if(($args['class_id'] == 0) && $args['class_level_id'] > 0) {
                $args['applied_for'] = CLASS_LEVEL_LEVEL;
            }
            if($args['class_id'] == 0 && $args['class_level_id'] == 0) {
                $args['applied_for'] = SCHOOL_LEVEL;
            }


            // 1. Insert lịch học vào bảng ci_schedule
            $scheduleId = $scheduleDao->insertSchedule($args);

            // Tăng lượt tương tác thêm mới - TaiLA
            addInteractive($school['page_id'], 'schedule', 'school_view', $scheduleId, 1);
            // 2. Insert vào bảng ci_schedule_detail
            $starts = array();
            $starts = array_map("trim", $_POST['start']);
            $starts = array_diff($starts, array(""));

            if(count($starts) == 0) {
                throw new Exception(__("Please enter schedule's information"));
            }

            $categorys = array();
            if($args['is_category']) {
                $categorys = array_map("trim", $_POST['category']);
                $categorys = array_diff($categorys, array(""));
            }
            if($args['is_category'] && (count($starts) != count($categorys))) {
                throw new Exception(__('Please enter the full information'));
            }
            $subject_detail_mon = array();
            $subject_detail_mon = $_POST['subject_detail_mon'];

            $subject_detail_tue = array();
            $subject_detail_tue = $_POST['subject_detail_tue'];

            $subject_detail_wed = array();
            $subject_detail_wed = $_POST['subject_detail_wed'];

            $subject_detail_thu = array();
            $subject_detail_thu = $_POST['subject_detail_thu'];

            $subject_detail_fri = array();
            $subject_detail_fri = $_POST['subject_detail_fri'];

            $subject_detail_sat = array();
            if($_POST['subject_detail_sat']) {
                $subject_detail_sat = $_POST['subject_detail_sat'];
            }

            $scheduleDao->insertScheduleDetail($scheduleId, $starts, $categorys, $subject_detail_mon, $subject_detail_tue, $subject_detail_wed, $subject_detail_thu, $subject_detail_fri, $subject_detail_sat);
            if($args['is_notified']) {
                if($args['applied_for'] == CLASS_LEVEL) {

                    //$class = $classDao->getClass($args['class_id']);
                    $class = getClassData($args['class_id'], CLASS_INFO);

                    // Danh sách trẻ trong lớp
                    //$childrenIds = $childDao->getChildIdOfClass($args['class_id']);
                    $children = getClassData($args['class_id'], CLASS_CHILDREN);
                    if (!is_null($class) && !is_null($children)) {
                        $childrenIds = array_keys($children);

                        //Thông báo tới phụ huynh có trẻ trong lớp được tạo lịch học.
                        if (count($childrenIds) > 0) {
                            $parents = $childDao->getListChildNParentId($childrenIds);
                            if(count($parents) > 0) {
                                foreach ($parents as $parent) {
                                    $userDao->postNotifications($parent['parent_id'], NOTIFICATION_NEW_SCHEDULE, NOTIFICATION_NODE_TYPE_CHILD,
                                        $scheduleId, convertText4Web($_POST['schedule_name']), $parent['child_id'], convertText4Web($class['group_title']));
                                }
                            }

                            //Thông báo tới giáo viên của lớp.
                            //$teachers = $teacherDao->getTeacherOfClass($class['group_id']);
                            $teachers = getClassData($class['group_id'], CLASS_TEACHERS);
                            if (!is_null($teachers)) {
                                $teacherIds = array_keys($teachers);
                                $userDao->postNotifications($teacherIds, NOTIFICATION_NEW_SCHEDULE, NOTIFICATION_NODE_TYPE_CLASS,
                                    $scheduleId, convertText4Web($_POST['schedule_name']), $class['group_name'], convertText4Web($class['group_title']));
                            }

                            // Thông báo đến các quản lý trường khác
                            $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'schedules', $school['page_admin']);
                            $userDao->postNotifications($userManagerIds, NOTIFICATION_NEW_SCHEDULE, NOTIFICATION_NODE_TYPE_SCHOOL,
                                $scheduleId, convertText4Web($_POST['schedule_name']), $school['page_name'], convertText4Web($class['group_title']));
                        }
                    }

                } elseif ($args['applied_for'] == CLASS_LEVEL_LEVEL) {
                    // Danh sách trẻ trong khối
                    $childrenIds = $childDao->getChildIdByClassLevel($args['class_level_id']);
                    $class_level = getClassLevelData($args['class_level_id'], CLASS_LEVEL_INFO);

                    //Thông báo tới phụ huynh có trẻ trong lớp được tạo lịch học.
                    if (count($childrenIds) > 0) {
                        $parents = $childDao->getListChildNParentId($childrenIds);
                        if(count($parents) > 0) {
                            foreach ($parents as $parent) {
                                $userDao->postNotifications($parent['parent_id'], NOTIFICATION_NEW_SCHEDULE, NOTIFICATION_NODE_TYPE_CHILD,
                                    $scheduleId, convertText4Web($_POST['schedule_name']), $parent['child_id'], convertText4Web($class_level['class_level_name']));
                            }
                        }
                        //Thông báo tới giáo viên của lớp.
                        //$classes = $classDao->getClassesOfLevel($args['class_level_id']);
                        $classes = getClassLevelData($args['class_level_id'], CLASS_LEVEL_CLASSES);
                        if (!is_null($classes)) {
                            foreach ($classes as $class) {
                                //$teachers = $teacherDao->getTeacherOfClass($class['group_id']);
                                $teachers = getClassData($class['group_id'], CLASS_TEACHERS);
                                if (!is_null($teachers)) {
                                    $teacherIds = array_keys($teachers);
                                    $userDao->postNotifications($teacherIds, NOTIFICATION_NEW_SCHEDULE, NOTIFICATION_NODE_TYPE_CLASS,
                                        $scheduleId, convertText4Web($_POST['schedule_name']), $class['group_name'], convertText4Web($class_level['class_level_name']));
                                }
                            }
                        }

                        // Thông báo đến các quản lý trường khác
                        $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'schedules', $school['page_admin']);
                        $userDao->postNotifications($userManagerIds, NOTIFICATION_NEW_SCHEDULE, NOTIFICATION_NODE_TYPE_SCHOOL,
                            $scheduleId, convertText4Web($_POST['schedule_name']), $school['page_name'], convertText4Web($class_level['class_level_name']));
                    }
                } else {
                    // Danh sách trẻ trong trường
                    //$childrenIds = $childDao->getChildIdBySchool($args['school_id']);
                    $children = getSchoolData($args['school_id'], SCHOOL_CHILDREN);
                    $childrenIds = array_keys($children);

                    //Thông báo tới phụ huynh có trẻ trong lớp được tạo lịch học.
                    if (count($childrenIds) > 0) {
                        $parents = $childDao->getListChildNParentId($childrenIds);
                        if(count($parents) > 0) {
                            foreach ($parents as $parent) {
                                $userDao->postNotifications($parent['parent_id'], NOTIFICATION_NEW_SCHEDULE, NOTIFICATION_NODE_TYPE_CHILD,
                                    $scheduleId, convertText4Web($_POST['schedule_name']), $parent['child_id'], convertText4Web($school['page_title']));
                            }
                        }
                        //Thông báo tới giáo viên của lớp.
                        // Lấy danh sách lớp
                        //$classes = $classDao->getClassesOfSchool($args['school_id']);
                        $classes = getSchoolData($args['school_id'], SCHOOL_CLASSES);
                        foreach ($classes as $class) {
                            //$teachers = $teacherDao->getTeacherOfClass($class['group_id']);
                            $teachers = getClassData($class['group_id'], CLASS_TEACHERS);
                            if(count($teachers) > 0) {
                                $teacherIds = array_keys($teachers);
                                $userDao->postNotifications($teacherIds, NOTIFICATION_NEW_SCHEDULE, NOTIFICATION_NODE_TYPE_CLASS,
                                    $scheduleId, convertText4Web($_POST['schedule_name']), $class['group_name'], convertText4Web($school['page_title']));
                            }
                        }
                        // Thông báo đến các quản lý trường khác
                        $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'schedules', $school['page_admin']);
                        $userDao->postNotifications($userManagerIds, NOTIFICATION_NEW_SCHEDULE, NOTIFICATION_NODE_TYPE_SCHOOL,
                            $scheduleId, convertText4Web($_POST['schedule_name']), $school['page_name'], convertText4Web($school['page_title']));
                    }
                }

                /* Inet - Tương tác trường */
                $scheduleIds = array();
                $scheduleIds[] = $scheduleId;
                setIsAddNew($school['page_id'], 'schedule_created', $scheduleIds);
                /* Inet - END */
            }
            $db->commit();
            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/school/' . $_POST['school_username'] . '/schedules";'));
//            return_json(array('success' => true, 'message' => __("Done, Schedule have been updated")));
            break;

        case 'copy':
            if (!canEdit($_POST['school_username'], 'schedules')) {
                _error(403);
            }
            $db->begin_transaction();
            // 1. copy schedule
            $args = array();
            $args['schedule_name'] = $_POST['schedule_name'];
            $args['begin'] = $_POST['begin'];
            $args['description'] = $_POST['description'];
            $args['school_id'] = $school['page_id'];
            $args['class_level_id'] = $_POST['class_level_id'];
            $args['class_id'] = $_POST['class_id'];
            $args['is_category'] = (isset($_POST['use_category']) && $_POST['use_category'] == 'on') ? 1 : 0;
            $args['is_saturday'] = (isset($_POST['study_saturday']) && $_POST['study_saturday'] == 'on') ? 1 : 0;
            $args['is_notified'] = (isset($_POST['notify_immediately']) && $_POST['notify_immediately'] == 'on')? 1: 0;
            $args['applied_for'] = 0;
            $level = $_POST['level'];
            if($level == 2 && $args['class_level_id'] == 0) {
                throw new Exception(__("You must select a class level"));
            } else if($level == 3 && $args['class_id'] == 0) {
                throw new Exception(__("You must select a class"));
            }
            if($args['class_id'] > 0) {
                $args['applied_for'] = CLASS_LEVEL;
            }
            if(($args['class_id'] == 0) && $args['class_level_id'] > 0) {
                $args['applied_for'] = CLASS_LEVEL_LEVEL;
            }
            if($args['class_id'] == 0 && $args['class_level_id'] == 0) {
                $args['applied_for'] = SCHOOL_LEVEL;
            }
            // 1. Insert lịch học vào bảng ci_schedule
            $scheduleId = $scheduleDao->insertSchedule($args);

            // Tăng lượt tương tác thêm mới - TaiLA
            addInteractive($school['page_id'], 'schedule', 'school_view', $scheduleId, 1);
            // 2. Insert vào bảng ci_schedule_detail
            $starts = array();
            $starts = $_POST['start'];
            if(count($starts) == 0) {
                throw new Exception(__('Please enter the full information'));
            }

            $categorys = array();
            if($args['is_category']) {
                $categorys = $_POST['category'];
            }
            $subject_detail_mon = array();
            $subject_detail_mon = $_POST['subject_detail_mon'];

            $subject_detail_tue = array();
            $subject_detail_tue = $_POST['subject_detail_tue'];

            $subject_detail_wed = array();
            $subject_detail_wed = $_POST['subject_detail_wed'];

            $subject_detail_thu = array();
            $subject_detail_thu = $_POST['subject_detail_thu'];

            $subject_detail_fri = array();
            $subject_detail_fri = $_POST['subject_detail_fri'];

            $subject_detail_sat = array();
            if($_POST['subject_detail_sat']) {
                $subject_detail_sat = $_POST['subject_detail_sat'];
            }

            $scheduleDao->insertScheduleDetail($scheduleId, $starts, $categorys, $subject_detail_mon, $subject_detail_tue, $subject_detail_wed, $subject_detail_thu, $subject_detail_fri, $subject_detail_sat);
            if($args['is_notified']) {
                // Thông báo tới phụ huynh và giáo viên
                if($args['applied_for'] == CLASS_LEVEL) {
                    // Danh sách trẻ trong lớp
                    //$childrenIds = $childDao->getChildIdOfClass($args['class_id']);
                    $children = getClassData($args['class_id'], CLASS_CHILDREN);
                    $childrenIds = array_keys($children);
                    //$class = $classDao->getClass($args['class_id']);
                    $class = getClassData($args['class_id'], CLASS_INFO);

                    //Thông báo tới phụ huynh có trẻ trong lớp được tạo lịch học.
                    if (count($childrenIds) > 0) {
                        $parents = $childDao->getListChildNParentId($childrenIds);
                        if(count($parents) > 0) {
                            foreach ($parents as $parent) {
                                $userDao->postNotifications($parent['parent_id'], NOTIFICATION_NEW_SCHEDULE, NOTIFICATION_NODE_TYPE_CHILD,
                                    $scheduleId, convertText4Web($_POST['schedule_name']), $parent['child_id'], convertText4Web($class['group_title']));
                            }
                        }
                        //Thông báo tới giáo viên của lớp.
                        //$teachers = $teacherDao->getTeacherOfClass($class['group_id']);
                        $teachers = getClassData($class['group_id'], CLASS_TEACHERS);
                        if(count($teachers) > 0) {
                            $teacherIds = array_keys($teachers);
                            $userDao->postNotifications($teacherIds, NOTIFICATION_NEW_SCHEDULE, NOTIFICATION_NODE_TYPE_CLASS,
                                $scheduleId, convertText4Web($_POST['schedule_name']), $class['group_name'], convertText4Web($class['group_title']));
                        }
                    }
                } elseif ($args['applied_for'] == CLASS_LEVEL_LEVEL) {
                    // Danh sách trẻ trong khối
                    $childrenIds = $childDao->getChildIdByClassLevel($args['class_level_id']);
                    //$class_level = $classLevelDao->getClassLevel($args['class_level_id']);
                    $class_level = getClassLevelData($args['class_level_id'], CLASS_LEVEL_INFO);

                    //Thông báo tới phụ huynh có trẻ trong lớp được tạo lịch học.
                    if (count($childrenIds) > 0) {
                        $parents = $childDao->getListChildNParentId($childrenIds);
                        foreach ($parents as $parent) {
                            $userDao->postNotifications($parent['parent_id'], NOTIFICATION_NEW_SCHEDULE, NOTIFICATION_NODE_TYPE_CHILD,
                                $scheduleId, convertText4Web($_POST['schedule_name']), $parent['child_id'], convertText4Web($class_level['class_level_name']));
                        }
                        //Thông báo tới giáo viên của lớp.
                        $classes = $classDao->getClassesOfLevel($args['class_level_id']);
                        $classes = getClassLevelData($args['class_level_id'], CLASS_LEVEL_CLASSES);
                        foreach ($classes as $class) {
                            //$teachers = $teacherDao->getTeacherOfClass($class['group_id']);
                            $teachers = getClassData($class['group_id'], CLASS_TEACHERS);
                            if(count($teachers) > 0) {
                                $teacherIds = array_keys($teachers);
                                $userDao->postNotifications($teacherIds, NOTIFICATION_NEW_SCHEDULE, NOTIFICATION_NODE_TYPE_CLASS,
                                    $scheduleId, convertText4Web($_POST['schedule_name']), $class['group_name'], convertText4Web($class_level['class_level_name']));
                            }
                        }
                    }
                } else {
                    // Danh sách trẻ trong trường
                    //$childrenIds = $childDao->getChildIdBySchool($args['school_id']);
                    $children = getSchoolData($args['school_id'], SCHOOL_CHILDREN);
                    $childrenIds = array_keys($children);

                    //Thông báo tới phụ huynh có trẻ trong lớp được tạo lịch học.
                    if (count($childrenIds) > 0) {
                        $parents = $childDao->getListChildNParentId($childrenIds);
                        foreach ($parents as $parent) {
                            $userDao->postNotifications($parent['parent_id'], NOTIFICATION_NEW_SCHEDULE, NOTIFICATION_NODE_TYPE_CHILD,
                                $scheduleId, convertText4Web($_POST['schedule_name']), $parent['child_id'], convertText4Web($school['page_title']));
                        }
                        //Thông báo tới giáo viên của lớp.
                        // Lấy danh sách lớp
                        //$classes = $classDao->getClassesOfSchool($args['school_id']);
                        $classes = getSchoolData($args['school_id'], SCHOOL_CLASSES);
                        foreach ($classes as $class) {
                            //$teachers = $teacherDao->getTeacherOfClass($class['group_id']);
                            $teachers = getClassData($class['group_id'], CLASS_TEACHERS);
                            if(count($teachers) > 0) {
                                $teacherIds = array_keys($teachers);
                                $userDao->postNotifications($teacherIds, NOTIFICATION_NEW_SCHEDULE, NOTIFICATION_NODE_TYPE_CLASS,
                                    $scheduleId, convertText4Web($_POST['schedule_name']), $class['group_name'], convertText4Web($school['page_title']));
                            }
                        }
                    }
                }

                /* Inet - Tương tác trường */
                $scheduleIds = array();
                $scheduleIds[] = $scheduleId;
                setIsAddNew($school['page_id'], 'schedule_created', $scheduleIds);
                /* Inet - END */
            }

            $db->commit();
            // return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/school/' . $_POST['school_username'] . '/schedules";'));
            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/school/' . $_POST['school_username'] . '/schedules";'));
            break;

        case 'delete_schedule':
            if (!canEdit($_POST['school_username'], 'schedules')) {
                _error(403);
            }
            $db->begin_transaction();
            if(!$_POST['id'] || !is_numeric($_POST['id'])) {
                _error(400);
            }
            $scheduleDao->deleteSchedule($_POST['id']);

            $db->commit();
            return_json( array('success' => true, 'message' => __("Success")));
            break;

        case 'search':
            $school_id = $school['page_id'];
            $class_level_id = $_POST['class_level_id'];
            $class_id = $_POST['class_id'];
            $level = $_POST['level'];

            if($level == 0) {
                $results['schedule'] = $scheduleDao->getScheduleOfSchool($school_id);
            } else if($level == 1) {
                // Lấy danh sách lịch học có phạm vi áp dụng là trường
                $results['schedule'] = $scheduleDao->getScheduleById($school_id, $class_level_id, $class_id);
            } else if($level == 2) {
                // Lấy danh sách lịch học áp dụng cho khối
                if($class_level_id == 0) {
                    // Lấy tất cả danh sách lịc học phạm vi áp dụng là khối (khối nào cũng được)
                    $results['schedule'] = $scheduleDao->getAllScheduleByLevel($school_id, $level);
                } else {
                    $results['schedule'] = $scheduleDao->getScheduleById($school_id, $class_level_id, $class_id);
                }
            } else {
                if($class_id == 0) {
                    // Lấy tất cả danh sách lịc học phạm vi áp dụng là LỚP (lớp nào cũng được)
                    $results['schedule'] = $scheduleDao->getAllScheduleByLevel($school_id, $level);
                } else {
                    $results['schedule'] = $scheduleDao->getScheduleById($school_id, $class_level_id, $class_id);
                }
            }

            $results['username'] = $school['page_name'];
            $smarty->assign('results', $results);

            //$class_levels = $classLevelDao->getClassLevels($class['school_id']);
            $class_levels = getSchoolData($school_id, SCHOOL_CLASS_LEVELS);
            $classes = getSchoolData($school_id, SCHOOL_CLASSES);

            $smarty->assign('class_levels', $class_levels);
            $smarty->assign('classes', $classes);
            $smarty->assign('canEdit', canEdit($_POST['school_username'], 'schedules'));

            $return['results'] = $smarty->fetch("ci/ajax.schedulelist.tpl");
            $return['no_data'] = (count($results) == 0);
            return_json($return);
            break;

        case 'notify':
            $db->begin_transaction();
            // Lấy thông tin lịch học
            $data = $scheduleDao->getScheduleDetailById($_POST['id']);
            if (is_null($data)) {
                _error(404);
            }
            if (!$data['is_notified']) {
                // Thông báo tới phụ huynh và giáo viên
                if($data['applied_for'] == CLASS_LEVEL) {
                    // Danh sách trẻ trong lớp
                    //$childrenIds = $childDao->getChildIdOfClass($data['class_id']);
                    $children = getChildData($data['class_id'], CLASS_CHILDREN);
                    $childrenIds = array_keys($children);
                    //$class = $classDao->getClass($data['class_id']);
                    $class = getClassData($data['class_id'], CLASS_INFO);

                    //Thông báo tới phụ huynh có trẻ trong lớp được tạo lịch học.
                    if (count($childrenIds) > 0) {
                        $parents = $childDao->getListChildNParentId($childrenIds);
                        foreach ($parents as $parent) {
                            $userDao->postNotifications($parent['parent_id'], NOTIFICATION_NEW_SCHEDULE, NOTIFICATION_NODE_TYPE_CHILD,
                                $data['schedule_id'], convertText4Web($data['schedule_name']), $parent['child_id'], convertText4Web($class['group_title']));
                        }
                        //Thông báo tới giáo viên của lớp.
                        //$teachers = $teacherDao->getTeacherOfClass($class['group_id']);
                        $teachers = getClassData($class['group_id'], CLASS_TEACHERS);
                        if(count($teachers) > 0) {
                            $teacherIds = array_keys($teachers);
                            $userDao->postNotifications($teacherIds, NOTIFICATION_NEW_SCHEDULE, NOTIFICATION_NODE_TYPE_CLASS,
                                $data['schedule_id'], convertText4Web($data['schedule_name']), $class['group_name'], convertText4Web($class['group_title']));
                        }
                        // Thông báo đến các quản lý trường khác
                        $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'schedules', $school['page_admin']);
                        $userDao->postNotifications($userManagerIds, NOTIFICATION_NEW_SCHEDULE, NOTIFICATION_NODE_TYPE_SCHOOL,
                            $data['schedule_id'], convertText4Web($data['schedule_name']), $school['page_name'], convertText4Web($class['group_title']));
                    }
                } elseif ($data['applied_for'] == CLASS_LEVEL_LEVEL) {
                    // Danh sách trẻ trong khối
                    $childrenIds = $childDao->getChildIdByClassLevel($data['class_level_id']);
                    //$class_level = $classLevelDao->getClassLevel($data['class_level_id']);
                    $class_level = getClassLevelData($data['class_level_id'], CLASS_LEVEL_INFO);

                    //Thông báo tới phụ huynh có trẻ trong lớp được tạo lịch học.
                    if (count($childrenIds) > 0) {
                        $parents = $childDao->getListChildNParentId($childrenIds);
                        foreach ($parents as $parent) {
                            $userDao->postNotifications($parent['parent_id'], NOTIFICATION_NEW_SCHEDULE, NOTIFICATION_NODE_TYPE_CHILD,
                                $data['schedule_id'], convertText4Web($data['schedule_name']), $parent['child_id'], convertText4Web($class_level['class_level_name']));
                        }
                        //Thông báo tới giáo viên của lớp.
                        $classes = getClassLevelData($data['class_level_id'], CLASS_LEVEL_CLASSES);
                        foreach ($classes as $class) {
                            //$teachers = $teacherDao->getTeacherOfClass($class['group_id']);
                            $teachers = getClassData($class['group_id'], CLASS_TEACHERS);
                            if(count($teachers) > 0) {
                                $teacherIds = array_keys($teachers);
                                $userDao->postNotifications($teacherIds, NOTIFICATION_NEW_SCHEDULE, NOTIFICATION_NODE_TYPE_CLASS,
                                    $data['schedule_id'], convertText4Web($data['schedule_name']), $class['group_name'], convertText4Web($class_level['class_level_name']));
                            }
                        }
                        // Thông báo đến các quản lý trường khác
                        $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'schedules', $school['page_admin']);
                        $userDao->postNotifications($userManagerIds, NOTIFICATION_NEW_SCHEDULE, NOTIFICATION_NODE_TYPE_SCHOOL,
                            $data['schedule_id'], convertText4Web($data['schedule_name']), $school['page_name'], convertText4Web($class_level['class_level_name']));
                    }
                } else {
                    // Danh sách trẻ trong trường
                    //$childrenIds = $childDao->getChildIdBySchool($data['school_id']);
                    $children = getSchoolData($data['school_id'], SCHOOL_CHILDREN);
                    $childrenIds = array_keys($children);

                    //Thông báo tới phụ huynh có trẻ trong lớp được tạo lịch học.
                    if (count($childrenIds) > 0) {
                        $parents = $childDao->getListChildNParentId($childrenIds);
                        foreach ($parents as $parent) {
                            $userDao->postNotifications($parent['parent_id'], NOTIFICATION_NEW_SCHEDULE, NOTIFICATION_NODE_TYPE_CHILD,
                                $data['schedule_id'], convertText4Web($data['schedule_name']), $parent['child_id'], convertText4Web($school['page_title']));
                        }
                        //Thông báo tới giáo viên của lớp.
                        // Lấy danh sách lớp
                        //$classes = $classDao->getClassesOfSchool($data['school_id']);
                        $classes = getSchoolData($data['school_id'], SCHOOL_CLASSES);
                        foreach ($classes as $class) {
                            //$teachers = $teacherDao->getTeacherOfClass($class['group_id']);
                            $teachers = getClassData($class['group_id'], CLASS_TEACHERS);
                            if(count($teachers) > 0) {
                                $teacherIds = array_keys($teachers);
                                $userDao->postNotifications($teacherIds, NOTIFICATION_NEW_SCHEDULE, NOTIFICATION_NODE_TYPE_CLASS,
                                    $data['schedule_id'], convertText4Web($data['schedule_name']), $class['group_name'], convertText4Web($school['page_title']));
                            }
                        }
                        // Thông báo đến các quản lý trường khác
                        $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'schedules', $school['page_admin']);
                        $userDao->postNotifications($userManagerIds, NOTIFICATION_NEW_SCHEDULE, NOTIFICATION_NODE_TYPE_SCHOOL,
                            $data['schedule_id'], convertText4Web($data['schedule_name']), $school['page_name'], convertText4Web($school['page_title']));
                    }
                }

                /* Inet - Tương tác trường */
                $scheduleIds = array();
                $scheduleIds[] = $data['schedule_id'];
                setIsAddNew($school['page_id'], 'schedule_created', $scheduleIds);
                /* Inet - END */

                // Cập nhật trạng thái đã gửi thông báo.
                $scheduleDao->updateStatusToNotified($_POST['id']);
            }

            $db->commit();
            return_json( array('success' => true, 'message' => __("Success")));
            break;

        case 'export':
            $A4_SCHEDULE_TEMPLATE_CATE_SAT = ABSPATH . "content/templates/schedule_A4_cate_sat_" . $system['language']['code'] . ".xlsx";
            $A4_SCHEDULE_TEMPLATE = ABSPATH . "content/templates/schedule_A4_" . $system['language']['code'] . ".xlsx";
            $A4_SCHEDULE_TEMPLATE_CATE = ABSPATH . "content/templates/schedule_A4_cate_" . $system['language']['code'] . ".xlsx";
            $A4_SCHEDULE_TEMPLATE_SAT = ABSPATH . "content/templates/schedule_A4_sat_" . $system['language']['code'] . ".xlsx";
            include_once(ABSPATH . 'includes/libs/PHPExcel/PHPExcel/IOFactory.php');
            include_once(ABSPATH . 'includes/libs/PHPExcel/PHPExcel.php');

            // Khởi tạo đối tượng
            $objPHPExcel = new PHPExcel();
            // Lấy thông tin lịch học
            $data = $scheduleDao->getScheduleDetailById($_POST['id']);
            if (is_null($data)) {
                _error(404);
            }
            if($data['applied_for'] == 2) {
                //$class_level = $classLevelDao->getClassLevel($data['class_level_id']);
                $class_level = getClassLevelData($data['class_level_id'], CLASS_LEVEL_INFO);
            }

            if($data['applied_for'] == 3) {
                //$class = $classDao->getClass($data['class_id']);
                $class = getClassData($data['class_id'], CLASS_INFO);
            }
            if($data['is_category'] && $data['is_saturday']) {
                $objPHPExcel = PHPExcel_IOFactory::load($A4_SCHEDULE_TEMPLATE_CATE_SAT);
                $firstSheet = $objPHPExcel->getSheet(0);
                $firstSheet->setCellValue('A1', mb_strtoupper(convertText4Web(str_replace('/', '_', $school['page_title'])), "UTF-8"))
                    ->setCellValue('A2', mb_strtoupper(convertText4Web(str_replace('/', '_', __("Schedule name").": " . $data['schedule_name'])), "UTF-8"));
                if($data['applied_for'] == 3) {
                    $firstSheet->setCellValue('A3', __("Scope") . ": " . convertText4Web($class['group_title']));
                } elseif ($data['applied_for'] == 2) {
                    $firstSheet->setCellValue('A3', __("Scope") . ": " . convertText4Web($class_level['class_level_name']));
                } else {
                    $firstSheet->setCellValue('A3', __("Scope") . ": " . __("School"));
                }

                $rowNumber = 6;

                foreach ($data['details'] as $detail) {
                    $array = array_values($detail);
                    $temp = array();
                    foreach ($array as $k => $value) {
                        if ($data['is_saturday']) {
                            if (($k >= 4) && ($k < (count($array)))) {
                                $temp[] = $value;
                            }
                        } else {
                            if (($k >= 4) && ($k < (count($array) - 1))) {
                                $temp[] = $value;
                            }
                        }
                    }

                    $colArray = array(
                        0 => "C",
                        1 => "D",
                        2 => "E",
                        3 => "F",
                        4 => "G",
                        5 => "H",
                    );
                    $conArray = array(
                        0 => $detail['monday'],
                        1 => $detail['tuesday'],
                        2 => $detail['wednesday'],
                        3 => $detail['thursday'],
                        4 => $detail['friday'],
                        5 => $detail['saturday'],
                    );

                    $firstSheet->setCellValue('A' . $rowNumber, convertText4Web($detail['subject_time']))
                        ->setCellValue('B' . $rowNumber, convertText4Web($detail['subject_name']));

                    for ($i = 0; $i < count($temp); $i++) {
                        for($j = $i + 1; $j < count($temp) + 1; $j++) {
                            if(($temp[$i] != $temp[$j])) {
                                if($j == $i + 1) {
                                    $firstSheet->setCellValue($colArray[$i] . $rowNumber, convertText4Web($conArray[$i]), PHPExcel_Cell_DataType::TYPE_STRING);
                                    $i = $j;
                                } else {
                                    $firstSheet->mergeCells($colArray[$i] . $rowNumber . ':' . $colArray[$j -1] . $rowNumber);
                                    $firstSheet->setCellValue($colArray[$i] . $rowNumber, convertText4Web($conArray[$i]), PHPExcel_Cell_DataType::TYPE_STRING);
                                    // Tính số ô được merge
                                    $k = $j - $i;
                                    // chiều rộng ô được merge
                                    $widthCol = $k * 18;
                                    // Set giá trị Y . $rowNumber = giá trị ô được merge
                                    $firstSheet->setCellValue('Y' . $rowNumber, convertText4Web($conArray[$i]), PHPExcel_Cell_DataType::TYPE_STRING);
                                    // Set width Y
                                    $firstSheet->getColumnDimension('Y')->setWidth($widthCol);
                                    // Set wrap text Y .$rowNumber
                                    $firstSheet->getStyle('Y' . $rowNumber)->getAlignment()->setWrapText(true);
                                    // Set autoFit row height Y.$rowNumber
                                    //$firstSheet->getRowDimension($rowNumber)->setRowHeight(-1);
                                    $firstSheet->getColumnDimension('Y')->setVisible(false);
                                    $i = $j;
                                }
                            }
                        }
                    }
                    $firstSheet->getRowDimension($rowNumber)->setRowHeight(-1);
                    $rowNumber++;
                }
//            if(!$data['is_saturday']) {
//                $firstSheet->removeColumn("H");
//            }
//            if(!$data['is_category']) {
//                $firstSheet->removeColumn("B");
//            }

                $numRow = $rowNumber - 1;

                // Set style cho thời gian và hoạt động
                $firstSheet->getStyle('A6:B'.$numRow)->applyFromArray(styleArray('Times new Roman', true, false, 11, PHPExcel_Style_Alignment::HORIZONTAL_CENTER, PHPExcel_Style_Alignment::VERTICAL_CENTER, true));

                // Set style cho nội dung
                $firstSheet->getStyle('C6:H' . $numRow)->applyFromArray(styleArray('Times new Roman', false, false, 11, PHPExcel_Style_Alignment::HORIZONTAL_CENTER, PHPExcel_Style_Alignment::VERTICAL_TOP, true));

                $firstSheet->getStyle('C6:H' . $rowNumber)->getAlignment()->setWrapText(true);
//            //$firstSheet->getDefaultRowDimension()->se‌​tRowHeight(-1);
//            $firstSheet->getRowDimension('7')->setRowHeight(-1);
//            foreach($firstSheet->getRowDimensions() as $rd) {
//                $rd->setRowHeight(-1);
//            }
                //Set border cho toàn bộ bảng
                $all_border_style = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('rgb' => '333333'))));
                $firstSheet->getStyle('A5:H' . $numRow)->applyFromArray($all_border_style);

                $firstSheet->setBreak( 'I'.$numRow , PHPExcel_Worksheet::BREAK_COLUMN );
                $firstSheet->getPageSetup()->setPrintArea('A1:H' . $numRow);
            } elseif ($data['is_category'] && !$data['is_saturday']) {
                $objPHPExcel = PHPExcel_IOFactory::load($A4_SCHEDULE_TEMPLATE_CATE);
                $firstSheet = $objPHPExcel->getSheet(0);
                $firstSheet->setCellValue('A1', mb_strtoupper(convertText4Web(str_replace('/', '_', $school['page_title'])), "UTF-8"))
                    ->setCellValue('A2', mb_strtoupper(convertText4Web(str_replace('/', '_', __("Schedule name").": " . $data['schedule_name'])), "UTF-8"));
                if($data['applied_for'] == 3) {
                    $firstSheet->setCellValue('A3', __("Scope") . ": " . convertText4Web($class['group_title']));
                } elseif ($data['applied_for'] == 2) {
                    $firstSheet->setCellValue('A3', __("Scope") . ": " . convertText4Web($class_level['class_level_name']));
                } else {
                    $firstSheet->setCellValue('A3', __("Scope") . ": " . __("School"));
                }

                $rowNumber = 6;

                foreach ($data['details'] as $detail) {
                    $array = array_values($detail);
                    $temp = array();
                    foreach ($array as $k => $value) {
                        if ($data['is_saturday']) {
                            if (($k >= 4) && ($k < (count($array)))) {
                                $temp[] = $value;
                            }
                        } else {
                            if (($k >= 4) && ($k < (count($array) - 1))) {
                                $temp[] = $value;
                            }
                        }
                    }

                    $colArray = array(
                        0 => "C",
                        1 => "D",
                        2 => "E",
                        3 => "F",
                        4 => "G",
                        5 => "H",
                    );
                    $conArray = array(
                        0 => $detail['monday'],
                        1 => $detail['tuesday'],
                        2 => $detail['wednesday'],
                        3 => $detail['thursday'],
                        4 => $detail['friday'],
                        5 => $detail['saturday'],
                    );

                    $firstSheet->setCellValue('A' . $rowNumber, convertText4Web($detail['subject_time']))
                        ->setCellValue('B' . $rowNumber, convertText4Web($detail['subject_name']));

                    for ($i = 0; $i < count($temp); $i++) {
                        for($j = $i + 1; $j < count($temp) + 1; $j++) {
                            if(($temp[$i] != $temp[$j])) {
                                if($j == $i + 1) {
                                    $firstSheet->setCellValue($colArray[$i] . $rowNumber, convertText4Web($conArray[$i]), PHPExcel_Cell_DataType::TYPE_STRING);
                                    $i = $j;
                                } else {
                                    $firstSheet->mergeCells($colArray[$i] . $rowNumber . ':' . $colArray[$j -1] . $rowNumber);
                                    $firstSheet->setCellValue($colArray[$i] . $rowNumber, convertText4Web($conArray[$i]), PHPExcel_Cell_DataType::TYPE_STRING);
                                    // Tính số ô được merge
                                    $k = $j - $i;
                                    // chiều rộng ô được merge
                                    $widthCol = $k * 18;
                                    // Set giá trị Y . $rowNumber = giá trị ô được merge
                                    $firstSheet->setCellValue('Y' . $rowNumber, convertText4Web($conArray[$i]), PHPExcel_Cell_DataType::TYPE_STRING);
                                    // Set width Y
                                    $firstSheet->getColumnDimension('Y')->setWidth($widthCol);
                                    // Set wrap text Y .$rowNumber
                                    $firstSheet->getStyle('Y' . $rowNumber)->getAlignment()->setWrapText(true);
                                    // Set autoFit row height Y.$rowNumber
                                    //$firstSheet->getRowDimension($rowNumber)->setRowHeight(-1);
                                    $firstSheet->getColumnDimension('Y')->setVisible(false);
                                    $i = $j;
                                }
                            }
                        }
                    }
                    $firstSheet->getRowDimension($rowNumber)->setRowHeight(-1);
                    $rowNumber++;
                }
//            if(!$data['is_saturday']) {
//                $firstSheet->removeColumn("H");
//            }
//            if(!$data['is_category']) {
//                $firstSheet->removeColumn("B");
//            }

                $numRow = $rowNumber - 1;

                // Set style cho thời gian và hoạt động
                $firstSheet->getStyle('A6:B'.$numRow)->applyFromArray(styleArray('Times new Roman', true, false, 11, PHPExcel_Style_Alignment::HORIZONTAL_CENTER, PHPExcel_Style_Alignment::VERTICAL_CENTER, true));

                // Set style cho nội dung
                $firstSheet->getStyle('C6:G' . $numRow)->applyFromArray(styleArray('Times new Roman', false, false, 11, PHPExcel_Style_Alignment::HORIZONTAL_CENTER, PHPExcel_Style_Alignment::VERTICAL_TOP, true));

                $firstSheet->getStyle('C6:G' . $rowNumber)->getAlignment()->setWrapText(true);
//            //$firstSheet->getDefaultRowDimension()->se‌​tRowHeight(-1);
//            $firstSheet->getRowDimension('7')->setRowHeight(-1);
//            foreach($firstSheet->getRowDimensions() as $rd) {
//                $rd->setRowHeight(-1);
//            }
                //Set border cho toàn bộ bảng
                $all_border_style = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('rgb' => '333333'))));
                $firstSheet->getStyle('A5:G' . $numRow)->applyFromArray($all_border_style);

                $firstSheet->setBreak( 'I'.$numRow , PHPExcel_Worksheet::BREAK_COLUMN );
                $firstSheet->getPageSetup()->setPrintArea('A1:G' . $numRow);
            }  elseif ($data['is_saturday'] && !$data['is_category']) {
                $objPHPExcel = PHPExcel_IOFactory::load($A4_SCHEDULE_TEMPLATE_SAT);
                $firstSheet = $objPHPExcel->getSheet(0);
                $firstSheet->setCellValue('A1', mb_strtoupper(convertText4Web(str_replace('/', '_', $school['page_title'])), "UTF-8"))
                    ->setCellValue('A2', mb_strtoupper(convertText4Web(str_replace('/', '_', __("Schedule name").": " . $data['schedule_name'])), "UTF-8"));
                if($data['applied_for'] == 3) {
                    $firstSheet->setCellValue('A3', __("Scope") . ": " . convertText4Web($class['group_title']));
                } elseif ($data['applied_for'] == 2) {
                    $firstSheet->setCellValue('A3', __("Scope") . ": " . convertText4Web($class_level['class_level_name']));
                } else {
                    $firstSheet->setCellValue('A3', __("Scope") . ": " . __("School"));
                }

                $rowNumber = 6;

                foreach ($data['details'] as $detail) {
                    $array = array_values($detail);
                    $temp = array();
                    foreach ($array as $k => $value) {
                        if ($data['is_saturday']) {
                            if (($k >= 4) && ($k < (count($array)))) {
                                $temp[] = $value;
                            }
                        } else {
                            if (($k >= 4) && ($k < (count($array) - 1))) {
                                $temp[] = $value;
                            }
                        }
                    }

                    $colArray = array(
                        0 => "B",
                        1 => "C",
                        2 => "D",
                        3 => "E",
                        4 => "F",
                        5 => "G",
                    );
                    $conArray = array(
                        0 => $detail['monday'],
                        1 => $detail['tuesday'],
                        2 => $detail['wednesday'],
                        3 => $detail['thursday'],
                        4 => $detail['friday'],
                        5 => $detail['saturday'],
                    );

                    $firstSheet->setCellValue('A' . $rowNumber, convertText4Web($detail['subject_time']));

                    for ($i = 0; $i < count($temp); $i++) {
                        for($j = $i + 1; $j < count($temp) + 1; $j++) {
                            if(($temp[$i] != $temp[$j])) {
                                if($j == $i + 1) {
                                    $firstSheet->setCellValue($colArray[$i] . $rowNumber, convertText4Web($conArray[$i]), PHPExcel_Cell_DataType::TYPE_STRING);
                                    $i = $j;
                                } else {
                                    $firstSheet->mergeCells($colArray[$i] . $rowNumber . ':' . $colArray[$j -1] . $rowNumber);
                                    $firstSheet->setCellValue($colArray[$i] . $rowNumber, convertText4Web($conArray[$i]), PHPExcel_Cell_DataType::TYPE_STRING);
                                    // Tính số ô được merge
                                    $k = $j - $i;
                                    // chiều rộng ô được merge
                                    $widthCol = $k * 18;
                                    // Set giá trị Y . $rowNumber = giá trị ô được merge
                                    $firstSheet->setCellValue('Y' . $rowNumber, convertText4Web($conArray[$i]), PHPExcel_Cell_DataType::TYPE_STRING);
                                    // Set width Y
                                    $firstSheet->getColumnDimension('Y')->setWidth($widthCol);
                                    // Set wrap text Y .$rowNumber
                                    $firstSheet->getStyle('Y' . $rowNumber)->getAlignment()->setWrapText(true);
                                    // Set autoFit row height Y.$rowNumber
                                    //$firstSheet->getRowDimension($rowNumber)->setRowHeight(-1);
                                    $firstSheet->getColumnDimension('Y')->setVisible(false);
                                    $i = $j;
                                }
                            }
                        }
                    }
                    $firstSheet->getRowDimension($rowNumber)->setRowHeight(-1);
                    $rowNumber++;
                }
//            if(!$data['is_saturday']) {
//                $firstSheet->removeColumn("H");
//            }
//            if(!$data['is_category']) {
//                $firstSheet->removeColumn("B");
//            }

                $numRow = $rowNumber - 1;

                // Set style cho thời gian và hoạt động
                $firstSheet->getStyle('A6:A'.$numRow)->applyFromArray(styleArray('Times new Roman', true, false, 11, PHPExcel_Style_Alignment::HORIZONTAL_CENTER, PHPExcel_Style_Alignment::VERTICAL_CENTER, true));

                // Set style cho nội dung
                $firstSheet->getStyle('B6:G' . $numRow)->applyFromArray(styleArray('Times new Roman', false, false, 11, PHPExcel_Style_Alignment::HORIZONTAL_CENTER, PHPExcel_Style_Alignment::VERTICAL_TOP, true));

                $firstSheet->getStyle('B6:G' . $rowNumber)->getAlignment()->setWrapText(true);
//            //$firstSheet->getDefaultRowDimension()->se‌​tRowHeight(-1);
//            $firstSheet->getRowDimension('7')->setRowHeight(-1);
//            foreach($firstSheet->getRowDimensions() as $rd) {
//                $rd->setRowHeight(-1);
//            }
                //Set border cho toàn bộ bảng
                $all_border_style = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('rgb' => '333333'))));
                $firstSheet->getStyle('A5:G' . $numRow)->applyFromArray($all_border_style);

                $firstSheet->setBreak( 'I'.$numRow , PHPExcel_Worksheet::BREAK_COLUMN );
                $firstSheet->getPageSetup()->setPrintArea('A1:G' . $numRow);
            } elseif (!$data['is_saturday'] && !$data['is_category']) {
                $objPHPExcel = PHPExcel_IOFactory::load($A4_SCHEDULE_TEMPLATE);
                $firstSheet = $objPHPExcel->getSheet(0);
                $firstSheet->setCellValue('A1', mb_strtoupper(convertText4Web(str_replace('/', '_', $school['page_title'])), "UTF-8"))
                    ->setCellValue('A2', mb_strtoupper(convertText4Web(str_replace('/', '_', __("Schedule name").": " . $data['schedule_name'])), "UTF-8"));
                if($data['applied_for'] == 3) {
                    $firstSheet->setCellValue('A3', __("Scope") . ": " . convertText4Web($class['group_title']));
                } elseif ($data['applied_for'] == 2) {
                    $firstSheet->setCellValue('A3', __("Scope") . ": " . convertText4Web($class_level['class_level_name']));
                } else {
                    $firstSheet->setCellValue('A3', __("Scope") . ": " . __("School"));
                }

                $rowNumber = 6;

                foreach ($data['details'] as $detail) {
                    $array = array_values($detail);
                    $temp = array();
                    foreach ($array as $k => $value) {
                        if ($data['is_saturday']) {
                            if (($k >= 4) && ($k < (count($array)))) {
                                $temp[] = $value;
                            }
                        } else {
                            if (($k >= 4) && ($k < (count($array) - 1))) {
                                $temp[] = $value;
                            }
                        }
                    }

                    $colArray = array(
                        0 => "B",
                        1 => "C",
                        2 => "D",
                        3 => "E",
                        4 => "F",
                        5 => "G",
                    );
                    $conArray = array(
                        0 => $detail['monday'],
                        1 => $detail['tuesday'],
                        2 => $detail['wednesday'],
                        3 => $detail['thursday'],
                        4 => $detail['friday'],
                        5 => $detail['saturday'],
                    );

                    $firstSheet->setCellValue('A' . $rowNumber, convertText4Web($detail['subject_time']));

                    for ($i = 0; $i < count($temp); $i++) {
                        for($j = $i + 1; $j < count($temp) + 1; $j++) {
                            if(($temp[$i] != $temp[$j])) {
                                if($j == $i + 1) {
                                    $firstSheet->setCellValue($colArray[$i] . $rowNumber, convertText4Web($conArray[$i]), PHPExcel_Cell_DataType::TYPE_STRING);
                                    $i = $j;
                                } else {
                                    $firstSheet->mergeCells($colArray[$i] . $rowNumber . ':' . $colArray[$j -1] . $rowNumber);
                                    $firstSheet->setCellValue($colArray[$i] . $rowNumber, convertText4Web($conArray[$i]), PHPExcel_Cell_DataType::TYPE_STRING);
                                    // Tính số ô được merge
                                    $k = $j - $i;
                                    // chiều rộng ô được merge
                                    $widthCol = $k * 18;
                                    // Set giá trị Y . $rowNumber = giá trị ô được merge
                                    $firstSheet->setCellValue('Y' . $rowNumber, convertText4Web($conArray[$i]), PHPExcel_Cell_DataType::TYPE_STRING);
                                    // Set width Y
                                    $firstSheet->getColumnDimension('Y')->setWidth($widthCol);
                                    // Set wrap text Y .$rowNumber
                                    $firstSheet->getStyle('Y' . $rowNumber)->getAlignment()->setWrapText(true);
                                    // Set autoFit row height Y.$rowNumber
                                    //$firstSheet->getRowDimension($rowNumber)->setRowHeight(-1);
                                    $firstSheet->getColumnDimension('Y')->setVisible(false);
                                    $i = $j;
                                }
                            }
                        }
                    }
                    $firstSheet->getRowDimension($rowNumber)->setRowHeight(-1);
                    $rowNumber++;
                }
//            if(!$data['is_saturday']) {
//                $firstSheet->removeColumn("H");
//            }
//            if(!$data['is_category']) {
//                $firstSheet->removeColumn("B");
//            }

                $numRow = $rowNumber - 1;

                // Set style cho thời gian và hoạt động
                $firstSheet->getStyle('A6:A'.$numRow)->applyFromArray(styleArray('Times new Roman', true, false, 11, PHPExcel_Style_Alignment::HORIZONTAL_CENTER, PHPExcel_Style_Alignment::VERTICAL_CENTER, true));

                // Set style cho nội dung
                $firstSheet->getStyle('B6:F' . $numRow)->applyFromArray(styleArray('Times new Roman', false, false, 11, PHPExcel_Style_Alignment::HORIZONTAL_CENTER, PHPExcel_Style_Alignment::VERTICAL_TOP, true));

                $firstSheet->getStyle('B6:F' . $rowNumber)->getAlignment()->setWrapText(true);

                //Set border cho toàn bộ bảng
                $all_border_style = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('rgb' => '333333'))));
                $firstSheet->getStyle('A5:F' . $numRow)->applyFromArray($all_border_style);

                $firstSheet->setBreak( 'I'.$numRow , PHPExcel_Worksheet::BREAK_COLUMN );
                $firstSheet->getPageSetup()->setPrintArea('A1:F' . $numRow);
            }

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            ob_start();
            // $month = str_replace('/', '_', $data['month']);
            // $group_title = convert_vi_to_en($data["group_title"]);
            // $group_title = str_replace(" ", '_', $group_title);
            $file_name = convertText4Web($data['schedule_name'] . "_" . $data['school_id'] . '.xlsx');
            $objWriter->save("php://output");

            $xlsData = ob_get_contents();
            ob_end_clean();
            return_json(array(
                'file_name' => $file_name,
                'file' => "data:application/vnd.ms-excel;base64," . base64_encode($xlsData)
            ));
            break;

        case 'import':
            if (!canEdit($_POST['school_username'], 'schedules')) {
                _error(403);
            }
            include_once(ABSPATH . 'includes/libs/PHPExcel/PHPExcel/IOFactory.php');
            include_once(ABSPATH . 'includes/ajax/ci/utilities.php');

            $file_name = $_FILES['file']['name'];
            $inputFileName = $_FILES['file']['tmp_name'];

            $results = array();
            $sheet = readScheduleInfoInExcelFile($inputFileName);
            // Lấy ra các thông tin của lịch học
            $args = array();
            $args['schedule_name'] = $_POST['schedule_name'];
            $args['begin'] = $_POST['begin'];
            $args['description'] = $_POST['description'];
            $args['school_id'] = $school['page_id'];
            $args['class_level_id'] = $_POST['class_level_id'];
            $args['class_id'] = $_POST['class_id'];
            $args['is_category'] = (isset($_POST['use_cate']) && $_POST['use_cate'] == 'on') ? 1 : 0;
            $args['is_saturday'] = (isset($_POST['study_saturday']) && $_POST['study_saturday'] == 'on') ? 1 : 0;
            $args['is_notified'] = (isset($_POST['notify_immediately']) && $_POST['notify_immediately'] == 'on')? 1: 0;
            $level = $_POST['level'];
            if($level == 2 && $args['class_level_id'] == 0) {
                throw new Exception(__("You must select a class level"));
            } else if($level == 3 && $args['class_id'] == 0) {
                throw new Exception(__("You must select a class"));
            }
            $args['applied_for'] = 0;
            if($args['class_id'] > 0) {
                $args['applied_for'] = CLASS_LEVEL;
            }
            if(($args['class_id'] == 0) && $args['class_level_id'] > 0) {
                $args['applied_for'] = CLASS_LEVEL_LEVEL;
            }
            if($args['class_id'] == 0 && $args['class_level_id'] == 0) {
                $args['applied_for'] = SCHOOL_LEVEL;
            }
            if ($sheet['error'] == 0) {

                $db->begin_transaction();

                // 1. Insert lịch học vào bảng ci_schedule
                $scheduleId = $scheduleDao->insertSchedule($args);

                // 2. Insert vào bảng ci_schedule_detail
                $new_subject_list = array();
                $succes = 1;
                foreach ($sheet['subject_list'] as $subject) { //Duyệt danh sách môn học
                    if ($subject['error'] == 0) {
                        try {
                            // Thêm schedule_id vào $subject
                            $subject['schedule_id'] = $scheduleId;
                            //1. Tạo thông tin chi tiết lịch học trong bảng ci_schedule_detail
                            if($args['is_category'] && (is_null($subject['start']) || is_null($subject['category']))) {
                                throw new Exception(__('Please enter the full information'));
                            }
                            $lastId = $scheduleDao->insertScheduleDetailImport($subject);

                            $db->commit();
                        } catch (Exception $e) {
                            $db->rollback();
                            $subject['error'] = 1;
                            $subject['message'] = $e->getMessage();
                        } finally {
                            $db->autocommit(true);
                        }
                    } else {
                        $succes = 0;
                    }
                    $new_subject_list[] = $subject;
                }
                if($succes == 1) {
                    // Thông báo tới các đối tượng liên quan
                    if($args['is_notified']) {
                        if($args['applied_for'] == CLASS_LEVEL) {
                            // Danh sách trẻ trong lớp
                            //$childrenIds = $childDao->getChildIdOfClass($args['class_id']);
                            $children = getClassData($args['class_id'], CLASS_CHILDREN);
                            $childrenIds = array_keys($children);
                            //$class = $classDao->getClass($args['class_id']);
                            $class = getClassData($args['class_id'], CLASS_INFO);

                            //Thông báo tới phụ huynh có trẻ trong lớp được tạo lịch học.
                            if (count($childrenIds) > 0) {
                                $parents = $childDao->getListChildNParentId($childrenIds);
                                foreach ($parents as $parent) {
                                    $userDao->postNotifications($parent['parent_id'], NOTIFICATION_NEW_SCHEDULE, NOTIFICATION_NODE_TYPE_CHILD,
                                        $scheduleId, convertText4Web($_POST['schedule_name']), $parent['child_id'], convertText4Web($class['group_title']));
                                }
                                //Thông báo tới giáo viên của lớp.
                                //$teachers = $teacherDao->getTeacherOfClass($class['group_id']);
                                $teachers = getClassData($class['group_id'], CLASS_TEACHERS);
                                if(count($teachers) > 0) {
                                    $teacherIds = array_keys($teachers);
                                    $userDao->postNotifications($teacherIds, NOTIFICATION_NEW_SCHEDULE, NOTIFICATION_NODE_TYPE_CLASS,
                                        $scheduleId, convertText4Web($_POST['schedule_name']), $class['group_name'], convertText4Web($class['group_title']));
                                }
                                // Thông báo đến các quản lý trường khác
                                $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'schedules', $school['page_admin']);
                                $userDao->postNotifications($userManagerIds, NOTIFICATION_NEW_SCHEDULE, NOTIFICATION_NODE_TYPE_SCHOOL,
                                    $scheduleId, convertText4Web($_POST['schedule_name']), $school['page_name'], convertText4Web($class['group_title']));
                            }
                        } elseif ($args['applied_for'] == CLASS_LEVEL_LEVEL) {
                            // Danh sách trẻ trong khối
                            $childrenIds = $childDao->getChildIdByClassLevel($args['class_level_id']);
                            //$class_level = $classLevelDao->getClassLevel($args['class_level_id']);
                            $class_level = getClassLevelData($args['class_level_id'], CLASS_LEVEL_INFO);

                            //Thông báo tới phụ huynh có trẻ trong lớp được tạo lịch học.
                            if (count($childrenIds) > 0) {
                                $parents = $childDao->getListChildNParentId($childrenIds);
                                foreach ($parents as $parent) {
                                    $userDao->postNotifications($parent['parent_id'], NOTIFICATION_NEW_SCHEDULE, NOTIFICATION_NODE_TYPE_CHILD,
                                        $scheduleId, convertText4Web($_POST['schedule_name']), $parent['child_id'], convertText4Web($class_level['class_level_name']));
                                }
                                //Thông báo tới giáo viên của lớp.
                                //$classes = $classDao->getClassesOfLevel($args['class_level_id']);
                                $classes = getClassLevelData($args['class_level_id'], CLASS_LEVEL_CLASSES);
                                foreach ($classes as $class) {
                                    //$teachers = $teacherDao->getTeacherOfClass($class['group_id']);
                                    $teachers = getClassData($class['group_id'], CLASS_TEACHERS);
                                    if(count($teachers) > 0) {
                                        $teacherIds = array_keys($teachers);
                                        $userDao->postNotifications($teacherIds, NOTIFICATION_NEW_SCHEDULE, NOTIFICATION_NODE_TYPE_CLASS,
                                            $scheduleId, convertText4Web($_POST['schedule_name']), $class['group_name'], convertText4Web($class_level['class_level_name']));
                                    }
                                }
                                // Thông báo đến các quản lý trường khác
                                $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'schedules', $school['page_admin']);
                                $userDao->postNotifications($userManagerIds, NOTIFICATION_NEW_SCHEDULE, NOTIFICATION_NODE_TYPE_SCHOOL,
                                    $scheduleId, convertText4Web($_POST['schedule_name']), $school['page_name'], convertText4Web($class_level['class_level_name']));
                            }
                        } else {
                            // Danh sách trẻ trong trường
                            //$childrenIds = $childDao->getChildIdBySchool($args['school_id']);
                            $children = getSchoolData($args['school_id'], SCHOOL_CHILDREN);
                            $childrenIds = array_keys($children);

                            //Thông báo tới phụ huynh có trẻ trong lớp được tạo lịch học.
                            if (count($childrenIds) > 0) {
                                $parents = $childDao->getListChildNParentId($childrenIds);
                                foreach ($parents as $parent) {
                                    $userDao->postNotifications($parent['parent_id'], NOTIFICATION_NEW_SCHEDULE, NOTIFICATION_NODE_TYPE_CHILD,
                                        $scheduleId, convertText4Web($_POST['schedule_name']), $parent['child_id'], convertText4Web($school['page_title']));
                                }
                                //Thông báo tới giáo viên của lớp.
                                // Lấy danh sách lớp
                                //$classes = $classDao->getClassesOfSchool($args['school_id']);
                                $classes = getSchoolData($args['school_id'], SCHOOL_CLASSES);
                                foreach ($classes as $class) {
                                    //$teachers = $teacherDao->getTeacherOfClass($class['group_id']);
                                    $teachers = getClassData($class['group_id'], CLASS_TEACHERS);
                                    if(count($teachers) > 0) {
                                        $teacherIds = array_keys($teachers);
                                        $userDao->postNotifications($teacherIds, NOTIFICATION_NEW_SCHEDULE, NOTIFICATION_NODE_TYPE_CLASS,
                                            $scheduleId, convertText4Web($_POST['schedule_name']), $class['group_name'], convertText4Web($school['page_title']));
                                    }
                                }
                                // Thông báo đến các quản lý trường khác
                                $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'schedules', $school['page_admin']);
                                $userDao->postNotifications($userManagerIds, NOTIFICATION_NEW_SCHEDULE, NOTIFICATION_NODE_TYPE_SCHOOL,
                                    $scheduleId, convertText4Web($_POST['schedule_name']), $school['page_name'], convertText4Web($school['page_title']));
                            }
                        }

                        /* Inet - Tương tác trường */
                        $scheduleIds = array();
                        $scheduleIds[] = $scheduleId;
                        setIsAddNew($school['page_id'], 'schedule_created', $scheduleIds);
                        /* Inet - END */
                    }
                }
                // Tăng lượt thêm mới
                addInteractive($school['page_id'], 'schedule', 'school_view', $scheduleId, 1);
            }

            $sheet['subject_list'] = $new_subject_list;
            $smarty->assign("sheet", $sheet);
            $results['results'] = $smarty->fetch("ci/school/ajax.schedule.importresult.tpl");
            return_json($results);
            break;
        case 'preview':
            $results = array();
            $results['schedule_name'] = $_POST['schedule_name'];
            $results['applied_for'] = $_POST['schedule_level'];
            $results['begin'] = $_POST['schedule_begin'];
            $results['class_level_id'] = $_POST['class_level_id'];
            $results['class_id'] = $_POST['class_id'];
            $results['description'] = $_POST['schedule_description'];
            $results['is_category'] = $_POST['schedule_category'] == 'on'? 1 : 0;
            $results['is_saturday'] = $_POST['schedule_saturday'] == 'on'? 1 : 0;

            $results['schedule_name'] = trim($results['schedule_name']);
            if($results['schedule_name'] == '') {
                throw new Exception(__("You must enter schedule name"));
            }
            if($results['applied_for'] == 2 && $results['class_level_id'] == 0) {
                throw new Exception(__("You must select a class level"));
            } else if($results['applied_for'] == 3 && $results['class_id'] == 0) {
                throw new Exception(__("You must select a class"));
            }

            $begin = toDBDate($results['begin']);
            date_default_timezone_set('Asia/Ho_Chi_Minh');
            $dateBegin = strtotime($begin);
            $weekday =  strtolower(date('D', $dateBegin));
            if(!($weekday === "mon")) {
                throw new Exception(__("Start date must be Monday"));
            }
            global $db;
            $begin = toDBDate($results['begin']);
            $strSql = sprintf("SELECT * FROM ci_schedule WHERE school_id = %s AND class_level_id = %s AND class_id = %s AND begin = %s", secure($school['page_id'], 'int'), secure($results['class_level_id'], 'int'), secure($results['class_id'], 'int'), secure($begin));

            $getSchedule = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
            if($getSchedule->num_rows > 0) {
                throw new Exception(__("There are schedules for this week, please check again!"));
            }

            if(count($_POST['startList']) == 0) {
                throw new Exception(__("Please enter schedule's information"));
            }

            if($results['is_category']) {
                $_POST['categoryList'] = array_map("trim", $_POST['categoryList']);
                $_POST['categoryList'] = array_diff($_POST['categoryList'], array(""));
            }
            if($results['is_category'] && (count($_POST['startList']) != count($_POST['categoryList']))) {
                throw new Exception(__('Please enter the full information'));
            }

            $details = array();
            foreach ($_POST['startList'] as $k => $row) {
                $detail = array();
                $detail['subject_name'] = $_POST['categoryList'][$k];
                $detail['subject_time'] = $row;
                $detail['monday'] = $_POST['monList'][$k];
                $detail['tuesday'] = $_POST['tueList'][$k];
                $detail['wednesday'] = $_POST['wedList'][$k];
                $detail['thursday'] = $_POST['thuList'][$k];
                $detail['friday'] = $_POST['friList'][$k];
                $detail['saturday'] = $_POST['satList'][$k];
                $details[] = $detail;
            }
            $results['details'] = $details;

            $smarty->assign('data', $results);

            //$class_levels = $classLevelDao->getClassLevels($class['school_id']);
            $class_levels = getSchoolData($school['page_id'], SCHOOL_CLASS_LEVELS);
            $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
            if($results['applied_for'] == 2) {
                foreach ($class_levels as $row) {
                    if($row['class_level_id'] == $results['class_level_id']) {
                        $smarty->assign('class_level', $row);
                    }
                }
            }
            if($results['applied_for'] == 3) {
                foreach ($classes as $row) {
                    if($row['group_id'] == $results['class_id']) {
                        $smarty->assign('class', $row);
                    }
                }
            }

            $return['results'] = $smarty->fetch("ci/school/ajax.school.schedule.preview.tpl");
            return_json($return);
            break;
        case 'preview_excel':
            if (!canEdit($_POST['school_username'], 'schedules')) {
                _error(403);
            }
            include_once(ABSPATH . 'includes/libs/PHPExcel/PHPExcel/IOFactory.php');
            include_once(ABSPATH . 'includes/ajax/ci/utilities.php');

            if($_FILES['file']['tmp_name'] == '') {
                throw new Exception(__("You have not chosen your file"));
            }
            $file_name = $_FILES['file']['name'];
            $inputFileName = $_FILES['file']['tmp_name'];

            $sheet = readScheduleInfoInExcelFile($inputFileName);
            // Lấy ra các thông tin của lịch học
            $args = array();
            $args['schedule_name'] = $_POST['schedule_name'];
            $args['begin'] = $_POST['begin'];
            $args['description'] = $_POST['description'];
            $args['school_id'] = $school['page_id'];
            $args['class_level_id'] = $_POST['class_level_id'];
            $args['class_id'] = $_POST['class_id'];
            $args['is_category'] = (isset($_POST['use_cate']) && $_POST['use_cate'] == 'on') ? 1 : 0;
            $args['is_saturday'] = (isset($_POST['study_saturday']) && $_POST['study_saturday'] == 'on') ? 1 : 0;

            $args['applied_for'] = 0;
            if($args['class_id'] > 0) {
                $args['applied_for'] = CLASS_LEVEL;
            }
            if(($args['class_id'] == 0) && $args['class_level_id'] > 0) {
                $args['applied_for'] = CLASS_LEVEL_LEVEL;
            }
            if($args['class_id'] == 0 && $args['class_level_id'] == 0) {
                $args['applied_for'] = SCHOOL_LEVEL;
            }
            if ($sheet['error'] == 0) {
                $args['schedule_name'] = trim($args['schedule_name']);
                if($args['schedule_name'] == '') {
                    throw new Exception(__("You must enter schedule name"));
                }

                $begin = toDBDate($args['begin']);
                date_default_timezone_set('Asia/Ho_Chi_Minh');
                $dateBegin = strtotime($begin);
                $weekday =  strtolower(date('D', $dateBegin));
                if(!($weekday === "mon")) {
                    throw new Exception(__("Start date must be Monday"));
                }
                global $db;
                $begin = toDBDate($args['begin']);
                $strSql = sprintf("SELECT * FROM ci_schedule WHERE school_id = %s AND class_level_id = %s AND class_id = %s AND begin = %s", secure($school['page_id'], 'int'), secure($args['class_level_id'], 'int'), secure($args['class_id'], 'int'), secure($begin));

                $getSchedule = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
                if($getSchedule->num_rows > 0) {
                    throw new Exception(__("There are schedules for this week, please check again!"));
                }

                $details = array();
                foreach ($sheet['subject_list'] as $row) {
                    if($row['error'] == 0) {
                        $detail = array();
                        $detail['subject_name'] = $row['category'];
                        $detail['subject_time'] = $row['start'];
                        $detail['monday'] = $row['mon'];
                        $detail['tuesday'] = $row['tue'];
                        $detail['wednesday'] = $row['wed'];
                        $detail['thursday'] = $row['thu'];
                        $detail['friday'] = $row['fri'];
                        $detail['saturday'] = $row['sat'];
                        $details[] = $detail;
                    }
                }

                $args['details'] = $details;

                $smarty->assign('data', $args);

                $class_levels = getSchoolData($school['page_id'], SCHOOL_CLASS_LEVELS);
                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                if($args['applied_for'] == 2) {
                    foreach ($class_levels as $row) {
                        if($row['class_level_id'] == $args['class_level_id']) {
                            $smarty->assign('class_level', $row);
                        }
                    }
                }
                if($args['applied_for'] == 3) {
                    foreach ($classes as $row) {
                        if($row['group_id'] == $args['class_id']) {
                            $smarty->assign('class', $row);
                        }
                    }
                }

                $return['results'] = $smarty->fetch("ci/school/ajax.school.schedule.preview.tpl");
                return_json($return);
            } else {
                throw new Exception($sheet['message']);
            }
            break;
        default:
            _error(400);
            break;

    }
} catch (Exception $e) {
    $db->rollback();
    return_json(array('error' => true, 'message' => $e->getMessage()));
} finally {
    $db->autocommit(true);
}
