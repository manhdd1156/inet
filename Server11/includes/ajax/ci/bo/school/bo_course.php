<?php
/**
 * Package: ajax/ci/bo
 *
 * @package Inet
 * @author ManhDD
 */

// fetch bootstrap
require('../../../../../bootstrap.php');
// check AJAX Request
is_ajax();
check_login();

if (is_empty($_POST['school_username']) || !valid_username($_POST['school_username'])) {
    _error(404);
}

include_once(DAO_PATH . 'dao_school.php');
include_once(DAO_PATH . 'dao_parent.php');
include_once(DAO_PATH . 'dao_teacher.php');
include_once(DAO_PATH . 'dao_user.php');
include_once(DAO_PATH . 'dao_class.php');
include_once(DAO_PATH . 'dao_child.php');
include_once(DAO_PATH . 'dao_mail.php');
include_once(DAO_PATH . 'dao_report.php');
include_once(DAO_PATH . 'dao_conduct.php');
include_once(DAO_PATH . 'dao_point.php');
include_once(DAO_PATH . 'dao_subject.php');
include_once(DAO_PATH . 'dao_class_level.php');

$schoolDao = new SchoolDAO();
$parentDao = new ParentDAO();
$teacherDao = new TeacherDAO();
$userDao = new UserDAO();
$classDao = new ClassDAO();
$childDao = new ChildDAO();
$mailDao = new MailDAO();
$reportDao = new ReportDAO();
$conductDao = new ConductDAO();
$pointDao = new PointDAO();
$subjectDao = new SubjectDAO();
$classLevelDao = new ClassLevelDAO();

$school = getSchoolDataByUsername($_POST['school_username'], SCHOOL_INFO);


try {
    global $db,$smarty;
    $return = array();
    $db->autocommit(false);
    switch ($_POST['do']) {
        case 'list_class':
            if (!isset($_POST['class_level_id']) || !is_numeric($_POST['class_level_id'])) {
                _error(404);
            }


            // Laays danh sách lớp của 1 khối
            $classes = getClassLevelData($_POST['class_level_id'], CLASS_LEVEL_CLASSES);

            $results = "<option value=''>" . __("Select class") . "...</option>";
            $idx = 1;
            if (count($classes) > 0) {
                foreach ($classes as $class) {
                    $results = $results . '<option value="' . $class['group_id'] . '">' . $idx . " - " . $class['group_title'] . '</option>';
                    $idx++;
                }
            }
            $return['results'] = $results;

            return_json($return);
            break;
        case 'list_subject' :
            $school_year = $_POST['school_year'];
            $search_with = $_POST['search_with'];
            $class_level = $classLevelDao->getClassLevel($_POST['class_level_id']);
//            class_id = $_POST['semester'];

                $classLevelSubjects = $subjectDao->getSubjectsByClassLevel($school['page_id'], $class_level['gov_class_level'], $school_year);
                $subjectCurrentOfClassLevel = array();
                // Lọc lấy id môn
                foreach ($classLevelSubjects as $classLevelSubject) {
                    $subjectCurrentOfClassLevel[] = $classLevelSubject['subject_id'];
                }
                $lastSubjects = array();

                $results = "<option value=''>" . __("Select subject") . "...</option>";
                $idx = 1;
                foreach ($classLevelSubjects as $subject) {
                    $results = $results . '<option value="' . $subject['subject_id'] . '">' . $idx . " - " . $subject['subject_name'] . '</option>';
                    $idx++;
                }
            $return['results'] = $results;

            return_json($return);
            break;
        case 'search_course':
            try {
                $result = array();

                if (!is_numeric($_POST['class_level_id'])) {
                    throw new Exception(__("You must choose a classlevel"));
                }
                if (!is_numeric($_POST['class_id'])) {
                    throw new Exception(__("You must choose a classlevel"));
                }
                $school_id = $schoolDao->getSchoolByUsername($_POST['school_username']);
                $url = 'https://elearning.mascom.com.vn/api/course/get/list';
                $data = array( 'secret' => '7402b1e6-933d-42eb-aff0-acceb5ccd13e',
                    'userId' => '',
                    'schoolId' => $school_id,
                    'classLevelId' => $_POST['class_level_id'],
                    'groupId' => $_POST['class_id'],
                    'limit' => '100',
                    'offset' => '0');
                $data = http_build_query($data);
//                $data_string = json_encode($data);
                $curl = curl_init();
                curl_setopt($curl, CURLOPT_URL, $url);
                curl_setopt($curl, CURLOPT_POST, 1);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 5.1; rv:5.0) Gecko/20100101 Firefox/5.0 Firefox/5.0');
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($curl, CURLOPT_TIMEOUT, 30);
                curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
                curl_setopt($curl, CURLOPT_MAXREDIRS, 10);
                curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
                $contents = curl_exec($curl);
                $status = curl_getinfo($curl);
                curl_close($curl);
                if ($status['http_code'] == 200) {
                    $contents = json_decode($contents, true);
                    if ($contents['error']) {
                        throw new Exception($contents['error']['message'] . ' Error Code #' . $contents['error']['code']);
                    }
                } else {
                    throw new Exception("Error Processing Request");
                }

                $smarty->assign('username', $_POST['school_username']);
                $smarty->assign('result', $contents['course']);
//                $smarty->assign('canEdit', canEdit($_POST['school_username'], 'courses'));
                $return['results'] = $smarty->fetch("ci/school/ajax.courselist.tpl");
                // return & exit
                return_json($return);
            } catch (Exception $e) {
                $db->rollback();
                return_json(array('error' => true, 'message' => $e->getMessage()));
            } finally {
                $db->autocommit(true);
            }
            break;
        default:
            _error(400);
            break;
    }

} catch (Exception $e) {
    $db->rollback();
    return_json(array('error' => true, 'message' => $e->getMessage()));
} finally {
    $db->autocommit(true);
}

?>