<?php
/**
 * Package: ajax/ci/bo/school
 * 
 * @package ConIu
 * @author QuanND
 */

// fetch bootstrap
require('../../../../../bootstrap.php');

// check AJAX Request
is_ajax();
check_login();

if(is_empty($_POST['school_username']) || !valid_username($_POST['school_username'])) {
    _error(404);
}

include_once(DAO_PATH.'dao_school.php');
include_once(DAO_PATH.'dao_class.php');
include_once(DAO_PATH.'dao_tuition.php');
include_once(DAO_PATH.'dao_child.php');
include_once(DAO_PATH.'dao_parent.php');
include_once(DAO_PATH.'dao_user.php');
include_once(DAO_PATH.'dao_teacher.php');
include_once(DAO_PATH.'dao_service.php');
include_once(DAO_PATH.'dao_attendance.php');
$schoolDao = new SchoolDAO();
$classDao = new ClassDAO();
$tuitionDao = new TuitionDAO();
$childDao = new ChildDAO();
$parentDao = new ParentDAO();
$userDao = new UserDAO();
$teacherDao = new TeacherDAO();
$serviceDao = new ServiceDAO();
$attendanceDao = new AttendanceDAO();

//$school = getSchool($_POST['school_username']);
$school = getSchoolDataByUsername($_POST['school_username'], SCHOOL_INFO);
if (is_null($school)) {
    _error(403);
}

try {
    $return = array();
    $db->autocommit(false);
    switch ($_POST['do']) {
        case 'edit_service':
            if (!canEdit($_POST['school_username'], 'services')) {
                _error(403);
            }
            if(!isset($_POST['service_id']) || !is_numeric($_POST['service_id'])) {
                _error(400);
            }
            $args = array();
            $args['service_id'] = $_POST['service_id'];
            $args['service_name'] = $_POST['service_name'];
            $args['type'] = $_POST['type'];
            //$args['must_register'] = $_POST['must_register'];
            $args['status'] = (isset($_POST['status']) && $_POST['status'] == 'on')? 1: 0;
            $args['is_food'] = (isset($_POST['is_food']) && $_POST['is_food'] == 'on')? 1: 0;
            $args['fee'] = str_replace(',', '', $_POST['fee']);
            $args['description'] = $_POST['description'];
            $args['parent_display'] = (isset($_POST['parent_display']) && $_POST['parent_display'] == 'on')? 1: 0;

            $db->begin_transaction();
            $serviceDao->updateService($args);
            $db->commit();

            /* ---------- UPDATE - MEMCACHE ---------- */
            //1.Cập nhật dịch vụ trong trường
            /*$arr_update = array(
                $args['service_id'] => $args
            );*/
            updateSchoolData($school['page_id'], SCHOOL_SERVICES);
            /* ---------- END - MEMCACHE ---------- */

            //return_json( array('success' => true, 'message' => __("Done, Service info have been updated")) );
            return_json(array('callback' => 'window.location = "'.$system['system_url'].'/school/'.$_POST['school_username'].'/services";'));
            break;
        case 'add_service':
            if (!canEdit($_POST['school_username'], 'services')) {
                _error(403);
            }
            $args = array();

            $args['service_name'] = $_POST['service_name'];
            $args['type'] = $_POST['type'];
            //$args['must_register'] = $_POST['must_register'];
            $args['status'] = (isset($_POST['status']) && $_POST['status'] == 'on')? 1: 0;
            $args['is_food'] = (isset($_POST['is_food']) && $_POST['is_food'] == 'on')? 1: 0;
            $args['fee'] = str_replace(',', '', $_POST['fee']);
            $args['school_id'] = $school['page_id'];
            $args['description'] = $_POST['description'];
            $args['status'] = STATUS_ACTIVE;
            $args['parent_display'] = $_POST['parent_display'];

            $db->begin_transaction();
            $args['service_id'] = $serviceDao->insertService($args);
            $db->commit();

            /* ---------- UPDATE - MEMCACHE ---------- */
            //1.Cập nhật dịch vụ trong trường
            updateSchoolData($school['page_id'], SCHOOL_SERVICES);
            /* ---------- END - MEMCACHE ---------- */

            return_json(array('callback' => 'window.location = "'.$system['system_url'].'/school/'.$_POST['school_username'].'/services";'));
            break;
        case 'del_service':
            if (!canEdit($_POST['school_username'], 'services')) {
                _error(403);
            }
            $db->begin_transaction();
            $serviceDao->deleteService($_POST['service_id']);
            $db->commit();

            /* ---------- UPDATE - MEMCACHE ---------- */
            //1.Xóa dịch vụ trong trường
            deleteSchoolData($school['page_id'], SCHOOL_SERVICES, $_POST['service_id']);
            /* ---------- END - MEMCACHE ---------- */

            return_json( array('success' => true, 'message' => __("Success")));
            break;
        case 'cancel_service':
            if (!canEdit($_POST['school_username'], 'services')) {
                _error(403);
            }
            $db->begin_transaction();
            $serviceDao->InactiveService($_POST['service_id']);
            $db->commit();

            /* ---------- UPDATE - MEMCACHE ---------- */
            //1.Xóa dịch vụ trong trường
            deleteSchoolData($school['page_id'], SCHOOL_SERVICES, $_POST['service_id']);
            /* ---------- END - MEMCACHE ---------- */

            return_json( array('success' => true, 'message' => __("Success")));
            break;
        case 'del_mdservice_registration':
            $inputCorrect = true;
            if (!isset($_POST['service_id']) || ($_POST['service_id'] <= 0)) {
                $inputCorrect = false;
            }
            if (!isset($_POST['child_id']) || ($_POST['child_id'] == '')) {
                $inputCorrect = false;
            }
            if ($inputCorrect) {
                $db->begin_transaction();
                $serviceDao->deleteServiceRegistration($_POST['service_id'], $_POST['child_id']);
                $db->commit();
            }
            //CHỖ NÀY KHÔNG CÓ BREAK ĐỂ CHẠY ĐOẠN DƯỚI LUÔN. KHÔNG ĐƯỢC CHÈN CODE VÀO GIỮA 2 CASE NÀY
        case 'list_child_4regmdservice': //Liệt kê danh sách trẻ trong màn hình đăng ký dịch vụ THÁNG/ĐIỂM DANH
            $return = array();

            //Validate thông tin đầu vào
            $inputCorrect = true;
            if (!isset($_POST['service_id']) || ($_POST['service_id'] <= 0)) {
                $inputCorrect = false;
            }
            if ($inputCorrect) {
                $results = $serviceDao->getServiceChild($school['page_id'], $_POST['service_id'], $_POST['class_id']);
                $smarty->assign('username', $_POST['school_username']);
                $smarty->assign('class_id', $_POST['class_id']);
                $smarty->assign('results', $results);
                $return['results'] = $smarty->fetch("ci/school/ajax.service.listchild.4regmdservice.tpl");
                $return['disableSave'] = (count($results['children']) == 0);
            } else {
                $return['results'] = __("No data or incorrect input data");
                $return['disableSave'] = true;
            }

            return_json($return);

            break;
        case 'list_child_4deduction': //Hiển thị danh sách trẻ trong chức năng GHI CHÚ GIẢM TRỪ
            $return = array();
            //Validate thông tin đầu vào
            $inputCorrect = true;
            if (!isset($_POST['service_id']) || ($_POST['service_id'] <= 0)) {
                $inputCorrect = false;
            }
            if (!isset($_POST['class_id']) || ($_POST['class_id'] <= 0)) {
                $inputCorrect = false;
            }
            if (!isset($_POST['deduction_date']) || ($_POST['deduction_date'] == '')) {
                $inputCorrect = false;
            }
            if ($inputCorrect) {
                $results = $serviceDao->getServiceChild4Deduction($_POST['class_id'], $_POST['service_id'], $_POST['deduction_date']);
                $smarty->assign('username', $_POST['school_username']);
                $smarty->assign('class_id', $_POST['class_id']);
                $smarty->assign('results', $results);
                $return['results'] = $smarty->fetch("ci/school/ajax.service.listchild.4deduction.tpl");
                $return['disableSave'] = (count($results) == 0);
            } else {
                $return['results'] = __("No data or incorrect input data");
                $return['disableSave'] = true;
            }

            return_json($return);
            break;
        case 'list_child_4record': //Lưu đăng ký dịch vụ theo SỐ LẦN
            $return = array();

            //Validate thông tin đầu vào
            $inputCorrect = true;
            if (!isset($_POST['service_id']) || ($_POST['service_id'] <= 0)) {
                $inputCorrect = false;
            }
            if (!isset($_POST['using_at']) || ($_POST['using_at'] == '')) {
                $inputCorrect = false;
            }
            if ($inputCorrect) {
                //$results = $serviceDao->getServiceChild4Record($school['page_id'], $_POST['service_id'], $_POST['using_at'], $_POST['class_id']);
                $results = $serviceDao->getSchoolChildService4Record($school['page_id'], $_POST['service_id'], $_POST['using_at'], $_POST['class_id']);
                $absentChildIds = $attendanceDao->getAbsentChildIds($school['page_id'], $_POST['using_at'], $_POST['class_id']);

                $smarty->assign('username', $_POST['school_username']);
                $smarty->assign('service_id', $_POST['service_id']);
                $smarty->assign('class_id', $_POST['class_id']);
                $smarty->assign('absent_child_ids', $absentChildIds);
                $smarty->assign('results', $results);
                $return['results'] = $smarty->fetch("ci/school/ajax.service.childlist4record.tpl");
                $return['disableSave'] = (count($results['children']) == 0);
            } else {
                $return['results'] = __("No data or incorrect input data");
                $return['disableSave'] = true;
            }
            return_json($return);
            break;
        case 'register': //Lưu dăng ký dịch vụ theo THÁNG/ĐIỂM DANH
            if (!canEdit($_POST['school_username'], 'services')) {
                _error(403);
            }

            $allChildIds = isset($_POST['allChildIds'])? $_POST['allChildIds']: array();
            $registerChildIds = isset($_POST['childIds'])? $_POST['childIds']: array(); //ID của trẻ đăng ký sử dụng dịch vụ
            $oldChildIds = isset($_POST['oldChildIds'])? $_POST['oldChildIds']: array(); //ID của trẻ đã đăng ký sử dụng trước đó

            //Danh sách trẻ đăng ký mới sử dụng dịch vụ
            $newChildIds = array_diff($registerChildIds, $oldChildIds);
            $newChildren = array();
            foreach ($newChildIds as $id) {
                $child = array();
                $child['child_id'] = $id;
                $child['begin'] = $_POST['begin_'.$id];
                $child['end'] = '';
                $newChildren[] = $child;
            }

            //Danh sách trẻ hủy sử dụng dịch vụ
            $cancelChildIds = array_diff($oldChildIds, $registerChildIds);
            $cancelChildren = array();
            foreach ($cancelChildIds as $id) {
                $child = array();
                $child['child_id'] = $id;
                $child['begin'] = $_POST['begin_'.$id];
                $child['end'] = $_POST['end_'.$id];
                $cancelChildren[] = $child;
            }

            //Kiểm tra phần còn lại của danh sách để xem có thay đổi begin, end không
            $remainingChildIds = array_diff($allChildIds, $newChildIds, $cancelChildIds);
            $changedChildren = array();
            foreach ($remainingChildIds as $id) {
                if (in_array($id, $registerChildIds) && ($_POST['begin_'.$id] != $_POST['old_begin_'.$id])) {
                    //Nếu vẫn trạng thái đăng ký nhưng thay đổi BEGIN thì cập nhật lại.
                    $child = array();
                    $child['status'] = 1;
                    $child['child_id'] = $id;
                    $child['begin'] = $_POST['begin_'.$id];
                    $child['end'] = '';

                    $changedChildren[] = $child;
                } else if ((!in_array($id, $registerChildIds)) && isset($_POST['old_end_'.$id]) && ($_POST['old_end_'.$id] != '')) {
                    //Nếu không trong danh sách đăng ký, MÀ đã từng đăng ký và có thay đổi BEGIN .
                    $child = array();
                    $child['status'] = 0;
                    $child['child_id'] = $id;
                    $child['begin'] = $_POST['begin_'.$id];
                    $child['end'] = $_POST['end_'.$id];

                    $changedChildren[] = $child;
                }
            }

            if ((count($newChildIds) + count($cancelChildIds) + count($remainingChildIds)) > 0) {
                $db->begin_transaction();
                $serviceDao->registerService($_POST['service_id'], $newChildIds, $newChildren, $cancelChildren, $changedChildren);

                $service = $serviceDao->getServiceById($_POST['service_id']);

                if (count($cancelChildIds) > 0) {
                    //Thông báo tới phụ huynh có trẻ huỷ sử dụng dịch vụ.
                    $parents = $childDao->getListChildNParentId($cancelChildIds);
                    foreach ($parents as $parent) {
                        $userDao->postNotifications($parent['parent_id'], NOTIFICATION_SERVICE_CANCEL_UNCOUNTBASED, NOTIFICATION_NODE_TYPE_CHILD,
                            $_POST['service_id'], convertText4Web($service['service_name']), $parent['child_id'], convertText4Web($parent['child_name']));
                    }
                    //Thông báo tới giáo viên của lớp.
                    foreach ($cancelChildIds as $id) {
                        $teachers = $teacherDao->getTeacherIdClassBySchoolAndChild($school['page_id'], $id);
                        $childName = $childDao->getChildName($id);
                        foreach ($teachers as $teacher) {
                            $userDao->postNotifications($teacher['teacher_id'], NOTIFICATION_SERVICE_CANCEL_UNCOUNTBASED, NOTIFICATION_NODE_TYPE_CLASS,
                                $_POST['service_id'], convertText4Web($service['service_name']), $teacher['class_name'], convertText4Web($childName));
                        }
                        // Thông báo đến các quản lý trường khác
                        $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'services', $school['page_admin']);
                        $userDao->postNotifications($userManagerIds, NOTIFICATION_SERVICE_CANCEL_UNCOUNTBASED, NOTIFICATION_NODE_TYPE_SCHOOL,
                            $_POST['service_id'], convertText4Web($service['service_name']), $school['page_name'], convertText4Web($childName));
                    }
                }

                //Thông báo tới phụ huynh có trẻ đăng ký sử dụng dịch vụ.
                if (count($newChildIds) > 0) {
                    $parents = $childDao->getListChildNParentId($newChildIds);
                    foreach ($parents as $parent) {
                        $userDao->postNotifications($parent['parent_id'], NOTIFICATION_SERVICE_REGISTER_UNCOUNTBASED, NOTIFICATION_NODE_TYPE_CHILD,
                            $_POST['service_id'], convertText4Web($service['service_name']), $parent['child_id'], convertText4Web($parent['child_name']));
                    }
                    //Thông báo tới giáo viên của lớp.
                    foreach ($newChildIds as $id) {
                        $teachers = $teacherDao->getTeacherIdClassBySchoolAndChild($school['page_id'], $id);
                        $childName = $childDao->getChildName($id);
                        foreach ($teachers as $teacher) {
                            $userDao->postNotifications($teacher['teacher_id'], NOTIFICATION_SERVICE_REGISTER_UNCOUNTBASED, NOTIFICATION_NODE_TYPE_CLASS,
                                $_POST['service_id'], convertText4Web($service['service_name']), $teacher['class_name'], convertText4Web($childName));
                        }
                        // Thông báo đến các quản lý trường khác
                        $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'services', $school['page_admin']);
                        $userDao->postNotifications($userManagerIds, NOTIFICATION_SERVICE_REGISTER_UNCOUNTBASED, NOTIFICATION_NODE_TYPE_SCHOOL,
                            $_POST['service_id'], convertText4Web($service['service_name']), $school['page_name'], convertText4Web($childName));
                    }
                }

                $db->commit();
                return_json(array('success' => true, 'message' => __("Done, Registration info have been updated")));
            } else {
                return_json(array('success' => true, 'message' => __("Registration info has no change")));
            }
            break;
        case 'deduction':
            if (!canEdit($_POST['school_username'], 'services')) {
                _error(403);
            }
            $inputCorrect = true;
            if (!isset($_POST['service_id']) || ($_POST['service_id'] <= 0)) {
                $inputCorrect = false;
            }
            if (!isset($_POST['deduction_date']) || ($_POST['deduction_date'] == '')) {
                $inputCorrect = false;
            }
            if ($inputCorrect) {
                $deductionChildIds = isset($_POST['childIds']) ? $_POST['childIds'] : array(); //ID của trẻ được giảm trừ
                $oldChildIds = isset($_POST['oldChildIds']) ? $_POST['oldChildIds'] : array(); //ID của trẻ đã được giảm trừ trước đó.

                //Danh sách trẻ giảm trừ mới.
                $newChildIds = array_diff($deductionChildIds, $oldChildIds);
                //Danh sách trẻ hủy giảm trừ
                $cancelChildIds = array_diff($oldChildIds, $deductionChildIds);

                if ((count($newChildIds) + count($cancelChildIds)) > 0) {
                    $db->begin_transaction();
                    $serviceDao->recordServiceService($_POST['service_id'], $_POST['deduction_date'], $newChildIds, $cancelChildIds);

                    $db->commit();
                    return_json(array('success' => true, 'message' => __("Service deduction info have been updated")));
                } else {
                    return_json(array('success' => true, 'message' => __("Service deduction info has no change")));
                }
            } else {
                return_json(array('error' => true, 'message' => __("Your input data is incorrect")));
            }
            break;
        case 'record': //Lưu đăng ký sử dụng dịch vụ theo SỐ LẦN
            if (!canEdit($_POST['school_username'], 'services')) {
                _error(403);
            }
            $registerChildIds = isset($_POST['childIds'])? $_POST['childIds']: array(); //ID của trẻ sử dụng dịch vụ
            //$allChildIds = $_POST['allChildIds']; //ID của tất cả trẻ trong danh sách
            $oldChildIds = isset($_POST['oldChildIds'])? $_POST['oldChildIds']: array(); //ID của trẻ đã sử dụng dịch vụ ở lần ghi trước

            //Danh sách trẻ sử dụng dịch vụ mới (so với lần lưu trước)
            $newChildIds = array_diff($registerChildIds, $oldChildIds);
            //Danh sách trẻ bỏ sử dụng dịch vụ (do lần trước ghi nhầm)
            $cancelChildIds = array_diff($oldChildIds, $registerChildIds);

            if ((count($newChildIds) + count($cancelChildIds)) > 0) {
                $db->begin_transaction();
                $serviceDao->recordServiceUsage($_POST['service_id'], $newChildIds, $cancelChildIds, $_POST['using_at']);
                $service = $serviceDao->getServiceById($_POST['service_id']);

                if (count($cancelChildIds) > 0) {
                    //Thông báo tới phụ huynh có trẻ huỷ sử dụng dịch vụ.
                    $parents = $childDao->getListChildNParentId($cancelChildIds);
                    foreach ($parents as $parent) {
                        $userDao->postNotifications($parent['parent_id'], NOTIFICATION_SERVICE_CANCEL_COUNTBASED, NOTIFICATION_NODE_TYPE_CHILD,
                            $_POST['service_id'], convertText4Web($service['service_name']), $parent['child_id'], convertText4Web($parent['child_name']));
                    }
                    //Thông báo tới giáo viên của lớp.
                    foreach ($cancelChildIds as $id) {
                        $teachers = $teacherDao->getTeacherIdClassBySchoolAndChild($school['page_id'], $id);
                        $childName = $childDao->getChildName($id);
                        foreach ($teachers as $teacher) {
                            $userDao->postNotifications($teacher['teacher_id'], NOTIFICATION_SERVICE_CANCEL_COUNTBASED, NOTIFICATION_NODE_TYPE_CLASS,
                                $_POST['service_id'], convertText4Web($service['service_name']), $teacher['class_name'], convertText4Web($childName));
                        }
                    }
                }

                //Thông báo tới phụ huynh có trẻ đăng ký sử dụng dịch vụ.
                if (count($newChildIds) > 0) {
                    $parents = $childDao->getListChildNParentId($newChildIds);
                    foreach ($parents as $parent) {
                        $userDao->postNotifications($parent['parent_id'], NOTIFICATION_SERVICE_REGISTER_COUNTBASED, NOTIFICATION_NODE_TYPE_CHILD,
                            $_POST['service_id'], convertText4Web($service['service_name']), $parent['child_id'], convertText4Web($parent['child_name']));
                    }
                    //Thông báo tới giáo viên của lớp.
                    foreach ($newChildIds as $id) {
                        $teachers = $teacherDao->getTeacherIdClassBySchoolAndChild($school['page_id'], $id);
                        $childName = $childDao->getChildName($id);
                        foreach ($teachers as $teacher) {
                            $userDao->postNotifications($teacher['teacher_id'], NOTIFICATION_SERVICE_REGISTER_COUNTBASED, NOTIFICATION_NODE_TYPE_CLASS,
                                $_POST['service_id'], convertText4Web($service['service_name']), $teacher['class_name'], convertText4Web($childName));
                        }
                    }
                }

                $db->commit();
                return_json(array('success' => true, 'message' => __("Done, Service usage info have been updated")));
            } else {
                return_json(array('success' => true, 'message' => __("Service usage info has no change")));
            }
            break;
        case 'list_child': //Lấy danh sách trẻ trong màn hình Lịch sử sử dụng dịch vụ. Điền vào combobox
            $return = array();
            if(!isset($_POST['class_id']) || !is_numeric($_POST['class_id'])) {
                _error(404);
            }

            //$children = $childDao->getChildrenOfClass($_POST['class_id']);
            //$children = sortArray($children, "DESC", "child_status");
            $children = $childDao->getChildrenOfClass4ComboBox($_POST['class_id']);
            $children = sortMultiOrderArray($children, [["child_status", "DESC"], ["name_for_sort", "ASC"]]);

            $results = "<option value=''>".__("Whole class")."</option>";
            $child_status = -1;
            $idx = 1;
            foreach ($children as $child) {
                // bỏ trẻ thôi học
//                if (($child_status >= 0) && ($child_status != $child['child_status'])) {
//                    $results = $results. '<option value="" disabled style="color: #31a1ff">----------'.__("Left children").'----------</option>';
//                }
                if($child['child_status'] > 0) {
                    $results = $results.'<option value="'.$child['child_id'].'">'.($idx++)." - ".$child['child_name'].' - '.$child['birthday'].'</option>';
                }
                $child_status = $child['child_status'];
            }
            $return['results'] = $results;
            return_json($return);

            break;
        case 'history': //Lấy thông tin lịch sử sử dụng dịch vụ
            $return = array();

            $class_id = (!isset($_POST['class_id']) || ($_POST['class_id'] <= 0))? 0: $_POST['class_id'];
            $child_id = (!isset($_POST['child_id']) || ($_POST['child_id'] <= 0))? 0: $_POST['child_id'];
            $service_id = (!isset($_POST['service_id']) || ($_POST['service_id'] <= 0))? 0: $_POST['service_id'];
            $service_type = (!isset($_POST['service_type']) || ($_POST['service_type'] <= 0))? 0: $_POST['service_type'];
            $smarty->assign('class_id', $class_id);
            $smarty->assign('service_id', $service_id);
            $smarty->assign('service_type', $service_type);
            $smarty->assign('child_id', $child_id);

            $begin = isset($_POST['begin'])? $_POST['begin']: (date($system['date_format'], mktime(0, 0, 0, date("m"), 1, date("Y"))));
            $end = isset($_POST['end'])? $_POST['end']: (toSysDate($date));

            if (($service_id > 0) || ($child_id > 0)) {
                if ($service_type == SERVICE_TYPE_COUNT_BASED) {
                    //Lấy ra thông tin sử dụng dịch vụ theo số lần
                    $history = $serviceDao->getServiceHistorySchool($school['page_id'], $class_id, $child_id, $_POST['service_id'], $begin, $end);
                    $smarty->assign('history', $history);
                } elseif ($service_type > 0) {
                    //Lấy ra thông tin sử dụng MỘT dịch vụ theo THÁNG hoặc theo ĐIỂM DANH
                    $history = $serviceDao->getNotCountBasedServiceChild($school['page_id'], $_POST['service_id'], $begin, $end, $class_id, $child_id);
                    $smarty->assign('history', $history);
                } else {
                    //Trường hợp chọn trẻ mà không chọn dịch vụ. Search tất cả dịch vụ mà trẻ đó sử dụng.
                    $notCountBasedServices = $serviceDao->getAllNotCountBasedServiceAChild($school['page_id'], $child_id, $begin, $end);
                    $countBasedServices = $serviceDao->getCountBasedServiceAChild($school['page_id'], $child_id, $begin, $end);
                    $smarty->assign('ncbs_usage', $notCountBasedServices);
                    $smarty->assign('cbs_usage', $countBasedServices);
                }
            } else {
                if ($class_id > 0) {
                    //Lấy ra thông tin tổng hợp sử dụng dịch của LỚP
                    $history = $serviceDao->getServiceUsageSummaryClass($school['page_id'], $class_id, $begin, $end);
                } else {
                    //Lấy ra thông tin tổng hợp sử dụng dịch của CẢ TRƯỜNG
                    $history = $serviceDao->getServiceUsageSummarySchool($school['page_id'], $begin, $end);
                }
                $smarty->assign('history', $history);
            }

            $return['results'] = $smarty->fetch("ci/school/ajax.service.history.tpl");

            return_json($return);

            break;
        case 'class':
            //Thống kê tất cả dịch vụ theo SỐ LẦN của một lớp trong một ngày.
            $return = array();

            $class_id = $_POST['class_id'];
            //$class = $classDao->getClass($class_id);
            $class = getClassData($class_id, CLASS_INFO);
            $classTitle = $class['group_title'];
            $smarty->assign('class_id', $class_id);

            $date_pos = isset($_POST['date'])? $_POST['date']: (date($system['date_format'], mktime(0, 0, 0, date("m"), 1, date("Y"))));

            $childList = array();
            if ($class_id > 0) {
                // Lấy danh sách child của lớp
                $children = $childDao->getChildrenOfClass($class_id, $date_pos);

                // Lấy ra tất cả dịch vụ theo số lần sử dụng của trường và danh sách trẻ
                //$servicesCountBase = $serviceDao->getAllServiceCBForAllChildOfClass($date, $childrenIds);
                // Lặp danh sách trẻ của lớp
                foreach ($children as $child) {
                    // Lấy danh sách dịch vụ theo số lần sử dụng của mỗi trẻ
                    $services = $serviceDao->getChildService4Record($school['page_id'], $child['child_id'], $date_pos);
                    $child['services'] = $services;
                    $childList[] = $child;
                }

            }
            // Lấy danh sách dịch vụ theo số lần sử dụng của trường
            $servicesCB = array();
            $services = getSchoolData($school['page_id'], SCHOOL_SERVICES);
            foreach ($services as $service) {
                if($service['type'] == SERVICE_TYPE_COUNT_BASED && $service['status'] == STATUS_ACTIVE) {
                    $servicesCB[] = $service;
                }
            }
            // Lấy tổng hợp sử dụng dịch vụ theo số lần sử dụng của lớp
            $history = $serviceDao->getServiceUsageSummaryClass($school['page_id'], $class_id, $date_pos, $date_pos);

            //$smarty->assign('servicesCountBase', $servicesCountBase);
            $smarty->assign('childList', $childList);
            $smarty->assign('classTitle', $classTitle);
            $smarty->assign('date_pos', $date_pos);
            $smarty->assign('servicesCB', $servicesCB);
            $smarty->assign('history', $history);

            $return['results'] = $smarty->fetch("ci/school/ajax.service.class.tpl");

            return_json($return);
            break;
        case 'foodsvr': //Hiển thị danh sách dịch vụ ĂN UỐNG
            $foodServiceList = $serviceDao->getFoodServiceSchool($school['page_id']);
            //1. Liệt kê danh sách điểm danh toàn trường của ngày HÔM NAY, sort theo lớp
            $presentChildIds = $attendanceDao->getPresentChildrenOfSchoolADay($school['page_id'], $_POST['date']);
            $classes = $presentChildIds['classes'];

            if ((count($foodServiceList) > 0) && (count($classes) > 0)) {
                //2. Liệt kê danh sách đăng ký dịch vụ ĂN UỐNG theo tháng và theo điểm danh, sort theo lớp
                $notCountBasedServices = $serviceDao->getFoodNotCountBasedServiceSchoolADay($school['page_id'], $presentChildIds['childIds'], $foodServiceList);
                //3. Liệt kê danh sách đăng ký dịch vụ ĂN UỐNG theo lần
                $countBasedServices = $serviceDao->getFoodCountBasedServiceSchoolADay($school['page_id'], $_POST['date'], $presentChildIds['childIds'], $foodServiceList);

                $totalServices = array();
                foreach ($foodServiceList as $service) {
                    $totalServices["'" . $service['service_id'] . "'"] = 0;
                }

                //4. Ghép tất mỗi loại dịch vụ vào danh sách lớp để hiện ra màn hình.
                $newClasses = array();
                foreach ($classes as $class) {
                    $newClass = $class;
                    $serviceFood = array();
                    foreach ($notCountBasedServices as $notCountBasedService) {
                        if ($class['class_id'] == $notCountBasedService['class_id']) {
                            $newClass['not_countbased_services'] = $notCountBasedService['not_countbased_services'];

                            //Tính tổng đối với từng dịch vụ
                            foreach ($notCountBasedService['not_countbased_services'] as $service) {
                                $totalServices["'" . $service['service_id'] . "'"] = $totalServices["'" . $service['service_id'] . "'"] + $service['total'];
                            }
                            foreach ($newClass['not_countbased_services'] as $value) {
                                $serviceFood[] = $value;
                            }
                            break;
                        }
                    }
                    foreach ($countBasedServices as $countBasedService) {
                        if ($class['class_id'] == $countBasedService['class_id']) {
                            $newClass['countbased_services'] = $countBasedService['countbased_services'];

                            //Tính tổng đối với từng dịch vụ
                            foreach ($countBasedService['countbased_services'] as $service) {
                                $totalServices["'" . $service['service_id'] . "'"] = $totalServices["'" . $service['service_id'] . "'"] + $service['total'];
                            }
                            foreach ($newClass['countbased_services'] as $value) {
                                $serviceFood[] = $value;
                            }
                            break;
                        }
                    }
                    $newClass['food_services'] = $serviceFood;
                    $newClasses[] = $newClass;
                }

                $smarty->assign('classes', $newClasses);
                $smarty->assign('service_list', $foodServiceList);
                $smarty->assign('service_total', $totalServices);
            }
            $return['results'] = $smarty->fetch("ci/school/ajax.service.foodsvr.tpl");

            return_json($return);

            break;
        default:
            _error(400);
            break;
    }
} catch (Exception $e) {
    $db->rollback();
    return_json( array('error' => true, 'message' => $e->getMessage()) );
} finally {
    $db->autocommit(true);
}

?>