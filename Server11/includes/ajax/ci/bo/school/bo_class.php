<?php
/**
 * Package: ajax/ci/bo
 * 
 * @package ConIu
 * @author QuanND
 */

// fetch bootstrap
require('../../../../../bootstrap.php');

// check AJAX Request
is_ajax();
check_login();

if(is_empty($_POST['school_username']) || !valid_username($_POST['school_username'])) {
    _error(404);
}

include_once(DAO_PATH . 'dao_school.php');
include_once(DAO_PATH . 'dao_class.php');
include_once(DAO_PATH . 'dao_teacher.php');
include_once(DAO_PATH . 'dao_user.php');
include_once(DAO_PATH . 'dao_parent.php');
include_once(DAO_PATH . 'dao_child.php');
include_once(DAO_PATH . 'dao_tuition.php');
include_once(DAO_PATH . 'dao_service.php');
include_once(DAO_PATH . 'dao_role.php');

$schoolDao = new SchoolDAO();
$classDao = new ClassDAO();
$teacherDao = new TeacherDAO();
$userDao = new UserDAO();
$parentDao = new ParentDAO();
$childDao = new ChildDAO();
$tuitionDao = new TuitionDAO();
$serviceDao = new ServiceDAO();
$roleDao = new RoleDAO();

//$school = getSchool($_POST['school_username']);
$school = getSchoolDataByUsername($_POST['school_username'], SCHOOL_INFO);
if (!canEdit($_POST['school_username'], 'classes')) {
    _error(403);
}

$args = array();
$args['title'] = $_POST['title'];
$args['class_id'] = $_POST['class_id'];
//$args['username'] = $_POST['username'];
$args['telephone'] = $_POST['telephone'];
$args['email'] = $_POST['email'];
$args['camera_url'] = isset($_POST['camera_url']) ? $_POST['camera_url'] : 'null';
$args['description'] = $_POST['description'];
$args['class_level_id'] = $_POST['class_level_id'];
$args['user_id'] = $user->_data['user_id'];
$args['school_id'] = $school['page_id'];

try {
    $db->autocommit(false);
    switch ($_POST['do']) {
        case 'edit':
            $db->begin_transaction();
            // valid inputs
            if(!isset($_POST['class_id']) || !is_numeric($_POST['class_id'])) {
                _error(400);
            }
            //$class = $classDao->getClass($_POST['class_id']);
            $class = getClassData($_POST['class_id'], CLASS_INFO);
            if (is_null($class)) {
                _error(404);
            }

            //1. Cập nhật thông tin lớp
            //$args['username_old'] = $class['group_name'];
            $args['group_id'] = $_POST['class_id'];
            $classDao->editClass($args);

            //2. Cập nhật danh sách giáo viên của lớp
            $teacherIds = isset($_POST['user_id']) ? $_POST['user_id'] : array();

            //$oldTeacherIds = $teacherDao->getTeacherIDOfClass($_POST['class_id']);
            $oldTeachers = getClassData($_POST['class_id'], CLASS_TEACHERS);
            $oldTeacherIds = array_keys($oldTeachers);

            //Xóa những giáo viên không còn trong danh sách mới
            $deletedTeacherIds = array_diff($oldTeacherIds, $teacherIds);
            if (count($deletedTeacherIds) > 0) {
                $teacherDao->deleteTeacherList($_POST['class_id'], $deletedTeacherIds);
                //2.1. Xoa giao vien khoi thanh vien nhom
                // Kiểm tra xem giáo viên có phải hiệu trưởng không, nếu là hiệu trưởng thì không loại khỏi group lớp
                $principals = array();
                $schoolConfig = getSchoolData($school['page_id'], SCHOOL_CONFIG);
                if($schoolConfig['principal'] != '') {
                    $principals = explode(",", $schoolConfig['principal']);
                }
                $deletedTeacherIdsGroup = array_diff($deletedTeacherIds, $principals);

                // Kiểm tra xem có được phân quyền quản lý group không, nếu được phân quyền thì không loại khỏi group
                $newManageUserIds = $roleDao->getUserIdsOfModule($school['page_id'], 'managegroups');
                $deletedTeacherIdsGroup = array_diff($deletedTeacherIdsGroup, $newManageUserIds);
                if(count($deletedTeacherIdsGroup) > 0) {
                    $classDao->deleteUserFromClass($_POST['class_id'], $deletedTeacherIdsGroup);
                }
                //Xóa giáo viên thì ko cần unfriend với phụ huynh
            }
            //Nhập những giáo viên mới được bổ xung so với danh sách cũ
            $newTeacherIds = array_diff($teacherIds, $oldTeacherIds);
            if (count($newTeacherIds) > 0) {
                $teacherDao->insertTeacherList($_POST['class_id'], $newTeacherIds);
                //3. Them moi giao vien vao thanh vien nhom
                $classDao->addUserToClass($_POST['class_id'], $newTeacherIds);

                //4. Thông báo giáo viên mới về việc được phân công lớp
                $userDao->postNotifications($newTeacherIds, NOTIFICATION_ASSIGN_CLASS, NOTIFICATION_NODE_TYPE_CLASS, $_POST['class_id']);

                //6. Kết bạn giữa giáo viên và phụ huynh
                // Tạm thời không cho kết bạn giữa giáo viên, phụ huynh
                /*$parentIds = $parentDao->getParentIdOfClass($_POST['class_id']);
                if (count($parentIds) > 0) {
                    $userDao->becomeFriends($newTeacherIds, $parentIds);
                }*/
                // Lấy danh sách trẻ của lớp
                //$children = $childDao->getChildrenOfClass($_POST['class_id']);
                //Memcache
                $children = getClassData($_POST['class_id'], CLASS_CHILDREN);
                foreach ($children as $child) {
                    //$parents = $parentDao->getParent($child['child_id']);
                    $parents = getChildData($child['child_id'], CHILD_PARENTS);
                    $parentIds = array_keys($parents);
                    if(count($parentIds) > 0) {
                        $userDao->postNotifications($parentIds, NOTIFICATION_NEW_TEACHER, NOTIFICATION_NODE_TYPE_CHILD, $_POST['class_id'], $_POST['title'], $child['child_id']);
                    }
                }

                //7. Thông báo phụ huynh về giáo viên mới của lớp
            }
            //8. Giáo viên trở thành bạn của nhau (tạm thời bỏ)
            // $userDao->becomeFriendTogether($teacherIds);
            //9. Cập nhật lại session Class Name
            //$classDao->refreshClassInSession($school['page_id']);

            //10. Thông báo cho các quản lý trường khác lớp được cập nhật
            $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'classes', $school['page_admin']);
            $userDao->postNotifications($userManagerIds, NOTIFICATION_UPDATE_CLASS, NOTIFICATION_NODE_TYPE_SCHOOL,
                $_POST['class_id'], convertText4Web($_POST['title']), $school['page_name'], convertText4Web($school['page_title']));

            $db->commit();

            /* ---------- CLASS - MEMCACHE ---------- */
            //1. Cập nhật lại thông tin lớp và danh sách giáo viên
            updateClassData($args['class_id'], CLASS_INFO);
            updateClassData($args['class_id'], CLASS_TEACHERS);

            //2.Cập nhật lại khối lớp
            updateClassLevelData($args['class_level_id']);

            //3.Cập nhật đối tượng quản lý cho giáo viên
            foreach ($newTeacherIds as $teacherId) {
                addTeacherData($teacherId, TEACHER_CLASSES, $args['class_id']);
                addUserManageData($teacherId, USER_MANAGE_CLASSES, $args['class_id']);
            }
            foreach ($deletedTeacherIds as $teacherId) {
                deleteTeacherData($teacherId, TEACHER_CLASSES, $args['class_id']);
                deleteUserManageData($teacherId, USER_MANAGE_CLASSES, $args['class_id']);
            }
            /* ---------- CLASS - MEMCACHE ---------- */

            //return_json( array('success' => true, 'message' => __("Done, Class info have been updated")) );
            return_json(array('callback' => 'window.location = "'.$system['system_url'].'/school/'.$_POST['school_username'].'/classes";'));
            break;

        case 'setting':
            // valid inputs
            if(!isset($_POST['class_id']) || !is_numeric($_POST['class_id'])) {
                _error(400);
            }
            $args['group_id'] = $_POST['class_id'];
            $db->begin_transaction();
            $classDao->editClass($args);

            /* ---------- CLASS - MEMCACHE ---------- */
            //1. Cập nhật lại thông tin lớp
            updateClassData($_POST['class_id'], CLASS_INFO);
            /* ---------- CLASS - MEMCACHE ---------- */

            $db->commit();

            return_json( array('success' => true, 'message' => __("Done, Class info have been updated")) );
            break;

        case 'add':
            $db->begin_transaction();
            $teacherIds = $_POST['user_id'];
            $teacherIds = isset($teacherIds)? $teacherIds: array();
            $teacherAddGroupIds = $teacherIds;
            $teacherAddGroupIds[] = $user->_data['user_id'];

            //1. Tạo thông tin lớp trong hệ thống
            $lastId = $classDao->createClass($args);

            if (count($teacherIds) > 0) {
                //2. Cập nhật danh sách giáo viên của lớp
                $teacherDao->insertTeacherList($lastId, $teacherIds);
                //3. Đưa tất cả giáo viên join vào group luôn
                $classDao->addUserToClass($lastId, $teacherAddGroupIds);

                //3.5: Cho giáo viên vào bảng groups_admins
                // 3.5.1: Lấy danh sách hiệu trưởng của trường
//                $schoolConfig = getSchoolData($school['page_id'], SCHOOL_CONFIG);
//                if($schoolConfig['principal'] != '') {
//                    $principals = explode(',', $schoolConfig['principal']);
//                }
//                $teacherIds = array_merge($principals, $teacherIds);
//                // 3.5.2: Lấy danh sách những người được phân quyền quản lý group
//                $classDao->addUserToGroupAdmin($lastId, $teacherIds);
                // Đoạn trên này định làm mà thấy phức tạp quá, chuyển qua check điều kiện cho dễ - Taila

                //4. Thông báo giáo viên mới về việc được phân công lớp
                $userDao->postNotifications($teacherIds, NOTIFICATION_ASSIGN_CLASS, NOTIFICATION_NODE_TYPE_CLASS, $lastId);

                //5. Các giáo viên trong 1 lớp sẽ thành bạn của nhau
                $userDao->becomeFriendTogether($teacherIds);
            }
            //6. Thông báo cho các quản lý trường khác lớp được thêm mới
            $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'classes', $school['page_admin']);
            $userDao->postNotifications($userManagerIds, NOTIFICATION_NEW_CLASS, NOTIFICATION_NODE_TYPE_SCHOOL,
                $lastId, convertText4Web($_POST['title']), $school['page_name'], convertText4Web($school['page_title']));
            //7. Cập nhật số lượng class của trường
            $schoolDao->updateClassCount($school['page_id'], 1);
            //unset($_SESSION[$_POST['school_username']]);

            //Cập nhật lại session Class Name
            //$classDao->refreshClassInSession($school['page_id']);

            // Tạo một bản ghi trong bảng ci_class_school_year
            $argsSY = array();

            $argsSY['class_id'] = $lastId;
            $argsSY['class_name'] = $_POST['title'];
            $argsSY['school_year'] = date("Y").'-'.(date("Y")+1);
            $argsSY['teacher_id'] = $teacherIds[0];

            // Insert db
            $classDao->createClassSchoolYear($argsSY);

            /* ---------- CLASS - MEMCACHE ---------- */
            //1.Cập nhật thông tin trường
            updateSchoolData($school['page_id'], SCHOOL_CLASSES);
            updateSchoolData($school['page_id'], SCHOOL_CONFIG);

            //2.Cập nhật lại khối lớp
            updateClassLevelData($args['class_level_id']);

            //3.Cập nhật danh sách giáo viên lớp
            updateClassData($lastId, CLASS_TEACHERS);

            //4.Cập nhật thông tin quản lý cho giáo viên và thông tin lớp tương ứng
            foreach ($teacherIds as $teacherId) {
                addUserManageData($teacherId, USER_MANAGE_CLASSES, $lastId);
            }
            /* ---------- CLASS - MEMCACHE ---------- */

            $db->commit();
            return_json(array('callback' => 'window.location = "'.$system['system_url'].'/school/'.$_POST['school_username'].'/classes";'));

            break;

        case 'delete':
            $db->begin_transaction();

            if(!isset($_POST['id']) || !is_numeric($_POST['id'])) {
                _error(400);
            }
            $class = getClassData($_POST['id'], CLASS_INFO);
            if (is_null($class)) {
                _error(400);
            }
            if($user->_data['user_id'] != $class['group_admin'] && $user->_data['user_id'] != $school['page_admin']) {
                throw new Exception(__("You do not have any authority to remove this class"));
            }
            // Lấy ra danh sách giáo viên của lớp
            $teachers = getClassData($_POST['id'], CLASS_TEACHERS);
            $teacherIds = array_keys($teachers);

            $classDao->deleteClass($_POST['id'], $school['page_admin']);

            // check xem lớp có còn tồn tại không
            $classNow = $classDao->getClass($_POST['id']);
            if(is_null($classNow)) {
                //Cập nhật số lượng class của trường
                $schoolDao->updateClassCount($school['page_id'], -1);
                //Cập nhật lại session Class Name
                //$classDao->refreshClassInSession($school['page_id']);

                /* ---------- CLASS - MEMCACHE ---------- */
                //1. Xóa thông tin lớp
                deleteClassData($_POST['id']);

                //2.Cập nhật số lượng class của trường
                deleteSchoolData($school['page_id'], SCHOOL_CLASSES, $_POST['id']);
                updateSchoolData($school['page_id'], SCHOOL_CONFIG);

                //3.Cập nhật khối lớp của trường
                updateClassLevelData($class['class_level_id']);

                //4.Cập nhật đối tượng quản lý cho giáo viên
                foreach ($teacherIds as $teacherId) {
                    deleteTeacherData($teacherId, TEACHER_CLASSES, $_POST['id']);
                    deleteUserManageData($teacherId, USER_MANAGE_CLASSES, $_POST['id']);
                }

                /* ---------- CLASS - MEMCACHE ---------- */
            }

            $db->commit();

            return_json(array('callback' => 'window.location = "'.$system['system_url'].'/school/'.$_POST['school_username'].'/classes";'));
            break;
        case 'class_up':
            $db->begin_transaction();
            // valid inputs

            if(!isset($_POST['class_id']) || !is_numeric($_POST['class_id'])) {
                throw  new Exception(__("Please select a class"));
            }
            //$class = $classDao->getClass($_POST['class_id']);
            $class = getClassData($_POST['class_id'], CLASS_INFO);
            if (is_null($class)) {
                _error(404);
            }

            //1. Cập nhật thông tin lớp
            //$args['username_old'] = $class['group_name'];
            $argsUp['group_id'] = $_POST['class_id'];
            $argsUp['class_level_id'] = $_POST['class_level_id'];
            $argsUp['title'] = trim($_POST['new_title']);
            $argsUp['school_id'] = $school['page_id'];
            $classDao->upClass($argsUp);

            $db->commit();

            /* ---------- CLASS - MEMCACHE ---------- */
            //1. Cập nhật lại thông tin lớp
            updateClassData($argsUp['group_id'], CLASS_INFO);

            //2.Cập nhật lại khối lớp
            updateClassLevelData($argsUp['class_level_id']);

            /* ---------- CLASS - MEMCACHE ---------- */

            return_json( array('success' => true, 'message' => __("Done, Class info have been updated")) );
            //return_json(array('callback' => 'window.location = "'.$system['system_url'].'/school/'.$_POST['school_username'].'/classes/classup";'));
            break;
        case 'class_leave_school':
            if (!canEdit($_POST['school_username'], 'classes')) {
                _error(403);
            }
            /**
             * Hàm này xử lý khi cho lớp tốt nghiệp
             * 1. Cập nhật thông tin trẻ trong lớp (chuyển trạng thái Inactive - bảng ci_class_child)
             * 2. Cập nhật thông tin trẻ trong trường (chuyển trạng thái Inactive - bảng ci_school_child)
             * 3. Hủy cha mẹ vào khỏi của lớp cũ.
             * 4. Loại giáo viên khỏi lớp
             */
            $db->begin_transaction();
            $class_id = $_POST['class_id'];
            $class = getClassData($class_id, CLASS_INFO);
            // Lấy danh sách id của trẻ post lên
            $childIds = $_POST['child_ids'];

            //1. Cập nhật thông tin trẻ trong lớp (chuyển trạng thái Inactive - bảng ci_class_child và ci_school_child) (làm 1 lượt tất cả trẻ trong lớp)
            $childDao->leaveChildrenInSchool($childIds, $_POST['class_id'], $_POST['class_end_at']);

            // Lặp danh sách id của trẻ, xử lý với từng trẻ
            if(count($childIds) > 0) {
                $parentIds_arr = array();
                $males = 0;
                $females = 0;
                foreach ($childIds as $childId) {
                    // Lấy ra thông tin cũ của trẻ để kiểm tra
                    $child = $childDao->getSchoolChild($childId, $school['page_id']);
                    if (is_null($child)) {
                        _error(404);
                    }
                    if($child['gender'] == 'male') {
                        $males = $males - 1;
                    } else {
                        $females = $females - 1;
                    }
                    $parentIds = array();
                    $parents = getChildData($childId, CHILD_PARENTS);
                    $parentIds = array_keys($parents);
                    // Lấy danh sách phụ huynh của toàn bộ trẻ, sau đó mới loại ra khỏi lớp (giữ nguyên like page trường)
                    $parentIds_arr = array_merge($parentIds, $parentIds_arr);

                    //2.1. Xóa thông tin trẻ trong ci_parent_manage
                    $childDao->deleteParentManageInfo($childId, $child['child_parent_id']);

                    //5. Cập nhật quyết toán học phí.
                    $tuition4Leave = array();
                    $tuition4Leave['attendance_count'] = $_POST['attendance_count_' . $childId];
                    $tuition4Leave['final_amount'] = str_replace(',', '', $_POST['final_amount_' . $childId]);
                    $tuition4Leave['debt_amount'] = isset($_POST['debt_amount_' . $childId])? str_replace(',', '', $_POST['debt_amount_' . $childId]): 0;
                    $tuition4Leave['total_deduction'] = str_replace(',', '', $_POST['total_deduction_' . $childId]);
                    $tuition4Leave['paid_amount'] = str_replace(',', '', $_POST['paid_amount_' . $childId]);
                    $tuition4Leave['school_id'] = $school['page_id'];
                    $tuition4Leave['child_id'] = $childId;

                    $tuition4LDetails = getTuitionChild4LeaveData($childId);
                    $tuitionDao->insertTuition4Leave($tuition4Leave, $tuition4LDetails);

                    //3.Xóa thông tin trẻ
                    deleteChildData($childId);
                    //1.Cập nhật thông tin trường
                    deleteSchoolData($school['page_id'], SCHOOL_CHILDREN, $childId);
                }

            }

            //2. Xóa cha mẹ khỏi lớp (nhưng vẫn giữ like page của trường) (hàm này loại bỏ 1 lần tất cả phụ huynh trong lớp)
            if (count($parentIds_arr) > 0) {
                $parentIds_arr = array_unique($parentIds_arr);
                if ($_POST['class_id'] > 0) {
                    $classDao->deleteUserFromClass($_POST['class_id'], $parentIds_arr);
                }
            }

            //3. Giảm số lượng trẻ của trường (giảm giới tính trẻ)
            $schoolDao->updateGenderCount($school['page_id'], 'male', $males);
            $schoolDao->updateGenderCount($school['page_id'], 'female', $females);

            $school['male_count'] = $school['male_count'] + $males;
            $school['female_count'] = $school['female_count'] + $females;


            //4.Hủy các dịch vụ trẻ đã đăng ký (hủy tất cả trẻ trong lớp)
            $serviceDao->updateServiceForChildrenLeave($childIds, $_POST['class_end_at']);

            // 6. Bỏ giáo viên khỏi lớp
            $oldTeachers = getClassData($class_id, CLASS_TEACHERS);
            $oldTeacherIds = array_keys($oldTeachers);
            if(count($oldTeacherIds) > 0) {
                $teacherDao->deleteTeacherList($class_id, $oldTeacherIds);
                //2.1. Xoa giao vien khoi thanh vien nhom
                $classDao->deleteUserFromClass($class_id, $oldTeacherIds);
            }

            // 7. Cập nhật số lượng class của trường
            $schoolDao->updateClassCount($school['page_id'], -1);

            // 8. Cập nhật lại trạng thái của lớp
            $classDao->updateGroupStatus($class_id);

            $db->commit();

            /* ---------- UPDATE - MEMCACHE ---------- */
            //2. Xóa thông tin lớp
            deleteClassData($class_id);

            //3.Cập nhật số lượng class của trường
            deleteSchoolData($school['page_id'], SCHOOL_CLASSES, $class_id);
            updateSchoolData($school['page_id'], SCHOOL_CONFIG);

            //4.Cập nhật khối lớp của trường
            updateClassLevelData($class['class_level_id']);

            //5.Cập nhật đối tượng quản lý cho giáo viên
            foreach ($oldTeacherIds as $teacherId) {
                deleteTeacherData($teacherId, TEACHER_CLASSES, $class_id);
                deleteUserManageData($teacherId, USER_MANAGE_CLASSES, $class_id);
            }

            /* ---------- END - MEMCACHE ---------- */

            return_json( array('success' => true, 'message' => __("Child's school leave information has been updated")) );
            break;

        default:
            _error(400);
            break;
    }

} catch (Exception $e) {
    $db->rollback();
    return_json( array('error' => true, 'message' => $e->getMessage()) );
} finally {
    $db->autocommit(true);
}

/**
 * Lấy ra thông tin học phí chi tiết cần quyết toán trước khi trẻ nghỉ. (có truyền vào child_id)
 *
 * @return array
 */
function getTuitionChild4LeaveData($child_id) {
    //Thông tin phí tính cho tháng này
    $feeIds = is_null($_POST['fee_id_' . $child_id])? array(): $_POST['fee_id_' . $child_id];
    $feeQuantities = is_null($_POST['quantity_' . $child_id])? array(): $_POST['quantity_' . $child_id];
    $feePrices = is_null($_POST['unit_price_' . $child_id])? array(): $_POST['unit_price_' . $child_id];

    $tuitionDetails = array();
    for ($idx = 0; $idx < count($feeIds); $idx++) {
        $tuitionDetail = array();
        $tuitionDetail['service_id'] = 0;
        $tuitionDetail['fee_id'] = $feeIds[$idx];
        $tuitionDetail['quantity'] = $feeQuantities[$idx];
        $tuitionDetail['unit_price'] = str_replace(',', '', $feePrices[$idx]);
        $tuitionDetail['type'] = TUITION_DETAIL_FEE;
        $tuitionDetail['service_type'] = 0;

        if ($tuitionDetail['quantity']*$tuitionDetail['unit_price'] != 0) {
            $tuitionDetails[] = $tuitionDetail;
        }
    }

    // Lấy dịch vụ của tháng hiện tại
    $serviceIds = is_null($_POST['service_id_' . $child_id])? array(): $_POST['service_id_' . $child_id];
    $serviceQuantities = is_null($_POST['service_quantity_' . $child_id])? array(): $_POST['service_quantity_' . $child_id];
    $servicePrices = is_null($_POST['service_fee_' . $child_id])? array(): $_POST['service_fee_' . $child_id];
    $serviceTypes = is_null($_POST['service_type_' . $child_id])? array(): $_POST['service_type_' . $child_id];
    for ($idx = 0; $idx < count($serviceIds); $idx++) {
        $tuitionDetail = array();
        $tuitionDetail['service_id'] = $serviceIds[$idx];
        $tuitionDetail['fee_id'] = 0;
        $tuitionDetail['quantity'] = $serviceQuantities[$idx];
        $tuitionDetail['unit_price'] = str_replace(',', '', $servicePrices[$idx]);
        $tuitionDetail['type'] = TUITION_DETAIL_SERVICE;
        $tuitionDetail['service_type'] = $serviceTypes[$idx];

        if ($tuitionDetail['quantity']*$tuitionDetail['unit_price'] != 0) {
            $tuitionDetails[] = $tuitionDetail;
        }
    }

    $cbServiceIds = is_null($_POST['cbservice_id_' . $child_id])? array(): $_POST['cbservice_id_' . $child_id];
    $cbServiceQuantities = is_null($_POST['cbservice_quantity_' . $child_id])? array(): $_POST['cbservice_quantity_' . $child_id];
    $cbServicePrices = is_null($_POST['cbservice_price_' . $child_id])? array(): $_POST['cbservice_price_' . $child_id];
    //Gộp dịch vụ tính theo SỐ LẦN
    for ($cbIdx = 0; $cbIdx < count($cbServiceIds); $cbIdx++) {
        $tuitionDetail = array();
        $tuitionDetail['service_id'] = $cbServiceIds[$cbIdx];
        $tuitionDetail['fee_id'] = 0;
        $tuitionDetail['quantity'] = $cbServiceQuantities[$cbIdx];
        $tuitionDetail['unit_price'] = convertMoneyToNumber($cbServicePrices[$cbIdx]);
        $tuitionDetail['service_type'] = SERVICE_TYPE_COUNT_BASED;
        if ($cbServiceIds[$cbIdx] > 0) {
            $tuitionDetail['type'] = TUITION_DETAIL_SERVICE;
        } else {
            $tuitionDetail['type'] = LATE_PICKUP_FEE;
        }
        if ($tuitionDetail['quantity']*$tuitionDetail['unit_price'] != 0) {
            $tuitionDetails[] = $tuitionDetail;
        }
    }

    return $tuitionDetails;
}

?>