<?php
/**
 * Package: ajax/ci/bo
 *
 * @package ConIu
 * @author TaiLA
 */

// fetch bootstrap
require('../../../../../bootstrap.php');

// check AJAX Request
is_ajax();
check_login();

if (is_empty($_POST['school_username']) || !valid_username($_POST['school_username'])) {
    _error(404);
}

include_once(DAO_PATH . 'dao_school.php');
include_once(DAO_PATH . 'dao_menu.php');
include_once(DAO_PATH . 'dao_child.php');
include_once(DAO_PATH . 'dao_parent.php');
include_once(DAO_PATH . 'dao_teacher.php');
include_once(DAO_PATH . 'dao_user.php');
include_once(DAO_PATH . 'dao_class_level.php');
include_once(DAO_PATH . 'dao_class.php');
include_once(DAO_PATH . 'dao_role.php');

$schoolDao = new SchoolDAO();
$menuDao = new MenuDAO();
$childDao = new ChildDAO();
$parentDao = new ParentDAO();
$teacherDao = new TeacherDAO();
$userDao = new UserDAO();
$classLevelDao = new ClassLevelDAO();
$classDao = new ClassDAO();
$roleDao = new RoleDAO();

//$school = getSchool($_POST['school_username']);
$school = getSchoolDataByUsername($_POST['school_username'], SCHOOL_INFO);
if (is_null($school)) {
    _error(403);
}

try {
    $return = array();
    $db->autocommit(false);
    switch ($_POST['do']) {
        case 'edit':
            if (!canEdit($_POST['school_username'], 'menus')) {
                _error(403);
            }
            $db->begin_transaction();
            // 1. Edit menu
            $args = array();
            $args['menu_id'] = $_POST['menu_id'];
            $args['menu_name'] = $_POST['menu_name'];
            $args['begin'] = $_POST['begin'];
            $args['description'] = $_POST['description'];
            $args['school_id'] = $school['page_id'];
            $args['class_level_id'] = $_POST['class_level_id'];
            $args['class_id'] = $_POST['class_id'];
            $args['is_meal'] = (isset($_POST['use_meal']) && $_POST['use_meal'] == 'on') ? 1 : 0;
            $args['is_saturday'] = (isset($_POST['study_saturday']) && $_POST['study_saturday'] == 'on') ? 1 : 0;
            $args['is_notified'] = (isset($_POST['notify_immediately']) && $_POST['notify_immediately'] == 'on')? 1: 0;
            $level = $_POST['level'];
            if($level == 2 && $args['class_level_id'] == 0) {
                throw new Exception(__("You must select a class level"));
            } else if($level == 3 && $args['class_id'] == 0) {
                throw new Exception(__("You must select a class"));
            }
            $args['applied_for'] = 0;
            if($args['class_id'] > 0) {
                $args['applied_for'] = CLASS_LEVEL;
            }
            if(($args['class_id'] == 0) && $args['class_level_id'] > 0) {
                $args['applied_for'] = CLASS_LEVEL_LEVEL;
            }
            if($args['class_id'] == 0 && $args['class_level_id'] == 0) {
                $args['applied_for'] = SCHOOL_LEVEL;
            }
            $data = $menuDao->getMenuDetailById($_POST['menu_id']);
            $menuDao->updateMenu($args, $data);

            // Xóa menu_detail cũ
            $menuDao->deleteMenuDetail($args['menu_id']);

            // Insert menu detail mới
            $starts = array();
            $starts = $_POST['start'];

            $meals = array();
            if($args['is_meal']) {
                $meals = $_POST['meal'];
            }
            if($args['is_meal'] && (count($starts) != count($meals))) {
                throw new Exception(__('Please enter the full information'));
            }
            $meal_detail_mon = array();
            $meal_detail_mon = $_POST['meal_detail_mon'];

            $meal_detail_tue = array();
            $meal_detail_tue = $_POST['meal_detail_tue'];

            $meal_detail_wed = array();
            $meal_detail_wed = $_POST['meal_detail_wed'];

            $meal_detail_thu = array();
            $meal_detail_thu = $_POST['meal_detail_thu'];

            $meal_detail_fri = array();
            $meal_detail_fri = $_POST['meal_detail_fri'];

            $meal_detail_sat = array();
            if($args['is_saturday']) {
                $meal_detail_sat = $_POST['meal_detail_sat'];
            }

            $menuDao->insertMenuDetail($args['menu_id'], $starts, $meals, $meal_detail_mon, $meal_detail_tue, $meal_detail_wed, $meal_detail_thu, $meal_detail_fri, $meal_detail_sat);
            if($args['is_notified']) {
                if($args['applied_for'] == CLASS_LEVEL) {
                    // Danh sách trẻ trong lớp
                    //$childrenIds = $childDao->getChildIdOfClass($args['class_id']);
                    $children = getClassData($args['class_id'], CLASS_CHILDREN);
                    $childrenIds = array_keys($children);
                    //$class = $classDao->getClass($args['class_id']);
                    $class = getClassData($args['class_id'], CLASS_INFO);

                    //Thông báo tới phụ huynh có trẻ trong lớp được tạo lịch học.
                    if (count($childrenIds) > 0) {
                        $parents = $childDao->getChildNParentIdForMemcache($childrenIds);
                        if(count($parents) > 0) {
                            foreach ($parents as $parent) {
                                $userDao->postNotifications($parent['parent_id'], NOTIFICATION_UPDATE_MENU, NOTIFICATION_NODE_TYPE_CHILD,
                                    $_POST['menu_id'], convertText4Web($_POST['menu_name']), $parent['child_id'], convertText4Web($class['group_title']));
                            }
                        }
                        //Thông báo tới giáo viên của lớp.
                        //$teachers = $teacherDao->getTeacherOfClass($class['group_id']);
                        $teachers = getClassData($class['group_id'], CLASS_TEACHERS);
                        if(count($teachers) > 0) {
                            $teacherIds = array_keys($teachers);
                            $userDao->postNotifications($teacherIds, NOTIFICATION_UPDATE_MENU, NOTIFICATION_NODE_TYPE_CLASS,
                                $_POST['menu_id'], convertText4Web($_POST['menu_name']), $class['group_name'], convertText4Web($class['group_title']));
                        }
                        // Thông báo đến các quản lý trường khác
                        $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'menus', $school['page_admin']);
                        $userDao->postNotifications($userManagerIds, NOTIFICATION_UPDATE_MENU, NOTIFICATION_NODE_TYPE_SCHOOL,
                            $_POST['menu_id'], convertText4Web($_POST['menu_name']), $school['page_name'], convertText4Web($class['group_title']));
                    }
                } elseif ($args['applied_for'] == CLASS_LEVEL_LEVEL) {
                    // Danh sách trẻ trong khối
                    $childrenIds = $childDao->getChildIdByClassLevel($args['class_level_id']);
                    //$class_level = $classLevelDao->getClassLevel($args['class_level_id']);
                    $class_level = getClassLevelData($args['class_level_id'], CLASS_LEVEL_INFO);

                    //Thông báo tới phụ huynh có trẻ trong lớp được tạo lịch học.
                    if (count($childrenIds) > 0) {
                        $parents = $childDao->getChildNParentIdForMemcache($childrenIds);
                        if(count($parents) > 0) {
                            foreach ($parents as $parent) {
                                $userDao->postNotifications($parent['parent_id'], NOTIFICATION_UPDATE_MENU, NOTIFICATION_NODE_TYPE_CHILD,
                                    $_POST['menu_id'], convertText4Web($_POST['menu_name']), $parent['child_id'], convertText4Web($class_level['class_level_name']));
                            }
                        }
                        //Thông báo tới giáo viên của lớp.
                        //$classes = $classDao->getClassesOfLevel($args['class_level_id']);
                        $classes = getClassLevelData($args['class_level_id'], CLASS_LEVEL_CLASSES);
                        foreach ($classes as $class) {
                            //$teachers = $teacherDao->getTeacherOfClass($class['group_id']);
                            $teachers = getClassData($class['group_id'], CLASS_TEACHERS);
                            if(count($teachers) > 0) {
                                $teacherIds = array_keys($teachers);
                                $userDao->postNotifications($teacherIds, NOTIFICATION_UPDATE_MENU, NOTIFICATION_NODE_TYPE_CLASS,
                                    $_POST['menu_id'], convertText4Web($_POST['menu_name']), $class['group_name'], convertText4Web($class_level['class_level_name']));
                            }
                        }
                        // Thông báo đến các quản lý trường khác
                        $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'menus', $school['page_admin']);
                        $userDao->postNotifications($userManagerIds, NOTIFICATION_UPDATE_MENU, NOTIFICATION_NODE_TYPE_SCHOOL,
                            $_POST['menu_id'], convertText4Web($_POST['menu_name']), $school['page_name'], convertText4Web($class_level['class_level_name']));
                    }
                } else {
                    // Danh sách trẻ trong trường
                    //$childrenIds = $childDao->getChildIdBySchool($args['school_id']);
                    $children = getSchoolData($args['school_id'], SCHOOL_CHILDREN);
                    $childrenIds = array_keys($children);

                    //Thông báo tới phụ huynh có trẻ trong lớp được tạo lịch học.
                    if (count($childrenIds) > 0) {
                        $parents = $childDao->getChildNParentIdForMemcache($childrenIds);
                        if(count($parents) >0) {
                            foreach ($parents as $parent) {
                                $userDao->postNotifications($parent['parent_id'], NOTIFICATION_UPDATE_MENU, NOTIFICATION_NODE_TYPE_CHILD,
                                    $_POST['menu_id'], convertText4Web($_POST['menu_name']), $parent['child_id'], convertText4Web($school['page_title']));
                            }
                        }
                        //Thông báo tới giáo viên của lớp.
                        // Lấy danh sách lớp
                        //$classes = $classDao->getClassesOfSchool($args['school_id']);
                        $classes = getSchoolData($args['school_id'], SCHOOL_CLASSES);
                        foreach ($classes as $class) {
                            //$teachers = $teacherDao->getTeacherOfClass($class['group_id']);
                            $teachers = getClassData($class['group_id'], CLASS_TEACHERS);
                            if(count($teachers) > 0) {
                                $teacherIds = array_keys($teachers);
                                $userDao->postNotifications($teacherIds, NOTIFICATION_UPDATE_MENU, NOTIFICATION_NODE_TYPE_CLASS,
                                    $_POST['menu_id'], convertText4Web($_POST['menu_name']), $class['group_name'], convertText4Web($school['page_title']));
                            }
                        }
                        // Thông báo đến các quản lý trường khác
                        $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'menus', $school['page_admin']);
                        $userDao->postNotifications($userManagerIds, NOTIFICATION_UPDATE_MENU, NOTIFICATION_NODE_TYPE_SCHOOL,
                            $_POST['menu_id'], convertText4Web($_POST['menu_name']), $school['page_name'], convertText4Web($school['page_title']));
                    }
                }
            }

            $db->commit();
            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/school/' . $_POST['school_username'] . '/menus";'));
            //return_json(array('success' => true, 'message' => __("Done, Menu have been updated")));

            break;
        case 'add':
            if (!canEdit($_POST['school_username'], 'menus')) {
                _error(403);
            }
            $db->begin_transaction();
            $args = array();
            $args['menu_name'] = $_POST['menu_name'];
            $args['begin'] = $_POST['begin'];
            $args['description'] = $_POST['description'];
            $args['school_id'] = $school['page_id'];
            $args['class_level_id'] = $_POST['class_level_id'];
            $args['class_id'] = $_POST['class_id'];
            $args['is_meal'] = (isset($_POST['use_mea']) && $_POST['use_mea'] == 'on') ? 1 : 0;
            $args['is_saturday'] = (isset($_POST['study_saturday']) && $_POST['study_saturday'] == 'on') ? 1 : 0;
            $args['is_notified'] = (isset($_POST['notify_immediately']) && $_POST['notify_immediately'] == 'on')? 1: 0;
            $level = $_POST['level'];
            if($level == 2 && $args['class_level_id'] == 0) {
                throw new Exception(__("You must select a class level"));
            } else if($level == 3 && $args['class_id'] == 0) {
                throw new Exception(__("You must select a class"));
            }
            $args['applied_for'] = 0;
            if($args['class_id'] > 0) {
                $args['applied_for'] = CLASS_LEVEL;
            }
            if(($args['class_id'] == 0) && $args['class_level_id'] > 0) {
                $args['applied_for'] = CLASS_LEVEL_LEVEL;
            }
            if($args['class_id'] == 0 && $args['class_level_id'] == 0) {
                $args['applied_for'] = SCHOOL_LEVEL;
            }


            // 1. Insert lịch học vào bảng ci_menu
            $menuId = $menuDao->insertMenu($args);

            // Tăng lượt tương tác thêm mới - TaiLA
            addInteractive($school['page_id'], 'menu', 'school_view', $menuId, 1);
            // 2. Insert vào bảng ci_menu_detail
            $starts = array();
            $starts = array_map("trim", $_POST['start']);
            $starts = array_diff($starts, array(""));

            if(count($starts) == 0) {
                throw new Exception(__("Please enter menu's information"));
            }

            $meals = array();
            if($args['is_meal']) {
                $meals = array_map("trim", $_POST['meal']);
                $meals = array_diff($meals, array(""));
            }
            if($args['is_meal'] && (count($starts) != count($meals))) {
                throw new Exception(__('Please enter the full information'));
            }
            $meal_detail_mon = array();
            $meal_detail_mon = $_POST['meal_detail_mon'];

            $meal_detail_tue = array();
            $meal_detail_tue = $_POST['meal_detail_tue'];

            $meal_detail_wed = array();
            $meal_detail_wed = $_POST['meal_detail_wed'];

            $meal_detail_thu = array();
            $meal_detail_thu = $_POST['meal_detail_thu'];

            $meal_detail_fri = array();
            $meal_detail_fri = $_POST['meal_detail_fri'];

            $meal_detail_sat = array();
            if($_POST['meal_detail_sat']) {
                $meal_detail_sat = $_POST['meal_detail_sat'];
            }

            $menuDao->insertMenuDetail($menuId, $starts, $meals, $meal_detail_mon, $meal_detail_tue, $meal_detail_wed, $meal_detail_thu, $meal_detail_fri, $meal_detail_sat);
            if($args['is_notified']) {
                if($args['applied_for'] == CLASS_LEVEL) {
                    // Danh sách trẻ trong lớp
                    //$childrenIds = $childDao->getChildIdOfClass($args['class_id']);
                    $children = getClassData($args['class_id'], CLASS_CHILDREN);
                    $childrenIds = array_keys($children);
                    //$class = $classDao->getClass($args['class_id']);
                    $class = getClassData($args['class_id'], CLASS_INFO);

                    //Thông báo tới phụ huynh có trẻ trong lớp được tạo lịch học.
                    if (count($childrenIds) > 0) {
                        $parents = $childDao->getChildNParentIdForMemcache($childrenIds);
                        if(count($parents) > 0) {
                            foreach ($parents as $parent) {
                                $userDao->postNotifications($parent['parent_id'], NOTIFICATION_NEW_MENU, NOTIFICATION_NODE_TYPE_CHILD,
                                    $menuId, convertText4Web($_POST['menu_name']), $parent['child_id'], convertText4Web($class['group_title']));
                            }
                        }
                        //Thông báo tới giáo viên của lớp.
                        //$teachers = $teacherDao->getTeacherOfClass($class['group_id']);
                        $teachers = getClassData($class['group_id'], CLASS_TEACHERS);
                        if(count($teachers) > 0) {
                            $teacherIds = array_keys($teachers);
                            $userDao->postNotifications($teacherIds, NOTIFICATION_NEW_MENU, NOTIFICATION_NODE_TYPE_CLASS,
                                $menuId, convertText4Web($_POST['menu_name']), $class['group_name'], convertText4Web($class['group_title']));
                        }
                        // Thông báo đến các quản lý trường khác
                        $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'menus', $school['page_admin']);
                        $userDao->postNotifications($userManagerIds, NOTIFICATION_NEW_MENU, NOTIFICATION_NODE_TYPE_SCHOOL,
                            $menuId, convertText4Web($_POST['menu_name']), $school['page_name'], convertText4Web($class['group_title']));
                    }
                } elseif ($args['applied_for'] == CLASS_LEVEL_LEVEL) {
                    // Danh sách trẻ trong khối
                    $childrenIds = $childDao->getChildIdByClassLevel($args['class_level_id']);
                    //$class_level = $classLevelDao->getClassLevel($args['class_level_id']);
                    $class_level = getClassLevelData($args['class_level_id'], CLASS_LEVEL_INFO);

                    //Thông báo tới phụ huynh có trẻ trong lớp được tạo lịch học.
                    if (count($childrenIds) > 0) {
                        $parents = $childDao->getChildNParentIdForMemcache($childrenIds);
                        if(count($parents) > 0) {
                            foreach ($parents as $parent) {
                                $userDao->postNotifications($parent['parent_id'], NOTIFICATION_NEW_MENU, NOTIFICATION_NODE_TYPE_CHILD,
                                    $menuId, convertText4Web($_POST['menu_name']), $parent['child_id'], convertText4Web($class_level['class_level_name']));
                            }
                        }
                        //Thông báo tới giáo viên của lớp.
                        //$classes = $classDao->getClassesOfLevel($args['class_level_id']);
                        $classes = getClassLevelData($args['class_level_id'], CLASS_LEVEL_CLASSES);
                        foreach ($classes as $class) {
                            //$teachers = $teacherDao->getTeacherOfClass($class['group_id']);
                            $teachers = getClassData($class['group_id'], CLASS_TEACHERS);
                            if(count($teachers) > 0) {
                                $teacherIds = array_keys($teachers);
                                $userDao->postNotifications($teacherIds, NOTIFICATION_NEW_MENU, NOTIFICATION_NODE_TYPE_CLASS,
                                    $menuId, convertText4Web($_POST['menu_name']), $class['group_name'], convertText4Web($class_level['class_level_name']));
                            }
                        }
                        // Thông báo đến các quản lý trường khác
                        $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'menus', $school['page_admin']);
                        $userDao->postNotifications($userManagerIds, NOTIFICATION_NEW_MENU, NOTIFICATION_NODE_TYPE_SCHOOL,
                            $menuId, convertText4Web($_POST['menu_name']), $school['page_name'], convertText4Web($class_level['class_level_name']));
                    }
                } else {
                    // Danh sách trẻ trong trường
                    //$childrenIds = $childDao->getChildIdBySchool($args['school_id']);
                    $children = getSchoolData($args['school_id'], SCHOOL_CHILDREN);
                    $childrenIds = array_keys($children);

                    //Thông báo tới phụ huynh có trẻ trong lớp được tạo lịch học.
                    if (count($childrenIds) > 0) {
                        $parents = $childDao->getChildNParentIdForMemcache($childrenIds);
                        if(count($parents) > 0) {
                            foreach ($parents as $parent) {
                                $userDao->postNotifications($parent['parent_id'], NOTIFICATION_NEW_MENU, NOTIFICATION_NODE_TYPE_CHILD,
                                    $menuId, convertText4Web($_POST['menu_name']), $parent['child_id'], convertText4Web($school['page_title']));
                            }
                        }

                        //Thông báo tới giáo viên của lớp.
                        // Lấy danh sách lớp
                        //$classes = $classDao->getClassesOfSchool($args['school_id']);
                        $classes = getSchoolData($args['school_id'], SCHOOL_CLASSES);
                        foreach ($classes as $class) {
                            //$teachers = $teacherDao->getTeacherOfClass($class['group_id']);
                            $teachers = getClassData($class['group_id'], CLASS_TEACHERS);
                            if(count($teachers) > 0) {
                                $teacherIds = array_keys($teachers);
                                $userDao->postNotifications($teacherIds, NOTIFICATION_NEW_MENU, NOTIFICATION_NODE_TYPE_CLASS,
                                    $menuId, convertText4Web($_POST['menu_name']), $class['group_name'], convertText4Web($school['page_title']));
                            }
                        }
                        // Thông báo đến các quản lý trường khác
                        $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'menus', $school['page_admin']);
                        $userDao->postNotifications($userManagerIds, NOTIFICATION_NEW_MENU, NOTIFICATION_NODE_TYPE_SCHOOL,
                            $menuId, convertText4Web($_POST['menu_name']), $school['page_name'], convertText4Web($school['page_title']));
                    }
                }

                /* Coniu - Tương tác trường */
                $menuIds[] = $menuId;
                setIsAddNew($school['page_id'], 'menu_created', $menuIds);
                /* Coniu - END */
            }

            $db->commit();
            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/school/' . $_POST['school_username'] . '/menus";'));
            // return_json(array('success' => true, 'message' => __("Done, Menu have been updated")));

            break;

        case 'copy':
            if (!canEdit($_POST['school_username'], 'menus')) {
                _error(403);
            }
            $db->begin_transaction();
            // 1. copy menu
            $args = array();
            $args['menu_name'] = $_POST['menu_name'];
            $args['begin'] = $_POST['begin'];
            $args['description'] = $_POST['description'];
            $args['school_id'] = $school['page_id'];
            $args['class_level_id'] = $_POST['class_level_id'];
            $args['class_id'] = $_POST['class_id'];
            $args['is_meal'] = (isset($_POST['use_meal']) && $_POST['use_meal'] == 'on') ? 1 : 0;
            $args['is_saturday'] = (isset($_POST['study_saturday']) && $_POST['study_saturday'] == 'on') ? 1 : 0;
            $args['is_notified'] = (isset($_POST['notify_immediately']) && $_POST['notify_immediately'] == 'on')? 1: 0;
            $level = $_POST['level'];
            if($level == 2 && $args['class_level_id'] == 0) {
                throw new Exception(__("You must select a class level"));
            } else if($level == 3 && $args['class_id'] == 0) {
                throw new Exception(__("You must select a class"));
            }
            $args['applied_for'] = 0;
            if($args['class_id'] > 0) {
                $args['applied_for'] = CLASS_LEVEL;
            }
            if(($args['class_id'] == 0) && $args['class_level_id'] > 0) {
                $args['applied_for'] = CLASS_LEVEL_LEVEL;
            }
            if($args['class_id'] == 0 && $args['class_level_id'] == 0) {
                $args['applied_for'] = SCHOOL_LEVEL;
            }
            // 1. Insert lịch học vào bảng ci_menu
            $menuId = $menuDao->insertMenu($args);

            // Tăng lượt tương tác thêm mới - TaiLA
            addInteractive($school['page_id'], 'menu', 'school_view', $menuId, 1);
            // 2. Insert vào bảng ci_menu_detail
            $starts = array();
            $starts = $_POST['start'];
            if(count($starts) == 0) {
                throw new Exception(__('Please enter the full information'));
            }

            $meals = array();
            if($args['is_meal']) {
                $meals = $_POST['meal'];
            }
            $meal_detail_mon = array();
            $meal_detail_mon = $_POST['meal_detail_mon'];

            $meal_detail_tue = array();
            $meal_detail_tue = $_POST['meal_detail_tue'];

            $meal_detail_wed = array();
            $meal_detail_wed = $_POST['meal_detail_wed'];

            $meal_detail_thu = array();
            $meal_detail_thu = $_POST['meal_detail_thu'];

            $meal_detail_fri = array();
            $meal_detail_fri = $_POST['meal_detail_fri'];

            $meal_detail_sat = array();
            if($_POST['meal_detail_sat']) {
                $meal_detail_sat = $_POST['meal_detail_sat'];
            }

            $menuDao->insertMenuDetail($menuId, $starts, $meals, $meal_detail_mon, $meal_detail_tue, $meal_detail_wed, $meal_detail_thu, $meal_detail_fri, $meal_detail_sat);
            if($args['is_notified']) {
                // Thông báo tới phụ huynh và giáo viên
                if($args['applied_for'] == CLASS_LEVEL) {
                    // Danh sách trẻ trong lớp
                    //$childrenIds = $childDao->getChildIdOfClass($args['class_id']);
                    $children = getClassData($args['class_id'], CLASS_CHILDREN);
                    $childrenIds = array_keys($children);
                    //$class = $classDao->getClass($args['class_id']);
                    $class = getClassData($args['class_id'], CLASS_INFO);

                    //Thông báo tới phụ huynh có trẻ trong lớp được tạo lịch học.
                    if (count($childrenIds) > 0) {
                        $parents = $childDao->getChildNParentIdForMemcache($childrenIds);
                        if(count($parents) > 0) {
                            foreach ($parents as $parent) {
                                $userDao->postNotifications($parent['parent_id'], NOTIFICATION_NEW_MENU, NOTIFICATION_NODE_TYPE_CHILD,
                                    $menuId, convertText4Web($_POST['menu_name']), $parent['child_id'], convertText4Web($class['group_title']));
                            }
                        }

                        //Thông báo tới giáo viên của lớp.
                        //$teachers = $teacherDao->getTeacherOfClass($class['group_id']);
                        $teachers = getClassData($class['group_id'], CLASS_TEACHERS);
                        if(count($teachers) > 0) {
                            $teacherIds = array_keys($teachers);
                            $userDao->postNotifications($teacherIds, NOTIFICATION_NEW_MENU, NOTIFICATION_NODE_TYPE_CLASS,
                                $menuId, convertText4Web($_POST['menu_name']), $class['group_name'], convertText4Web($class['group_title']));
                        }
                    }
                } elseif ($args['applied_for'] == CLASS_LEVEL_LEVEL) {
                    // Danh sách trẻ trong khối
                    $childrenIds = $childDao->getChildIdByClassLevel($args['class_level_id']);
                    //$class_level = $classLevelDao->getClassLevel($args['class_level_id']);
                    $class_level = getClassLevelData($args['class_level_id'], CLASS_LEVEL_INFO);

                    //Thông báo tới phụ huynh có trẻ trong lớp được tạo lịch học.
                    if (count($childrenIds) > 0) {
                        $parents = $childDao->getChildNParentIdForMemcache($childrenIds);
                        if(count($parents) > 0) {
                            foreach ($parents as $parent) {
                                $userDao->postNotifications($parent['parent_id'], NOTIFICATION_NEW_MENU, NOTIFICATION_NODE_TYPE_CHILD,
                                    $menuId, convertText4Web($_POST['menu_name']), $parent['child_id'], convertText4Web($class_level['class_level_name']));
                            }
                        }

                        //Thông báo tới giáo viên của lớp.
                        //$classes = $classDao->getClassesOfLevel($args['class_level_id']);
                        $classes = getClassLevelData($args['class_level_id'], CLASS_LEVEL_CLASSES);
                        foreach ($classes as $class) {
                            //$teachers = $teacherDao->getTeacherOfClass($class['group_id']);
                            $teachers = getClassData($class['group_id'], CLASS_TEACHERS);
                            if(count($teachers) > 0) {
                                $teacherIds = array_keys($teachers);
                                $userDao->postNotifications($teacherIds, NOTIFICATION_NEW_MENU, NOTIFICATION_NODE_TYPE_CLASS,
                                    $menuId, convertText4Web($_POST['menu_name']), $class['group_name'], convertText4Web($class_level['class_level_name']));
                            }
                        }
                    }
                } else {
                    // Danh sách trẻ trong trường
                    //$childrenIds = $childDao->getChildIdBySchool($args['school_id']);
                    $childs = getSchoolData($args['school_id'], SCHOOL_CHILDREN);
                    $childrenIds = array_keys($childs);

                    //Thông báo tới phụ huynh có trẻ trong lớp được tạo lịch học.
                    if (count($childrenIds) > 0) {
                        $parents = $childDao->getChildNParentIdForMemcache($childrenIds);
                        if(count($parents) > 0) {
                            foreach ($parents as $parent) {
                                $userDao->postNotifications($parent['parent_id'], NOTIFICATION_NEW_MENU, NOTIFICATION_NODE_TYPE_CHILD,
                                    $menuId, convertText4Web($_POST['menu_name']), $parent['child_id'], convertText4Web($school['page_title']));
                            }
                        }

                        //Thông báo tới giáo viên của lớp.
                        // Lấy danh sách lớp
                        //$classes = $classDao->getClassesOfSchool($args['school_id']);
                        $classes = getSchoolData($args['school_id'], SCHOOL_CLASSES);
                        foreach ($classes as $class) {
                            //$teachers = $teacherDao->getTeacherOfClass($class['group_id']);
                            $teachers = getClassData($class['group_id'], CLASS_TEACHERS);
                            if(count($teachers) > 0) {
                                $teacherIds = array_keys($teachers);
                                $userDao->postNotifications($teacherIds, NOTIFICATION_NEW_MENU, NOTIFICATION_NODE_TYPE_CLASS,
                                    $menuId, convertText4Web($_POST['menu_name']), $class['group_name'], convertText4Web($school['page_title']));
                            }
                        }
                    }
                }

                /* Coniu - Tương tác trường */
                $menuIds[] = $menuId;
                setIsAddNew($school['page_id'], 'menu_created', $menuIds);
                /* Coniu - END */
            }

            $db->commit();
            // return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/school/' . $_POST['school_username'] . '/menus";'));
            return_json(array('success' => true, 'message' => __("Done, Menu have been updated")));
            break;

        case 'delete_menu':
            if (!canEdit($_POST['school_username'], 'menus')) {
                _error(403);
            }
            $db->begin_transaction();
            if(!$_POST['id'] || !is_numeric($_POST['id'])) {
                _error(400);
            }
            $menuDao->deleteMenu($_POST['id']);

            $db->commit();
            return_json( array('success' => true, 'message' => __("Success")));
            break;

        case 'search':
            $school_id = $school['page_id'];
            $class_level_id = $_POST['class_level_id'];
            $class_id = $_POST['class_id'];
            $level = $_POST['level'];
            if($level == 0) {
                $results['menu'] = $menuDao->getMenuOfSchool($school_id);
            } else if($level == 1) {
                // Lấy danh sách lịch học có phạm vi áp dụng là trường
                $results['menu'] = $menuDao->getMenuById($school_id, $class_level_id, $class_id);
            } else if($level == 2) {
                // Lấy danh sách lịch học áp dụng cho khối
                if($class_level_id == 0) {
                    // Lấy tất cả danh sách lịc học phạm vi áp dụng là khối (khối nào cũng được)
                    $results['menu'] = $menuDao->getAllMenuByLevel($school_id, $level);
                } else {
                    $results['menu'] = $menuDao->getMenuById($school_id, $class_level_id, $class_id);
                }
            } else {
                if($class_id == 0) {
                    // Lấy tất cả danh sách lịc học phạm vi áp dụng là LỚP (lớp nào cũng được)
                    $results['menu'] = $menuDao->getAllMenuByLevel($school_id, $level);
                } else {
                    $results['menu'] = $menuDao->getMenuById($school_id, $class_level_id, $class_id);
                }
            }
            $results['username'] = $school['page_name'];
            $smarty->assign('results', $results);

            //$class_levels = $classLevelDao->getClassLevels($school_id);
            $class_levels = getSchoolData($school_id, SCHOOL_CLASS_LEVELS);
            //$classes = $classDao->getClassesOfSchool($school_id);
            $classes = getSchoolData($school_id, SCHOOL_CLASSES);

            $smarty->assign('class_levels', $class_levels);
            $smarty->assign('classes', $classes);
            $smarty->assign('canEdit', canEdit($_POST['school_username'], 'menus'));

            $return['results'] = $smarty->fetch("ci/ajax.menulist.tpl");
            $return['no_data'] = (count($results) == 0);
            return_json($return);
            break;
        case 'notify':
            $db->begin_transaction();
            // Lấy thông tin lịch học
            $data = $menuDao->getMenuDetailById($_POST['id']);
            if (is_null($data)) {
                _error(404);
            }
            if (!$data['is_notified']) {
                // Thông báo tới phụ huynh và giáo viên
                if($data['applied_for'] == CLASS_LEVEL) {
                    // Danh sách trẻ trong lớp
                    //$childrenIds = $childDao->getChildIdOfClass($data['class_id']);
                    $children = getClassData($data['class_id'], CLASS_CHILDREN);
                    $childrenIds = array_keys($children);
                    //$class = $classDao->getClass($data['class_id']);
                    $class = getClassData($data['class_id'], CLASS_INFO);

                    //Thông báo tới phụ huynh có trẻ trong lớp được tạo lịch học.
                    if (count($childrenIds) > 0) {
                        $parents = $childDao->getChildNParentIdForMemcache($childrenIds);
                        if(count($parents) > 0) {
                            foreach ($parents as $parent) {
                                $userDao->postNotifications($parent['parent_id'], NOTIFICATION_NEW_MENU, NOTIFICATION_NODE_TYPE_CHILD,
                                    $data['menu_id'], convertText4Web($data['menu_name']), $parent['child_id'], convertText4Web($class['group_title']));
                            }
                        }
                        //Thông báo tới giáo viên của lớp.
                        //$teachers = $teacherDao->getTeacherOfClass($class['group_id']);
                        $teachers = getClassData($class['group_id'], CLASS_TEACHERS);
                        if(count($teachers) > 0) {
                            $teacherIds = array_keys($teachers);
                            $userDao->postNotifications($teacherIds, NOTIFICATION_NEW_MENU, NOTIFICATION_NODE_TYPE_CLASS,
                                $data['menu_id'], convertText4Web($data['menu_name']), $class['group_name'], convertText4Web($class['group_title']));
                        }
                        // Thông báo đến các quản lý trường khác
                        $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'menus', $school['page_admin']);
                        $userDao->postNotifications($userManagerIds, NOTIFICATION_NEW_MENU, NOTIFICATION_NODE_TYPE_SCHOOL,
                            $data['menu_id'], convertText4Web($data['menu_name']), $school['page_name'], convertText4Web($class['group_title']));
                    }
                } elseif ($data['applied_for'] == CLASS_LEVEL_LEVEL) {
                    // Danh sách trẻ trong khối
                    $childrenIds = $childDao->getChildIdByClassLevel($data['class_level_id']);
                    //$class_level = $classLevelDao->getClassLevel($data['class_level_id']);
                    $class_level = getClassLevelData($data['class_level_id'], CLASS_LEVEL_INFO);

                    //Thông báo tới phụ huynh có trẻ trong lớp được tạo lịch học.
                    if (count($childrenIds) > 0) {
                        $parents = $childDao->getChildNParentIdForMemcache($childrenIds);
                        if(count($parents) > 0) {
                            foreach ($parents as $parent) {
                                $userDao->postNotifications($parent['parent_id'], NOTIFICATION_NEW_MENU, NOTIFICATION_NODE_TYPE_CHILD,
                                    $data['menu_id'], convertText4Web($data['menu_name']), $parent['child_id'], convertText4Web($class_level['class_level_name']));
                            }
                        }

                        //Thông báo tới giáo viên của lớp.
                        //$classes = $classDao->getClassesOfLevel($data['class_level_id']);
                        $classes = getClassLevelData($data['class_level_id'], CLASS_LEVEL_CLASSES);
                        foreach ($classes as $class) {
                            //$teachers = $teacherDao->getTeacherOfClass($class['group_id']);
                            $teachers = getClassData($class['group_id'], CLASS_TEACHERS);
                            if(count($teachers) > 0) {
                                $teacherIds = array_keys($teachers);
                                $userDao->postNotifications($teacherIds, NOTIFICATION_NEW_MENU, NOTIFICATION_NODE_TYPE_CLASS,
                                    $data['menu_id'], convertText4Web($data['menu_name']), $class['group_name'], convertText4Web($class_level['class_level_name']));
                            }
                        }
                        // Thông báo đến các quản lý trường khác
                        $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'menus', $school['page_admin']);
                        $userDao->postNotifications($userManagerIds, NOTIFICATION_NEW_MENU, NOTIFICATION_NODE_TYPE_SCHOOL,
                            $data['menu_id'], convertText4Web($data['menu_name']), $school['page_name'], convertText4Web($class_level['class_level_name']));
                    }
                } else {
                    // Danh sách trẻ trong trường
                    //$childrenIds = $childDao->getChildIdBySchool($data['school_id']);
                    $children = getSchoolData($data['school_id'], SCHOOL_CHILDREN);
                    $childrenIds = array_keys($children);

                    //Thông báo tới phụ huynh có trẻ trong lớp được tạo lịch học.
                    if (count($childrenIds) > 0) {
                        $parents = $childDao->getChildNParentIdForMemcache($childrenIds);
                        if(count($parents) > 0) {
                            foreach ($parents as $parent) {
                                $userDao->postNotifications($parent['parent_id'], NOTIFICATION_NEW_MENU, NOTIFICATION_NODE_TYPE_CHILD,
                                    $data['menu_id'], convertText4Web($data['menu_name']), $parent['child_id'], convertText4Web($school['page_title']));
                            }
                        }
                        //Thông báo tới giáo viên của lớp.
                        // Lấy danh sách lớp
                        //$classes = $classDao->getClassesOfSchool($data['school_id']);
                        $classes = getSchoolData($data['school_id'], SCHOOL_CLASSES);
                        foreach ($classes as $class) {
                            //$teachers = $teacherDao->getTeacherOfClass($class['group_id']);
                            $teachers = getClassData($class['group_id'], CLASS_TEACHERS);
                            if(count($teachers) > 0) {
                                $teacherIds = array_keys($teachers);
                                $userDao->postNotifications($teacherIds, NOTIFICATION_NEW_MENU, NOTIFICATION_NODE_TYPE_CLASS,
                                    $data['menu_id'], convertText4Web($data['menu_name']), $class['group_name'], convertText4Web($school['page_title']));
                            }
                        }
                        // Thông báo đến các quản lý trường khác
                        $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'menus', $school['page_admin']);
                        $userDao->postNotifications($userManagerIds, NOTIFICATION_NEW_MENU, NOTIFICATION_NODE_TYPE_SCHOOL,
                            $data['menu_id'], convertText4Web($data['menu_name']), $school['page_name'], convertText4Web($school['page_title']));
                    }
                }
                // Cập nhật trạng thái đã gửi thông báo.
                $menuDao->updateStatusToNotified($_POST['id']);
            }

            $db->commit();
            return_json( array('success' => true, 'message' => __("Success")));
            break;
        case 'import':
            if (!canEdit($_POST['school_username'], 'menus')) {
                _error(403);
            }
            include_once(ABSPATH . 'includes/libs/PHPExcel/PHPExcel/IOFactory.php');
            include_once(ABSPATH . 'includes/ajax/ci/utilities.php');

            $file_name = $_FILES['file']['name'];
            $inputFileName = $_FILES['file']['tmp_name'];

            $results = array();
            $sheet = readMenuInfoInExcelFile($inputFileName);
            // Lấy ra các thông tin của menu
            $args = array();
            $args['menu_name'] = $_POST['menu_name'];
            $args['begin'] = $_POST['begin'];
            $args['description'] = $_POST['description'];
            $args['school_id'] = $school['page_id'];
            $args['class_level_id'] = $_POST['class_level_id'];
            $args['class_id'] = $_POST['class_id'];
            $args['is_meal'] = (isset($_POST['use_mea']) && $_POST['use_mea'] == 'on') ? 1 : 0;
            $args['is_saturday'] = (isset($_POST['study_saturday']) && $_POST['study_saturday'] == 'on') ? 1 : 0;
            $args['is_notified'] = (isset($_POST['notify_immediately']) && $_POST['notify_immediately'] == 'on')? 1: 0;
            $level = $_POST['level'];
            if($level == 2 && $args['class_level_id'] == 0) {
                throw new Exception(__("You must select a class level"));
            } else if($level == 3 && $args['class_id'] == 0) {
                throw new Exception(__("You must select a class"));
            }
            $args['applied_for'] = 0;
            if($args['class_id'] > 0) {
                $args['applied_for'] = CLASS_LEVEL;
            }
            if(($args['class_id'] == 0) && $args['class_level_id'] > 0) {
                $args['applied_for'] = CLASS_LEVEL_LEVEL;
            }
            if($args['class_id'] == 0 && $args['class_level_id'] == 0) {
                $args['applied_for'] = SCHOOL_LEVEL;
            }
            if ($sheet['error'] == 0) {

                $db->begin_transaction();

                // 1. Insert menu vào bảng ci_menu
                $menuId = $menuDao->insertMenu($args);

                // 2. Insert vào bảng ci_menu_detail
                $new_menu_list = array();
                $succes = 1;
                foreach ($sheet['menu_list'] as $subject) { //Duyệt danh sách bữa ăn
                    if ($subject['error'] == 0) {
                        try {
                            // Thêm menu_id vào $subject
                            $subject['menu_id'] = $menuId;
                            //1. Tạo thông tin chi tiết lịch học trong bảng ci_menu_detail
                            if($args['is_meal'] && (is_null($subject['start']) || is_null($subject['meal']))) {
                                throw new Exception(__('Please enter the full information'));
                            }
                            $lastId = $menuDao->insertMenuDetailImport($subject);

                            $db->commit();
                        } catch (Exception $e) {
                            $db->rollback();
                            $subject['error'] = 1;
                            $subject['message'] = $e->getMessage();
                        } finally {
                            $db->autocommit(true);
                        }
                    } else {
                        $succes = 0;
                    }
                    $new_menu_list[] = $subject;
                }
                if($succes == 1) {
                    // Thông báo tới các đối tượng liên quan
                    if($args['is_notified']) {
                        if($args['applied_for'] == CLASS_LEVEL) {
                            // Danh sách trẻ trong lớp
                            //$childrenIds = $childDao->getChildIdOfClass($args['class_id']);
                            $children = getClassData($args['class_id'], CLASS_CHILDREN);
                            $childrenIds = array_keys($children);
                            //$class = $classDao->getClass($args['class_id']);
                            $class = getClassData($args['class_id'], CLASS_INFO);

                            //Thông báo tới phụ huynh có trẻ trong lớp được tạo lịch học.
                            if (count($childrenIds) > 0) {
                                $parents = $childDao->getListChildNParentId($childrenIds);
                                if(count($parents) > 0) {
                                    foreach ($parents as $parent) {
                                        $userDao->postNotifications($parent['parent_id'], NOTIFICATION_NEW_MENU, NOTIFICATION_NODE_TYPE_CHILD,
                                            $menuId, convertText4Web($_POST['menu_name']), $parent['child_id'], convertText4Web($class['group_title']));
                                    }
                                }

                                //Thông báo tới giáo viên của lớp.
                                //$teachers = $teacherDao->getTeacherOfClass($class['group_id']);
                                $teachers = getClassData($class['group_id'], CLASS_TEACHERS);
                                if(count($teachers) > 0) {
                                    $teacherIds = array_keys($teachers);
                                    $userDao->postNotifications($teacherIds, NOTIFICATION_NEW_MENU, NOTIFICATION_NODE_TYPE_CLASS,
                                        $menuId, convertText4Web($_POST['menu_name']), $class['group_name'], convertText4Web($class['group_title']));
                                }
                                // Thông báo đến các quản lý trường khác
                                $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'menus', $school['page_admin']);
                                $userDao->postNotifications($userManagerIds, NOTIFICATION_NEW_MENU, NOTIFICATION_NODE_TYPE_SCHOOL,
                                    $menuId, convertText4Web($_POST['menu_name']), $school['page_name'], convertText4Web($class['group_title']));
                            }
                        } elseif ($args['applied_for'] == CLASS_LEVEL_LEVEL) {
                            // Danh sách trẻ trong khối
                            $childrenIds = $childDao->getChildIdByClassLevel($args['class_level_id']);
                            //$class_level = $classLevelDao->getClassLevel($args['class_level_id']);
                            $class_level = getClassLevelData($args['class_level_id'], CLASS_LEVEL_INFO);

                            //Thông báo tới phụ huynh có trẻ trong lớp được tạo lịch học.
                            if (count($childrenIds) > 0) {
                                $parents = $childDao->getListChildNParentId($childrenIds);
                                foreach ($parents as $parent) {
                                    $userDao->postNotifications($parent['parent_id'], NOTIFICATION_NEW_MENU, NOTIFICATION_NODE_TYPE_CHILD,
                                        $menuId, convertText4Web($_POST['menu_name']), $parent['child_id'], convertText4Web($class_level['class_level_name']));
                                }
                                //Thông báo tới giáo viên của lớp.
                                //$classes = $classDao->getClassesOfLevel($args['class_level_id']);
                                $classes = getClassLevelData($args['class_level_id'], CLASS_LEVEL_CLASSES);
                                foreach ($classes as $class) {
                                    //$teachers = $teacherDao->getTeacherOfClass($class['group_id']);
                                    $teachers = getClassData($class['group_id'], CLASS_TEACHERS);
                                    if(count($teachers) > 0) {
                                        $teacherIds = array_keys($teachers);
                                        $userDao->postNotifications($teacherIds, NOTIFICATION_NEW_MENU, NOTIFICATION_NODE_TYPE_CLASS,
                                            $menuId, convertText4Web($_POST['schedule_name']), $class['group_name'], convertText4Web($class_level['class_level_name']));
                                    }
                                }
                                // Thông báo đến các quản lý trường khác
                                $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'menus', $school['page_admin']);
                                $userDao->postNotifications($userManagerIds, NOTIFICATION_NEW_MENU, NOTIFICATION_NODE_TYPE_SCHOOL,
                                    $menuId, convertText4Web($_POST['menu_name']), $school['page_name'], convertText4Web($class_level['class_level_name']));
                            }
                        } else {
                            // Danh sách trẻ trong trường
                            //$childrenIds = $childDao->getChildIdBySchool($args['school_id']);
                            $children = getSchoolData($args['school_id'], SCHOOL_CHILDREN);
                            $childrenIds = array_keys($children);

                            //Thông báo tới phụ huynh có trẻ trong lớp được tạo lịch học.
                            if (count($childrenIds) > 0) {
                                $parents = $childDao->getListChildNParentId($childrenIds);
                                if(count($parents) > 0) {
                                    foreach ($parents as $parent) {
                                        $userDao->postNotifications($parent['parent_id'], NOTIFICATION_NEW_MENU, NOTIFICATION_NODE_TYPE_CHILD,
                                            $menuId, convertText4Web($_POST['menu_name']), $parent['child_id'], convertText4Web($school['page_title']));
                                    }
                                }
                                //Thông báo tới giáo viên của lớp.
                                // Lấy danh sách lớp
                                //$classes = $classDao->getClassesOfSchool($args['school_id']);
                                $classes = getSchoolData($args['school_id'], SCHOOL_CLASSES);
                                foreach ($classes as $class) {
                                    //$teachers = $teacherDao->getTeacherOfClass($class['group_id']);
                                    $teachers = getClassData($class['group_id'], CLASS_TEACHERS);
                                    if(count($teachers) > 0) {
                                        $teacherIds = array_keys($teachers);
                                        $userDao->postNotifications($teacherIds, NOTIFICATION_NEW_MENU, NOTIFICATION_NODE_TYPE_CLASS,
                                            $menuId, convertText4Web($_POST['menu_name']), $class['group_name'], convertText4Web($school['page_title']));
                                    }
                                }
                                // Thông báo đến các quản lý trường khác
                                $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'schedules', $school['page_admin']);
                                $userDao->postNotifications($userManagerIds, NOTIFICATION_NEW_MENU, NOTIFICATION_NODE_TYPE_SCHOOL,
                                    $menuId, convertText4Web($_POST['menu_name']), $school['page_name'], convertText4Web($school['page_title']));
                            }
                        }
                        /* Coniu - Tương tác trường */
                        $menuIds[] = $menuId;
                        setIsAddNew($school['page_id'], 'menu_created', $menuIds);
                        /* Coniu - END */
                    }
                }
                // Tăng lượt thêm mới
                addInteractive($school['page_id'], 'schedule', 'school_view', $scheduleId, 1);

            }
            $sheet['menu_list'] = $new_menu_list;
            $smarty->assign("sheet", $sheet);
            $results['results'] = $smarty->fetch("ci/school/ajax.menu.importresult.tpl");
            return_json($results);
            break;
        case 'preview':
            $results = array();
            $results['menu_name'] = $_POST['menu_name'];
            $results['applied_for'] = $_POST['menu_level'];
            $results['begin'] = $_POST['menu_begin'];
            $results['class_level_id'] = $_POST['class_level_id'];
            $results['class_id'] = $_POST['class_id'];
            $results['description'] = $_POST['menu_description'];
            $results['is_meal'] = $_POST['menu_meal'] == 'on'? 1 : 0;
            $results['is_saturday'] = $_POST['menu_saturday'] == 'on'? 1 : 0;

            $results['menu_name'] = trim($results['menu_name']);
            if($results['menu_name'] == '') {
                throw new Exception(__("You must enter menu name"));
            }

            if($results['applied_for'] == 2 && $results['class_level_id'] == 0) {
                throw new Exception(__("You must select a class level"));
            } else if($results['applied_for'] == 3 && $results['class_id'] == 0) {
                throw new Exception(__("You must select a class"));
            }

            $begin = toDBDate($results['begin']);
            date_default_timezone_set('Asia/Ho_Chi_Minh');
            $dateBegin = strtotime($begin);
            $weekday =  strtolower(date('D', $dateBegin));
            if(!($weekday === "mon")) {
                throw new Exception(__("Start date must be Monday"));
            }
            global $db;
            $begin = toDBDate($results['begin']);
            $strSql = sprintf("SELECT * FROM ci_menu WHERE school_id = %s AND class_level_id = %s AND class_id = %s AND begin = %s", secure($school['page_id'], 'int'), secure($results['class_level_id'], 'int'), secure($results['class_id'], 'int'), secure($begin));

            $getSchedule = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
            if($getSchedule->num_rows > 0) {
                throw new Exception(__("There are menu for this week, please check again!"));
            }

            if(count($_POST['startList']) == 0) {
                throw new Exception(__("Please enter menu's information"));
            }

            if($results['is_meal']) {
                $_POST['mealList'] = array_map("trim", $_POST['mealList']);
                $_POST['mealList'] = array_diff($_POST['mealList'], array(""));
            }
            if($results['is_meal'] && (count($_POST['startList']) != count($_POST['mealList']))) {
                throw new Exception(__('Please enter the full information'));
            }

            $details = array();
            foreach ($_POST['startList'] as $k => $row) {
                $detail = array();
                $detail['meal_name'] = $_POST['mealList'][$k];
                $detail['meal_time'] = $row;
                $detail['monday'] = $_POST['monList'][$k];
                $detail['tuesday'] = $_POST['tueList'][$k];
                $detail['wednesday'] = $_POST['wedList'][$k];
                $detail['thursday'] = $_POST['thuList'][$k];
                $detail['friday'] = $_POST['friList'][$k];
                $detail['saturday'] = $_POST['satList'][$k];
                $details[] = $detail;
            }
            $results['details'] = $details;

            $smarty->assign('data', $results);

            //$class_levels = $classLevelDao->getClassLevels($class['school_id']);
            $class_levels = getSchoolData($school['page_id'], SCHOOL_CLASS_LEVELS);
            $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
            if($results['applied_for'] == 2) {
                foreach ($class_levels as $row) {
                    if($row['class_level_id'] == $results['class_level_id']) {
                        $smarty->assign('class_level', $row);
                    }
                }
            }
            if($results['applied_for'] == 3) {
                foreach ($classes as $row) {
                    if($row['group_id'] == $results['class_id']) {
                        $smarty->assign('class', $row);
                    }
                }
            }

            $return['results'] = $smarty->fetch("ci/school/ajax.school.menu.preview.tpl");
            return_json($return);
            break;
        case 'preview_excel':
            if (!canEdit($_POST['school_username'], 'menus')) {
                _error(403);
            }
            include_once(ABSPATH . 'includes/libs/PHPExcel/PHPExcel/IOFactory.php');
            include_once(ABSPATH . 'includes/ajax/ci/utilities.php');

            if($_FILES['file']['tmp_name'] == '') {
                throw new Exception(__("You have not chosen your file"));
            }
            $file_name = $_FILES['file']['name'];
            $inputFileName = $_FILES['file']['tmp_name'];

            $sheet = readMenuInfoInExcelFile($inputFileName);
            // Lấy ra các thông tin của lịch học
            $args = array();
            $args['menu_name'] = $_POST['menu_name'];
            $args['begin'] = $_POST['begin'];
            $args['description'] = $_POST['description'];
            $args['school_id'] = $school['page_id'];
            $args['class_level_id'] = $_POST['class_level_id'];
            $args['class_id'] = $_POST['class_id'];
            $args['is_meal'] = (isset($_POST['use_mea']) && $_POST['use_mea'] == 'on') ? 1 : 0;
            $args['is_saturday'] = (isset($_POST['study_saturday']) && $_POST['study_saturday'] == 'on') ? 1 : 0;

            $args['applied_for'] = 0;
            if($args['class_id'] > 0) {
                $args['applied_for'] = CLASS_LEVEL;
            }
            if(($args['class_id'] == 0) && $args['class_level_id'] > 0) {
                $args['applied_for'] = CLASS_LEVEL_LEVEL;
            }
            if($args['class_id'] == 0 && $args['class_level_id'] == 0) {
                $args['applied_for'] = SCHOOL_LEVEL;
            }
            if ($sheet['error'] == 0) {

                $args['menu_name'] = trim($args['menu_name']);
                if($args['menu_name'] == '') {
                    throw new Exception(__("You must enter menu name"));
                }

                $begin = toDBDate($args['begin']);
                date_default_timezone_set('Asia/Ho_Chi_Minh');
                $dateBegin = strtotime($begin);
                $weekday =  strtolower(date('D', $dateBegin));
                if(!($weekday === "mon")) {
                    throw new Exception(__("Start date must be Monday"));
                }
                global $db;
                $begin = toDBDate($args['begin']);
                $strSql = sprintf("SELECT * FROM ci_menu WHERE school_id = %s AND class_level_id = %s AND class_id = %s AND begin = %s", secure($school['page_id'], 'int'), secure($args['class_level_id'], 'int'), secure($args['class_id'], 'int'), secure($begin));

                $getMenu = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
                if($getMenu->num_rows > 0) {
                    throw new Exception(__("There are menu for this week, please check again!"));
                }

                $details = array();
                foreach ($sheet['menu_list'] as $row) {
                    if($row['error'] == 0) {
                        $detail = array();
                        $detail['meal_name'] = $row['meal'];
                        $detail['meal_time'] = $row['start'];
                        $detail['monday'] = $row['mon'];
                        $detail['tuesday'] = $row['tue'];
                        $detail['wednesday'] = $row['wed'];
                        $detail['thursday'] = $row['thu'];
                        $detail['friday'] = $row['fri'];
                        $detail['saturday'] = $row['sat'];
                        $details[] = $detail;
                    }
                }

                $args['details'] = $details;

                $smarty->assign('data', $args);

                $class_levels = getSchoolData($school['page_id'], SCHOOL_CLASS_LEVELS);
                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                if($args['applied_for'] == 2) {
                    foreach ($class_levels as $row) {
                        if($row['class_level_id'] == $args['class_level_id']) {
                            $smarty->assign('class_level', $row);
                        }
                    }
                }
                if($args['applied_for'] == 3) {
                    foreach ($classes as $row) {
                        if($row['group_id'] == $args['class_id']) {
                            $smarty->assign('class', $row);
                        }
                    }
                }

                $return['results'] = $smarty->fetch("ci/school/ajax.school.menu.preview.tpl");
                return_json($return);
            } else {
                throw new Exception($sheet['message']);
            }
            break;
        case 'export':
            $A4_MENU_TEMPLATE_MEAL_SAT = ABSPATH . "content/templates/menu_A4_meal_sat_" . $system['language']['code'] . ".xlsx";
            $A4_MENU_TEMPLATE = ABSPATH . "content/templates/menu_A4_" . $system['language']['code'] . ".xlsx";
            $A4_MENU_TEMPLATE_MEAL = ABSPATH . "content/templates/menu_A4_meal_" . $system['language']['code'] . ".xlsx";
            $A4_MENU_TEMPLATE_SAT = ABSPATH . "content/templates/menu_A4_sat_" . $system['language']['code'] . ".xlsx";
            include_once(ABSPATH . 'includes/libs/PHPExcel/PHPExcel/IOFactory.php');
            include_once(ABSPATH . 'includes/libs/PHPExcel/PHPExcel.php');

            // Khởi tạo đối tượng
            $objPHPExcel = new PHPExcel();
            // Lấy thông tin lịch học
            $data = $menuDao->getMenuDetailById($_POST['id']);
            if (is_null($data)) {
                _error(404);
            }

            if($data['applied_for'] == 2) {
                //$class_level = $classLevelDao->getClassLevel($data['class_level_id']);
                $class_level = getClassLevelData($data['class_level_id'], CLASS_LEVEL_INFO);
            }

            if($data['applied_for'] == 3) {
                //$class = $classDao->getClass($data['class_id']);
                $class = getClassData($data['class_id'], CLASS_INFO);
            }
            if($data['is_meal'] && $data['is_saturday']) {
                $objPHPExcel = PHPExcel_IOFactory::load($A4_MENU_TEMPLATE_MEAL_SAT);
                $firstSheet = $objPHPExcel->getSheet(0);
                $firstSheet->setCellValue('A1', mb_strtoupper(convertText4Web(str_replace('/', '_', $school['page_title'])), "UTF-8"))
                    ->setCellValue('A2', mb_strtoupper(convertText4Web(str_replace('/', '_', __("Menu name").": " . $data['menu_name'])), "UTF-8"));
                if($data['applied_for'] == 3) {
                    $firstSheet->setCellValue('A3', __("Scope") . ": " . convertText4Web($class['group_title']));
                } elseif ($data['applied_for'] == 2) {
                    $firstSheet->setCellValue('A3', __("Scope") . ": " . convertText4Web($class_level['class_level_name']));
                } else {
                    $firstSheet->setCellValue('A3', __("Scope") . ": " . __("School"));
                }

                $rowNumber = 6;

                foreach ($data['details'] as $detail) {
                    $array = array_values($detail);
                    $temp = array();
                    foreach ($array as $k => $value) {
                        if ($data['is_saturday']) {
                            if (($k >= 4) && ($k < (count($array)))) {
                                $temp[] = $value;
                            }
                        } else {
                            if (($k >= 4) && ($k < (count($array) - 1))) {
                                $temp[] = $value;
                            }
                        }
                    }

                    $colArray = array(
                        0 => "C",
                        1 => "D",
                        2 => "E",
                        3 => "F",
                        4 => "G",
                        5 => "H",
                    );
                    $conArray = array(
                        0 => $detail['monday'],
                        1 => $detail['tuesday'],
                        2 => $detail['wednesday'],
                        3 => $detail['thursday'],
                        4 => $detail['friday'],
                        5 => $detail['saturday'],
                    );

                    $firstSheet->setCellValue('A' . $rowNumber, convertText4Web($detail['meal_time']))
                        ->setCellValue('B' . $rowNumber, convertText4Web($detail['meal_name']));

                    for ($i = 0; $i < count($temp); $i++) {
                        for($j = $i + 1; $j < count($temp) + 1; $j++) {
                            if(($temp[$i] != $temp[$j])) {
                                if($j == $i + 1) {
                                    $firstSheet->setCellValue($colArray[$i] . $rowNumber, convertText4Web($conArray[$i]), PHPExcel_Cell_DataType::TYPE_STRING);
                                    $i = $j;
                                } else {
                                    $firstSheet->mergeCells($colArray[$i] . $rowNumber . ':' . $colArray[$j -1] . $rowNumber);
                                    $firstSheet->setCellValue($colArray[$i] . $rowNumber, convertText4Web($conArray[$i]), PHPExcel_Cell_DataType::TYPE_STRING);
                                    // Tính số ô được merge
                                    $k = $j - $i;
                                    // chiều rộng ô được merge
                                    $widthCol = $k * 18;
                                    // Set giá trị Y . $rowNumber = giá trị ô được merge
                                    $firstSheet->setCellValue('Y' . $rowNumber, convertText4Web($conArray[$i]), PHPExcel_Cell_DataType::TYPE_STRING);
                                    // Set width Y
                                    $firstSheet->getColumnDimension('Y')->setWidth($widthCol);
                                    // Set wrap text Y .$rowNumber
                                    $firstSheet->getStyle('Y' . $rowNumber)->getAlignment()->setWrapText(true);
                                    // Set autoFit row height Y.$rowNumber
                                    //$firstSheet->getRowDimension($rowNumber)->setRowHeight(-1);
                                    $firstSheet->getColumnDimension('Y')->setVisible(false);
                                    $i = $j;
                                }
                            }
                        }
                    }
                    $firstSheet->getRowDimension($rowNumber)->setRowHeight(-1);
                    $rowNumber++;
                }
//            if(!$data['is_saturday']) {
//                $firstSheet->removeColumn("H");
//            }
//            if(!$data['is_category']) {
//                $firstSheet->removeColumn("B");
//            }

                $numRow = $rowNumber - 1;

                // Set style cho thời gian và hoạt động
                $firstSheet->getStyle('A6:B'.$numRow)->applyFromArray(styleArray('Times new Roman', true, false, 11, PHPExcel_Style_Alignment::HORIZONTAL_CENTER, PHPExcel_Style_Alignment::VERTICAL_CENTER, true));

                // Set style cho nội dung
                $firstSheet->getStyle('C6:H' . $numRow)->applyFromArray(styleArray('Times new Roman', false, false, 11, PHPExcel_Style_Alignment::HORIZONTAL_CENTER, PHPExcel_Style_Alignment::VERTICAL_TOP, true));

                $firstSheet->getStyle('C6:H' . $rowNumber)->getAlignment()->setWrapText(true);
//            //$firstSheet->getDefaultRowDimension()->se‌​tRowHeight(-1);
//            $firstSheet->getRowDimension('7')->setRowHeight(-1);
//            foreach($firstSheet->getRowDimensions() as $rd) {
//                $rd->setRowHeight(-1);
//            }
                //Set border cho toàn bộ bảng
                $all_border_style = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('rgb' => '333333'))));
                $firstSheet->getStyle('A5:H' . $numRow)->applyFromArray($all_border_style);

                $firstSheet->setBreak( 'I'.$numRow , PHPExcel_Worksheet::BREAK_COLUMN );
                $firstSheet->getPageSetup()->setPrintArea('A1:H' . $numRow);
            } elseif ($data['is_meal'] && !$data['is_saturday']) {
                $objPHPExcel = PHPExcel_IOFactory::load($A4_MENU_TEMPLATE_MEAL);
                $firstSheet = $objPHPExcel->getSheet(0);
                $firstSheet->setCellValue('A1', mb_strtoupper(convertText4Web(str_replace('/', '_', $school['page_title'])), "UTF-8"))
                    ->setCellValue('A2', mb_strtoupper(convertText4Web(str_replace('/', '_', __("Menu name").": " . $data['menu_name'])), "UTF-8"));
                if($data['applied_for'] == 3) {
                    $firstSheet->setCellValue('A3', __("Scope") . ": " . convertText4Web($class['group_title']));
                } elseif ($data['applied_for'] == 2) {
                    $firstSheet->setCellValue('A3', __("Scope") . ": " . convertText4Web($class_level['class_level_name']));
                } else {
                    $firstSheet->setCellValue('A3', __("Scope") . ": " . __("School"));
                }

                $rowNumber = 6;

                foreach ($data['details'] as $detail) {
                    $array = array_values($detail);
                    $temp = array();
                    foreach ($array as $k => $value) {
                        if ($data['is_saturday']) {
                            if (($k >= 4) && ($k < (count($array)))) {
                                $temp[] = $value;
                            }
                        } else {
                            if (($k >= 4) && ($k < (count($array) - 1))) {
                                $temp[] = $value;
                            }
                        }
                    }

                    $colArray = array(
                        0 => "C",
                        1 => "D",
                        2 => "E",
                        3 => "F",
                        4 => "G",
                        5 => "H",
                    );
                    $conArray = array(
                        0 => $detail['monday'],
                        1 => $detail['tuesday'],
                        2 => $detail['wednesday'],
                        3 => $detail['thursday'],
                        4 => $detail['friday'],
                        5 => $detail['saturday'],
                    );

                    $firstSheet->setCellValue('A' . $rowNumber, convertText4Web($detail['meal_time']))
                        ->setCellValue('B' . $rowNumber, convertText4Web($detail['meal_name']));

                    for ($i = 0; $i < count($temp); $i++) {
                        for($j = $i + 1; $j < count($temp) + 1; $j++) {
                            if(($temp[$i] != $temp[$j])) {
                                if($j == $i + 1) {
                                    $firstSheet->setCellValue($colArray[$i] . $rowNumber, convertText4Web($conArray[$i]), PHPExcel_Cell_DataType::TYPE_STRING);
                                    $i = $j;
                                } else {
                                    $firstSheet->mergeCells($colArray[$i] . $rowNumber . ':' . $colArray[$j -1] . $rowNumber);
                                    $firstSheet->setCellValue($colArray[$i] . $rowNumber, convertText4Web($conArray[$i]), PHPExcel_Cell_DataType::TYPE_STRING);
                                    // Tính số ô được merge
                                    $k = $j - $i;
                                    // chiều rộng ô được merge
                                    $widthCol = $k * 18;
                                    // Set giá trị Y . $rowNumber = giá trị ô được merge
                                    $firstSheet->setCellValue('Y' . $rowNumber, convertText4Web($conArray[$i]), PHPExcel_Cell_DataType::TYPE_STRING);
                                    // Set width Y
                                    $firstSheet->getColumnDimension('Y')->setWidth($widthCol);
                                    // Set wrap text Y .$rowNumber
                                    $firstSheet->getStyle('Y' . $rowNumber)->getAlignment()->setWrapText(true);
                                    // Set autoFit row height Y.$rowNumber
                                    //$firstSheet->getRowDimension($rowNumber)->setRowHeight(-1);
                                    $firstSheet->getColumnDimension('Y')->setVisible(false);
                                    $i = $j;
                                }
                            }
                        }
                    }
                    $firstSheet->getRowDimension($rowNumber)->setRowHeight(-1);
                    $rowNumber++;
                }
//            if(!$data['is_saturday']) {
//                $firstSheet->removeColumn("H");
//            }
//            if(!$data['is_category']) {
//                $firstSheet->removeColumn("B");
//            }

                $numRow = $rowNumber - 1;

                // Set style cho thời gian và hoạt động
                $firstSheet->getStyle('A6:B'.$numRow)->applyFromArray(styleArray('Times new Roman', true, false, 11, PHPExcel_Style_Alignment::HORIZONTAL_CENTER, PHPExcel_Style_Alignment::VERTICAL_CENTER, true));

                // Set style cho nội dung
                $firstSheet->getStyle('C6:G' . $numRow)->applyFromArray(styleArray('Times new Roman', false, false, 11, PHPExcel_Style_Alignment::HORIZONTAL_CENTER, PHPExcel_Style_Alignment::VERTICAL_TOP, true));

                $firstSheet->getStyle('C6:G' . $rowNumber)->getAlignment()->setWrapText(true);
//            //$firstSheet->getDefaultRowDimension()->se‌​tRowHeight(-1);
//            $firstSheet->getRowDimension('7')->setRowHeight(-1);
//            foreach($firstSheet->getRowDimensions() as $rd) {
//                $rd->setRowHeight(-1);
//            }
                //Set border cho toàn bộ bảng
                $all_border_style = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('rgb' => '333333'))));
                $firstSheet->getStyle('A5:G' . $numRow)->applyFromArray($all_border_style);

                $firstSheet->setBreak( 'I'.$numRow , PHPExcel_Worksheet::BREAK_COLUMN );
                $firstSheet->getPageSetup()->setPrintArea('A1:G' . $numRow);
            }  elseif ($data['is_saturday'] && !$data['is_meal']) {
                $objPHPExcel = PHPExcel_IOFactory::load($A4_MENU_TEMPLATE_SAT);
                $firstSheet = $objPHPExcel->getSheet(0);
                $firstSheet->setCellValue('A1', mb_strtoupper(convertText4Web(str_replace('/', '_', $school['page_title'])), "UTF-8"))
                    ->setCellValue('A2', mb_strtoupper(convertText4Web(str_replace('/', '_', __("Menu name").": " . $data['menu_name'])), "UTF-8"));
                if($data['applied_for'] == 3) {
                    $firstSheet->setCellValue('A3', __("Scope") . ": " . convertText4Web($class['group_title']));
                } elseif ($data['applied_for'] == 2) {
                    $firstSheet->setCellValue('A3', __("Scope") . ": " . convertText4Web($class_level['class_level_name']));
                } else {
                    $firstSheet->setCellValue('A3', __("Scope") . ": " . __("School"));
                }

                $rowNumber = 6;

                foreach ($data['details'] as $detail) {
                    $array = array_values($detail);
                    $temp = array();
                    foreach ($array as $k => $value) {
                        if ($data['is_saturday']) {
                            if (($k >= 4) && ($k < (count($array)))) {
                                $temp[] = $value;
                            }
                        } else {
                            if (($k >= 4) && ($k < (count($array) - 1))) {
                                $temp[] = $value;
                            }
                        }
                    }

                    $colArray = array(
                        0 => "B",
                        1 => "C",
                        2 => "D",
                        3 => "E",
                        4 => "F",
                        5 => "G",
                    );
                    $conArray = array(
                        0 => $detail['monday'],
                        1 => $detail['tuesday'],
                        2 => $detail['wednesday'],
                        3 => $detail['thursday'],
                        4 => $detail['friday'],
                        5 => $detail['saturday'],
                    );

                    $firstSheet->setCellValue('A' . $rowNumber, convertText4Web($detail['meal_time']));

                    for ($i = 0; $i < count($temp); $i++) {
                        for($j = $i + 1; $j < count($temp) + 1; $j++) {
                            if(($temp[$i] != $temp[$j])) {
                                if($j == $i + 1) {
                                    $firstSheet->setCellValue($colArray[$i] . $rowNumber, convertText4Web($conArray[$i]), PHPExcel_Cell_DataType::TYPE_STRING);
                                    $i = $j;
                                } else {
                                    $firstSheet->mergeCells($colArray[$i] . $rowNumber . ':' . $colArray[$j -1] . $rowNumber);
                                    $firstSheet->setCellValue($colArray[$i] . $rowNumber, convertText4Web($conArray[$i]), PHPExcel_Cell_DataType::TYPE_STRING);
                                    // Tính số ô được merge
                                    $k = $j - $i;
                                    // chiều rộng ô được merge
                                    $widthCol = $k * 18;
                                    // Set giá trị Y . $rowNumber = giá trị ô được merge
                                    $firstSheet->setCellValue('Y' . $rowNumber, convertText4Web($conArray[$i]), PHPExcel_Cell_DataType::TYPE_STRING);
                                    // Set width Y
                                    $firstSheet->getColumnDimension('Y')->setWidth($widthCol);
                                    // Set wrap text Y .$rowNumber
                                    $firstSheet->getStyle('Y' . $rowNumber)->getAlignment()->setWrapText(true);
                                    // Set autoFit row height Y.$rowNumber
                                    //$firstSheet->getRowDimension($rowNumber)->setRowHeight(-1);
                                    $firstSheet->getColumnDimension('Y')->setVisible(false);
                                    $i = $j;
                                }
                            }
                        }
                    }
                    $firstSheet->getRowDimension($rowNumber)->setRowHeight(-1);
                    $rowNumber++;
                }
//            if(!$data['is_saturday']) {
//                $firstSheet->removeColumn("H");
//            }
//            if(!$data['is_category']) {
//                $firstSheet->removeColumn("B");
//            }

                $numRow = $rowNumber - 1;

                // Set style cho thời gian và hoạt động
                $firstSheet->getStyle('A6:A'.$numRow)->applyFromArray(styleArray('Times new Roman', true, false, 11, PHPExcel_Style_Alignment::HORIZONTAL_CENTER, PHPExcel_Style_Alignment::VERTICAL_CENTER, true));

                // Set style cho nội dung
                $firstSheet->getStyle('B6:G' . $numRow)->applyFromArray(styleArray('Times new Roman', false, false, 11, PHPExcel_Style_Alignment::HORIZONTAL_CENTER, PHPExcel_Style_Alignment::VERTICAL_TOP, true));

                $firstSheet->getStyle('B6:G' . $rowNumber)->getAlignment()->setWrapText(true);
//            //$firstSheet->getDefaultRowDimension()->se‌​tRowHeight(-1);
//            $firstSheet->getRowDimension('7')->setRowHeight(-1);
//            foreach($firstSheet->getRowDimensions() as $rd) {
//                $rd->setRowHeight(-1);
//            }
                //Set border cho toàn bộ bảng
                $all_border_style = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('rgb' => '333333'))));
                $firstSheet->getStyle('A5:G' . $numRow)->applyFromArray($all_border_style);

                $firstSheet->setBreak( 'I'.$numRow , PHPExcel_Worksheet::BREAK_COLUMN );
                $firstSheet->getPageSetup()->setPrintArea('A1:G' . $numRow);
            } elseif (!$data['is_saturday'] && !$data['is_meal']) {
                $objPHPExcel = PHPExcel_IOFactory::load($A4_MENU_TEMPLATE);
                $firstSheet = $objPHPExcel->getSheet(0);
                $firstSheet->setCellValue('A1', mb_strtoupper(convertText4Web(str_replace('/', '_', $school['page_title'])), "UTF-8"))
                    ->setCellValue('A2', mb_strtoupper(convertText4Web(str_replace('/', '_', __("Menu name").": " . $data['menu_name'])), "UTF-8"));
                if($data['applied_for'] == 3) {
                    $firstSheet->setCellValue('A3', __("Scope") . ": " . convertText4Web($class['group_title']));
                } elseif ($data['applied_for'] == 2) {
                    $firstSheet->setCellValue('A3', __("Scope") . ": " . convertText4Web($class_level['class_level_name']));
                } else {
                    $firstSheet->setCellValue('A3', __("Scope") . ": " . __("School"));
                }

                $rowNumber = 6;

                foreach ($data['details'] as $detail) {
                    $array = array_values($detail);
                    $temp = array();
                    foreach ($array as $k => $value) {
                        if ($data['is_saturday']) {
                            if (($k >= 4) && ($k < (count($array)))) {
                                $temp[] = $value;
                            }
                        } else {
                            if (($k >= 4) && ($k < (count($array) - 1))) {
                                $temp[] = $value;
                            }
                        }
                    }

                    $colArray = array(
                        0 => "B",
                        1 => "C",
                        2 => "D",
                        3 => "E",
                        4 => "F",
                        5 => "G",
                    );
                    $conArray = array(
                        0 => $detail['monday'],
                        1 => $detail['tuesday'],
                        2 => $detail['wednesday'],
                        3 => $detail['thursday'],
                        4 => $detail['friday'],
                        5 => $detail['saturday'],
                    );

                    $firstSheet->setCellValue('A' . $rowNumber, convertText4Web($detail['meal_time']));

                    for ($i = 0; $i < count($temp); $i++) {
                        for($j = $i + 1; $j < count($temp) + 1; $j++) {
                            if(($temp[$i] != $temp[$j])) {
                                if($j == $i + 1) {
                                    $firstSheet->setCellValue($colArray[$i] . $rowNumber, convertText4Web($conArray[$i]), PHPExcel_Cell_DataType::TYPE_STRING);
                                    $i = $j;
                                } else {
                                    $firstSheet->mergeCells($colArray[$i] . $rowNumber . ':' . $colArray[$j -1] . $rowNumber);
                                    $firstSheet->setCellValue($colArray[$i] . $rowNumber, convertText4Web($conArray[$i]), PHPExcel_Cell_DataType::TYPE_STRING);
                                    // Tính số ô được merge
                                    $k = $j - $i;
                                    // chiều rộng ô được merge
                                    $widthCol = $k * 18;
                                    // Set giá trị Y . $rowNumber = giá trị ô được merge
                                    $firstSheet->setCellValue('Y' . $rowNumber, convertText4Web($conArray[$i]), PHPExcel_Cell_DataType::TYPE_STRING);
                                    // Set width Y
                                    $firstSheet->getColumnDimension('Y')->setWidth($widthCol);
                                    // Set wrap text Y .$rowNumber
                                    $firstSheet->getStyle('Y' . $rowNumber)->getAlignment()->setWrapText(true);
                                    // Set autoFit row height Y.$rowNumber
                                    //$firstSheet->getRowDimension($rowNumber)->setRowHeight(-1);
                                    $firstSheet->getColumnDimension('Y')->setVisible(false);
                                    $i = $j;
                                }
                            }
                        }
                    }
                    $firstSheet->getRowDimension($rowNumber)->setRowHeight(-1);
                    $rowNumber++;
                }
//            if(!$data['is_saturday']) {
//                $firstSheet->removeColumn("H");
//            }
//            if(!$data['is_category']) {
//                $firstSheet->removeColumn("B");
//            }

                $numRow = $rowNumber - 1;

                // Set style cho thời gian và hoạt động
                $firstSheet->getStyle('A6:A'.$numRow)->applyFromArray(styleArray('Times new Roman', true, false, 11, PHPExcel_Style_Alignment::HORIZONTAL_CENTER, PHPExcel_Style_Alignment::VERTICAL_CENTER, true));

                // Set style cho nội dung
                $firstSheet->getStyle('B6:F' . $numRow)->applyFromArray(styleArray('Times new Roman', false, false, 11, PHPExcel_Style_Alignment::HORIZONTAL_CENTER, PHPExcel_Style_Alignment::VERTICAL_TOP, true));

                $firstSheet->getStyle('B6:F' . $rowNumber)->getAlignment()->setWrapText(true);

                //Set border cho toàn bộ bảng
                $all_border_style = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('rgb' => '333333'))));
                $firstSheet->getStyle('A5:F' . $numRow)->applyFromArray($all_border_style);

                $firstSheet->setBreak( 'I'.$numRow , PHPExcel_Worksheet::BREAK_COLUMN );
                $firstSheet->getPageSetup()->setPrintArea('A1:F' . $numRow);
            }

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            ob_start();
            // $month = str_replace('/', '_', $data['month']);
            // $group_title = convert_vi_to_en($data["group_title"]);
            // $group_title = str_replace(" ", '_', $group_title);
            $file_name = convertText4Web($data['menu_name'] . "_" . $data['school_id'] . '.xlsx');
            $objWriter->save("php://output");

            $xlsData = ob_get_contents();
            ob_end_clean();
            return_json(array(
                'file_name' => $file_name,
                'file' => "data:application/vnd.ms-excel;base64," . base64_encode($xlsData)
            ));
            break;
        default:
            _error(400);
            break;

    }
} catch (Exception $e) {
    $db->rollback();
    return_json(array('error' => true, 'message' => $e->getMessage()));
} finally {
    $db->autocommit(true);
}
