<?php
/**
 * ajax -> data -> search
 * 
 * @package CINet v1.1
 * @author NOGA Co., Ltd
 */

// fetch bootstrap
require('../../../../../bootstrap.php');

// check AJAX Request
is_ajax();

// check user logged in
if(!$user->_logged_in) {
    modal(LOGIN);
}

// search
try {

	// initialize the return array
	$return = array();
//    include_once(DAO_PATH . 'dao_user.php');
    include_once(DAO_PATH . 'dao_class_level.php');
//    $userDao = new UserDAO();
    $classlevelDao = new ClassLevelDAO();
    $classlevel_ids = count($_POST['classlevel_ids']) ? $_POST['classlevel_ids'] : array();
    $classlevel_id = $_POST['classlevel_id'];

    // Loại bỏ classlevel_id trùng
    $classlevel_ids = array_unique($classlevel_ids);

    // Loại bỏ classlevel_id = ''
    $classlevel_ids = array_diff($classlevel_ids, array(""));

    switch ($_POST['func']) {
        case 'add':
            $classlevel_ids[] = $_POST['classlevel_id'];
            break;
        case 'remove':
            $classlevel_ids = array_diff($classlevel_ids, [$_POST['classlevel_id']]);
            break;
    }

//    $results = $userDao->getUsers($user_ids);
    $results = $classlevelDao->getClasslevelsByIds($classlevel_ids);
    $return['no_data'] = (count($results) == 0);
    if (count($results) > 0) {
        /* assign variables */
        $smarty->assign('results', $results);
        /* return */
        $return['results'] = $smarty->fetch("ci/ajax.classlevellist.tpl");
//        $return['phone'] = $results[0]['user_phone'];
//        $return['email'] = $results[0]['user_email'];
    } else {
        $return['results'] = __("No class level");
    }
	// return & exit
	return_json($return);

} catch (Exception $e) {
	modal(ERROR, __("Error"), $e->getMessage());
}

?>