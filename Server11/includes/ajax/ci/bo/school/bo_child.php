<?php
/**
 * Package: ajax/ci/bo
 * 
 * @package ConIu
 * @author QuanND
 */

// fetch bootstrap
require('../../../../../bootstrap.php');
require(ABSPATH.'includes/class-image.php');

// check AJAX Request
is_ajax();
check_login();

$objPHPExcel = null;
try {
	// initialize the return array
	$return = array();
	$return['callback'] = 'window.location.replace(response.path);';

    include_once(DAO_PATH . 'dao_child.php');
    include_once(DAO_PATH . 'dao_parent.php');
    include_once(DAO_PATH . 'dao_school.php');
    include_once(DAO_PATH . 'dao_class.php');
    include_once(DAO_PATH . 'dao_subject.php');
    include_once(DAO_PATH . 'dao_conduct.php');
    include_once(DAO_PATH . 'dao_teacher.php');
    include_once(DAO_PATH . 'dao_user.php');
    include_once(DAO_PATH . 'dao_mail.php');
    include_once(DAO_PATH . 'dao_service.php');
    include_once(DAO_PATH . 'dao_tuition.php');
    include_once(DAO_PATH . 'dao_journal.php');
    include_once(DAO_PATH . 'dao_report.php');
    include_once(DAO_PATH . 'dao_point.php');
    $childDao = new ChildDAO();
    $parentDao = new ParentDAO();
    $schoolDao = new SchoolDAO();
    $classDao = new ClassDAO();
    $subjectDao = new SubjectDAO();
    $teacherDao = new TeacherDAO();
    $userDao = new UserDAO();
    $mailDao = new MailDAO();
    $serviceDao = new ServiceDAO();
    $tuitionDao = new TuitionDAO();
    $journalDao = new JournalDAO();
    $reportDao = new ReportDAO();
    $pointDao = new PointDAO();
    $conductDao = new ConductDAO();

    $school = getSchoolDataByUsername($_POST['school_username'], SCHOOL_DATA);

    $args = array();
    $args['user_id'] = $user->_data['user_id'];
    $args['child_code'] = isset($_POST['child_code'])? trim($_POST['child_code']): "";
    $args['first_name'] = trim($_POST['first_name']);
    $args['last_name'] = trim($_POST['last_name']);
    $args['child_name'] = $args['last_name']." ".$args['first_name'];
    $args['name_for_sort'] = convert_to_en_4sort($args['first_name'].$args['last_name']);
    $args['description'] = isset($_POST['description'])? trim($_POST['description']) : "";
    $args['gender'] = $_POST['gender'];
    $args['parent_phone'] = standardizePhone($_POST['parent_phone']);
    $args['parent_phone_dad'] = standardizePhone($_POST['parent_phone_dad']);
    $args['parent_email'] = isset($_POST['parent_email'])? trim($_POST['parent_email']) : "";
    $args['parent_name'] = (isset($_POST['parent_name']) && $_POST['parent_name'] != '')? trim($_POST['parent_name']) : '';
    $args['parent_name_dad'] = (isset($_POST['parent_name_dad']) && $_POST['parent_name_dad'] != '')? trim($_POST['parent_name_dad']) : '';
    if($args['parent_name'] == '' && $args['parent_name_dad'] == '') {
        $args['parent_name'] = __("Parent of")." ".$args['child_name'];
        $args['parent_name_creat_account'] = $args['parent_name'];
        $args['parent_phone_creat_account'] = $args['parent_phone'];
    } else {
        if($args['parent_name'] != '') {
            $args['parent_name_creat_account'] = $args['parent_name'];
            $args['parent_phone_creat_account'] = $args['parent_phone'];
        } else {
            $args['parent_name_creat_account'] = $args['parent_name_dad'];
            $args['parent_phone_creat_account'] = $args['parent_phone_dad'];
        }
    }

    $args['parent_job'] = trim($_POST['parent_job']);
    $args['parent_job_dad'] = trim($_POST['parent_job_dad']);
    $args['address'] = isset($_POST['address'])? trim($_POST['address']) : "";
    $args['birthday'] = isset($_POST['birthday'])? trim($_POST['birthday']) : "";
    $args['begin_at'] = $_POST['begin_at'];

    $db->autocommit(false);

    // child create
    switch ($_POST['do']) {

        case 'import':
            if (!canEdit($_POST['school_username'], 'children')) {
                _error(403);
            }
            include_once(ABSPATH . 'includes/libs/PHPExcel/PHPExcel/IOFactory.php');
            include_once(ABSPATH . 'includes/ajax/ci/utilities.php');

            $file_name = $_FILES['file']['name'];
            $inputFileName = $_FILES['file']['tmp_name'];

            $results = array();
            $newChildIds = array();
            $new_child_list = array();
            $sheet = readChildInfoInExcelFile($inputFileName);

            //Lấy ra class_id của lớp
            $class_id = $_POST['class_id'];

            if ($sheet['error'] == 0) {
                foreach ($sheet['child_list'] as $child) { //Duyệt danh sách trẻ
                    if ($child['error'] == 0) {
                        if ($childDao->isCreatedInClass($class_id, $child['name_for_sort'], $child['parent_email'], $child['birthday'], $child['parent_phone'])) {
                            $child['error'] = 1;
                            $child['message'] = __("Student is existing in class");

                            // $new_child_list[] = $child;
                        } else {
                            try {
                                $new_child_list[] = $child;
                                $db->autocommit(false);
                                $db->begin_transaction();

                                $childInfo = $childDao->getStatusChildInSchool($child['child_code'], $school['page_id']);
                                // Nếu trẻ chưa có trong hệ thống thì tạo mới thông tin trẻ trong trường
                                if ($childInfo['status'] == 0) {
                                    $child['child_parent_id'] = 0;
                                    //1. Tạo thông tin trẻ trong bảng ci_child
                                    $lastId = $childDao->createChild($child);

                                    // Danh sách trẻ đã được thêm
                                    $newChildIds[] = $lastId;

                                    //2. Cập nhật danh sách lớp (đưa trẻ vào lớp - bảng ci_class_child)
                                    $child['class_id'] = $class_id;
                                    $child['child_id'] = $lastId;

                                    // Nếu trẻ chưa xếp lớp thì không insert vào bảng ci_class_child
                                    if($child['class_id'] > 0) {
                                        $childDao->addChildToClass($child);
                                        // ADD START MANHDD 03/06/2021
                                        //2.2 nhập thông tin của trẻ vào bảng điểm ci_point_
                                        $subjectOfClass = $subjectDao->getSubjectsByClassId($class_id,$school['page_id']);
                                        if(count($subjectOfClass)>0) {
                                            foreach($subjectOfClass as $subject) {
                                                $childDao->addChildPoints($subject['subject_id'],$args['child_code'],$class_id,$subject['school_year']);
                                            }
                                        }
                                        //2.3 Nhập thông tin của trẻ vào bảng hạnh kiểm ci_conduct
                                        $school_year = date("Y", strtotime($_POST['begin_at'])).'-'.(date("Y", strtotime($_POST['begin_at']))+1);
                                        $childDao->addChildConduct($class_id,$child['child_id'],$school_year);
                                        // ADD END MANHDD 03/06/2021
                                    }

                                    //3. Cập nhật thông tin trẻ trong trường (cập nhật bảng ci_school_child)
                                    $child['school_id'] = $school['page_id'];
                                    $child['child_parent_id'] = 0;
                                    $childDao->addChildToSchool($child);

                                    $parentIds = array();
                                    //3.1. Tự động tạo tài khoản phụ huynh
                                    if ($child['create_parent_account'] && is_empty($child['create_account_result'])) {
                                        //Trường hợp có chọn tạo tài khoản phụ huynh & định dạng email đúng hoặc không nhập địa chỉ email
                                        try {
                                            $argsParent = array();
                                            //$argsParent['full_name'] = __("Parent of") . " " . $child['child_name'];
                                            if($child['parent_name'] == '' && $child['parent_name_dad'] == '') {
                                                $argsParent['parent_name'] = __("Parent of")." ".$child['child_name'];
                                                $argsParent['parent_name_creat_account'] = $argsParent['parent_name'];
                                                $argsParent['parent_phone_creat_account'] = $child['parent_phone'];
                                            } else {
                                                if($child['parent_name'] != '') {
                                                    $argsParent['parent_name_creat_account'] = $child['parent_name'];
                                                    $argsParent['parent_phone_creat_account'] = $child['parent_phone'];
                                                } else {
                                                    $argsParent['parent_name_creat_account'] = $child['parent_name_dad'];
                                                    $argsParent['parent_phone_creat_account'] = $child['parent_phone_dad'];
                                                }
                                            }
                                            $argsParent['full_name'] = $argsParent['parent_name_creat_account'];
                                            $argsParent['first_name'] = split_name($argsParent['full_name'])[0];
                                            $argsParent['last_name'] = split_name($argsParent['full_name'])[1];
                                            $argsParent['username'] = generateUsername($argsParent['full_name']);
                                            $argsParent['user_phone'] = standardizePhone($argsParent['parent_phone_creat_account']);
                                            $argsParent['email'] = ($child['parent_email'] != '') ? $child['parent_email'] : 'null';
                                            $argsParent['password'] = (is_empty($argsParent['user_phone']) ? "coniu.vn" : $argsParent['user_phone']);
                                            $argsParent['gender'] = FEMALE;

                                            $parentId = $userDao->createUser($argsParent);
                                            $parentIds[] = $parentId;
                                            if($argsParent['email'] != 'null') {
                                                //Gửi mail cho phụ huynh của trẻ.
                                                $argEmail = array();
                                                $argEmail['action'] = "create_parent_account";
                                                $argEmail['receivers'] = $argsParent['email'];
                                                $argEmail['subject'] = "[" . convertText4Web($school['page_title']) . "]" . __("Use Inet application to connect student's parents");

                                                //Tạo nên nội dung email
                                                $smarty->assign('full_name', $argsParent['full_name']);
                                                $smarty->assign('child_name', $child['child_name']);
                                                $smarty->assign('username', $argsParent['email']);
                                                $smarty->assign('password', $argsParent['password']);
                                                $argEmail['content'] = $smarty->fetch("ci/email_templates/create_parent_account.tpl");

                                                $argEmail['delete_after_sending'] = 1;
                                                $argEmail['school_id'] = $school['page_id'];
                                                $argEmail['user_id'] = $user->_data['user_id'];
                                                $mailDao->insertEmail($argEmail);
                                            }

                                            $child['create_account_result'] = __("Create parent account successfully");
                                        } catch (Exception $e) {
                                            $child['create_account_result'] = __("Can not create parent account");
                                        }
                                    } else if ($child['create_parent_account'] == 0) {
                                        $child['create_account_result'] = __("Not create parent account");
                                    }
                                    if (count($parentIds) > 0) {
                                        //3.2. Cho phụ huynh like các page của hệ thống, join các group của hệ thống
                                        $userDao->addUsersLikeSomePages($system_page_ids, $parentIds);
                                        $userDao->addUserToSomeGroups($system_group_ids, $parentIds);

                                        //4. Thêm cha mẹ vào nhóm của lớp (bao gồm việc tăng thành viên nhóm)
                                        $classDao->addUserToClass($class_id, $parentIds);

                                        //5. Thêm cha mẹ cho trẻ
                                        $parentDao->addParentList($lastId, $parentIds);

                                        //6. Cho cha mẹ like trang của trường (tăng số like)
                                        $likeCnt = $schoolDao->addUsersLikeSchool($school['page_id'], $parentIds);
                                        $school['page_likes'] = $school['page_likes'] + $likeCnt;

                                        //7. Cho cha mẹ thành bạn của giáo viên (bao gồm cả quản lý trường)
                                        // Tạm thời không cho kết bạn giữa giáo viên, phụ huynh
                                        /*$teacherIds = $teacherDao->getTeacherIDOfClass($class_id);
                                        $teacherIds[] = $user->_data['user_id']; //User hiện tại là quản lý của trường
                                        $userDao->becomeFriends($teacherIds, $parentIds);*/

                                        //7. Tạo thông tin trẻ cho phụ huynh
                                        $child['child_admin'] = $parentIds[0];
                                        $childParentId = $childDao->createChildForParent($child);

                                        //7.1. Gắn trẻ được quản lý cho phụ huynh
                                        $childDao->createParentManageInfo($parentIds, $childParentId, $child['child_id'], $school['page_id']);

                                        //8.Memcache - Cập nhật user quản lý
                                        foreach ($parentIds as $parentId) {
                                            updateUserManageData($parentId, USER_MANAGE_CHILDREN);
                                        }
                                    } else {
                                        //7. Tạo thông tin trẻ cho phụ huynh
                                        $child['child_admin'] = 0;
                                        $childParentId = $childDao->createChildForParent($child);
                                    }
                                    //8. Tăng số lượng trẻ của trường
                                    $schoolDao->updateGenderCount($school['page_id'], $child['gender'], 1);
                                    //unset($_SESSION[$_POST['school_username']]);
                                    //Cập nhật thông tin trường trong session
                                    /*$schoolInfo = $_SESSION[$_POST['school_username']];
                                    if ($child['gender'] == MALE) {
                                        $schoolInfo['male_count'] = $schoolInfo['male_count'] + 1;
                                    } else {
                                        $schoolInfo['female_count'] = $schoolInfo['female_count'] + 1;
                                    }
                                    $_SESSION[$_POST['school_username']] = $schoolInfo;*/

                                    if ($child['gender'] == MALE) {
                                        $school['male_count'] = $school['male_count'] + 1;
                                    } else {
                                        $school['female_count'] = $school['female_count'] + 1;
                                    }
                                } else {
                                    $child['error'] = 1;
                                    $child['message'] = __("Student is existing in system, check student code");
                                    continue;
                                }
                                $db->commit();
                            } catch (Exception $e) {
                                $db->rollback();
                                $child['error'] = 1;
                                $child['message'] = $e->getMessage();
                            } finally {
                                $db->autocommit(true);
                            }
                        }
                    }
                }

                /* ---------- UPDATE - MEMCACHE ---------- */
                //1. Cập nhật thông tin của trường
                $info_update = array(
                    'page_likes' => $school['page_likes']
                );
                $config_update = array(
                    'male_count' => $school['male_count'],
                    'female_count' => $school['female_count']
                );
                updateSchoolData($school['page_id'], SCHOOL_INFO, $info_update);
                updateSchoolData($school['page_id'], SCHOOL_CONFIG, $config_update);
                updateSchoolData($school['page_id'], SCHOOL_CHILDREN);

                //2. Cập nhật thông tin lớp
                updateClassData($class_id, CLASS_INFO);
                updateClassData($class_id, CLASS_CHILDREN);

                //3. Cập nhật thông tin trẻ
                foreach ($newChildIds as $childId) {
                    updateChildData($childId);
                }

                /* ---------- END - MEMCACHE ---------- */
            }

            $sheet['child_list'] = $new_child_list;
            $smarty->assign("sheet", $sheet);
            $results['results'] = $smarty->fetch("ci/school/ajax.children.importresult.tpl");
            return_json($results);
            break;
        case 'export':
            if (!canView($_POST['school_username'], 'children')) {
                _error(403);
            }
            /**
             * Hàm xử lý khi click vào export trong danh sách trẻ
             * Xuất ra file excel danh sách trẻ của một lớp, của trường
             */
            $reportTemplate = ABSPATH."content/templates/children_list_vi_VN.xlsx";
            // include thư viện
            include_once(ABSPATH . 'includes/libs/PHPExcel/PHPExcel/IOFactory.php');
            include_once(ABSPATH . 'includes/libs/PHPExcel/PHPExcel.php');

            $objPHPExcel = PHPExcel_IOFactory::load($reportTemplate);

            $condition = $_SESSION[SESSION_KEY_SEARCH_CHILDREN];
            $result = $childDao->searchHaveMonth($condition['keyword'], $school['page_id'], $condition['class_id'], $condition['child_month']);

            // set wraptext
            $objPHPExcel->getDefaultStyle()->getAlignment()->setWrapText(true);

            // set vertical
            $style = array(
                'alignment' => array(
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            );
            $objPHPExcel->getDefaultStyle()->applyFromArray($style);

            // set border
            $all_border_style = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_NONE,
                        'color' => array('rgb' => '333333')
                    )
                )
            );
            $border_style = array(
                'borders' => array(
                    'top' => array(
                        'style' => PHPExcel_Style_Border::BORDER_NONE,
                        'color' => array('rgb' => '777777')
                    ),
                    'bottom' => array(
                        'style' => PHPExcel_Style_Border::BORDER_NONE,
                        'color' => array('rgb' => '777777')
                    ),
                    'left' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => '333333')
                    ),
                    'right' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => '333333')
                    )
                )
            );
            $border_style_top = array(
                'borders' => array(
                    'top' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => '777777')
                    ),
                    'bottom' => array(
                        'style' => PHPExcel_Style_Border::BORDER_NONE,
                        'color' => array('rgb' => '777777')
                    ),
                    'left' => array(
                        'style' => PHPExcel_Style_Border::BORDER_NONE,
                        'color' => array('rgb' => '333333')
                    ),
                    'right' => array(
                        'style' => PHPExcel_Style_Border::BORDER_NONE,
                        'color' => array('rgb' => '333333')
                    )
                )
            );

            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', mb_strtoupper(convertText4Web($school['page_title']),"UTF-8"));

            if($condition['class_id'] != null){
                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A2', __('Student list') . ': ' .convertText4Web($result['children'][0]['group_title']));
            } else {
                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A2', __('Student list') . ': ' .convertText4Web($school['page_title']));
            }
            $rowNumber = 5;
            $idx = 1;

            foreach ($result['children'] as $row) {
                $row['begin_at'] = toSysDate($row['begin_at']);
                $objPHPExcel->getActiveSheet()->getStyle( 'A'.$rowNumber )->applyFromArray(
                    styleArray('Times new Roman',true, false, 11,
                        PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        PHPExcel_Style_Alignment::VERTICAL_CENTER, true
                    )
                );
                $objPHPExcel->getActiveSheet()->getStyle( 'D'.$rowNumber )->applyFromArray(
                    styleArray('Times new Roman',false, false, 11,
                        PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        PHPExcel_Style_Alignment::VERTICAL_CENTER, true
                    )
                );
                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A'.$rowNumber, $idx++)
                    ->setCellValue('B'.$rowNumber, convertText4Web($row['child_name']))
                    ->setCellValue('C'.$rowNumber, $row['birthday'])
                    ->setCellValue('D'.$rowNumber, ($row['gender'] == "male") ? __('Male') : __('Female'))
                    ->setCellValue('E'.$rowNumber, convertText4Web($row['group_title']))
                    ->setCellValue('F'.$rowNumber, convertText4Web($row['begin_at']))
                    ->setCellValue('G'.$rowNumber, convertText4Web($row['parent_name']))
                    ->setCellValue('H'.$rowNumber, ($row['parent_phone']))
                    ->setCellValue('I'.$rowNumber, convertText4Web($row['parent_job']))
                    ->setCellValue('J'.$rowNumber, convertText4Web($row['parent_name_dad']))
                    ->setCellValue('K'.$rowNumber, ($row['parent_phone_dad']))
                    ->setCellValue('L'.$rowNumber, convertText4Web($row['parent_job_dad']))
                    ->setCellValue('N'.$rowNumber, convertText4Web($row['address']));

                if(count($row['parent']) > 0) {
                    foreach ($row['parent'] as $key => $_user) {
                        if($_user['user_id'] == $row['child_admin']) {
                            if(!is_null($_user['user_email'])) {
                                $objPHPExcel->setActiveSheetIndex(0)
                                    ->setCellValue('M'.$rowNumber, convertText4Web($_user['user_email']));
                            } else {
                                $objPHPExcel->setActiveSheetIndex(0)
                                    ->setCellValue('M'.$rowNumber, convertText4Web($_user['user_name']));
                            }
                        }
                    }
                }
                $objPHPExcel->getActiveSheet()->getStyle('A' . $rowNumber)
                    ->applyFromArray($border_style);
                $objPHPExcel->getActiveSheet()->getStyle('B' . $rowNumber)
                    ->applyFromArray($border_style);
                $objPHPExcel->getActiveSheet()->getStyle('C' . $rowNumber)
                    ->applyFromArray($border_style);
                $objPHPExcel->getActiveSheet()->getStyle('D' . $rowNumber)
                    ->applyFromArray($border_style);
                $objPHPExcel->getActiveSheet()->getStyle('E' . $rowNumber)
                    ->applyFromArray($border_style);
                $objPHPExcel->getActiveSheet()->getStyle('F' . $rowNumber)
                    ->applyFromArray($border_style);
                $objPHPExcel->getActiveSheet()->getStyle('G' . $rowNumber)
                    ->applyFromArray($border_style);
                $objPHPExcel->getActiveSheet()->getStyle('H' . $rowNumber)
                    ->applyFromArray($border_style);
                $objPHPExcel->getActiveSheet()->getStyle('I' . $rowNumber)
                    ->applyFromArray($border_style);
                $objPHPExcel->getActiveSheet()->getStyle('J' . $rowNumber)
                    ->applyFromArray($border_style);
                $objPHPExcel->getActiveSheet()->getStyle('K' . $rowNumber)
                    ->applyFromArray($border_style);
                $objPHPExcel->getActiveSheet()->getStyle('L' . $rowNumber)
                    ->applyFromArray($border_style);
                $objPHPExcel->getActiveSheet()->getStyle('M' . $rowNumber)
                    ->applyFromArray($border_style);
                $objPHPExcel->getActiveSheet()->getStyle('N' . $rowNumber)
                    ->applyFromArray($border_style);
                $rowNumber++;
            }
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A' . $rowNumber . ':N' . $rowNumber);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $rowNumber . ':N' . $rowNumber)
                ->applyFromArray($border_style_top);

            // Ngày xuất phiếu
            $rowNumber++;
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A' . $rowNumber . ':M' . $rowNumber);
            $objPHPExcel->getActiveSheet()->getStyle('A' .$rowNumber)
                ->applyFromArray(
                    styleArray('Times new Roman',false, true, 11,
                        PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                        PHPExcel_Style_Alignment::VERTICAL_CENTER, true
                    )
                );
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A'.$rowNumber, '..........., '. __('Day') .' .... '. __('Month') .' .... '. __('Year') .' ...... ');

            $rowNumber = $rowNumber + 2;
            // Những người liên quan
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A' . $rowNumber . ':E' . $rowNumber);
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('J' . $rowNumber . ':N' . $rowNumber);
            $objPHPExcel->getActiveSheet()->getStyle('A' .$rowNumber. ':J'.$rowNumber)
                ->applyFromArray(
                    styleArray('Times new Roman',true, false, 11,
                        PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        PHPExcel_Style_Alignment::VERTICAL_CENTER, true
                    )
                );
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A' . $rowNumber, __('Schoolmaster'))
                ->setCellValue('J' . $rowNumber, __('Manage'));

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            ob_start();
            if($condition['class_id'] != null) {
                $group_name = str_replace('/', '_', $result['children'][0]['group_title']);
                $file_name = convertText4Web('danhsachtre_'.$group_name.'.xlsx');
            } else {
                $school_name = str_replace('/', '_', $school['page_title']);
                $file_name = convertText4Web('danhsachtre_'.$school_name.'.xlsx');
            }
            $file_name = convert_vi_to_en($file_name);
            $file_name = str_replace(" ",'_', $file_name);

            $objWriter->save("php://output");

            $xlsData = ob_get_contents();
            ob_end_clean();

            return_json(array(
                'file_name' => $file_name,
                'file' => "data:application/vnd.ms-excel;base64,".base64_encode($xlsData)
            ));
            break;
        case 'moveclass':
            if (!canEdit($_POST['school_username'], 'children')) {
                _error(403);
            }
            /**
             * Hàm này xử lý khi chuyển lớp cho trẻ
             * 1. Cập nhật danh sách lớp (đưa trẻ vào lớp - bảng ci_class_child)
             * 2. Cập nhật thông tin trẻ trong trường (cập nhật bảng ci_school_child)
             * 3. Cập nhật cha mẹ vào nhóm của lớp cũ/mới.
             * 4. Cho cha mẹ thành bạn của giáo viên lớp mới.
             * 5. Chuyển thông tin điểm danh từ đầu tháng của trẻ sang lớp mới.
             * 6.chuyển thông tin điểm của trẻ sang lớp mới (nếu là cấp 1 hoặc cấp 2
             * 7. Chuyển thông tin hạnh kiểm của trẻ sang lớp mới
             */
            $db->begin_transaction();
            // Lấy ra thông tin cũ của trẻ để kiểm tra
            $oldChild = $childDao->getSchoolChild($_POST['child_id'], $school['page_id']);
            if (is_null($oldChild)) {
                _error(404);
            }

            //2. Cập nhật danh sách lớp (đưa trẻ vào lớp - bảng ci_class_child) (nếu có thay đổi về lớp)
            $childDao->moveClassInClassChild($_POST['child_id'], $_POST['old_class_id'], $_POST['new_class_id']);

            //3. Cập nhật thông tin trẻ trong trường (cập nhật bảng ci_school_child)
            $childDao->moveClassInSchoolChild($school['page_id'], $_POST['child_id'], $_POST['new_class_id']);

            //4. Cập nhật thông tin học phí.
            $tuitionDao->updateTuition4MoveClass($_POST['child_id'], $_POST['old_class_id'], $_POST['new_class_id']);

            //$parentIds = $parentDao->getParentIds($_POST['child_id']);
            $parents = getChildData($_POST['child_id'], CHILD_PARENTS);
            $parentIds = array_keys($parents);
            if (count($parentIds) > 0) {
                //Xóa cha mẹ khỏi lớp cũ
                if ($_POST['old_class_id'] > 0) {
                    $classDao->deleteUserFromClass($_POST['old_class_id'], $parentIds);
                }
                // Thêm cha mẹ vào nhóm của lớp mới.
                $classDao->addUserToClass($_POST['new_class_id'], $parentIds);

                //4. Kết bạn cha mẹ với cô giáo lớp mới
                // Tạm thời không cho kết bạn giữa giáo viên, phụ huynh
                /*$teacherIds = $teacherDao->getTeacherIDOfClass($_POST['new_class_id']);
                $userDao->becomeFriends($teacherIds, $parentIds);*/

            }
            //5. Chuyển thông tin điểm danh từ đầu tháng của trẻ sang lớp mới.
            include_once(DAO_PATH . 'dao_attendance.php');
            $attendanceDao = new AttendanceDAO();
            $beginDate = date($system['date_format'], mktime(0, 0, 0, date("m"), 1, date("Y")));
            $endDate = date($system['date_format']);
            //5.1 Lấy thông tin điểm danh của trẻ ở lớp CŨ
            $oldAtts = $attendanceDao->getAttendanceChild4MoveClass($_POST['old_class_id'], $_POST['child_id'], $beginDate, $endDate);
            //5.2 Lấy thông tin điểm danh trong tháng của lớp MỚI.
            $newAtts = $attendanceDao->getAttendanceClass4MoveClass($_POST['new_class_id'], $beginDate, $endDate);
            //5.3 Duyệt danh sách điểm danh lớp CŨ
            $tmpAtt = null;
            foreach ($oldAtts as $oldAtt) {
                $found = false;
                foreach ($newAtts as $newAtt) {
                    if ($oldAtt['attendance_date'] == $newAtt['attendance_date']) {
                        $found = true;
                        $tmpAtt = $newAtt;
                        break;
                    }
                }
                if ($found) {
                    //5.4 Chuyển thông tin điểm danh sang lớp mới
                    $attendanceDao->updateAttendanceDetail4MoveClass($oldAtt['attendance_detail_id'], $tmpAtt['attendance_id']);
                    //5.5 Cập nhật số lượng đi/nghỉ của lớp cũ
                    $attendanceDao->updatePresentAbsence4OldClass($oldAtt['attendance_id'], $oldAtt['status']);
                    //5.6 Cập nhật số lượng đi/nghỉ của lớp mới.
                    $attendanceDao->updatePresentAbsence4NewClass($tmpAtt['attendance_id'], $oldAtt['status']);
                } else {
                    //Trường hợp này là lớp CŨ có điểm danh, nhưng lớp mới không điểm danh ngày đó.
                    // Khởi tạo điểm danh lớp mới
                    $args['attendance_date'] = toSysDate($oldAtt['attendance_date']);
                    $args['absence_count'] = 0;
                    $args['present_count'] = 0;
                    $args['is_checked'] = 0;
                    $args['class_id'] = $_POST['new_class_id'];
                    // Tạo bản ghi
                    $atds['attendance_id'] = $attendanceDao->insertAttendance($args);
                    //5.4 Chuyển thông tin điểm danh sang lớp mới
                    $attendanceDao->updateAttendanceDetail4MoveClass($oldAtt['attendance_detail_id'], $atds['attendance_id']);
                    //5.5 Cập nhật số lượng đi/nghỉ của lớp cũ
                    $attendanceDao->updatePresentAbsence4OldClass($oldAtt['attendance_id'], $oldAtt['status']);

                    //Xóa điểm danh ở lớp cũ khi không tìm thấy điểm danh ở lớp mới.
                    //$attendanceDao->deleteAttendanceDetail4OldClass($oldAtt);
                }
            }

            // 6. Chuyển thông tin điểm của trẻ dang lớp mới
            $childInfo = getChildData($_POST['child_id'], CHILD_INFO);
            if($school['grade'] > 0) {
                $pointDao->updateMoveClassId($childInfo['child_code'], $_POST['old_class_id'], $_POST['new_class_id'], $school['grade']);
            }
            // 7. Chuyển thông tin hạnh kiểm của trẻ dang lớp mới
            if($school['grade'] > 0) {
                $conductDao->updateMoveClassId($_POST['child_id'], $_POST['old_class_id'], $_POST['new_class_id']);
            }

            /* ---------- UPDATE - MEMCACHE ---------- */
            //1.Cập nhật thông tin trẻ
            updateChildData($_POST['child_id']);

            //2. Cập nhật thông tin lớp cũ
            updateClassData($_POST['old_class_id'], CLASS_INFO);
            deleteClassData($_POST['old_class_id'], CLASS_CHILDREN, $_POST['child_id']);

            //3. Cập nhật thông tin lớp mới
            updateClassData($_POST['new_class_id'], CLASS_INFO);
            updateClassData($_POST['new_class_id'], CLASS_CHILDREN);

            /* ---------- END - MEMCACHE ---------- */

            $db->commit();
            return_json( array('success' => true, 'message' => __("Done, Child info have been updated")) );
            break;
        case 'delete_child_leave':
            if (!canEdit($_POST['school_username'], 'children')) {
                _error(403);
            }
            $db->begin_transaction();
            // Lấy ra thông tin cũ của trẻ để kiểm tra
            $child = $childDao->getSchoolChild($_POST['child_id'], $school['page_id']);
            if (is_null($child)) {
                _error(404);
            }

            $childDao->deleteChildLeave($school['page_id'], $_POST['child_id']);
            $db->commit();
            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/school/' . $_POST['school_username'] . '/children/leaveschool' . '";'));
            break;
        case 'leave_school':
            if (!canEdit($_POST['school_username'], 'children')) {
                _error(403);
            }
            /**
             * Hàm này xử lý khi cho trẻ thôi học
             * 1. Cập nhật thông tin trẻ trong lớp (chuyển trạng thái Inactive - bảng ci_class_child)
             * 2. Cập nhật thông tin trẻ trong trường (chuyển trạng thái Inactive - bảng ci_school_child)
             * 3. Hủy cha mẹ vào khỏi của lớp cũ.
             */
            $db->begin_transaction();
            // Lấy ra thông tin cũ của trẻ để kiểm tra
            $child = $childDao->getSchoolChild($_POST['child_id'], $school['page_id']);
            if (is_null($child)) {
                _error(404);
            }

            //1. Cập nhật thông tin trẻ trong lớp (chuyển trạng thái Inactive - bảng ci_class_child và ci_school_child)
            $childDao->leaveChildInSchool($_POST['child_id'], $_POST['class_id'], $_POST['end_at']);

            //2. Xóa cha mẹ khỏi lớp (nhưng vẫn giữ like page của trường)
            //$parentIds = $parentDao->getParentIds($_POST['child_id']);
            $parents = getChildData($_POST['child_id'], CHILD_PARENTS);
            $parentIds = array_keys($parents);
            if (count($parentIds) > 0) {
                if ($_POST['class_id'] > 0) {
                    $classDao->deleteUserFromClass($_POST['class_id'], $parentIds);
                }
            }
            //2.1. Xóa thông tin trẻ trong ci_parent_manage
            $childDao->deleteParentManageInfo($_POST['child_id'], $child['child_parent_id']);

            //3. Giảm số lượng trẻ của trường (giảm giới tính trẻ)
            $schoolDao->updateGenderCount($school['page_id'], $child['gender'], -1);

            if ($child['gender'] == MALE) {
                $school['male_count'] = $school['male_count'] - 1;
            } else {
                $school['female_count'] = $school['female_count'] - 1;
            }

            //4.Hủy các dịch vụ trẻ đã đăng ký
            $serviceDao->updateServiceForChildLeave($_POST['child_id'], $_POST['end_at']);

            //5. Cập nhật quyết toán học phí.
            $tuition4Leave = array();
            $tuition4Leave['attendance_count'] = $_POST['attendance_count'];
            $tuition4Leave['final_amount'] = str_replace(',', '', $_POST['final_amount']);
            $tuition4Leave['debt_amount'] = isset($_POST['debt_amount'])? str_replace(',', '', $_POST['debt_amount']): 0;
            $tuition4Leave['total_deduction'] = str_replace(',', '', $_POST['total_deduction']);
            $tuition4Leave['paid_amount'] = str_replace(',', '', $_POST['paid_amount']);
            $tuition4Leave['school_id'] = $school['page_id'];
            $tuition4Leave['child_id'] = $_POST['child_id'];

            $tuition4LDetails = getTuition4LeaveData();
            $tuitionDao->insertTuition4Leave($tuition4Leave, $tuition4LDetails);

            /* ---------- UPDATE - MEMCACHE ---------- */
            //1.Cập nhật thông tin trường
            $config_update = array(
                'male_count' => $school['male_count'],
                'female_count' => $school['female_count']
            );
            updateSchoolData($school['page_id'], SCHOOL_CONFIG, $config_update);
            deleteSchoolData($school['page_id'], SCHOOL_CHILDREN, $_POST['child_id']);

            //2. Cập nhật thông tin lớp cũ
            updateClassData($_POST['class_id'], CLASS_INFO);
            deleteClassData($_POST['class_id'], CLASS_CHILDREN, $_POST['child_id']);

            //3.Xóa thông tin trẻ
            deleteChildData($_POST['child_id']);
            /* ---------- END - MEMCACHE ---------- */

            $db->commit();
            return_json( array('success' => true, 'message' => __("Child's school leave information has been updated")) );
            break;
        case 'edit_leave':
            if (!canEdit($_POST['school_username'], 'children')) {
                _error(403);
            }
            $db->begin_transaction();

            //1. Cập nhật quyết toán học phí.
            $tuition4Leave = array();
            $tuition4Leave['attendance_count'] = $_POST['attendance_count'];
            $tuition4Leave['final_amount'] = str_replace(',','',$_POST['final_amount']);
            $tuition4Leave['debt_amount'] = isset($_POST['debt_amount'])? str_replace(',','',$_POST['debt_amount']): 0;
            $tuition4Leave['total_deduction'] = str_replace(',','',$_POST['total_deduction']);
            $tuition4Leave['paid_amount'] = str_replace(',','',$_POST['paid_amount']);
            $tuition4Leave['tuition_4leave_id'] = $_POST['tuition_4leave_id'];
            $tuition4LDetails = getTuition4LeaveData();
            $tuitionDao->updateTuition4Leave($tuition4Leave, $tuition4LDetails);

            //2. Cập nhật ngày thôi học.
            if (strcmp($_POST['end_at'], $_POST['old_end_at']) != 0) {
                $childDao->leaveChildInSchool($_POST['child_id'], $_POST['class_id'], $_POST['end_at']);
            }

            $db->commit();
            return_json( array('success' => true, 'message' => __("Child's school leave information has been updated")) );
            break;
        case 'edit':
            if (!canEdit($_POST['school_username'], 'children')) {
                _error(403);
            }
            /**
             * Hàm này xử lý khi trường edit thông tin của trẻ
             * 1. Cập nhật thông tin trẻ trong bảng ci_child
             * 1.1 Cập nhật thông tin trẻ trong bảng ci_child_parent
             * 2. Cập nhật danh sách lớp (đưa trẻ vào lớp - bảng ci_class_child) (nếu có thay đổi về lớp)
             * 3. Cập nhật thông tin trẻ trong trường (cập nhật bảng ci_school_child)
             * 4. Cập nhật cha mẹ vào nhóm của lớp (bao gồm việc tăng/giảm member)
             * 5. Cập nhật danh sách cha mẹ của trẻ
             * 6. Cập nhật danh sách cha mẹ like trang của trường (bao gồm cả việc thay đổi số like).
             * 7. Cho cha mẹ thành bạn của giáo viên (bao gồm cả quản lý trường)
             */

            if(isset($_POST['begin_at']) && !validateDate($_POST['begin_at'])) {
                throw new Exception(__("You must enter study start date"));
            }
            $birthday = toDBDate($args['birthday']);
            $beginAt = toDBDate($args['begin_at']);
            if(strtotime($birthday) > strtotime($beginAt)) {
                throw new Exception(__("Begin at not before birthdate"));
            }

            //Validate điều kiện tạo tài khoản phụ huynh
            $create_parent_account = $_POST['create_parent_account'];
            if ($create_parent_account && !is_empty($args['parent_email']) && !valid_email($args['parent_email'])) {
                throw new Exception(__("To create parent account, you must enter correct parent email or parent email is empty."));
            }
            $class_id = is_numeric($_POST['class_id']) ? $_POST['class_id'] : 0;

            $db->begin_transaction();
            // Lấy ra thông tin cũ của trẻ để kiểm tra
            $oldChild = getChildData($_POST['child_id'], CHILD_INFO);
            if (is_null($oldChild)) {
                _error(404);
            }

            $child = $childDao->getChildByParent($_POST['child_parent_id']);
            if (is_null($child)) {
                _error(403);
            }

            //1. Cập nhật thông tin trẻ trong bảng ci_child
            $args['child_id'] = $_POST['child_id'];
            $childDao->editChild($args);

            // Nếu trẻ đang đi học ở trường thì mới cập nhật thông tin trong bảng ci_child_parent
            if($oldChild['status']) {
                // 1.1 Cập nhật thông tin trẻ trong bảng ci_child_parent
                $args['child_parent_id'] = $_POST['child_parent_id'];
                $childDao->editChildBySchool($args);
            }

            //Bỏ bước này vì sửa trẻ không được sửa lớp.
            //2. Cập nhật danh sách lớp (đưa trẻ vào lớp - bảng ci_class_child) (nếu có thay đổi về lớp)
            $argsCC['begin_at'] = isset($_POST['begin_at'])? trim($_POST['begin_at']) : "";
            $argsCC['class_id'] = $class_id;
            $argsCC['child_id'] = $_POST['child_id'];

            //3. Cập nhật thông tin trẻ trong trường (cập nhật bảng ci_school_child)
            $argsCC['school_id'] = $school['page_id'];
            $childDao->updateSchoolChild($argsCC);

            //$oldParentIds = $parentDao->getParentIds($_POST['child_id']);
            $parents = getChildData($_POST['child_id'], CHILD_PARENTS);
            $oldParentIds = array_keys($parents);
            $newParentIds = (isset($_POST['user_id']) && (count($_POST['user_id']) > 0))? $_POST['user_id']: array();

            // bỏ user_id trùng
            $newParentIds = array_unique($newParentIds);

            // Xóa danh sách cha mẹ cũ đi
            $deletedParentIds = array_diff($oldParentIds, $newParentIds);
            if (count($deletedParentIds) > 0) {
                // Không được xóa phụ huynh admin của trẻ
                if(in_array($child['child_admin'], $deletedParentIds)) {
                    throw new Exception(__("You do not have the right to remove a parent who administers a student"));
                }
                if($oldChild['status']) {
                    //Xóa cha mẹ khỏi lớp cũ nếu trẻ đang đi học
                    $classDao->deleteUserFromClass($oldChild['class_id'], $deletedParentIds);
                }
                //Xóa danh sách cha mẹ trong danh sách đc quản lý (bảng ci_user_manage)
                $parentDao->deleteParentList($_POST['child_id'], $deletedParentIds);

                // Nếu trẻ đang đi học thì mới cập nhật thông tin phụ huynh trong bảng ci_parent_manage
                if($oldChild['status']) {
                    // Xóa danh sách cha mẹ trong bảng ci_parent_manage
                    $parentDao->deleteParentListByParent($_POST['child_parent_id'], $deletedParentIds);
                }
                if($oldChild['status']) {
                    //Xóa danh sách cha mẹ like page trường (nếu trẻ đang học tại trường)
                    $schoolDao->deleteUserLikeSchool($school['page_id'], $deletedParentIds); //Xem xét có thể để cha mẹ like page của trường, ko cần thiết phải xóa
                }
            }

            // Cập nhật child_admin
            if(count($newParentIds) == 0) {
                $child_admin = '';
            } else {
                $child_admin = $_POST['child_admin'];
            }
            $childDao->updateChildAdmin($_POST['child_parent_id'], $child_admin);
            //3.1. Tự động tạo tài khoản phụ huynh
            if ($create_parent_account) {
                $argsParent = array();
                $argsParent['full_name'] = $args['parent_name_creat_account'];
                $argsParent['first_name'] = split_name($argsParent['full_name'])[0];
                $argsParent['last_name'] = split_name($argsParent['full_name'])[1];
                $argsParent['username'] = generateUsername($argsParent['full_name']);
                $argsParent['user_phone'] = standardizePhone($args['parent_phone_creat_account']);
                /*if (!validatePhone($args['user_phone'])) {
                    return_json( array('error' => true, 'message' => __("The phone number you entered is incorrect")) );
                }*/
                $argsParent['email'] = ($args['parent_email'] != '') ? $args['parent_email'] : 'null';
                $argsParent['password'] = (is_empty($argsParent['user_phone'])? "coniu.vn": $argsParent['user_phone']);
                $argsParent['gender'] = FEMALE;

                $parentId = $userDao->createUser($argsParent);

                $newParentIds[] = $parentId;
                // Set child_admin cho trẻ
                $childDao->setChildAdmin($_POST['child_parent_id'], $parentId);
                if($argsParent['email'] != 'null') {
                    //Gửi mail cho phụ huynh của trẻ.
                    $argEmail = array();
                    $argEmail['action'] = "create_parent_account";
                    $argEmail['receivers'] = $argsParent['email'];
                    $argEmail['subject'] = "[".convertText4Web($school['page_title'])."]".__("Use Inet application to connect student's parents");

                    //Tạo nên nội dung email
                    $smarty->assign('full_name', $argsParent['full_name']);
                    $smarty->assign('child_name', $args['child_name']);
                    $smarty->assign('username', $argsParent['email']);
                    $smarty->assign('password', $argsParent['password']);
                    $argEmail['content'] = $smarty->fetch("ci/email_templates/create_parent_account.tpl");

                    $argEmail['delete_after_sending'] = 1;
                    $argEmail['school_id'] = $school['page_id'];
                    $argEmail['user_id'] = $user->_data['user_id'];

                    $mailDao->insertEmail($argEmail);
                }
            }
            // Lấy danh sách giáo viên của lớp, nếu chưa xếp lớp thì bỏ qua
            $teacherIds = array();
            if ($class_id > 0) {
                //$teacherIds = $teacherDao->getTeacherIDOfClass($class_id);
                $teachers = getClassData($class_id, CLASS_TEACHERS);
                $teacherIds = array_keys($teachers);
            }
            $teacherIds[] = $user->_data['user_id']; //User hiện tại là quản lý của trường
            // Thêm danh sách mới vào
            $addedParentIds = array_diff($newParentIds, $oldParentIds);
            if (count($addedParentIds) > 0) {
                //3.2. Cho phụ huynh like các page của hệ thống, join các group của hệ thống
                $userDao->addUsersLikeSomePages($system_page_ids, $addedParentIds);
                $userDao->addUserToSomeGroups($system_group_ids, $addedParentIds);

                //4. Thêm cha mẹ vào nhóm của lớp (bao gồm việc tăng thành viên nhóm)
                // Nếu chưa chọn lớp thì bỏ qua
                if ($class_id > 0) {
                    $classDao->addUserToClass($class_id, $addedParentIds);
                }
                //5. Thêm cha mẹ cho trẻ & người tạo ra thông tin trẻ vào danh sách người có thể quản lý
                $parentDao->addParentList($_POST['child_id'], $addedParentIds);

                // Nếu trẻ đang đi học tại trường thì mới cập nhật thông tin trong bảng ci_parent_manage
                if($oldChild['status']) {
                    //5.1 Thêm cha mẹ quản lý trẻ trong bảng ci_parent_manage
                    $childDao->createParentManageInfo($addedParentIds, $_POST['child_parent_id'], $_POST['child_id'], $school['page_id']);
                }
                //7. Trẻ đang đi học và chưa có phụ huynh thì cập nhật child_admin(ci_child_parent)
                if (count($oldParentIds) == 0 && $oldChild['status']) {
                    $childAdmin = $addedParentIds[0];
                    $childParentId = $oldChild['child_parent_id'];
                    $childDao->updateChildForParent($childParentId, $childAdmin);
                }
                //6. Cho cha mẹ like trang của trường (tăng số like)
                $likeCnt = $schoolDao->addUsersLikeSchool($school['page_id'], $addedParentIds);
                $school['page_likes'] = $school['page_likes'] + $likeCnt;

                //7. Cho cha mẹ thành bạn của giáo viên (bao gồm cả quản lý trường)
                // Nếu chưa chọn lớp thì bỏ qua
//                $teacherIds = array();
//                if ($class_id > 0) {
//                    $teacherIds = $teacherDao->getTeacherIDOfClass($class_id);
//                }
//                $teacherIds[] = $user->_data['user_id']; //User hiện tại là quản lý của trường
                // Tạm thời không cho kết bạn giữa giáo viên, phụ huynh
                /*$userDao->becomeFriends($teacherIds, $addedParentIds);*/
            }
            //Cập nhật số lượng học sinh theo giới tính của trường
            if ($oldChild['gender'] != $args['gender']) {
                //Giảm giới tính cũ
                $schoolDao->updateGenderCount($school['page_id'], $oldChild['gender'], -1);
                //Tăng giới tính mới
                $schoolDao->updateGenderCount($school['page_id'], $args['gender'], 1);

                //Cập nhật thông tin trường
                if ($oldChild['gender'] == MALE) {
                    $school['male_count'] = $school['male_count'] - 1;
                    $school['female_count'] = $school['female_count'] + 1;
                } else {
                    $school['female_count'] = $school['female_count'] - 1;
                    $school['male_count'] = $school['male_count'] + 1;
                }
            }

            // Thông báo cho phụ huynh biết trẻ được sửa
            $userDao->postNotifications($newParentIds, NOTIFICATION_UPDATE_CHILD_SCHOOL, NOTIFICATION_NODE_TYPE_CHILD,
                $args['child_parent_id'], convertText4Web($args['child_name']), $args['child_parent_id'], convertText4Web($school['page_title']));

            /* ---------- UPDATE - MEMCACHE ---------- */
            //1. Cập nhật thông tin của trường
            $info_update = array(
                'page_likes' => $school['page_likes']
            );
            $config_update = array(
                'male_count' => $school['male_count'],
                'female_count' => $school['female_count']
            );
            updateSchoolData($school['page_id'], SCHOOL_INFO, $info_update);
            updateSchoolData($school['page_id'], SCHOOL_CONFIG, $config_update);

            //2. Cập nhật thông tin lớp
            if ($class_id > 0) {
                updateClassData($class_id, CLASS_INFO);
            }
            //3.Cập nhật thông tin trẻ
            updateChildData($_POST['child_id'], CHILD_INFO);
            updateChildData($_POST['child_id'], CHILD_PARENTS);

            //4. Cập nhật tin tin quản lý trẻ
            foreach ($addedParentIds as $parentId) {
                updateUserManageData($parentId, USER_MANAGE_CHILDREN);
            }
            foreach ($deletedParentIds as $parentId) {
                updateUserManageData($parentId, USER_MANAGE_CHILDREN);
            }
            /* ---------- END - MEMCACHE ---------- */

            $db->commit();
            //return_json( array('success' => true, 'message' => __("Done, Child info have been updated")) );
            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/school/' . $_POST['school_username'] . '/children' . '";'));
            break;
        case 'add':
            if (!canEdit($_POST['school_username'], 'children')) {
                _error(403);
            }
            /**
             * Hàm này xử lý khi trường tạo ra thông tin một trẻ. Công việc bao gồm
             * 1. Tạo thông tin trẻ trong bảng ci_child
             * 2. Cập nhật danh sách lớp (đưa trẻ vào lớp - bảng ci_class_child)
             * 2.2 nhập thông tin của trẻ vào bảng điểm ci_point_
             * 2.3 Nhập thông tin của trẻ vào bảng hạnh kiểm ci_conduct
             * 3. Cập nhật thông tin trẻ trong trường (cập nhật bảng ci_school_child)
             * 3.4. Tự động tạo tài khoản học sinh
             * 3.5. Tự động tạo tài khoản phụ huynh
             * 4. Thêm cha mẹ vào nhóm của lớp (bao gồm việc tăng
             * 5. Thêm cha mẹ cho trẻ
             * 6. Cho cha mẹ like trang của trường
             * 7. Cho cha mẹ thành bạn của giáo viên (bao gồm cả quản lý trường)
             * 8. Tăng số lượng trẻ của trường
             * 9. Đăng ký dịch vụ cho trẻ nếu chọn
             */

            if(isset($_POST['begin_at']) && !validateDate($_POST['begin_at'])) {
                throw new Exception(__("You must enter study start date"));
            }
            $birthday = toDBDate($args['birthday']);
            $beginAt = toDBDate($args['begin_at']);
            if(strtotime($birthday) > strtotime($beginAt)) {
                throw new Exception(__("Begin at not before birthdate"));
            }
            //Validate điều kiện tạo tài khoản phụ huynh
            $create_parent_account = $_POST['create_parent_account'];
            if ($create_parent_account && !is_empty($args['parent_email']) && !valid_email($args['parent_email'])) {
                throw new Exception(__("To create parent account, you must enter correct parent email or parent email is empty."));
            }
            //Validate điều kiện tạo tài khoản học sinh
            if (!is_empty($_POST['child_email']) && !valid_email($_POST['child_email'])) {
                throw new Exception(__("To create children account, you must enter correct Student email or Student email is empty."));
            }
            if (is_empty($_POST['child_password']) || is_empty($_POST['child_password_confirm'])) {
                throw new Exception(__("Please enter a password and confirm your password."));
            }
            if ($_POST['child_password'] != $_POST['child_password_confirm']) {
                throw new Exception(__("Those passwords didn’t match. Try again."));
            }
            $class_id = is_numeric($_POST['class_id']) ? $_POST['class_id'] : 0;
            $db->begin_transaction();

            $args['code_auto'] = isset($_POST['code_auto'])? $_POST['code_auto']: 0;
            //Sinh mã trẻ nếu cần
            if ($args['code_auto']) {
                $args['child_code'] = generateCode($args['child_name']);
            }

            $childInfo = $childDao->getStatusChildInSchool($args['child_code'], $school['page_id']);
            // Nếu trẻ chưa có trong hệ thống thì tạo mới thông tin trẻ trong trường
            if ($childInfo['status'] == 0) {
                $args['child_parent_id'] = 0;
                //1. Tạo thông tin trẻ trong bảng ci_child
                $lastId = $childDao->createChild($args);

                //2. Cập nhật danh sách lớp (đưa trẻ vào lớp - bảng ci_class_child)
                // Nếu ko chọn class thì thêm trẻ vào trường, bỏ qua bước này
                $argsCC['class_id'] = $class_id;
                $argsCC['begin_at'] = isset($_POST['begin_at'])? trim($_POST['begin_at']) : "";
                $argsCC['child_id'] = $lastId;
                if ($class_id > 0) {
                    $childDao->addChildToClass($argsCC);
                    //2.2 nhập thông tin của trẻ vào bảng điểm ci_point_
                    $subjectOfClass = $subjectDao->getSubjectsByClassId($class_id,$school['page_id']);
                    if(count($subjectOfClass)>0) {
                        foreach($subjectOfClass as $subject) {
                            $childDao->addChildPoints($subject['subject_id'],$args['child_code'],$class_id,$subject['school_year']);
                        }
                    }
                    //2.3 Nhập thông tin của trẻ vào bảng hạnh kiểm ci_conduct
                    $school_year = date("Y", strtotime(str_replace('/', '-', $_POST['begin_at']))).'-'.(date("Y", strtotime(str_replace('/', '-', $_POST['begin_at'])))+1);
                    $childDao->addChildConduct($class_id,$argsCC['child_id'],$school_year);
                }

                //3. Cập nhật thông tin trẻ trong trường (cập nhật bảng ci_school_child)
                $argsCC['school_id'] = $school['page_id'];
                $argsCC['child_parent_id'] = 0;
                $childDao->addChildToSchool($argsCC);

                $parentIds = (isset($_POST['user_id'])? $_POST['user_id'] : array());

                // bỏ user_id trùng
                $parentIds = array_unique($parentIds);



                //3.1. Tự động tạo tài khoản cho học sinh


                    $argsChildren = array();
                    $argsChildren['first_name'] = $args['first_name'];
                    $argsChildren['last_name'] = $args['last_name'];
                    $argsChildren['full_name'] = $args['last_name'].' '.$args['first_name'];
                    $argsChildren['username'] = generateUsername($argsChildren['full_name']);
                    $argsChildren['user_phone'] = standardizePhone($args['parent_phone']);
                    /*if (!validatePhone($args['user_phone'])) {
                        return_json( array('error' => true, 'message' => __("The phone number you entered is incorrect")) );
                    }*/
                $argsChildren['email'] = $_POST['child_email'];
                if(check_email($_POST['child_email'])) {
                    throw new Exception(__("Sorry, it looks like") ." ". $_POST['email'] ." ". __("belongs to an existing account"));
                }
                $argsChildren['password'] = $_POST['child_password'];
                $argsChildren['gender'] = $args['gender'];

                    $childrenId = $userDao->createUser($argsChildren);
                    $childrenIds[] = $childrenId;
                    // tạo user của trẻ tự quản lý bản thân trong bảng ci_user_manage
                    $parentDao->addItSelf($lastId, $childrenId);
                    //3.2. Cho học sinh like các page của hệ thống, join các group của hệ thống
                    $userDao->addUsersLikeSomePages($system_page_ids, $childrenIds);
                    $userDao->addUserToSomeGroups($system_group_ids, $childrenIds);

                    //4. Thêm học sinh vào nhóm của lớp (bao gồm việc tăng thành viên nhóm)
                    // Nếu chưa chọn lớp thì bỏ qua
                    if ($class_id > 0) {
                        $classDao->addUserToClass($class_id, $childrenIds);
                    }
                    //6. Cho học sinh like trang của trường (tăng số like)
                    $likeCnt = $schoolDao->addUsersLikeSchool($school['page_id'], $childrenIds);
                    $school['page_likes'] = $school['page_likes'] + $likeCnt;

                //3.1. Tự động tạo tài khoản phụ huynh
                if ($create_parent_account) {
                    $argsParent = array();
                    $argsParent['full_name'] = $args['parent_name_creat_account'];
                    $argsParent['first_name'] = split_name($argsParent['full_name'])[0];
                    $argsParent['last_name'] = split_name($argsParent['full_name'])[1];
                    $argsParent['username'] = generateUsername($argsParent['full_name']);
                    $argsParent['user_phone'] = standardizePhone($args['parent_phone_creat_account']);
                    /*if (!validatePhone($args['user_phone'])) {
                        return_json( array('error' => true, 'message' => __("The phone number you entered is incorrect")) );
                    }*/
                    $argsParent['email'] = ($args['parent_email'] != '') ? $args['parent_email']: 'null';
                    $argsParent['password'] = (is_empty($argsParent['user_phone'])? "coniu.vn": $argsParent['user_phone']);
                    $argsParent['gender'] = FEMALE;

                    $parentId = $userDao->createUser($argsParent);
                    $parentIds[] = $parentId;
                    if($argsParent['email'] != 'null') {
                        //Gửi mail cho phụ huynh của trẻ.
                        $argEmail = array();
                        $argEmail['action'] = "create_parent_account";
                        $argEmail['receivers'] = $argsParent['email'];
                        $argEmail['subject'] = "[". convertText4Web($school['page_title']) ."]".__("Use Inet application to connect student's parents");

                        //Tạo nên nội dung email
                        $smarty->assign('full_name', $argsParent['full_name']);
                        $smarty->assign('child_name', $args['child_name']);
                        $smarty->assign('username', $argsParent['email']);
                        $smarty->assign('password', $argsParent['password']);
                        $argEmail['content'] = $smarty->fetch("ci/email_templates/create_parent_account.tpl");

                        $argEmail['delete_after_sending'] = 1;
                        $argEmail['school_id'] = $school['page_id'];
                        $argEmail['user_id'] = $user->_data['user_id'];

                        $mailDao->insertEmail($argEmail);
                    }
                }
                if (count($parentIds) > 0){
                    //3.2. Cho phụ huynh like các page của hệ thống, join các group của hệ thống
                    $userDao->addUsersLikeSomePages($system_page_ids, $parentIds);
                    $userDao->addUserToSomeGroups($system_group_ids, $parentIds);

                    //4. Thêm cha mẹ vào nhóm của lớp (bao gồm việc tăng thành viên nhóm)
                    // Nếu chưa chọn lớp thì bỏ qua
                    if ($class_id > 0) {
                        $classDao->addUserToClass($class_id, $parentIds);
                    }

                    //5. Thêm cha mẹ cho trẻ
                    $parentDao->addParentList($lastId, $parentIds);

                    //6. Cho cha mẹ like trang của trường (tăng số like)
                    $likeCnt = $schoolDao->addUsersLikeSchool($school['page_id'], $parentIds);
                    $school['page_likes'] = $school['page_likes'] + $likeCnt;

                    //7. Cho cha mẹ thành bạn của giáo viên (bao gồm cả quản lý trường)
                    // Nếu chưa chọn lớp thì bỏ qua
                    // Tạm thời không cho kết bạn giữa giáo viên, phụ huynh
                    /*if ($class_id > 0) {
                        $teacherIds = $teacherDao->getTeacherIDOfClass($class_id);
                    }
                    $teacherIds[] = $user->_data['user_id']; //User hiện tại là quản lý của trường
                    $userDao->becomeFriends($teacherIds, $parentIds);*/

                    //7. Tạo thông tin trẻ cho phụ huynh (ci_child_parent)
                    $args['child_id'] = $lastId;
                    $args['child_admin'] = $parentIds[0];
                    $args['school_id'] = $school['page_id'];
                    $childParentId = $childDao->createChildForParent($args);

                    //7.1. Gắn trẻ được quản lý cho phụ huynh (ci_parent_manage)
                    $childDao->createParentManageInfo($parentIds, $childParentId, $lastId, $school['page_id']);
                } else {
                    //7. Tạo thông tin trẻ cho phụ huynh (ci_child_parent)
                    $args['child_id'] = $lastId;
                    $child['child_admin'] = 0;
                    $args['school_id'] = $school['page_id'];
                    $childParentId = $childDao->createChildForParent($args);
                }
                //8. Tăng số lượng trẻ của trường
                $schoolDao->updateGenderCount($school['page_id'], $args['gender'], 1);
                // 9. Đăng ký sử dụng dịch vụ
                if($_POST['for_service'] == "on") {
                    $serviceIds = $_POST['serviceIds'];
                    if(count($serviceIds) > 0) {
                        $serviceDao->registerServiceForChildOfAddChild($lastId, $serviceIds, $_POST['begin_at']);
                    }
                }

                if ($args['gender'] == MALE) {
                    $school['male_count'] = $school['male_count'] + 1;
                } else {
                    $school['female_count'] = $school['female_count'] + 1;
                }
            } else {
                throw new Exception(__("Student is existing in system, check student code"));
            }

            /* ---------- UPDATE - MEMCACHE ---------- */
            //1. Cập nhật thông tin của trường
            $info_update = array(
                'page_likes' => $school['page_likes']
            );
            $config_update = array(
                'male_count' => $school['male_count'],
                'female_count' => $school['female_count']
            );

            updateSchoolData($school['page_id'], SCHOOL_INFO, $info_update);
            updateSchoolData($school['page_id'], SCHOOL_CONFIG, $config_update);
            addSchoolData($school['page_id'], SCHOOL_CHILDREN, $args['child_id']);

            //2. Cập nhật thông tin trẻ và lớp
            addClassData($class_id, CLASS_CHILDREN, $args['child_id']);
            updateClassData($class_id, CLASS_INFO);

            //3. Cập nhật tin tin quản lý trẻ
            foreach ($parentIds as $parentId) {
                updateUserManageData($parentId, USER_MANAGE_CHILDREN);
            }
            /* ---------- END - MEMCACHE ---------- */

            $db->commit();
            return_json( array('success' => true, 'message' => __("Done, Child info has been created")) );

            break;
        case 'addexisting':
            if (!canEdit($_POST['school_username'], 'children')) {
                _error(403);
            }
            /**
             * Hàm này xử lý khi trường tạo ra thông tin một trẻ. Công việc bao gồm
             * 1. Tạo thông tin trẻ trong bảng ci_child
             * 2. Cập nhật danh sách lớp (đưa trẻ vào lớp - bảng ci_class_child)
             * 2.2 nhập thông tin của trẻ vào bảng điểm ci_point_
             * 2.3 Nhập thông tin của trẻ vào bảng hạnh kiểm ci_conduct
             * 3. Cập nhật thông tin trẻ trong trường (cập nhật bảng ci_school_child)
             * 3.5. Tự động tạo tài khoản phụ huynh
             * 4. Thêm cha mẹ vào nhóm của lớp (bao gồm việc tăng
             * 5. Thêm cha mẹ cho trẻ
             * 6. Cho cha mẹ like trang của trường
             * 7. Cho cha mẹ thành bạn của giáo viên (bao gồm cả quản lý trường)
             * 8. Tăng số lượng trẻ của trường
             */

            if(isset($_POST['begin_at']) && !validateDate($_POST['begin_at'])) {
                throw new Exception(__("You must enter study start date"));
            }
            $birthday = toDBDate($args['birthday']);
            $beginAt = toDBDate($args['begin_at']);
            if(strtotime($birthday) > strtotime($beginAt)) {
                throw new Exception(__("Begin at not before birthdate"));
            }
            //Validate điều kiện tạo tài khoản phụ huynh
            $create_parent_account = $_POST['create_parent_account'];
            if ($create_parent_account && !is_empty($args['parent_email']) && !valid_email($args['parent_email'])) {
                throw new Exception(__("To create parent account, you must enter correct parent email or parent email is empty."));
            }
            $class_id = is_numeric($_POST['class_id']) ? $_POST['class_id'] : 0;
            $db->begin_transaction();

            //$args['code_auto'] = isset($_POST['code_auto'])? $_POST['code_auto']: 0;
            $args['child_parent_id'] = $_POST['child_parent_id'];
            //Sinh mã trẻ nếu cần
            /*if ($args['code_auto']) {
                $args['child_code'] = generateCode($args['child_name']);
            }*/

            //1. Tạo thông tin trẻ trong bảng ci_child
            $lastId = $childDao->createChild($args);

            //2. Cập nhật danh sách lớp (đưa trẻ vào lớp - bảng ci_class_child)
            // Nếu ko chọn class thì thêm trẻ vào trường, bỏ qua bước này
            $argsCC['class_id'] = $class_id;
            $argsCC['begin_at'] = isset($_POST['begin_at'])? trim($_POST['begin_at']) : "";
            $argsCC['child_id'] = $lastId;
            if ($class_id > 0) {
                $childDao->addChildToClass($argsCC);
                // ADD START MANHDD 03/06/2021
                //2.2 nhập thông tin của trẻ vào bảng điểm ci_point_
                $subjectOfClass = $subjectDao->getSubjectsByClassId($class_id,$school['page_id']);
                if(count($subjectOfClass)>0) {
                    foreach($subjectOfClass as $subject) {
                        $childDao->addChildPoints($subject['subject_id'],$args['child_code'],$class_id,$subject['school_year']);
                    }
                }
                //2.3 Nhập thông tin của trẻ vào bảng hạnh kiểm ci_conduct
                $school_year = date("Y", strtotime($_POST['begin_at'])).'-'.(date("Y", strtotime($_POST['begin_at']))+1);
                $childDao->addChildConduct($class_id,$argsCC['child_id'],$school_year);
                // ADD END MANHDD 03/06/2021
            }

            //3. Cập nhật thông tin trẻ trong trường (cập nhật bảng ci_school_child)
            $argsCC['school_id'] = $school['page_id'];
            $argsCC['child_parent_id'] = $_POST['child_parent_id'];
            $childDao->addChildToSchool($argsCC);

            $parentIds = (isset($_POST['user_id'])? $_POST['user_id'] : array());

            // bỏ user_id trùng
            $parentIds = array_unique($parentIds);

            //3.1. Tự động tạo tài khoản phụ huynh
            if ($create_parent_account) {
                $argsParent = array();
                $argsParent['full_name'] = $args['parent_name_creat_account'];
                $argsParent['first_name'] = split_name($argsParent['full_name'])[0];
                $argsParent['last_name'] = split_name($argsParent['full_name'])[1];
                $argsParent['username'] = generateUsername($argsParent['full_name']);
                $argsParent['user_phone'] = standardizePhone($args['parent_phone_creat_account']);
                /*if (!validatePhone($args['user_phone'])) {
                    return_json( array('error' => true, 'message' => __("The phone number you entered is incorrect")) );
                }*/
                $argsParent['email'] = ($args['parent_email']!= '')?$args['parent_email']:'null';
                $argsParent['password'] = (is_empty($argsParent['user_phone'])? "coniu.vn": $argsParent['user_phone']);
                $argsParent['gender'] = FEMALE;

                $parentId = $userDao->createUser($argsParent);
                $parentIds[] = $parentId;
                if($argsParent['email'] != 'null') {
                    //Gửi mail cho phụ huynh của trẻ.
                    $argEmail = array();
                    $argEmail['action'] = "create_parent_account";
                    $argEmail['receivers'] = $argsParent['email'];
                    $argEmail['subject'] = "[".convertText4Web($school['page_title'])."]".__("Use Inet application to connect student's parents");

                    //Tạo nên nội dung email
                    $smarty->assign('full_name', $argsParent['full_name']);
                    $smarty->assign('child_name', $args['child_name']);
                    $smarty->assign('username', $argsParent['email']);
                    $smarty->assign('password', $argsParent['password']);
                    $argEmail['content'] = $smarty->fetch("ci/email_templates/create_parent_account.tpl");

                    $argEmail['delete_after_sending'] = 1;
                    $argEmail['school_id'] = $school['page_id'];
                    $argEmail['user_id'] = $user->_data['user_id'];

                    $mailDao->insertEmail($argEmail);
                }

            }
            if (count($parentIds) > 0){
                //3.2. Cho phụ huynh like các page của hệ thống, join các group của hệ thống
                $userDao->addUsersLikeSomePages($system_page_ids, $parentIds);
                $userDao->addUserToSomeGroups($system_group_ids, $parentIds);

                //4. Thêm cha mẹ vào nhóm của lớp (bao gồm việc tăng thành viên nhóm)
                // Nếu chưa chọn lớp thì bỏ qua
                if ($class_id > 0) {
                    $classDao->addUserToClass($class_id, $parentIds);
                }

                //5. Thêm cha mẹ cho trẻ
                $parentDao->addParentList($lastId, $parentIds);

                //6. Cho cha mẹ like trang của trường (tăng số like)
                $likeCnt = $schoolDao->addUsersLikeSchool($school['page_id'], $parentIds);
                $school['page_likes'] = $school['page_likes'] + $likeCnt;

                //7. Cho cha mẹ thành bạn của giáo viên (bao gồm cả quản lý trường)
                // Nếu chưa chọn lớp thì bỏ qua
                // Tạm thời không cho kết bạn giữa giáo viên, phụ huynh
                /*if ($class_id > 0) {
                    $teacherIds = $teacherDao->getTeacherIDOfClass($class_id);
                }
                $teacherIds[] = $user->_data['user_id']; //User hiện tại là quản lý của trường
                $userDao->becomeFriends($teacherIds, $parentIds);*/

                //7. Gắn trẻ được quản lý cho phụ huynh (ci_parent_manage)
                $parents = $childDao->getParentManageChild($_POST['child_parent_id']);
                $oldParentIds = array();
                foreach ($parents as $parent) {
                    $oldParentIds[] = $parent['user_id'];
                }
                $addedParentIds = array_diff($parentIds, $oldParentIds);

                $childDao->createParentManageInfo($addedParentIds, $_POST['child_parent_id'], $lastId, $school['page_id']);
                $childDao->updateParentManageInfo($oldParentIds, $_POST['child_parent_id'], $lastId, $school['page_id']);
            }
            //8. Tăng số lượng trẻ của trường
            $schoolDao->updateGenderCount($school['page_id'], $args['gender'], 1);

            if ($args['gender'] == MALE) {
                $school['male_count'] = $school['male_count'] + 1;
            } else {
                $school['female_count'] = $school['female_count'] + 1;
            }

            // Thông báo cho phụ huynh biết trẻ được thêm vào trường
            $userDao->postNotifications($parentIds, NOTIFICATION_NEW_CHILD_EXIST, NOTIFICATION_NODE_TYPE_CHILD,
                $lastId, convertText4Web($args['child_name']), $lastId, convertText4Web($school['page_title']));

            /* ---------- UPDATE - MEMCACHE ---------- */
            //1. Cập nhật thông tin của trường
            $info_update = array(
                'page_likes' => $school['page_likes']
            );
            $config_update = array(
                'male_count' => $school['male_count'],
                'female_count' => $school['female_count']
            );

            updateSchoolData($school['page_id'], SCHOOL_INFO, $info_update);
            updateSchoolData($school['page_id'], SCHOOL_CONFIG, $config_update);
            addSchoolData($school['page_id'], SCHOOL_CHILDREN, $lastId);

            //2. Cập nhật thông tin trẻ và lớp
            addClassData($class_id, CLASS_CHILDREN, $lastId);
            updateClassData($class_id, CLASS_INFO);

            //3. Cập nhật tin tin quản lý trẻ
            foreach ($parentIds as $parentId) {
                updateUserManageData($parentId, USER_MANAGE_CHILDREN);
            }
            /* ---------- END - MEMCACHE ---------- */

            $db->commit();
//            return_json( array('success' => true, 'message' => __("Done, Child info has been created")) );
            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/school/' . $_POST['school_username'] . '/children/' . '";'));
            break;
        case 'approve':
            if (!canEdit($_POST['school_username'], 'children')) {
                _error(403);
            }
            //Xác nhận user là phụ huynh của trẻ
            $db->begin_transaction();

            //1. Lấy ra thông tin cũ của trẻ để kiểm tra
            $oldChild = getChildData($_POST['child_id'], CHILD_INFO);
            if (is_null($oldChild)) {
                _error(404);
            }
            //2. Lấy ra thông tin cũ của trẻ để kiểm tra
            $oldParents = getChildData($_POST['child_id'], CHILD_PARENTS);
            $oldParentIds = array_keys($oldParents);

            //1. Lấy ra thông tin lớp của trẻ
            $class = getChildData($_POST['child_id'], CHILD_CLASS);
            //2. Thêm cha mẹ vào nhóm của lớp (bao gồm việc tăng thành viên nhóm)
            $classDao->addUserToClass($class['group_id'], [$_POST['parent_id']]);
            //3. Cho cha mẹ like trang của trường (tăng số like)
            $likeCnt = $schoolDao->addUsersLikeSchool($school['page_id'], [$_POST['parent_id']]);
            $school['page_likes'] = $school['page_likes'] + $likeCnt;
            //4. Cho cha mẹ thành bạn của giáo viên (bao gồm cả quản lý trường)
            // Tạm thời không cho kết bạn giữa giáo viên, phụ huynh
            /*$teacherIds = $teacherDao->getTeacherIDOfClass($class['group_id']);
            $teacherIds[] = $user->_data['user_id']; //User hiện tại là quản lý của trường
            $userDao->becomeFriends($teacherIds, [$_POST['parent_id']]);*/

            //5. Thêm cha mẹ vào danh sách cha mẹ của trẻ
            $parentDao->addParentList($_POST['child_id'], [$_POST['parent_id']]);

            //7. Tạo thông tin trẻ cho phụ huynh (ci_child_parent)
            if ($oldChild['status'] == STATUS_ACTIVE && count($oldParentIds) == 0) {
                $childAdmin = $_POST['parent_id'];
                $childParentId = $oldChild['child_parent_id'];
                $childDao->updateChildForParent($childParentId, $childAdmin);
            }

            //7.1. Gắn trẻ được quản lý cho phụ huynh (ci_parent_manage)
            $childDao->createParentManageInfo([$_POST['parent_id']], $oldChild['child_parent_id'], $_POST['child_id'], $school['page_id']);
            $db->commit();

            /* ---------- UPDATE - MEMCACHE ---------- */
            //1. Cập nhật thông tin trường
            $info_update = array(
                'page_likes' => $school['page_likes']
            );
            updateSchoolData($school['page_id'], SCHOOL_INFO, $info_update);
            //2. Cập nhật thông tin lớp
            updateClassData($class['group_id']);
            //3. Cập nhật thông tin trẻ
            updateChildData($_POST['child_id']);
            /* ---------- END - MEMCACHE ---------- */

            return_json( array('success' => true, 'message' => __("Approved parent")) );
            break;
//        case 'delete':
//            if (!canEdit($_POST['school_username'], 'children')) {
//                _error(403);
//            }
//            if(!isset($_POST['id']) || !is_numeric($_POST['id'])) {
//                _error(400);
//            }
//            $db->begin_transaction();
//            $today = date('d/m/Y');
//            //$child = $childDao->getChild($_POST['id']);
//            $child = getChildData($_POST['id'], CHILD_INFO);
//
//            // Lấy danh sách phụ huynh của trẻ
//            $parents = getChildData($_POST['id'], CHILD_PARENTS);
//            $parentIds = array_keys($parents);
//
//            // 1. Kiểm tra xem trẻ đã có hoạt động trong trường chưa (điểm danh hoặc học phí hoặc đăng ký dịch vụ hoặc tạo sổ liên lạc
//            //Học phí
//            $tuitions = $tuitionDao->getTuitionsByChild($_POST['id']);
//            // Sổ liên lạc
//            $reports = $reportDao->getChildReport($_POST['id']);
//            // dịch vụ
//            $notCountBasedServices = $serviceDao->getChildServiceNotCountBased($school['page_id'], $child['child_id']);
//            // điểm danh
//            $attendances = $attendanceDao->getAttendanceChild($_POST['id'], '01/01/2016', $today);
//
//            if(count($tuitions) > 0 || count($reports) > 0 || count($notCountBasedServices) > 0 || count($attendances['attendance']) > 0) {
//                // Trường hợp Trẻ đã có tương tác với hệ thống, xóa trẻ khỏi bảng ci_school_child và ci_class_child, không xóa trẻ
//
//                // Loại phụ huynh khỏi group
//                $classDao->deleteUserFromClass($child['class_id'], $parentIds);
//
//            } else {
//                // Trường hợp trẻ chưa có tương tác với hệ thống
//                if(in_array($child['created_user_id'], $parentIds)) {
//                    // trẻ do phụ huynh tạo (loại trẻ ra khỏi trường)
//
//                    // Loại phụ huynh khỏi group
//                    $classDao->deleteUserFromClass($child['class_id'], $parentIds);
//                } else {
//                    // Trẻ do trường tạo (không phải phụ huynh tạo)
//
//                    // Xóa trẻ khỏi bảng ci_child
//                    $childDao->deleteChild($_POST['id']);
//
//                    // Xóa trẻ khỏi bảng ci_child_parent
//                    $childDao->deleteChildForParent($child['child_parent_id']);
//
//                    // Xóa trẻ khỏi bảng ci_parent_manage
//                    $childDao->deleteAllParentOfChildForParent($child['child_parent_id']);
//
//                    // Kiểm tra tài khoản phụ huynh của trẻ là do trường tạo hay phụ huynh tự tạo
//                    foreach ($parents as $parent) {
//                        if($parent['created_school_id'] == $school['page_id']) {
//                            // Trường hợp phụ huynh do trường tạo thì xóa luôn phụ huynh
//                            $userDao->deleteUser($parent['user_id']);
//                            // Xóa thông tin quản lý trẻ
//                            deleteUserManageData($parent['user_id'], ALL);
//                        }
//                    }
//                }
//            }
//
//            // Xóa trẻ khỏi bảng ci_class_child và ci_school_child
//            $childDao->deleteClassChild($_POST['id']);
//            $childDao->deleteSchoolChild($_POST['id']);
//
//            // Xóa trẻ khỏi bảng ci_user_manage
//            $childDao->deleteAllParentOfChild($_POST['id']);
//
//            // Cho trẻ nghỉ học trong bảng ci_parent_manage
//            $childDao->deleteParentManageInfo($child['child_id'], $child['child_parent_id']);
//
//            //Cập nhật số lượng trẻ của trường
//            $schoolDao->updateGenderCount($school['page_id'], $child['gender'], -1);
//
//            if ($child['gender'] == MALE) {
//                $school['male_count'] = $school['male_count'] - 1;
//            } else {
//                $school['female_count'] = $school['female_count'] - 1;
//            }
//
//            /* ---------- UPDATE - MEMCACHE ---------- */
//            //1. Cập nhật thông tin của trường
//            $config_update = array(
//                'male_count' => $school['male_count'],
//                'female_count' => $school['female_count']
//            );
//            updateSchoolData($school['page_id'], SCHOOL_CONFIG, $config_update);
//            deleteSchoolData($school['page_id'], SCHOOL_CHILDREN, $child['child_id']);
//
//            //2. Cập nhật thông tin lớp
//            deleteClassData($child['class_id'], CLASS_CHILDREN, $child['child_id']);
//            updateClassData($child['class_id'], CLASS_INFO);
//
//            //3. Cập nhật thông tin trẻ
//            deleteChildData($child['child_id']);
//
//            //4. Cập nhật thông tin quản lý trẻ của phụ huynh
//            foreach ($parentIds as $parentId) {
//                updateUserManageData($parentId, USER_MANAGE_CHILDREN);
//            }
//            /* ---------- END - MEMCACHE ---------- */
//
//            $db->commit();
//            $return['path'] = $system['system_url']."/school/".$_POST['school_username']."/children";
//            return_json($return);
//            break;
        case 'delete': // Đây là case delete cũ, để đây đề phòng dùng lại (đã dùng lại)
            if (!canEdit($_POST['school_username'], 'children')) {
                _error(403);
            }
            if(!isset($_POST['id']) || !is_numeric($_POST['id'])) {
                _error(400);
            }
            $db->begin_transaction();

            // Xóa học phí tháng hiện tại của trẻ nếu có
            $month = date('Y-m-01');
            $tuitionChildId = $tuitionDao->getTuitionChildId($_POST['id'], $month);
            if(!is_null($tuitionChildId)) {
                $tuitionChild = $tuitionDao->getTuitionChildOnly($tuitionChildId);
                if (!is_null($tuitionChild)) {
                    //Cập nhật thông tin học phí của lớp.
                    $tuitionDao->updateTuitionWhenDeletingChild($tuitionChild);
                    //Xóa học phí của trẻ.
                    $tuitionDao->deleteTuitionChild($tuitionChildId);
                }
            }

            //$child = $childDao->getChild($_POST['id']);
            $child = getChildData($_POST['id'], CHILD_INFO);
            $childDao->deleteChild($_POST['id']);
            $childDao->deleteClassChild($_POST['id']);
            $childDao->deleteSchoolChild($_POST['id']);
            //Cập nhật số lượng trẻ của trường
            $schoolDao->updateGenderCount($school['page_id'], $child['gender'], -1);

            if ($child['gender'] == MALE) {
                $school['male_count'] = $school['male_count'] - 1;
            } else {
                $school['female_count'] = $school['female_count'] - 1;
            }

            //Xóa trẻ trong trường mà phụ huynh quản lý (ci_parent_manage)
            $childDao->deleteParentManageInfo($_POST['id'], $child['child_parent_id']);

            /* ---------- UPDATE - MEMCACHE ---------- */
            //1. Cập nhật thông tin của trường
            $config_update = array(
                'male_count' => $school['male_count'],
                'female_count' => $school['female_count']
            );
            updateSchoolData($school['page_id'], SCHOOL_CONFIG, $config_update);
            deleteSchoolData($school['page_id'], SCHOOL_CHILDREN, $child['child_id']);

            //2. Cập nhật thông tin lớp
            deleteClassData($child['class_id'], CLASS_CHILDREN, $child['child_id']);
            updateClassData($child['class_id'], CLASS_INFO);

            //3. Cập nhật thông tin trẻ
            deleteChildData($child['child_id']);
            /* ---------- END - MEMCACHE ---------- */

            $db->commit();
            $return['path'] = $system['system_url']."/school/".$_POST['school_username']."/children";
            return_json($return);
            break;
        case 'export_4leaving':
            $reportTemplate = ABSPATH."content/templates/tuition_4leave_".$school['tuition_child_report_template']."_".$system['language']['code'].".xlsx";
            include_once(ABSPATH . 'includes/libs/PHPExcel/PHPExcel/IOFactory.php');
            include_once(ABSPATH . 'includes/libs/PHPExcel/PHPExcel.php');

            // Lấy thông tin học phí
            $data = $tuitionDao->getLeaveTuitionDetail($school['page_id'], $_POST['child_id']);
            if (is_null($data)) {
                _error(404);
            }
            $objPHPExcel = PHPExcel_IOFactory::load($reportTemplate);
            $child = $childDao->getChild4Leave($_POST['child_id']);

            processExport4Leave($objPHPExcel, $child, $school, $data, "Tuition");

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            ob_start();
            $childName = str_replace(" ", '_', convert_vi_to_en($child["child_name"]));
            $fileName = convertText4Web($childName . '_account_4leaving.xlsx');
            $objWriter->save("php://output");

            $xlsData = ob_get_contents();
            ob_end_clean();
            return_json(array(
                'file_name' => $fileName,
                'file' => "data:application/vnd.ms-excel;base64," . base64_encode($xlsData)
            ));

            break;
        case 'add_health_index':
// Thêm thông tin chiều cao cân nặng của tất cả trẻ trong lớp
            if (!isset($_POST['recorded_at'])) {
                throw new Exception(__("You must enter correct recorded_at"));
            }
            $childIds = isset($_POST['childIds'])?$_POST['childIds']:array();
//            $height = isset($_POST['height'])?$_POST['height']:array();
//            $weight = isset($_POST['weight'])?$_POST['weight']:array();
//            $nutritureSstatus = isset($_POST['nutriture_status'])?$_POST['nutriture_status']:array();
//            $heart = isset($_POST['heart'])?$_POST['heart']:array();
//            $bloodPressure = isset($_POST['blood_pressure'])?$_POST['blood_pressure']:array();
//            $ear = isset($_POST['ear'])?$_POST['ear']:array();
//            $eye = isset($_POST['eye'])?$_POST['eye']:array();
//            $description = isset($_POST['description'])?$_POST['description']:array();
            if(count($childIds) > 0) {
                foreach ($childIds as $childId) {
                    if(isset($_POST['height_'.$childId]) && is_numeric($_POST['height_'.$childId]) && isset($_POST['weight_'.$childId]) && is_numeric($_POST['weight_'.$childId])) {
                        // Lấy child_parent_id
                        $child_parent_id = $childDao->getChildParentIdFromChildId($childId);

                        $file_name = "";
                        // Upload file đính kèm
                        if (file_exists($_FILES['file_'.$childId]['tmp_name'])) {
                            // check file upload enabled
                            if (!$system['file_enabled']) {
                                throw new Exception(__("This feature has been disabled"));
                            }

                            // valid inputs
                            if (!isset($_FILES['file_'.$childId]) || $_FILES['file_'.$childId]["error"] != UPLOAD_ERR_OK) {
                                throw new Exception(__("Something wrong with upload! Is 'upload_max_filesize' set correctly?"));
                            }

                            // check file size
                            $max_allowed_size = $system['max_file_size'] * 1024;
                            if ($_FILES['file_'.$childId]["size"] > $max_allowed_size) {
                                throw new Exception(__("The file size is so big"));
                            }

                            // check file extesnion
                            $extension = get_extension($_FILES['file_'.$childId]['name']);
                            if (!valid_extension($extension, $system['file_medicine_extensions'])) {
                                //modal(MESSAGE, __("Upload Error"), __("The file type is not valid or not supported"));
                                throw new Exception (__("The file type is not valid or not supported"));
                            }

                            /* check & create uploads dir */
                            $depth = '../../../../../';
                            $folder = 'growths';
                            if (!file_exists($depth . $system['system_uploads_directory'] . '/' . $folder)) {
                                @mkdir($depth . $system['system_uploads_directory'] . '/' . $folder, 0777, true);
                            }
                            if (!file_exists($depth . $system['system_uploads_directory'] . '/' . $folder . '/' . date('Y'))) {
                                @mkdir($depth . $system['system_uploads_directory'] . '/' . $folder . '/' . date('Y'), 0777, true);
                            }
                            if (!file_exists($system['system_uploads_directory'] . '/' . $folder . '/' . date('Y') . '/' . date('m'))) {
                                @mkdir($depth . $system['system_uploads_directory'] . '/' . $folder . '/' . date('Y') . '/' . date('m'), 0777, true);
                            }

                            /* prepare new file name */
                            $directory = $folder . '/' . date('Y') . '/' . date('m') . '/';
                            $prefix = $system['uploads_prefix'] . '_' . md5(time() * rand(1, 9999));

                            $image = new Image($_FILES['file_'.$childId]["tmp_name"]);

                            $file_name = $directory . $prefix . $image->_img_ext;
                            $file_name_tmp = $directory . $prefix . '_tmp' . $image->_img_ext;

                            $path = $depth . $system['system_uploads_directory'] . '/' . $file_name;
                            $path_tmp = $depth . $system['system_uploads_directory'] . '/' . $file_name_tmp;

                            /* check if the file uploaded successfully */
                            if (!@move_uploaded_file($_FILES['file_'.$childId]['tmp_name'], $path_tmp)) {
                                throw new Exception (__("Sorry, can not upload the file"));
                            }

                            /* save the new image */
                            $image->save($path, $path_tmp);

                            /* delete the tmp image */
                            unlink($path_tmp);
                            $argsGR['source_file'] = $file_name;
                            $argsGR['file_name'] = $_FILES['file_'.$childId]['name'];

                        }

                        $argsGR['child_parent_id'] = $child_parent_id;
                        $argsGR['height'] = $_POST['height_'.$childId];
                        $argsGR['weight'] = $_POST['weight_'.$childId];
                        $argsGR['nutriture_status'] = $_POST['nutriture_status_'.$childId];
                        $argsGR['heart'] = $_POST['heart_'.$childId];
                        $argsGR['blood_pressure'] = $_POST['blood_pressure_'.$childId];
                        $argsGR['ear'] = $_POST['ear_'.$childId];
                        $argsGR['eye'] = $_POST['eye_'.$childId];
                        $argsGR['nose'] = $_POST['nose_'.$childId];
                        $argsGR['description'] = $_POST['description_'.$childId];
                        $argsGR['is_parent'] = 0;


                        // Lấy thông tin chiều cao cân nặng của trẻ
                        $growth = $childDao->getChildGrowth($child_parent_id);
                        $growth_recorded = array();
                        foreach ($growth as $row) {
                            $growth_recorded[] = $row['recorded_at'];
                        }
                        // $date_now = date('d/m/Y');
                        if(!isset($_POST['recorded_at']) || is_null($_POST['recorded_at'])) {
                            throw new Exception(__("Data record date can not be empty"));
                        }
                        $recorded_at = $_POST['recorded_at'];
                        $argsGR['recorded_at'] = $recorded_at;
                        if(in_array($recorded_at, $growth_recorded)) {
                            $growth_now = $childDao->getChildGrowthByDate($argsGR['recorded_at'], $child_parent_id);
                            if(!isset($argsGR['source_file'])) {
                                $argsGR['source_file'] = $growth_now['source_file_path'];
                                $argsGR['file_name'] = $growth_now['file_name'];
                            }
                            $childDao->updateChildGrowthDateNow($argsGR);
                            $updateIdGR = $growth_now['child_growth_id'];
                        } else {
                            $insertIdGR = $childDao->insertChildGrowth($argsGR);
                        }
                        // Lấy thông tin chi tiết trẻ
                        $child = getChildData($childId, CHILD_INFO);

                        // Lấy danh sách phụ huynh của trẻ
                        $parents = getChildData($childId, CHILD_PARENTS);
                        $parentIds = array();
                        if(count($parents) > 0) {
                            foreach ($parents as $parent) {
                                $parentIds[] = $parent['user_id'];
                            }

                            // Thông báo cho phụ huynh có thông tin sức khỏe mới
                            if(isset($updateIdGR) && !isset($insertIdGR)) {
                                $userDao->postNotifications($parentIds, NOTIFICATION_UPDATE_CHILD_HEALTH, NOTIFICATION_NODE_TYPE_CHILD,
                                    $child['birthday'], $_POST['recorded_at'], $child_parent_id, convertText4Web($child['child_name']));
                            } else if(!isset($updateIdGR) && isset($insertIdGR)) {
                                $userDao->postNotifications($parentIds, NOTIFICATION_NEW_CHILD_HEALTH, NOTIFICATION_NODE_TYPE_CHILD,
                                    $child['birthday'], $_POST['recorded_at'], $child_parent_id, convertText4Web($child['child_name']));
                            }
                        }
                    }
                }
            }
            $db->begin_transaction();
            $db->commit();
            return_json( array('success' => true, 'message' => __("Done, Child info has been created")) );
            break;
        case 'edit_growth':
            // Thay đổi thông tin chiều cao, cân nặng của trẻ
            if(!isset($_POST['child_growth_id']) || !is_numeric($_POST['child_growth_id'])) {
                _error(404);
            }
            $db->begin_transaction();
            $child_growth = $childDao->getChildGrowthById($_POST['child_growth_id']);
            $args = array();
            $args['recorded_at'] = $_POST['recorded_at'];
            // Lấy thông tin chiều cao cân nặng của trẻ
            $growth = $childDao->getChildGrowth($child_growth['child_parent_id']);
            $growth_recorded = array();
            foreach ($growth as $row) {
                $growth_recorded[] = $row['recorded_at'];
            }
            if(in_array($args['recorded_at'], $growth_recorded) && ($args['recorded_at'] != $child_growth['recorded_at'])) {
                throw new Exception(__("Selected date existed"));
            }
            $args['height'] = $_POST['height'];
            $args['weight'] = $_POST['weight'];
            $args['nutriture_status'] = $_POST['nutriture_status'];
            $args['heart'] = $_POST['heart'];
            $args['blood_pressure'] = $_POST['blood_pressure'];
            $args['ear'] = $_POST['ear'];
            $args['eye'] = $_POST['eye'];
            $args['nose'] = $_POST['nose'];
            $args['description'] = $_POST['description'];
            $args['child_growth_id'] = $_POST['child_growth_id'];

            $args['source_file'] = $child_growth['source_file_path'];
            $args['file_name'] = convertText4Web($child_growth['file_name']);

            $file_name = "";
            // Upload file đính kèm
            if (file_exists($_FILES['file']['tmp_name'])) {
                // check file upload enabled
                if (!$system['file_enabled']) {
                    modal(MESSAGE, __("Not Allowed"), __("This feature has been disabled"));
                }

                // valid inputs
                if (!isset($_FILES["file"]) || $_FILES["file"]["error"] != UPLOAD_ERR_OK) {
                    modal(MESSAGE, __("Upload Error"), __("Something wrong with upload! Is 'upload_max_filesize' set correctly?"));
                }

                // check file size
                $max_allowed_size = $system['max_file_size'] * 1024;
                if ($_FILES["file"]["size"] > $max_allowed_size) {
                    modal(MESSAGE, __("Upload Error"), __("The file size is so big"));
                }

                // check file extesnion
                $extension = get_extension($_FILES['file']['name']);
                if (!valid_extension($extension, $system['file_medicine_extensions'])) {
                    //modal(MESSAGE, __("Upload Error"), __("The file type is not valid or not supported"));
                    return_json(array('error' => true, 'message' => __("The file type is not valid or not supported")));
                }

                /* check & create uploads dir */
                $depth = '../../../../../';
                $folder = 'growths';
                if (!file_exists($depth . $system['system_uploads_directory'] . '/' . $folder)) {
                    @mkdir($depth . $system['system_uploads_directory'] . '/' . $folder, 0777, true);
                }
                if (!file_exists($depth . $system['system_uploads_directory'] . '/' . $folder . '/' . date('Y'))) {
                    @mkdir($depth . $system['system_uploads_directory'] . '/' . $folder . '/' . date('Y'), 0777, true);
                }
                if (!file_exists($system['system_uploads_directory'] . '/' . $folder . '/' . date('Y') . '/' . date('m'))) {
                    @mkdir($depth . $system['system_uploads_directory'] . '/' . $folder . '/' . date('Y') . '/' . date('m'), 0777, true);
                }

                /* prepare new file name */
                $directory = $folder . '/' . date('Y') . '/' . date('m') . '/';
                $prefix = $system['uploads_prefix'] . '_' . md5(time() * rand(1, 9999));

                $image = new Image($_FILES["file"]["tmp_name"]);

                $file_name = $directory . $prefix . $image->_img_ext;
                $file_name_tmp = $directory . $prefix . '_tmp' . $image->_img_ext;

                $path = $depth . $system['system_uploads_directory'] . '/' . $file_name;
                $path_tmp = $depth . $system['system_uploads_directory'] . '/' . $file_name_tmp;

                /* check if the file uploaded successfully */
                if (!@move_uploaded_file($_FILES['file']['tmp_name'], $path_tmp)) {
                    modal(MESSAGE, __("Upload Error"), __("Sorry, can not upload the file"));
                }

                /* save the new image */
                $image->save($path, $path_tmp);

                /* delete the tmp image */
                unlink($path_tmp);
                $args['source_file'] = $file_name;
                $args['file_name'] = $_FILES['file']['name'];

            }

            $childDao->updateChildGrowth($args);

            // Lấy thông tin chi tiết trẻ
            $child = getChildData($_POST['child_id'], CHILD_INFO);

            // Lấy danh sách phụ huynh của trẻ
            $parents = getChildData($_POST['child_id'], CHILD_PARENTS);
            $parentIds = array();
            if(count($parents) > 0) {
                foreach ($parents as $parent) {
                    $parentIds[] = $parent['user_id'];
                }

                // Thông báo cho phụ huynh có thông tin sức khỏe mới
                $userDao->postNotifications($parentIds, NOTIFICATION_UPDATE_CHILD_HEALTH, NOTIFICATION_NODE_TYPE_CHILD,
                    $child['birthday'], $_POST['recorded_at'], $child_parent_id, convertText4Web($child['child_name']));
            }

            $db->commit();

            if(isset($_POST['is_module']) && $_POST['is_module'] == 1) {
                return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/school/' . $_POST['school_username'] . '/healths' . '";'));
            } else {
                return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/school/' . $_POST['school_username'] . '/children/health/'. $_POST['child_id'] . '";'));
            }
            break;

        case 'add_growth':
            // Thêm thông tin chiều cao cân nặng của trẻ trong lớp
            if (!isset($_POST['child_id']) || !is_numeric($_POST['child_id'])) {
                _error(404);
            }
            $db->begin_transaction();

            // Lấy child_parent_id
            $child_parent_id = $childDao->getChildParentIdFromChildId($_POST['child_id']);

            $file_name = "";
            // Upload file đính kèm
            if (file_exists($_FILES['file']['tmp_name'])) {
                // check file upload enabled
                if (!$system['file_enabled']) {
                    throw new Exception(__("This feature has been disabled"));
                }

                // valid inputs
                if (!isset($_FILES["file"]) || $_FILES["file"]["error"] != UPLOAD_ERR_OK) {
                    throw new Exception(__("Something wrong with upload! Is 'upload_max_filesize' set correctly?"));
                }

                // check file size
                $max_allowed_size = $system['max_file_size'] * 1024;
                if ($_FILES["file"]["size"] > $max_allowed_size) {
                    throw new Exception(__("The file size is so big"));
                }

                // check file extesnion
                $extension = get_extension($_FILES['file']['name']);
                if (!valid_extension($extension, $system['file_medicine_extensions'])) {
                    //modal(MESSAGE, __("Upload Error"), __("The file type is not valid or not supported"));
                    throw new Exception(__("The file type is not valid or not supported") );
                }

                /* check & create uploads dir */
                $depth = '../../../../../';
                $folder = 'growths';
                if (!file_exists($depth . $system['system_uploads_directory'] . '/' . $folder)) {
                    @mkdir($depth . $system['system_uploads_directory'] . '/' . $folder, 0777, true);
                }
                if (!file_exists($depth . $system['system_uploads_directory'] . '/' . $folder . '/' . date('Y'))) {
                    @mkdir($depth . $system['system_uploads_directory'] . '/' . $folder . '/' . date('Y'), 0777, true);
                }
                if (!file_exists($system['system_uploads_directory'] . '/' . $folder . '/' . date('Y') . '/' . date('m'))) {
                    @mkdir($depth . $system['system_uploads_directory'] . '/' . $folder . '/' . date('Y') . '/' . date('m'), 0777, true);
                }

                /* prepare new file name */
                $directory = $folder . '/' . date('Y') . '/' . date('m') . '/';
                $prefix = $system['uploads_prefix'] . '_' . md5(time() * rand(1, 9999));

                $image = new Image($_FILES["file"]["tmp_name"]);

                $file_name = $directory . $prefix . $image->_img_ext;
                $file_name_tmp = $directory . $prefix . '_tmp' . $image->_img_ext;

                $path = $depth . $system['system_uploads_directory'] . '/' . $file_name;
                $path_tmp = $depth . $system['system_uploads_directory'] . '/' . $file_name_tmp;

                /* check if the file uploaded successfully */
                if (!@move_uploaded_file($_FILES['file']['tmp_name'], $path_tmp)) {
                    throw new Exception (__("Sorry, can not upload the file"));
                }

                /* save the new image */
                $image->save($path, $path_tmp);

                /* delete the tmp image */
                unlink($path_tmp);
                $argsGR['source_file'] = $file_name;
                $argsGR['file_name'] = $_FILES['file']['name'];

            }

            $argsGR['child_parent_id'] = $child_parent_id;
            $argsGR['height'] = $_POST['height'];
            $argsGR['weight'] = $_POST['weight'];
            $argsGR['nutriture_status'] = $_POST['nutriture_status'];
            $argsGR['heart'] = $_POST['heart'];
            $argsGR['blood_pressure'] = $_POST['blood_pressure'];
            $argsGR['ear'] = $_POST['ear'];
            $argsGR['eye'] = $_POST['eye'];
            $argsGR['nose'] = $_POST['nose'];
            $argsGR['description'] = $_POST['description'];
            $argsGR['is_parent'] = 0;


            // Lấy thông tin chiều cao cân nặng của trẻ
            $growth = $childDao->getChildGrowth($child_parent_id);
            $growth_recorded = array();
            foreach ($growth as $row) {
                $growth_recorded[] = $row['recorded_at'];
            }
            // $date_now = date('d/m/Y');
            if(!isset($_POST['recorded_at']) || is_null($_POST['recorded_at'])) {
                throw new Exception(__("Data record date can not be empty"));
            }
            $recorded_at = $_POST['recorded_at'];
            $argsGR['recorded_at'] = $recorded_at;
            if(in_array($recorded_at, $growth_recorded)) {
                $growth_now = $childDao->getChildGrowthByDate($argsGR['recorded_at'], $child_parent_id);
                if(!isset($argsGR['source_file'])) {
                    $argsGR['source_file'] = $growth_now['source_file_path'];
                    $argsGR['file_name'] = $growth_now['file_name'];
                }
                $childDao->updateChildGrowthDateNow($argsGR);
                $updateIdGR = $growth_now['child_growth_id'];
            } else {
                $insertIdGR = $childDao->insertChildGrowth($argsGR);
            }

            // Lấy thông tin trẻ
            $child = getChildData($_POST['child_id'], CHILD_INFO);

            // Lấy danh sách phụ huynh của trẻ
            $parents = getChildData($_POST['child_id'], CHILD_PARENTS);
            $parentIds = array();
            if(count($parents) > 0) {
                foreach ($parents as $parent) {
                    $parentIds[] = $parent['user_id'];
                }

                // Thông báo cho phụ huynh có thông tin sức khỏe mới
                if(isset($updateIdGR) && !isset($insertIdGR)) {
                    $userDao->postNotifications($parentIds, NOTIFICATION_UPDATE_CHILD_HEALTH, NOTIFICATION_NODE_TYPE_CHILD,
                        $child['birthday'], $_POST['recorded_at'], $child_parent_id, convertText4Web($child['child_name']));
                } else if(!isset($updateIdGR) && isset($insertIdGR)) {
                    $userDao->postNotifications($parentIds, NOTIFICATION_NEW_CHILD_HEALTH, NOTIFICATION_NODE_TYPE_CHILD,
                        $child['birthday'], $_POST['recorded_at'], $child_parent_id, convertText4Web($child['child_name']));
                }
            }

            $db->commit();
//            return_json( array('success' => true, 'message' => __("Done, Child info has been created")) );
            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/school/' . $_POST['school_username'] . '/children/health/'. $_POST['child_id'] . '";'));
            break;
        case 'delete_growth':
            if(!isset($_POST['child_id']) || !is_numeric($_POST['child_id'])) {
                _error(404);
            }
            if(!isset($_POST['id']) || !is_numeric($_POST['id'])) {
                _error(404);
            }
            $module = $_POST['module'];
            $child_parent_id = $childDao->getChildParentIdFromChildId($_POST['child_id']);
            $db->begin_transaction();

            $childDao->deleteChildGrowth($child_parent_id, $_POST['id']);

            $db->commit();
            if($module) {
                return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/school/' . $_POST['school_username'] . '/healths/' . '";'));
            } else {
                return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/school/' . $_POST['school_username'] . '/children/health/'. $_POST['child_id'] . '";'));
            }
            break;
        case 'development_index_search':
            if(!isset($_POST['class_id']) || !is_numeric($_POST['class_id'])) {
                _error(404);
            }
            $db->begin_transaction();

            // Lấy danh sách trẻ trong lớp
            $children = getClassData($_POST['class_id'], CLASS_CHILDREN);
            $classChildGrowth = array();
            $monthGrowth = array();
            // Lặp danh sách trẻ lấy danh sách sức khỏe của từng trẻ
            foreach ($children as $child) {
                // Lấy child_parent_id từ child_id
                $child_parent_id = $childDao->getChildParentIdFromChildId($child['child_id']);
                $growths = array();
                $growths = $childDao->getChildGrowthBySchoolOrClass($child_parent_id);
                // Thêm vào mảng $classChildGrowth
                $classChildGrowth[$child['child_id']] = $growths;

                // Lấy ra recorded_at add vào mảng $monthGrowth
                foreach ($growths as $growth) {
                    $monthGrowth[] = $growth['recorded_at'];
                }
            }
            $monthGrowth = array_unique($monthGrowth);
            $monthGrowthDefine = array();
            foreach ($monthGrowth as $month) {
                $month = substr( $month,  3, 7);
                $monthGrowthDefine[] = $month;
            }
            $totalMonthSummary = array();
            // Lặp danh sách các tháng
            foreach ($monthGrowthDefine as $month) {
                $monthSummary = array();
                $monthUp = 0;
                $monthDown = 0;
                $monthEqual = 0;
                $monthNo = 0;
                // Lặp danh sách trẻ, kiểm tra xem trong tháng đó trẻ có tăng cân không
                foreach ($children as $child) {
                    // Lấy lần kiểm tra sức khỏe cuối cùng trong tháng của trẻ (được tạo bởi school hoặc class)
                    $child_parent_id = $childDao->getChildParentIdFromChildId($child['child_id']);
                    $growthLastestMonth = $childDao->getChildGrowthLastestMonthBySchoolOrClass($child_parent_id, $month);

                    if(is_null($growthLastestMonth)) {
                        $monthNo = $monthNo + 1;
                    } else {
                        // Lấy lần nhập thông tin sức khỏe trước lần cuối cùng trong tháng
                        $growthNearLastestMonth = $childDao->getChildGrowthNearLastestMonthBySchoolOrClass($child_parent_id, $growthLastestMonth['recorded_at']);
                        if(is_null($growthNearLastestMonth)) {
                            $monthNo = $monthNo + 1;
                        } else {
                            if(($growthLastestMonth['weight'] - $growthNearLastestMonth['weight']) > 0.1) {
                                $monthUp = $monthUp + 1;
                            } else if (($growthNearLastestMonth['weight'] - $growthLastestMonth['weight']) > 0.1) {
                                $monthDown = $monthDown + 1;
                            } else {
                                $monthEqual = $monthEqual + 1;
                            }
                        }
                    }
                }
                $totalMonthSummary[$month]['month_up'] = round(($monthUp / count($children) * 100), 2);
                $totalMonthSummary[$month]['month_down'] = round(($monthDown / count($children) * 100), 2);
                $totalMonthSummary[$month]['month_equal'] = round(($monthEqual / count($children) * 100), 2);
                $totalMonthSummary[$month]['month_no'] = round(($monthNo / count($children) * 100), 2);
            }
            // Lấy tất cả thông tin sức khỏe của trẻ trong lớp, chỉ lấy những thông tin sức khỏe do trường hoặc lớp tạo
            $return = array();
            $smarty->assign("totalMonthSummary", $totalMonthSummary);
            $smarty->assign("class_id", $_POST['class_id']);
            $smarty->assign("username", $school['page_name']);
            $return['results'] = $smarty->fetch("ci/school/ajax.school.development.tpl");
            return_json($return);
            break;

        case 'search_chart':
            $db->begin_transaction();
            if($_POST['begin'] == '' || !validateDate($_POST['begin']) ) {
                $begin = null;
            } else {
                $begin = $_POST['begin'];
            }
            if($_POST['end'] == '' || !validateDate($_POST['end'])) {
                $end = null;
            } else {
                $end = $_POST['end'];
            }

            // Lấy ngày hiện tại
            $dateNow = date('Y-m-d');

            $child_parent_id = $childDao->getChildParentIdFromChildId($_POST['child_id']);
            // Lấy thông tin trẻ
            // $child = $childDao->getChildByParent($child_parent_id);
            $child = getChildData($_POST['child_id'], CHILD_INFO);

            // Lấy ngày sinh nhật của trẻ
            $birthday = toDBDate($child['birthday']);

            // Khởi tạo mảng lưu chỉ số sức khỏe của trẻ
            $childGrowth = array();

            if(is_null($begin) || is_null($end)) {
                // Lấy toàn bộ thông tin sức khỏe của trẻ
                $childGrowth = $childDao->getChildGrowthForChart($child['child_parent_id']);

                // Tính xem trẻ được bao nhiêu ngày tuổi
                $day_age_now = (strtotime($dateNow) - strtotime($birthday)) / (60*60*24);

                // đoạn này xử lý để lấy ngày bắt đầu (đặt tạm a, b, c; bí tên quá)
                $a = $day_age_now % 30;
                $b = $day_age_now - $a;
                $c = $b - 30*10;
                $dayGet = (int)$c;
                $day_age_max = $day_age_now + 65;
                if($day_age_now >= 300) {
                    $day_age_min = $dayGet;
                } else {
                    $day_age_min = 0;
                }

                // Lấy dữ liệu người dùng nhập vào vẽ biểu đồ
                $heightForChartChartJS = getDataJsonForChartChartJS($childGrowth, $day_age_min, $day_age_max, $birthday, 'height');
                $weightForChartChartJS = getDataJsonForChartChartJS($childGrowth, $day_age_min, $day_age_max, $birthday, 'weight');
                if($child['gender'] == 'male') {
                    // Dữ liệu chuẩn
                    $hightStandardArray = getHightDataForChart($hightStandardJsonMale);
                    $weightStandardArray = getHightDataForChart($weightStandardJsonMale);
                } else {
                    $hightStandardArray = getHightDataForChart($hightStandardJsonFeMale);
                    $weightStandardArray = getHightDataForChart($weightStandardJsonFeMale);

                }
                // 1. Chiều cao
                // Lấy 2 ngày gần nhất của $begin_day có dữ liệu, nội suy chuẩn của ngày bắt đầu
                $hightStandardArray[$day_age_min]['highest'] = getStandardDataBeginAndEnd($hightStandardArray, $day_age_min, 'highest');

                $hightStandardArray[$day_age_min]['lowest'] = getStandardDataBeginAndEnd($hightStandardArray, $day_age_min, 'lowest');

                // Lấy 2 ngày gần nhất của $end_day có dữ liệu, nội suy chuẩn của ngày kết thúc
                $hightStandardArray[$day_age_max]['highest'] = getStandardDataBeginAndEnd($hightStandardArray, $day_age_max, 'highest');

                $hightStandardArray[$day_age_max]['lowest'] = getStandardDataBeginAndEnd($hightStandardArray, $day_age_max, 'lowest');

                // dỮ LIỆU đường cao nhất (thư viện charts.js)
                $highestForChartChartsJS = getStandardDataForChartChartJS($hightStandardArray, $day_age_min, $day_age_max, 'highest');

                // Dữ liệu đường thấp nhất (thư viện charts.js)
                $lowestForChartChartsJS = getStandardDataForChartChartJS($hightStandardArray, $day_age_min, $day_age_max, 'lowest');

                // 2. Cân nặng
                // Lấy 2 ngày gần nhất của $begin_day có dữ liệu, nội suy chuẩn của ngày bắt đầu
                $weightStandardArray[$day_age_min]['highest'] = getStandardDataBeginAndEnd($weightStandardArray, $day_age_min, 'highest');

                $weightStandardArray[$day_age_min]['lowest'] = getStandardDataBeginAndEnd($weightStandardArray, $day_age_min, 'lowest');

                // Lấy 2 ngày gần nhất của $end_day có dữ liệu, nội suy chuẩn của ngày kết thúc
                $weightStandardArray[$day_age_max]['highest'] = getStandardDataBeginAndEnd($weightStandardArray, $day_age_max, 'highest');

                $weightStandardArray[$day_age_max]['lowest'] = getStandardDataBeginAndEnd($weightStandardArray, $day_age_max, 'lowest');

                // Dữ liệu đường cao nhất (charts.js)
                $highestWeightForChartChartJS = getStandardDataForChartChartJS($weightStandardArray, $day_age_min, $day_age_max, 'highest');

                // Dữ liệu đường thấp nhất (charts.js)
                $lowestWeightForChartChartJS = getStandardDataForChartChartJS($weightStandardArray, $day_age_min, $day_age_max, 'lowest');

                // 3. BMI (charts.js)
                $bmiForChartChartJS = getDataJsonForChartChartJS($childGrowth, $day_age_min, $day_age_max, $birthday, 'bmi');

                // GEt label
                $label = '';
                for($i = $day_age_min; $i <= $day_age_max; $i++) {
                    if($i%30 == 0) {
                        $month = $i/30;
                        $label .= ' ' .$month. ',';
                    } else {
                        $label .= ' ,';
                    }
                }
                $smarty->assign('label', $label);
            } else {
                // Lấy dữ liệu nhập và của trẻ
                $childGrowth = $childDao->getChildGrowthSearchForChartBeginEnd($child['child_parent_id'], $begin, $end);

                // Lấy ngày bắt đầu và ngày kết thúc
                $begin = toDBDate($begin);
                $end = toDBDate($end);

                $end_day = (strtotime($end) - strtotime($birthday)) / (60*60*24);
                $end_day = $end_day / 30;
                $end_day = (int)$end_day;
                $end_day = ($end_day + 1) * 30;

                $begin_day = (strtotime($begin) - strtotime($birthday)) / (60*60*24);
                $begin_day = $begin_day / 30;
                $begin_day = (int)$begin_day;
                $begin_day = $begin_day * 30;
                if($begin_day < 0) {
                    throw new Exception(__("You must select day begin after birthday"));
                }

                // Dữ liệu chuẩn người dùng nhập vào vẽ biểu đồ chiều cao (thư viện charts.js)
                $heightForChartChartJS = getDataJsonForChartChartJS($childGrowth, $begin_day, $end_day, $birthday, 'height');

                // Lấy dữ liệu người dùng nhập vào vẽ biểu đồ cân nặng (charts.js)
                $weightForChartChartJS = getDataJsonForChartChartJS($childGrowth, $begin_day, $end_day, $birthday, 'weight');

                if($child['gender'] == 'male') {
                    // Dữ liệu chuẩn
                    $hightStandardArray = getHightDataForChart($hightStandardJsonMale);
                    $weightStandardArray = getHightDataForChart($weightStandardJsonMale);
                } else {
                    $hightStandardArray = getHightDataForChart($hightStandardJsonFeMale);
                    $weightStandardArray = getHightDataForChart($weightStandardJsonFeMale);
                }

                // 1. Chiều cao
                // Lấy 2 ngày gần nhất của $begin_day có dữ liệu, nội suy chuẩn của ngày bắt đầu
                $hightStandardArray[$begin_day]['highest'] = getStandardDataBeginAndEnd($hightStandardArray, $begin_day, 'highest');

                $hightStandardArray[$begin_day]['lowest'] = getStandardDataBeginAndEnd($hightStandardArray, $begin_day, 'lowest');

                // Lấy 2 ngày gần nhất của $end_day có dữ liệu, nội suy chuẩn của ngày kết thúc
                $hightStandardArray[$end_day]['highest'] = getStandardDataBeginAndEnd($hightStandardArray, $end_day, 'highest');

                $hightStandardArray[$end_day]['lowest'] = getStandardDataBeginAndEnd($hightStandardArray, $end_day, 'lowest');

                // dỮ LIỆU đường cao nhất (thư viện charts.js)
                $highestForChartChartsJS = getStandardDataForChartChartJS($hightStandardArray, $begin_day, $end_day, 'highest');

                // Dữ liệu đường thấp nhất (thư viện charts.js)
                $lowestForChartChartsJS = getStandardDataForChartChartJS($hightStandardArray, $begin_day, $end_day, 'lowest');

                // 2. Cân nặng
                // Lấy 2 ngày gần nhất của $begin_day có dữ liệu, nội suy chuẩn của ngày bắt đầu
                $weightStandardArray[$begin_day]['highest'] = getStandardDataBeginAndEnd($weightStandardArray, $begin_day, 'highest');

                $weightStandardArray[$begin_day]['lowest'] = getStandardDataBeginAndEnd($weightStandardArray, $begin_day, 'lowest');

                // Lấy 2 ngày gần nhất của $end_day có dữ liệu, nội suy chuẩn của ngày kết thúc
                $weightStandardArray[$end_day]['highest'] = getStandardDataBeginAndEnd($weightStandardArray, $end_day, 'highest');

                $weightStandardArray[$end_day]['lowest'] = getStandardDataBeginAndEnd($weightStandardArray, $end_day, 'lowest');

                // Dữ liệu đường cao nhất (charts.js)
                $highestWeightForChartChartJS = getStandardDataForChartChartJS($weightStandardArray, $begin_day, $end_day, 'highest');

                // Dữ liệu đường thấp nhất (charts.js)
                $lowestWeightForChartChartJS = getStandardDataForChartChartJS($weightStandardArray, $begin_day, $end_day, 'lowest');

                // 3. BMI (charts.js)
                $bmiForChartChartJS = getDataJsonForChartChartJS($childGrowth, $begin_day, $end_day, $birthday, 'bmi');

                // GEt label
                $label = '';
                for($i = $begin_day; $i <= $end_day; $i++) {
                    if($i%30 == 0) {
                        $month = $i/30;
                        $label .= ' ' .$month. ',';
                    } else {
                        $label .= ' ,';
                    }
                }
                $smarty->assign('label', $label);
            }

            // Chiều cao for charts.js
            $smarty->assign('highestForChartChartJS', $highestForChartChartsJS);
            $smarty->assign('lowestForChartChartJS', $lowestForChartChartsJS);
            $smarty->assign('heightForChartChartJS', $heightForChartChartJS);

            //Cân nặng cho charts.js
            $smarty->assign('highestWeightForChartChartJS', $highestWeightForChartChartJS);
            $smarty->assign('lowestWeightForChartChartJS', $lowestWeightForChartChartJS);
            $smarty->assign('weightForChartChartJS', $weightForChartChartJS);

            // BMI (charts.js)
            $smarty->assign('bmiForChartChartJS', $bmiForChartChartJS);

            $smarty->assign('username', $school['page_name']);
            $smarty->assign('child', $child);
            $smarty->assign('growth', array_reverse($childGrowth));

            $return['results'] = $smarty->fetch("ci/school/ajax.school.chartsearch.tpl");
            //$return['lowestForChart'] = $smarty->fetch("ci/childinfo/ajax.child.chartsearch.tpl");

            return_json($return);
            break;
        case 'child_edit_search':

            //Memcache - Lấy ra các lớp của 1 trường
            $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
            $smarty->assign('classes', $classes);

            $dateNow = date('Y-m-d');
            $day_search = $_POST['day_search'];
            if($day_search == 'month') {
                $dateBefore = '01/01/2010';
            } else {
                $dateBefore1Week = date('Y-m-d', strtotime("-1 weeks", strtotime($dateNow)));
                $dateBefore = toSysDate($dateBefore1Week);
            }
            $return = array();
            $results = $childDao->getChildEditHistory($school['page_id'], $dateBefore);
            $smarty->assign('results', $results);
            $return['results'] = $smarty->fetch("ci/school/ajax.childeditlist.tpl");
            return_json($return);
            break;
        case 'child_new_search':
            //Memcache - Lấy ra các lớp của 1 trường
            $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
            $smarty->assign('classes', $classes);

            $dateNow = date('Y-m-d');
            $today = toSysDate($dateNow);
            $day_search = $_POST['day_search'];
            if($day_search == 'month') {
                $dateBefore = date('Y-m-d', strtotime('- 1 month', strtotime($dateNow)));
                $dateBefore = toSysDate($dateBefore);
            } else {
                $dateBefore1Week = date('Y-m-d', strtotime("-1 weeks", strtotime($dateNow)));
                $dateBefore = toSysDate($dateBefore1Week);
            }
            $return = array();
            $results = $childDao->getChildAddNewInTheMonth($school['page_id'], $dateBefore, $today);
            $smarty->assign('results', $results);
            $return['results'] = $smarty->fetch("ci/school/ajax.childnewlist.tpl");
            return_json($return);
            break;
        case 'add_photo':
            // Thêm album ảnh nhật ký của trẻ
            if (!isset($_POST['child_id']) || !is_numeric($_POST['child_id'])) {
                _error(404);
            }
            $reload = (isset($_POST['reload']) && $_POST['reload'] == 1)?1:0;
            $db->begin_transaction();
            $child_parent_id = $childDao->getChildParentIdFromChildId($_POST['child_id']);
            $caption = isset($_POST['caption']) ? $_POST['caption'] : 'null';
            // get allowed file size
            $max_allowed_size = $system['max_photo_size'] * 1024;

            /* check & create uploads dir */
            $depth = '../../../../../';
            $folder = 'children';
            if(!file_exists($depth.$system['system_uploads_directory'].'/'.$folder)) {
                @mkdir($depth.$system['system_uploads_directory'].'/'.$folder, 0777, true);
            }
            if(!file_exists($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y'))) {
                @mkdir($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y'), 0777, true);
            }
            if(!file_exists($system['system_uploads_directory'].'/'.$folder.'/' . date('Y') . '/' . date('m'))) {
                @mkdir($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y') . '/' . date('m'), 0777, true);
            }

            $files = array();
            foreach ($_FILES['file'] as $key => $val) {
                for ($i = 0; $i < count($val); $i++) {
                    $files[$i][$key] = $val[$i];
                }
            }

            $return_files = array();
            $files_num = count($files);

            foreach ($files as $file) {

                // valid inputs
                if (!isset($file) || $file["error"] != UPLOAD_ERR_OK) {
                    if ($files_num > 1) {
                        continue;
                    } else {
//                        modal(MESSAGE, __("Upload Error"), __("Something wrong with upload! Is 'upload_max_filesize' set correctly?"));
                        return_json( array('error' => true, 'message' => __("You must add at least one image")) );
                    }
                }

                // check file extesnion
                $extension = get_extension($file['name']);
                if(!valid_extension($extension, $system['file_medicine_extensions'])) {
                    // modal(MESSAGE, __("Upload Error"), __("The file type is not valid or not supported"));
                    return_json( array('error' => true, 'message' => __("The file type is not valid or not supported")) );
                }

                // check file size
                if ($file["size"] > $max_allowed_size) {
                    if ($files_num > 1) {
                        continue;
                    } else {
                        //modal(MESSAGE, __("Upload Error"), __("The file size is so big"));
                        return_json( array('error' => true, 'message' => __("The file size is so big")) );
                    }
                }

                /* prepare new file name */
                $directory = $folder . '/' . date('Y') . '/' . date('m') . '/';
                $prefix = $system['uploads_prefix'] . '_' . md5(time() * rand(1, 9999));


                $image = new Image($file["tmp_name"]);

                $image_tmp_name = $directory . $prefix . '_tmp' . $image->_img_ext;
                $image_new_name = $directory . $prefix . $image->_img_ext;
                $path_tmp = $depth . $system['system_uploads_directory'] . '/' . $image_tmp_name;
                $path_new = $depth . $system['system_uploads_directory'] . '/' . $image_new_name;

                /* check if the file uploaded successfully */
                if (!@move_uploaded_file($file['tmp_name'], $path_tmp)) {
                    if ($files_num > 1) {
                        continue;
                    } else {
                        //modal(MESSAGE, __("Upload Error"), __("Sorry, can not upload the file"));
                        return_json( array('error' => true, 'message' => __("Sorry, can not upload the file")) );
                    }
                }

                /* save the new image */
                $image->save($path_new, $path_tmp);

                /* delete the tmp image */
                unlink($path_tmp);

                /* return */
                $return_files[] = $image_new_name;
            }
            //Thêm album nhật ký
            $isParent = 0;
            $journalAlbumId = $journalDao->insertJournalAlbum($child_parent_id, $caption, $isParent);
            // Thêm ảnh vào album
            $journalDao->insertJournal($journalAlbumId, $return_files);

            // Thông báo đến phụ huynh
            // Lấy danh sách phụ huynh của trẻ
            $parents = getChildData($_POST['child_id'], CHILD_PARENTS);
            $child = getChildData($_POST['child_id'], CHILD_INFO);
            $parentIds = array();
            if(count($parents) > 0) {
                foreach ($parents as $parent) {
                    $parentIds[] = $parent['user_id'];
                }

                // Thông báo cho phụ huynh có nhật ký mới
                $userDao->postNotifications($parentIds, NOTIFICATION_ADD_DIARY_SCHOOL, NOTIFICATION_NODE_TYPE_CHILD,
                    $journalAlbumId, convertText4Web($caption), $child_parent_id, convertText4Web($child['child_name']));
            }

            $db->commit();
            if($reload) {
                $module = $_POST['module'];
                if($module) {
                    return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/school/' . $_POST['school_username'] . '/diarys/add' . '";'));
                } else {
                    return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/school/' . $_POST['school_username'] . '/children/adddiary' . '";'));
                }
            } else {
                return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/school/' . $_POST['school_username'] . '/children/journal/' . $_POST['child_id'] . '";'));
            }
            break;
        case 'delete_photo':
            if (!isset($_POST['child_journal_id']) || !is_numeric($_POST['child_journal_id'])) {
                _error(404);
            }
            if (!isset($_POST['child_id']) || !is_numeric($_POST['child_id'])) {
                _error(404);
            }
            $db->begin_transaction();
            $journal_id = $journalDao->getJournalAlbumIdFromJournalId($_POST['child_journal_id']);
            $journal = $journalDao->getJournalById($journal_id);
            $journalDao->deleteJournal($_POST['child_journal_id']);

            // Thông báo đến phụ huynh
            // Lấy danh sách phụ huynh của trẻ
            $parents = getChildData($_POST['child_id'], CHILD_PARENTS);
            $child = getChildData($_POST['child_id'], CHILD_INFO);
            $parentIds = array();
            if(count($parents) > 0) {
                foreach ($parents as $parent) {
                    $parentIds[] = $parent['user_id'];
                }

                // Thông báo cho phụ huynh xóa ảnh trong album nhật ký
                $userDao->postNotifications($parentIds, NOTIFICATION_DELETE_PHOTO_DIARY_SCHOOL, NOTIFICATION_NODE_TYPE_CHILD,
                    $journal_id, convertText4Web($journal['caption']), $child['child_parent_id'], '');
            }

            $db->commit();
            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/school/' . $_POST['school_username'] . '/children/journal/' . $_POST['child_id'] . '";'));
            break;
        case 'delete_album':
            if (!isset($_POST['child_journal_album_id']) || !is_numeric($_POST['child_journal_album_id'])) {
                _api_error(404);
            }
            if (!isset($_POST['child_id']) || !is_numeric($_POST['child_id'])) {
                _api_error(404);
            }
            $db->begin_transaction();
            $journalDao->deleteJournalAlbum($_POST['child_journal_album_id']);
            $db->commit();
            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/school/' . $_POST['school_username'] . '/children/journal/' . $_POST['child_id'] . '";'));
            break;
        case 'edit_caption':
            if (!isset($_POST['child_journal_album_id']) || !is_numeric($_POST['child_journal_album_id'])) {
                _error(404);
            }
            if (!isset($_POST['child_id']) || !is_numeric($_POST['child_id'])) {
                _error(404);
            }
            $db->begin_transaction();
            $journal = $journalDao->getJournalById($_POST['child_journal_album_id']);
            $oldCaption = $journal['caption'];
            $journalDao->editCaptionAlbum($_POST['child_journal_album_id'], $_POST['caption']);

            // Thông báo đến phụ huynh
            // Lấy danh sách phụ huynh của trẻ
            $parents = getChildData($_POST['child_id'], CHILD_PARENTS);
            $child = getChildData($_POST['child_id'], CHILD_INFO);
            $parentIds = array();
            if(count($parents) > 0) {
                foreach ($parents as $parent) {
                    $parentIds[] = $parent['user_id'];
                }

                // Thông báo cho phụ huynh thêm ảnh trong album nhật ký
                $userDao->postNotifications($parentIds, NOTIFICATION_EDIT_CAPTION_DIARY_SCHOOL, NOTIFICATION_NODE_TYPE_CHILD,
                    $_POST['child_journal_album_id'], convertText4Web($oldCaption), $child['child_parent_id'], '');
            }

            $db->commit();
            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/school/' . $_POST['school_username'] . '/children/journal/' . $_POST['child_id'] . '";'));
            break;
        case 'add_photo_journal':
            // valid inputs
            if(!isset($_POST['child_journal_album_id']) || !is_numeric($_POST['child_journal_album_id'])) {
                _error(400);
            }
            $db->begin_transaction();

            // get allowed file size
            $max_allowed_size = $system['max_photo_size'] * 1024;

            /* check & create uploads dir */
            $depth = '../../../../../';
            $folder = 'children';
            if(!file_exists($depth.$system['system_uploads_directory'].'/'.$folder)) {
                @mkdir($depth.$system['system_uploads_directory'].'/'.$folder, 0777, true);
            }
            if(!file_exists($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y'))) {
                @mkdir($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y'), 0777, true);
            }
            if(!file_exists($system['system_uploads_directory'].'/'.$folder.'/' . date('Y') . '/' . date('m'))) {
                @mkdir($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y') . '/' . date('m'), 0777, true);
            }

            $files = array();
            foreach ($_FILES['file'] as $key => $val) {
                for ($i = 0; $i < count($val); $i++) {
                    $files[$i][$key] = $val[$i];
                }
            }

            $return_files = array();
            $files_num = count($files);

            foreach ($files as $file) {

                // valid inputs
                if (!isset($file) || $file["error"] != UPLOAD_ERR_OK) {
                    if ($files_num > 1) {
                        continue;
                    } else {
//                        modal(MESSAGE, __("Upload Error"), __("Something wrong with upload! Is 'upload_max_filesize' set correctly?"));
                        return_json( array('error' => true, 'message' => __("You must add at least one image")) );
                    }
                }

                // check file extesnion
                $extension = get_extension($file['name']);
                if(!valid_extension($extension, $system['file_medicine_extensions'])) {
                    // modal(MESSAGE, __("Upload Error"), __("The file type is not valid or not supported"));
                    return_json( array('error' => true, 'message' => __("The file type is not valid or not supported")) );
                }

                // check file size
                if ($file["size"] > $max_allowed_size) {
                    if ($files_num > 1) {
                        continue;
                    } else {
                        //modal(MESSAGE, __("Upload Error"), __("The file size is so big"));
                        return_json( array('error' => true, 'message' => __("The file size is so big")) );
                    }
                }

                /* prepare new file name */
                $directory = $folder . '/' . date('Y') . '/' . date('m') . '/';
                $prefix = $system['uploads_prefix'] . '_' . md5(time() * rand(1, 9999));


                $image = new Image($file["tmp_name"]);

                $image_tmp_name = $directory . $prefix . '_tmp' . $image->_img_ext;
                $image_new_name = $directory . $prefix . $image->_img_ext;
                $path_tmp = $depth . $system['system_uploads_directory'] . '/' . $image_tmp_name;
                $path_new = $depth . $system['system_uploads_directory'] . '/' . $image_new_name;

                /* check if the file uploaded successfully */
                if (!@move_uploaded_file($file['tmp_name'], $path_tmp)) {
                    if ($files_num > 1) {
                        continue;
                    } else {
                        //modal(MESSAGE, __("Upload Error"), __("Sorry, can not upload the file"));
                        return_json( array('error' => true, 'message' => __("Sorry, can not upload the file")) );
                    }
                }

                /* save the new image */
                $image->save($path_new, $path_tmp);

                /* delete the tmp image */
                unlink($path_tmp);

                /* return */
                $return_files[] = $image_new_name;
            }

            // Thêm ảnh vào album
            $journalDao->addPhotoForAlbum($_POST['child_journal_album_id'], $return_files);
//            $post_id = $user->add_album_photos($inputs);

            $journal = $journalDao->getJournalById($_POST['child_journal_album_id']);
            // Thông báo đến phụ huynh
            // Lấy danh sách phụ huynh của trẻ
            $parents = getChildData($_POST['child_id'], CHILD_PARENTS);
            $child = getChildData($_POST['child_id'], CHILD_INFO);
            $parentIds = array();
            if(count($parents) > 0) {
                foreach ($parents as $parent) {
                    $parentIds[] = $parent['user_id'];
                }

                // Thông báo cho phụ huynh thêm ảnh trong album nhật ký
                $userDao->postNotifications($parentIds, NOTIFICATION_ADD_PHOTO_DIARY_SCHOOL, NOTIFICATION_NODE_TYPE_CHILD,
                    $_POST['child_journal_album_id'], convertText4Web($journal['caption']), $child['child_parent_id'], '');
            }
            $db->commit();
            // Nếu là module nhật ký thì reload về màn hình danh sách nhật ký chung, nếu trong chi tiết trẻ thì reload về màn hình danh sách nhật ký của trẻ
            if($_POST['diary_module']) {
                return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/school/' . $_POST['school_username'] . '/diarys' . '";'));
            } else {
                return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/school/' . $_POST['school_username'] . '/children/journal/' . $_POST['child_id'] . '";'));
            }
            break;
        case 'search_diary':
            // Hàm search ở màn hình riêng từng trẻ
            if(!isset($_POST['child_id']) || !is_numeric(($_POST['child_id']))) {
                _error(404);
            }
            $child_parent_id = $childDao->getChildParentIdFromChildId($_POST['child_id']);
            $results = $journalDao->getAllJournalChildForSchoolOfYear($child_parent_id, $_POST['year']);
            $child = getChildData($_POST['child_id'], CHILD_INFO);
            $smarty->assign('results', $results);
            $smarty->assign('child_parent_id', $child_parent_id);
            $smarty->assign('child', $child);
            $smarty->assign('username', $_POST['school_username']);
            $return['results'] = $smarty->fetch("ci/ajax.school.journal.list.tpl");
            $return['no_data'] = (count($results) == 0);
            return_json($return);
            break;
        case 'search_diary_class':
            // Hàm search ở màn hình góc nhật ký, không phải màn hình riêng từng trẻ
            if(!isset($_POST['class_id']) || !is_numeric($_POST['class_id'])) {
                throw new Exception(__("Please select a class"));
            }

            if(!isset($_POST['child_id']) || !is_numeric($_POST['child_id'])) {
                throw new Exception(__("You have not picked a student yet"));
            }
            $isNewSearch = isset($_POST['is_new'])? $_POST['is_new']: 0;
            $condition = array();

            if($isNewSearch) { // Nếu là lần search mới
                // Đưa vào session để lần sau search lại
                $condition['class_id'] = $_POST['class_id'];
                $condition['child_id'] = $_POST['child_id'];
                $condition['year'] = $_POST['year'];
                $condition['school_id'] = $school['page_id'];
                $_SESSION[SESSION_KEY_SEARCH_DIARY] = $condition;
            }
            $child_parent_id = $childDao->getChildParentIdFromChildId($_POST['child_id']);
            $child = getChildData($_POST['child_id'], CHILD_INFO);
            $results = array();

            $results = $journalDao->getAllJournalChildForSchoolOfYear($child_parent_id, $_POST['year']);
            $smarty->assign('results', $results);
            $smarty->assign('child_parent_id', $child_parent_id);
            $smarty->assign('child', $child);
            $smarty->assign('username', $_POST['school_username']);
            $smarty->assign('is_module', 1);
            $return['results'] = $smarty->fetch("ci/ajax.school.journal.list.tpl");
            $return['no_data'] = (count($results) == 0);
            return_json($return);
            break;
        case 'search_health_class':
            // Hàm search ở màn hình chỉ số sức khởe, không phải màn hình riêng từng trẻ
            if(!isset($_POST['class_id']) || !is_numeric($_POST['class_id'])) {
                throw new Exception(__("Please select a class"));
            }

            if(!isset($_POST['child_id']) || !is_numeric($_POST['child_id'])) {
                throw new Exception(__("You have not picked a student yet"));
            }
            $isNewSearch = isset($_POST['is_new'])? $_POST['is_new']: 0;
            $condition = array();

            if($isNewSearch) { // Nếu là lần search mới
                // Đưa vào session để lần sau search lại
                $condition['class_id'] = $_POST['class_id'];
                $condition['child_id'] = $_POST['child_id'];
                $condition['begin'] = $_POST['begin'];
                $condition['end'] = $_POST['end'];
                $condition['school_id'] = $school['page_id'];
                $_SESSION[SESSION_KEY_SEARCH_CHILDREN_HEALTH] = $condition;
            }
            $child_parent_id = $childDao->getChildParentIdFromChildId($_POST['child_id']);
            $child = getChildData($_POST['child_id'], CHILD_INFO);

            if($_POST['begin'] == '' || !validateDate($_POST['begin']) ) {
                $begin = null;
            } else {
                $begin = $_POST['begin'];
            }
            if($_POST['end'] == '' || !validateDate($_POST['end'])) {
                $end = null;
            } else {
                $end = $_POST['end'];
            }

            // Lấy ngày hiện tại
            $dateNow = date('Y-m-d');

            $child_parent_id = $childDao->getChildParentIdFromChildId($_POST['child_id']);
            // Lấy thông tin trẻ
            // $child = $childDao->getChildByParent($child_parent_id);
            $child = getChildData($_POST['child_id'], CHILD_INFO);

            // Lấy ngày sinh nhật của trẻ
            $birthday = toDBDate($child['birthday']);

            // Khởi tạo mảng lưu chỉ số sức khỏe của trẻ
            $childGrowth = array();

            if(is_null($begin) || is_null($end)) {
                // Lấy toàn bộ thông tin sức khỏe của trẻ
                $childGrowth = $childDao->getChildGrowthForChart($child['child_parent_id']);

                // Tính xem trẻ được bao nhiêu ngày tuổi
                $day_age_now = (strtotime($dateNow) - strtotime($birthday)) / (60*60*24);

                // đoạn này xử lý để lấy ngày bắt đầu (đặt tạm a, b, c; bí tên quá)
                $a = $day_age_now % 30;
                $b = $day_age_now - $a;
                $c = $b - 30*10;
                $dayGet = (int)$c;
                $day_age_max = $day_age_now + 65;
                if($day_age_now >= 300) {
                    $day_age_min = $dayGet;
                } else {
                    $day_age_min = 0;
                }

                // Lấy dữ liệu người dùng nhập vào vẽ biểu đồ
                $heightForChartChartJS = getDataJsonForChartChartJS($childGrowth, $day_age_min, $day_age_max, $birthday, 'height');
                $weightForChartChartJS = getDataJsonForChartChartJS($childGrowth, $day_age_min, $day_age_max, $birthday, 'weight');
                if($child['gender'] == 'male') {
                    // Dữ liệu chuẩn
                    $hightStandardArray = getHightDataForChart($hightStandardJsonMale);
                    $weightStandardArray = getHightDataForChart($weightStandardJsonMale);
                } else {
                    $hightStandardArray = getHightDataForChart($hightStandardJsonFeMale);
                    $weightStandardArray = getHightDataForChart($weightStandardJsonFeMale);

                }
                // 1. Chiều cao
                // Lấy 2 ngày gần nhất của $begin_day có dữ liệu, nội suy chuẩn của ngày bắt đầu
                $hightStandardArray[$day_age_min]['highest'] = getStandardDataBeginAndEnd($hightStandardArray, $day_age_min, 'highest');

                $hightStandardArray[$day_age_min]['lowest'] = getStandardDataBeginAndEnd($hightStandardArray, $day_age_min, 'lowest');

                // Lấy 2 ngày gần nhất của $end_day có dữ liệu, nội suy chuẩn của ngày kết thúc
                $hightStandardArray[$day_age_max]['highest'] = getStandardDataBeginAndEnd($hightStandardArray, $day_age_max, 'highest');

                $hightStandardArray[$day_age_max]['lowest'] = getStandardDataBeginAndEnd($hightStandardArray, $day_age_max, 'lowest');

                // dỮ LIỆU đường cao nhất (thư viện charts.js)
                $highestForChartChartsJS = getStandardDataForChartChartJS($hightStandardArray, $day_age_min, $day_age_max, 'highest');

                // Dữ liệu đường thấp nhất (thư viện charts.js)
                $lowestForChartChartsJS = getStandardDataForChartChartJS($hightStandardArray, $day_age_min, $day_age_max, 'lowest');

                // 2. Cân nặng
                // Lấy 2 ngày gần nhất của $begin_day có dữ liệu, nội suy chuẩn của ngày bắt đầu
                $weightStandardArray[$day_age_min]['highest'] = getStandardDataBeginAndEnd($weightStandardArray, $day_age_min, 'highest');

                $weightStandardArray[$day_age_min]['lowest'] = getStandardDataBeginAndEnd($weightStandardArray, $day_age_min, 'lowest');

                // Lấy 2 ngày gần nhất của $end_day có dữ liệu, nội suy chuẩn của ngày kết thúc
                $weightStandardArray[$day_age_max]['highest'] = getStandardDataBeginAndEnd($weightStandardArray, $day_age_max, 'highest');

                $weightStandardArray[$day_age_max]['lowest'] = getStandardDataBeginAndEnd($weightStandardArray, $day_age_max, 'lowest');

                // Dữ liệu đường cao nhất (charts.js)
                $highestWeightForChartChartJS = getStandardDataForChartChartJS($weightStandardArray, $day_age_min, $day_age_max, 'highest');

                // Dữ liệu đường thấp nhất (charts.js)
                $lowestWeightForChartChartJS = getStandardDataForChartChartJS($weightStandardArray, $day_age_min, $day_age_max, 'lowest');

                // 3. BMI (charts.js)
                $bmiForChartChartJS = getDataJsonForChartChartJS($childGrowth, $day_age_min, $day_age_max, $birthday, 'bmi');

                // GEt label
                $label = '';
                for($i = $day_age_min; $i <= $day_age_max; $i++) {
                    if($i%30 == 0) {
                        $month = $i/30;
                        $label .= ' ' .$month. ',';
                    } else {
                        $label .= ' ,';
                    }
                }
                $smarty->assign('label', $label);
            } else {
                // Lấy dữ liệu nhập và của trẻ
                $childGrowth = $childDao->getChildGrowthSearchForChartBeginEnd($child['child_parent_id'], $begin, $end);

                // Lấy ngày bắt đầu và ngày kết thúc
                $begin = toDBDate($begin);
                $end = toDBDate($end);

                $end_day = (strtotime($end) - strtotime($birthday)) / (60*60*24);
                $end_day = $end_day / 30;
                $end_day = (int)$end_day;
                $end_day = ($end_day + 1) * 30;

                $begin_day = (strtotime($begin) - strtotime($birthday)) / (60*60*24);
                $begin_day = $begin_day / 30;
                $begin_day = (int)$begin_day;
                $begin_day = $begin_day * 30;
                if($begin_day < 0) {
                    throw new Exception(__("You must select day begin after birthday"));
                }

                // Dữ liệu chuẩn người dùng nhập vào vẽ biểu đồ chiều cao (thư viện charts.js)
                $heightForChartChartJS = getDataJsonForChartChartJS($childGrowth, $begin_day, $end_day, $birthday, 'height');

                // Lấy dữ liệu người dùng nhập vào vẽ biểu đồ cân nặng (charts.js)
                $weightForChartChartJS = getDataJsonForChartChartJS($childGrowth, $begin_day, $end_day, $birthday, 'weight');

                if($child['gender'] == 'male') {
                    // Dữ liệu chuẩn
                    $hightStandardArray = getHightDataForChart($hightStandardJsonMale);
                    $weightStandardArray = getHightDataForChart($weightStandardJsonMale);
                } else {
                    $hightStandardArray = getHightDataForChart($hightStandardJsonFeMale);
                    $weightStandardArray = getHightDataForChart($weightStandardJsonFeMale);
                }

                // 1. Chiều cao
                // Lấy 2 ngày gần nhất của $begin_day có dữ liệu, nội suy chuẩn của ngày bắt đầu
                $hightStandardArray[$begin_day]['highest'] = getStandardDataBeginAndEnd($hightStandardArray, $begin_day, 'highest');

                $hightStandardArray[$begin_day]['lowest'] = getStandardDataBeginAndEnd($hightStandardArray, $begin_day, 'lowest');

                // Lấy 2 ngày gần nhất của $end_day có dữ liệu, nội suy chuẩn của ngày kết thúc
                $hightStandardArray[$end_day]['highest'] = getStandardDataBeginAndEnd($hightStandardArray, $end_day, 'highest');

                $hightStandardArray[$end_day]['lowest'] = getStandardDataBeginAndEnd($hightStandardArray, $end_day, 'lowest');

                // dỮ LIỆU đường cao nhất (thư viện charts.js)
                $highestForChartChartsJS = getStandardDataForChartChartJS($hightStandardArray, $begin_day, $end_day, 'highest');

                // Dữ liệu đường thấp nhất (thư viện charts.js)
                $lowestForChartChartsJS = getStandardDataForChartChartJS($hightStandardArray, $begin_day, $end_day, 'lowest');

                // 2. Cân nặng
                // Lấy 2 ngày gần nhất của $begin_day có dữ liệu, nội suy chuẩn của ngày bắt đầu
                $weightStandardArray[$begin_day]['highest'] = getStandardDataBeginAndEnd($weightStandardArray, $begin_day, 'highest');

                $weightStandardArray[$begin_day]['lowest'] = getStandardDataBeginAndEnd($weightStandardArray, $begin_day, 'lowest');

                // Lấy 2 ngày gần nhất của $end_day có dữ liệu, nội suy chuẩn của ngày kết thúc
                $weightStandardArray[$end_day]['highest'] = getStandardDataBeginAndEnd($weightStandardArray, $end_day, 'highest');

                $weightStandardArray[$end_day]['lowest'] = getStandardDataBeginAndEnd($weightStandardArray, $end_day, 'lowest');

                // Dữ liệu đường cao nhất (charts.js)
                $highestWeightForChartChartJS = getStandardDataForChartChartJS($weightStandardArray, $begin_day, $end_day, 'highest');

                // Dữ liệu đường thấp nhất (charts.js)
                $lowestWeightForChartChartJS = getStandardDataForChartChartJS($weightStandardArray, $begin_day, $end_day, 'lowest');

                // 3. BMI (charts.js)
                $bmiForChartChartJS = getDataJsonForChartChartJS($childGrowth, $begin_day, $end_day, $birthday, 'bmi');

                // GEt label
                $label = '';
                for($i = $begin_day; $i <= $end_day; $i++) {
                    if($i%30 == 0) {
                        $month = $i/30;
                        $label .= ' ' .$month. ',';
                    } else {
                        $label .= ' ,';
                    }
                }
                $smarty->assign('label', $label);
            }

            // Chiều cao for charts.js
            $smarty->assign('highestForChartChartJS', $highestForChartChartsJS);
            $smarty->assign('lowestForChartChartJS', $lowestForChartChartsJS);
            $smarty->assign('heightForChartChartJS', $heightForChartChartJS);

            //Cân nặng cho charts.js
            $smarty->assign('highestWeightForChartChartJS', $highestWeightForChartChartJS);
            $smarty->assign('lowestWeightForChartChartJS', $lowestWeightForChartChartJS);
            $smarty->assign('weightForChartChartJS', $weightForChartChartJS);

            // BMI (charts.js)
            $smarty->assign('bmiForChartChartJS', $bmiForChartChartJS);

            $smarty->assign('username', $school['page_name']);
            $smarty->assign('growth', array_reverse($childGrowth));

            $smarty->assign('is_module', 1);
            $children = getClassData($_POST['class_id'], CLASS_CHILDREN);
            $smarty->assign('username', $school['page_name']);
            $smarty->assign('child', $child);
            $smarty->assign('children', $children);
            $smarty->assign('childId', $_POST['child_id']);
            $smarty->assign('classId', $_POST['class_id']);

            $return['results'] = $smarty->fetch("ci/school/ajax.school.chartsearch.tpl");
            //$return['lowestForChart'] = $smarty->fetch("ci/childinfo/ajax.child.chartsearch.tpl");

            return_json($return);
            break;
        case 'search_move_class_child':
            if(!isset($_POST['class_id']) || !is_numeric($_POST['class_id'])) {
                _error(404);
            }
            if($_POST['class_id'] == 0) {
                // Lấy danh sách trẻ chưa phân lớp của trường
                $children = $childDao->getChildNoClass($school['page_id']);
            } else {
                $children = getClassData($_POST['class_id'], CLASS_CHILDREN);
            }

            $results = "";
            $results = $results.'<div class="col-sm-12"><input type="checkbox" id="select_all_child">' . ' ' . '<strong>' . __("Select all") . '</strong>'  . '</div>';
            foreach ($children as $child) {
                $results = $results.'<div class="col-xs-12 col-sm-6"><input type="checkbox" class = "checkbox_child" name = "child[]" value="'.$child['child_id'].'"> <strong>'.$child['child_name'].'</strong></div>';
            }
            $return['results'] = $results;
            return_json($return);
            break;
        case 'movesclass':
            // Hàm xử lý khi chuyển lớp cho nhiều trẻ
            if (!canEdit($_POST['school_username'], 'children')) {
                _error(403);
            }
            if(!isset($_POST['child']) || count($_POST['child']) == 0) {
                throw new Exception(__("You have not picked a student yet"));
            }
            // validate lớp cũ và lớp mới
            if($_POST['old_class_id'] == $_POST['new_class_id']) {
                throw new Exception(__("New class dont overlap with old class"));
            }
            /**
             * Hàm này xử lý khi chuyển lớp cho nhiều trẻ
             * 1. Cập nhật danh sách lớp (đưa nhiều trẻ vào lớp - bảng ci_class_child)
             * 2. Cập nhật thông tin trẻ trong trường (cập nhật bảng ci_school_child)
             * 3. Cập nhật cha mẹ vào nhóm của lớp cũ/mới.
             * 4. Cho cha mẹ thành bạn của giáo viên lớp mới.
             * 5. Chuyển thông tin điểm danh từ đầu tháng của trẻ sang lớp mới.
             * 6. Chuyển thông tin điểm của trẻ sang lớp mới
             * 7. CHuyển thông tin hạnh kiểm của trẻ sang lớp mới
             *
             */
            $db->begin_transaction();

//            // Lấy ra thông tin cũ của trẻ để kiểm tra
//            $oldChild = $childDao->getSchoolChild($_POST['child_id'], $school['page_id']);
//            if (is_null($oldChild)) {
//                _error(404);
//            }

            //2. Cập nhật danh sách lớp (đưa trẻ vào lớp - bảng ci_class_child) (nếu có thay đổi về lớp)
            $childDao->moveClassInClassChildren($_POST['child'], $_POST['old_class_id'], $_POST['new_class_id']);

            //3. Cập nhật thông tin trẻ trong trường (cập nhật bảng ci_school_child) - Khi chuyển nhiều trẻ
            $childDao->moveClassInSchoolChildren($school['page_id'], $_POST['child'], $_POST['new_class_id']);

            //4. Cập nhật thông tin học phí.
//            $tuitionDao->updateTuition4MoveClass($_POST['child_id'], $_POST['old_class_id'], $_POST['new_class_id']);

            //$parentIds = $parentDao->getParentIds($_POST['child_id']);
            $parentAllIds = array();
            foreach ($_POST['child'] as $childId) {
                $parents = getChildData($childId, CHILD_PARENTS);
                $parentIds = array_keys($parents);
                $parentAllIds = array_merge($parentAllIds, $parentIds);
            }
            $parentAllIds = array_unique($parentAllIds);
            if (count($parentAllIds) > 0) {
                //Xóa cha mẹ khỏi lớp cũ
                if ($_POST['old_class_id'] > 0) {
                    $classDao->deleteUserFromClass($_POST['old_class_id'], $parentAllIds);
                }
                // Thêm cha mẹ vào nhóm của lớp mới.
                $classDao->addUserToClass($_POST['new_class_id'], $parentAllIds);

                //4. Kết bạn cha mẹ với cô giáo lớp mới
                // Tạm thời không cho kết bạn giữa giáo viên, phụ huynh
                /*$teacherIds = $teacherDao->getTeacherIDOfClass($_POST['new_class_id']);
                $userDao->becomeFriends($teacherIds, $parentIds);*/

            }
            //5. Chuyển thông tin điểm danh từ đầu tháng của trẻ sang lớp mới.
            foreach ($_POST['child'] as $childId) {
                include_once(DAO_PATH . 'dao_attendance.php');
                $attendanceDao = new AttendanceDAO();
                $beginDate = date($system['date_format'], mktime(0, 0, 0, date("m"), 1, date("Y")));
                $endDate = date($system['date_format']);
                //5.1 Lấy thông tin điểm danh của trẻ ở lớp CŨ
                $oldAtts = $attendanceDao->getAttendanceChild4MoveClass($_POST['old_class_id'], $childId, $beginDate, $endDate);
                //5.2 Lấy thông tin điểm danh trong tháng của lớp MỚI.
                $newAtts = $attendanceDao->getAttendanceClass4MoveClass($_POST['new_class_id'], $beginDate, $endDate);
                //5.3 Duyệt danh sách điểm danh lớp CŨ
                $tmpAtt = null;
                foreach ($oldAtts as $oldAtt) {
                    $found = false;
                    foreach ($newAtts as $newAtt) {
                        if ($oldAtt['attendance_date'] == $newAtt['attendance_date']) {
                            $found = true;
                            $tmpAtt = $newAtt;
                            break;
                        }
                    }
                    if ($found) {
                        //5.4 Chuyển thông tin điểm danh sang lớp mới
                        $attendanceDao->updateAttendanceDetail4MoveClass($oldAtt['attendance_detail_id'], $tmpAtt['attendance_id']);
                        //5.5 Cập nhật số lượng đi/nghỉ của lớp cũ
                        $attendanceDao->updatePresentAbsence4OldClass($oldAtt['attendance_id'], $oldAtt['status']);
                        //5.6 Cập nhật số lượng đi/nghỉ của lớp mới.
                        $attendanceDao->updatePresentAbsence4NewClass($tmpAtt['attendance_id'], $oldAtt['status']);
                    } else {
                        //Trường hợp này là lớp CŨ có điểm danh, nhưng lớp mới không điểm danh ngày đó.
                        // Khởi tạo điểm danh lớp mới
                        $args['attendance_date'] = toSysDate($oldAtt['attendance_date']);
                        $args['absence_count'] = 0;
                        $args['present_count'] = 0;
                        $args['is_checked'] = 0;
                        $args['class_id'] = $_POST['new_class_id'];
                        // Tạo bản ghi
                        $atds['attendance_id'] = $attendanceDao->insertAttendance($args);
                        //5.4 Chuyển thông tin điểm danh sang lớp mới
                        $attendanceDao->updateAttendanceDetail4MoveClass($oldAtt['attendance_detail_id'], $atds['attendance_id']);
                        //5.5 Cập nhật số lượng đi/nghỉ của lớp cũ
                        $attendanceDao->updatePresentAbsence4OldClass($oldAtt['attendance_id'], $oldAtt['status']);

                        //Xóa điểm danh ở lớp cũ khi không tìm thấy điểm danh ở lớp mới.
                        // $attendanceDao->deleteAttendanceDetail4OldClass($oldAtt);
                    }
                }
                //ADD START MANHDD 03/06/2021
                // 6. Chuyển thông tin điểm của trẻ dang lớp mới
                $childInfo = getChildData($childId, CHILD_INFO);
                if($school['grade'] > 0) {
                    $pointDao->updateMoveClassId($childInfo['child_code'], $_POST['old_class_id'], $_POST['new_class_id'], $school['grade']);
                }
                // 7. Chuyển thông tin hạnh kiểm của trẻ dang lớp mới
                if($school['grade'] > 0) {
                    $conductDao->updateMoveClassId($childId, $_POST['old_class_id'], $_POST['new_class_id']);
                }
                //ADD END MANHDD 03/06/2021
            }

            /* ---------- UPDATE - MEMCACHE ---------- */
            foreach ($_POST['child'] as $childId) {
                //1.Cập nhật thông tin trẻ
                updateChildData($childId);
                deleteClassData($_POST['old_class_id'], CLASS_CHILDREN, $childId);
            }

            //2. Cập nhật thông tin lớp cũ
            updateClassData($_POST['old_class_id'], CLASS_INFO);

            //3. Cập nhật thông tin lớp mới
            updateClassData($_POST['new_class_id'], CLASS_INFO);
            updateClassData($_POST['new_class_id'], CLASS_CHILDREN);

            /* ---------- END - MEMCACHE ---------- */

            $db->commit();
            return_json( array('success' => true, 'message' => __("Done, move class success")) );
            break;
        default:
            _error(400);
            break;
    }

	// return & exit
	//return_json($return);
} catch (Exception $e) {
    $db->rollback();
	return_json( array('error' => true, 'message' => $e->getMessage()) );
} finally {
    $db->autocommit(true);
    if (!is_null($objPHPExcel)) {
        $objPHPExcel->disconnectWorksheets();
        //$objPHPExcel->__destruct();
        unset($objPHPExcel);
    }
}
/**
 * check_email
 *
 * @param string $email
 * @param boolean $return_info
 * @return boolean|array
 *
 */
function check_email($email, $return_info = false) {
    global $db;
    $query = $db->query(sprintf("SELECT * FROM users WHERE user_email = %s", secure($email) )) or _error(SQL_ERROR_THROWEN);
    if($query->num_rows > 0) {
        if($return_info) {
            $info = $query->fetch_assoc();
            return $info;
        }
        return true;
    }
    return false;
}

/**
 * Lấy ra thông tin học phí chi tiết cần quyết toán trước khi trẻ nghỉ.
 *
 * @return array
 */
function getTuition4LeaveData() {
    //Thông tin phí tính cho tháng này
    $feeIds = is_null($_POST['fee_id'])? array(): $_POST['fee_id'];
    $feeQuantities = is_null($_POST['quantity'])? array(): $_POST['quantity'];
    $feePrices = is_null($_POST['unit_price'])? array(): $_POST['unit_price'];

    $tuitionDetails = array();
    for ($idx = 0; $idx < count($feeIds); $idx++) {
        $tuitionDetail = array();
        $tuitionDetail['service_id'] = 0;
        $tuitionDetail['fee_id'] = $feeIds[$idx];
        $tuitionDetail['quantity'] = $feeQuantities[$idx];
        $tuitionDetail['unit_price'] = str_replace(',', '', $feePrices[$idx]);
        $tuitionDetail['type'] = TUITION_DETAIL_FEE;
        $tuitionDetail['service_type'] = 0;

        if ($tuitionDetail['quantity']*$tuitionDetail['unit_price'] != 0) {
            $tuitionDetails[] = $tuitionDetail;
        }
    }

    // Lấy dịch vụ của tháng hiện tại
    $serviceIds = is_null($_POST['service_id'])? array(): $_POST['service_id'];
    $serviceQuantities = is_null($_POST['service_quantity'])? array(): $_POST['service_quantity'];
    $servicePrices = is_null($_POST['service_fee'])? array(): $_POST['service_fee'];
    $serviceTypes = is_null($_POST['service_type'])? array(): $_POST['service_type'];
    for ($idx = 0; $idx < count($serviceIds); $idx++) {
        $tuitionDetail = array();
        $tuitionDetail['service_id'] = $serviceIds[$idx];
        $tuitionDetail['fee_id'] = 0;
        $tuitionDetail['quantity'] = $serviceQuantities[$idx];
        $tuitionDetail['unit_price'] = str_replace(',', '', $servicePrices[$idx]);
        $tuitionDetail['type'] = TUITION_DETAIL_SERVICE;
        $tuitionDetail['service_type'] = $serviceTypes[$idx];

        if ($tuitionDetail['quantity']*$tuitionDetail['unit_price'] != 0) {
            $tuitionDetails[] = $tuitionDetail;
        }
    }

    $cbServiceIds = is_null($_POST['cbservice_id'])? array(): $_POST['cbservice_id'];
    $cbServiceQuantities = is_null($_POST['cbservice_quantity'])? array(): $_POST['cbservice_quantity'];
    $cbServicePrices = is_null($_POST['cbservice_price'])? array(): $_POST['cbservice_price'];
    //Gộp dịch vụ tính theo SỐ LẦN
    for ($cbIdx = 0; $cbIdx < count($cbServiceIds); $cbIdx++) {
        $tuitionDetail = array();
        $tuitionDetail['service_id'] = $cbServiceIds[$cbIdx];
        $tuitionDetail['fee_id'] = 0;
        $tuitionDetail['quantity'] = $cbServiceQuantities[$cbIdx];
        $tuitionDetail['unit_price'] = convertMoneyToNumber($cbServicePrices[$cbIdx]);
        $tuitionDetail['service_type'] = SERVICE_TYPE_COUNT_BASED;
        if ($cbServiceIds[$cbIdx] > 0) {
            $tuitionDetail['type'] = TUITION_DETAIL_SERVICE;
        } else {
            $tuitionDetail['type'] = LATE_PICKUP_FEE;
        }
        if ($tuitionDetail['quantity']*$tuitionDetail['unit_price'] != 0) {
            $tuitionDetails[] = $tuitionDetail;
        }
    }

    return $tuitionDetails;
}

/**
 * Xử lý điền dữ liệu quyết toán học phí vào file Excel.
 *
 * @param $objPHPExcel
 * @param $child
 * @param $school
 * @param $data
 * @param $sheetName
 */
function processExport4Leave($objPHPExcel, $child, $school, $data, $sheetName)
{
    //$objPHPExcel = new PHPExcel();
    $sheet = $objPHPExcel->getSheetByName($sheetName);

    $all_border_style = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('rgb' => '333333'))));

    //Set thông tin trường/lớp và trẻ ở đầu bảng học phí.
    $sheet->setCellValue('A1', mb_strtoupper(convertText4Web(str_replace('/', '_', $school['page_title'])), "UTF-8"))
        ->setCellValue('A3', __('Child') . ': ' . convertText4Web($child['child_name']))
        ->setCellValue('A4', __('Leaving date') . ': ' . convertText4Web($child['end_at']));

    $firstRow = 7;
    $rowNumber = $firstRow;
    // Lặp lấy ra thông tin học phí ước tính của trẻ
    $idx = 1;
    foreach ($data['fees'] as $detail) {
        $sheet->setCellValue('A' . $rowNumber, $idx)
            ->setCellValue('B' . $rowNumber, convertText4Web($detail['fee_name']))
            ->setCellValue('C' . $rowNumber, $detail['quantity'])
            ->setCellValue('D' . $rowNumber, $detail['unit_price'])
            ->setCellValue('E' . $rowNumber, $detail['to_money']);
        $rowNumber++;
        $idx++;
    }

    //Thông tin dịch vụ ước tính
    if (count($data['services']) > 0) {
        $sheet->mergeCells('A' . $rowNumber . ':E' . $rowNumber);
        $sheet->getStyle('A' . $rowNumber)->applyFromArray(styleArray('Times new Roman', true, false, 11, PHPExcel_Style_Alignment::HORIZONTAL_LEFT, PHPExcel_Style_Alignment::VERTICAL_CENTER, true));
        $sheet->setCellValue('A' . $rowNumber, __('Service'));
        $rowNumber++;
        // Lặp lấy ra thông tin dịch vụ ước tính tháng này của trẻ
        $idx = 1;
        foreach ($data['services'] as $detail) {
            $sheet->setCellValue('A' . $rowNumber, $idx)
                ->setCellValue('B' . $rowNumber, convertText4Web($detail['service_name']))
                ->setCellValue('C' . $rowNumber, $detail['quantity'])
                ->setCellValue('D' . $rowNumber, $detail['unit_price'])
                ->setCellValue('E' . $rowNumber, $detail['to_money']);
            $rowNumber++;
            $idx++;
        }
    }

    // Phí dịch vụ theo số lần - sử dụng tháng trước
    if (count($data['count_based_services']) > 0) {
        $sheet->mergeCells('A' . $rowNumber . ':E' . $rowNumber);
        $sheet->getStyle('A' . $rowNumber)->applyFromArray(styleArray('Times new Roman', true, false, 11, PHPExcel_Style_Alignment::HORIZONTAL_LEFT, PHPExcel_Style_Alignment::VERTICAL_CENTER, true));
        $sheet->setCellValue('A' . $rowNumber, __("Count-based service"));
        $rowNumber++;

        // Lặp lấy ra thông tin dịch vụ tính theo số lần sử dụng tháng trước của trẻ
        $idx = 1;
        foreach ($data['count_based_services'] as $detail) {

            $sheet->setCellValue('A' . $rowNumber, $idx)
                ->setCellValue('B' . $rowNumber, convertText4Web($detail['service_name']))
                ->setCellValue('C' . $rowNumber, $detail['quantity'])
                ->setCellValue('D' . $rowNumber, $detail['unit_price'])
                ->setCellValue('E' . $rowNumber, $detail['to_money']);
            $rowNumber++;
            $idx++;
        }
    }

    //Nợ tháng trước
    if ($data['debt_amount'] > 0) {
        $sheet->mergeCells('A' . $rowNumber . ':D' . $rowNumber);
        $sheet->getStyle('A' . $rowNumber . ':E' . $rowNumber)->applyFromArray(styleArray('Times new Roman', true, false, 11, PHPExcel_Style_Alignment::HORIZONTAL_RIGHT, PHPExcel_Style_Alignment::VERTICAL_CENTER, true));
        $sheet->setCellValue('A' . $rowNumber, __('Debt amount of previous month')." (".MONEY_UNIT.")")->setCellValue('E' . $rowNumber, $data['debt_amount']);
        $rowNumber++;
    }

    //Giảm trừ tháng trước
    $sheet->mergeCells('A' . $rowNumber . ':D' . $rowNumber);
    $sheet->getStyle('A' . $rowNumber . ':E' . $rowNumber)->applyFromArray(styleArray('Times new Roman', true, false, 11, PHPExcel_Style_Alignment::HORIZONTAL_RIGHT, PHPExcel_Style_Alignment::VERTICAL_CENTER, true));
    $sheet->setCellValue('A' . $rowNumber, __('Tuition deduction of previous month')." (".MONEY_UNIT.")")->setCellValue('E' . $rowNumber, $data['total_deduction']);
    $rowNumber++;

    //Số tiền đã nộp đầu tháng
    $sheet->mergeCells('A' . $rowNumber . ':D' . $rowNumber);
    $sheet->getStyle('A' . $rowNumber . ':E' . $rowNumber)->applyFromArray(styleArray('Times new Roman', true, false, 11, PHPExcel_Style_Alignment::HORIZONTAL_RIGHT, PHPExcel_Style_Alignment::VERTICAL_CENTER, true));
    $sheet->setCellValue('A' . $rowNumber, __('Paid tuition amount at begining')." (".MONEY_UNIT.")")->setCellValue('E' . $rowNumber, $data['paid_amount']);
    $rowNumber++;

    // Số tiền phải đóng cuối cùng
    $sheet->mergeCells('A' . $rowNumber . ':D' . $rowNumber);
    $sheet->getStyle('A' . $rowNumber . ':E' . $rowNumber)->applyFromArray(styleArray('Times new Roman', true, false, 11, PHPExcel_Style_Alignment::HORIZONTAL_RIGHT, PHPExcel_Style_Alignment::VERTICAL_CENTER, true));
    $sheet->setCellValue('A' . $rowNumber, __('Final total')." (".MONEY_UNIT.")")->setCellValue('E' . $rowNumber, $data['final_amount']);

    // Căn giữa trường STT
    $sheet->getStyle('A7:A' . ($rowNumber - 1))->applyFromArray(styleArray('Times new Roman', false, false, 11, PHPExcel_Style_Alignment::HORIZONTAL_CENTER, PHPExcel_Style_Alignment::VERTICAL_CENTER, true));
    $sheet->getStyle('A' . $firstRow . ':E' . $rowNumber)->applyFromArray($all_border_style);

    // Ngày nộp tiền
    $rowNumber++;
    $sheet->mergeCells('A' . $rowNumber . ':E' . $rowNumber);
    $sheet->getStyle('A' . $rowNumber . ':C' . $rowNumber)->applyFromArray(
        styleArray('Times new Roman', false, true, 11, PHPExcel_Style_Alignment::HORIZONTAL_RIGHT, PHPExcel_Style_Alignment::VERTICAL_CENTER, true)
    );
    $sheet->setCellValue('A' . $rowNumber, '..........., ' . __('day') . ' .... ' . __('month') . ' .... ' . __('year') . ' ...... ');

    // Những người liên quan
    $rowNumber = $rowNumber + 2;
    $sheet->mergeCells('A' . $rowNumber . ':B' . $rowNumber);
    $sheet->mergeCells('C' . $rowNumber . ':E' . $rowNumber);
    $sheet->getStyle('A' . $rowNumber . ':C' . $rowNumber)->applyFromArray(styleArray('Times new Roman', true, false, 11, PHPExcel_Style_Alignment::HORIZONTAL_CENTER, PHPExcel_Style_Alignment::VERTICAL_CENTER, true));
    $sheet->setCellValue('A' . $rowNumber, __('Cashier'))->setCellValue('C' . $rowNumber, __('Payer'));
    unset($sheet);
}
?>