<?php
/**
 * Created by PhpStorm.
 * User: Taila
 * Date: 12/20/2016
 * Time: 12:22 AM
 */
require('../../../../../bootstrap.php');
// check AJAX request
is_ajax();
include_once(DAO_PATH . 'dao_contact.php');
include_once(DAO_PATH . 'dao_mail.php');

$contactDao = new ContactDAO();
$mailDao = new MailDAO();

try {
    $return = array();
    $db->autocommit(false);
    $db->begin_transaction();
    $args = array();
    $args['name'] = $_POST['name2'];
    $args['email'] = $_POST['email2'];
    $args['phone'] = $_POST['phone2'];
//    $args['select_school'] = $_POST['select_school2'];
    $args['content'] = $_POST['content2'];
    $args['contact_ip'] = $_SERVER['REMOTE_ADDR'];
    if (!$user->_logged_in) {
        $res = $_POST['g-recaptcha-response'];
        /* check reCAPTCHA */
        if($system['reCAPTCHA_enabled']) {
            require(ABSPATH . 'includes/libs/ReCaptcha/autoload.php');
            $recaptcha = new \ReCaptcha\ReCaptcha($system['reCAPTCHA_secret_key']);
            $resp = $recaptcha->verify($res, $_SERVER['REMOTE_ADDR']);
            if (!$resp->isSuccess()) {
                throw new Exception(__("The security check is incorrect. Please try again"));
            }
        }
    }

    // Lưu thông tin vào hệ thống
    $contactDao->saveContact($args);

    // lấy giá trị $body để gửi mail
    $content = "";
    $content .= 'Họ tên: ' . $args['name'] . "\n";
    $content .= 'Email: ' . $args['email'] . "\n";
    $content .= 'Điện thoại: ' . $args['phone'] . "\n";
//    $content .= 'Ý kiến đóng góp: ' . $args['select_school'] . "\n";
    $content .= 'Nội dung: ' . $args['content'] . "\n";

    // đặt subject
    $subject = $system['system_title'];
    $argEmail = array();
    $argEmail['action'] = "send_contact";
    $argEmail['receivers'] = $receive_email;
    $argEmail['subject'] = $subject;

    //Tạo nên nội dung email
    $argEmail['content'] = $content;

    $argEmail['delete_after_sending'] = 1;
    $argEmail['school_id'] = 0;
    $argEmail['user_id'] = 0;
    $mailDao->insertEmail($argEmail);
    // Gọi hàm sendEmail
    //$mailDao->sendEmail($receive_email, $subject, $content, $isHtml = true);
    // $receive-email: Các mail được nhận khi gửi thông tin liên hệ, cấu hình trong contants.php

    return_json( array('success' => true, 'message' => __("Thanks for Your Help")) );
    $db->commit();
}
catch (Exception $e) {
    $db->rollback();
    return_json( array('error' => true, 'message' => $e->getMessage()) );
} finally {
    $db->autocommit(true);
}
?>
