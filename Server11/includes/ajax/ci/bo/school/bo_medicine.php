<?php
/**
 * Package: ajax/ci/bo
 * 
 * @package ConIu
 * @author QuanND
 */

// fetch bootstrap
require('../../../../../bootstrap.php');

// fetch image class

require(ABSPATH . 'includes/class-image.php');

// check AJAX Request
is_ajax();
check_login();

if(is_empty($_POST['school_username']) || !valid_username($_POST['school_username'])) {
    _error(404);
}

include_once(DAO_PATH . 'dao_school.php');
include_once(DAO_PATH . 'dao_medicine.php');
include_once(DAO_PATH . 'dao_parent.php');
include_once(DAO_PATH . 'dao_teacher.php');
include_once(DAO_PATH . 'dao_user.php');
include_once(DAO_PATH . 'dao_class.php');
include_once(DAO_PATH . 'dao_child.php');
$schoolDao = new SchoolDAO();
$medicineDao = new MedicineDAO();
$parentDao = new ParentDAO();
$teacherDao = new TeacherDAO();
$userDao = new UserDAO();
$classDao = new ClassDAO();
$childDao = new ChildDAO();

//$school = getSchool($_POST['school_username']);
$school = getSchoolDataByUsername($_POST['school_username'], SCHOOL_INFO);
if (is_null($school)) {
    _error(403);
}

try {
    $return = array();
    $db->autocommit(false);
    switch ($_POST['do']) {
        case 'list_child_search':
            if(!isset($_POST['class_id']) || !is_numeric($_POST['class_id'])) {
                _error(404);
            }

            $children = getClassData($_POST['class_id'], CLASS_CHILDREN);

            $results = "";
            foreach ($children as $child) {
                $results = $results.'<option value="'.$child['child_id'].'">'.$child['child_name'].' - '.$child['birthday'].'</option>';
            }
            $return['results'] = $results;
            return_json($return);
            break;
        case 'edit':
            if (!canEdit($_POST['school_username'], 'medicines')) {
                _error(403);
            }
            // valid inputs
            if(!isset($_POST['medicine_id']) || !is_numeric($_POST['medicine_id'])) {
                _error(400);
            }
            $data = $medicineDao->getMedicine($_POST['medicine_id'], false);
            if (is_null($data)) {
                _error(404);
            }
            $db->begin_transaction();

            $args = array();
            $args['medicine_id'] = $_POST['medicine_id'];
            $args['child_id'] = $_POST['child_id'];
            $args['medicine_list'] = $_POST['medicine_list'];
            $args['begin'] = $_POST['begin'];
            $args['end'] = $_POST['end'];
            $args['guide'] = $_POST['guide'];
            $args['time_per_day'] = $_POST['time_per_day'];
            $args['status'] = MEDICINE_STATUS_CONFIRMED;
            $args['source_file'] = $data['source_file_path'];
            $args['file_name'] = convertText4Web($data['file_name']);

            $args['source_file'] = $data['source_file_path'];
            $args['file_name'] = convertText4Web($data['file_name']);
            if(!$_POST['is_file']) {
                $args['source_file'] = "";
                $args['file_name'] = "";
            }

            $file_name = "";
            // Upload file đính kèm
            if(file_exists($_FILES['file']['tmp_name'])) {
                // check file upload enabled
                if(!$system['file_enabled']) {
                    throw new Exception(__("This feature has been disabled"));
                }

                // valid inputs
                if(!isset($_FILES["file"]) || $_FILES["file"]["error"] != UPLOAD_ERR_OK) {
                    throw new Exception(__("Something wrong with upload! Is 'upload_max_filesize' set correctly?"));
                }

                // check file size
                $max_allowed_size = $system['max_file_size'] * 1024;
                if($_FILES["file"]["size"] > $max_allowed_size) {
                    throw new Exception(__("The file size is so big"));
                }

                // check file extesnion
                $extension = get_extension($_FILES['file']['name']);
                if(!valid_extension($extension, $system['file_medicine_extensions'])) {
                    // modal(MESSAGE, __("Upload Error"), __("The file type is not valid or not supported"));
                    return_json( array('error' => true, 'message' => __("The file type is not valid or not supported")) );
                }

                /* check & create uploads dir */
                $depth = '../../../../../';
                $folder = 'medicines/'. $school['school_id'];
                if(!file_exists($depth.$system['system_uploads_directory'].'/'.$folder)) {
                    @mkdir($depth.$system['system_uploads_directory'].'/'.$folder, 0777, true);
                }
                if(!file_exists($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y'))) {
                    @mkdir($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y'), 0777, true);
                }
                if(!file_exists($system['system_uploads_directory'].'/'.$folder.'/' . date('Y') . '/' . date('m'))) {
                    @mkdir($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y') . '/' . date('m'), 0777, true);
                }

                /* prepare new file name */
                $directory = $folder.'/'. date('Y') . '/' . date('m') . '/';
                $prefix = $system['uploads_prefix'].'_'.md5(time()*rand(1, 9999));

                $image = new Image($_FILES["file"]["tmp_name"]);
                $file_name = $directory.$prefix.$image->_img_ext;
                $file_name_tmp = $directory.$prefix.'_tmp'.$image->_img_ext;

                $path = $depth.$system['system_uploads_directory'].'/'.$file_name;
                $path_tmp = $depth.$system['system_uploads_directory'].'/'.$file_name_tmp;

                /* check if the file uploaded successfully */
                if(!@move_uploaded_file($_FILES['file']['tmp_name'], $path_tmp)) {
                    throw new Exception(__("Sorry, can not upload the file"));
                }
                /* save the new image */
                $image->save($path, $path_tmp);

                /* delete the tmp image */
                unlink($path_tmp);
                $args['source_file'] = $file_name;
                $args['file_name'] = $_FILES['file']['name'];

            }
            //Cập nhật thông tin vào hệ thống
            $medicineDao->updateMedicine($args);

            //Thông báo cô giáo và phụ huynh

            // Lấy danh sách phụ huynh của trẻ
            //$child = $childDao->getChild($_POST['child_id']);
            $child = getChildData($_POST['child_id'], CHILD_INFO);
            //$parents = array();
            //$parents = $parentDao->getParent($_POST['child_id']);
            $parents = getChildData($_POST['child_id'], CHILD_PARENTS);

            if (!is_null($child) && !is_null($parents)) {
                $parentIds = array_keys($parents);
                $userDao->postNotifications($parentIds, NOTIFICATION_UPDATE_MEDICINE, NOTIFICATION_NODE_TYPE_CHILD,
                    $_POST['medicine_id'], convertText4Web($_POST['medicine_list']), $_POST['child_id'], convertText4Web($child['child_name']));
            }

            // Lấy danh sách giáo viên của lớp
            //$class = $classDao->getClass($_POST['class_id']);
            $class = getClassData($_POST['class_id'], CLASS_INFO);
            //$teacherIds = $teacherDao->getTeacherIDOfClass($_POST['class_id']);
            $teachers = getClassData($_POST['class_id'], CLASS_TEACHERS);

            if (!is_null($child) && !is_null($class) && !is_null($teachers)) {
                $teacherIds = array_keys($teachers);
                $userDao->postNotifications($teacherIds, NOTIFICATION_UPDATE_MEDICINE, NOTIFICATION_NODE_TYPE_CLASS,
                    $_POST['medicine_id'], convertText4Web($_POST['medicine_list']), $class['group_name'], convertText4Web($child['child_name']));

                // Thông báo cho các quản lý trường khác đơn thuốc được cập nhật
                $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'medicines', $school['page_admin']);
                $userDao->postNotifications($userManagerIds, NOTIFICATION_UPDATE_MEDICINE, NOTIFICATION_NODE_TYPE_SCHOOL,
                    $_POST['medicine_id'], convertText4Web($_POST['medicine_list']), $school['page_name'], convertText4Web($child['child_name']));

            }

            $db->commit();
            // return_json( array('success' => true, 'message' => __("Done, Medicine info have been updated")) );
            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/school/' . $_POST['school_username'] . '/medicines";'));
            break;
        case 'add':
            if (!canEdit($_POST['school_username'], 'medicines')) {
                _error(403);
            }
            $db->begin_transaction();

            $args = array();
            $args['child_id'] = $_POST['child_id'];
            $args['medicine_list'] = $_POST['medicine_list'];
            $args['begin'] = $_POST['begin'];
            $args['end'] = $_POST['end'];
            $args['guide'] = $_POST['guide'];
            $args['time_per_day'] = $_POST['time_per_day'];
            $args['created_user_id'] = $user->_data['user_id'];
            $args['status'] = MEDICINE_STATUS_CONFIRMED; //Do trường tạo ra thì chuyển trạng thái xác nhận luôn
            $file_name = "";

            // Upload file đính kèm
            if(file_exists($_FILES['file']['tmp_name'])) {
                // check file upload enabled
                if(!$system['file_enabled']) {
                    throw new Exception(__("This feature has been disabled"));
                }

                // valid inputs
                if(!isset($_FILES["file"]) || $_FILES["file"]["error"] != UPLOAD_ERR_OK) {
                    throw new Exception(__("Something wrong with upload! Is 'upload_max_filesize' set correctly?"));
                }

                // check file size
                $max_allowed_size = $system['max_file_size'] * 1024;
                if($_FILES["file"]["size"] > $max_allowed_size) {
                    throw new Exception(__("The file size is so big"));
                }

                // check file extesnion
                $extension = get_extension($_FILES['file']['name']);
                if(!valid_extension($extension, $system['file_medicine_extensions'])) {
                    //modal(MESSAGE, __("Upload Error"), __("The file type is not valid or not supported"));
                    return_json( array('error' => true, 'message' => __("The file type is not valid or not supported")) );
                }

                /* check & create uploads dir */
                $depth = '../../../../../';
                $folder = 'medicines/'. $school['school_id'];
                if(!file_exists($depth.$system['system_uploads_directory'].'/'.$folder)) {
                    @mkdir($depth.$system['system_uploads_directory'].'/'.$folder, 0777, true);
                }
                if(!file_exists($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y'))) {
                    @mkdir($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y'), 0777, true);
                }
                if(!file_exists($system['system_uploads_directory'].'/'.$folder.'/' . date('Y') . '/' . date('m'))) {
                    @mkdir($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y') . '/' . date('m'), 0777, true);
                }

                /* prepare new file name */
                $directory = $folder.'/'. date('Y') . '/' . date('m') . '/';
                $prefix = $system['uploads_prefix'].'_'.md5(time()*rand(1, 9999));

                $image = new Image($_FILES["file"]["tmp_name"]);
                $file_name = $directory.$prefix.$image->_img_ext;
                $file_name_tmp = $directory.$prefix.'_tmp'.$image->_img_ext;

                $path = $depth.$system['system_uploads_directory'].'/'.$file_name;
                $path_tmp = $depth.$system['system_uploads_directory'].'/'.$file_name_tmp;

                /* check if the file uploaded successfully */
                if(!@move_uploaded_file($_FILES['file']['tmp_name'], $path_tmp)) {
                    throw new Exception(__("Sorry, can not upload the file"));
                }
                /* save the new image */
                $image->save($path, $path_tmp);

                /* delete the tmp image */
                unlink($path_tmp);
            }

            $args['source_file'] = $file_name;
            $args['file_name'] = $_FILES['file']['name'];
            //Nhập thông tin vào hệ thống
            $medicineId = $medicineDao->insertMedicine($args);

            // Tăng lượt tương tác thêm mới - TaiLA
            addInteractive($school['page_id'], 'medicine', 'school_view', $medicineId, 1);

            //Thông báo cô giáo và phụ huynh
            // Lấy danh sách phụ huynh của trẻ
            //$child = $childDao->getChild($_POST['child_id']);
            $child = getChildData($_POST['child_id'], CHILD_INFO);
            //$parents = array();
            //$parents = $parentDao->getParent($_POST['child_id']);
            $parents = getChildData($_POST['child_id'], CHILD_PARENTS);

            if (!is_null($child) && !is_null($parents)) {
                $parentIds = array_keys($parents);
                $userDao->postNotifications($parentIds, NOTIFICATION_NEW_MEDICINE, NOTIFICATION_NODE_TYPE_CHILD,
                    $medicineId, convertText4Web($_POST['medicine_list']), $_POST['child_id'], convertText4Web($child['child_name']));
            }


            // Lấy danh sách giáo viên của lớp
            //$class = $classDao->getClass($_POST['class_id']);
            $class = getClassData($_POST['class_id'], CLASS_INFO);
            //$teacherIds = $teacherDao->getTeacherIDOfClass($_POST['class_id']);
            $teachers = getClassData($_POST['class_id'], CLASS_TEACHERS);
            if (!is_null($child) && !is_null($class) && !is_null($teachers)) {
                $teacherIds = array_keys($teachers);
                $userDao->postNotifications($teacherIds, NOTIFICATION_NEW_MEDICINE, NOTIFICATION_NODE_TYPE_CLASS,
                    $medicineId, convertText4Web($_POST['medicine_list']), $class['group_name'], convertText4Web($child['child_name']));

                // Thông báo cho các quản lý trường khác đơn thuốc được thêm mới
                $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'medicines', $school['page_admin']);
                $userDao->postNotifications($userManagerIds, NOTIFICATION_UPDATE_MEDICINE, NOTIFICATION_NODE_TYPE_SCHOOL,
                    $medicineId, convertText4Web($_POST['medicine_list']), $school['page_name'], convertText4Web($child['child_name']));
            }

            $db->commit();
//            return_json( array('success' => true, 'message' => 'Tạo gửi thuốc thành công.'));
            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/school/' . $_POST['school_username'] . '/medicines";'));
            break;

        case 'delete':
            if (!canEdit($_POST['school_username'], 'medicines')) {
                _error(403);
            }
            if(!isset($_POST['id']) || !is_numeric($_POST['id'])) {
                _error(400);
            }
            $db->begin_transaction();

            $medicineDao->deleteMedicine($_POST['id']);

            $db->commit();
            return_json(array('callback' => 'window.location = "'.$system['system_url'].'/school/'.$_POST['school_username'].'/medicines";'));
            break;

        case 'medicate':
            if (!canEdit($_POST['school_username'], 'medicines')) {
                _error(403);
            }
            $db->begin_transaction();
            //Thêm lần uống thuốc vào hệ thống
            $medicineDao->addMedicateTime($_POST['id'], $_POST['max']);
            //Cập nhật trạng thái của lần gửi thuốc
            $medicineDao->updateMedicineStatus(MEDICINE_STATUS_CONFIRMED, $_POST['id']);
            $medicine = $medicineDao->getMedicine($_POST['id']);

            //Thông báo cho phụ huynh
            // Lấy danh sách phụ huynh của trẻ
            //$child = $childDao->getChild($_POST['child_id']);
            $child = getChildData($_POST['child_id'], CHILD_INFO);
            //$parents = array();
            //$parents = $parentDao->getParent($_POST['child_id']);
            $parents = getChildData($_POST['child_id'], CHILD_PARENTS);
            if (!is_null($child) && !is_null($parents)) {
                $parentIds = array_keys($parents);
                $userDao->postNotifications($parentIds, NOTIFICATION_USE_MEDICINE, NOTIFICATION_NODE_TYPE_CHILD,
                        $_POST['id'], convertText4Web($medicine['medicine_list']), $_POST['child_id'], convertText4Web($child['child_name']));
            }

            // Thông báo cho các quản lý trường khác trẻ được cho uống thuốc
            $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'medicines', $school['page_admin']);
            $userDao->postNotifications($userManagerIds, NOTIFICATION_USE_MEDICINE, NOTIFICATION_NODE_TYPE_SCHOOL,
                $_POST['id'], convertText4Web($_POST['medicine_list']), $school['page_name'], convertText4Web($child['child_name']));

            // Lấy danh sách giáo viên của lớp
            //$class = $classDao->getClass($_POST['class_id']);
            $class = getClassData($_POST['class_id'], CLASS_INFO);
            //$teacherIds = $teacherDao->getTeacherIDOfClass($_POST['class_id']);
            $teachers = getClassData($_POST['class_id'], CLASS_TEACHERS);
            if (!is_null($child) && !is_null($class) && !is_null($teachers)) {
                $teacherIds = array_keys($teachers);
                $userDao->postNotifications($teacherIds, NOTIFICATION_USE_MEDICINE, NOTIFICATION_NODE_TYPE_CLASS,
                    $_POST['id'], convertText4Web($medicine['medicine_list']), $class['group_name'], convertText4Web($child['child_name']));
            }

            $db->commit();
            if (isset($_POST['screen']) && ($_POST['screen'] == 'dashboard')) {
                return_json(array('callback' => 'window.location = "'.$system['system_url'].'/school/'.$_POST['school_username'].'";'));
            } else {
                return_json(array('callback' => 'window.location = "'.$system['system_url'].'/school/'.$_POST['school_username'].'/medicines";'));
            }
            break;

        case 'confirm':
            if (!canEdit($_POST['school_username'], 'medicines')) {
                _error(403);
            }
            $db->begin_transaction();
            //Cập nhật trạng thái của lần gửi thuốc
            $medicineDao->updateMedicineStatus(MEDICINE_STATUS_CONFIRMED, $_POST['id']);
            $medicine = $medicineDao->getMedicine($_POST['id']);

            //Thông báo cho phụ huynh
            // Lấy danh sách phụ huynh của trẻ
            //$child = $childDao->getChild($_POST['child_id']);
            $child = getChildData($_POST['child_id'], CHILD_INFO);
            //$parents = array();
            //$parents = $parentDao->getParent($_POST['child_id']);
            $parents = getChildData($_POST['child_id'], CHILD_PARENTS);
            if (!is_null($child) && !is_null($parents)) {
                $parentIds = array_keys($parents);
                $userDao->postNotifications($parentIds, NOTIFICATION_CONFIRM_MEDICINE, NOTIFICATION_NODE_TYPE_CHILD,
                    $_POST['id'], convertText4Web($medicine['medicine_list']), $_POST['child_id'], convertText4Web($child['child_name']));
            }

            // Lấy danh sách giáo viên của lớp
            //$class = $classDao->getClassOfChild($medicine['child_id']);
            $class = getClassData($child['class_id'], CLASS_INFO);
            //$teacherIds = $teacherDao->getTeacherIDOfClass($class['group_id']);
            $teachers = getClassData($class['group_id'], CLASS_TEACHERS);
            if (!is_null($child) && !is_null($class) && !is_null($teachers)) {
                $teacherIds= array_keys($teachers);
                $userDao->postNotifications($teacherIds, NOTIFICATION_CONFIRM_MEDICINE, NOTIFICATION_NODE_TYPE_CLASS,
                    $medicine['medicine_id'], convertText4Web($medicine['medicine_list']), $class['group_name'], convertText4Web($child['child_name']));

                // Thông báo cho các quản lý trường khác trẻ được cho uống thuốc
                $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'medicines', $school['page_admin']);
                $userDao->postNotifications($userManagerIds, NOTIFICATION_CONFIRM_MEDICINE, NOTIFICATION_NODE_TYPE_SCHOOL,
                    $medicine['medicine_id'], convertText4Web($_POST['medicine_list']), $school['page_name'], convertText4Web($child['child_name']));
            }


            $db->commit();
            if ($_POST['screen'] == 'dashboard') {
                return_json(array('callback' => 'window.location = "'.$system['system_url'].'/school/'.$_POST['school_username'].'";'));
            } elseif ($_POST['screen'] == 'all') {
                return_json( array('success' => true, 'message' => __("Success")) );
            } else {
                return_json( array('success' => true, 'message' => __("Success")) );
            }
            break;

        case 'confirm_all':
            if (!canEdit($_POST['school_username'], 'medicines')) {
                _error(403);
            }
            $db->begin_transaction();
            // Lấy danh sách id thuốc chưa được xác nhận
            $medicineIds = $medicineDao->getAllMedicineIdsNoConfirm($school['page_id']);
            //Cập nhật trạng thái của lần gửi thuốc
            $medicineDao->updateAllMedicineStatus(MEDICINE_STATUS_CONFIRMED, $medicineIds);
            foreach ($medicineIds as $medicineId) {
                $medicine = $medicineDao->getMedicine($medicineId);
                //Thông báo cho phụ huynh
                // Lấy danh sách phụ huynh của trẻ
                $child = getChildData($medicine['child_id'], CHILD_INFO);
                //$parents = array();
                //$parents = $parentDao->getParent($_POST['child_id']);
                $parents = getChildData($medicine['child_id'], CHILD_PARENTS);

                if (!is_null($child) && !is_null($parents)) {
                    $parentIds = array_keys($parents);
                    $userDao->postNotifications($parentIds, NOTIFICATION_CONFIRM_MEDICINE, NOTIFICATION_NODE_TYPE_CHILD,
                        $medicine['medicine_id'], convertText4Web($medicine['medicine_list']), $medicine['child_id'], convertText4Web($child['child_name']));
                }

                // Lấy danh sách giáo viên của lớp
                //$class = $classDao->getClassOfChild($medicine['child_id']);
                $class = getClassData($child['class_id'], CLASS_INFO);
                //$teacherIds = $teacherDao->getTeacherIDOfClass($class['group_id']);
                $teachers = getClassData($class['group_id'], CLASS_TEACHERS);

                if (!is_null($child) && !is_null($class) && !is_null($teachers)) {
                    $teacherIds= array_keys($teachers);
                    $userDao->postNotifications($teacherIds, NOTIFICATION_CONFIRM_MEDICINE, NOTIFICATION_NODE_TYPE_CLASS,
                            $medicine['medicine_id'], convertText4Web($medicine['medicine_list']), $class['group_name'], convertText4Web($child['child_name']));

                    // Thông báo cho các quản lý trường khác trẻ được cho uống thuốc
                    $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'medicines', $school['page_admin']);
                    $userDao->postNotifications($userManagerIds, NOTIFICATION_CONFIRM_MEDICINE, NOTIFICATION_NODE_TYPE_SCHOOL,
                        $medicine['medicine_id'], convertText4Web($_POST['medicine_list']), $school['page_name'], convertText4Web($child['child_name']));
                }
            }
            $db->commit();
            if ($_POST['screen'] == 'dashboard') {
                return_json(array('callback' => 'window.location = "'.$system['system_url'].'/school/'.$_POST['school_username'].'";'));
            } elseif ($_POST['screen'] == 'all') {
                return_json(array('callback' => 'window.location = "'.$system['system_url'].'/school/'.$_POST['school_username'].'/medicines/all";'));
            } else {
                return_json(array('callback' => 'window.location = "'.$system['system_url'].'/school/'.$_POST['school_username'].'/medicines";'));
            }
            break;

        case 'confirm_all_today':
            if (!canEdit($_POST['school_username'], 'medicines')) {
                _error(403);
            }
            $db->begin_transaction();
            $today = date($system['date_format']);
            // Lấy danh sách id thuốc chưa đưucọ xác nhận hôm nay
            $medicineIds = $medicineDao->getMedicineOnDateNoConfirm($school['page_id'], $today);
            //Cập nhật trạng thái của lần gửi thuốc
            $medicineDao->updateAllMedicineStatus(MEDICINE_STATUS_CONFIRMED, $medicineIds);
            foreach ($medicineIds as $medicineId) {
                $medicine = $medicineDao->getMedicine($medicineId);
                //Thông báo cho phụ huynh
                // Lấy danh sách phụ huynh của trẻ
                $child = getChildData($medicine['child_id'], CHILD_INFO);
                //$parents = array();
                //$parents = $parentDao->getParent($_POST['child_id']);
                $parents = getChildData($medicine['child_id'], CHILD_PARENTS);
                if (!is_null($child) && !is_null($parents)) {
                    $parentIds = array_keys($parents);
                    $userDao->postNotifications($parentIds, NOTIFICATION_CONFIRM_MEDICINE, NOTIFICATION_NODE_TYPE_CHILD,
                            $medicine['medicine_id'], convertText4Web($medicine['medicine_list']), $medicine['child_id'], convertText4Web($child['child_name']));
                }

                // Lấy danh sách giáo viên của lớp
                //$class = $classDao->getClassOfChild($medicine['child_id']);
                $class = getClassData($child['class_id'], CLASS_INFO);
                //$teacherIds = $teacherDao->getTeacherIDOfClass($class['group_id']);

                $teachers = getClassData($class['group_id'], CLASS_TEACHERS);
                if (!is_null($child) && !is_null($class) && !is_null($teachers)) {
                    $teacherIds= array_keys($teachers);
                    $userDao->postNotifications($teacherIds, NOTIFICATION_CONFIRM_MEDICINE, NOTIFICATION_NODE_TYPE_CLASS,
                        $medicine['medicine_id'], convertText4Web($medicine['medicine_list']), $class['group_name'], convertText4Web($child['child_name']));

                    // Thông báo cho các quản lý trường khác trẻ được cho uống thuốc
                    $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'medicines', $school['page_admin']);
                    $userDao->postNotifications($userManagerIds, NOTIFICATION_CONFIRM_MEDICINE, NOTIFICATION_NODE_TYPE_SCHOOL,
                        $medicine['medicine_id'], convertText4Web($_POST['medicine_list']), $school['page_name'], convertText4Web($child['child_name']));
                }
            }
            $db->commit();
            if ($_POST['screen'] == 'dashboard') {
                return_json(array('callback' => 'window.location = "'.$system['system_url'].'/school/'.$_POST['school_username'].'";'));
            } elseif ($_POST['screen'] == 'all') {
                return_json(array('callback' => 'window.location = "'.$system['system_url'].'/school/'.$_POST['school_username'].'/medicines/all";'));
            } else {
                return_json(array('callback' => 'window.location = "'.$system['system_url'].'/school/'.$_POST['school_username'].'/medicines";'));
            }
            break;

        case 'cancel':
            if (!canEdit($_POST['school_username'], 'medicines')) {
                _error(403);
            }
            $db->begin_transaction();
            //Cập nhật trạng thái của lần gửi thuốc
            $medicineDao->updateMedicineStatus(MEDICINE_STATUS_CANCEL, $_POST['id']);
            $medicine = $medicineDao->getMedicine($_POST['id']);

            //Thông báo cho phụ huynh
            // Lấy danh sách phụ huynh của trẻ
            $child = getChildData($_POST['child_id'], CHILD_INFO);
            //$parents = array();
            //$parents = $parentDao->getParent($_POST['child_id']);
            $parents = getChildData($_POST['child_id'], CHILD_PARENTS);
            if (!is_null($child) && !empty($parents)) {
                $parentIds = array_keys($parents);
                $userDao->postNotifications($parentIds, NOTIFICATION_CANCEL_MEDICINE, NOTIFICATION_NODE_TYPE_CHILD,
                    $_POST['id'], convertText4Web($medicine['medicine_list']), $_POST['child_id'], convertText4Web($child['child_name']));
            }

            // Lấy danh sách giáo viên của lớp
            //$class = $classDao->getClass($_POST['class_id']);
            $class = getClassData($child['class_id'], CLASS_INFO);
            //$teacherIds = $teacherDao->getTeacherIDOfClass($_POST['class_id']);
            $teachers = getClassData($class['group_id'], CLASS_TEACHERS);
            if (!is_null($child) && !is_null($class) && !empty($teachers)) {
                $teacherIds= array_keys($teachers);
                $userDao->postNotifications($teacherIds, NOTIFICATION_CANCEL_MEDICINE, NOTIFICATION_NODE_TYPE_CLASS,
                    $_POST['id'], convertText4Web($medicine['medicine_list']), $class['group_name'], convertText4Web($child['child_name']));

                // Thông báo cho các quản lý trường khác trẻ được cho uống thuốc
                $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'medicines', $school['page_admin']);
                $userDao->postNotifications($userManagerIds, NOTIFICATION_CANCEL_MEDICINE, NOTIFICATION_NODE_TYPE_SCHOOL,
                    $_POST['id'], convertText4Web($_POST['medicine_list']), $school['page_name'], convertText4Web($child['child_name']));
            }

            $db->commit();
            return_json( array('success' => true, 'message' => __("Success")) );
            break;

        case 'list_child':
            if(!isset($_POST['class_id']) || !is_numeric($_POST['class_id'])) {
                _error(404);
            }

            //$children = $childDao->getChildrenOfClass($_POST['class_id']);
            $children = getClassData($_POST['class_id'], CLASS_CHILDREN);
            $results = "<option value=''>".__("Select student")."...</option>";
            $idx = 1;
            foreach ($children as $child) {
                $results = $results.'<option value="'.$child['child_id'].'">'.$idx." - ".$child['child_name'].' - '.$child['birthday'].'</option>';
                $idx++;
            }
            $return['results'] = $results;

            return_json($return);

            //$medicine_id = $_POST['medicine_id'];
            //return_json(array('callback' => 'window.location = "'.$system['system_url'].'/school/'.$_POST['school_username'].'/medicines/participants/'.$_POST['medicine_id'].'/'.$_POST['class_id'].'";'));
            break;
        case 'detail':
            if(!isset($_POST['id']) || !is_numeric($_POST['id'])) {
                _error(404);
            }

            $details = $medicineDao->getMedicineDetail($_POST['id']);
            $results = "<br/>".__("Medicated").":";
            foreach ($details as $detail) {
                $results = $results.'<br/> - '.$detail['usage_date'].' | '.__("Medication time").' '.$detail['time_on_day'].' | '.$detail['created_at'].' | '.$detail['user_fullname'];
                $results = $results.' <a href="#" class="far fa-comments fa-lg js_chat-start" data-name="'.$detail['user_fullname'].'" data-uid="'.$detail['created_user_id'].'"></a>';
            }
            $return['results'] = $results;

            return_json($return);
            break;

        case 'search_medicine':
            try {
                if(!isset($_POST['level']) || !is_numeric($_POST['level'])) {
                    _error(404);
                }
                $count_no_confirm = 0;

                $result = array();
                switch ($_POST['level']) {
                    case CLASS_LEVEL:
                        if(!is_numeric($_POST['class_id'])) {
                            throw new Exception(__("You must choose a class"));
                        }
                        if(validateDate($_POST['begin']) && validateDate($_POST['end'])) {
                            $result = $medicineDao->getMedicineSearchClass($school['page_id'], $_POST['class_id'], $_POST['begin'], $_POST['end'], $count_no_confirm);
                        } else {
                            $result = $medicineDao->getClassAllMedicines($_POST['class_id'], $count_no_confirm);
                        }
                        break;
                    case PARENT_LEVEL:
                        if(!is_numeric($_POST['child_id'])) {
                            throw new Exception(__("You must choose a student"));
                        }
                        if(validateDate($_POST['begin']) && validateDate($_POST['end'])) {
                            $result = $medicineDao->getMedicineSearchChild($school['page_id'], $_POST['child_id'], $_POST['begin'], $_POST['end'], $count_no_confirm);
                        } else {
                            $result = $medicineDao->getChildAllMedicines($_POST['class_id'], $count_no_confirm);
                        }
                        break;

                    default:
                }
                $smarty->assign('username', $_POST['school_username']);
                $smarty->assign('count_no_confirm', $count_no_confirm);
                $smarty->assign('rows', $result);
                $smarty->assign('canEdit', canEdit($_POST['school_username'], 'medicines'));

                $return['results'] = $smarty->fetch("ci/school/ajax.school.medicinelist.tpl");

                // return & exit
                return_json($return);
            } catch (Exception $e) {
                $db->rollback();
                return_json( array('error' => true, 'message' => $e->getMessage()));
            } finally {
                $db->autocommit(true);
            }
            break;
        default:
            _error(400);
            break;
    }

} catch (Exception $e) {
    $db->rollback();
    return_json( array('error' => true, 'message' => $e->getMessage()) );
} finally {
    $db->autocommit(true);
}

?>