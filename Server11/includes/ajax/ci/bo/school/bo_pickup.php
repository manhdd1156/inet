<?php
/**
 * Package: ajax/ci/bo/school
 *
 * @package ConIu
 * @author QuanND
 */

// fetch bootstrap
require('../../../../../bootstrap.php');

// check AJAX Request
is_ajax();
check_login();

if (is_empty($_POST['school_username']) || !valid_username($_POST['school_username'])) {
    _error(403);
}

include_once(DAO_PATH . 'dao_school.php');
include_once(DAO_PATH . 'dao_class.php');
include_once(DAO_PATH . 'dao_child.php');
include_once(DAO_PATH . 'dao_parent.php');
include_once(DAO_PATH . 'dao_teacher.php');
include_once(DAO_PATH . 'dao_user.php');
include_once(DAO_PATH . 'dao_pickup.php');
$schoolDao = new SchoolDAO();
$classDao = new ClassDAO();
$childDao = new ChildDAO();
$parentDao = new ParentDAO();
$teacherDao = new TeacherDAO();
$userDao = new UserDAO();
$pickupDao = new PickupDAO();

//$school = getSchool($_POST['school_username']);
$school = getSchoolDataByUsername($_POST['school_username'], SCHOOL_INFO);
if (is_null($school)) {
    _error(403);
}
$canEdit = canEdit($_POST['school_username'], 'pickup');

try {
    $return = array();
    $db->autocommit(false);
    switch ($_POST['do']) {
        case 'add_template':
            if (!$canEdit) {
                _error(403);
            }
            if (!isset($_POST['begin_pickup_time'])) {
                throw new Exception(__("You must enter the beginning pickup time"));
            }
            $args = array();
            $args['school_id'] = $school['page_id'];
            $unitPriceIds = $_POST['unit_price_id'];
            $beginTimes = $_POST['begin_pickup_time'];
            $endingTimes = $_POST['ending_time'];
            //$unitPrices = str_replace(',', '', $_POST['unit_price']);
            $unitPrices = $_POST['unit_price'];

            // Tạo ra mảng begining_time
            $resultSort = array();
            $strBegin = strtotime($beginTimes);
            for ($idx = 0; $idx < count($endingTimes); $idx++) {
                $strEnd = strtotime($endingTimes[$idx]);
                if ($strBegin >= $strEnd) {
                    //throw new Exception(("Bảng giá không đúng. Vui lòng kiểm tra lại."));
                    throw new Exception(__("The pick-up time must be greater than the start time"));
                }

                $resultSort[$strEnd]['ending_time'] = $endingTimes[$idx];
                $resultSort[$strEnd]['unit_price'] = $unitPrices[$idx];
            }
            ksort($resultSort);
            $resultSort = array_values($resultSort);
            $resultSort[0]['beginning_time'] = $beginTimes;
            for ($idx = 1; $idx < count($resultSort); $idx++) {
                $resultSort[$idx]['beginning_time'] = date("H:i:s", strtotime('+1 seconds', strtotime($resultSort[$idx - 1]['ending_time'])));
            }
            $args['price_list'] = $resultSort;

            $args['service_id'] = $_POST['service_id'];
            $args['service_name'] = $_POST['service_name'];
            $args['service_price'] = $_POST['service_price'];
            $args['service_description'] = $_POST['service_description'];

            $args['class_id'] = $_POST['class_id'];
            $args['class_name'] = $_POST['class_name'];
            $args['class_description'] = $_POST['class_description'];

            $args['class_delete_id'] = isset($_POST['class_delete_id']) ? $_POST['class_delete_id'] : array();
            $args['service_delete_id'] = isset($_POST['service_delete_id']) ? $_POST['service_delete_id'] : array();

            if (!empty($_POST['teacher'])) {
                $args['teacher'] = $_POST['teacher'];
                foreach ($args['teacher'] as $teacher_id) {
                	$args['teacher_fullname_' . $teacher_id] = $_POST['teacher_fullname_' . $teacher_id];
                	$args['teacher_name_' . $teacher_id] = $_POST['teacher_name_' . $teacher_id];
            	}
            }

            $template = $pickupDao->getTemplate($school['page_id']);

            $db->begin_transaction();
            if (is_null($template)) {
                $pickupDao->insertPickupTemplate($args);
            } else {
                $pickupDao->updatePickupTemplate($args);
            }
            $db->commit();

            /* ---------- MEMCACHED ---------- */
            //1. Cập nhật cấu hình đón muộn
            updateSchoolData($school['page_id'], SCHOOL_LATE_PICKUP);
            /* ---------- END - MEMCACHED ---------- */

            return_json(array('success' => true, 'message' => __("Configure late pickup service successfully")));
            break;

        case 'assign':
            if (!$canEdit) {
                _error(403);
            }
            $args = array();
            $assign_type = $_POST['assign_type'];
            $args['school_id'] = $school['page_id'];
            $args['pickup_class_id'] = $_POST['pickup_class_assign'];
            $args['month'] = $_POST['month'];
            $args['date'] = $_POST['date'];
            $args['teacher'] = $_POST['teacher'];
            $args['assign_type'] = $_POST['assign_type'];

            $pickup_configured = $pickupDao->checkPickupConfiguration($args['school_id']);
            if (!$pickup_configured) {
                throw new Exception(__("The school has not establish the late pickup configuration"));
            }

            if ($assign_type == 'month') {
                $firstDate = toDBDate("01/".$args['month']);

                $db->begin_transaction();

                //1.Phân công giáo viên trong tuần
                //Lấy ra mảng các giáo viên đã được phân công trong tháng
                $assignedInMonth = $pickupDao->getAssignInMonthForNotification($args['school_id'], $firstDate);
                //Xóa phân công giáo viên đã phân công trong tháng
                $pickupDao->deletePickupAssignInMonth($args['school_id'], $args['pickup_class_id'], $firstDate);

                $assignInMonthCheck = array();
                //Phân công
                for ($idx = 0; $idx < count($args['teacher']); $idx++) {
                    $args['user_id'] = $args['teacher'][$idx];

                    $pickupTimes = isset($_POST['teacher_' . $args['user_id']]) ? $_POST['teacher_' . $args['user_id']] : null;
                    if (is_array($pickupTimes)) {
                        foreach ($pickupTimes as $pickupTime) {
                            $args['pickup_time'] = $pickupTime ;
                            $args['pickup_day'] = $pickupDao->getDayOfCurrentDate($args['pickup_time']);

                            $arr = array(
                                $args['user_id'],
                                $args['pickup_time'],
                                $args['pickup_class_id']
                            );
                            /*if (in_array($arr, $assignInMonthCheck)) {
                                throw new Exception(sprintf(__("Teacher is only assigned to a late pickup class, check on %s"), toSysDate($args['pickup_time'])));
                            }
                            $assignInMonthCheck[] = $arr;
                            */

                            $args['pickup_id'] = $pickupDao->getPickupId($args['school_id'], $args['pickup_time'], $args['pickup_class_id']);
                            if ($args['pickup_id'] == 0) {
                                $args['pickup_id'] = $pickupDao->insertPickup($args);
                            }
                            $pickupDao->assignPickup($args);

                            //Gửi thông báo đến những giáo viên được phân công mới
                            if (!in_array($arr, $assignedInMonth)) {
                                $pickup_class = $pickupDao->getPickupClass($args['pickup_class_id']);
                                $class_name = isset($pickup_class['class_name']) ? convertText4Web($pickup_class['class_name']) : '';

                                $userDao->postNotifications($args['user_id'], NOTIFICATION_ASSIGN_LATEPICKUP_CLASS, NOTIFICATION_NODE_TYPE_CLASS,
                                    $args['pickup_id'], toSysDate($args['pickup_time']), $school['page_name'], $class_name);
                            }
                        }
                    }


                }
            } else {

                // Phân công theo tuần.
                $monday = date("Y-m-d", strtotime("monday this week", strtotime(toDBDate($args['date']))));

                $db->begin_transaction();

                //1.Phân công giáo viên trong tuần
                //Lấy ra mảng các giáo viên đã được phân công trong tuần
                $assignedInWeek = $pickupDao->getAssignInWeekForNotification($args['school_id'], $monday);
                //Xóa phân công giáo viên đã phân công trong tuần
                $pickupDao->deletePickupAssignInWeek($args['school_id'], $args['pickup_class_id'], $monday);

                $assignInWeekCheck = array();
                //Phân công
                for ($idx = 0; $idx < count($args['teacher']); $idx++) {
                    $args['user_id'] = $args['teacher'][$idx];

                    $pickupTimes = isset($_POST['teacher_' . $args['user_id']]) ? $_POST['teacher_' . $args['user_id']] : null;
                    if (is_array($pickupTimes)) {
                        foreach ($pickupTimes as $pickupTime) {
                            $args['pickup_time'] = $pickupTime;
                            $args['pickup_day'] = $pickupDao->getDayOfCurrentDate($args['pickup_time']);

                            $arr = array(
                                $args['user_id'],
                                $args['pickup_time'],
                                $args['pickup_class_id']
                            );
                            /*if (in_array($arr, $assignInWeekCheck)) {
                                throw new Exception(sprintf(__("Teacher is only assigned to a late pickup class, check on %s"), toSysDate($args['pickup_time'])));
                            }
                            $assignInWeekCheck[] = $arr;*/

                            $args['pickup_id'] = $pickupDao->getPickupId($args['school_id'], $args['pickup_time'], $args['pickup_class_id']);
                            if ($args['pickup_id'] == 0) {
                                $args['pickup_id'] = $pickupDao->insertPickup($args);
                            }
                            $pickupDao->assignPickup($args);

                            //Gửi thông báo đến những giáo viên được phân công mới
                            if (!in_array($arr, $assignedInWeek)) {
                                $pickup_class = $pickupDao->getPickupClass($args['pickup_class_id']);
                                $class_name = isset($pickup_class['class_name']) ? convertText4Web($pickup_class['class_name']) : '';

                                $userDao->postNotifications($args['user_id'], NOTIFICATION_ASSIGN_LATEPICKUP_CLASS, NOTIFICATION_NODE_TYPE_CLASS,
                                    $args['pickup_id'], toSysDate($args['pickup_time']), $school['page_name'], $class_name);
                            }
                        }
                    }
                }

                //2.Nếu bật lặp lịch phân công theo tuần, thì insert luôn phân công tuần sau
                $args['is_repeat'] = (isset($_POST['is_repeat']) && $_POST['is_repeat'] == 'on') ? 1 : 0;
                $args['beginning_repeat_date'] = ($args['is_repeat'] == 1) ? $monday : 'null';
                $pickupDao->updateRepeatInfo($args);
                if ($args['is_repeat']) {
                    $assigns = $pickupDao->getAssignInWeekForSchedule($args['school_id'], $args['beginning_repeat_date']);


                    //3. Phân công giáo viên tuần tới
                    $monday = date("Y-m-d", strtotime("monday next week", strtotime($args['beginning_repeat_date'])));
                    $args['date_2'] = $monday;
                    $args['date_3'] = date("Y-m-d", strtotime("+1 day", strtotime($monday)));
                    $args['date_4'] = date("Y-m-d", strtotime("+2 day", strtotime($monday)));
                    $args['date_5'] = date("Y-m-d", strtotime("+3 day", strtotime($monday)));
                    $args['date_6'] = date("Y-m-d", strtotime("+4 day", strtotime($monday)));
                    $args['date_7'] = date("Y-m-d", strtotime("+5 day", strtotime($monday)));
                    $args['date_8'] = date("Y-m-d", strtotime("+6 day", strtotime($monday)));

                    //Lấy ra mảng các giáo viên đã được phân công trong tuần (Tạm comment ko gửi thông báo)
                    $assignedInWeek = $pickupDao->getAssignInWeekForNotification($args['school_id'], $monday);
                    //Xóa phân công giáo viên đã phân công trông trong tương lai
                    $pickupDao->deletePickupAssign($args['school_id'], $args['date_2']);

                    ///$assignInWeekCheck = array();
                    //Phân công
                    foreach ($assigns as $assign) {
                        $args['user_id'] = $assign['user_id'];
                        $args['pickup_time'] = $args['date_' . $assign['assign_day']];
                        $args['pickup_day'] = $assign['assign_day'];
                        $args['pickup_class_id'] = $assign['pickup_class_id'];

                        $arr = array(
                            $args['user_id'],
                            $args['pickup_time'],
                            $args['pickup_class_id']
                        );
                        /*if (in_array($arr, $assignedInWeek)) {
                            throw new Exception(sprintf(__("Teacher is only assigned to a late pickup class, check on %s"), toSysDate($args['pickup_time'])));
                        }
                        $assignInWeekCheck[] = $arr;*/

                        $args['pickup_id'] = $pickupDao->getPickupId($args['school_id'], $args['pickup_time'], $args['pickup_class_id']);
                        if ($args['pickup_id'] == 0) {
                            $args['pickup_id'] = $pickupDao->insertPickup($args);
                        }
                        $pickupDao->assignPickup($args);

                        //Gửi thông báo đến những giáo viên được phân công mới
                        if (!in_array($arr, $assignedInWeek)) {
                            $pickup_class = $pickupDao->getPickupClass($args['pickup_class_id']);
                            $class_name = isset($pickup_class['class_name']) ? convertText4Web($pickup_class['class_name']) : '';

                            $userDao->postNotifications($args['user_id'], NOTIFICATION_ASSIGN_LATEPICKUP_CLASS, NOTIFICATION_NODE_TYPE_CLASS,
                                $args['pickup_id'], toSysDate($args['pickup_time']), $school['page_name'], $class_name);
                        }
                    }

                    //4.Update thứ 2 - tuần lặp lịch gần nhất vào ci_pickup_template
                    $args['is_repeat'] = 1;
                    $args['beginning_repeat_date'] = $monday;
                    $pickupDao->updateRepeatInfo($args);

                }
            }

            $pickupDao->updateAssignType($school['page_id'], $assign_type);

            $db->commit();

            /* ---------- MEMCACHED ---------- */
            //1. Cập nhật danh sách giáo viên đã được phân công
            $teacherIds = $pickupDao->getTeachersAssigned($school['page_id']);
            updateSchoolData($school['page_id'], SCHOOL_LATE_PICKUP, array('teachers' => $teacherIds));
            /* ---------- END - MEMCACHED ---------- */

            return_json(array('success' => true, 'message' => __("Assign teacher successfully")));
            break;

        case 'get_assign_by_month':

            $args = array();
            $args['school_id'] = $school['page_id'];
            $args['pickup_class_id'] = $_POST['pickup_class_assign'];
            $args['month'] = $_POST['month'];

            $fromDate = toDBDate("01/".$args['month']);
            $toDate = date("Y-m-t", strtotime($fromDate));

            $results = $pickupDao->getAssignHistory($school['page_id'], $args['pickup_class_id'], $fromDate, $toDate);

            $fDate = strtotime($fromDate);
            $tDate = strtotime($toDate);

            $displayDates = array();
            $displayDays = array();
            while ($fDate <= $tDate) {
                $day = $pickupDao->getDayShortOfDate($fDate);
                $displayDays[] = $day;

                $parts = explode("-", date("Y-m-d", $fDate));
                $displayDates[] = $parts[2];

                //$dates[] = $fromDate;
                $fDate = strtotime("+1 day", $fDate);
            }
            $smarty->assign('dates', $displayDates);
            $smarty->assign('days', $displayDays);

            //Mảng 2 chiều lưu bảng phân công toàn trường
            $newResults = array();

            //Băm danh sách kết quả thành mảng 2 chiều theo từng giáo viên
            $lastTeacherId = -1;
            $aRow = array(); //Dòng lưu thông tin phân công của 1 giáo viên (chỉ những ngày đc phân công).

            $assignTime = array();
            foreach ($results as $result) {
                //Nếu user_id thay đổi thì bắt đầu một dòng mới.
                if ($lastTeacherId != $result['user_id']) {

                    //Nếu trước đó là một dòng (không phải là dòng đầu tiên), thì xử lý bổ xung dòng đã xong trước đó.
                    if ($lastTeacherId > 0) {
                        $isFirstCell = true; //Check có phải cell đầu dòng hay không
                        $newRow = array(); //Dòng lưu thông tin phân công 1 giáo viên (bao gồm những ngày không phân công).
                        $newCells = array();
                        $lastDate = strtotime($fromDate);
                        $dateRow = $fromDate;

                        foreach ($aRow as $cell) {
                            if (is_empty($cell['assign_time'])) {
                                $newCells[] = array("is_checked" => 0, 'date_row' => date('Y-m-d', strtotime($dateRow)) );
                                $dateRow = date('Y-m-d', strtotime("+1 day", strtotime($dateRow)));
                            }
                            else {
                                //Xét ngày đầu hàng là ngày ko được phân công thì thêm thành phần vào
                                while ($isFirstCell && $lastDate < strtotime($cell['assign_time']) ) {
                                    $newCells[] = array("is_checked" => 0, 'date_row' => date('Y-m-d', strtotime($dateRow)) );
                                    $dateRow = date('Y-m-d', strtotime("+1 day", strtotime($dateRow)));

                                    $lastDate = strtotime("+1 day", $lastDate);
                                }

                                //Xét thành phần giữa hàng, nếu có ngày không được phân công thì chèn thêm vào
                                while ((!$isFirstCell) && strtotime("+1 day", $lastDate) < strtotime($cell['assign_time'])) {
                                    $newCells[] = array("is_checked" => 0, 'date_row' => date('Y-m-d', strtotime($dateRow)) );
                                    $dateRow = date('Y-m-d', strtotime("+1 day", strtotime($dateRow)));

                                    $lastDate = strtotime("+1 day", $lastDate);
                                }
                                $lastDate = strtotime($cell['assign_time']);
                            }


                            if ($cell['pickup_id']) {
                                $newCells[] = array("is_checked" => 1, 'date_row' => date('Y-m-d', strtotime($dateRow)) );
                                $dateRow = date('Y-m-d', strtotime("+1 day", strtotime($dateRow)));
                            }
                            $newRow['teacher_name'] = $cell['user_fullname'];
                            $newRow['user_id'] = $cell['user_id'];
                            $isFirstCell = false;

                        }
                        //Xét cuối hàng, nếu những ngày cuối hàng là ngày nghỉ thì thêm ô trống
                        while ($lastDate < strtotime($toDate)) {
                            $newCells[] = array("is_checked" => 0, 'date_row' => date('Y-m-d', strtotime($dateRow)) );
                            $dateRow = date('Y-m-d', strtotime("+1 day", strtotime($dateRow)));
                            $lastDate = strtotime("+1 day", $lastDate);
                        }
                        $newRow['cells'] = $newCells;
                        $newResults[] = $newRow;
                    }
                    $aRow = array();
                }

                $assignTime[strtotime($result['assign_time'])][] = $result;
                $aRow[] = $result;
                $lastTeacherId = $result['user_id'];
            }

            //Xử lý hàng cuối cùng (giáo viên cuối cùng)
            if ($lastTeacherId > 0) {
                $isFirstCell = true;
                $newRow = array(); //Dòng lưu thông phần công của giáo viên (bao gồm những ngày không phân công).
                $newCells = array();
                $lastDate = strtotime($fromDate);
                $dateRow = $fromDate;

                foreach ($aRow as $cell) {
                    if (is_empty($cell['assign_time'])) {
                        $newCells[] = array("is_checked" => 0, 'date_row' => date('Y-m-d', strtotime($dateRow)) );
                        $dateRow = date('Y-m-d', strtotime("+1 day", strtotime($dateRow)));
                    }
                    else {
                        //Xét ngày đầu hàng là ngày ko được phân công thì thêm thành phần vào
                        while ($isFirstCell && $lastDate < strtotime($cell['assign_time']) ) {
                            $newCells[] = array("is_checked" => 0, 'date_row' => date('Y-m-d', strtotime($dateRow)) );
                            $dateRow = date('Y-m-d', strtotime("+1 day", strtotime($dateRow)));

                            $lastDate = strtotime("+1 day", $lastDate);
                        }

                        //Xét thành phần giữa hàng, nếu có ngày không được phân công thì chèn thêm vào
                        while ((!$isFirstCell) && strtotime("+1 day", $lastDate) < strtotime($cell['assign_time'])) {
                            $newCells[] = array("is_checked" => 0, 'date_row' => date('Y-m-d', strtotime($dateRow)) );
                            $dateRow = date('Y-m-d', strtotime("+1 day", strtotime($dateRow)));

                            $lastDate = strtotime("+1 day", $lastDate);
                        }
                        $lastDate = strtotime($cell['assign_time']);
                    }


                    if ($cell['pickup_id']) {
                        $newCells[] = array("is_checked" => 1, 'date_row' => date('Y-m-d', strtotime($dateRow)) );
                        $dateRow = date('Y-m-d', strtotime("+1 day", strtotime($dateRow)));
                    }
                    $newRow['teacher_name'] = $cell['user_fullname'];
                    $newRow['user_id'] = $cell['user_id'];
                    $isFirstCell = false;
                }

                //Xét cuối hàng, nếu những ngày cuối hàng là ngày nghỉ thì thêm ô trống
                while ($lastDate < strtotime($toDate)) {
                    $newCells[] = array("is_checked" => 0, 'date_row' => date('Y-m-d', strtotime($dateRow)) );
                    $dateRow = date('Y-m-d', strtotime("+1 day", strtotime($dateRow)));

                    $lastDate = strtotime("+1 day", $lastDate);
                }
                $newRow['cells'] = $newCells;
                $newResults[] = $newRow;
            }

            /*echo '<pre>';
            print_r($newResults);
            echo '<pre>';die;*/

            $twoLastRows = array(); //Biến này lưu tổng kết số lượng phân công theo ngày của trường.

            $timeFromDate = strtotime($fromDate);
            $timeToDate = strtotime($toDate);
            for ($i = $timeFromDate; $i <= $timeToDate; $i = strtotime("+1 day", $i)) {
                if (isset($assignTime[$i])) {
                    $twoLastRows[] = array("is_checked" => 1, 'pickup_count' => count($assignTime[$i]));
                } else {
                    $twoLastRows[] = array("is_checked" => 0);
                }
            }

            $smarty->assign('rows', $newResults);
            $smarty->assign('last_rows', $twoLastRows);

            $return['results'] = $smarty->fetch("ci/school/ajax.school.pickup.assign.tpl");
            $return['disableSave'] = false;

            return_json($return);
            break;

        case 'get_assign_by_date':

            $args = array();
            $args['school_id'] = $school['page_id'];
            $args['pickup_class_id'] = $_POST['pickup_class_assign'];
            $args['date'] = toDBDate($_POST['date']);

            $fromDate = date("Y-m-d", strtotime("monday this week", strtotime($args['date'])));
            $toDate = date("Y-m-d", strtotime("+6 day", strtotime($fromDate)));

            $results = $pickupDao->getAssignHistory($school['page_id'], $args['pickup_class_id'], $fromDate, $toDate);

            $fDate = strtotime($fromDate);
            $tDate = strtotime($toDate);

            $displayDates = array();
            $displayDays = array();
            while ($fDate <= $tDate) {
                $day = $pickupDao->getDayShortOfDate($fDate);
                $displayDays[] = $day;

                $parts = explode("-", date("Y-m-d", $fDate));
                $displayDates[] = $parts[2];

                //$dates[] = $fromDate;
                $fDate = strtotime("+1 day", $fDate);
            }
            $smarty->assign('dates', $displayDates);
            $smarty->assign('days', $displayDays);

            //Mảng 2 chiều lưu bảng phân công toàn trường
            $newResults = array();

            //Băm danh sách kết quả thành mảng 2 chiều theo từng giáo viên
            $lastTeacherId = -1;
            $aRow = array(); //Dòng lưu thông tin phân công của 1 giáo viên (chỉ những ngày đc phân công).

            $assignTime = array();
            foreach ($results as $result) {
                //Nếu user_id thay đổi thì bắt đầu một dòng mới.
                if ($lastTeacherId != $result['user_id']) {

                    //Nếu trước đó là một dòng (không phải là dòng đầu tiên), thì xử lý bổ xung dòng đã xong trước đó.
                    if ($lastTeacherId > 0) {
                        $isFirstCell = true; //Check có phải cell đầu dòng hay không
                        $newRow = array(); //Dòng lưu thông tin phân công 1 giáo viên (bao gồm những ngày không phân công).
                        $newCells = array();
                        $lastDate = strtotime($fromDate);
                        $dateRow = $fromDate;

                        foreach ($aRow as $cell) {
                            if (is_empty($cell['assign_time'])) {
                                $newCells[] = array("is_checked" => 0, 'date_row' => date('Y-m-d', strtotime($dateRow)) );
                                $dateRow = date('Y-m-d', strtotime("+1 day", strtotime($dateRow)));
                            }
                            else {
                                //Xét ngày đầu hàng là ngày ko được phân công thì thêm thành phần vào
                                while ($isFirstCell && $lastDate < strtotime($cell['assign_time']) ) {
                                    $newCells[] = array("is_checked" => 0, 'date_row' => date('Y-m-d', strtotime($dateRow)) );
                                    $dateRow = date('Y-m-d', strtotime("+1 day", strtotime($dateRow)));

                                    $lastDate = strtotime("+1 day", $lastDate);
                                }

                                //Xét thành phần giữa hàng, nếu có ngày không được phân công thì chèn thêm vào
                                while ((!$isFirstCell) && strtotime("+1 day", $lastDate) < strtotime($cell['assign_time'])) {
                                    $newCells[] = array("is_checked" => 0, 'date_row' => date('Y-m-d', strtotime($dateRow)) );
                                    $dateRow = date('Y-m-d', strtotime("+1 day", strtotime($dateRow)));

                                    $lastDate = strtotime("+1 day", $lastDate);
                                }
                                $lastDate = strtotime($cell['assign_time']);
                            }


                            if ($cell['pickup_id']) {
                                $newCells[] = array("is_checked" => 1, 'date_row' => date('Y-m-d', strtotime($dateRow)) );
                                $dateRow = date('Y-m-d', strtotime("+1 day", strtotime($dateRow)));
                            }
                            $newRow['teacher_name'] = $cell['user_fullname'];
                            $newRow['user_id'] = $cell['user_id'];
                            $isFirstCell = false;

                        }
                        //Xét cuối hàng, nếu những ngày cuối hàng là ngày nghỉ thì thêm ô trống
                        while ($lastDate < strtotime($toDate)) {
                            $newCells[] = array("is_checked" => 0, 'date_row' => date('Y-m-d', strtotime($dateRow)) );
                            $dateRow = date('Y-m-d', strtotime("+1 day", strtotime($dateRow)));

                            $lastDate = strtotime("+1 day", $lastDate);
                        }
                        $newRow['cells'] = $newCells;
                        $newResults[] = $newRow;
                    }
                    $aRow = array();
                }

                $assignTime[strtotime($result['assign_time'])][] = $result;
                $aRow[] = $result;
                $lastTeacherId = $result['user_id'];
            }

            //Xử lý hàng cuối cùng (giáo viên cuối cùng)
            if ($lastTeacherId > 0) {
                $isFirstCell = true;
                $newRow = array(); //Dòng lưu thông phần công của giáo viên (bao gồm những ngày không phân công).
                $newCells = array();
                $lastDate = strtotime($fromDate);
                $dateRow = $fromDate;

                foreach ($aRow as $cell) {
                    if (is_empty($cell['assign_time'])) {
                        $newCells[] = array("is_checked" => 0, 'date_row' => date('Y-m-d', strtotime($dateRow)) );
                        $dateRow = date('Y-m-d', strtotime("+1 day", strtotime($dateRow)));
                    }
                    else {
                        //Xét ngày đầu hàng là ngày ko được phân công thì thêm thành phần vào
                        while ($isFirstCell && $lastDate < strtotime($cell['assign_time']) ) {
                            $newCells[] = array("is_checked" => 0, 'date_row' => date('Y-m-d', strtotime($dateRow)) );
                            $dateRow = date('Y-m-d', strtotime("+1 day", strtotime($dateRow)));

                            $lastDate = strtotime("+1 day", $lastDate);
                        }

                        //Xét thành phần giữa hàng, nếu có ngày không được phân công thì chèn thêm vào
                        while ((!$isFirstCell) && strtotime("+1 day", $lastDate) < strtotime($cell['assign_time'])) {
                            $newCells[] = array("is_checked" => 0, 'date_row' => date('Y-m-d', strtotime($dateRow)) );
                            $dateRow = date('Y-m-d', strtotime("+1 day", strtotime($dateRow)));

                            $lastDate = strtotime("+1 day", $lastDate);
                        }
                        $lastDate = strtotime($cell['assign_time']);
                    }


                    if ($cell['pickup_id']) {
                        $newCells[] = array("is_checked" => 1, 'date_row' => date('Y-m-d', strtotime($dateRow)) );
                        $dateRow = date('Y-m-d', strtotime("+1 day", strtotime($dateRow)));
                    }
                    $newRow['teacher_name'] = $cell['user_fullname'];
                    $newRow['user_id'] = $cell['user_id'];
                    $isFirstCell = false;
                }

                //Xét cuối hàng, nếu những ngày cuối hàng là ngày nghỉ thì thêm ô trống
                while ($lastDate < strtotime($toDate)) {
                    $newCells[] = array("is_checked" => 0, 'date_row' => date('Y-m-d', strtotime($dateRow)) );
                    $dateRow = date('Y-m-d', strtotime("+1 day", strtotime($dateRow)));

                    $lastDate = strtotime("+1 day", $lastDate);
                }
                $newRow['cells'] = $newCells;
                $newResults[] = $newRow;
            }

            /*echo '<pre>';
            print_r($newResults);
            echo '<pre>';die;*/

            $twoLastRows = array(); //Biến này lưu tổng kết số lượng phân công theo ngày của trường.

            $timeFromDate = strtotime($fromDate);
            $timeToDate = strtotime($toDate);
            for ($i = $timeFromDate; $i <= $timeToDate; $i = strtotime("+1 day", $i)) {
                if (isset($assignTime[$i])) {
                    $twoLastRows[] = array("is_checked" => 1, 'pickup_count' => count($assignTime[$i]));
                } else {
                    $twoLastRows[] = array("is_checked" => 0);
                }
            }

            $smarty->assign('rows', $newResults);
            $smarty->assign('last_rows', $twoLastRows);

            $return['results'] = $smarty->fetch("ci/school/ajax.school.pickup.assign.tpl");
            $return['disableSave'] = false;

            return_json($return);
            break;


        case 'save':
            if (!$canEdit) {
                _error(403);
            }

            //Xử lý nghiệp vụ khi quản lý trường lưu thông tin lớp đón muộn
            if (isset($_POST['pickup_date']) && validateDate($_POST['pickup_date'])) {
                $args = array();
                $args['pickup_date'] = toDBDate($_POST['pickup_date']);
                $args['using_at'] = $args['pickup_date'];
                $args['school_id'] = $school['page_id'];

                if (!is_numeric($_POST['pickup_id'])) {
                    throw new Exception(__("Late pickup class information is incorrect, please check again"));
                }
                $args['pickup_id'] = $_POST['pickup_id'];
                // Quản lý trường ghi sử dụng dịch vụ type = 1
                $args['type'] = 1;
                $args['total'] = convertMoneyToNumber($_POST['total']);

                $db->begin_transaction();
                $childIds = $_POST['child'];
                for ($idx = 0; $idx < count($childIds); $idx++) {
                    $args['child_id'] = $childIds[$idx];
                    $args['status'] = is_empty($_POST['pickup_time'][$idx]) ? 0 : 1;
                    $args['pickup_time'] = $_POST['pickup_time'][$idx];
                    $args['pickup_fee'] = convertMoneyToNumber($_POST['pickup_fee'][$idx]);
                    $args['service_fee'] = convertMoneyToNumber($_POST['service_fee'][$idx]);
                    $args['total_child'] = convertMoneyToNumber($_POST['total_child'][$idx]);
                    $args['note'] = $_POST['pickup_note'][$idx];
                    // Update các thông tin chung của lần đón muộn (ci_pickup + ci_pickup_child)
                    $pickupDao->updatePickup($args);

                    /* Ghi sử dụng dịch vụ */
                    // Lấy dịch vụ sử dụng
                    $serviceIds = isset($_POST['service_' . $childIds[$idx]]) ? $_POST['service_' . $childIds[$idx]] : array();
                    // Lấy dịch vụ đã sử dụng
                    $serviceUsedIds = $pickupDao->getIdServiceUsage($args['pickup_id'], $args['child_id']);
                    //$serviceUsedIds = isset($_POST['service_used_' . $childIds[$idx]]) ? $_POST['service_used_' . $childIds[$idx]] : array();
                    // Lấy ra dịch vụ mới đăng ký
                    $args['recordServiceIds'] = array_diff($serviceIds, $serviceUsedIds);
                    $args['deleteServiceIds'] = array_diff($serviceUsedIds, $serviceIds);
                    // Ghi sử dụng dịch vụ mới
                    if (count($args['recordServiceIds']) > 0) {
                        $pickupDao->recordPickupServiceOfMan($args);
                    }
                    // Xóa dịch vụ ghi nhầm
                    if (count($args['deleteServiceIds']) > 0) {
                        $pickupDao->deletePickupServiceOfMan($args);
                    }
                }

                $db->commit();
            } else {
                _error(404);
            }
            return_json(array('success' => true, 'message' => __("Save your late Pickup information successfully")));
            break;

        case 'pickup':
            if (!$canEdit) {
                _error(403);
            }

            $pickup = $pickupDao->getPickup($_POST['pickup_id']);
            if (is_null($pickup)) {
                throw new Exception(__("Thông tin lớp đón muộn không đúng"));
            }

            $totalChild = 0;
            //Xử lý nghiệp vụ khi trả trẻ
            $args = array();

            $args['using_at'] = $pickup['pickup_time'];
            $args['school_id'] = $school['page_id'];
            $args['pickup_id'] = $_POST['pickup_id'];
            $args['child_id'] = $_POST['child_id'];
            $args['services_fee'] = convertMoneyToNumber($_POST['services_fee']);
            $args['total_fee'] = convertMoneyToNumber($_POST['total_fee']);
            $serviceIdList = $_POST['serviceIdList'];
            $args['pickup_note'] = $_POST['note'];
            $args['recordServiceIds'] = $serviceIdList;
            $serviceFeeList = $_POST['serviceFeeList'];
            $args['type'] = 1;

            if(is_empty($_POST['time'])) {
                $args['pickup_at'] = date('H:i');
                $pickupFee = $pickupDao->getPickupFee($args['school_id'], $args['pickup_at']);
                $args['pickup_fee'] = $pickupFee;
            } else {
                $args['pickup_at'] = $_POST['time'];
                $args['pickup_fee'] = convertMoneyToNumber($_POST['pickup_fee']);
            }
            // Tổng tiền của trẻ
            $totalChild = $args['pickup_fee'] + $args['services_fee'];

            // Ghi sử dụng dịch vụ mới
            if (!empty($serviceIdList)) {
                $recordServicePrice = $pickupDao->recordPickupServiceUsage($args);
            }

            // Khởi tạo các giá trị truyền vào
            $args['late_pickup_fee'] = $args['pickup_fee'];
            $args['total_child'] = $totalChild;
            $total = $pickup['total'] + $args['total_child'];
            $args['total'] = ($total > 0) ? $total : 0;

            $db->begin_transaction();
            $pickupDao->recordPickupOfChild($args);

            // Thông báo cho phụ huynh khi trẻ được trả
            //$child = $childDao->getChild($args['child_id']);
            $child = getChildData($args['child_id'], CHILD_INFO);
            //$parentIds = array();
            //$parentIds = $parentDao->getParentIds($args['child_id']);
            $parents = getChildData($args['child_id'], CHILD_PARENTS);
            $parentIds = array_keys($parents);
            if(count($parentIds) > 0) {
                $userDao->postNotifications($parentIds, NOTIFICATION_CHILD_PICKEDUP, NOTIFICATION_NODE_TYPE_CHILD,
                    $args['pickup_id'], $args['pickup_at'], $args['child_id'], $child['child_name']);
            }
            $db->commit();

            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/school/' . $_POST['school_username'] . '/pickup/detail/' . $_POST['pickup_id'] . '";'));
            break;

        case 'get_pickup_fee':
            // Lấy ra phí trông muộn
            $school_id = $school['page_id'];
            $late_pickup_fee = $pickupDao->getPickupFee($school_id, $_POST['pickup_time']);

            $return['results'] = $late_pickup_fee;
            return_json($return);
            break;

        case 'list_child_edit':
            $class_id = $_POST['class_id'];
            $pickup_class_id = $_POST['pickup_class_id'];
            $date = toDBDate($_POST['add_to_date']);

            $can_add = true;
            $can_remove = true;
            $child_count = 0;
            $childrenRegistered = array();

            $pickupId = $pickupDao->getPickupId($school['page_id'], $date, $pickup_class_id);
            $children = $pickupDao->getChildOfClass($class_id, $pickupId, $pickup_class_id, $child_count, $date);
            $pickupIds = $pickupDao->getPickupIds($school['page_id'], $date);
            if (!is_null($pickupIds)) {
                $childrenRegistered = $pickupDao->getChildRegistered($class_id, $pickupIds, $pickup_class_id);
            }

            $disableSave = (count($children) == 0) ? true : false;

            $smarty->assign('child_count', $child_count);
            $smarty->assign('children', $children);
            $smarty->assign('childrenRegistered', $childrenRegistered);
            $smarty->assign('today', toSysDate($date));
            $smarty->assign('can_add', $can_add);
            $smarty->assign('can_remove', $can_remove);

            $return['results'] = $smarty->fetch("ci/class/ajax.pickupchild.tpl");
            $return['disableSave'] = $disableSave;
            return_json($return);
            break;

        case 'cancel':
            if (!$canEdit) {
                _error(403);
            }
            //Xử lý nghiệp vụ khi cô giáo hủy 1 trẻ khỏi lớp trông muộn
            $args = array();
            $pickup = $pickupDao->getPickup($_POST['pickup_id']);
            if (is_null($pickup)) {
                throw new Exception(__("Late pickup class information is incorrect, please check again"));
            }

            $args['school_id'] = $school['page_id'];
            $args['pickup_id'] = $_POST['pickup_id'];
            $args['child_id'] = $_POST['child_id'];
            $args['remove_child'][] = $_POST['child_id'];

            $args['deduction'] = $pickupDao->getTotalAmountPickupChild($args);
            $args['count_remove'] = 1;

            $db->begin_transaction();
            $pickupDao->deletePickupChild($args);

            /*// Thông báo cho phụ huynh khi trẻ bị hủy khỏi lớp trông muộn
            //$child = $childDao->getChild($args['child_id']);
            $child = getChildData($args['child_id'], CHILD_INFO);
            //$parentIds = array();
            //$parentIds = $parentDao->getParentIds($args['child_id']);
            $parents = getChildData($args['child_id'], CHILD_PARENTS);
            $parentIds = array_keys($parents);
            foreach ($parentIds as $parentId) {
                $userDao->postNotifications($parentId, NOTIFICATION_REMOVE_LATEPICKUP_CLASS, NOTIFICATION_NODE_TYPE_CHILD,
                    $args['pickup_id'], toSysDate($pickup['pickup_time']), $args['child_id'], convertText4Web($child['child_name']));
            }*/

            $db->commit();
            return_json(array('success' => true, 'message' => __("Add child to the late pickup class successfully")));
            break;

        case 'search_pickup':
            if (validateDate($_POST['fromDate']) && validateDate($_POST['toDate'])) {
                $fromDate = $_POST['fromDate'];
                $toDate = $_POST['toDate'];

                $pickups = $pickupDao->getPickupInMonth($school['page_id'], toDBDate($fromDate), toDBDate($toDate));

                $smarty->assign('username', $school['page_name']);
                $smarty->assign('pickups', $pickups);
                $smarty->assign('canEdit', $canEdit);
                $return['results'] = $smarty->fetch("ci/school/ajax.school.pickuplist.tpl");
                return_json($return);

            } else {
                throw new Exception(__("The time is incorrect, please check again"));
            }
            break;

        case 'add_child':
            if (!$canEdit) {
                _error(403);
            }
            //Xử lý nghiệp vụ khi thêm trẻ vào lớp trông muộn
            $template = $pickupDao->getTemplate($school['page_id']);
            if (is_null($template)) {
                throw new Exception(__("The school has not establish the late pickup configuration"));
            }

            $args = array();
            $args['pickup_time'] = toDBDate($_POST['date']);
            $args['pickup_day'] = $pickupDao->getDayOfCurrentDate($args['pickup_time']);

            $args['class_id'] = $_POST['class_id'];
            $args['pickup_class_id'] = $_POST['pickup_class_id'];
            $args['school_id'] = $school['page_id'];
            $args['status'] = 0; // Quản lý đăng ký type = 1
            $args['type'] = 1;

            //Lấy id của những trẻ thêm vào trông muộn
            $args['new_child'] = isset($_POST['childIds']) ? $_POST['childIds'] : array();
            $args['old_child'] = array();

            $db->begin_transaction();
            //Kiểm tra xem lớp đã có bản ghi điểm danh chưa
            $args['pickup_id'] = $pickupDao->getPickupId($school['page_id'], $args['pickup_time'], $args['pickup_class_id']);
            if ($args['pickup_id'] == 0) {
                $args['pickup_id'] = $pickupDao->insertPickup($args);
            } else {
                //Lấy id của những trẻ đã có trong lớp
                $args['old_child'] = $pickupDao->getChildIdOfPickup($args['pickup_id'], $args['class_id']);
                $args['remove_child'] = array_diff($args['old_child'], $args['new_child']);

                if (count($args['remove_child']) > 0) {
                    $args['deduction'] = $pickupDao->getTotalAmountPickupChild($args);
                    $args['count_remove'] = count($args['remove_child']);
                    $pickupDao->deletePickupChild($args);
                }
            }

            // Thêm trẻ vào lớp đón muộn
            $args['add_child'] = array_diff($args['new_child'], $args['old_child']);
            if (count($args['add_child']) > 0) {
                $pickupDao->insertPickupChild($args);
            }

            $db->commit();
            $_SESSION['pickup_time_edit'] = toDBDate($_POST['date']);
            return_json(array('success' => true, 'message' => __("Add child to the late pickup class successfully")));
            break;

        case 'edit':
            if (!$canEdit) {
                _error(403);
            }
            if (isset($_POST['pickup_time']) && validateDate($_POST['pickup_time'])) {
                $datePickup = toDBDate($_POST['pickup_time']);
                $pickupId = $pickupDao->getPickupId($school['page_id'], $datePickup, $_POST['pickup_class_id']);
                if ($pickupId > 0) {
                    return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/school/' . $_POST['school_username'] . '/pickup/detail/' . $pickupId . '";'));
                } else {
                    $_SESSION['pickup_time_edit'] = $datePickup;
                    return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/school/' . $_POST['school_username'] . '/pickup/add' . '";'));
                }
            } else {
                _error(404);
            }
            break;

        case 'get_pickup_child': //Hiển thị danh sách điểm danh một trẻ trong khoảng thời gian.
            if (!canView($_POST['school_username'], 'pickup')) {
                _error(403);
            }

            $data = array();
            $results = array();
            $data['fromDate'] = $_POST['fromDate'];
            $data['toDate'] = $_POST['toDate'];

            $child = getChildData($_POST['child_id'], CHILD_INFO);
            $data['child'] = $child;

            $total = 0;
            $results = $pickupDao->getChildPickupHistory($child['school_id'], $child['child_id'], toDBDate($_POST['fromDate']), toDBDate($_POST['toDate']), $total);
            /*echo '<pre>';
            print_r($results);
            echo '<pre>';die;*/

            $smarty->assign('results', $results);
            $smarty->assign('total', $total);
            $smarty->assign('data', $data);

            /* return */
            $return['results'] = $smarty->fetch("ci/school/ajax.child.pickup.tpl");

            return_json($return);
            break;

        case 'display_teacher_assign':
            if (!canView($_POST['school_username'], 'pickup')) {
                _error(403);
            }

            $teachers = $pickupDao->getPickupTeachers($school['page_id']);
            $smarty->assign('teachers', $teachers);
            $smarty->assign('username', $_POST['school_username']);

            $return['teachers_assign'] = $smarty->fetch("ci/school/ajax.school.pickup.teacher.popup.tpl");
            $return['callback'] = "$('#modal').modal('show'); $('.modal-content:last').html(response.teachers_assign);";
            return_json($return);

            break;

        case 'save_teacher_assign':
            if (!canEdit($_POST['school_username'], 'pickup')) {
                _error(403);
            }

            $args = array();
            $args['school_id'] = $school['page_id'];
            if (!empty($_POST['teacher'])) {
                $args['teacher'] = $_POST['teacher'];
                foreach ($args['teacher'] as $teacher_id) {
                    $args['teacher_fullname_' . $teacher_id] = $_POST['teacher_fullname_' . $teacher_id];
                    $args['teacher_name_' . $teacher_id] = $_POST['teacher_name_' . $teacher_id];
                }
            }

            $pickupDao->updatePickupTeacherGroup($args);
            $db->commit();

            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/school/' . $_POST['school_username'] . '/pickup/assign' . '";'));

            break;
        case 'list_child':
            if(!isset($_POST['class_id']) || !is_numeric($_POST['class_id'])) {
                _error(404);
            }

            $children = $childDao->getChildrenOfClass4ComboBoxNoLeave($_POST['class_id']);
            //$children = sortMultiOrderArray($children, [["name_for_sort", "ASC"]]);
            $results = "";
            $idx = 1;
            foreach ($children as $child) {
                $results = $results.'<option value="'.$child['child_id'].'">'.($idx++)." - ".$child['child_name'].' - '.$child['birthday'].'</option>';
                $child_status = $child['child_status'];
            }


            $return['results'] = $results;
            $return['disableSearch'] = (count($children) == 0);

            return_json($return);
            break;

        default:
            _error(400);
            break;
    }
} catch (Exception $e) {
    $db->rollback();
    return_json(array('error' => true, 'message' => $e->getMessage()));
} finally {
    $db->autocommit(true);
}

?>