<?php
/**
 * Package: ajax/ci/bo
 *
 * @package ConIu
 * @author Taila
 */
// fetch bootstrap
require('../../../../../bootstrap.php');

// check AJAX Request
is_ajax();
check_login();

if (is_empty($_POST['school_username']) || !valid_username($_POST['school_username'])) {
    _error(404);
}

include_once(DAO_PATH . 'dao_school.php');
include_once(DAO_PATH . 'dao_class.php');
include_once(DAO_PATH . 'dao_subject.php');
include_once(DAO_PATH . 'dao_user.php');
include_once(DAO_PATH . 'dao_point.php');
include_once(DAO_PATH . 'dao_child.php');
include_once(DAO_PATH . 'dao_conduct.php');

$schoolDao = new SchoolDAO();
$classDao = new ClassDAO();
$subjectDao = new SubjectDao();
$userDao = new UserDao();
$pointDao = new PointDao();
$childDao = new ChildDAO();
$conductDao = new ConductDAO();

//$school = getSchool($_POST['school_username']);
$school = getSchoolDataByUsername($_POST['school_username'], SCHOOL_INFO);
if (is_null($school)) {
    _error(403);
}
if (!canEdit($_POST['school_username'], 'points')) {
    _error(403);
}

try {
    $db->autocommit(false);
    switch ($_POST['do']) {
        case 'list_class':
            if (!isset($_POST['class_level_id']) || !is_numeric($_POST['class_level_id'])) {
                _error(404);
            }


            // Laays danh sách lớp học của 1 khối
            $classes = getClassLevelData($_POST['class_level_id'], CLASS_LEVEL_CLASSES);

            $results = "<option value=''>" . __("Select class") . "...</option>";
            $idx = 1;
            if (count($classes) > 0) {
                foreach ($classes as $class) {
                    $results = $results . '<option value="' . $class['group_id'] . '">' . $idx . " - " . $class['group_title'] . '</option>';
                    $idx++;
                }
            }
            $return['results'] = $results;

            return_json($return);
            break;
        case 'list_subject':
            if (!isset($_POST['class_id']) || !is_numeric($_POST['class_id'])) {
                _error(404);
            }

            if (!isset($_POST['class_level_id']) || !is_numeric($_POST['class_level_id'])) {
                _error(404);
            }
            $school_year = $_POST['school_year'];
            // Lấy chi tiết gov_class_level
            $class_level = getClassLevelData($_POST['class_level_id'], CLASS_LEVEL_INFO);
            // Lấy danh sách môn học của 1 khối
            $results = $pointDao->getClassLevelSubject($school['page_id'], $class_level['gov_class_level'], $school_year);

            // Lấy danh sách giáo viên tương ứng với từng môn
            $subjects = array();
            foreach ($results as $subject) {
                // Lấy giáo viên
                $subject['teacher_id'] = $pointDao->getSubjectTeacherId($school_year, $_POST['class_id'], $subject['subject_id'], $_POST['semester']);
                // Lấy tên môn học
                $subject_detail = $subjectDao->getSubjectById($subject['subject_id']);
                $subject['subject_name'] = $subject_detail['subject_name'];
                $subjects[] = $subject;
            }

            // Get school_config
            $schoolConfig = getSchoolData($school['page_id'], SCHOOL_CONFIG);
            // Lấy danh sách môn học mặc định

            // Lấy danh sách giáo viên của trường
            $teachers = getSchoolData($school['page_id'], SCHOOL_TEACHERS);
            $smarty->assign('results', $subjects);
            $smarty->assign('username', $_POST['school_username']);
            $smarty->assign('teachers', $teachers);
//            $smarty->assign('default_subjects', $default_subjects);
            $smarty->assign('grade', $schoolConfig['grade']);

            $return['results'] = $smarty->fetch("ci/school/ajax.school.point.subject.tpl");
            return_json($return);
            break;

        case 'list_subject_import':
            if (!isset($_POST['class_id']) || !is_numeric($_POST['class_id'])) {
                _error(404);
            }

            if (!isset($_POST['class_level_id']) || !is_numeric($_POST['class_level_id'])) {
                _error(404);
            }

            $school_year = $_POST['school_year'];
            /* ADD START - ManhDD 06/04/2021 */
            $search_with = $_POST['search_with'];
            /* ADD END - ManhDD 06/04/2021 */
            // Lấy chi tiết gov_class_level
            $class_level = getClassLevelData($_POST['class_level_id'], CLASS_LEVEL_INFO);
            // Nếu xem theo môn học thì lấy danh sách môn học của 1 khối
            if ($search_with == 'search_with_subject' || $search_with == 'importExcel' || $search_with == 'importManual') {
                $subjects = $pointDao->getClassLevelSubject($school['page_id'], $class_level['gov_class_level'], $school_year);
                $lastSubjects = array();
                // Lấy school_config
                $schoolConfig = getSchoolData($school['page_id'], SCHOOL_CONFIG);
                foreach ($subjects as $subject) {
                    // Lấy tên môn học
                    $subject_detail = $subjectDao->getSubjectById($subject['subject_id']);
                    $subject['subject_name'] = $subject_detail['subject_name'];
                    $lastSubjects[] = $subject;
                }
                $results = "<option value=''>" . __("Select subject") . "...</option>";
                $idx = 1;
                foreach ($lastSubjects as $subject) {
                    $results = $results . '<option value="' . $subject['subject_id'] . '">' . $idx . " - " . $subject['subject_name'] . '</option>';
                    $idx++;
                }
            } else {            // Nếu xem theo học sinh thì lấy danh sách học sinh của 1 lớp
                $students = $childDao->getChildrenOfClass($_POST['class_id']);
//                $lastStudents = array();
                // Lấy school_config
                $schoolConfig = getSchoolData($school['page_id'], SCHOOL_CONFIG);
//                foreach ($students as $student) {
//                    foreach ($default_subjects[$schoolConfig['grade']][$class_level['gov_class_level']] as $subject_id => $subject_name) {
//                        if($subject_id == $subject['subject_id']) {
//                            $subject['subject_name'] = $subject_name;
//                        }
//                    }
//                    $lastSubjects[] = $subject;
//                }
//              echo "<pre>";
//              print_r($lastSubjects); die('xxx');
                $results = "<option value=''>" . __("Select student") . "...</option>";
                $idx = 1;
                foreach ($students as $student) {
                    $results = $results . '<option value="' . $student['child_code'] . '">' . $idx . " - " . $student['last_name'] . " " . $student['first_name'] . '</option>';
                    $idx++;
                }
            }

            $return['results'] = $results;

            return_json($return);
            break;

        case 'assign':
            if (!isset($_POST['class_id']) || !is_numeric($_POST['class_id'])) {
                _error(404);
            }
            $db->begin_transaction();

            $semester = $_POST['semester'];
            $subject_ids = $_POST['subject_ids'];
            $teacher_ids = $_POST['teacher_ids'];
            $school_year = $_POST['school_year'];

            //Xóa dữ liệu phân công giáo viên trước đó
//            $pointDao->deleteClassSubjectTeacher($_POST['class_id'], $se)
            // Insert vào bảng ci_class_subject_teacher
            $pointDao->createClassSubjectTeacher($_POST['class_id'], $semester, $subject_ids, $teacher_ids, $school_year);

            $db->commit();

            //return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/school/' . $_POST['school_username'] . '/reports";'));
            return_json(array('success' => true, 'message' => __("Success")));
            break;
        case 'showDataImport' :
            if (!canEdit($_POST['school_username'], 'points')) {
                _error(403);
            }

            //Lấy ra class_id của lớp
            $class_id = $_POST['class_id'];
            $school_year = $_POST['school_year'];

            $subject_id = $_POST['subject_id'];
            $semester = $_POST['semester'];
            $is_last_semester = $_POST['is_last_semester'];

            // Lấy school_config
            $schoolConfig = getSchoolData($school['page_id'], SCHOOL_CONFIG);
            // Lấy chi tiết gov_class_level
            $class_level = getClassLevelData($_POST['class_level_id'], CLASS_LEVEL_INFO);
            // Lấy danh sách điểm của trẻ trong năm học
            $children_point = $pointDao->getChildrenPointsBySubjectId($class_id, $school_year, $subject_id);
            $countColumnMaxPoint_hk1 = $countColumnMaxPoint_hk2 = 2;
            $countColumnMaxPoint_gk1 = $countColumnMaxPoint_gk2 = 1;

            $children_point_last = array();
            if (count($children_point) > 0) {
                foreach ($children_point as $child) {
                    // Lấy child_name
                    $child_detail = $childDao->getChildByCode($child['child_code']);
                    $child_id = $childDao->getChildIdByCode($child['child_code']);
                    // Lấy số buổi nghỉ có phép và không phép của học sinh
                    $child_absent = $childDao->getAbsentsChild($child_id, $class_id);
                    // lấy tên môn học và đánh dấu là môn thi lại
                    $subject_detail = $subjectDao->getSubjectById($child['subject_id']);
                    //đếm số cột các điểm đã có
                    if ($semester != 0) {
                        for ($i = 1; $i <= 6; $i++) {
                            // điểm hệ số 1
                            if (isset($child['hs' . $semester . $i])) {
                                // lưu số cột đã có điểm của hệ số 1
                                if ($i > $countColumnMaxPoint_hk1) {
                                    $countColumnMaxPoint_hk1 = $i;
                                }
                            }
                            // điểm hệ số 2
                            if (isset($child['gk' . $semester . $i])) {
                                // lưu số cột đã có điểm
                                $countColumnMaxPoint_gk1 = $i;
                            }
                        }
                    } else {
                        for ($i = 1; $i <= 6; $i++) {
                            // điểm hệ số 1
                            if (isset($child['hs1' . $i])) {
                                // lưu số cột đã có điểm của hệ số 1
                                if ($i > $countColumnMaxPoint_hk1) {
                                    $countColumnMaxPoint_hk1 = $i;
                                }
                            }
                            if (isset($child['hs2' . $i])) {
                                // lưu số cột đã có điểm của hệ số 1
                                if ($i > $countColumnMaxPoint_hk2) {
                                    $countColumnMaxPoint_hk2 = $i;
                                }
                            }
                            // điểm hệ số 2
                            if (isset($child['gk1' . $i])) {
                                // lưu số cột đã có điểm
                                $countColumnMaxPoint_gk1 = $i;
                            }
                            if (isset($child['gk2' . $i])) {
                                // lưu số cột đã có điểm
                                $countColumnMaxPoint_gk2 = $i;
                            }
                        }
                    }
                    if ($schoolConfig['score_fomula'] == 'vn') {
                        $child['is_reexam'] = 1;
                    } else {
                        $child['is_reexam'] = $subject_detail['re_exam'];
                    }
                    $child['child_firstname'] = $child_detail['first_name'];
                    $child['child_lastname'] = $child_detail['last_name'];
                    $child['child_id'] = $child_id;

                    $children_point_last[] = $child;
                }
            }
            if ($schoolConfig['score_fomula'] != 'vn') {
                $countColumnMaxPoint_hk1 = 3;
                $countColumnMaxPoint_gk1 = 1;
                $countColumnMaxPoint_hk2 = 3;
                $countColumnMaxPoint_gk2 = 1;
            }
            $subject_key = array();
            if ($semester == 0) {
                for ($i = 1; $i <= $countColumnMaxPoint_hk1; $i++) {
                    $subject_key[] = 'hs1' . $i;
                }
                for ($i = 1; $i <= $countColumnMaxPoint_gk1; $i++) {
                    $subject_key[] = 'gk1' . $i;
                }
                if ($schoolConfig['score_fomula'] == 'vn') {
                    $subject_key[] = 'ck1';
                }
                for ($i = 1; $i <= $countColumnMaxPoint_hk2; $i++) {
                    $subject_key[] = 'hs2' . $i;
                }
                for ($i = 1; $i <= $countColumnMaxPoint_gk2; $i++) {
                    $subject_key[] = 'gk2' . $i;
                }
                if ($schoolConfig['score_fomula'] == 'vn') {
                    $subject_key[] = 'ck2';
                }
                $subject_key[] = 're_exam';
            } else {
                for ($i = 1; $i <= $countColumnMaxPoint_hk1; $i++) {
                    $subject_key[] = 'hs' . $semester . $i;
                }
                for ($i = 1; $i <= $countColumnMaxPoint_gk1; $i++) {
                    $subject_key[] = 'gk' . $semester . $i;
                }
                if ($schoolConfig['score_fomula'] == 'vn') {
                    $subject_key[] = 'ck' . $semester;
                }
            }
            $smarty->assign("column_hk1", $countColumnMaxPoint_hk1);
            $smarty->assign("column_gk1", $countColumnMaxPoint_gk1);
            $smarty->assign("column_hk2", $countColumnMaxPoint_hk2);
            $smarty->assign("column_gk2", $countColumnMaxPoint_gk2);
            $smarty->assign("score_fomula", $schoolConfig['score_fomula']);
            $smarty->assign("rows", $children_point_last);
            $smarty->assign("semester", $semester);
            $smarty->assign("grade", $schoolConfig['grade']);
            $smarty->assign("subject_key", $subject_key);
            $smarty->assign("username", $school['page_name']);
            $smarty->assign("search_with", $_POST['do']);
            $results['results'] = $smarty->fetch("ci/school/ajax.points.searchresult.tpl");
            return_json($results);
            break;
        case 'importManual':
            if (!canEdit($_POST['school_username'], 'points')) {
                _error(403);
            }
            $results = array();
            $newChildIds = array();
            $new_child_list = array();
            $sheet = array();
            $sheet['error'] = 0;

            // Lấy school_config
            $schoolConfig = getSchoolData($school['page_id'], SCHOOL_CONFIG);
            //Lấy ra thông số từ form gửi lên
            $class_id = $_POST['class_id'];
            $subject_id = $_POST['subject_id'];
            $semester = $_POST['semester'];
            $is_last_semester = $_POST['is_last_semester'];
            $school_year = $_POST['school_year'];
            $childs_points = $_POST['childs_points'];

            // Lấy danh sách điểm của trẻ trong năm học
            $children_point = $pointDao->getChildrenPointsBySubjectId($class_id, $school_year, $subject_id);

            foreach ($childs_points as $child) { // Duyệt danh sách trẻ
                $child['error'] = 0;
                // Kiểm tra xem trẻ có tồn tại trong lớp không
                $is_child = $childDao->checkExistChildByCodeAndClassId($child['child_code'], $class_id);
                if (!$is_child) {
                    $child['error'] = 1;
                    $child['message'] = __("Student not in class");
                    $new_child_list[] = $child;
                } else {
                    if ($child['error'] == 0) {
                        // Kiểm tra xem trẻ đã được tạo bản ghi điểm trước đó chưa
                        $is_create_point = $pointDao->checkCreatedPoint($subject_id, $class_id, $child['child_code'], $school_year);

                        if ($is_create_point) {
                            // Kiểm tra xem điểm gửi lên có khác gì điểm trước đó không, nếu khác thì update, không thì thôi (tạm thời bỏ qua bước này, update luôn
                            $db->autocommit(false);
                            $db->begin_transaction();
                            $pointDao->updateChildPoint($class_id, $subject_id, $school_year, $child);
                            $db->commit();
                            $child['message'] = __("Update point success");
                            $child['child_fullname'] = $child['child_lastname'] . ' ' . $child['child_firstname'];
                            //thông báo về cho phụ huynh
                            $childId = $childDao->getChildIdByCode($child['child_code']);
                            $child['child_id'] = $childId;
                            $parents = getChildData($child['child_id'], CHILD_PARENTS);
                            if (!is_null($parents)) {
                                $parentIds = array_keys($parents);

                                $subject_detail = $subjectDao->getSubjectById($subject_id);
                                $userDao->postNotifications($parentIds, NOTIFICATION_NEW_POINT, NOTIFICATION_NODE_TYPE_CHILD,
                                    $user->_data['user_id'], convertText4Web($subject_detail['subject_name']), $child['child_id'], convertText4Web($child['child_fullname']));
                                // đồng thời thông báo cho học sinh
                                // lấy user của học sinh theo child id
                                $userOfChild = $childDao->getUserOfChildByChildId($child['child_id']);
                                $userDao->postNotifications($userOfChild['user_id'], NOTIFICATION_NEW_POINT, NOTIFICATION_NODE_TYPE_CHILD,
                                    $user->_data['user_id'], convertText4Web($subject_detail['subject_name']), $child['child_id'], convertText4Web($child['child_fullname']));

                            }
                            $new_child_list[] = $child;
                        } else {
                            // Insert bản ghi điểm mới
                            try {
                                $new_child_list[] = $child;
                                $db->autocommit(false);
                                $db->begin_transaction();

                                $pointDao->createChildPoint($class_id, $subject_id, $school_year, $child);
                                $db->commit();
                            } catch (Exception $e) {
                                $db->rollback();
                                $child['error'] = 1;
                                $child['message'] = $e->getMessage();
                            } finally {
                                $db->autocommit(true);
                            }
                        }
                    }
                }
            }


            $sheet['child_list'] = $new_child_list;
            //echo "<pre>"; print_r($sheet); die('vvv');
            $smarty->assign("sheet", $sheet);
            $results['results'] = $smarty->fetch("ci/school/ajax.points.importresult.tpl");
            return_json($results);
            break;
        case 'importExcel':
            if (!canEdit($_POST['school_username'], 'points')) {
                _error(403);
            }

            include_once(ABSPATH . 'includes/libs/PHPExcel/PHPExcel/IOFactory.php');
            include_once(ABSPATH . 'includes/ajax/ci/utilities.php');

            $file_name = $_FILES['file']['name'];
            $inputFileName = $_FILES['file']['tmp_name'];
            $results = array();
            $newChildIds = array();
            $new_child_list = array();

            // Lấy school_config
            $schoolConfig = getSchoolData($school['page_id'], SCHOOL_CONFIG);
            //UPDATE START MANHDD 15/06/2021
//            if ($schoolConfig['grade'] == 1) {
//                $sheet = readPointsInExcelFileC1($inputFileName);
//            } elseif ($schoolConfig['grade'] == 2) {
//                if ($_POST['semester'] == 0) {
//                    $sheet = readPointsInExcelFileC2All($inputFileName);
//                } else {
//                    $sheet = readPointsInExcelFileC2($inputFileName);
//                }
//            }
            if ($schoolConfig['points']) {
                $sheet = readPointsInExcelFile($inputFileName);
                if ($schoolConfig['score_fomula'] == "vn") {

                } else {

                }
            }

            //UPDATE END MANHDD 15/06/2021
            //Lấy ra thông số từ form gửi lên
            $class_id = $_POST['class_id'];
            $subject_id = $_POST['subject_id'];
            $semester = $_POST['semester'];
            $is_last_semester = $_POST['is_last_semester'];
            $school_year = $_POST['school_year'];

            // Lấy danh sách điểm của trẻ trong năm học
            $children_point = $pointDao->getChildrenPointsBySubjectId($class_id, $school_year, $subject_id);

            $children_codes = array();
            if (count($children_point) > 0) {
                foreach ($children_point as $child) {
                    $children_codes[] = $child['child_code'];
                }
            }

            if ($sheet['error'] == 0) {
                foreach ($sheet['child_list'] as $child) { // Duyệt danh sách trẻ
                    // Kiểm tra xem trẻ có tồn tại trong lớp không
                    $is_child = $childDao->checkExistChildByCodeAndClassId($child['child_code'], $class_id);
                    if (!$is_child) {
                        $child['error'] = 1;
                        $child['message'] = __("Student not in class");
                        $new_child_list[] = $child;
                    } else {
                        if ($child['error'] == 0) {
                            // Kiểm tra xem trẻ đã được tạo bản ghi điểm trước đó chưa
                            $is_create_point = $pointDao->checkCreatedPoint($subject_id, $class_id, $child['child_code'], $school_year);

                            if ($is_create_point) {
                                // Kiểm tra xem điểm gửi lên có khác gì điểm trước đó không, nếu khác thì update, không thì thôi (tạm thời bỏ qua bước này, update luôn
                                $db->autocommit(false);
                                $db->begin_transaction();
                                $pointDao->updateChildPoint($class_id, $subject_id, $school_year, $child);
                                $db->commit();
                                $child['message'] = __("Update point success");
                                $new_child_list[] = $child;
                            } else {
                                // Insert bản ghi điểm mới
                                try {
                                    $new_child_list[] = $child;
                                    $db->autocommit(false);
                                    $db->begin_transaction();

                                    $pointDao->createChildPoint($class_id, $subject_id, $school_year, $child);
                                    $db->commit();
                                } catch (Exception $e) {
                                    $db->rollback();
                                    $child['error'] = 1;
                                    $child['message'] = $e->getMessage();
                                } finally {
                                    $db->autocommit(true);
                                }
                            }
                        }
                    }
                }
            }

            $sheet['child_list'] = $new_child_list;
            //echo "<pre>"; print_r($sheet); die('vvv');
            $smarty->assign("sheet", $sheet);
            $results['results'] = $smarty->fetch("ci/school/ajax.points.importresult.tpl");
            return_json($results);
            break;
        case 'search_with_subject':
            if (!canEdit($_POST['school_username'], 'points')) {
                _error(403);
            }

            //Lấy ra class_id của lớp
            $class_id = $_POST['class_id'];
            $school_year = $_POST['school_year'];

            $subject_id = $_POST['subject_id'];
            $semester = $_POST['semester'];
            $is_last_semester = $_POST['is_last_semester'];

            // Lấy school_config
            $schoolConfig = getSchoolData($school['page_id'], SCHOOL_CONFIG);
            // Lấy chi tiết gov_class_level
            $class_level = getClassLevelData($_POST['class_level_id'], CLASS_LEVEL_INFO);
            // Lấy danh sách điểm của trẻ trong năm học
            $children_point = $pointDao->getChildrenPointsBySubjectId($class_id, $school_year, $subject_id);
            if (count($children_point) > 0) {
                // flag đánh dấu đã đủ điểm các môn hay chưa
                $flag_enough_point = true;
                // trạng thái fail/pass hay re-exam hay chưa đủ điểm ( Waiting.. )
                $status_result = 'fail';
                $subject_key = array();
                $children_point_last = array();
                // lưu số cột mỗi đầu điểm
                $countColumnMaxPoint_hk1 = 2;
                $countColumnMaxPoint_hk2 = 2;
                $countColumnMaxPoint_gk1 = 1;
                $countColumnMaxPoint_gk2 = 1;

                if ($schoolConfig['score_fomula'] == "km") {
                    // Mặc định cột điểm của khmer là 3
                    $countColumnMaxPoint_hk1 = 3;
                    $countColumnMaxPoint_hk2 = 3;
                    foreach ($children_point as $child) {
                        // Lấy child_name
                        $child_detail = $childDao->getChildByCode($child['child_code']);
                        $child_id = $childDao->getChildIdByCode($child['child_code']);
                        // Lấy số buổi nghỉ có phép và không phép của học sinh
                        $child_absent = $childDao->getAbsentsChild($child_id, $class_id);

                        // Lấy danh sách điểm của trẻ trong năm học
                        $each_child_points = $pointDao->getChildrenPointsByStudentCode($class_id, $school_year, $child['child_code']);

//                    $children_point_last = array();
                        // Lưu điểm của các môn thi lại
                        $children_subject_reexams = array();
                        // flag đánh dấu đã đủ điểm các môn hay chưa
                        $child_enough_point = true;
                        // trạng thái fail/pass hay re-exam hay chưa đủ điểm ( Waiting.. )
                        $status = '';
                        // điểm các tháng của 2 kỳ
                        $child_m11_point = $child_m12_point = $child_m13_point = $child_m21_point = $child_m22_point = $child_m23_point = 0;
                        // điểm trung bình các tháng của 2 kỳ
                        $child_avM1_point = $child_avM2_point = 0;
                        // điểm trung bình của cuối kỳ
                        $child_avd1_point = 0;
                        $child_avd2_point = 0;
                        $child_final_semester1 = $child_final_semester2 = 0;
                        // điểm trung bình năm

                        $child_avYear_point = 0;
                        if (count($each_child_points) > 0) {
//                        $child_id = $childDao->getChildIdByCode($children_point[0]['child_code']);
                            // Lấy số buổi nghỉ có phép và không phép của học sinh
//                        $child_absent = $childDao->getAbsentsChild($child_id, $class_id);
                            foreach ($each_child_points as $each) {
                                if (!isset($each['hs11']) || !isset($each['hs12']) || !isset($each['hs13']) || !isset($each['gk11'])
                                    || !isset($each['hs21']) || !isset($each['hs22']) || !isset($each['hs23']) || !isset($each['gk21'])) {
                                    $flag_enough_point = false;
                                }
                                // tạm thời tính tổng các đầu điểm
                                // kỳ 1
                                $child_m11_point += (int)$each['hs11'];
                                $child_m12_point += (int)$each['hs12'];
                                $child_m13_point += (int)$each['hs13'];
                                $child_avd1_point += (int)$each['gk11'];
                                // kỳ 2
                                $child_m21_point += (int)$each['hs21'];
                                $child_m22_point += (int)$each['hs22'];
                                $child_m23_point += (int)$each['hs23'];
                                $child_avd2_point += (int)$each['gk21'];

                                // lấy tên môn học
                                $subject_detail = $subjectDao->getSubjectById($each['subject_id']);
                                $each['subject_name'] = $subject_detail['subject_name'];
                                if (isset($subject_detail['re_exam']) && $subject_detail['re_exam']) { // nếu môn đó là môn thi lại thì lưu vào array
                                    $subject_reexam['name'] = $subject_detail['subject_name'];
                                    $subject_reexam['point'] = $each['re_exam'];
                                    $children_subject_reexams[] = $subject_reexam;
                                }
                            }
                        }
//                        $child['subject_reexam']= $subject_detail['re_exam'];
                        // định nghĩa số bị chia của từng khối
                        if ($class_level['gov_class_level'] == '7' || $class_level['gov_class_level'] == '8') {
                            $dividend = 15;
                        } else if ($class_level['gov_class_level'] == '9') {
                            $dividend = 11.4;
                        } else if ($class_level['gov_class_level'] == '10') {
                            $dividend = 15.26;
                        } else if ($class_level['gov_class_level'] == '11') {
                            $dividend = 16.5;
                        } else if ($class_level['gov_class_level'] == '12') {
                            $dividend = 14.5;
                        }
                        // tính điểm theo công thức từng khối
                        // kỳ 1
                        $child_m11_point /= $dividend;
                        $child_m12_point /= $dividend;
                        $child_m13_point /= $dividend;
                        $child_avd1_point /= $dividend;
                        $child_avM1_point = ($child_m11_point + $child_m12_point + $child_m13_point) / 3;
                        $child_final_semester1 = ($child_avd1_point + $child_avM1_point) / 2;
                        // kỳ 2
                        $child_m21_point /= $dividend;
                        $child_m22_point /= $dividend;
                        $child_m23_point /= $dividend;
                        $child_avd2_point /= $dividend;
                        $child_avM2_point = ($child_m21_point + $child_m22_point + $child_m23_point) / 3;
                        $child_final_semester2 = ($child_avd2_point + $child_avM2_point) / 2;
                        // cả năm
                        $child_avYear_point = ($child_final_semester1 + $child_final_semester2) / 2;
                        // lưu lại vào array

                        //xét điều kiện để đánh giá pass hay fail
                        if ($child_avYear_point < 25) { // dưới 25 điểm thì fail chặt
                            $status = 'Fail';
                        } else { // nếu trên 25 điểm thì xét các điều kiện khác
                            $status = 'Pass';
                            if ((isset($child_absent['absent_true']) && (int)$child_absent['absent_true'] >= 55)
                                || (isset($child_absent['absent_false']) && (int)$child_absent['absent_false'] >= 30)
                                || (((int)$child_absent['absent_true'] + (int)$child_absent['absent_false']) >= 55)) {
                                if ($class_level['gov_class_level'] == '7' || $class_level['gov_class_level'] == '8'
                                    || $class_level['gov_class_level'] == '10' || $class_level['gov_class_level'] == '11') {
                                    $status = 'Re-exam';
                                } else if ($class_level['gov_class_level'] == '9' || $class_level['gov_class_level'] == '12') {
                                    $status = 'Pass';
                                }
                            }
                        }
                        // xét đến đánh giá điểm thi lại
                        if ($status == 'Re-exam') {
                            $result_exam = 0;
                            foreach ($children_subject_reexams as $subject_reexam) {
                                if (!isset($subject_reexam['point'])) {
                                    $child_enough_point = false;
                                } else {
                                    $result_exam += (int)$subject_reexam['point'];
                                }
                            }
                            if ($class_level['gov_class_level'] == '7' || $class_level['gov_class_level'] == '8'
                                || $class_level['gov_class_level'] == '11') {
                                $result_exam /= 4;
                            } else if ($class_level['gov_class_level'] == '10') {
                                $result_exam /= 4;
                            }
                            // nếu điểm thi lại trên 25 điểm thì pass còn lại fail
                            if ($child_enough_point && $result_exam >= 25) {
                                $status = 'Pass';
                            } else if ($child_enough_point && $result_exam < 25) {
                                $status = 'Fail';
                            } else { // chuaw du diem
                                $status = 'Re-exam';
                            }
                        }
//                        $children_point_last[] = $child;
                        if (!$child_enough_point && $status != 'Re-exam') {
                            $status = 'Waiting..';
                        }
                        $child['status'] = $status;
                        $child['without_permission'] = $child_absent['absent_true'];
                        $child['with_permission'] = $child_absent['absent_false'];
                        $child['child_enough_point'] = $child_enough_point;
                        $child['x1'] = number_format($child_avM1_point, 2);
                        $child['x2'] = number_format($child_avM2_point, 2);;
                        $child['e1'] = number_format($child_final_semester1, 2);;
                        $child['e2'] = number_format($child_final_semester2, 2);;
                        $child['y'] = number_format($child_avYear_point, 2);;

                        $child['child_firstname'] = $child_detail['first_name'];
                        $child['child_lastname'] = $child_detail['last_name'];
                        $child['child_id'] = $child_id;
                        $children_point_last[] = $child;
                    }

//                    $subject_key = array();
                    if ($schoolConfig['grade'] == 1) {
//                        if (!$is_last_semester) {
//                            $subject_key[] = 'gk' . $semester . 'nx';
//                            $subject_key[] = 'gk' . $semester . 'nl';
//                            $subject_key[] = 'gk' . $semester . 'pc';
//                            $subject_key[] = 'gk' . $semester . 'kt';
//                            $subject_key[] = 'gk' . $semester . 'xl';
//                        } else {
//                            $subject_key[] = 'ck' . $semester . 'nx';
//                            $subject_key[] = 'ck' . $semester . 'nl';
//                            $subject_key[] = 'ck' . $semester . 'pc';
//                            $subject_key[] = 'ck' . $semester . 'kt';
//                            $subject_key[] = 'ck' . $semester . 'xl';
//                        }
                    } elseif ($schoolConfig['grade'] == 2) {
                        if ($semester == 0) {
                            $subject_key[] = 'hs11';
                            $subject_key[] = 'hs12';
                            $subject_key[] = 'hs13';
                            $subject_key[] = 'gk11';
                            $subject_key[] = 'hs21';
                            $subject_key[] = 'hs22';
                            $subject_key[] = 'hs23';
                            $subject_key[] = 'gk21';
                            $subject_key[] = 'x1';
                            $subject_key[] = 'x2';
                            $subject_key[] = 'e1';
                            $subject_key[] = 'e2';
                            $subject_key[] = 'y';
                            $subject_key[] = 're_exam';
                            $subject_key[] = 'without_permission';
                            $subject_key[] = 'with_permission';
                            $subject_key[] = 'status';
                        } else {
                            $subject_key[] = 'hs' . $semester . '1';
                            $subject_key[] = 'hs' . $semester . '2';
                            $subject_key[] = 'hs' . $semester . '3';
                            $subject_key[] = 'gk' . $semester . '1';
                        }
                    }
                    // mặc định sinh ra 3 cột Month và 1 cột last
                    $smarty->assign("column_hk", 3);
                    $smarty->assign("column_gk", 1);
                } else if ($schoolConfig['score_fomula'] == "vn") {
//                    foreach ($children_point as $child) {

                    // điều kiện điểm của môn 1: kém, 2 yếu, 3 trung bình, 4 tiên tiến, 5 giỏi
                    $point_avg1_condition1 = 5;//hk1
                    $point_avg2_condition1 = 5;//hk2
                    $point_avgYearAll_condition1 = 5;// cả năm
                    // điều kiện điểm của môn toán hoặc văn  1: kém, 2 yếu, 3 trung bình, 4 tiên tiến, 5 giỏi
                    $point_avg1_condition2 = 5; //hk1
                    $point_avg2_condition2 = 5; //hk2
                    $point_avgYearAll_condition2 = 5; // cả năm
                    // trạng thái
                    $status_point1 = '';
                    $status_point2 = '';
                    $status_pointAll = '';
                    // flag đánh dấu học sinh đã thi lại hay chưa
                    $flag_reexam = false;
                    // trạng thái lên lớp hay thi lại hay chưa đủ điểm...
                    $status_result = 'fail';

                    // điểm trung bình các môn 2 kỳ
                    $child_TBM1_point = $child_TBM2_point = 0;
                    // điểm trung bình năm
                    $child_avYearAll_point = 0;
                    // TÍnh điểm mỗi môn
                    if ($semester != 0) {
                        $countColumnMaxPoint_hk = 2;
                        $countColumnMaxPoint_gk = 1;
                        foreach ($children_point as $child) {
                            $child_detail = $childDao->getChildByCode($child['child_code']);
                            $child_id = $childDao->getChildIdByCode($child['child_code']);
                            if (!isset($child['hs' . $semester . '1']) || !isset($child['hs' . $semester . '2'])
                                || !isset($child['gk' . $semester . '1']) || !isset($child['ck' . $semester])) {
                                $flag_enough_point = false;
                            }
                            // điểm hệ số 2 kỳ
                            $child_hk_point = 0;
                            $child_TBM_current_point = 0;
                            // đếm số điểm đã có
                            $count_hk_point = 0;
                            //Tính điểm trung bình môn
                            for ($i = 1; $i <= 6; $i++) {
                                // điểm hệ số 1
                                if (isset($child['hs' . $semester . $i])) {
                                    $child_hk_point += (int)$child['hs' . $semester . $i];
                                    $count_hk_point++;
                                    // lưu số cột đã có điểm của hệ số 1
                                    if ($i > $countColumnMaxPoint_hk) {
                                        $countColumnMaxPoint_hk = $i;
                                    }
                                }
                                // điểm hệ số 2
                                if (isset($child['gk' . $semester . $i])) {
                                    $child_hk_point += (int)$child['gk' . $semester . $i] * 2;
                                    $count_hk_point += 2;
                                    // lưu số cột đã có điểm
                                    $countColumnMaxPoint_gk = $i;
                                }
                            }
                            //điểm cuối kì

                            if (isset($child['ck' . $semester])) {
                                $child_hk_point += (int)$child['ck' . $semester] * 3;
                                $count_hk_point += 3;
                            }
                            // Điểm trung bình học kỳ
                            if ($count_hk_point > 0) {
                                $child_TBM_current_point = $child_hk_point / $count_hk_point;
                                $child['tb_hk'] = number_format($child_TBM_current_point, 2);
                            }
                            $child['child_firstname'] = $child_detail['first_name'];
                            $child['child_lastname'] = $child_detail['last_name'];
                            $child['child_id'] = $child_id;
                            $children_point_last[] = $child;
                        }
                        // setup subject_key
                        for ($i = 1; $i <= $countColumnMaxPoint_hk; $i++) {
                            $subject_key[] = 'hs' . $semester . $i;
                        }
                        for ($i = 1; $i <= $countColumnMaxPoint_gk; $i++) {
                            $subject_key[] = 'gk' . $semester . $i;
                        }
                        $subject_key[] = 'ck' . $semester;
                        $subject_key[] = 'tb_hk';

                        $smarty->assign("column_hk", $countColumnMaxPoint_hk);
                        $smarty->assign("column_gk", $countColumnMaxPoint_gk);
                        $smarty->assign("rows", $children_point_last);
                        $smarty->assign("semester", $semester);
                        $smarty->assign("grade", $schoolConfig['grade']);
                        $smarty->assign("score_fomula", $schoolConfig['score_fomula']);
                        $smarty->assign("subject_key", $subject_key);
                        $smarty->assign("username", $school['page_name']);
                        $smarty->assign("search_with", $_POST['do']);
                        $results['results'] = $smarty->fetch("ci/school/ajax.points.searchresult.tpl");
                        return_json($results);
                    } else if ($semester == 0) {
                        // lưu số cột mỗi đầu điểm

                        foreach ($children_point as $child) {
                            // Lấy child_name
                            $child_detail = $childDao->getChildByCode($child['child_code']);
                            $child_id = $childDao->getChildIdByCode($child['child_code']);
//                            // Lấy số buổi nghỉ có phép và không phép của học sinh
//                            $child_absent = $childDao->getAbsentsChild($child_id, $class_id);
                            // flag đánh dấu đã đủ điểm các môn hay chưa
                            $flag_enough_point = true;
                            // trạng thái fail/pass hay re-exam hay chưa đủ điểm ( Waiting.. )
                            $status_result = 'fail';

//                            // Lấy danh sách điểm tất cả các môn của trẻ trong năm học để tính điêm trung bình năm
//                            $each_child_points = $pointDao->getChildrenPointsByStudentCode($class_id, $school_year, $child['child_code']);
//                            foreach ($each_child_points as $each) {
                            if (!isset($child['hs11']) || !isset($child['hs12']) || !isset($child['gk11'])
                                || !isset($child['hs21']) || !isset($child['hs22']) || !isset($child['gk21'])
                                || !isset($child['ck1']) || !isset($child['ck2'])) {
                                $flag_enough_point = false;
                            }
                            // điểm hệ số 2 kỳ
                            $child_hk1_point = $child_hk2_point = 0;
                            $child_TBM1_current_point = $child_TBM2_current_point = 0;
                            // đếm số điểm đã có
                            $count_hk1_point = 0;
                            $count_hk2_point = 0;
                            //Tính điểm trung bình môn

                            for ($i = 1; $i <= 6; $i++) {
                                // điểm hệ số 1
                                if (isset($child['hs1' . $i])) {
                                    $child_hk1_point += (int)$child['hs1' . $i];
                                    $count_hk1_point++;
                                    // lưu số cột đã có điểm của hệ số 1 kỳ 1
                                    if ($i > $countColumnMaxPoint_hk1) {
                                        $countColumnMaxPoint_hk1 = $i;
                                    }
                                }
                                if (isset($child['hs2' . $i])) {
                                    $child_hk2_point += (int)$child['hs2' . $i];
                                    $count_hk2_point++;
                                    // lưu số cột đã có điểm của hệ số 1 kỳ 2
                                    if ($i > $countColumnMaxPoint_hk2) {
                                        $countColumnMaxPoint_hk2 = $i;
                                    }
                                }
                                // điểm hệ số 2
                                if (isset($child['gk1' . $i])) {
                                    $child_hk1_point += (int)$child['gk1' . $i] * 2;
                                    $count_hk1_point += 2;
                                    // lưu số cột đã có điểm
                                    if ($i > $countColumnMaxPoint_gk1) {
                                        $countColumnMaxPoint_gk1 = $i;
                                    }

                                }
                                if (isset($child['gk2' . $i])) {
                                    $child_hk2_point += (int)$child['gk2' . $i] * 2;
                                    $count_hk2_point += 2;
                                    // lưu số cột đã có điểm
                                    if ($i > $countColumnMaxPoint_gk2) {
                                        $countColumnMaxPoint_gk2 = $i;
                                    }
                                }
                            }
                            //điểm cuối kì

                            if (isset($child['ck1'])) {
                                $child_hk1_point += (int)$child['ck1'] * 3;
                                $count_hk1_point += 3;
                            }
                            if (isset($child['ck2'])) {
                                // Nếu có điểm thi lại thì điểm tổng kết sẽ tính dựa theo điểm thi lại
                                if (isset($child['re_exam'])) {
                                    $child_hk2_point += (int)$child['re_exam'] * 3;
                                    $flag_reexam = true;
                                } else {
                                    $child_hk2_point += (int)$child['ck2'] * 3;
                                }
                                $count_hk2_point += 3;
                            }
                            if ($count_hk1_point > 0) {
                                $child_TBM1_current_point = $child_hk1_point / $count_hk1_point;
//                                    $child['tb_hk1'] = $child_TBM1_current_point;
//                                    if ($each['subject_id'] == $child['subject_id']) {
                                $child['tb_hk1'] = number_format($child_TBM1_current_point, 2);
//                                    }
                            }
                            if ($count_hk2_point > 0) {
                                $child_TBM2_current_point = $child_hk2_point / $count_hk2_point;
//                                    $each['tb_hk2'] = $child_TBM2_current_point;
//                                    if ($each['subject_id'] == $child['subject_id']) {
                                $child['tb_hk2'] = number_format($child_TBM2_current_point, 2);
//                                    }
                            }

                            // Điểm tổng kết
                            $child_avYear_point = ($child_TBM1_current_point + $child_TBM2_current_point * 2) / 3;

                            if (!is_infinite($child_avYear_point)) {
                                $child['tb_year'] = number_format($child_avYear_point, 2);
                            } else {
                                $child['tb_year'] = null;
                            }

//                                }
                            // xét xem điều kiện các môn ở mức nào
//                                //học kỳ 1
//                                if ($child_TBM1_current_point < 2 && $point_avg1_condition1 > 1) {
//                                    $point_avg1_condition1 = 1;
//                                } else if ($child_TBM1_current_point < 3.5 && $point_avg1_condition1 > 2) {
//                                    $point_avg1_condition1 = 2;
//                                } else if ($child_TBM1_current_point < 5 && $point_avg1_condition1 > 3) {
//                                    $point_avg1_condition1 = 3;
//                                } else if ($child_TBM1_current_point < 6.5 && $point_avg1_condition1 > 4) {
//                                    $point_avg1_condition1 = 4;
//                                }
//                                //học kỳ 2
//                                if ($child_TBM2_current_point < 2 && $point_avg2_condition1 > 1) {
//                                    $point_avg2_condition1 = 1;
//                                } else if ($child_TBM2_current_point < 3.5 && $point_avg2_condition1 > 2) {
//                                    $point_avg2_condition1 = 2;
//                                } else if ($child_TBM2_current_point < 5 && $point_avg2_condition1 > 3) {
//                                    $point_avg2_condition1 = 3;
//                                } else if ($child_TBM2_current_point < 6.5 && $point_avg2_condition1 > 4) {
//                                    $point_avg2_condition1 = 4;
//                                }
//                                // cả năm
//                                if ($child_avYear_point < 2 && $point_avgYearAll_condition1 > 1) {
//                                    $point_avgYearAll_condition1 = 1;
//                                } else if ($child_avYear_point < 3.5 && $point_avgYearAll_condition1 > 2) {
//                                    $point_avgYearAll_condition1 = 2;
//                                } else if ($child_avYear_point < 5 && $point_avgYearAll_condition1 > 3) {
//                                    $point_avgYearAll_condition1 = 3;
//                                } else if ($child_avYear_point < 6.5 && $point_avgYearAll_condition1 > 4) {
//                                    $point_avgYearAll_condition1 = 4;
//                                }
//                                // xét xem điều kiện của môn toán văn
//                                // lấy tên môn học
//                                $subject_detail = $subjectDao->getSubjectById($each['subject_id']);
////                                $each['subject_name'] = $subject_detail['subject_name'];
//                                if (convert_vi_to_en(trim(strtolower($subject_detail['subject_name']))) == "toan" || trim(strtolower($subject_detail['subject_name'])) == "van") {
//                                    //học kỳ 1
//                                    if ($child_TBM1_current_point < 2 && $point_avg1_condition2 > 1) {
//                                        $point_avg1_condition2 = 1;
//                                    } else if ($child_TBM1_current_point < 3.5 && $point_avg1_condition2 > 2) {
//                                        $point_avg1_condition2 = 2;
//                                    } else if ($child_TBM1_current_point < 5 && $point_avg1_condition2 > 3) {
//                                        $point_avg1_condition2 = 3;
//                                    } else if ($child_TBM1_current_point < 6.5 && $point_avg1_condition2 > 4) {
//                                        $point_avg1_condition2 = 4;
//                                    }
//                                    // học kỳ 2
//                                    if ($child_TBM2_current_point < 2 && $point_avg2_condition2 > 1) {
//                                        $point_avg2_condition2 = 1;
//                                    } else if ($child_TBM2_current_point < 3.5 && $point_avg2_condition2 > 2) {
//                                        $point_avg2_condition2 = 2;
//                                    } else if ($child_TBM2_current_point < 5 && $point_avg2_condition2 > 3) {
//                                        $point_avg2_condition2 = 3;
//                                    } else if ($child_TBM2_current_point < 6.5 && $point_avg2_condition2 > 4) {
//                                        $point_avg2_condition2 = 4;
//                                    }
//                                    // cả năm
//                                    if ($child_avYear_point < 2 && $point_avgYearAll_condition2 > 1) {
//                                        $point_avgYearAll_condition2 = 1;
//                                    } else if ($child_avYear_point < 3.5 && $point_avgYearAll_condition2 > 2) {
//                                        $point_avgYearAll_condition2 = 2;
//                                    } else if ($child_avYear_point < 5 && $point_avgYearAll_condition2 > 3) {
//                                        $point_avgYearAll_condition2 = 3;
//                                    } else if ($child_avYear_point < 6.5 && $point_avgYearAll_condition2 > 4) {
//                                        $point_avgYearAll_condition2 = 4;
//                                    }
//                                }
                            // Cộng vào điểm trung bình các môn
//                                $child_TBM1_point += $child_TBM1_current_point;
//                                $child_TBM2_point += $child_TBM2_current_point;
//                                $child_avYearAll_point += $child_avYear_point;
//                                $children_point_last[] = $each;
//                            }
                            // điểm trung bình các môn
//                            $child_TBM1_point /= count($children_point);
//                            $child_TBM2_point /= count($children_point);
//                            $child_avYearAll_point /= count($children_point);

//                            //xét điều kiện để đánh giá pass hay fail
//                            //học kỳ 1
//                            if ($child_TBM1_point >= 8 && $point_avg1_condition1 == 5 && $point_avg1_condition2 == 5) { // trên 8 điểm thì đánh giá loại giỏi
//                                $status_point1 = 5;
//                            } else if ($child_TBM1_point >= 8 && ($point_avg1_condition1 == 3 || $point_avg1_condition2 == 3)) {
//                                $status_point1 = 4;
//                            } else if ($child_TBM1_point >= 8 && ($point_avg1_condition1 == 2 || $point_avg1_condition2 == 2)) {
//                                $status_point1 = 3;
//                            } else if ($child_TBM1_point >= 6.5 && $point_avg1_condition1 >= 4 && $point_avg1_condition2 >= 4) {  // trên 6.5 điểm thì đánh giá loại tiên tiến
//                                $status_point1 = 4;
//                            } else if ($child_TBM1_point >= 6.5 && ($point_avg1_condition1 == 2 || $point_avg1_condition2 == 2)) {  // trên 6.5 điểm thì đánh giá loại tiên tiến
//                                $status_point1 = 3;
//                            } else if ($child_TBM1_point >= 6.5 && ($point_avg1_condition1 == 1 || $point_avg1_condition2 == 1)) {  // trên 6.5 điểm thì đánh giá loại tiên tiến
//                                $status_point1 = 2;
//                            } else if ($child_TBM1_point >= 5 && $point_avg1_condition1 >= 3 && $point_avg1_condition2 >= 3) {  // trên 5 điểm thì đánh giá loại trung bình
//                                $status_point1 = 3;
//                            } else if ($child_TBM1_point >= 3.5 && $point_avg1_condition1 >= 2 && $point_avg1_condition2 >= 2) {  // trên 3.5 điểm thì đánh giá loại yếu
//                                $status_point1 = 2;
//                            } else if ($child_TBM1_point < 3.5 && $point_avg1_condition1 >= 1 && $point_avg1_condition2 >= 1) {
//                                $status_point1 = 1;
//                            }
//                            // học kỳ 2
//                            if ($child_TBM2_point >= 8 && $point_avg2_condition1 == 5 && $point_avg2_condition2 == 5) { // trên 8 điểm thì đánh giá loại giỏi
//                                $status_point2 = 5;
//                            } else if ($child_TBM2_point >= 8 && ($point_avg2_condition1 == 3 || $point_avg2_condition2 == 3)) {
//                                $status_point2 = 4;
//                            } else if ($child_TBM2_point >= 8 && ($point_avg2_condition1 == 2 || $point_avg2_condition2 == 2)) {
//                                $status_point2 = 3;
//                            } else if ($child_TBM2_point >= 6.5 && $point_avg2_condition1 >= 4 && $point_avg2_condition2 >= 4) {  // trên 6.5 điểm thì đánh giá loại tiên tiến
//                                $status_point2 = 4;
//                            } else if ($child_TBM2_point >= 6.5 && ($point_avg2_condition1 == 2 || $point_avg2_condition2 == 2)) {  // trên 6.5 điểm thì đánh giá loại tiên tiến
//                                $status_point2 = 3;
//                            } else if ($child_TBM2_point >= 6.5 && ($point_avg2_condition1 == 1 || $point_avg2_condition2 == 1)) {  // trên 6.5 điểm thì đánh giá loại tiên tiến
//                                $status_point2 = 2;
//                            } else if ($child_TBM2_point >= 5 && $point_avg2_condition1 >= 3 && $point_avg2_condition2 >= 3) {  // trên 5 điểm thì đánh giá loại trung bình
//                                $status_point2 = 3;
//                            } else if ($child_TBM2_point >= 3.5 && $point_avg2_condition1 >= 2 && $point_avg2_condition2 >= 2) {  // trên 3.5 điểm thì đánh giá loại yếu
//                                $status_point2 = 2;
//                            } else if ($child_TBM2_point < 3.5 && $point_avg2_condition1 >= 1 && $point_avg2_condition2 >= 1) {
//                                $status_point2 = 1;
//                            }
//                            // cả năm
//                            if ($child_avYearAll_point >= 8 && $point_avgYearAll_condition1 == 5 && $point_avgYearAll_condition2 == 5) { // trên 8 điểm thì đánh giá loại giỏi
//                                $status_pointAll = 5;
//                            } else if ($child_avYearAll_point >= 8 && ($point_avgYearAll_condition1 == 3 || $point_avgYearAll_condition2 == 3)) {
//                                $status_pointAll = 4;
//                            } else if ($child_avYearAll_point >= 8 && ($point_avgYearAll_condition1 == 2 || $point_avgYearAll_condition2 == 2)) {
//                                $status_pointAll = 3;
//                            } else if ($child_avYearAll_point >= 6.5 && $point_avgYearAll_condition1 >= 4 && $point_avgYearAll_condition2 >= 4) {  // trên 6.5 điểm thì đánh giá loại tiên tiến
//                                $status_pointAll = 4;
//                            } else if ($child_avYearAll_point >= 6.5 && ($point_avgYearAll_condition1 == 2 || $point_avgYearAll_condition2 == 2)) {  // trên 6.5 điểm thì đánh giá loại tiên tiến
//                                $status_pointAll = 3;
//                            } else if ($child_avYearAll_point >= 6.5 && ($point_avgYearAll_condition1 == 1 || $point_avgYearAll_condition2 == 1)) {  // trên 6.5 điểm thì đánh giá loại tiên tiến
//                                $status_pointAll = 2;
//                            } else if ($child_avYearAll_point >= 5 && $point_avgYearAll_condition1 >= 3 && $point_avgYearAll_condition2 >= 3) {  // trên 5 điểm thì đánh giá loại trung bình
//                                $status_pointAll = 3;
//                            } else if ($child_avYearAll_point >= 3.5 && $point_avgYearAll_condition1 >= 2 && $point_avgYearAll_condition2 >= 2) {  // trên 3.5 điểm thì đánh giá loại yếu
//                                $status_pointAll = 2;
//                            } else if ($child_avYearAll_point < 3.5 && $point_avgYearAll_condition1 >= 1 && $point_avgYearAll_condition2 >= 1) {
//                                $status_pointAll = 1;
//                            }
//
//                            // Lấy hạnh kiểm của học sinh
//                            $conduct = $conductDao->getConductOfChildren($class_id, $child_id, $school_year);
//                            if (!isset($conduct) || !isset($conduct['ck'])) {
//                                $flag_enough_point = false;
//                            } else if ($child_absent['absent_true'] + $child_absent['absent_false'] <= 45
//                                && $status_pointAll >= 3 && $conduct['ck'] != "Weak") {
//                                $status_result = "pass";
//                            }
//
//                            if (!$flag_enough_point) {
//                                $status_result = "waiting..";
//                            }
//                            $child['status'] = $status_result;
//                            $child['tb_total_hk1'] = number_format($child_TBM1_point, 2);
//                            $child['tb_total_hk2'] = number_format($child_TBM2_point, 2);
//                            $child['tb_total_year'] = number_format($child_avYear_point, 2);
                            $child['child_firstname'] = $child_detail['first_name'];
                            $child['child_lastname'] = $child_detail['last_name'];
                            $child['child_id'] = $child_id;
                            $children_point_last[] = $child;

                        }
                        // setup subject_key
                        for ($i = 1; $i <= $countColumnMaxPoint_hk1; $i++) {
                            $subject_key[] = 'hs1' . $i;
                        }
                        for ($i = 1; $i <= $countColumnMaxPoint_gk1; $i++) {
                            $subject_key[] = 'gk1' . $i;
                        }
                        $subject_key[] = 'ck1';
                        $subject_key[] = 'tb_hk1';
                        for ($i = 1; $i <= $countColumnMaxPoint_hk2; $i++) {
                            $subject_key[] = 'hs2' . $i;
                        }
                        for ($i = 1; $i <= $countColumnMaxPoint_gk2; $i++) {
                            $subject_key[] = 'gk2' . $i;
                        }
                        $subject_key[] = 'ck2';
                        $subject_key[] = 'tb_hk2';
                        $subject_key[] = 'tb_year';
                        $subject_key[] = 're_exam';
                    }
                }
                $smarty->assign("column_hk1", $countColumnMaxPoint_hk1);
                $smarty->assign("column_hk2", $countColumnMaxPoint_hk2);
                $smarty->assign("column_gk1", $countColumnMaxPoint_gk1);
                $smarty->assign("column_gk2", $countColumnMaxPoint_gk2);
                $smarty->assign("rows", $children_point_last);
                $smarty->assign("semester", $semester);
                $smarty->assign("grade", $schoolConfig['grade']);
                $smarty->assign("subject_key", $subject_key);
                $smarty->assign("username", $school['page_name']);
                $smarty->assign("search_with", $_POST['do']);
                $smarty->assign("score_fomula", $schoolConfig['score_fomula']);

            }
            $results['results'] = $smarty->fetch("ci/school/ajax.points.searchresult.tpl");
            return_json($results);
            break;
        case 'search_with_student':
            if (!canEdit($_POST['school_username'], 'points')) {
                _error(403);
            }

            //Lấy ra class_id của lớp
            $class_id = $_POST['class_id'];
            $school_year = $_POST['school_year'];

            // Lấy school_config
            $studentCode = $_POST['student_code'];
            $schoolConfig = getSchoolData($school['page_id'], SCHOOL_CONFIG);
            // Lấy chi tiết gov_class_level
            $class_level = getClassLevelData($_POST['class_level_id'], CLASS_LEVEL_INFO);
            // Lấy danh sách điểm của trẻ trong năm học
            $children_point = $pointDao->getChildrenPointsByStudentCode($class_id, $school_year, $studentCode);
            if (count($children_point) > 0) {
                // Lấy child_id
                $child_id = $childDao->getChildIdByCode($studentCode);
                // flag đánh dấu đã đủ điểm các môn hay chưa
                $flag_enough_point = true;
                // trạng thái fail/pass hay re-exam hay chưa đủ điểm ( Waiting.. )
                $status_result = 'fail';
                $subject_key = array();
                $children_point_last = array();
                // Lấy số buổi nghỉ có phép và không phép của học sinh
                $child_absent = $childDao->getAbsentsChild($child_id, $class_id);
                // lưu số cột mỗi đầu điểm
                $countColumnMaxPoint_hk1 = 2;
                $countColumnMaxPoint_hk2 = 2;
                $countColumnMaxPoint_gk1 = 1;
                $countColumnMaxPoint_gk2 = 1;
                if ($schoolConfig['score_fomula'] == "km") {
                    // Mặc định cột điểm của khmer là 3
                    $countColumnMaxPoint_hk1 = 3;
                    $countColumnMaxPoint_hk2 = 3;
                    // Lưu các điểm đã được tính toán
                    $children_point_avgs = array();
                    // Lưu điểm của các môn thi lại
                    $children_subject_reexams = array();
                    // điểm các tháng của 2 kỳ
                    $child_m11_point = $child_m12_point = $child_m13_point = $child_m21_point = $child_m22_point = $child_m23_point = 0;
                    // điểm trung bình các tháng của 2 kỳ
                    $child_avM1_point = $child_avM2_point = 0;
                    // điểm trung bình của cuối kỳ
                    $child_avd1_point = 0;
                    $child_avd2_point = 0;
                    $child_final_semester1 = $child_final_semester2 = 0;
                    // điểm trung bình năm
                    $child_avYear_point = 0;
                    // Lấy số môn hiện tại của 1 khối
                    $classLevelSubjects = $subjectDao->getSubjectsByClassLevel($school['page_id'], $class_level['gov_class_level'], $school_year);
                    $subjectCurrentOfClassLevel = array();
                    // Lọc lấy id môn
                    foreach ($classLevelSubjects as $classLevelSubject) {
                        $subjectCurrentOfClassLevel[] = $classLevelSubject['subject_id'];
                    }
                    foreach ($children_point as $each) {
                        if (!in_array($each['subject_id'], $subjectCurrentOfClassLevel)) {
                            continue;
                        }
                        // Nếu bất kì điểm nào thiếu sẽ đánh dấu là chưa đủ điểm
//                            if (!isset($child['m11']) || !isset($child['m12']) || !isset($child['m13']) || !isset($child['d1'])
//                                || !isset($child['m21']) || !isset($child['m22']) || !isset($child['m23']) || !isset($child['d2'])) {
//                                $child_enough_point = false;
//                            }
                        if (!isset($each['hs11']) || !isset($each['hs12']) || !isset($each['hs13']) || !isset($each['gk11'])
                            || !isset($each['hs21']) || !isset($each['hs22']) || !isset($each['hs23']) || !isset($each['gk21'])) {
                            $flag_enough_point = false;
                        }
                        // tạm thời tính tổng các đầu điểm
                        // kỳ 1
                        $child_m11_point += (int)$each['hs11'];
                        $child_m12_point += (int)$each['hs12'];
                        $child_m13_point += (int)$each['hs13'];
                        $child_avd1_point += (int)$each['gk11'];
                        // kỳ 2
                        $child_m21_point += (int)$each['hs21'];
                        $child_m22_point += (int)$each['hs22'];
                        $child_m23_point += (int)$each['hs23'];
                        $child_avd2_point += (int)$each['gk21'];

                        // lấy tên môn học
                        $subject_detail = $subjectDao->getSubjectById($each['subject_id']);
                        $each['subject_name'] = $subject_detail['subject_name'];
                        if (isset($subject_detail['re_exam']) && $subject_detail['re_exam']) { // nếu môn đó là môn thi lại thì lưu vào array
                            $subject_reexam['name'] = $subject_detail['subject_name'];
                            $subject_reexam['point'] = $each['re_exam'];
                            $children_subject_reexams[] = $subject_reexam;
                        }
//                        $child['subject_reexam']= $subject_detail['re_exam'];

                        $children_point_last[] = $each;
                    }

                    // định nghĩa số bị chia của từng khối
                    if ($class_level['gov_class_level'] == '7' || $class_level['gov_class_level'] == '8') {
                        $dividend = 15;
                    } else if ($class_level['gov_class_level'] == '9') {
                        $dividend = 11.4;
                    } else if ($class_level['gov_class_level'] == '10') {
                        $dividend = 15.26;
                    } else if ($class_level['gov_class_level'] == '11') {
                        $dividend = 16.5;
                    } else if ($class_level['gov_class_level'] == '12') {
                        $dividend = 14.5;
                    }
                    // tính điểm theo công thức từng khối
                    // kỳ 1
                    $child_m11_point /= $dividend;
                    $child_m12_point /= $dividend;
                    $child_m13_point /= $dividend;
                    $child_avd1_point /= $dividend;
                    $child_avM1_point = ($child_m11_point + $child_m12_point + $child_m13_point) / 3;
                    $child_final_semester1 = ($child_avd1_point + $child_avM1_point) / 2;
                    // kỳ 2
                    $child_m21_point /= $dividend;
                    $child_m22_point /= $dividend;
                    $child_m23_point /= $dividend;
                    $child_avd2_point /= $dividend;
                    $child_avM2_point = ($child_m21_point + $child_m22_point + $child_m23_point) / 3;
                    $child_final_semester2 = ($child_avd2_point + $child_avM2_point) / 2;
                    // cả năm
                    $child_avYear_point = ($child_final_semester1 + $child_final_semester2) / 2;
                    // lưu lại vào array
                    $children_point_avgs['a1'] = $child_m11_point;
                    $children_point_avgs['b1'] = $child_m12_point;
                    $children_point_avgs['c1'] = $child_m13_point;
                    $children_point_avgs['d1'] = $child_avd1_point;
                    $children_point_avgs['x1'] = $child_avM1_point;
                    $children_point_avgs['e1'] = $child_final_semester1;
                    $children_point_avgs['a2'] = $child_m21_point;
                    $children_point_avgs['b2'] = $child_m22_point;
                    $children_point_avgs['c2'] = $child_m23_point;
                    $children_point_avgs['d2'] = $child_avd2_point;
                    $children_point_avgs['x2'] = $child_avM2_point;
                    $children_point_avgs['e2'] = $child_final_semester2;
                    $children_point_avgs['y'] = $child_avYear_point;

                    //xét điều kiện để đánh giá pass hay fail
                    if ($child_avYear_point < 25) { // dưới 25 điểm thì fail chặt
                        $status_result = 'Fail';
                    } else { // nếu trên 25 điểm thì xét các điều kiện khác
                        $status_result = 'Pass';
                        if ((isset($child_absent['absent_true']) && (int)$child_absent['absent_true'] >= 55)
                            || (isset($child_absent['absent_false']) && (int)$child_absent['absent_false'] >= 30)
                            || (((int)$child_absent['absent_true'] + (int)$child_absent['absent_false']) >= 55)) {
                            if ($class_level['gov_class_level'] == '7' || $class_level['gov_class_level'] == '8'
                                || $class_level['gov_class_level'] == '10' || $class_level['gov_class_level'] == '11') {
                                $status_result = 'Re-exam';
                            } else if ($class_level['gov_class_level'] == '9' || $class_level['gov_class_level'] == '12') {
                                $status_result = 'Pass';
                            }
                        }
                    }
                    // xét đến đánh giá điểm thi lại
                    if ($status_result == 'Re-exam') {
                        $result_exam = 0;
                        foreach ($children_subject_reexams as $subject_reexam) {
                            if (!isset($subject_reexam['point'])) {
                                $child_enough_point = false;
                            } else {
                                $result_exam += (int)$subject_reexam['point'];
                            }
                        }
                        if ($class_level['gov_class_level'] == '7' || $class_level['gov_class_level'] == '8'
                            || $class_level['gov_class_level'] == '11') {
                            $result_exam /= 4;
                        } else if ($class_level['gov_class_level'] == '10') {
                            $result_exam /= 4;
                        }
                        // nếu điểm thi lại trên 25 điểm thì pass còn lại fail
                        if ($child_enough_point && $result_exam >= 25) {
                            $status_result = 'Pass';
                        } else if ($child_enough_point && $result_exam < 25) {
                            $status_result = 'Fail';
                        } else { // chuaw du diem
                            $status_result = 'Re-exam';
                        }
                    }
                    if (!$child_enough_point && $status_result != 'Re-exam') {
                        $status_result = 'Waiting..';
                    }

                    if ($schoolConfig['grade'] == 1) { // chưa làm cho cấp 1

                    } elseif ($schoolConfig['grade'] == 2) {
                        $subject_key[] = 'hs11';
                        $subject_key[] = 'hs12';
                        $subject_key[] = 'hs13';
                        $subject_key[] = 'gk11';
                        $subject_key[] = 'hs21';
                        $subject_key[] = 'hs22';
                        $subject_key[] = 'hs23';
                        $subject_key[] = 'gk21';
                    }

                    $smarty->assign("children_point_avgs", $children_point_avgs);
                    $smarty->assign("children_subject_reexams", $children_subject_reexams);
                    $smarty->assign("result_exam", $result_exam);
                } else if ($schoolConfig['score_fomula'] == "vn") {
                    // điều kiện điểm của môn 1: kém, 2 yếu, 3 trung bình, 4 tiên tiến, 5 giỏi
                    $point_avg1_condition1 = 5;//hk1
                    $point_avg2_condition1 = 5;//hk2
                    $point_avgYearAll_condition1 = 5;// cả năm
                    // điều kiện điểm của môn toán hoặc văn  1: kém, 2 yếu, 3 trung bình, 4 tiên tiến, 5 giỏi
                    $point_avg1_condition2 = 5; //hk1
                    $point_avg2_condition2 = 5; //hk2
                    $point_avgYearAll_condition2 = 5; // cả năm
                    // trạng thái
                    $status_point1 = '';
                    $status_point2 = '';
                    $status_pointAll = '';
                    // flag đánh dấu học sinh đã thi lại hay chưa
                    $flag_reexam = false;
                    // trạng thái lên lớp hay thi lại hay chưa đủ điểm...
                    $status_result = 'fail';

                    // điểm trung bình các môn 2 kỳ
                    $child_TBM1_point = $child_TBM2_point = 0;
                    // điểm trung bình năm
                    $child_avYearAll_point = 0;
                    // TÍnh điểm mỗi môn
                    if (count($children_point) > 0) {
                        foreach ($children_point as $each) {
                            if (!isset($each['hs11']) || !isset($each['hs12']) || !isset($each['gk11'])
                                || !isset($each['hs21']) || !isset($each['hs22']) || !isset($each['gk21'])
                                || !isset($each['ck1']) || !isset($each['ck2'])) {
                                $flag_enough_point = false;
                            }
                            // điểm hệ số 2 kỳ
                            $child_hk1_point = $child_hk2_point = 0;
                            $child_TBM1_current_point = $child_TBM2_current_point = 0;
                            // đếm số điểm đã có
                            $count_hk1_point = 0;
                            $count_hk2_point = 0;
                            //Tính điểm trung bình môn

                            for ($i = 1; $i <= 6; $i++) {
                                // điểm hệ số 1
                                if (isset($each['hs1' . $i])) {
                                    $child_hk1_point += (int)$each['hs1' . $i];
                                    $count_hk1_point++;
                                    // lưu số cột đã có điểm của hệ số 1 kỳ 1
                                    if ($i > $countColumnMaxPoint_hk1) {
                                        $countColumnMaxPoint_hk1 = $i;
                                    }
                                }
                                if (isset($each['hs2' . $i])) {
                                    $child_hk2_point += (int)$each['hs2' . $i];
                                    $count_hk2_point++;
                                    // lưu số cột đã có điểm của hệ số 1 kỳ 2
                                    if ($i > $countColumnMaxPoint_hk2) {
                                        $countColumnMaxPoint_hk2 = $i;
                                    }
                                }
                                // điểm hệ số 2
                                if (isset($each['gk1' . $i])) {
                                    $child_hk1_point += (int)$each['gk1' . $i] * 2;
                                    $count_hk1_point += 2;
                                    // lưu số cột đã có điểm
                                    $countColumnMaxPoint_gk1 = $i;
                                }
                                if (isset($each['gk2' . $i])) {
                                    $child_hk2_point += (int)$each['gk2' . $i] * 2;
                                    $count_hk2_point += 2;
                                    // lưu số cột đã có điểm
                                    $countColumnMaxPoint_gk2 = $i;
                                }
                            }
                            //điểm cuối kì

                            if (isset($each['ck1'])) {
                                $child_hk1_point += (int)$each['ck1'] * 3;
                                $count_hk1_point += 3;
                            }
                            if (isset($each['ck2'])) {
                                // Nếu có điểm thi lại thì điểm tổng kết sẽ tính dựa theo điểm thi lại
                                if (isset($each['re_exam'])) {
                                    $child_hk2_point += (int)$each['re_exam'] * 3;
                                    $flag_reexam = true;
                                } else {
                                    $child_hk2_point += (int)$each['ck2'] * 3;
                                }
                                $count_hk2_point += 3;
                            }
                            if ($count_hk1_point > 0) {
                                $child_TBM1_current_point = $child_hk1_point / $count_hk1_point;
                                $each['tb_hk1'] = number_format($child_TBM1_current_point, 2);
                            }
                            if ($count_hk2_point > 0) {
                                $child_TBM2_current_point = $child_hk2_point / $count_hk2_point;
                                $each['tb_hk2'] = number_format($child_TBM2_current_point, 2);
                            }
                            // Lấy tên môn học
                            $subject_detail = $subjectDao->getSubjectById($each['subject_id']);
                            $each['subject_name'] = $subject_detail['subject_name'];
                            // Điểm tổng kết
                            $child_avYear_point = ($child_TBM1_current_point + $child_TBM2_current_point * 2) / 3;
                            $each['tb_year'] = number_format($child_avYear_point, 2);
                            // xét xem điều kiện các môn ở mức nào
                            //học kỳ 1
                            if ($child_TBM1_current_point < 2 && $point_avg1_condition1 > 1) {
                                $point_avg1_condition1 = 1;
                            } else if ($child_TBM1_current_point < 3.5 && $point_avg1_condition1 > 2) {
                                $point_avg1_condition1 = 2;
                            } else if ($child_TBM1_current_point < 5 && $point_avg1_condition1 > 3) {
                                $point_avg1_condition1 = 3;
                            } else if ($child_TBM1_current_point < 6.5 && $point_avg1_condition1 > 4) {
                                $point_avg1_condition1 = 4;
                            }
                            //học kỳ 2
                            if ($child_TBM2_current_point < 2 && $point_avg2_condition1 > 1) {
                                $point_avg2_condition1 = 1;
                            } else if ($child_TBM2_current_point < 3.5 && $point_avg2_condition1 > 2) {
                                $point_avg2_condition1 = 2;
                            } else if ($child_TBM2_current_point < 5 && $point_avg2_condition1 > 3) {
                                $point_avg2_condition1 = 3;
                            } else if ($child_TBM2_current_point < 6.5 && $point_avg2_condition1 > 4) {
                                $point_avg2_condition1 = 4;
                            }
                            // cả năm
                            if ($child_avYear_point < 2 && $point_avgYearAll_condition1 > 1) {
                                $point_avgYearAll_condition1 = 1;
                            } else if ($child_avYear_point < 3.5 && $point_avgYearAll_condition1 > 2) {
                                $point_avgYearAll_condition1 = 2;
                            } else if ($child_avYear_point < 5 && $point_avgYearAll_condition1 > 3) {
                                $point_avgYearAll_condition1 = 3;
                            } else if ($child_avYear_point < 6.5 && $point_avgYearAll_condition1 > 4) {
                                $point_avgYearAll_condition1 = 4;
                            }
                            // xét xem điều kiện của môn toán văn
                            // lấy tên môn học
                            $subject_detail = $subjectDao->getSubjectById($each['subject_id']);
                            $each['subject_name'] = $subject_detail['subject_name'];
                            if (convert_vi_to_en(strtolower(html_entity_decode($each['subject_name']))) == "toan" || convert_vi_to_en(strtolower(html_entity_decode($each['subject_name']))) == "van") {
                                //học kỳ 1
                                if ($child_TBM1_current_point < 2 && $point_avg1_condition2 > 1) {
                                    $point_avg1_condition2 = 1;
                                } else if ($child_TBM1_current_point < 3.5 && $point_avg1_condition2 > 2) {
                                    $point_avg1_condition2 = 2;
                                } else if ($child_TBM1_current_point < 5 && $point_avg1_condition2 > 3) {
                                    $point_avg1_condition2 = 3;
                                } else if ($child_TBM1_current_point < 6.5 && $point_avg1_condition2 > 4) {
                                    $point_avg1_condition2 = 4;
                                }
                                // học kỳ 2
                                if ($child_TBM2_current_point < 2 && $point_avg2_condition2 > 1) {
                                    $point_avg2_condition2 = 1;
                                } else if ($child_TBM2_current_point < 3.5 && $point_avg2_condition2 > 2) {
                                    $point_avg2_condition2 = 2;
                                } else if ($child_TBM2_current_point < 5 && $point_avg2_condition2 > 3) {
                                    $point_avg2_condition2 = 3;
                                } else if ($child_TBM2_current_point < 6.5 && $point_avg2_condition2 > 4) {
                                    $point_avg2_condition2 = 4;
                                }
                                // cả năm
                                if ($child_avYear_point < 2 && $point_avgYearAll_condition2 > 1) {
                                    $point_avgYearAll_condition2 = 1;
                                } else if ($child_avYear_point < 3.5 && $point_avgYearAll_condition2 > 2) {
                                    $point_avgYearAll_condition2 = 2;
                                } else if ($child_avYear_point < 5 && $point_avgYearAll_condition2 > 3) {
                                    $point_avgYearAll_condition2 = 3;
                                } else if ($child_avYear_point < 6.5 && $point_avgYearAll_condition2 > 4) {
                                    $point_avgYearAll_condition2 = 4;
                                }
                            }
                            // Cộng vào điểm trung bình các môn
                            $child_TBM1_point += $child_TBM1_current_point;
                            $child_TBM2_point += $child_TBM2_current_point;
                            $child_avYearAll_point += $child_avYear_point;
                            $children_point_last[] = $each;
                        }
                        // điểm trung bình các môn
                        $child_TBM1_point /= count($children_point);
                        $child_TBM2_point /= count($children_point);
                        $child_avYearAll_point /= count($children_point);

                        //xét điều kiện để đánh giá pass hay fail
                        //học kỳ 1
                        if ($child_TBM1_point >= 8 && $point_avg1_condition1 == 5 && $point_avg1_condition2 == 5) { // trên 8 điểm thì đánh giá loại giỏi
                            $status_point1 = 5;
                        } else if ($child_TBM1_point >= 8 && ($point_avg1_condition1 == 3 || $point_avg1_condition2 == 3)) {
                            $status_point1 = 4;
                        } else if ($child_TBM1_point >= 8 && ($point_avg1_condition1 == 2 || $point_avg1_condition2 == 2)) {
                            $status_point1 = 3;
                        } else if ($child_TBM1_point >= 6.5 && $point_avg1_condition1 >= 4 && $point_avg1_condition2 >= 4) {  // trên 6.5 điểm thì đánh giá loại tiên tiến
                            $status_point1 = 4;
                        } else if ($child_TBM1_point >= 6.5 && ($point_avg1_condition1 == 2 || $point_avg1_condition2 == 2)) {  // trên 6.5 điểm thì đánh giá loại tiên tiến
                            $status_point1 = 3;
                        } else if ($child_TBM1_point >= 6.5 && ($point_avg1_condition1 == 1 || $point_avg1_condition2 == 1)) {  // trên 6.5 điểm thì đánh giá loại tiên tiến
                            $status_point1 = 2;
                        } else if ($child_TBM1_point >= 5 && $point_avg1_condition1 >= 3 && $point_avg1_condition2 >= 3) {  // trên 5 điểm thì đánh giá loại trung bình
                            $status_point1 = 3;
                        } else if ($child_TBM1_point >= 3.5 && $point_avg1_condition1 >= 2 && $point_avg1_condition2 >= 2) {  // trên 3.5 điểm thì đánh giá loại yếu
                            $status_point1 = 2;
                        } else if ($child_TBM1_point < 3.5 && $point_avg1_condition1 >= 1 && $point_avg1_condition2 >= 1) {
                            $status_point1 = 1;
                        }
                        // học kỳ 2
                        if ($child_TBM2_point >= 8 && $point_avg2_condition1 == 5 && $point_avg2_condition2 == 5) { // trên 8 điểm thì đánh giá loại giỏi
                            $status_point2 = 5;
                        } else if ($child_TBM2_point >= 8 && ($point_avg2_condition1 == 3 || $point_avg2_condition2 == 3)) {
                            $status_point2 = 4;
                        } else if ($child_TBM2_point >= 8 && ($point_avg2_condition1 == 2 || $point_avg2_condition2 == 2)) {
                            $status_point2 = 3;
                        } else if ($child_TBM2_point >= 6.5 && $point_avg2_condition1 >= 4 && $point_avg2_condition2 >= 4) {  // trên 6.5 điểm thì đánh giá loại tiên tiến
                            $status_point2 = 4;
                        } else if ($child_TBM2_point >= 6.5 && ($point_avg2_condition1 == 2 || $point_avg2_condition2 == 2)) {  // trên 6.5 điểm thì đánh giá loại tiên tiến
                            $status_point2 = 3;
                        } else if ($child_TBM2_point >= 6.5 && ($point_avg2_condition1 == 1 || $point_avg2_condition2 == 1)) {  // trên 6.5 điểm thì đánh giá loại tiên tiến
                            $status_point2 = 2;
                        } else if ($child_TBM2_point >= 5 && $point_avg2_condition1 >= 3 && $point_avg2_condition2 >= 3) {  // trên 5 điểm thì đánh giá loại trung bình
                            $status_point2 = 3;
                        } else if ($child_TBM2_point >= 3.5 && $point_avg2_condition1 >= 2 && $point_avg2_condition2 >= 2) {  // trên 3.5 điểm thì đánh giá loại yếu
                            $status_point2 = 2;
                        } else if ($child_TBM2_point < 3.5 && $point_avg2_condition1 >= 1 && $point_avg2_condition2 >= 1) {
                            $status_point2 = 1;
                        }
                        // cả năm
                        if ($child_avYearAll_point >= 8 && $point_avgYearAll_condition1 == 5 && $point_avgYearAll_condition2 == 5) { // trên 8 điểm thì đánh giá loại giỏi
                            $status_pointAll = 5;
                        } else if ($child_avYearAll_point >= 8 && ($point_avgYearAll_condition1 == 3 || $point_avgYearAll_condition2 == 3)) {
                            $status_pointAll = 4;
                        } else if ($child_avYearAll_point >= 8 && ($point_avgYearAll_condition1 == 2 || $point_avgYearAll_condition2 == 2)) {
                            $status_pointAll = 3;
                        } else if ($child_avYearAll_point >= 6.5 && $point_avgYearAll_condition1 >= 4 && $point_avgYearAll_condition2 >= 4) {  // trên 6.5 điểm thì đánh giá loại tiên tiến
                            $status_pointAll = 4;
                        } else if ($child_avYearAll_point >= 6.5 && ($point_avgYearAll_condition1 == 2 || $point_avgYearAll_condition2 == 2)) {  // trên 6.5 điểm thì đánh giá loại tiên tiến
                            $status_pointAll = 3;
                        } else if ($child_avYearAll_point >= 6.5 && ($point_avgYearAll_condition1 == 1 || $point_avgYearAll_condition2 == 1)) {  // trên 6.5 điểm thì đánh giá loại tiên tiến
                            $status_pointAll = 2;
                        } else if ($child_avYearAll_point >= 5 && $point_avgYearAll_condition1 >= 3 && $point_avgYearAll_condition2 >= 3) {  // trên 5 điểm thì đánh giá loại trung bình
                            $status_pointAll = 3;
                        } else if ($child_avYearAll_point >= 3.5 && $point_avgYearAll_condition1 >= 2 && $point_avgYearAll_condition2 >= 2) {  // trên 3.5 điểm thì đánh giá loại yếu
                            $status_pointAll = 2;
                        } else if ($child_avYearAll_point < 3.5 && $point_avgYearAll_condition1 >= 1 && $point_avgYearAll_condition2 >= 1) {
                            $status_pointAll = 1;
                        }

                        // Lấy hạnh kiểm của học sinh
                        $conduct = $conductDao->getConductOfChildren($class_id, $child_id, $school_year);
                        if (!isset($conduct) || !isset($conduct['ck'])) {
                            $flag_enough_point = false;
                        } else if ($child_absent['absent_true'] + $child_absent['absent_false'] <= 45
                            && $status_pointAll >= 3 && $conduct['ck'] != "Weak") {
                            $status_result = "pass";
                        }


                    }
                    if (!$flag_enough_point) {
                        $status_result = "waiting..";
                    }
                    // setup subject_key
                    for ($i = 1; $i <= $countColumnMaxPoint_hk1; $i++) {
                        $subject_key[] = 'hs1' . $i;
                    }
                    for ($i = 1; $i <= $countColumnMaxPoint_gk1; $i++) {
                        $subject_key[] = 'gk1' . $i;
                    }
                    $subject_key[] = 'ck1';
                    $subject_key[] = 'tb_hk1';
                    for ($i = 1; $i <= $countColumnMaxPoint_hk2; $i++) {
                        $subject_key[] = 'hs2' . $i;
                    }
                    for ($i = 1; $i <= $countColumnMaxPoint_gk2; $i++) {
                        $subject_key[] = 'gk2' . $i;
                    }
                    $subject_key[] = 'ck2';
                    $subject_key[] = 'tb_hk2';
                    $subject_key[] = 're_exam';


                    $smarty->assign("tb_total_hk1", $child_TBM1_point);
                    $smarty->assign("tb_total_hk2", $child_TBM2_point);
                    $smarty->assign("tb_total_year", $child_avYearAll_point);
                }
                $smarty->assign("column_hk1", $countColumnMaxPoint_hk1);
                $smarty->assign("column_hk2", $countColumnMaxPoint_hk2);
                $smarty->assign("column_gk1", $countColumnMaxPoint_gk1);
                $smarty->assign("column_gk2", $countColumnMaxPoint_gk2);
                $smarty->assign("rows", $children_point_last);
                $smarty->assign("grade", $schoolConfig['grade']);
                $smarty->assign("subject_key", $subject_key);
                $smarty->assign("username", $school['page_name']);
                $smarty->assign("search_with", $_POST['do']);
                $smarty->assign("child_absent", $child_absent);
                $smarty->assign("status", $status_result);
                $smarty->assign("score_fomula", $schoolConfig['score_fomula']);
            }
            $results['results'] = $smarty->fetch("ci/school/ajax.points.searchresult.tpl");
            return_json($results);
            break;
        case
        'show_comment_child':
            if (!isset($_POST['class_id']) || !is_numeric($_POST['class_id'])) {
                _error(404);
            }

            if (!isset($_POST['child_id']) || !is_numeric($_POST['child_id'])) {
                _error(404);
            }

            // Get school_config
            $schoolConfig = getSchoolData($school['page_id'], SCHOOL_CONFIG);

            $school_year = $_POST['school_year'];
            $is_last_semester = $_POST['is_last_semester'];
            // Lấy chi tiết nhận xét của trẻ theo điều kiện gửi lên
            $child_comment = $pointDao->getChildComment($_POST['child_id'], $_POST['class_id'], $school_year, $_POST['semester'], $is_last_semester, $schoolConfig['grade']);

            $smarty->assign('results', $child_comment);
            $smarty->assign('username', $_POST['school_username']);
            $smarty->assign('grade', $schoolConfig['grade']);

            $return['results'] = $smarty->fetch("ci/school/ajax.school.point.comment.tpl");
            return_json($return);
            break;

        case 'comment':
            if (!isset($_POST['class_id']) || !is_numeric($_POST['class_id'])) {
                _error(404);
            }

            if (!isset($_POST['child_id']) || !is_numeric($_POST['child_id'])) {
                _error(404);
            }

            $db->begin_transaction();

            $child_id = $_POST['child_id'];
            $school_year = $_POST['school_year'];
            $is_last_semester = $_POST['is_last_semester'];

            // Get school_config
            $schoolConfig = getSchoolData($school['page_id'], SCHOOL_CONFIG);

            // Kiểm tra xem trẻ đã được thêm  nhận xét trước đó chưa
            $is_child_comment = $pointDao->checkChildComment($child_id, $_POST['class_id'], $school_year, $schoolConfig['grade']);

            if ($is_child_comment) {
                // Update child comment
                $pointDao->updateChildComment($child_id, $_POST['class_id'], $school_year, $_POST['semester'], $is_last_semester, $schoolConfig['grade'], $_POST['child_comment']);
            } else {
                // insert child comment
                $pointDao->createChildComment($child_id, $_POST['class_id'], $school_year, $_POST['semester'], $is_last_semester, $schoolConfig['grade'], $_POST['child_comment']);
            }

            $db->commit();

            return_json(array('success' => true, 'message' => __("Success")));
            break;
        default:
            _error(400);
            break;
    }

} catch
(Exception $e) {
    $db->rollback();
    return_json(array('error' => true, 'message' => $e->getMessage()));
} finally {
    $db->autocommit(true);
}
?>