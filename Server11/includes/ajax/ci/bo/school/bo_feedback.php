<?php
/**
 * Package: ajax/ci/bo
 * 
 * @package ConIu
 * @author QuanND
 */

// fetch bootstrap
require('../../../../../bootstrap.php');

// check AJAX Request
is_ajax();
check_login();

if(is_empty($_POST['school_username']) || !valid_username($_POST['school_username'])) {
    _error(404);
}

include_once(DAO_PATH.'dao_school.php');
include_once(DAO_PATH.'dao_user.php');
include_once(DAO_PATH.'dao_feedback.php');
$schoolDao = new SchoolDAO();
$userDao = new UserDAO();
$feedbackDao = new FeedbackDAO();

//$school = getSchool($_POST['school_username']);
$school = getSchoolDataByUsername($_POST['school_username'], SCHOOL_INFO);
if (is_null($school)) {
    _error(403);
}

try {
    $return = array();
    $db->autocommit(false);
    switch ($_POST['do']) {
        case 'confirm':
            if (!canEdit($_POST['school_username'], 'feedback')) {
                _error(403);
            }

            if(!isset($_POST['feedback_id']) || !is_numeric($_POST['feedback_id'])) {
                _error(404);
            }
            $feedback = $feedbackDao->getFeedback($_POST['feedback_id']);
            if (is_null($feedback)) {
                _error(404);
            }
            // 1.Update trạng thái
            $feedbackDao->updateStatusToConfirmed($_POST['feedback_id']);
            // 2. Thông báo đến phụ huynh
            //Thông báo xác nhận thông báo đến phụ huynh
            $uId[] = $feedback['created_user_id'];
            $userDao->postNotifications($uId, NOTIFICATION_CONFIRM_FEEDBACK, NOTIFICATION_NODE_TYPE_CHILD, $_POST['feedback_id']);

            // Thông báo cho các quản lý trường khác
            $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'feedback', $school['page_admin']);
            $userDao->postNotifications($userManagerIds, NOTIFICATION_CONFIRM_FEEDBACK, NOTIFICATION_NODE_TYPE_SCHOOL,
                $_POST['feedback_id'], '', $school['page_name'], convertText4Web($school['page_title']));

            $db->commit();
            return_json( array('success' => true, 'message' => __("Success")) );
            break;

        case 'delete':
            if (!canEdit($_POST['school_username'], 'feedback')) {
                _error(403);
            }

            if(!isset($_POST['feedback_id']) || !is_numeric($_POST['feedback_id'])) {
                _error(404);
            }
            $feedback = $feedbackDao->getFeedback($_POST['feedback_id']);
            if (is_null($feedback)) {
                _error(404);
            }
            // 1.Ẩn feedback khỏi danh dách
            $feedbackDao->deleteFeedback($_POST['feedback_id']);

            $db->commit();

            return_json( array('success' => true, 'message' => __("Success")) );
            break;
        case 'search_feedback':
            if(!isset($_POST['class_id']) || !is_numeric($_POST['class_id'])) {
                _error(404);
            }
            if ($_POST['class_id'] == 0) {
                $result = $feedbackDao->getFeedbackOfSchool($school['page_id']);
            } else {
                $result = $feedbackDao->getFeedbackOfClass($_POST['class_id']);
            }

            $smarty->assign('username', $_POST['school_username']);
            $smarty->assign('rows', $result);
            $smarty->assign('canEdit', canEdit($_POST['school_username'], 'feedback'));

            $return['results'] = $smarty->fetch("ci/school/ajax.school.feedbacklist.tpl");
            return_json($return);
            break;

        default:
            _error(400);
            break;
    }

} catch (Exception $e) {
    $db->rollback();
    return_json( array('error' => true, 'message' => $e->getMessage()) );
} finally {
    $db->autocommit(true);
}

?>