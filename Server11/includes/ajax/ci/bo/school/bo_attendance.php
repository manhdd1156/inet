<?php
/**
 * Package: ajax/ci/bo
 * 
 * @package ConIu
 * @author QuanND
 */

// fetch bootstrap
require('../../../../../bootstrap.php');

// check AJAX Request
is_ajax();
check_login();

if(is_empty($_POST['school_username']) || !valid_username($_POST['school_username'])) {
    _error(404);
}

include_once(DAO_PATH . 'dao_school.php');
include_once(DAO_PATH . 'dao_event.php');
include_once(DAO_PATH . 'dao_parent.php');
include_once(DAO_PATH . 'dao_teacher.php');
include_once(DAO_PATH . 'dao_user.php');
include_once(DAO_PATH . 'dao_class.php');
include_once(DAO_PATH . 'dao_attendance.php');
include_once(DAO_PATH . 'dao_child.php');

$schoolDao = new SchoolDAO();
$eventDao = new EventDAO();
$parentDao = new ParentDAO();
$teacherDao = new TeacherDAO();
$userDao = new UserDAO();
$classDao = new ClassDAO();
$attendanceDao = new AttendanceDAO();
$childDao = new ChildDAO();

//$school = getSchool($_POST['school_username']);
$school = getSchoolDataByUsername($_POST['school_username'], SCHOOL_DATA);
if (is_null($school)) {
    _error(403);
}
$smarty->assign('username', $_POST['school_username']);

try {
    $return = array();
    switch ($_POST['do']) {
        case 'search':
            if (!canView($_POST['school_username'], 'attendance')) {
                _error(403);
            }
            if (isset($_POST['class_id']) && $_POST['class_id'] > 0) {
                //Lấy ra điểm danh của một lớp
                $results = $attendanceDao->getAttendanceOfClass($_POST['class_id'], $_POST['fromDate'], $_POST['toDate']);
                $smarty->assign('class_id', $_POST['class_id']);
            } else {
                //Lấy ra điểm danh của trường
                $results = $attendanceDao->getAttendanceOfSchool($school['page_id'], $_POST['fromDate'], $_POST['toDate']);
            }

            $smarty->assign('rows', $results);
            $return['results'] = $smarty->fetch("ci/school/ajax.school.attendancesearch.tpl");

            return_json($return);
            break;
        case 'list_4rollup': //Hiện thông tin điểm danh lớp/ngày. Trong màn hình điểm danh của lớp.
            $data = $attendanceDao->getAttendanceDetail($_POST['class_id'], $_POST['attendance_date']);

//            $att = array();
            // lặp danh sách trẻ, nếu những trẻ nhập học trong tháng này thì những tháng trước không lấy trẻ đấy
            foreach ($data['detail'] as $k => $row) {
                // Lấy ngày trẻ bắt đầu đi học trong lớp
                $begin_at = $childDao->getChildBeginAtInClass($row['child_id'], $_POST['class_id']);
                // lấy ngày đầu tháng hiện tại
                $firstDayofMonthNow = date('Y-m-01');
                $attendanceDate = toDBDate($_POST['attendance_date']);
                if((strtotime($begin_at) - strtotime($firstDayofMonthNow)) > 0 && (strtotime($firstDayofMonthNow) - strtotime($attendanceDate)) > 0) {
                    unset($data['detail'][$k]);
//                    $att[] = $row;
                }
            }

            // Lấy thông tin điểm danh về của lớp
            $data_cameback = $attendanceDao->getAttendanceCameBackdetail($_POST['class_id'], $_POST['attendance_date']);
            for($i = 0; $i < count($data_cameback['detail']); $i++) {
                $data_cameback['detail'][$i]['status'] = $data['detail'][$i]['status'];
            }
            $hour = date('H:i');
//            die;
//            echo "<pre>";
//            print_r($data);
//            print_r($data_cameback); die;
            $smarty->assign('data', $data);
            $smarty->assign('hour', $hour);
            $smarty->assign('data_cameback', $data_cameback);
            $smarty->assign('school', $school);
            $return['results'] = $smarty->fetch("ci/school/ajax.school.attendance.list4rollup.tpl");
            $return['disableSave'] = (count($data['detail']) == 0);
            return_json($return);
            break;
        case 'save_rollup': //Xử lý nghiệp vụ khi lưu ĐIỂM DANH NGÀY CỦA MỘT LỚP
            if (!canEdit($_POST['school_username'], 'attendance')) {
                _error(403);
            }

            $args = array();
            $args['attendance_date'] = $_POST['attendance_date'];
            $attendanceDate = toDBDate($args['attendance_date']);
            $dateNow = toDBDate(toSysDate($date));

            $args['class_id'] = $_POST['class_id'];

            //Kiểm tra xem lớp đã có bản ghi điểm danh chưa
            $attendanceId = $attendanceDao->getAttendanceId($args['class_id'], $args['attendance_date']);

            $allChildIds = $_POST['allChildIds'];
            $allStatus = array();
            foreach ($allChildIds as $child_id) {
                $allStatus[] = $_POST['rollup_' . $child_id];
            }
            // $allStatus = $_POST['allStatus'];
            $allReasons = $_POST['allReasons'];
            //echo "<pre>"; print_r($allStatus); die;
            $absenceCount = 0;
            $presentCount = 0;
            foreach ($allStatus as $status) {
                if (($status == ATTENDANCE_ABSENCE) || ($status == ATTENDANCE_ABSENCE_NO_REASON)) {
                    $absenceCount++;
                } else {
                    //ATTENDANCE_PRESENT, ATTENDANCE_COME_LATE, ATTENDANCE_EARLY_LEAVE
                    $presentCount++;
                }
            }

            $args['absence_count'] = $absenceCount;
            $args['present_count'] = $presentCount;
            $args['is_checked'] = 1;

            $db->begin_transaction();
            if ($attendanceId > 0) {
                // Lấy trạng thái điểm danh của cả lớp theo attendance_id
                $attendance = $attendanceDao->getAttendance($attendanceId, $school['page_id'], $class['group_id']);

                $attendanceChildIds = array();
                // Lặp danh sách điểm danh chi tiết, đưa vào mảng để tiện so sánh
                $childrenAtt = array();
                foreach ($attendance['detail'] as $row) {
                    if(isset($row['attendance_detail_id']) && $row['attendance_detail_id'] > 0) {
                        $childrenAtt[$row['child_id']]['status'] = $row['status'];
                        $childrenAtt[$row['child_id']]['reason'] = $row['reason'];
                        $childrenAtt[$row['child_id']]['is_parent'] = $row['is_parent'];
                        $childrenAtt[$row['child_id']]['feedback'] = $row['feedback'];
                        $childrenAtt[$row['child_id']]['attendance_detail_id'] = $row['attendance_detail_id'];
                    } else {
                        $childrenAtt[$row['child_id']]['status'] = '';
                        $childrenAtt[$row['child_id']]['reason'] = '';
                        $childrenAtt[$row['child_id']]['is_parent'] = '';
                        $childrenAtt[$row['child_id']]['feedback'] = '';
                        $childrenAtt[$row['child_id']]['attendance_detail_id'] = '';
                    }

                    // Lấy danh sách id những trẻ đã điểm danh rồi
                    $attendanceChildIds[] = $row['child_id'];
                }

                // Lấy danh sách những trẻ có trong lớp nhưng chưa được điểm danh trước đó
                $childIdsNew = array_diff($allChildIds, $attendanceChildIds);
                foreach ($childIdsNew as $childId) {
                    $childrenAtt[$childId]['status'] = '';
                    $childrenAtt[$childId]['reason'] = '';
                    $childrenAtt[$childId]['is_parent'] = '';
                    $childrenAtt[$childId]['feedback'] = '';
                    $childrenAtt[$childId]['attendance_detail_id'] = '';
                }

                //print_r($childrenAtt); die;
                // Lặp danh sách id trẻ và trạng thái điểm danh
                foreach ($childrenAtt as $childId => $att) {
                    // kiểm tra nếu trạng thái khác thì thông báo update, nếu không có trạng thái điểm danh trước đó thì thông báo điểm danh mới
                    if(in_array($childId, $allChildIds) && $att['status'] != '') {
                        // trẻ đã điểm danh trước đó - update
                        // Lặp danh sách id gửi lên

                        $parents = getChildData($childId, CHILD_PARENTS);
                        foreach ($allChildIds as $k => $id) {
                            // Nếu phụ huynh xin nghỉ mà chưa xác nhận
                            if($id == $childId && $att['is_parent'] == 1 && $att['feedback'] == 0) {
                                // Cập nhật trạng thái đã xác nhận và gửi thông báo về cho phụ huynh
                                $attendanceDao->updateAttendanceDetailStatus($att['attendance_detail_id']);
                                $parentIds = array_keys($parents);
                                $userDao->postNotifications($parentIds, NOTIFICATION_ATTENDANCE_CONFIRM, NOTIFICATION_NODE_TYPE_CHILD, $att['attendance_detail_id'], '', $childId);
                            }

                            // Nếu thay đổi trạng thái điểm danh thì gửi thông báo update điểm danh
                            if($id == $childId && ($allStatus[$k] != $att['status'] || $allReasons[$k] != $att['reason'])) {
                                // Cập nhật chi tiết điểm danh
                                $attendanceDao->updateChildAttendanceDetail($attendanceId, $childId, $allStatus[$k], $allReasons[$k]);

                                // Thông báo cập nhật điểm danh
                                //$child = $childDao->getChild($_POST['child_id']);
                                $child = getChildData($childId, CHILD_INFO);

                                if (!is_null($child) && !is_null($parents)) {
                                    foreach ($parents as $parent) {
                                        $userDao->postNotifications($parent['user_id'], NOTIFICATION_UPDATE_ATTENDANCE, NOTIFICATION_NODE_TYPE_CHILD,
                                            $allStatus[$k], $args['attendance_date'], $childId, convertText4Web($child['child_name']));
                                    }
                                }
                            }
                        }
                    } else {
                        if(in_array($childId, $allChildIds)) {
                            // Lấy trạng thái điểm danh của trẻ
                            foreach ($allChildIds as $k => $allChildId) {
                                if($allChildId == $childId) {
                                    $status = $allStatus[$k];
                                    $reason = $allReasons[$k];
                                }
                            }

                            // insert thông tin điểm danh của trẻ vào bảng ci_attendance_detail
                            $attendanceDao->saveAttendanceDetailChild($attendanceId, $childId, $status, $reason);

                            // Thông báo đến phụ huynh trẻ được điểm danh mới
                            //$child = $childDao->getChild($_POST['child_id']);
                            $child = getChildData($childId, CHILD_INFO);
                            //$parents = array();
                            //$parents = $parentDao->getParent($_POST['child_id']);
                            $parents = getChildData($childId, CHILD_PARENTS);

                            if (!is_null($child) && !is_null($parents)) {
                                $parentIds = array_keys($parents);
                                $userDao->postNotifications($parentIds, NOTIFICATION_NEW_ATTENDANCE, NOTIFICATION_NODE_TYPE_CHILD,
                                    $status, $args['attendance_date'], $childId, convertText4Web($child['child_name']));
                            }
                        }
                    }
                }

                $attendanceDao->updateAttendance($args);
            } else {
                $attendanceId = $attendanceDao->insertAttendance($args);

                foreach ($allChildIds as $k => $childId) {
                    // Lặp danh sách trẻ, gửi thông báo về cho phụ huynh
                    // Thông báo điểm danh
                    //$child = $childDao->getChild($_POST['child_id']);
                    $child = getChildData($childId, CHILD_INFO);
                    //$parents = array();
                    //$parents = $parentDao->getParent($_POST['child_id']);
                    $parents = getChildData($childId, CHILD_PARENTS);

                    if (!is_null($child) && !is_null($parents)) {
                        $parentIds = array_keys($parents);
                        $userDao->postNotifications($parentIds, NOTIFICATION_NEW_ATTENDANCE, NOTIFICATION_NODE_TYPE_CHILD,
                            $allStatus[$k], $args['attendance_date'], $childId, convertText4Web($child['child_name']));
                    }
                }

                // Insert điểm danh của cả lớp
                //$allResignTime = $_POST['allResignTime'];
                //$allIsParent = $_POST['allIsParent'];
                //$allRecordUserId = $_POST['allRecordUserId'];
                //$allStartDate = $_POST['allStartDate'];
                //$allEndDate = $_POST['allEndDate'];
//                foreach ($allChildIds as $child_id) {
//                    $allFeedback[] = $_POST['feedback_' . $child_id];
//                }
                $attendanceDao->saveAttendanceDetail($attendanceId, $allChildIds, $allStatus, $allReasons);

                // TaiLA  - Tạo hàm lưu thông tin điểm danh chi tiết mới
                //$attendanceDao->saveAttendanceDetailNew($attendanceId, $allChildIds, $allStatus, $allReasons, $allResignTime, $allIsParent, $allRecordUserId, $allStartDate, $allEndDate, $allFeedback);
            }

            $db->commit();
            return_json( array('success' => true, 'message' => __("Class attendance have been updated")) );
            break;

        case 'save_rollup_back': //Xử lý nghiệp vụ khi lưu ĐIỂM DANH về NGÀY CỦA MỘT LỚP
            if (!canEdit($_POST['school_username'], 'attendance')) {
                _error(403);
            }

            $args = array();
            $args['attendance_date'] = $_POST['attendance_date'];
            $attendanceDate = toDBDate($args['attendance_date']);
            $dateNow = toDBDate(toSysDate($date));

            $args['class_id'] = $_POST['class_id'];

            //Kiểm tra xem lớp đã có bản ghi điểm danh về chưa
            $attendanceBackId = $attendanceDao->getAttendanceBackId($args['class_id'], $args['attendance_date']);

            $allChildIds = $_POST['allChildBackIds'];
            $allStatus = array();
            foreach ($allChildIds as $child_id) {
                if(isset($_POST['status_' . $child_id])) {
                    $allStatus[] = 1;
                } else {
                    $allStatus[] = 0;
                }

            }

            //$allStatus = $_POST['allStatus'];
            $allCameBackTimes = $_POST['cameback_time'];
            $allCameBackNotes = $_POST['cameback_note'];
            // echo "<pre>"; print_r($allStatus); die;
            $cameBackCount = 0;
            $notCameBackCount = 0;
            foreach ($allStatus as $status) {
                if ($status == 1) {
                    $cameBackCount++;
                } else {
                    //ATTENDANCE_PRESENT, ATTENDANCE_COME_LATE, ATTENDANCE_EARLY_LEAVE
                    $notCameBackCount++;
                }
            }

            $args['cameback_count'] = $cameBackCount;
            $args['not_cameback_count'] = $notCameBackCount;

            $db->begin_transaction();
            if ($attendanceBackId > 0) {
                // Lấy trạng thái điểm danh về của cả lớp theo attendance_back_id
                $attendance = $attendanceDao->getAttendanceBack($attendanceBackId, $school['page_id']);

                // Lặp danh sách điểm danh chi tiết, đưa vào mảng để tiện so sánh
                $childrenAtt = array();
                $childCameBack = array();
                foreach ($attendance['detail'] as $row) {
                    if(isset($row['attendance_back_detail_id']) && $row['attendance_back_detail_id'] > 0) {
                        $childrenAtt[$row['child_id']] = $row['status'];
                    } else {
                        $childrenAtt[$row['child_id']] = '';
                    }
                    $childCameBack[] = $row['child_id'];
                }

                foreach ($allChildIds as $k => $childId) {
                    if(in_array($childId, $childCameBack)) {
                        if($allStatus[$k] == 0) {
                            // Gửi thông báo update trẻ chưa về
                        } else {
                            // Không gửi thông báo gì
                        }
                    } else {
                        $status = $allStatus[$k];
                        if($status == 1) {
                            // Thông báo đến phụ huynh trẻ được điểm danh về
                            //$child = $childDao->getChild($_POST['child_id']);
                            $child = getChildData($childId, CHILD_INFO);
                            //$parents = array();
                            //$parents = $parentDao->getParent($_POST['child_id']);
                            $parents = getChildData($childId, CHILD_PARENTS);

                            if (!is_null($child) && !is_null($parents)) {
                                $parentIds = array();
                                foreach ($parents as $parent) {
                                    $parentIds[] = $parent['user_id'];
                                }
                                $userDao->postNotifications($parentIds, NOTIFICATION_NEW_ATTENDANCE_BACK, NOTIFICATION_NODE_TYPE_CHILD,
                                    $status, $allCameBackTimes[$k], $childId, convertText4Web($child['child_name']));
                            }
                        }
                    }
                }

                $attendanceDao->updateAttendanceBack($args);
                $attendanceId = $attendanceBackId;
            } else {
                $attendanceId = $attendanceDao->insertAttendanceBack($args);
                foreach ($allChildIds as $k => $childId) {
                    if($allStatus[$k] == 1) {
                        // Lặp danh sách trẻ, gửi thông báo về cho phụ huynh
                        // Thông báo điểm danh
                        //$child = $childDao->getChild($_POST['child_id']);
                        $child = getChildData($childId, CHILD_INFO);
                        //$parents = array();
                        //$parents = $parentDao->getParent($_POST['child_id']);
                        $parents = getChildData($childId, CHILD_PARENTS);
                        if (!is_null($child) && !is_null($parents)) {
                            $parentIds = array();
                            foreach ($parents as $parent) {
                                $parentIds[] = $parent['user_id'];
                            }
                            $userDao->postNotifications($parentIds, NOTIFICATION_NEW_ATTENDANCE_BACK, NOTIFICATION_NODE_TYPE_CHILD,
                                $allStatus[$k], $allCameBackTimes[$k], $childId, convertText4Web($child['child_name']));
                        }
                    }
                }
            }
            $attendanceDao->saveAttendanceBackDetail($attendanceId, $allChildIds, $allStatus, $allCameBackTimes, $allCameBackNotes);

//            echo "<pre>";
//            print_r($allCameBackTimes); die;
            $db->commit();
            return_json( array('success' => true, 'message' => __("Class attendance have been updated")) );
            break;

        case 'save_child_rollup': //Lưu thông tin điểm danh của một trẻ
            if (!canEdit($_POST['school_username'], 'attendance')) {
                _error(403);
            }
            if(!isset($_POST['child_id']) || !is_numeric($_POST['child_id'])) {
                _error(404);
            }

            $attendanceIds = isset($_POST['attendanceIds'])? $_POST['attendanceIds']: array();
            $childId = $_POST['child_id'];
            $allStatus = $_POST['allStatus'];
            $allReasons = $_POST['allReasons'];
            $allOldStatus = $_POST['allOldStatus'];

            if (count($attendanceIds) > 0) {
                $db->begin_transaction();

                $attendanceDao->updateAttendanceOfChild($childId, $attendanceIds, $allStatus, $allOldStatus, $allReasons);

                $db->commit();
                return_json( array('success' => true, 'message' => __("Done, Student attendance have been updated")) );
            }
            break;
        case 'delete':
            if (!canEdit($_POST['school_username'], 'attendance')) {
                _error(403);
            }
            $db->autocommit(false);
            $db->begin_transaction();
            $attendanceDao->deleteAttendance($_POST['attendance_id']);
            $db->commit();
            //CHỖ NÀY KHÔNG CÓ BREAK ĐỂ NÓ CHẠY CASE DƯỚI LUÔN. KHÔNG ĐƯỢC CHÈN CASE VÀO GIỮA 'delete' VÀ 'classatt'
        case 'classatt':
            if (!canView($_POST['school_username'], 'attendance')) {
                _error(403);
            }
            if (validateDate($_POST['fromDate']) && validateDate($_POST['toDate'])) {
                //Lấy ra điểm danh của một lớp
                $results = $attendanceDao->getFullAttendanceOfClass($_POST['class_id'], $_POST['fromDate'], $_POST['toDate']);

                //Tạo ra danh sách các ngày để build lên header của bảng.
                $fromDate = strtotime(toDBDate($_POST['fromDate']));
                $toDate = strtotime(toDBDate($_POST['toDate']));

                //$dates = array();
                $displayDates = array();
                $displayDays = array();
                while ($fromDate <= $toDate) {
                    $day = getDayShortOfDateFun($fromDate);
                    $displayDays[] = $day;

                    $parts = explode("-",date("Y-m-d", $fromDate));
                    $displayDates[] = $parts[2];

                    //$dates[] = $fromDate;
                    $fromDate = strtotime("+1 day", $fromDate);
                }
                $smarty->assign('dates', $displayDates);
                $smarty->assign('days', $displayDays);

                //Mãng 2 chiều lưu bảng điểm danh toàn lớp
                $newResults = array();

                //Băm danh sách kết quả thành mãng 2 chiều theo từng cháu
                $lastChildId = -1;
                $aRow = array(); //Dòng lưu thông tin điểm danh 1 trẻ (chỉ những ngày có điểm danh).
                foreach ($results as $result) {
                    //Nếu child_id thay đổi thì bắt đầu một dòng mới.
                    if ($lastChildId != $result['child_id']) {

                        //Nếu trước đó là một dòng (không phải là dòng đầu tiên), thì xử lý bổ xung dòng đã xong trước đó.
                        if ($lastChildId > 0) {
                            $isFirstCell = true; //Check có phải cell đầu dòng hay không
                            $newRow = array(); //Dòng lưu thông tin điểm danh 1 trẻ (bao gồm những ngày không điểm danh).
                            $newCells = array();
                            $lastDate = strtotime(toDBDate($_POST['fromDate']));
                            foreach ($aRow as $cell) {
                                //Xét ngày đầu hàng là ngày nghỉ thì thêm thành phần vào
                                while ($isFirstCell && ($lastDate < strtotime($cell['attendance_date']))) {
                                    $newCells[] = array("is_checked" => 0);
                                    $lastDate = strtotime("+1 day", $lastDate);
                                }

                                //Xét thành phần giữa hàng, nếu có ngày nghỉ thì chèn thêm vào
                                while ((!$isFirstCell) && (strtotime("+1 day", $lastDate) < strtotime($cell['attendance_date']))) {
                                    $newCells[] = array("is_checked" => 0);
                                    $lastDate = strtotime("+1 day", $lastDate);
                                }

                                $newCells[] = $cell;
                                $newRow['child_name'] = $cell['child_name'];
                                $newRow['child_id'] = $cell['child_id'];
                                $newRow['child_status'] = $cell['child_status'];
                                $isFirstCell = false;
                                $lastDate = strtotime($cell['attendance_date']);
                            }
                            //Xét cuối hàng, nếu những ngày cuối hàng là ngày nghỉ thì thêm ô trống
                            while ($lastDate < strtotime(toDBDate($_POST['toDate']))) {
                                $newCells[] = array("is_checked" => 0);
                                $lastDate = strtotime("+1 day", $lastDate);
                            }
                            $newRow['cells'] = $newCells;
                            $newResults[] = $newRow;
                        }
                        $aRow = array();
                    }

                    $aRow[] = $result;
                    $lastChildId = $result['child_id'];
                }

                //Xử lý hàng cuối cùng (cháu cuối cùng).
                $summaries = array(); //Lưu thống kê theo ngày của lớp
                if ($lastChildId > 0) {
                    $isFirstCell = true;
                    $newRow = array(); //Dòng lưu thông tin điểm danh 1 trẻ (bao gồm những ngày không điểm danh).
                    $newCells = array();
                    $lastDate = strtotime(toDBDate($_POST['fromDate']));
                    foreach ($aRow as $cell) {
                        //Xét ngày đầu hàng là ngày nghỉ thì thêm thành phần vào
                        while ($isFirstCell && ($lastDate < strtotime($cell['attendance_date']))) {
                            $newCells[] = array("is_checked" => 0);
                            $lastDate = strtotime("+1 day", $lastDate);
                        }

                        //Xét thành phần giữa hàng, nếu có ngày nghỉ thì chèn thêm vào
                        while ((!$isFirstCell) && (strtotime("+1 day", $lastDate) < strtotime($cell['attendance_date']))) {
                            $newCells[] = array("is_checked" => 0);
                            $lastDate = strtotime("+1 day", $lastDate);
                        }

                        $newCells[] = $cell;
                        $newRow['child_name'] = $cell['child_name'];
                        $newRow['child_id'] = $cell['child_id'];
                        $newRow['child_status'] = $cell['child_status'];

                        $isFirstCell = false;
                        $lastDate = strtotime($cell['attendance_date']);
                    }
                    //Xét cuối hàng, nếu những ngày cuối hàng là ngày nghỉ thì thêm ô trống
                    while ($lastDate < strtotime(toDBDate($_POST['toDate']))) {
                        $newCells[] = array("is_checked" => 0);
                        $lastDate = strtotime("+1 day", $lastDate);
                    }
                    $newRow['cells'] = $newCells;
                    $newResults[] = $newRow;

                    ///// Xử lý phần tổng hợp cuối cùng ----------
                    $attendances = $attendanceDao->getAttendanceOfClass4Summary($_POST['class_id'], $_POST['fromDate'], $_POST['toDate']);
                    $isFirstCell = true;
                    $lastDate = strtotime(toDBDate($_POST['fromDate']));
                    foreach ($attendances as $cell) {
                        //Xét ngày đầu hàng là ngày nghỉ thì thêm thành phần vào
                        while ($isFirstCell && ($lastDate < strtotime($cell['attendance_date']))) {
                            $lastDate = strtotime("+1 day", $lastDate);
                            $summaries[] = array("is_checked" => 0);
                        }
                        //Xét thành phần giữa hàng, nếu có ngày nghỉ thì chèn thêm vào
                        while ((!$isFirstCell) && (strtotime("+1 day", $lastDate) < strtotime($cell['attendance_date']))) {
                            $lastDate = strtotime("+1 day", $lastDate);
                            $summaries[] = array("is_checked" => 0);
                        }
                        $isFirstCell = false;
                        $lastDate = strtotime($cell['attendance_date']);

                        $summaries[] = $cell;
                    }
                    //Xét cuối hàng, nếu những ngày cuối hàng là ngày nghỉ thì thêm ô trống
                    while ($lastDate < strtotime(toDBDate($_POST['toDate']))) {
                        $lastDate = strtotime("+1 day", $lastDate);
                        $summaries[] = array("is_checked" => 0);
                    }
                }

                $smarty->assign('rows', $newResults);
                $smarty->assign('last_rows', $summaries);
                $smarty->assign('canEdit', canEdit($_POST['school_username'], 'attendance'));
                $smarty->assign('school', $school);
                $return['results'] = $smarty->fetch("ci/school/ajax.school.attendancefullclass.tpl");
            } else {
                $return['results'] = __("Invalid input");
            }
            return_json($return);
            break;
        case 'search_aday':
            if (!canView($_POST['school_username'], 'attendance')) {
                _error(403);
            }
            $results = $attendanceDao->getAttendanceOfSchoolADay($school['page_id'], $_POST['attendanceDate']);
            $smarty->assign('rows', $results);

//            $canEdit = canEdit($_POST['school_username'], 'attendance');
//            $smarty->assign('canEdit', $canEdit);
            $return['results'] = $smarty->fetch("ci/school/ajax.school.attendanceaday.tpl");

            return_json($return);
            break;
        case 'child': //Hiển thị danh sách điểm danh một trẻ trong khoảng thời gian.
            if (!canView($_POST['school_username'], 'attendance')) {
                _error(403);
            }
            $data = array();
            $attendances = $attendanceDao->getAttendanceChild($_POST['child_id'], $_POST['fromDate'], $_POST['toDate']);
            if (count($attendances) > 0) {
                $data['attendance'] = $attendances;
                $child = getChildData($_POST['child_id'], CHILD_INFO);
                $data['child'] = $child;
                $smarty->assign('data', $data);
                $smarty->assign('school', $school);
                /* return */
                $return['results'] = $smarty->fetch("ci/school/ajax.school.attendance.child.tpl");
                $return['disableSave'] = ((!canEdit($_POST['school_username'], 'attendance')) || (count($data['attendance']['attendance']) == 0));
            }

            return_json($return);
            break;
        case 'list_child':
            if(!isset($_POST['class_id']) || !is_numeric($_POST['class_id'])) {
                _error(404);
            }

            $children = $childDao->getChildrenOfClass4ComboBox($_POST['class_id']);
            $children = sortMultiOrderArray($children, [["child_status", "DESC"], ["name_for_sort", "ASC"]]);
            $results = "";
            $child_status = -1;
            $idx = 1;
            foreach ($children as $child) {
                if (($child_status >= 0) && ($child_status != $child['child_status'])) {
                    $results = $results.'<option value="" disabled>----------'.__('Left children').'----------</option>';
                }
                $results = $results.'<option value="'.$child['child_id'].'">'.($idx++)." - ".$child['child_name'].' - '.$child['birthday'].'</option>';
                $child_status = $child['child_status'];
            }


            $return['results'] = $results;
            $return['disableSearch'] = (count($children) == 0);

            return_json($return);
            break;
        case 'conabs':
            if (!canView($_POST['school_username'], 'attendance')) {
                _error(403);
            }
            if (!is_numeric($_POST['daynum']) || $_POST['daynum'] <= 0) {
                throw new Exception(__("Please select a valid min number of days"));
            }
            $daynum = $_POST['daynum'];
            if($daynum < 1 || $daynum > 31) {
                throw new Exception(__("The number of consecutive absent day must be greater than 0 and less than 32"));
            }
            $children = $attendanceDao->getConsecutiveAbsentChild($school['page_id'], $daynum);
            $tmpArray = array();
            while ((count($children) > 0) && (MAX_CONSECUTIVE_ABSENT_DAY >= $daynum)) {
                $tmpArray[$daynum] = $children;
                $daynum++;
                if (MAX_CONSECUTIVE_ABSENT_DAY >= $daynum) {
                    $children = $attendanceDao->getConsecutiveAbsentChild($school['page_id'], $daynum);
                }
            }
            $newArrays = array();
            while ($daynum > $_POST['daynum']) {
                $daynum--;
                foreach ($tmpArray[$daynum] as $childOut) {
                    $found = false;
                    foreach ($newArrays as $childIn) {
                        if ($childOut['child_id'] == $childIn['child_id']) {
                            $found = true;
                            break;
                        }
                    }
                    if (!$found) {
                        $newChild = $childOut;
                        $newChild['number_of_absent_day'] = $daynum;
                        $newArrays[] = $newChild;
                    }
                }
            }

            // lặp danh sách trẻ nghỉ nhiều liên tục lấy lý do nghỉ
            // Lấy ngày điểm danh cuối cùng của trường
            $dates = $attendanceDao->getLastAttendanceDates($school['page_id'], $daynum);
            $toDate = $dates[0];
            $endDate = toSysDate($toDate);

            // Khai báo mảng mới
            $resultsAb = array();
            foreach ($newArrays as $row) {
                $beginDate = date('Y-m-d', strtotime('- ' . ($row['number_of_absent_day'] + 2) . ' days', strtotime($toDate)));
                $beginDate = toSysDate($beginDate);
                $attendances = $attendanceDao->getAttendanceChild($row['child_id'], $beginDate, $endDate);
                $reason = '';

                // LẶp mảng thông tin điểm danh của trẻ laayslys do nghỉ (lấy lần cuối cùng có lý do)
                foreach ($attendances['attendance'] as $value) {
                    if($value['reason'] != '') {
                        $reason = $value['reason'];
                        break;
                    }
                }
                $row['reason'] = $reason;
                $resultsAb[] = $row;
            }
            $smarty->assign('children', $resultsAb);
            $return['results'] = $smarty->fetch("ci/school/ajax.school.attendance.conabs.tpl");
            return_json($return);
            break;

        case 'confirm':
            $db->begin_transaction();
            //Cập nhật trại thái
            $attendanceDao->updateAttendanceDetailStatus($_POST['attendance_detail_id']);
            //Thông báo phụ huynh
            //$parentIds = $parentDao->getParentIds($_POST['child_id']);
            $parents = getChildData($_POST['child_id'], CHILD_PARENTS);
            $parentIds = array_keys($parents);
            $userDao->postNotifications($parentIds, NOTIFICATION_ATTENDANCE_CONFIRM, NOTIFICATION_NODE_TYPE_CHILD, $_POST['attendance_detail_id']);

            $db->commit();

            return_json($return);
            //return_json(array('callback' => 'window.location = "'.$system['system_url'].'/class/'.$_POST['username'].'/attendance";'));
            break;
        case 'change_attendance_status':
            $db->begin_transaction();
            if($_POST['attendance_status'] != $_POST['old_status']) {
                // Cập nhật trạng thái điểm danh
                $attendanceDao->updateChildAttendanceStatus($_POST['id'], $_POST['child_id'], $_POST['attendance_status'], $_POST['old_status']);
            }

            $db->commit();
            return_json($return);
            break;
        case 'export_attendance_class':
            $reportTemplate = ABSPATH."content/templates/attendance.xlsx";
            include_once(ABSPATH . 'includes/libs/PHPExcel/PHPExcel/IOFactory.php');
            include_once(ABSPATH . 'includes/libs/PHPExcel/PHPExcel.php');

            $objPHPExcel = PHPExcel_IOFactory::load($reportTemplate);
            //Xử lý sheet cả lớp.
            if(!isset($_POST['class_id']) || !is_numeric($_POST['class_id'])) {
                throw new Exception(__("Please select a class"));
            }
            $COLUMN_OF_EACH_TYPE = 10;
            $START_ROW = 6;
            $sheet = $objPHPExcel->getSheet(0);
            //Lấy ra điểm danh của một lớp
            $results = $attendanceDao->getFullAttendanceOfClass($_POST['class_id'], $_POST['fromDate'], $_POST['toDate']);

            $class = getClassData($_POST['class_id'], CLASS_INFO);

            //Tạo ra danh sách các ngày để build lên header của bảng.
            $fromDate = strtotime(toDBDate($_POST['fromDate']));
            $toDate = strtotime(toDBDate($_POST['toDate']));

            //$dates = array();
            $displayDates = array();
            $dateArrays = array();
            while ($fromDate <= $toDate) {
                $temp = array();
                $parts = explode("-",date("Y-m-d", $fromDate));
                $displayDates[] = $parts[2];
                $temp['displayDate'] = $parts[2];
                $temp['dbDate'] = date("Y-m-d", $fromDate);
                //$dates[] = $fromDate;
                $dateArrays[] = $temp;
                $fromDate = strtotime("+1 day", $fromDate);
            }

            //Mãng 2 chiều lưu bảng điểm danh toàn lớp
            $newResults = array();

            //Băm danh sách kết quả thành mãng 2 chiều theo từng cháu
            $lastChildId = -1;
            $aRow = array(); //Dòng lưu thông tin điểm danh 1 trẻ (chỉ những ngày có điểm danh).
            foreach ($results as $result) {
                //Nếu child_id thay đổi thì bắt đầu một dòng mới.
                if ($lastChildId != $result['child_id']) {

                    //Nếu trước đó là một dòng (không phải là dòng đầu tiên), thì xử lý bổ xung dòng đã xong trước đó.
                    if ($lastChildId > 0) {
                        $isFirstCell = true; //Check có phải cell đầu dòng hay không
                        $newRow = array(); //Dòng lưu thông tin điểm danh 1 trẻ (bao gồm những ngày không điểm danh).
                        $newCells = array();
                        $lastDate = strtotime(toDBDate($_POST['fromDate']));
                        foreach ($aRow as $cell) {
                            //Xét ngày đầu hàng là ngày nghỉ thì thêm thành phần vào
                            while ($isFirstCell && ($lastDate < strtotime($cell['attendance_date']))) {
                                $newCells[] = array("is_checked" => 0);
                                $lastDate = strtotime("+1 day", $lastDate);
                            }

                            //Xét thành phần giữa hàng, nếu có ngày nghỉ thì chèn thêm vào
                            while ((!$isFirstCell) && (strtotime("+1 day", $lastDate) < strtotime($cell['attendance_date']))) {
                                $newCells[] = array("is_checked" => 0);
                                $lastDate = strtotime("+1 day", $lastDate);
                            }

                            $newCells[] = $cell;
                            $newRow['child_name'] = $cell['child_name'];
                            $newRow['child_id'] = $cell['child_id'];
                            $newRow['child_status'] = $cell['child_status'];
                            $isFirstCell = false;
                            $lastDate = strtotime($cell['attendance_date']);
                        }
                        //Xét cuối hàng, nếu những ngày cuối hàng là ngày nghỉ thì thêm ô trống
                        while ($lastDate < strtotime(toDBDate($_POST['toDate']))) {
                            $newCells[] = array("is_checked" => 0);
                            $lastDate = strtotime("+1 day", $lastDate);
                        }
                        $newRow['cells'] = $newCells;
                        $newResults[] = $newRow;
                    }
                    $aRow = array();
                }

                $aRow[] = $result;
                $lastChildId = $result['child_id'];
            }

            //Xử lý hàng cuối cùng (cháu cuối cùng).
            $summaries = array(); //Lưu thống kê theo ngày của lớp
            if ($lastChildId > 0) {
                $isFirstCell = true;
                $newRow = array(); //Dòng lưu thông tin điểm danh 1 trẻ (bao gồm những ngày không điểm danh).
                $newCells = array();
                $lastDate = strtotime(toDBDate($_POST['fromDate']));
                foreach ($aRow as $cell) {
                    //Xét ngày đầu hàng là ngày nghỉ thì thêm thành phần vào
                    while ($isFirstCell && ($lastDate < strtotime($cell['attendance_date']))) {
                        $newCells[] = array("is_checked" => 0);
                        $lastDate = strtotime("+1 day", $lastDate);
                    }

                    //Xét thành phần giữa hàng, nếu có ngày nghỉ thì chèn thêm vào
                    while ((!$isFirstCell) && (strtotime("+1 day", $lastDate) < strtotime($cell['attendance_date']))) {
                        $newCells[] = array("is_checked" => 0);
                        $lastDate = strtotime("+1 day", $lastDate);
                    }

                    $newCells[] = $cell;
                    $newRow['child_name'] = $cell['child_name'];
                    $newRow['child_id'] = $cell['child_id'];
                    $newRow['child_status'] = $cell['child_status'];

                    $isFirstCell = false;
                    $lastDate = strtotime($cell['attendance_date']);
                }
                //Xét cuối hàng, nếu những ngày cuối hàng là ngày nghỉ thì thêm ô trống
                while ($lastDate < strtotime(toDBDate($_POST['toDate']))) {
                    $newCells[] = array("is_checked" => 0);
                    $lastDate = strtotime("+1 day", $lastDate);
                }
                $newRow['cells'] = $newCells;
                $newResults[] = $newRow;

                ///// Xử lý phần tổng hợp cuối cùng ----------
                $attendances = $attendanceDao->getAttendanceOfClass4Summary($_POST['class_id'], $_POST['fromDate'], $_POST['toDate']);
                $isFirstCell = true;
                $lastDate = strtotime(toDBDate($_POST['fromDate']));
                foreach ($attendances as $cell) {
                    //Xét ngày đầu hàng là ngày nghỉ thì thêm thành phần vào
                    while ($isFirstCell && ($lastDate < strtotime($cell['attendance_date']))) {
                        $lastDate = strtotime("+1 day", $lastDate);
                        $summaries[] = array("is_checked" => 0);
                    }
                    //Xét thành phần giữa hàng, nếu có ngày nghỉ thì chèn thêm vào
                    while ((!$isFirstCell) && (strtotime("+1 day", $lastDate) < strtotime($cell['attendance_date']))) {
                        $lastDate = strtotime("+1 day", $lastDate);
                        $summaries[] = array("is_checked" => 0);
                    }
                    $isFirstCell = false;
                    $lastDate = strtotime($cell['attendance_date']);

                    $summaries[] = $cell;
                }
                //Xét cuối hàng, nếu những ngày cuối hàng là ngày nghỉ thì thêm ô trống
                while ($lastDate < strtotime(toDBDate($_POST['toDate']))) {
                    $lastDate = strtotime("+1 day", $lastDate);
                    $summaries[] = array("is_checked" => 0);
                }
            }

            // 1. Điền thông tin ngày vào header
            $newColumns = array();
            $col = 2;
            foreach ($dateArrays as $dateA) {
                $sheet->setCellValueByColumnAndRow($col, 5, $dateA['displayDate']);
                $dateA['col'] = $col;
                $newColumns['attendance_date' . "_" . $dateA['dbDate']] = $dateA;
                $col = $col + 1;
            }

            // 2. Điền thông tin trẻ
            $childCnt = 1;
            $rowNumber = $START_ROW;
            foreach ($newResults as $child) { //Điền dữ liệu của từng trẻ một
                $presentCnt = 0;
                $absenceCnt = 0;

                //Điền tên trẻ
                $sheet->setCellValue('A' . $rowNumber, $childCnt)
                    ->setCellValue('B' . $rowNumber, convertText4Web($child['child_name']));
                //$sheet->getHyperlink('B' . $rowNumber)->setUrl("sheet://'" . $sheetName . "'!A1");
                $childCnt++;

                foreach ($child['cells'] as $cell) {
                    $column = $newColumns['attendance_date' . "_" . $cell['attendance_date']];
                    if($cell['is_checked']) {
                        if($cell['status'] == ATTENDANCE_PRESENT) {
                            $sheet->setCellValueByColumnAndRow($column['col'], $rowNumber, '✓');
                            $presentCnt++;
                        } elseif ($cell['status'] == ATTENDANCE_COME_LATE){
                            $sheet->setCellValueByColumnAndRow($column['col'], $rowNumber, '✓');
                            $presentCnt++;
                        } elseif ($cell['status'] == ATTENDANCE_EARLY_LEAVE){
                            $sheet->setCellValueByColumnAndRow($column['col'], $rowNumber, '✓');
                            $presentCnt++;
                        } elseif ($cell['status'] == ATTENDANCE_ABSENCE_NO_REASON){
                            $sheet->setCellValueByColumnAndRow($column['col'], $rowNumber, 'X');
                            $absenceCnt++;
                        } else {
                            $sheet->setCellValueByColumnAndRow($column['col'], $rowNumber, 'X');
                            $absenceCnt++;
                        }
                    }
                    else{
                        //$sheet->setCellValueByColumnAndRow($column['col'], $rowNumber, '');
                    }
                }
                $col_end = $col;
                $sheet->setCellValueByColumnAndRow($col_end, $rowNumber, $presentCnt);
                $col_end++;
                $sheet->setCellValueByColumnAndRow($col_end, $rowNumber, $absenceCnt);
                $rowNumber++;
            }
            // merge cell cột A và B
            $sheet->mergeCells('A' . $rowNumber . ':B' . $rowNumber);
            $sheet->setCellValue('A' . $rowNumber, 'Tổng đi học của lớp');
            $totalClassPre = 0;
            $totalClassAbs = 0;
            // Set tổng của cả lớp
            foreach ($summaries as $summary) {
                $columnTotal = $newColumns['attendance_date' . "_" . $summary['attendance_date']];
                if($summary['is_checked']) {
                    $sheet->setCellValueByColumnAndRow($columnTotal['col'], $rowNumber, $summary['present_count']);
                    $totalClassPre = $totalClassPre + $summary['present_count'];
                }
            }

            $col_end_pre = $col;
            $sheet->setCellValueByColumnAndRow($col_end_pre, $rowNumber, $totalClassPre);
            $rowNumber++;
            $sheet->mergeCells('A' . $rowNumber . ':B' . $rowNumber);
            $sheet->setCellValue('A' . $rowNumber, 'Tổng nghỉ học của lớp');
            foreach ($summaries as $summary) {
                $columnTotal = $newColumns['attendance_date' . "_" . $summary['attendance_date']];
                if($summary['is_checked']) {
                    $sheet->setCellValueByColumnAndRow($columnTotal['col'], $rowNumber, $summary['absence_count']);
                    $totalClassAbs = $totalClassAbs + $summary['absence_count'];
                }
            }
            $col_end_abs = $col + 1;
            $sheet->setCellValueByColumnAndRow($col_end_abs, $rowNumber, $totalClassAbs);

            $strEndCol = PHPExcel_Cell::stringFromColumnIndex($col_end_abs);
            $strEndColPre = PHPExcel_Cell::stringFromColumnIndex($col_end_pre);
            // Set tên trường
            $sheet->mergeCells('A1:' . $strEndCol . 1);
            $sheet->setCellValue('A1', mb_strtoupper(convertText4Web(str_replace('/', '_', $school['page_title'])), "UTF-8"));

            // Set ngày điểm danh
            $sheet->mergeCells('C2:' . $strEndCol . 2);
            $sheet->setCellValue('C2', 'Từ ' . $_POST['fromDate'] . ' đến ' . $_POST['toDate']);

            // Set tên lớp
            $sheet->mergeCells('C3:' . $strEndCol . 3);
            $sheet->setCellValue('C3', convertText4Web($class['group_title']));

            //Set boarder cho toàn bộ bảng
            $all_border_style = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('rgb' => '333333'))));
            $sheet->getStyle('A5:'.$strEndCol.$rowNumber)->applyFromArray($all_border_style);

            // Set font cho bảng
            $sheet->getStyle('A6:' .$strEndCol.$rowNumber)->applyFromArray(styleArray('Times new Roman', false, false, 11, PHPExcel_Style_Alignment::HORIZONTAL_CENTER, PHPExcel_Style_Alignment::VERTICAL_CENTER, true));
            //set cột tên
            $sheet->getStyle('A6:A' .$rowNumber)->applyFromArray(styleArray('Times new Roman', true, false, 11, PHPExcel_Style_Alignment::HORIZONTAL_CENTER, PHPExcel_Style_Alignment::VERTICAL_CENTER, true));
            //set cột tên
            $sheet->getStyle('B6:B' .$rowNumber)->applyFromArray(styleArray('Times new Roman', false, false, 11, PHPExcel_Style_Alignment::HORIZONTAL_LEFT, PHPExcel_Style_Alignment::VERTICAL_CENTER, true));

            $sheet->getStyle('A5:' .$strEndCol .'5')->applyFromArray(styleArray('Times new Roman', true, false, 11, PHPExcel_Style_Alignment::HORIZONTAL_CENTER, PHPExcel_Style_Alignment::VERTICAL_CENTER, true));

            //set hàng tổng của lớp
            $rowNumberPre = $rowNumber - 1;
            $sheet->getStyle('A' .$rowNumberPre . ':A' . $rowNumber)->applyFromArray(styleArray('Times new Roman', true, false, 11, PHPExcel_Style_Alignment::HORIZONTAL_RIGHT, PHPExcel_Style_Alignment::VERTICAL_CENTER, true));
            $sheet->getStyle('C' .$rowNumberPre . ':' . $strEndCol . $rowNumber)->applyFromArray(styleArray('Times new Roman', true, false, 11, PHPExcel_Style_Alignment::HORIZONTAL_CENTER, PHPExcel_Style_Alignment::VERTICAL_CENTER, true));

            // Set ô đi học và nghỉ học của trẻ
            $sheet->setCellValue($strEndColPre . '5', convertText4Web('Đi học'));
            // Set ô đi học và nghỉ học của trẻ
            $sheet->setCellValue($strEndCol . '5', convertText4Web('Nghỉ học'));
            $sheet->getColumnDimension($strEndColPre)->setWidth(10);
            $sheet->getColumnDimension($strEndCol)->setWidth(10);
            //set hàng tổng của trẻ

            $sheet->getStyle($strEndColPre . '5:' . $strEndCol . $rowNumber)->applyFromArray(styleArray('Times new Roman', true, false, 11, PHPExcel_Style_Alignment::HORIZONTAL_CENTER, PHPExcel_Style_Alignment::VERTICAL_CENTER, true));

            // Ngày tháng
            $rowNumber = $rowNumber + 2;
            $sheet->mergeCells('A' . $rowNumber . ':'. $strEndCol . $rowNumber);
            $sheet->getStyle('A' . $rowNumber)->applyFromArray(styleArray('Times new Roman', false, true, 11, PHPExcel_Style_Alignment::HORIZONTAL_RIGHT, PHPExcel_Style_Alignment::VERTICAL_CENTER, true));
            $sheet->setCellValue('A' . $rowNumber, '..........., ' . __('day') . ' .... ' . __('month') . ' .... ' . __('year') . ' ...... ');

            // Người lập đơn
            $rowNumber++;
            if($col_end_abs > 5) {
                $strFromCol = PHPExcel_Cell::stringFromColumnIndex($col_end_abs - 5);
                $sheet->mergeCells($strFromCol . $rowNumber . ':'. $strEndCol . $rowNumber);
                $sheet->getStyle($strFromCol.$rowNumber)->applyFromArray(styleArray('Times new Roman', true, false, 11, PHPExcel_Style_Alignment::HORIZONTAL_CENTER, PHPExcel_Style_Alignment::VERTICAL_CENTER, true));
                $sheet->setCellValue($strFromCol . $rowNumber, mb_strtoupper(__('Issuer')));
            } else {
                $sheet->mergeCells('C' . $rowNumber . ':'. $strEndCol . $rowNumber);
                $sheet->getStyle('C' . $rowNumber)->applyFromArray(styleArray('Times new Roman', true, false, 11, PHPExcel_Style_Alignment::HORIZONTAL_CENTER, PHPExcel_Style_Alignment::VERTICAL_CENTER, true));
                $sheet->setCellValue('C' . $rowNumber, mb_strtoupper(__('Issuer')));
            }
            unset($sheet);
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            ob_start();
            $file_name = convertText4Web( __("Attendance") . '_' . $class['group_title'] . '.xlsx');
            $objWriter->save("php://output");

            $xlsData = ob_get_contents();
            ob_end_clean();
            return_json(array(
                'file_name' => $file_name,
                'file' => "data:application/vnd.ms-excel;base64," . base64_encode($xlsData)
            ));

            break;

        case 'conabs_dashboard':
            include_once(ABSPATH . 'includes/ajax/ci/memcache_caching.php');
            $memcache = new CacheMemcache();

            /* ---------- Trẻ nghỉ nhiều liên tục ---------- */
            $daynum = 3;
            $children = $attendanceDao->getConsecutiveAbsentChild($school['page_id'], $daynum);

            $tmpArray = array();
            while ((count($children) > 0) && (MAX_CONSECUTIVE_ABSENT_DAY >= $daynum)) {
                $tmpArray[$daynum] = $children;
                $daynum++;
                if (MAX_CONSECUTIVE_ABSENT_DAY >= $daynum) {
                    $children = $attendanceDao->getConsecutiveAbsentChild($school['page_id'], $daynum);
                }
            }

            $newArrays = array();
            while ($daynum > 3) {
                $daynum--;
                foreach ($tmpArray[$daynum] as $childOut) {
                    $found = false;
                    foreach ($newArrays as $childIn) {
                        if ($childOut['child_id'] == $childIn['child_id']) {
                            $found = true;
                            break;
                        }
                    }
                    if (!$found) {
                        $newChild = $childOut;
                        $newChild['number_of_absent_day'] = $daynum;
                        $newArrays[] = $newChild;
                    }
                }
            }
            // lặp danh sách trẻ nghỉ nhiều liên tục lấy lý do nghỉ
            // Lấy ngày điểm danh cuối cùng của trường
            $dates = $attendanceDao->getLastAttendanceDates($school['page_id'], $daynum);
            $toDate = $dates[0];
            $endDate = toSysDate($toDate);

            // Khai báo mảng mới
            $resultsAb['list'] = array();
            foreach ($newArrays as $row) {
                $beginDate = date('Y-m-d', strtotime('- ' . ($row['number_of_absent_day'] + 2) . ' days', strtotime($toDate)));
                $beginDate = toSysDate($beginDate);
                $attendances = $attendanceDao->getAttendanceChild($row['child_id'], $beginDate, $endDate);
                $reason = '';

                // Lặp mảng thông tin điểm danh của trẻ lấy lý do nghỉ (lấy lần cuối cùng có lý do)
                foreach ($attendances['attendance'] as $value) {
                    if($value['reason'] != '') {
                        $reason = $value['reason'];
                        break;
                    }
                }
                $row['reason'] = $reason;
                $resultsAb['list'][] = $row;
            }
            $resultsAb['updated_at'] = date('H:i d/m/Y');
            if ($memcache->bEnabled) {
                $memcache->setData(SCHOOL_CONSECUTIVE_ABSENT . $school['page_id'], $resultsAb);
            }

            $smarty->assign('conabs', $resultsAb);
            $return['results'] = $smarty->fetch("ci/school/ajax.school.attendance.conabs.dashboard.tpl");
            return_json($return);

            break;

        case 'delete_attendance_child':
            if (!canEdit($_POST['school_username'], 'attendance')) {
                _error(403);
            }
            $db->autocommit(false);
            $db->begin_transaction();
            // Lấy chi tiết điểm danh
            $attendance_detail = $attendanceDao->getAttendanceDetailById($_POST['attendance_detail_id']);
            $attendanceDao->deleteAttendanceDetail($attendance_detail);
            $db->commit();

            return_json(array('callback' => 'window.location = "'.$system['system_url'].'/school/'.$_POST['school_username'].'/attendance/child";'));
            break;
        default:
            _error(400);
            break;
    }
} catch (Exception $e) {
    $db->rollback();
    return_json( array('error' => true, 'message' => $e->getMessage()) );
} finally {
    $db->autocommit(true);
}

/**
 * Kiểm tra một ngày có phải là thứ 7 hay Chủ Nhật hay không
 * Kiểu ngày tháng truyền vào phải có dạng 'Y-m-d'.
 *
 * @param $date
 * @return bool
 */
function isWeekend($date) {
    $weekDay = date('w', strtotime($date));
    return ($weekDay == 0 || $weekDay == 6);
}

function cellColor($cells,$color){
    global $objPHPExcel;
    $objPHPExcel->getActiveSheet()->getStyle($cells)->getFill()->applyFromArray(array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'startcolor' => array(
            'rgb' => $color
        )
    ));
}

?>