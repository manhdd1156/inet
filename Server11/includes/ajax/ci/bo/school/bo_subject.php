<?php
/**
 * Package: ajax/ci/bo
 *
 * @package ConIu
 * @author QuanND
 */

// fetch bootstrap
require('../../../../../bootstrap.php');

// check AJAX Request
is_ajax();
check_login();

if (is_empty($_POST['school_username']) || !valid_username($_POST['school_username'])) {
    _error(404);
}

include_once(DAO_PATH . 'dao_school.php');
include_once(DAO_PATH . 'dao_class.php');
include_once(DAO_PATH . 'dao_class_level.php');
include_once(DAO_PATH . 'dao_teacher.php');
include_once(DAO_PATH . 'dao_user.php');
include_once(DAO_PATH . 'dao_parent.php');
include_once(DAO_PATH . 'dao_child.php');
include_once(DAO_PATH . 'dao_tuition.php');
include_once(DAO_PATH . 'dao_service.php');
include_once(DAO_PATH . 'dao_role.php');

$schoolDao = new SchoolDAO();
$classDao = new ClassDAO();
$classLevelDao = new ClassLevelDAO();
$teacherDao = new TeacherDAO();
$userDao = new UserDAO();
$parentDao = new ParentDAO();
$childDao = new ChildDAO();
$tuitionDao = new TuitionDAO();
$serviceDao = new ServiceDAO();
$roleDao = new RoleDAO();

//$school = getSchool($_POST['school_username']);
$school = getSchoolDataByUsername($_POST['school_username'], SCHOOL_INFO);
if (!canEdit($_POST['school_username'], 'subjects')) {
    _error(403);
}

$args = array();
$args['subject_id'] = $_POST['subject_id'];
$args['class_level_ids'] = $_POST['classlevel_id'];

try {
    $db->autocommit(false);
    switch ($_POST['do']) {
        case 'edit':
            $db->begin_transaction();
            // valid inputs
            if (!isset($_POST['subject_id']) || !is_numeric($_POST['subject_id'])) {
                _error(400);
            }
            //$class = $classDao->getClass($_POST['class_id']);
//            $class = getClassData($_POST['class_id'], CLASS_INFO);
//            if (is_null($class)) {
//                _error(404);
//            }
            $govClassLevels = array();
            // Lấy thông tin gov_class_level

            foreach ($args['class_level_ids'] as $classLevel) {
                $classLevelSubject = $classLevelDao->getClassLevel($classLevel);
                $govClassLevels[] = $classLevelSubject['gov_class_level'];

            }
//            //1. Cập nhật thông tin lớp
//            //$args['username_old'] = $class['group_name'];
//            $args['group_id'] = $_POST['class_id'];
//            $classDao->editClass($args);

//            //2. Cập nhật danh sách giáo viên của lớp
//            $teacherIds = isset($_POST['user_id']) ? $_POST['user_id'] : array();
//            $oldClassLevelSubjects =
            $curDate = date('Y').'-'.(date('Y')+1);
            // Lấy danh sách khối ban đầu được gán môn học
            $classLevelSubjects = $classLevelDao->getClassLevelsBySubjectId($args['subject_id'],$school['page_id'],$curDate);
            //$oldTeacherIds = $teacherDao->getTeacherIDOfClass($_POST['class_id']);

            $oldGovClassLevels = array();
            // Lọc lấy khối
            foreach($classLevelSubjects as $classLevelSubject) {
                $oldGovClassLevels[] = $classLevelSubject['gov_class_level'];
            }
//            $oldTeachers = getClassData($_POST['class_id'], CLASS_TEACHERS);
//            $oldTeacherIds = array_keys($oldTeachers);
//            $oldClassLevelSubjectIds = array_keys($oldClassLevelSubjects);

            //Xóa những khối không còn trong danh sách mới
            $deletedGovClassLevels = array_diff($oldGovClassLevels, $govClassLevels);
            if (count($deletedGovClassLevels) > 0) {
                $classLevelDao->deleteClassLevelSubjectList($args['subject_id'],$school['page_id'],$curDate,$deletedGovClassLevels);
            }
            // Gán môn vào các khối mới được bổ xung so với danh sách cũ
            $newGovClassLevels = array_diff($govClassLevels, $oldGovClassLevels);
            if (count($newGovClassLevels) > 0) {
                $classLevelDao->insertClassLevelSubjectList($args['subject_id'],$school['page_id'],$curDate,$newGovClassLevels);
            }
            $db->commit();

            //return_json( array('success' => true, 'message' => __("Done, Class info have been updated")) );
            return_json(array('callback' => 'window.location = "' . $system['system_url'] . '/school/' . $_POST['school_username'] . '/subjects";'));
            break;
        default:
            _error(400);
            break;
    }

} catch (Exception $e) {
    $db->rollback();
    return_json(array('error' => true, 'message' => $e->getMessage()));
} finally {
    $db->autocommit(true);
}

/**
 * Lấy ra thông tin học phí chi tiết cần quyết toán trước khi trẻ nghỉ. (có truyền vào child_id)
 *
 * @return array
 */
function getTuitionChild4LeaveData($child_id)
{
    //Thông tin phí tính cho tháng này
    $feeIds = is_null($_POST['fee_id_' . $child_id]) ? array() : $_POST['fee_id_' . $child_id];
    $feeQuantities = is_null($_POST['quantity_' . $child_id]) ? array() : $_POST['quantity_' . $child_id];
    $feePrices = is_null($_POST['unit_price_' . $child_id]) ? array() : $_POST['unit_price_' . $child_id];

    $tuitionDetails = array();
    for ($idx = 0; $idx < count($feeIds); $idx++) {
        $tuitionDetail = array();
        $tuitionDetail['service_id'] = 0;
        $tuitionDetail['fee_id'] = $feeIds[$idx];
        $tuitionDetail['quantity'] = $feeQuantities[$idx];
        $tuitionDetail['unit_price'] = str_replace(',', '', $feePrices[$idx]);
        $tuitionDetail['type'] = TUITION_DETAIL_FEE;
        $tuitionDetail['service_type'] = 0;

        if ($tuitionDetail['quantity'] * $tuitionDetail['unit_price'] != 0) {
            $tuitionDetails[] = $tuitionDetail;
        }
    }

    // Lấy dịch vụ của tháng hiện tại
    $serviceIds = is_null($_POST['service_id_' . $child_id]) ? array() : $_POST['service_id_' . $child_id];
    $serviceQuantities = is_null($_POST['service_quantity_' . $child_id]) ? array() : $_POST['service_quantity_' . $child_id];
    $servicePrices = is_null($_POST['service_fee_' . $child_id]) ? array() : $_POST['service_fee_' . $child_id];
    $serviceTypes = is_null($_POST['service_type_' . $child_id]) ? array() : $_POST['service_type_' . $child_id];
    for ($idx = 0; $idx < count($serviceIds); $idx++) {
        $tuitionDetail = array();
        $tuitionDetail['service_id'] = $serviceIds[$idx];
        $tuitionDetail['fee_id'] = 0;
        $tuitionDetail['quantity'] = $serviceQuantities[$idx];
        $tuitionDetail['unit_price'] = str_replace(',', '', $servicePrices[$idx]);
        $tuitionDetail['type'] = TUITION_DETAIL_SERVICE;
        $tuitionDetail['service_type'] = $serviceTypes[$idx];

        if ($tuitionDetail['quantity'] * $tuitionDetail['unit_price'] != 0) {
            $tuitionDetails[] = $tuitionDetail;
        }
    }

    $cbServiceIds = is_null($_POST['cbservice_id_' . $child_id]) ? array() : $_POST['cbservice_id_' . $child_id];
    $cbServiceQuantities = is_null($_POST['cbservice_quantity_' . $child_id]) ? array() : $_POST['cbservice_quantity_' . $child_id];
    $cbServicePrices = is_null($_POST['cbservice_price_' . $child_id]) ? array() : $_POST['cbservice_price_' . $child_id];
    //Gộp dịch vụ tính theo SỐ LẦN
    for ($cbIdx = 0; $cbIdx < count($cbServiceIds); $cbIdx++) {
        $tuitionDetail = array();
        $tuitionDetail['service_id'] = $cbServiceIds[$cbIdx];
        $tuitionDetail['fee_id'] = 0;
        $tuitionDetail['quantity'] = $cbServiceQuantities[$cbIdx];
        $tuitionDetail['unit_price'] = convertMoneyToNumber($cbServicePrices[$cbIdx]);
        $tuitionDetail['service_type'] = SERVICE_TYPE_COUNT_BASED;
        if ($cbServiceIds[$cbIdx] > 0) {
            $tuitionDetail['type'] = TUITION_DETAIL_SERVICE;
        } else {
            $tuitionDetail['type'] = LATE_PICKUP_FEE;
        }
        if ($tuitionDetail['quantity'] * $tuitionDetail['unit_price'] != 0) {
            $tuitionDetails[] = $tuitionDetail;
        }
    }

    return $tuitionDetails;
}

?>