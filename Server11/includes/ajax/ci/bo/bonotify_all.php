<?php
/**
 * Package: ajax/ci/bo
 * 
 * @package inet
 * @author QuanND
 */

// fetch bootstrap
require('../../../../bootstrap.php');

// check AJAX Request
is_ajax();
check_login();

include_once(DAO_PATH.'dao_school.php');
include_once(DAO_PATH.'dao_class.php');
include_once(DAO_PATH.'dao_child.php');
include_once(DAO_PATH.'dao_event.php');
include_once(DAO_PATH.'dao_medicine.php');
include_once(DAO_PATH.'dao_tuition.php');
include_once(DAO_PATH.'dao_user.php');
include_once(DAO_PATH.'dao_attendance.php');
include_once(DAO_PATH.'dao_tuition.php');
include_once(DAO_PATH.'dao_report.php');
include_once(DAO_PATH.'dao_feedback.php');
include_once(DAO_PATH.'dao_schedule.php');

$schoolDao = new SchoolDAO();
$classDao = new ClassDAO();
$childDao = new ChildDAO();
$eventDao = new EventDAO();
$medicineDao = new MedicineDAO();
$userDao = new UserDAO();
$attendanceDao = new AttendanceDAO();
$tuitionDao = new TuitionDAO();
$reportDao = new ReportDAO();
$feedbackDao = new FeedbackDAO();
$scheduleDao = new ScheduleDAO();

try {

    $ntf = $userDao->getSNotification($_POST['id']);
    if (is_null($ntf)) {
        throw new Exception(__("The requested URL was not found on this server. That's all we know"));
    }

    $url = $system['system_url'];
    switch ($ntf['action']) {
        case NOTIFICATION_ASSIGN_CLASS:
            //Khi phân lớp cho giáo viên. node_url = class_id
            //$class = $classDao->getClass($ntf['node_url']);
            $class = getClassData($ntf['node_url'], CLASS_INFO);
            if (is_null($class)) {
                throw new Exception(__("The class does not exist, it should be deleted."));
            }
            $url = $url . '/class/' . $class['group_name'];
            break;
        case NOTIFICATION_NEW_CLASS:
        case NOTIFICATION_UPDATE_CLASS:
            //Khi thêm mới lớp node_url = class_id
            $url = $url . '/school/' . $ntf['extra2'] . '/classes';
            break;
        case NOTIFICATION_NEW_CLASSLEVEL:
        case NOTIFICATION_UPDATE_CLASSLEVEL:
            //Khi thêm mới lớp node_url = class_id
            $url = $url . '/school/' . $ntf['extra2'] . '/classlevels/edit/' . $ntf['node_url'];
            break;
        case NOTIFICATION_NEW_CHILD_TEACHER:
            $url = $url . '/school/' . $ntf['extra2'] . '/children/detail/' .$ntf['node_url'];
            break;
        case NOTIFICATION_UPDATE_CHILD_TEACHER:
            // Thông báo sửa trẻ từ giáo viên, chỉ có trường mới nhận được thông báo này
            //QuanND - Bỏ bước check này
//            include_once(DAO_PATH.'dao_child.php');
//            $childDao = new ChildDAO();
//            $school = getSchoolData($ntf['extra2'], SCHOOL_INFO);
//            $child = $childDao->getChildEditOfTeacher($ntf['node_url'], $school['page_id']);
//            if(is_null($child)) {
//                _error("Error", "The student does not exist or has been confirmed");
//            }
            $url = $url . '/school/' . $ntf['extra2'] . '/children/confirm/' . $ntf['node_url'];
            break;
        case NOTIFICATION_CONFIRM_CHILD_TEACHER:
        case NOTIFICATION_UNCONFIRM_CHILD_TEACHER:
            // Thông báo xác nhận sửa thông tin trẻ, chỉ có giáo viên mới nhận được thông báo này
            $url = $url . '/class/' . $ntf['extra2'] . '/children/detail/' . $ntf['node_url'];
            break;
        case NOTIFICATION_NEW_TEACHER:
            //Trường hợp lớp có thêm giáo viên mới, thông báo cho phụ huynh và giáo viên trong lớp. node_url = class_id
            if ($ntf['node_type'] == NOTIFICATION_NODE_TYPE_CLASS) {
                //$class = $classDao->getClass($ntf['node_url']);
                $class = getClassData($ntf['node_url'], CLASS_INFO);
                if (is_null($class)) {
                    throw new Exception(__("The class does not exist, it should be deleted."));
                }

                $url = $url.'/groups/'.$class['group_name'];
            } elseif ($ntf['node_type'] == NOTIFICATION_NODE_TYPE_CHILD) {
                $url = $url.'/child/'.$ntf['extra2'];
            }
            break;
        case NOTIFICATION_NEW_EVENT_SCHOOL:
        case NOTIFICATION_UPDATE_EVENT_SCHOOL:
            //Sự kiện cấp trường. node_url = event_id
            $event = $eventDao->getEvent($ntf['node_url']);
            if (is_null($event)) {
                throw new Exception(__("The event does not exist, it should be deleted."));
            }
            if ($ntf['node_type'] == NOTIFICATION_NODE_TYPE_CLASS) {
                $url = $url.'/class/'.$ntf['extra2'].'/events/detail/'.$event['event_id'];
            } elseif ($ntf['node_type'] == NOTIFICATION_NODE_TYPE_CHILD) {
                //Phụ huynh chỉ có thể xem trong màn hình quản lý thông tin con
                $url = $url.'/child/'.$ntf['extra2'].'/events/detail/'.$event['event_id'];
            } else {
                $url = $url.'/school/'.$ntf['extra2'].'/events/detail/'.$ntf['node_url'];
            }
            break;
        case NOTIFICATION_NEW_EVENT_CLASS_LEVEL:
        case NOTIFICATION_UPDATE_EVENT_CLASS_LEVEL:
            $event = $eventDao->getEvent($ntf['node_url']);
            if (is_null($event)) {
                throw new Exception(__("The event does not exist, it should be deleted."));
            }
            if ($ntf['node_type'] == NOTIFICATION_NODE_TYPE_CLASS) {
                $url = $url.'/class/'.$ntf['extra2'].'/events/detail/'.$event['event_id'];
            } elseif ($ntf['node_type'] == NOTIFICATION_NODE_TYPE_CHILD) {
                $url = $url.'/child/'.$ntf['extra2'].'/events/detail/'.$event['event_id'];
            } else {
                $url = $url.'/school/'.$ntf['extra2'].'/events/detail/'.$ntf['node_url'];
            }
            break;
        case NOTIFICATION_NEW_EVENT_CLASS:
        case NOTIFICATION_UPDATE_EVENT_CLASS:
            $event = $eventDao->getEvent($ntf['node_url']);
            if (is_null($event)) {
                throw new Exception(__("The event does not exist, it should be deleted."));
            }
            if ($ntf['node_type'] == NOTIFICATION_NODE_TYPE_CLASS) {
                $url = $url.'/class/'.$ntf['extra2'].'/events/detail/'.$event['event_id'];
            } elseif ($ntf['node_type'] == NOTIFICATION_NODE_TYPE_CHILD) {
                $url = $url.'/child/'.$ntf['extra2'].'/events/detail/'.$event['event_id'];
            } else {
                $url = $url.'/school/'.$ntf['extra2'].'/events/detail/'.$ntf['node_url'];
            }
            break;
        case NOTIFICATION_CANCEL_EVENT_SCHOOL:
        case NOTIFICATION_CANCEL_EVENT_CLASS_LEVEL:
        case NOTIFICATION_CANCEL_EVENT_CLASS:
            $event = $eventDao->getEvent($ntf['node_url']);
            if (is_null($event)) {
                throw new Exception(__("The requested URL was not found on this server. That's all we know"));
            }
            if ($ntf['node_type'] == NOTIFICATION_NODE_TYPE_CLASS) {
                $url = $url.'/class/'.$ntf['extra2'].'/events';
            } elseif ($ntf['node_type'] == NOTIFICATION_NODE_TYPE_CHILD) {
                $url = $url.'/child/'.$ntf['extra2'].'/events';
            } else {
                $url = $url.'/school/'.$ntf['extra2'].'/events';
            }
            break;

        case NOTIFICATION_REGISTER_EVENT_CHILD:
        case NOTIFICATION_CANCEL_REGISTRATION_EVENT_CHILD:
        case NOTIFICATION_REGISTER_EVENT_PARENT:
        case NOTIFICATION_CANCEL_REGISTRATION_EVENT_PARENT:
            if ($ntf['node_type'] == NOTIFICATION_NODE_TYPE_SCHOOL) {
                $url = $url.'/school/'.$ntf['extra2'].'/events/participants/'.$ntf['node_url'];
            } else if ($ntf['node_type'] == NOTIFICATION_NODE_TYPE_CLASS) {
                $url = $url.'/class/'.$ntf['extra2'].'/events/participants/'.$ntf['node_url'];
            } else {
                $url = $url.'/child/'.$ntf['extra2'].'/events/detail/'.$ntf['node_url'];
            }
            break;

        case NOTIFICATION_NEW_MEDICINE:
        case NOTIFICATION_UPDATE_MEDICINE:
        case NOTIFICATION_USE_MEDICINE:
        case NOTIFICATION_CONFIRM_MEDICINE:
            //Thông báo 'gửi thuốc' cho con. node_url = medicine_id
//            $medicine = $medicineDao->getMedicine($ntf['node_url']);
//            if (is_null($medicine)) {
//                _error("Error", "The medicine does not exist, it should be deleted.");
//            }
            if ($ntf['node_type'] == NOTIFICATION_NODE_TYPE_CLASS) {
//                $class = $classDao->getClassByChildAndTeacher($medicine['child_id'], $user->_data['user_id']);
//                if (is_null($class)) {
//                    //check thêm trường hợp không phải là giáo viên
//                    $class = $classDao->getClassOfChild($medicine['child_id']);
//                    if (is_null($class)) {
//                        _error(404);
//                    }
//                }
                $url = $url.'/class/'.$ntf['extra2'].'/medicines/detail/'.$ntf['node_url'];
            } elseif ($ntf['node_type'] == NOTIFICATION_NODE_TYPE_CHILD) {
                $url = $url . '/child/' . $ntf['extra2'] . '/medicines/detail/' . $ntf['node_url'];
            } elseif ($ntf['node_type'] == NOTIFICATION_NODE_TYPE_SCHOOL) {
                $url = $url.'/school/'.$ntf['extra2'].'/medicines/detail/'.$ntf['node_url'];
            }
            break;
        case NOTIFICATION_CANCEL_MEDICINE:
            if ($ntf['node_type'] == NOTIFICATION_NODE_TYPE_CLASS) {
//                $class = $classDao->getClassByChildAndTeacher($medicine['child_id'], $user->_data['user_id']);
//                if (is_null($class)) {
//                    //check thêm trường hợp không phải là giáo viên
//                    $class = $classDao->getClassOfChild($medicine['child_id']);
//                    if (is_null($class)) {
//                        _error(404);
//                    }
//                }
                $url = $url.'/class/'.$ntf['extra2'].'/medicines';
            } elseif ($ntf['node_type'] == NOTIFICATION_NODE_TYPE_CHILD) {
                $url = $url . '/child/' . $ntf['extra2'] . '/medicines';
            } elseif ($ntf['node_type'] == NOTIFICATION_NODE_TYPE_SCHOOL) {
                $url = $url.'/school/'.$ntf['extra2'].'/medicines';
            }
            break;
        case NOTIFICATION_NEW_TUITION:
        case NOTIFICATION_UPDATE_TUITION:
        case NOTIFICATION_UNCONFIRM_TUITION:
            //Thông báo 'học phí'. node_url = tuition_id. Cả 04 loại thông báo trên đều gửi phụ huynh
            if ($ntf['node_type'] == NOTIFICATION_NODE_TYPE_SCHOOL) {
                $url = $url.'/school/'.$ntf['extra2'].'/tuitions/detail/' . $ntf['node_url'];
            } elseif ($ntf['node_type'] == NOTIFICATION_NODE_TYPE_CHILD) {
                $url = $url.'/child/'.$ntf['extra2'].'/tuitions';
            }
            break;
        case NOTIFICATION_CONFIRM_TUITION:
            if ($ntf['node_type'] == NOTIFICATION_NODE_TYPE_SCHOOL) {
                $url = $url.'/school/'.$ntf['extra2'].'/tuitions/detail/'.$ntf['node_url'];
            } elseif ($ntf['node_type'] == NOTIFICATION_NODE_TYPE_CHILD) {
                //Thông báo 'học phí'. node_url = tuition_id. Cả 04 loại thông báo trên đều gửi phụ huynh
                $url = $url.'/child/'.$ntf['extra2'].'/tuitions';
            } else {
                $url = $url.'/class/'.$ntf['extra2'].'/tuitions/detail/' .$ntf['node_url'];
            }
            break;
        case NOTIFICATION_CONFIRM_TUITION_SCHOOL:
            if ($ntf['node_type'] == NOTIFICATION_NODE_TYPE_SCHOOL) {
                $url = $url.'/school/'.$ntf['extra2'].'/tuitions/detail/'.$ntf['node_url'];
            }
            break;
        case NOTIFICATION_ATTENDANCE_RESIGN:
            $attendanceDetail = $attendanceDao->getAttendanceDetailById($ntf['node_url']);
            if (is_null($attendanceDetail)) {
                throw new Exception(__("The resign does not exist, it should be deleted."));
            }
            if ($ntf['node_type'] == NOTIFICATION_NODE_TYPE_SCHOOL) {
                $url = $url.'/school/'.$ntf['extra2'].'/attendance/child';
            } else {
                $url = $url.'/class/'.$ntf['extra2'].'/attendance/rollup/' . $attendanceDetail['attendance_id'];
            }
            break;
        case NOTIFICATION_ATTENDANCE_CONFIRM:
            $attendanceDetail = $attendanceDao->getAttendanceDetailById($ntf['node_url']);
            $url = $url.'/child/'.$attendanceDetail['child_id'].'/attendance';
            break;

        case NOTIFICATION_NEW_REPORT:
            if($ntf['node_type'] == NOTIFICATION_NODE_TYPE_SCHOOL) {
                $url = $url.'/school/' . $ntf['extra2'] . '/reports';
            } elseif ($ntf['node_type'] == NOTIFICATION_NODE_TYPE_CHILD) {
                //Thông báo 'báo cáo'. node_url = report_id. Cả 02 loại thông báo trên đều gửi phụ huynh
                $url = $url.'/child/'.$ntf['extra2'].'/reports/detail/' . $ntf['node_url'];
            }
            break;
        case NOTIFICATION_UPDATE_REPORT:
            if($ntf['node_type'] == NOTIFICATION_NODE_TYPE_SCHOOL) {
                $url = $url.'/school/' . $ntf['extra2'] . '/reports/detail/' . $ntf['node_url'];
            } elseif ($ntf['node_type'] == NOTIFICATION_NODE_TYPE_CHILD) {
                //Thông báo 'báo cáo'. node_url = report_id. Cả 02 loại thông báo trên đều gửi phụ huynh
                $url = $url.'/child/'.$ntf['extra2'].'/reports/detail/'.$ntf['node_url'];
            }
            break;
        case NOTIFICATION_NEW_POINT:
            if($ntf['node_type'] == NOTIFICATION_NODE_TYPE_SCHOOL) {
                $url = $url.'/school/' . $ntf['extra2'] . '/points/';
            } elseif ($ntf['node_type'] == NOTIFICATION_NODE_TYPE_CHILD) {
                //Thông báo 'báo cáo'. node_url = report_id. Cả 02 loại thông báo trên đều gửi phụ huynh
                $url = $url.'/child/'.$ntf['extra2'].'/points/';
            }
            break;
        case NOTIFICATION_NEW_REPORT_TEMPLATE:
            if($ntf['node_type'] == NOTIFICATION_NODE_TYPE_CLASS) {
                $url = $url.'/class/' . $ntf['extra2'] . '/reports/edittemp/' . $ntf['node_url'];
            }
            break;

        case NOTIFICATION_NEW_FEEDBACK:
            if ($ntf['node_type'] == NOTIFICATION_NODE_TYPE_SCHOOL) {
                $url = $url.'/school/'.$ntf['extra2'].'/feedback';
            }
            /*elseif ($ntf['node_type'] == NOTIFICATION_NODE_TYPE_CLASS) {
                $feedback = $feedbackDao->getFeedback($_POST['node_url']);
                //Thông báo gửi đến giáo viên
                if (is_null($feedback)) {
                    _api_error(404);
                }
                $class = $classDao->getClassByChildAndTeacher($feedback['child_id'], $user->_data['user_id']);
                if (is_null($class)) {
                    //check thêm trường hợp không phải là giáo viên
                    $class = $classDao->getClassOfChild($feedback['child_id']);
                    if (is_null($class)) {
                        _error(404);
                    }
                }
                $url = $url.'/class/'.$class['group_name'].'/feedback';
            }*/
            break;

        case NOTIFICATION_CONFIRM_FEEDBACK:
            //Thông báo 'góp ý'. node_url = feedback_id. Thông báo này gửi cho phụ huynh
            $feedback = $feedbackDao->getFeedback($ntf['node_url']);
            if($ntf['node_type'] == NOTIFICATION_NODE_TYPE_SCHOOL) {
                $url = $url. '/school/' . $ntf['extra2'] . '/feedback';
            } elseif ($ntf['node_type'] == NOTIFICATION_NODE_TYPE_CLASS) {
                $url = $url. '/class/' . $ntf['extra2'] . '/feedback';
            } else {
                //Phụ huynh chỉ có thể xem thông báo trong màn hình quản lý thông tin con
                if (is_null($feedback)) {
                    throw new Exception(__("The requested URL was not found on this server. That's all we know"));
                }
                $url = $url.'/child/'.$feedback['child_id'].'/feedback/list';
            }
            break;
        case NOTIFICATION_SERVICE_CANCEL_UNCOUNTBASED:
        case NOTIFICATION_SERVICE_REGISTER_UNCOUNTBASED:
            if ($ntf['node_type'] == NOTIFICATION_NODE_TYPE_SCHOOL) {
                $url = $url.'/school/'.$ntf['extra2'].'/useservices/history';
            } else if ($ntf['node_type'] == NOTIFICATION_NODE_TYPE_CLASS) {
                $url = $url.'/class/'.$ntf['extra2'].'/useservices/history';
            } else {
                $url = $url.'/child/'.$ntf['extra2'].'/services';
            }

            break;
        case NOTIFICATION_SERVICE_CANCEL_COUNTBASED:
        case NOTIFICATION_SERVICE_REGISTER_COUNTBASED:
        case NOTIFICATION_SERVICE_REGISTER_COUNTBASED_CLASS:
            if ($ntf['node_type'] == NOTIFICATION_NODE_TYPE_SCHOOL) {
                $url = $url.'/school/'.$ntf['extra2'].'/useservices/history';
            } else if ($ntf['node_type'] == NOTIFICATION_NODE_TYPE_CLASS) {
                $url = $url.'/class/'.$ntf['extra2'].'/useservices/history';
            } else {
                $url = $url.'/child/'.$ntf['extra2'].'/services';
            }
            break;
        case NOTIFICATION_SERVICE_REGISTER_COUNTBASED_MOBILE:
            if ($ntf['node_type'] == NOTIFICATION_NODE_TYPE_SCHOOL) {
                $strReplace = str_replace('/', '_', $ntf['extra3']);
                $url = $url.'/school/'.$ntf['extra2'].'/useservices/classdate/'.$ntf['node_url'].'_'.$strReplace;
            }
            break;
        case NOTIFICATION_UPDATE_PICKER:
            //Thông báo 'sửa thông tin người đón' cho con. node_url = child_id
            if ($ntf['node_type'] == NOTIFICATION_NODE_TYPE_CLASS) {
                $url = $url.'/class/'.$ntf['extra2'].'/children/detail/'.$ntf['node_url'];
            } elseif ($ntf['node_type'] == NOTIFICATION_NODE_TYPE_SCHOOL) {
                $url = $url.'/school/'.$ntf['extra2'].'/children/detail/'.$ntf['node_url'];
            }
            break;
        case NOTIFICATION_CONFIRM_PICKER:
            // Thông báo đã trả trẻ cho phụ huynh
            $url = $url.'/child/'.$ntf['node_url'].'/informations';
            break;
        case NOTIFICATION_NEW_SCHEDULE:
        case NOTIFICATION_UPDATE_SCHEDULE:
            if ($ntf['node_type'] == NOTIFICATION_NODE_TYPE_CLASS) {
                $url = $url.'/class/'.$ntf['extra2'].'/schedules/detail/' . $ntf['node_url'];
            } elseif ($ntf['node_type'] == NOTIFICATION_NODE_TYPE_CHILD) {
                $url = $url.'/child/'.$ntf['extra2'].'/schedules/detail/' . $ntf['node_url'];
            } else {
                $url = $url.'/school/'.$ntf['extra2'].'/schedules/detail/' . $ntf['node_url'];
            }
            break;

        case NOTIFICATION_NEW_MENU:
        case NOTIFICATION_UPDATE_MENU:
            if ($ntf['node_type'] == NOTIFICATION_NODE_TYPE_CLASS) {
                $url = $url.'/class/'.$ntf['extra2'].'/menus/detail/' . $ntf['node_url'];
            } elseif ($ntf['node_type'] == NOTIFICATION_NODE_TYPE_CHILD) {
                $url = $url.'/child/'.$ntf['extra2'].'/menus/detail/' . $ntf['node_url'];
            } else {
                $url = $url.'/school/'.$ntf['extra2'].'/menus/detail/' . $ntf['node_url'];
            }
            break;

        case NOTIFICATION_UPDATE_ROLE:
            $url = $url.'/school/'.$ntf['extra2'];
            break;

        case NOTIFICATION_ADD_LATEPICKUP_CLASS:
        case NOTIFICATION_REMOVE_LATEPICKUP_CLASS:
        case NOTIFICATION_UPDATE_LATEPICKUP_INFO:
        case NOTIFICATION_CHILD_PICKEDUP:
            $url = $url.'/child/'.$ntf['extra2'].'/pickup/list';
            break;

        case NOTIFICATION_REGISTER_LATEPICKUP:
            $url = $url.'/class/'.$ntf['extra2'].'/pickup';
            break;
        case NOTIFICATION_ASSIGN_LATEPICKUP_CLASS:
//            $classes = $classDao->getClassesBySchoolAndTeacher($ntf['extra2'], $user->_data['user_id']);
//            if (count($classes) == 0) {
//                throw new Exception(__("The requested URL was not found on this server. That's all we know"));
//            }
            $url = $url.'/school/'.$ntf['extra2'].'/pickupteacher/assign';
            break;
        case NOTIFICATION_FOETUS_DEVELOPMENT:
            include_once(DAO_PATH.'dao_foetus_development.php');
            $foetusDevelopmentDao = new FoetusDevelopmentDAO();
            $data = $foetusDevelopmentDao->getFoetusInfoDetail($ntf['node_url']);
            if(!is_null($data)) {
                if($data['content_type'] == 2) {
                    $url = $data['link'];
                } else {
                    $url = $url.'/childinfo/'.$ntf['extra2'].'/foetusdevelopments/detail/' . $ntf['node_url'];
                }
            } else {
                _error(404);
            }
            break;
        case NOTIFICATION_CHILD_DEVELOPMENT:
            include_once(DAO_PATH.'dao_child_development.php');
            $childDevelopmentDao = new ChildDevelopmentDAO();
            $data = $childDevelopmentDao->getChildDevelopmentDetail($ntf['node_url']);
            if(!is_null($data)) {
                if($data['content_type'] == 2) {
                    $url = $data['link'];
                } else {
                    $url = $url.'/childinfo/'.$ntf['extra2'].'/childdevelopments/detail/' . $ntf['node_url'];
                }
            } else {
                _error(404);
            }
            break;
        case NOTIFICATION_UPDATE_CHILD_SCHOOL:
            $url = $url.'/childinfo/'.$ntf['extra2'].'/childdetail/';
            break;
        case NOTIFICATION_NEW_CHILD_EXIST:
            $url = $url.'/child/'.$ntf['extra2'];
            break;
        case NOTIFICATION_NEW_CHILD_HEALTH:
        case NOTIFICATION_UPDATE_CHILD_HEALTH:
            $url = $url.'/childinfo/'.$ntf['extra2'].'/health';
            break;
        case NOTIFICATION_NEW_CHILD_EDIT_BY_TEACHER:
            $url = $url.'/school/'.$ntf['extra2'].'/children/listchildedit';
            break;

        case NOTIFICATION_LEAVE_SCHOOL_BY_PARENT:
            $url = $url.'/school/'.$ntf['extra2'].'/children/leaveschool';
            break;
        case NOTIFICATION_ADD_DIARY_SCHOOL:
        case NOTIFICATION_EDIT_CAPTION_DIARY_SCHOOL:
        case NOTIFICATION_DELETE_PHOTO_DIARY_SCHOOL:
        case NOTIFICATION_ADD_PHOTO_DIARY_SCHOOL:
            $url = $url.'/childinfo/'.$ntf['extra2'].'/journals';
            break;
        case NOTIFICATION_NEW_ATTENDANCE:
        case NOTIFICATION_NEW_ATTENDANCE_BACK:
        case NOTIFICATION_UPDATE_ATTENDANCE:
        case NOTIFICATION_UPDATE_ATTENDANCE_BACK:
            $url = $url.'/child/'.$ntf['extra2'].'/attendance';
            break;
        case NOTIFICATION_REMIND_ATTENDANCE:
            if ($ntf['node_type'] == NOTIFICATION_NODE_TYPE_CLASS) {
                $url = $url.'/class/'.$ntf['extra2'].'/attendance/rollup';
            } else {
                $url = $url.'/school/'.$ntf['extra2'].'/attendance';
            }
            break;
        case NOTIFICATION_REMIND_SCHEDULE:
            if ($ntf['node_type'] == NOTIFICATION_NODE_TYPE_CLASS) {
                $url = $url.'/class/'.$ntf['extra2'].'/schedules';
            } elseif ($ntf['node_type'] == NOTIFICATION_NODE_TYPE_SCHOOL) {
                $url = $url.'/school/'.$ntf['extra2'].'/schedules';
            } else {
                if($ntf['node_url'] == 0) {
                    $url = $url.'/child/'.$ntf['extra2'].'/schedules';
                } else {
                    $url = $url.'/child/'.$ntf['extra2'].'/schedules/detail/' . $ntf['node_url'];
                }
            }
            break;
        case NOTIFICATION_REMIND_MENU:
            if ($ntf['node_type'] == NOTIFICATION_NODE_TYPE_CLASS) {
                $url = $url.'/class/'.$ntf['extra2'].'/menus';
            } elseif ($ntf['node_type'] == NOTIFICATION_NODE_TYPE_SCHOOL) {
                $url = $url.'/school/'.$ntf['extra2'].'/menus';
            } else {
                if($ntf['node_url'] == 0) {
                    $url = $url.'/child/'.$ntf['extra2'].'/menus';
                } else {
                    $url = $url.'/child/'.$ntf['extra2'].'/menus/detail/' . $ntf['node_url'];
                }
            }
            break;
        case NOTIFICATION_REMIND_REPORT:
            if ($ntf['node_type'] == NOTIFICATION_NODE_TYPE_SCHOOL) {
                $url = $url.'/school/'.$ntf['extra2'].'/reports';
            } else {
                $url = $url.'/class/'.$ntf['extra2'].'/reports';
            }
            break;

        default:
            throw new Exception(__("The requested URL was not found on this server. That's all we know"));
    }

    return_json(array('callback' => 'window.location = "'.$url.'";'));
} catch (Exception $e) {
    modal(ERROR, __("Error"), $e->getMessage());
}
?>