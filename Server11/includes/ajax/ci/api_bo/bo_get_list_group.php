<?php
/**
 * Package: ajax/ci/api_bo
 *
 * @package Coniu
 * @author TaiLA
 */

include_once(DAO_PATH . 'dao_class.php');
include_once(DAO_PATH . 'dao_child.php');
include_once(DAO_PATH . 'dao_school.php');
include_once(DAO_PATH . 'dao_pickup.php');
include_once(DAO_PATH . 'dao_role.php');

$classDao = new ClassDAO();
$childDao = new ChildDAO();
$schoolDao = new SchoolDAO();
$pickupDao = new PickupDAO();
$roleDao = new RoleDAO();

try {
    switch ($action) {
        case 'get_list_group':

            $classes = getSchoolData($_POST['school_id'], SCHOOL_CLASSES);

            $temp = array();
            foreach ($classes as $k => $class) {
                $tmp = array();
                $tmp['class_id'] = $class['group_id'];
                $tmp['group_name'] = $class['group_name'];
                $tmp['group_title'] = $class['group_title'];
                $tmp['group_picture'] = $class['group_picture'];
                $temp[] = $tmp;
            }
            // return
            return_json(array(
                'code' => 200,
                'message' => 'OK',
                'data' => array(
                    'classes' => $temp,
                )
            ));
            break;

        default:
            _api_error(400);
            break;
    }

} catch (Exception $e) {
    $db->rollback();
    _api_error(0, $e->getMessage());
} finally {
    $db->autocommit(true);
}

?>