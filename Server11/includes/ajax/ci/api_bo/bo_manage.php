<?php
/**
 * Package: ajax/ci/api_bo
 * 
 * @package Coniu
 * @author Coniu
 */

include_once(DAO_PATH . 'dao_class.php');
include_once(DAO_PATH . 'dao_child.php');
include_once(DAO_PATH . 'dao_school.php');
include_once(DAO_PATH . 'dao_pickup.php');
include_once(DAO_PATH . 'dao_role.php');

$classDao = new ClassDAO();
$childDao = new ChildDAO();
$schoolDao = new SchoolDAO();
$pickupDao = new PickupDAO();
$roleDao = new RoleDAO();

try {
    switch ($action) {
        case 'manage_home':

            $data_classes = array();
            $data_children = array();

            //$classes = $classDao->getClasses($user->_data['user_id']);
            $classes = getUserManageData($user->_data['user_id'], USER_MANAGE_CLASSES);
            foreach ($classes as $class) {
                //$school = $schoolDao->getSchoolByClass($class['group_id']);
                $school = getSchoolData($class['school_id'], SCHOOL_INFO);
                $schoolConfig = getSchoolData($class['school_id'], SCHOOL_CONFIG);

                if (!is_null($school)) {
                    $class['page_id'] = $school['page_id'];
                    $class['page_title'] = $school['page_title'];
                    $class['page_name'] = $school['page_name'];
                    $class['page_picture'] = $school['page_picture'];

                    /*$schoolConfig = getSchoolData($class['school_id'], SCHOOL_CONFIG);
                    $class['allow_class_see_tuition'] = $schoolConfig['allow_class_see_tuition'] ? true : false;
                    $class['allow_class_event'] = $schoolConfig['allow_class_event'] ? true : false;
                    $check_pickup = $pickupDao->checkPickupPermission($school['page_id'], $user->_data['user_id']);
                    $class['is_assigned_pickup'] = $check_pickup;
                    $class['allow_teacher_edit_pickup_before'] = $schoolConfig['allow_teacher_edit_pickup_before'] ? true : false;
                    $class['attendance_use_leave_early'] = $schoolConfig['attendance_use_leave_early'] ? true : false;
                    $class['attendance_use_come_late'] = $schoolConfig['attendance_use_come_late'] ? true : false;
                    $class['attendance_absence_no_reason'] = $schoolConfig['attendance_absence_no_reason'] ? true : false;*/

                    // Thêm  chức năng on/off module
                    $modules = array();
                    foreach ($moduleName as $module => $name) {
                        $modules[$module] = $schoolConfig[$module] ? true : false;
                    }
                    //ADD START MANHDD 25/06/2021 ( tách point thành 2 phần, xem điểm và chấm điểm )
                    if($modules['points']) {
                        if(canView_class($user->_data['user_id'],$class['group_id'])) {
                            $modules['point_edit'] = true;
                        }else {
                            $modules['point_edit'] = false;
                        }
                        $modules['point_view'] = true;
//                        $modules['point_edit'] = true;
                    }else {
                        $modules['point_view'] = false;
                        $modules['point_edit'] = false;
                    }
                    //ADD END MANHDD 25/06/2021
                    $class['modules'] = $modules;
                }
                $data_classes[] = $class;
            }

            //$children = $childDao->getChildren($user->_data['user_id']);
            $children = $childDao->getChildrenOfParent($user->_data['user_id']);
            // nếu là học sinh thì lấy dữ liệu như sau:
            if(!$children) {
                $children_itself = $childDao->getChildren4Memcache($user->_data['user_id'], 0);
                $parent = $childDao->getParent($children_itself['ids'][0]);
                $children = $childDao->getChildrenOfParent($parent[0]['user_id']);
            }
            foreach ($children as $child) {
                //$school = $childDao->getCurrentSchoolOfChild($child['child_id']);
                if ($child['school_id'] > 0) {
                    $school = getSchoolData($child['school_id'], SCHOOL_INFO);

                    //if (!is_null($school)) {
                        //$schoolConfig = $schoolDao->getConfiguration($school['page_id']);
                        $schoolConfig = getSchoolData($child['school_id'], SCHOOL_CONFIG);

                        //$class = $classDao->getClassOfChild($child['child_id'], $school['page_id']);
                        $class = getChildData($child['child_id'], CHILD_CLASS);
                        if (!is_null($class)) {
                            $child['group_id'] = $class['group_id'];
                            $child['group_name'] = $class['group_name'];
                            $child['group_title'] = $class['group_title'];
                            $child['group_picture'] = $class['group_picture'];
                            $child['camera_url'] = $class['camera_url'];
                        }

                        $child['page_id'] = $school['page_id'];
                        $child['page_title'] = $school['page_title'];
                        $child['page_name'] = $school['page_name'];
                        $child['page_picture'] = $school['page_picture'];
                        $child['telephone'] = $school['telephone'];

                        $child['allow_parent_register_service'] = $schoolConfig['allow_parent_register_service'] ? true : false;
                        $child['camera_guide'] = $schoolConfig['camera_guide'];
                        $child['grade'] = $schoolConfig['grade'];

                        // Thêm  chức năng on/off module
                        $modules = array();
                        foreach ($moduleName as $module => $name) {
                            $modules[$module] = $schoolConfig[$module] ? true : false;
                        }
                    //ADD START MANHDD 25/06/2021 ( tách point thành 2 phần, xem điểm và chấm điểm )
                    if($modules['points']) {
                        $modules['point_view'] = true;
                        $modules['point_edit'] = false;
                    }
                    //ADD END MANHDD 25/06/2021
                        $child['modules'] = $modules;
                        //$child['display_children_list'] = $schoolConfig['display_children_list'] ? true : false;
                        //$child['display_parent_info_for_others'] = $schoolConfig['display_parent_info_for_others'] ? true : false;
                    //}
                }

                $data_children[] = $child;
            }

            $schools = getUserManageData($user->_data['user_id'], USER_MANAGE_SCHOOL);
            $schoolsPer = array();
            foreach ($schools as $school) {
                if($school['school_status'] == SCHOOL_USING_CONIU) {

                    if($user->_data['user_id'] != $school['page_admin']) {
                        // đối với nhân viên thường
                        $permissions = isset($school['permissions']) ? $school['permissions'] : array();
                        $pers = array();
                        foreach ($permissions as $k => $permission) {
                            $pers[$permission['module']] = (int)$permission['value'];
                            if($permission['module'] == 'events' && $permission['value'] == PERM_NONE) {
                                $pers[$permission['module']] = PERM_VIEW;
                            }
                        }
                        if(count($pers) == 0) {
                            $pers['events'] = PERM_VIEW;
                        }
                        $school['permissions'] = $pers;
                    } else {
                        // Đối với admin
                        $pers = null;
                        foreach ($systemModules as $module) {
                            $pers[$module['module']] = PERM_EDIT;
                        }
                        $school['permissions'] = $pers;
                    }
                    $principals = explode(',', $school['principal']);
                    if(in_array($user->_data['user_id'], $principals)) {
                        $school['is_principal'] = 1;
                    } else {
                        $school['is_principal'] = 0;
                    }

                    // Thêm  chức năng on/off module
                    $modules = array();
                    foreach ($moduleName as $module => $name) {
                        $modules[$module] = $school[$module] ? true : false;
                    }
                    //ADD START MANHDD 25/06/2021 ( tách point thành 2 phần, xem điểm và chấm điểm )
                    if($modules['points']) {
                        $modules['point_view'] = true;
                        $modules['point_edit'] = true;
                    }
                    //ADD END MANHDD 25/06/2021
                    $school['modules'] = $modules;

                    $schoolsPer[] = $school;
                }
            }

            // return
            return_json(array(
                'code' => 200,
                'message' => 'OK',
                'data' => array(
                    'classes' => $data_classes,
                    'children' => $data_children,
                    'schools' => $schoolsPer
                )
            ));
            break;

        default:
            _api_error(400);
            break;
    }

} catch (Exception $e) {
    $db->rollback();
    _api_error(0, $e->getMessage());
} finally {
    $db->autocommit(true);
}

?>