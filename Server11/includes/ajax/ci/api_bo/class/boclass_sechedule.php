<?php
/**
 * Package: ajax/ci/bo/class
 *
 * @package ConIu
 * @author TaiLA
 */

include_once(DAO_PATH . 'dao_class.php');
include_once(DAO_PATH . 'dao_class_level.php');
include_once(DAO_PATH . 'dao_schedule.php');
$classDao = new ClassDAO();
$classLevelDao = new ClassLevelDAO();
$scheduleDao = new ScheduleDAO();

//Lấy ra thông tin lớp
//$class = $classDao->getClassByUsername($_POST['group_name'], $user->_data['user_id']);
$class = getClassDataByUsername($_POST['group_name'], CLASS_INFO);
if (is_null($class)) {
    _api_error(403);
}

try {
    $return = array();
    $db->autocommit(false);
    // Tăng số lượt tương tác - TaiLa
    addInteractive($class['school_id'], 'schedule', 'school_view');
    switch ($_POST['do']) {
        case 'show_schedule':

            switch ($_POST['view']) {
                case 'this_week':

                    $monday = date('Y-m-d', strtotime('monday this week'));
                    //Lấy ra danh sách lịch học áp dụng cho lớp
                    $data = $scheduleDao->getScheduleSchoolInWeekForApi($class['school_id'], $class['class_level_id'], $class['group_id'], $monday);
                    $removeArr = array();
                    for ($i = 0; $i < count($data); $i++) {
                        for ($j = 0; $j < count($data); $j++) {
                            if ($i != $j) {
                                $beginI = strtotime(toDBDate($data[$i]['begin']));
                                $beginJ = strtotime(toDBDate($data[$j]['begin']));
                                if ($beginI == $beginJ) {
                                    if ($data[$i]['applied_for'] > $data[$j]['applied_for']) {
                                        $removeArr[] = $j;
                                    }
                                }
                            }
                        }
                    }
                    $schedule = null;

                    for($k = 0; $k < count($data); $k++) {
                        if(!in_array($k, $removeArr)) {
                            $schedule = $scheduleDao->getScheduleDetailForApi($data[$k]['schedule_id']);

                            if($schedule['applied_for'] == 1) {
                                $schedule['level'] =  __("School");
                            } elseif($schedule['applied_for'] == 2) {
                                //$class_level = $classLevelDao->getClassLevel($schedule['class_level_id']);
                                $class_level = getClassLevelData($schedule['class_level_id'], CLASS_LEVEL_INFO);
                                $schedule['level'] = $class_level['class_level_name'];
                            } elseif($schedule['applied_for'] == 3) {
                                //$classes = $classDao->getClass($schedule['class_id']);
                                $classes = getClassData($schedule['class_id'], CLASS_INFO);
                                $schedule['level'] = $classes['group_title'];
                            }
                            if(!in_array($k, $removeArr)) {
                                $schedule['use'] = STATUS_ACTIVE;
                            } else {
                                $schedule['use'] = STATUS_INACTIVE;
                            }
                        }
                    }
                    return_json(array(
                        'code' => 200,
                        'message' => 'OK',
                        'data' => array(
                            'schedule_class' => $schedule
                        )
                    ));

                    break;

                case 'history':
                    $page = isset($_POST['page']) ? $_POST['page']  : 0;

                    //Lấy ra danh sách lịch học áp dụng cho lớp
                    $data = $scheduleDao->getScheduleSchoolForApi($class['school_id'], $class['class_level_id'], $class['group_id'], $page);
                    $removeArr = array();
                    for ($i = 0; $i < count($data); $i++) {
                        for ($j = 0; $j < count($data); $j++) {
                            if ($i != $j) {
                                $beginI = strtotime(toDBDate($data[$i]['begin']));
                                $beginJ = strtotime(toDBDate($data[$j]['begin']));
                                if ($beginI == $beginJ) {
                                    if ($data[$i]['applied_for'] > $data[$j]['applied_for']) {
                                        $removeArr[] = $j;
                                    }
                                }
                            }
                        }
                    }
                    $schedules = array();

                    for($k = 0; $k < count($data); $k++) {
                        $schedule = $data[$k];

                        if($schedule['applied_for'] == 1) {
                            $schedule['level'] =  __("School");
                        } elseif($schedule['applied_for'] == 2) {
                            //$class_level = $classLevelDao->getClassLevel($schedule['class_level_id']);
                            $class_level = getClassLevelData($schedule['class_level_id'], CLASS_LEVEL_INFO);
                            $schedule['level'] = $class_level['class_level_name'];
                        } elseif($schedule['applied_for'] == 3) {
                            //$classes = $classDao->getClass($schedule['class_id']);
                            $classes = getClassData($schedule['class_id'], CLASS_INFO);
                            $schedule['level'] = $classes['group_title'];
                        }
                        if(!in_array($k, $removeArr)) {
                            $schedule['use'] = STATUS_ACTIVE;
                        } else {
                            $schedule['use'] = STATUS_INACTIVE;
                        }
                        $schedules[] = $schedule;
                    }
                    return_json(array(
                        'code' => 200,
                        'message' => 'OK',
                        'data' => array(
                            'schedule_history' => $schedules
                        )
                    ));
                    break;

                case 'detail':
                    $schedule = $scheduleDao->getScheduleDetailForApi($_POST['schedule_id']);
                    if (is_null($schedule)) {
                        _api_error(404);
                    }

                    if($schedule['applied_for'] == 1) {
                        $schedule['level'] =  __("School");
                    } elseif($schedule['applied_for'] == 2) {
                        //$class_level = $classLevelDao->getClassLevel($schedule['class_level_id']);
                        $class_level = getClassLevelData($schedule['class_level_id'], CLASS_LEVEL_INFO);
                        $schedule['level'] = $class_level['class_level_name'];
                    } elseif($schedule['applied_for'] == 3) {
                        //$classes = $classDao->getClass($schedule['class_id']);
                        $classes = getClassData($schedule['class_id'], CLASS_INFO);
                        $schedule['level'] = $classes['group_title'];
                    }

                    // Tăng số lượt tương tác - TaiLa
                    addInteractive($class['school_id'], 'schedule', 'school_view', $schedule['schedule_id']);

                    return_json(array(
                        'code' => 200,
                        'message' => 'OK',
                        'data' => array(
                            'schedule_detail' => $schedule
                        )
                    ));
                    break;
            }

            break;


        default:
            _api_error(400);
            break;
    }
}   catch (Exception $e) {
    $db->rollback();
    _api_error(0, $e->getMessage());
} finally {
    $db->autocommit(true);
}

?>