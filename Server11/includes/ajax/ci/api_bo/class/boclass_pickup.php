<?php
/**
* Package: ajax/ci/bo/class
*
 * @package ConIu
* @author ConIu
*/

include_once(DAO_PATH.'dao_class.php');
include_once(DAO_PATH.'dao_child.php');
include_once(DAO_PATH.'dao_parent.php');
include_once(DAO_PATH.'dao_user.php');
include_once(DAO_PATH.'dao_pickup.php');
$classDao = new ClassDAO();
$childDao = new ChildDAO();
$parentDao = new ParentDAO();
$userDao = new UserDAO();
$pickupDao = new PickupDAO();

//Lấy ra thông tin lớp
//$class = $classDao->getClassByUsername($_POST['group_name'], $user->_data['user_id']);
$class = getClassDataByUsername($_POST['group_name'], CLASS_INFO);
if (is_null($class)) {
    _api_error(403);
}

try {
    $return = array();
    $db->autocommit(false);
    // Tăng số lượt tương tác - TaiLa
    addInteractive($class['school_id'], 'pickup', 'school_view');
    switch ($_POST['do']) {
        case 'list_child':
            // Lấy danh sách trẻ lớp đón muộn
            $args = array();
            $child_count = 0;
            $date = date('Y-m-d');
            $defaultIds[] = 0;

            $pickupIds = $pickupDao->getPickupIds($class['school_id'], $date);
            if(is_null($pickupIds)) {
                $pickupIds = $defaultIds;
            }
            $children = $pickupDao->getChildOfClassInDay($class['group_id'], $pickupIds, $date, $child_count);

            // return & exit
            return_json(array(
                'code' => 200,
                'message' => 'OK',
                'data' => array(
                    'registered_count' => $child_count,
                    'list_child' => $children
                )
            ));

            break;

        case 'manage':
            //Hiển thị thông tin lớp đón muộn
            $children = array();
            $child_count = 0;
            $pickup_time = date('Y-m-d');
            $pickup = $pickupDao->getPickupAssign($class['school_id'], $pickup_time);
            if (is_null($pickup)) {
                throw new Exception(__("Thông tin lớp đón muộn không đúng"));
            }
            $children = $pickupDao->getChildOfPickup($pickup['pickup_id'], $child_count);

            //Được phân công trông muộn?
            $is_assigned_pickup = $pickupDao->checkPickupPermission($class['school_id']);

            // return & exit
            return_json(array(
                'code' => 200,
                'message' => 'OK',
                'data' => array(
                    'late_pickup' => $children,
                    'picked_up' => $child_count,
                    'total' => $pickup['total'],
                    'pickup_id' => $pickup['pickup_id'],
                    'pickup_class_id' => $pickup['pickup_class_id'],
                    'pickup_class_name' => $pickup['class_name'],
                    'is_assigned_pickup' => $is_assigned_pickup
                )
            ));

            break;

        case 'save_pickup_child':
            $pickup = $pickupDao->getPickup($_POST['pickup_id']);
            if (is_null($pickup)) {
                throw new Exception(__("Thông tin lớp đón muộn không đúng"));
            }
            $args = array();
            $args['using_at'] = $pickup['pickup_time'];
            $args['pickup_id'] = $pickup['pickup_id'];
            $args['child_id'] = $_POST['child_id'];
            $args['class_id'] = $class['group_id'];
            $args['school_id'] = $class['school_id'];
            $args['type'] = 0; //Giáo viên ghi sử dụng dịch vụ type = 0

            $childFee = $pickupDao->getPickupChildFee($args['pickup_id'], $args['child_id']);
            $latePickupFee = isset($childFee['late_pickup_fee']) ? $childFee['late_pickup_fee'] : 0;
            $totalAmount = isset($childFee['total_amount']) ? $childFee['total_amount'] : 0;

            $pickupTime = $_POST['pickup_at'];
            $total = $pickup['total'];
            $pickupFee = 0;
            $totalChild = 0;

            if (is_empty($pickupTime)) {
                $args['status'] = 0;
                if ($totalAmount >= $latePickupFee) {
                    $totalChild = $totalAmount - $latePickupFee;
                }
            } else {
                $args['status'] = 1;
                $pickupFee = $pickupDao->getPickupFee($args['school_id'], $pickupTime);
                $totalChild = $totalAmount + $pickupFee - $latePickupFee;
            }

            $db->begin_transaction();
            // Lấy dịch vụ sử dụng
            $serviceIds = isset($_REQUEST['service']) ? $_REQUEST['service'] : array();
            // Lấy dịch vụ đã sử dụng
            $serviceUsedIds = $pickupDao->getIdServiceUsage($args['pickup_id'], $args['child_id']);
            //$serviceUsedIds = isset($_REQUEST['service_used']) ? $_REQUEST['service_used'] : array();
            // Lấy ra dịch vụ mới đăng ký
            $args['recordServiceIds'] = array_values($serviceIds);
            $args['deleteServiceIds'] = array_values(array_diff($serviceUsedIds, $serviceIds));

            // Xóa dịch vụ ghi nhầm
            if (!empty($args['deleteServiceIds'])) {
                $deleteServicePrice = $pickupDao->deletePickupServiceUsage($args);
                $totalChild = $totalChild - $deleteServicePrice;
            }

            // Ghi sử dụng dịch vụ mới
            if (!empty($args['recordServiceIds'])) {
                $recordServicePrice = $pickupDao->recordPickupServiceUsage($args);
                $totalChild = $totalChild + $recordServicePrice;
            }

            $args['description'] = $_POST['note'];
            $args['pickup_time'] = $pickupTime;
            $args['pickup_fee'] = $pickupFee;
            $args['total_child'] = $totalChild;
            $total = $total - $totalAmount + $totalChild;
            $total = ($total > 0) ? $total : 0;

            // Update các thông tin chung của lần đón muộn (ci_pickup_child)
            $pickupDao->updatePickupChildApi($args);
            // Update các tổng tiền đón muộn (ci_pickup)
            $pickupDao->updateTotalPickup($args['pickup_id'], $total);

            $db->commit();
            // return & exit
            return_json(array(
                'code' => 200,
                'message' => __("Lưu thông tin đón muộn thành công"),
                'data' => array()
            ));

            break;

        case 'assign':
            // Lấy thông tin giáo viên được phân công trong tuần này và tuần sau
            $this_monday = date('Y-m-d', strtotime('monday this week'));
            $next_monday = date('Y-m-d', strtotime('monday next week'));

            $assign_this_week = $pickupDao->getAssignInWeekForApi($class['school_id'], $this_monday);
            $assign_next_week = $pickupDao->getAssignInWeekForApi($class['school_id'], $next_monday);

            // return & exit
            return_json(array(
                'code' => 200,
                'message' => 'OK',
                'data' => array(
                    'this_week' => $assign_this_week,
                    'next_week' => $assign_next_week
                )
            ));

            break;

        case 'pickup':

            $pickup = $pickupDao->getPickup($_POST['pickup_id']);
            if (is_null($pickup)) {
                throw new Exception(__("Thông tin lớp đón muộn không đúng"));
            }

            //Xử lý nghiệp vụ khi 1 trẻ được trả khỏi lớp
            $args = array();
            $args['pickup_at'] = date('H:i');
            $args['class_id'] = $class['group_id'];
            $args['school_id'] = $class['school_id'];
            $args['pickup_id'] = $_POST['pickup_id'];
            $args['child_id'] = $_POST['child_id'];
            // Lấy ra thông tin trẻ đã có (tiền đón muộn + tiền sử dụng dịch vụ)
            $childFee = $pickupDao->getPickupChildFee($args['pickup_id'], $args['child_id']);
            $latePickupFee = isset($childFee['late_pickup_fee']) ? $childFee['late_pickup_fee'] : 0;
            $totalAmount = isset($childFee['total_amount']) ? $childFee['total_amount'] : 0;
            // Tính tiền đón muộn theo thời gian trả trẻ
            $pickupFee = $pickupDao->getPickupFee($args['school_id'], $args['pickup_at']);

            // Khởi tạo các giá trị truyền vào
            $args['late_pickup_fee'] = $pickupFee;
            $args['total_child'] = $totalAmount + $pickupFee - $latePickupFee;
            $total = $pickup['total'] - $totalAmount + $args['total_child'];
            $args['total'] = ($total > 0) ? $total : 0;

            $db->begin_transaction();
            $pickupDao->recordPickupOfChild($args);

            // Thông báo cho phụ huynh khi trẻ được trả
            //$child = $childDao->getChild($args['child_id']);
            $child = getChildData($args['child_id'], CHILD_INFO);
            //$parentIds = array();
            //$parentIds = $parentDao->getParentIds($args['child_id']);
            $parents = getChildData($args['child_id'], CHILD_PARENTS);
            $parentIds = array_keys($parents);
            foreach ($parentIds as $parentId) {
                $userDao->postNotifications($parentId, NOTIFICATION_CHILD_PICKEDUP, NOTIFICATION_NODE_TYPE_CHILD,
                    $args['pickup_id'], $args['pickup_at'], $args['child_id'], convertText4Web($child['child_name']));
            }

            $db->commit();

            // return & exit
            return_json(array(
                'code' => 200,
                'message' => 'Lưu thông tin trả trẻ thành công',
                'data' => array()
            ));

            break;

        case 'cancel':
            //Xử lý nghiệp vụ khi cô giáo hủy 1 trẻ khỏi lớp trông muộn
            $args = array();
            $pickup = $pickupDao->getPickup($_POST['pickup_id']);
            if (is_null($pickup)) {
                throw new Exception(__("Thông tin lớp đón muộn không đúng"));
            }

            $args['class_id'] = $class['group_id'];
            $args['school_id'] = $class['school_id'];
            $args['pickup_id'] = $_POST['pickup_id'];
            $args['child_id'] = $_POST['child_id'];
            $args['remove_child'][] = $_POST['child_id'];

            $args['deduction'] = $pickupDao->getTotalAmountPickupChild($args);
            $args['count_remove'] = 1;

            $db->begin_transaction();
            $pickupDao->deletePickupChild($args);

            /*// Thông báo cho phụ huynh khi trẻ bị hủy khỏi lớp trông muộn
            //$child = $childDao->getChild($args['child_id']);
            $child = getChildData($args['child_id'], CHILD_INFO);
            //$parentIds = array();
            //$parentIds = $parentDao->getParentIds($args['child_id']);
            $parents = getChildData($args['child_id'], CHILD_PARENTS);
            $parentIds = array_keys($parents);
            foreach ($parentIds as $parentId) {
                $userDao->postNotifications($parentId, NOTIFICATION_REMOVE_LATEPICKUP_CLASS, NOTIFICATION_NODE_TYPE_CHILD,
                    $args['pickup_id'], toSysDate($pickup['pickup_time']), $args['child_id'], convertText4Web($child['child_name']));
            }*/

            $db->commit();

            // return & exit
            return_json(array(
                'code' => 200,
                'message' => 'Hủy trẻ khỏi lớp đón muộn thành công',
                'data' => array()
            ));

            break;

        case 'list_class':
            //$classes = $classDao->getClassesOfSchool($class['school_id']);
            $schoolClass = getSchoolData($class['school_id'], SCHOOL_CLASSES);
            $schoolClass = sortArray($schoolClass, "ASC", "class_level_id");
            $classes = array();
            foreach ($schoolClass as $class) {
                $result['group_id'] = $class['group_id'];
                $result['group_title'] = $class['group_title'];
                $result['group_name'] = $class['group_name'];
                $classes[] = $result;
            }

            // return & exit
            return_json(array(
                'code' => 200,
                'message' => 'OK',
                'data' => array(
                    'classes' => $classes
                )
            ));

            break;

        case 'list_child_pickup':
            $pickup_id = $_POST['pickup_id'];
            $pickup = $pickupDao->getPickup($pickup_id);
            if (is_null($pickup)) {
                throw new Exception(__("Thông tin lớp đón muộn không đúng"));
            }

            $class_id = $_POST['class_id'];
            $pickup_class_id = $pickup['pickup_class_id'];

            $child_count = 0;
            //$date = isset($_POST['add_to_date']) ? toDBDate($_POST['add_to_date']) : date('Y-m-d');
            $childrenRegistered = array();

            $pickupIds = $pickupDao->getPickupIds($class['school_id'], $pickup['pickup_time']);
            if (!is_null($pickupIds) && $pickup_class_id > 0) {
                $childrenRegistered = $pickupDao->getChildRegisteredApi($class_id, $pickupIds, $pickup_class_id);
            }
            $children = $pickupDao->getChildOfClassApi($class_id, $pickup_id, $pickup_class_id, $child_count, $pickup['pickup_time'], $childrenRegistered);

            // return & exit
            return_json(array(
                'code' => 200,
                'message' => 'OK',
                'data' => array(
                    'list_child' => $children,
                    'registered_child' => $child_count
                )
            ));

            break;

        case 'add_child':
            //Xử lý nghiệp vụ khi thêm trẻ vào lớp trông muộn
            $template = $pickupDao->getTemplate($class['school_id']);
            if (is_null($template)) {
                throw new Exception(__("The school has not establish the late pickup configuration"));
            }

            $pickup = $pickupDao->getPickup($_POST['pickup_id']);
            if (is_null($pickup)) {
                throw new Exception(__("Thông tin lớp đón muộn không đúng"));
            }

            $args = array();
            $args['pickup_id'] = $pickup['pickup_id'];
            $args['pickup_time'] = $pickup['pickup_time'];
            $args['pickup_day'] = $pickup['pickup_day'];

            $args['class_id'] = $_POST['class_id'];
            $args['pickup_class_id'] = $pickup['pickup_class_id'];
            $args['school_id'] = $class['school_id'];
            $args['status'] = 0;
            $args['type'] = 0; // Giáo viên đăng ký type = 0

            //Lấy id của những trẻ thêm vào trông muộn
            $args['new_child'] = isset($_REQUEST['childIds'])? $_REQUEST['childIds']: array();
            //Lấy id của những trẻ đã có trong lớp
            $args['old_child'] = $pickupDao->getChildIdOfPickup($args['pickup_id'], $args['class_id']);
            $args['remove_child'] = array_diff($args['old_child'], $args['new_child']);

            $db->begin_transaction();
            if (count($args['remove_child']) > 0) {
                $args['deduction'] = $pickupDao->getTotalAmountPickupChild($args);
                $args['count_remove'] = count($args['remove_child']);
                $pickupDao->deletePickupChild($args);
            }


            /*if ($args['pickup_id'] > 0) {
                //Lấy id của những trẻ đã có trong lớp
                $args['old_child'] = $pickupDao->getChildIdOfPickup($args['pickup_id'], $args['class_id']);
                $args['remove_child'] = array_diff($args['old_child'], $args['new_child']);

                if ((strtotime(date('H:i:s')) > strtotime($template['begin_pickup_time'])) &&
                    count($args['remove_child']) > 0 ){
                    throw new Exception(__("Không thể hủy trẻ đã đăng ký sau"). ": " . $template['begin_pickup'] . " giờ");
                } elseif (count($args['remove_child']) > 0) {
                    $args['deduction'] = $pickupDao->getTotalAmountPickupChild($args);
                    $args['count_remove'] = count($args['remove_child']);
                    $pickupDao->deletePickupChild($args);
                }
            } else {
                $args['template_id'] = $template['template_id'];
                $args['pickup_id'] = $pickupDao->insertPickup($args);
            }*/

            // Thêm trẻ vào lớp đón muộn
            $args['add_child'] = array_diff($args['new_child'], $args['old_child']);
            if (count($args['add_child']) > 0) {
                $pickupDao->insertPickupChild($args);
            }

            $parents = $_REQUEST['registered'];
            $parentIds = $_REQUEST['registered_user_id'];
            $parentNames = $_REQUEST['registered_user_fullname'];
            //Lấy mô tả của những trẻ thêm vào trông muộn
            $descriptions = isset($_REQUEST['notes'])? $_REQUEST['notes']: array();

            // Lưu thông tin đăng ký dịch vụ
            foreach ($args['new_child'] as $presentChildId) {
                $args['child_id'] = $presentChildId;
                $args['description'] = $descriptions[$presentChildId];
                    $pickupDao->updateNoteOfChildApi($args);
            }

            /*// Thông báo cho phụ huynh biết trẻ được thêm vào lớp trông muộn
            foreach ($args['add_child'] as $childId) {
                // Lấy danh sách phụ huynh của trẻ
                //$child = $childDao->getChild($childId);
                $child = getChildData($childId, CHILD_INFO);
                //$parentIds = array();
                //$parentIds = $parentDao->getParentIds($childId);
                $parents = getChildData($childId, CHILD_PARENTS);
                $parentIds = array_keys($parents);
                foreach ($parentIds as $parentId) {
                    $userDao->postNotifications($parentId, NOTIFICATION_ADD_LATEPICKUP_CLASS, NOTIFICATION_NODE_TYPE_CHILD,
                        $args['pickup_id'], toSysDate($args['pickup_time']), $childId, convertText4Web($child['child_name']));
                }
            }*/

            $db->commit();

            // return & exit
            return_json(array(
                'code' => 200,
                'message' => __("Thêm trẻ vào lớp đón muộn thành công"),
                'data' => array()
            ));

            break;

        case 'history':

            if (validateDate($_POST['fromDate']) || validateDate($_POST['toDate'])) {
                $pickups = $pickupDao->getPickupTeacherInMonth($class['school_id'], $user->_data['user_id'], toDBDate($_POST['fromDate']), toDBDate($_POST['toDate']));
            } else {
                $pickups = $pickupDao->getPickupTeacherInMonth($class['school_id'], $user->_data['user_id'], date('Y-m-01'), date('Y-m-t'));
            }
            $class_history = empty($pickups) ? null : $pickups;

            //Lấy ra cấu hình cho phép sửa đón muộn trọng quá khứ
            $schoolConfig = getSchoolData($class['school_id'], SCHOOL_CONFIG);
            $allow_teacher_edit_pickup_before = $schoolConfig['allow_teacher_edit_pickup_before'] ? true : false;

            // return & exit
            return_json(array(
                'code' => 200,
                'message' => __("OK"),
                'data' => array(
                    'class_history' => $class_history,
                    'allow_teacher_edit_pickup_before' => $allow_teacher_edit_pickup_before
                )
            ));

            break;

        case 'detail':

            //Hiển thị thông tin lớp đón muộn
            $children = array();
            $child_count = 0;
            $pickup = $pickupDao->getPickup($_POST['pickup_id']);
            if (is_null($pickup)) {
                throw new Exception(__("Thông tin lớp đón muộn không đúng"));
            }
            $children = $pickupDao->getChildOfPickup($pickup['pickup_id'], $child_count);

            // return & exit
            return_json(array(
                'code' => 200,
                'message' => 'OK',
                'data' => array(
                    'late_pickup' => $children,
                    'picked_up' => $child_count,
                    'total' => $pickup['total'],
                    'pickup_id' => $pickup['pickup_id'],
                    'pickup_class_id' => $pickup['pickup_class_id'],
                    'pickup_class_name' => $pickup['class_name']
                )
            ));

            break;

        case 'save':
            $pickup = $pickupDao->getPickup($_POST['pickup_id']);
            if (is_null($pickup)) {
                throw new Exception(__("Thông tin lớp đón muộn không đúng"));
            }
            //Xử lý nghiệp vụ khi cô giáo ghi sử dụng dịch vụ
            $args = array();
            $args['pickup_date'] = $pickup['pickup_time'];
            $args['using_at'] = $pickup['pickup_time'];
            $args['class_id'] = $class['group_id'];
            $args['school_id'] = $class['school_id'];
            $args['pickup_id'] = $pickup['pickup_id'];

            //Giáo viên ghi sử dụng dịch vụ type = 0
            $args['type'] = 0;
            $childIds = $_REQUEST['list_child'];
            $pickupTimes = $_REQUEST['pickup_time'];
            //$serviceFees = $_REQUEST['service_fee'];
            $pickupChildFees = $pickupDao->getPickupChildsFee($pickup['pickup_id']);
            $childsInClass = array_keys($pickupChildFees);
            //$pickupFees = $_REQUEST['late_pickup_fee'];
            //$totalChild = $_REQUEST['total_amount'];
            $services = $_REQUEST['services'];
            $notes = $_REQUEST['notes'];
            $total = 0;

            $db->begin_transaction();
            for ($idx = 0; $idx < count($childIds); $idx++) {

                $args['child_id'] = $childIds[$idx];
                if (!in_array($args['child_id'], $childsInClass)) continue;
                $latePickupFee = $pickupChildFees[$args['child_id']]['late_pickup_fee'];
                $totalAmount = $pickupChildFees[$args['child_id']]['total_amount'];

                $pickupTime = $pickupTimes[$idx];
                $pickupFee = 0;
                $totalChild = 0;

                if (is_empty($pickupTime)) {
                    $args['status'] = 0;
                    if ($latePickupFee >= 0 && $totalAmount >= $latePickupFee) {
                        $totalChild = $totalAmount - $latePickupFee;
                    }
                } else {
                    $args['status'] = 1;
                    $pickupFee = $pickupDao->getPickupFee($args['school_id'], $pickupTime);
                    $totalChild = $totalAmount + $pickupFee - $latePickupFee;
                }

                // Lấy dịch vụ sử dụng
                $serviceIds = isset($services[$args['child_id']]) ? $services[$args['child_id']] : array();
                // Lấy dịch vụ đã sử dụng
                $serviceUsedIds = $pickupDao->getIdServiceUsage($args['pickup_id'], $args['child_id']);
                //$serviceUsedIds = isset($_REQUEST['service_used']) ? $_REQUEST['service_used'] : array();
                // Lấy ra dịch vụ mới đăng ký
                //$args['recordServiceIds'] = array_values(array_diff($serviceIds, $serviceUsedIds));
                $args['recordServiceIds'] = array_values($serviceIds);
                $args['deleteServiceIds'] = array_values(array_diff($serviceUsedIds, $serviceIds));

                // Xóa dịch vụ ghi nhầm
                if (!empty($args['deleteServiceIds'])) {
                    $deleteServicePrice = $pickupDao->deletePickupServiceUsage($args);
                    $totalChild = $totalChild - $deleteServicePrice;
                }

                // Ghi sử dụng dịch vụ mới
                if (!empty($args['recordServiceIds'])) {
                    $recordServicePrice = $pickupDao->recordPickupServiceUsage($args);
                    $totalChild = $totalChild + $recordServicePrice;
                }

                $args['pickup_time'] = $pickupTime;
                $args['pickup_fee'] = $pickupFee;
                $args['description'] = $notes[$idx];
                $args['total_child'] = $totalChild;
                $total = $total + $totalChild;

                // Update các thông tin chung của lần đón muộn (ci_pickup_child)
                $pickupDao->updatePickupChildApi($args);
            }
            //Update các tổng tiền đón muộn (ci_pickup)
            $pickupDao->updateTotalPickup($args['pickup_id'], $total);

            $db->commit();
            // return & exit
            return_json(array(
                'code' => 200,
                'message' => __("Lưu thông tin đón muộn thành công"),
                'data' => null
            ));

            break;


        default:
            _error(400);
            break;
    }
} catch (Exception $e) {
    $db->rollback();
    _api_error(0, $e->getMessage());
} finally {
    $db->autocommit(true);
}

?>