<?php
/**
 * Package: ajax/ci/bo
 * 
 * @package ConIu
 * @author QuanND
 */

require(ABSPATH . 'includes/class-image.php');

include_once(DAO_PATH . 'dao_child.php');
include_once(DAO_PATH . 'dao_parent.php');
include_once(DAO_PATH . 'dao_school.php');
include_once(DAO_PATH . 'dao_class.php');
include_once(DAO_PATH . 'dao_subject.php');
include_once(DAO_PATH . 'dao_teacher.php');
include_once(DAO_PATH . 'dao_user.php');
include_once(DAO_PATH . 'dao_mail.php');
include_once(DAO_PATH . 'dao_journal.php');

$journalDao = new JournalDAO();
$childDao = new ChildDAO();
$parentDao = new ParentDAO();
$schoolDao = new SchoolDAO();
$subjectDao = new SubjectDAO();
$classDao = new ClassDAO();
$teacherDao = new TeacherDAO();
$userDao = new UserDAO();
$mailDao = new MailDAO();

//Lấy ra thông tin lớp
//$class = $classDao->getClassByUsername($_POST['group_name'], $user->_data['user_id']);
$class = getClassDataByUsername($_POST['group_name'], CLASS_INFO);

// Lấy cấu hình thông báo của trường
$notify_settings = getSchoolData($class['school_id'], SCHOOL_NOTIFICATIONS);
if (is_null($class)) {
    _api_error(403);
}

try {
//    $args = array();
//    $args['user_id'] = $user->_data['user_id'];
//    $args['child_name'] = $_POST['child_name'];
//    $args['description'] = $_POST['description'];
//    $args['gender'] = $_POST['gender'];
//    $args['parent_phone'] = standardizePhone($_POST['parent_phone']);
//    /*if (!validatePhone($args['parent_phone'])) {
//        _api_error(0, __("The phone number you entered is incorrect"));
//    }*/
//    $args['parent_email'] = $_POST['parent_email'];
//    $args['address'] = $_POST['address'];
//    $args['birthday'] = $_POST['birthday'];
//    $args['begin_at'] = $_POST['begin_at'];

    $args = array();
    $args['user_id'] = $user->_data['user_id'];
    $args['child_id'] = $_POST['child_id'];
    $args['child_code'] = isset($_POST['child_code'])? trim($_POST['child_code']): "";
    $args['first_name'] = trim($_POST['first_name']);
    $args['last_name'] = trim($_POST['last_name']);
    $args['child_name'] = $args['last_name']." ".$args['first_name'];
    $args['name_for_sort'] = convert_to_en_4sort($args['first_name'].$args['last_name']);
    $args['description'] = isset($_POST['description'])? trim($_POST['description']) : "";
    $args['gender'] = $_POST['gender'];
    $args['parent_phone'] = standardizePhone($_POST['parent_phone']);
    /*if (!validatePhone($args['parent_phone'])) {
        return_json( array('error' => true, 'message' => __("The phone number you entered is incorrect")) );
    }*/
    $args['parent_email'] = isset($_POST['parent_email'])? trim($_POST['parent_email']) : "";
    $args['parent_name'] = (isset($_POST['parent_name']) && $_POST['parent_name'] != '')? trim($_POST['parent_name']) : __("Parent of")." ".$args['child_name'];
    $args['address'] = isset($_POST['address'])? trim($_POST['address']) : "";
    $args['birthday'] = isset($_POST['birthday'])? trim($_POST['birthday']) : "";
    $args['begin_at'] = $_POST['begin_at'];
    $args['school_id'] = $class['school_id'];
    $args['class_id'] = $class['group_id'];

    if(isset($_POST['password'])) {
        $checkPass = $userDao->comparePassword($user->_data['user_id'], $_POST['password']);
        //if($userPass != md5($user_password)) {
        if(!$checkPass) {
            throw new Exception(__("The password you entered is incorrect"));
        }
    }

    $db->autocommit(false);

    switch ($_POST['do']) {
        case 'edit':
            /**
             * Hàm này xử lý khi giáo viên edit thông tin trẻ
             */

            if(isset($_POST['begin_at']) && !validateDate($_POST['begin_at'])) {
                throw new Exception(__("You must enter study start date"));
            }
            $birthday = toDBDate($args['birthday']);
            $beginAt = toDBDate($args['begin_at']);
            if(strtotime($birthday) > strtotime($beginAt)) {
                throw new Exception(__("Begin at not before birthdate"));
            }

            //Validate điều kiện tạo tài khoản phụ huynh
            $create_parent_account = $_POST['create_parent_account'];
            if ($create_parent_account && !is_empty($args['parent_email']) && !valid_email($args['parent_email'])) {
                throw new Exception(__("To create parent account, you must enter correct parent email or parent email is empty."));
            }

            // Lấy thông tin trường
            //$school = $schoolDao->getSchoolById($class['school_id']);
            $school = getSchoolData($class['school_id'], SCHOOL_DATA);

            $oldChild = getChildData($_POST['child_id'], CHILD_INFO);
            if (is_null($oldChild)) {
                _error(404);
            }

            $child_parent_id = $childDao->getChildParentIdFromChildId($_POST['child_id']);

            // Lấy thông tin trẻ
            $child = $childDao->getChildByParent($child_parent_id);
            if (is_null($child)) {
                _error(404);
            }
            $db->begin_transaction();

            //1. Cập nhật thông tin trẻ trong bảng ci_child
            $args['child_id'] = $_POST['child_id'];
            $childDao->editChild($args);

            // Nếu trẻ đang đi học ở trường thì mới cập nhật thông tin trong bảng ci_child_parent
            if($oldChild['status']) {
                // 1.1 Cập nhật thông tin trẻ trong bảng ci_child_parent
                $args['child_parent_id'] = $child_parent_id;
                $childDao->editChildBySchool($args);
            }

            $class_id = $class['group_id'];

            //3. Cập nhật thông tin trẻ trong trường (cập nhật bảng ci_school_child)
            $argsCC['begin_at'] = isset($_POST['begin_at'])? trim($_POST['begin_at']) : "";
            $argsCC['class_id'] = $class_id;
            $argsCC['child_id'] = $_POST['child_id'];
            $argsCC['school_id'] = $school['page_id'];

            $childDao->updateSchoolChild($argsCC);

            // Lấy danh sách phụ huynh
            $parents = getChildData($_POST['child_id'], CHILD_PARENTS);
            $oldParentIds = array_keys($parents);
            $newParentIds = (isset($_REQUEST['user_id_parent']) && (count($_REQUEST['user_id_parent']) > 0))? $_REQUEST['user_id_parent']: array();

            // Xóa danh sách cha mẹ cũ đi
            $deletedParentIds = array_diff($oldParentIds, $newParentIds);
            if (count($deletedParentIds) > 0) {
                // Không được xóa phụ huynh admin của trẻ
                if(in_array($child['child_admin'], $deletedParentIds)) {
                    throw new Exception(__("You do not have the right to remove a parent who administers a student"));
                }
                if($oldChild['status']) {
                    //Xóa cha mẹ khỏi lớp cũ nếu trẻ đang đi học
                    $classDao->deleteUserFromClass($oldChild['class_id'], $deletedParentIds);
                }
                //Xóa danh sách cha mẹ trong danh sách đc quản lý (bảng ci_user_manage)
                $parentDao->deleteParentList($_POST['child_id'], $deletedParentIds);

                // Nếu trẻ đang đi học thì mới cập nhật thông tin phụ huynh trong bảng ci_parent_manage
                if($oldChild['status']) {
                    // Xóa danh sách cha mẹ trong bảng ci_parent_manage
                    $parentDao->deleteParentListByParent($child_parent_id, $deletedParentIds);
                }
                if($oldChild['status']) {
                    //Xóa danh sách cha mẹ like page trường (nếu trẻ đang học tại trường)
                    $schoolDao->deleteUserLikeSchool($school['page_id'], $deletedParentIds); //Xem xét có thể để cha mẹ like page của trường, ko cần thiết phải xóa
                }
            }

            //3.1. Tự động tạo tài khoản phụ huynh
            if ($create_parent_account) {
                $argsParent = array();
                $argsParent['full_name'] = $args['parent_name'];
                $argsParent['first_name'] = split_name($argsParent['full_name'])[0];
                $argsParent['last_name'] = split_name($argsParent['full_name'])[1];
                $argsParent['username'] = generateUsername($argsParent['full_name']);
                $argsParent['user_phone'] = standardizePhone($args['parent_phone']);
                /*if (!validatePhone($args['user_phone'])) {
                    return_json( array('error' => true, 'message' => __("The phone number you entered is incorrect")) );
                }*/
                $argsParent['email'] = ($args['parent_email'] != '') ? $args['parent_email'] : 'null';
                $argsParent['password'] = (is_empty($argsParent['user_phone'])? "coniu.vn": $argsParent['user_phone']);
                $argsParent['gender'] = FEMALE;

                $parentId = $userDao->createUser($argsParent);
                $newParentIds[] = $parentId;
                // Set child_admin cho trẻ
                $childDao->setChildAdmin($child_parent_id, $parentId);
                if($argsParent['email'] != 'null') {
                    //Gửi mail cho phụ huynh của trẻ.
                    $argEmail = array();
                    $argEmail['action'] = "create_parent_account";
                    $argEmail['receivers'] = $argsParent['email'];
                    $argEmail['subject'] = "[".convertText4Web($school['page_title'])."]".__("Use Inet application to connect student's parents");

                    //Tạo nên nội dung email
                    $smarty->assign('full_name', $argsParent['full_name']);
                    $smarty->assign('child_name', $args['child_name']);
                    $smarty->assign('username', $argsParent['email']);
                    $smarty->assign('password', $argsParent['password']);
                    $argEmail['content'] = $smarty->fetch("ci/email_templates/create_parent_account.tpl");

                    $argEmail['delete_after_sending'] = 1;
                    $argEmail['school_id'] = $school['page_id'];
                    $argEmail['user_id'] = $user->_data['user_id'];

                    $mailDao->insertEmail($argEmail);
                }
            }

            // Thêm danh sách phụ huynh mới
            $addedParentIds = array_diff($newParentIds, $oldParentIds);
            if (count($addedParentIds) > 0) {
                //3.2. Cho phụ huynh like các page của hệ thống, join các group của hệ thống
                $userDao->addUsersLikeSomePages($system_page_ids, $addedParentIds);
                $userDao->addUserToSomeGroups($system_group_ids, $addedParentIds);

                //4. Thêm cha mẹ vào nhóm của lớp (bao gồm việc tăng thành viên nhóm)
                $classDao->addUserToClass($class_id, $addedParentIds);

                //5. Thêm cha mẹ cho trẻ & người tạo ra thông tin trẻ vào danh sách người có thể quản lý
                $parentDao->addParentList($_POST['child_id'], $addedParentIds);

                // Nếu trẻ đang đi học tại trường thì mới cập nhật thông tin trong bảng ci_parent_manage
                if($oldChild['status']) {
                    //5.1 Thêm cha mẹ quản lý trẻ trong bảng ci_parent_manage
                    $childDao->createParentManageInfo($addedParentIds, $child_parent_id, $_POST['child_id'], $school['page_id']);
                }
                //7. Trẻ đang đi học và chưa có phụ huynh thì cập nhật child_admin(ci_child_parent)
                if (count($oldParentIds) == 0 && $oldChild['status']) {
                    $childAdmin = $addedParentIds[0];
                    $childParentId = $oldChild['child_parent_id'];
                    $childDao->updateChildForParent($childParentId, $childAdmin);
                }
                //6. Cho cha mẹ like trang của trường (tăng số like)
                $likeCnt = $schoolDao->addUsersLikeSchool($school['page_id'], $addedParentIds);
                $school['page_likes'] = $school['page_likes'] + $likeCnt;
            }

            //Cập nhật số lượng học sinh theo giới tính của trường
            if ($oldChild['gender'] != $args['gender']) {
                //Giảm giới tính cũ
                $schoolDao->updateGenderCount($school['page_id'], $oldChild['gender'], -1);
                //Tăng giới tính mới
                $schoolDao->updateGenderCount($school['page_id'], $args['gender'], 1);

                //Cập nhật thông tin trường
                if ($oldChild['gender'] == MALE) {
                    $school['male_count'] = $school['male_count'] - 1;
                    $school['female_count'] = $school['female_count'] + 1;
                } else {
                    $school['female_count'] = $school['female_count'] - 1;
                    $school['male_count'] = $school['male_count'] + 1;
                }
            }

            // Thông báo cho phụ huynh biết trẻ được sửa
            $userDao->postNotifications($newParentIds, NOTIFICATION_UPDATE_CHILD_SCHOOL, NOTIFICATION_NODE_TYPE_CHILD,
                $args['child_parent_id'], convertText4Web($args['child_name']), $args['child_parent_id'], convertText4Web($school['page_title']));

            /* ---------- UPDATE - MEMCACHE ---------- */
            //1. Cập nhật thông tin của trường
            $info_update = array(
                'page_likes' => $school['page_likes']
            );
            $config_update = array(
                'male_count' => $school['male_count'],
                'female_count' => $school['female_count']
            );
            updateSchoolData($school['page_id'], SCHOOL_INFO, $info_update);
            updateSchoolData($school['page_id'], SCHOOL_CONFIG, $config_update);

            //2. Cập nhật thông tin lớp
            if ($class_id > 0) {
                updateClassData($class_id, CLASS_INFO);
            }
            //3.Cập nhật thông tin trẻ
            updateChildData($_POST['child_id'], CHILD_INFO);
            updateChildData($_POST['child_id'], CHILD_PARENTS);

            //4. Cập nhật tin tin quản lý trẻ
            foreach ($addedParentIds as $parentId) {
                updateUserManageData($parentId, USER_MANAGE_CHILDREN);
            }
            foreach ($deletedParentIds as $parentId) {
                updateUserManageData($parentId, USER_MANAGE_CHILDREN);
            }
            /* ---------- END - MEMCACHE ---------- */

            // Check xem trẻ đã được sửa trước đó (có trong bảng ci_child_edit_history) chưa
            $issetEditHistory = $childDao->checkEditHistory($_POST['child_id'], $school['page_id']);

            $argc = array();
            $argc['edit_by'] = EDIT_BY_TEACHER;
            $argc['child_id'] = $_POST['child_id'];
            $argc['school_id'] = $school['page_id'];

            if($issetEditHistory) {
                // Cập nhật
                $childDao->updateChildEditHistory($argc);
            } else {
                // Thêm trẻ vào bảng ci_child_edit_history
                $childDao->insertChildEditHistory($argc);
            }

            $db->commit();
            // return
            return_json(array(
                'code' => 200,
                'message' => __("Done, Child info have been updated"),
                'data' => array()
            ));

            break;
        case 'add':
            //$school = $schoolDao->getSchoolByClass($class['group_id']);
            $school = getSchoolData($class['school_id'], SCHOOL_DATA);
            /**
             * Hàm này xử lý khi trường tạo ra thông tin một trẻ. Công việc bao gồm
             * 1. Tạo thông tin trẻ trong bảng ci_child
             * 2. Cập nhật danh sách lớp (đưa trẻ vào lớp - bảng ci_class_child)
             * 2.2 nhập thông tin của trẻ vào bảng điểm ci_point_
             * 2.3 Nhập thông tin của trẻ vào bảng hạnh kiểm ci_conduct
             * 3. Cập nhật thông tin trẻ trong trường (cập nhật bảng ci_school_child)
             * 3.5. Tự động tạo tài khoản phụ huynh
             * 4. Thêm cha mẹ vào nhóm của lớp (bao gồm việc tăng
             * 5. Thêm cha mẹ cho trẻ
             * 6. Cho cha mẹ like trang của trường
             * 7. Cho cha mẹ thành bạn của giáo viên (bao gồm cả quản lý trường)
             * 8. Tăng số lượng trẻ của trường
             */

            if(isset($_POST['begin_at']) && !validateDate($_POST['begin_at'])) {
                throw new Exception(__("You must enter study start date"));
            }
            $birthday = toDBDate($args['birthday']);
            $beginAt = toDBDate($args['begin_at']);
            if(strtotime($birthday) > strtotime($beginAt)) {
                _api_error(0, __("Begin at not before birthdate"));
            }

            //Validate điều kiện tạo tài khoản phụ huynh
            $create_parent_account = $_POST['create_parent_account'];
            if ($create_parent_account && !is_empty($args['parent_email']) && !valid_email($args['parent_email'])) {
                throw new Exception(__("To create parent account, you must enter correct parent email or parent email is empty."));
            }
            $class_id = is_numeric($class['group_id']) ? $class['group_id'] : 0;
            $db->begin_transaction();

            $args['code_auto'] = 1;
            //Sinh mã trẻ nếu cần
            if ($args['code_auto']) {
                $args['child_code'] = generateCode($args['child_name']);
            }
            $childInfo = $childDao->getStatusChildInSchool($args['child_code'], $school['page_id']);
            // Nếu trẻ chưa có trong hệ thống thì tạo mới thông tin trẻ trong trường
            if ($childInfo['status'] == 0) {
                $args['child_parent_id'] = 0;
                //1. Tạo thông tin trẻ trong bảng ci_child
                $lastId = $childDao->createChild($args);

                //2. Cập nhật danh sách lớp (đưa trẻ vào lớp - bảng ci_class_child)
                // Nếu ko chọn class thì thêm trẻ vào trường, bỏ qua bước này
                $argsCC['class_id'] = $class_id;
                $argsCC['begin_at'] = isset($_POST['begin_at'])? trim($_POST['begin_at']) : "";
                $argsCC['child_id'] = $lastId;
                if ($class_id > 0) {
                    $childDao->addChildToClass($argsCC);
                    // ADD START MANHDD 03/06/2021
                    //2.2 nhập thông tin của trẻ vào bảng điểm ci_point_
                    $subjectOfClass = $subjectDao->getSubjectsByClassId($class_id,$class['school_id']);
                    if(count($subjectOfClass)>0) {
                        foreach($subjectOfClass as $subject) {
                            $childDao->addChildPoints($subject['subject_id'],$args['child_code'],$class_id,$subject['school_year']);
                        }
                    }
                    //2.3 Nhập thông tin của trẻ vào bảng hạnh kiểm ci_conduct
                    $school_year = date("Y", strtotime($_POST['begin_at'])).'-'.(date("Y", strtotime($_POST['begin_at']))+1);
                    $childDao->addChildConduct($class_id,$argsCC['child_id'],$school_year);
                    // ADD END MANHDD 03/06/2021
                }

                //3. Cập nhật thông tin trẻ trong trường (cập nhật bảng ci_school_child)
                $argsCC['school_id'] = $class['school_id'];
                $argsCC['child_parent_id'] = 0;
                $childDao->addChildToSchool($argsCC);

                $parentIds = (isset($_REQUEST['user_id_parent'])? $_REQUEST['user_id_parent'] : array());
                //3.1. Tự động tạo tài khoản phụ huynh
                if ($create_parent_account) {
                    $argsParent = array();
                    $argsParent['full_name'] = $args['parent_name'];
                    $argsParent['first_name'] = split_name($argsParent['full_name'])[0];
                    $argsParent['last_name'] = split_name($argsParent['full_name'])[1];
                    $argsParent['username'] = generateUsername($argsParent['full_name']);
                    $argsParent['user_phone'] = $args['parent_phone'];
                    /*if (!validatePhone($args['user_phone'])) {
                        return_json( array('error' => true, 'message' => __("The phone number you entered is incorrect")) );
                    }*/
                    $argsParent['email'] = ($args['parent_email'] != '') ? $args['parent_email'] : 'null';
                    $argsParent['password'] = (is_empty($argsParent['user_phone'])? "coniu.vn": $argsParent['user_phone']);
                    $argsParent['gender'] = FEMALE;

                    $parentId = $userDao->createUser($argsParent);
                    $parentIds[] = $parentId;

                    if($argsParent['email'] != 'null') {
                        //Gửi mail cho phụ huynh của trẻ.
                        $argEmail = array();
                        $argEmail['action'] = "create_parent_account";
                        $argEmail['receivers'] = $argsParent['email'];
                        $argEmail['subject'] = "[".convertText4Web($school['page_title'])."]".__("Use Inet application to connect student's parents");

                        //Tạo nên nội dung email
                        $smarty->assign('full_name', $argsParent['full_name']);
                        $smarty->assign('child_name', $args['child_name']);
                        $smarty->assign('username', $argsParent['email']);
                        $smarty->assign('password', $argsParent['password']);
                        $argEmail['content'] = $smarty->fetch("ci/email_templates/create_parent_account.tpl");

                        $argEmail['delete_after_sending'] = 1;
                        $argEmail['school_id'] = $class['school_id'];
                        $argEmail['user_id'] = $user->_data['user_id'];

                        $mailDao->insertEmail($argEmail);
                    }
                }
                if (count($parentIds) > 0){
                    //3.2. Cho phụ huynh like các page của hệ thống, join các group của hệ thống
                    $userDao->addUsersLikeSomePages($system_page_ids, $parentIds);
                    $userDao->addUserToSomeGroups($system_group_ids, $parentIds);

                    //4. Thêm cha mẹ vào nhóm của lớp (bao gồm việc tăng thành viên nhóm)
                    // Nếu chưa chọn lớp thì bỏ qua
                    if ($class_id > 0) {
                        $classDao->addUserToClass($class_id, $parentIds);
                    }

                    //5. Thêm cha mẹ cho trẻ
                    $parentDao->addParentList($lastId, $parentIds);

                    //6. Cho cha mẹ like trang của trường (tăng số like)
                    $schoolDao->addUsersLikeSchool($class['school_id'], $parentIds);

                    //7. Cho cha mẹ thành bạn của giáo viên (bao gồm cả quản lý trường)
                    // Nếu chưa chọn lớp thì bỏ qua
                    // Tạm thời không cho kết bạn giữa giáo viên, phụ huynh
                    /*if ($class_id > 0) {
                        $teacherIds = $teacherDao->getTeacherIDOfClass($class_id);
                    }
                    $teacherIds[] = $user->_data['user_id']; //User hiện tại là quản lý của trường
                    $userDao->becomeFriends($teacherIds, $parentIds);*/

                    //7. Tạo thông tin trẻ cho phụ huynh
                    $args['child_id'] = $lastId;
                    $args['child_admin'] = $parentIds[0];
                    $args['school_id'] = $school['page_id'];
                    $childParentId = $childDao->createChildForParent($args);

                    //7.1. Gắn trẻ được quản lý cho phụ huynh
                    $childDao->createParentManageInfo($parentIds, $childParentId, $lastId, $school['page_id']);
                } else {
                    //7. Tạo thông tin trẻ cho phụ huynh (ci_child_parent)
                    $args['child_id'] = $lastId;
                    $child['child_admin'] = 0;
                    $args['school_id'] = $school['page_id'];
                    $childParentId = $childDao->createChildForParent($args);
                }
                //8. Tăng số lượng trẻ của trường
                $schoolDao->updateGenderCount($class['school_id'], $args['gender'], 1);
                if ($args['gender'] == MALE) {
                    $school['male_count'] = $school['male_count'] + 1;
                } else {
                    $school['female_count'] = $school['female_count'] + 1;
                }

                // Thông báo cho quản lý trường biết
//                $userManagerIds = getUserIdsManagerReceiveNotify($class['school_id'], 'children', $school['page_admin']);
                $userManagerIds = getUserIdsReceiveNotifyOfSchoolForClass($notify_settings, 'children', $class['school_id'], $school['page_admin']);
                $userDao->postNotifications($userManagerIds, NOTIFICATION_NEW_CHILD_TEACHER, NOTIFICATION_NODE_TYPE_SCHOOL,
                    $args['child_id'], convertText4Web($args['child_name']), $school['page_name'], convertText4Web($class['group_title']));
            } else {
                throw new Exception(__("Student is existing in system, check student code"));
            }

            /* ---------- UPDATE - MEMCACHE ---------- */
            //1. Cập nhật thông tin của trường
            $info_update = array(
                'page_likes' => $school['page_likes']
            );
            $config_update = array(
                'male_count' => $school['male_count'],
                'female_count' => $school['female_count']
            );

            updateSchoolData($school['page_id'], SCHOOL_INFO, $info_update);
            updateSchoolData($school['page_id'], SCHOOL_CONFIG, $config_update);
            addSchoolData($school['page_id'], SCHOOL_CHILDREN, $lastId);

            //2. Cập nhật thông tin trẻ và lớp
            addClassData($class_id, CLASS_CHILDREN, $lastId);
            updateClassData($class_id, CLASS_INFO);

            //4. Cập nhật tin tin quản lý trẻ
            foreach ($parentIds as $parentId) {
                updateUserManageData($parentId, USER_MANAGE_CHILDREN);
            }
            /* ---------- END - MEMCACHE ---------- */
            $argc = array();
            $argc['edit_by'] = EDIT_BY_TEACHER;
            $argc['child_id'] = $lastId;
            $argc['school_id'] = $school['page_id'];

            // Thêm trẻ vào bảng ci_child_edit_history
            $childDao->insertChildEditHistory($argc);

            $db->commit();

            return_json(array(
                'code' => 200,
                'message' => __("Done, Child info has been created"),
                'data' => array()
            ));

            break;
        case 'approve':
            $school = getSchoolData($class['school_id'], SCHOOL_DATA);

            //Xác nhận user là phụ huynh của trẻ
            $db->begin_transaction();
            //1. Thêm cha mẹ cho trẻ
            $parentDao->addParentList($_POST['child_id'], [$_POST['parent_id']]);

            //2. Thêm cha mẹ vào nhóm của lớp (bao gồm việc tăng thành viên nhóm)
            $classDao->addUserToClass($class['group_id'], [$_POST['parent_id']]);
            //3. Cho cha mẹ like trang của trường (tăng số like)
            $schoolDao->addUsersLikeSchool($class['school_id'], [$_POST['parent_id']]);

            //4. Cho cha mẹ thành bạn của giáo viên (bao gồm cả quản lý trường)
            // Tạm thời không cho kết bạn giữa giáo viên, phụ huynh
            /*$teacherIds = $teacherDao->getTeacherIDOfClass($class['group_id']);
            //Lấy ra admin của trường
            $teacherIds[] = $schoolDao->getAdminId($class['school_id']);
            $userDao->becomeFriends($teacherIds, [$_POST['parent_id']]);*/

            $db->commit();

            /* ---------- UPDATE - MEMCACHE ---------- */
            //1. Cập nhật thông tin của trường
            $info_update = array(
                'page_likes' => $school['page_likes']
            );
            updateSchoolData($school['page_id'], SCHOOL_INFO, $info_update);
            //2. Cập nhật thông tin lớp
            updateClassData($class['group_id']);
            //3. Cập nhật thông tin trẻ
            updateChildData($_POST['child_id']);
            /* ---------- END - MEMCACHE ---------- */

            // return
            return_json(array(
                'code' => 200,
                'message' => __("Success"),
                'data' => array()
            ));
            break;

        case 'list_child_picker':
            if(!isset($_POST['child_id']) || !is_numeric($_POST['child_id'])) {
                _error(404);
            }
            //$data = $childDao->getChildInformation($_POST['child_id']);
            $data = getChildData($_POST['child_id'], CHILD_PICKER_INFO);
            unset($data['source_file']);
            // return
            return_json(array(
                'code' => 200,
                'message' => __("OK"),
                'data' => $data
            ));
            break;

        case 'confirm':
            if(!isset($_POST['child_id']) || !is_numeric($_POST['child_id'])) {
                _api_error(404);
            }
            $status = PICKER_CONFIRMED;
            // update trạng thái đã trả trẻ
            $childDao->updateChildInformationConfirm($_POST['child_id'], $status);

            // Thông báo đến phụ huynh
            //$child = $childDao->getChild($_POST['child_id']);
            $child = getChildData($_POST['child_id'], CHILD_INFO);
            //$parentIds = $parentDao->getParentIds($_POST['child_id']);
            $parents = getChildData($_POST['child_id'], CHILD_PARENTS);
            if (!is_null($child) && !is_null($parents)) {
                $parentIds = array_keys($parents);
                $userDao->postNotifications($parentIds, NOTIFICATION_CONFIRM_PICKER, NOTIFICATION_NODE_TYPE_CHILD, $_POST['child_id'], '', '', convertText4Web($child['child_name']));
            }
            $db->commit();

            /* ---------- UPDATE - MEMCACHE ---------- */
            //1.Cập nhật thông tin trẻ
            updateChildData($_POST['child_id'], CHILD_PICKER_INFO);
            /* ---------- END - MEMCACHE ---------- */

            // return
            return_json(array(
                'code' => 200,
                'message' => __("Success"),
                'data' => array()
            ));
            break;

//        case 'listchildedit':
//            // Lấy toàn bộ danh sách trẻ đang chờ xác nhận của lớp
//
//            $children = array();
//            $children = $childDao->getAllChildEditClassOfTeacher($class['school_id'], $class['group_id']);
//
//            // return
//            return_json(array(
//                'code' => 200,
//                'message' => 'OK',
//                'data' => array(
//                    'child_list' => $children,
//                )
//            ));
//            break;
//        case 'reedit':
//            // valid inputs
//            if(!isset($_POST['child_id']) || !is_numeric($_POST['child_id'])) {
//                _error(404);
//            }
//
//            //$child = $childDao->getChild($_POST['child_id']);
//            $child = getChildData($_POST['child_id'], CHILD_INFO);
//            if (is_null($child)) {
//                _error(404);
//            }
//            $childAdmin = $childDao->getChildAdminFromChildId($_POST['child_id']);
//
//            // lấy thông tin trẻ edit
//            $child = $childDao->getChildEditOfTeacher($_POST['child_id'], $class['school_id']);
//            $child['child_admin'] = $childAdmin;
//
//            // return
//            return_json(array(
//                'code' => 200,
//                'message' => 'OK',
//                'data' => array(
//                    'child' => $child,
//                )
//            ));
//            break;
//        case 'delete_edit':
//            // Xóa trẻ khỏi danh sách edit
//            $db->begin_transaction();
//            $childDao->deleteChildEditOfTeacher($class['school_id'], $_POST['child_id']);
//            $db->commit();
//            // return
//            return_json(array(
//                'code' => 200,
//                'message' => __("Success"),
//                'data' => array()
//            ));
//            break;
        case 'view_info':
            /**
             * Lấy thông tin sức khỏe của trẻ
             */
            if (!isset($_POST['child_id']) || !is_numeric($_POST['child_id'])) {
                _api_error(404);
            }

            //$childInfo = $childDao->getFullInfoOfChildByParent($_POST['child_id']);
            // Lấy child_parent_id
            $child_parent_id = $childDao->getChildParentIdFromChildId($_POST['child_id']);
            
            $childHealth = $childDao->getChildHealth($child_parent_id);
            $childGrowth = $childDao->getChildGrowthLastest($child_parent_id);
            $childGr = array();
            if(!is_null($childGrowth)) {
                $childGr[] = $childGrowth;
            }

            return_json(array(
                'code' => 200,
                'message' => __("OK"),
                'data' =>array(
                    'child_health' => $childHealth,
                    'child_growth' => $childGr
                )
            ));
            break;
        case 'add_growth':
            // Thêm thông tin chiều cao cân nặng của trẻ trong lớp
            if (!isset($_POST['child_id']) || !is_numeric($_POST['child_id'])) {
                _api_error(404);
            }
            $db->begin_transaction();

            // Lấy child_parent_id
            $child_parent_id = $childDao->getChildParentIdFromChildId($_POST['child_id']);

            $file_name = "";
            // Upload file đính kèm
            if (file_exists($_FILES['file']['tmp_name'])) {
                // check file upload enabled
                if (!$system['file_enabled']) {
                    _api_error(0, __("This feature has been disabled"));
                }

                // valid inputs
                if (!isset($_FILES["file"]) || $_FILES["file"]["error"] != UPLOAD_ERR_OK) {
                    _api_error(0, __("Something wrong with upload! Is 'upload_max_filesize' set correctly?"));
                }

                // check file size
                $max_allowed_size = $system['max_file_size'] * 1024;
                if ($_FILES["file"]["size"] > $max_allowed_size) {
                    _api_error(0, __("The file size is so big"));
                }

                // check file extesnion
                $extension = get_extension($_FILES['file']['name']);
                if (!valid_extension($extension, $system['file_medicine_extensions'])) {
                    //modal(MESSAGE, __("Upload Error"), __("The file type is not valid or not supported"));
                    _api_error(0, __("The file type is not valid or not supported") );
                }

                /* check & create uploads dir */
                $depth = '';
                $folder = 'growths';
                if (!file_exists($depth . $system['system_uploads_directory'] . '/' . $folder)) {
                    @mkdir($depth . $system['system_uploads_directory'] . '/' . $folder, 0777, true);
                }
                if (!file_exists($depth . $system['system_uploads_directory'] . '/' . $folder . '/' . date('Y'))) {
                    @mkdir($depth . $system['system_uploads_directory'] . '/' . $folder . '/' . date('Y'), 0777, true);
                }
                if (!file_exists($system['system_uploads_directory'] . '/' . $folder . '/' . date('Y') . '/' . date('m'))) {
                    @mkdir($depth . $system['system_uploads_directory'] . '/' . $folder . '/' . date('Y') . '/' . date('m'), 0777, true);
                }

                /* prepare new file name */
                $directory = $folder . '/' . date('Y') . '/' . date('m') . '/';
                $prefix = $system['uploads_prefix'] . '_' . md5(time() * rand(1, 9999));

                $image = new Image($_FILES["file"]["tmp_name"]);

                $file_name = $directory . $prefix . $image->_img_ext;
                $file_name_tmp = $directory . $prefix . '_tmp' . $image->_img_ext;

                $path = $depth . $system['system_uploads_directory'] . '/' . $file_name;
                $path_tmp = $depth . $system['system_uploads_directory'] . '/' . $file_name_tmp;

                /* check if the file uploaded successfully */
                if (!@move_uploaded_file($_FILES['file']['tmp_name'], $path_tmp)) {
                    _api_error(0, __("Sorry, can not upload the file"));
                }

                /* save the new image */
                $image->save($path, $path_tmp);

                /* delete the tmp image */
                unlink($path_tmp);
                $argsGR['source_file'] = $file_name;
                $argsGR['file_name'] = $_FILES['file']['name'];

            }

            $argsGR['child_parent_id'] = $child_parent_id;
            $argsGR['height'] = $_POST['height'];
            $argsGR['weight'] = $_POST['weight'];
            $argsGR['nutriture_status'] = $_POST['nutriture_status'];
            $argsGR['heart'] = $_POST['heart'];
            $argsGR['blood_pressure'] = $_POST['blood_pressure'];
            $argsGR['ear'] = $_POST['ear'];
            $argsGR['eye'] = $_POST['eye'];
            $argsGR['description'] = $_POST['description'];
            $argsGR['is_parent'] = 0;

            
            // Lấy thông tin chiều cao cân nặng của trẻ
            $growth = $childDao->getChildGrowth($child_parent_id);
            $growth_recorded = array();
            foreach ($growth as $row) {
                $growth_recorded[] = $row['recorded_at'];
            }
            // $date_now = date('d/m/Y');
            if(!isset($_POST['recorded_at']) || is_null($_POST['recorded_at'])) {
                _api_error(0, __("Data record date can not be empty"));
            }
            $recorded_at = $_POST['recorded_at'];
            $argsGR['recorded_at'] = $recorded_at;
            if(in_array($recorded_at, $growth_recorded)) {
                $growth_now = $childDao->getChildGrowthByDate($argsGR['recorded_at'], $child_parent_id);
                if(!isset($argsGR['source_file'])) {
                    $argsGR['source_file'] = $growth_now['source_file_path'];
                    $argsGR['file_name'] = $growth_now['file_name'];
                }
                $childDao->updateChildGrowthDateNow($argsGR);
                $updateIdGR = $growth_now['child_growth_id'];
            } else {
                $insertIdGR = $childDao->insertChildGrowth($argsGR);
            }

            // Lấy thông tin trẻ
            $child = getChildData($_POST['child_id'], CHILD_INFO);

            // Lấy danh sách phụ huynh của trẻ
            $parents = getChildData($_POST['child_id'], CHILD_PARENTS);

            $parentIds = array();
            if(count($parents) > 0) {
                foreach ($parents as $parent) {
                    $parentIds[] = $parent['user_id'];
                }

                // Thông báo cho phụ huynh có thông tin sức khỏe mới
                if(isset($updateIdGR) && !isset($insertIdGR)) {
                    $userDao->postNotifications($parentIds, NOTIFICATION_UPDATE_CHILD_HEALTH, NOTIFICATION_NODE_TYPE_CHILD,
                        $child['birthday'], $_POST['recorded_at'], $child_parent_id, convertText4Web($child['child_name']));
                } else if(!isset($updateIdGR) && isset($insertIdGR)) {
                    $userDao->postNotifications($parentIds, NOTIFICATION_NEW_CHILD_HEALTH, NOTIFICATION_NODE_TYPE_CHILD,
                        $child['birthday'], $_POST['recorded_at'], $child_parent_id, convertText4Web($child['child_name']));
                }
            }
            $db->commit();
            return_json(array(
                'code' => 200,
                'message' =>  __("Done, Child info has been created"),
                'data' => array()
            ));
            break;
        case 'search':
            if(!isset($_POST['child_id']) || !is_numeric(($_POST['child_id']))) {
                _api_error(404);
            }
            // Lấy child_parent_id
            $child_parent_id = $childDao->getChildParentIdFromChildId($_POST['child_id']);
            
            $results = $childDao->getChildGrowthLimit($child_parent_id);

            return_json(array(
                'code' => 200,
                'message' => __("OK"),
                'data' =>array(
                    'growths' => $results
                )
            ));
            break;
        case 'delete_growth':
            if(!isset($_POST['child_id']) || !is_numeric($_POST['child_id'])) {
                _api_error(404);
            }
            $child_parent_id = $childDao->getChildParentIdFromChildId($_POST['child_id']);
            if(!isset($_POST['child_growth_id']) || !is_numeric($_POST['child_growth_id'])) {
                _api_error(404);
            }

            $db->begin_transaction();

            $childDao->deleteChildGrowth($child_parent_id, $_POST['child_growth_id']);

            $db->commit();
            return_json(array(
                'code' => 200,
                'message' =>  __("OK"),
                'data' => null
            ));
            break;
        case 'edit_growth':
            // Thay đổi thông tin chiều cao, cân nặng của trẻ
            if(!isset($_POST['child_growth_id']) || !is_numeric($_POST['child_growth_id'])) {
                _api_error(404);
            }
            $child_parent_id = $childDao->getChildParentIdFromChildId($_POST['child_id']);
            $db->begin_transaction();
            $child_growth = $childDao->getChildGrowthById($_POST['child_growth_id']);
            $args = array();
            $args['recorded_at'] = $_POST['recorded_at'];
            $args['height'] = $_POST['height'];
            $args['weight'] = $_POST['weight'];
            $args['nutriture_status'] = $_POST['nutriture_status'];
            $args['heart'] = $_POST['heart'];
            $args['blood_pressure'] = $_POST['blood_pressure'];
            $args['ear'] = $_POST['ear'];
            $args['eye'] = $_POST['eye'];
            $args['description'] = $_POST['description'];
            $args['child_growth_id'] = $_POST['child_growth_id'];

            // Lấy thông tin chiều cao cân nặng của trẻ
            $growth = $childDao->getChildGrowth($child_parent_id);
            $growth_recorded = array();
            foreach ($growth as $row) {
                $growth_recorded[] = $row['recorded_at'];
            }
            if(in_array($args['recorded_at'], $growth_recorded) && ($args['recorded_at'] != $child_growth['recorded_at'])) {
                throw new Exception(__("Selected date existed"));
            }

            $args['source_file'] = $child_growth['source_file_path'];
            $args['file_name'] = convertText4Web($child_growth['file_name']);

            $file_name = "";
            // Upload file đính kèm
            if (file_exists($_FILES['file']['tmp_name'])) {
                // check file upload enabled
                if (!$system['file_enabled']) {
                    _api_error(0, __("This feature has been disabled"));
                }

                // valid inputs
                if (!isset($_FILES["file"]) || $_FILES["file"]["error"] != UPLOAD_ERR_OK) {
                    _api_error(0, __("Something wrong with upload! Is 'upload_max_filesize' set correctly?"));
                }

                // check file size
                $max_allowed_size = $system['max_file_size'] * 1024;
                if ($_FILES["file"]["size"] > $max_allowed_size) {
                    _api_error(0, __("The file size is so big"));
                }

                // check file extesnion
                $extension = get_extension($_FILES['file']['name']);
                if (!valid_extension($extension, $system['file_medicine_extensions'])) {
                    //modal(MESSAGE, __("Upload Error"), __("The file type is not valid or not supported"));
                    _api_error(0, __("The file type is not valid or not supported") );
                }

                /* check & create uploads dir */
                $depth = '';
                $folder = 'growths';
                if (!file_exists($depth . $system['system_uploads_directory'] . '/' . $folder)) {
                    @mkdir($depth . $system['system_uploads_directory'] . '/' . $folder, 0777, true);
                }
                if (!file_exists($depth . $system['system_uploads_directory'] . '/' . $folder . '/' . date('Y'))) {
                    @mkdir($depth . $system['system_uploads_directory'] . '/' . $folder . '/' . date('Y'), 0777, true);
                }
                if (!file_exists($system['system_uploads_directory'] . '/' . $folder . '/' . date('Y') . '/' . date('m'))) {
                    @mkdir($depth . $system['system_uploads_directory'] . '/' . $folder . '/' . date('Y') . '/' . date('m'), 0777, true);
                }

                /* prepare new file name */
                $directory = $folder . '/' . date('Y') . '/' . date('m') . '/';
                $prefix = $system['uploads_prefix'] . '_' . md5(time() * rand(1, 9999));

                $image = new Image($_FILES["file"]["tmp_name"]);

                $file_name = $directory . $prefix . $image->_img_ext;
                $file_name_tmp = $directory . $prefix . '_tmp' . $image->_img_ext;

                $path = $depth . $system['system_uploads_directory'] . '/' . $file_name;
                $path_tmp = $depth . $system['system_uploads_directory'] . '/' . $file_name_tmp;

                /* check if the file uploaded successfully */
                if (!@move_uploaded_file($_FILES['file']['tmp_name'], $path_tmp)) {
                    _api_error(0, __("Sorry, can not upload the file"));
                }

                /* save the new image */
                $image->save($path, $path_tmp);

                /* delete the tmp image */
                unlink($path_tmp);
                $args['source_file'] = $file_name;
                $args['file_name'] = $_FILES['file']['name'];

            }

            $childDao->updateChildGrowth($args);

            // Lấy thông tin chi tiết trẻ
            $child = getChildData($_POST['child_id'], CHILD_INFO);

            // Lấy danh sách phụ huynh của trẻ
            $parents = getChildData($_POST['child_id'], CHILD_PARENTS);
            $parentIds = array();
            if(count($parents) > 0) {
                foreach ($parents as $parent) {
                    $parentIds[] = $parent['user_id'];
                }

                // Thông báo cho phụ huynh có thông tin sức khỏe mới
                $userDao->postNotifications($parentIds, NOTIFICATION_UPDATE_CHILD_HEALTH, NOTIFICATION_NODE_TYPE_CHILD,
                    $child['birthday'], $_POST['recorded_at'], $child_parent_id, convertText4Web($child['child_name']));
            }


            $db->commit();
            return_json(array(
                'code' => 200,
                'message' =>  __("Done, Child info have been updated"),
                'data' => array()
            ));
            break;
        case 'searchchildexist':
            $result = array();
            $childInfo = $childDao->getStatusChildInSchool($_POST['child_code'], $class['school_id']);
            if ($childInfo['status'] == 1) {
                $child = $childDao->getChildByCode($_POST['child_code']);
                $child['parent'] = $childDao->getParentManageChild($child['child_parent_id']);
                return_json(array(
                    'code' => 200,
                    'message' =>  __("OK"),
                    'data' => array(
                        'child' => $child
                    )
                ));
            } elseif ($childInfo['status'] == 2) {
                _api_error(0, __("Trẻ đã được thêm vào trường"));
            } elseif ($childInfo['status'] == 3) {
                _api_error(0, __("Trẻ đang được học ở trường khác"));
            } else {
                _api_error(0, __("Không tìm thấy thông tin trẻ"));
            }
            break;
        case 'addexisting':
            /**
             * Hàm này xử lý khi trường tạo ra thông tin một trẻ. Công việc bao gồm
             * 1. Tạo thông tin trẻ trong bảng ci_child
             * 2. Cập nhật danh sách lớp (đưa trẻ vào lớp - bảng ci_class_child)
             * 2.2 nhập thông tin của trẻ vào bảng điểm ci_point_
             * 2.3 Nhập thông tin của trẻ vào bảng hạnh kiểm ci_conduct
             * 3. Cập nhật thông tin trẻ trong trường (cập nhật bảng ci_school_child)
             * 3.5. Tự động tạo tài khoản phụ huynh
             * 4. Thêm cha mẹ vào nhóm của lớp (bao gồm việc tăng
             * 5. Thêm cha mẹ cho trẻ
             * 6. Cho cha mẹ like trang của trường
             * 7. Cho cha mẹ thành bạn của giáo viên (bao gồm cả quản lý trường)
             * 8. Tăng số lượng trẻ của trường
             */
            //Validate điều kiện tạo tài khoản phụ huynh
            $create_parent_account = $_POST['create_parent_account'];
            if ($create_parent_account && !is_empty($args['parent_email']) && !valid_email($args['parent_email'])) {
                throw new Exception(__("To create parent account, you must enter correct parent email or parent email is empty."));
            }
            $class_id = $class['group_id'];
            $db->begin_transaction();

            $args['child_parent_id'] = $_POST['child_parent_id'];

            //1. Tạo thông tin trẻ trong bảng ci_child
            $lastId = $childDao->createChild($args);

            //2. Cập nhật danh sách lớp (đưa trẻ vào lớp - bảng ci_class_child)
            // Nếu ko chọn class thì thêm trẻ vào trường, bỏ qua bước này
            $argsCC['class_id'] = $class_id;
            $argsCC['begin_at'] = isset($_POST['begin_at'])? trim($_POST['begin_at']) : "";
            $argsCC['child_id'] = $lastId;
            if ($class_id > 0) {
                $childDao->addChildToClass($argsCC);
                // ADD START MANHDD 03/06/2021
                //2.2 nhập thông tin của trẻ vào bảng điểm ci_point_
                $subjectOfClass = $subjectDao->getSubjectsByClassId($class_id,$class['school_id']);
                if(count($subjectOfClass)>0) {
                    foreach($subjectOfClass as $subject) {
                        $childDao->addChildPoints($subject['subject_id'],$args['child_code'],$class_id,$subject['school_year']);
                    }
                }
                //2.3 Nhập thông tin của trẻ vào bảng hạnh kiểm ci_conduct
                $school_year = date("Y", strtotime($_POST['begin_at'])).'-'.(date("Y", strtotime($_POST['begin_at']))+1);
                $childDao->addChildConduct($class_id,$argsCC['child_id'],$school_year);
                // ADD END MANHDD 03/06/2021
            }

            //3. Cập nhật thông tin trẻ trong trường (cập nhật bảng ci_school_child)
            $school = getSchoolData($class['school_id'], SCHOOL_DATA);

            $argsCC['school_id'] = $school['page_id'];
            $argsCC['child_parent_id'] = $_POST['child_parent_id'];
            $childDao->addChildToSchool($argsCC);

            $parentIds = (isset($_REQUEST['user_id_parent'])? $_REQUEST['user_id_parent'] : array());
            $parentIds = array_unique($parentIds);
            //3.1. Tự động tạo tài khoản phụ huynh
            if ($create_parent_account) {
                $argsParent = array();
                $argsParent['full_name'] = $args['parent_name'];
                $argsParent['first_name'] = split_name($argsParent['full_name'])[0];
                $argsParent['last_name'] = split_name($argsParent['full_name'])[1];
                $argsParent['username'] = generateUsername($argsParent['full_name']);
                $argsParent['user_phone'] = $args['parent_phone'];
                /*if (!validatePhone($args['user_phone'])) {
                    return_json( array('error' => true, 'message' => __("The phone number you entered is incorrect")) );
                }*/
                $argsParent['email'] = ($args['parent_email'] != '') ? $args['parent_email'] : 'null';
                $argsParent['password'] = (is_empty($argsParent['user_phone'])? "coniu.vn": $argsParent['user_phone']);
                $argsParent['gender'] = FEMALE;

                $parentId = $userDao->createUser($argsParent);
                $parentIds[] = $parentId;

                if($argsParent['email'] != 'null') {
                    //Gửi mail cho phụ huynh của trẻ.
                    $argEmail = array();
                    $argEmail['action'] = "create_parent_account";
                    $argEmail['receivers'] = $argsParent['email'];
                    $argEmail['subject'] = "[".convertText4Web($school['page_title'])."]".__("Use Inet application to connect student's parents");

                    //Tạo nên nội dung email
                    $smarty->assign('full_name', $argsParent['full_name']);
                    $smarty->assign('child_name', $args['child_name']);
                    $smarty->assign('username', $argsParent['email']);
                    $smarty->assign('password', $argsParent['password']);
                    $argEmail['content'] = $smarty->fetch("ci/email_templates/create_parent_account.tpl");

                    $argEmail['delete_after_sending'] = 1;
                    $argEmail['school_id'] = $school['page_id'];
                    $argEmail['user_id'] = $user->_data['user_id'];

                    $mailDao->insertEmail($argEmail);
                }
            }
            if (count($parentIds) > 0){
                //3.2. Cho phụ huynh like các page của hệ thống, join các group của hệ thống
                $userDao->addUsersLikeSomePages($system_page_ids, $parentIds);
                $userDao->addUserToSomeGroups($system_group_ids, $parentIds);

                //4. Thêm cha mẹ vào nhóm của lớp (bao gồm việc tăng thành viên nhóm)
                // Nếu chưa chọn lớp thì bỏ qua
                if ($class_id > 0) {
                    $classDao->addUserToClass($class_id, $parentIds);
                }

                //5. Thêm cha mẹ cho trẻ
                $parentDao->addParentList($lastId, $parentIds);

                //6. Cho cha mẹ like trang của trường (tăng số like)
                $likeCnt = $schoolDao->addUsersLikeSchool($school['page_id'], $parentIds);
                $school['page_likes'] = $school['page_likes'] + $likeCnt;

                //7. Cho cha mẹ thành bạn của giáo viên (bao gồm cả quản lý trường)
                // Nếu chưa chọn lớp thì bỏ qua
                // Tạm thời không cho kết bạn giữa giáo viên, phụ huynh
                /*if ($class_id > 0) {
                    $teacherIds = $teacherDao->getTeacherIDOfClass($class_id);
                }
                $teacherIds[] = $user->_data['user_id']; //User hiện tại là quản lý của trường
                $userDao->becomeFriends($teacherIds, $parentIds);*/

                //7. Gắn trẻ được quản lý cho phụ huynh (ci_parent_manage)
                $parents = $childDao->getParentManageChild($_POST['child_parent_id']);
                $oldParentIds = array();
                foreach ($parents as $parent) {
                    $oldParentIds[] = $parent['user_id'];
                }
                $addedParentIds = array_diff($parentIds, $oldParentIds);

                $childDao->createParentManageInfo($addedParentIds, $_POST['child_parent_id'], $lastId, $school['page_id']);
                $childDao->updateParentManageInfo($oldParentIds, $_POST['child_parent_id'], $lastId, $school['page_id']);
            }
            //8. Tăng số lượng trẻ của trường
            $schoolDao->updateGenderCount($school['page_id'], $args['gender'], 1);

            if ($args['gender'] == MALE) {
                $school['male_count'] = $school['male_count'] + 1;
            } else {
                $school['female_count'] = $school['female_count'] + 1;
            }

            // Thông báo cho phụ huynh biết trẻ được thêm vào trường
            $userDao->postNotifications($parentIds, NOTIFICATION_NEW_CHILD_EXIST, NOTIFICATION_NODE_TYPE_CHILD,
                $lastId, convertText4Web($args['child_name']), $lastId, convertText4Web($school['page_title']));

            /* ---------- UPDATE - MEMCACHE ---------- */
            //1. Cập nhật thông tin của trường
            $info_update = array(
                'page_likes' => $school['page_likes']
            );
            $config_update = array(
                'male_count' => $school['male_count'],
                'female_count' => $school['female_count']
            );

            updateSchoolData($school['page_id'], SCHOOL_INFO, $info_update);
            updateSchoolData($school['page_id'], SCHOOL_CONFIG, $config_update);
            addSchoolData($school['page_id'], SCHOOL_CHILDREN, $lastId);

            //2. Cập nhật thông tin trẻ và lớp
            addClassData($class_id, CLASS_CHILDREN, $lastId);
            updateClassData($class_id, CLASS_INFO);

            //3. Cập nhật tin tin quản lý trẻ
            foreach ($parentIds as $parentId) {
                updateUserManageData($parentId, USER_MANAGE_CHILDREN);
            }
            /* ---------- END - MEMCACHE ---------- */

            $argc = array();
            $argc['edit_by'] = EDIT_BY_TEACHER;
            $argc['child_id'] = $lastId;
            $argc['school_id'] = $school['page_id'];

            // Thêm trẻ vào bảng ci_child_edit_history
            $childDao->insertChildEditHistory($argc);

            $db->commit();
            return_json(array(
                'code' => 200,
                'message' =>  __("Done, Child info has been created"),
                'data' => array()
            ));
            break;
        case 'add_photo':
            // Thêm album ảnh nhật ký của trẻ
            if (!isset($_POST['child_id']) || !is_numeric($_POST['child_id'])) {
                _api_error(404);
            }
            $db->begin_transaction();
            $child = getChildData($_POST['child_id'], CHILD_INFO);
            $child_parent_id = $child['child_parent_id'];
            $caption = isset($_POST['caption']) ? $_POST['caption'] : 'null';
            // get allowed file size
            $max_allowed_size = $system['max_photo_size'] * 1024;

            /* check & create uploads dir */
            $depth = '';
            $folder = 'children';
            if(!file_exists($depth.$system['system_uploads_directory'].'/'.$folder)) {
                @mkdir($depth.$system['system_uploads_directory'].'/'.$folder, 0777, true);
            }
            if(!file_exists($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y'))) {
                @mkdir($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y'), 0777, true);
            }
            if(!file_exists($system['system_uploads_directory'].'/'.$folder.'/' . date('Y') . '/' . date('m'))) {
                @mkdir($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y') . '/' . date('m'), 0777, true);
            }

            $files = array();
            foreach ($_FILES['file'] as $key => $val) {
                for ($i = 0; $i < count($val); $i++) {
                    $files[$i][$key] = $val[$i];
                }
            }

            $return_files = array();
            $files_num = count($files);

            foreach ($files as $file) {

                // valid inputs
                if (!isset($file) || $file["error"] != UPLOAD_ERR_OK) {
                    if ($files_num > 1) {
                        continue;
                    } else {
//                        modal(MESSAGE, __("Upload Error"), __("Something wrong with upload! Is 'upload_max_filesize' set correctly?"));
                        return_json( array('error' => true, 'message' => __("You must add at least one image")) );
                    }
                }

                // check file size
                if ($file["size"] > $max_allowed_size) {
                    if ($files_num > 1) {
                        continue;
                    } else {
                        //modal(MESSAGE, __("Upload Error"), __("The file size is so big"));
                        return_json( array('error' => true, 'message' => __("The file size is so big")) );
                    }
                }

                /* prepare new file name */
                $directory = $folder . '/' . date('Y') . '/' . date('m') . '/';
                $prefix = $system['uploads_prefix'] . '_' . md5(time() * rand(1, 9999));


                $image = new Image($file["tmp_name"]);

                $image_tmp_name = $directory . $prefix . '_tmp' . $image->_img_ext;
                $image_new_name = $directory . $prefix . $image->_img_ext;
                $path_tmp = $depth . $system['system_uploads_directory'] . '/' . $image_tmp_name;
                $path_new = $depth . $system['system_uploads_directory'] . '/' . $image_new_name;

                /* check if the file uploaded successfully */
                if (!@move_uploaded_file($file['tmp_name'], $path_tmp)) {
                    if ($files_num > 1) {
                        continue;
                    } else {
                        //modal(MESSAGE, __("Upload Error"), __("Sorry, can not upload the file"));
                        return_json( array('error' => true, 'message' => __("Sorry, can not upload the file")) );
                    }
                }

                /* save the new image */
                $image->save($path_new, $path_tmp);

                /* delete the tmp image */
                unlink($path_tmp);

                /* return */
                $return_files[] = $image_new_name;
            }
            //Thêm album nhật ký
            $isParent = 0;
            $journalAlbumId = $journalDao->insertJournalAlbum($child_parent_id, $caption, $isParent);
            // Thêm ảnh vào album
            $journalDao->insertJournal($journalAlbumId, $return_files);

            // Thông báo đến phụ huynh
            // Lấy danh sách phụ huynh của trẻ
            $parents = getChildData($_POST['child_id'], CHILD_PARENTS);
            $child = getChildData($_POST['child_id'], CHILD_INFO);
            $parentIds = array();
            if(count($parents) > 0) {
                foreach ($parents as $parent) {
                    $parentIds[] = $parent['user_id'];
                }

                // Thông báo cho phụ huynh có nhật ký mới
                $userDao->postNotifications($parentIds, NOTIFICATION_ADD_DIARY_SCHOOL, NOTIFICATION_NODE_TYPE_CHILD,
                    $journalAlbumId, convertText4Web($caption), $child_parent_id, convertText4Web($child['child_name']));
            }

            $db->commit();
            return_json(array(
                'code' => 200,
                'message' =>  __("OK"),
                'data' => array()
            ));
            break;
        case 'delete_photo':
            if (!isset($_POST['child_journal_id']) || !is_numeric($_POST['child_journal_id'])) {
                _error(404);
            }
//            if (!isset($_POST['child_id']) || !is_numeric($_POST['child_id'])) {
//                _error(404);
//            }
            $db->begin_transaction();

            $journal_id = $journalDao->getJournalAlbumIdFromJournalId($_POST['child_journal_id']);
            $journal = $journalDao->getJournalById($journal_id);

            $journalDao->deleteJournal($_POST['child_journal_id']);

            // Thông báo đến phụ huynh
            // Lấy danh sách phụ huynh của trẻ
            $parents = getChildData($_POST['child_id'], CHILD_PARENTS);
            $child = getChildData($_POST['child_id'], CHILD_INFO);
            $parentIds = array();
            if(count($parents) > 0) {
                foreach ($parents as $parent) {
                    $parentIds[] = $parent['user_id'];
                }

                // Thông báo cho phụ huynh xóa ảnh trong album nhật ký
                $userDao->postNotifications($parentIds, NOTIFICATION_DELETE_PHOTO_DIARY_SCHOOL, NOTIFICATION_NODE_TYPE_CHILD,
                    $journal_id, convertText4Web($journal['caption']), $child['child_parent_id'], '');
            }

            $db->commit();
            return_json(array(
                'code' => 200,
                'message' =>  __("OK"),
                'data' => array()
            ));
            break;
        case 'delete_album':
            if (!isset($_POST['child_journal_album_id']) || !is_numeric($_POST['child_journal_album_id'])) {
                _api_error(404);
            }
//            if (!isset($_POST['child_id']) || !is_numeric($_POST['child_id'])) {
//                _api_error(404);
//            }
            $db->begin_transaction();
            $journalDao->deleteJournalAlbum($_POST['child_journal_album_id']);
            $db->commit();
            return_json(array(
                'code' => 200,
                'message' =>  __("OK"),
                'data' => array()
            ));
            break;
        case 'edit_caption':
            if (!isset($_POST['child_journal_album_id']) || !is_numeric($_POST['child_journal_album_id'])) {
                _error(404);
            }
//            if (!isset($_POST['child_id']) || !is_numeric($_POST['child_id'])) {
//                _error(404);
//            }
            $db->begin_transaction();

            $journal = $journalDao->getJournalById($_POST['child_journal_album_id']);
            $oldCaption = $journal['caption'];

            $journalDao->editCaptionAlbum($_POST['child_journal_album_id'], $_POST['caption']);

            // Thông báo đến phụ huynh
            // Lấy danh sách phụ huynh của trẻ
            $parents = getChildData($_POST['child_id'], CHILD_PARENTS);
            $child = getChildData($_POST['child_id'], CHILD_INFO);
            $parentIds = array();
            if(count($parents) > 0) {
                foreach ($parents as $parent) {
                    $parentIds[] = $parent['user_id'];
                }

                // Thông báo cho phụ huynh thêm ảnh trong album nhật ký
                $userDao->postNotifications($parentIds, NOTIFICATION_EDIT_CAPTION_DIARY_SCHOOL, NOTIFICATION_NODE_TYPE_CHILD,
                    $_POST['child_journal_album_id'], convertText4Web($oldCaption), $child['child_parent_id'], '');
            }

            $db->commit();
            return_json(array(
                'code' => 200,
                'message' =>  __("OK"),
                'data' => array()
            ));
            break;
        case 'add_photo_journal':
            // valid inputs
            if(!isset($_POST['child_journal_album_id']) || !is_numeric($_POST['child_journal_album_id'])) {
                _api_error(400);
            }
            $db->begin_transaction();

            // get allowed file size
            $max_allowed_size = $system['max_photo_size'] * 1024;

            /* check & create uploads dir */
            $depth = '';
            $folder = 'children';
            if(!file_exists($depth.$system['system_uploads_directory'].'/'.$folder)) {
                @mkdir($depth.$system['system_uploads_directory'].'/'.$folder, 0777, true);
            }
            if(!file_exists($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y'))) {
                @mkdir($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y'), 0777, true);
            }
            if(!file_exists($system['system_uploads_directory'].'/'.$folder.'/' . date('Y') . '/' . date('m'))) {
                @mkdir($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y') . '/' . date('m'), 0777, true);
            }

            $files = array();
            foreach ($_FILES['file'] as $key => $val) {
                for ($i = 0; $i < count($val); $i++) {
                    $files[$i][$key] = $val[$i];
                }
            }

            $return_files = array();
            $files_num = count($files);

            foreach ($files as $file) {

                // valid inputs
                if (!isset($file) || $file["error"] != UPLOAD_ERR_OK) {
                    if ($files_num > 1) {
                        continue;
                    } else {
//                        modal(MESSAGE, __("Upload Error"), __("Something wrong with upload! Is 'upload_max_filesize' set correctly?"));
                        return_json( array('error' => true, 'message' => __("You must add at least one image")) );
                    }
                }

                // check file size
                if ($file["size"] > $max_allowed_size) {
                    if ($files_num > 1) {
                        continue;
                    } else {
                        //modal(MESSAGE, __("Upload Error"), __("The file size is so big"));
                        return_json( array('error' => true, 'message' => __("The file size is so big")) );
                    }
                }

                /* prepare new file name */
                $directory = $folder . '/' . date('Y') . '/' . date('m') . '/';
                $prefix = $system['uploads_prefix'] . '_' . md5(time() * rand(1, 9999));


                $image = new Image($file["tmp_name"]);

                $image_tmp_name = $directory . $prefix . '_tmp' . $image->_img_ext;
                $image_new_name = $directory . $prefix . $image->_img_ext;
                $path_tmp = $depth . $system['system_uploads_directory'] . '/' . $image_tmp_name;
                $path_new = $depth . $system['system_uploads_directory'] . '/' . $image_new_name;

                /* check if the file uploaded successfully */
                if (!@move_uploaded_file($file['tmp_name'], $path_tmp)) {
                    if ($files_num > 1) {
                        continue;
                    } else {
                        //modal(MESSAGE, __("Upload Error"), __("Sorry, can not upload the file"));
                        return_json( array('error' => true, 'message' => __("Sorry, can not upload the file")) );
                    }
                }

                /* save the new image */
                $image->save($path_new, $path_tmp);

                /* delete the tmp image */
                unlink($path_tmp);

                /* return */
                $return_files[] = $image_new_name;
            }

            // Thêm ảnh vào album
            $journalDao->addPhotoForAlbum($_POST['child_journal_album_id'], $return_files);

            $journal = $journalDao->getJournalById($_POST['child_journal_album_id']);
            // Thông báo đến phụ huynh
            // Lấy danh sách phụ huynh của trẻ
            $parents = getChildData($_POST['child_id'], CHILD_PARENTS);
            $child = getChildData($_POST['child_id'], CHILD_INFO);
            $parentIds = array();
            if(count($parents) > 0) {
                foreach ($parents as $parent) {
                    $parentIds[] = $parent['user_id'];
                }

                // Thông báo cho phụ huynh thêm ảnh trong album nhật ký
                $userDao->postNotifications($parentIds, NOTIFICATION_ADD_PHOTO_DIARY_SCHOOL, NOTIFICATION_NODE_TYPE_CHILD,
                    $_POST['child_journal_album_id'], convertText4Web($journal['caption']), $child['child_parent_id'], '');
            }
//            $post_id = $user->add_album_photos($inputs);
            $db->commit();
            return_json(array(
                'code' => 200,
                'message' =>  __("OK"),
                'data' => array()
            ));
            break;
        case 'list_diary':
            /**
             * Lấy danh sách nhật ký của trẻ
             */
            if (!isset($_POST['child_id']) || !is_numeric($_POST['child_id'])) {
                _api_error(404);
            }
            $child = getChildData($_POST['child_id'], CHILD_INFO);
            $db->begin_transaction();

            $journals = $journalDao->getAllJournalChildOfSchoolForAPI($child['child_parent_id']);

            $db->commit();

            return_json(array(
                'code' => 200,
                'message' => __("OK"),
                'data' =>array('journals' => $journals)
            ));
            break;
        case 'search_diary':
            /**
             * Lấy danh sách nhật ký của trẻ theo năm
             */
            if (!isset($_POST['child_id']) || !is_numeric($_POST['child_id'])) {
                _api_error(404);
            }

            $child = getChildData($_POST['child_id'], CHILD_INFO);
            $db->begin_transaction();
            if(!isset($_POST['year']) || !is_numeric($_POST['year'])) {
                $journals = $journalDao->getAllJournalChildOfSchoolForAPI($child['child_parent_id']);
            } else {
                $journals = $journalDao->getAllJournalChildOfYearOfSchoolForAPI($child['child_parent_id'], $_POST['year']);
            }

            $db->commit();
            return_json(array(
                'code' => 200,
                'message' => __("OK"),
                'data' =>array(
                    'journals' => $journals
                )
            ));
            break;
        case 'album_detail':
            if (!isset($_POST['child_journal_album_id']) || !is_numeric($_POST['child_journal_album_id'])) {
                _api_error(404);
            }

            $db->begin_transaction();
            $journal = $journalDao->getJournalById($_POST['child_journal_album_id']);
            $db->commit();

            return_json(array(
                'code' => 200,
                'message' => __("OK"),
                'data' =>array('journal' => $journal)
            ));
            break;
        default:
            _api_error(400);
            break;
    }
	
} catch (Exception $e) {
    $db->rollback();
    // return
    header('HTTP/1.1 400 API Error');
    return_json(array(
        'code' => 0,
        'message' => $e->getMessage(),
        'data' => array()
    ));
} finally {
    $db->autocommit(true);
}

?>