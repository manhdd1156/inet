<?php
/**
 * Package: ajax/ci/bo
 * 
 * @package ConIu
 * @author QuanND
 */

include_once(DAO_PATH . 'dao_school.php');
include_once(DAO_PATH . 'dao_attendance.php');
include_once(DAO_PATH . 'dao_parent.php');
include_once(DAO_PATH . 'dao_teacher.php');
include_once(DAO_PATH . 'dao_user.php');
include_once(DAO_PATH . 'dao_class.php');
include_once(DAO_PATH . 'dao_child.php');
$schoolDao = new SchoolDAO();
$attendanceDao = new AttendanceDAO();
$parentDao = new ParentDAO();
$teacherDao = new TeacherDAO();
$userDao = new UserDAO();
$classDao = new ClassDAO();
$childDao = new ChildDAO();

//Lấy ra thông tin lớp
//$class = $classDao->getClassByUsername($_POST['group_name'], $user->_data['user_id']);
$class = getClassDataByUsername($_POST['group_name'], CLASS_INFO);
if (is_null($class)) {
    _api_error(403);
}
$school = getSchoolData($class['school_id'], SCHOOL_DATA);

try {
    $return = array();
    $db->autocommit(false);

    // Tăng số lượt tương tác - TaiLa
    addInteractive($class['school_id'], 'tuition', 'school_view');
    switch ($_POST['do']) {
        case 'rollup':
            //Xử lý nghiệp vụ khi cô giáo điểm danh lớp
            $args = array();
            $args['attendance_date'] = $_POST['attendance_date'];
            $attendanceDate = toDBDate($args['attendance_date']);
            $dateNow = toDBDate(toSysDate($date));
            if (($school['allow_teacher_rolls_days_before'] == 0) && (strtotime($attendanceDate) < strtotime($dateNow))) {
                throw new Exception(__("You do not have permission rollup dates in the past, please contact the school administration"));
            }

            $args['class_id'] = $class['group_id'];

            //Kiểm tra xem lớp đã có bản ghi điểm danh chưa
            $attendanceId = $attendanceDao->getAttendanceId($args['class_id'], $args['attendance_date']);

            $allChildIds = $_REQUEST['allChildIds'];
            $allStatus = $_REQUEST['allStatus'];
            $allReasons = $_REQUEST['allReasons'];
            $absenceCount = 0;
            $presentCount = 0;
            foreach ($allStatus as $status) {
                if (($status == ATTENDANCE_ABSENCE) || ($status == ATTENDANCE_ABSENCE_NO_REASON)) {
                    $absenceCount++;
                } else {
                    //ATTENDANCE_PRESENT, ATTENDANCE_COME_LATE, ATTENDANCE_EARLY_LEAVE
                    $presentCount++;
                }
            }

            $args['absence_count'] = $absenceCount;
            $args['present_count'] = $presentCount;
            $args['is_checked'] = 1;

            $db->begin_transaction();
            if ($attendanceId > 0) {
                // Lấy trạng thái điểm danh của cả lớp theo attendance_id
                $attendance = $attendanceDao->getAttendance($attendanceId, $school['page_id'], $class['group_id']);

                $attendanceChildIds = array();
                // Lặp danh sách điểm danh chi tiết, đưa vào mảng để tiện so sánh
                $childrenAtt = array();
                foreach ($attendance['detail'] as $row) {
                    if(isset($row['attendance_detail_id']) && $row['attendance_detail_id'] > 0) {
                        $childrenAtt[$row['child_id']]['status'] = $row['status'];
                        $childrenAtt[$row['child_id']]['reason'] = $row['reason'];
                        $childrenAtt[$row['child_id']]['is_parent'] = $row['is_parent'];
                        $childrenAtt[$row['child_id']]['feedback'] = $row['feedback'];
                        $childrenAtt[$row['child_id']]['attendance_detail_id'] = $row['attendance_detail_id'];
                    } else {
                        $childrenAtt[$row['child_id']]['status'] = '';
                        $childrenAtt[$row['child_id']]['reason'] = '';
                        $childrenAtt[$row['child_id']]['is_parent'] = '';
                        $childrenAtt[$row['child_id']]['feedback'] = '';
                        $childrenAtt[$row['child_id']]['attendance_detail_id'] = '';
                    }

                    // Lấy danh sách id những trẻ đã điểm danh rồi
                    $attendanceChildIds[] = $row['child_id'];
                }

                // Lấy danh sách những trẻ có trong lớp nhưng chưa được điểm danh trước đó
                $childIdsNew = array_diff($allChildIds, $attendanceChildIds);
                foreach ($childIdsNew as $childId) {
                    $childrenAtt[$childId]['status'] = '';
                    $childrenAtt[$childId]['reason'] = '';
                    $childrenAtt[$childId]['is_parent'] = '';
                    $childrenAtt[$childId]['feedback'] = '';
                    $childrenAtt[$childId]['attendance_detail_id'] = '';
                }

                //print_r($childrenAtt); die;
                // Lặp danh sách id trẻ và trạng thái điểm danh
                foreach ($childrenAtt as $childId => $att) {
                    // kiểm tra nếu trạng thái khác thì thông báo update, nếu không có trạng thái điểm danh trước đó thì thông báo điểm danh mới
                    if(in_array($childId, $allChildIds) && $att['status'] != '') {
                        // trẻ đã điểm danh trước đó - update
                        // Lặp danh sách id gửi lên

                        $parents = getChildData($childId, CHILD_PARENTS);
                        foreach ($allChildIds as $k => $id) {
                            // Nếu phụ huynh xin nghỉ mà chưa xác nhận
                            if($id == $childId && $att['is_parent'] == 1 && $att['feedback'] == 0) {
                                // Cập nhật trạng thái đã xác nhận và gửi thông báo về cho phụ huynh
                                $child = getChildData($childId, CHILD_INFO);

                                $attendanceDao->updateAttendanceDetailStatus($att['attendance_detail_id']);
                                if (!is_null($child) && !is_null($parents)) {
                                    foreach ($parents as $parent) {
                                        $userDao->postNotifications($parent['user_id'], NOTIFICATION_ATTENDANCE_CONFIRM, NOTIFICATION_NODE_TYPE_CHILD, $att['attendance_detail_id'], '', $childId);
//                                        $userDao->postNotifications($parent['user_id'], NOTIFICATION_UPDATE_ATTENDANCE, NOTIFICATION_NODE_TYPE_CHILD,
//                                            $allStatus[$k], $args['attendance_date'], $childId, convertText4Web($child['child_name']));
                                    }
                                }
                            }

                            // Nếu thay đổi trạng thái điểm danh thì gửi thông báo update điểm danh
                            if($id == $childId && ($allStatus[$k] != $att['status'] || $allReasons[$k] != $att['reason'])) {
                                // Cập nhật chi tiết điểm danh
                                $attendanceDao->updateChildAttendanceDetail($attendanceId, $childId, $allStatus[$k], $allReasons[$k]);

                                // Thông báo cập nhật điểm danh
                                //$child = $childDao->getChild($_POST['child_id']);
                                $child = getChildData($childId, CHILD_INFO);

                                if (!is_null($child) && !is_null($parents)) {
                                    foreach ($parents as $parent) {
                                        $userDao->postNotifications($parent['user_id'], NOTIFICATION_UPDATE_ATTENDANCE, NOTIFICATION_NODE_TYPE_CHILD,
                                            $allStatus[$k], $args['attendance_date'], $childId, convertText4Web($child['child_name']));
                                    }
                                }
                            }
                        }
                    } else {
                        if(in_array($childId, $allChildIds)) {
                            // Lấy trạng thái điểm danh của trẻ
                            foreach ($allChildIds as $k => $allChildId) {
                                if($allChildId == $childId) {
                                    $status = $allStatus[$k];
                                    $reason = $allReasons[$k];
                                }
                            }

                            // insert thông tin điểm danh của trẻ vào bảng ci_attendance_detail
                            $attendanceDao->saveAttendanceDetailChild($attendanceId, $childId, $status, $reason);

                            // Thông báo đến phụ huynh trẻ được điểm danh mới
                            //$child = $childDao->getChild($_POST['child_id']);
                            $child = getChildData($childId, CHILD_INFO);
                            //$parents = array();
                            //$parents = $parentDao->getParent($_POST['child_id']);
                            $parents = getChildData($childId, CHILD_PARENTS);

                            if (!is_null($child) && !is_null($parents)) {
                                $parentIds = array_keys($parents);
                                $userDao->postNotifications($parentIds, NOTIFICATION_NEW_ATTENDANCE, NOTIFICATION_NODE_TYPE_CHILD,
                                    $status, $args['attendance_date'], $childId, convertText4Web($child['child_name']));
                            }
                        }
                    }
                }

                $attendanceDao->updateAttendance($args);
            } else {
                $attendanceId = $attendanceDao->insertAttendance($args);

                foreach ($allChildIds as $k => $childId) {
                    // Lặp danh sách trẻ, gửi thông báo về cho phụ huynh
                    // Thông báo điểm danh
                    //$child = $childDao->getChild($_POST['child_id']);
                    $child = getChildData($childId, CHILD_INFO);
                    //$parents = array();
                    //$parents = $parentDao->getParent($_POST['child_id']);
                    $parents = getChildData($childId, CHILD_PARENTS);

                    if (!is_null($child) && !is_null($parents)) {
                        $parentIds = array_keys($parents);
                        $userDao->postNotifications($parentIds, NOTIFICATION_NEW_ATTENDANCE, NOTIFICATION_NODE_TYPE_CHILD,
                            $allStatus[$k], $args['attendance_date'], $childId, convertText4Web($child['child_name']));
                    }
                }

                // Insert điểm danh của cả lớp
                //$allResignTime = $_POST['allResignTime'];
                //$allIsParent = $_POST['allIsParent'];
                //$allRecordUserId = $_POST['allRecordUserId'];
                //$allStartDate = $_POST['allStartDate'];
                //$allEndDate = $_POST['allEndDate'];
//                foreach ($allChildIds as $child_id) {
//                    $allFeedback[] = $_POST['feedback_' . $child_id];
//                }
                $attendanceDao->saveAttendanceDetail($attendanceId, $allChildIds, $allStatus, $allReasons);

                // TaiLA  - Tạo hàm lưu thông tin điểm danh chi tiết mới
                //$attendanceDao->saveAttendanceDetailNew($attendanceId, $allChildIds, $allStatus, $allReasons, $allResignTime, $allIsParent, $allRecordUserId, $allStartDate, $allEndDate, $allFeedback);
            }
            $db->commit();

            // return
            return_json(array(
                'code' => 200,
                'message' => __("Done, Attendance info have been updated."),
                'data' => array()
            ));
            break;
        case 'rollup_back': //Xử lý nghiệp vụ khi lưu ĐIỂM DANH về NGÀY CỦA MỘT LỚP
            $args = array();
            $args['attendance_date'] = $_POST['attendance_date'];
            $attendanceDate = toDBDate($args['attendance_date']);
            $dateNow = toDBDate(toSysDate($date));

            $args['class_id'] = $class['group_id'];

            //Kiểm tra xem lớp đã có bản ghi điểm danh về chưa
            $attendanceBackId = $attendanceDao->getAttendanceBackId($args['class_id'], $args['attendance_date']);

            $allChildIds = $_REQUEST['allChildBackIds'];
//            $allStatus = array();
//            foreach ($allChildIds as $child_id) {
//                if(isset($_POST['status_' . $child_id])) {
//                    $allStatus[] = 1;
//                } else {
//                    $allStatus[] = 0;
//                }
//
//            }

            $allStatus = $_REQUEST['allStatusBack'];
            $allCameBackTimes = $_REQUEST['cameback_time'];
            $allCameBackNotes = $_REQUEST['cameback_note'];
            // echo "<pre>"; print_r($allStatus); die;
            $cameBackCount = 0;
            $notCameBackCount = 0;
            foreach ($allStatus as $status) {
                if ($status == 1) {
                    $cameBackCount++;
                } else {
                    //ATTENDANCE_PRESENT, ATTENDANCE_COME_LATE, ATTENDANCE_EARLY_LEAVE
                    $notCameBackCount++;
                }
            }

            $args['cameback_count'] = $cameBackCount;
            $args['not_cameback_count'] = $notCameBackCount;

            $db->begin_transaction();
            if ($attendanceBackId > 0) {
                // Lấy trạng thái điểm danh về của cả lớp theo attendance_back_id
                $attendance = $attendanceDao->getAttendanceBack($attendanceBackId, $class['school_id']);

                // Lặp danh sách điểm danh chi tiết, đưa vào mảng để tiện so sánh
                $childrenAtt = array();
                $childCameBack = array();
                foreach ($attendance['detail'] as $row) {
                    if(isset($row['attendance_back_detail_id']) && $row['attendance_back_detail_id'] > 0) {
                        $childrenAtt[$row['child_id']] = $row['status'];
                    } else {
                        $childrenAtt[$row['child_id']] = '';
                    }
                    $childCameBack[] = $row['child_id'];
                }

                foreach ($allChildIds as $k => $childId) {
                    if(in_array($childId, $childCameBack)) {
                        if($allStatus[$k] == 0) {
                            // Gửi thông báo update trẻ chưa về
                        } else {
                            // Không gửi thông báo gì
                        }
                    } else {
                        $status = $allStatus[$k];
                        // Thông báo đến phụ huynh trẻ được điểm danh về
                        //$child = $childDao->getChild($_POST['child_id']);
                        $child = getChildData($childId, CHILD_INFO);
                        //$parents = array();
                        //$parents = $parentDao->getParent($_POST['child_id']);
                        $parents = getChildData($childId, CHILD_PARENTS);

                        if (!is_null($child) && !is_null($parents)) {
                            $parentIds = array();
                            foreach ($parents as $parent) {
                                $parentIds[] = $parent['user_id'];
                            }
                            $userDao->postNotifications($parentIds, NOTIFICATION_NEW_ATTENDANCE_BACK, NOTIFICATION_NODE_TYPE_CHILD,
                                $status, $allCameBackTimes[$k], $childId, convertText4Web($child['child_name']));
                        }
                    }
                }

                $attendanceDao->updateAttendanceBack($args);
                $attendanceId = $attendanceBackId;
            } else {
                $attendanceId = $attendanceDao->insertAttendanceBack($args);
                foreach ($allChildIds as $k => $childId) {
                    if($allStatus[$k] == 1) {
                        // Lặp danh sách trẻ, gửi thông báo về cho phụ huynh
                        // Thông báo điểm danh
                        //$child = $childDao->getChild($_POST['child_id']);
                        $child = getChildData($childId, CHILD_INFO);
                        //$parents = array();
                        //$parents = $parentDao->getParent($_POST['child_id']);
                        $parents = getChildData($childId, CHILD_PARENTS);
                        if (!is_null($child) && !is_null($parents)) {
                            $parentIds = array();
                            foreach ($parents as $parent) {
                                $parentIds[] = $parent['user_id'];
                            }
                            $userDao->postNotifications($parentIds, NOTIFICATION_NEW_ATTENDANCE_BACK, NOTIFICATION_NODE_TYPE_CHILD,
                                $allStatus[$k], $allCameBackTimes[$k], $childId, convertText4Web($child['child_name']));
                        }
                    }
                }
            }
            $attendanceDao->saveAttendanceBackDetail($attendanceId, $allChildIds, $allStatus, $allCameBackTimes, $allCameBackNotes);

//            echo "<pre>";
//            print_r($allCameBackTimes); die;
            $db->commit();
            return_json(array(
                    'code' => 200,
                    'success' => true,
                    'message' => __("Class attendance have been updated")
                )
            );
            break;
        case 'list':
            $results = $attendanceDao->getAttendanceDetailApi($class['group_id'], $_POST['attendance_date']);
            $details = array();

            foreach ($results['detail'] as $k => $row) {
                // Lấy ngày trẻ bắt đầu đi học trong lớp
                $begin_at = $childDao->getChildBeginAtInClass($row['child_id'], $class['group_id']);
                // lấy ngày đầu tháng hiện tại
                $firstDayofMonthNow = date('Y-m-01');
                $attendanceDate = toDBDate($_POST['attendance_date']);
                if((strtotime($begin_at) - strtotime($firstDayofMonthNow)) <= 0 || (strtotime($firstDayofMonthNow) - strtotime($attendanceDate)) <= 0) {
                    //unset($results['detail'][$k]);
                    $details[] = $row;
                }
            }
            $results['detail'] = $details;

            //Lấy ra cấu hình nghỉ học vẫn tính phí
            $schoolConfig = getSchoolData($class['school_id'], SCHOOL_CONFIG);
            $attendanceConfig['attendance_use_leave_early'] = $schoolConfig['attendance_use_leave_early'] ? true : false;
            $attendanceConfig['attendance_use_come_late'] = $schoolConfig['attendance_use_come_late'] ? true : false;
            $attendanceConfig['attendance_absence_no_reason'] = $schoolConfig['attendance_absence_no_reason'] ? true : false;

            // Lấy thông tin điểm danh về của lớp
            $data_cameback = $attendanceDao->getAttendanceCameBackdetail($class['group_id'], $_POST['attendance_date']);
            for($i = 0; $i < count($data_cameback['detail']); $i++) {
                $data_cameback['detail'][$i]['status'] = $results['detail'][$i]['status'];
            }
            // return
            return_json(array(
                'code' => 200,
                'message' => __("OK"),
                'data' => array(
                    'attendance_list' => $results,
                    'attendance_back' => $data_cameback,
                    'attendanceConfig' => $attendanceConfig
                )
            ));

            break;

        case 'search':
            $results = $attendanceDao->getAttendanceOfClass($class['group_id'], $_POST['fromDate'], $_POST['toDate']);
            return_json(array(
                'code' => 200,
                'message' => __("OK"),
                'data' => array(
                    'attendance_search' => $results
                )
            ));
            break;

        case 'child':
            $data = array();
            $attendances = $attendanceDao->getAttendanceChild($_POST['child_id'], $_POST['fromDate'], $_POST['toDate']);
            return_json(array(
                'code' => 200,
                'message' => __("OK"),
                'data' => $attendances
            ));
            break;

        case 'confirm':
            $db->begin_transaction();
            //Cập nhật trại thái
            $attendanceDao->updateAttendanceDetailStatus($_POST['attendance_detail_id']);
            //Thông báo phụ huynh
            //$parentIds = $parentDao->getParentIds($_POST['child_id']);
            $parents = getChildData($_POST['child_id'], CHILD_PARENTS);
            if (!is_null($parents)) {
                $parentIds = array_keys($parents);
                $userDao->postNotifications($parentIds, NOTIFICATION_ATTENDANCE_CONFIRM, NOTIFICATION_NODE_TYPE_CHILD, $_POST['attendance_detail_id'], '', $_POST['child_id']);
            }

            $db->commit();

            return_json(array(
                'code' => 200,
                'message' => __("OK"),
                'data' => array()
            ));

            break;

        default:
            _error(400);
            break;
    }

} catch (Exception $e) {
    $db->rollback();
    header('HTTP/1.1 400 API Error');
    return_json(array(
        'code' => 0,
        'message' => $e->getMessage(),
        'data' => null
    ));
} finally {
    $db->autocommit(true);
}

?>