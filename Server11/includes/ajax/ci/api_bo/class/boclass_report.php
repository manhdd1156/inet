<?php
/**
 * Package: ajax/ci/bo/class
 *
 * @package ConIu
 * @author TaiLA
 */

// fetch image class
require(ABSPATH.'includes/class-image.php');

include_once(DAO_PATH.'dao_school.php');
include_once(DAO_PATH.'dao_class.php');
include_once(DAO_PATH.'dao_child.php');
include_once(DAO_PATH.'dao_parent.php');
include_once(DAO_PATH.'dao_user.php');
include_once(DAO_PATH.'dao_report.php');
include_once(DAO_PATH.'dao_mail.php');

$schoolDao = new SchoolDAO();
$classDao = new ClassDAO();
$childDao = new ChildDAO();
$parentDao = new ParentDAO();
$userDao = new UserDAO();
$reportDao = new ReportDAO();
$mailDao = new MailDAO();

//Lấy ra thông tin lớp
//$class = $classDao->getClassByUsername($_POST['group_name'], $user->_data['user_id']);
$class = getClassDataByUsername($_POST['group_name'], CLASS_INFO);
if (is_null($class)) {
    _api_error(403);
}

try {
    $return = array();
    $db->autocommit(false);
    // Tăng số lượt tương tác - TaiLa
    addInteractive($class['school_id'], 'pickup', 'school_view');
    switch ($_POST['do']) {
        case 'child_list':
            $children = $childDao->getChildrenOfClassForReport($class['group_id']);

            // return
            return_json(array(
                'code' => 200,
                'message' => 'OK',
                'data' => array(
                    'child_list' => $children
                )
            ));
            break;

        case 'list_report':
            $page = isset($_POST['page']) ? $_POST['page']:0;
            if (isset($_POST['child_id'])) {
                $result = $reportDao->getClassReportOfChild($class['group_id'], $_POST['child_id']);
            } else {
                $result = $reportDao->getClassReportForAPI($class['group_id'], $page);
            }
            // return
            return_json(array(
                'code' => 200,
                'message' => 'OK',
                'data' => array(
                    'list_report' => $result
                )
            ));
            break;

        case 'detail_report':

            if(!isset($_POST['report_id']) || !is_numeric($_POST['report_id'])) {
                _api_error(400);
            }
//            $result = $reportDao->getReportByIdForApi($_POST['report_id']);
            $data= $reportDao->getReportById($_POST['report_id']);
            // return
            return_json(array(
                'code' => 200,
                'message' => 'OK',
                'data' => array(
                    'report' => $data
                )
            ));
            break;

        case 'add_temp':
            $db->begin_transaction();
            $args = array();
            $args['template_name'] = trim($_POST['template_name']);
            $args['class_id'] = $class['group_id'];
            $args['school_id'] = $class['school_id'];
            $args['class_level_id'] = 0;
            $args['level'] = CLASS_LEVEL;

            // 1. Insert vào bảng ci_report_template
            $reportTemplateId = $reportDao->insertReportTemplate($args);

            $categoryIds = isset($_REQUEST['category_ids'])?$_REQUEST['category_ids']:array();

            $contents = isset($_REQUEST['contents'])?$_REQUEST['contents']:array();

            // 2. Insert vào bảng ci_report_template_detail
            $reportDao->insertReportTemplateDetail($reportTemplateId, $categoryIds, $contents);

            $db->commit();
            // return
            return_json(array(
                'code' => 200,
                'message' => __('Template has been created'),
                'data' => array()
            ));

            break;

        case 'list_temp':
            $results = $reportDao->getClassReportTemplate($class['group_id'], $class['class_level_id'], $class['school_id']);
            // return
            return_json(array(
                'code' => 200,
                'message' => 'OK',
                'data' => array(
                    'list_template' => $results
                )
            ));

            break;

        case 'detail_temp':
            $results = $reportDao->getReportTemplateDetailForApi($_POST['report_template_id']);
            if(!isset($_POST['report_template_id']) || !is_numeric($_POST['report_template_id'])) {
                _error(404);
            }
            $temp = $reportDao->getReportTemplate($_POST['report_template_id']);
            if(is_null($temp) || $temp['school_id'] != $class['school_id']) {
                _error(404);
            }
            $tempCategoryIds = array();
            foreach ($temp['details'] as $detail) {
                $tempCategoryIds[] = $detail['report_template_category_id'];
            }
            // Lấy danh sách các hạng mục của trường
            $categorys = $reportDao->getAllCategoryOfSchool($class['school_id']);
            $categorysTemp = array();
            foreach ($categorys as $category) {
                if(in_array($category['report_template_category_id'], $tempCategoryIds)){
                    $category['checked'] = 1;
                    foreach ($temp['details'] as $detail) {
                        if($detail['report_template_category_id'] == $category['report_template_category_id']) {
                            $category['template_content'] = $detail['template_content'];
                        }
                        $tempCategoryIds[] = $detail['report_template_category_id'];
                    }
                } else {
                    $category['checked'] = 0;
                }
                $categorysTemp[] = $category;
            }
            // return
            return_json(array(
                'code' => 200,
                'message' => 'OK',
                'data' => array(
                    'template' => $temp,
                    'categorysTemp' => $categorysTemp
                )
            ));

            break;
        case 'edit_temp':

            $db->begin_transaction();
            if(!isset($_POST['report_template_id'])) {
                _api_error(404);
            }
            $temp = $reportDao->getReportTemplate($_POST['report_template_id']);
            if($temp['level'] != CLASS_LEVEL) {
                _api_error(0, __("You not permission edit this template"));
            }
            $args = array();
            $args['report_template_id'] = $_POST['report_template_id'];
            $args['template_name'] = trim($_POST['template_name']);
            $args['level'] = CLASS_LEVEL;
            $args['class_id'] = $class['group_id'];
            $args['class_level_id'] = 0;
            $args['school_id'] = $class['school_id'];

            // 1. Update bảng ci_report_template
            $reportDao->updateReportTemplate($args);

            // 2. Update bảng ci_report_template_detail
            // Xóa toàn bộ chi tiết mẫu cũ
            $reportDao->deleteReportTemplateDetail($_POST['report_template_id']);

            // Lấy dữ liệu mới
            $categoryIds = isset($_REQUEST['category_ids'])?$_REQUEST['category_ids']:array();

            $contents = isset($_REQUEST['contents'])?$_REQUEST['contents']:array();

            // Insert vào bảng ci_report_template_detail
            $reportDao->insertReportTemplateDetail($_POST['report_template_id'], $categoryIds, $contents);

            $db->commit();

            // return
            return_json(array(
                'code' => 200,
                'message' => __('Contact book template has been updated'),
                'data' => array()
            ));

            break;
        case 'add_report':
            $db->begin_transaction();
            if(is_null($_REQUEST['child'])) {
                throw new Exception(__("You have not picked a student yet"));
            }
            $args = array();
            $args['report_name'] = trim($_POST['title']);
            $args['school_id'] = $class['school_id'];
            $args['child'] = $_REQUEST['child'];
            $args['is_notified'] = (isset($_POST['notify_immediately']) && $_POST['notify_immediately'] == 'on')? 1: 0;
            $args['status'] = 1;
            $args['date'] = 'null';
            if($args['is_notified'] == 1) {
                $args['date'] = $date;
            }

            $file_name = "";
            // Upload file đính kèm
            if(file_exists($_FILES['file']['tmp_name'])) {
                // check file upload enabled
                if(!$system['file_enabled']) {
                    _api_error(0, __("This feature has been disabled"));
                }

                // valid inputs
                if(!isset($_FILES["file"]) || $_FILES["file"]["error"] != UPLOAD_ERR_OK) {
                    _api_error(0, __("Something wrong with upload! Is 'upload_max_filesize' set correctly?"));
                }

                // check file size
                $max_allowed_size = $system['max_file_size'] * 1024;
                if($_FILES["file"]["size"] > $max_allowed_size) {
                    _api_error(0, __("The file size is so big"));
                }

                // check file extesnion
                $extension = get_extension($_FILES['file']['name']);
                if(!valid_extension($extension, $system['file_report_extensions'])) {
                    _api_error(0, __("The file type is not valid or not supported"));
                }

                /* check & create uploads dir */
                $depth = '';
                $folder = 'reports/'. $class['school_id'];
                if(!file_exists($depth.$system['system_uploads_directory'].'/'.$folder)) {
                    @mkdir($depth.$system['system_uploads_directory'].'/'.$folder, 0777, true);
                }
                if(!file_exists($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y'))) {
                    @mkdir($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y'), 0777, true);
                }
                if(!file_exists($system['system_uploads_directory'].'/'.$folder.'/' . date('Y') . '/' . date('m'))) {
                    @mkdir($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y') . '/' . date('m'), 0777, true);
                }

                /* prepare new file name */
                $directory = $folder.'/'. date('Y') . '/' . date('m') . '/';
                $prefix = $system['uploads_prefix'].'_'.md5(time()*rand(1, 9999));
                $file_name = $directory.$prefix.'.'.$extension;
                $path = $depth.$system['system_uploads_directory'].'/'.$file_name;

                /* check if the file uploaded successfully */
                if(!@move_uploaded_file($_FILES['file']['tmp_name'], $path)) {
                    _api_error(0, __("Sorry, can not upload the file"));
                }
            }

            $args['source_file'] = $file_name;
            $args['file_name'] = $_FILES['file']['name'];

            $categoryIds = isset($_REQUEST['category_ids'])?$_REQUEST['category_ids']:array();

            if(count($categoryIds) == 0 && is_empty($args['source_file'])) {
                throw new Exception(__("You must select report template"));
            }
            // 1. Insert vào bảng ci_report
            if(is_empty($args['source_file']) && (count($_REQUEST['category_ids']) == 0)){
                throw new Exception(__("You must have an attachment or contact book content"));
            }

            $reportIds = $reportDao->insertReportClass($args);
            foreach ($reportIds as $reportId) {
                // Tăng lượt thêm mới
                addInteractive($class['school_id'], 'report', 'school_view', $reportId, 1);
            }

            // 2. Inser vào bảng ci_report_category
            // Lấy id category;

            $suggestsPost = isset($_REQUEST['suggests'])?$_REQUEST['suggests']:array();
            $contentsPost = isset($_REQUEST['contents'])?$_REQUEST['contents']:array();

            $suggests = array();
            $contents = array();

            $categoryNames = array();
            $categoryMulContent = array();
            // Lặp mảng categoryIds lấy category_name và template_multi_content, suggests, contents
            foreach ($categoryIds as $id) {
                // Lấy chi tiết của category
                $categoryTemp = $reportDao->getReportTemplateCategoryDetailForAPI($id);
                $categoryNames[] = convertText4Web($categoryTemp['category_name']);
                $categoryMulContent[] = convertText4Web($categoryTemp['template_multi_content']);

                // Lấy dánh sách gợi ý và nội dung gửi lên
                $suggests[] = isset($suggestsPost[$id])?$suggestsPost[$id]:array();
                $contents[] = isset($contentsPost[$id])?$contentsPost[$id]:'';
            }

            // Lặp mảng $reportIds, insert vào bảng ci_report_category
            for($i = 0; $i < count($reportIds); $i++) {
                $reportDao->insertReportDetailForAPI($reportIds[$i], $categoryIds, $suggests, $contents, $categoryNames, $categoryMulContent);
            }

            //3. Gửi email và thông báo đến phụ huynh
            if ($args['is_notified']) {
                $argEmail = array();
                if (!is_empty($file_name)) {
                    $argEmail['file_attachment'] = $system['system_uploads_directory'].'/'.$file_name;
                    $argEmail['file_attachment_name'] = $args['file_name'];
                }

                $content = "";
                $isOther = false;
                for($i = 0; $i < count($cates); $i ++) {
                    $content = $content . "- <b>" . $cates[$i] . "-</b>: " . $contents[$i] . "<br/><br/>";
                }
                $smarty->assign('title', $args['report_name']);
                $smarty->assign('content', $content);
                $smarty->assign('class_name', convertText4Web($class['group_title']));

                $argEmail['action'] = "create_report";
                $argEmail['subject'] = $args['report_name'];
                $argEmail['school_id'] = $args['school_id'];
                $argEmail['user_id'] = $user->_data['user_id'];


                $receivers = "";
                foreach ($args['child'] as $child_id) {
                    $parent_email = array();
                    //$child = $childDao->getChild($child_id);
                    $child = getChildData($child_id, CHILD_INFO);
                    if (!is_null($child)) {
                        if (!is_empty($child['parent_email'])) {
                            $parent_email[] = $child['parent_email'];

                        }
                        //$parents = $childDao->getParent($child_id);
                        $parents = getChildData($child_id, CHILD_PARENTS);
                        if (!is_null($parents)) {
                            foreach ($parents as $parent) {
                                $reports = $reportDao->getChildReport($child_id);
                                foreach ($reports as $report) {
                                    if(in_array($report['report_id'], $reportIds)) {
                                        //thông báo về cho phụ huynh
                                        $userDao->postNotifications($parent['user_id'], NOTIFICATION_NEW_REPORT, NOTIFICATION_NODE_TYPE_CHILD,
                                            $report['report_id'], convertText4Web($_POST['title']), $child_id, convertText4Web($child['child_name']));
                                    }
                                }

                                // lấy danh sách email
                                if (!is_empty($parent['user_email'])) {
                                    $parent_email[] = $parent['user_email'];
                                }
                            }
                        }

                        $parent_email = array_unique($parent_email);
                        $emai_str = implode(",", $parent_email);
                        $receivers = $receivers . ',' . $emai_str;

                        $smarty->assign('child_name', convertText4Web($child['child_name']));
                        $argEmail['content'] = $smarty->fetch("ci/email_templates/create_report.tpl");
                        $argEmail['receivers'] = $emai_str;

                        $argEmail['delete_after_sending'] = 1;
                        // $mailDao->insertEmail($argEmail);
                    }
                }

                /* Coniu - Tương tác trường */
                setIsAddNew($class['school_id'], 'report_created', $reportIds);
                /* Coniu - END */
            }

            $db->commit();

            // return
            return_json(array(
                'code' => 200,
                'message' => __('Contact book has been created'),
                'data' => array()
            ));

            break;
        case 'edit_report':
            $data = $reportDao->getReportById($_POST['report_id']);
            if (is_null($data)) {
                _api_error(404);
            }
            $args = array();
            $args['report_id'] = trim($_POST['report_id']);
            $args['is_notified'] = (isset($_POST['notify_immediately']) && $_POST['notify_immediately'] == 'on')? 1: 0;
            $args['report_name'] = $_POST['title'];

            $args['date'] = 'null';
            if($args['is_notified'] == 1) {
                $args['date'] = $date;
            }

            $args['source_file'] = $data['source_file_path'];
            $args['file_name'] = convertText4Web($data['file_name']);

            if(!$_POST['is_file']) {
                $args['source_file'] = "";
                $args['file_name'] = "";
            }

            $file_name = "";
            // Upload file đính kèm
            if(file_exists($_FILES['file']['tmp_name'])) {
                // check file upload enabled
                if(!$system['file_enabled']) {
                    _api_error(0, __("This feature has been disabled"));
                }

                // valid inputs
                if(!isset($_FILES["file"]) || $_FILES["file"]["error"] != UPLOAD_ERR_OK) {
                    _api_error(0, __("Something wrong with upload! Is 'upload_max_filesize' set correctly?"));
                }

                // check file size
                $max_allowed_size = $system['max_file_size'] * 1024;
                if($_FILES["file"]["size"] > $max_allowed_size) {
                    _api_error(0, __("The file size is so big"));
                }

                // check file extesnion
                $extension = get_extension($_FILES['file']['name']);
                if(!valid_extension($extension, $system['file_report_extensions'])) {
                    _api_error(0, __("The file type is not valid or not supported"));
                }

                /* check & create uploads dir */
                $depth = '';
                $folder = 'reports/'. $class['school_id'];
                if(!file_exists($depth.$system['system_uploads_directory'].'/'.$folder)) {
                    @mkdir($depth.$system['system_uploads_directory'].'/'.$folder, 0777, true);
                }
                if(!file_exists($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y'))) {
                    @mkdir($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y'), 0777, true);
                }
                if(!file_exists($system['system_uploads_directory'].'/'.$folder.'/' . date('Y') . '/' . date('m'))) {
                    @mkdir($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y') . '/' . date('m'), 0777, true);
                }

                /* prepare new file name */
                $directory = $folder.'/'. date('Y') . '/' . date('m') . '/';
                $prefix = $system['uploads_prefix'].'_'.md5(time()*rand(1, 9999));
                $file_name = $directory.$prefix.'.'.$extension;
                $path = $depth.$system['system_uploads_directory'].'/'.$file_name;

                /* check if the file uploaded successfully */
                if(!@move_uploaded_file($_FILES['file']['tmp_name'], $path)) {
                    _api_error(0, __("Sorry, can not upload the file"));
                }
                $args['source_file'] = $file_name;
                $args['file_name'] = $_FILES['file']['name'];
            }

            // Nếu không có nội dung hoặc ảnh thì báo lỗi
            if(is_empty($args['source_file']) && !isset($_REQUEST['category_ids'])){
                _api_error(0, __("You must have an attachment or contact book content"));
            }

            // 1. Cập nhật bảng ci_report
            $reportDao->updateReport($args);

            // Nếu không chọn mẫu khác thì update category cũ
            if(($_POST['report_template_id'] == '') || !isset($_POST['report_template_id'])) {
                // 2. cập nhật bảng ci_report_category
                // Lấy id category
                $categoryIds = isset($_REQUEST['category_ids'])?$_REQUEST['category_ids']:array();

                $suggestsPost = isset($_REQUEST['suggests'])?$_REQUEST['suggests']:array();
                $contentsPost = isset($_REQUEST['contents'])?$_REQUEST['contents']:array();

                $suggests = array();
                $contents = array();

                foreach($categoryIds as $id) {
                    $suggests[$id] = isset($suggestsPost[$id]) ? $suggestsPost[$id] : array();
                    $contents[$id] = isset($contentsPost[$id]) ? $contentsPost[$id] : '';

                    $reportDao->updateReportCategoryForAPI($id, $suggests[$id], $contents[$id]);
                }
            } else {
                // Nếu chọn mẫu khác thì insert category mới
                //3. Xóa report category cũ
                $reportDao->deleteReportDetail($args['report_id']);

                //4. Thêm report category mới
                // Lấy id category
                $categoryIds = isset($_REQUEST['category_ids'])?$_REQUEST['category_ids']:array();

                $suggestsPost = isset($_REQUEST['suggests'])?$_REQUEST['suggests']:array();
                $contentsPost = isset($_REQUEST['contents'])?$_REQUEST['contents']:array();

                $suggests = array();
                $contents = array();

                $categoryNames = array();
                $categoryMulContent = array();

                // Lặp mảng categoryIds lấy category_name và template_multi_content
                foreach ($categoryIds as $id) {
                    // Lấy chi tiết của category
                    $categoryTemp = $reportDao->getReportTemplateCategoryDetailForAPI($id);
                    $categoryNames[] = convertText4Web($categoryTemp['category_name']);
                    $categoryMulContent[] = convertText4Web($categoryTemp['template_multi_content']);

                    // Lấy dánh sách gợi ý và nội dung gửi lên
                    $suggests[] = isset($suggestsPost[$id])?$suggestsPost[$id]:array();
                    $contents[] = isset($contentsPost[$id])?$contentsPost[$id]:'';
                }

                // insert vào bảng ci_report_category
                $reportDao->insertReportDetailForAPI($_POST['report_id'], $categoryIds, $suggests, $contents, $categoryNames, $categoryMulContent);
            }

            // Gửi email và thông báo đến phụ huynh
            if ($args['is_notified']) {
                $argEmail = array();
                if (!is_empty($file_name)) {
                    $argEmail['file_attachment'] = $system['system_uploads_directory'].'/'.$file_name;
                    $argEmail['file_attachment_name'] = $args['file_name'];
                } else {
                    $argEmail['file_attachment'] = $system['system_uploads_directory'].'/' . $args['source_file'];
                    $argEmail['file_attachment_name'] = $args['file_name'];
                }

                $content = "";
                $isOther = false;
                for($i = 0; $i < count($cates); $i ++) {
                    $content = $content . "- <b>" . $cates[$i] . "-</b>: " . $contents[$i] . "<br/><br/>";
                }
                $smarty->assign('title', $args['report_name']);
                $smarty->assign('content', $content);
                $smarty->assign('class_name', convertText4Web($class['group_title']));

                $argEmail['action'] = "create_report";
                $argEmail['subject'] = $args['report_name'];
                $argEmail['school_id'] = $args['school_id'];
                $argEmail['user_id'] = $user->_data['user_id'];


                $receivers = "";
                $parent_email = array();
                //$child = $childDao->getChild($data['child_id']);
                $child = getChildData($data['child_id'], CHILD_INFO);
                if (!is_null($child)) {
                    if (!is_empty($child['parent_email'])) {
                        $parent_email[] = $child['parent_email'];

                    }
                    //$parents = $childDao->getParent($child['child_id']);
                    $parents = getChildData($child['child_id'], CHILD_PARENTS);
                    if (!is_null($parents)) {
                        foreach ($parents as $parent) {
                            //thông báo về cho phụ huynh
                            $userDao->postNotifications($parent['user_id'], NOTIFICATION_UPDATE_REPORT, NOTIFICATION_NODE_TYPE_CHILD,
                                $_POST['report_id'], convertText4Web($_POST['title']), $child['child_id'], convertText4Web($child['child_name']));

                            // lấy danh sách email
                            if (!is_empty($parent['user_email'])) {
                                $parent_email[] = $parent['user_email'];
                            }
                        }
                    }

                    $parent_email = array_unique($parent_email);
                    $emai_str = implode(",", $parent_email);
                    $receivers = $receivers . ',' . $emai_str;

                    $smarty->assign('child_name', convertText4Web($child['child_name']));
                    $argEmail['content'] = $smarty->fetch("ci/email_templates/create_report.tpl");
                    $argEmail['receivers'] = $emai_str;

                    $argEmail['delete_after_sending'] = 1;
                    //$mailDao->insertEmail($argEmail);
                }
            }

            $db->commit();

            // return
            return_json(array(
                'code' => 200,
                'message' => __("Contact book has been updated"),
                'data' => array()
            ));

            break;
        case 'notify':
            $db->begin_transaction();
            //Lấy ra thông tin thông báo
            $data = $reportDao->getReportById($_POST['report_id']);

            if (is_null($data)) {
                _api_error(404);
            }
            $data['file_name'] = convertText4Web($data['file_name']);

    //            if ($data['created_user_id'] != $user->_data['user_id']) {
    //                _api_error(403);
    //            }

            if (!$data['is_notified']) {
                $argEmail = array();
                if (!is_empty($data['source_file'])) {
                    $argEmail['file_attachment'] = $system['system_uploads_directory'].'/'.$data['source_file_path'];
                    $argEmail['file_attachment_name'] = $data['file_name'];
                }

                $content = "";
                for($i = 0; $i < count($data['detail']); $i++) {
                    $content = $content . "- <b>" . $data['detail'][$i]['report_detaill_name'] . "-</b>: " . $data['detail'][$i]['report_detail_content'] . "<br/><br/>";
                }
                $smarty->assign('title', $data['report_name']);
                $smarty->assign('content', $content);
                $smarty->assign('class_name', convertText4Web($class['group_title']));

                $argEmail['action'] = "create_report";
                $argEmail['subject'] = $data['report_name'];
                $argEmail['school_id'] = $data['school_id'];
                $argEmail['user_id'] = $user->_data['user_id'];


                $receivers = "";
                $parent_email = array();
                //$child = $childDao->getChild($data['child_id']);
                $child = getChildData($data['child_id'], CHILD_INFO);
                if (!is_null($child)) {
                    if (!is_empty($child['parent_email'])) {
                        $parent_email[] = $child['parent_email'];

                    }
                    $parentIds = array();
                    //$parents = $childDao->getParent($data['child_id']);
                    $parents = getChildData($data['child_id'], CHILD_PARENTS);
                    if (!is_null($parents)) {
                        foreach ($parents as $parent) {
                            //thông báo về cho phụ huynh
                            $userDao->postNotifications($parent['user_id'], NOTIFICATION_NEW_REPORT, NOTIFICATION_NODE_TYPE_CHILD,
                                $data['report_id'], convertText4Web($data['report_name']), $child['child_id'], convertText4Web($child['child_name']));

                            // lấy danh sách email
                            if (!is_empty($parent['user_email'])) {
                                $parent_email[] = $parent['user_email'];
                            }
                        }
                    }

                    $parent_email = array_unique($parent_email);
                    $emai_str = implode(",", $parent_email);
                    $receivers = $receivers . ',' . $emai_str;

                    $smarty->assign('child_name', convertText4Web($child['child_name']));
                    $argEmail['content'] = $smarty->fetch("ci/email_templates/create_report.tpl");
                    $argEmail['receivers'] = $emai_str;

                    $argEmail['delete_after_sending'] = 1;
                    $mailDao->insertEmail($argEmail);
                }

                /* Coniu - Tương tác trường */
                setIsAddNew($class['school_id'], 'report_created', [$data['report_id']]);
                /* Coniu - END */
            }

            // Cập nhật trạng thái đã gửi thông báo.
            $reportDao->updateStatusToNotified($_POST['report_id']);

            $db->commit();
            // return
            return_json(array(
                'code' => 200,
                'message' => __('Notification has sent'),
                'data' => array()
            ));

            break;
        case 'notify_all_select':
            $db->begin_transaction();
            $ids = isset($_REQUEST['report_ids'])? $_REQUEST['report_ids']: array();

            if(count($ids) > 0) {
                foreach ($ids as $id) {
                    $report = $reportDao->getReportById($id);
                    //$parents = $childDao->getParent($child['child_id']);
                    $child = getChildData($report['child_id'], CHILD_INFO);
                    $parents = getChildData($report['child_id'], CHILD_PARENTS);
                    if (!is_null($parents)) {
                        foreach ($parents as $parent) {
                            //thông báo về cho phụ huynh
                            $userDao->postNotifications($parent['user_id'], NOTIFICATION_NEW_REPORT, NOTIFICATION_NODE_TYPE_CHILD,
                                $id, convertText4Web($report['report_name']), $child['child_id'], convertText4Web($child['child_name']));
                        }
                    }
                }
            }

            // Đổi trạng thái thành đã thông báo
            $reportDao->updateNotifiedForReports($ids);

            $db->commit();
            // return
            return_json(array(
                'code' => 200,
                'message' => __("OK"),
                'data' =>array()
            ));
            break;

        case 'delete_report':
            if(!isset($_POST['report_id']) || !is_numeric($_POST['report_id'])) {
                _error(404);
            }

            $db->begin_transaction();
            $reportDao->deleteReport($_POST['report_id']);

            $db->commit();

            // return
            return_json(array(
                'code' => 200,
                'message' => __('Contact book has been deleted'),
                'data' => array()
            ));

            break;

        case 'delete_temp':
            if(!$_POST['report_template_id'] || !is_numeric($_POST['report_template_id'])) {
                _api_error(400);
            }
            $temp = $reportDao->getReportTemplate($_POST['report_template_id']);
            if($temp['level'] != CLASS_LEVEL) {
                _api_error(0, __("You not permission edit this template"));
            }
            $db->begin_transaction();
            // Xóa trong bảng ci_report_template
            $reportDao->deleteReportTemplate($_POST['report_template_id']);

            // Xóa trong bảng ci_report_template_detail
            $reportDao->deleteReportTemplateDetail($_POST['report_template_id']);

            $db->commit();

            // return
            return_json(array(
                'code' => 200,
                'message' => __('Contact book template has been deleted'),
                'data' => array()
            ));

            break;
        case 'list_cate':
            $categorys = $reportDao->getAllCategoryOfSchool($class['school_id']);
            // return
            return_json(array(
                'code' => 200,
                'message' => 'OK',
                'data' => array(
                    'categorys' => $categorys
                )
            ));
            break;
        default:
            _error(400);
            break;
    }
}   catch (Exception $e) {
    $db->rollback();
    _api_error(0, $e->getMessage());
} finally {
    $db->autocommit(true);
}

?>