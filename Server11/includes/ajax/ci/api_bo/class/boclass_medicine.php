<?php
/**
 * Package: ajax/ci/bo
 * 
 * @package ConIu
 * @author QuanND
 */

// fetch image class
require(ABSPATH . 'includes/class-image.php');

// ConIu - Lấy ra danh sách
include_once(DAO_PATH . 'dao_school.php');
include_once(DAO_PATH . 'dao_medicine.php');
include_once(DAO_PATH . 'dao_parent.php');
include_once(DAO_PATH . 'dao_teacher.php');
include_once(DAO_PATH . 'dao_user.php');
include_once(DAO_PATH . 'dao_class.php');
include_once(DAO_PATH . 'dao_child.php');


$schoolDao = new SchoolDAO();
$medicineDao = new MedicineDAO();
$parentDao = new ParentDAO();
$teacherDao = new TeacherDAO();
$userDao = new UserDAO();
$classDao = new ClassDAO();
$childDao = new ChildDAO();

//Lấy ra thông tin lớp
//group_name chính là username của lớp
//$class = $classDao->getClassByUsername($_POST['group_name'], $user->_data['user_id']);
$class = getClassDataByUsername($_POST['group_name'], CLASS_INFO);
if (is_null($class)) {
    _api_error(403);
}
// Lấy ra thông tin trường
//$school = $schoolDao->getSchoolById($class['school_id']);
$school = getSchoolData($class['school_id'], SCHOOL_INFO);

// Lấy cấu hình thông báo của những user quản lý trường
//$notify_settings = $schoolDao->getUserNotificationSettingOfSchool($class['school_id']);
$notify_settings = getSchoolData($class['school_id'], SCHOOL_NOTIFICATIONS);

try {
    $return = array();
    $db->autocommit(false);
    // Tăng số lượt tương tác - TaiLa
    addInteractive($class['school_id'], 'medicine', 'school_view');
    switch ($_POST['do']) {
        case 'edit':
            // valid inputs
            if(!isset($_POST['medicine_id']) || !is_numeric($_POST['medicine_id'])) {
                _api_error(400);
            }
            $medicine = $medicineDao->getMedicine($_POST['medicine_id'], false);
            if (is_null($medicine)) {
                _api_error(0, __("The medicine does not exist, it should be deleted."));
            }

            $db->begin_transaction();

            $args = array();
            $args['medicine_id'] = $_POST['medicine_id'];
            $args['child_id'] = $_POST['child_id'];
            $args['medicine_list'] = $_POST['medicine_list'];
            $args['begin'] = $_POST['begin'];
            $args['end'] = $_POST['end'];
            $args['guide'] = $_POST['guide'];
            $args['time_per_day'] = $_POST['time_per_day'];
            $args['status'] = MEDICINE_STATUS_CONFIRMED;
            $medicine['count'] = $medicineDao->_getCountDetail($medicine['medicine_id']);
            //$medicine['can_delete'] = (($medicine['count'] == 0) && $medicine['created_user_id'] == $user->_data['user_id']);
            if ($medicine['status'] == MEDICINE_STATUS_CANCEL || $medicine['count'] > 0) {
                _api_error(0, __("You can not edit this medicine"));
            }

            $args['source_file'] = $medicine['source_file_path'];
            $args['file_name'] = convertText4Web($medicine['file_name']);

            $file_name = "";
            // Upload file đính kèm
            if(file_exists($_FILES['file']['tmp_name'])) {
                // check file upload enabled
                if(!$system['file_enabled']) {
                    _api_error(0, __("This feature has been disabled"));
                }

                // valid inputs
                if(!isset($_FILES["file"]) || $_FILES["file"]["error"] != UPLOAD_ERR_OK) {
                    _api_error(0, __("Something wrong with upload! Is 'upload_max_filesize' set correctly?"));
                }

                // check file size
                $max_allowed_size = $system['max_file_size'] * 1024;
                if($_FILES["file"]["size"] > $max_allowed_size) {
                    _api_error(0, __("The file size is so big"));
                }

                // check file extesnion
                $extension = get_extension($_FILES['file']['name']);
                if(!valid_extension($extension, $system['file_medicine_extensions'])) {
                    //modal(MESSAGE, __("Upload Error"), __("The file type is not valid or not supported"));
                    _api_error(0, __("The file type is not valid or not supported") );
                }

                /* check & create uploads dir */
                $depth = '';
                $folder = 'medicines/'. $class['school_id'];
                if(!file_exists($depth.$system['system_uploads_directory'].'/'.$folder)) {
                    @mkdir($depth.$system['system_uploads_directory'].'/'.$folder, 0777, true);
                }
                if(!file_exists($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y'))) {
                    @mkdir($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y'), 0777, true);
                }
                if(!file_exists($system['system_uploads_directory'].'/'.$folder.'/' . date('Y') . '/' . date('m'))) {
                    @mkdir($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y') . '/' . date('m'), 0777, true);
                }

                /* prepare new file name */
                $directory = $folder.'/'. date('Y') . '/' . date('m') . '/';
                $prefix = $system['uploads_prefix'].'_'.md5(time()*rand(1, 9999));

                $image = new Image($_FILES["file"]["tmp_name"]);
                $file_name = $directory.$prefix.$image->_img_ext;
                $file_name_tmp = $directory.$prefix.'_tmp'.$image->_img_ext;

                $path = $depth.$system['system_uploads_directory'].'/'.$file_name;
                $path_tmp = $depth.$system['system_uploads_directory'].'/'.$file_name_tmp;

                /* check if the file uploaded successfully */
                if(!@move_uploaded_file($_FILES['file']['tmp_name'], $path_tmp)) {
                    _api_error(0, __("Sorry, can not upload the file"));
                }
                /* save the new image */
                $image->save($path, $path_tmp);

                /* delete the tmp image */
                unlink($path_tmp);
                $args['source_file'] = $file_name;
                $args['file_name'] = $_FILES['file']['name'];
            } else {
                $args['source_file'] = '';
                $args['file_name'] = '';
            }

            //Cập nhật thông tin vào hệ thống
            $medicineDao->updateMedicine($args);

            // Lấy danh sách phụ huynh của trẻ
            //$child = $childDao->getChild($_POST['child_id']);
            $child = getChildData($_POST['child_id'], CHILD_INFO);
            //$parentIds = array();
            //$parentIds = $parentDao->getParentIds($_POST['child_id']);
            $parents = getChildData($_POST['child_id'], CHILD_PARENTS);
            if (!is_null($child) && !is_null($parents)) {
                $parentIds = array_keys($parents);
                foreach ($parentIds as $parentId) {
                    $userDao->postNotifications($parentId, NOTIFICATION_UPDATE_MEDICINE, NOTIFICATION_NODE_TYPE_CHILD,
                        $_POST['medicine_id'], convertText4Web($_POST['medicine_list']), $_POST['child_id'], convertText4Web($child['child_name']));
                }

                // Lấy id của những user quản lý được nhận thông báo
                $userIds = getUserIdsReceiveNotifyOfSchoolForClass($notify_settings, 'medicines', $class['school_id'], $school['page_admin']);

                $userDao->postNotifications($userIds, NOTIFICATION_UPDATE_MEDICINE, NOTIFICATION_NODE_TYPE_SCHOOL,
                    $_POST['medicine_id'], convertText4Web($_POST['medicine_list']), $school['page_name'], convertText4Web($child['child_name']));
            }

            $db->commit();

            // return
            return_json(array(
                'code' => 200,
                'message' => __("Done, Medicine info have been updated"),
                'data' => array()
            ));
            break;

        case 'add':
            $db->begin_transaction();

            $args = array();
            $args['child_id'] = $_POST['child_id'];
            $args['medicine_list'] = $_POST['medicine_list'];
            $args['begin'] = $_POST['begin'];
            $args['end'] = $_POST['end'];
            $args['guide'] = $_POST['guide'];
            $args['time_per_day'] = $_POST['time_per_day'];
            $args['created_user_id'] = $user->_data['user_id'];
            $args['status'] = MEDICINE_STATUS_CONFIRMED; //Do giáo viên tạo ra thì chuyển trạng thái xác nhận luôn
            //Nhập thông tin vào hệ thống

            $file_name = "";
            // Upload file đính kèm
            if(file_exists($_FILES['file']['tmp_name'])) {
                // check file upload enabled
                if(!$system['file_enabled']) {
                    _api_error(0, __("This feature has been disabled"));
                }

                // valid inputs
                if(!isset($_FILES["file"]) || $_FILES["file"]["error"] != UPLOAD_ERR_OK) {
                    _api_error(0, __("Something wrong with upload! Is 'upload_max_filesize' set correctly?"));
                }

                // check file size
                $max_allowed_size = $system['max_file_size'] * 1024;
                if($_FILES["file"]["size"] > $max_allowed_size) {
                    _api_error(0, __("The file size is so big"));
                }

                // check file extesnion
                $extension = get_extension($_FILES['file']['name']);
                if(!valid_extension($extension, $system['file_medicine_extensions'])) {
                    // modal(MESSAGE, __("Upload Error"), __("The file type is not valid or not supported"));
                    _api_error(0, __("The file type is not valid or not supported") );
                }

                /* check & create uploads dir */
                $depth = '';
                $folder = 'medicines/'. $class['school_id'];
                if(!file_exists($depth.$system['system_uploads_directory'].'/'.$folder)) {
                    @mkdir($depth.$system['system_uploads_directory'].'/'.$folder, 0777, true);
                }
                if(!file_exists($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y'))) {
                    @mkdir($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y'), 0777, true);
                }
                if(!file_exists($system['system_uploads_directory'].'/'.$folder.'/' . date('Y') . '/' . date('m'))) {
                    @mkdir($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y') . '/' . date('m'), 0777, true);
                }

                /* prepare new file name */
                $directory = $folder.'/'. date('Y') . '/' . date('m') . '/';
                $prefix = $system['uploads_prefix'].'_'.md5(time()*rand(1, 9999));

                $image = new Image($_FILES["file"]["tmp_name"]);
                $file_name = $directory.$prefix.$image->_img_ext;
                $file_name_tmp = $directory.$prefix.'_tmp'.$image->_img_ext;

                $path = $depth.$system['system_uploads_directory'].'/'.$file_name;
                $path_tmp = $depth.$system['system_uploads_directory'].'/'.$file_name_tmp;

                /* check if the file uploaded successfully */
                if(!@move_uploaded_file($_FILES['file']['tmp_name'], $path_tmp)) {
                    _api_error(0, __("Sorry, can not upload the file"));
                }
                /* save the new image */
                $image->save($path, $path_tmp);

                /* delete the tmp image */
                unlink($path_tmp);
            }

            $args['source_file'] = $file_name;
            $args['file_name'] = $_FILES['file']['name'];

            $medicineId = $medicineDao->insertMedicine($args);

            /* Thông báo cô giáo và phụ huynh */
            // Lấy danh sách phụ huynh của trẻ
            //$child = $childDao->getChild($_POST['child_id']);
            $child = getChildData($_POST['child_id'], CHILD_INFO);
            //$parentIds = array();
            //$parentIds = $parentDao->getParentIds($_POST['child_id']);
            $parents = getChildData($_POST['child_id'], CHILD_PARENTS);
            if (!is_null($child) && !is_null($parents)) {
                $parentIds = array_keys($parents);

                foreach ($parentIds as $parentId) {
                    $userDao->postNotifications($parentId, NOTIFICATION_NEW_MEDICINE, NOTIFICATION_NODE_TYPE_CHILD,
                        $medicineId, convertText4Web($_POST['medicine_list']), $_POST['child_id'], convertText4Web($child['child_name']));
                }

                // Lấy id của những user quản lý được nhận thông báo
                $userIds = getUserIdsReceiveNotifyOfSchoolForClass($notify_settings, 'medicines', $class['school_id'], $school['page_admin']);

                $userDao->postNotifications($userIds, NOTIFICATION_NEW_MEDICINE, NOTIFICATION_NODE_TYPE_SCHOOL,
                    $medicineId, convertText4Web($_POST['medicine_list']), $school['page_name'], convertText4Web($child['child_name']));
            }

            // Tăng số lượt tương tác - TaiLa
            addInteractive($class['school_id'], 'medicine', 'school_view', $medicineId, 1);

            $db->commit();
            // return
            return_json(array(
                'code' => 200,
                'message' => __("Done, Medicine info have been created"),
                'data' => array()
            ));
            break;

        case 'delete':
            if(!isset($_POST['medicine_id']) || !is_numeric($_POST['medicine_id'])) {
                _api_error(400);
            }
            $db->begin_transaction();

            $medicine = $medicineDao->getMedicine($_POST['medicine_id'], false);
            if (is_null($medicine)) {
                _api_error(0, __("The medicine does not exist, it should be deleted."));
            }
            $medicine['count'] = $medicineDao->_getCountDetail($medicine['medicine_id']);
            $medicine['can_delete'] = (($medicine['count'] == 0) && $medicine['created_user_id'] == $user->_data['user_id']);
            if (!$medicine['can_delete']) {
                _api_error(0, __("You can not delete this medicine"));
            }
            $medicineDao->deleteMedicine($medicine['medicine_id']);
            /* Thông báo quản lý trường và phụ huynh */

            $child = getChildData($medicine['child_id'], CHILD_INFO);
            //$parentIds = array();
            //$parentIds = $parentDao->getParentIds($medicine['child_id']);
            $parents = getChildData($medicine['child_id'], CHILD_PARENTS);
            if (!is_null($child) && !is_null($parents)) {
                $parentIds = array_keys($parents);

                foreach ($parentIds as $parentId) {
                    $userDao->postNotifications($parentId['user_id'], NOTIFICATION_CANCEL_MEDICINE, NOTIFICATION_NODE_TYPE_CHILD,
                        $_POST['medicine_id'], convertText4Web($medicine['medicine_list']), $child['child_id'], convertText4Web($child['child_name']));
                }

                // Lấy id của những user quản lý được nhận thông báo
                $userIds = getUserIdsReceiveNotifyOfSchoolForClass($notify_settings, 'medicines', $class['school_id'], $school['page_admin']);

                $userDao->postNotifications($userIds, NOTIFICATION_CANCEL_MEDICINE, NOTIFICATION_NODE_TYPE_SCHOOL,
                    $_POST['medicine_id'], convertText4Web($_POST['medicine_list']), $school['page_name'], convertText4Web($child['child_name']));
            }

            $db->commit();
            // return
            return_json(array(
                'code' => 200,
                'message' => __("Done, Medicine info have been deleted"),
                'data' => array()
            ));

            break;
        case 'medicate':
            if(!isset($_POST['medicine_id']) || !is_numeric($_POST['medicine_id'])
                || !isset($_POST['max']) || !is_numeric($_POST['max'])) {
                _api_error(400);
            }
            $db->begin_transaction();

            $medicine = $medicineDao->getMedicine($_POST['medicine_id'], false);
            if (is_null($medicine)) {
                _api_error(0, __("The medicine does not exist, it should be deleted."));
            }
            $medicine['count'] = $medicineDao->_getCountDetail($medicine['medicine_id']);
            if ($medicine['status'] == MEDICINE_STATUS_CANCEL) {
                _api_error(0, __("This medicine have been cancelled before!"));
            }
            if ($medicine['time_per_day'] <= $medicine['count']) {
                _api_error(0, __("Trẻ đã được cho uống đủ liều"));
            }
            //Thêm lần uống thuốc vào hệ thống
            $medicineDao->addMedicateTime($_POST['medicine_id'], $_POST['max']);
            //Cập nhật trạng thái của lần gửi thuốc
            $medicineDao->updateMedicineStatus(MEDICINE_STATUS_CONFIRMED, $_POST['medicine_id']);

            /* Thông báo quản lý trường và phụ huynh */
            // Lấy danh sách phụ huynh của trẻ
            $child = getChildData($_POST['child_id'], CHILD_INFO);
            //$parentIds = array();
            //$parentIds = $parentDao->getParentIds($medicine['child_id']);
            $parents = getChildData($_POST['child_id'], CHILD_PARENTS);
            if (!is_null($child) && !is_null($parents)) {
                $parentIds = array_keys($parents);
                foreach ($parentIds as $parentId) {
                    $userDao->postNotifications($parentId, NOTIFICATION_USE_MEDICINE, NOTIFICATION_NODE_TYPE_CHILD,
                        $_POST['medicine_id'], convertText4Web($_POST['medicine_list']), $_POST['child_id'], convertText4Web($child['child_name']));
                }

                // Lấy id của những user quản lý được nhận thông báo
                $userIds = getUserIdsReceiveNotifyOfSchoolForClass($notify_settings, 'medicines', $class['school_id'], $school['page_admin']);

                $userDao->postNotifications($userIds, NOTIFICATION_USE_MEDICINE, NOTIFICATION_NODE_TYPE_SCHOOL,
                    $_POST['medicine_id'], convertText4Web($_POST['medicine_list']), $school['page_name'], convertText4Web($child['child_name']));
            }

            $db->commit();
            // return
            return_json(array(
                'code' => 200,
                'message' => __("Success"),
                'data' => array()
            ));
            break;

        case 'confirm':
            $db->begin_transaction();
            $medicine = $medicineDao->getMedicine($_POST['medicine_id'], false);
            if (is_null($medicine)) {
                _api_error(0, __("The medicine does not exist, it should be deleted."));
            }
            if ($medicine['status'] == MEDICINE_STATUS_CANCEL) {
                _api_error(0, __("This medicine have been cancelled before!"));
            }
            if ($medicine['status'] == MEDICINE_STATUS_NEW) {
                //Cập nhật trạng thái của lần gửi thuốc
                $medicineDao->updateMedicineStatus(MEDICINE_STATUS_CONFIRMED, $_POST['medicine_id']);

                /* Thông báo quản lý trường và phụ huynh */
                //$child = $childDao->getChild($_POST['child_id']);
                $child = getChildData($_POST['child_id'], CHILD_INFO);

                // Lấy danh sách phụ huynh của trẻ
                //$parentIds = array();
                //$parentIds = $parentDao->getParentIds($_POST['child_id']);
                $parents = getChildData($_POST['child_id'], CHILD_PARENTS);
                if (!is_null($child) && !is_null($parents)) {
                    $parentIds = array_keys($parents);
                    foreach ($parentIds as $parentId) {
                        $userDao->postNotifications($parentId, NOTIFICATION_CONFIRM_MEDICINE, NOTIFICATION_NODE_TYPE_CHILD,
                            $_POST['medicine_id'], convertText4Web($medicine['medicine_list']), $child['child_id'], convertText4Web($child['child_name']));
                    }

                    // Lấy id của những user quản lý được nhận thông báo
                    $userIds = getUserIdsReceiveNotifyOfSchoolForClass($notify_settings, 'medicines', $class['school_id'], $school['page_admin']);

                    $userDao->postNotifications($userIds, NOTIFICATION_CONFIRM_MEDICINE, NOTIFICATION_NODE_TYPE_SCHOOL,
                        $_POST['medicine_id'], convertText4Web($_POST['medicine_list']), $school['page_name'], convertText4Web($child['child_name']));
                }
            }

            $db->commit();
            // return
            return_json(array(
                'code' => 200,
                'message' => __("Success"),
                'data' => array()
            ));
            break;
        case 'confirm_selected':
            $db->begin_transaction();

            // Lấy tất cả danh sách thuốc chưa xác nhận của lớp
            $medicineIds = isset($_REQUEST['list_medicine_id'])? $_REQUEST['list_medicine_id']: array();
            //Cập nhật trạng thái của lần gửi thuốc
            $medicineDao->updateAllMedicineStatus(MEDICINE_STATUS_CONFIRMED, $medicineIds);
            foreach ($medicineIds as $medicineId) {
                $medicine = $medicineDao->getMedicine($medicineId);
                //$child = $childDao->getChild($medicine['child_id']);
                $child = getChildData($medicine['child_id'], CHILD_INFO);
                // Lấy danh sách phụ huynh của trẻ
                //$parentIds = array();
                //$parentIds = $parentDao->getParentIds($medicine['child_id']);
                $parents = getChildData($medicine['child_id'], CHILD_PARENTS);
                if (!is_null($child) && !is_null($parents)) {
                    $parentIds = array_keys($parents);
                    foreach ($parentIds as $parentId) {
                        $userDao->postNotifications($parentId, NOTIFICATION_CONFIRM_MEDICINE, NOTIFICATION_NODE_TYPE_CHILD,
                            $medicine['medicine_id'], convertText4Web($medicine['medicine_list']), $child['child_id'], convertText4Web($child['child_name']));
                    }

                    // Lấy id của những user quản lý được nhận thông báo
                    $userIds = getUserIdsReceiveNotifyOfSchoolForClass($notify_settings, 'medicines', $class['school_id'], $school['page_admin']);

                    $userDao->postNotifications($userIds, NOTIFICATION_CONFIRM_MEDICINE, NOTIFICATION_NODE_TYPE_SCHOOL,
                        $medicine['medicine_id'], convertText4Web($medicine['medicine_list']), $school['page_name'], convertText4Web($child['child_name']));
                }
            }

            $db->commit();
            // return
            return_json(array(
                'code' => 200,
                'message' => __("Success"),
                'data' => array()
            ));
            break;
        case 'confirm_all_today':
            $db->begin_transaction();

            // Lấy tất cả danh sách thuốc chưa xác nhận của lớp trong ngày hôm nay
            $today = date($system['date_format']);
            $medicineIds = $medicineDao->getClassMedicineIdsOnDateNoConfirm($class['group_id'], $today);
            //Cập nhật trạng thái của lần gửi thuốc

            if(count($medicineIds) > 0) {
                $medicineDao->updateAllMedicineStatus(MEDICINE_STATUS_CONFIRMED, $medicineIds);
                foreach ($medicineIds as $medicineId) {
                    $medicine = $medicineDao->getMedicine($medicineId);
                    //$child = $childDao->getChild($medicine['child_id']);
                    $child = getChildData($medicine['child_id'], CHILD_INFO);
                    //Thông báo quản lý trường và phụ huynh

                    // Lấy danh sách phụ huynh của trẻ
                    //$parentIds = array();
                    //$parentIds = $parentDao->getParentIds($medicine['child_id']);
                    $parents = getChildData($medicine['child_id'], CHILD_PARENTS);
                    if (!is_null($child) && !is_null($parents)) {
                        $parentIds = array_keys($parents);
                        foreach ($parentIds as $parentId) {
                            $userDao->postNotifications($parentId, NOTIFICATION_CONFIRM_MEDICINE, NOTIFICATION_NODE_TYPE_CHILD,
                                $medicine['medicine_id'], convertText4Web($medicine['medicine_list']), $child['child_id'], convertText4Web($child['child_name']));
                        }

                        // Lấy id của những user quản lý được nhận thông báo
                        $userIds = getUserIdsReceiveNotifyOfSchoolForClass($notify_settings, 'medicines', $class['school_id'], $school['page_admin']);

                        $userDao->postNotifications($userIds, NOTIFICATION_CONFIRM_MEDICINE, NOTIFICATION_NODE_TYPE_SCHOOL,
                            $medicine['medicine_id'], convertText4Web($medicine['medicine_list']), $school['page_name'], convertText4Web($child['child_name']));
                    }
                }

                $db->commit();
            }

            // return
            return_json(array(
                'code' => 200,
                'message' => __("Success"),
                'data' => array()
            ));
            break;
        case 'confirm_all':
            $db->begin_transaction();

            // Lấy tất cả danh sách thuốc chưa xác nhận của lớp
            $medicineIds = $medicineDao->getClassAllMedicineIdsNoConfirm($class['group_id']);
            //Cập nhật trạng thái của lần gửi thuốc
            $medicineDao->updateAllMedicineStatus(MEDICINE_STATUS_CONFIRMED, $medicineIds);
            foreach ($medicineIds as $medicineId) {
                $medicine = $medicineDao->getMedicine($medicineId);
                //$child = $childDao->getChild($medicine['child_id']);
                $child = getChildData($medicine['child_id'], CHILD_INFO);
                //Thông báo quản lý trường và phụ huynh

                // Lấy danh sách phụ huynh của trẻ
                //$parentIds = array();
                //$parentIds = $parentDao->getParentIds($medicine['child_id']);
                $parents = getChildData($medicine['child_id'], CHILD_PARENTS);
                if (!is_null($child) && !is_null($parents)) {
                    $parentIds = array_keys($parents);
                    foreach ($parentIds as $parentId) {
                        $userDao->postNotifications($parentId, NOTIFICATION_CONFIRM_MEDICINE, NOTIFICATION_NODE_TYPE_CHILD,
                            $medicine['medicine_id'], convertText4Web($medicine['medicine_list']), $child['child_id'], convertText4Web($child['child_name']));
                    }

                    // Lấy id của những user quản lý được nhận thông báo
                    $userIds = getUserIdsReceiveNotifyOfSchoolForClass($notify_settings, 'medicines', $class['school_id'], $school['page_admin']);

                    $userDao->postNotifications($userIds, NOTIFICATION_CONFIRM_MEDICINE, NOTIFICATION_NODE_TYPE_SCHOOL,
                        $medicine['medicine_id'], convertText4Web($medicine['medicine_list']), $school['page_name'], convertText4Web($child['child_name']));
                }
            }

            $db->commit();
            // return
            return_json(array(
                'code' => 200,
                'message' => __("Success"),
                'data' => array()
            ));
            break;

        case 'cancel':
            $db->begin_transaction();
            $medicine = $medicineDao->getMedicine($_POST['medicine_id'], false);
            if (is_null($medicine)) {
                _api_error(0, __("The medicine does not exist, it should be deleted."));
            }
            if ($medicine['status'] == MEDICINE_STATUS_CANCEL) {
                _api_error(0, __("This medicine have been cancelled before!"));
            }
            //Cập nhật trạng thái của lần gửi thuốc
            $medicineDao->updateMedicineStatus(MEDICINE_STATUS_CANCEL, $_POST['medicine_id']);

            /* Thông báo quản lý trường và phụ huynh */
            //$child = $childDao->getChild($_POST['child_id']);
            $child = getChildData($_POST['child_id'], CHILD_INFO);
            // Lấy danh sách phụ huynh của trẻ
            //$parentIds = array();
            //$parentIds = $parentDao->getParentIds($_POST['child_id']);
            $parents = getChildData($_POST['child_id'], CHILD_PARENTS);
            if (!is_null($child) && !is_null($parents)) {
                $parentIds = array_keys($parents);
                foreach ($parentIds as $parentId) {
                    $userDao->postNotifications($parentId, NOTIFICATION_CANCEL_MEDICINE, NOTIFICATION_NODE_TYPE_CHILD,
                        $_POST['medicine_id'], convertText4Web($medicine['medicine_list']), $child['child_id'], convertText4Web($child['child_name']));
                }

                // Lấy id của những user quản lý được nhận thông báo
                $userIds = getUserIdsReceiveNotifyOfSchoolForClass($notify_settings, 'medicines', $class['school_id'], $school['page_admin']);

                $userDao->postNotifications($userIds, NOTIFICATION_CANCEL_MEDICINE, NOTIFICATION_NODE_TYPE_SCHOOL,
                    $_POST['medicine_id'], convertText4Web($_POST['medicine_list']), $school['page_name'], convertText4Web($child['child_name']));
            }

            $db->commit();
            // return
            return_json(array(
                'code' => 200,
                'message' => __("Done, Medicine info have been cancelled"),
                'data' => array()
            ));
            break;

        case 'detail':
            if(!isset($_POST['medicine_id']) || !is_numeric($_POST['medicine_id'])) {
                _error(400);
            }

            $details = $medicineDao->getMedicineDetail($_POST['medicine_id']);
            // return
            return_json(array(
                'code' => 200,
                'message' => "OK",
                'data' => $details
            ));
            break;

        default:
            _api_error(404);
            break;
    }

} catch (Exception $e) {
    $db->rollback();
    _api_error(0, $e->getMessage());
} finally {
    $db->autocommit(true);
}

?>