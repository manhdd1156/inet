<?php
/**
 * Package: ajax/ci/bo/class
 *
 * @package ConIu
 * @author QuanND
 */

include_once(DAO_PATH.'dao_school.php');
include_once(DAO_PATH.'dao_class.php');
include_once(DAO_PATH.'dao_child.php');
include_once(DAO_PATH.'dao_parent.php');
include_once(DAO_PATH.'dao_user.php');
include_once(DAO_PATH.'dao_service.php');
include_once(DAO_PATH.'dao_attendance.php');

$schoolDao = new SchoolDAO();
$classDao = new ClassDAO();
$childDao = new ChildDAO();
$parentDao = new ParentDAO();
$userDao = new UserDAO();
$serviceDao = new ServiceDAO();
$attendanceDao = new AttendanceDAO();

//Lấy ra thông tin lớp
//$class = $classDao->getClassByUsername($_POST['group_name'], $user->_data['user_id']);
$class = getClassDataByUsername($_POST['group_name'], CLASS_INFO);
if (is_null($class)) {
    _api_error(403);
}
//$smarty->assign('username', $_POST['username']);
// Lấy ra thông tin trường
//$school = $schoolDao->getSchoolById($class['school_id']);
$school = getSchoolData($class['school_id'], SCHOOL_INFO);
// Lấy cấu hình thông báo của những user quản lý trường
//$notify_settings = $schoolDao->getUserNotificationSettingOfSchool($class['school_id']);
$notify_settings = getSchoolData($class['school_id'], SCHOOL_NOTIFICATIONS);

try {
    $db->autocommit(false);
    addInteractive($class['school_id'], 'service', 'school_view');

    switch ($_POST['do']) {
        case 'show_service':
            $valid = array('services', 'all');
            if (!in_array($_POST['view'], $valid)) {
                _api_error(400);
            }

            switch ($_POST['view']) {
                case 'services':
                    //Lấy ra danh sách dịch vụ tính phí theo số lần sử dụng
                    $services = $serviceDao->getCountBasedServices($class['school_id']);
                    // return
                    return_json(array(
                        'code' => 200,
                        'message' => 'OK',
                        'data' => array(
                            'services' => $services
                        )
                    ));
                    break;

                case 'all':
                    //Lấy ra danh sách dịch vụ tính phí theo số lần sử dụng
                    $services = $serviceDao->getCountBasedServices($class['school_id']);
                    //Lấy ra danh sách trẻ của lớp
                    $childs = $childDao->getChildrenOfClass($class['group_id']);
                    $children = array();
                    foreach ($childs as $child) {
                        $result['child_id'] = $child['child_id'];
                        $result['child_name'] = $child['child_name'];
                        $result['child_status'] = $child['child_status'];
                        $children[] = $result;
                    }
                    // return
                    return_json(array(
                        'code' => 200,
                        'message' => 'OK',
                        'data' => array(
                            'services' => $services,
                            'children' => $children
                        )
                    ));
                    break;

            }
            break;

        case 'list_usage':
            //Validate thông tin đầu vào
            $inputCorrect = true;
            if (!isset($_POST['using_at']) || !validateDate($_POST['using_at'])) {
                $inputCorrect = false;
            }

            // Lấy danh sách trẻ của lớp đã đăng ký dịch vụ trong ci_service_usage
            if ($inputCorrect) {

                // Lấy danh sách dịch vụ theo số lần sử dụng của trường
                $cb_services = $serviceDao->getCountBasedServicesForAPI($class['school_id']);

                // Lấy danh sách trẻ của một lớp
                $children = $childDao->getChildrenOfClass($class['group_id'], $_POST['using_at']);

                // Lấy danh sách id của trẻ trong lớp
                $childrenIds = array();
                foreach ($children as $child) {
                    $childrenIds[] = $child['child_id'];
                }
                if(!(count($childrenIds) > 0)) {
                    throw new Exception(__("Not student in class"));
                }

                // Lấy danh sách trẻ đã nghỉ học trong ngày
                $absentChildIds = $attendanceDao->getAbsentChildIds($class['school_id'], $_POST['using_at'], $class['group_id']);

                $results = $serviceDao->getAllServiceCBForAllChildOfClass($_POST['using_at'], $childrenIds);
                $list_child = array();
                $fields = array( "begin_at", "end_at", "child_status", "class_id", "child_id", "child_name", "birthday");
                foreach ($children as $child) {
                    $child = getArrayFromKeys($child, $fields);

                    $cb_services_full = array();
                    foreach ($cb_services as $service) {
                        $service['service_usage_id'] = null;
                        $service['using_at'] = null;
                        $service['recorded_at'] = null;
                        $service['user_fullname'] = null;
                        foreach ($results as $result) {
                            if(($result['child_id'] == $child['child_id']) && ($result['service_id'] == $service['service_id'])){
                                $service['service_usage_id'] = $result['service_usage_id'];
                                $service['using_at'] = $result['using_at'];
                                $service['recorded_at'] = $result['recorded_at'];
                                $service['user_fullname'] = $result['user_fullname'];
                            }
                        }
                        $cb_services_full[] = $service;
                    }
                    $child['cb_services'] = $cb_services_full;
                    $list_child[] = $child;
                }
                // return & exit
                return_json(array(
                    'code' => 200,
                    'message' => 'OK',
                    'data' => array(
                        'list_usage' => $list_child,
//                        'usage_count' => $usage_count,
                        'absentChildIds' => $absentChildIds
                    )
                ));
            } else {
                _api_error(0, __("No data or incorrect input data"));
            }
            break;

        case 'save':
            //Danh ID trẻ sử dụng, tương ứng 1 ID đi kèm với danh sách dịch vụ sử dụng
            $registerChildIds = isset($_REQUEST['list_childId'])? $_REQUEST['list_childId']: array();
            // Lấy danh sách ID trẻ của một lớp
            //$childrenIds = $childDao->getChildIdOfClass($class['group_id']);
            $children = getClassData($class['group_id'], CLASS_CHILDREN);
            $childrenIds = array_keys($children);

            //Validate thông tin đầu vào
            $inputCorrect = true;
            if (!isset($_POST['using_at']) || !validateDate($_POST['using_at'])) {
                $inputCorrect = false;
            }
            if($inputCorrect && (count($childrenIds) > 0)) {
                // Lặp danh sách trẻ của lớp, kiểm tra xem trẻ có được thay đổi đăng ký dịch vụ không
                foreach ($childrenIds as $childId) {
                    // Lấy Id những dịch vụ theo số lần sử dụng mà trẻ đã đăng ký trước đó theo ngày truyền vào
                    $servicesIdCB = $serviceDao->getCountBasedServiceAChildOfDay($class['school_id'], $childId, $_POST['using_at']);

                    // Lấy Id những dịch vụ mà trẻ đăng ký hiện tại
                    $serviceIdRegister = isset($registerChildIds[$childId])?$registerChildIds[$childId]:array();
                    // Những trẻ có thay đổi đk dịch vụ thì thông báo về cho phụ huynh
                    if(!array_equal($servicesIdCB, $serviceIdRegister)) {
                        $child = getChildData($childId, CHILD_INFO);
                        $parents = getChildData($childId, CHILD_PARENTS);
                        if (!is_null($child) && !is_null($parents)) {
                            $parentIds = array_keys($parents);
                            foreach ($parentIds as $parentId) {
                                $userDao->postNotifications($parentId, NOTIFICATION_SERVICE_REGISTER_COUNTBASED, NOTIFICATION_NODE_TYPE_CHILD,
                                    $childId, '', $childId, convertText4Web($child['child_name']));
                            }
                        }
                    }
                }

                // Lấy danh sách trẻ đã đăng ký dịch vụ của lớp
                $db->begin_transaction();
                $serviceDao->deleteAllServiceCBForAllChildOfClass($_POST['using_at'], $childrenIds);
                if (count($registerChildIds) > 0) {
                    // Lưu danh sách trẻ đăng ký dịch vụ
                    $serviceDao->recordServiceUsageForAllChild($registerChildIds, $_POST['using_at']);
                }
                // Lấy id của những user quản lý được nhận thông báo
                $userIds = getUserIdsReceiveNotifyOfSchoolForClass($notify_settings, 'services', $class['school_id'], $school['page_admin']);

                $userDao->postNotifications($userIds, NOTIFICATION_SERVICE_REGISTER_COUNTBASED_MOBILE, NOTIFICATION_NODE_TYPE_SCHOOL,
                    $class['group_id'], convertText4Web($class['group_title']), $school['page_name'], $_POST['using_at']);

                $db->commit();

                return_json(array(
                    'code' => 200,
                    'message' => __("Done, Service usage info have been updated"),
                    'data' => null
                ));
            } else {
                _api_error(0, __("No data or incorrect input data"));
            }

            break;
        case 'history':
            $class_id = $class['group_id'];
            $services = getSchoolData($class['school_id'], SCHOOL_SERVICES);

            $data = array();
            $fields = array('service_name', 'children', 'usage_count');
            foreach ($services as $service) {
                if($service['type'] == SERVICE_TYPE_COUNT_BASED) {
                    $results = $serviceDao->getSchoolChildService4RecordForAPI($service['service_id'], $_POST['using_at'], $class_id);
                    $service['usage_count'] = $results['usage_count'];
                    $service['children'] = $results['children'];
                    $service = getArrayFromKeys($service, $fields);
                    $data[] = $service;
                }
            }

            return_json(array(
                'code' => 200,
                'message' => __("OK"),
                'data' => array(
                    'history' => $data
                )
            ));
            break;
        default:
            _error(400);
            break;
    }
} catch (Exception $e) {
    $db->rollback();
    _api_error(0, $e->getMessage());
} finally {
    $db->autocommit(true);
}

?>