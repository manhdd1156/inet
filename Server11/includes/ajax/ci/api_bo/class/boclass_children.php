<?php
/**
 * Package: ajax/ci/bo
 * 
 * @package ConIu
 * @author Coniu
 */

include_once(DAO_PATH . 'dao_class.php');
include_once(DAO_PATH . 'dao_child.php');

$classDao = new ClassDAO();
$childDao = new ChildDAO();

//Lấy ra thông tin lớp, bao gồm cả role_id của user hiện tại
//$class = $classDao->getClassByUsername($_POST['group_name'], $user->_data['user_id']);
$class = getClassDataByUsername($_POST['group_name'], CLASS_INFO);
if (is_null($class)) {
    _api_error(403);
}

try {
    $return = array();
    //$db->autocommit(false);
    switch ($_POST['do']) {
        case 'show_children':
            //$children = $childDao->getChildrenOfClassForAPI($class['group_id'], true);
            $children = array();
            $childs = getClassData($class['group_id'], CLASS_CHILDREN);
            $childs = sortArray($childs, "ASC", "name_for_sort");
            $fields = array('child_id', 'child_code', 'child_name', 'child_nickname', 'birthday', 'gender', 'child_picture', 'parent_name',
                'parent_phone', 'parent_email', 'address', 'description', 'created_user_id');
            foreach ($childs as $child) {
                $child = getArrayFromKeys($child, $fields);
                $parents = getChildData($child['child_id'], CHILD_PARENTS);
                $arrParents = array();
                foreach ($parents as $parent) {
                    $arrParents[] = $parent;
                }
                $child['parent'] = $arrParents;
                $children[] = $child;
            }
            // return
            return_json(array(
                'code' => 200,
                'message' => 'OK',
                'data' => array(
                    'child_list' => $children
                )
            ));
            break;

        case 'show_active_children_short':
            //$children = $childDao->getChildrenShortClassForAPI($class['group_id']);
            $children = array();
            $childs = getClassData($class['group_id'], CLASS_CHILDREN);
            $childs = sortArray($childs, "ASC", "name_for_sort");
            $fields = array('child_id', 'child_code', 'child_name', 'birthday', 'gender',  'parent_name',
                'parent_phone', 'status', 'child_picture');
            foreach ($childs as $child) {
                $child = getArrayFromKeys($child, $fields);
                $children[] = $child;
            }
            // return
            return_json(array(
                'code' => 200,
                'message' => 'OK',
                'data' => array(
                    'child_list' => $children
                )
            ));
            break;

        case 'show_children_short':
            $children = $childDao->getChildrenShortClassForAPI($class['group_id']);

            // return
            return_json(array(
                'code' => 200,
                'message' => 'OK',
                'data' => array(
                    'child_list' => $children
                )
            ));
            break;

        case 'detail':
            if(!isset($_POST['child_id']) || !is_numeric($_POST['child_id'])) {
                _api_error(400);
            }

            //$child = $childDao->getChildDetailOfClassForAPI($class['group_id'], $_POST['child_id']);

            //$chidrenOfClass = getClassData($class['group_id'], CLASS_CHILDREN);
            $fields = array('child_id', 'child_code', 'first_name', 'last_name', 'child_name', 'child_nickname', 'birthday', 'gender', 'child_picture', 'child_admin', 'parent_name',
                'parent_phone', 'parent_email', 'address', 'description', 'created_user_id', 'school_id', 'status', 'begin_at');
            $child = getChildData($_POST['child_id'], CHILD_INFO);
            if (is_null($child)) {
                _api_error(400);
            }
            $child = getArrayFromKeys($child, $fields);
            $parents = getChildData($child['child_id'], CHILD_PARENTS);
            $arrParents = array();
            foreach ($parents as $parent) {
                $arrParents[] = $parent;
            }
            $child['parent'] = $arrParents;
            
            // return
            return_json(array(
                'code' => 200,
                'message' => 'OK',
                'data' => array(
                    'child' => $child
                )
            ));
            break;

        default:
            _api_error(404);
            break;
    }

} catch (Exception $e) {
    //$db->rollback();
    _api_error(0, $e->getMessage());
} /*finally {
    $db->autocommit(true);
}*/

?>