<?php
/**
 * Package: ajax/ci/bo
 * 
 * @package ConIu
 * @author ConIu
 */

include_once(DAO_PATH . 'dao_class.php');
include_once(DAO_PATH . 'dao_tuition.php');
$classDao = new ClassDAO();
$tuitionDao = new TuitionDAO();

// check username
if (is_empty($_POST['group_name'])) {
    _api_error(400);
}
//Lấy ra thông tin lớp, bao gồm cả role_id của user hiện tại
//$class = $classDao->getClassByUsername($_POST['group_name'], $user->_data['user_id']);
$class = getClassDataByUsername($_POST['group_name'], CLASS_INFO);
if (is_null($class)) {
    _api_error(403);
}

try {
    $return = array();
    addInteractive($class['school_id'], 'tuition', 'school_view');

    switch ($_POST['do']) {
        case 'show_tuition':

            switch ($_POST['view']) {
                case 'list': //Lấy ra danh sách học phí của lớp (danh sách tháng).
                    $page = isset($_POST['page']) ? $_POST['page'] : 0;
                    $tuitions = $tuitionDao->getClassTuitionsForApi($class['group_id'], $page);

                    //Lấy ra cấu hình cho phép xem học phí của lớp
                    $schoolConfig = getSchoolData($class['school_id'], SCHOOL_CONFIG);
                    $allow_class_see_tuition = $schoolConfig['allow_class_see_tuition'] ? true : false;
                    // return
                    return_json(array(
                        'code' => 200,
                        'message' => 'OK',
                        'data' => array(
                            'tuitions' => $tuitions,
                            'allow_class_see_tuition' => $allow_class_see_tuition
                        )
                    ));
                    break;

                case 'detail_class': //Trả về danh sách học phí của trẻ trong lớp.
                    if (!is_numeric($_POST['tuition_id'])) {
                        _api_error(400);
                    }
                    $tuitionChildren = $tuitionDao->getTuitionChildForApi($_POST['tuition_id'], $class['school_id']);
                    if (is_null($tuitionChildren)) {
                        _api_error(400);
                    }

                    addInteractive($class['school_id'], 'tuition', 'school_view', $_POST['tuition_id']);


                    return_json(array(
                        'code' => 200,
                        'message' => 'OK',
                        'data' => array(
                            'tuition_child' => $tuitionChildren
                        )
                    ));
                    break;

                case 'detail_child':
                    if (!is_numeric($_POST['tuition_child_id'])) {
                        _api_error(400);
                    }

                    $tuitionDetails = $tuitionDao->getTuitionDetailOfChildForApi($_POST['tuition_child_id']);
                    if (is_null($tuitionDetails)) {
                        _api_error(400);
                    }
                    $tuitionChild = $tuitionDao->getTuitionChildOnly($_POST['tuition_child_id']);
                    if (is_null($tuitionChild)) {
                        _api_error(404);
                    }

                    // $preUsageHistory = $tuitionDao->getUsageHistoryOfChildForApi($_POST['tuition_child_id']);
                    $preUsageHistory = $tuitionDao->getTuitionChildHistory($tuitionChild['tuition_id'], $tuitionChild['child_id']);
//                    $preUsageHistory = $tuitionDao->getTuitionDetail4History($tuitionChild['tuition_child_id'], $attendanceCount);
                    if(empty($preUsageHistory['tuition_detail'])) {
                        $usage_history = null;
                    } else {
                        $usage_history = $preUsageHistory['tuition_detail'];
                    }
                    return_json(array(
                        'code' => 200,
                        'message' => 'OK',
                        'data' => array(
                            'tuition_detail' => $tuitionDetails,
                            'status' => $tuitionChild['status'],
                            'month' => $tuitionChild['month'],
                            'description' => $tuitionChild['description'],
                            'total_amount' => $tuitionChild['total_amount'],
                            'total_deduction' => $tuitionChild['total_deduction'],
                            'debt_amount' => $tuitionChild['debt_amount'],
                            'paid_amount' => $tuitionChild['paid_amount'],
                            'pre_month' => $preUsageHistory['pre_month'],
                            'usage_history' => $usage_history
                        )
                    ));
                    break;
            }
            break;

        default:
            _api_error(404);
            break;
    }

} catch (Exception $e) {
    _api_error(0, $e->getMessage());
}

?>