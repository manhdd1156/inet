<?php
/**
 * Package: ajax/ci/bo
 *
 * @package ConIu
 * @author QuanND
 */

include_once(DAO_PATH . 'dao_school.php');
include_once(DAO_PATH . 'dao_event.php');
include_once(DAO_PATH . 'dao_parent.php');
include_once(DAO_PATH . 'dao_teacher.php');
include_once(DAO_PATH . 'dao_user.php');
include_once(DAO_PATH . 'dao_class.php');
include_once(DAO_PATH . 'dao_child.php');

$schoolDao = new SchoolDAO();
$eventDao = new EventDAO();
$parentDao = new ParentDAO();
$teacherDao = new TeacherDAO();
$userDao = new UserDAO();
$classDao = new ClassDAO();
$childDao = new ChildDAO();

//Lấy ra thông tin lớp
//$class = $classDao->getClassByUsername($_POST['group_name'], $user->_data['user_id']);
$class = getClassDataByUsername($_POST['group_name'], CLASS_INFO);
if (is_null($class)) {
    _api_error(403);
}
// Lấy cấu hình thông báo của những user quản lý trường
//$notify_settings = $schoolDao->getUserNotificationSettingOfSchool($class['school_id']);
$notify_settings = getSchoolData($class['school_id'], SCHOOL_NOTIFICATIONS);

try {
    $return = array();
    $db->autocommit(false);
    // Tăng số lượt tương tác - TaiLa
    addInteractive($class['school_id'], 'event', 'school_view');
    switch ($_POST['do']) {
        case 'show_event':

            switch ($_POST['view']) {
                case 'all':
                    $page = (isset($_POST['page'])) ? $_POST['page'] : 0;
                    $events = $eventDao->getClassEventsForApi($class['group_id'], $class['class_level_id'], $class['school_id'], $page);

                    // return
                    return_json(array(
                        'code' => 200,
                        'message' => 'OK',
                        'data' => array(
                            'events' => $events
                        )
                    ));

                    break;

                case 'participant' :
                    // valid inputs
                    if(!isset($_POST['event_id']) || !is_numeric($_POST['event_id'])) {
                        _api_error(400);
                    }
                    $events = $eventDao->getClassEvents($class['group_id'], $class['class_level_id'], $class['school_id']);

                    $childCount = 0;
                    $parentCount = 0;
                    $participant = array();
                    foreach ($events as $event) {
                        if ($event['event_id'] == $_POST['event_id']) {
                            $participant = $eventDao->getParticipantsClassForApi($class['group_id'], $event['event_id'], $event['for_parent'], $childCount, $parentCount);
                        }
                    }

                    // return
                    return_json(array(
                        'code' => 200,
                        'message' => 'OK',
                        'data' => array(
                            'participant' => $participant
                        )
                    ));
                    break;

                case 'detail':
                    // valid inputs
                    if(!isset($_POST['event_id']) || !is_numeric($_POST['event_id'])) {
                        _error(400);
                    }
                    $event = $eventDao->getEventDetailForApi($_POST['event_id']);
                    // Tăng số lượt tương tác - TaiLa
                    addInteractive($class['school_id'], 'event', 'school_view', $_POST['event_id']);

                    return_json(array(
                        'code' => 200,
                        'message' => 'OK',
                        'data' => array(
                            'event' => $event
                        )
                    ));
                    break;
            }
            break;

        case 'edit':
            // valid inputs
            if(!isset($_POST['event_id']) || !is_numeric($_POST['event_id'])) {
                _api_error(400);
            }
            $db->begin_transaction();

            $args = array();
            $args['event_id'] = $_POST['event_id'];

            $args['event_name'] = $_POST['event_name'];
//            $args['location'] = $_POST['location'];
//            $args['price'] = (isset($_POST['price'])? $_POST['price']: 0);
//            $args['begin'] = $_POST['begin'];
//            $args['end'] = $_POST['end'];
            $args['level'] = CLASS_LEVEL;
            $args['description'] = $_POST['description'];
            $args['post_on_wall'] = (isset($_POST['post_on_wall']) && $_POST['post_on_wall'] == 'on')? 1: 0;
            $args['is_notified'] = (isset($_POST['notify_immediately']) && $_POST['notify_immediately'] == 'on')? 1: 0;
            $args['school_id'] = $class['school_id'];
            $args['class_id'] = $class['group_id'];
            $args['must_register'] = (isset($_POST['must_register']) && $_POST['must_register'] == 'on')? 1: 0;
            //Nếu không điền thông tin deadline thì hạn đăng ký là ngày bắt đâu sự kiện
//            if (($args['must_register'] == 1) && (!isset($_POST['registration_deadline']) || ($_POST['registration_deadline'] == ''))) {
//                $args['registration_deadline'] = $args['begin'];
//            } else {
                $args['registration_deadline'] = $_POST['registration_deadline'];
//            }
            $args['for_parent'] = (isset($_POST['for_parent']) && $_POST['for_parent'] == 'on' && (!$args['for_teacher']))? 1: 0;
            $args['for_child'] = (isset($_POST['for_child']) && $_POST['for_child'] == 'on' && (!$args['for_teacher']))? 1: 0;

            $event = $eventDao->getClassEventDetailApi($args['event_id']);
            if (is_null($event) || $event['level'] != CLASS_LEVEL
                || $args['school_id'] != $event['school_id'] || $args['class_id'] != $event['class_id']
                || !$event['can_edit'] || ($event['happened'] && $event['is_notified'])) {
                _api_error(0, __("You can not edit this event"));
            }

            //1. Cập nhật thông tin event
            $eventDao->updateEvent($args);
            $userDao->deletePosts($event['post_ids']);

            //2. Thông báo các đối tượng liên quan với trường hợp user click vào Save and Notify
            // - Tất cả giáo viên của lớp đều nhận được
            // - Tất cả phụ huynh của lớp đều nhận được.
            if ($args['is_notified']) {
                //$teacherIds = $teacherDao->getTeacherIDOfClass($args['class_id']);
                $teachers = getClassData($args['class_id'], CLASS_TEACHERS);
                $teacherIds = array_keys($teachers);
                $userDao->postNotifications($teacherIds, NOTIFICATION_NEW_EVENT_CLASS, NOTIFICATION_NODE_TYPE_CLASS, $args['event_id'], $args['event_name']);
                $parentIds = $parentDao->getParentIdOfClass($args['class_id']);
                $userDao->postNotifications($parentIds, NOTIFICATION_NEW_EVENT_CLASS, NOTIFICATION_NODE_TYPE_CHILD, $args['event_id'], $args['event_name']);

                //3.Post thông tin sự kiên lên tường của lớp. Chỉ có thông báo luôn thì mới đưa lên tường
                if ($args['post_on_wall']) {
                    $content = $args['event_name']."\n\n".$args['description'];
                    $postId = $userDao->postOnGroup($class, $content, $class['school_id']);
                    $eventDao->updatePostIds($postId, $args['event_id']);
                }
            }

            $db->commit();
            // return
            return_json(array(
                'code' => 200,
                'message' => __("Done, Event info have been updated"),
                'data' => array()
            ));
            break;

        case 'add':
            //$schoolConfig = $schoolDao->getConfiguration($class['school_id']);
            $schoolConfig = getSchoolData($class['school_id'], SCHOOL_CONFIG);
            if (!$schoolConfig['allow_class_event']) {
                _api_error(0,  __("You have no permission to do this"));
            }

            $db->begin_transaction();

            $args = array();
            $args['event_name'] = $_POST['event_name'];
            $args['location'] = $_POST['location'];
            $args['price'] = (isset($_POST['price'])? $_POST['price']: 0);
            $args['begin'] = $_POST['begin'];
            $args['end'] = $_POST['end'];
            $args['level'] = CLASS_LEVEL;
            $args['description'] = $_POST['description'];
            $args['post_on_wall'] = (isset($_POST['post_on_wall']) && $_POST['post_on_wall'] == 'on')? 1: 0;
            $args['is_notified'] = (isset($_POST['notify_immediately']) && $_POST['notify_immediately'] == 'on')? 1: 0;
            $args['school_id'] = $class['school_id'];
            $args['class_id'] = $class['group_id'];
            $args['created_user_id'] = $user->_data['user_id'];

            $args['must_register'] = (isset($_POST['must_register']) && $_POST['must_register'] == 'on')? 1: 0;
            //Nếu không điền thông tin deadline thì hạn đăng ký là ngày bắt đâu sự kiện
            if (($args['must_register'] == 1) && (!isset($_POST['registration_deadline']) || ($_POST['registration_deadline'] == ''))) {
                $args['registration_deadline'] = $args['begin'];
            } else {
                $args['registration_deadline'] = $_POST['registration_deadline'];
            }
            $args['for_parent'] = (isset($_POST['for_parent']) && $_POST['for_parent'] == 'on')? 1: 0;
            $args['for_child'] = (isset($_POST['for_child']) && $_POST['for_child'] == 'on')? 1: 0;
            //1. Đưa thông báo vào hệ thống
            $eventId = $eventDao->insertEvent($args);

            //2. Thông báo các đối tượng liên quan với trường hợp user click vào Save and Notify
            // - Quản lý trường nhận được
            // - Tất cả giáo viên của lớp đều nhận được
            // - Tất cả phụ huynh của lớp đều nhận được.
            if ($args['is_notified']) {
                //$teacherIds = $teacherDao->getTeacherIDOfClass($args['class_id']);
                $teachers = getClassData($args['class_id'], CLASS_TEACHERS);
                $teacherIds = array_keys($teachers);
                //$schoolManId = $schoolDao->getAdminId($args['school_id']);
                $schoolInfo = getSchoolData($args['school_id'], SCHOOL_INFO);
                $schoolManId = isset($schoolInfo['page_admin']) ? $schoolInfo['page_admin'] : 0;
                $teacherIds[] = $schoolManId;

                $userDao->postNotifications($teacherIds, NOTIFICATION_NEW_EVENT_CLASS, NOTIFICATION_NODE_TYPE_CLASS, $eventId, $args['event_name']);
                $parentIds = $parentDao->getParentIdOfClass($args['class_id']);
                $userDao->postNotifications($parentIds, NOTIFICATION_NEW_EVENT_CLASS, NOTIFICATION_NODE_TYPE_CHILD, $eventId, $args['event_name']);

                //3.Post thông tin sự kiên lên tường của lớp. Chỉ có thông báo luôn thì mới đưa lên tường
                if ($args['post_on_wall']) {
                    $content = $args['event_name']."\n\n".$args['description'];
                    $postId = $userDao->postOnGroup($class, $content, $class['school_id']);
                    $eventDao->updatePostIds($postId, $eventId);
                }

                /* Coniu - Tương tác trường */
                $eventIds = array();
                $eventIds[] = $eventId;
                setIsAddNew($class['school_id'], 'event_created', $eventIds);
                /* Coniu - END */
            }

            // Tăng lượt thêm mới
            addInteractive($class['school_id'], 'event', 'school_view', $eventId, 1);

            $db->commit();
            // return
            return_json(array(
                'code' => 200,
                'message' => __("Done, Event info have been created"),
                'data' => array()
            ));
            break;

        case 'notify':
            $db->begin_transaction();
            //Lấy ra thông tin thông báo
            $event = $eventDao->getClassEventDetailApi($_POST['event_id']);
            if (is_null($event) || $event['level'] != CLASS_LEVEL
                || $class['school_id'] != $event['school_id'] || $class['group_id'] != $event['class_id']
                || $event['is_notified'] || $event['happened']) {
                _api_error(0, __("You can not edit this event"));
            }

            //2. Thông báo các đối tượng liên quan với trường hợp user click vào Save and Notify
            // - Tất cả giáo viên của lớp đều nhận được
            // - Tất cả phụ huynh của lớp đều nhận được.
            if (!$event['is_notified']) {
                //$teacherIds = $teacherDao->getTeacherIDOfClass($event['class_id']);
                $teachers = getClassData($event['class_id'], CLASS_TEACHERS);
                $teacherIds = array_keys($teachers);
                //$schoolManId = $schoolDao->getAdminId($event['school_id']);
                $schoolInfo = getSchoolData($event['school_id'], SCHOOL_INFO);
                $schoolManId = isset($schoolInfo['page_admin']) ? $schoolInfo['page_admin'] : 0;
                $teacherIds[] = $schoolManId;
                $userDao->postNotifications($teacherIds, NOTIFICATION_NEW_EVENT_CLASS, NOTIFICATION_NODE_TYPE_CLASS, $event['event_id'], convertText4Web($event['event_name']));

                $parentIds = $parentDao->getParentIdOfClass($event['class_id']);
                $userDao->postNotifications($parentIds, NOTIFICATION_NEW_EVENT_CLASS, NOTIFICATION_NODE_TYPE_CHILD, $event['event_id'], convertText4Web($event['event_name']));

                //3.Post thông tin sự kiện lên tường của lớp. Chỉ có thông báo luôn thì mới đưa lên tường
                if ($event['post_on_wall']) {
                    $content = convertText4Web($event['event_name']."\n\n".$event['description']);
                    $postId = $userDao->postOnGroup($class, $content, $class['school_id']);
                    $eventDao->updatePostIds($postId, $event['event_id']);
                }

                /* Coniu - Tương tác trường */
                $eventIds = array();
                $eventIds[] = $event['event_id'];
                setIsAddNew($class['school_id'], 'event_created', $eventIds);
                /* Coniu - END */
            }
            // Cập nhật trạng thái đã gửi thông báo sự kiện.
            $eventDao->updateStatusToNotified($event['event_id']);

            $db->commit();
            // return
            return_json(array(
                'code' => 200,
                'message' => __("Sự kiện đã được thông báo"),
                'data' => array()
            ));
            break;

        case 'delete':
            if(!isset($_POST['event_id']) || !is_numeric($_POST['event_id'])) {
                _api_error(400);
            }
            $db->begin_transaction();

            $event = $eventDao->getClassEventDetailApi($_POST['event_id']);
            if (is_null($event) || $event['level'] != CLASS_LEVEL
                || $class['school_id'] != $event['school_id'] || $class['group_id'] != $event['class_id']
                || !$event['can_edit'] ) {
                _api_error(0, __("You can not delete this event"));
            }
            $eventDao->deleteClassEvent($_POST['event_id']);
            //Xóa tất cả bài post liên quan đến sự kiện
            $userDao->deletePosts($event['post_ids']);

            $db->commit();
            // return
            return_json(array(
                'code' => 200,
                'message' => __("Done, Event info have been deleted"),
                'data' => null
            ));
            break;

        case 'add_pp':
            //Cập nhật danh sách người tham gia sự kiện
            $childIds = isset($_REQUEST['child_id']) ? $_REQUEST['child_id'] : array();
            $parentIds = isset($_REQUEST['parent_id']) ? $_REQUEST['parent_id'] : array();

            $db->begin_transaction();
            $event = $eventDao->getEvent($_POST['event_id']);
            if (is_null($event))
                _api_error(0, __("You can not edit this event"));

            if (!$event['can_register']
                || ($event['happened'] && $event['is_notified'])) {
                _api_error(0, __("You can not edit this event"));
            }

            $participants = $eventDao->getParticipantsIdOfClass($class['group_id'], $event['event_id'], $event['for_parent']);

            $oldChildIds = isset($participants['childs']) ? array_unique($participants['childs']) : array();
            $oldParentIds = isset($participants['parents']) ? array_unique($participants['parents']) : array();

            //Lấy sẳn các thông tin liên quan để thông báo.
            //$school = $schoolDao->getConfiguration($class['school_id']);
            $school = getSchoolData($class['school_id'], SCHOOL_CONFIG);
            //$teacherIds = $teacherDao->getTeacherIDOfClass($class['group_id']);
            $teachers = getClassData($class['group_id'], CLASS_TEACHERS);
            $teacherIds = array_keys($teachers);
            //$children = $childDao->getChildrenOfClassInSession($class['group_id']);
            //$children = getClassData($class['group_id'], CLASS_CHILDREN);
            $children = array();
            $childs = getClassData($class['group_id'], CLASS_CHILDREN);
            foreach ($childs as $child) {
                //$parents = $parentDao->getParent($child['child_id']);
                $child['parent'] = getChildData($child['child_id'], CHILD_PARENTS);
                $children[] = $child;
            }
            $tmpChild = null;

            //Nếu sự kiện trẻ có thể tham gia
            if ($event['for_child']) {
                //Xóa đối tượng đã bị loại bỏ
                $deletedChildIds = array_diff($oldChildIds, $childIds);
                $eventDao->deleteParticipants($_POST['event_id'], PARTICIPANT_TYPE_CHILD, $deletedChildIds);

                //Phần thông báo đến các đối tượng liên quan
                foreach ($deletedChildIds as $id) {
                    foreach ($children as $child) {
                        if ($child['child_id'] == $id) {
                            $tmpChild = $child;
                            break;
                        }
                    }
                    //Thông báo cho quản lý trường
                    // Lấy id của những user quản lý được nhận thông báo
                    // Tắt thông báo đến quản lý trường và giáo viên khác trong lớp, chỉ thông báo cho phụ huynh
//                    $userIds = getUserIdsReceiveNotifyOfSchoolForClass($notify_settings, 'events', $class['school_id'],  $school['page_admin']);
//
//                    $userDao->postNotifications($userIds, NOTIFICATION_CANCEL_REGISTRATION_EVENT_CHILD, NOTIFICATION_NODE_TYPE_SCHOOL,
//                        $_POST['event_id'], convertText4Web($event['event_name']), $school['page_name'], convertText4Web($tmpChild['child_name']));
//
//                    //Thông báo những giáo viên khác trong lớp
//                    $userDao->postNotifications($teacherIds, NOTIFICATION_CANCEL_REGISTRATION_EVENT_CHILD, NOTIFICATION_NODE_TYPE_CLASS, $_POST['event_id'],
//                        convertText4Web($event['event_name']), $_POST['group_name'], convertText4Web($tmpChild['child_name']));
                    //Thông báo cho phụ huynh từng trẻ.
                    foreach ($tmpChild['parent'] as $parent) {
                        $userDao->postNotifications($parent['user_id'], NOTIFICATION_CANCEL_REGISTRATION_EVENT_CHILD, NOTIFICATION_NODE_TYPE_CHILD, $_POST['event_id'],
                            convertText4Web($event['event_name']), $id, convertText4Web($tmpChild['child_name']));
                    }
                }

                //Thêm những đối tượng mới danh sách đăng ký
                $newChildIds = array_diff($childIds, $oldChildIds);
                $eventDao->insertParticipants($_POST['event_id'], PARTICIPANT_TYPE_CHILD, $newChildIds);

                //Thông báo tới các đối tượng liên quan
                foreach ($newChildIds as $id) {
                    foreach ($children as $child) {
                        if ($child['child_id'] == $id) {
                            $tmpChild = $child;
                            break;
                        }
                    }
                    //Thông báo cho quản lý trường
                    // Lấy id của những user quản lý được nhận thông báo
                    // Tắt thông báo cho quản lý trường và giáo viên khác, chỉ thông báo cho phụ huynh
//                    $userIds = getUserIdsReceiveNotifyOfSchoolForClass($notify_settings, 'events', $class['school_id'], $school['page_admin']);
//
//                    $userDao->postNotifications($userIds, NOTIFICATION_REGISTER_EVENT_CHILD, NOTIFICATION_NODE_TYPE_SCHOOL,
//                        $_POST['event_id'], convertText4Web($event['event_name']), $school['page_name'], convertText4Web($tmpChild['child_name']));
//
//                    //Thông báo những giáo viên khác trong lớp
//                    $userDao->postNotifications($teacherIds, NOTIFICATION_REGISTER_EVENT_CHILD, NOTIFICATION_NODE_TYPE_CLASS, $_POST['event_id'],
//                        convertText4Web($event['event_name']), $_POST['group_name'], convertText4Web($tmpChild['child_name']));
                    //Thông báo cho phụ huynh từng trẻ.
                    foreach ($tmpChild['parent'] as $parent) {
                        $userDao->postNotifications($parent['user_id'], NOTIFICATION_REGISTER_EVENT_CHILD, NOTIFICATION_NODE_TYPE_CHILD, $_POST['event_id'],
                            convertText4Web($event['event_name']), $id, convertText4Web($tmpChild['child_name']));
                    }
                }
            }

            //Nếu sự kiện mà phụ huynh có thể tham gia
            if ($event['for_parent']) {
                //Xóa đối tượng đã bị loại bỏ
                $deletedParentIds = array_diff($oldParentIds, $parentIds);
                $deletedParentIds = array_unique($deletedParentIds);
                $eventDao->deleteParticipants($_POST['event_id'], PARTICIPANT_TYPE_PARENT, $deletedParentIds);

                $tmpParent = null;
                //Thông báo
                foreach ($deletedParentIds as $id) {
                    $found = false;
                    foreach ($children as $child) { //Tìm ra trẻ và phụ huynh có ID tương ứng.
                        foreach ($child['parent'] as $parent) {
                            if ($parent['user_id'] == $id) {
                                $found = true;
                                $tmpParent = $parent;
                                break;
                            }
                        }
                        if ($found) {
                            $tmpChild = $child;
                            break;
                        }
                    }

                    //Thông báo cho quản lý trường
                    // Lấy id của những user quản lý được nhận thông báo
                    // Tắt thông báo cho quản lý trường và giáo viên khác
//                    $userIds = getUserIdsReceiveNotifyOfSchoolForClass($notify_settings, 'events', $class['school_id'], $school['page_admin']);
//
//                    $userDao->postNotifications($userIds, NOTIFICATION_CANCEL_REGISTRATION_EVENT_PARENT, NOTIFICATION_NODE_TYPE_SCHOOL,
//                        $_POST['event_id'], convertText4Web($event['event_name']), $school['page_name'], convertText4Web($tmpParent['user_fullname']));
//
//                    //Thông báo những giáo viên khác trong lớp
//                    $userDao->postNotifications($teacherIds, NOTIFICATION_CANCEL_REGISTRATION_EVENT_PARENT, NOTIFICATION_NODE_TYPE_CLASS, $_POST['event_id'],
//                        convertText4Web($event['event_name']), $_POST['group_name'], convertText4Web($tmpParent['user_fullname']));
                    //Thông báo cho phụ huynh.
                    $userDao->postNotifications($id, NOTIFICATION_CANCEL_REGISTRATION_EVENT_PARENT, NOTIFICATION_NODE_TYPE_CHILD, $_POST['event_id'],
                        convertText4Web($event['event_name']), $tmpChild['child_id'], __("you"));
                }


                //Thêm những đối tượng mới danh sách đăng ký
                $newParentIds = array_diff($parentIds, $oldParentIds);
                $newParentIds = array_unique($newParentIds);
                $eventDao->insertParticipants($_POST['event_id'], PARTICIPANT_TYPE_PARENT, $newParentIds);

                //Thông báo
                foreach ($newParentIds as $id) {
                    $found = false;
                    foreach ($children as $child) { //Tìm ra trẻ và phụ huynh có ID tương ứng.
                        foreach ($child['parent'] as $parent) {
                            if ($parent['user_id'] == $id) {
                                $found = true;
                                $tmpParent = $parent;
                                break;
                            }
                        }
                        if ($found) {
                            $tmpChild = $child;
                            break;
                        }
                    }

                    //Thông báo cho quản lý trường
                    // Lấy id của những user quản lý được nhận thông báo
                    // Tắt thông báo cho quản ký trường và giáo viên khác
//                    $userIds = getUserIdsReceiveNotifyOfSchoolForClass($notify_settings, 'events', $class['school_id'], $school['page_admin']);
//
//                    $userDao->postNotifications($userIds, NOTIFICATION_REGISTER_EVENT_PARENT, NOTIFICATION_NODE_TYPE_SCHOOL,
//                        $_POST['event_id'], convertText4Web($event['event_name']), $school['page_name'], convertText4Web($tmpParent['user_fullname']));
//
//                    //Thông báo những giáo viên khác trong lớp
//                    $userDao->postNotifications($teacherIds, NOTIFICATION_REGISTER_EVENT_PARENT, NOTIFICATION_NODE_TYPE_CLASS, $_POST['event_id'],
//                        convertText4Web($event['event_name']), $_POST['group_name'], convertText4Web($tmpParent['user_fullname']));
                    //Thông báo cho phụ huynh.
                    $userDao->postNotifications($id, NOTIFICATION_REGISTER_EVENT_PARENT, NOTIFICATION_NODE_TYPE_CHILD, $_POST['event_id'],
                        convertText4Web($event['event_name']), $tmpChild['child_id'], __("you"));
                }
            }
            $db->commit();

            // return
            return_json(array(
                'code' => 200,
                'message' => __("Participants have been updated"),
                'data' =>
                    array(
                        'old' => $participants
                    )
            ));
            break;

        case 'reject':
            $db->begin_transaction();
            $event = $eventDao->getEvent($_POST['event_id']);
            if (is_null($event))
                _api_error(0, __("The event does not exist, it should be deleted."));

            if ($event['begin'] < $date) {
                $event['happened'] = 1; //sự kiện đã qua rồi hay chưa
            } else {
                $event['happened'] = 0;
            }
            if ($event['level'] != CLASS_LEVEL
                || $class['school_id'] != $event['school_id'] || $class['group_id'] != $event['class_id']
                || ($event['created_user_id'] != $user->_data['user_id']) || ($event['happened'] == 1 && $event['is_notified'] == 1)) {
                _api_error(0, __("You can not edit this event"));
            }

            $eventDao->deleteParticipants($_POST['event_id'], $_POST['type'], [$_POST['pp_id']]);

            //$classTeacherIds = $teacherDao->getTeacherIDOfClass($class['group_id']);
            $teachers = getClassData($class['group_id'], CLASS_TEACHERS);
            $classTeacherIds = array_keys($teachers);
            if ($_POST['type'] == PARTICIPANT_TYPE_PARENT) {
                //Thông báo cho phụ huynh
                $activeParentIds = $parentDao->getParentIdOfClass($class['group_id']);
                //Thông báo cho phụ huynh
                if (in_array($_POST['pp_id'], $activeParentIds)) {
                    $userDao->postNotifications($_POST['pp_id'], NOTIFICATION_CANCEL_REGISTRATION_EVENT_PARENT, NOTIFICATION_NODE_TYPE_CHILD, $_POST['event_id'], $_POST['event_name'], $_POST['child_id'], __("you"));

                }
                //Thông báo cho giáo viên khác của lớp
                $userDao->postNotifications($classTeacherIds, NOTIFICATION_CANCEL_REGISTRATION_EVENT_PARENT, NOTIFICATION_NODE_TYPE_CLASS, $_POST['event_id'], $_POST['event_name'], $_POST['group_name'], $_POST['name']);
            } else if ($_POST['type'] == PARTICIPANT_TYPE_CHILD) {
                $parents = $childDao->getListChildNParentId([$_POST['pp_id']]);
                foreach ($parents as $parent) {
                    $userDao->postNotifications($parent['parent_id'], NOTIFICATION_CANCEL_REGISTRATION_EVENT_CHILD, NOTIFICATION_NODE_TYPE_CHILD, $_POST['event_id'], $_POST['event_name'], $parent['child_id'], $_POST['name']);
                }
                //Thông báo tới giáo viên của lớp.
                $userDao->postNotifications($classTeacherIds, NOTIFICATION_CANCEL_REGISTRATION_EVENT_CHILD, NOTIFICATION_NODE_TYPE_CLASS, $_POST['event_id'], $_POST['event_name'], $_POST['group_name'], $_POST['name']);
            }

            $db->commit();
            // return
            return_json(array(
                'code' => 200,
                'message' => __("Event have been reject"),
                'data' => array()
            ));
            break;

        default:
            _api_error(400);
            break;
    }

} catch (Exception $e) {
    $db->rollback();
    header('HTTP/1.1 400 API Error');
    return_json(array(
        'code' => 0,
        'message' => $e->getMessage(),
        'data' => array()
    ));
} finally {
    $db->autocommit(true);
}

?>