<?php
/**
 * ajax -> data -> search
 * 
 * @package CINet v1.1
 * @author NOGA Co., Ltd
 */

// valid inputs
if(!isset($_POST['query']) && ($_POST['type'] != 'searchchild')) {
	_error(400);
}

// search
try {

	// initialize the return array
	$return = array();
    switch ($_POST['type']) {
        case 'user':
            include_once(DAO_PATH.'dao_user.php');
            $userDao = new UserDAO();

            // Danh sách user đã chọn thì ko chọn nữa
            $userIds = $_POST['user_ids'];
            $userIds[] = $user->_data['user_id']; //Tính cả user hiện tại cũng ko được chọn

            // get results
            $results = $userDao->searchUser($_POST['query'], $userIds);
            if (count($results) > 0) {
                /* assign variables */
                $smarty->assign('results', $results);
                /* return */
                $return['results'] = $smarty->fetch("ci/ajax.search4teacher.tpl");
            }
            break;

        /* CI - mobile tìm kiếm phụ huynh trong màn hình edit trẻ */
        case 'parent':
            include_once(DAO_PATH.'dao_user.php');
            $userDao = new UserDAO();

            // Danh sách user đã chọn thì ko chọn nữa
            $userIds = $_REQUEST['parent_id'];
            $userIds = array_unique($userIds);
            $userIds[] = $user->_data['user_id']; //Tính cả user hiện tại cũng ko được chọn

            // get results
            $results = $userDao->searchUser($_POST['query'], $userIds);

                // return
                return_json(array(
                    'code' => 200,
                    'message' => __("OK"),
                    'data' => $results
                ));

            break;

        case 'teacher':
            include_once(DAO_PATH.'dao_teacher.php');
            $teacherDao = new TeacherDAO();

            // Danh sách user đã chọn thì ko chọn nữa
            $teacherIds = $_POST['user_ids'];

            // Loại bỏ những user_ids = null
//            $teacherIds = array_diff($teacherIds, array('null',''));

            $teacherIds[] = $user->_data['user_id']; //Tính cả user hiện tại cũng ko được chọn

            $results = $teacherDao->searchTeacher($_POST['school_username'], $_POST['query'], $teacherIds);
            if (count($results) > 0) {
                /* assign variables */
                $smarty->assign('results', $results);
                /* return */
                $return['results'] = $smarty->fetch("ci/school/ajax.searchteacher.tpl");
            }
            break;
        case 'searchchild':
            include_once(DAO_PATH.'dao_child.php');
            $childDao = new ChildDAO();

            $result = $childDao->search($_POST['keyword'], $_POST['school_id'], $_POST['class_id']);
            $smarty->assign('username', $_POST['school_username']);
            $smarty->assign('result', $result);

            $return['results'] = $smarty->fetch("ci/school/ajax.school.children.list.tpl");

            break;
    }
	// return & exit
	return_json($return);

} catch (Exception $e) {
    header('HTTP/1.1 400 API Error');
    return_json(array(
        'code' => 0,
        'message' => $e->getMessage(),
        'data' => null
    ));
}

?>