<?php
/**
 * Created by PhpStorm.
 * User: Tuan Anh
 * Date: 01/23/2017
 * Time: 12:22 AM
 */
    include_once(DAO_PATH.'dao_contact.php');
    include_once(DAO_PATH.'dao_mail.php');

    $contactDao = new ContactDAO();
    $mailDao = new MailDAO();

    try {
        $return = array();
        $db->autocommit(false);
        $db->begin_transaction();

        $args = array();
        $args['name'] = convertText4Web($user->_data['user_firstname'] . " " . $user->_data['user_lastname']);
        $args['email'] = $user->_data['user_email'];
        $args['phone'] = $user->_data['user_phone'];

        $args['select_school'] = $_POST['select_contact'];
        $args['content'] = $_POST['content'];
        $args['contact_ip'] = $_SERVER['REMOTE_ADDR'];

        // Lưu thông tin vào hệ thống
        $contactDao->saveContact($args);

        // lấy giá trị $body để gửi mail
        $content = "";
        $content .= 'Họ tên: ' . $args['name'] . "\n";
        $content .= 'Email: ' . $args['email'] . "\n";
        $content .= 'Điện thoại: ' . $args['phone'] . "\n";
        $content .= 'Ý kiến đóng góp: ' . $args['select_school'] . "\n";
        $content .= 'Nội dung: ' . $args['content'] . "\n";

        // đặt subject
        $subject = $system['system_title'];
        $argEmail = array();
        $argEmail['action'] = "send_contact";
        $argEmail['receivers'] = $receive_email;
        $argEmail['subject'] = $subject;

        //Tạo nên nội dung email
        $argEmail['content'] = $content;

        $argEmail['delete_after_sending'] = 1;
        $argEmail['school_id'] = 0;
        $argEmail['user_id'] = $user->_data['user_id'];
        $mailDao->insertEmail($argEmail);

        $db->commit();

        return_json(array(
            'code' => 200,
            'message' => __("Thanks for Your Help"),
            'data' => null,
        ));
    }
    catch (Exception $e) {
        $db->rollback();
        _api_error(0, $e->getMessage());
    } finally {
        $db->autocommit(true);
    }
?>