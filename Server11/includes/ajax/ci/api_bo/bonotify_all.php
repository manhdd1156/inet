<?php
/**
 * Package: ajax/ci/bo
 * 
 * @package ConIu
 * @author QuanND
 */

include_once(DAO_PATH.'dao_class.php');
include_once(DAO_PATH.'dao_child.php');
include_once(DAO_PATH.'dao_event.php');
include_once(DAO_PATH.'dao_medicine.php');
include_once(DAO_PATH.'dao_tuition.php');
include_once(DAO_PATH.'dao_user.php');
include_once(DAO_PATH.'dao_attendance.php');
include_once(DAO_PATH.'dao_teacher.php');
include_once(DAO_PATH.'dao_school.php');
include_once(DAO_PATH.'dao_report.php');
include_once(DAO_PATH.'dao_class_level.php');
include_once(DAO_PATH.'dao_schedule.php');
include_once(DAO_PATH.'dao_menu.php');
include_once(DAO_PATH.'dao_pickup.php');
include_once(DAO_PATH.'dao_foetus_development.php');
include_once(DAO_PATH.'dao_child_development.php');

$classDao = new ClassDAO();
$childDao = new ChildDAO();
$eventDao = new EventDAO();
$medicineDao = new MedicineDAO();
$userDao = new UserDAO();
$attendanceDao = new AttendanceDAO();
$tuitionDao = new TuitionDAO();
$teacherDao = new TeacherDAO();
$schoolDao = new SchoolDAO();
$reportDao = new ReportDAO();
$classLevelDao = new ClassLevelDAO();
$scheduleDao = new ScheduleDAO();
$menuDao = new MenuDAO();
$pickupDao = new PickupDAO();
$foetusDevelopmentDao = new FoetusDevelopmentDAO();
$childDevelopmentDao = new ChildDevelopmentDAO();

try {
    $url = $system['system_url'];
    switch ($_POST['action']) {
        //Phân công giáo viên vào lớp - (Giáo viên)
        case NOTIFICATION_ASSIGN_CLASS:
            //Khi phân lớp cho giáo viên. node_url = class_id
            /*$class = $classDao->getClass($_POST['node_url']);
            if (is_null($class)) {
                _error("Error", "The class does not exist, it should be deleted.");
            }*/
            // return
            return_json(array(
                'code' => 200,
                'message' => 'OK',
                'data' => array()
            ));
            break;

        //Thêm mới lớp - (Quản lý trường)
        case NOTIFICATION_NEW_CLASS:
            return_json(array(
                'code' => 200,
                'message' =>  __("OK"),
                'data' => array()
            ));
            break;

        //Cập nhật thông tin lớp - (Quản lý trường)
        case NOTIFICATION_UPDATE_CLASS:
            return_json(array(
                'code' => 200,
                'message' =>  __("OK"),
                'data' => array()
            ));
            break;

        //Thêm mới khối lớp - (Quản lý trường)
        case NOTIFICATION_NEW_CLASSLEVEL:
            return_json(array(
                'code' => 200,
                'message' =>  __("OK"),
                'data' => array()
            ));
            break;

        //Cập nhật khối lớp - (Quản lý trường)
        case NOTIFICATION_UPDATE_CLASSLEVEL:
            return_json(array(
                'code' => 200,
                'message' =>  __("OK"),
                'data' => array()
            ));
            break;

        //Chưa có action
        case NOTIFICATION_NEW_CHILD:
            return_json(array(
                'code' => 200,
                'message' =>  __("OK"),
                'data' => array()
            ));
            break;

        //Giáo viên sửa thông tin trẻ - (Quản lý trường)
        case NOTIFICATION_UPDATE_CHILD_TEACHER:
            return_json(array(
                'code' => 200,
                'message' =>  __("OK"),
                'data' => array()
            ));
            break;

        //Trường xác nhận thông tin trẻ giáo viên sửa - (Giáo viên)
        case NOTIFICATION_CONFIRM_CHILD_TEACHER:
            return_json(array(
                'code' => 200,
                'message' =>  __("OK"),
                'data' => array()
            ));
            break;

        //Trường hủy xác nhận thông tin trẻ giáo viên sửa - (Giáo viên)
        case NOTIFICATION_UNCONFIRM_CHILD_TEACHER:
            return_json(array(
                'code' => 200,
                'message' =>  __("OK"),
                'data' => array()
            ));
            break;

        //Lớp có giáo viên mới - (Phụ huynh)
        case NOTIFICATION_NEW_TEACHER:
            if ($_POST['node_type'] == NOTIFICATION_NODE_TYPE_CHILD) {
                //Trường hợp lớp có thêm giáo viên mới, thông báo cho phụ huynh. node_url = class_id
                /*$childIds = $childDao->getChildIdByClassAndParent($_POST['node_url'], $user->_data['user_id']);
                if (count($childIds) == 0) {
                    _api_error(404);
                }*/
                $child = getChildDataById($_POST['extra2'], CHILD_INFO);
                if (is_null($child)) {
                    _api_error(404);
                }
                $class = getClassData($ntf['node_url'], CLASS_INFO);
                if (is_null($class)) {
                    _api_error(0, __("The class does not exist, it should be deleted."));
                }

                $class_info = array();
                $school_info = array();

                $class_info['group_id'] = $class['group_id'];
                $class_info['group_name'] = $class['group_name'];
                $class_info['group_title'] = $class['group_title'];
                $class_info['group_description'] = $class['group_description'];
                $class_info['school_id'] = $class['school_id'];

                //$teachers = $teacherDao->getTeacherOfClass($class['group_id']);
                $teachers = getClassData($class['group_id'], CLASS_TEACHERS);
                $teachers = sortArray($teachers, "ASC", "user_fullname");
                foreach ($teachers as $teacher) {
                    $result[] = $teacher;
                }
                //$school = $schoolDao->getSchoolByClass($class['group_id']);
                $school = getSchoolData($class['school_id'], SCHOOL_INFO);
                $school_info['page_id'] = $school['page_id'];
                $school_info['page_name'] = $school['page_name'];
                $school_info['page_title'] = $school['page_title'];
                $school_info['page_description'] = $school['page_description'];

                //$admin = $schoolDao->getAdmin($school['page_id']);
                //$school_info['admin'] = $admin;
                $school_info['admin'] = isset($school['page_admin']) ? $school['page_admin'] : 0;

                //$childrens = $childDao->getChildrenOfClass($class['group_id'], true);
                $children = array();
                $childs = getClassData($class['group_id'], CLASS_CHILDREN);
                $childs = sortArray($childs, "ASC", "name_for_sort");
                foreach ($childs as $child) {
                    $result['child_id'] = $child['child_id'];
                    $result['child_name'] = $child['child_name'];
                    $children[] = $result;
                }

                // return
                return_json(array(
                    'code' => 200,
                    'message' => 'OK',
                    'data' => array(
                        'school' => $school_info,
                        'class' => $class_info,
                        'teacher' => $teachers,
                        'child_list' => $children,
                    )
                ));
            }
            break;

        //Phụ huynh đăng ký tham gia sự kiện
        case NOTIFICATION_REGISTER_EVENT_CHILD:
        case NOTIFICATION_CANCEL_REGISTRATION_EVENT_CHILD:
        case NOTIFICATION_REGISTER_EVENT_PARENT:
        case NOTIFICATION_CANCEL_REGISTRATION_EVENT_PARENT:
            $event = array();
            if ($_POST['node_type'] == NOTIFICATION_NODE_TYPE_CHILD) {
                $event = $eventDao->getEvent($_POST['node_url']);
                if (is_null($event)) {
                    _api_error(0, __("The event does not exist, it should be deleted."));
                }

                //Phụ huynh chỉ có thể xem trong màn hình quản lý thông tin con
                $childIds = $childDao->getChildIdBySchoolAndParent($event['school_id'], $user->_data['user_id']);
                if (count($childIds) == 0) {
                    _api_error(404);
                }

                //Lấy ra thông tin cha mẹ và trẻ có tham gia không
                $pp = array();
                if ($event['must_register']) {
                    $child = array();
                    $child['pp_id'] = $childIds[0];
                    $child['is_registered'] = false;

                    $parent = array();
                    $parent['pp_id'] = $user->_data['user_id'];
                    $parent['is_registered'] = false;

                    $participants = $eventDao->_getChildPaticipants($event['event_id'], $childIds[0], $user->_data['user_id']);
                    foreach ($participants as $participant) {
                        if ($participant['pp_type'] == PARTICIPANT_TYPE_CHILD) {
                            $child['is_registered'] = true;
                            $child['is_paid'] = $participant['is_paid'];
                        } else {
                            $parent['is_registered'] = true;
                            $parent['is_paid'] = $participant['is_paid'];
                        }
                    }
                    if ($event['for_child']) {
                        $pp['child'] = $child;
                    }
                    if ($event['for_parent']) {
                        $pp['parent'] = $parent;
                    }
                }
                $event['participants'] = empty($pp) ? null : $pp;
            }
            // return
            return_json(array(
                'code' => 200,
                'message' => 'OK',
                'data' => array(
                    'event_child' => $event
                )
            ));
            break;

        case NOTIFICATION_NEW_EVENT_SCHOOL:
        case NOTIFICATION_UPDATE_EVENT_SCHOOL:
            //Sự kiện cấp trường. node_url = event_id

            if ($_POST['node_type'] == NOTIFICATION_NODE_TYPE_CLASS) {
                $event = $eventDao->getClassEventDetailApi($_POST['node_url']);
                if (is_null($event)) {
                    _api_error(0, __("The event does not exist, it should be deleted."));
                }
                //Giáo viên chỉ có thể xem sự kiện trong màn hình lớp
                $classes = $classDao->getClassesBySchoolAndTeacher($event['school_id'], $user->_data['user_id']);
                if (count($classes) == 0) {
                    _api_error(404);
                }

                $childCount = 0;
                $parentCount = 0;
                $results = array();

                $event['participant'] = $eventDao->getParticipantsOfClass($classes[0]['group_id'], $event['event_id'], $event['for_parent'], $childCount, $parentCount);

                // return
                return_json(array(
                    'code' => 200,
                    'message' => 'OK',
                    'data' => array(
                        'group_name' => $classes[0]['group_name'],
                        'event_class' => $event
                    )
                ));
            } elseif ($_POST['node_type'] == NOTIFICATION_NODE_TYPE_CHILD) {
                $event = $eventDao->getEvent($_POST['node_url']);
                if (is_null($event)) {
                    _api_error(0, __("The event does not exist, it should be deleted."));
                }

                //Phụ huynh chỉ có thể xem trong màn hình quản lý thông tin con
                $childIds = $childDao->getChildIdBySchoolAndParent($event['school_id'], $user->_data['user_id']);
                if (count($childIds) == 0) {
                    _api_error(404);
                }

                //Lấy ra thông tin cha mẹ và trẻ có tham gia không
                $pp = array();
                if ($event['must_register']) {
                    $child = array();
                    $child['pp_id'] = $childIds[0];
                    $child['is_registered'] = false;

                    $parent = array();
                    $parent['pp_id'] = $user->_data['user_id'];
                    $parent['is_registered'] = false;

                    $participants = $eventDao->_getChildPaticipants($event['event_id'], $childIds[0], $user->_data['user_id']);
                    foreach ($participants as $participant) {
                        if ($participant['pp_type'] == PARTICIPANT_TYPE_CHILD) {
                            $child['is_registered'] = true;
                            $child['is_paid'] = $participant['is_paid'];
                        } else {
                            $parent['is_registered'] = true;
                            $parent['is_paid'] = $participant['is_paid'];
                        }
                    }
                    if ($event['for_child']) {
                        $pp['child'] = $child;
                    }
                    if ($event['for_parent']) {
                        $pp['parent'] = $parent;
                    }
                }
                $event['participants'] = empty($pp) ? null : $pp;

                // return
                return_json(array(
                    'code' => 200,
                    'message' => 'OK',
                    'data' => array(
                        'child_id' => $childIds[0],
                        'event_child' => $event
                    )
                ));
            } else {
                // Thông báo tài khoản trường
                $event = $eventDao->getEvent($_POST['node_url']);
                if (is_null($event)) {
                    _api_error(0, __("The event does not exist, it should be deleted."));
                }

                // Nhân viên chỉ có thể xem thông báo ở màn hình trường
//                $school = $schoolDao->getSchoolById($event['school_id']);
//                if (count($school) == 0) {
//                    _api_error(404);
//                }

                //Lấy ra thông tin user có tham gia không
                $pp = array();
                if ($event['must_register']) {

                    $teacher = array();
                    $teacher['pp_id'] = $user->_data['user_id'];
                    $teacher['is_registered'] = false;

                    $participants = $eventDao->_getTeacherPaticipants($event['event_id'], $user->_data['user_id']);
                    foreach ($participants as $participant) {
                        if ($participant['pp_type'] == PARTICIPANT_TYPE_TEACHER) {
                            $teacher['is_registered'] = true;
                            $teacher['is_paid'] = $participant['is_paid'];
                        }
                    }
                    if ($event['for_teacher']) {
                        $event['is_registered'] = $teacher['is_registered'];
                    }
                }

                // return
                return_json(array(
                    'code' => 200,
                    'message' => 'OK',
                    'data' => array(
                        'page_name' => $_POST['extra2'],
                        'event_school' => $event
                    )
                ));
            }
            break;
        case NOTIFICATION_NEW_EVENT_CLASS_LEVEL:
        case NOTIFICATION_UPDATE_EVENT_CLASS_LEVEL:
            
            if ($_POST['node_type'] == NOTIFICATION_NODE_TYPE_CLASS) {
                $event = $eventDao->getClassEventDetailApi($_POST['node_url']);
                if (is_null($event)) {
                    _api_error(0, "The event does not exist, it should be deleted.");
                }
                
                $classes = $classDao->getClassesByClassLevelAndTeacher($event['class_level_id'], $user->_data['user_id']);
                if (count($classes) == 0) {
                    _api_error(404);
                }

                $childCount = 0;
                $parentCount = 0;
                $results = array();

                $event['participant'] = $eventDao->getParticipantsOfClass($classes[0]['group_id'], $event['event_id'], $event['for_parent'], $childCount, $parentCount);
                // return
                return_json(array(
                    'code' => 200,
                    'message' => 'OK',
                    'data' => array(
                        'group_name' => $classes[0]['group_name'],
                        'event_class' => $event
                    )
                ));
            } elseif ($_POST['node_type'] == NOTIFICATION_NODE_TYPE_CHILD) {
                $event = $eventDao->getEvent($_POST['node_url']);
                if (is_null($event)) {
                    _api_error(0, __("The event does not exist, it should be deleted."));
                }

                $childIds = $childDao->getChildIdByClassLevelAndParent($event['class_level_id'], $user->_data['user_id']);
                if (count($childIds) == 0) {
                    _api_error(404);
                }

                //Lấy ra thông tin cha mẹ và trẻ có tham gia không
                $pp = array();
                if ($event['must_register']) {
                    $child = array();
                    $child['pp_id'] = $childIds[0];
                    $child['is_registered'] = false;

                    $parent = array();
                    $parent['pp_id'] = $user->_data['user_id'];
                    $parent['is_registered'] = false;

                    $participants = $eventDao->_getChildPaticipants($event['event_id'], $childIds[0], $user->_data['user_id']);
                    foreach ($participants as $participant) {
                        if ($participant['pp_type'] == PARTICIPANT_TYPE_CHILD) {
                            $child['is_registered'] = true;
                            $child['is_paid'] = $participant['is_paid'];
                        } else {
                            $parent['is_registered'] = true;
                            $parent['is_paid'] = $participant['is_paid'];
                        }
                    }
                    if ($event['for_child']) {
                        $pp['child'] = $child;
                    }
                    if ($event['for_parent']) {
                        $pp['parent'] = $parent;
                    }
                }
                $event['participants'] = $pp;

                // return
                return_json(array(
                    'code' => 200,
                    'message' => 'OK',
                    'data' => array(
                        'child_id' => $childIds[0],
                        'event_child' => $event
                    )
                ));
            } else {
                // Thông báo tài khoản trường
                $event = $eventDao->getEvent($_POST['node_url']);
                if (is_null($event)) {
                    _api_error(0, __("The event does not exist, it should be deleted."));
                }

                // Nhân viên chỉ có thể xem thông báo ở màn hình trường
//                $school = $schoolDao->getSchoolById($event['school_id']);
//                if (count($school) == 0) {
//                    _api_error(404);
//                }

//                //Lấy ra thông tin user có tham gia không
//                $pp = array();
//                if ($event['must_register']) {
//
//                    $teacher = array();
//                    $teacher['pp_id'] = $user->_data['user_id'];
//                    $teacher['is_registered'] = false;
//
//                    $participants = $eventDao->_getTeacherPaticipants($event['event_id'], $user->_data['user_id']);
//                    foreach ($participants as $participant) {
//                        if ($participant['pp_type'] == PARTICIPANT_TYPE_TEACHER) {
//                            $teacher['is_registered'] = true;
//                            $teacher['is_paid'] = $participant['is_paid'];
//                        }
//                    }
//                    if ($event['for_teacher']) {
//                        $event['participants'] = $teacher['is_registered'];
//                    }
//                }

                // return
                return_json(array(
                    'code' => 200,
                    'message' => 'OK',
                    'data' => array(
                        'page_name' => $_POST['extra2'],
                        'event_school' => $event
                    )
                ));
            }
            break;
        case NOTIFICATION_NEW_EVENT_CLASS:
        case NOTIFICATION_UPDATE_EVENT_CLASS:

            if ($_POST['node_type'] == NOTIFICATION_NODE_TYPE_CLASS) {
                $event = $eventDao->getClassEventDetailApi($_POST['node_url']);
                if (is_null($event)) {
                    _api_error(0, __("The event does not exist, it should be deleted."));
                }
                //Giáo viên chỉ có thể xem thông báo trong màn hình lớp
                //$class = $classDao->getClass($event['class_id']);
                $class = getClassData($event['class_id'], CLASS_INFO);
                if (is_null($class)) {
                    _api_error(404);
                }
                $event['group_title'] = $class['group_title'];
                $childCount = 0;
                $parentCount = 0;
                $results = array();

                //$event['participant'] = $eventDao->getParticipantsOfClass($class['group_id'], $event['event_id'], $event['for_parent'], $childCount, $parentCount);
                // return
                return_json(array(
                    'code' => 200,
                    'message' => 'OK',
                    'data' => array(
                        'group_name' => $class['group_name'],
                        'event_class' => $event
                    )
                ));
            } elseif ($_POST['node_type'] == NOTIFICATION_NODE_TYPE_CHILD) {
                $event = $eventDao->getEvent($_POST['node_url']);
                if (is_null($event)) {
                    _api_error(0, __("The event does not exist, it should be deleted."));
                }

                //Phụ huynh chỉ có thể xem thông báo trong màn hình quản lý thông tin con
                $childIds = $childDao->getChildIdByClassAndParent($event['class_id'], $user->_data['user_id']);
                if (count($childIds) == 0) {
                    _api_error(404);
                }

                //Lấy ra thông tin cha mẹ và trẻ có tham gia không
                $pp = array();
                if ($event['must_register']) {
                    $child = array();
                    $child['pp_id'] = $childIds[0];
                    $child['is_registered'] = false;

                    $parent = array();
                    $parent['pp_id'] = $user->_data['user_id'];
                    $parent['is_registered'] = false;

                    $participants = $eventDao->_getChildPaticipants($event['event_id'], $childIds[0], $user->_data['user_id']);
                    foreach ($participants as $participant) {
                        if ($participant['pp_type'] == PARTICIPANT_TYPE_CHILD) {
                            $child['is_registered'] = true;
                            $child['is_paid'] = $participant['is_paid'];
                        } else {
                            $parent['is_registered'] = true;
                            $parent['is_paid'] = $participant['is_paid'];
                        }
                    }
                    if ($event['for_child']) {
                        $pp['child'] = $child;
                    }
                    if ($event['for_parent']) {
                        $pp['parent'] = $parent;
                    }
                }

                $event['participants'] = empty($pp) ? null : $pp;

                // return
                return_json(array(
                    'code' => 200,
                    'message' => 'OK',
                    'data' => array(
                        'child_id' => $childIds[0],
                        'event_child' => $event
                    )
                ));
            } else {
                // Thông báo tài khoản trường
                $event = $eventDao->getEvent($_POST['node_url']);
                if (is_null($event)) {
                    _api_error(0, __("The event does not exist, it should be deleted."));
                }

                // Nhân viên chỉ có thể xem thông báo ở màn hình trường
//                $school = $schoolDao->getSchoolById($event['school_id']);
//                if (count($school) == 0) {
//                    _api_error(404);
//                }

                //Lấy ra thông tin user có tham gia không
//                $pp = array();
//                if ($event['must_register']) {
//
//                    $teacher = array();
//                    $teacher['pp_id'] = $user->_data['user_id'];
//                    $teacher['is_registered'] = false;
//
//                    $participants = $eventDao->_getTeacherPaticipants($event['event_id'], $user->_data['user_id']);
//                    foreach ($participants as $participant) {
//                        if ($participant['pp_type'] == PARTICIPANT_TYPE_TEACHER) {
//                            $teacher['is_registered'] = true;
//                            $teacher['is_paid'] = $participant['is_paid'];
//                        }
//                    }
//                    if ($event['for_teacher']) {
//                        $event['participants'] = $teacher['is_registered'];
//                    }
//                }

                // return
                return_json(array(
                    'code' => 200,
                    'message' => 'OK',
                    'data' => array(
                        'page_name' => $_POST['extra2'],
                        'event_school' => $event
                    )
                ));
            }
            break;
        case NOTIFICATION_NEW_MEDICINE:
        case NOTIFICATION_UPDATE_MEDICINE:
        case NOTIFICATION_USE_MEDICINE:
        case NOTIFICATION_CONFIRM_MEDICINE:
        case NOTIFICATION_CANCEL_MEDICINE:
            //Thông báo 'gửi thuốc' cho con. node_url = medicine_id
            $medicine = $medicineDao->getMedicine($_POST['node_url']);
            if (is_null($medicine)) {
                _api_error(0, __("The medicine does not exist, it should be deleted."));
            }
            if ($_POST['node_type'] == NOTIFICATION_NODE_TYPE_CLASS) {
                //$class = $classDao->getClassByChildAndTeacher($medicine['child_id'], $user->_data['user_id']);
                $class = getClassDataByUsername($_POST['extra2']);
                if (is_null($class)) {
                    _api_error(404);
                }
                $schoolConfig = getSchoolData($class['school_id'], SCHOOL_CONFIG);
                $school_allow_medicate = $schoolConfig['school_allow_medicate'];

                //$medicine['details'] = $medicineDao->getMedicineDetail($medicine['medicine_id']);
                $medicine['count'] = count($medicine['detail']);
                $medicine['can_delete'] = (($medicine['count'] == 0) && ($medicine['editable'] == 1) && $medicine['created_user_id']==$user->_data['user_id']);

                // return
                return_json(array(
                    'code' => 200,
                    'message' => 'OK',
                    'data' => array(
                        'group_name' => $class['group_name'],
                        'medicine_class' => $medicine,
                        'school_allow_medicate' => $school_allow_medicate
                    )
                ));

            } elseif ($_POST['node_type'] == NOTIFICATION_NODE_TYPE_CHILD) {
                //$child = $childDao->getChild($medicine['child_id']);
                $child = getChildData($medicine['child_id'], CHILD_INFO);
                if (is_null($child)) {
                    _api_error(404);
                }

                //Lấy ra thông tin uống thuốc chi tiết hôm nay
                $medicine['count'] = $medicineDao->_getCountDetail($medicine['medicine_id']);
                $medicine['can_delete'] = (($medicine['count'] == 0) && $medicine['created_user_id']==$user->_data['user_id']);

                $medicine['child_name'] = $child['child_name'];
                $medicine['birthday'] = $child['birthday'];
                //Lấy ra thông tin uống thuôc ( thời gian, số lần và người cho uống)
                $details = $medicineDao->getMedicineDetail($medicine['medicine_id']);
                $medicine['details'] = $details;

                // return
                return_json(array(
                    'code' => 200,
                    'message' => 'OK',
                    'data' => array(
                        'child_id' => $medicine['child_id'],
                        'medicine_child' => $medicine
                    )
                ));
            }
            break;
        case NOTIFICATION_NEW_TUITION:
        case NOTIFICATION_UPDATE_TUITION:
        case NOTIFICATION_CONFIRM_TUITION:
        case NOTIFICATION_UNCONFIRM_TUITION:
            //Thông báo 'học phí'. node_url = tuition_child_id. Cả 04 loại thông báo trên đều gửi phụ huynh
            $tuition = $tuitionDao->getTuitionDetailOfChildInMonthByTuitionChildId($_POST['node_url']);
            if (is_null($tuition)) {
                _api_error(0, __("The tuition does not exist, it should be deleted."));
            }

            $class = $classDao->getClassOfChild($_POST['extra2']);
            if (is_null($class)) {
                _api_error(404);
            }

            // Lấy thông tin tài khoản của nhà trường
            $school = $schoolDao->getSchoolByClass($class['group_id']);
            $schoolId = $school['page_id'];
            //$dataCon = $schoolDao->getConfiguration($schoolId);
            $dataCon = getSchoolData($schoolId, SCHOOL_CONFIG);
            $bank_account = "";
            if (isset($dataCon['bank_account'])) {
                $bank_account = $dataCon['bank_account'];
            }

            // return
            return_json(array(
                'code' => 200,
                'message' => 'OK',
                'data' => array(
                    'child_id' => $_POST['extra2'],
                    'bank_account' => $bank_account,
                    'tuition_child' => $tuition
                )
            ));
            break;
        case NOTIFICATION_ATTENDANCE_RESIGN:
            //$attendanceDetail = $attendanceDao->getAttendanceDetailById($_POST['node_url']);
            $attendanceDetail = $attendanceDao->getAttendanceChildApi($_POST['node_url']);
            if (is_null($attendanceDetail)) {
                _api_error(0, __("The resign does not exist, it should be deleted."));
            }
            //$class = $classDao->getClass($attendanceDetail['class_id']);
            $class = getClassData($attendanceDetail['class_id'], CLASS_INFO);

            //$results = $attendanceDao->getAttendanceChildApi($_POST['node_url']);
            // return
            return_json(array(
                'code' => 200,
                'message' => __("OK"),
                'data' => array(
                    'group_name' => $class['group_name'],
                    'attendance_class' => $attendanceDetail
                )
            ));

            break;
        case NOTIFICATION_ATTENDANCE_CONFIRM:
            /*$attendanceDetail = $attendanceDao->getAttendanceDetailById($_POST['node_url']);
            $url = $url.'/child/'.$attendanceDetail['child_id'].'/attendance';*/
            break;

        case NOTIFICATION_NEW_REPORT:
        case NOTIFICATION_UPDATE_REPORT:
            //Thông báo 'báo cáo'. node_url = report_id. Cả 02 loại thông báo trên đều gửi phụ huynh
            $report = $reportDao->getReportById($_POST['node_url']);

            //Phụ huynh chỉ có thể xem thông báo trong màn hình quản lý thông tin con
            $childId = $_POST['extra2'];

            // return
            return_json(array(
                'code' => 200,
                'message' => __("OK"),
                'data' => array(
                    'child_id' => $childId,
                    'report' => !is_null($report) ? $report : array()
                )
            ));
            break;

        case NOTIFICATION_NEW_FEEDBACK:
        case NOTIFICATION_CONFIRM_FEEDBACK:
            return_json(array(
                'code' => 200,
                'message' =>  __("OK"),
                'data' => array()
            ));
            break;

        case NOTIFICATION_UPDATE_PICKER:
            //Thông báo 'sửa thông tin người đón' cho con. node_url = child_id
            $information = $childDao->getChildInformation($_POST['node_url']);
            $information = getChildData($_POST['node_url'], CHILD_PICKER_INFO);
            if (is_null($information)) {
                _api_error(0, "The information does not exist, it should be deleted.");
            }
            if ($_POST['node_type'] == NOTIFICATION_NODE_TYPE_CLASS) {
                $class = $classDao->getClassByChildAndTeacher($information['child_id'], $user->_data['user_id']);
                if (is_null($class)) {
                    _api_error(404);
                }

                // return
                return_json(array(
                    'code' => 200,
                    'message' => __("OK"),
                    'data' => array(
                        'group_name' => $class['group_name'],
                        'picker' => $information
                    )
                ));
            }

            break;
        case NOTIFICATION_CONFIRM_PICKER:
            //Thông báo 'sửa thông tin người đón' cho con. node_url = child_id
            //$information = $childDao->getChildInformation($_POST['node_url']);
            $information = getChildData($_POST['node_url'], CHILD_PICKER_INFO);
            if (is_null($information)) {
                _api_error(0, "The information does not exist, it should be deleted.");
            }
            if ($_POST['node_type'] == NOTIFICATION_NODE_TYPE_CHILD) {
                $class = $classDao->getClassByChildAndTeacher($information['child_id'], $user->_data['user_id']);
                // return
                return_json(array(
                    'code' => 200,
                    'message' => __("OK"),
                    'data' => array(
                        'child_id' => $information['child_id'],
                        'picker' => $information
                    )
                ));
            }
            break;
        case NOTIFICATION_NEW_SCHEDULE:
        case NOTIFICATION_UPDATE_SCHEDULE:
            if ($_POST['node_type'] == NOTIFICATION_NODE_TYPE_CLASS) {
                //$url = $url.'/class/'.$ntf['extra2'].'/schedules/detail/' . $ntf['node_url'];

                //Lấy ra thông tin lớp, bao gồm cả role_id của user hiện tại
                //$class = $classDao->getClassByUsername($_POST['extra2'], $user->_data['user_id']);
                $class = getClassDataByUsername($_POST['extra2'], CLASS_INFO);

                if (is_null($class)) {
                    _api_error(404);
                }
                //Lấy ra danh sách lịch học áp dụng cho lớp

                $schedule = $scheduleDao->getScheduleDetailById($_POST['node_url']);

                if($schedule['applied_for'] == 1) {
                    $schedule['level'] =  __("School");
                } elseif($schedule['applied_for'] == 2) {
                    //$class_level = $classLevelDao->getClassLevel($schedule['class_level_id']);
                    $class_level = getClassLevelData($schedule['class_level_id'], CLASS_LEVEL_INFO);
                    $schedule['level'] = $class_level['class_level_name'];
                } elseif($schedule['applied_for'] == 3) {
                    //$classes = $classDao->getClass($schedule['class_id']);
                    $classes = getClassData($schedule['class_id'], CLASS_INFO);
                    $schedule['level'] = $classes['group_title'];
                }

                return_json(array(
                    'code' => 200,
                    'message' => 'OK',
                    'data' => array(
                        'schedule_class' => $schedule
                    )
                ));

            } else {
                //$url = $url.'/child/'.$ntf['extra2'].'/schedules/detail/' . $ntf['node_url'];

                //$class = $classDao->getClassOfChild($_POST['extra2']);
                $class = getChildDataById($_POST['extra2'], CHILD_CLASS);
                if (is_null($class)) {
                    _api_error(404);
                }

                $schedule = $scheduleDao->getScheduleDetailById($_POST['node_url']);

                if($schedule['applied_for'] == 1) {
                    $schedule['level'] =  __("School");
                } elseif($schedule['applied_for'] == 2) {
                    //$class_level = $classLevelDao->getClassLevel($schedule['class_level_id']);
                    $class_level = getClassLevelData($schedule['class_level_id'], CLASS_LEVEL_INFO);
                    $schedule['level'] = $class_level['class_level_name'];
                } elseif($schedule['applied_for'] == 3) {
                    //$classes = $classDao->getClass($schedule['class_id']);
                    $classes = getClassData($schedule['class_id'], CLASS_INFO);
                    $schedule['level'] = $classes['group_title'];
                }

                return_json(array(
                    'code' => 200,
                    'message' => 'OK',
                    'data' => array(
                        'schedule_child' => $schedule
                    )
                ));
            }
            break;

        case NOTIFICATION_NEW_MENU:
        case NOTIFICATION_UPDATE_MENU:
            if ($_POST['node_type'] == NOTIFICATION_NODE_TYPE_CLASS) {
                //$url = $url.'/class/'.$ntf['extra2'].'/schedules/detail/' . $ntf['node_url'];

                //Lấy ra thông tin lớp, bao gồm cả role_id của user hiện tại
                //$class = $classDao->getClassByUsername($_POST['extra2'], $user->_data['user_id']);
                $class = getClassDataByUsername($_POST['extra2'], CLASS_INFO);
                if (is_null($class)) {
                    _api_error(404);
                }
                //Lấy ra danh sách lịch học áp dụng cho lớp

                $menu = $menuDao->getMenuDetailById($_POST['node_url']);

                if($menu['applied_for'] == 1) {
                    $menu['level'] =  __("School");
                } elseif($menu['applied_for'] == 2) {
                    //$class_level = $classLevelDao->getClassLevel($menu['class_level_id']);
                    $class_level = getClassLevelData($menu['class_level_id'], CLASS_LEVEL_INFO);
                    $menu['level'] = $class_level['class_level_name'];
                } elseif($menu['applied_for'] == 3) {
                    //$classes = $classDao->getClass($menu['class_id']);
                    $classes = getClassData($menu['class_id'], CLASS_INFO);
                    $menu['level'] = $classes['group_title'];
                }

                return_json(array(
                    'code' => 200,
                    'message' => 'OK',
                    'data' => array(
                        'menu_class' => $menu
                    )
                ));

            } else {
                //$url = $url.'/child/'.$ntf['extra2'].'/schedules/detail/' . $ntf['node_url'];

                $class = getChildDataById($_POST['extra2'], CHILD_CLASS);
                if (is_null($class)) {
                    _api_error(404);
                }

                    $menu = $menuDao->getMenuDetailById($_POST['node_url']);

                if($menu['applied_for'] == 1) {
                    $menu['level'] =  __("School");
                } elseif($menu['applied_for'] == 2) {
                    //$class_level = $classLevelDao->getClassLevel($menu['class_level_id']);
                    $class_level = getClassLevelData($menu['class_level_id'], CLASS_LEVEL_INFO);
                    $menu['level'] = $class_level['class_level_name'];
                } elseif($menu['applied_for'] == 3) {
                    //$classes = $classDao->getClass($menu['class_id']);
                    $classes = getClassData($menu['class_id'], CLASS_INFO);
                    $menu['level'] = $classes['group_title'];
                }

                return_json(array(
                    'code' => 200,
                    'message' => 'OK',
                    'data' => array(
                        'menu_child' => $menu
                    )
                ));
            }
            break;
        case NOTIFICATION_CONFIRM_CHILD_TEACHER:
        case NOTIFICATION_UNCONFIRM_CHILD_TEACHER:
//        $class = getClassDataByUsername($_POST['extra2'], CLASS_INFO);
//
//        if (is_null($class)) {
//            _api_error(0, __("The class does not exist, it should be deleted."));
//        }
        //$child = $childDao->getChild($_POST['node_url']);
        $child = getChildData($_POST['node_url'], CHILD_INFO);

        return_json(array(
            'code' => 200,
            'message' => 'OK',
            'data' => array(
                'child' => $child
            )
        ));

        break;


        case NOTIFICATION_ADD_LATEPICKUP_CLASS:
        case NOTIFICATION_UPDATE_LATEPICKUP_INFO:
            $return = array();
            //Lấy ra thông tin lớp, bao gồm cả role_id của user hiện tại
            $class = $classDao->getClassOfChild($_POST['extra2']);
            if (is_null($class)) {
                _api_error(0, __("The class does not exist, it should be deleted."));
            }

            $total = 0;
            $rs = $pickupDao->getChildPickupHistory($class['school_id'], $_POST['extra2'], toDBDate($_POST['extra1']), toDBDate($_POST['extra1']), $total);
            $results = empty($rs) ? null : $rs[0];

            return_json(array(
                'code' => 200,
                'message' => __("OK"),
                'data' => array(
                    'pickup_detail' => $results,
                    'total' => $total
                )
            ));
            break;

        case NOTIFICATION_CHILD_PICKEDUP:
            $return = array();
            //Lấy ra thông tin lớp, bao gồm cả role_id của user hiện tại
            $class = $classDao->getClassOfChild($_POST['extra2']);
            if (is_null($class)) {
                _api_error(0, __("The class does not exist, it should be deleted."));
            }
            $pickup = $pickupDao->getPickup($_POST['node_url']);

            $total = 0;
            $rs = $pickupDao->getChildPickupHistory($class['school_id'], $_POST['extra2'], $pickup['pickup_time'], $pickup['pickup_time'], $total);
            $results = empty($rs) ? null : $rs[0];

            return_json(array(
                'code' => 200,
                'message' => __("OK"),
                'data' => array(
                    'pickup_detail' => $results,
                    'total' => $total
                )
            ));
            break;

        case NOTIFICATION_REMOVE_LATEPICKUP_CLASS:
            break;
        case NOTIFICATION_REGISTER_LATEPICKUP:
            break;
        case NOTIFICATION_SERVICE_REGISTER_UNCOUNTBASED:
        case NOTIFICATION_SERVICE_REGISTER_COUNTBASED:
        case NOTIFICATION_SERVICE_CANCEL_UNCOUNTBASED:
        case NOTIFICATION_SERVICE_CANCEL_COUNTBASED:
            break;

        case NOTIFICATION_ASSIGN_LATEPICKUP_CLASS:
            $monday = date("Y-m-d", strtotime("monday this week", strtotime(toDBDate($_POST['extra1']))));

            $assign_week = $pickupDao->getAssignInWeekForApi($_POST['extra2'], $monday);

            // return & exit
            return_json(array(
                'code' => 200,
                'message' => 'OK',
                'data' => array(
                    'assign_week' => $assign_week,
                    'begin_date' => toSysDate($monday)
                )
            ));
            break;
        case NOTIFICATION_REGISTER_EVENT_PARENT:
            if (!isset($_POST['node_url']) || !is_numeric($_POST['node_url'])) {
                _error(404);
            }
            $event = $eventDao->getChildEventDetail($_POST['node_url'], $_POST['extra2']);
            if (is_null($event)) {
                _error(404);
            }
            return_json(array(
                'code' => 200,
                'message' => 'OK',
                'data' => array(
                    'event_child' => $event
                )
            ));
            break;
        case NOTIFICATION_FOETUS_DEVELOPMENT:
            if (!isset($_POST['node_url']) || !is_numeric($_POST['node_url'])) {
                _error(404);
            }
            $foetus_development = $foetusDevelopmentDao->getFoetusInfoDetail($_POST['node_url']);
            if (is_null($foetus_development)) {
                _error(404);
            }
            return_json(array(
                'code' => 200,
                'message' => 'OK',
                'data' => array(
                    'foetus_development' => $foetus_development
                )
            ));
            break;
        case NOTIFICATION_CHILD_DEVELOPMENT:
            if (!isset($_POST['node_url']) || !is_numeric($_POST['node_url'])) {
                _error(404);
            }
            $child_development = $childDevelopmentDao->getChildDevelopmentDetail($_POST['node_url']);
            if (is_null($child_development)) {
                _error(404);
            }
            return_json(array(
                'code' => 200,
                'message' => 'OK',
                'data' => array(
                    'child_development' => $child_development
                )
            ));
            break;
        case NOTIFICATION_UPDATE_CHILD_SCHOOL:
            if (!isset($_POST['node_url']) || !is_numeric($_POST['node_url'])) {
                _error(404);
            }
            $db->begin_transaction();
            //$childInfo = $childDao->getFullInfoOfChildByParent($_POST['child_id']);
            $childInfo = $childDao->getChildBasicInfo($_POST['node_url']);

            // Lấy thông tin chiều cao cân nặng trung bình của trẻ
            //Lấy class ID của lớp trẻ đang học (nếu có)
            $class = $classDao->getClassOfChild($childInfo['child']['child_id'], $childInfo['child']['school_id']);
            $growths = array();
            if(!is_null($class)) {
                $childIds = $childDao->getChildIdOfClass($class['group_id']);
                // Lấy chiều cao, cân nặng lần gần nhất của mỗi trẻ
                $total_child = count($childIds);
                foreach ($childIds as $childId) {
                    $child_parent_id = $childDao->getChildParentIdFromChildId($childId);
                    $growth = $childDao->getChildGrowthLastest($child_parent_id);
                    if(!is_null($growth)) {
                        $growths[] = $growth;
                    }
                }
            }
            $db->commit();

            return_json(array(
                'code' => 200,
                'message' => __("OK"),
                'data' =>array(
                    'child_info' => $childInfo,
                    'growths_class' => $growths,
                    'total_child' => $total_child
                )
            ));
            break;

        default:
            _api_error(404);
    }

} catch (Exception $e) {
    header('HTTP/1.1 400 API Error');
    return_json(array(
        'code' => 0,
        'message' => $e->getMessage(),
        'data' => null
    ));
}
?>