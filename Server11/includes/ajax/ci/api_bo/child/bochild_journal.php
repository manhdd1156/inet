<?php
/**
 * Package: ajax/ci/api_bo
 *
 * @package ConIu
 * @author TaiLA
 */

require(ABSPATH.'includes/class-image.php');

include_once(DAO_PATH.'dao_child.php');
include_once(DAO_PATH.'dao_journal.php');
include_once(DAO_PATH.'dao_user.php');

$childDao = new ChildDAO();
$journalDao = new JournalDAO();
$userDao = new UserDAO();


//if (!is_numeric($_POST['child_parent_id'])) {
//    _api_error(400);
//}
//
////Kiểm tra xem có phải phụ huynh của trẻ hay không
//if (!$childDao->isParent($_POST['child_parent_id'])) {
//    _api_error(403);
//}

try {
    $db->autocommit(false);

    $childTemp = $childDao->getChildByParent($_POST['child_parent_id']);
    // Tăng số lượt tương tác - TaiLa
    addInteractive($childTemp['school_id'], 'diary', 'parent_view');
    switch ($_POST['do']) {
        case 'lists':
            /**
             * Lấy danh sách nhật ký của trẻ
             */
            if (!isset($_POST['child_parent_id']) || !is_numeric($_POST['child_parent_id'])) {
                _api_error(404);
            }
            $db->begin_transaction();

            $journals = $journalDao->getAllJournalChildForAPI($_POST['child_parent_id']);

            $db->commit();

            return_json(array(
                'code' => 200,
                'message' => __("OK"),
                'data' =>array('journals' => $journals)
            ));
            break;
        case 'album_detail':
            if (!isset($_POST['child_journal_album_id']) || !is_numeric($_POST['child_journal_album_id'])) {
                _api_error(404);
            }

            $db->begin_transaction();
            $journal = $journalDao->getJournalById($_POST['child_journal_album_id']);
            $db->commit();

            return_json(array(
                'code' => 200,
                'message' => __("OK"),
                'data' =>array('journal' => $journal)
            ));
            break;
        case 'add':
            /**
             * Hàm này xử lý khi phụ huynh thêm thông tin nhật ký cho trẻ
             */
            if(!isset($_POST['child_parent_id']) || !is_numeric($_POST['child_parent_id'])) {
                _error(404);
            }

            $db->begin_transaction();

            $child_parent_id = $_POST['child_parent_id'];
            $caption = (isset($_POST['caption']) && ($_POST['caption'] != null)) ? $_POST['caption'] : 'null';
            // get allowed file size
            $max_allowed_size = $system['max_photo_size'] * 1024;

            /* check & create uploads dir */
            $depth = '';
            $folder = 'children';
            if(!file_exists($depth.$system['system_uploads_directory'].'/'.$folder)) {
                @mkdir($depth.$system['system_uploads_directory'].'/'.$folder, 0777, true);
            }
            if(!file_exists($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y'))) {
                @mkdir($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y'), 0777, true);
            }
            if(!file_exists($system['system_uploads_directory'].'/'.$folder.'/' . date('Y') . '/' . date('m'))) {
                @mkdir($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y') . '/' . date('m'), 0777, true);
            }

            $files = array();
            foreach ($_FILES['file'] as $key => $val) {
                for ($i = 0; $i < count($val); $i++) {
                    $files[$i][$key] = $val[$i];
                }
            }

            $return_files = array();
            $files_num = count($files);

            foreach ($files as $file) {

                // valid inputs
                if (!isset($file) || $file["error"] != UPLOAD_ERR_OK) {
                    if ($files_num > 1) {
                        continue;
                    } else {
                        modal(MESSAGE, __("Upload Error"), __("You must add at least one image"));
                    }
                }

                // check file size
                if ($file["size"] > $max_allowed_size) {
                    if ($files_num > 1) {
                        continue;
                    } else {
                        modal(MESSAGE, __("Upload Error"), __("The file size is so big"));
                    }
                }

                /* prepare new file name */
                $directory = $folder . '/' . date('Y') . '/' . date('m') . '/';
                $prefix = $system['uploads_prefix'] . '_' . md5(time() * rand(1, 9999));


                $image = new Image($file["tmp_name"]);

                $image_tmp_name = $directory . $prefix . '_tmp' . $image->_img_ext;
                $image_new_name = $directory . $prefix . $image->_img_ext;
                $path_tmp = $depth . $system['system_uploads_directory'] . '/' . $image_tmp_name;
                $path_new = $depth . $system['system_uploads_directory'] . '/' . $image_new_name;

                /* check if the file uploaded successfully */
                if (!@move_uploaded_file($file['tmp_name'], $path_tmp)) {
                    if ($files_num > 1) {
                        continue;
                    } else {
                        modal(MESSAGE, __("Upload Error"), __("Sorry, can not upload the file"));
                    }
                }

                /* save the new image */
                $image->save($path_new, $path_tmp);

                /* delete the tmp image */
                unlink($path_tmp);

                /* return */
                $return_files[] = $image_new_name;
            }
            //Thêm album nhật ký
            $isParent = 1;
            $journalAlbumId = $journalDao->insertJournalAlbum($child_parent_id, $caption, $isParent);
            // Thêm ảnh vào album
            $journalDao->insertJournal($journalAlbumId, $return_files);

            // Tăng số lượt tương tác - TaiLa
            addInteractive($childTemp['school_id'], 'diary', 'parent_view', $journalAlbumId, 2);

            $db->commit();

            return_json(array(
                'code' => 200,
                'message' =>  __("Done, journal corner has been created"),
                'data' => array()
            ));

            break;
        case 'add_photo':
            // valid inputs
            if(!isset($_POST['child_journal_album_id']) || !is_numeric($_POST['child_journal_album_id'])) {
                _error(400);
            }
            $db->begin_transaction();

            // get allowed file size
            $max_allowed_size = $system['max_photo_size'] * 1024;

            /* check & create uploads dir */
            $depth = '';
            $folder = 'children';
            if(!file_exists($depth.$system['system_uploads_directory'].'/'.$folder)) {
                @mkdir($depth.$system['system_uploads_directory'].'/'.$folder, 0777, true);
            }
            if(!file_exists($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y'))) {
                @mkdir($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y'), 0777, true);
            }
            if(!file_exists($system['system_uploads_directory'].'/'.$folder.'/' . date('Y') . '/' . date('m'))) {
                @mkdir($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y') . '/' . date('m'), 0777, true);
            }

            $files = array();
            foreach ($_FILES['file'] as $key => $val) {
                for ($i = 0; $i < count($val); $i++) {
                    $files[$i][$key] = $val[$i];
                }
            }

            $return_files = array();
            $files_num = count($files);

            foreach ($files as $file) {

                // valid inputs
                if (!isset($file) || $file["error"] != UPLOAD_ERR_OK) {
                    if ($files_num > 1) {
                        continue;
                    } else {
//                        modal(MESSAGE, __("Upload Error"), __("Something wrong with upload! Is 'upload_max_filesize' set correctly?"));
                        return_json( array('error' => true, 'message' => __("You must add at least one image")) );
                    }
                }

                // check file size
                if ($file["size"] > $max_allowed_size) {
                    if ($files_num > 1) {
                        continue;
                    } else {
                        //modal(MESSAGE, __("Upload Error"), __("The file size is so big"));
                        return_json( array('error' => true, 'message' => __("The file size is so big")) );
                    }
                }

                /* prepare new file name */
                $directory = $folder . '/' . date('Y') . '/' . date('m') . '/';
                $prefix = $system['uploads_prefix'] . '_' . md5(time() * rand(1, 9999));


                $image = new Image($file["tmp_name"]);

                $image_tmp_name = $directory . $prefix . '_tmp' . $image->_img_ext;
                $image_new_name = $directory . $prefix . $image->_img_ext;
                $path_tmp = $depth . $system['system_uploads_directory'] . '/' . $image_tmp_name;
                $path_new = $depth . $system['system_uploads_directory'] . '/' . $image_new_name;

                /* check if the file uploaded successfully */
                if (!@move_uploaded_file($file['tmp_name'], $path_tmp)) {
                    if ($files_num > 1) {
                        continue;
                    } else {
                        //modal(MESSAGE, __("Upload Error"), __("Sorry, can not upload the file"));
                        return_json( array('error' => true, 'message' => __("Sorry, can not upload the file")) );
                    }
                }

                /* save the new image */
                $image->save($path_new, $path_tmp);

                /* delete the tmp image */
                unlink($path_tmp);

                /* return */
                $return_files[] = $image_new_name;
            }

            // Thêm ảnh vào album
            $journalDao->addPhotoForAlbum($_POST['child_journal_album_id'], $return_files);

            $db->commit();
            return_json(array(
                'code' => 200,
                'message' =>  __("Done, journal corner has been created"),
                'data' => array()
            ));
            break;
        case 'delete':
            if (!isset($_POST['child_journal_id']) || !is_numeric($_POST['child_journal_id'])) {
                _api_error(404);
            }
            if (!isset($_POST['child_parent_id']) || !is_numeric($_POST['child_parent_id'])) {
                _api_error(404);
            }
            $db->begin_transaction();
            $journalDao->deleteJournal($_POST['child_journal_id']);
            $db->commit();
            return_json(array(
                'code' => 200,
                'message' =>  __("OK"),
                'data' => array()
            ));
            break;
        case 'delete_album':
            if (!isset($_POST['child_journal_album_id']) || !is_numeric($_POST['child_journal_album_id'])) {
                _api_error(404);
            }
            if (!isset($_POST['child_parent_id']) || !is_numeric($_POST['child_parent_id'])) {
                _api_error(404);
            }
            $db->begin_transaction();
            $journalDao->deleteJournalAlbum($_POST['child_journal_album_id']);
            $db->commit();
            return_json(array(
                'code' => 200,
                'message' =>  __("OK"),
                'data' => array()
            ));
            break;
        case 'search':
            /**
             * Lấy danh sách nhật ký của trẻ theo năm
             */
            if (!isset($_POST['child_parent_id']) || !is_numeric($_POST['child_parent_id'])) {
                _api_error(404);
            }
            $db->begin_transaction();
            if(!isset($_POST['year']) || !is_numeric($_POST['year'])) {
                $journals = $journalDao->getAllJournalChildForAPI($_POST['child_parent_id']);
            } else {
                $journals = $journalDao->getAllJournalChildOfYearForAPI($_POST['child_parent_id'], $_POST['year']);
            }

            $db->commit();
            return_json(array(
                'code' => 200,
                'message' => __("OK"),
                'data' =>array(
                    'journals' => $journals
                )
            ));
            break;
        case 'edit_caption':
            if (!isset($_POST['child_journal_album_id']) || !is_numeric($_POST['child_journal_album_id'])) {
                _api_error(404);
            }
            if (!isset($_POST['child_parent_id']) || !is_numeric($_POST['child_parent_id'])) {
                _api_error(404);
            }
            $db->begin_transaction();
            $journalDao->editCaptionAlbum($_POST['child_journal_album_id'], $_POST['caption']);
            $db->commit();
            return_json(array(
                'code' => 200,
                'message' => __("OK"),
                'data' => null
            ));
            break;
        default:
            _api_error(400);
            break;
    }

} catch (Exception $e) {
    $db->rollback();
    header('HTTP/1.1 400 API Error');
    return_json(array(
        'code' => 0,
        'message' => $e->getMessage(),
        'data' => array()
    ));
} finally {
    $db->autocommit(true);
}

?>