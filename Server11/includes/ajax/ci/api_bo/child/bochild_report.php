<?php
/**
 * Package: ajax/ci/bo
 * 
 * @package ConIu
 * @author Coniu
 */

include_once(DAO_PATH . 'dao_child.php');
include_once(DAO_PATH.'dao_report.php');
include_once(DAO_PATH . 'dao_class.php');
include_once(DAO_PATH . 'dao_school.php');

$childDao = new ChildDAO();
$reportDao = new ReportDAO();
$classDao = new ClassDAO();
$schoolDao = new SchoolDAO();

if (!isset($_POST['child_id']) || !is_numeric($_POST['child_id'])) {
    _api_error(400);
}
// Kiểm tra xem có phải phụ huynh ko?
//if (!$childDao->isParent($_POST['child_id'])) {
//    _error(403);
//}
$child = getChildDataById($_POST['child_id'], CHILD_INFO);
if (is_null($child)) {
    _api_error(403);
}

try {
    $return = array();
    // Tăng số lượt tương tác - TaiLa
    addInteractive($child['school_id'], 'report', 'parent_view');
    switch ($_POST['do']) {
        /* ---------- VIEW ----------*/
        case 'report':
            $reports = $reportDao->getChildReportForApi($_POST['child_id']);

            return_json(array(
                'code' => 200,
                'message' => 'OK',
                'data' => array(
                    'report_list' => $reports
                )
            ));
            break;

        case 'detail':
            $report = $reportDao->getReportById($_POST['report_id']);

            /* Coniu - Tương tác trường */
            $reportIds = array();
            $reportIds[] = $_POST['report_id'];
            increaseCountViews($child['school_id'], 'report', $reportIds);

            // Tăng số lượt tương tác - TaiLa
            addInteractive($child['school_id'], 'report', 'parent_view', $_POST['report_id']);
            /* Coniu - END */

            return_json(array(
                'code' => 200,
                'message' => 'OK',
                'data' => array(
                    'report' => $report
                )
            ));
            break;

        default:
            _api_error(404);
            break;
    }

} catch (Exception $e) {
    _api_error(0, $e->getMessage());
}

?>