<?php
/**
 * Package: ajax/ci/api_bo
 *
 * @package inet
 * @author TaiLA
 */

include_once(DAO_PATH.'dao_child_development.php');

$childDevelopmentDao = new ChildDevelopmentDAO();


//if (!is_numeric($_POST['child_parent_id'])) {
//    _api_error(400);
//}
//
////Kiểm tra xem có phải phụ huynh của trẻ hay không
//if (!$childDao->isParent($_POST['child_parent_id'])) {
//    _api_error(403);
//}

try {
    $db->autocommit(false);

    switch ($_POST['do']) {
        case 'lists':
            /**
             * Lấy danh sách quá trình phát triển của trẻ
             */
            if (!isset($_POST['child_parent_id']) || !is_numeric($_POST['child_parent_id'])) {
                _api_error(404);
            }
            $db->begin_transaction();

            $child_development = $childDevelopmentDao->getChildDevelopmentForParent($_POST['child_parent_id']);

            $db->commit();

            return_json(array(
                'code' => 200,
                'message' => __("OK"),
                'data' =>array('child_development' => $child_development)
            ));
            break;
        case 'lists_vaccinationed':
            /**
             * Lấy danh sách quá trình phát triển của trẻ (những lần tiêm phòng đã được tiêm)
             */
            if (!isset($_POST['child_parent_id']) || !is_numeric($_POST['child_parent_id'])) {
                _api_error(404);
            }
            $db->begin_transaction();

            $child_vaccinationed = $childDevelopmentDao->getChildDevelopmentVaccinationed($_POST['child_parent_id']);

            $db->commit();

            return_json(array(
                'code' => 200,
                'message' => __("OK"),
                'data' =>array('child_vaccinationed' => $child_vaccinationed)
            ));
            break;
        case 'lists_not_vaccination':
            /**
             * Lấy danh sách quá trình phát triển của trẻ (những lần tiêm phòng chưa được tiêm)
             */
            if (!isset($_POST['child_parent_id']) || !is_numeric($_POST['child_parent_id'])) {
                _api_error(404);
            }
            $db->begin_transaction();

            $child_not_vaccination = $childDevelopmentDao->getChildDevelopmentNotVaccination($_POST['child_parent_id']);

            $db->commit();

            return_json(array(
                'code' => 200,
                'message' => __("OK"),
                'data' =>array('child_not_vaccination' => $child_not_vaccination)
            ));
            break;
        case 'detail':
            if (!isset($_POST['child_development_id']) || !is_numeric($_POST['child_development_id'])) {
                _api_error(404);
            }

            $db->begin_transaction();
            $child_development_detail = $childDevelopmentDao->getChildDevelopmentDetail($_POST['child_development_id']);
            $db->commit();

            return_json(array(
                'code' => 200,
                'message' => __("OK"),
                'data' =>array('child_development_detail' => $child_development_detail)
            ));
            break;
        case 'vaccinationed':
            if (!isset($_POST['child_development_id']) || !is_numeric($_POST['child_development_id'])) {
                _api_error(404);
            }

            if (!isset($_POST['child_parent_id']) || !is_numeric($_POST['child_parent_id'])) {
                _api_error(404);
            }

            $db->begin_transaction();
            $childDevelopmentDao->insertChildVaccinationed($_POST['child_development_id'], $_POST['child_parent_id']);
            $db->commit();

            return_json(array(
                'code' => 200,
                'message' => __("OK"),
                'data' =>array()
            ));
            break;
        case 'cancel_vaccination':
            if (!isset($_POST['child_development_id']) || !is_numeric($_POST['child_development_id'])) {
                _api_error(404);
            }

            if (!isset($_POST['child_parent_id']) || !is_numeric($_POST['child_parent_id'])) {
                _api_error(404);
            }

            $db->begin_transaction();
            $childDevelopmentDao->deleteChildVaccination($_POST['child_development_id'], $_POST['child_parent_id']);
            $db->commit();

            return_json(array(
                'code' => 200,
                'message' => __("OK"),
                'data' =>array()
            ));
            break;
        case 'lists_vaccinating':
            /**
             * Lấy danh sách quá trình phát triển của trẻ (những lần được tiêm sắp tới)
             */
            if (!isset($_POST['child_parent_id']) || !is_numeric($_POST['child_parent_id'])) {
                _api_error(404);
            }
            $db->begin_transaction();

            $child_vaccinating = $childDevelopmentDao->getChildDevelopmentVaccinating($_POST['child_parent_id']);

            $db->commit();

            return_json(array(
                'code' => 200,
                'message' => __("OK"),
                'data' =>array('child_vaccinating' => $child_vaccinating)
            ));
            break;

        case 'lists_vaccination_history':
            /**
             * Lấy danh sách quá trình phát triển của trẻ (Những lần đã qua thời gian tiêm và chưa tiêm)
             */
            if (!isset($_POST['child_parent_id']) || !is_numeric($_POST['child_parent_id'])) {
                _api_error(404);
            }
            $db->begin_transaction();

            $child_vaccination_history = $childDevelopmentDao->getChildDevelopmentVaccinationHistory($_POST['child_parent_id']);

            $db->commit();

            return_json(array(
                'code' => 200,
                'message' => __("OK"),
                'data' =>array('child_vaccination_history' => $child_vaccination_history)
            ));
            break;
        default:
            _api_error(400);
            break;
    }

} catch (Exception $e) {
    $db->rollback();
    header('HTTP/1.1 400 API Error');
    return_json(array(
        'code' => 0,
        'message' => $e->getMessage(),
        'data' => array()
    ));
} finally {
    $db->autocommit(true);
}

?>