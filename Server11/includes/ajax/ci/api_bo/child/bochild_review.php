<?php
/**
 * Package: ajax/ci/bo
 * 
 * @package inet
 * @author inet
 */

include_once(DAO_PATH.'dao_child.php');
include_once(DAO_PATH.'dao_school.php');
$childDao = new ChildDAO();
$schoolDao = new SchoolDAO();


try {
    $db->autocommit(false);

    switch ($_POST['do']) {
        case 'createReview':
            $validType = array('school', 'teacher');
            if (!in_array($_POST['type'], $validType)) {
                _api_error(400);
            }
            if (!isset($_REQUEST['school_id'])) {
                _api_error(400);
            }
            $ratings = isset($_REQUEST['rating']) ? $_REQUEST['rating'] : array();
            $comments = isset($_REQUEST['comment']) ? $_REQUEST['comment'] : array();
            $time = date('Y-m-01');

            if ($_POST['type'] == 'school') {
                //$schoolIds = isset($_REQUEST['school_id']) ? $_REQUEST['school_id'] : array();
                $childDao->createSchoolReview($_REQUEST['school_id'], $ratings, $comments, $time);
            } elseif ($_POST['type'] == 'teacher') {
                if (!isset($_REQUEST['teacher_id']) || !isset($_REQUEST['class_id'])) {
                    _api_error(400);
                }
                //$teacherIds = isset($_REQUEST['teacher_id']) ? $_REQUEST['teacher_id'] : array();
                //$classIds = isset($_REQUEST['class_id']) ? $_REQUEST['class_id'] : array();
                $childDao->createTeacherReview($_REQUEST['teacher_id'], $_REQUEST['school_id'], $_REQUEST['class_id'], $ratings, $comments, $time);
            }

            $db->commit();

            /* ---------- UPDATE - MEMCACHE ---------- */
            //Cập nhật thông tin phụ huynh đánh giá
            updateParentReview($user->_data['user_id']);
            /* ---------- END - MEMCACHE ---------- */

            // return
            return_json(array(
                'code' => 200,
                'message' => 'OK',
                'data' => array()
            ));
            break;

        case 'getReview':
            //Lấy danh sách những đối tượng (trường/giáo viên) cần đánh giá.
            $children = getUserManageData($user->_data['user_id'], USER_MANAGE_CHILDREN);
            $result = null;

            $time = date('Y-m-01');
            $ratedSchool = array();
            $ratedClass = array();
            $schoolParam = array('page_id', 'page_name', 'page_title');
            $classParam = array('group_id', 'group_name', 'group_title');
            $teacherParam = array('user_id', 'user_name', 'user_fullname');

            foreach ($children as $child) {
                // Lấy ra tất cả các trường phải đánh giá
                if ($_POST['type'] != 'teacher') {
                    if (isset($child['school_id']) && $child['school_id'] > 0 && !in_array($child['school_id'], $ratedSchool)) {
                        $ratedSchool[] = $child['school_id'];
                        $checkSchoolRating = $childDao->checkReviewedSchool($child['school_id'], $time);
                        if (!$checkSchoolRating) {
                            $schoolInfo = getSchoolData($child['school_id'], SCHOOL_INFO);
                            $schoolInfo = getArrayFromKeys($schoolInfo, $schoolParam);
                            $result['school_review'][] = $schoolInfo;
                        }
                    }
                }

                if ($_POST['type'] != 'school') {
                    // Lấy ra tất cả các giáo viên phải đánh giá
                    $result_teacher = array();
                    if (isset($child['class_id']) && $child['class_id'] > 0 && !in_array($child['class_id'], $ratedClass)) {
                        $ratedClass[] = $child['class_id'];
                        $teachers = getClassData($child['class_id'], CLASS_TEACHERS);
                        $classInfo = getClassData($child['class_id'], CLASS_INFO);
                        $classInfo = getArrayFromKeys($classInfo, $classParam);
                        $classInfo['teacher_review'] = array();

                        foreach ($teachers as $teacher) {
                            $checkSchoolRating = $childDao->checkReviewedTeacher($teacher['user_id'], $child['class_id'], $time);
                            if (!$checkSchoolRating) {
                                $teacherInfo = getArrayFromKeys($teachers[$teacher['user_id']], $teacherParam);
                                $teacherInfo['school_id'] = $child['school_id'];
                                $teacherInfo['class_id'] = $child['class_id'];
                                $result_teacher[] = $teacherInfo;
                            }
                        }
                        if (!empty($result_teacher)) {
                            $classInfo['teacher_review'] = $result_teacher;
                        }
                        $result['class_review'][] = $classInfo;
                    }
                }
            }

            // return
            /*return_json(array(
                'code' => 200,
                'message' => 'OK',
                'data' => $result
            ));*/
            break;

        case 'getSchoolReview':
            if (!is_numeric($_POST['school_id'])) {
                _api_error(400);
            }
            $schoolConfig = getSchoolData($_POST['school_id'], SCHOOL_CONFIG);
            if (!$schoolConfig['display_rate_school']) {
                _api_error(400);
            }
            $page = (isset($_POST['page'])) ? $_POST['page'] : 0;
            $userRatings = $schoolDao->getUserRating($_POST['school_id'], $page);

            // return
            return_json(array(
                'code' => 200,
                'message' => 'OK',
                'data' => array(
                    'school_review' => $userRatings
                )
            ));
            break;

        default:
            _api_error(400);
            break;
    }

} catch (Exception $e) {
    $db->rollback();
    _api_error(0, $e->getMessage());
} finally {
    $db->autocommit(true);
}

?>