<?php
/**
 * Package: ajax/ci/api_bo
 *
 * @package inet
 * @author TaiLA
 */

include_once(DAO_PATH.'dao_foetus_development.php');

$foetusDevelopmentDao = new FoetusDevelopmentDAO();


//if (!is_numeric($_POST['child_parent_id'])) {
//    _api_error(400);
//}
//
////Kiểm tra xem có phải phụ huynh của trẻ hay không
//if (!$childDao->isParent($_POST['child_parent_id'])) {
//    _api_error(403);
//}

try {
    $db->autocommit(false);

    switch ($_POST['do']) {
        case 'lists':
            /**
             * Lấy danh sách quá trình phát triển của thai nhi
             */
            if (!isset($_POST['child_parent_id']) || !is_numeric($_POST['child_parent_id'])) {
                _api_error(404);
            }
            $db->begin_transaction();

            $foetus_info = $foetusDevelopmentDao->getFoetusInfoForParent();

            $db->commit();

            return_json(array(
                'code' => 200,
                'message' => __("OK"),
                'data' =>array('foetus_development' => $foetus_info)
            ));
            break;
        case 'detail':
            if (!isset($_POST['foetus_info_id']) || !is_numeric($_POST['foetus_info_id'])) {
                _api_error(404);
            }

            $db->begin_transaction();
            $foetus_development_detail = $foetusDevelopmentDao->getFoetusInfoDetail($_POST['foetus_info_id']);
            $db->commit();

            return_json(array(
                'code' => 200,
                'message' => __("OK"),
                'data' =>array('foetus_development_detail' => $foetus_development_detail)
            ));
            break;
        default:
            _api_error(400);
            break;
    }

} catch (Exception $e) {
    $db->rollback();
    header('HTTP/1.1 400 API Error');
    return_json(array(
        'code' => 0,
        'message' => $e->getMessage(),
        'data' => array()
    ));
} finally {
    $db->autocommit(true);
}

?>