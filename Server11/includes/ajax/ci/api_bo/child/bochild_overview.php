<?php
/**
 * Package: ajax/ci/api_bo
 *
 * @package ConIu
 * @author TaiLA
 */

// fetch image class
require(ABSPATH.'includes/class-image.php');

include_once(DAO_PATH.'dao_child.php');
include_once(DAO_PATH.'dao_parent.php');
include_once(DAO_PATH.'dao_medical.php');
include_once(DAO_PATH.'dao_child_development.php');
include_once(DAO_PATH.'dao_foetus_development.php');
include_once(DAO_PATH.'dao_childmonth.php');

$childDao = new ChildDAO();
$parentDao = new ParentDAO();
$medicalDao = new MedicalDAO();
$childDevelopmentDao = new ChildDevelopmentDAO();
$foetusDevelopmentDao = new FoetusDevelopmentDAO();
$childMonthDao = new ChildMonthDAO();

//if (!is_numeric($_POST['child_parent_id'])) {
//    _api_error(400);
//}

//Kiểm tra xem có phải phụ huynh của trẻ hay không
//if (!$childDao->isParent($_POST['child_parent_id'])) {
//    _api_error(403);
//}

try {

    $db->autocommit(false);

    switch ($_POST['do']) {
        case 'show_overview':
            /**
             * Xem thông tin tổng thể của trẻ
             */
            if (!isset($_POST['child_parent_id']) || !is_numeric($_POST['child_parent_id'])){
                _api_error(404);
            }
            // Lấy thông tin trẻ
            $child = $childDao->getChildByParent($_POST['child_parent_id']);
            // check xem là trẻ hay thai nhi
            if($child['is_pregnant']) {
                // Lấy thông tin thai nhi đang ở tuần thai thứ mấy
                $weekNow = $child['pregnant_week'];

                // Lấy link tuần thai hiện tại
                $link = $childMonthDao->getLinkBasedOnWeek($weekNow);

                // Lấy thông tin chiều cao và cân nặng gần nhất
                $foetusGrowthLatsest = $childDao->getChildFoetusHealthLastest($child['child_parent_id']);

                // Lấy thông tin chỉ số sức khỏe của thai nhi
                $foetusGrowth = $childDao->getChildFoetusHealthForChart($child['child_parent_id']);
                $foetusGrowthLast = array();
                $field = array('child_foetus_growth_id', 'from_head_to_hips_length', 'weight', 'recorded_at', 'pregnant_week');
                foreach ($foetusGrowth as $growth) {
                    $recordedDB = toDBDate($growth['recorded_at']);
                    $foetusBeginDB = toDBDate($child['foetus_begin_date']);
//                    $growth['pregnant_week'] = (strtotime($recordedDB) - strtotime($foetusBeginDB)) / (60*60*60*24*7);
//                    $growth['pregnant_week'] = (int)$growth['pregnant_week'] + 1;
                    $foetusGrowthLast[] = getArrayFromKeys($growth, $field);
                }
                // Lấy thông tin lần khám thai gần nhất sắp tới và thông tin quan tâm
                $result = $foetusDevelopmentDao->getFoetusDevelopmentInfo($child['child_parent_id']);

                $db->commit();
                return_json(array(
                    'code' => 200,
                    'message' =>  __("OK"),
                    'data' => array(
                        'childPicture' => $child['child_picture'],
                        'weekNow' => $weekNow,
                        'link' => $link,
                        'foetusGrowthLatest' => $foetusGrowthLatsest,
                        'foetusGrowths' => $foetusGrowthLast,
                        'pregnancyCheck' => $result['pregnancy'],
                        'interestInfo' => $result['interest']
                    )
                ));
            } else {
                // Lấy thông tin trẻ đang ở tháng thứ mấy
                $birthday = toDBDate($child['birthday']);
                $dateNow = date("Y-m-d");
                $birthday = strtotime($birthday);
                $dateNow = strtotime($dateNow);
                $monthNow = floor(abs($dateNow - $birthday)/(30*60*60*24)) + 1;

                // Lấy link
                $link = $childMonthDao->getLinkBasedOnMonth($monthNow);

                // Lấy chiều cao cân nặng lần gần nhất
                $growthLastest = $childDao->getChildGrowthLastestWeightNHeight($child['child_parent_id']);

                // Lấy thông tin chỉ số sức khỏe theo năm
                $growths = $childDao->getChildGrowth12MonthLastest($child['child_parent_id']);
                $growthsLast = array();
                $field = array('child_growth_id', 'height', 'weight', 'recorded_at');
                foreach ($growths as $growth) {
                    $growthsLast[] = getArrayFromKeys($growth, $field);
                }

                // Lấy lần sắp tiêm gần nhất
                $result = $childDevelopmentDao->getChildDevelopmentInfo($child['child_parent_id']);

                // Lấy school_id của trường
                $school_id = $child['school_id'];

                $db->commit();
                return_json(array(
                    'code' => 200,
                    'message' =>  __("OK"),
                    'data' => array(
                        'school_id' => $school_id,
                        'childPicture' => $child['child_picture'],
                        'monthNow' => $monthNow,
                        'link' => $link,
                        'growthLatest' => $growthLastest,
                        'growths' => $growthsLast,
//                        'medicals' => $medical_count_month,
                        'vaccinating' => $result['vaccinating'],
                        'interestInfo' => $result['interest']
                    )
                ));
            }
            break;
        default:
            _api_error(400);
            break;
    }

} catch (Exception $e) {
    $db->rollback();
    header('HTTP/1.1 400 API Error');
    return_json(array(
        'code' => 0,
        'message' => $e->getMessage(),
        'data' => array()
    ));
} finally {
    $db->autocommit(true);
}

?>