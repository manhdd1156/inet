<?php
/**
 * Package: ajax/ci/bo
 * 
 * @package ConIu
 * @author Coniu
 */

include_once(DAO_PATH . 'dao_class.php');
include_once(DAO_PATH . 'dao_class_level.php');
include_once(DAO_PATH . 'dao_child.php');
include_once(DAO_PATH . 'dao_schedule.php');

$classDao = new ClassDAO();
$classLevelDao = new ClassLevelDAO();
$childDao = new ChildDAO();
$scheduleDao = new ScheduleDAO();

// Kiểm tra xem có phải phụ huynh ko?
//if (!$childDao->isParent($_POST['child_id'])) {
//    _error(403);
//}
$child = getChildDataById($_POST['child_id'], CHILD_INFO);
if (is_null($child)) {
    _api_error(403);
}

try {
    $return = array();
    $db->autocommit(false);
    // Tăng số lượt tương tác - TaiLa
    addInteractive($child['school_id'], 'schedule', 'parent_view');
    switch ($_POST['do']) {
        case 'show_schedule':

            switch ($_POST['view']) {
                    case 'this_week':
                    /*$child = $childDao->getChild($_POST['child_id']);
                    if (is_null($child)) {
                        _api_error(404);
                    }*/
                    //$class = $classDao->getClassOfChild($_POST['child_id']);
                    $class = getChildData($_POST['child_id'], CHILD_CLASS);
                    if ($class == null) {
                        _api_error(404);
                    }

                    $return = array();
                    $monday = date('Y-m-d', strtotime('monday this week'));
                    //Lấy ra danh sách lịch học áp dụng cho lớp tuần này
                    $data = $scheduleDao->getScheduleSchoolInWeekForApi($class['school_id'], $class['class_level_id'], $class['group_id'], $monday);

                    $removeArr = array();
                    for($i = 0; $i < count($data); $i++) {
                        for($j = 0; $j < count($data); $j++) {
                            if($i != $j) {
                                $beginI = strtotime(toDBDate($data[$i]['begin']));
                                $beginJ = strtotime(toDBDate($data[$j]['begin']));
                                if($beginI == $beginJ) {
                                    if($data[$i]['applied_for'] > $data[$j]['applied_for']) {
                                        $removeArr[] = $j;
                                    }
                                }
                            }
                        }
                    }
                    $schedule = null;

                    for($k = 0; $k < count($data); $k++) {
                        if(!in_array($k, $removeArr)) {
                            $schedule = $scheduleDao->getScheduleDetailForApi($data[$k]['schedule_id']);

                            if($schedule['applied_for'] == 1) {
                                $schedule['level'] =  __("School");
                            } elseif($schedule['applied_for'] == 2) {
                                //$class_level = $classLevelDao->getClassLevel($schedule['class_level_id']);
                                $class_level = getClassLevelData($schedule['class_level_id'], CLASS_LEVEL_INFO);
                                $schedule['level'] = $class_level['class_level_name'];
                            } elseif($schedule['applied_for'] == 3) {
                                //$classes = $classDao->getClass($schedule['class_id']);
                                $classes = getClassData($schedule['class_id'], CLASS_INFO);
                                $schedule['level'] = $classes['group_title'];
                            }
                            if(!in_array($k, $removeArr)) {
                                $schedule['use'] = STATUS_ACTIVE;
                            } else {
                                $schedule['use'] = STATUS_INACTIVE;
                            }
                        }
                    }
                    return_json(array(
                        'code' => 200,
                        'message' => 'OK',
                        'data' => array(
                            'schedule_child' => $schedule
                        )
                    ));
                    break;

                case 'history':
                    $page = isset($_POST['page']) ? $_POST['page']  : 0;

                    //$class = $classDao->getClassOfChild($_POST['child_id']);
                    $class = getChildData($_POST['child_id'], CHILD_CLASS);
                    if (is_null($class)) {
                        _api_error(404);
                    }

                    //Lấy ra danh sách lịch học áp dụng cho lớp
                    $data = $scheduleDao->getScheduleSchoolForApi($class['school_id'], $class['class_level_id'], $class['group_id'], $page);

                    $removeArr = array();
                    for($i = 0; $i < count($data); $i++) {
                        for($j = 0; $j < count($data); $j++) {
                            if($i != $j) {
                                $beginI = strtotime(toDBDate($data[$i]['begin']));
                                $beginJ = strtotime(toDBDate($data[$j]['begin']));
                                if($beginI == $beginJ) {
                                    if($data[$i]['applied_for'] > $data[$j]['applied_for']) {
                                        $removeArr[] = $j;
                                    }
                                }
                            }
                        }
                    }

                    $schedules = array();
                    for($k = 0; $k < count($data); $k++) {
                        $schedule = $data[$k];

                        if($schedule['applied_for'] == 1) {
                            $schedule['level'] =  __("School");
                        } elseif($schedule['applied_for'] == 2) {
                            //$class_level = $classLevelDao->getClassLevel($schedule['class_level_id']);
                            $class_level = getClassLevelData($schedule['class_level_id'], CLASS_LEVEL_INFO);
                            $schedule['level'] = $class_level['class_level_name'];
                        } elseif($schedule['applied_for'] == 3) {
                            //$classes = $classDao->getClass($schedule['class_id']);
                            $classes = getClassData($schedule['class_id'], CLASS_INFO);
                            $schedule['level'] = $classes['group_title'];
                        }

                        if(!in_array($k, $removeArr)) {
                            $schedule['use'] = STATUS_ACTIVE;
                        } else {
                            $schedule['use'] = STATUS_INACTIVE;
                        }
                        $schedules[] = $schedule;
                    }
                    return_json(array(
                        'code' => 200,
                        'message' => 'OK',
                        'data' => array(
                            'schedule_history' => $schedules
                        )
                    ));
                    break;

                case 'detail':
                    $schedule = $scheduleDao->getScheduleDetailForApi($_POST['schedule_id']);
                    if (is_null($schedule)) {
                        _api_error(404);
                    }

                    /* Coniu - Tương tác trường */
                    $scheduleIds = array();
                    $scheduleIds[] = $schedule['schedule_id'];
                    increaseCountViews($child['school_id'], 'schedule', $scheduleIds);

                    // Tăng số lượt tương tác - TaiLa
                    addInteractive($child['school_id'], 'schedule', 'parent_view', $schedule['schedule_id']);
                    /* Coniu - END */

                    if($schedule['applied_for'] == 1) {
                        $schedule['level'] =  __("School");
                    } elseif($schedule['applied_for'] == 2) {
                        //$class_level = $classLevelDao->getClassLevel($schedule['class_level_id']);
                        $class_level = getClassLevelData($schedule['class_level_id'], CLASS_LEVEL_INFO);
                        $schedule['level'] = $class_level['class_level_name'];
                    } elseif($schedule['applied_for'] == 3) {
                        //$classes = $classDao->getClass($schedule['class_id']);
                        $classes = getClassData($schedule['class_id'], CLASS_INFO);
                        $schedule['level'] = $classes['group_title'];
                    }

                    return_json(array(
                        'code' => 200,
                        'message' => 'OK',
                        'data' => array(
                            'schedule_detail' => $schedule
                        )
                    ));
                    break;

            }
            break;

        default:
            _api_error(404);
            break;
    }

} catch (Exception $e) {
    $db->rollback();
    _api_error(0, $e->getMessage());
} finally {
    $db->autocommit(true);
}

?>