<?php
/**
 * Package: ajax/ci/bo
 * 
 * @package inet
 * @author QuanND
 */

// fetch image class
require(ABSPATH . 'includes/class-image.php');

include_once(DAO_PATH . 'dao_class.php');
include_once(DAO_PATH . 'dao_child.php');
include_once(DAO_PATH . 'dao_event.php');

$classDao = new ClassDAO();
$childDao = new ChildDAO();
$eventDao = new EventDAO();

///Lấy ra thông tin trường, bao gồm cả role_id của user hiện tại
$children = $childDao->getChildren($user->_data['user_id']);
if (count($children) == 0) {
    _api_error(404);
}

$child = null;
if ((isset($_POST['child_id']) && ($_POST['child_id'] > 0))) {
    foreach ($children as $_child) {
        if ($_child['child_id'] == $_POST['child_id']) {
            $child = $_child;
            break;
        }
    }
} else {
    $child = $children[0];
}

$class = $classDao->getClassOfChild($child['child_id']);
if (is_null($class)) {
    _api_error(404);
}

try {
    $return = array();
    $db->autocommit(false);
    // Tăng lượt tương tác thêm mới - TaiLA
    addInteractive($class['school_id'], 'event', 'parent_view');
    switch ($_POST['do']) {
        case 'show_event':
            switch ($_POST['view']) {
                case 'list':
                    $page = isset($_POST['page']) ? $_POST['page']  : 0;
                    $events = $eventDao->getChildEventsApi($class['group_id'], $class['class_level_id'], $class['school_id'], $child['child_id'], $page);

                    // return
                    return_json(array(
                        'code' => 200,
                        'message' => 'OK',
                        'data' => array(
                            'events' => $events
                        )
                    ));
                    break;

                case 'detail':
                    $event = $eventDao->getChildEventDetailForApi($_POST['event_id'], $child['child_id']);

                    /* Inet - Tương tác trường */
                    //1. Cập nhật lượt xem sự kiện của trường
                    $eventIds = array();
                    $eventIds[] = $event['event_id'];
                    increaseCountViews($child['school_id'], 'event', $eventIds);

                    // Tăng lượt tương tác thêm mới - TaiLA
                    addInteractive($class['school_id'], 'event', 'parent_view', $event['event_id']);
                    /* inet - END */

                    // return
                    return_json(array(
                        'code' => 200,
                        'message' => 'OK',
                        'data' => array(
                            'event' => $event
                        )
                    ));
                    break;

            }
            break;

        case 'register':

            $event = $eventDao->getEvent($_POST['event_id']);
            if (is_null($event)) {
                _api_error(0, "The event does not exist, it should be deleted.");
            }
            if (!$event['can_register']) {
                _api_error(403);
            }

            if ($event['for_child'] && (isset($_POST['participant_child']) && !is_numeric($_POST['participant_child']))) {
                _api_error(404);
            }

            if ($event['for_parent'] && (isset($_POST['participant_parent']) && !is_numeric($_POST['participant_parent']))) {
                _api_error(404);
            }

            $db->autocommit(false);
            $db->begin_transaction();

            if ($event['for_child']) {
                if (isset($_POST['participant_child'])) {
                    if ($_POST['participant_child'] == 1) {
                        $eventDao->insertParticipants($_POST['event_id'], PARTICIPANT_TYPE_CHILD, [$_POST['child_id']]);
                        //Thông báo cho Quản lý và Giáo viên lớp
                        // notifySchoolManagerAndTearcher($_POST['child_id'], NOTIFICATION_REGISTER_EVENT_CHILD, $_POST['event_id'], convertText4Web($event['event_name']));

                    } else {
                        //Huỷ đăng ký trẻ
                        $eventDao->deleteParticipants($_POST['event_id'], PARTICIPANT_TYPE_CHILD, [$_POST['child_id']]);
                        //Thông báo cho Quản lý và Giáo viên lớp
                        // notifySchoolManagerAndTearcher($_POST['child_id'], NOTIFICATION_CANCEL_REGISTRATION_EVENT_CHILD, $_POST['event_id'], convertText4Web($event['event_name']));
                    }
                }
            }

            if ($event['for_parent']) {
                if (isset($_POST['participant_parent'])) {
                    if ($_POST['participant_parent'] == 1) {
                        //Đăng ký phụ huynh
                        $eventDao->insertParticipants($_POST['event_id'], PARTICIPANT_TYPE_PARENT, [$user->_data['user_id']]);
                        //Thông báo cho Quản lý và Giáo viên lớp
//                            notifySchoolManagerAndTearcher($_POST['child_id'], NOTIFICATION_REGISTER_EVENT_CHILD, $_POST['event_id'],
//                                convertText4Web($event['event_name']), convertText4Web($user->_data['user_fullname']));
                    } else {
                        //Huỷ đăng ký phụ huynh
                        $eventDao->deleteParticipants($_POST['event_id'], PARTICIPANT_TYPE_PARENT, [$user->_data['user_id']]);
                        //Thông báo cho Quản lý và Giáo viên lớp
//                            notifySchoolManagerAndTearcher($_POST['child_id'], NOTIFICATION_CANCEL_REGISTRATION_EVENT_CHILD, $_POST['event_id'],
//                                convertText4Web($event['event_name']), convertText4Web($user->_data['user_fullname']));
                    }
                }
            }

            $db->commit();

            // return
            return_json(array(
                'code' => 200,
                'message' => 'OK',
                'data' => array()
            ));
            break;

        default:
            _api_error(404);
            break;
    }

} catch (Exception $e) {
    $db->rollback();
    _api_error(0, $e->getMessage());
} finally {
    $db->autocommit(true);
}

?>