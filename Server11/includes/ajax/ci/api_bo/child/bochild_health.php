<?php
/**
 * Package: ajax/ci/api_bo
 *
 * @package inet
 * @author TaiLA
 */

// fetch image class
require(ABSPATH.'includes/class-image.php');

include_once(DAO_PATH.'dao_child.php');
include_once(DAO_PATH.'dao_parent.php');
include_once(DAO_PATH.'dao_user.php');

$childDao = new ChildDAO();
$parentDao = new ParentDAO();
$userDao = new UserDAO();

//if (!is_numeric($_POST['child_parent_id'])) {
//    _api_error(400);
//}

//Kiểm tra xem có phải phụ huynh của trẻ hay không
//if (!$childDao->isParent($_POST['child_parent_id'])) {
//    _api_error(403);
//}

try {

    $db->autocommit(false);

    $childTemp = $childDao->getChildByParent($_POST['child_parent_id']);
    // Tăng lượt tương tác thêm mới - TaiLA
    addInteractive($childTemp['school_id'], 'health', 'parent_view');
    switch ($_POST['do']) {
        case 'lists':
            /**
             * Lấy thông tin sức khỏe của trẻ
             */
            if (!isset($_POST['child_parent_id']) || !is_numeric($_POST['child_parent_id'])) {
                _api_error(404);
            }

            //$childInfo = $childDao->getFullInfoOfChildByParent($_POST['child_parent_id']);
            $child = $childDao->getChildByParent($_POST['child_parent_id']);
//            if(!$child['is_pregnant']) {
                $childGrowth = $childDao->getChildGrowthLastest($child['child_parent_id']);
                $childGr = array();
                if(!is_null($childGrowth)) {
                    $childGr[] = $childGrowth;
                }
                $childFoetusGrowth = $childDao->getChildFoetusHealth($child['child_parent_id']);
                return_json(array(
                    'code' => 200,
                    'message' => __("OK"),
                    'data' =>array(
                        'child_health' => $childHealth,
                        'child_growth' => $childGr,
                        'child_foetus_growth' => $childFoetusGrowth
                    )
                ));
//            } else {


//                return_json(array(
//                    'code' => 200,
//                    'message' => __("OK"),
//                    'data' =>array('child_foetus_growth' => $childFoetusGrowth)
//                ));
//            }

            break;
        case 'search_chart':
            if(!isset($_POST['child_parent_id']) || !is_numeric(($_POST['child_parent_id']))) {
                _api_error(404);
            }
            $month_begin = $_POST['month_begin'];
            $month_end = $_POST['month_end'];

            $child = $childDao->getChildByParent($_POST['child_parent_id']);
            $birthday = toDBDate($child['birthday']);
            $time1 = '+' . $month_begin . ' months';
            $time2 = '+' . $month_end . ' months';
            $begin = date('Y-m-d', strtotime($time1, strtotime($birthday)));
            $end = date('Y-m-d', strtotime($time2, strtotime($birthday)));

            $begin = toSysDate($begin);
            $end = toSysDate($end);

            $results = $childDao->getChildGrowthSearchForChartBeginEnd($_POST['child_parent_id'], $begin, $end);

            return_json(array(
                'code' => 200,
                'message' => __("OK"),
                'data' =>array(
                    'growths' => $results
                )
            ));
            break;
//        case 'edit':
//            /**
//             * Hàm này xử lý khi phụ huynh edit thông tin sức khỏe của trẻ
//             */
//            if(!isset($_POST['child_parent_id']) || !is_numeric($_POST['child_parent_id'])) {
//                _error(404);
//            }
//            $db->begin_transaction();
//            $args = array();
//            $args['child_parent_id'] = $_POST['child_parent_id'];
//            $args['blood_type'] = $_POST['blood_type'];
//            $args['heart'] = $_POST['heart'];
//            $args['ear'] = $_POST['ear'];
//            $args['eye'] = $_POST['eye'];
//            $args['allergy'] = $_POST['allergy'];
//            $args['swim'] = $_POST['swim'];
//            $args['hobby'] = $_POST['hobby'];
//            $args['description'] = $_POST['description'];
//
//            $file_name = "";
//            // Upload file đính kèm
//            if (file_exists($_FILES['file']['tmp_name'])) {
//                // check file upload enabled
//                if (!$system['file_enabled']) {
//                    _api_error(0, __("This feature has been disabled"));
//                }
//
//                // valid inputs
//                if (!isset($_FILES["file"]) || $_FILES["file"]["error"] != UPLOAD_ERR_OK) {
//                    _api_error(0, __("Something wrong with upload! Is 'upload_max_filesize' set correctly?"));
//                }
//
//                // check file size
//                $max_allowed_size = $system['max_file_size'] * 1024;
//                if ($_FILES["file"]["size"] > $max_allowed_size) {
//                    _api_error(0, __("The file size is so big"));
//                }
//
//                // check file extesnion
//                $extension = get_extension($_FILES['file']['name']);
//                if (!valid_extension($extension, $system['file_medicine_extensions'])) {
//                    //modal(MESSAGE, __("Upload Error"), __("The file type is not valid or not supported"));
//                    _api_error(0, __("The file type is not valid or not supported") );
//                }
//
//                /* check & create uploads dir */
//                $depth = '';
//                $folder = 'growths';
//                if (!file_exists($depth . $system['system_uploads_directory'] . '/' . $folder)) {
//                    @mkdir($depth . $system['system_uploads_directory'] . '/' . $folder, 0777, true);
//                }
//                if (!file_exists($depth . $system['system_uploads_directory'] . '/' . $folder . '/' . date('Y'))) {
//                    @mkdir($depth . $system['system_uploads_directory'] . '/' . $folder . '/' . date('Y'), 0777, true);
//                }
//                if (!file_exists($system['system_uploads_directory'] . '/' . $folder . '/' . date('Y') . '/' . date('m'))) {
//                    @mkdir($depth . $system['system_uploads_directory'] . '/' . $folder . '/' . date('Y') . '/' . date('m'), 0777, true);
//                }
//
//                /* prepare new file name */
//                $directory = $folder . '/' . date('Y') . '/' . date('m') . '/';
//                $prefix = $system['uploads_prefix'] . '_' . md5(time() * rand(1, 9999));
//
//                $image = new Image($_FILES["file"]["tmp_name"]);
//
//                $file_name = $directory . $prefix . $image->_img_ext;
//                $file_name_tmp = $directory . $prefix . '_tmp' . $image->_img_ext;
//
//                $path = $depth . $system['system_uploads_directory'] . '/' . $file_name;
//                $path_tmp = $depth . $system['system_uploads_directory'] . '/' . $file_name_tmp;
//
//                /* check if the file uploaded successfully */
//                if (!@move_uploaded_file($_FILES['file']['tmp_name'], $path_tmp)) {
//                    _api_error(0, __("Sorry, can not upload the file"));
//                }
//
//                /* save the new image */
//                $image->save($path, $path_tmp);
//
//                /* delete the tmp image */
//                unlink($path_tmp);
//                $argsGR['source_file'] = $file_name;
//                $argsGR['file_name'] = $_FILES['file']['name'];
//
//            }
//
//            // Lấy thông tin insert vào mảng ci_child_growth
//            $argsGR['child_parent_id'] = $_POST['child_parent_id'];
//            $argsGR['height'] = $_POST['height'];
//            $argsGR['weight'] = $_POST['weight'];
//
//            // Lấy thông tin sức khỏe của trẻ
//            $health = $childDao->getChildHealth($_POST['child_parent_id']);
//
//            // Lấy thông tin chiều cao cân nặng của trẻ
//            $growth = $childDao->getChildGrowth($_POST['child_parent_id']);
//            $growth_recorded = array();
//            foreach ($growth as $row) {
//                $growth_recorded[] = $row['recorded_at'];
//            }
//            // $date_now = date('d/m/Y');
//            if(!isset($_POST['recorded_at']) || is_null($_POST['recorded_at'])) {
//                _api_error(0, __("Data record date can not be empty"));
//            }
//            $recorded_at = $_POST['recorded_at'];
//            $argsGR['recorded_at'] = $recorded_at;
//            if(in_array($recorded_at, $growth_recorded)) {
//                $growth_now = $childDao->getChildGrowthByDate($argsGR['recorded_at'], $_POST['child_parent_id']);
//                if(!isset($argsGR['source_file'])) {
//                    $argsGR['source_file'] = $growth_now['source_file_path'];
//                    $argsGR['file_name'] = $growth_now['file_name'];
//                }
//                $childDao->updateChildGrowthDateNow($argsGR);
//            } else {
//                $insertIdGR = $childDao->insertChildGrowth($argsGR);
//            }
//
//            if(is_null($health)) {
//                $insertId = $childDao->insertChildHealth($args);
//            } else {
//                $childDao->updateChildHealth($args);
//            }
//
//            $db->commit();
//            return_json(array(
//                'code' => 200,
//                'message' =>  __("Done, Child info have been updated"),
//                'data' => array()
//            ));
//            break;
        case 'add_growth':
            // Thêm thông tin chiều cao cân nặng của trẻ
            if (!isset($_POST['child_parent_id']) || !is_numeric($_POST['child_parent_id'])) {
                _api_error(404);
            }
            $db->begin_transaction();

            $file_name = "";
            // Upload file đính kèm
            if (file_exists($_FILES['file']['tmp_name'])) {
                // check file upload enabled
                if (!$system['file_enabled']) {
                    _api_error(0, __("This feature has been disabled"));
                }

                // valid inputs
                if (!isset($_FILES["file"]) || $_FILES["file"]["error"] != UPLOAD_ERR_OK) {
                    _api_error(0, __("Something wrong with upload! Is 'upload_max_filesize' set correctly?"));
                }

                // check file size
                $max_allowed_size = $system['max_file_size'] * 1024;
                if ($_FILES["file"]["size"] > $max_allowed_size) {
                    _api_error(0, __("The file size is so big"));
                }

                // check file extesnion
                $extension = get_extension($_FILES['file']['name']);
                if (!valid_extension($extension, $system['file_medicine_extensions'])) {
                    //modal(MESSAGE, __("Upload Error"), __("The file type is not valid or not supported"));
                    _api_error(0, __("The file type is not valid or not supported") );
                }

                /* check & create uploads dir */
                $depth = '';
                $folder = 'growths';
                if (!file_exists($depth . $system['system_uploads_directory'] . '/' . $folder)) {
                    @mkdir($depth . $system['system_uploads_directory'] . '/' . $folder, 0777, true);
                }
                if (!file_exists($depth . $system['system_uploads_directory'] . '/' . $folder . '/' . date('Y'))) {
                    @mkdir($depth . $system['system_uploads_directory'] . '/' . $folder . '/' . date('Y'), 0777, true);
                }
                if (!file_exists($system['system_uploads_directory'] . '/' . $folder . '/' . date('Y') . '/' . date('m'))) {
                    @mkdir($depth . $system['system_uploads_directory'] . '/' . $folder . '/' . date('Y') . '/' . date('m'), 0777, true);
                }

                /* prepare new file name */
                $directory = $folder . '/' . date('Y') . '/' . date('m') . '/';
                $prefix = $system['uploads_prefix'] . '_' . md5(time() * rand(1, 9999));

                $image = new Image($_FILES["file"]["tmp_name"]);

                $file_name = $directory . $prefix . $image->_img_ext;
                $file_name_tmp = $directory . $prefix . '_tmp' . $image->_img_ext;

                $path = $depth . $system['system_uploads_directory'] . '/' . $file_name;
                $path_tmp = $depth . $system['system_uploads_directory'] . '/' . $file_name_tmp;

                /* check if the file uploaded successfully */
                if (!@move_uploaded_file($_FILES['file']['tmp_name'], $path_tmp)) {
                    _api_error(0, __("Sorry, can not upload the file"));
                }

                /* save the new image */
                $image->save($path, $path_tmp);

                /* delete the tmp image */
                unlink($path_tmp);
                $argsGR['source_file'] = $file_name;
                $argsGR['file_name'] = $_FILES['file']['name'];

            }

            $argsGR['child_parent_id'] = $_POST['child_parent_id'];
            $argsGR['height'] = $_POST['height'];
            $argsGR['weight'] = $_POST['weight'];
            $argsGR['nutriture_status'] = $_POST['nutriture_status'];
            $argsGR['heart'] = $_POST['heart'];
            $argsGR['blood_pressure'] = $_POST['blood_pressure'];
            $argsGR['ear'] = $_POST['ear'];
            $argsGR['eye'] = $_POST['eye'];
            $argsGR['nose'] = $_POST['nose'];
            $argsGR['description'] = $_POST['description'];
            // Set người tạo là phụ huynh
            $argsGR['is_parent'] = 1;


            // Lấy thông tin chiều cao cân nặng của trẻ
            $growth = $childDao->getChildGrowth($_POST['child_parent_id']);
            $growth_recorded = array();
            foreach ($growth as $row) {
                $growth_recorded[] = $row['recorded_at'];
            }
            // $date_now = date('d/m/Y');
            if(!isset($_POST['recorded_at']) || is_null($_POST['recorded_at'])) {
                _api_error(0, __("Data record date can not be empty"));
            }
            $recorded_at = $_POST['recorded_at'];
            $argsGR['recorded_at'] = $recorded_at;
            if(in_array($recorded_at, $growth_recorded)) {
                $growth_now = $childDao->getChildGrowthByDate($argsGR['recorded_at'], $_POST['child_parent_id']);
                if(!isset($argsGR['source_file'])) {
                    $argsGR['source_file'] = $growth_now['source_file_path'];
                    $argsGR['file_name'] = $growth_now['file_name'];
                }
                $childDao->updateChildGrowthDateNow($argsGR);
            } else {
                $insertIdGR = $childDao->insertChildGrowth($argsGR);
                // Tăng lượt tương tác thêm mới - TaiLA
                addInteractive($childTemp['school_id'], 'health', 'parent_view', $insertIdGR, 2);
            }

            $db->commit();
            return_json(array(
                'code' => 200,
                'message' =>  __("Done, Child info has been created"),
                'data' => array()
            ));
            break;
        case 'edit_growth':
            // Thay đổi thông tin chiều cao, cân nặng của trẻ
            if(!isset($_POST['child_growth_id']) || !is_numeric($_POST['child_growth_id'])) {
                _api_error(404);
            }
            $db->begin_transaction();
            $child_growth = $childDao->getChildGrowthById($_POST['child_growth_id']);
            $args = array();
            $args['recorded_at'] = $_POST['recorded_at'];
            $args['height'] = $_POST['height'];
            $args['weight'] = $_POST['weight'];
            $args['nutriture_status'] = $_POST['nutriture_status'];
            $args['heart'] = $_POST['heart'];
            $args['blood_pressure'] = $_POST['blood_pressure'];
            $args['ear'] = $_POST['ear'];
            $args['eye'] = $_POST['eye'];
            $args['nose'] = $_POST['nose'];
            $args['description'] = $_POST['description'];
            $args['child_growth_id'] = $_POST['child_growth_id'];

            // Lấy thông tin chiều cao cân nặng của trẻ
            $growth = $childDao->getChildGrowth($_POST['child_parent_id']);
            $growth_recorded = array();
            foreach ($growth as $row) {
                $growth_recorded[] = $row['recorded_at'];
            }
            if(in_array($args['recorded_at'], $growth_recorded) && ($args['recorded_at'] != $child_growth['recorded_at'])) {
                throw new Exception(__("Selected date existed"));
            }

            $args['source_file'] = $child_growth['source_file_path'];
            $args['file_name'] = convertText4Web($child_growth['file_name']);

            $file_name = "";
            // Upload file đính kèm
            if (file_exists($_FILES['file']['tmp_name'])) {
                // check file upload enabled
                if (!$system['file_enabled']) {
                    _api_error(0, __("This feature has been disabled"));
                }

                // valid inputs
                if (!isset($_FILES["file"]) || $_FILES["file"]["error"] != UPLOAD_ERR_OK) {
                    _api_error(0, __("Something wrong with upload! Is 'upload_max_filesize' set correctly?"));
                }

                // check file size
                $max_allowed_size = $system['max_file_size'] * 1024;
                if ($_FILES["file"]["size"] > $max_allowed_size) {
                    _api_error(0, __("The file size is so big"));
                }

                // check file extesnion
                $extension = get_extension($_FILES['file']['name']);
                if (!valid_extension($extension, $system['file_medicine_extensions'])) {
                    //modal(MESSAGE, __("Upload Error"), __("The file type is not valid or not supported"));
                    _api_error(0, __("The file type is not valid or not supported") );
                }

                /* check & create uploads dir */
                $depth = '';
                $folder = 'growths';
                if (!file_exists($depth . $system['system_uploads_directory'] . '/' . $folder)) {
                    @mkdir($depth . $system['system_uploads_directory'] . '/' . $folder, 0777, true);
                }
                if (!file_exists($depth . $system['system_uploads_directory'] . '/' . $folder . '/' . date('Y'))) {
                    @mkdir($depth . $system['system_uploads_directory'] . '/' . $folder . '/' . date('Y'), 0777, true);
                }
                if (!file_exists($system['system_uploads_directory'] . '/' . $folder . '/' . date('Y') . '/' . date('m'))) {
                    @mkdir($depth . $system['system_uploads_directory'] . '/' . $folder . '/' . date('Y') . '/' . date('m'), 0777, true);
                }

                /* prepare new file name */
                $directory = $folder . '/' . date('Y') . '/' . date('m') . '/';
                $prefix = $system['uploads_prefix'] . '_' . md5(time() * rand(1, 9999));

                $image = new Image($_FILES["file"]["tmp_name"]);

                $file_name = $directory . $prefix . $image->_img_ext;
                $file_name_tmp = $directory . $prefix . '_tmp' . $image->_img_ext;

                $path = $depth . $system['system_uploads_directory'] . '/' . $file_name;
                $path_tmp = $depth . $system['system_uploads_directory'] . '/' . $file_name_tmp;

                /* check if the file uploaded successfully */
                if (!@move_uploaded_file($_FILES['file']['tmp_name'], $path_tmp)) {
                    _api_error(0, __("Sorry, can not upload the file"));
                }

                /* save the new image */
                $image->save($path, $path_tmp);

                /* delete the tmp image */
                unlink($path_tmp);
                $args['source_file'] = $file_name;
                $args['file_name'] = $_FILES['file']['name'];

            }

            $childDao->updateChildGrowth($args);

            $db->commit();
            return_json(array(
                'code' => 200,
                'message' =>  __("Done, Child info have been updated"),
                'data' => array()
            ));
            break;
        case 'add_foetus':
            // Thêm thông tin sức khỏe thai nhi
            if(!isset($_POST['child_parent_id']) || !is_numeric($_POST['child_parent_id'])) {
                _api_error(404);
            }
            $db->begin_transaction();
            $args = array();

            $args['child_parent_id'] = $_POST['child_parent_id'];
            $args['recorded_at'] = $_POST['recorded_at'];
            $args['pregnant_week'] = $_POST['pregnant_week'];
            $args['due_date_of_childbearing'] = $_POST['due_date_of_childbearing'];
            $args['femus_length'] = $_POST['femus_length'];
            $args['from_head_to_hips_length'] = $_POST['from_head_to_hips_length'];
            $args['weight'] = $_POST['weight'];
//            $args['biparietal_diameter'] = $_POST['biparietal_diameter'];
//            $args['transverse_abdominal_diameter'] = $_POST['transverse_abdominal_diameter'];
//            $args['nuchal_translucency'] = $_POST['nuchal_translucency'];
            $args['fetal_heart'] = $_POST['fetal_heart'];
//            $args['placenta'] = $_POST['placenta'];
//            $args['umbilical_cord'] = $_POST['umbilical_cord'];
//            $args['amniotic_fluid'] = $_POST['amniotic_fluid'];
            $args['hospital'] = $_POST['hospital'];
            $args['hospital_address'] = $_POST['hospital_address'];
            $args['doctor_name'] = $_POST['doctor_name'];
            $args['doctor_phone'] = $_POST['doctor_phone'];
            $args['description'] = $_POST['description'];

            // Thêm thông tin trẻ vào bảng ci_child_foetus_growth
            $child_foetus_growth_id = $childDao->createChildFoetusGrowth($args);

            // Tăng số lượt tương tác - TaiLA
            addInteractive($childTemp['school_id'], 'health', 'parent_view', $child_foetus_growth_id, 2);

            $db->commit();
            return_json(array(
                'code' => 200,
                'message' =>  __("Done, Child foetus info has been created"),
                'data' => array()
            ));
            break;

        case 'edit_foetus':
            // Thay đổi thông tin sức khỏe thai nhi
            if(!isset($_POST['child_foetus_growth_id']) || !is_numeric($_POST['child_foetus_growth_id'])) {
                _api_error(404);
            }
            $db->begin_transaction();
            $args = array();

            $args['child_foetus_growth_id'] = $_POST['child_foetus_growth_id'];
            $args['recorded_at'] = $_POST['recorded_at'];
            $args['pregnant_week'] = $_POST['pregnant_week'];
            $args['due_date_of_childbearing'] = $_POST['due_date_of_childbearing'];
            $args['femus_length'] = $_POST['femus_length'];
            $args['from_head_to_hips_length'] = $_POST['from_head_to_hips_length'];
            $args['weight'] = $_POST['weight'];
//            $args['biparietal_diameter'] = $_POST['biparietal_diameter'];
//            $args['transverse_abdominal_diameter'] = $_POST['transverse_abdominal_diameter'];
//            $args['nuchal_translucency'] = $_POST['nuchal_translucency'];
            $args['fetal_heart'] = $_POST['fetal_heart'];
//            $args['placenta'] = $_POST['placenta'];
//            $args['umbilical_cord'] = $_POST['umbilical_cord'];
//            $args['amniotic_fluid'] = $_POST['amniotic_fluid'];
            $args['hospital'] = $_POST['hospital'];
            $args['hospital_address'] = $_POST['hospital_address'];
            $args['doctor_name'] = $_POST['doctor_name'];
            $args['doctor_phone'] = $_POST['doctor_phone'];
            $args['description'] = $_POST['description'];

            // Thêm thông tin trẻ vào bảng ci_child_foetus_growth
            $childDao->updateChildFoetusGrowth($args);

            $db->commit();
            return_json(array(
                'code' => 200,
                'message' =>  __("Done, Child foetus info has been updated"),
                'data' => array()
            ));
            break;

        case 'delete_growth':
            if(!isset($_POST['child_parent_id']) || !is_numeric($_POST['child_parent_id'])) {
                _api_error(404);
            }
            if(!isset($_POST['child_growth_id']) || !is_numeric($_POST['child_growth_id'])) {
                _api_error(404);
            }

            $db->begin_transaction();

            $childDao->deleteChildGrowth($_POST['child_parent_id'], $_POST['child_growth_id']);

            $db->commit();
            return_json(array(
                'code' => 200,
                'message' =>  __("OK"),
                'data' => null
            ));
            break;
        case 'delete_foetus':
            if(!isset($_POST['child_parent_id']) || !is_numeric($_POST['child_parent_id'])) {
                _api_error(404);
            }
            if(!isset($_POST['child_foetus_growth_id']) || !is_numeric($_POST['child_foetus_growth_id'])) {
                _api_error(404);
            }

            $db->begin_transaction();

            $childDao->deleteChildFoetusGrowth($_POST['child_parent_id'], $_POST['child_foetus_growth_id']);

            $db->commit();
            return_json(array(
                'code' => 200,
                'message' =>  __("OK"),
                'data' => null
            ));
            break;
        case 'search':
            if(!isset($_POST['child_parent_id']) || !is_numeric(($_POST['child_parent_id']))) {
                _api_error(404);
            }

            $results = $childDao->getChildGrowthSearch($_POST['child_parent_id'], $_POST['year']);

            return_json(array(
                'code' => 200,
                'message' => __("OK"),
                'data' =>array(
                    'growths' => $results
                )
            ));
            break;
        default:
            _api_error(400);
            break;
    }

} catch (Exception $e) {
    $db->rollback();
    header('HTTP/1.1 400 API Error');
    return_json(array(
        'code' => 0,
        'message' => $e->getMessage(),
        'data' => array()
    ));
} finally {
    $db->autocommit(true);
}

?>