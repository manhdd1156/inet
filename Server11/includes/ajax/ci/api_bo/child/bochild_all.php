<?php
/**
 * Package: ajax/ci/bo
 * 
 * @package ConIu
 * @author QuanND
 */

// fetch image class
require_once(ABSPATH . 'includes/class-image.php');

include_once(DAO_PATH.'dao_child.php');
include_once(DAO_PATH.'dao_parent.php');
include_once(DAO_PATH.'dao_school.php');
include_once(DAO_PATH.'dao_class.php');
include_once(DAO_PATH.'dao_class_level.php');
include_once(DAO_PATH.'dao_teacher.php');
include_once(DAO_PATH.'dao_user.php');
include_once(DAO_PATH.'dao_event.php');
include_once(DAO_PATH.'dao_tuition.php');
include_once(DAO_PATH.'dao_attendance.php');
include_once(DAO_PATH.'dao_service.php');
include_once(DAO_PATH.'dao_report.php');
include_once(DAO_PATH.'dao_feedback.php');
include_once(DAO_PATH.'dao_schedule.php');
include_once(DAO_PATH.'dao_pickup.php');
include_once(DAO_PATH.'dao_menu.php');

$childDao = new ChildDAO();
$parentDao = new ParentDAO();
$schoolDao = new SchoolDAO();
$classDao = new ClassDAO();
$classLevelDao = new ClassLevelDAO();
$teacherDao = new TeacherDAO();
$userDao = new UserDAO();
$eventDao = new EventDAO();
$tuitionDao = new TuitionDAO();
$attendanceDao = new AttendanceDAO();
$serviceDao = new ServiceDAO();
$reportDao = new ReportDAO();
$feedbackDao = new FeedbackDAO();
$scheduleDao = new ScheduleDAO();
$pickupDao = new PickupDAO();
$menuDao = new MenuDAO();

if (!is_numeric($_POST['child_id'])) {
    _api_error(400);
}

//Lấy ra thông tin lớp
//if (!$childDao->isParent($_POST['child_id'])) {
//    _api_error(403);
//}
$child = getChildDataById($_POST['child_id'], CHILD_INFO);
if (is_null($child)) {
    _api_error(403);
}

try {
    $db->autocommit(false);

    switch ($_POST['do']) {
        case 'show_child':
            /*$child = $childDao->getChild($_POST['child_id']);
            if (is_null($child)) {
                _api_error(404);
            }*/

            //$class = $classDao->getClassOfChild($child['child_id']);
            $class = getChildData($child['child_id'], CHILD_CLASS);
            if (is_null($class)) {
                _api_error(404);
            }

            $class_info = array();
            $school_info = array();

            $class_info['group_id'] = $class['group_id'];
            $class_info['group_name'] = $class['group_name'];
            $class_info['group_title'] = $class['group_title'];
            $class_info['group_description'] = $class['group_description'];
            $class_info['school_id'] = $class['school_id'];

            //$teachers = $teacherDao->getTeacherOfClass($class['group_id']);
            $teachers = array();
            $class_teachers = getClassData($class['group_id'], CLASS_TEACHERS);
            $class_teachers = sortArray($class_teachers, "ASC", "user_fullname");
            foreach ($class_teachers as $teacher) {
                $teachers[] = $teacher;
            }
            //$school = $schoolDao->getSchoolByClass($class['group_id']);
            $school = getSchoolData($class['school_id'], SCHOOL_DATA);

            $showChildrenList = (isset($school['display_children_list']) && $school['display_children_list']) ? true : false;
            $includeParent = (isset($school['display_parent_info_for_others']) && $school['display_parent_info_for_others']) ? true : false;
            $school_info['page_id'] = $school['page_id'];
            $school_info['page_name'] = $school['page_name'];
            $school_info['page_title'] = $school['page_title'];
            $school_info['page_description'] = $school['page_description'];
            $school_info['telephone'] = $school['telephone'];

            //$admin = $schoolDao->getAdmin($school['page_id']);
            //$school_info['admin'] = $admin;
            $admin = $schoolDao->getAdminById($school['page_admin']);
            $school_info['admin'] = $admin;

            $children = array();
            if ($showChildrenList) {
                //$childrens = $childDao->getChildrenOfClass($class['group_id'], $includeParent);
                $childs = getClassData($class['group_id'], CLASS_CHILDREN);
                $childs = sortArray($childs, "ASC", "name_for_sort");
                foreach ($childs as $child) {
                    if ($includeParent) {
                        //$childrens = $childDao->getChildrenOfClass($class['group_id'], $includeParent);
                        $childParent = array();
                        $parents = getChildData($child['child_id'], CHILD_PARENTS);
                        foreach ($parents as $parent) {
                            $childParent[] = $parent;
                        }
                        $child['parent'] = $childParent;
                    }
                    $children[] = $child;
                }
            }

            // return
            return_json(array(
                'code' => 200,
                'message' => 'OK',
                'data' => array(
                    'school' => $school_info,
                    'class' => $class_info,
                    'teacher' => $teachers,
                    'child_list' => $children
                )
            ));

            break;

        case 'change_child':
            return_json(array('callback' => 'window.location = "'.$system['system_url'].'/child/'.$_POST['child_id'].'";'));
            break;
        case 'event':
            /**
             * Xứ lý khi user đăng ký/hủy tham gia sự kiện
             */
            // Tăng lượt tương tác thêm mới - TaiLA
            addInteractive($child['school_id'], 'event', 'parent_view');
            $db->begin_transaction();

            if ($_POST['register'] == 'true') {
                if ($_POST['type'] == PARTICIPANT_TYPE_CHILD) {
                    $eventDao->insertParticipants($_POST['event_id'], PARTICIPANT_TYPE_CHILD, [$_POST['child_id']]);

                    //Thông báo cho Quản lý và Giáo viên lớp (tắt)
                    // notifySchoolManagerAndTearcherOfView($_POST['child_id'], NOTIFICATION_REGISTER_EVENT_CHILD, $_POST['event_id'], $_POST['event_name'], '', 'events');
                } else {
                    $eventDao->insertParticipants($_POST['event_id'], PARTICIPANT_TYPE_PARENT, [$user->_data['user_id']]);
                    //Thông báo cho Quản lý và Giáo viên lớp (tắt)
//                    notifySchoolManagerAndTearcherOfView($_POST['child_id'], NOTIFICATION_REGISTER_EVENT_PARENT, $_POST['event_id'],
//                        $_POST['event_name'], convertText4Web($user->_data['user_fullname']), 'events');
                }
            } else {
                if ($_POST['type'] == PARTICIPANT_TYPE_CHILD) {
                    $eventDao->deleteParticipants($_POST['event_id'], PARTICIPANT_TYPE_CHILD, [$_POST['child_id']]);
                    //Thông báo cho Quản lý và Giáo viên lớp (tắt)
//                    notifySchoolManagerAndTearcherOfView($_POST['child_id'], NOTIFICATION_CANCEL_REGISTRATION_EVENT_CHILD, $_POST['event_id'], $_POST['event_name'], '', 'events');
                } else {
                    $eventDao->deleteParticipants($_POST['event_id'], PARTICIPANT_TYPE_PARENT, [$user->_data['user_id']]);
                    //Thông báo cho Quản lý và Giáo viên lớp (tắt)
//                    notifySchoolManagerAndTearcherOfView($_POST['child_id'], NOTIFICATION_CANCEL_REGISTRATION_EVENT_PARENT, $_POST['event_id'],
//                        $_POST['event_name'], convertText4Web($user->_data['user_fullname']), 'events');
                }
            }
            $db->commit();
            break;

        case 'attendance':
            // Tăng lượt tương tác thêm mới - TaiLA
            addInteractive($child['school_id'], 'attendance', 'parent_view');
            $data = array();
            $attendances = $attendanceDao->getAttendanceChild($_POST['child_id'], $_POST['fromDate'], $_POST['toDate']);
            // return
            return_json(array(
                'code' => 200,
                'message' => 'OK',
                'data' => $attendances,
            ));

            break;

        case 'service_history':
            // Tăng lượt tương tác thêm mới - TaiLA
            addInteractive($child['school_id'], 'service', 'parent_view');

            $history = array();
            //Validate thông tin đầu vào
            $inputCorrect = true;
            if (!isset($_POST['service_id']) || ($_POST['service_id'] <= 0)) {
                $inputCorrect = false;
            }
            if (!isset($_POST['begin']) || !validateDate($_POST['begin'])
            || !isset($_POST['end']) || !validateDate($_POST['end'])) {
                $inputCorrect = false;
            }

            if ($inputCorrect) {
                $history = $serviceDao->getServiceHistory($_POST['child_id'], $_POST['service_id'], $_POST['begin'], $_POST['end']);
                // return & exit
                return_json(array(
                    'code' => 200,
                    'message' => 'OK',
                    'data' => array(
                        'history' => $history
                    )
                ));
            } else {
                _api_error(0, __("No data or incorrect input data"));
            }

            break;

        case 'register_not_countbased':
            // Tăng lượt tương tác thêm mới - TaiLA
            addInteractive($child['school_id'], 'service', 'parent_view');
            $ncbServiceIds = isset($_REQUEST['list_service_id'])? $_REQUEST['list_service_id']: array(); //ID của dịch vụ
            //Danh sách dịch vụ đăng ký mới của trẻ

            if (count($ncbServiceIds) > 0) {
                $db->begin_transaction();

                //Đăng ký dịch vụ THÁNG và theo ĐIỂM DANH
                $serviceDao->registerNotCountBasedServiceForChild($_POST['child_id'], $ncbServiceIds);

                foreach ($ncbServiceIds as $id) {
                    //memcache???
                    $service = $serviceDao->getServiceById($id);
                    //Thông báo cho Quản lý và Giáo viên lớp
                    notifySchoolManagerAndTearcherOfView($_POST['child_id'], NOTIFICATION_SERVICE_REGISTER_UNCOUNTBASED,
                        $id, convertText4Web($service['service_name']), '', 'services');
                }
                $db->commit();
            }

            // return
            return_json(array(
                'code' => 200,
                'message' =>  __("Done, Registration info have been updated"),
                'data' => array()
            ));

            break;

        case 'register_countbased':
            // Tăng lượt tương tác thêm mới - TaiLA
            addInteractive($child['school_id'], 'service', 'parent_view');
            $cbServiceIds = isset($_REQUEST['list_service_id'])? $_REQUEST['list_service_id']: array(); //ID của dịch vụ

            //Lấy ra danh sách ID dịch vụ THEO LẦN đã đăng ký
            $registeredIds = $serviceDao->getRegisteredCountBasedServiceIdAChild($_POST['child_id'], $_POST['using_at']);
            $newServiceIds = array_diff($cbServiceIds, $registeredIds);
            if (count($newServiceIds) > 0) {
                $db->begin_transaction();

                //Đăng ký dịch vụ theo SỐ LẦN
                $serviceDao->registerCountBasedServiceForChild($_POST['child_id'], $newServiceIds, $_POST['using_at']);

                foreach ($newServiceIds as $id) {
                    $service = $serviceDao->getServiceById($id);
                    //Thông báo cho Quản lý và Giáo viên lớp
                    notifySchoolManagerAndTearcherOfView($_POST['child_id'], NOTIFICATION_SERVICE_REGISTER_COUNTBASED,
                        $id, convertText4Web($service['service_name']), '', 'services');
                }
                $db->commit();
            }

            // return
            return_json(array(
                'code' => 200,
                'message' =>  __("Done, Registration info have been updated"),
                'data' => array()
            ));

            break;

        case 'resign':
            // Tăng lượt tương tác thêm mới - TaiLA
            addInteractive($child['school_id'], 'attendance', 'parent_view');
            $db->begin_transaction();
            //Xử lý sự kiện khi phụ huynh xin nghỉ cho con
            $args = array();
            $atds = array();

            //1. Lấy ra thông tin lớp của trẻ
            //$class = $classDao->getClassOfChild($_POST['child_id']);
            $class = getChildData($_POST['child_id'], CHILD_CLASS);
            $args['class_id'] = $class['group_id'];

            $start_date = toDBDate($_POST['start_date']);
            $end_date = toDBDate($_POST['end_date']);
            if (strtotime($start_date) > strtotime($end_date)) {
                throw new Exception(__("You must enter a valid date"));
            }
            $atds['start_date'] = $start_date;
            $atds['end_date'] = $end_date;

            $sent = false;
            $detail_id = 0;
            while (strtotime($start_date) <= strtotime($end_date)) {
                $args['attendance_date'] = toSysDate($start_date);
                //2. Kiểm tra xem đã có bản ghi
                $attendance = $attendanceDao->getAttendanceByClassNDate($args['class_id'], $args['attendance_date']);
                $atds['attendance_id'] = 0;
                if (is_null($attendance)) {
                    $args['absence_count'] = 0;
                    $args['present_count'] = 0;
                    $args['is_checked'] = 0;
                    //Lấy ra danh sách ID của tất cả trẻ trong lớp
                    $atds['attendance_id'] = $attendanceDao->insertAttendance($args);
                } else {
                    $atds['attendance_id'] = $attendance['attendance_id'];
                }
                $atds['child_id'] = $_POST['child_id'];
                $atds['reason'] = $_POST['reason'];
                $atds['isParent'] = 1;

                $result = $attendanceDao->saveAbsentChild($atds);

                $start_date = date ("Y-m-d", strtotime("+1 day", strtotime($start_date)));
                if (!$sent && $result['is_updated']) {
                    $detail_id = $result['attendance_detail_id'];
                    $sent = true;
                }
            }

            if ($detail_id > 0) {
                //Thông báo quản lý trường và giáo viên về việc xin nghỉ của con
                notifySchoolManagerAndTearcherOfView($_POST['child_id'], NOTIFICATION_ATTENDANCE_RESIGN,
                    $detail_id, '', '', 'attendance');
            }

            $db->commit();
            // return
            return_json(array(
                'code' => 200,
                'message' => 'OK',
                'data' => array()
            ));

            break;

        case 'feedback':
            $return = array();
            //Validate thông tin đầu vào
            if (!isset($_POST['child_id']) || ($_POST['child_id'] <= 0)) {
                _api_error(404);
            }
            if ($_POST['feedback_level'] != CLASS_LEVEL && $_POST['feedback_level'] != SCHOOL_LEVEL) {
                _api_error(404);
            }
            //1. Lấy data để insert vào bảng ci_feedback
            // Lấy ra thông tin lớp của trẻ
            //$class = $classDao->getClassOfChild($_POST['child_id']);
            $class = getChildData($_POST['child_id'], CHILD_CLASS);
            if (is_null($class)) {
                _api_error(404);
            }
            $args['child_id'] = $_POST['child_id'];
            $args['class_id'] = $class['group_id'];
            $args['school_id'] = $class['school_id'];
            $args['name'] = convertText4Web($user->_data['user_fullname']);
            $args['email'] = convertText4Web($user->_data['user_email']);
            $args['phone'] = $user->_data['user_phone'];
            $args['content'] = $_POST['content'];
            $args['level'] = $_POST['feedback_level'];
            $args['confirm'] = 0;
            $args['is_incognito'] = (isset($_POST['incognito']) && $_POST['incognito'] == 'on')? 1: 0;
            $args['user_id'] = $user->_data['user_id'];

            $feedback_id = $feedbackDao->insertFeedback($args);
            // Tăng lượt tương tác thêm mới - TaiLA
            addInteractive($class['school_id'], 'feedback', 'parent_view', $feedback_id, 1);

                /*if ($args['level'] == CLASS_LEVEL) {
                    //Thông báo giáo viên về góp ý của phụ huynh
                    $teacherIds = $teacherDao->getTeacherIDOfClass($args['class_id']);
                    $userDao->postNotifications($teacherIds, NOTIFICATION_NEW_FEEDBACK, NOTIFICATION_NODE_TYPE_CLASS, $feedback_id);
                } else*/
            if ($args['level'] == SCHOOL_LEVEL) {
                //$school = $schoolDao->getSchoolById($args['school_id']);
                $school = getSchoolData($args['school_id'], SCHOOL_INFO);

                //Notify trạng thái cho quản lý trường
                notifySchoolManagerOfView($_POST['child_id'], NOTIFICATION_NEW_FEEDBACK, $feedback_id, '', convertText4Web($school['page_title']), 'feedback');

//                $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'feedback', $school['page_admin']);
//                $userDao->postNotifications($userManagerIds, NOTIFICATION_NEW_FEEDBACK, NOTIFICATION_NODE_TYPE_SCHOOL, $feedback_id, '', $school['page_name'], convertText4Web($school['page_title']));
            }
            $db->commit();
            return_json(array(
                'code' => 200,
                'message' => __("Góp ý đã được gửi đi, xin chờ phản hồi từ phía nhà trường."),
                'data' => array()
            ));
            break;

        case 'schedule':
            // Tăng lượt tương tác thêm mới - TaiLA
            addInteractive($child['school_id'], 'schedule', 'parent_view');
            $return = array();

            //$class = $classDao->getClassOfChild($_POST['child_id']);
            $class = getChildData($_POST['child_id'], CHILD_CLASS);
            if ($class == null) {
                _api_error(404);
            }

            //Lấy ra danh sách lịch học áp dụng cho lớp
            $data = $scheduleDao->getScheduleOfSchoolById($class['school_id'], $class['class_level_id'], $class['group_id']);
            $removeArr = array();
            for($i = 0; $i < count($data); $i++) {
                for($j = 0; $j < count($data); $j++) {
                    if($i != $j) {
                        $beginI = strtotime(toDBDate($data[$i]['begin']));
                        $beginJ = strtotime(toDBDate($data[$j]['begin']));
                        if($beginI == $beginJ) {
                            if($data[$i]['applied_for'] > $data[$j]['applied_for']) {
                                $removeArr[] = $j;
                            }
                        }
                    }
                }
            }
            $schedules = array();

            for($k = 0; $k < count($data); $k++) {

                $schedule = $scheduleDao->getScheduleDetailById($data[$k]['schedule_id']);

                if($schedule['applied_for'] == 1) {
                    $schedule['level'] =  __("School");
                } elseif($schedule['applied_for'] == 2) {
                    //$class_level = $classLevelDao->getClassLevel($schedule['class_level_id']);
                    $class_level = getClassLevelData($schedule['class_level_id'], CLASS_LEVEL_INFO);
                    $schedule['level'] = $class_level['class_level_name'];
                } elseif($schedule['applied_for'] == 3) {
                    //$classes = $classDao->getClass($schedule['class_id']);
                    $classes = getClassData($schedule['class_id'], CLASS_INFO);
                    $schedule['level'] = $classes['group_title'];
                }
                if(!in_array($k, $removeArr)) {
                    $schedule['use'] = STATUS_ACTIVE;
                } else {
                    $schedule['use'] = STATUS_INACTIVE;
                }
                $schedules[] = $schedule;
            }
            return_json(array(
                'code' => 200,
                'message' => 'OK',
                'data' => array(
                    'schedule_child' => $schedules
                )
            ));
            break;
        case 'menu':
            // Tăng lượt tương tác thêm mới - TaiLA
            addInteractive($child['school_id'], 'menu', 'parent_view');
            $return = array();

            //$class = $classDao->getClassOfChild($_POST['child_id']);
            $class = getChildData($_POST['child_id'], CHILD_CLASS);
            if ($class == null) {
                _api_error(404);
            }

            //Lấy ra danh sách lịch học áp dụng cho lớp
            $data = $menuDao->getMenuOfSchoolById($class['school_id'], $class['class_level_id'], $class['group_id']);
            $removeArr = array();
            for($i = 0; $i < count($data); $i++) {
                for($j = 0; $j < count($data); $j++) {
                    if($i != $j) {
                        $beginI = strtotime(toDBDate($data[$i]['begin']));
                        $beginJ = strtotime(toDBDate($data[$j]['begin']));
                        if($beginI == $beginJ) {
                            if($data[$i]['applied_for'] > $data[$j]['applied_for']) {
                                $removeArr[] = $j;
                            }
                        }
                    }
                }
            }
            $menus = array();

            for($k = 0; $k < count($data); $k++) {
                $menu = $menuDao->getMenuDetailById($data[$k]['menu_id']);

                if($menu['applied_for'] == 1) {
                    $menu['level'] =  __("School");
                } elseif($menu['applied_for'] == 2) {
                    //$class_level = $classLevelDao->getClassLevel($menu['class_level_id']);
                    $class_level = getClassLevelData($menu['class_level_id'], CLASS_LEVEL_INFO);
                    $menu['level'] = $class_level['class_level_name'];
                } elseif($menu['applied_for'] == 3) {
                    //$classes = $classDao->getClass($menu['class_id']);
                    $classes = getClassData($menu['class_id'], CLASS_INFO);
                    $menu['level'] = $classes['group_title'];
                }
                if(!in_array($k, $removeArr)) {
                    $menu['use'] = STATUS_ACTIVE;
                } else {
                    $menu['use'] = STATUS_INACTIVE;
                }
                $menus[] = $menu;
            }
            return_json(array(
                'code' => 200,
                'message' => 'OK',
                'data' => array(
                    'menu_child' => $menus
                )
            ));
            break;

        case 'picker_list':
            // Tăng lượt tương tác thêm mới - TaiLA
            addInteractive($child['school_id'], 'picker', 'parent_view');
            $return = array();
            //Validate thông tin đầu vào
            if (!isset($_POST['child_id']) || ($_POST['child_id'] <= 0)) {
                _api_error(404);
            }

            //$childInfo = $childDao->getChildInformation($_POST['child_id']);
            $childInfo = getChildData($_POST['child_id'], CHILD_PICKER_INFO);
            unset($childInfo['source_file']);
            return_json(array(
                'code' => 200,
                'message' => __("OK"),
                'data' => $childInfo
            ));
            break;

        case 'edit_picker':
            // Tăng lượt tương tác thêm mới - TaiLA
            addInteractive($child['school_id'], 'picker', 'parent_view');
            $return = array();
            //Validate thông tin đầu vào
            if (!isset($_POST['child_info_id']) || ($_POST['child_info_id'] <= 0)) {
                _api_error(404);
            }
            if (!isset($_POST['child_id']) || !is_numeric($_POST['child_id'])) {
                _api_error(404);
            }
            //1. Lấy ra thông tin lớp của trẻ
            //$class = $classDao->getClassOfChild($_POST['child_id']);
            $class = getChildData($_POST['child_id'], CHILD_CLASS);
            if (is_null($class)) {
                _api_error(404);
            }
            //$data = $childDao->getChildInformation($_POST['child_id']);
            $data = getChildData($_POST['child_id'], CHILD_PICKER_INFO);

            $db->begin_transaction();

            $args = array();
            $args['child_id'] = $_POST['child_id'];
            $args['child_info_id'] = $_POST['child_info_id'];
            $args['picker_name'] = $_POST['picker_name'];
            $args['picker_relation'] = $_POST['picker_relation'];
            $args['picker_phone'] = $_POST['picker_phone'];
            $args['picker_address'] = $_POST['picker_address'];
            $args['source_file'] = $data['source_file'];
            $args['file_name'] = convertText4Web($data['picker_file_name']);

            $file_name = "";
            // Upload file đính kèm
            if(file_exists($_FILES['file']['tmp_name'])) {
                // check file upload enabled
                if(!$system['file_enabled']) {
                    _api_error(0, __("This feature has been disabled"));
                }

                // valid inputs
                if(!isset($_FILES["file"]) || $_FILES["file"]["error"] != UPLOAD_ERR_OK) {
                    _api_error(0, __("Something wrong with upload! Is 'upload_max_filesize' set correctly?"));
                }

                // check file size
                $max_allowed_size = $system['max_file_size'] * 1024;
                if($_FILES["file"]["size"] > $max_allowed_size) {
                    _api_error(0, __("The file size is so big"));
                }

                // check file extesnion
                $extension = get_extension($_FILES['file']['name']);
                if(!valid_extension($extension, $system['file_medicine_extensions'])) {
                    //modal(MESSAGE, __("Upload Error"), __("The file type is not valid or not supported"));
                    _api_error(0, __("The file type is not valid or not supported") );
                }

                /* check & create uploads dir */
                $depth = '';
                $folder = 'informations/'. $class['school_id'];
                if(!file_exists($depth.$system['system_uploads_directory'].'/'.$folder)) {
                    @mkdir($depth.$system['system_uploads_directory'].'/'.$folder, 0777, true);
                }
                if(!file_exists($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y'))) {
                    @mkdir($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y'), 0777, true);
                }
                if(!file_exists($system['system_uploads_directory'].'/'.$folder.'/' . date('Y') . '/' . date('m'))) {
                    @mkdir($depth.$system['system_uploads_directory'].'/'.$folder.'/' . date('Y') . '/' . date('m'), 0777, true);
                }

                /* prepare new file name */
                $directory = $folder.'/'. date('Y') . '/' . date('m') . '/';
                $prefix = $system['uploads_prefix'].'_'.md5(time()*rand(1, 9999));

                $image = new Image($_FILES["file"]["tmp_name"]);

                $file_name = $directory.$prefix.$image->_img_ext;
                $file_name_tmp = $directory.$prefix.'_tmp'.$image->_img_ext;

                $path = $depth.$system['system_uploads_directory'].'/'.$file_name;
                $path_tmp = $depth.$system['system_uploads_directory'].'/'.$file_name_tmp;

                /* check if the file uploaded successfully */
                if(!@move_uploaded_file($_FILES['file']['tmp_name'], $path_tmp)) {
                    _api_error(0, __("Sorry, can not upload the file"));
                }
                /* save the new image */
                $image->save($path, $path_tmp);

                /* delete the tmp image */
                unlink($path_tmp);
                $args['source_file'] = $file_name;
                $args['file_name'] = $_FILES['file']['name'];
            } else {
                $args['source_file'] = '';
                $args['file_name'] = '';
            }

            $childDao->updateChildInformation($args);

            //Thông báo quản lý trường, cô giáo và phụ huynh
            //$child = $childDao->getChild($args['child_id']);
//            $child = getChildData($args['child_id'], CHILD_INFO);
//            notifySchoolManagerAndTearcherOfView($args['child_id'], NOTIFICATION_UPDATE_PICKER, $args['child_id'], '', convertText4Web($child['child_name']), 'children');

            /* ---------- UPDATE - MEMCACHE ---------- */
            //1.Cập nhật thông tin trẻ
            updateChildData($args['child_id'], CHILD_PICKER_INFO);
            /* ---------- END - MEMCACHE ---------- */
            
            $db->commit();
            return_json(array(
                'code' => 200,
                'message' => __("Done, Picker info have been updated"),
                'data' => array()
            ));
            break;

        case 'add_picker':
            // Tăng lượt tương tác thêm mới - TaiLA
            addInteractive($child['school_id'], 'picker', 'parent_view');
            if (!isset($_POST['child_id']) || !is_numeric($_POST['child_id'])) {
                _error(404);
            }
            //1. Lấy ra thông tin lớp của trẻ
            //$class = $classDao->getClassOfChild($_POST['child_id']);
            $class = getChildData($_POST['child_id'], CHILD_CLASS);

            //$data = $childDao->getChildInformation($_POST['child_id']);
            $data = getChildData($_POST['child_id'], CHILD_PICKER_INFO);

            $db->begin_transaction();
            $args = array();
            $args['child_id'] = $_POST['child_id'];
            $args['picker_name'] = $_POST['picker_name'];
            $args['picker_relation'] = $_POST['picker_relation'];
            $args['picker_phone'] = $_POST['picker_phone'];
            $args['picker_address'] = $_POST['picker_address'];
            $args['source_file'] = $data['source_file'];
            $args['file_name'] = convertText4Web($data['picker_file_name']);


            $file_name = "";
            // Upload file đính kèm
            if (file_exists($_FILES['file']['tmp_name'])) {
                // check file upload enabled
                if (!$system['file_enabled']) {
                    modal(MESSAGE, __("Not Allowed"), __("This feature has been disabled"));
                }

                // valid inputs
                if (!isset($_FILES["file"]) || $_FILES["file"]["error"] != UPLOAD_ERR_OK) {
                    modal(MESSAGE, __("Upload Error"), __("Something wrong with upload! Is 'upload_max_filesize' set correctly?"));
                }

                // check file size
                $max_allowed_size = $system['max_file_size'] * 1024;
                if ($_FILES["file"]["size"] > $max_allowed_size) {
                    modal(MESSAGE, __("Upload Error"), __("The file size is so big"));
                }

                // check file extesnion
                $extension = get_extension($_FILES['file']['name']);
                if (!valid_extension($extension, $system['file_medicine_extensions'])) {
                    //modal(MESSAGE, __("Upload Error"), __("The file type is not valid or not supported"));
                    return_json(array('error' => true, 'message' => __("The file type is not valid or not supported")));
                }

                /* check & create uploads dir */
                $depth = '';
                $folder = 'informations/' . $class['school_id'];
                if (!file_exists($depth . $system['system_uploads_directory'] . '/' . $folder)) {
                    @mkdir($depth . $system['system_uploads_directory'] . '/' . $folder, 0777, true);
                }
                if (!file_exists($depth . $system['system_uploads_directory'] . '/' . $folder . '/' . date('Y'))) {
                    @mkdir($depth . $system['system_uploads_directory'] . '/' . $folder . '/' . date('Y'), 0777, true);
                }
                if (!file_exists($system['system_uploads_directory'] . '/' . $folder . '/' . date('Y') . '/' . date('m'))) {
                    @mkdir($depth . $system['system_uploads_directory'] . '/' . $folder . '/' . date('Y') . '/' . date('m'), 0777, true);
                }

                /* prepare new file name */
                $directory = $folder . '/' . date('Y') . '/' . date('m') . '/';
                $prefix = $system['uploads_prefix'] . '_' . md5(time() * rand(1, 9999));

                $image = new Image($_FILES["file"]["tmp_name"]);

                $file_name = $directory . $prefix . $image->_img_ext;
                $file_name_tmp = $directory . $prefix . '_tmp' . $image->_img_ext;

                $path = $depth . $system['system_uploads_directory'] . '/' . $file_name;
                $path_tmp = $depth . $system['system_uploads_directory'] . '/' . $file_name_tmp;

                /* check if the file uploaded successfully */
                if (!@move_uploaded_file($_FILES['file']['tmp_name'], $path_tmp)) {
                    modal(MESSAGE, __("Upload Error"), __("Sorry, can not upload the file"));
                }

                /* save the new image */
                $image->save($path, $path_tmp);

                /* delete the tmp image */
                unlink($path_tmp);
                $args['source_file'] = $file_name;
                $args['file_name'] = $_FILES['file']['name'];

            }
            // Insert vào bảng ci_child_info
            $childDao->insertChildInfomation($args);

            //$child = $childDao->getChild($args['child_id']);
            //$child = getChildData($args['child_id'], CHILD_INFO);
//            if(!is_null($child)) {
//                notifySchoolManagerAndTearcherOfView($args['child_id'], NOTIFICATION_UPDATE_PICKER, $args['child_id'], '', convertText4Web($child['child_name']), 'children');
//            }
            $db->commit();

            /* ---------- UPDATE - MEMCACHE ---------- */
            //1.Cập nhật thông tin trẻ
            updateChildData($args['child_id'], CHILD_PICKER_INFO);
            /* ---------- END - MEMCACHE ---------- */

            return_json(array(
                'code' => 200,
                'message' => __("Done, Picker info have been created"),
                'data' => array()
            ));
            break;
        case 'detail_picker':
            // Tăng lượt tương tác thêm mới - TaiLA
            addInteractive($child['school_id'], 'picker', 'parent_view');
            if(!isset($_POST['child_info_id']) || !is_numeric($_POST['child_info_id'])) {
                _api_error(404);
            }
            $picker = $childDao->getChildInformationDetail($_POST['child_info_id']);

            return_json(array(
                'code' => 200,
                'message' => __("OK"),
                'data' => array(
                    'picker' => $picker
                )
            ));
            break;

        case 'delete_picker':
            // Tăng lượt tương tác thêm mới - TaiLA
            addInteractive($child['school_id'], 'picker', 'parent_view');
            $db->begin_transaction();
            if(!isset($_POST['child_info_id']) || !is_numeric($_POST['child_info_id'])) {
                _api_error(404);
            }
            $childDao->deleteChildInformation($_POST['child_info_id']);

            $db->commit();

            /* ---------- UPDATE - MEMCACHE ---------- */
            //1.Cập nhật thông tin trẻ
            updateChildData($_POST['child_id'], CHILD_PICKER_INFO);
            /* ---------- END - MEMCACHE ---------- */

            return_json(array(
                'code' => 200,
                'message' => __("OK"),
                'data' => null
            ));
            break;
        case 'register_pickup_info':
            // Tăng lượt tương tác thêm mới - TaiLA
            addInteractive($child['school_id'], 'pickup', 'parent_view');
            //$class = $classDao->getClassOfChild($_POST['child_id']);
            $class = getChildData($_POST['child_id'], CHILD_CLASS);
            if (is_null($class)) {
                _api_error(0, "The class does not exist, it should be deleted.");
            }

            $currentDate = date('Y-m-d');

            $template = $pickupDao->getTemplate($class['school_id'], false, true, true);
            if (is_null($template)){
                throw new Exception(__("The school has not establish the late pickup configuration"));
            }

            return_json(array(
                'code' => 200,
                'message' => __("OK"),
                'data' => array(
                    'register_info' => $template
                )
            ));
            break;

        case 'register_pickup':
            // Tăng lượt tương tác thêm mới - TaiLA
            addInteractive($child['school_id'], 'pickup', 'parent_view');
            // Lấy ra thông tin lớp của trẻ
            //$class = $classDao->getClassOfChild($_POST['child_id']);
            $class = getChildData($_POST['child_id'], CHILD_CLASS);
            if (is_null($class)) {
                _api_error(0, "The class does not exist, it should be deleted.");
            }
            $args = array();
            $args['child_id'] = $_POST['child_id'];
            $args['class_id'] = $class['group_id'];
            $args['school_id'] = $class['school_id'];
            $args['registered_date'] = date('Y-m-d');

            $args['service_ids'] = isset($_REQUEST['serviceIds']) ? $_REQUEST['serviceIds'] : array();
            //$args['description'] = $_POST['description'];

            // Phụ huynh đăng ký type = 2
            $args['type'] = 2;

            $template = $pickupDao->getTemplate($args['school_id']);
            if (is_null($template)) {
                throw new Exception(__("The school has not establish the late pickup configuration"));
            }
            if (strtotime(date('H:i:s')) > strtotime($template['begin_pickup_time'])) {
                throw new Exception(__("Đã quá thời gian đăng ký, vui lòng liên hệ quản lý trường"));
            }

            $oldServiceIds = array();
            // Kiểm tra xem đã có bản ghi đăng ký chưa
            $registered = $pickupDao->getChildRegisteredByParent($args['child_id'], $args['school_id'], $args['registered_date']);
            if (is_null($registered)) {
                $args['pickup_register_id'] = $pickupDao->insertPickupRegister($args);
            } else {
                $args['pickup_register_id'] = $registered['pickup_register_id'];
                $pickupDao->updatePickupRegister($args);
            }

            /*//Thông báo cho Giáo viên lớp
            $teachers = $teacherDao->getTeacherIdClassBySchoolAndChild($args['school_id'], $args['child_id']);
            //$childName = $childDao->getChildName($args['child_id']);
            $child = getChildData($args['child_id'], CHILD_INFO);
            $childName = $child['child_name'];
            foreach ($teachers as $teacher) {
                $userDao->postNotifications($teacher['teacher_id'], NOTIFICATION_REGISTER_LATEPICKUP, NOTIFICATION_NODE_TYPE_CLASS,
                    $args['pickup_register_id'], toSysDate($args['registered_date']), $teacher['class_name'], convertText4Web($childName));
            }*/

            $db->commit();

            return_json(array(
                'code' => 200,
                'message' => __("Đăng ký thành công"),
                'data' => array()
            ));

            break;

        case 'pickup_history':
            // Tăng lượt tương tác thêm mới - TaiLA
            addInteractive($child['school_id'], 'pickup', 'parent_view');
            $return = array();

            if (!isset($_POST['from_date']) || !isset($_POST['to_date'])) {
                throw new Exception(__("The requested URL was not found on this server. That's all we know"));
            }

            //$class = $classDao->getClassOfChild($_POST['child_id']);
            $class = getChildData($_POST['child_id'], CHILD_CLASS);
            if (is_null($class)) {
                throw new Exception(__("The class does not exist, it should be deleted."));
            }

            $total = 0;
            $rs = $pickupDao->getChildPickupHistory($class['school_id'], $_POST['child_id'], toDBDate($_POST['from_date']), toDBDate($_POST['to_date']), $total);
            $results = empty($rs) ? null : $rs;

            return_json(array(
                'code' => 200,
                'message' => __("OK"),
                'data' => array(
                    'pickup_history' => $results,
                    'total' => $total
                )
            ));
            break;

        case 'leave_school':
            /* check password */
            if(!isset($_POST['user_password'])) {
                _api_error(404);
            }

            $checkPass = $userDao->comparePassword($user->_data['user_id'], $_POST['user_password']);
            if(!$checkPass) {
                throw new Exception(__("The password you entered is incorrect"));
            } else {

                $db->begin_transaction();

                $end_at = date('d/m/Y');
                //1. Cập nhật thông tin trẻ trong lớp (chuyển trạng thái Inactive - bảng ci_class_child và ci_school_child)
                $childDao->leaveChildInSchool($child['child_id'], $child['class_id'], $end_at);

                //2. Xóa cha mẹ khỏi lớp (nhưng vẫn giữ like page của trường)
                //$parentIds = $parentDao->getParentIds($_POST['child_id']);
                $parents = getChildData($child['child_id'], CHILD_PARENTS);
                $parentIds = array_keys($parents);
                if (count($parentIds) > 0) {
                    if ($child['class_id'] > 0) {
                        $classDao->deleteUserFromClass($child['class_id'], $parentIds);
                    }
                }
                //2.1. Xóa thông tin trẻ trong ci_parent_manage
                $childDao->deleteParentManageInfo($child['child_id'], $child['child_parent_id']);

                //3. Giảm số lượng trẻ của trường (giảm giới tính trẻ)
                $schoolDao->updateGenderCount($child['school_id'], $child['gender'], -1);

                $school = getSchoolData($child['school_id'], SCHOOL_DATA);
                if ($child['gender'] == MALE) {
                    $school['male_count'] = $school['male_count'] - 1;
                } else {
                    $school['female_count'] = $school['female_count'] - 1;
                }

                //4.Hủy các dịch vụ trẻ đã đăng ký
                $serviceDao->updateServiceForChildLeave($child['child_id'], $end_at);

                //5. Cập nhật quyết toán học phí.
                //$attendanceCount = $_POST['attendance_count'];
                //$prePaidTuition = $_POST['pre_paid_tuition'];
                //$totalAmount = $_POST['total_amount'];
                //$tuition4LDetails = getTuition4LeaveData();
                //$tuitionDao->insertTuition4Leave($school['page_id'], $_POST['child_id'], $totalAmount, $attendanceCount, $prePaidTuition, $tuition4LDetails);

                //6.Thông báo cho quản lý trường phụ huynh đã cho trẻ nghỉ học
                $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'children', $school['page_admin']);

                if(count($userManagerIds) > 0) {
                    $userDao->postNotifications($userManagerIds, NOTIFICATION_LEAVE_SCHOOL_BY_PARENT, NOTIFICATION_NODE_TYPE_SCHOOL,
                        '', convertText4Web($school['page_title']), $school['page_name'], $child['child_name'], $user->_data['user_id']);
                }

                /* ---------- UPDATE - MEMCACHE ---------- */
                //1.Cập nhật thông tin trường
                $config_update = array(
                    'male_count' => $school['male_count'],
                    'female_count' => $school['female_count']
                );
                updateSchoolData($school['page_id'], SCHOOL_CONFIG, $config_update);
                deleteSchoolData($school['page_id'], SCHOOL_CHILDREN, $child['child_id']);

                //2. Cập nhật thông tin lớp cũ
                updateClassData($child['class_id'], CLASS_INFO);
                deleteClassData($child['class_id'], CLASS_CHILDREN, $child['child_id']);

                //3.Xóa thông tin trẻ
                deleteChildData($child['child_id']);
                /* ---------- END - MEMCACHE ---------- */

                $db->commit();
            }

            return_json(array(
                'code' => 200,
                'message' => __("OK"),
                'data' => array()
            ));
            break;

        default:
            _api_error(400);
            break;
    }

} catch (Exception $e) {
    $db->rollback();
    _api_error(0, $e->getMessage());
} finally {
    $db->autocommit(true);
}

?>