<?php
/**
 * Package: ajax/ci/bo
 * 
 * @package ConIu
 * @author Coniu
 */

include_once(DAO_PATH . 'dao_child.php');
include_once(DAO_PATH . 'dao_tuition.php');
include_once(DAO_PATH . 'dao_class.php');
include_once(DAO_PATH . 'dao_school.php');

$childDao = new ChildDAO();
$tuitionDao = new TuitionDAO();
$classDao = new ClassDAO();
$schoolDao = new SchoolDAO();

if (!isset($_POST['child_id']) || !is_numeric($_POST['child_id'])) {
    _api_error(400);
}

// Kiểm tra xem có phải phụ huynh ko?
/*if (!$childDao->isParent($_POST['child_id'])) {
    _api_error(403);
}*/
$child = getChildDataById($_POST['child_id'], CHILD_INFO);
if (is_null($child)) {
    _api_error(403);
}

try {
    $return = array();
    $db->autocommit(false);
    // Tăng số lượt tương tác - TaiLa
    addInteractive($child['school_id'], 'tuition', 'parent_view');
    switch ($_POST['do']) {
        /* ---------- VIEW ----------*/
        case 'show_tuition':

            switch ($_POST['view']) {
                case 'list':
                    $page = (isset($_POST['page'])) ? $_POST['page'] : 0;
                    //$child = $childDao->getChild($_POST['child_id']);
                    $child = getChildData($_POST['child_id'], CHILD_INFO);
                    if (is_null($child)) {
                        _api_error(404);
                    }
                    $tuitions = $tuitionDao->getTuitionsOfChildForApi($child['child_id'], $page);
                    if (is_null($tuitions)) {
                        _api_error(404);
                    }

                    // return
                    return_json(array(
                        'code' => 200,
                        'message' => 'OK',
                        'data' => array(
                            'tuitions' => $tuitions
                        )
                    ));
                    break;

                case 'detail':
                    $tuitionDetail = $tuitionDao->getTuitionDetailOfChildForApi($_POST['tuition_child_id']);
                    if (is_null($tuitionDetail)) {
                        _api_error(404);
                    }
                    // Lấy tuition_id
                    $tuitionId = $tuitionDao->getTuitionIdFromTuitionChildId($_POST['tuition_child_id']);

                    $tuitionChild = $tuitionDao->getTuitionChildInfoForApi($_POST['tuition_child_id']);
                    if (is_null($tuitionChild)) {
                        _api_error(404);
                    }

                    /* Coniu - Tương tác trường */
                    $tuitionChildIds = array();
                    $tuitionChildIds[] = $_POST['tuition_child_id'];
                    increaseCountViews($child['school_id'], 'tuition', $tuitionChildIds);

                    // Tăng số lượt tương tác - TaiLa
                    addInteractive($child['school_id'], 'tuition', 'parent_view', $tuitionId);
                    /* Coniu - END */

                    //$class = $classDao->getClassOfChild($_POST['child_id']);
                    $child = getChildData($_POST['child_id'], CHILD_INFO);
                    if (is_null($child)) {
                        _api_error(404);
                    }

                    // Lấy thông tin tài khoản của nhà trường
                    //$school = $schoolDao->getSchoolByClass($class['group_id']);
                    //$schoolId = $school['page_id'];
                    $dataCon = getSchoolData($child['school_id'], SCHOOL_CONFIG);
                    $bank_account = "";
                    if (isset($dataCon['bank_account'])) {
                        $bank_account = $dataCon['bank_account'];
                    }

                    // $preUsageHistory = $tuitionDao->getUsageHistoryOfChildForApi($_POST['tuition_child_id']);
                    $preUsageHistory = $tuitionDao->getTuitionChildHistory($tuitionId, $_POST['child_id']);
                    if(empty($preUsageHistory['tuition_detail'])) {
                        $usage_history = null;
                    } else {
                        $usage_history = $preUsageHistory['tuition_detail'];
                    }
                    // return
                    return_json(array(
                        'code' => 200,
                        'message' => 'OK',
                        'data' => array(
                            'bank_account' => $bank_account,
                            'tuition_detail' => $tuitionDetail,
                            'status' => $tuitionChild['status'],
                            'description' => $tuitionChild['description'],
                            'total_amount' => $tuitionChild['total_amount'],
                            'total_deduction' => $tuitionChild['total_deduction'],
                            'debt_amount' => $tuitionChild['debt_amount'],
                            'paid_amount' => $tuitionChild['paid_amount'],
                            'month' => $tuitionChild['month'],
                            'pre_month' => $preUsageHistory['pre_month'],
                            'usage_history' => $usage_history
                        )
                    ));
                    break;
            }
            break;

            /*---------- END - VIEW ----------*/
        case 'tuition_paid' :
            $db->begin_transaction();

            $tuition = $tuitionDao->getTuitionChildById($_POST['tuition_child_id']);
            if (is_null($tuition)) {
                _api_error(404);
            }
            //Cập nhật trạng thái của học phí
            $result = $tuitionDao->updateTuitionChildStatus($_POST['tuition_child_id'], TUITION_CHILD_PARENT_PAID);

            if ($result > 0) {
                $school = $schoolDao->getSchoolById($tuition['school_id']);
                //$child = $childDao->getChild($tuition['child_id']);

                //Thông báo cho Quản lý
                notifySchoolManagerOfView($tuition['child_id'], NOTIFICATION_CONFIRM_TUITION, $tuition['tuition_id'], $tuition['month'], convertText4Web($child['child_name']), 'tuitions');

                //$userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'tuitions', $school['page_admin']);
                //$userDao->postNotifications($userManagerIds, NOTIFICATION_CONFIRM_TUITION, NOTIFICATION_NODE_TYPE_SCHOOL, $tuition['tuition_id'], $tuition['month'], $school['page_name'], convertText4Web($child['child_name']));
            }
            $db->commit();

            // return
            return_json(array(
                'code' => 200,
                'message' => 'OK',
                'data' => array()
            ));
            break;

        default:
            _api_error(404);
            break;
    }

} catch (Exception $e) {
    $db->rollback();
    _api_error(0, $e->getMessage());
} finally {
    $db->autocommit(true);
}

?>