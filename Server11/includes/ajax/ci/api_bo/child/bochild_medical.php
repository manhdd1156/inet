<?php
/**
 * Package: ajax/ci/api_bo
 *
 * @package ConIu
 * @author TaiLA
 */


include_once(DAO_PATH.'dao_child.php');
include_once(DAO_PATH.'dao_medical.php');
include_once(DAO_PATH.'dao_user.php');

$childDao = new ChildDAO();
$medicalDao = new MedicalDAO();
$userDao = new UserDAO();


//if (!is_numeric($_POST['child_parent_id'])) {
//    _api_error(400);
//}
//
////Kiểm tra xem có phải phụ huynh của trẻ hay không
//if (!$childDao->isParent($_POST['child_parent_id'])) {
//    _api_error(403);
//}

try {
    $args = array();
    $args['child_parent_id'] = $_POST['child_parent_id'];
    $args['recorded_at'] = $_POST['recorded_at'];
    $args['diseased_name'] = $_POST['diseased_name'];
    $args['medicine_list'] = $_POST['medicine_list'];
    $args['usage_guide'] = $_POST['usage_guide'];
    $args['symptom'] = $_POST['symptom'];
    $args['day_use'] = $_POST['day_use'];
    $args['hospital'] = $_POST['hospital'];
    $args['hospital_address'] = $_POST['hospital_address'];
    $args['doctor_name'] = $_POST['doctor_name'];
    $args['doctor_phone'] = $_POST['doctor_phone'];
    $args['description'] = $_POST['description'];

    $db->autocommit(false);

    switch ($_POST['do']) {
        case 'lists':
            /**
             * Lấy danh sách y bạ của trẻ
             */
            if (!isset($_POST['child_parent_id']) || !is_numeric($_POST['child_parent_id'])) {
                _api_error(404);
            }
            $db->begin_transaction();
            $medicals = $medicalDao->getChildAllMedicalForAPI($_POST['child_parent_id']);
            $db->commit();

            return_json(array(
                'code' => 200,
                'message' => __("OK"),
                'data' =>array('medicals' => $medicals)
            ));
            break;
        case 'detail':
            if (!isset($_POST['child_medical_id']) || !is_numeric($_POST['child_medical_id'])) {
                _api_error(404);
            }

            $db->begin_transaction();
            $medical = $medicalDao->getChildMedical($_POST['child_medical_id']);
            $db->commit();

            return_json(array(
                'code' => 200,
                'message' => __("OK"),
                'data' =>array('medical' => $medical)
            ));
            break;
        case 'add':
            /**
             * Hàm này xử lý khi phụ huynh thêm thông tin y bạ cho trẻ
             */
            if(!isset($_POST['child_parent_id']) || !is_numeric($_POST['child_parent_id'])) {
                _api_error(404);
            }

            $db->begin_transaction();
            $child_medical_id = $medicalDao->insertChildMedical($args);

            $db->commit();

            return_json(array(
                'code' => 200,
                'message' =>  __("Done, medical report book information has been created"),
                'data' => array()
            ));

            break;
        case 'edit':
            /**
             * Hàm này xử lý khi phụ huynh edit thông tin y bạ của trẻ
             */
            if (!isset($_POST['child_medical_id']) || !is_numeric($_POST['child_medical_id'])) {
                _api_error(404);
            }
            $medical = $medicalDao->getChildMedical($_POST['child_medical_id']);
            if(is_null($medical)) {
                throw new Exception(__("Medical report book not exist"));
            }
            $args['child_medical_id'] = $_POST['child_medical_id'];
            $db->begin_transaction();
            $medicalDao->updateChildMedical($args);

            $db->commit();
            return_json(array(
                'code' => 200,
                'message' =>  __("Done, Child info have been updated"),
                'data' => array()
            ));
            break;
        case 'delete':
            if (!isset($_POST['child_medical_id']) || !is_numeric($_POST['child_medical_id'])) {
                _api_error(404);
            }
            $db->begin_transaction();
            $medicalDao->deleteChildMedical($_POST['child_medical_id']);
            $db->commit();
            return_json(array(
                'code' => 200,
                'message' =>  __("OK"),
                'data' => array()
            ));
            break;
        case 'search':
            if(!isset($_POST['child_parent_id']) || !is_numeric(($_POST['child_parent_id']))) {
                _api_error(404);
            }

            $results = $medicalDao->getChildMedicalSearch($_POST['child_parent_id'], $_POST['year']);
            $medicals = $medicalDao->getChildMedicalSearchForChart($_POST['child_parent_id'], $_POST['year']);

            $jan = 0;
            $feb = 0;
            $mar = 0;
            $apr = 0;
            $may = 0;
            $jun = 0;
            $jul = 0;
            $aug = 0;
            $sep = 0;
            $oct = 0;
            $nov = 0;
            $dec = 0;
            foreach ($medicals as $medical) {
                if($medical == "01") {
                    $jan++;
                } else if($medical == "02") {
                    $feb++;
                } else if($medical == "03") {
                    $mar++;
                } else if($medical == "04") {
                    $apr++;
                } else if($medical == "05") {
                    $may++;
                } else if($medical == "06") {
                    $jun++;
                } else if($medical == "07") {
                    $jul++;
                } else if($medical == "08") {
                    $aug++;
                } else if($medical == "09") {
                    $sep++;
                } else if($medical == "10") {
                    $oct++;
                } else if($medical == "11") {
                    $nov++;
                } else {
                    $dec++;
                }
            }
            $count_month = array(
                array(
                    'month' => __("Jan"),
                    'count' => $jan
                ),
                array(
                    'month' => __('Feb'),
                    'count' => $feb
                ),
                array(
                    'month' => __('Mar'),
                    'count' => $mar
                ),
                array(
                    'month' => __('Apr'),
                    'count' => $apr
                ),
                array(
                    'month' => __('May'),
                    'count' => $may
                ),
                array(
                    'month' => __('Jun'),
                    'count' => $jun
                ),
                array(
                    'month' => __('Jul'),
                    'count' => $jul
                ),
                array(
                    'month' => __('Aug'),
                    'count' => $aug
                ),
                array(
                    'month' => __('Sep'),
                    'count' => $sep
                ),
                array(
                    'month' => __('Oct'),
                    'count' => $oct
                ),array(
                    'month' => __('Nov'),
                    'count' => $nov
                ),
                array(
                    'month' => __('Dec'),
                    'count' => $dec
                ),

            );
            return_json(array(
                'code' => 200,
                'message' => __("OK"),
                'data' =>array(
                    'medicals' => $results,
                    'count_month' => $count_month
                )
            ));
            break;
        default:
            _api_error(400);
            break;
    }

} catch (Exception $e) {
    $db->rollback();
    header('HTTP/1.1 400 API Error');
    return_json(array(
        'code' => 0,
        'message' => $e->getMessage(),
        'data' => array()
    ));
} finally {
    $db->autocommit(true);
}

?>