<?php
/**
 * Package: ajax/ci/bo
 * 
 * @package ConIu
 * @author Coniu
 */

include_once(DAO_PATH . 'dao_class.php');
include_once(DAO_PATH . 'dao_class_level.php');
include_once(DAO_PATH . 'dao_child.php');
include_once(DAO_PATH . 'dao_menu.php');

$classDao = new ClassDAO();
$classLevelDao = new ClassLevelDAO();
$childDao = new ChildDAO();
$menuDao = new MenuDAO();

// Kiểm tra xem có phải phụ huynh ko?
//if (!$childDao->isParent($_POST['child_id'])) {
//    _api_error(403);
//}
$child = getChildDataById($_POST['child_id'], CHILD_INFO);
if (is_null($child)) {
    _api_error(403);
}

try {
    $return = array();
    $db->autocommit(false);
    // Tăng số lượt tương tác - TaiLa
    addInteractive($child['school_id'], 'menu', 'parent_view');
    switch ($_POST['do']) {
        case 'show_menu':

            switch ($_POST['view']) {
                case 'this_week':
                    /*$child = $childDao->getChild($_POST['child_id']);
                    if (is_null($child)) {
                        _api_error(404);
                    }*/
                    $class = getChildData($_POST['child_id'], CHILD_CLASS);
                    if (is_null($class)) {
                        _api_error(404);
                    }

                    $return = array();
                    $monday = date('Y-m-d', strtotime('monday this week'));
                    //Lấy ra danh sách lịch học áp dụng cho lớp tuần này
                    $data = $menuDao->getMenuOfSchoolInWeekForApi($class['school_id'], $class['class_level_id'], $class['group_id'], $monday);

                    $removeArr = array();
                    for ($i = 0; $i < count($data); $i++) {
                        for ($j = 0; $j < count($data); $j++) {
                            if ($i != $j) {
                                $beginI = strtotime(toDBDate($data[$i]['begin']));
                                $beginJ = strtotime(toDBDate($data[$j]['begin']));
                                if ($beginI == $beginJ) {
                                    if ($data[$i]['applied_for'] > $data[$j]['applied_for']) {
                                        $removeArr[] = $j;
                                    }
                                }
                            }
                        }
                    }
                    $menu = null;

                    for ($k = 0; $k < count($data); $k++) {
                        if (!in_array($k, $removeArr)) {
                            $menu = $menuDao->getMenuDetailForApi($data[$k]['menu_id']);

                            if ($menu['applied_for'] == 1) {
                                $menu['level'] = __("School");
                            } elseif ($menu['applied_for'] == 2) {
                                //$class_level = $classLevelDao->getClassLevel($menu['class_level_id']);
                                $class_level = getClassLevelData($menu['class_level_id'], CLASS_LEVEL_INFO);
                                $menu['level'] = $class_level['class_level_name'];
                            } elseif ($menu['applied_for'] == 3) {
                                //$classes = $classDao->getClass($menu['class_id']);
                                $classes = getClassData($menu['class_id'], CLASS_INFO);
                                $menu['level'] = $classes['group_title'];
                            }
                            $menu['use'] = STATUS_ACTIVE;
                        } else {
                            $menu['use'] = STATUS_INACTIVE;
                        }
                    }
                    return_json(array(
                        'code' => 200,
                        'message' => 'OK',
                        'data' => array(
                            'menu_child' => $menu
                        )
                    ));
                    break;

                case 'history':
                    $page = isset($_POST['page']) ? $_POST['page']  : 0;

                    //$class = $classDao->getClassOfChild($_POST['child_id']);
                    $class = getChildData($_POST['child_id'], CHILD_CLASS);
                    if ($class == null) {
                        _api_error(404);
                    }
                    //Lấy ra danh sách thực đơn áp dụng cho lớp
                    $data = $menuDao->getMenuSchoolForApi($class['school_id'], $class['class_level_id'], $class['group_id'], $page);

                    $removeArr = array();
                    for ($i = 0; $i < count($data); $i++) {
                        for ($j = 0; $j < count($data); $j++) {
                            if ($i != $j) {
                                $beginI = strtotime(toDBDate($data[$i]['begin']));
                                $beginJ = strtotime(toDBDate($data[$j]['begin']));
                                if ($beginI == $beginJ) {
                                    if ($data[$i]['applied_for'] > $data[$j]['applied_for']) {
                                        $removeArr[] = $j;
                                    }
                                }
                            }
                        }
                    }
                    $menus = array();

                    for ($k = 0; $k < count($data); $k++) {
                        $menu = $data[$k];

                        if ($menu['applied_for'] == 1) {
                            $menu['level'] = __("School");
                        } elseif ($menu['applied_for'] == 2) {
                            //$class_level = $classLevelDao->getClassLevel($menu['class_level_id']);
                            $class_level = getClassLevelData($menu['class_level_id'], CLASS_LEVEL_INFO);
                            $menu['level'] = $class_level['class_level_name'];
                        } elseif ($menu['applied_for'] == 3) {
                            //$classes = $classDao->getClass($menu['class_id']);
                            $classes = getClassData($menu['class_id'], CLASS_INFO);
                            $menu['level'] = $classes['group_title'];
                        }
                        if (!in_array($k, $removeArr)) {
                            $menu['use'] = STATUS_ACTIVE;
                        } else {
                            $menu['use'] = STATUS_INACTIVE;
                        }
                        $menus[] = $menu;
                    }
                    return_json(array(
                        'code' => 200,
                        'message' => 'OK',
                        'data' => array(
                            'menu_history' => $menus
                        )
                    ));
                    break;

                case 'detail':
                    $menu = $menuDao->getMenuDetailForApi($_POST['menu_id']);
                    if (is_null($menu)) {
                        _api_error(404);
                    }

                    /* Coniu - Tương tác trường */
                    $menuIds = array();
                    $menuIds[] = $menu['menu_id'];
                    increaseCountViews($child['school_id'], 'menu', $menuIds);

                    // Tăng số lượt tương tác - TaiLa
                    addInteractive($child['school_id'], 'menu', 'parent_view', $menu['menu_id']);
                    /* Coniu - END */

                    if($menu['applied_for'] == 1) {
                        $menu['level'] =  __("School");
                    } elseif($menu['applied_for'] == 2) {
                        //$class_level = $classLevelDao->getClassLevel($menu['class_level_id']);
                        $class_level = getClassLevelData($menu['class_level_id'], CLASS_LEVEL_INFO);
                        $menu['level'] = $class_level['class_level_name'];
                    } elseif($menu['applied_for'] == 3) {
                        //$classes = $classDao->getClass($menu['class_id']);
                        $classes = getClassData($menu['class_id'], CLASS_INFO);
                        $menu['level'] = $classes['group_title'];
                    }

                    return_json(array(
                        'code' => 200,
                        'message' => 'OK',
                        'data' => array(
                            'menu_detail' => $menu
                        )
                    ));
                    break;

            }
            break;

        default:
            _api_error(400);
            break;
    }

} catch (Exception $e) {
    $db->rollback();
    _api_error(0, $e->getMessage());
} finally {
    $db->autocommit(true);
}

?>