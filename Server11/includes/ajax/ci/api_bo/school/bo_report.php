<?php
/**
 * Package: ajax/ci/api_bo/school
 *
 * @package ConIu
 * @author TaiLA
 */

if(is_empty($_POST['page_name']) || !valid_username($_POST['page_name'])) {
    _error(404);
}

include_once(DAO_PATH . 'dao_report.php');

$reportDao = new ReportDAO();

//$school = getSchool($_POST['page_name']);
$school = getSchoolDataByUsername($_POST['page_name'], SCHOOL_INFO);
if (is_null($school)) {
    _error(403);
}

try {
    $return = array();
    // Tăng lượt tương tác - TaiLA
    addInteractive($school['page_id'], 'report', 'school_view');
    switch ($_POST['do']) {
        case 'list_class':
            // Tăng số đếm tương tác của trường
            increaseSchoolInteractive($school['page_id'], 'report');

            if (!canView($_POST['page_name'], 'reports')) {
                _api_error(403);
            }

            // Lấy danh sách SLL của toàn trường theo năm và tháng truyền vào
            $reports = $reportDao->getAllReportOfMonth($school['page_id'], $_POST['year'], $_POST['month']);

            // return
            return_json(array(
                'code' => 200,
                'message' => __("OK"),
                'data' =>array('reports' => $reports)
            ));
            break;
        case 'detail':
            // Tăng số đếm tương tác của trường
            increaseSchoolInteractive($school['page_id'], 'report');
            // Tăng lượt tương tác - TaiLA
            addInteractive($school['page_id'], 'report', 'school_view', $_POST['report_id']);

            if (!canView($_POST['page_name'], 'reports')) {
                _api_error(403);
            }
            if(!isset($_POST['report_id']) || !is_numeric($_POST['report_id'])) {
                _error(404);
            }
            $data = $reportDao->getReportById($_POST['report_id']);

            // return
            return_json(array(
                'code' => 200,
                'message' => __("OK"),
                'data' =>array('report_detail' => $data)
            ));
            break;
        case 'notify_all_select':
            if (!canView($_POST['page_name'], 'reports')) {
                _api_error(403);
            }

            $db->begin_transaction();
            $ids = isset($_REQUEST['report_ids'])? $_REQUEST['report_ids']: array();

            if(count($ids) > 0) {
                foreach ($ids as $id) {
                    $report = $reportDao->getReportById($id);
                    //$parents = $childDao->getParent($child['child_id']);
                    $child = getChildData($report['child_id'], CHILD_INFO);
                    $parents = getChildData($report['child_id'], CHILD_PARENTS);
                    if (!is_null($parents)) {
                        foreach ($parents as $parent) {
                            //thông báo về cho phụ huynh
                            $userDao->postNotifications($parent['user_id'], NOTIFICATION_NEW_REPORT, NOTIFICATION_NODE_TYPE_CHILD,
                                $id, convertText4Web($report['report_name']), $child['child_id'], convertText4Web($child['child_name']));
                        }
                    }
                }
            }

            // Đổi trạng thái thành đã thông báo
            $reportDao->updateNotifiedForReports($ids);

            $db->commit();
            // return
            return_json(array(
                'code' => 200,
                'message' => __("OK"),
                'data' =>array()
            ));
            break;
        default:
            _api_error(400);
            break;
    }

} catch (Exception $e) {
    //$db->rollback();
    // return
    header('HTTP/1.1 400 API Error');
    return_json(array(
        'code' => 0,
        'message' => $e->getMessage(),
        'data' => array()
    ));
} finally {
    //$db->autocommit(true);
}
?>