<?php
/**
 * Package: ajax/ci/bo
 *
 * @package ConIu
 * @author ManhDD
 */
// fetch bootstrap
//require('../../../../../bootstrap.php');

// check AJAX Request
//is_ajax();
//check_login();

if (is_empty($_POST['school_username']) || !valid_username($_POST['school_username'])) {
    _error(404);
}

include_once(DAO_PATH . 'dao_school.php');
include_once(DAO_PATH . 'dao_class.php');
include_once(DAO_PATH . 'dao_subject.php');
include_once(DAO_PATH . 'dao_teacher.php');
include_once(DAO_PATH . 'dao_report.php');
include_once(DAO_PATH . 'dao_user.php');
include_once(DAO_PATH . 'dao_point.php');
include_once(DAO_PATH . 'dao_child.php');

$schoolDao = new SchoolDAO();
$classDao = new ClassDAO();
$teacherDao = new TeacherDAO();
$reportDao = new ReportDAO();
$subjectDao = new SubjectDao();
$userDao = new UserDao();
$pointDao = new PointDao();
$childDao = new ChildDAO();

//$school = getSchool($_POST['school_username']);
$school = getSchoolDataByUsername($_POST['school_username'], SCHOOL_INFO);
if (is_null($school)) {
    _error(403);
}

try {
    $db->autocommit(false);
    switch ($_POST['do']) {
        case 'list_search_condition':
            // User hiệu trưởng
            // Lấy danh sách khối
            $class_levels = getSchoolData($school['page_id'], SCHOOL_CLASS_LEVELS);
            $results = array();
            $class_levels = array_values($class_levels);
            if (count($class_levels) > 0) {
                foreach ($class_levels as $key => $class_level) {
                    // Lấy danh sách lớp của 1 khối
                    $classes = getClassLevelData($class_level['class_level_id'], CLASS_LEVEL_CLASSES);
                    $class_level['classes'] = array_values($classes);
                    // Lấy danh sách môn học của 1 khối
                    $subjects = $subjectDao->getSubjectsByClassLevel($school['page_id'], $class_level['gov_class_level'], $_POST['school_year']);
                    $class_level['subjects'] = array_values($subjects);
                    $class_levels[$key] = $class_level;
                }
            }

            //Lấy ra danh sách lớp
//            $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);

//            getClasses($user_id)
            return_json(array(
                'code' => 200,
                'message' => 'OK',
                'data' => $class_levels
            ));
            break;
        case 'list_scores_subject' :
            if (!canEdit($_POST['school_username'], 'points')) {
                _error(403);
            }

            //Lấy ra class_id của lớp
            $class_id = $_POST['class_id'];
            $school_year = $_POST['school_year'];

            $subject_id = $_POST['subject_id'];
            $semester = $_POST['semester'];
            $is_last_semester = $_POST['is_last_semester'];
            // Lấy school_config
            $schoolConfig = getSchoolData($school['page_id'], SCHOOL_CONFIG);
            // Lấy chi tiết gov_class_level
            $class_level = getClassLevelData($_POST['class_level_id'], CLASS_LEVEL_INFO);
            // Lấy danh sách điểm của trẻ trong năm học
            $children_point = $pointDao->getChildrenPointsBySubjectId($class_id, $school_year, $subject_id);
            $children_last = array();

            if (count($children_point) > 0) {
                foreach ($children_point as $child) {
                    // Lấy child_name
                    $returnPoint = array();
                    $child_detail = $childDao->getChildByCode($child['child_code']);
                    $child_id = $childDao->getChildIdByCode($child['child_code']);
                    $subject_detail = $subjectDao->getSubjectById($child['subject_id']);
                    $child['is_reexam'] = $subject_detail['re_exam'];
                    $child['child_firstname'] = $child_detail['first_name'];
                    $child['child_lastname'] = $child_detail['last_name'];
                    $child['child_picture'] = $child_detail['child_picture'];
                    $child['child_id'] = $child_id;
                    if ($semester == 1 || $semester == 2) {
                        foreach ($child as $key => $value) {
                            if (in_array(substr($key, 0, 3), array("hs" . $semester, "gk" . $semester, "ck" . $semester))) {
                                if (isset($value)) {
                                    $temp['key'] = $key;
                                    $temp['value'] = $value;
                                    $returnPoint[] = $temp;
                                }
                            }
                            if (in_array(substr($key, 0, 2), array("hs", "gk", "ck", "re"))) {
                                unset($child[$key]);
                            }
                        }
                        $child['points'] = $returnPoint;
                    }else if($semester == 0) {
                        foreach ($child as $key => $value) {
                            if (in_array(substr($key, 0, 2), array("hs", "gk", "ck"))) {
                                if (isset($value)) {
                                    $temp['key'] = $key;
                                    $temp['value'] = $value;
                                    $returnPoint[] = $temp;
                                }
                            }
                            if (in_array(substr($key, 0, 2), array("hs", "gk", "ck"))) {
                                unset($child[$key]);
                            }
                        }
                        $child['points'] = $returnPoint;
                    } else if ($semester == 3) {
//                        if (!isset($child['re_exam'])) {
//                            continue;
//                        }
                        foreach ($child as $key => $value) {
                            if (in_array(substr($key, 0, 2), array("hs", "gk", "ck"))) {
                                unset($child[$key]);
                            }
                        }
                    }


                    $children_last[] = $child;
                }
            }

            return_json(array(
                'code' => 200,
                'message' => 'OK',
                'data' => $children_last
            ));
            break;
        case 'list_students':
            if (!isset($_POST['class_id']) || !is_numeric($_POST['class_id'])) {
                _error(404);
            }
            $students = $childDao->getChildrenOfClass($_POST['class_id']);
            $new_students = array();
            if (count($students) > 0) {
                foreach ($students as $student) {
                    $new_student['child_id'] = $student['child_id'];
                    $new_student['child_code'] = $student['child_code'];
                    $new_student['child_name'] = $student['child_name'];
                    $new_student['child_picture'] = $student['child_picture'];
                    $new_students[] = $new_student;
                }
            }
            return_json(array(
                'code' => 200,
                'message' => 'OK',
                'data' => $new_students
            ));
            break;
        case 'get_info_student':
            if (!isset($_POST['student_id']) || !is_numeric($_POST['student_id'])) {
                _error(404);
            }

            $student = $childDao->getChildbyChildId($_POST['student_id']);
            return_json(array(
                'code' => 200,
                'message' => 'OK',
                'data' => $student
            ));
            break;
        case 'get_score_of_student':
            if (!canEdit($_POST['school_username'], 'points')) {
                _error(403);
            }

            //Lấy ra class_id của lớp
            $class_id = $_POST['class_id'];
            $school_year = $_POST['school_year'];

            // Lấy school_config
            $child_id = $_POST['student_id'];
//            // Lấy đầy đủ thông tin của child
//            $child_detail = $childDao->getChild($child_id);
            $schoolConfig = getSchoolData($school['page_id'], SCHOOL_CONFIG);
            // Lấy chi tiết gov_class_level
            $class_level = $classDao->getClassLevelsbyClassId($class_id);

            // Lấy danh sách điểm của trẻ trong năm học
            $children_point = $pointDao->getChildrenPointsByStudentId($class_id, $school_year, $child_id);

            $children_point_last = array();
            if (count($children_point) > 0) {
                // Lấy số buổi nghỉ có phép và không phép của học sinh
                $child_absent = $childDao->getAbsentsChild($child_id, $class_id);
                foreach ($children_point as $child) {
                    $returnPoint = array();
                    foreach ($child as $key => $value) {
                        if (in_array(substr($key, 0, 2), array("hs", "gk", "ck", "re"))) {
                            if (isset($value)) {
                                $temp['key'] = $key;
                                $temp['value'] = $value;
                                $returnPoint[] = $temp;
                            }
                        }
                        if (in_array(substr($key, 0, 2), array("hs", "gk", "ck", "re"))) {
                            unset($child[$key]);
                        }
                    }
                    $child['points'] = $returnPoint;
                    //Lấy tên môn học
                    $subject_detail = $subjectDao->getSubjectById($child['subject_id']);
                    $child['subject_name'] = $subject_detail['subject_name'];
                    unset($child['school_year'], $child['class_id'], $child['child_code'], $child['point_id']);
                    $children_point_last[] = $child;
                }
            }
            return_json(array(
                'code' => 200,
                'message' => 'OK',
                'data' => $children_point_last
            ));
            break;
        case 'get_score_of_student_by_report':
            if (!canEdit($_POST['school_username'], 'points')) {
                _error(403);
            }


//            $school_year = $_POST['school_year'];
            $report_id = $_POST['report_id'];
            // Lấy reprot info
            $report = $reportDao->getReportById($report_id);
            // Lấy school_config
            $child_id = $report['child_id'];
//            // Lấy đầy đủ thông tin của child
            $child = getChildData($child_id, CHILD_INFO);
            //Lấy ra class_id của lớp
            $class_id = $child['class_id'];
//            $child_detail = $childDao->getChild($child_id);
            $schoolConfig = getSchoolData($school['page_id'], SCHOOL_CONFIG);
            // Lấy chi tiết gov_class_level
            $class_level = $classDao->getClassLevelsbyClassId($class_id);
            //Lấy năm tạo report để tạo school_year
            $school_year = date("Y", strtotime($report['created_at'])) . '-' . (date("Y", strtotime($report['created_at'])) + 1);
            // Lấy danh sách điểm của trẻ trong năm học
            $children_point = $pointDao->getChildrenPointsByStudentId($class_id, $school_year, $child_id);
            // Lấy child_name


            $children_point_last = array();
            if (count($children_point) > 0) {
                // Lấy số buổi nghỉ có phép và không phép của học sinh
                $child_absent = $childDao->getAbsentsChild($child_id, $class_id);
                foreach ($children_point as $child) {
                    $returnPoint = array();
                    foreach ($child as $key => $value) {
                        if (in_array(substr($key, 0, 2), array("hs", "gk", "ck", "re"))) {
                            if (isset($value)) {
                                $temp['key'] = $key;
                                $temp['value'] = $value;
                                $returnPoint[] = $temp;
                            }
                        }
                        if (in_array(substr($key, 0, 2), array("hs", "gk", "ck", "re"))) {
                            unset($child[$key]);
                        }
                    }
                    $child['points'] = $returnPoint;
                    //Lấy tên môn học
                    $subject_detail = $subjectDao->getSubjectById($child['subject_id']);
                    $child['subject_name'] = $subject_detail['subject_name'];
                    unset($child['school_year'], $child['class_id'], $child['child_code'], $child['point_id']);
                    $children_point_last[] = $child;
                }
            }
            return_json(array(
                'code' => 200,
                'message' => 'OK',
                'data' => $children_point_last
            ));
            break;
        case
        'get_avgs_score_of_student':
            if (!canEdit($_POST['school_username'], 'points')) {
                _error(403);
            }

            //Lấy ra class_id của lớp
            $class_id = $_POST['class_id'];
            $school_year = $_POST['school_year'];

            // Lấy school_config
            $child_id = $_POST['student_id'];
//            // Lấy đầy đủ thông tin của child
//            $child_detail = $childDao->getChild($child_id);
            $schoolConfig = getSchoolData($school['page_id'], SCHOOL_CONFIG);
            $class = $classDao->getClass($class_id);
            // Lấy chi tiết gov_class_level
            $class_level = $classDao->getClassLevelsbyClassId($class_id);
//            $class_level = getClassLevelData($_POST['class_level_id'], CLASS_LEVEL_INFO);
            // Lấy danh sách điểm của trẻ trong năm học
            $children_point = $pointDao->getChildrenPointsByStudentId($class_id, $school_year, $child_id);
            // Lấy child_name
            if (count($children_point) > 0) {
                // flag đánh dấu đã đủ điểm các môn hay chưa
                $flag_enough_point = true;
                // Lấy số buổi nghỉ có phép và không phép của học sinh
                $child_absent = $childDao->getAbsentsChild($child_id, $class_id);
                // Lưu điểm của các môn thi lại
                $children_subject_reexams = array();
                if ($schoolConfig['score_fomula'] == "km") {
                    // Lưu các điểm đã được tính toán
                    $children_point_avgs = array();
                    // trạng thái 0: waitting - chưa đủ điểm, 1: pass, 2: fail, 3: reexam
                    $status = 0;
                    // điểm các tháng của 2 kỳ
                    $child_m11_point = $child_m12_point = $child_m13_point = $child_m21_point = $child_m22_point = $child_m23_point = 0;
                    // điểm trung bình các tháng của 2 kỳ
                    $child_avM1_point = $child_avM2_point = 0;
                    // điểm trung bình của cuối kỳ
                    $child_avd1_point = 0;
                    $child_avd2_point = 0;
                    $child_final_semester1 = $child_final_semester2 = 0;
                    // điểm trung bình năm
                    $child_avYear_point = 0;
                    foreach ($children_point as $child) {
                        // Nếu bất kì điểm nào thiếu sẽ đánh dấu là chưa đủ điểm
                        if (!isset($child['hs11']) || !isset($child['hs12']) || !isset($child['hs13']) || !isset($child['gk11'])
                            || !isset($child['hs21']) || !isset($child['hs22']) || !isset($child['hs23']) || !isset($child['gk21'])) {
                            $flag_enough_point = false;
                        }
                        // tạm thời tính tổng các đầu điểm
                        // kỳ 1
                        $child_m11_point += (int)$child['hs11'];
                        $child_m12_point += (int)$child['hs12'];
                        $child_m13_point += (int)$child['hs13'];
                        $child_avd1_point += (int)$child['gk11'];
                        // kỳ 2
                        $child_m21_point += (int)$child['hs21'];
                        $child_m22_point += (int)$child['hs22'];
                        $child_m23_point += (int)$child['hs23'];
                        $child_avd2_point += (int)$child['gk21'];

                        // lấy tên môn học
                        $subject_detail = $subjectDao->getSubjectById($child['subject_id']);
                        $child['subject_name'] = $subject_detail['subject_name'];
                        if (isset($subject_detail['re_exam']) && $subject_detail['re_exam']) { // nếu môn đó là môn thi lại thì lưu vào array
                            $subject_reexam['name'] = $subject_detail['subject_name'];
                            $subject_reexam['point'] = $child['re_exam'];
                            $children_subject_reexams[] = $subject_reexam;
                        }
                        unset($child['school_year'], $child['class_id'], $child['child_code'], $child['point_id']);
                    }


                    // định nghĩa số bị chia của từng khối
                    if ($class_level['gov_class_level'] == '7' || $class_level['gov_class_level'] == '8') {
                        $dividend = 15;
                    } else if ($class_level['gov_class_level'] == '9') {
                        $dividend = 11.4;
                    } else if ($class_level['gov_class_level'] == '10') {
                        $dividend = 15.26;
                    } else if ($class_level['gov_class_level'] == '11') {
                        $dividend = 16.5;
                    } else if ($class_level['gov_class_level'] == '12') {
                        $dividend = 14.5;
                    }
                    // tính điểm theo công thức từng khối
                    // kỳ 1
                    $child_m11_point /= $dividend;
                    $child_m12_point /= $dividend;
                    $child_m13_point /= $dividend;
                    $child_avd1_point /= $dividend;
                    $child_avM1_point = ($child_m11_point + $child_m12_point + $child_m13_point) / 3;
                    $child_final_semester1 = ($child_avd1_point + $child_avM1_point) / 2;
                    // kỳ 2
                    $child_m21_point /= $dividend;
                    $child_m22_point /= $dividend;
                    $child_m23_point /= $dividend;
                    $child_avd2_point /= $dividend;
                    $child_avM2_point = ($child_m21_point + $child_m22_point + $child_m23_point) / 3;
                    $child_final_semester2 = ($child_avd2_point + $child_avM2_point) / 2;
                    // cả năm
                    $child_avYear_point = ($child_final_semester1 + $child_final_semester2) / 2;
                    // lưu lại vào array
                    $children_point_avgs['a1'] = number_format($child_m11_point, 2);
                    $children_point_avgs['b1'] = number_format($child_m12_point, 2);
                    $children_point_avgs['c1'] = number_format($child_m13_point, 2);
                    $children_point_avgs['d1'] = number_format($child_avd1_point, 2);
                    $children_point_avgs['x1'] = number_format($child_avM1_point, 2);
                    $children_point_avgs['e1'] = number_format($child_final_semester1, 2);
                    $children_point_avgs['a2'] = number_format($child_m21_point, 2);
                    $children_point_avgs['b2'] = number_format($child_m22_point, 2);
                    $children_point_avgs['c2'] = number_format($child_m23_point, 2);
                    $children_point_avgs['d2'] = number_format($child_avd2_point, 2);
                    $children_point_avgs['x2'] = number_format($child_avM2_point, 2);
                    $children_point_avgs['e2'] = number_format($child_final_semester2, 2);
                    $children_point_avgs['y'] = number_format($child_avYear_point, 2);

                    //xét điều kiện để đánh giá pass hay fail
                    if ($child_avYear_point < 25) { // dưới 25 điểm thì fail chặt
                        $status = 2;
                    } else { // nếu trên 25 điểm thì xét các điều kiện khác
                        $status = 1;
                        if ((isset($child_absent['absent_true']) && (int)$child_absent['absent_true'] >= 55)
                            || (isset($child_absent['absent_false']) && (int)$child_absent['absent_false'] >= 30)
                            || (((int)$child_absent['absent_true'] + (int)$child_absent['absent_false']) >= 55)) {
                            if ($class_level['gov_class_level'] == '7' || $class_level['gov_class_level'] == '8'
                                || $class_level['gov_class_level'] == '10' || $class_level['gov_class_level'] == '11') {
                                $status = 3;
                            } else if ($class_level['gov_class_level'] == '9' || $class_level['gov_class_level'] == '12') {
                                $status = 1;
                            }
                        }
                    }
                    // xét đến đánh giá điểm thi lại
                    if ($status == 3) {
                        $result_exam = 0;
                        foreach ($children_subject_reexams as $subject_reexam) {
                            if (!isset($subject_reexam['point'])) {
                                $flag_enough_point = false;
                            } else {
                                $result_exam += (int)$subject_reexam['point'];
                            }
                        }
                        if ($class_level['gov_class_level'] == '7' || $class_level['gov_class_level'] == '8'
                            || $class_level['gov_class_level'] == '11') {
                            $result_exam /= 4;
                        } else if ($class_level['gov_class_level'] == '10') {
                            $result_exam /= 4;
                        }
                        // nếu điểm thi lại trên 25 điểm thì pass còn lại fail
                        if ($flag_enough_point && $result_exam >= 25) {
                            $status = 1;
                        } else if ($flag_enough_point && $result_exam < 25) {
                            $status = 2;
                        } else { // Chưa du diem
                            $status = 3;
                        }
                    } else {
                        $children_subject_reexams = array();
                    }
                    if (!$flag_enough_point && $status != 3) {
                        $status = 1;
                    }
                    $points_key = array();
                    $result = array();
                    $result['point_avgs'] = (object)$children_point_avgs;
//            $result['enough_point'] = $flag_enough_point;
                    $result['absent_with_permission'] = $child_absent['absent_true'];
                    $result['absent_without_permission'] = $child_absent['absent_false'];
                    $result['status'] = $status;
                    $result['subject_reexams'] = $children_subject_reexams;
                } else if ($schoolConfig['score_fomula'] == "vn") {
                    include_once(DAO_PATH . 'dao_conduct.php');
                    $conductDao = new ConductDAO();
                    // điều kiện điểm của môn 1: kém, 2 yếu, 3 trung bình, 4 tiên tiến, 5 giỏi
                    $point_avg1_condition1 = 5;//hk1
                    $point_avg2_condition1 = 5;//hk2
                    $point_avgYearAll_condition1 = 5;// cả năm
                    // điều kiện điểm của môn toán hoặc văn  1: kém, 2 yếu, 3 trung bình, 4 tiên tiến, 5 giỏi
                    $point_avg1_condition2 = 5; //hk1
                    $point_avg2_condition2 = 5; //hk2
                    $point_avgYearAll_condition2 = 5; // cả năm
                    // trạng thái
                    $status_point1 = '';
                    $status_point2 = '';
                    $status_pointAll = '';
                    // flag đánh dấu học sinh đã thi lại hay chưa
                    $flag_reexam = false;
                    // trạng thái lên lớp hay thi lại hay chưa đủ điểm...
                    $status_result = 2;

                    // điểm trung bình các môn 2 kỳ
                    $child_TBM1_point = $child_TBM2_point = 0;
                    // điểm trung bình năm
                    $child_avYearAll_point = 0;
                    // TÍnh điểm mỗi môn
                    if (count($children_point) > 0) {
                        foreach ($children_point as $each) {
                            if (!isset($each['hs11']) || !isset($each['hs12']) || !isset($each['gk11'])
                                || !isset($each['hs21']) || !isset($each['hs22']) || !isset($each['gk21'])
                                || !isset($each['ck1']) || !isset($each['ck2'])) {
                                $flag_enough_point = false;
                            }
                            // điểm hệ số 2 kỳ
                            $child_hk1_point = $child_hk2_point = 0;
                            $child_TBM1_current_point = $child_TBM2_current_point = 0;
                            // đếm số điểm đã có
                            $count_hk1_point = 0;
                            $count_hk2_point = 0;
                            //Tính điểm trung bình môn

                            for ($i = 1; $i <= 6; $i++) {
                                // điểm hệ số 1
                                if (isset($each['hs1' . $i])) {
                                    $child_hk1_point += (int)$each['hs1' . $i];
                                    $count_hk1_point++;
                                }
                                if (isset($each['hs2' . $i])) {
                                    $child_hk2_point += (int)$each['hs2' . $i];
                                    $count_hk2_point++;
                                }
                                // điểm hệ số 2
                                if (isset($each['gk1' . $i])) {
                                    $child_hk1_point += (int)$each['gk1' . $i] * 2;
                                    $count_hk1_point += 2;
                                }
                                if (isset($each['gk2' . $i])) {
                                    $child_hk2_point += (int)$each['gk2' . $i] * 2;
                                    $count_hk2_point += 2;
                                }
                            }
                            //điểm cuối kì

                            if (isset($each['ck1'])) {
                                $child_hk1_point += (int)$each['ck1'] * 3;
                                $count_hk1_point += 3;
                            }
                            if (isset($each['ck2'])) {
                                // Nếu có điểm thi lại thì điểm tổng kết sẽ tính dựa theo điểm thi lại
                                if(isset($each['re_exam'])) {
                                    $child_hk2_point += (int)$each['re_exam'] * 3;
                                    $flag_reexam = true;
                                }else {
                                    $child_hk2_point += (int)$each['ck2'] * 3;
                                }
                                $count_hk2_point += 3;
                            }
                            if ($count_hk1_point > 0) {
                                $child_TBM1_current_point = $child_hk1_point / $count_hk1_point;
                            }
                            if ($count_hk2_point > 0) {
                                $child_TBM2_current_point = $child_hk2_point / $count_hk2_point;
                            }
                            // Lấy tên môn học
                            $subject_detail = $subjectDao->getSubjectById($each['subject_id']);

                            // Nếu có điểm thi lại thì điểm tổng kết sẽ tính dựa theo điểm thi lại
                            if (isset($each['re_exam'])) {
                                $returnSubject = array();
                                $returnSubject['subject_name'] = $subject_detail['subject_name'];
                                $returnSubject['point'] = $each['re_exam'];
                                $children_subject_reexams[] = $returnSubject;

                                $flag_reexam = true;
                            }
                            // Điểm tổng kết
                                $child_avYear_point = ($child_TBM1_current_point + $child_TBM2_current_point * 2) / 3;

                            // xét xem điều kiện các môn ở mức nào
                            //học kỳ 1
                            if ($child_TBM1_current_point < 2 && $point_avg1_condition1 > 1) {
                                $point_avg1_condition1 = 1;
                            } else if ($child_TBM1_current_point < 3.5 && $point_avg1_condition1 > 2) {
                                $point_avg1_condition1 = 2;
                            } else if ($child_TBM1_current_point < 5 && $point_avg1_condition1 > 3) {
                                $point_avg1_condition1 = 3;
                            } else if ($child_TBM1_current_point < 6.5 && $point_avg1_condition1 > 4) {
                                $point_avg1_condition1 = 4;
                            }
                            //học kỳ 2
                            if ($child_TBM2_current_point < 2 && $point_avg2_condition1 > 1) {
                                $point_avg2_condition1 = 1;
                            } else if ($child_TBM2_current_point < 3.5 && $point_avg2_condition1 > 2) {
                                $point_avg2_condition1 = 2;
                            } else if ($child_TBM2_current_point < 5 && $point_avg2_condition1 > 3) {
                                $point_avg2_condition1 = 3;
                            } else if ($child_TBM2_current_point < 6.5 && $point_avg2_condition1 > 4) {
                                $point_avg2_condition1 = 4;
                            }
                            // cả năm
                            if ($child_avYear_point < 2 && $point_avgYearAll_condition1 > 1) {
                                $point_avgYearAll_condition1 = 1;
                            } else if ($child_avYear_point < 3.5 && $point_avgYearAll_condition1 > 2) {
                                $point_avgYearAll_condition1 = 2;
                            } else if ($child_avYear_point < 5 && $point_avgYearAll_condition1 > 3) {
                                $point_avgYearAll_condition1 = 3;
                            } else if ($child_avYear_point < 6.5 && $point_avgYearAll_condition1 > 4) {
                                $point_avgYearAll_condition1 = 4;
                            }
                            // xét xem điều kiện của môn toán văn
                            // lấy tên môn học
                            $subject_detail = $subjectDao->getSubjectById($each['subject_id']);
                            $each['subject_name'] = $subject_detail['subject_name'];
                            if (convert_vi_to_en(strtolower(html_entity_decode($each['subject_name']))) == "toan" || convert_vi_to_en(strtolower(html_entity_decode($each['subject_name']))) == "van") {
                                //học kỳ 1
                                if ($child_TBM1_current_point < 2 && $point_avg1_condition2 > 1) {
                                    $point_avg1_condition2 = 1;
                                } else if ($child_TBM1_current_point < 3.5 && $point_avg1_condition2 > 2) {
                                    $point_avg1_condition2 = 2;
                                } else if ($child_TBM1_current_point < 5 && $point_avg1_condition2 > 3) {
                                    $point_avg1_condition2 = 3;
                                } else if ($child_TBM1_current_point < 6.5 && $point_avg1_condition2 > 4) {
                                    $point_avg1_condition2 = 4;
                                }
                                // học kỳ 2
                                if ($child_TBM2_current_point < 2 && $point_avg2_condition2 > 1) {
                                    $point_avg2_condition2 = 1;
                                } else if ($child_TBM2_current_point < 3.5 && $point_avg2_condition2 > 2) {
                                    $point_avg2_condition2 = 2;
                                } else if ($child_TBM2_current_point < 5 && $point_avg2_condition2 > 3) {
                                    $point_avg2_condition2 = 3;
                                } else if ($child_TBM2_current_point < 6.5 && $point_avg2_condition2 > 4) {
                                    $point_avg2_condition2 = 4;
                                }
                                // cả năm
                                if ($child_avYear_point < 2 && $point_avgYearAll_condition2 > 1) {
                                    $point_avgYearAll_condition2 = 1;
                                } else if ($child_avYear_point < 3.5 && $point_avgYearAll_condition2 > 2) {
                                    $point_avgYearAll_condition2 = 2;
                                } else if ($child_avYear_point < 5 && $point_avgYearAll_condition2 > 3) {
                                    $point_avgYearAll_condition2 = 3;
                                } else if ($child_avYear_point < 6.5 && $point_avgYearAll_condition2 > 4) {
                                    $point_avgYearAll_condition2 = 4;
                                }
                            }
                            // Cộng vào điểm trung bình các môn
                            $child_TBM1_point += $child_TBM1_current_point;
                            $child_TBM2_point += $child_TBM2_current_point;
                            $child_avYearAll_point += $child_avYear_point;
                        }
                        // điểm trung bình các môn
                        $child_TBM1_point /= count($children_point);
                        $child_TBM2_point /= count($children_point);
                        $child_avYearAll_point /= count($children_point);

                        //xét điều kiện để đánh giá pass hay fail
                        //học kỳ 1
                        if ($child_TBM1_point >= 8 && $point_avg1_condition1 == 5 && $point_avg1_condition2 == 5) { // trên 8 điểm thì đánh giá loại giỏi
                            $status_point1 = 5;
                        } else if ($child_TBM1_point >= 8 && ($point_avg1_condition1 == 3 || $point_avg1_condition2 == 3)) {
                            $status_point1 = 4;
                        } else if ($child_TBM1_point >= 8 && ($point_avg1_condition1 == 2 || $point_avg1_condition2 == 2)) {
                            $status_point1 = 3;
                        } else if ($child_TBM1_point >= 6.5 && $point_avg1_condition1 >= 4 && $point_avg1_condition2 >= 4) {  // trên 6.5 điểm thì đánh giá loại tiên tiến
                            $status_point1 = 4;
                        } else if ($child_TBM1_point >= 6.5 && ($point_avg1_condition1 == 2 || $point_avg1_condition2 == 2)) {  // trên 6.5 điểm thì đánh giá loại tiên tiến
                            $status_point1 = 3;
                        } else if ($child_TBM1_point >= 6.5 && ($point_avg1_condition1 == 1 || $point_avg1_condition2 == 1)) {  // trên 6.5 điểm thì đánh giá loại tiên tiến
                            $status_point1 = 2;
                        } else if ($child_TBM1_point >= 5 && $point_avg1_condition1 >= 3 && $point_avg1_condition2 >= 3) {  // trên 5 điểm thì đánh giá loại trung bình
                            $status_point1 = 3;
                        } else if ($child_TBM1_point >= 3.5 && $point_avg1_condition1 >= 2 && $point_avg1_condition2 >= 2) {  // trên 3.5 điểm thì đánh giá loại yếu
                            $status_point1 = 2;
                        } else if ($child_TBM1_point < 3.5 && $point_avg1_condition1 >= 1 && $point_avg1_condition2 >= 1) {
                            $status_point1 = 1;
                        }
                        // học kỳ 2
                        if ($child_TBM2_point >= 8 && $point_avg2_condition1 == 5 && $point_avg2_condition2 == 5) { // trên 8 điểm thì đánh giá loại giỏi
                            $status_point2 = 5;
                        } else if ($child_TBM2_point >= 8 && ($point_avg2_condition1 == 3 || $point_avg2_condition2 == 3)) {
                            $status_point2 = 4;
                        } else if ($child_TBM2_point >= 8 && ($point_avg2_condition1 == 2 || $point_avg2_condition2 == 2)) {
                            $status_point2 = 3;
                        } else if ($child_TBM2_point >= 6.5 && $point_avg2_condition1 >= 4 && $point_avg2_condition2 >= 4) {  // trên 6.5 điểm thì đánh giá loại tiên tiến
                            $status_point2 = 4;
                        } else if ($child_TBM2_point >= 6.5 && ($point_avg2_condition1 == 2 || $point_avg2_condition2 == 2)) {  // trên 6.5 điểm thì đánh giá loại tiên tiến
                            $status_point2 = 3;
                        } else if ($child_TBM2_point >= 6.5 && ($point_avg2_condition1 == 1 || $point_avg2_condition2 == 1)) {  // trên 6.5 điểm thì đánh giá loại tiên tiến
                            $status_point2 = 2;
                        } else if ($child_TBM2_point >= 5 && $point_avg2_condition1 >= 3 && $point_avg2_condition2 >= 3) {  // trên 5 điểm thì đánh giá loại trung bình
                            $status_point2 = 3;
                        } else if ($child_TBM2_point >= 3.5 && $point_avg2_condition1 >= 2 && $point_avg2_condition2 >= 2) {  // trên 3.5 điểm thì đánh giá loại yếu
                            $status_point2 = 2;
                        } else if ($child_TBM2_point < 3.5 && $point_avg2_condition1 >= 1 && $point_avg2_condition2 >= 1) {
                            $status_point2 = 1;
                        }
                        // cả năm
                        if ($child_avYearAll_point >= 8 && $point_avgYearAll_condition1 == 5 && $point_avgYearAll_condition2 == 5) { // trên 8 điểm thì đánh giá loại giỏi
                            $status_pointAll = 5;
                        } else if ($child_avYearAll_point >= 8 && ($point_avgYearAll_condition1 == 3 || $point_avgYearAll_condition2 == 3)) {
                            $status_pointAll = 4;
                        } else if ($child_avYearAll_point >= 8 && ($point_avgYearAll_condition1 == 2 || $point_avgYearAll_condition2 == 2)) {
                            $status_pointAll = 3;
                        } else if ($child_avYearAll_point >= 6.5 && $point_avgYearAll_condition1 >= 4 && $point_avgYearAll_condition2 >= 4) {  // trên 6.5 điểm thì đánh giá loại tiên tiến
                            $status_pointAll = 4;
                        } else if ($child_avYearAll_point >= 6.5 && ($point_avgYearAll_condition1 == 2 || $point_avgYearAll_condition2 == 2)) {  // trên 6.5 điểm thì đánh giá loại tiên tiến
                            $status_pointAll = 3;
                        } else if ($child_avYearAll_point >= 6.5 && ($point_avgYearAll_condition1 == 1 || $point_avgYearAll_condition2 == 1)) {  // trên 6.5 điểm thì đánh giá loại tiên tiến
                            $status_pointAll = 2;
                        } else if ($child_avYearAll_point >= 5 && $point_avgYearAll_condition1 >= 3 && $point_avgYearAll_condition2 >= 3) {  // trên 5 điểm thì đánh giá loại trung bình
                            $status_pointAll = 3;
                        } else if ($child_avYearAll_point >= 3.5 && $point_avgYearAll_condition1 >= 2 && $point_avgYearAll_condition2 >= 2) {  // trên 3.5 điểm thì đánh giá loại yếu
                            $status_pointAll = 2;
                        } else if ($child_avYearAll_point < 3.5 && $point_avgYearAll_condition1 >= 1 && $point_avgYearAll_condition2 >= 1) {
                            $status_pointAll = 1;
                        }

                        // Lấy hạnh kiểm của học sinh
                        $conduct = $conductDao->getConductOfChildren($class_id, $child_id, $school_year);
                        if (!isset($conduct) || !isset($conduct['ck'])) {
                            $flag_enough_point = false;
                        } else if ($child_absent['absent_true'] + $child_absent['absent_false'] <= 45
                            && $status_pointAll >= 3 && $conduct['ck'] != "Weak") {
                            $status_result = 1;
                        }


                    }
                    if (!$flag_enough_point) {
                        $status_result = 0;
                    }
                    $children_point_avgs['tb_hk1'] = number_format($child_TBM1_point, 2);
                    $children_point_avgs['tb_hk2'] = number_format($child_TBM2_point, 2);
                    $children_point_avgs['tb_year'] = number_format($child_avYearAll_point, 2);

                    $result = array();
                    $result['point_avgs'] = (object)$children_point_avgs;
                    $result['school_year'] = $school_year;
                    $result['class_name'] = $class['group_title'];
                    $result['absent_with_permission'] = $child_absent['absent_true'];
                    $result['absent_without_permission'] = $child_absent['absent_false'];
                    $result['status'] = $status_result;
                    $result['subject_reexams'] = $children_subject_reexams;
                }
                return_json(array(
                    'code' => 200,
                    'message' => 'OK',
                    'data' => $result
                ));
            }
            break;
        case 'get_avgs_score_of_student_by_report':
            if (!canEdit($_POST['school_username'], 'points')) {
                _error(403);
            }
            //Lấy ra class_id của lớp
            $class_id = $_POST['class_id'];
//            $school_year = $_POST['school_year'];
            $report_id = $_POST['report_id'];
            // Lấy reprot info
            $report = $reportDao->getReportById($report_id);

            // Lấy school_config
            $child_id = $report['child_id'];
//            // Lấy đầy đủ thông tin của child
            $child_detail = getChildData($child_id, CHILD_CLASS);
            //Lấy ra class_id của lớp
            $class_id = $child_detail['group_id'];
//            $school_year =
//            $child_detail = $childDao->getChild($child_id);
            $schoolConfig = getSchoolData($school['page_id'], SCHOOL_CONFIG);
            // Lấy chi tiết gov_class_level
            $class_level = $classDao->getClassLevelsbyClassId($class_id);
            //Lấy năm tạo report để tạo school_year
            $school_year = date("Y", strtotime($report['created_at'])) . '-' . (date("Y", strtotime($report['created_at'])) + 1);

            // Lấy danh sách điểm của trẻ trong năm học
            $children_point = $pointDao->getChildrenPointsByStudentId($class_id, $school_year, $child_id);
            // Lấy child_name


            $children_point_last = array();
            // Lưu các điểm đã được tính toán
            $children_point_avgs = array();
            // Lưu điểm của các môn thi lại
            $children_subject_reexams = array();
            // flag đánh dấu đã đủ điểm các môn hay chưa
            $child_enough_point = true;
            // trạng thái 0: waitting - chưa đủ điểm, 1: pass, 2: fail, 3: reexam
            $status = 0;
            // điểm các tháng của 2 kỳ
            $child_m11_point = $child_m12_point = $child_m13_point = $child_m21_point = $child_m22_point = $child_m23_point = 0;
            // điểm trung bình các tháng của 2 kỳ
            $child_avM1_point = $child_avM2_point = 0;
            // điểm trung bình của cuối kỳ
            $child_avd1_point = 0;
            $child_avd2_point = 0;
            $child_final_semester1 = $child_final_semester2 = 0;
            // điểm trung bình năm
            $child_avYear_point = 0;
            if (count($children_point) > 0) {
//                $child_id = $childDao->getChildIdByCode($children_point[0]['child_code']);
                // Lấy số buổi nghỉ có phép và không phép của học sinh
                $child_absent = $childDao->getAbsentsChild($child_id, $class_id);
                foreach ($children_point as $child) {
                    // Nếu bất kì điểm nào thiếu sẽ đánh dấu là chưa đủ điểm
                    if (!isset($child['m11']) || !isset($child['m12']) || !isset($child['m13']) || !isset($child['d1'])
                        || !isset($child['m21']) || !isset($child['m22']) || !isset($child['m23']) || !isset($child['d2'])) {
                        $child_enough_point = false;
                    }
                    // tạm thời tính tổng các đầu điểm
                    // kỳ 1
                    $child_m11_point += (int)$child['m11'];
                    $child_m12_point += (int)$child['m12'];
                    $child_m13_point += (int)$child['m13'];
                    $child_avd1_point += (int)$child['d1'];
                    // kỳ 2
                    $child_m21_point += (int)$child['m21'];
                    $child_m22_point += (int)$child['m22'];
                    $child_m23_point += (int)$child['m23'];
                    $child_avd2_point += (int)$child['d2'];

                    // lấy tên môn học
                    $subject_detail = $subjectDao->getSubjectById($child['subject_id']);
                    $child['subject_name'] = $subject_detail['subject_name'];
                    if (isset($subject_detail['re_exam']) && $subject_detail['re_exam']) { // nếu môn đó là môn thi lại thì lưu vào array
                        $subject_reexam['name'] = $subject_detail['subject_name'];
                        $subject_reexam['point'] = $child['re_exam'];
                        $children_subject_reexams[] = $subject_reexam;
                    }
//                        $child['subject_reexam']= $subject_detail['re_exam'];
                    unset($child['school_year'], $child['class_id'], $child['child_code'], $child['point_id']);
                    $children_point_last[] = $child;
                }


                // định nghĩa số bị chia của từng khối
                if ($class_level['gov_class_level'] == '7' || $class_level['gov_class_level'] == '8') {
                    $dividend = 15;
                } else if ($class_level['gov_class_level'] == '9') {
                    $dividend = 11.4;
                } else if ($class_level['gov_class_level'] == '10') {
                    $dividend = 15.26;
                } else if ($class_level['gov_class_level'] == '11') {
                    $dividend = 16.5;
                } else if ($class_level['gov_class_level'] == '12') {
                    $dividend = 14.5;
                }
                // tính điểm theo công thức từng khối
                // kỳ 1
                $child_m11_point /= $dividend;
                $child_m12_point /= $dividend;
                $child_m13_point /= $dividend;
                $child_avd1_point /= $dividend;
                $child_avM1_point = ($child_m11_point + $child_m12_point + $child_m13_point) / 3;
                $child_final_semester1 = ($child_avd1_point + $child_avM1_point) / 2;
                // kỳ 2
                $child_m21_point /= $dividend;
                $child_m22_point /= $dividend;
                $child_m23_point /= $dividend;
                $child_avd2_point /= $dividend;
                $child_avM2_point = ($child_m21_point + $child_m22_point + $child_m23_point) / 3;
                $child_final_semester2 = ($child_avd2_point + $child_avM2_point) / 2;
                // cả năm
                $child_avYear_point = ($child_final_semester1 + $child_final_semester2) / 2;
                // lưu lại vào array
                $children_point_avgs['a1'] = number_format($child_m11_point, 2);
                $children_point_avgs['b1'] = number_format($child_m12_point, 2);
                $children_point_avgs['c1'] = number_format($child_m13_point, 2);
                $children_point_avgs['d1'] = number_format($child_avd1_point, 2);
                $children_point_avgs['x1'] = number_format($child_avM1_point, 2);
                $children_point_avgs['e1'] = number_format($child_final_semester1, 2);
                $children_point_avgs['a2'] = number_format($child_m21_point, 2);
                $children_point_avgs['b2'] = number_format($child_m22_point, 2);
                $children_point_avgs['c2'] = number_format($child_m23_point, 2);
                $children_point_avgs['d2'] = number_format($child_avd2_point, 2);
                $children_point_avgs['x2'] = number_format($child_avM2_point, 2);
                $children_point_avgs['e2'] = number_format($child_final_semester2, 2);
                $children_point_avgs['y'] = number_format($child_avYear_point, 2);

                //xét điều kiện để đánh giá pass hay fail
                if ($child_avYear_point < 25) { // dưới 25 điểm thì fail chặt
                    $status = 2;
                } else { // nếu trên 25 điểm thì xét các điều kiện khác
                    $status = 1;
                    if ((isset($child_absent['absent_true']) && (int)$child_absent['absent_true'] >= 55)
                        || (isset($child_absent['absent_false']) && (int)$child_absent['absent_false'] >= 30)
                        || (((int)$child_absent['absent_true'] + (int)$child_absent['absent_false']) >= 55)) {
                        if ($class_level['gov_class_level'] == '7' || $class_level['gov_class_level'] == '8'
                            || $class_level['gov_class_level'] == '10' || $class_level['gov_class_level'] == '11') {
                            $status = 3;
                        } else if ($class_level['gov_class_level'] == '9' || $class_level['gov_class_level'] == '12') {
                            $status = 1;
                        }
                    }
                }
                // xét đến đánh giá điểm thi lại
                if ($status == 3) {
                    $result_exam = 0;
                    foreach ($children_subject_reexams as $subject_reexam) {
                        if (!isset($subject_reexam['point'])) {
                            $child_enough_point = false;
                        } else {
                            $result_exam += (int)$subject_reexam['point'];
                        }
                    }
                    if ($class_level['gov_class_level'] == '7' || $class_level['gov_class_level'] == '8'
                        || $class_level['gov_class_level'] == '11') {
                        $result_exam /= 4;
                    } else if ($class_level['gov_class_level'] == '10') {
                        $result_exam /= 4;
                    }
                    // nếu điểm thi lại trên 25 điểm thì pass còn lại fail
                    if ($child_enough_point && $result_exam >= 25) {
                        $status = 1;
                    } else if ($child_enough_point && $result_exam < 25) {
                        $status = 2;
                    } else { // Chưa du diem
                        $status = 3;
                    }
                } else {
                    $children_subject_reexams = array();
                }
                if (!$child_enough_point && $status != 3) {
                    $status = 1;
                }
            }
//            $points_key = array();
            $result = array();
            $result['point_avgs'] = (object)$children_point_avgs;
            $result['school_year'] = $school_year;
            $result['class_name'] = $child_detail['group_title'];
//            $result['enough_point'] = $child_enough_point;
            $result['absent_with_permission'] = $child_absent['absent_true'];
            $result['absent_without_permission'] = $child_absent['absent_false'];
            $result['status'] = $status;
//            $result['point_subject'] = $children_point_last;
            $result['subject_reexams'] = $children_subject_reexams;

            return_json(array(
                'code' => 200,
                'message' => 'OK',
                'data' => $result
            ));
            break;
        case 'update_scores':
            if (!canEdit($_POST['school_username'], 'points')) {
                _error(403);
            }

            $results = array();
            $newChildIds = array();
            $new_child_list = array();
            $sheet = array();
            $sheet['error'] = 0;

            // Lấy school_config
            $schoolConfig = getSchoolData($school['page_id'], SCHOOL_CONFIG);
//
//            if ($schoolConfig['grade'] == 1) {
////                $sheet = readPointsInExcelFileC1($inputFileName);
//            } elseif ($schoolConfig['grade'] == 2) {
//                if ($_POST['semester'] == 0) {
////                    $sheet = readPointsInExcelFileC2All($inputFileName);
//                } else {
////                    $sheet = readPointsInExcelFileC2($inputFileName);
//                }
//            }

            //Lấy ra thông số từ form gửi lên
            $class_id = $_POST['class_id'];
            $subject_id = $_POST['subject_id'];
//            $semester = $_POST['semester'];
            $is_last_semester = $_POST['is_last_semester'];
            $school_year = $_POST['school_year'];
            $childs_points = json_decode($_POST['childs_points'], true);

//            // Lấy danh sách điểm của trẻ trong năm học
//            $children_point = $pointDao->getChildrenPointsBySubjectId($class_id, $school_year, $subject_id);
            foreach ($childs_points as $index => $child) {
                if(count($child['points'])==0 && !isset($child['re_exam'])) {
                    unset($childs_points[$index]);
                    continue;
                }
                foreach ($child['points'] as $object) {
                    if($object['key']=="hs17" || $object['key'] == "hs27" || $object['key'] == "gk13" || $object['key'] == "gk23") {
                        return_json(array(
                            'code' => 400,
                            'success' => 'false',
                            'message' => __("Student"). ' '.$child['child_lastname'] . ' ' . $child['child_firstname'].' : '.__("excess score"),
                        ));
                    }
                    $key = $object['key'];
                    $value = $object['value'];
                    $child[$key] = $value;
                }

                unset($child['points']);
                $childs_points[$index] = $child;
            }

            foreach ($childs_points as $child) { // Duyệt danh sách trẻ
                $child['error'] = 0;
                // Kiểm tra xem trẻ có tồn tại trong lớp không
                $is_child = $childDao->checkExistChildByCodeAndClassId($child['child_code'], $class_id);
                if (!$is_child) {
                    $child['error'] = 1;
                    $child['message'] = __("Student not in class");
                    $new_child_list[] = $child;
                } else {
                    if ($child['error'] == 0) {
                        // Kiểm tra xem trẻ đã được tạo bản ghi điểm trước đó chưa
                        $is_create_point = $pointDao->checkCreatedPoint($subject_id, $class_id, $child['child_code'], $school_year);

                        if ($is_create_point) {
                            // Kiểm tra xem điểm gửi lên có khác gì điểm trước đó không, nếu khác thì update, không thì thôi (tạm thời bỏ qua bước này, update luôn
                            $db->autocommit(false);
                            $db->begin_transaction();
                            $pointDao->updateChildPoint($class_id, $subject_id, $school_year, $child);
                            $db->commit();
                            $child['message'] = __("Update point success");
                            $child['child_fullname'] = $child['child_lastname'] . ' ' . $child['child_firstname'];
                            $new_child_list[] = $child;
                        } else {
                            // Insert bản ghi điểm mới
                            try {
                                $new_child_list[] = $child;
                                $db->autocommit(false);
                                $db->begin_transaction();

                                $pointDao->createChildPoint($class_id, $subject_id, $school_year, $child);
                                $db->commit();
                            } catch (Exception $e) {
                                $db->rollback();
                                $child['error'] = 1;
                                $child['message'] = $e->getMessage();
                                return_json(array(
                                    'code' => 200,
                                    'success' => 'false',
                                    'message' => $child['message']
                                ));
                            } finally {
                                $db->autocommit(true);
                            }
                        }
                    }
                }
            }


            $sheet['child_list'] = $new_child_list;
            return_json(array(
                'code' => 200,
                'success' => 'true',
                'message' => 'OK'
            ));
            break;
        default:
            _error(400);
            break;
    }

} catch
(Exception $e) {
    $db->rollback();
    return_json(array('error' => true, 'message' => $e->getMessage()));
} finally {
    $db->autocommit(true);
}
?>