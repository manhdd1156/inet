<?php
/**
 * Package: ajax/ci/api_bo/school
 *
 * @package ConIu
 * @author TaiLA
 */

if(is_empty($_POST['page_name']) || !valid_username($_POST['page_name'])) {
    _error(404);
}

include_once(DAO_PATH.'dao_school.php');
include_once(DAO_PATH.'dao_event.php');
include_once(DAO_PATH.'dao_parent.php');
include_once(DAO_PATH.'dao_teacher.php');
include_once(DAO_PATH.'dao_user.php');
include_once(DAO_PATH.'dao_class.php');
include_once(DAO_PATH.'dao_child.php');

$schoolDao = new SchoolDAO();
$eventDao = new EventDAO();
$parentDao = new ParentDAO();
$teacherDao = new TeacherDAO();
$userDao = new UserDAO();
$classDao = new ClassDAO();
$childDao = new ChildDAO();

//Lấy ra thông tin lớp
//$school = getSchool($_POST['page_name']);
$school = getSchoolDataByUsername($_POST['page_name'], SCHOOL_INFO);
if (is_null($school)) {
    _error(403);
}
$canEdit = canEdit($_POST['page_name'], 'events');

try {
    $return = array();
    $db->autocommit(false);
    // Tăng lượt tương tác - TaiLA
    addInteractive($school['page_id'], 'event', 'school_view');
    switch ($_POST['do']) {
        case 'show_event_for_employee':

            switch ($_POST['view']) {
                case 'all':
                    // Tăng số đếm tương tác của trường
                    increaseSchoolInteractive($school['page_id'], 'event');

                    $page = (isset($_POST['page'])) ? $_POST['page'] : 0;

                    // Kiểm tra quyền của user xem có phải quản lý không
                    if (canView($_POST['page_name'], 'events')) {
                        // $events = $eventDao->getEventsForEmployee($school['page_id']);
                        $events = $eventDao->getEventsForApi($school['page_id'], $page);
                    } else {
                        // $events = $eventDao->getEvents($school['page_id']);
                        $events = $eventDao->getEventsEmployeeForApi($school['page_id'], $page);
                    }


                    // return
                    return_json(array(
                        'code' => 200,
                        'message' => 'OK',
                        'data' => array(
                            'can_edit' => $canEdit,
                            'events' => $events
                        )
                    ));

                    break;

                case 'participant' :
                    $event = $eventDao->getEvent($_POST['event_id']);
                    $participant = array();
                    if($event['for_teacher']) {
                        $participant = $eventDao->getTeacherParticipants($school['page_id'], $event['event_id']);
                        // return
                        return_json(array(
                            'code' => 200,
                            'message' => 'OK',
                            'data' => array(
                                'participant' => $participant
                            )
                        ));
                    } else {
                        $childCount = 0;
                        $parentCount = 0;
                        if ($event['level'] == SCHOOL_LEVEL) {
                            //Lấy ra danh sách lớp của trường hoặc khối
                            $classes = $classDao->getClassNamesOfSchool($school['page_id']);
                            // return
                            return_json(array(
                                'code' => 200,
                                'message' => 'OK',
                                'data' => array(
                                    'classes' => $classes
                                )
                            ));
                        } else if ($event['level'] == CLASS_LEVEL_LEVEL) {
                            $classes = $classDao->getClassesOfLevel($event['class_level_id']);
                            // return
                            return_json(array(
                                'code' => 200,
                                'message' => 'OK',
                                'data' => array(
                                    'classes' => $classes
                                )
                            ));
                        } else {
                            $participant = $eventDao->getParticipantsOfClassForApi($event['class_id'], $event['event_id'], $event['for_parent'], $childCount, $parentCount);
                            return_json(array(
                                'code' => 200,
                                'message' => 'OK',
                                'data' => array(
                                    'participant' => $participant,
                                    'childCount' => $childCount,
                                    'parentCount'=> $parentCount
                                )
                            ));
                        }
                    }
                    break;

                case 'detail':
                    // valid inputs
                    if(!isset($_POST['event_id']) || !is_numeric($_POST['event_id'])) {
                        _error(400);
                    }
                    $event = $eventDao->getEventDetailForApi($_POST['event_id'], true);

                    // Tăng lượt tương tác - TaiLA
                    addInteractive($school['page_id'], 'event', 'school_view', $_POST['event_id']);

                    return_json(array(
                        'code' => 200,
                        'message' => 'OK',
                        'data' => array(
                            'event' => $event
                        )
                    ));

                    break;
            }
            break;

        case 'register':
            $event = $eventDao->getEvent($_POST['event_id']);
            if (is_null($event)) {
                _api_error(0, "The event does not exist, it should be deleted.");
            }
            if (!$event['can_register'] || !$event['for_teacher']) {
                _api_error(403);
            }
            if ($event['for_teacher'] && (isset($_POST['participant_teacher']) && !is_numeric($_POST['participant_teacher']))) {
                _api_error(404);
            }

            $db->autocommit(false);
            $db->begin_transaction();

            if ($event['for_teacher']) {
                if (isset($_POST['participant_teacher'])) {
                    if ($_POST['participant_teacher'] == 1) {
                        $eventDao->insertParticipants($_POST['event_id'], PARTICIPANT_TYPE_TEACHER, [$_POST['user_id']]);
                        //Thông báo cho Quản lý và Giáo viên lớp
                        // Tạm tắt
                        //notifySchoolManagerAndTearcher($_POST['child_id'], NOTIFICATION_REGISTER_EVENT_CHILD, $_POST['event_id'], convertText4Web($event['event_name']));

                    } else {
                        //Huỷ đăng ký cho nhân viên
                        $eventDao->deleteParticipants($_POST['event_id'], PARTICIPANT_TYPE_TEACHER, [$_POST['user_id']]);
                        //Thông báo cho Quản lý và Giáo viên lớp
                        // Tạm tắt
                        // notifySchoolManagerAndTearcher($_POST['child_id'], NOTIFICATION_CANCEL_REGISTRATION_EVENT_CHILD, $_POST['event_id'], convertText4Web($event['event_name']));
                    }
                }
            }
            $db->commit();

            // return
            return_json(array(
                'code' => 200,
                'message' => 'OK',
                'data' => array()
            ));
            break;

        case 'edit':
            if (!$canEdit) {
                _error(403);
            }
            // valid inputs
            if(!isset($_POST['event_id']) || !is_numeric($_POST['event_id'])) {
                _error(400);
            }
            $db->begin_transaction();

            $args = array();
            $args['event_id'] = $_POST['event_id'];

            $args['event_name'] = $_POST['event_name'];
//            $args['location'] = $_POST['location'];
//            $args['price'] = (isset($_POST['price'])? $_POST['price']: 0);
//            $args['begin'] = $_POST['begin'];
//            $args['end'] = $_POST['end'];
            $args['level'] = $_POST['event_level'];
            $args['description'] = $_POST['description'];
            $args['post_on_wall'] = (isset($_POST['post_on_wall']) && $_POST['post_on_wall'] == 'on')? 1: 0;
            $args['post_ids'] = $_POST['post_ids'];
            $args['is_notified'] = (isset($_POST['notify_immediately']) && $_POST['notify_immediately'] == 'on')? 1: 0;
            $args['for_teacher'] = (isset($_POST['for_teacher']) && ($_POST['for_teacher'] == 'on'))? 1: 0;
            $args['school_id'] = $school['page_id'];
            if($args['for_teacher']) {
                $args['level'] = SCHOOL_LEVEL;
                $args['class_level_id'] = 0;
                $args['class_id'] = 0;
            } else {
                if($args['level'] == SCHOOL_LEVEL) {
                    $args['class_level_id'] = 0;
                    $args['class_id'] = 0;
                } elseif ($args['level'] == CLASS_LEVEL_LEVEL) {
                    $args['class_level_id'] = $_POST['class_level_id'];
                    $args['class_id'] = 0;
                } else {
                    $args['class_level_id'] = 0;
                    $args['class_id'] = $_POST['class_id'];
                }
            }
            $args['created_user_id'] = $user->_data['user_id'];
            $args['must_register'] = (isset($_POST['must_register']) && $_POST['must_register'] == 'on')? 1: 0;

            // Nếu phải đăng ký thì phải có deadline
            if($args['must_register']) {
                if(!validateDateTime($_POST['registration_deadline'])) {
                    throw new Exception(__("Please enter the registration deadline"));
                }
                $args['registration_deadline'] = $_POST['registration_deadline'];
            } else {
                $args['registration_deadline'] = '';
            }
            //Nếu không điền thông tin deadline thì hạn đăng ký là ngày bắt đâu sự kiện
//            if (($args['must_register'] == 1) && (!isset($_POST['registration_deadline']) || ($_POST['registration_deadline'] == ''))) {
//                $args['registration_deadline'] = $args['begin'];
//            } else {
//            }
            $args['for_parent'] = (isset($_POST['for_parent']) && ($_POST['for_parent'] == 'on') && ($args['must_register'] == 1) && ($args['level'] != TEACHER_LEVEL))? 1: 0;
            $args['for_child'] = (isset($_POST['for_child']) && ($_POST['for_child'] == 'on')  && ($args['must_register'] == 1) && ($args['level'] != TEACHER_LEVEL))? 1: 0;

            if ($args['for_teacher'] == 0 && $args['must_register'] == 1 && $args['for_parent'] == 0 && $args['for_child'] == 0) {
                throw new Exception(__("Phải chọn đối tượng tham gia trẻ, phụ huynh hay cả hai"));
            }

            //1. Cập nhật thông tin event
            $eventDao->updateEvent($args);
            $userDao->deletePosts($args['post_ids']);

            if ($args['is_notified']) {
                if($args['for_teacher']) {
                    // Thông báo cho giáo viên/nhân viên trong trường
                    //$teacherIds = $teacherDao->getTeacherIDOfSchool($school['page_id']);
                    $teachers = getSchoolData($school['page_id'], SCHOOL_TEACHERS);
                    $teacherIds = array_keys($teachers);
                    $teacherIds[] = $school['page_admin'];
                    $userDao->postNotifications($teacherIds, NOTIFICATION_UPDATE_EVENT_SCHOOL, NOTIFICATION_NODE_TYPE_SCHOOL, $_POST['event_id'], convertText4Web($_POST['event_name']), $school['page_name'], convertText4Web($school['page_title']));
                    // Chỉ thông báo luôn thì mới đăng lên tường
                    if ($args['post_on_wall']) {
                        $content = $args['event_name']."<br/><br/>".$args['description'];
                        $postId = $userDao->postOnPage($school, $content);
                        $eventDao->updatePostIds($postId, $eventId);
                    }
                } else {
                    switch ($args['level']) {
                        case SCHOOL_LEVEL: //Trường hợp sự kiện cấp trường thì
                            //2. Thông báo các đối tượng liên quan với trường hợp user click vào Save and Notify
                            // - Chỉ giáo viên dạy ít nhất một lớp mới nhận được
                            // - Tất cả phụ huynh đều nhận được.

                            // Lấy danh sách lớp của trường
                            $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                            if(count($classes) > 0) {
                                foreach ($classes as $class) {
                                    // Lấy danh sách giáo viên của lớp
                                    $teachers = getClassData($class['group_id'], CLASS_TEACHERS);
                                    $teacherIds = array_keys($teachers);

                                    if(count($teacherIds) > 0) {
                                        // Gửi thông báo về cho giáo viên lớp
                                        $userDao->postNotifications($teacherIds, NOTIFICATION_UPDATE_EVENT_SCHOOL, NOTIFICATION_NODE_TYPE_CLASS, $args['event_id'], convertText4Web($args['event_name']), $class['group_name']);
                                    }

                                    // Lấy danh sách trẻ trong lớp
                                    $children = getClassData($class['group_id'], CLASS_CHILDREN);
                                    foreach ($children as $child) {
                                        // Lấy danh sách phụ huynh của trẻ
                                        $parents = getChildData($child['child_id'], CHILD_PARENTS);
                                        $parentIds = array_keys($parents);

                                        if(count($parentIds) > 0) {
                                            // Gửi thông báo đến phụ huynh
                                            $userDao->postNotifications($parentIds, NOTIFICATION_UPDATE_EVENT_SCHOOL, NOTIFICATION_NODE_TYPE_CHILD, $args['event_id'], convertText4Web($args['event_name']), $child['child_id']);
                                        }
                                    }
                                }
                            }

                            // Thông báo cho các quản lý trường khác sự kiện được cập nhật
                            $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'events', $school['page_admin']);

                            if(count($userManagerIds) > 0) {
                                $userDao->postNotifications($userManagerIds, NOTIFICATION_UPDATE_EVENT_SCHOOL, NOTIFICATION_NODE_TYPE_SCHOOL,
                                    $_POST['event_id'], convertText4Web($_POST['event_name']), $school['page_name'], convertText4Web($school['page_title']));
                            }

                            //3.Post sự kiện lên tường của trường. Chỉ có sự kiện luôn thì mới đưa lên tường
                            if ($args['post_on_wall']) {
                                $content = $args['event_name'] . "<br/><br/>" . $args['description'];
                                $postId = $userDao->postOnPage($school, $content);
                                $eventDao->updatePostIds($postId, $args['event_id']);
                            }

                            break;
                        case CLASS_LEVEL_LEVEL:
                            //$classIds = $classDao->getClassIdOfLevel($_POST['class_level_id']);
                            $classes = getClassLevelData($_POST['class_level_id'], CLASS_LEVEL_CLASSES);
                            //2. Thông báo các đối tượng liên quan với trường hợp user click vào Save and Notify
                            // - Tất cả giáo viên trong khối đều nhận được
                            // - Tất cả phụ huynh của khối đều nhận được.

                            if(count($classes) > 0) {
                                foreach ($classes as $class) {
                                    $teachers = getClassData($class['group_id'], CLASS_TEACHERS);
                                    $teacherIds = array_keys($teachers);
                                    if(count($teacherIds) > 0) {
                                        // Gửi thông báo đến cho giáo viên
                                        $userDao->postNotifications($teacherIds, NOTIFICATION_UPDATE_EVENT_CLASS_LEVEL, NOTIFICATION_NODE_TYPE_CLASS, $args['event_id'], convertText4Web($args['event_name']), $class['group_name']);
                                    }

                                    // Lấy danh sách trẻ trong lớp
                                    $children = getClassData($class['group_id'], CLASS_CHILDREN);
                                    foreach ($children as $child) {
                                        // Lấy danh sách phụ huynh của trẻ
                                        $parents = getChildData($child['child_id'], CHILD_PARENTS);
                                        $parentIds = array_keys($parents);
                                        if(count($parentIds) > 0) {
                                            // THông báo đến phụ huynh của trẻ
                                            $userDao->postNotifications($parentIds, NOTIFICATION_UPDATE_EVENT_CLASS_LEVEL, NOTIFICATION_NODE_TYPE_CHILD, $args['event_id'], convertText4Web($args['event_name']), $child['child_id']);
                                        }
                                    }
                                }
                            }

                            // Thông báo cho các quản lý trường khác sự kiện được cập nhật
                            $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'events', $school['page_admin']);
                            if(count($userManagerIds) > 0) {
                                $userDao->postNotifications($userManagerIds, NOTIFICATION_UPDATE_EVENT_CLASS_LEVEL, NOTIFICATION_NODE_TYPE_SCHOOL,
                                    $_POST['event_id'], convertText4Web($_POST['event_name']), $school['page_name'], convertText4Web($school['page_title']));
                            }

                            //3.Post sự kiện lên tường của các lớp trong khối. Chỉ có thông báo luôn thì mới đưa lên tường
                            if ($args['post_on_wall']) {
                                $content = $args['event_name'] . "<br/><br/>" . $args['description'];
                                $postIds = "";
                                foreach ($classes as $class) {
                                    //$class = $classDao->getClass($classId);
                                    $postIds = $postIds . "," . $userDao->postOnGroup($class, $content, $school['page_id']);
                                }
                                $postIds = trim($postIds, ",");
                                $eventDao->updatePostIds($postIds, $args['event_id']);
                            }
                            break;
                        case CLASS_LEVEL:
                            //2. Thông báo các đối tượng liên quan với trường hợp user click vào Save and Notify
                            // - Tất cả giáo viên của lớp đều nhận được
                            // - Tất cả phụ huynh của lớp đều nhận được.
                            //$teacherIds = $teacherDao->getTeacherIDOfClass($_POST['class_id']);
                            $class = getClassData($_POST['class_id'], CLASS_INFO);
                            $teachers = getClassData($_POST['class_id'], CLASS_TEACHERS);

                            // Gửi thông báo cho giáo viên
                            $teacherIds = array_keys($teachers);
                            if(count($teacherIds) > 0) {
                                $userDao->postNotifications($teacherIds, NOTIFICATION_UPDATE_EVENT_CLASS, NOTIFICATION_NODE_TYPE_CLASS, $args['event_id'], convertText4Web($args['event_name']), $class['group_name']);
                            }

                            // Lấy danh sách trẻ trong lớp
                            $children = getClassData($_POST['class_id'], CLASS_CHILDREN);
                            if(count($children) > 0) {
                                foreach ($children as $child) {
                                    // Lấy danh sách phụ huynh của trẻ
                                    $parents = getChildData($child['child_id'], CHILD_PARENTS);
                                    $parentIds = array_keys($parents);

                                    if(count($parentIds) > 0) {
                                        $userDao->postNotifications($parentIds, NOTIFICATION_UPDATE_EVENT_CLASS, NOTIFICATION_NODE_TYPE_CHILD, $args['event_id'], convertText4Web($args['event_name']), $child['child_id']);
                                    }
                                }
                            }

                            // Thông báo cho các quản lý trường khác sự kiện được cập nhật
                            $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'events', $school['page_admin']);
                            $userDao->postNotifications($userManagerIds, NOTIFICATION_UPDATE_EVENT_CLASS, NOTIFICATION_NODE_TYPE_SCHOOL,
                                $_POST['event_id'], convertText4Web($_POST['event_name']), $school['page_name'], convertText4Web($school['page_title']));

                            //$class = $classDao->getClass($args['class_id']);
                            $class = getClassData($args['class_id'], CLASS_INFO);
                            //3.Post thông tin sự kiên lên tường của lớp. Chỉ có thông báo luôn thì mới đưa lên tường
                            if ($args['post_on_wall']) {
                                $content = $args['event_name'] . "<br/><br/>" . $args['description'];
                                $postId = $userDao->postOnGroup($class, $content, $school['page_id']);
                                $eventDao->updatePostIds($postId, $args['event_id']);
                            }

                            break;
                        default:
                            _error(400);
                            break;
                    }
                }
            }

            $db->commit();
            // return
            return_json(array(
                'code' => 200,
                'message' => __("Done, Event info have been updated"),
                'data' => array()
            ));
            break;

        case 'add':
            if (!$canEdit) {
                _error(403);
            }
            $db->begin_transaction();

            $args = array();
            $args['event_name'] = $_POST['event_name'];
//            $args['location'] = $_POST['location'];
//            $args['price'] = (isset($_POST['price'])? $_POST['price']: 0);
//            $args['begin'] = $_POST['begin'];
//            $args['end'] = $_POST['end'];
            $args['level'] = $_POST['event_level'];
            $args['description'] = $_POST['description'];
            $args['post_on_wall'] = (isset($_POST['post_on_wall']) && $_POST['post_on_wall'] == 'on')? 1: 0;
            $args['is_notified'] = (isset($_POST['notify_immediately']) && $_POST['notify_immediately'] == 'on')? 1: 0;
            $args['school_id'] = $school['page_id'];
            $args['for_teacher'] = (isset($_POST['for_teacher']) && $_POST['for_teacher'] == 'on')? 1: 0;
            if($args['for_teacher']) {
                $args['level'] = SCHOOL_LEVEL;
                $args['class_level_id'] = 0;
                $args['class_id'] = 0;
            } else {
                if($args['level'] == SCHOOL_LEVEL) {
                    $args['class_level_id'] = 0;
                    $args['class_id'] = 0;
                } elseif ($args['level'] == CLASS_LEVEL_LEVEL) {
                    $args['class_level_id'] = $_POST['class_level_id'];
                    $args['class_id'] = 0;
                } else {
                    $args['class_level_id'] = 0;
                    $args['class_id'] = $_POST['class_id'];
                }
            }
            $args['created_user_id'] = $user->_data['user_id'];
            $args['must_register'] = (isset($_POST['must_register']) && $_POST['must_register'] == 'on')? 1: 0;

            // Nếu phải đăng ký thì phải có deadline
            if($args['must_register']) {
                if(!validateDateTime($_POST['registration_deadline'])) {
                    throw new Exception(__("Please enter the registration deadline"));
                }
                $args['registration_deadline'] = $_POST['registration_deadline'];
            } else {
                $args['registration_deadline'] = '';
            }

            //Nếu không điền thông tin deadline thì hạn đăng ký là ngày bắt đâu sự kiện
//            if (($args['must_register'] == 1) && (!isset($_POST['registration_deadline']) || ($_POST['registration_deadline'] == ''))) {
//                $args['registration_deadline'] = $args['begin'];
//            } else {
//            }
            $args['for_parent'] = (isset($_POST['for_parent']) && ($_POST['for_parent'] == 'on') && ($args['must_register'] == 1) && (!$args['for_teacher']))? 1: 0;
            $args['for_child'] = (isset($_POST['for_child']) && ($_POST['for_child'] == 'on') && ($args['must_register'] == 1) && (!$args['for_teacher']))? 1: 0;

            if ($args['for_teacher'] == 0 && $args['must_register'] == 1 && $args['for_parent'] == 0 && $args['for_child'] == 0) {
                throw new Exception(__("Phải chọn đối tượng tham gia trẻ, phụ huynh hay cả hai"));
            }
            //1. Đưa thông báo vào hệ thống
            $eventId = $eventDao->insertEvent($args);

            if ($args['is_notified']) {
                if($args['for_teacher']) {
                    // Thông báo cho giáo viên/nhân viên trong trường
                    //$teacherIds = $teacherDao->getTeacherIDOfSchool($school['page_id']);
                    $teachers = getSchoolData($school['page_id'], SCHOOL_TEACHERS);
                    $teacherIds = array_keys($teachers);
                    $teacherIds[] = $school['page_admin'];
                    $userDao->postNotifications($teacherIds, NOTIFICATION_NEW_EVENT_SCHOOL, NOTIFICATION_NODE_TYPE_SCHOOL, $eventId, convertText4Web($_POST['event_name']), $school['page_name'], convertText4Web($school['page_title']));
                    // Chỉ thông báo luôn thì mới đăng lên tường
                    if ($args['post_on_wall']) {
                        $content = $args['event_name']."<br/><br/>".$args['description'];
                        $postId = $userDao->postOnPage($school, $content);
                        $eventDao->updatePostIds($postId, $eventId);
                    }
                } else {
                    switch ($args['level']) {
                        case SCHOOL_LEVEL: //Trường hợp cấp trường cấp trường thì
                            //2. Thông báo các đối tượng liên quan với trường hợp user click vào Save and Notify
                            // Nếu không phải sự kiện dành cho giáo viên thì thông báo về cho phụ huynh và giáo viên lớp
                            // - Tất cả giáo viên trong trường đều nhận được
                            // - Tất cả phụ huynh đều nhận được.

                            // Lấy danh sách lớp của trường
                            $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                            if(count($classes) > 0) {
                                foreach ($classes as $class) {
                                    // Lấy danh sách giáo viên của lớp
                                    $teachers = getClassData($class['group_id'], CLASS_TEACHERS);
                                    $teacherIds = array_keys($teachers);

                                    if(count($teacherIds) > 0) {
                                        // Gửi thông báo về cho giáo viên lớp
                                        $userDao->postNotifications($teacherIds, NOTIFICATION_NEW_EVENT_SCHOOL, NOTIFICATION_NODE_TYPE_CLASS, $eventId, convertText4Web($args['event_name']), $class['group_name']);
                                    }

                                    // Lấy danh sách trẻ trong lớp
                                    $children = getClassData($class['group_id'], CLASS_CHILDREN);
                                    foreach ($children as $child) {
                                        // Lấy danh sách phụ huynh của trẻ
                                        $parents = getChildData($child['child_id'], CHILD_PARENTS);
                                        $parentIds = array_keys($parents);

                                        if(count($parentIds) > 0) {
                                            // Gửi thông báo đến phụ huynh
                                            $userDao->postNotifications($parentIds, NOTIFICATION_NEW_EVENT_SCHOOL, NOTIFICATION_NODE_TYPE_CHILD, $eventId, convertText4Web($args['event_name']), $child['child_id']);
                                        }
                                    }
                                }
                            }

                            // Thông báo cho các quản lý trường khác sự kiện được cập nhật
                            $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'events', $school['page_admin']);
                            if(count($userManagerIds) > 0) {
                                $userDao->postNotifications($userManagerIds, NOTIFICATION_NEW_EVENT_SCHOOL, NOTIFICATION_NODE_TYPE_SCHOOL,
                                    $eventId, convertText4Web($_POST['event_name']), $school['page_name'], convertText4Web($school['page_title']));
                            }

                            //3.Post thông tin sự kiên lên tường của trường.
                            // Chỉ có thông báo luôn thì mới đưa lên tường
                            if ($args['post_on_wall']) {
                                $content = $args['event_name']."<br/><br/>".$args['description'];
                                $postId = $userDao->postOnPage($school, $content);
                                $eventDao->updatePostIds($postId, $eventId);
                            }

                            break;
                        case CLASS_LEVEL_LEVEL:
                            //$classIds = $classDao->getClassIdOfLevel($_POST['class_level_id']);
                            $classes = getClassLevelData($_POST['class_level_id'], CLASS_LEVEL_CLASSES);

                            if(count($classes) > 0) {
                                foreach ($classes as $class) {
                                    $teachers = getClassData($class['group_id'], CLASS_TEACHERS);
                                    $teacherIds = array_keys($teachers);
                                    if(count($teacherIds) > 0) {
                                        // Gửi thông báo đến cho giáo viên
                                        $userDao->postNotifications($teacherIds, NOTIFICATION_NEW_EVENT_CLASS_LEVEL, NOTIFICATION_NODE_TYPE_CLASS, $eventId, convertText4Web($args['event_name']), $class['group_name']);
                                    }

                                    // Lấy danh sách trẻ trong lớp
                                    $children = getClassData($class['group_id'], CLASS_CHILDREN);
                                    foreach ($children as $child) {
                                        // Lấy danh sách phụ huynh của trẻ
                                        $parents = getChildData($child['child_id'], CHILD_PARENTS);
                                        $parentIds = array_keys($parents);
                                        if(count($parentIds) > 0) {
                                            // THông báo đến phụ huynh của trẻ
                                            $userDao->postNotifications($parentIds, NOTIFICATION_NEW_EVENT_CLASS_LEVEL, NOTIFICATION_NODE_TYPE_CHILD, $eventId, convertText4Web($args['event_name']), $child['child_id']);
                                        }
                                    }
                                }
                            }

                            // Thông báo cho các quản lý trường khác sự kiện được cập nhật
                            $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'events', $school['page_admin']);
                            $userDao->postNotifications($userManagerIds, NOTIFICATION_NEW_EVENT_CLASS_LEVEL, NOTIFICATION_NODE_TYPE_SCHOOL,
                                $eventId, convertText4Web($_POST['event_name']), $school['page_name'], convertText4Web($school['page_title']));

                            //3.Post thông tin sự kiên lên tường của các lớp trong khối.
                            // Chỉ có thông báo luôn thì mới đưa lên tường
                            if ($args['post_on_wall']) {
                                $content = $args['event_name']."<br/><br/>".$args['description'];
                                $postIds = "";
                                foreach ($classes as $class) {
                                    //$class = $classDao->getClass($classId);
                                    $class = getClassData($class['group_id'], CLASS_INFO);
                                    $postIds = $postIds.",".$userDao->postOnGroup($class, $content, $school['page_id']);
                                }
                                $postIds = trim($postIds, ",");
                                $eventDao->updatePostIds($postIds, $eventId);
                            }
                            break;
                        case CLASS_LEVEL:
                            //2. Thông báo các đối tượng liên quan với trường hợp user click vào Save and Notify
                            // - Tất cả giáo viên của lớp đều nhận được
                            // - Tất cả phụ huynh của lớp đều nhận được.
                            //$teacherIds = $teacherDao->getTeacherIDOfClass($_POST['class_id']);
                            $class = getClassData($_POST['class_id'], CLASS_INFO);
                            $teachers = getClassData($_POST['class_id'], CLASS_TEACHERS);

                            // Gửi thông báo cho giáo viên
                            $teacherIds = array_keys($teachers);
                            if(count($teacherIds) > 0) {
                                $userDao->postNotifications($teacherIds, NOTIFICATION_NEW_EVENT_CLASS, NOTIFICATION_NODE_TYPE_CLASS, $eventId, convertText4Web($args['event_name']), $class['group_name']);
                            }

                            // Lấy danh sách trẻ trong lớp
                            $children = getClassData($_POST['class_id'], CLASS_CHILDREN);
                            if(count($children) > 0) {
                                foreach ($children as $child) {
                                    // Lấy danh sách phụ huynh của trẻ
                                    $parents = getChildData($child['child_id'], CHILD_PARENTS);
                                    $parentIds = array_keys($parents);

                                    if(count($parentIds) > 0) {
                                        $userDao->postNotifications($parentIds, NOTIFICATION_NEW_EVENT_CLASS, NOTIFICATION_NODE_TYPE_CHILD, $eventId, convertText4Web($args['event_name']), $child['child_id']);
                                    }
                                }
                            }

                            // Thông báo cho các quản lý trường khác sự kiện được thêm mới
                            $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'events', $school['page_admin']);
                            $userDao->postNotifications($userManagerIds, NOTIFICATION_NEW_EVENT_CLASS, NOTIFICATION_NODE_TYPE_SCHOOL,
                                $eventId, convertText4Web($_POST['event_name']), $school['page_name'], convertText4Web($school['page_title']));

                            //$class = $classDao->getClass($args['class_id']);
                            $class = getClassData($args['class_id'], CLASS_INFO);
                            //3.Post thông tin sự kiên lên tường của lớp. Chỉ có thông báo luôn thì mới đưa lên tường
                            if ($args['post_on_wall']) {
                                $content = $args['event_name']."<br/><br/>".$args['description'];
                                $postId = $userDao->postOnGroup($class, $content, $school['page_id']);
                                $eventDao->updatePostIds($postId, $eventId);
                            }

                            break;
                        default:
                            _error(400);
                            break;
                    }

                    /* Coniu - Tương tác trường */
                    $eventIds = array();
                    $eventIds[] = $eventId;
                    setIsAddNew($school['page_id'], 'event_created', $eventIds);
                    /* Coniu - END */
                }
            }

            // Tăng lượt thêm mới
            addInteractive($school['page_id'], 'event', 'school_view', $eventId, 1);

            $db->commit();
            return_json(array(
                'code' => 200,
                'message' => __("Done, Event info have been created"),
                'data' => array()
            ));
            break;

        case 'notify':
            if (!$canEdit) {
                _error(403);
            }
            $db->begin_transaction();
            //Lấy ra thông tin thông báo
            $event = $eventDao->getEvent($_POST['event_id']);

            if (!$event['is_notified']) {
                if($event['for_teacher']) {
                    // Thông báo cho giáo viên/nhân viên trong trường
                    //$teacherIds = $teacherDao->getTeacherIDOfSchool($school['page_id']);
                    $teachers = getSchoolData($school['page_id'], SCHOOL_TEACHERS);
                    $teacherIds = array_keys($teachers);
                    $teacherIds[] = $school['page_admin'];
                    $userDao->postNotifications($teacherIds, NOTIFICATION_NEW_EVENT_SCHOOL, NOTIFICATION_NODE_TYPE_SCHOOL, $_POST['event_id'], convertText4Web($event['event_name']), $school['page_name'], convertText4Web($school['page_title']));
                    // Chỉ thông báo luôn thì mới đăng lên tường
                    if ($event['post_on_wall']) {
                        $content = convertText4Web($event['event_name']."<br/><br/>".$event['description']);
                        $postId = $userDao->postOnPage($school, $content);
                        $eventDao->updatePostIds($postId, $event['event_id']);
                    }
                } else {
//                    switch ($event['level']) {
//                        case SCHOOL_LEVEL: //Trường hợp sự kiện cấp trường thì
//                            //2. Thông báo các đối tượng liên quan với trường hợp user click vào Save and Notify
//                            // - Tất cả giáo viên trong trường đều nhận được
//                            // - Tất cả phụ huynh đều nhận được.
//                            //$teacherIds = $teacherDao->getTeacherIDClassOfSchool($school['page_id']);
//                            $teachers = getSchoolData($school['page_id'], SCHOOL_TEACHERS);
//                            $teacherIds = array_keys($teachers);
//                            $userDao->postNotifications($teacherIds, NOTIFICATION_NEW_EVENT_SCHOOL, NOTIFICATION_NODE_TYPE_CLASS, $event['event_id'], convertText4Web($event['event_name']));
//
//                            $parentIds = $parentDao->getParentIdOfSchool($school['page_id']);
//                            $userDao->postNotifications($parentIds, NOTIFICATION_NEW_EVENT_SCHOOL, NOTIFICATION_NODE_TYPE_CHILD, $event['event_id'], convertText4Web($event['event_name']));
//                            // Thông báo cho các quản lý trường khác sự kiện được cập nhật
//                            $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'events', $school['page_admin']);
//                            $userDao->postNotifications($userManagerIds, NOTIFICATION_NEW_EVENT_SCHOOL, NOTIFICATION_NODE_TYPE_SCHOOL,
//                                $event['event_id'], convertText4Web($event['event_name']), $school['page_name'], convertText4Web($school['page_title']));
//
//                            //3.Post sự kiện lên tường của trường. Chỉ có sự kiện luôn thì mới đưa lên tường
//                            if ($event['post_on_wall']) {
//                                $content = convertText4Web($event['event_name'] . "<br/><br/>" . $event['description']);
//                                $postId = $userDao->postOnPage($school, $content);
//                                $eventDao->updatePostIds($postId, $event['event_id']);
//                            }
//
//                            break;
//                        case CLASS_LEVEL_LEVEL:
//                            $classIds = $classDao->getClassIdOfLevel($event['class_level_id']);
//                            //2. Thông báo các đối tượng liên quan với trường hợp user click vào Save and Notify
//                            // - Tất cả giáo viên trong khối đều nhận được
//                            // - Tất cả phụ huynh của khối đều nhận được.
//                            $teacherIds = array();
//                            $parentIds = array();
//                            foreach ($classIds as $classId) {
//                                $teachers = getClassData($classId, CLASS_TEACHERS);
//                                $teacherIds = array_merge($teacherIds, array_keys($teachers));
//                                $parentIds = array_merge($parentIds, $parentDao->getParentIdOfClass($classId));
//                            }
//                            $userDao->postNotifications($teacherIds, NOTIFICATION_NEW_EVENT_CLASS_LEVEL, NOTIFICATION_NODE_TYPE_CLASS, $event['event_id'], convertText4Web($event['event_name']));
//                            $userDao->postNotifications($parentIds, NOTIFICATION_NEW_EVENT_CLASS_LEVEL, NOTIFICATION_NODE_TYPE_CHILD, $event['event_id'], convertText4Web($event['event_name']));
//
//                            // Thông báo cho các quản lý trường khác sự kiện được cập nhật
//                            $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'events', $school['page_admin']);
//                            $userDao->postNotifications($userManagerIds, NOTIFICATION_NEW_EVENT_CLASS_LEVEL, NOTIFICATION_NODE_TYPE_SCHOOL,
//                                $event['event_id'], convertText4Web($event['event_name']), $school['page_name'], convertText4Web($school['page_title']));
//
//                            //3.Post sự kiện lên tường của các lớp trong khối. Chỉ có thông báo luôn thì mới đưa lên tường
//                            if ($event['post_on_wall']) {
//                                $content = $event['event_name'] . "<br/><br/>" . $event['description'];
//                                $postIds = "";
//                                foreach ($classIds as $classId) {
//                                    //$class = $classDao->getClass($classId);
//                                    $class = getClassData($classId, CLASS_INFO);
//                                    $postIds = $postIds . "," . $userDao->postOnGroup($class, $content, $school['page_id']);
//                                }
//                                $postIds = trim($postIds, ",");
//                                $eventDao->updatePostIds($postIds, $_POST['id']);
//                            }
//                            break;
//                        case CLASS_LEVEL:
//                            //2. Thông báo các đối tượng liên quan với trường hợp user click vào Save and Notify
//                            // - Tất cả giáo viên của lớp đều nhận được
//                            // - Tất cả phụ huynh của lớp đều nhận được.
//                            //$teacherIds = $teacherDao->getTeacherIDOfClass($event['class_id']);
//                            $teachers = getClassData($event['class_id'], CLASS_TEACHERS);
//                            $teacherIds = array_keys($teachers);
//                            $userDao->postNotifications($teacherIds, NOTIFICATION_NEW_EVENT_CLASS, NOTIFICATION_NODE_TYPE_CLASS, $event['event_id'], convertText4Web($event['event_name']));
//                            $parentIds = $parentDao->getParentIdOfClass($event['class_id']);
//                            $userDao->postNotifications($parentIds, NOTIFICATION_NEW_EVENT_CLASS, NOTIFICATION_NODE_TYPE_CHILD, $event['event_id'], convertText4Web($event['event_name']));
//
//                            // Thông báo cho các quản lý trường khác sự kiện được cập nhật
//                            $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'events', $school['page_admin']);
//                            $userDao->postNotifications($userManagerIds, NOTIFICATION_NEW_EVENT_CLASS, NOTIFICATION_NODE_TYPE_SCHOOL,
//                                $event['event_id'], convertText4Web($event['event_name']), $school['page_name'], convertText4Web($school['page_title']));
//
//                            //$class = $classDao->getClass($event['class_id']);
//                            $class = getClassData($event['class_id'], CLASS_INFO);
//                            //3.Post thông tin sự kiên lên tường của lớp. Chỉ có thông báo luôn thì mới đưa lên tường
//                            if ($event['post_on_wall']) {
//                                $content = $event['event_name'] . "<br/><br/>" . $event['description'];
//                                $postId = $userDao->postOnGroup($class, $content, $school['page_id']);
//                                $eventDao->updatePostIds($postId, $_POST['event_id']);
//                            }
//
//                            break;
//                        default:
//                            _error(400);
//                            break;
//                    }

                    switch ($event['level']) {
                        case SCHOOL_LEVEL: //Trường hợp sự kiện cấp trường thì
                            //2. Thông báo các đối tượng liên quan với trường hợp user click vào Save and Notify
                            // - Chỉ giáo viên dạy ít nhất một lớp mới nhận được
                            // - Tất cả phụ huynh đều nhận được.

                            // Lấy danh sách lớp của trường
                            $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                            if(count($classes) > 0) {
                                foreach ($classes as $class) {
                                    // Lấy danh sách giáo viên của lớp
                                    $teachers = getClassData($class['group_id'], CLASS_TEACHERS);
                                    $teacherIds = array_keys($teachers);

                                    if(count($teacherIds) > 0) {
                                        // Gửi thông báo về cho giáo viên lớp
                                        $userDao->postNotifications($teacherIds, NOTIFICATION_UPDATE_EVENT_SCHOOL, NOTIFICATION_NODE_TYPE_CLASS, $event['event_id'], convertText4Web($event['event_name']), $class['group_name']);
                                    }

                                    // Lấy danh sách trẻ trong lớp
                                    $children = getClassData($class['group_id'], CLASS_CHILDREN);
                                    foreach ($children as $child) {
                                        // Lấy danh sách phụ huynh của trẻ
                                        $parents = getChildData($child['child_id'], CHILD_PARENTS);
                                        $parentIds = array_keys($parents);

                                        if(count($parentIds) > 0) {
                                            // Gửi thông báo đến phụ huynh
                                            $userDao->postNotifications($parentIds, NOTIFICATION_UPDATE_EVENT_SCHOOL, NOTIFICATION_NODE_TYPE_CHILD, $event['event_id'], convertText4Web($event['event_name']), $child['child_id']);
                                        }
                                    }
                                }
                            }

                            // Thông báo cho các quản lý trường khác sự kiện được cập nhật
                            $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'events', $school['page_admin']);

                            if(count($userManagerIds) > 0) {
                                $userDao->postNotifications($userManagerIds, NOTIFICATION_UPDATE_EVENT_SCHOOL, NOTIFICATION_NODE_TYPE_SCHOOL,
                                    $event['event_id'], convertText4Web($event['event_name']), $school['page_name'], convertText4Web($school['page_title']));
                            }

                            //3.Post sự kiện lên tường của trường. Chỉ có sự kiện luôn thì mới đưa lên tường
                            if ($event['post_on_wall']) {
                                $content = convertText4Web($event['event_name'] . "<br/><br/>" . $event['description']);
                                $postId = $userDao->postOnPage($school, $content);
                                $eventDao->updatePostIds($postId, $event['event_id']);
                            }

                            break;
                        case CLASS_LEVEL_LEVEL:
                            //$classIds = $classDao->getClassIdOfLevel($_POST['class_level_id']);
                            $classes = getClassLevelData($event['class_level_id'], CLASS_LEVEL_CLASSES);
                            //2. Thông báo các đối tượng liên quan với trường hợp user click vào Save and Notify
                            // - Tất cả giáo viên trong khối đều nhận được
                            // - Tất cả phụ huynh của khối đều nhận được.

                            if(count($classes) > 0) {
                                foreach ($classes as $class) {
                                    $teachers = getClassData($class['group_id'], CLASS_TEACHERS);
                                    $teacherIds = array_keys($teachers);
                                    if(count($teacherIds) > 0) {
                                        // Gửi thông báo đến cho giáo viên
                                        $userDao->postNotifications($teacherIds, NOTIFICATION_UPDATE_EVENT_CLASS_LEVEL, NOTIFICATION_NODE_TYPE_CLASS, $event['event_id'], convertText4Web($event['event_name']), $class['group_name']);
                                    }

                                    // Lấy danh sách trẻ trong lớp
                                    $children = getClassData($class['group_id'], CLASS_CHILDREN);
                                    foreach ($children as $child) {
                                        // Lấy danh sách phụ huynh của trẻ
                                        $parents = getChildData($child['child_id'], CHILD_PARENTS);
                                        $parentIds = array_keys($parents);
                                        if(count($parentIds) > 0) {
                                            // THông báo đến phụ huynh của trẻ
                                            $userDao->postNotifications($parentIds, NOTIFICATION_UPDATE_EVENT_CLASS_LEVEL, NOTIFICATION_NODE_TYPE_CHILD, $event['event_id'], convertText4Web($event['event_name']), $child['child_id']);
                                        }
                                    }
                                }
                            }

                            // Thông báo cho các quản lý trường khác sự kiện được cập nhật
                            $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'events', $school['page_admin']);
                            if(count($userManagerIds) > 0) {
                                $userDao->postNotifications($userManagerIds, NOTIFICATION_UPDATE_EVENT_CLASS_LEVEL, NOTIFICATION_NODE_TYPE_SCHOOL,
                                    $event['event_id'], convertText4Web($event['event_name']), $school['page_name'], convertText4Web($school['page_title']));
                            }

                            //3.Post sự kiện lên tường của các lớp trong khối. Chỉ có thông báo luôn thì mới đưa lên tường
                            if ($event['post_on_wall']) {
                                $content = convertText4Web($event['event_name'] . "<br/><br/>" . $event['description']);
                                $postIds = "";
                                foreach ($classes as $class) {
                                    //$class = $classDao->getClass($classId);
                                    $postIds = $postIds . "," . $userDao->postOnGroup($class, $content, $school['page_id']);
                                }
                                $postIds = trim($postIds, ",");
                                $eventDao->updatePostIds($postIds, $event['event_id']);
                            }
                            break;
                        case CLASS_LEVEL:
                            //2. Thông báo các đối tượng liên quan với trường hợp user click vào Save and Notify
                            // - Tất cả giáo viên của lớp đều nhận được
                            // - Tất cả phụ huynh của lớp đều nhận được.
                            //$teacherIds = $teacherDao->getTeacherIDOfClass($_POST['class_id']);
                            $class = getClassData($event['class_id'], CLASS_INFO);
                            $teachers = getClassData($event['class_id'], CLASS_TEACHERS);

                            // Gửi thông báo cho giáo viên
                            $teacherIds = array_keys($teachers);
                            if(count($teacherIds) > 0) {
                                $userDao->postNotifications($teacherIds, NOTIFICATION_UPDATE_EVENT_CLASS, NOTIFICATION_NODE_TYPE_CLASS, $event['event_id'], convertText4Web($event['event_name']), $class['group_name']);
                            }

                            // Lấy danh sách trẻ trong lớp
                            $children = getClassData($event['class_id'], CLASS_CHILDREN);
                            if(count($children) > 0) {
                                foreach ($children as $child) {
                                    // Lấy danh sách phụ huynh của trẻ
                                    $parents = getChildData($child['child_id'], CHILD_PARENTS);
                                    $parentIds = array_keys($parents);

                                    if(count($parentIds) > 0) {
                                        $userDao->postNotifications($parentIds, NOTIFICATION_UPDATE_EVENT_CLASS, NOTIFICATION_NODE_TYPE_CHILD, $event['event_id'], convertText4Web($event['event_name']), $child['child_id']);
                                    }
                                }
                            }

                            // Thông báo cho các quản lý trường khác sự kiện được cập nhật
                            $userManagerIds = getUserIdsManagerReceiveNotify($school['page_id'], 'events', $school['page_admin']);
                            $userDao->postNotifications($userManagerIds, NOTIFICATION_UPDATE_EVENT_CLASS, NOTIFICATION_NODE_TYPE_SCHOOL,
                                $event['event_id'], convertText4Web($event['event_name']), $school['page_name'], convertText4Web($school['page_title']));

                            //$class = $classDao->getClass($args['class_id']);
                            $class = getClassData($event['class_id'], CLASS_INFO);
                            //3.Post thông tin sự kiên lên tường của lớp. Chỉ có thông báo luôn thì mới đưa lên tường
                            if ($event['post_on_wall']) {
                                $content = convertText4Web($event['event_name'] . "<br/><br/>" . $event['description']);
                                $postId = $userDao->postOnGroup($class, $content, $school['page_id']);
                                $eventDao->updatePostIds($postId, $event['event_id']);
                            }

                            break;
                        default:
                            _error(400);
                            break;
                    }

                    /* Coniu - Tương tác trường */
                    $eventIds = array();
                    $eventIds[] = $event['event_id'];
                    setIsAddNew($school['page_id'], 'event_created', $eventIds);
                    /* Coniu - END */
                }
            }
            // Cập nhật trạng thái đã gửi thông báo sự kiện.
            $eventDao->updateStatusToNotified($_POST['event_id']);

            $db->commit();
            // return
            return_json(array(
                'code' => 200,
                'message' => __("Done, Event have been notified"),
                'data' => array()
            ));
            break;

        case 'delete':
            if (!$canEdit) {
                _error(403);
            }
            if(!isset($_POST['event_id']) || !is_numeric($_POST['event_id'])) {
                _error(400);
            }
            $db->begin_transaction();

            $event = $eventDao->getEvent($_POST['event_id']);
            if (is_null($event)) {
                _error("Error", "The event does not exist, it should be deleted.");
            }

            //Xóa thông tin sự kiện. Trường hợp xóa không được sẽ báo lỗi và không chạy đoạn code dưới.
            $eventDao->deleteEvent($_POST['event_id'], true);
            //Xóa tất cả bài post liên quan đến sự kiện
            $userDao->deletePosts($event['post_ids']);

            $db->commit();
            // return
            return_json(array(
                'code' => 200,
                'message' => __("Done, Event info have been deleted"),
                'data' => null
            ));
            break;

        case 'reject':
            $db->begin_transaction();
            $event = $eventDao->getEvent($_POST['event_id']);
            if (is_null($event))
                _api_error(0, __("The event does not exist, it should be deleted."));

//            if ($event['begin'] < $date) {
//                $event['happened'] = 1; //sự kiện đã qua rồi hay chưa
//            } else {
//                $event['happened'] = 0;
//            }
            if ($event['level'] != CLASS_LEVEL
                || $class['school_id'] != $event['school_id'] || $class['group_id'] != $event['class_id'] && $event['is_notified'] == 1) {
                _api_error(0, __("You can not edit this event"));
            }

            $eventDao->deleteParticipants($_POST['event_id'], $_POST['type'], [$_POST['pp_id']]);

            //$classTeacherIds = $teacherDao->getTeacherIDOfClass($class['group_id']);
            $teachers = getClassData($class['group_id'], CLASS_TEACHERS);
            $classTeacherIds = array_keys($teachers);
            if ($_POST['type'] == PARTICIPANT_TYPE_PARENT) {
                //Thông báo cho phụ huynh
                $userDao->postNotifications($_POST['pp_id'], NOTIFICATION_CANCEL_REGISTRATION_EVENT_PARENT, NOTIFICATION_NODE_TYPE_CHILD, $_POST['event_id'], $_POST['event_name'], $_POST['child_id'], __("you"));
                //Thông báo cho giáo viên khác của lớp
                $userDao->postNotifications($classTeacherIds, NOTIFICATION_CANCEL_REGISTRATION_EVENT_PARENT, NOTIFICATION_NODE_TYPE_CLASS, $_POST['event_id'], $_POST['event_name'], $_POST['group_name'], $_POST['name']);
            } else if ($_POST['type'] == PARTICIPANT_TYPE_CHILD) {
                $parents = $childDao->getListChildNParentId([$_POST['pp_id']]);
                foreach ($parents as $parent) {
                    $userDao->postNotifications($parent['parent_id'], NOTIFICATION_CANCEL_REGISTRATION_EVENT_CHILD, NOTIFICATION_NODE_TYPE_CHILD, $_POST['event_id'], $_POST['event_name'], $parent['child_id'], $_POST['name']);
                }
                //Thông báo tới giáo viên của lớp.
                $userDao->postNotifications($classTeacherIds, NOTIFICATION_CANCEL_REGISTRATION_EVENT_CHILD, NOTIFICATION_NODE_TYPE_CLASS, $_POST['event_id'], $_POST['event_name'], $_POST['group_name'], $_POST['name']);
            }

            $db->commit();
            // return
            return_json(array(
                'code' => 200,
                'message' => __("Event have been reject"),
                'data' => array()
            ));
            break;

        default:
            _api_error(400);
            break;
    }

} catch (Exception $e) {
    $db->rollback();
    header('HTTP/1.1 400 API Error');
    return_json(array(
        'code' => 0,
        'message' => $e->getMessage(),
        'data' => array()
    ));
} finally {
    $db->autocommit(true);
}

?>