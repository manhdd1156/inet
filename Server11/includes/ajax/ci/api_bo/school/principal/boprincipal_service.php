<?php
/**
 * Package: ajax/ci/api_bo/school
 *
 * @package ConIu
 * @author TaiLA
 */

if(is_empty($_POST['page_name']) || !valid_username($_POST['page_name'])) {
    _error(404);
}

include_once(DAO_PATH.'dao_child.php');
include_once(DAO_PATH.'dao_parent.php');
include_once(DAO_PATH.'dao_user.php');
include_once(DAO_PATH.'dao_teacher.php');
include_once(DAO_PATH.'dao_service.php');

$childDao = new ChildDAO();
$parentDao = new ParentDAO();
$userDao = new UserDAO();
$teacherDao = new TeacherDAO();
$serviceDao = new ServiceDAO();

$school = getSchoolDataByUsername($_POST['page_name'], SCHOOL_CONFIG);
if (is_null($school)) {
    _api_error(403);
}
// Check xem có phải hiệu trưởng trường không
$principals = array();
if (!is_empty($school['principal'])) {
    $principals = explode(',', $school['principal']);
}
if(!in_array($user->_data['user_id'], $principals)) {
    _api_error(403);
}
try {
    $return = array();
    switch ($_POST['do']) {
        case 'service_based_month':
            // Tổng hợp dịch vụ theo tháng
            $datePost = isset($_POST['month'])? $_POST['month'] : date('m/Y');
            $begin = toDBDate("01/" . $datePost);
            $end = addMonthToDBDate($begin);
            $new_end = strtotime ('-1 day' , strtotime($end));
            $new_end = date ('Y-m-d', $new_end);

            $begin = toSysDate($begin);
            $end = toSysDate($new_end);

            // Lấy ra tất cả dịch vụ của toàn trường có người sử dụng
            $services = $serviceDao->getServiceUsageSummarySchoolForPrincipal($school['school_id'], $begin, $end);

            $serviceIds = array();
            foreach ($services as $service) {
                $serviceIds[] = $service['service_id'];
            }
            // Lấy tất cả dịch vụ của toàn trường
            $serviceAll = $serviceDao->getAllSchoolServicesForAPI($school['school_id']);
            $servicesNoUse = array();
            foreach ($serviceAll as $service) {
                $serviceNoUse = array();
                if(!in_array($service['service_id'], $serviceIds)) {
                    $serviceNoUse['service_id'] = $service['service_id'];
                    $serviceNoUse['type'] = $service['type'];
                    $serviceNoUse['service_name'] = $service['service_name'];
                    $serviceNoUse['fee'] = $service['fee'];
                    $serviceNoUse['is_food'] = $service['is_food'];
                    $serviceNoUse['cnt'] = 0;
                    $serviceNoUse['money'] = 0;
                }
                if(count($serviceNoUse) > 0) {
                    $servicesNoUse[] = $serviceNoUse;
                }
            }

            // Gộp hai mảng service với nhau
            $servicesAll = array_merge($services, $servicesNoUse);
            $servicesAll = sortMultiOrderArray($servicesAll, [["is_food", "DESC"], ["type", "DESC"]]);

            // Tổng tiền sử dụng dịch vụ trong tháng
            $allService = array();
            $totalMonth = 0;
            $fields = array('service_id', 'service_name', 'totalUsageService', 'money');
            foreach ($servicesAll as $row) {
                $totalMonth = $totalMonth + $row['money'];
                $row['totalUsageService'] = $row['cnt'];
                $row = getArrayFromKeys($row, $fields);
                $allService[] = $row;
            }
            // return
            return_json(array(
                'code' => 200,
                'message' => __("OK"),
                'data' =>array(
                    'services' => $allService,
                    'totalFeeInMonth' => $totalMonth
                )
            ));
            break;

        case 'service_based_year':
            // THống kê tổng tiền dịch vụ sử dụng dịch vụ của trường theo từng tháng
            $transdate = date('Y-m-d');
            $month = date('m', strtotime($transdate));
            $year = isset($_POST['year']) ? $_POST['year'] : date('Y');
            $totalYear = array();
            for ($i = 1; $i <= $month; $i++) {
                $begin = toDBDate("01/" . $i . "/" . $year);
                $end = addMonthToDBDate($begin);
                $new_end = strtotime ('-1 day' , strtotime($end));
                $new_end = date ('Y-m-d', $new_end);

                $begin = toSysDate($begin);
                $end = toSysDate($new_end);
                // Lấy ra tất cả dịch vụ của toàn trường có người sử dụng trong tháng
                $services = $serviceDao->getServiceUsageSummarySchoolForPrincipal($school['school_id'], $begin, $end);

                $serviceIds = array();
                foreach ($services as $service) {
                    $serviceIds[] = $service['service_id'];
                }
                // Lấy tất cả dịch vụ của toàn trường
                $serviceAll = $serviceDao->getAllSchoolServicesForAPI($school['school_id']);
                $servicesNoUse = array();
                foreach ($serviceAll as $service) {
                    $serviceNoUse = array();
                    if(!in_array($service['service_id'], $serviceIds)) {
                        $serviceNoUse['service_id'] = $service['service_id'];
                        $serviceNoUse['type'] = $service['type'];
                        $serviceNoUse['service_name'] = $service['service_name'];
                        $serviceNoUse['fee'] = $service['fee'];
                        $serviceNoUse['is_food'] = $service['is_food'];
                        $serviceNoUse['cnt'] = 0;
                        $serviceNoUse['money'] = 0;
                    }
                    if(count($serviceNoUse) > 0) {
                        $servicesNoUse[] = $serviceNoUse;
                    }
                }

                // Gộp hai mảng service với nhau
                $servicesAll = array_merge($services, $servicesNoUse);
                $servicesAll = sortMultiOrderArray($servicesAll, [["is_food", "DESC"], ["type", "DESC"]]);
                $totalMonth = 0;
                foreach ($servicesAll as $row) {
                    $totalMonth = $totalMonth + $row['money'];
                }
                switch ($i) {
                    case '1':
                        $totalYear['jan'] = $totalMonth;
                        break;
                    case '2':
                        $totalYear['feb'] = $totalMonth;
                        break;
                    case '3':
                        $totalYear['mar'] = $totalMonth;
                        break;
                    case '4':
                        $totalYear['apr'] = $totalMonth;
                        break;
                    case '5':
                        $totalYear['may'] = $totalMonth;
                        break;
                    case '6':
                        $totalYear['jun'] = $totalMonth;
                        break;
                    case '7':
                        $totalYear['jul'] = $totalMonth;
                        break;
                    case '8':
                        $totalYear['aug'] = $totalMonth;
                        break;
                    case '9':
                        $totalYear['sep'] = $totalMonth;
                        break;
                    case '10':
                        $totalYear['oct'] = $totalMonth;
                        break;
                    case '11':
                        $totalYear['nov'] = $totalMonth;
                        break;
                    case '12':
                        $totalYear['dec'] = $totalMonth;
                        break;
                    default:
                        _api_error(400);
                        break;
                }
            }

            // return
            return_json(array(
                'code' => 200,
                'message' => __("OK"),
                'data' =>array(
                    'totalYear' => $totalYear
                )
            ));
            break;

        default:
            _api_error(400);
            break;
    }

} catch (Exception $e) {
    //$db->rollback();
    // return
    _api_error(0, $e->getMessage());
} finally {
    //$db->autocommit(true);
}
?>