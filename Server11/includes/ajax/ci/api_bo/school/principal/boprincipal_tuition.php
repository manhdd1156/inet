<?php
/**
 * Package: ajax/ci/api_bo/school
 *
 * @package ConIu
 * @author TaiLA
 */
if(is_empty($_POST['page_name']) || !valid_username($_POST['page_name'])) {
    _api_error(404);
}

include_once(DAO_PATH . 'dao_tuition.php');
include_once(DAO_PATH . 'dao_school.php');
include_once(DAO_PATH . 'dao_class.php');
include_once(DAO_PATH . 'dao_child.php');

$tuitionDao = new TuitionDAO();
$schoolDao = new SchoolDAO();
$classDao = new ClassDAO();
$childDao = new ChildDAO();

//$school = getSchool($_POST['page_name']);
$school = getSchoolDataByUsername($_POST['page_name'], SCHOOL_CONFIG);
if (is_null($school)) {
    _api_error(403);
}
// Check xem có phải hiệu trưởng trường không
$principals = array();
if (!is_empty($school['principal'])) {
    $principals = explode(',', $school['principal']);
}
if(!in_array($user->_data['user_id'], $principals)) {
    _api_error(403);
}

try {
    $return = array();
    switch ($_POST['do']) {
        case 'month_summary':
            $month = isset($_POST['month']) ? $_POST['month'] : date("m/Y");
            // Lấy ra danh sách học phí các tháng của trường
            $tuitions = $tuitionDao->getAllTuitionOfMonthForAPI($school['school_id'], $month);

            // Tổng học phí và đã thanh toán của tháng
            $totalTuitionInMonth = 0;
            $totalPaidTuitionInMonth = 0;

            foreach ($tuitions as $tuition) {
                $totalTuitionInMonth = $totalTuitionInMonth + $tuition['total_amount'];
                $totalPaidTuitionInMonth = $totalPaidTuitionInMonth + $tuition['paid_amount'];
            }
            // return
            return_json(array(
                'code' => 200,
                'message' => __("OK"),
                'data' =>array(
                    'tuitions' => $tuitions,
                    'totalTuitionInMonth' => $totalTuitionInMonth,
                    'totalPaidTuitionInMonth' => $totalPaidTuitionInMonth
                )
            ));
            break;
        default:
            _api_error(400);
            break;
    }

} catch (Exception $e) {
    //$db->rollback();
    // return
    header('HTTP/1.1 400 API Error');
    return_json(array(
        'code' => 0,
        'message' => $e->getMessage(),
        'data' => array()
    ));
} finally {
    //$db->autocommit(true);
}
?>