<?php
/**
 * Package: ajax/ci/api_bo/school
 *
 * @package ConIu
 * @author TaiLA
 */

if(is_empty($_POST['page_name']) || !valid_username($_POST['page_name'])) {
    _api_error(404);
}

include_once(DAO_PATH . 'dao_class_level.php');
include_once(DAO_PATH . 'dao_class.php');
include_once(DAO_PATH . 'dao_user.php');
include_once(DAO_PATH . 'dao_teacher.php');
include_once(DAO_PATH . 'dao_child.php');

$classDao = new ClassDAO();
$classLevelDao = new ClassLevelDAO();
$userDao = new UserDAO();
$teacherDao = new TeacherDAO();
$childDao = new ChildDAO();

//$school = getSchool($_POST['page_name']);
$school = getSchoolDataByUsername($_POST['page_name'], SCHOOL_CONFIG);
if (is_null($school)) {
    _api_error(403);
}
// Check xem có phải hiệu trưởng trường không
$principals = array();
if (!is_empty($school['principal'])) {
    $principals = explode(',', $school['principal']);
}
if(!in_array($user->_data['user_id'], $principals)) {
    _api_error(403);
}

try {
    $return = array();
    switch ($_POST['do']) {
        case 'children_based_class_level':
            $schoolLevel = array();
            $totalChildInSchool = 0;
            // Lấy danh sách khối của trường
            $class_levels = $classLevelDao->getClassLevelsForAPI($school['school_id'], true);
            $temps = array();
            foreach ($class_levels as $class_level) {
                // Lấy danh sách lớp của từng khối
                $classesId = $classDao->getClassIdOfLevel($class_level['class_level_id']);

                // Lấy số lượng trẻ của từng lớp
                $child_class_level_ids = array();
                foreach ($classesId as $classId) {
                    //$childIds = $childDao->getChildIdOfClass($classId);
                    $children = getClassData($classId, CLASS_CHILDREN);
                    $childIds = array_keys($children);
                    $child_class_level_ids = array_merge($child_class_level_ids, $childIds);
                }

                $child_class_level_ids = array_unique($child_class_level_ids);
                $class_level['total_child'] = count($child_class_level_ids);
                $totalChildInSchool = $totalChildInSchool + $class_level['total_child'];
                $temps[] = $class_level;
            }
            $schoolLevel['totalChildInSchool'] = $totalChildInSchool;
            $schoolLevel['class_levels'] = $temps;
            // return
            return_json(array(
                'code' => 200,
                'message' => __("OK"),
                'data' =>array(
                    'school' => $schoolLevel
                )
            ));
            break;
        case 'children_based_year':
            // Lấy thông tin trẻ đi học theo năm
            $transdate = date('Y-m-d');
            $month = date('m', strtotime($transdate));

            $year = isset($_POST['year']) ? $_POST['year'] : date('Y');
            $childrenBasedYear = array();
            for ($i = 1; $i <= $month; $i++) {
                $begin = toDBDate("01/" . $i . "/" . $year);
                $end = addMonthToDBDate($begin);
                $new_end = strtotime('-1 day', strtotime($end));
                $new_end = date('Y-m-d', $new_end);

                $begin = toSysDate($begin);
                $end = toSysDate($new_end);

                $countChild = 0;
                // Lấy ra số trẻ của toàn trường trong tháng
                $countChild = $childDao->getCountChildOfSchoolBasedMonth($school['school_id'], $begin, $end);
                switch ($i) {
                    case '1':
                        $childrenBasedYear['jan'] = $countChild;
                        break;
                    case '2':
                        $childrenBasedYear['feb'] = $countChild;
                        break;
                    case '3':
                        $childrenBasedYear['mar'] = $countChild;
                        break;
                    case '4':
                        $childrenBasedYear['apr'] = $countChild;
                        break;
                    case '5':
                        $childrenBasedYear['may'] = $countChild;
                        break;
                    case '6':
                        $childrenBasedYear['jun'] = $countChild;
                        break;
                    case '7':
                        $childrenBasedYear['jul'] = $countChild;
                        break;
                    case '8':
                        $childrenBasedYear['aug'] = $countChild;
                        break;
                    case '9':
                        $childrenBasedYear['sep'] = $countChild;
                        break;
                    case '10':
                        $childrenBasedYear['oct'] = $countChild;
                        break;
                    case '11':
                        $childrenBasedYear['nov'] = $countChild;
                        break;
                    case '12':
                        $childrenBasedYear['dec'] = $countChild;
                        break;
                    default:
                        _api_error(400);
                        break;
                }
            }

            return_json(array(
                'code' => 200,
                'message' => __("OK"),
                'data' =>array(
                    'childrenBasedYear' => $childrenBasedYear
                )
            ));
            break;
        default:
            _api_error(400);
            break;
    }

} catch (Exception $e) {
    //$db->rollback();
    // return
    header('HTTP/1.1 400 API Error');
    return_json(array(
        'code' => 0,
        'message' => $e->getMessage(),
        'data' => array()
    ));
} finally {
    //$db->autocommit(true);
}
?>