<?php
/**
 * Package: ajax/ci/api_bo/school
 *
 * @package ConIu
 * @author QuanND
 */

if(is_empty($_POST['page_name']) || !valid_username($_POST['page_name'])) {
    _error(404);
}

include_once(DAO_PATH . 'dao_school.php');
include_once(DAO_PATH . 'dao_event.php');
include_once(DAO_PATH . 'dao_parent.php');
include_once(DAO_PATH . 'dao_teacher.php');
include_once(DAO_PATH . 'dao_user.php');
include_once(DAO_PATH . 'dao_class.php');
include_once(DAO_PATH . 'dao_attendance.php');
include_once(DAO_PATH . 'dao_child.php');

$schoolDao = new SchoolDAO();
$eventDao = new EventDAO();
$parentDao = new ParentDAO();
$teacherDao = new TeacherDAO();
$userDao = new UserDAO();
$classDao = new ClassDAO();
$attendanceDao = new AttendanceDAO();
$childDao = new ChildDAO();

//$school = getSchool($_POST['page_name']);
$school = getSchoolDataByUsername($_POST['page_name'], SCHOOL_INFO);
if (is_null($school)) {
    _error(403);
}

try {
    $return = array();
    // Tăng lượt tương tác - TaiLA
    addInteractive($school['page_id'], 'attendance', 'school_view');

    switch ($_POST['do']) {
        case 'aday':
            // Tăng số đếm tương tác của trường
            increaseSchoolInteractive($school['page_id'], 'attendance');

            //INPUT: do=aday, page_name, attendanceDate
            if (!canView($_POST['page_name'], 'attendance')) {
                _api_error(403);
            }
            $today = date($system['date_format']);
            $date = isset($_POST['attendanceDate']) ? $_POST['attendanceDate'] : $today;

            //Lấy ra danh sách điểm danh cả trường (theo từng lớp, sắp xếp theo khối).
            $attendances = $attendanceDao->getAttendanceOfSchoolADay($school['page_id'], $_POST['attendanceDate']);

            $results = array();
            $idx = 1;
            $classLevelId = -1;
            $classLevel = null;
            $classes = null;
            foreach ($attendances as $attendance) {
                if ($idx == 1) {
                    $classLevel = array(); //Tạo một khối mới.
                    $classLevel['class_level_name'] = $attendance['class_level_name'];
                    $classes = array();
                } else if (($idx > 1) && ($classLevelId != $attendance['class_level_id'])) {
                    $classLevel['classes'] = $classes; //Gắn danh  sách lớp vào khối trước đó.
                    $results[] = $classLevel;

                    $classLevel = array(); //Tạo một khối mới.
                    $classLevel['class_level_name'] = $attendance['class_level_name'];
                    $classes = array();
                }

                $class = array();
                $class['group_title'] = $attendance['group_title'];
                $class['is_checked'] = $attendance['is_checked'];
                $class['total'] = $attendance['total'];
                $class['present_count'] = $attendance['present_count'];
                $class['absence_count'] = $attendance['absence_count'];
                $class['recorded_at'] = $attendance['recorded_at'];

                $classes[] = $class;
                $idx++;
                $classLevelId = $attendance['class_level_id'];
            }
            if ($idx > 1) {
                $classLevel['classes'] = $classes;
                $results[] = $classLevel;
            }

            // return
            return_json(array(
                'code' => 200,
                'message' => __("OK"),
                'data' =>array('class_levels' => $results)
            ));
            break;
        case 'conabs':
            //INPUT: do=conabs, page_name, daynum
            if (!canView($_POST['page_name'], 'attendance')) {
                _api_error(403);
            }
            $daynum = $_POST['daynum'];
            $children = $attendanceDao->getConsecutiveAbsentChild($school['page_id'], $daynum);
            $tmpArray = array();
            while ((count($children) > 0) && (MAX_CONSECUTIVE_ABSENT_DAY >= $daynum)) {
                $tmpArray[$daynum] = $children;
                $daynum++;
                if (MAX_CONSECUTIVE_ABSENT_DAY >= $daynum) {
                    $children = $attendanceDao->getConsecutiveAbsentChild($school['page_id'], $daynum);
                }
            }
            $newArrays = array();
            while ($daynum > $_POST['daynum']) {
                $daynum--;
                foreach ($tmpArray[$daynum] as $childOut) {
                    $found = false;
                    foreach ($newArrays as $childIn) {
                        if ($childOut['child_id'] == $childIn['child_id']) {
                            $found = true;
                            break;
                        }
                    }
                    if (!$found) {
                        $newChild = $childOut;
                        $newChild['number_of_absent_day'] = $daynum;
                        $newArrays[] = $newChild;
                    }
                }
            }

            return_json(array(
                'code' => 200,
                'message' => __("OK"),
                'data' =>array('children' => $newArrays)
            ));
            break;
        default:
            _api_error(400);
            break;
    }

} catch (Exception $e) {
    //$db->rollback();
    // return
    header('HTTP/1.1 400 API Error');
    return_json(array(
        'code' => 0,
        'message' => $e->getMessage(),
        'data' => array()
    ));
} finally {
    //$db->autocommit(true);
}
?>