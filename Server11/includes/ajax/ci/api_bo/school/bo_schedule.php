<?php
/**
 * Package: ajax/ci/api_bo/school
 *
 * @package ConIu
 * @author TaiLA
 */

if(is_empty($_POST['page_name']) || !valid_username($_POST['page_name'])) {
    _error(404);
}

include_once(DAO_PATH . 'dao_schedule.php');
include_once(DAO_PATH . 'dao_class.php');
include_once(DAO_PATH . 'dao_class_level.php');

$classDao = new ClassDAO();
$classLevelDao = new ClassLevelDAO();
$scheduleDao = new ScheduleDAO();

//$school = getSchool($_POST['page_name']);
$school = getSchoolDataByUsername($_POST['page_name'], SCHOOL_INFO);
if (is_null($school)) {
    _error(403);
}
// Lấy danh sách khối lớp của trường
//$class_levels = $classLevelDao->getClassLevels($school['page_id']);
$class_levels = getSchoolData($school['page_id'], SCHOOL_CLASS_LEVELS);
// Lấy danh sách lớp của trường
//$classes = $classDao->getClassesOfSchool($school['page_id']);
$classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);

try {
    $return = array();
    switch ($_POST['do']) {
        case 'list_schedule':
            // Tăng số đếm tương tác của trường
            increaseSchoolInteractive($school['page_id'], 'schedule');
            // Tăng lượt tương tác - TaiLA
            addInteractive($school['page_id'], 'schedule', 'school_view');

            if (!canView($_POST['page_name'], 'schedules')) {
                _api_error(403);
            }

            $page = isset($_POST['page']) ? $_POST['page'] : 0;
            // Lấy ra tất cả thực đơn của toàn trường
            $schedules = $scheduleDao->getScheduleOfSchoolForAPI($school['page_id'], $page);

            $schedules_school = array();
            foreach ($schedules as $schedule) {
                if($schedule['applied_for'] == CLASS_LEVEL_LEVEL) {
                    foreach ($class_levels as $class_level) {
                        if($class_level['class_level_id'] == $schedule['class_level_id']) {
                            $schedule['class_level_name'] = $class_level['class_level_name'];
                            break;
                        }
                    }
                } elseif ($schedule['applied_for'] == CLASS_LEVEL) {
                    foreach ($classes as $class) {
                        if ($class['class_id'] == $schedule['class_id']) {
                            $schedule['class_name'] = $class['group_name'];
                            break;
                        }
                    }
                }
                $schedules_school[] = $schedule;
            }
            // return
            return_json(array(
                'code' => 200,
                'message' => __("OK"),
                'data' =>array('schedules' => $schedules_school)
            ));
            break;
        case 'detail':
            if (!canView($_POST['page_name'], 'schedules')) {
                _api_error(403);
            }
            if(!isset($_POST['schedule_id']) || !is_numeric($_POST['schedule_id'])) {
                _error(404);
            }
            $data = $scheduleDao->getScheduleDetailById($_POST['schedule_id']);

            if($data['applied_for'] == CLASS_LEVEL_LEVEL) {
                foreach ($class_levels as $class_level) {
                    if($class_level['class_level_id'] == $data['class_level_id']) {
                        $data['class_level_name'] = $class_level['class_level_name'];
                        break;
                    }
                }
            } elseif ($data['applied_for'] == CLASS_LEVEL) {
                foreach ($classes as $class) {
                    if ($class['class_id'] == $data['class_id']) {
                        $data['class_name'] = $class['group_name'];
                        break;
                    }
                }
            }

            // Tăng lượt tương tác - TaiLA
            addInteractive($school['page_id'], 'schedule', 'school_view', $data['schedule_id']);
            // return
            return_json(array(
                'code' => 200,
                'message' => __("OK"),
                'data' =>array('schedule_detail' => $data)
            ));
            break;
        default:
            _api_error(400);
            break;
    }

} catch (Exception $e) {
    //$db->rollback();
    // return
    header('HTTP/1.1 400 API Error');
    return_json(array(
        'code' => 0,
        'message' => $e->getMessage(),
        'data' => array()
    ));
} finally {
    //$db->autocommit(true);
}
?>