<?php
/**
 * Package: ajax/ci/api_bo/school
 *
 * @package ConIu
 * @author TaiLA
 */

if(is_empty($_POST['page_name']) || !valid_username($_POST['page_name'])) {
    _error(404);
}

include_once(DAO_PATH.'dao_school.php');
include_once(DAO_PATH.'dao_review.php');

$schoolDao = new SchoolDAO();
$reviewDao = new ReviewDAO();

$school = getSchoolDataByUsername($_POST['page_name'], SCHOOL_INFO);
if (is_null($school)) {
    _api_error(403);
}

try {
    $return = array();

    switch ($_POST['view']) {
        case 'info':
            //Lấy ra danh sách đánh giá trường
            $info = $reviewDao->getReviewInfo($school['page_id']);
            // return
            return_json(array(
                'code' => 200,
                'message' => 'OK',
                'data' => array(
                    'review_info' => $info
                )
            ));
            break;

        case 'school':
            $page = (isset($_POST['page'])) ? $_POST['page'] : 0;

            //Lấy ra danh sách đánh giá trường
            $reviews = $reviewDao->getSchoolReview($school['page_id'], $page);
            // return
            return_json(array(
                'code' => 200,
                'message' => 'OK',
                'data' => array(
                    'school_reviews' => $reviews
                )
            ));
            break;

        case 'teachers':
            //Lấy ra danh sách đánh giá giáo viên toàn trường
            $reviews = $reviewDao->getTeacherReviewInSchool($school['page_id']);
            // return
            return_json(array(
                'code' => 200,
                'message' => 'OK',
                'data' => array(
                    'teachers_reviews' => $reviews
                )
            ));
            break;
        case 'teacher':
            $page = (isset($_POST['page'])) ? $_POST['page'] : 0;

            if(!isset($_POST['class_id']) || !is_numeric($_POST['class_id'])) {
                _error(404);
            }
            $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
            $classIds = array_keys($classes);
            if (!in_array($_POST['class_id'], $classIds)) {
                _error(404);
            }

            if(!isset($_POST['teacher_id']) || !is_numeric($_POST['teacher_id'])) {
                _error(404);
            }
            $result = array();

            $class = getClassData($_POST['class_id'], CLASS_INFO);
            if(is_null($class)) {
                _error(404);
            }
            $result['class'] = $class;

            $teacher = getTeacherData($_POST['teacher_id'], TEACHER_INFO);
            if(is_null($class)) {
                _error(404);
            }
            $result['teacher'] = $teacher;

            //Lấy ra danh sách đánh giá trường
            $result['detail'] = $reviewDao->getTeacherReviewDetail($_POST['teacher_id'], $_POST['class_id']);

            // return
            return_json(array(
                'code' => 200,
                'message' => __("OK"),
                'data' =>array(
                    'teacher_reviews_detail' => $result
                )
            ));
            break;

        default:
            _api_error(400);
            break;
    }

} catch (Exception $e) {
    //$db->rollback();
    // return
    _api_error(0, $e->getMessage());
} finally {
    //$db->autocommit(true);
}
?>