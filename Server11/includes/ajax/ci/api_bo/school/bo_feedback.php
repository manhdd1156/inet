<?php
/**
 * Package: ajax/ci/api_bo/school
 *
 * @package ConIu
 * @author TaiLA
 */

if(is_empty($_POST['page_name']) || !valid_username($_POST['page_name'])) {
    _api_error(404);
}

include_once(DAO_PATH . 'dao_feedback.php');
include_once(DAO_PATH . 'dao_user.php');

$feedbackDao = new FeedbackDAO();
$userDao = new UserDAO();

$school = getSchoolDataByUsername($_POST['page_name'], SCHOOL_INFO);
if (is_null($school)) {
    _api_error(403);
}

try {
    $return = array();
    $db->autocommit(false);
    // Tăng lượt tương tác - TaiLA
    addInteractive($school['page_id'], 'feedback', 'school_view');
    switch ($_POST['do']) {
        case 'lists':
            // Tăng số đếm tương tác của trường
            increaseSchoolInteractive($school['page_id'], 'feedback');

            if (!canView($_POST['page_name'], 'feedback')) {
                _api_error(403);
            }
            // Lấy ra danh sách feedback gửi cho trường
            $page = isset($_POST['page']) ? $_POST['page']:0;
            $feedbacks = $feedbackDao->getFeedbackOfSchoolForAPI($school['page_id'], $page);
            // return
            return_json(array(
                'code' => 200,
                'message' => __("OK"),
                'data' =>array('feedbacks' => $feedbacks)
            ));
            break;
        case 'delete':
            if (!canEdit($_POST['page_name'], 'feedback')) {
                _api_error(403);
            }
            if(is_empty($_POST['feedback_id']) || !is_numeric($_POST['feedback_id'])) {
                _api_error(404);
            }
            $db->begin_transaction();
            $feedback = $feedbackDao->getFeedback($_POST['feedback_id']);
            if(is_null($feedback)) {
                throw new Exception("The feedback has been deleted or does not exist");
            }
            // Xóa feedback
            $feedbackDao->deleteFeedback($_POST['feedback_id']);
            $db->commit();
            // return
            return_json(array(
                'code' => 200,
                'message' => __("Delete feedback for success"),
                'data' => null
            ));
            break;
        case 'delete_all':
            if (!canEdit($_POST['page_name'], 'feedback')) {
                _api_error(403);
            }
            // Xóa tất cả feedback
            $db->begin_transaction();
            $feedbackDao->deleteAllFeedback($school['page_id']);
            $db->commit();
            // return
            return_json(array(
                'code' => 200,
                'message' => __("Delete feedback for success"),
                'data' => null
            ));
            break;
        case 'confirm':
            if (!canEdit($_POST['page_name'], 'feedback')) {
                _api_error(403);
            }
            if(is_empty($_POST['feedback_id']) || !is_numeric($_POST['feedback_id'])) {
                _api_error(404);
            }
            $db->begin_transaction();
            $feedback = $feedbackDao->getFeedback($_POST['feedback_id']);
            if(is_null($feedback)) {
                throw new Exception("The feedback has been deleted or does not exist");
            }
            // Xóa feedback
            $feedbackDao->updateStatusToConfirmed($_POST['feedback_id']);
            $uId[] = $feedback['created_user_id'];
            $userDao->postNotifications($uId, NOTIFICATION_CONFIRM_FEEDBACK, NOTIFICATION_NODE_TYPE_CHILD, $_POST['feedback_id']);
            $db->commit();
            // return
            return_json(array(
                'code' => 200,
                'message' => __("Success"),
                'data' => null
            ));
            break;
        default:
            _api_error(400);
            break;
    }

} catch (Exception $e) {
    //$db->rollback();
    // return
    _api_error(0, $e->getMessage());
} finally {
    //$db->autocommit(true);
}
?>