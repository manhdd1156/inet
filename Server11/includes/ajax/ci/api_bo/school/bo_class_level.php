<?php
/**
 * Package: ajax/ci/api_bo/school
 *
 * @package ConIu
 * @author TaiLA
 */

if(is_empty($_POST['page_name']) || !valid_username($_POST['page_name'])) {
    _api_error(404);
}

include_once(DAO_PATH . 'dao_class_level.php');
include_once(DAO_PATH . 'dao_class.php');
include_once(DAO_PATH . 'dao_user.php');
include_once(DAO_PATH . 'dao_teacher.php');
include_once(DAO_PATH . 'dao_child.php');

$classDao = new ClassDAO();
$classLevelDao = new ClassLevelDAO();
$userDao = new UserDAO();
$teacherDao = new TeacherDAO();
$childDao = new ChildDAO();

//$school = getSchool($_POST['page_name']);
$school = getSchoolDataByUsername($_POST['page_name'], SCHOOL_INFO);
if (is_null($school)) {
    _error(403);
}

try {
    $return = array();
    switch ($_POST['do']) {
        case 'list_class_level':
            if (!canView($_POST['page_name'], 'classlevels')) {
                _api_error(403);
            }
            $schoolLevel = array();
            // Lấy số lượng lớp của trường
            $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);

            $schoolLevel['total_class'] = count($classes);
            // Lấy số lượng giáo viên của trường
            $teachers = getSchoolData($school['page_id'], SCHOOL_TEACHERS);
            $schoolLevel['total_teacher'] = count($teachers);
            // Lấy ra tổng trẻ của trường
            $children = getSchoolData($school['page_id'], SCHOOL_CHILDREN);
            $schoolLevel['total_child'] = count($children);

            // Lấy danh sách khối của trường
            $class_levels = $classLevelDao->getClassLevelsForAPI($school['page_id'], true);
            $temps = array();
            foreach ($class_levels as $class_level) {
                // Lấy danh sách lớp của từng khối
                $classesId = $classDao->getClassIdOfLevel($class_level['class_level_id']);

                // Lấy số lượng giáo viên của từng lớp
                $teacher_class_level_ids = array();
                // Lấy số lượng trẻ của từng lớp
                $child_class_level_ids = array();
                $classList = array();
                foreach ($classesId as $classId) {
                    $classRe = array();

                    $class_info = getClassData($classId, CLASS_INFO);
                    //$teacherIds = $teacherDao->getTeacherIdOfClass($classId);
                    $teachers = getClassData($classId, CLASS_TEACHERS);
                    $teacherIds = array_keys($teachers);
                    $teacher_class_level_ids = array_merge($teacher_class_level_ids, $teacherIds);
                    //$childIds = $childDao->getChildIdOfClass($classId);
                    $children = getClassData($classId, CLASS_CHILDREN);
                    $childIds = array_keys($children);
                    $child_class_level_ids = array_merge($child_class_level_ids, $childIds);
                    $classRe['class_id'] = $classId;
                    $classRe['class_name'] = $class_info['group_name'];
                    $classRe['class_title'] = $class_info['group_title'];
                    $classRe['count_child'] = count($children);
                    $classList[] = $classRe;
                }
                $teacher_class_level_ids = array_unique($teacher_class_level_ids);
                $child_class_level_ids = array_unique($child_class_level_ids);
                $class_level['total_child'] = count($child_class_level_ids);
                $class_level['total_teacher'] = count($teacher_class_level_ids);
                $class_level['classes'] = $classList;
                $temps[] = $class_level;
            }
            $schoolLevel['class_levels'] = $temps;
            // return
            return_json(array(
                'code' => 200,
                'message' => __("OK"),
                'data' =>array('school' => $schoolLevel)
            ));
            break;
        case 'detail_class_level':
            // Cái này không dùng đến nữa
            if (!canView($_POST['page_name'], 'classlevels')) {
                _api_error(403);
            }
            if(is_empty($_POST['class_level_id']) || !is_numeric($_POST['class_level_id'])) {
                _api_error(404);
            }
//            $classes = $classDao->getClassOfLevel($_POST['class_level_id']);
            $classes = getClassLevelData($_POST['class_level_id'], CLASS_LEVEL_CLASSES);
            $classes_of_level = array();
            foreach ($classes as $class) {
                // Lấy số lượng trẻ của từng lớp
                $count_child = getClassData($class['group_id'], CLASS_CHILDREN);
                $class['total_child'] = count($count_child);

                // Lấy danh sách giáo viên của từng lớp
                //$teachers = $teacherDao->getTeacherOfClass($class['group_id']);
                $teachers = array();
                $class_teachers = getClassData($class['group_id'], CLASS_TEACHERS);
                $class_teachers = sortArray($class_teachers, "ASC", "user_fullname");
                foreach ($class_teachers as $teacher) {
                    $teachers[] = $teacher;
                }
                $class['teachers'] = $teachers;
                $classes_of_level[] = $class;
            }

            return_json(array(
                'code' => 200,
                'message' => __("OK"),
                'data' =>array('classes' => $classes_of_level)
            ));
            break;
        case 'list_child_class':
            if (!canView($_POST['page_name'], 'classes')) {
                _api_error(403);
            }
            if(is_empty($_POST['class_id']) || !is_numeric($_POST['class_id'])) {
                _api_error(404);
            }

            $children = getClassData($_POST['class_id'], CLASS_CHILDREN);
            $children = array_values($children);
            return_json(array(
                'code' => 200,
                'message' => __("OK"),
                'data' =>array('children' => $children)
            ));
            break;
        default:
            _api_error(400);
            break;
    }

} catch (Exception $e) {
    //$db->rollback();
    // return
    header('HTTP/1.1 400 API Error');
    return_json(array(
        'code' => 0,
        'message' => $e->getMessage(),
        'data' => array()
    ));
} finally {
    //$db->autocommit(true);
}
?>