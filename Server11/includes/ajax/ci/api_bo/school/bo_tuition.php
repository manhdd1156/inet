<?php
/**
 * Package: ajax/ci/api_bo/school
 *
 * @package ConIu
 * @author TaiLA
 */

if(is_empty($_POST['page_name']) || !valid_username($_POST['page_name'])) {
    _api_error(404);
}

include_once(DAO_PATH . 'dao_tuition.php');
include_once(DAO_PATH . 'dao_school.php');
include_once(DAO_PATH . 'dao_class.php');
include_once(DAO_PATH . 'dao_child.php');

$tuitionDao = new TuitionDAO();
$schoolDao = new SchoolDAO();
$classDao = new ClassDAO();
$childDao = new ChildDAO();

//$school = getSchool($_POST['page_name']);
$school = getSchoolDataByUsername($_POST['page_name'], SCHOOL_INFO);
if (is_null($school)) {
    _error(403);
}

try {
    $return = array();
    // Tăng lượt tương tác - TaiLA
    addInteractive($school['page_id'], 'tuition', 'school_view');
    switch ($_POST['do']) {
        case 'list_month':
            // Tăng số đếm tương tác của trường
            increaseSchoolInteractive($school['page_id'], 'tuition');

            if (!canView($_POST['page_name'], 'tuitions')) {
                _api_error(403);
            }
            // Lấy ra danh sách học phí các tháng của trường
            $tuitions = $tuitionDao->getSchoolTuitions($school['page_id']);

            // Xử lý lấy ra các thông tin trả về app
            $totalMonth = 0;
            $totalPaidMonth = 0;
            $totalPaidCount = 0;

            $tuitions_month = array();
            for ($i = 0; $i < count($tuitions); $i++) {
                $j = $i + 1;
                $totalMonth = $totalMonth + $tuitions[$i]['total_amount'];
                $totalPaidMonth = $totalPaidMonth + $tuitions[$i]['paid_amount'];
                $totalPaidCount = $totalPaidCount + $tuitions[$i]['paid_count'];
                if($tuitions[$i]['month'] != $tuitions[$j]['month']) {
                    $tuition_month['month'] = $tuitions[$i]['month'];
                    $tuition_month['paid_count'] = $totalPaidCount;
                    $tuition_month['total_month'] = $totalMonth;
                    $tuition_month['total_paid_month'] = $totalPaidMonth;
                    $tuitions_month[] = $tuition_month;

                    $totalMonth = 0;
                    $totalPaidMonth = 0;
                    $totalPaidCount = 0;
                }
            }
            // return
            return_json(array(
                'code' => 200,
                'message' => __("OK"),
                'data' =>array('tuitions_month' => $tuitions_month)
            ));
            break;
        case 'list_tuition_of_month':
            if(is_empty($_POST['month'])) {
                _api_error(400);
            }
            $month = $_POST['month'];
            // Lấy danh sách học phí tháng
            $tuitions = $tuitionDao->getAllTuitionOfMonth($school['page_id'], $month);
            $temps = array();
            foreach ($tuitions as $tuition) {
                //$childIds = $childDao->getChildIdOfClass($tuition['class_id']);
                $class = getClassData($tuition['class_id'], CLASS_INFO);
                $children = getClassData($tuition['class_id'], CLASS_CHILDREN);
                $childIds = array_keys($children);
                $totalChild = count($childIds);
                $tuition['total_child'] = $totalChild;
                $tuition['group_title'] = $class['group_title'];
                $temps[] = $tuition;
            }
            // return
            return_json(array(
                'code' => 200,
                'message' => __("OK"),
                'data' =>array('tuitions_of_month' => $temps)
            ));
            break;
        case 'tuition_detail':
            if(!isset($_POST['tuition_id']) || !is_numeric($_POST['tuition_id'])) {
                _api_error(400);
            }
            $data = $tuitionDao->getTuition($_POST['tuition_id']);
            if (is_null($data)) {
                _api_error(404);
            }
            // Tăng lượt tương tác - TaiLA
            addInteractive($school['page_id'], 'tuition', 'school_view', $_POST['tuition_id']);
            // return
            return_json(array(
                'code' => 200,
                'message' => __("OK"),
                'data' =>array('tuitions_detail' => $data)
            ));
            break;
        default:
            _api_error(400);
            break;
    }

} catch (Exception $e) {
    //$db->rollback();
    // return
    header('HTTP/1.1 400 API Error');
    return_json(array(
        'code' => 0,
        'message' => $e->getMessage(),
        'data' => array()
    ));
} finally {
    //$db->autocommit(true);
}
?>