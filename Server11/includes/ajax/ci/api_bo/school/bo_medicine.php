<?php
/**
 * Package: ajax/ci/api_bo/school
 *
 * @package ConIu
 * @author TaiLA
 */

if(is_empty($_POST['page_name']) || !valid_username($_POST['page_name'])) {
    _api_error(404);
}

include_once(DAO_PATH . 'dao_medicine.php');
include_once(DAO_PATH . 'dao_user.php');
include_once(DAO_PATH . 'dao_class.php');
include_once(DAO_PATH . 'dao_parent.php');
include_once(DAO_PATH . 'dao_child.php');


$medicineDao = new MedicineDAO();
$userDao = new UserDAO();
$classDao = new ClassDAO();
$parentDao = new ParentDAO();
$childDao = new ChildDAO();


//$school = getSchool($_POST['page_name']);
$school = getSchoolDataByUsername($_POST['page_name'], SCHOOL_INFO);
if (is_null($school)) {
    _error(403);
}

try {
    $return = array();
    $db->autocommit(false);
    switch ($_POST['do']) {
        case 'list_today':
            // Tăng số đếm tương tác của trường
            increaseSchoolInteractive($school['page_id'], 'medicine');
            // Tăng lượt tương tác - TaiLA
            addInteractive($school['page_id'], 'medicine', 'school_view');

            if (!canView($_POST['page_name'], 'medicines')) {
                _api_error(403);
            }
            // Lấy ra danh sách lớp của trường
            //$classes = $classDao->getClassesOfSchoolForAPI($school['page_id']);
            $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
            $classesMedicine = array();
            foreach ($classes as $class) {
                $today = date($system['date_format']);
                $medicines = $medicineDao->getClassMedicineOnDateForAPI($class['group_id'], $today);
                $class['medicines'] = $medicines;
                $classesMedicine[] = $class;
            }
            // return
            return_json(array(
                'code' => 200,
                'message' => __("OK"),
                'data' =>array('classes' => $classesMedicine)
            ));
            break;
        case 'confirm_selected':
            $db->begin_transaction();

            // Lấy danh sách thuốc cần xác nhận của lớp
            $medicineIds = isset($_REQUEST['list_medicine_id'])? $_REQUEST['list_medicine_id']: array();
            // Lấy danh sách thuốc đã xác nhận trong ngày hôm nay
            $today = date($system['date_format']);
            $medicineIdsConfirmed = $medicineDao->getAllMedicineIdConfirmed($school['page_id'], $today);
            // Lấy danh sách id của đơn thuốc cần xác nhận, loại bỏ những đơn thuốc đã hủy hoặc đã xác nhận rồi
            $medicineIds = array_diff($medicineIds, $medicineIdsConfirmed);
            //Cập nhật trạng thái của lần gửi thuốc
            if(count($medicineIds) > 0) {
                $medicineDao->updateAllMedicineStatus(MEDICINE_STATUS_CONFIRMED, $medicineIds);
                foreach ($medicineIds as $medicineId) {
                    $medicine = $medicineDao->getMedicine($medicineId);
                    //$child = $childDao->getChild($medicine['child_id']);
                    $child = getChildData($medicine['child_id'], CHILD_INFO);
                    //Thông báo quản lý trường và phụ huynh

                    // Lấy danh sách phụ huynh của trẻ
                    //$parentIds = array();
                    //$parentIds = $parentDao->getParentIds($medicine['child_id']);
                    $parents = getChildData($medicine['child_id'], CHILD_PARENTS);
                    if (!is_null($child) && !is_null($parents)) {
                        $parentIds = array_keys($parents);
                        if(count($parentIds) > 0) {
                            $userDao->postNotifications($parentIds, NOTIFICATION_CONFIRM_MEDICINE, NOTIFICATION_NODE_TYPE_CHILD,
                                $medicine['medicine_id'], convertText4Web($medicine['medicine_list']), $child['child_id'], convertText4Web($child['child_name']));
                        }
                    }

                    // Lấy user của những quản lý nhận được thông báo
//                $userIds = getUserIdsReceiveNotifyOfSchoolForClass($notify_settings, 'medicines', $class['school_id'], $school['page_admin']);
//
//                $userDao->postNotifications($userIds, NOTIFICATION_CONFIRM_MEDICINE, NOTIFICATION_NODE_TYPE_SCHOOL,
//                    $medicine['medicine_id'], convertText4Web($medicine['medicine_list']), $school['page_name'], convertText4Web($child['child_name']));
                }
            }

            $db->commit();
            // return
            return_json(array(
                'code' => 200,
                'message' => __("Success"),
                'data' => array()
            ));
            break;
        default:
            _api_error(400);
            break;
    }

} catch (Exception $e) {
    //$db->rollback();
    // return
    _api_error(0, $e->getMessage());
} finally {
    //$db->autocommit(true);
}
?>