<?php
/**
 * Package: ajax/ci/api_bo/school
 *
 * @package ConIu
 * @author TaiLA
 */

if(is_empty($_POST['page_name']) || !valid_username($_POST['page_name'])) {
    _error(404);
}

include_once(DAO_PATH.'dao_school.php');
include_once(DAO_PATH.'dao_class.php');
include_once(DAO_PATH.'dao_tuition.php');
include_once(DAO_PATH.'dao_child.php');
include_once(DAO_PATH.'dao_parent.php');
include_once(DAO_PATH.'dao_user.php');
include_once(DAO_PATH.'dao_teacher.php');
include_once(DAO_PATH.'dao_service.php');
include_once(DAO_PATH.'dao_attendance.php');
$schoolDao = new SchoolDAO();
$classDao = new ClassDAO();
$tuitionDao = new TuitionDAO();
$childDao = new ChildDAO();
$parentDao = new ParentDAO();
$userDao = new UserDAO();
$teacherDao = new TeacherDAO();
$serviceDao = new ServiceDAO();
$attendanceDao = new AttendanceDAO();

$school = getSchoolDataByUsername($_POST['page_name'], SCHOOL_INFO);
if (is_null($school)) {
    _api_error(403);
}

try {
    $return = array();
    // Tăng lượt tương tác - TaiLA
    addInteractive($school['page_id'], 'service', 'school_view');
    switch ($_POST['do']) {
        case 'history':
            // Tăng số đếm tương tác của trường
            increaseSchoolInteractive($school['page_id'], 'service');

            if (!canView($_POST['page_name'], 'services')) {
                _api_error(403);
            }
            $begin = isset($_POST['begin'])? $_POST['begin']: (date($system['date_format'], mktime(0, 0, 0, date("m"), 1, date("Y"))));
            $end = isset($_POST['end'])? $_POST['end']: (toSysDate($date));

            // Lấy ra tất cả dịch vụ của toàn trường
            $services = $serviceDao->getServiceUsageSummarySchoolForAPI($school['page_id'], $begin, $end);
            $serviceIds = array();
            foreach ($services as $service) {
                $serviceIds[] = $service['service_id'];
            }
            // Lấy tất cả dịch vụ của toàn trường
            $serviceAll = $serviceDao->getAllSchoolServicesForAPI($school['page_id']);
            $servicesNoUse = array();
            foreach ($serviceAll as $service) {
                $serviceNoUse = array();
                if(!in_array($service['service_id'], $serviceIds)) {
                    $serviceNoUse['service_id'] = $service['service_id'];
                    $serviceNoUse['type'] = $service['type'];
                    $serviceNoUse['service_name'] = $service['service_name'];
                    $serviceNoUse['fee'] = $service['fee'];
                    $serviceNoUse['is_food'] = $service['is_food'];
                    $serviceNoUse['cnt'] = 0;
                }
                if(count($serviceNoUse) > 0) {
                    $servicesNoUse[] = $serviceNoUse;
                }
            }
            // Gộp hai mảng service với nhau
            $servicesAll = array_merge($services, $servicesNoUse);
            $servicesAll = sortMultiOrderArray($servicesAll, [["is_food", "DESC"], ["type", "DESC"]]);
            $fields = array('service_id', 'type', 'service_name', 'fee', 'is_food', 'totalUsageService');
            $allService = array();
            foreach ($servicesAll as $row) {
                $row['totalUsageService'] = $row['cnt'];
                $row = getArrayFromKeys($row, $fields);
                $allService[] = $row;
            }

            // return
            return_json(array(
                'code' => 200,
                'message' => __("OK"),
                'data' =>array('services' => $allService)
            ));
            break;
        case 'service_history':
            // Tổng hợp số sử dụng dịch vụ từng lớp theo dịch vụ
//            if (!canEdit($_POST['page_name'], 'services')) {
//                _error(403);
//            }
            $using_at = isset($_POST['using_at'])? $_POST['using_at']: (date($system['date_format'], mktime(0, 0, 0, date("m"), 1, date("Y"))));
            if (!isset($_POST['service_id']) || ($_POST['service_id'] <= 0)) {
                _api_error(400);
            }

            // Lấy chi tiết dịch vụ
            $services = getSchoolData($school['page_id'], SCHOOL_SERVICES);
            $service = $services[$_POST['service_id']];

            if ($service['type'] == SERVICE_TYPE_COUNT_BASED) {
                // Lấy danh sách lớp của trường
                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                $serviceClass = array();
                // Lặp danh sách lớp, Lấy số lượng sử dụng dịch vụ theo từng lớp
                foreach ($classes as $class) {
                    $countClass = array();
                    $count = $serviceDao->getCountServiceBasedClass($_POST['service_id'], $class['group_id'], $using_at);
                    $countClass['totalUsageService'] = $count;
                    $countClass['group_title'] = $class['group_title'];
                    $serviceClass[] = $countClass;
                }
            } else {
                // Lấy danh sách lớp của trường
                $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
                $serviceClass = array();
                // Lặp danh sách lớp, Lấy số lượng sử dụng dịch vụ theo từng lớp
                foreach ($classes as $class) {
                    $countClass = array();
                    $count = $serviceDao->getCountServiceBasedClassNCB($_POST['service_id'], $class['group_id'], $using_at);
                    $countClass['totalUsageService'] = $count;
                    $countClass['group_title'] = $class['group_title'];
                    $serviceClass[] = $countClass;
                }
            }
            // return
            return_json(array(
                'code' => 200,
                'message' => $message,
                'data' => array(
                    'history' => $serviceClass
                )
            ));
            break;
        case 'show_service':
            if (!canView($_POST['page_name'], 'services')) {
                _api_error(403);
            }

            // Lấy ra tất cả dịch vụ của toàn trường
            $schoolService = getSchoolData($school['page_id'], SCHOOL_SERVICES);
            $schoolService = sortMultiOrderArray($schoolService, [["is_food", "DESC"], ["type", "DESC"]]);
            $services = array();
            foreach ($schoolService as $service) {
                if ($service['type'] == SERVICE_TYPE_COUNT_BASED) {
                    $services[] = $service;
                }
            }

            $schoolClass = getSchoolData($school['page_id'], SCHOOL_CLASSES);
            $schoolClass = sortArray($schoolClass, "ASC", "class_level_id");
            $classes = array();
            foreach ($schoolClass as $class) {
                $result['group_id'] = $class['group_id'];
                $result['group_title'] = $class['group_title'];
                $classes[] = $result;
            }
            // return
            return_json(array(
                'code' => 200,
                'message' => __("OK"),
                'data' =>array(
                    'services' => $services,
                    'classes' => $classes
                )
            ));
            break;
        case 'get_children':
            if (!canView($_POST['page_name'], 'services')) {
                _api_error(403);
            }
            $return = array();

            //Validate thông tin đầu vào
            if (!isset($_POST['service_id']) || ($_POST['service_id'] <= 0)) {
                _api_error(400);
            }
            if (!isset($_POST['using_at']) || ($_POST['using_at'] == '')) {
                _api_error(400);
            }
            //$results = $serviceDao->getServiceChild4Record($school['page_id'], $_POST['service_id'], $_POST['using_at'], $_POST['class_id']);
            $results = $serviceDao->getSchoolChildService4Record($school['page_id'], $_POST['service_id'], $_POST['using_at'], $_POST['class_id']);

            $absentChildIds = $attendanceDao->getAbsentChildIds($school['page_id'], $_POST['using_at'],$_POST['class_id']);

            // return
            return_json(array(
                'code' => 200,
                'message' => __("OK"),
                'data' =>array(
                    'children' => $results['children'],
                    'usage_count' => $results['usage_count'],
                    'absentChildIds' => $absentChildIds
                )
            ));
            break;

        case 'record':
            if (!canEdit($_POST['page_name'], 'services')) {
                _error(403);
            }
            $registerChildIds = isset($_REQUEST['childIds'])? $_REQUEST['childIds']: array(); //ID của trẻ sử dụng dịch vụ
            //$oldChildIds = isset($_POST['oldChildIds'])? $_POST['oldChildIds']: array(); //ID của trẻ đã sử dụng dịch vụ ở lần ghi trước
            $children = $serviceDao->getSchoolChildService4Record($school['page_id'], $_POST['service_id'], $_POST['using_at'], $_POST['class_id']);
            $oldChildIds = array();
            foreach ($children['children'] as $child) {
                if ($child['recorded_user_id'] > 0) {
                    $oldChildIds[] = $child['child_id'];
                }
            }

            //Danh sách trẻ sử dụng dịch vụ mới (so với lần lưu trước)
            $newChildIds = array_diff($registerChildIds, $oldChildIds);

            //Danh sách trẻ bỏ sử dụng dịch vụ (do lần trước ghi nhầm)
            $cancelChildIds = array_diff($oldChildIds, $registerChildIds);

            if ((count($newChildIds) + count($cancelChildIds)) > 0) {
                $db->begin_transaction();
                $serviceDao->recordServiceUsage($_POST['service_id'], $newChildIds, $cancelChildIds, $_POST['using_at']);
                $service = $serviceDao->getServiceById($_POST['service_id']);

                if (count($cancelChildIds) > 0) {
                    //Thông báo tới phụ huynh có trẻ huỷ sử dụng dịch vụ.
                    $parents = $childDao->getListChildNParentId($cancelChildIds);
                    if(count($parents) > 0) {
                        foreach ($parents as $parent) {
                            $userDao->postNotifications($parent['parent_id'], NOTIFICATION_SERVICE_CANCEL_COUNTBASED, NOTIFICATION_NODE_TYPE_CHILD,
                                $_POST['service_id'], convertText4Web($service['service_name']), $parent['child_id'], convertText4Web($parent['child_name']));
                        }
                    }
                    //Thông báo tới giáo viên của lớp.
                    foreach ($cancelChildIds as $id) {
                        $teachers = $teacherDao->getTeacherIdClassBySchoolAndChild($school['page_id'], $id);
                        $childName = $childDao->getChildName($id);
                        if(count($teachers) > 0) {
                            foreach ($teachers as $teacher) {
                                $userDao->postNotifications($teacher['teacher_id'], NOTIFICATION_SERVICE_CANCEL_COUNTBASED, NOTIFICATION_NODE_TYPE_CLASS,
                                    $_POST['service_id'], convertText4Web($service['service_name']), $teacher['class_name'], convertText4Web($childName));
                            }
                        }
                    }
                }

                //Thông báo tới phụ huynh có trẻ đăng ký sử dụng dịch vụ.
                if (count($newChildIds) > 0) {
                    $parents = $childDao->getListChildNParentId($newChildIds);
                    if(count($parents) > 0) {
                        foreach ($parents as $parent) {
                            $userDao->postNotifications($parent['parent_id'], NOTIFICATION_SERVICE_REGISTER_COUNTBASED, NOTIFICATION_NODE_TYPE_CHILD,
                                $_POST['service_id'], convertText4Web($service['service_name']), $parent['child_id'], convertText4Web($parent['child_name']));
                        }
                    }
                    //Thông báo tới giáo viên của lớp.
                    foreach ($newChildIds as $id) {
                        $teachers = $teacherDao->getTeacherIdClassBySchoolAndChild($school['page_id'], $id);
                        $childName = $childDao->getChildName($id);
                        if(count($teachers) > 0) {
                            foreach ($teachers as $teacher) {
                                $userDao->postNotifications($teacher['teacher_id'], NOTIFICATION_SERVICE_REGISTER_COUNTBASED, NOTIFICATION_NODE_TYPE_CLASS,
                                    $_POST['service_id'], convertText4Web($service['service_name']), $teacher['class_name'], convertText4Web($childName));
                            }
                        }
                    }
                }

                $db->commit();
                $message = __("Done, Service usage info have been updated");
            } else {
                $message = __("Service usage info has no change");
            }
            // return
            return_json(array(
                'code' => 200,
                'message' => $message,
                'data' => array()
            ));

            break;

        default:
            _api_error(400);
            break;
    }

} catch (Exception $e) {
    //$db->rollback();
    // return
    _api_error(0, $e->getMessage());
} finally {
    //$db->autocommit(true);
}
?>