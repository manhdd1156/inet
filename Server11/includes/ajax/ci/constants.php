<?php
/**
 * Created by PhpStorm.
 * User: FIRST
 * Date: 4/12/2016
 * Time: 5:19 PM
 * MODIFY GRADE LEVEL OF SCHOOL BY MANHDD 04/01/2021
 */

// Hằng lưu số ngày tuổi của trẻ theo số năm để vẽ biểu đồ
define("DAY_AGE_MAX_FOR_CHART", 1826);

$year_begin = 2015;
$year_end = 2030;

$schoolTypes = array(
    array('type_name' => 'Tư thục', 'type_value' => '1'),
    array('type_name' => 'Bán công', 'type_value' => '2'),
    array('type_name' => 'Công lập', 'type_value' => '3'),
    array('type_name' => 'Quốc tế', 'type_value' => '4'),
    array('type_name' => 'Song ngữ', 'type_value' => '5')
);
$cities = array(
    array('city_id' => '1', 'city_slug' => 'ha-noi', 'city_name' => 'Hà Nội', 'district' => array(
        array('district_slug' => 'quan-ba-dinh', 'district_name' => 'Quận Ba Đình'),
        array('district_slug' => 'quan-hoan-kiem', 'district_name' => 'Quận Hoàn Kiếm'),
        array('district_slug' => 'quan-hai-ba-trung', 'district_name' => 'Quận Hai Bà Trưng'),
        array('district_slug' => 'quan-dong-da', 'district_name' => 'Quận Đống Đa'),
        array('district_slug' => 'quan-tay-ho', 'district_name' => 'Quận Tây Hồ'),
        array('district_slug' => 'quan-cau-giay', 'district_name' => 'Quận Cầu Giấy'),
        array('district_slug' => 'quan-thanh-xuan', 'district_name' => 'Quận Thanh Xuân'),
        array('district_slug' => 'quan-hoang-mai', 'district_name' => 'Quận Hoàng Mai'),
        array('district_slug' => 'quan-long-bien', 'district_name' => 'Quận Long Biên'),
        array('district_slug' => 'quan-ha-dong', 'district_name' => 'Quận Hà Đông'),
        array('district_slug' => 'tx-son-tay', 'district_name' => 'Thị xã Sơn Tây'),
        array('district_slug' => 'huyen-thanh-tri', 'district_name' => 'Huyện Thanh Trì'),
        array('district_slug' => 'huyen-gia-lam', 'district_name' => 'Huyện Gia Lâm'),
        array('district_slug' => 'huyen-dong-anh', 'district_name' => 'Huyện Đông Anh'),
        array('district_slug' => 'huyen-soc-son', 'district_name' => 'Huyện Sóc Sơn'),
        array('district_slug' => 'huyen-tu-liem', 'district_name' => 'Huyện Từ Liêm'),
        array('district_slug' => 'huyen-ba-vi', 'district_name' => 'Huyện Ba Vì'),
        array('district_slug' => 'huyen-phuc-tho', 'district_name' => 'Huyện Phúc Thọ'),
        array('district_slug' => 'huyen-thach-that', 'district_name' => 'Huyện Thạch Thất'),
        array('district_slug' => 'huyen-quoc-oai', 'district_name' => 'Huyện Quốc Oai'),
        array('district_slug' => 'huyen-chuong-my', 'district_name' => 'Huyện Chương Mỹ'),
        array('district_slug' => 'huyen-dan-phuong', 'district_name' => 'Huyện Đan Phượng'),
        array('district_slug' => 'huyen-hoai-duc', 'district_name' => 'Huyện Hoài Đức'),
        array('district_slug' => 'huyen-thanh-oai', 'district_name' => 'Huyện Thanh Oai'),
        array('district_slug' => 'huyen-my-duc', 'district_name' => 'Huyện Mỹ Đức'),
        array('district_slug' => 'huyen-ung-hoa', 'district_name' => 'Huyện Ứng Hoà'),
        array('district_slug' => 'huyen-thuong-tin', 'district_name' => 'Huyện Thường Tín'),
        array('district_slug' => 'huyen-phu-xuyen', 'district_name' => 'Huyện Phú Xuyên'),
        array('district_slug' => 'huyen-me-linh', 'district_name' => 'Huyện Mê Linh')
    )),
    array('city_id' => '2', 'city_slug' => 'tp-ho-chi-minh', 'city_name' => 'TP. Hồ Chí Minh', 'district' => array(
        array('district_slug' => 'quan-1', 'district_name' => 'Quận 1'),
        array('district_slug' => 'quan-2', 'district_name' => 'Quận 2'),
        array('district_slug' => 'quan-3', 'district_name' => 'Quận 3'),
        array('district_slug' => 'quan-4', 'district_name' => 'Quận 4'),
        array('district_slug' => 'quan-5', 'district_name' => 'Quận 5'),
        array('district_slug' => 'quan-6', 'district_name' => 'Quận 6'),
        array('district_slug' => 'quan-7', 'district_name' => 'Quận 7'),
        array('district_slug' => 'quan-8', 'district_name' => 'Quận 8'),
        array('district_slug' => 'quan-9', 'district_name' => 'Quận 9'),
        array('district_slug' => 'quan-10', 'district_name' => 'Quận 10'),
        array('district_slug' => 'quan-11', 'district_name' => 'Quận 11'),
        array('district_slug' => 'quan-12', 'district_name' => 'Quận 12'),
        array('district_slug' => 'quan-thu-duc', 'district_name' => 'Quận Thủ Đức'),
        array('district_slug' => 'quan-go-vap', 'district_name' => 'Quận Gò Vấp'),
        array('district_slug' => 'quan-binh-thanh', 'district_name' => 'Quận Bình Thạnh'),
        array('district_slug' => 'quan-tan-binh', 'district_name' => 'Quận Tân Bình'),
        array('district_slug' => 'quan-tan-phu', 'district_name' => 'Quận Tân Phú'),
        array('district_slug' => 'quan-phu-nhuan', 'district_name' => 'Quận Phú Nhuận'),
        array('district_slug' => 'quan-binh-tan', 'district_name' => 'Quận Bình Tân'),
        array('district_slug' => 'huyen-cu-chi', 'district_name' => 'Huyện Củ Chi'),
        array('district_slug' => 'huyen-hoc-mon', 'district_name' => 'Huyện Hóc Môn'),
        array('district_slug' => 'huyen-binh-chanh', 'district_name' => 'Huyện Bình Chánh'),
        array('district_slug' => 'huyen-nha-be', 'district_name' => 'Huyện Nhà Bè'),
        array('district_slug' => 'huyen-can-gio', 'district_name' => 'Huyện Cần Giờ')
    )),
    array('city_id' => '17', 'city_slug' => 'da-nang', 'city_name' => 'Đà Nẵng', 'district' => array(
        array('district_slug' => 'quan-hai-chau', 'district_name' => 'Quận Hải Châu'),
        array('district_slug' => 'quan-thanh-khe', 'district_name' => 'Quận Thanh Khê'),
        array('district_slug' => 'quan-son-tra', 'district_name' => 'Quận Sơn Trà'),
        array('district_slug' => 'quan-ngu-hanh-son', 'district_name' => 'Quận Ngũ Hành Sơn'),
        array('district_slug' => 'quan-lien-chieu', 'district_name' => 'Quận Liên Chiểu'),
        array('district_slug' => 'quan-cam-le', 'district_name' => 'Quận Cẩm Lệ'),
        array('district_slug' => 'huyen-hoa-vang', 'district_name' => 'Huyện Hoà Vang'),

    )),
    array('city_id' => '3', 'city_name' => 'An Giang'),
    array('city_id' => '4', 'city_name' => 'Bà Rịa - Vũng Tàu'),
    array('city_id' => '5', 'city_name' => 'Bắc Giang'),
    array('city_id' => '6', 'city_name' => 'Bắc Kạn'),
    array('city_id' => '7', 'city_name' => 'Bạc Liêu'),
    array('city_id' => '8', 'city_name' => 'Bắc Ninh'),
    array('city_id' => '9', 'city_name' => 'Bến Tre'),
    array('city_id' => '10', 'city_name' => 'Bình Định'),
    array('city_id' => '11', 'city_name' => 'Bình Dương'),
    array('city_id' => '12', 'city_name' => 'Bình Phước'),
    array('city_id' => '13', 'city_name' => 'Bình Thuận'),
    array('city_id' => '14', 'city_name' => 'Cà Mau'),
    array('city_id' => '15', 'city_name' => 'Cần Thơ'),
    array('city_id' => '16', 'city_name' => 'Cao Bằng'),
    array('city_id' => '18', 'city_name' => 'Đắk Lắk'),
    array('city_id' => '19', 'city_name' => 'Đắk Nông'),
    array('city_id' => '20', 'city_name' => 'Điện Biên'),
    array('city_id' => '21', 'city_name' => 'Đồng Nai'),
    array('city_id' => '22', 'city_name' => 'Đồng Tháp'),
    array('city_id' => '23', 'city_name' => 'Gia Lai'),
    array('city_id' => '24', 'city_name' => 'Hà Giang'),
    array('city_id' => '25', 'city_name' => 'Hà Nam'),
    array('city_id' => '26', 'city_name' => 'Hà Tĩnh'),
    array('city_id' => '27', 'city_name' => 'Hải Dương'),
    array('city_id' => '28', 'city_name' => 'Hải Phòng'),
    array('city_id' => '29', 'city_name' => 'Hậu Giang'),
    array('city_id' => '30', 'city_name' => 'Hòa Bình'),
    array('city_id' => '31', 'city_name' => 'Hưng Yên'),
    array('city_id' => '32', 'city_name' => 'Khánh Hòa'),
    array('city_id' => '33', 'city_name' => 'Kiên Giang'),
    array('city_id' => '34', 'city_name' => 'Kon Tum'),
    array('city_id' => '35', 'city_name' => 'Lai Châu'),
    array('city_id' => '36', 'city_name' => 'Lâm Đồng'),
    array('city_id' => '37', 'city_name' => 'Lạng Sơn'),
    array('city_id' => '38', 'city_name' => 'Lào Cai'),
    array('city_id' => '39', 'city_name' => 'Long An'),
    array('city_id' => '40', 'city_name' => 'Nam Định'),
    array('city_id' => '41', 'city_name' => 'Nghệ An'),
    array('city_id' => '42', 'city_name' => 'Ninh Bình'),
    array('city_id' => '43', 'city_name' => 'Ninh Thuận'),
    array('city_id' => '44', 'city_name' => 'Phú Thọ'),
    array('city_id' => '45', 'city_name' => 'Phú Yên'),
    array('city_id' => '46', 'city_name' => 'Quảng Bình'),
    array('city_id' => '47', 'city_name' => 'Quảng Nam'),
    array('city_id' => '48', 'city_name' => 'Quảng Ngãi'),
    array('city_id' => '49', 'city_name' => 'Quảng Ninh'),
    array('city_id' => '50', 'city_name' => 'Quảng Trị'),
    array('city_id' => '51', 'city_name' => 'Sóc Trăng'),
    array('city_id' => '52', 'city_name' => 'Sơn La'),
    array('city_id' => '53', 'city_name' => 'Tây Ninh'),
    array('city_id' => '54', 'city_name' => 'Thái Bình'),
    array('city_id' => '55', 'city_name' => 'Thái Nguyên'),
    array('city_id' => '56', 'city_name' => 'Thanh Hóa'),
    array('city_id' => '57', 'city_name' => 'Thừa Thiên Huế'),
    array('city_id' => '58', 'city_name' => 'Tiền Giang'),
    array('city_id' => '59', 'city_name' => 'Trà Vinh'),
    array('city_id' => '60', 'city_name' => 'Tuyên Quang'),
    array('city_id' => '61', 'city_name' => 'Vĩnh Long'),
    array('city_id' => '62', 'city_name' => 'Vĩnh Phúc',
        'district' => array(
            array('district_slug' => 'huyen-yen-lac', 'district_name' => 'Huyện Yên Lạc'),
        )
    ),
    array('city_id' => '63', 'city_name' => 'Yên Bái')
);

define("PERMISSION_NONE", 0);
define("PERMISSION_JUST_VIEW", 1);
define("PERMISSION_MANAGE", 2);
define("PERMISSION_ALL", 3);

$roles = array(array('role_id' => PERMISSION_ALL, 'role_name' => 'Toàn quyền xử lý'),
    array('role_id' => PERMISSION_MANAGE, 'role_name' => 'Chỉ quản lý, không sửa thông tin chính'),
    array('role_id' => PERMISSION_JUST_VIEW, 'role_name' => 'Chỉ xem')
);

define("CONIU_CATEGORY_ID", 1);
define("SCHOOL_CATEGORY_ID", 2);
define("CLASS_CATEGORY_ID", 2);

// Trạng thái của trường
define("SCHOOL_USING_CONIU", 1);
define("SCHOOL_HAVE_PAGE_CONIU", 2);
define("SCHOOL_WAITING_CONFIRM", 3);

//Định nghĩa loại quản lý của user.
define("MANAGE_ITSELF", 0);
define("MANAGE_CHILD", 1);
define("MANAGE_SCHOOL", 2);
define("MANAGE_CLASS", 3);

define("STATUS_WAIT_APPROVAL", 2);
define("STATUS_ACTIVE", 1);
define("STATUS_INACTIVE", 0);

//Danh sách notification gửi từ page, group
$group_notification = array(
    'post_in_page', 'post_in_group'
);
/* Định nghĩa các loại notification của NOGA */
define("NOTIFICATION_POST_IN_PAGE", 'post_in_page');
define("NOTIFICATION_POST_IN_GROUP", 'post_in_group');
define("NOTIFICATION_CONIU", 'notification_coniu');
define("NOTIFICATION_CONIU_CONGRATULATIONS", 'notification_coniu_congratulations');
define("NOTIFICATION_CONIU_INFO", 'notification_coniu_info');

define("NOTIFICATION_MUST_UPDATE_INFORMATION", 'notification_must_update_information');
define("NOTIFICATION_INFORMATION_INTERESTED", 'notification_information_interested');
define("NOTIFICATION_REMIND_ATTENDANCE", 'notification_remind_attendance');
define("NOTIFICATION_REMIND_SCHEDULE", 'notification_remind_schedule');
define("NOTIFICATION_REMIND_MENU", 'notification_remind_menu');
define("NOTIFICATION_REMIND_INPUT_FOETUS_INFO", 'notification_remind_input_foetus_info');
define("NOTIFICATION_REMIND_INPUT_CHILD_HEALTH", 'notification_remind_input_child_health');
define("NOTIFICATION_POPULAR_POST", 'notification_popular_post');
define("NOTIFICATION_REMIND_REPORT", 'notification_remind_report');

define("NOTIFICATION_REMIND_OPEN_APP", 'notification_remind_open_app');
define("NOTIFICATION_REMIND_HEIGHT_WEIGHT", 'notification_remind_height_weight');
define("NOTIFICATION_ADD_NEW_CHILD", 'notification_add_new_child');

define("NOTIFICATION_REMIND_SCHOOL_REVIEW", 'notification_remind_school_review');
define("NOTIFICATION_SCHOOL_REVIEWS_SUMMARIES", 'notification_school_reviews_summaries');
/* END - Các loại notification của NOGA */

// Định nghĩa các đối tượng nhận notification
define("ALL_USER", 1);
define("SCHOOL_MANAGER", 2);
define("TEACHER", 3);
define("PARENT", 4);

//Định nghĩa các loại notification
define("NOTIFICATION_NEW_CLASS", 'new_class');
define("NOTIFICATION_UPDATE_CLASS", 'update_class');

define("NOTIFICATION_NEW_CLASSLEVEL", 'new_class_level');
define("NOTIFICATION_UPDATE_CLASSLEVEL", 'update_class_level');

define("NOTIFICATION_ASSIGN_CLASS", 'assign_class');
define("NOTIFICATION_NEW_TEACHER", 'new_teacher');
define("NOTIFICATION_NEW_CHILD", 'new_child');

// Thông báo thêm, sửa trẻ từ giáo viên
define("NOTIFICATION_UPDATE_CHILD_TEACHER", 'update_child_teacher');
define("NOTIFICATION_NEW_CHILD_TEACHER", 'new_child_teacher');
// Thông báo xác nhận trẻ từ nhà trường
define("NOTIFICATION_CONFIRM_CHILD_TEACHER", 'confirm_child_teacher');
define("NOTIFICATION_UNCONFIRM_CHILD_TEACHER", 'unconfirm_child_teacher');

define("NOTIFICATION_NEW_EVENT_SCHOOL", 'notification_new_event_school');
define("NOTIFICATION_NEW_EVENT_CLASS_LEVEL", 'notification_new_event_class_level');
define("NOTIFICATION_NEW_EVENT_CLASS", 'notification_new_event_class');
define("NOTIFICATION_UPDATE_EVENT_SCHOOL", 'notification_update_event_school');
define("NOTIFICATION_UPDATE_EVENT_CLASS_LEVEL", 'notification_update_event_class_level');
define("NOTIFICATION_UPDATE_EVENT_CLASS", 'notification_update_event_class');
define("NOTIFICATION_CANCEL_EVENT_SCHOOL", 'notification_cancel_event_school');
define("NOTIFICATION_CANCEL_EVENT_CLASS_LEVEL", 'notification_cancel_event_class_level');
define("NOTIFICATION_CANCEL_EVENT_CLASS", 'notification_cancel_event_class');
define("NOTIFICATION_REGISTER_EVENT_CHILD", 'notification_register_event_child');
define("NOTIFICATION_CANCEL_REGISTRATION_EVENT_CHILD", 'notification_cancel_registration_event_child');
define("NOTIFICATION_REGISTER_EVENT_PARENT", 'notification_register_event_parent');
define("NOTIFICATION_CANCEL_REGISTRATION_EVENT_PARENT", 'notification_cancel_registration_event_parent');

define("NOTIFICATION_NEW_MEDICINE", 'notification_new_medicine');
define("NOTIFICATION_UPDATE_MEDICINE", 'notification_update_medicine');
define("NOTIFICATION_USE_MEDICINE", 'notification_use_medicine');
define("NOTIFICATION_CONFIRM_MEDICINE", 'notification_confirm_medicine');
define("NOTIFICATION_CANCEL_MEDICINE", 'notification_cancel_medicine');

define("NOTIFICATION_NEW_TUITION", 'notification_new_tuition');
define("NOTIFICATION_UPDATE_TUITION", 'notification_update_tuition');
define("NOTIFICATION_CONFIRM_TUITION", 'notification_confirm_tuition');
define("NOTIFICATION_CONFIRM_TUITION_SCHOOL", 'notification_confirm_tuition_school');
define("NOTIFICATION_UNCONFIRM_TUITION", 'notification_unconfirm_tuition');

// Thông báo được phân quyền
define("NOTIFICATION_UPDATE_ROLE", 'notification_update_role');

// Thông báo có lịch học
define("NOTIFICATION_NEW_SCHEDULE", 'notification_new_schedule');
define("NOTIFICATION_UPDATE_SCHEDULE", 'notification_update_schedule');

// Thông báo có thực đơn
define("NOTIFICATION_NEW_MENU", 'notification_new_menu');
define("NOTIFICATION_UPDATE_MENU", 'notification_update_menu');

// Thông báo người đón thay
define("NOTIFICATION_UPDATE_PICKER", 'notification_update_picker');
define("NOTIFICATION_CONFIRM_PICKER", 'notification_confirm_picker');

//Thông báo xin nghỉ của con
define("NOTIFICATION_ATTENDANCE_RESIGN", 'notification_attendance_resign');
define("NOTIFICATION_ATTENDANCE_CONFIRM", 'notification_attendance_confirm');

//Thông báo có báo cáo mới (sổ liên lạc)
define("NOTIFICATION_NEW_REPORT", 'notification_new_report');
define("NOTIFICATION_UPDATE_REPORT", 'notification_update_report');
define("NOTIFICATION_NEW_REPORT_TEMPLATE", 'notification_new_report_template');

//Thông báo có điểm mới ( giáo viên nhập điểm )
define("NOTIFICATION_NEW_POINT", 'notification_new_point');

//Thông báo có góp ý mới (phụ huynh đóng góp ý kiến)
define("NOTIFICATION_NEW_FEEDBACK", 'notification_new_feedback');
define("NOTIFICATION_CONFIRM_FEEDBACK", 'notification_confirm_feedback');

//Thông báo phụ huynh đăng ký dịch vụ mới
define("NOTIFICATION_SERVICE_REGISTER_UNCOUNTBASED", 'notification_service_register_uncountbased');
define("NOTIFICATION_SERVICE_REGISTER_COUNTBASED", 'notification_service_register_countbased');
//define("NOTIFICATION_REGISTER_SERVICE", 'notification_service_record');
define("NOTIFICATION_SERVICE_CANCEL_UNCOUNTBASED", 'notification_service_cancel_uncountbased');
define("NOTIFICATION_SERVICE_CANCEL_COUNTBASED", 'notification_service_cancel_countbased');

// Thông báo giáo viên lớp đăng ký dịch vụ số lần sử dụng
define("NOTIFICATION_SERVICE_REGISTER_COUNTBASED_CLASS", 'notification_service_register_countbased_class');
// Thông báo giáo viên lớp đăng ký dịch vụ số lần sử dụng từ mobile
define("NOTIFICATION_SERVICE_REGISTER_COUNTBASED_MOBILE", 'notification_service_register_countbased_mobile');

//Thông báo đón muộn
define("NOTIFICATION_ADD_LATEPICKUP_CLASS", 'notification_add_latepickup_class');
define("NOTIFICATION_REMOVE_LATEPICKUP_CLASS", 'notification_remove_latepickup_class');
define("NOTIFICATION_REGISTER_LATEPICKUP", 'notification_register_latepickup');
define("NOTIFICATION_UPDATE_LATEPICKUP_INFO", 'notification_updated_latepickup_info');
define("NOTIFICATION_ASSIGN_LATEPICKUP_CLASS", 'notification_assign_latepickup_class');
define("NOTIFICATION_CHILD_PICKEDUP", 'notification_child_pickedup');

// THông báo cho phụ huynh có thông tin sức khỏe mới từ trường hoặc lớp
define("NOTIFICATION_NEW_CHILD_HEALTH", 'notification_new_child_health');
define("NOTIFICATION_UPDATE_CHILD_HEALTH", 'notification_update_child_health');

//Định nghĩa các loại node của notification
define("NOTIFICATION_NODE_TYPE_USER", 'user');
define("NOTIFICATION_NODE_TYPE_CLASS", 'class');
define("NOTIFICATION_NODE_TYPE_CHILD", 'child');
define("NOTIFICATION_NODE_TYPE_SCHOOL", 'school');

// Thông báo qua trình phát triển của thai nhi
define("NOTIFICATION_FOETUS_DEVELOPMENT", 'notification_foetus_development');
// Thông báo qua trình phát triển của trẻ
define("NOTIFICATION_CHILD_DEVELOPMENT", 'notification_child_development');

// Thông báo quản lý trường có trẻ được giáo viên sửa trong tuần
define("NOTIFICATION_NEW_CHILD_EDIT_BY_TEACHER", 'notification_new_child_edit_by_teacher');

// Thông báo sửa trẻ từ nhà trường về phía phụ huynh
define("NOTIFICATION_UPDATE_CHILD_SCHOOL", 'notification_update_child_school');

// Thông báo phụ huynh thôi học cho trẻ
define("NOTIFICATION_LEAVE_SCHOOL_BY_PARENT", 'notification_leave_school_by_parent');

// Thông báo cho phụ huynh biết trẻ được thêm vào trường
define("NOTIFICATION_NEW_CHILD_EXIST", 'notification_new_child_exist');

// Thông báo cập nhật thông tin
define("NOTIFICATION_UPDATE_INFO", 'notification_update_info');

// Thông báo nhật ký
define("NOTIFICATION_ADD_DIARY_SCHOOL", 'notification_add_diary_school');
define("NOTIFICATION_EDIT_CAPTION_DIARY_SCHOOL", 'notification_edit_caption_diary_school');
define("NOTIFICATION_DELETE_PHOTO_DIARY_SCHOOL", 'notification_delete_photo_diary_school');
define("NOTIFICATION_ADD_PHOTO_DIARY_SCHOOL", 'notification_add_photo_diary_school');

// Thông báo điểm danh
define("NOTIFICATION_UPDATE_ATTENDANCE", 'notification_update_attendance');
define("NOTIFICATION_NEW_ATTENDANCE", 'notification_new_attendance');

// Thông báo điểm danh
define("NOTIFICATION_UPDATE_ATTENDANCE_BACK", 'notification_update_attendance_back');
define("NOTIFICATION_NEW_ATTENDANCE_BACK", 'notification_new_attendance_back');

//Định nghĩa phạm vi
define("SCHOOL_LEVEL", 1);
define("CLASS_LEVEL_LEVEL", 2);
define("CLASS_LEVEL", 3);
define("PARENT_LEVEL", 4);
define("CHILD_LEVEL", 5);
define("TEACHER_LEVEL", 6);

//Định nghĩa các định dạng ngày tháng
define("DB_DATE_FORMAT", 'YYYY-MM-DD');
define("DB_DATETIME_FORMAT", 'YYYY-MM-DD HH:MM:SS');
define("DB_DATETIME_FORMAT_DEFAULT", 'Y-m-d H:i:s');

define("MONEY_UNIT", 'VNĐ');
define("MAX_CONSECUTIVE_ABSENT_DAY", 7);

//Định nghĩa các loại đối tượng đăng ký tham gia sự kiện
define("PARTICIPANT_TYPE_CHILD", 0);
define("PARTICIPANT_TYPE_PARENT", 1);
define("PARTICIPANT_TYPE_TEACHER", 2);

//Định nghĩa trạng thái của đơn thuốc
define("MEDICINE_STATUS_NEW", 0);
define("MEDICINE_STATUS_CONFIRMED", 1);
define("MEDICINE_STATUS_CANCEL", 2);

//Định nghĩa trạng thái sự kiện
define("EVENT_STATUS_NORMAL", 0);
define("EVENT_STATUS_CANCELLED", 1);

//Định nghĩa các loại của loại phí
define("FEE_TYPE_MONTHLY", 1); //Phí thu hàng tháng
define("FEE_TYPE_DAILY", 2);   //Phí thu hàng ngày

//Định nghĩa các loại trạng thái của floại fee
define("FEE_ON", 1); //Đang sử dụng
define("FEE_OFF", 0);   //Hiện tại không sử dụng

//Định nghĩa các trạng thái người đón thay
define("PICKER_CONFIRMED", 1); // Đã đón trẻ (Đã xác nhận)
define("PICKER_NOT_CONFIRMED", 0); // Chưa trả trẻ (Chưa xác nhận)

//Định nghĩa loại học phí (thu hàng tháng, hay thu một lần)
define("TUITION_TYPE_MONTHLY", 0);
define("TUITION_TYPE_ONE_TIME", 1);

//Định nghĩa trạng thái nộp học phí
define("TUITION_CHILD_NOT_PAID", 0);
define("TUITION_CHILD_PARENT_PAID", 1);
define("TUITION_CHILD_CONFIRMED", 2);

//Định nghĩa đối tượng gọi đến bootstrap
define("BOOTSTRAP_CALLING_OTHER", 0);
define("BOOTSTRAP_CALLING_SCHOOL", 1);
//Thời gian gọi các hàm friends, new_people... trong User_construct. Đơn vị là second
define("NEW_USER_RELOAD_INTERVAL", 900); //= 15 phút * 60 giây

define("ATTENDANCE_ABSENCE", 0); //Nghỉ - Không tính tiền ăn
define("ATTENDANCE_PRESENT", 1); //Đi - Tính tiền ăn
define("ATTENDANCE_COME_LATE", 2); //Ghi đi, tính tiền ăn.
define("ATTENDANCE_EARLY_LEAVE", 3); //Các trường hợp Ghi đi, nhưng không tính tiền ăn.
define("ATTENDANCE_ABSENCE_NO_REASON", 4); //Ghi nghỉ, nhưng tính tiền ăn.

define("MALE", "male");
define("FEMALE", "female");

define("SYSTEM_USER_ID", 1);

//Định nghĩa một số loại user quản lý của cty NOGA
define("USER_MOD", 2);
define("USER_NOGA_MANAGE_ALL", 4);
define("USER_NOGA_MANAGE_CITY", 5);
define("USER_NOGA_MANAGE_SCHOOL", 6);

//Loại quản lý trong bảng NOGA_MANAGE
define("NOGA_MANAGE_TYPE_SCHOOL", 0);
define("NOGA_MANAGE_TYPE_CITY", 1);

//Các loại hình dịch vụ
define("SERVICE_TYPE_MONTHLY", 1);
define("SERVICE_TYPE_DAILY", 2);
define("SERVICE_TYPE_COUNT_BASED", 3);

define("TUITION_DETAIL_FEE", 0);
define("TUITION_DETAIL_SERVICE", 1);
define("LATE_PICKUP_FEE", 2);

define("PAGING_LIMIT", 30);

define("SESSION_KEY_CLASS_NAME_PREFIX", "ClasseNames_");
define("SESSION_KEY_SEARCH_CHILDREN", "search_children");
define("SESSION_KEY_SEARCH_CHILDREN_STATISTICS", "search_children_statistics");
define("SESSION_KEY_SEARCH_CHILDREN_LEAVE", "search_children_leave");
define("SESSION_KEY_SEARCH_CHILDREN_HEALTH", "search_children_health");
define("SESSION_KEY_SEARCH_CHILDREN_HEALTH_CLASS", "search_children_health_class");
define("SESSION_KEY_SEARCH_CHILDREN_NOGA", "search_children_noga");
define("SESSION_KEY_SEARCH_DIARY", "search_diary");
define("SESSION_KEY_SEARCH_DIARY_CLASS", "search_diary_class");
define("SESSION_USER_CHAT", "search_user_chat");
define("SESSION_KEY_SEARCH_USER", "search_user");

// Hiển thị tên ngắn của page, group NOGA trên App
$nogaPages = array(
    '1' => 'Mang bầu',
    '2' => 'Trên 1 tuổi',
    '3' => 'Kiến thức, giáo dục mầm non'
);

define("WEIGHT_UNAVAILABLE", 0);
define("WEIGHT_GET_WEIGHT", 1);
define("WEIGHT_LOSE_WEIGHT", 2);
define("WEIGHT_NO_CHANGED", 3);
// Mảng tuổi, phục vụ chức năng lọc theo độ tuổi

define("SCHOOL_STEP_FINISH", 100);
define("SCHOOL_STEP_START", 0);

$nogaGroups = array(
    '1' => 'Inet+'
);
// Định nghĩa chuỗi ký tự ngăn cách các gợi ý trong sổ liên lạc
define("BREAK_CHARACTER", '111_222_333_aa');
//Định nghĩa các giá trị của một quyền
define("PERM_NONE", 0);
define("PERM_VIEW", 1);
define("PERM_EDIT", 2);

/**Định nghĩa các giá trị của cấu hình thông báo
 * 1. Chỉ dành cho giáo viên
 * 2. Chỉ dành cho phụ huynh
 * 3. Dành cho cả hai
 */
define("NOTIFY_TEACHER", 1);
define("NOTIFY_PARENT", 2);
define("NOTIFY_FOR_ALL", 3);

/* Định nghĩa các giá trị trạng thái của cấu hình thông báo
0. Không nhận thông báo
1. Chỉ nhận thông báo từ giáo viên
2. Chỉ nhận thông báo từ phụ huynh
3. Nhận thông báo từ cả phụ huynh và giáo viên
*/
define("NOTIFY_NO", 0);
define("NOTIFY_ONLY_TEACHER", 1);
define("NOTIFY_ONLY_PARENT", 2);
define("NOTIFY_ALL", 3);

// Định nghĩa trẻ được edit bởi đối tượng nào
define("EDIT_BY_MANAGE", 1);
define("EDIT_BY_TEACHER", 2);
define("EDIT_BY_PARENT", 3);

/* MEMCACHED - Định nghĩa các key */
// 1. Trường
define("ALL", 'all');
define("SCHOOL_DATA", 'school');
define("SCHOOL_INFO", 'info');
define("SCHOOL_CONFIG", 'config');
/* ADD START - ManhDD 05/04/2021 */
define("SCHOOL_SUBJECTS", 'subjects');
/* ADD END - ManhDD 05/04/2021 */
define("SCHOOL_TEACHERS", 'teachers');
define("SCHOOL_CLASSES", 'classes');
define("SCHOOL_CLASS_LEVELS", 'class_levels');
define("SCHOOL_CHILDREN", 'children');

define("SCHOOL_NOTIFICATIONS", 'notifications');
define("SCHOOL_LATE_PICKUP", 'late_pickup');
define("SCHOOL_SERVICES", 'services');

$school_keys = array(
    SCHOOL_INFO, SCHOOL_CONFIG, SCHOOL_TEACHERS, SCHOOL_CLASSES, SCHOOL_CLASS_LEVELS,
    SCHOOL_CHILDREN, SCHOOL_NOTIFICATIONS, SCHOOL_LATE_PICKUP, SCHOOL_SERVICES
);

// 2. Lớp
define("CLASS_INFO", 'info');
define("CLASS_TEACHERS", 'teachers');
define("CLASS_CHILDREN", 'children');
$class_keys = array(
    CLASS_INFO, CLASS_TEACHERS, CLASS_CHILDREN
);

// 3. Khối
define("CLASS_LEVEL_INFO", 'info');
define("CLASS_LEVEL_CLASSES", 'classes');
$class_level_keys = array(
    CLASS_LEVEL_INFO, CLASS_LEVEL_CLASSES
);

// 3. Giáo viên
define("TEACHER_INFO", 'info');
define("TEACHER_CLASSES", 'classes');
define("TEACHER_SCHOOL", 'schools');
$teacher_keys = array(
    TEACHER_INFO, TEACHER_CLASSES, TEACHER_SCHOOL
);
// 4. Trẻ
define("CHILD_INFO", 'info');
define("CHILD_PARENTS", 'parents');
define("CHILD_SCHOOL", 'school');
define("CHILD_CLASS", 'class');
define("CHILD_PICKER_INFO", 'picker_info');
$child_keys = array(
    CHILD_INFO, CHILD_PARENTS, CHILD_SCHOOL, CHILD_CLASS, CHILD_PICKER_INFO
);
// 5. User manage
define("USER_MANAGE_SCHOOL", 'schools');
define("USER_MANAGE_CLASSES", 'classes');
define("USER_MANAGE_CHILDREN", 'children');
define("USER_MANAGE_CHILDREN_ITSELF", 'itself');
$user_manage_keys = array(
    USER_MANAGE_SCHOOL, USER_MANAGE_CLASSES, USER_MANAGE_CHILDREN, USER_MANAGE_CHILDREN_ITSELF
);

//6. Tổng hợp tương tác trường
$school_statistic_keys = array(
    'page', 'post', 'event', 'schedule', 'menu', 'report', 'tuition',
    'post_created', 'event_created', 'schedule_created', 'menu_created', 'report_created', 'tuition_created'
);

//6. Tổng hợp tương tác từ nhà trường cho NOGA
$noga_statistic_school_keys = array(
    'dashboard', 'attendance', 'service', 'pickup',
    'tuition', 'event', 'schedule', 'menu',
    'report', 'medicine', 'feedback', 'birthday'
);

// Tổng hợp tương tác trường mới - Taila

// tổng hợp tương tác của phụ huynh và trường (NOGA và school view)
$statistic_keys = array(
    'page', 'post', 'school_dashboard', 'attendance', 'service', 'pickup', 'tuition', 'event', 'schedule', 'menu', 'report',
    'medicine', 'feedback', 'diary', 'health', 'birthday', 'picker'
);
define("GROUP_NAME_CONIU", 'coniu');
define("GROUP_NAME_ANDAM", 'andam');
define("GROUP_NAME_DINHDUONG", 'dinhduongthaiky');
define("GROUP_NAME_DAYCON", 'daycondungcach');
define("GROUP_NAME_THAIGIAO", 'thaigiao');
define("GROUP_NAME_HIEMMUON", 'hiemmuon');
define("GROUP_NAME_HONNHAN", 'tamsuhonnhangiadinh');
define("GROUP_NAME_THUCPHAMORGANIC", 'thucphamorganic');
$noga_statistic_topic_keys = array(
    GROUP_NAME_CONIU, GROUP_NAME_ANDAM, GROUP_NAME_DINHDUONG, GROUP_NAME_DAYCON,
    GROUP_NAME_THAIGIAO, GROUP_NAME_HIEMMUON, GROUP_NAME_HONNHAN, GROUP_NAME_THUCPHAMORGANIC
);

// Định nghĩa Action khi set dữ liệu Memcache ( ADD, UPDATE, DELETE)
define("ADD", 'add');
define("UPDATE", 'update');
define("DELETE", 'delete');

/* END - Memcached */


/* Định nghĩa các action khi chạy background Firebase */
define("CREATE_USER", 'create_user');
define("DELETE_USER", 'delete_user');
define("UPDATE_USER", 'update_user');
define("ADD_FRIEND", 'add_friend');
define("REMOVE_FRIEND", 'remove_friend');
define("DELETE_AFTER_SENDING", 1);
/* END - Background Firebase */

/* BEGIN - Định nghĩa các category của hội nhóm */
define("CATEGORY_THAIKY", 3);
define("CATEGORY_CHAMCON", 4);
define("CATEGORY_DAYCON", 5);
define("CATEGORY_GAIDINH", 6);
/* END - Category group */

/* Định nghĩa các kiểu của quá trình phát triển thai nhi*/
define("PREGNANCY_CHECK", 1);
define("FOETUS_INFORMATION", 2);

/* Định nghĩa các kiểu của quá trình phát triển trẻ*/
define("VACCINATION", 1);
define("CHILD_INFORMATION", 2);

// Định nghĩa các cấp của hệ thống

$grades = array(
    '0' => 'Pre - school',
    '1' => 'Tiểu học',
    '2' => 'Trung học cơ sở',
    '99' => 'Liên cấp',
);

//Định nghĩa các module của hệ thống
$systemModules = array(
    array('module' => 'events', 'module_name' => 'Notification - Event'),
    array('module' => 'attendance', 'module_name' => 'Attendance'),
    array('module' => 'birthdays', 'module_name' => 'Birthday'),
    array('module' => 'classes', 'module_name' => 'Class'),
    array('module' => 'feedback', 'module_name' => 'Feedback'),
    array('module' => 'tuitions', 'module_name' => 'Tuition'),
    array('module' => 'medicines', 'module_name' => 'Medicines'),
    array('module' => 'pickup', 'module_name' => 'Late PickUp'),
    array('module' => 'reports', 'module_name' => 'Contact book'),
    array('module' => 'schedules', 'module_name' => 'Schedule'),
    array('module' => 'services', 'module_name' => 'Service'),
    array('module' => 'teachers', 'module_name' => 'Teacher - Employee'),
    array('module' => 'children', 'module_name' => 'Children'),
    array('module' => 'roles', 'module_name' => 'Permission'),
    array('module' => 'settings', 'module_name' => 'Settings'),
    array('module' => 'classlevels', 'module_name' => 'Class level'),
    array('module' => 'menus', 'module_name' => 'Menu'),
    array('module' => 'diarys', 'module_name' => 'Diary corner'),
    array('module' => 'healths', 'module_name' => 'Health information'),
    array('module' => 'managegroups', 'module_name' => 'Manage groups'),
    array('module' => 'loginstatistics', 'module_name' => 'Login statistics'),
    array('module' => 'points', 'module_name' => 'Points'),
    //ADD START MANHDD 14/04/2021
    array('module' => 'subjects', 'module_name' => 'Subjects'),
    //ADD END MANHDD 14/04/2021
    //ADD START MANHDD 02/06/2021
    array('module' => 'conducts', 'module_name' => 'Conducts'),
    //ADD END MANHDD 02/06/2021
    //ADD START MANHDD 17/06/2021
    array('module' => 'courses', 'module_name' => 'Courses'),
    //ADD END MANHDD 17/06/2021

);

// Định nghĩa các module cấu hình thông báo
$notifyModules = array(
    array('module' => 'events', 'module_name' => 'Notification - Event', 'module_type' => NOTIFY_FOR_ALL),
    array('module' => 'attendance', 'module_name' => 'Attendance', 'module_type' => NOTIFY_FOR_ALL),
    array('module' => 'feedback', 'module_name' => 'Feedback', 'module_type' => NOTIFY_PARENT),
    array('module' => 'tuitions', 'module_name' => 'Tuition', 'module_type' => NOTIFY_PARENT),
    array('module' => 'medicines', 'module_name' => 'Medicines', 'module_type' => NOTIFY_FOR_ALL),
    array('module' => 'pickup', 'module_name' => 'Late PickUp', 'module_type' => NOTIFY_FOR_ALL),
    array('module' => 'reports', 'module_name' => 'Contact book', 'module_type' => NOTIFY_TEACHER),
    array('module' => 'services', 'module_name' => 'Service', 'module_type' => NOTIFY_FOR_ALL),
    array('module' => 'children', 'module_name' => 'Children', 'module_type' => NOTIFY_FOR_ALL),
);

// Năm học hiện tại
$school_year = '2021-2022';

// Định nghĩa các học kỳ
$semesters = array(
    '1' => 'Học kỳ 1',
    '2' => 'Học kỳ 2'
);

// Các năm học mặc định
$default_school_year = array(
    '0' => '2015-2016',
    '1' => '2016-2017',
    '2' => '2017-2018',
    '3' => '2018-2019',
    '4' => '2019-2020',
    '5' => '2020-2021',
    '6' => '2021-2022',
    '7' => '2022-2023',
    '8' => '2023-2024',
    '9' => '2024-2025',
);

// Định nghĩa các môn học của các khối
$default_subjects = array(
    '1' => array(
        '1' => array(
            '0' => 'Toán',
            '1' => 'Tiếng Việt',
            '2' => 'Tiếng Anh',
            '3' => 'Tin học',
            '4' => 'Đạo đức',
            '5' => 'TN-XH',
            '6' => 'Âm nhạc',
            '7' => 'Mĩ thuật',
            '8' => 'Thủ công',
            '9' => 'Thể dục',
            // ...
        ),
        '2' => array(
            '0' => 'Toán',
            '1' => 'Tiếng Việt',
            '2' => 'Tiếng Anh',
            '3' => 'Tin học',
            '4' => 'Đạo đức',
            '5' => 'TN-XH',
            '6' => 'Âm nhạc',
            '7' => 'Mĩ thuật',
            '8' => 'Thủ công',
            '9' => 'Thể dục',
            // ...
        ),
        '3' => array(
            '0' => 'Toán',
            '1' => 'Tiếng Việt',
            '2' => 'Tiếng Anh',
            '3' => 'Tin học',
            '4' => 'Đạo đức',
            '5' => 'TN-XH',
            '6' => 'Âm nhạc',
            '7' => 'Mĩ thuật',
            '8' => 'Thủ công',
            '9' => 'Thể dục',
            // ...
        ),
        '4' => array(
            '0' => 'Toán',
            '1' => 'Tiếng Việt',
            '2' => 'Khoa học',
            '3' => 'Lịch sử và Địa lí',
            '4' => 'Tiếng Anh',
            '5' => 'Tin học',
            '6' => 'Đạo đức',
            '7' => 'Âm nhạc',
            '8' => 'Mĩ thuật',
            '9' => 'Kĩ thuật',
            '10' => 'Thể dục',
            // ...
        ),
        '5' => array(
            '0' => 'Toán',
            '1' => 'Tiếng Việt',
            '2' => 'Khoa học',
            '3' => 'Lịch sử và Địa lí',
            '4' => 'Tiếng Anh',
            '5' => 'Tin học',
            '6' => 'Đạo đức',
            '7' => 'Âm nhạc',
            '8' => 'Mĩ thuật',
            '9' => 'Kĩ thuật',
            '10' => 'Thể dục',
            // ...
        ),
    ),
    '2' => array(
        /* DELETE START - ManhDD 01/04/2021 */
//        '6' => array(
//            '0' => 'Toán học',
//            '1' => 'Vật lí',
//            '2' => 'Hóa học',
//            '3' => 'Sinh học',
//            '4' => 'Tin học',
//            '5' => 'Ngữ văn',
//            '6' => 'Lịch sử',
//            '7' => 'Địa lí',
//            '8' => 'Ngoại ngữ',
//            '9' => 'GDCD',
//            '10' => 'Công nghệ',
//            '11' => 'Âm nhạc',
//            '12' => 'Mĩ thuật',
//            '13' => 'Thể dục',
//            // ...
//        ),
        /* DELETE END - ManhDD 01/04/2021 */
        '7' => array(
            '0' => 'Toán học',
            '1' => 'Vật lí',
            '2' => 'Hóa học',
            '3' => 'Sinh học',
            '4' => 'Tin học',
            '5' => 'Ngữ văn',
            '6' => 'Lịch sử',
            '7' => 'Địa lí',
            '8' => 'Ngoại ngữ',
            '9' => 'GDCD',
            '10' => 'Công nghệ',
            '11' => 'Âm nhạc',
            '12' => 'Mĩ thuật'
            /* DELETE START - ManhDD 01/04/2021 */
//        ,
//            '13' => 'Thể dục'
            /* DELETE END - ManhDD 01/04/2021 */
        ),
        '8' => array(
            '0' => 'Toán học',
            '1' => 'Vật lí',
            '2' => 'Hóa học',
            '3' => 'Sinh học',
            '4' => 'Tin học',
            '5' => 'Ngữ văn',
            '6' => 'Lịch sử',
            '7' => 'Địa lí',
            '8' => 'Ngoại ngữ',
            '9' => 'GDCD',
            '10' => 'Công nghệ',
            '11' => 'Âm nhạc',
            '12' => 'Mĩ thuật'
            /* DELETE START - ManhDD 01/04/2021 */
//        ,
//            '13' => 'Thể dục'
            /* DELETE END - ManhDD 01/04/2021 */
            // ...
        ),
        '9' => array(
            '0' => 'Toán học',
            '1' => 'Vật lí',
            '2' => 'Hóa học',
            '3' => 'Sinh học',
            '4' => 'Tin học',
            '5' => 'Ngữ văn',
            '6' => 'Lịch sử',
            '7' => 'Địa lí',
            '8' => 'Ngoại ngữ',
            '9' => 'GDCD',
            '10' => 'Công nghệ',
            '11' => 'Âm nhạc'
            /* DELETE START - ManhDD 01/04/2021 */
//        ,
//            '12' => 'Mĩ thuật',
//            '13' => 'Thể dục'
            /* DELETE END - ManhDD 01/04/2021 */
        ),
        /* ADD START - ManhDD 01/04/2021 */
        '10' => array(
            '0' => 'Toán học',
            '1' => 'Vật lí',
            '2' => 'Hóa học',
            '3' => 'Sinh học',
            '4' => 'Tin học',
            '5' => 'Ngữ văn',
            '6' => 'Lịch sử',
            '7' => 'Địa lí',
            '8' => 'Ngoại ngữ',
            '9' => 'GDCD',
            '10' => 'Công nghệ',
            '11' => 'Âm nhạc'
            // ...
        ),
        '11' => array(
            '0' => 'Toán học',
            '1' => 'Vật lí',
            '2' => 'Hóa học',
            '3' => 'Sinh học',
            '4' => 'Tin học',
            '5' => 'Ngữ văn',
            '6' => 'Lịch sử',
            '7' => 'Địa lí',
            '8' => 'Ngoại ngữ',
            '9' => 'GDCD',
            '10' => 'Công nghệ',
            '11' => 'Âm nhạc',
            '12' => 'Mĩ thuật'
            // ...
        ),
        '12' => array(
            '0' => 'Toán học',
            '1' => 'Vật lí',
            '2' => 'Hóa học',
            '3' => 'Sinh học',
            '4' => 'Tin học',
            '5' => 'Ngữ văn',
            '6' => 'Lịch sử',
            '7' => 'Địa lí',
            '8' => 'Ngoại ngữ',
            '9' => 'GDCD',
            '10' => 'Công nghệ'
            // ...
        ),
        /* ADD END - ManhDD 01/04/2021 */
    )
);

// json chiều cao chuẩn
$hightStandardJsonMale = '[{
    highest = "53.7";
    lowest = "46.1";
    month = 0;
    standard = "49.9";
},
{
    highest = "58.6";
    lowest = "50.8";
    month = 1;
    standard = "54.7";
},
{
    highest = "62.4";
    lowest = "54.4";
    month = 2;
    standard = "58.4";
},
{
    highest = "65.5";
    lowest = "57.3";
    month = 3;
    standard = "61.4";
},
{
    highest = 68;
    lowest = "59.7";
    month = 4;
    standard = "63.9";
},
{
    highest = "70.1";
    lowest = "61.7";
    month = 5;
    standard = "65.9";
},
{
    highest = "71.9";
    lowest = "63.3";
    month = 6;
    standard = "67.6";
},
{
    highest = "73.5";
    lowest = "64.8";
    month = 7;
    standard = "69.2";
},
{
    highest = 75;
    lowest = "66.2";
    month = 8;
    standard = "70.6";
},
{
    highest = "76.5";
    lowest = "67.5";
    month = 9;
    standard = 72;
},
{
    highest = "77.9";
    lowest = "68.7";
    month = 10;
    standard = "73.3";
},
{
    highest = "79.2";
    lowest = "69.9";
    month = 11;
    standard = "74.5";
},
{
    highest = "80.5";
    lowest = 71;
    month = 12;
    standard = "75.7";
},
{
    highest = "81.8";
    lowest = "72.1";
    month = 13;
    standard = "76.9";
},
{
    highest = 83;
    lowest = "73.1";
    month = 14;
    standard = 78;
},
{
    highest = "84.2";
    lowest = "74.1";
    month = 15;
    standard = "79.1";
},
{
    highest = "85.4";
    lowest = 75;
    month = 16;
    standard = "80.2";
},
{
    highest = "86.5";
    lowest = 76;
    month = 17;
    standard = "81.2";
},
{
    highest = "87.7";
    lowest = "76.9";
    month = 18;
    standard = "82.3";
},
{
    highest = "88.8";
    lowest = "77.7";
    month = 19;
    standard = "83.2";
},
{
    highest = "89.8";
    lowest = "78.6";
    month = 20;
    standard = "84.2";
},
{
    highest = "90.9";
    lowest = "79.4";
    month = 21;
    standard = "85.1";
},
{
    highest = "91.9";
    lowest = "80.2";
    month = 22;
    standard = 86;
},
{
    highest = "92.9";
    lowest = 81;
    month = 23;
    standard = "86.9";
},
{
    highest = "93.9";
    lowest = "81.7";
    month = 24;
    standard = "93.9";
},
{
    highest = "94.2";
    lowest = "81.7";
    month = 25;
    standard = 88;
},
{
    highest = "95.2";
    lowest = "82.5";
    month = 26;
    standard = "88.8";
},
{
    highest = "96.1";
    lowest = "83.1";
    month = 27;
    standard = "89.6";
},
{
    highest = 97;
    lowest = "83.8";
    month = 28;
    standard = "90.4";
},
{
    highest = "97.9";
    lowest = "84.5";
    month = 29;
    standard = "91.2";
},
{
    highest = "98.7";
    lowest = "85.1";
    month = 30;
    standard = "91.9";
},
{
    highest = "99.6";
    lowest = "85.7";
    month = 31;
    standard = "92.7";
},
{
    highest = "100.4";
    lowest = "86.4";
    month = 32;
    standard = "93.4";
},
{
    highest = "101.2";
    lowest = "86.9";
    month = 33;
    standard = "94.1";
},
{
    highest = 102;
    lowest = "87.5";
    month = 34;
    standard = "94.8";
},
{
    highest = "102.7";
    lowest = "88.1";
    month = 35;
    standard = "95.4";
},
{
    highest = "103.5";
    lowest = "88.7";
    month = 36;
    standard = "96.1";
},
{
    highest = "104.2";
    lowest = "89.2";
    month = 37;
    standard = "96.7";
},
{
    highest = 105;
    lowest = "89.8";
    month = 38;
    standard = "97.4";
},
{
    highest = "105.7";
    lowest = "90.3";
    month = 39;
    standard = 98;
},
{
    highest = "106.4";
    lowest = "90.9";
    month = 40;
    standard = "98.6";
},
{
    highest = "107.1";
    lowest = "91.4";
    month = 41;
    standard = "99.2";
},
{
    highest = "107.8";
    lowest = "91.9";
    month = 42;
    standard = "99.9";
},
{
    highest = "108.5";
    lowest = "92.4";
    month = 43;
    standard = "100.4";
},
{
    highest = "109.1";
    lowest = 93;
    month = 44;
    standard = 101;
},
{
    highest = "109.8";
    lowest = "93.5";
    month = 45;
    standard = "109.8";
},
{
    highest = "110.4";
    lowest = 94;
    month = 46;
    standard = "102.2";
},
{
    highest = "111.1";
    lowest = "94.4";
    month = 47;
    standard = "102.8";
},
{
    highest = "111.7";
    lowest = "94.9";
    month = 48;
    standard = "103.3";
},
{
    highest = "112.4";
    lowest = "95.4";
    month = 49;
    standard = "103.9";
},
{
    highest = 113;
    lowest = "95.9";
    month = 50;
    standard = "104.4";
},
{
    highest = "113.6";
    lowest = "96.4";
    month = 51;
    standard = 105;
},
{
    highest = "114.2";
    lowest = "96.9";
    month = 52;
    standard = "105.6";
},
{
    highest = "114.9";
    lowest = "97.4";
    month = 53;
    standard = "106.1";
},
{
    highest = "115.5";
    lowest = "97.8";
    month = 54;
    standard = "106.7";
},
{
    highest = "116.1";
    lowest = "98.3";
    month = 55;
    standard = "107.2";
},
{
    highest = "116.7";
    lowest = "98.8";
    month = 56;
    standard = "107.8";
},
{
    highest = "117.4";
    lowest = "99.3";
    month = 57;
    standard = "108.3";
},
{
    highest = 118;
    lowest = "99.7";
    month = 58;
    standard = "108.9";
},
{
    highest = "118.6";
    lowest = "100.2";
    month = 59;
    standard = "109.4"
},
{
    highest = "119.2";
    lowest = "100.7";
    month = 60;
    standard = 110;
}]';

$hightStandardJsonFeMale = '[{
    highest = "52.9";
    lowest = "45.4";
    month = 0;
    standard = "49.1";
},
{
    highest = "57.6";
    lowest = "49.8";
    month = 1;
    standard = "53.7";
},
{
    highest = "61.1";
    lowest = 53;
    month = 2;
    standard = "57.1";
},
{
    highest = 64;
    lowest = "55.6";
    month = 3;
    standard = "59.8";
},
{
    highest = "66.4";
    lowest = "57.8";
    month = 4;
    standard = "62.1";
},
{
    highest = "68.5";
    lowest = "59.6";
    month = 5;
    standard = 64;
},
{
    highest = "70.3";
    lowest = "61.2";
    month = 6;
    standard = "65.7";
},
{
    highest = "71.9";
    lowest = "62.7";
    month = 7;
    standard = "67.3";
},
{
    highest = "73.5";
    lowest = "64.0";
    month = 8;
    standard = "68.7";
},
{
    highest = 75;
    lowest = "65.3";
    month = 9;
    standard = "70.1";
},
{
    highest = "76.4";
    lowest = "66.5";
    month = 10;
    standard = "71.5";
},
{
    highest = "77.8";
    lowest = "67.7";
    month = 11;
    standard = "72.8";
},
{
    highest = "79.2";
    lowest = "68.9";
    month = 12;
    standard = 74;
},
{
    highest = "80.5";
    lowest = 70;
    month = 13;
    standard = "75.2";
},
{
    highest = "81.7";
    lowest = 71;
    month = 14;
    standard = "76.4";
},
{
    highest = 83;
    lowest = 72;
    month = 15;
    standard = "77.5";
},
{
    highest = "84.2";
    lowest = 73;
    month = 16;
    standard = "78.6";
},
{
    highest = "85.4";
    lowest = 74;
    month = 17;
    standard = "79.7";
},
{
    highest = "86.5";
    lowest = "74.9";
    month = 18;
    standard = "80.7";
},
{
    highest = "87.6";
    lowest = "75.8";
    month = 19;
    standard = "81.7";
},
{
    highest = "88.7";
    lowest = "76.7";
    month = 20;
    standard = "82.7";
},
{
    highest = "89.8";
    lowest = "77.5";
    month = 21;
    standard = "83.7";
},
{
    highest = "90.8";
    lowest = "78.4";
    month = 22;
    standard = "84.6";
},
{
    highest = "91.9";
    lowest = "79.2";
    month = 23;
    standard = "85.5";
},
{
    highest = "92.9";
    lowest = 80;
    month = 24;
    standard = "86.4";
},
{
    highest = "93.1";
    lowest = 80;
    month = 25;
    standard = "86.6";
},
{
    highest = "94.1";
    lowest = "80.8";
    month = 26;
    standard = "87.4";
},
{
    highest = 95;
    lowest = "81.5";
    month = 27;
    standard = "88.3";
},
{
    highest = 96;
    lowest = "82.2";
    month = 28;
    standard = "89.1";
},
{
    highest = "96.9";
    lowest = "82.9";
    month = 29;
    standard = "89.9";
},
{
    highest = "97.7";
    lowest = "83.6";
    month = 30;
    standard = "90.7";
},
{
    highest = "98.6";
    lowest = "84.3";
    month = 31;
    standard = "91.4";
},
{
    highest = "99.4";
    lowest = "84.9";
    month = 32;
    standard = "92.2";
},
{
    highest = "100.3";
    lowest = "85.6";
    month = 33;
    standard = "92.9";
},
{
    highest = "101.1";
    lowest = "86.2";
    month = 34;
    standard = "93.6";
},
{
    highest = "101.9";
    lowest = "86.8";
    month = 35;
    standard = "94.4";
},
{
    highest = "102.7";
    lowest = "87.4";
    month = 36;
    standard = "95.1";
},
{
    highest = "103.4";
    lowest = 88;
    month = 37;
    standard = "95.7";
},
{
    highest = "104.2";
    lowest = "88.6";
    month = 38;
    standard = "96.4";
},
{
    highest = 105;
    lowest = "89.2";
    month = 39;
    standard = "97.1";
},
{
    highest = "105.7";
    lowest = "89.8";
    month = 40;
    standard = "97.7";
},
{
    highest = "106.4";
    lowest = "90.4";
    month = 41;
    standard = "98.4";
},
{
    highest = "107.2";
    lowest = "90.9";
    month = 42;
    standard = 99;
},
{
    highest = "107.9";
    lowest = "91.5";
    month = 43;
    standard = "99.7";
},
{
    highest = "108.6";
    lowest = 92;
    month = 44;
    standard = "100.3";
},
{
    highest = "109.3";
    lowest = "92.5";
    month = 45;
    standard = "100.9";
},
{
    highest = 110;
    lowest = "93.1";
    month = 46;
    standard = "101.5";
},
{
    highest = "110.7";
    lowest = "93.6";
    month = 47;
    standard = "102.1";
},
{
    highest = "111.3";
    lowest = "94.1";
    month = 48;
    standard = "102.7";
},
{
    highest = 112;
    lowest = "94.6";
    month = 49;
    standard = "103.3";
},
{
    highest = "112.7";
    lowest = "95.1";
    month = 50;
    standard = "103.9";
},
{
    highest = "113.3";
    lowest = "95.6";
    month = 51;
    standard = "104.5";
},
{
    highest = 114;
    lowest = "96.1";
    month = 52;
    standard = 105;
},
{
    highest = "114.6";
    lowest = "96.6";
    month = 53;
    standard = "105.6";
},
{
    highest = "115.2";
    lowest = "97.1";
    month = 54;
    standard = "106.2";
},
{
    highest = "115.9";
    lowest = "97.6";
    month = 55;
    standard = "106.7";
},
{
    highest = "116.5";
    lowest = "98.1";
    month = 56;
    standard = "107.3";
},
{
    highest = "117.1";
    lowest = "98.5";
    month = 57;
    standard = "107.8";
},
{
    highest = "117.7";
    lowest = 99;
    month = 58;
    standard = "108.4";
},
{
    highest = "118.3";
    lowest = "99.5";
    month = 59;
    standard = "108.9";
},
{
    highest = "118.9";
    lowest = "99.9";
    month = 60;
    standard = "109.4";
}]';

$weightStandardJsonMale = '[{
    highest = "4.4";
    lowest = "2.5";
    month = 0;
    standard = "3.3";
},
{
    highest = "5.8";
    lowest = "3.4";
    month = 1;
    standard = "4.5";
},
{
    highest = "7.1";
    lowest = "4.3";
    month = 2;
    standard = "5.6";
},
{
    highest = 8;
    lowest = 5;
    month = 3;
    standard = "6.4";
},
{
    highest = "8.7";
    lowest = "5.6";
    month = 4;
    standard = "7.0";
},
{
    highest = "9.3";
    lowest = 6;
    month = 5;
    standard = "7.5";
},
{
    highest = "9.8";
    lowest = "6.4";
    month = 6;
    standard = "7.9";
},
{
    highest = "10.3";
    lowest = "6.7";
    month = 7;
    standard = "8.3";
},
{
    highest = "10.7";
    lowest = "6.9";
    month = 8;
    standard = "8.6";
},
{
    highest = 11;
    lowest = "7.1";
    month = 9;
    standard = "8.9";
},
{
    highest = "11.4";
    lowest = "7.4";
    month = 10;
    standard = "9.2";
},
{
    highest = "11.7";
    lowest = "7.6";
    month = 11;
    standard = "9.4";
},
{
    highest = 12;
    lowest = "7.7";
    month = 12;
    standard = "9.6";
},
{
    highest = "12.3";
    lowest = "7.9";
    month = 13;
    standard = "9.9";
},
{
    highest = "12.6";
    lowest = "8.1";
    month = 14;
    standard = "10.1";
},
{
    highest = "12.8";
    lowest = "8.3";
    month = 15;
    standard = "10.3";
},
{
    highest = "13.1";
    lowest = "8.4";
    month = 16;
    standard = "10.5";
},
{
    highest = "13.4";
    lowest = "8.6";
    month = 17;
    standard = "10.7";
},
{
    highest = "13.7";
    lowest = "8.8";
    month = 18;
    standard = "10.9";
},
{
    highest = "13.9";
    lowest = "8.9";
    month = 19;
    standard = "11.1";
},
{
    highest = "14.2";
    lowest = "9.1";
    month = 20;
    standard = "11.3";
},
{
    highest = "14.5";
    lowest = "9.2";
    month = 21;
    standard = "11.5";
},
{
    highest = "14.7";
    lowest = "9.4";
    month = 22;
    standard = "11.8";
},
{
    highest = "14.7";
    lowest = "9.5";
    month = 23;
    standard = "11.8";
},
{
    highest = "15.3";
    lowest = "9.7";
    month = 24;
    standard = "12.2";
},
{
    highest = "15.5";
    lowest = "9.8";
    month = 25;
    standard = "12.4";
},
{
    highest = "15.8";
    lowest = 10;
    month = 26;
    standard = "12.5";
},
{
    highest = "16.1";
    lowest = "10.1";
    month = 27;
    standard = "12.7";
},
{
    highest = "16.3";
    lowest = "10.2";
    month = 28;
    standard = "12.9";
},
{
    highest = "16.6";
    lowest = "10.4";
    month = 29;
    standard = "13.1";
},
{
    highest = "16.9";
    lowest = "10.5";
    month = 30;
    standard = "13.3";
},
{
    highest = "17.1";
    lowest = "10.7";
    month = 31;
    standard = "13.5";
},
{
    highest = "17.4";
    lowest = "10.8";
    month = 32;
    standard = "13.7";
},
{
    highest = "17.6";
    lowest = "10.9";
    month = 33;
    standard = "13.8";
},
{
    highest = "17.8";
    lowest = 11;
    month = 34;
    standard = 14;
},
{
    highest = "18.1";
    lowest = "11.2";
    month = 35;
    standard = "14.2";
},
{
    highest = "18.3";
    lowest = "11.3";
    month = 36;
    standard = "14.3";
},
{
    highest = "18.6";
    lowest = "11.4";
    month = 37;
    standard = "14.5";
},
{
    highest = "18.8";
    lowest = "11.5";
    month = 38;
    standard = "14.7";
},
{
    highest = 19;
    lowest = "11.6";
    month = 39;
    standard = "14.8";
},
{
    highest = "19.3";
    lowest = "11.8";
    month = 40;
    standard = 15;
},
{
    highest = "19.5";
    lowest = "11.9";
    month = 41;
    standard = "15.2";
},
{
    highest = "19.7";
    lowest = 12;
    month = 42;
    standard = "15.3";
},
{
    highest = 20;
    lowest = "12.1";
    month = 43;
    standard = "15.5";
},
{
    highest = "20.2";
    lowest = "12.2";
    month = 44;
    standard = "15.7";
},
{
    highest = "20.5";
    lowest = "12.4";
    month = 45;
    standard = "15.8";
},
{
    highest = "20.7";
    lowest = "12.5";
    month = 46;
    standard = 16;
},
{
    highest = "20.9";
    lowest = "12.6";
    month = 47;
    standard = "16.2";
},
{
    highest = "21.2";
    lowest = "12.7";
    month = 48;
    standard = "16.3";
},
{
    highest = "21.4";
    lowest = "12.8";
    month = 49;
    standard = "16.5";
},
{
    highest = "21.7";
    lowest = "14.7";
    month = 50;
    standard = "16.7";
},
{
    highest = "21.9";
    lowest = "13.1";
    month = 51;
    standard = "16.8";
},
{
    highest = "22.2";
    lowest = "13.2";
    month = 52;
    standard = 17;
},
{
    highest = "22.4";
    lowest = "13.3";
    month = 53;
    standard = "17.2";
},
{
    highest = "22.7";
    lowest = "13.4";
    month = 54;
    standard = "17.3";
},
{
    highest = "22.9";
    lowest = "13.5";
    month = 55;
    standard = "17.5";
},
{
    highest = "23.2";
    lowest = "13.6";
    month = 56;
    standard = "17.7";
},
{
    highest = "23.4";
    lowest = "13.7";
    month = 57;
    standard = "17.8";
},
{
    highest = "23.7";
    lowest = "13.8";
    month = 58;
    standard = 18;
},
{
    highest = "23.9";
    lowest = 14;
    month = 59;
    standard = "18.2";
},
{
    highest = "24.2";
    lowest = "14.1";
    month = 60;
    standard = "18.3";
}]';

$weightStandardJsonFeMale = '[{
    highest = "4.2";
    lowest = "2.4";
    month = 0;
    standard = "3.2";
},
{
    highest = "5.5";
    lowest = "3.2";
    month = 1;
    standard = "4.2";
},
{
    highest = "6.6";
    lowest = "3.9";
    month = 2;
    standard = "5.1";
},
{
    highest = "7.5";
    lowest = "4.5";
    month = 3;
    standard = "5.8";
},
{
    highest = "8.2";
    lowest = "5.0";
    month = 4;
    standard = "6.4";
},
{
    highest = "8.8";
    lowest = "5.4";
    month = 5;
    standard = "6.9";
},
{
    highest = "9.3";
    lowest = "5.7";
    month = 6;
    standard = "7.3";
},
{
    highest = "9.8";
    lowest = "6.0";
    month = 7;
    standard = "7.6";
},
{
    highest = "10.2";
    lowest = "6.3";
    month = 8;
    standard = "7.9";
},
{
    highest = "10.5";
    lowest = "6.5";
    month = 9;
    standard = "8.2";
},
{
    highest = "10.9";
    lowest = "6.7";
    month = 10;
    standard = "8.5";
},
{
    highest = "11.2";
    lowest = "6.9";
    month = 11;
    standard = "8.7";
},
{
    highest = "11.5";
    lowest = "7.0";
    month = 12;
    standard = "8.9";
},
{
    highest = "11.8";
    lowest = "7.2";
    month = 13;
    standard = "9.2";
},
{
    highest = "11.8";
    lowest = "7.4";
    month = 14;
    standard = "9.4";
},
{
    highest = "12.4";
    lowest = "7.6";
    month = 15;
    standard = "9.6";
},
{
    highest = "12.6";
    lowest = "7.7";
    month = 16;
    standard = "9.8";
},
{
    highest = "12.9";
    lowest = "7.9";
    month = 17;
    standard = 10;
},
{
    highest = "13.2";
    lowest = "8.1";
    month = 18;
    standard = "10.2";
},
{
    highest = "13.5";
    lowest = "8.2";
    month = 19;
    standard = "10.4";
},
{
    highest = "13.7";
    lowest = "8.4";
    month = 20;
    standard = "10.6";
},
{
    highest = 14;
    lowest = "8.6";
    month = 21;
    standard = "10.9";
},
{
    highest = "14.3";
    lowest = "8.7";
    month = 22;
    standard = "11.1";
},
{
    highest = "14.6";
    lowest = "8.9";
    month = 23;
    standard = "11.3";
},
{
    highest = "14.8";
    lowest = 9;
    month = 24;
    standard = "11.5";
},
{
    highest = "15.1";
    lowest = "9.2";
    month = 25;
    standard = "11.7";
},
{
    highest = "15.4";
    lowest = "9.4";
    month = 26;
    standard = "11.9";
},
{
    highest = "15.7";
    lowest = "9.5";
    month = 27;
    standard = "12.1";
},
{
    highest = 16;
    lowest = "9.7";
    month = 28;
    standard = "12.3";
},
{
    highest = "16.2";
    lowest = "9.8";
    month = 29;
    standard = "12.5";
},
{
    highest = "16.5";
    lowest = 10;
    month = 30;
    standard = "12.7";
},
{
    highest = "16.8";
    lowest = "10.1";
    month = 31;
    standard = "12.9";
},
{
    highest = "17.1";
    lowest = "10.3";
    month = 32;
    standard = "13.1";
},
{
    highest = "17.3";
    lowest = "10.4";
    month = 33;
    standard = "13.3";
},
{
    highest = "17.6";
    lowest = "10.5";
    month = 34;
    standard = "13.5";
},
{
    highest = "17.9";
    lowest = "10.7";
    month = 35;
    standard = "13.7";
},
{
    highest = "18.1";
    lowest = "10.8";
    month = 36;
    standard = "13.9";
},
{
    highest = "18.4";
    lowest = "10.9";
    month = 37;
    standard = 14;
},
{
    highest = "18.7";
    lowest = "11.1";
    month = 38;
    standard = "14.2";
},
{
    highest = 19;
    lowest = "11.2";
    month = 39;
    standard = "14.4";
},
{
    highest = "19.2";
    lowest = "11.3";
    month = 40;
    standard = "14.6";
},
{
    highest = "19.5";
    lowest = "11.5";
    month = 41;
    standard = "14.8";
},
{
    highest = "19.8";
    lowest = "11.6";
    month = 42;
    standard = 15;
},
{
    highest = "20.1";
    lowest = "11.7";
    month = 43;
    standard = "15.2";
},
{
    highest = "20.4";
    lowest = "11.8";
    month = 44;
    standard = "15.3";
},
{
    highest = "20.7";
    lowest = 12;
    month = 45;
    standard = "15.5";
},
{
    highest = "20.9";
    lowest = "12.1";
    month = 46;
    standard = "15.7";
},
{
    highest = "21.2";
    lowest = "12.2";
    month = 47;
    standard = "15.9";
},
{
    highest = "21.5";
    lowest = "12.3";
    month = 48;
    standard = "16.1";
},
{
    highest = "21.8";
    lowest = "12.4";
    month = 49;
    standard = "16.3";
},
{
    highest = "22.1";
    lowest = "12.6";
    month = 50;
    standard = "16.4";
},
{
    highest = "22.4";
    lowest = "12.7";
    month = 51;
    standard = "16.6";
},
{
    highest = "22.6";
    lowest = "12.8";
    month = 52;
    standard = "16.8";
},
{
    highest = "22.9";
    lowest = "12.9";
    month = 53;
    standard = 17;
},
{
    highest = "23.2";
    lowest = 13;
    month = 54;
    standard = "17.2";
},
{
    highest = "23.5";
    lowest = "13.2";
    month = 55;
    standard = "17.3";
},
{
    highest = "23.8";
    lowest = "13.3";
    month = 56;
    standard = "17.5";
},
{
    highest = "24.1";
    lowest = "13.4";
    month = 57;
    standard = "17.7";
},
{
    highest = "24.4";
    lowest = "13.5";
    month = 58;
    standard = "17.9";
},
{
    highest = "24.6";
    lowest = "13.6";
    month = 59;
    standard = 18;
},
{
    highest = "24.9";
    lowest = "13.7";
    month = 60;
    standard = "18.2";
}]';

// Mảng lưu chuẩn thai nhi
$foetusStandard = '[{
    height = "16";
    week = 8;
    weight = 1;
},
{
    height = "23";
    week = 9;
    weight = 2;
},
{
    height = "31";
    week = 10;
    weight = 4;
},
{
    height = "41";
    week = 11;
    weight = 7;
},
{
    height = "54";
    week = 12;
    weight = 14;
},
{
    height = "74";
    week = 13;
    weight = 23;
},
{
    height = "87";
    week = 14;
    weight = 43;
},
{
    height = "101";
    week = 15;
    weight = 70;
},
{
    height = "116";
    week = 16;
    weight = 100;
},
{
    height = 130;
    week = 17;
    weight = 140;
},
{
    height = "142";
    week = 18;
    weight = 190;
},
{
    height = "153";
    week = 19;
    weight = 240;
},
{
    height = "164";
    week = 20;
    weight = 300;
},
{
    height = "267";
    week = 21;
    weight = 360;
},
{
    height = "278";
    week = 22;
    weight = 430;
},
{
    height = "289";
    week = 23;
    weight = 500;
},
{
    height = 300;
    week = 24;
    weight = 600;
},
{
    height = "346";
    week = 25;
    weight = 660;
},
{
    height = "356";
    week = 26;
    weight = 760;
},
{
    height = "366";
    week = 27;
    weight = 875;
},
{
    height = "376";
    week = 28;
    weight = 1005;
},
{
    height = "386";
    week = 29;
    weight = 1150;
},
{
    height = "399";
    week = 30;
    weight = 1320;
},
{
    height = "411";
    week = 31;
    weight = 1500;
},
{
    height = "424";
    week = 32;
    weight = 1700;
},
{
    height = "437";
    week = 33;
    weight = 1920;
},
{
    height = 450;
    week = 34;
    weight = 2150;
},
{
    height = "462";
    week = 35;
    weight = 2380;
},
{
    height = "474";
    week = 36;
    weight = 2620;
},
{
    height = "486";
    week = 37;
    weight = 2860;
},
{
    height = "498";
    week = 38;
    weight = 3080;
},
{
    height = "507";
    week = 39;
    weight = 3290;
},
{
    height = "512";
    week = 40;
    weight = 3460;
}]';

define("CI_LOG_FILE", "C://ci_log.log");

//Định nghĩa các khối mặc định khi tạo trường (mầm non)
$defaultGrades = array(
    array('name' => 'Khối nhà trẻ', 'description' => '', 'gov_class_level' => '-1'),
    array('name' => 'Khối mẫu giáo', 'description' => '', 'gov_class_level' => '0')
);

//Định nghĩa các khối mặc định khi tạo trường (cấp 1)
$defaultGrades1 = array(
    array('name' => 'Khối 1', 'description' => '', 'gov_class_level' => '1'),
    array('name' => 'Khối 2', 'description' => '', 'gov_class_level' => '2'),
    array('name' => 'Khối 3', 'description' => '', 'gov_class_level' => '3'),
    array('name' => 'Khối 4', 'description' => '', 'gov_class_level' => '4'),
    array('name' => 'Khối 5', 'description' => '', 'gov_class_level' => '5'),
);

// Module mặc định đối với các cấp
$defaultModule = array(
    '0' => array(
        'attendance' => true,
        'services' => true,
        'pickup' => true,
        'pickupteacher' => true,
        'tuitions' => true,
        'events' => true,
        'schedules' => true,
        'menus' => true,
        'reports' => true,
        'medicines' => true,
        'feedback' => true,
        'birthdays' => true,
        'diarys' => true,
        'healths' => true,
        'classes' => true,
        'classlevels' => true,
        'teachers' => true,
        'children' => true,
        'roles' => true,
        'loginstatistics' => true,
        'settings' => true,
        'conducts' => false,
        'points' => false,
        'courses' => false,
    ),
    '1' => array(
        'attendance' => true,
        'services' => true,
        'pickup' => false,
        'pickupteacher' => false,
        'tuitions' => true,
        'events' => true,
        'schedules' => true,
        'menus' => true,
        'reports' => true,
        'medicines' => false,
        'feedback' => true,
        'birthdays' => true,
        'diarys' => true,
        'healths' => false,
        'classes' => true,
        'classlevels' => true,
        'teachers' => true,
        'children' => true,
        'roles' => true,
        'loginstatistics' => true,
        'settings' => true,
        'conducts' => true,
        'points' => true,
        'subjects' => true,
        'courses' => true,
    ),
    '2' => array(
        'attendance' => true,
        'services' => true,
        'pickup' => false,
        'pickupteacher' => false,
        'tuitions' => true,
        'events' => true,
        'schedules' => true,
        'menus' => true,
        'reports' => true,
        'medicines' => false,
        'feedback' => true,
        'birthdays' => true,
        'diarys' => true,
        'healths' => false,
        'classes' => true,
        'classlevels' => true,
        'teachers' => true,
        'children' => true,
        'roles' => true,
        'loginstatistics' => true,
        'settings' => true,
        'conducts' => true,
        'points' => true,
        'subjects' => true,
        'courses' => true
    ),
    '99' => array(
        'attendance' => true,
        'services' => true,
        'pickup' => true,
        'pickupteacher' => true,
        'tuitions' => true,
        'events' => true,
        'schedules' => true,
        'menus' => true,
        'reports' => true,
        'medicines' => true,
        'feedback' => true,
        'birthdays' => true,
        'diarys' => true,
        'healths' => true,
        'classes' => true,
        'classlevels' => true,
        'teachers' => true,
        'children' => true,
        'roles' => true,
        'loginstatistics' => true,
        'settings' => true,
        'conducts' => true,
        'points' => true,
        'subjects' => true,
    )
);

$moduleName = array(
    'attendance' => "Attendance",
    'services' => "Service",
    'pickup' => "Late pickup",
    'pickupteacher' => "Late pickup teachers",
    'tuitions' => "Tuition",
    'events' => "Notification - Event",
    'schedules' => "Schedule",
    'menus' => "Menu",
    'reports' => "Contact book",
    'medicines' => "Medicine",
    'feedback' => "Feedback",
    'birthdays' => "Birthday",
    'diarys' => "Diary",
    'healths' => "Health information",
    'classes' => "Class",
    'classlevels' => "Class level",
    'teachers' => "Teacher",
    'children' => "Chidren",
    'roles' => "Permission",
    'loginstatistics' => "Login statistics",
    'settings' => "Cài đặt và kiểm soát",
    'conducts' => "Conduct",
    'points' => "Point",
    'subjects' => "Subject",
    'courses' => "Course",
);
//Định nghĩa các khối mặc định khi tạo trường (cấp 2)
/* DELETE START - ManhDD 01/04/2021 */
//$defaultGrades2 = array(
//    array('name'=>'Khối 6','description'=>'', 'gov_class_level' => '6'),
//    array('name'=>'Khối 7','description'=>'', 'gov_class_level' => '7'),
//    array('name'=>'Khối 8','description'=>'', 'gov_class_level' => '8'),
//    array('name'=>'Khối 9','description'=>'', 'gov_class_level' => '9'),
//);
/* DELETE END - ManhDD 01/04/2021 */
/* ADD START - ManhDD 01/04/2021 */
//$defaultGrades2 = array(
//    array('name' => 'Khối 7', 'description' => '', 'gov_class_level' => '7'),
//    array('name' => 'Khối 8', 'description' => '', 'gov_class_level' => '8'),
//    array('name' => 'Khối 9', 'description' => '', 'gov_class_level' => '9'),
//    array('name' => 'Khối 10', 'description' => '', 'gov_class_level' => '10'),
//    array('name' => 'Khối 11', 'description' => '', 'gov_class_level' => '11'),
//    array('name' => 'Khối 12', 'description' => '', 'gov_class_level' => '12'),
//);
$defaultGrades2 = array(
    array('name' => 'Khối 6', 'description' => '', 'gov_class_level' => '6'),
    array('name' => 'Khối 7', 'description' => '', 'gov_class_level' => '7'),
    array('name' => 'Khối 8', 'description' => '', 'gov_class_level' => '8'),
    array('name' => 'Khối 9', 'description' => '', 'gov_class_level' => '9'),
);
$defaultGrades3 = array(
    array('name' => 'Khối 10', 'description' => '', 'gov_class_level' => '10'),
    array('name' => 'Khối 11', 'description' => '', 'gov_class_level' => '11'),
    array('name' => 'Khối 12', 'description' => '', 'gov_class_level' => '12'),
);
/* ADD END - ManhDD 01/04/2021 */
// ADD START  MANHDD 03/06/2021
// Định nghĩa hạnh kiểm của học sinh
$default_conducts = array(
    "Excellent",
    "Good",
    "Average",
    "Weak");
// ADD END  MANHDD 03/06/2021
define("PARAM_PROFILE", "user_id, user_group, user_name, user_email, user_email_activation, user_phone, user_phone_signin, user_id_card_number, date_of_issue, place_of_issue, city_id, user_fullname, user_firstname, user_lastname, user_gender, 
user_picture, user_picture_id, user_cover, user_cover_id, user_pinned_post, user_work_title, user_work_place, 
user_current_city, user_hometown, user_edu_major, user_edu_school, user_edu_class, user_birthdate, user_activated,
user_privacy_wall, user_privacy_birthdate, user_privacy_work, user_privacy_location, user_privacy_education, 
user_privacy_friends, user_privacy_photos, user_privacy_pages, user_privacy_groups");

define("PARAM_SCHOOL", "SC.school_id, P.page_admin, P.page_category, P.page_name, P.page_title, P.page_description, P.page_picture, P.page_cover, P.page_likes, P.city_id, P.telephone, P.website, P.email, P.address,
        	SC.teacher_count, SC.class_count, (SC.male_count + SC.female_count) AS child_count, SC.type, SC.lat, SC.lng, SC.city_slug, SC.city_name, SC.district_slug, SC.district_name, SC.short_overview, SC.review_id, SC.start_age, SC.end_age, SC.start_tuition_fee, SC.end_tuition_fee, SC.note_for_tuition, SC.admission, SC.note_for_admission, SC.image_url,       
        	SC.verify, SC.facility_pool, SC.facility_playground_out, SC.facility_playground_in, SC.facility_library, SC.facility_camera, SC.note_for_facility, SC.service_breakfast, SC.service_belated, SC.service_saturday, SC.service_bus, SC.note_for_service,
        	SC.info_leader, SC.info_method, SC.info_teacher, SC.info_nutrition");

// URL của ứng dụng trên Store
define("APPSTORE_URL", "https://itunes.apple.com/us/app/coniu-%E1%BB%A9ng-d%E1%BB%A5ng-t%C6%B0%C6%A1ng-t%C3%A1c-m%E1%BA%A7m-non/id1166247192?ls=1&mt=8");
define("GOOGLEPLAY_URL", "https://play.google.com/store/apps/details?id=com.project.coniu.coniu");

// Định nghĩa những ngày nghỉ trong năm (dùng cho các hàm chạy ngầm)
$holidayBackground = array('01/01/2019', '02/02/2019', '03/02/2019', '04/02/2019', '05/02/2019', '06/02/2019', '07/02/2019',
    '08/02/2019', '09/02/2019', '10/02/2019', '13/04/2019', '14/04/2019', '15/04/2019', '27/04/2019', '28/04/2019',
    '29/04/2019', '30/04/2019', '01/05/2019', '31/08/2019', '01/09/2019', '02/09/2019');
?>