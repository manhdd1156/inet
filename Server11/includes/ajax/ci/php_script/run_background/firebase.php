<?php
/**
 * Hàm chạy tuơng tác ngầm với Firebase
 * 
 * @package Coniu v2
 * @author Coniu
 */

// set override_shutdown
$override_shutdown = true;
//echo 'test firebase - 11';
error_log('firebase.php - 11 ');
$path = str_replace('includes/ajax/ci/php_script/run_background', '', dirname(__FILE__));
set_include_path($path);
// fetch bootstrap
//require('bootstrap.php');

include_once(DAO_PATH . 'dao_firebase.php');
//include_once('D:/workplace/mascom-edu-server/Server11/includes/ajax/ci/dao/dao_firebase.php');
$firebaseDao = new FirebaseDAO();
error_log('firebase.php - 19 ');
//Firebase
include_once(ABSPATH . 'includes/libs/FirebaseAdmin/autoload.php');
//include_once('D:/workplace/mascom-edu-server/Server11/includes/libs/FirebaseAdmin/autoload.php');
error_log('firebase.php - 23 ');
//use Kreait\Firebase\Factory;
//use Kreait\Firebase\ServiceAccount;

use Kreait\Firebase\Exception\FirebaseException;
try {
    // The operation you want to perform
    echo '<';
    var_dump(is_callable(ServiceAccount::class, 'fromJsonFile'));
    echo '>';
//error_log('firebase.php - 36 '.file_exists(ABSPATH . 'includes/libs/FirebaseAdmin/cam-edu-528b1-firebase-adminsdk-i953m-82a063e199.json'));
// This assumes that you have placed the Firebase credentials in the same directory
// as this PHP file.bs/FirebaseAdmin/secret/inet-eab23-firebase-adminsdk-5ytxd-c26eae3ec4.json');
////UPDATE MANHDD 12/05/2021 - sửa đường dân file json cho Server Edu.mas
//$serviceAccount = ServiceAccount::fromJsonFile(ABSPATH . 'includes/licom.com.vn
//error_log('check path :'.ABSPATH . 'includes/libs/FirebaseAdmin/cam-edu-528b1-firebase-adminsdk-i953m-82a063e199.json');
$serviceAccount = ServiceAccount::fromJsonFile(ABSPATH . 'includes/libs/FirebaseAdmin/sll-app-876d5-firebase-adminsdk-4avd0-7a897259b5.json');

error_log('firebase 31 :$serviceAccount : ');
$firebase = (new Factory)
    ->withServiceAccount($serviceAccount)
    // The following line is optional if the project id in your credentials file
    // is identical to the subdomain of your Firebase project. If you need it,
    // make sure to replace the URL with the URL of your project.
    ->withDatabaseUri('https://sll-app-876d5-default-rtdb.asia-southeast1.firebasedatabase.app')
    ->create();
$dbFirebase = $firebase->getDatabase();
error_log('firebase 40 :$firebase : '.json_decode($firebase));
error_log('firebase 41 :$dbFirebase : '.json_decode($dbFirebase));
echo 'Success';
} catch (FirebaseException $e) {
    echo 'An error has occurred while working with the SDK: '.$e->getMessage;
} catch (Exception $e) {
    echo 'exception : '.$e->getMessage;
}
$loopCount = 0;
while (true) {
    try {
        sleep(1);
        $actionsFirebase = $firebaseDao->getActionFirebases();

        $deletedIds = array();
        $failedAndRetryIds = array();
        $sentAndSavedIds = array();
        foreach ($actionsFirebase as $action) {
            $result = 0;
            //Thực hiện tác vụ
            switch ($action['action']) {
                case CREATE_USER:
                    $result = $dbFirebase->getReference('Users/_'. $action['user_id'])
                        ->set([
                            'email' => $action['user_email'],
                            'fullName' => convertText4Web($action['user_fullname']),
                            'username' => $action['user_name'],
                            'avatar' => $action['user_picture'],
                            'isOnline' => 0,
                            'countChat' => 0,
                            'lastLogin' => date(DB_DATETIME_FORMAT_DEFAULT),
                            'provider' => $action['provider'],
                        ]);
                    break;

                case DELETE_USER:
                    $result = $dbFirebase->getReference('Users/_'. $action['user_id'])->remove();
                    break;

                case UPDATE_USER:
                    $updates = array();
                    if (!is_empty($action['user_email'])) {
                        $updates['email'] = $action['user_email'];
                    }
                    if (!is_empty($action['user_name'])) {
                        $updates['username'] = $action['user_name'];
                    }
                    if (!is_empty($action['user_fullname'])) {
                        $updates['fullName'] = convertText4Web($action['user_fullname']);
                    }
                    if (!is_empty($action['user_picture'])) {
                        $updates['avatar'] = $action['user_picture'];
                    }
                    if (!empty($updates)) {
                        $result = $dbFirebase->getReference('Users/_'. $action['user_id']) // this is the root reference
                        ->update($updates);
                    }

                    break;

                case ADD_FRIEND:
                    $id1 = '_'.$action['user_one_id'];
                    $id2 = '_'.$action['user_two_id'];

                    $conversations = $dbFirebase->getReference('Users/'. $id1 .'/conversations') // this is the root reference
                    ->getValue();

                    $conversation_id = null;
                    foreach ($conversations as $key => $value ) {
                        if ($value['user_id'] == $id2) {
                            $conversation_id = $key;
                        }
                    }

                    if (!is_null($conversation_id)) {
                        $friendData = [
                            $id1. '/friends/'. $id2 => $conversation_id,
                            $id2 . '/friends/'. $id1 => $conversation_id,
                        ];
                        error_log('$firebase - friendData : '.json_decode($friendData));
                        $result = $dbFirebase->getReference('Users') // this is the root reference
                        ->update($friendData);
                    } else {
                        $names = array();
                        // get list name of participant
                        $get_name = $db->query(sprintf("SELECT user_id, user_firstname, user_lastname FROM users WHERE user_id = %s OR user_id = %s", secure($action['user_one_id'], 'int'), secure($action['user_two_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
                        if($get_name->num_rows > 0) {
                            while ($name = $get_name->fetch_assoc()) {
                                $names[$name['user_id']] = convertText4Web($name['user_firstname'] . ' ' . $name['user_lastname']);
                            }
                        }

                        //add conversation
                        $conversationData = [
                            'conversationInfo' => [
                                'avatarGroup' => '',
                                'isGroup' => 0,
                                'nameGroup' => '',
                                'lastTimeUpdated' => '',
                                'lastMessage' => '',
                                'participants' => [
                                    $id1 => [
                                        'fromMessageKey' => '',
                                        'isAdmin' => 0,
                                        'isSeen' => 1,
                                        'isTyping' => 0,
                                        'name' => isset($names[$action['user_one_id']]) ? $names[$action['user_one_id']] : ''
                                    ],
                                    $id2 => [
                                        'fromMessageKey' => '',
                                        'isAdmin' => 0,
                                        'isSeen' => 1,
                                        'isTyping' => 0,
                                        'name' => isset($names[$action['user_two_id']]) ? $names[$action['user_two_id']] : ''
                                    ]
                                ]
                            ]
                        ];
                        $conversationRef = $dbFirebase->getReference('Conversations')->push($conversationData);
                        $conversationKey = $conversationRef->getKey();
                        $conversationOneData = [
                            'isDelete' => 0,
                            'user_id' => $id2,
                        ];
                        $conversationTwoData = [
                            'isDelete' => 0,
                            'user_id' => $id1,
                        ];

                        $updates = [
                            $id1. '/friends/'. $id2 => $conversationKey,
                            $id2 . '/friends/'. $id1 => $conversationKey,
                            $id1. '/conversations/'. $conversationKey => $conversationOneData,
                            $id2 . '/conversations/'. $conversationKey => $conversationTwoData
                        ];

                        $result = $dbFirebase->getReference('Users') // this is the root reference
                        ->update($updates);
                    }
                    break;

                case REMOVE_FRIEND:
                    $id1 = '_'.$action['user_one_id'];
                    $id2 = '_'.$action['user_two_id'];
                    //delete friends data and add conversations info to list chat unfriends
                    $deleteData = [
                        $id1. '/friends/'. $id2 => null,
                        $id2 . '/friends/' .$id1 => null
                    ];

                    $result = $dbFirebase->getReference('Users') // this is the root reference
                    ->update($deleteData);

                    break;
            }

            if ((is_object($result)) && ($action['delete_after_sending'] == 1)) {
                //Gửi thành công và xóa action
                $deletedIds[] = $action['firebase_id'];
            } elseif (is_object($result)) {
                //Gửi thành công nhưng không xóa action
                $sentAndSavedIds[] = $action['firebase_id'];
            } else {
                //Trường hợp action bị lỗi (KHÔNG XÓA), tăng số lần thử lại
                $failedAndRetryIds[] = $action['firebase_id'];
                /*if ($action['retry_count'] >= MAX_RETRY_TIME) {
                    //Nếu vẫn tiếp tục lỗi mà quá số lần thử lại thì xóa
                    $deletedIds[] = $action['firebase_id'];
                } else {
                    $failedAndRetryIds[] = $action['firebase_id'];
                }*/
            }
        }
        //Trường hợp action bị lỗi (KHÔNG XÓA)
        /*if ($loopCount++ > 10000) { //Tức là sau (2s * 10000) giây, thì mới check mail expired để xóa
            //Lấy ra những email đã gửi, và quá ngày lưu trữ.
            $expiredIds = $firebaseDao->getExpiredActionFirebaseIds(EXPIRED_INTERVAL);
            $deletedIds = array_merge($deletedIds, $expiredIds);
            $loopCount = 0;
        }*/
        //Xóa những email đủ điều kiện xóa
        $firebaseDao->deleteSomeActions($deletedIds);

        //Cập nhật những tác vụ thành công
        $firebaseDao->setCompletedStatus($sentAndSavedIds);

        //Cập nhật số lần retry của mail gửi lỗi
        $firebaseDao->increaseRetryCountSomeAction($failedAndRetryIds);
    } catch (Exception $e) {
        echo $e->getMessage();
    }

    if (date('H:i') == '05:02') {
        break;
    }
}

?>