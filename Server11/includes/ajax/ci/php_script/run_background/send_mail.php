<?php
/**
 * Hàm chạy các các nhiệm vụ ngầm theo lịch
 * 
 * @package ConIu v1
 * @author QuanND
 */

//Số lần thử gửi lại khi gửi email lỗi
define("MAX_RETRY_TIME", 3);
//Số ngày giữ lại những email đã gửi.
define("EXPIRED_INTERVAL", 3);

// set override_shutdown
$override_shutdown = true;

$path = str_replace('includes/ajax/ci/php_script/run_background', '', dirname(__FILE__));
set_include_path($path);

// fetch bootstrap
require('bootstrap.php');

include_once(DAO_PATH . 'dao_mail.php');
$mailDao = new MailDAO();
$loopCount = 0;

while (true) {
    try {
        sleep(2);
        $unsentEmails = $mailDao->getUnsentEmails();
        $deletedIds = array();
        $failedAndRetryIds = array();
        $sentAndSavedIds = array();
        foreach ($unsentEmails as $email) {
            //Gửi email đó đi
            $email['receivers'] = convertText4Web($email['receivers']);
            $email['subject'] = convertText4Web($email['subject']);
            $email['content'] = convertText4Web($email['content']);
            $email['file_attachment'] = convertText4Web($email['file_attachment']);
            $email['file_attachment_name'] = convertText4Web($email['file_attachment_name']);
            $result = $mailDao->sendEmail($email['receivers'], $email['subject'], $email['content'], false, false, $email['file_attachment'], $email['file_attachment_name']);
            if (($result) && ($email['delete_after_sending'] == 1)) {
                //Gửi thành công và xóa sau khi gửi
                $deletedIds[] = $email['email_id'];
            } elseif ($result) {
                //Gửi thành công nhưng không xóa mail gửi đi
                $sentAndSavedIds[] = $email['email_id'];
            } else {
                //Trường hợp gửi mail bị lỗi
                if ($email['retry_count'] >= MAX_RETRY_TIME) {
                    //Nếu vẫn tiếp tục lỗi mà quá số lần thử lại thì xóa
                    $deletedIds[] = $email['email_id'];
                } else {
                    $failedAndRetryIds[] = $email['email_id'];
                }
            }
        }
        if ($loopCount++ > 10000) { //Tức là sau (2s * 10000) giây, thì mới check mail expired để xóa
            //Lấy ra những email đã gửi, và quá ngày lưu trữ.
            $expiredIds = $mailDao->getExpiredSentEmailIds(EXPIRED_INTERVAL);
            $deletedIds = array_merge($deletedIds, $expiredIds);
            $loopCount = 0;
        }
        //Xóa những email đủ điều kiện xóa
        $mailDao->deleteSomeEmails($deletedIds);

        //Cập nhật những email gửi thành công
        $mailDao->setSentStatus($sentAndSavedIds);

        //Cập nhật số lần retry của mail gửi lỗi
        $mailDao->increaseRetryCountSomeEmail($failedAndRetryIds);
    } catch (Exception $e) {
        echo $e->getMessage();
    }

    if (date('H:i') == '05:02') {
        break;
    }
}

?>