<?php
/**
 * Hàm chạy các các nhiệm vụ ngầm theo lịch
 *
 * @package ConIu v1
 * @author Coniu
 */

// set override_shutdown
$override_shutdown = true;

$path = str_replace('includes/ajax/ci/php_script/run_background', '', dirname(__FILE__));
set_include_path($path);

// fetch bootstrap
require('bootstrap.php');

include_once(DAO_PATH . 'dao_send_notification.php');
$sendNotificationDao = new SendNotificationDAO();

//Firebase
include_once(ABSPATH . 'includes/libs/FirebaseAdmin/vendor/autoload.php');
use Kreait\Firebase\Factory;
use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\ServiceAccount;

// This assumes that you have placed the Firebase credentials in the same directory
// as this PHP file.
//$serviceAccount = ServiceAccount::fromJsonFile(ABSPATH . 'includes/libs/FirebaseAdmin/secret/inet-eab23-firebase-adminsdk-5ytxd-c26eae3ec4.json');
//UPDATE MANHDD 12/05/2021 - sửa đường dân file json cho Server Edu.mascom.com.vn
$serviceAccount = ServiceAccount::fromJsonFile(ABSPATH . 'includes/libs/FirebaseAdmin/sll-app-876d5-firebase-adminsdk-4avd0-7a897259b5.json');

$firebase = (new Factory)
    ->withServiceAccount($serviceAccount)
    // The following line is optional if the project id in your credentials file
    // is identical to the subdomain of your Firebase project. If you need it,
    // make sure to replace the URL with the URL of your project.
    ->withDatabaseUri('https://sll-app-876d5-default-rtdb.asia-southeast1.firebasedatabase.app')
    ->create();
$messaging = $firebase->getMessaging();
//$dbFirebase = $firebase->getDatabase();


while (true) {
    try {
        sleep(2);
        $unsentNotifications = $sendNotificationDao->getUnsentNotifications();
        $deletedIds = array();
        $messages = array();
        //$sentAndSavedIds = array();
        foreach ($unsentNotifications as $notification) {
            $node = array();
            $device_tokens = explode(';', $notification['device_token']);
            $send_notification_id = $notification['send_notification_id'];
            //$notification['user_id'] = $notification['to_user_id'];

            /* Xóa các giá trị không cần thiết */
            unset($notification['send_notification_id'], $notification['device_token']);

            $notification['user_fullname'] = html2text($notification['user_fullname']);
            $notification['message'] = html2text($notification['message']);
            $node['notification'] = array(
                //'title' => 'Coniu',
                'body' => $notification['user_fullname'] . "\n" . $notification['message']
            );
            $node['data'] = $notification;

            $node['android'] = [
                'priority' => 'high',
                'notification' => [
                    'channel_id' => 'inet_channel_01',
                    'sound' => 'default',
                    'default_light_settings' => true,
                    "notification_count" => (int)$notification['count_notifications']
                ]
            ];

            $node['apns'] = array(
                'headers' => [
                    'apns-priority' => '10'
                ],
                'payload' => [
                    'aps' => [
                        'sound' => 'default',
                        'badge' => (int)$notification['count_notifications']
                    ]
                ]
            );

            if (count($device_tokens) == 1) {
                $node['token'] = $device_tokens[0];
                //Thêm vào danh sách gửi
                $messages[] = CloudMessage::fromArray($node);
            } else {
                $message = CloudMessage::fromArray($node);
                $sendMulticast = $messaging->sendMulticast($message, $device_tokens);
            }
            //Thêm vào danh sách xóa sau khi gửi
            $deletedIds[] = $send_notification_id;
        }

        if (!empty($messages)) {
            //$message = CloudMessage::new(); // Any instance of Kreait\Messaging\Message
            $sendAll = $messaging->sendAll($messages);
        }

        // Xóa những notification đủ điều kiện xóa
        $sendNotificationDao->deleteSomeNotifications($deletedIds);

    } catch (Exception $e) {
        echo $e->getMessage();
    }

    if (date('H:i') == '05:02') {
        break;
    }
}


?>