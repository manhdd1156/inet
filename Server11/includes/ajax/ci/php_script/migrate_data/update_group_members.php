<?php
/**
 * Hàm chạy ngầm cập nhật số lượng thành viên trong groups
 *
 * @package ConIu v1
 * @author ConIu v1
 */

$path = str_replace('includes/ajax/ci/php_script/migrate_data', '', dirname(__FILE__));
set_include_path($path);

// fetch bootstrap
require('bootstrap.php');

$db->autocommit(false);

try {
    $db->begin_transaction();

    //1. Lấy ra tất cả groups lớp
    $strSql = sprintf("SELECT group_id FROM groups ORDER BY group_id ASC");

    $get_groups = $db->query($strSql) or _error('error', $strSql);
    if($get_groups->num_rows > 0) {
        while($group = $get_groups->fetch_assoc()) {
            $group_members = getGroupMembers($group['group_id']);
            updateGroupMembers($group['group_id'], $group_members);
        }
    }

    $db->commit();

} catch (Exception $e) {
    $db->rollback();
    return_json( array('error' => true, 'message' => $e->getMessage()) );
} finally {
    $db->autocommit(true);
}


function getGroupMembers($group_id) {
    global $db;
    // Lấy ra số lượng members đã approved vào 1 group
    $strSql = sprintf("SELECT count(M.user_id) as members FROM groups_members M
                INNER JOIN users U ON U.user_id = M.user_id 
                WHERE M.group_id = %s AND M.approved = '1'", secure($group_id, 'int'));


    $get_members = $db->query($strSql) or _error('error', $strSql);
    if($get_members->num_rows > 0) {
        return $get_members->fetch_assoc()['members'];
    }
    return 0;
}

function updateGroupMembers($group_id, $group_members) {
    global $db;
    // Update số lượng members trong group
    $strSql = sprintf("UPDATE groups SET group_members = %s WHERE group_id = %s", secure($group_members, 'int'), secure($group_id, 'int'));

    $db->query($strSql) or _error('error', $strSql);
}

?>