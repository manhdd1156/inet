<?php
/**
 * Hàm chạy ngầm lượt tương tác của page trường
 *
 * @package ConIu v1
 * @author ConIu v1
 */

$path = str_replace('includes/ajax/ci/php_script/migrate_data', '', dirname(__FILE__));
set_include_path($path);

// fetch bootstrap
require('bootstrap.php');

$db->autocommit(false);

try {
    $db->begin_transaction();

    //1. Lấy ra tất cả post trong page
    $strSql = sprintf("SELECT post_id FROM posts WHERE user_type = 'page' AND user_id IN (1,2,3)");

    $postIds = array();
    $get_posts = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    if($get_posts->num_rows > 0) {
        while($post = $get_posts->fetch_assoc()) {
            $postIds[] = $post['post_id'];
        }
    }

    // Xóa tất cả các post liên quan
    $strPost = implode(',', $postIds);

    //Xóa pages_likes
    $strSql = sprintf("DELETE FROM pages_likes WHERE page_id IN (1,2,3)");
    $db->query($strSql) or _error('error', $strSql);

    //Xóa posts_files
    $strSql = sprintf("DELETE FROM posts_files WHERE post_id IN (%s)", $strPost);
    $db->query($strSql) or _error('error', $strSql);

    //Xóa posts_links
    $strSql = sprintf("DELETE FROM posts_links WHERE post_id IN (%s)", $strPost);
    $db->query($strSql) or _error('error', $strSql);

    //Xóa posts_media
    $strSql = sprintf("DELETE FROM posts_media WHERE post_id IN (%s)", $strPost);
    $db->query($strSql) or _error('error', $strSql);

    //1. Lấy ra tất cả post trong page
    $strSql = sprintf("SELECT photo_id FROM posts_photos WHERE post_id IN (%s)", $strPost);

    $photoIds = array();
    $get_photos = $db->query($strSql) or _error('error', $strSql);
    if($get_photos->num_rows > 0) {
        while($photo = $get_photos->fetch_assoc()) {
            $photoIds[] = $photo['photo_id'];
        }
    }

    //Xóa posts_photos_likes
    $strPhotoIds = implode(',', $photoIds);
    $strSql = sprintf("DELETE FROM posts_photos_likes WHERE photo_id IN (%s)", $strPhotoIds);
    $db->query($strSql) or _error('error', $strSql);


    // Lấy ra tất cả comment cần xóa
    $strSql = sprintf("SELECT comment_id FROM posts_comments WHERE (node_type = 'post' AND node_id IN (%s)) OR
              (node_type = 'photo' AND node_id IN (%s))", $strPost, $strPhotoIds);

    $commentIds = array();
    $get_comments = $db->query($strSql) or _error('error', $strSql);
    if($get_comments->num_rows > 0) {
        while($comment = $get_comments->fetch_assoc()) {
            $commentIds[] = $comment['comment_id'];
        }
    }

    //Xóa posts_comments
    $strCommentIds = implode(',', $commentIds);
    $strSql = sprintf("DELETE FROM posts_comments WHERE comment_id IN (%s)", $strCommentIds);
    $db->query($strSql) or _error('error', $strSql);


    //Xóa posts_photos
    $strSql = sprintf("DELETE FROM posts_photos WHERE post_id IN (%s)", $strPost);
    $db->query($strSql) or _error('error', $strSql);

    //Xóa posts_photos_album
    $strSql = sprintf("DELETE FROM posts_photos_albums WHERE user_type = 'page' AND user_id IN (1,2,3)", $strPost);
    $db->query($strSql) or _error('error', $strSql);

    //Xóa posts_saved
    $strSql = sprintf("DELETE FROM posts_saved WHERE post_id IN (%s)", $strPost);
    $db->query($strSql) or _error('error', $strSql);

    //Xóa posts_videos
    $strSql = sprintf("DELETE FROM posts_videos WHERE post_id IN (%s)", $strPost);
    $db->query($strSql) or _error('error', $strSql);

    //Xóa pages
    $strSql = sprintf("DELETE FROM pages WHERE page_id IN (1,2,3)");
    $db->query($strSql) or _error('error', $strSql);

    $db->commit();

} catch (Exception $e) {
    $db->rollback();
    return_json( array('error' => true, 'message' => $e->getMessage()) );
} finally {
    $db->autocommit(true);
}

?>