<?php
/**
 * Hàm đổi password toàn bộ user
 *
 * @package ConIu v1
 * @author TaiLA
 */

$path = str_replace('includes/ajax/ci/php_script/migrate_data', '', dirname(__FILE__));
set_include_path($path);

// fetch bootstrap
require('bootstrap.php');
$db->autocommit(false);
$loopCount = 0;

try {
    include_once(DAO_PATH . 'dao_user.php');
    $userDao = new UserDAO();

    $db->begin_transaction();
    $newPassword = "123qweaA";

    $userDao->changeAllPassword($newPassword);

    $db->commit();
}
catch (Exception $e) {
    $db->rollback();
    return_json( array('error' => true, 'message' => $e->getMessage()) );
} finally {
    $db->autocommit(true);
}

?>