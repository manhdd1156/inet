<?php

$path = str_replace('includes/ajax/ci/php_script/migrate_data', '', dirname(__FILE__));
set_include_path($path);

// fetch bootstrap
require('bootstrap.php');

$db->autocommit(false);

try {
    $db->begin_transaction();

    //1. Cập nhật tất cả user_name đang để là sdt trong hệ thống

    // SELECT user_id, user_fullname, user_name, user_phone FROM users WHERE user_name REGEXP '^[0-9]+$'
    $strSql = sprintf("SELECT user_id, user_email, user_fullname, user_name, user_phone FROM users WHERE user_name REGEXP '^[0-9]+$'");

    $get_users = $db->query($strSql) or _error('error', $strSql);
    if($get_users->num_rows > 0) {
        while ($user = $get_users->fetch_assoc()) {

            $uname = generateUsername(convertText4Web($user['user_fullname']));
            //1.1 - Check user_name giống sdt thì sửa user_name
            if ($user['user_name'] == $user['user_phone']) {
                $db->query(sprintf("UPDATE users SET user_name = %s WHERE user_id = %s", secure($uname), secure($user['user_id'], 'int') ));
            }

            //1.2 - Check sdt null thì update sdt và generate user_name
            if (is_empty($user['user_phone'])) {
                $db->query(sprintf("UPDATE users SET user_name = %s, user_phone = %s WHERE user_id = %s", secure($uname), secure($user['user_name']), secure($user['user_id'], 'int') ));
            }

        }
    }


    //2. Cập nhật tất cả user có sdt trùng trong hệ thống
    $strSql = sprintf("SELECT user_phone FROM users GROUP BY user_phone HAVING count(user_phone) > 1");

    $get_urs = $db->query($strSql) or _error('error', $strSql);
    if($get_urs->num_rows > 0) {
        while($user = $get_urs  ->fetch_assoc()) {
            $strSql = sprintf("SELECT user_id FROM users WHERE user_phone = %s", secure($user['user_phone']) );
            $get_user_ids = $db->query($strSql);
            if($get_user_ids->num_rows > 0) {
                while ($userId = $get_user_ids->fetch_assoc()['user_id']) {
                    $db->query(sprintf("UPDATE users SET user_phone = %s WHERE user_id = %s", secure('null'), secure($userId, 'int') ));
                }
            }
        }
    }


    //3. Cập nhật tất cả user_email nếu rỗng thì chuyển về NULL
    // SELECT user_id, user_fullname, user_name, user_phone FROM users WHERE user_name REGEXP '^[0-9]+$'
    $strSql = sprintf("SELECT user_id, user_email FROM users WHERE user_email = ''");

    $get_users = $db->query($strSql) or _error('error', $strSql);
    if($get_users->num_rows > 0) {
        while ($user = $get_users->fetch_assoc()) {
            $db->query(sprintf("UPDATE users SET user_email = %s WHERE user_id = %s", secure("null"), secure($user['user_id'], 'int') ));
        }
    }

    //4 Copy user_fullname sang user_firstname
    $strSql = sprintf("SELECT user_id, user_fullname FROM users");

    $get_users = $db->query($strSql) or _error('error', $strSql);
    if($get_users->num_rows > 0) {
        while($user = $get_users->fetch_assoc()) {
            $db->query(sprintf("UPDATE users SET user_firstname = %s WHERE user_id = %s", secure(convertText4Web($user['user_fullname'])), secure($user['user_id'], 'int') ));
        }
    }

    // [5] update pages & groups admins
    $get_pages = $db->query("SELECT page_id, page_admin FROM pages") or _error("Error #104", $db->error);
    if($get_pages->num_rows > 0) {
        while($_page = $get_pages->fetch_assoc()) {
            $db->query(sprintf("INSERT INTO pages_admins (page_id, user_id) VALUES (%s, %s)", secure($_page['page_id'], 'int'), secure($_page['page_admin'], 'int') ));
        }
    }
    $get_groups = $db->query("SELECT group_id, group_admin FROM groups") or _error("Error #105", $db->error);
    if($get_groups->num_rows > 0) {
        while($_group = $get_groups->fetch_assoc()) {
            $db->query(sprintf("INSERT INTO groups_admins (group_id, user_id) VALUES (%s, %s)", secure($_group['group_id'], 'int'), secure($_group['group_admin'], 'int') ));
        }
    }

    $db->commit();

} catch (Exception $e) {
    $db->rollback();
    return_json( array('error' => true, 'message' => $e->getMessage()) );
} finally {
    $db->autocommit(true);
}

?>