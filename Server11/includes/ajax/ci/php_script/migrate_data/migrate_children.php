<?php
/**
 * Hàm migrate dữ liệu trẻ từ trường sang phụ huynh
 * 
 * @package ConIu v1
 * @author ConIu
 */

// set override_shutdown
$override_shutdown = true;

$path = str_replace('includes/ajax/ci/php_script/migrate_data', '', dirname(__FILE__));
set_include_path($path);

// fetch bootstrap
require('bootstrap.php');

try {
    $db->autocommit(false);
    $db->begin_transaction();

    //1. Lấy toàn bộ danh sách trẻ trong trường
    $strSql = sprintf("SELECT C.*, SC.school_id FROM ci_child C
                       INNER JOIN ci_school_child SC ON C.child_id = SC.child_id ORDER BY C.child_id ASC");

    $children = array();
    $get_children = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    if($get_children->num_rows > 0) {
        while($child = $get_children->fetch_assoc()) {

            //2. Lấy ra trẻ được quản lý bởi phụ huynh
            $strSql = sprintf("SELECT UM.user_manage_id, UM.user_id, UM.description AS child_description, SC.school_id, SC.status AS child_status FROM ci_user_manage UM
               INNER JOIN ci_school_child SC ON SC.child_id = UM.object_id WHERE UM.object_id = %s AND UM.object_type = %s ORDER BY UM.user_manage_id ASC", secure($child['child_id'], 'int'), MANAGE_CHILD);

            $get_user_manage = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
            if($get_user_manage->num_rows > 0) {

                //5. Lưu thông tin quản lý trẻ của phụ huynh
                $i = 1;
                $childParentId = 0;
                while ($user_manage = $get_user_manage->fetch_assoc()) {
                    if ($i == 1) {
                        //3. Thêm thông tin trẻ cho phụ huynh
                        $strSql = sprintf("INSERT INTO ci_child_parent (child_admin, child_code, first_name, last_name, child_name, child_nickname, birthday, gender, child_picture, parent_name, parent_phone, parent_email, parent_img1, parent_img2, parent_img3, parent_img4, address, description, created_user_id, name_for_sort) 
                               VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s,%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
                            secure($user_manage['user_id'], 'int'), secure($child['child_code']), secure(convertText4Web($child['first_name'])), secure(convertText4Web($child['last_name'])), secure(convertText4Web($child['child_name'])), secure(convertText4Web($child['child_nickname'])), secure($child['birthday']), secure($child['gender'], 'int'), secure($child['child_picture']), secure(convertText4Web($child['parent_name'])), secure($child['parent_phone']), secure($child['parent_email']),
                            secure($child['parent_img1']), secure($child['parent_img2']), secure($child['parent_img3']), secure($child['parent_img4']), secure(convertText4Web($child['address'])), secure(convertText4Web($child['description'])), secure($child['created_user_id'], 'int'), secure($child['name_for_sort']));
                        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
                        $childParentId = $db->insert_id;

                        //4. Cập nhật thôn tin lại bảng ci_child ( Lưu thông tin trẻ trong trường)
                        $strSql = sprintf("UPDATE ci_child SET child_parent_id = %s WHERE child_id = %s",  secure($childParentId, 'int'), secure($child['child_id'], 'int'));
                        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

                        //4. Cập nhật bảng ci_school_child
                        $strSql = sprintf("UPDATE ci_school_child SET child_parent_id = %s WHERE child_id = %s AND school_id = %s",  secure($childParentId, 'int'), secure($child['child_id'], 'int'), secure($user_manage['school_id'], 'int'));
                        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
                    }
                    if ($childParentId > 0) {
                        $strSql = sprintf("INSERT INTO ci_parent_manage (user_id, child_parent_id, child_id, school_id, status, description) VALUES (%s, %s, %s, %s, %s, %s)",
                            secure($user_manage['user_id'], 'int'), secure($childParentId, 'int'), secure($child['child_id'], 'int'), secure($user_manage['school_id'], 'int'), secure($user_manage['child_status'], 'int'), secure($user_manage['child_description']));
                        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
                    }
                    $i++;
                }

            } else {
                $strSql = sprintf("INSERT INTO ci_child_parent (child_admin, child_code, first_name, last_name, child_name, child_nickname, birthday, gender, child_picture, parent_name, parent_phone, parent_email, parent_img1, parent_img2, parent_img3, parent_img4, address, description, created_user_id, name_for_sort) 
                               VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s,%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
                    secure(0, 'int'), secure($child['child_code']), secure(convertText4Web($child['first_name'])), secure(convertText4Web($child['last_name'])), secure(convertText4Web($child['child_name'])), secure(convertText4Web($child['child_nickname'])), secure($child['birthday']), secure($child['gender'], 'int'), secure($child['child_picture']), secure(convertText4Web($child['parent_name'])), secure($child['parent_phone']), secure($child['parent_email']),
                    secure($child['parent_img1']), secure($child['parent_img2']), secure($child['parent_img3']), secure($child['parent_img4']), secure(convertText4Web($child['address'])), secure(convertText4Web($child['description'])), secure($child['created_user_id'], 'int'), secure($child['name_for_sort']));
                $db->query($strSql) or _error(SQL_ERROR_THROWEN);
                $childParentId = $db->insert_id;

                //4. Cập nhật thôn tin lại bảng ci_child ( Lưu thông tin trẻ trong trường)
                $strSql = sprintf("UPDATE ci_child SET child_parent_id = %s WHERE child_id = %s",  secure($childParentId, 'int'), secure($child['child_id'], 'int'));
                $db->query($strSql) or _error(SQL_ERROR_THROWEN);

                //4. Cập nhật bảng ci_school_child
                $strSql = sprintf("UPDATE ci_school_child SET child_parent_id = %s WHERE child_id = %s AND school_id = %s",  secure($childParentId, 'int'), secure($child['child_id'], 'int'), secure($child['school_id'], 'int'));
                $db->query($strSql) or _error(SQL_ERROR_THROWEN);
            }

        }
        $db->commit();
    }

} catch (Exception $e) {
    $db->rollback();
    return_json( array('error' => true, 'message' => $e->getMessage()) );
} finally {
    $db->autocommit(true);
}

?>