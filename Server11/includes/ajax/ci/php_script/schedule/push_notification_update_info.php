<?php
/**
 * Hàm chạy các các nhiệm vụ ngầm theo lịch
 *
 * @package ConIu v1
 * @author TaiLA
 */

$path = str_replace('includes/ajax/ci/php_script/schedule', '', dirname(__FILE__));
set_include_path($path);

// fetch bootstrap
require('bootstrap.php');

include_once(DAO_PATH . 'dao_user.php');
$userDao = new UserDAO();

// Push thông báo cập nhật thông tin  chưa chính xác (1 tuần chạy một lần)
$loopCount = 0;
$limit = 100;
try {
    sleep(2);
    // Lấy count users
    $countUser = $userDao->getCountUser();

    $begin = 0;
    $extra3 = $system['system_url']. '/content/themes/inet/images/favicon.png';
    // Lặp danh sách user, gửi thông báo cập nhật thông tin
    while($countUser > 0) {
        $users = getUserEmailOrPhoneNotValid($begin);
        foreach ($users as $row) {
            $userDao->postNotifications($row['user_id'], NOTIFICATION_UPDATE_INFO, 'user',
                $row['user_email'], $row['user_phone'], $row['user_name'], $extra3, 1);
        }

        $begin = $begin + $limit;
        $countUser = $countUser - $limit;
        sleep(2);
    }
} catch (Exception $e) {
    echo $e->getMessage();
}
?>