<?php
/**
 * Hàm chạy các các nhiệm vụ ngầm theo lịch
 *
 * @package ConIu v1
 * @author TaiLA
 */

$path = str_replace('includes/ajax/ci/php_script/schedule', '', dirname(__FILE__));
set_include_path($path);

// fetch bootstrap
require('bootstrap.php');

include_once(DAO_PATH . 'dao_child.php');

$childDao = new ChildDAO();

// Delete danh sách trẻ được giáo viên sửa trước đó hơn 1 tháng
$loopCount = 0;
try {
    sleep(2);

    // Delete danh sách birthday cũ
    $childDao->deleteChildEditAfter1Month();

} catch (Exception $e) {
    echo $e->getMessage();
}
?>