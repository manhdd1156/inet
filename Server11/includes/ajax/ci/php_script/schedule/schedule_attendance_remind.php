<?php
/**
 * Hàm push notification nhắc nhở điểm danh
 *
 * @package ConIu v1
 * @author TaiLA
 */

$path = str_replace('includes/ajax/ci/php_script/schedule', '', dirname(__FILE__));
set_include_path($path);

// fetch bootstrap
require('bootstrap.php');

$db->autocommit(false);

try {
    $today = date('d/m/Y');
    if(!in_array($today, $holidayBackground)) {
        include_once(DAO_PATH . 'dao_attendance.php');
        $attendanceDao = new AttendanceDAO();

        include_once(DAO_PATH . 'dao_school.php');
        $schoolDao = new SchoolDAO();

        include_once(DAO_PATH . 'dao_user.php');
        $userDao = new UserDAO();

        $db->begin_transaction();
        // Lấy danh sách trường đang sử dụng coniu
        $schools = $schoolDao->getAllSchoolsUsingInet(1);

        // Lặp danh sách trường, lấy danh sách lớp của trường
        foreach ($schools as $school) {
            $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);

            if(count($classes) > 0) {
                // Lặp danh sách lớp
                $countNoAttendance = 0;
                $classList = '';

                foreach ($classes as $k => $class) {
                    //kiểm tra xem lớp đó đã được điểm danh chưa
                    $attendance = $attendanceDao->getAttendanceByClassIdAndDate($class['group_id'], $today);
                    if(!$attendance['isRollUp'] || !$attendance['is_checked']) {
                        $countNoAttendance = $countNoAttendance + 1;
                        if($countNoAttendance <= 5) {
                            $classList .= ' ' . $class['group_title'] . ',';
                        }
                        // Lấy danh sách giáo viên của lớp
                        $teachers = getClassData($class['group_id'], CLASS_TEACHERS);
                        $teacherIds = array();
                        foreach ($teachers as $teacher) {
                            $teacherIds[] = $teacher['user_id'];
                        }

                        if(count($teacherIds) > 0) {
                            // Gửi thông báo về giáo viên cho giáo viên của lớp là lớp chưa được điểm danh
                            $userDao->postNotifications($teacherIds, NOTIFICATION_REMIND_ATTENDANCE, NOTIFICATION_NODE_TYPE_CLASS,
                                $class['group_id'], $today, convertText4Web($class['group_name']), convertText4Web($class['group_title']), 1);
                        }
                    }
                }
                if($countNoAttendance > 5) {
                    $classList .= ' ...';
                }

                include_once(DAO_PATH . 'dao_role.php');
                $roleDao = new RoleDAO();
                // Lấy danh sách user có quyền đối với module $view
                $userIdsYesPermission = $roleDao->getUserIdsOfModule($school['page_id'], 'attendance');
                $userIdsYesPermission[] = $school['page_admin'];

                // Lấy danh sách hiệu trưởng của trường
                $principals = array();
                if (!is_empty($school['principal'])) {
                    $principals = $school['principal'];
                    $principals = explode(',', $principals);
                }
                foreach ($principals as $row) {
                    $userIdsYesPermission[] = $row;
                }
                $userIdsYesPermission = array_unique($userIdsYesPermission);

                if($countNoAttendance > 0) {
                    if($classList != '') {
                        $classList = trim($classList, " ,");
                    }
                    $message = sprintf(__("%s's  attendance information on  %s: there are  %s/%s classes have been checked attendance and class %s have not been checked attendance"), convertText4Web($school['page_title']), $today, count($classes) - $countNoAttendance, count($classes), convertText4Web($classList));
                } else {
                    $attendances = $attendanceDao->getAttendanceOfSchoolADay($school['page_id'], $today);
                    $presentCount = 0;
                    $total = 0;
                    foreach ($attendances as $attendance) {
                        if($attendance['present_count'] != null) {
                            $presentCount = $presentCount + $attendance['present_count'];
                        }

                        $total = $total + $attendance['total'];
                    }

                    $message = sprintf(__("%s's attendance information on %s: all classes have been checked attendance and there are %s/%s student attending school"), convertText4Web($school['page_title']), $today, $presentCount, $total);
                }

                if(count($userIdsYesPermission) > 0) {
                    $userDao->postNotifications($userIdsYesPermission, NOTIFICATION_REMIND_ATTENDANCE, NOTIFICATION_NODE_TYPE_SCHOOL,
                        $school['page_id'], $message, convertText4Web($school['page_name']), '', 1);
                }
            }
        }
        $db->commit();
    }
}
catch (Exception $e) {
    $db->rollback();
    return_json( array('error' => true, 'message' => $e->getMessage()) );
} finally {
    $db->autocommit(true);
}


?>