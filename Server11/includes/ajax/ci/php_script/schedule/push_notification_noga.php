<?php
/**
 * Hàm chạy các các nhiệm vụ ngầm theo lịch
 * 
 * @package ConIu v1
 * @author Coniu
 */


// set override_shutdown
$override_shutdown = true;

$path = str_replace('includes/ajax/ci/php_script/schedule', '', dirname(__FILE__));
set_include_path($path);

// fetch bootstrap
require('bootstrap.php');

include_once(DAO_PATH . 'dao_send_notification.php');
include_once(DAO_PATH . 'dao_noga_notification.php');
$sendNotificationDao = new SendNotificationDAO();
$nogaNotificationDao = new NogaNotificationDAO();

try {
    $unsentNotifications = $nogaNotificationDao->getUnsentNotifications();

    foreach ($unsentNotifications as $notification) {
        $extra1 = convertText4Web($notification['title']);
        $extra2 = convertText4Web($notification['content']);
        $extra3 = '';
        $type = ($notification['type'] == 0) ? 'text' : 'link';
        $node_url = $notification['link'];
        $fromUserId = $notification['created_user_id'];
        //1. Lấy ra những người nhận thông báo
        $receivers = $nogaNotificationDao->getNotificationReceiver($notification);

        $sentUsers = array(); // Những user đã nhận thông báo
        //2. Gửi thông báo đến từng user
        foreach ($receivers as $receiver) {
            if (!in_array($receiver['user_id'], $sentUsers)) {
                //$sendNotificationDao->postNotification($receiver['user_id'], NOTIFICATION_CONIU, $type, $node_url, $extra1, $extra2, $extra3, $fromUserId);
                $sentUsers[] = $receiver['user_id'];
            }
        }
    }

} catch (Exception $e) {
    echo $e->getMessage();
}

?>