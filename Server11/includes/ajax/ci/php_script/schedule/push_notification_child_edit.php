<?php
/**
 * Hàm chạy các các nhiệm vụ ngầm theo lịch
 *
 * @package ConIu v1
 * @author TaiLA
 */

$path = str_replace('includes/ajax/ci/php_script/schedule', '', dirname(__FILE__));
set_include_path($path);

// fetch bootstrap
require('bootstrap.php');

include_once(DAO_PATH . 'dao_child.php');
include_once(DAO_PATH . 'dao_school.php');
include_once(DAO_PATH . 'dao_user.php');

$childDao = new ChildDAO();
$schoolDao = new SchoolDAO();
$userDao = new UserDAO();

// Push thông báo có trẻ được chỉn sửa bởi giáo viên trong tuần cho quản lý trường
$loopCount = 0;
try {
    sleep(2);
    // Lấy ngày hiện tại
    $weekday = date("l");
    $weekday = strtolower($weekday);

//    Nếu ngày hiện tại là thứ 7 thì push thông báo về
    if($weekday == 'saturday') {
        $allChildEdit = $childDao->getAllChildEditHistory();
        $field = array('school_id');
        $schoolIds = array();
        foreach ($allChildEdit as $row) {
            $schoolIds = getArrayFromKeys($row, $field);
        }
        $schoolIds = array_unique($schoolIds);
        $schoolIds = array_values($schoolIds);
        if(count($schoolIds) > 0) {
            foreach ($schoolIds as $schoolId) {
                // Lấy chi tiết trường
                $school = getSchoolData($schoolId, SCHOOL_INFO);
                // Lấy dánh sách quản lý của trường
                $userManagerIds = getUserIdsManagerReceiveNotify($schoolId, 'children', $school['page_admin']);

                if(count($userManagerIds) > 0) {
                    $userDao->postNotifications($userManagerIds, NOTIFICATION_NEW_CHILD_EDIT_BY_TEACHER, NOTIFICATION_NODE_TYPE_SCHOOL,
                            '', convertText4Web($school['page_title']), $school['page_name'], '', SYSTEM_USER_ID);
                }
            }
        }
    }

} catch (Exception $e) {
    echo $e->getMessage();
}
?>