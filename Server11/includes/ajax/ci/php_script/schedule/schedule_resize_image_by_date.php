<?php
/**
 * Hàm chạy các các nhiệm vụ ngầm theo lịch
 *
 * @package ConIu v1
 * @author Coniu
 */

// set override_shutdown
$override_shutdown = true;

$path = str_replace('includes/ajax/ci/php_script/schedule', '', dirname(__FILE__));
set_include_path($path);

// fetch bootstrap
require('bootstrap.php');

require(ABSPATH . 'includes/class-image.php');

global $db;

$date = date('Y/m/d', strtotime("-1 days"));
$monthQuery = date('Y/m', strtotime("-1 days"));

//while (true) {
try {
    $get_user = $db->query(sprintf("SELECT user_picture FROM users WHERE user_picture IS NOT NULL AND user_picture LIKE %s", secure($monthQuery, 'search'))) or _error(SQL_ERROR_THROWEN);

    if($get_user->num_rows > 0) {
        while($avatar = $get_user->fetch_assoc()['user_picture']){
            if (!is_empty($avatar)) {
                $path = ABSPATH . 'content/uploads/' . $avatar;

                if (file_exists($path) && $date == date('Y/m/d', filemtime($path)) ) {
                    $image = new Image($path);
                    $strNotExt = str_replace($image->_img_ext, "", $path);
                    $newPath = $strNotExt . '_100' . $image->_img_ext;

                    /* save the new image */
                    if (!file_exists($newPath)) {
                        $image->save_to_small($newPath);
                    }
                }
            }
        }
    }


} catch (Exception $e) {
    echo $e->getMessage();
}
//}

?>