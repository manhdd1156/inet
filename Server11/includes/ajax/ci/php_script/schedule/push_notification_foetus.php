<?php
/**
 * Hàm push notification thông tin phát triển thai nhi
 *
 * @package ConIu v1
 * @author TaiLA
 */

$path = str_replace('includes/ajax/ci/php_script/schedule', '', dirname(__FILE__));
set_include_path($path);

// fetch bootstrap
require('bootstrap.php');

$db->autocommit(false);
$hourSys = date('H');

try {
    include_once(DAO_PATH . 'dao_parent.php');
    $parentDao = new ParentDAO();

    include_once(DAO_PATH . 'dao_foetus_development.php');
    $foetusDevelopmentDao = new FoetusDevelopmentDAO();

    include_once(DAO_PATH . 'dao_user.php');
    $userDao = new UserDAO();

    $db->begin_transaction();
    $foetusDevelopment = $foetusDevelopmentDao->getFoetusInfoForNoga();
    foreach ($foetusDevelopment as $row) {
        $extra1 = convertText4Web($row['title']);
        if (!is_empty($row['image'])) {
            $extra3 = $system['system_uploads'] . '/' . $row['image'];
        } else {
            $extra3 = $system['system_url']. '/content/themes/inet/images/favicon.png';
        }
        //$fromUserId = $notification['created_user_id'];

        $hourPush = date('H', strtotime($row['time']));
        if($hourPush == $hourSys) {
            $daysPush = getDayForPushNotification($row['day_push'], $row['is_notice_before'], $row['notice_before_days'], $row['is_reminder_before'], $row['reminder_before_days']);
            foreach ($daysPush as $dayPush) {
                $childParentIds = getChildIdsPushNotify($dayPush);
                foreach ($childParentIds as $childParentId) {
                    $parentIds = $parentDao->getParentIdsByParent($childParentId);
                    $userDao->postNotifications($parentIds, NOTIFICATION_FOETUS_DEVELOPMENT, NOTIFICATION_NODE_TYPE_CHILD, $row['foetus_info_id'], $extra1, $childParentId, $extra3, 1);
                }
            }
        }
    }

    $db->commit();
} catch (Exception $e) {
    $db->rollback();
    return_json( array('error' => true, 'message' => $e->getMessage()) );
} finally {
    $db->autocommit(true);
}


/**
 * Lấy ra ngày gửi thông báo từ ngày + thời gian thông báo trước + thời gian nhắc lại
 *
 * @return array
 */
function getDayForPushNotification($day_push, $is_notice_before, $notice_before_days, $is_reminder_before, $reminder_before_days) {
    $days = array();
    if ($is_notice_before) {
        $days[] = $day_push - $notice_before_days;
    }
    if ($is_reminder_before) {
        $days[] = $day_push - $reminder_before_days;
    }
    if (!$is_notice_before && !$is_reminder_before) {
        $days[] = $day_push;
    }
    $days = array_unique($days);
    return $days;
}

?>