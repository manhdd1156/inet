<?php
/**
 * Hàm chạy các các nhiệm vụ ngầm theo lịch
 *
 * @package ConIu v1
 * @author Coniu
 */

// set override_shutdown
$override_shutdown = true;

$path = str_replace('includes/ajax/ci/php_script/schedule', '', dirname(__FILE__));
set_include_path($path);

// fetch bootstrap
require('bootstrap.php');

global $db;

//while (true) {
try {
    sleep(1);

    $get_user = $db->query(sprintf("SELECT user_picture FROM users WHERE user_id = %s",
        secure('1', 'int'))) or _error(SQL_ERROR_THROWEN);

    if($get_user->num_rows > 0) {
        $avatar = $get_user->fetch_assoc()['user_picture'];
        print_r($avatar);
    }


} catch (Exception $e) {
    echo $e->getMessage();
}
//}

?>