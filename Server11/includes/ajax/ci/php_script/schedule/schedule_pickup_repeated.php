<?php
/**
 * Hàm chạy ngầm lặp lcihj phân công trông muộn
 *
 * @package ConIu v1
 * @author CinIu v1
 */

$path = str_replace('includes/ajax/ci/php_script/schedule', '', dirname(__FILE__));
set_include_path($path);

// fetch bootstrap
require('bootstrap.php');

$db->autocommit(false);

try {
    include_once(DAO_PATH . 'dao_pickup.php');
    include_once(DAO_PATH . 'dao_user.php');
    $pickupDao = new PickupDAO();
    $userDao = new UserDAO();

    $db->begin_transaction();
    //1.Lấy ra tất cả những trường lặp lịch phân công
    $schools = $pickupDao->getAllschoolHasRepeated();
    foreach ($schools as $school) {
        $args['school_id'] = $school['school_id'];
        //2. Lấy ra thông tin lặp lịch phân công
        $assigns = $pickupDao->getAssignInWeekForSchedule($school['school_id'], $school['beginning_repeat_date']);

        //3. Phân công giáo viên tuần tới
        $monday = date("Y-m-d", strtotime("monday next week", strtotime($school['beginning_repeat_date'])));
        $args['date_2'] = $monday;
        $args['date_3'] = date("Y-m-d", strtotime("+1 day", strtotime($monday)));
        $args['date_4'] = date("Y-m-d", strtotime("+2 day", strtotime($monday)));
        $args['date_5'] = date("Y-m-d", strtotime("+3 day", strtotime($monday)));
        $args['date_6'] = date("Y-m-d", strtotime("+4 day", strtotime($monday)));
        $args['date_7'] = date("Y-m-d", strtotime("+5 day", strtotime($monday)));
        $args['date_8'] = date("Y-m-d", strtotime("+6 day", strtotime($monday)));

        //Lấy ra mảng các giáo viên đã được phân công trong tuần (Tạm comment ko gửi thông báo)
        $assignedInWeek = $pickupDao->getAssignInWeekForNotification($args['school_id'], $monday);
        //Xóa phân công giáo viên đã phân công trông trong tương lai
        $pickupDao->deletePickupAssign($args['school_id'], $args['date_2']);

        //$assignInWeekCheck = array();
        //Phân công
        foreach ($assigns as $assign) {
            $args['user_id'] = $assign['user_id'];
            $args['pickup_time'] = $args['date_' . $assign['assign_day']];
            $args['pickup_day'] = $assign['assign_day'];
            $args['pickup_class_id'] = $assign['pickup_class_id'];

            $arr = array(
                $args['user_id'],
                $args['pickup_time'],
                $args['pickup_class_id']
            );
            /*if (in_array($arr, $assignedInWeek)) {
                throw new Exception(sprintf(__("Teacher is only assigned to a late pickup class, check on %s"), toSysDate($args['pickup_time'])));
            }
            $assignInWeekCheck[] = $arr;*/

            $args['pickup_id'] = $pickupDao->getPickupId($args['school_id'], $args['pickup_time'], $args['pickup_class_id']);
            if ($args['pickup_id'] == 0) {
                $args['pickup_id'] = $pickupDao->insertPickup($args);
            }
            $pickupDao->assignPickup($args);

            //Gửi thông báo đến những giáo viên được phân công mới
            if (!in_array($arr, $assignedInWeek)) {
                $pickup_class = $pickupDao->getPickupClass($args['pickup_class_id']);
                $class_name = isset($pickup_class['class_name']) ? convertText4Web($pickup_class['class_name']) : '';

                $userDao->postNotifications($args['user_id'], NOTIFICATION_ASSIGN_LATEPICKUP_CLASS, NOTIFICATION_NODE_TYPE_CLASS,
                    $args['pickup_id'], toSysDate($args['pickup_time']), $school['page_name'], $class_name);
            }
        }

        //4.Update thứ 2 - tuần lặp lịch gần nhất vào ci_pickup_template
        $args['is_repeat'] = 1;
        $args['beginning_repeat_date'] = $monday;
        $pickupDao->updateRepeatInfo($args);
    }

    $db->commit();

} catch (Exception $e) {
    $db->rollback();
    return_json( array('error' => true, 'message' => $e->getMessage()) );
} finally {
    $db->autocommit(true);
}

?>