<?php
/**
 * Hàm push notification nhắc nhở tạo thực đơn
 *
 * @package ConIu v1
 * @author TaiLA
 */

$path = str_replace('includes/ajax/ci/php_script/schedule', '', dirname(__FILE__));
set_include_path($path);

// fetch bootstrap
require('bootstrap.php');

try {
    $today = date('d/m/Y');
    if(!in_array($today, $holidayBackground)) {
        $db->autocommit(false);

        include_once(DAO_PATH . 'dao_menu.php');
        $menuDao = new MenuDAO();

        include_once(DAO_PATH . 'dao_school.php');
        $schoolDao = new SchoolDAO();

        include_once(DAO_PATH . 'dao_user.php');
        $userDao = new UserDAO();

        $db->begin_transaction();

        // Lấy ngày thứ 2 của tuần hiện tại
        $monday_date = strtotime('monday this week');
        $monday_date = date('d/m/Y',$monday_date);

        // Lấy danh sách trường đang sử dụng coniu
        $schools = $schoolDao->getAllSchoolsUsingInet(1);

        // Lặp danh sách trường, lấy danh sách lớp của trường
        foreach ($schools as $school) {
            $classes = getSchoolData($school['page_id'], SCHOOL_CLASSES);
            if(count($classes) > 0) {
                $countNoMenu = 0;
                $classList = '';
                // Lặp danh sách lớp
                foreach ($classes as $class) {
                    //kiểm tra xem lớp đó đã có thực đơn trong tuần chưa
                    $menus = $menuDao->getMenuOfSchoolByIdOnDate($school['page_id'], $class['class_level_id'], $class['group_id'], $monday_date);
                    if(count($menus) == 0) {
                        $countNoMenu = $countNoMenu + 1;
                        if($countNoMenu <= 5) {
                            $classList .= ' ' . $class['group_title'] . ',';
                        }
                        // Lấy danh sách giáo viên của lớp
                        $teachers = getClassData($class['group_id'], CLASS_TEACHERS);

                        // Gửi thông báo về giáo viên cho giáo viên của lớp là lớp chưa có lịch học
                        if(count($teachers) > 0) {
                            $teacherIds = array();
                            foreach ($teachers as $teacher) {
                                $teacherIds[] = $teacher['user_id'];
                            }

                            $userDao->postNotifications($teacherIds, NOTIFICATION_REMIND_MENU, NOTIFICATION_NODE_TYPE_CLASS,
                                $class['group_id'], '', $class['group_name'], convertText4Web($class['group_title']), 1);
                        }
                        // Lấy danh sách phụ huynh của lớp
                        $children = getClassData($class['group_id'], CLASS_CHILDREN);
                        foreach ($children as $child) {
                            $parents = getChildData($child['child_id'], CHILD_PARENTS);
                            $parentIds = array_keys($parents);
                            // Gửi thông báo về cho phụ huynh là lớp chưa có lịch học tuần này
                            if(count($parentIds) > 0) {
                                $userDao->postNotifications($parentIds, NOTIFICATION_REMIND_MENU, NOTIFICATION_NODE_TYPE_CHILD,
                                    0, '', $child['child_id'], convertText4Web($child['child_name']), 1);
                            }
                        }
                    } else {
                        $removeArr = array();
                        for($i = 0; $i < count($menus); $i++) {
                            for($j = 0; $j < count($menus); $j++) {
                                if($i != $j) {
                                    $beginI = strtotime(toDBDate($menus[$i]['begin']));
                                    $beginJ = strtotime(toDBDate($menus[$j]['begin']));
                                    if($beginI == $beginJ) {
                                        if($menus[$i]['applied_for'] > $menus[$j]['applied_for']) {
                                            $removeArr[] = $j;
                                        }
                                    }
                                }
                            }
                        }
                        $data = array();
                        for($k = 0; $k < count($menus); $k++) {
                            if(!in_array($k, $removeArr)) {
                                $data = $menus[$k];
                            }
                        }

                        // Lấy anh sách trẻ của lớp
                        $children = getClassData($class['group_id'], CLASS_CHILDREN);
                        // Lặp danh sách trẻ, lấy danh sách phụ huynh của trẻ
                        foreach ($children as $child) {
                            $parents = getChildData($child['child_id'], CHILD_PARENTS);

                            if(count($parents) > 0) {
                                $parentIds = array();
                                // Gửi thông báo về cho phụ huynh của trẻ
                                foreach ($parents as $parent) {
                                    $parentIds[] = $parent['user_id'];
                                }
                                $userDao->postNotifications($parentIds, NOTIFICATION_REMIND_MENU, NOTIFICATION_NODE_TYPE_CHILD,
                                    $data['menu_id'], $today, $child['child_id'], convertText4Web($child['child_name']), 1);
                            }
                        }
                    }
                }

                if($countNoMenu > 5) {
                    $classList .= ' ...';
                }

                include_once(DAO_PATH . 'dao_role.php');
                $roleDao = new RoleDAO();
                // Lấy danh sách user có quyền đối với module $view
                $userIdsYesPermission = $roleDao->getUserIdsOfModule($school['page_id'], 'menus');
                $userIdsYesPermission[] = $school['page_admin'];

                // Lấy danh sách hiệu trưởng của trường
                if (!is_empty($school['principal'])) {
                    $principals = explode(',', $school['principal']);
                    foreach ($principals as $row) {
                        $userIdsYesPermission[] = $row;
                    }
                }
                $userIdsYesPermission = array_unique($userIdsYesPermission);

                if($countNoMenu > 0) {
                    if($classList != '') {
                        $classList = trim($classList, " ,");
                    }
                    $message = sprintf(__("%s: There are  %s/%s classes (%s) have not menus in this week"), $school['page_title'], $countNoMenu, count($classes), convertText4Web($classList));
                    if(count($userIdsYesPermission) > 0) {
                        $userDao->postNotifications($userIdsYesPermission, NOTIFICATION_REMIND_MENU, NOTIFICATION_NODE_TYPE_SCHOOL,
                            $school['page_id'], $message, convertText4Web($school['page_name']), '', 1);
                    }
                }
            }
        }

        $db->commit();
    }
}
catch (Exception $e) {
    $db->rollback();
    return_json( array('error' => true, 'message' => $e->getMessage()) );
} finally {
    $db->autocommit(true);
}
?>