<?php
/**
 * Hàm chạy các các nhiệm vụ ngầm theo lịch
 *
 * @package ConIu v1
 * @author TaiLA
 */

$path = str_replace('includes/ajax/ci/php_script/schedule', '', dirname(__FILE__));
set_include_path($path);

// fetch bootstrap
require('bootstrap.php');

include_once(DAO_PATH . 'dao_user.php');

$userDao = new UserDAO();

try {
    $db->autocommit(false);

    $db->begin_transaction();

    $userLastActive = getUserLastActive();
    if(!is_null($userLastActive)) {
        foreach ($userLastActive as $user_id => $time) {
            $userDao->updateUserLastActive($time, $user_id);
        }
    }
    $db->commit();
} catch (Exception $e) {
    echo $e->getMessage();
}
?>