<?php
/**
 * Created by PhpStorm.
 * User: Tuan Anh
 * Date: 9/14/17
 * Time: 11:57 PM
 */

class CacheMemcache {

    var $iTtl = 0; // Time To Live
    var $bEnabled = false; // Memcache enabled?
    var $oCache = null;

    // constructor
    function __construct() {
        if (class_exists('Memcached')) {
            $this->oCache = new Memcached();
            $this->bEnabled = true;
            $connected = $this->oCache->addServer('localhost', 11211);
            // If connection was successful, return the Memcache object.
            if (!$connected) {
                $this->oCache = null;
                $this->bEnabled = true;
            }
        } elseif (class_exists('Memcache')) {
            $this->oCache = new Memcache();
            $this->bEnabled = true;
            if (! $this->oCache->connect('localhost', 11211))  { // Instead 'localhost' here can be IP
                $this->oCache = null;
                $this->bEnabled = true;
            }
        }
    }

    // get data from cache server
    function getData($sKey) {
        $vData = $this->oCache->get($sKey);
        return false === $vData ? null : $vData;
    }

    // save data to cache server
    function setData($sKey, $vData) {
        //Use MEMCACHE_COMPRESSED to store the item compressed (uses zlib).
        //return $this->oCache->set($sKey, $vData, 0, $this->iTtl);
        return $this->oCache->set($sKey, $vData);
    }

    // delete data from cache server
    function delData($sKey) {
        return $this->oCache->delete($sKey);
    }
}

?>