<?php
/**
 * Created by PhpStorm.
 * User: QuanND
 * Date: 12/28/2016
 * Time: 2:31 PM
 */

/**
 * Đọc nội dung file Excel để lấy ra thông tin trẻ cần import vào hệ thống.
 *
 * @param $inputFileName
 * @return array
 * @throws PHPExcel_Exception
 */
function readChildInfoInExcelFile($inputFileName)
{
    global $user;
    include_once(DAO_PATH . 'dao_child.php');
    $childDao = new ChildDAO();
    include_once(ABSPATH.'includes/libs/PHPExcel/PHPExcel/IOFactory.php');
    include_once(ABSPATH.'includes/functions.php');
    $sheetInfo = array();
    $sheetInfo['error'] = 0;
    try {
        $inputFileType = PHPExcel_IOFactory::identify($inputFileName); //Identify the file
        $objReader = PHPExcel_IOFactory::createReader($inputFileType); //Creating the reader
        $objPHPExcel = $objReader->load($inputFileName); //Loading the file
    } catch (Exception $e) {
        $sheetInfo['error'] = 1;
        $sheetInfo['message'] = __("Can not read the file");

        return $sheetInfo;
    }

    $sheet = $objPHPExcel->getSheet(0); //Lấy ra danh sách là sheet đầu tiên
    //Phần xử lý với 1 sheet
    $checkOriginalFile = $sheet->getCell('Q3');
    if (strpos($checkOriginalFile, 'www.coniu.vn') !== false) { //Kiểm tra xem có đúng file mẫu dùng cho coniu không
        $row = 5;
        $index = 0;
        $child_list = array();
        while (true) { //Đọc đến khi gặp dòng trống thì thôi
            $row++; //Bắt đầu đọc từ dòng thứ 6

            $childInfo = array();
            $childInfo['error'] = 0;
            $childInfo['index'] = ++$index;
            //Lấy ra thông tin họ và tên lót
            $childInfo['last_name'] = (is_empty($sheet->getCell('C' . $row)) || is_empty(trim($sheet->getCell('C' . $row)))) ? "" : trim($sheet->getCell('C' . $row));
            $childInfo['first_name'] = (is_empty($sheet->getCell('D' . $row)) || is_empty(trim($sheet->getCell('D' . $row)))) ? "" : trim($sheet->getCell('D' . $row));

            //Nếu gặp dòng có 'họ và tên lót' và 'tên' đều rỗng thì coi như hết danh sách.
            if (strcmp($childInfo['last_name'], "") == 0 && strcmp($childInfo['first_name'], "") == 0) {
                break;
            }
            $childInfo['gender'] = (!is_empty($sheet->getCell('F' . $row)) && strcmp(trim($sheet->getCell('F' . $row)), 'Nam') == 0) ? MALE : FEMALE;
            // Thêm tên phụ huynh vào file excel
            // thông tin mẹ
            $childInfo['parent_name'] = (is_empty($sheet->getCell('G' . $row)) || is_empty(trim($sheet->getCell('G' . $row)))) ? "" : trim($sheet->getCell('G' . $row));
            $childInfo['parent_phone'] = (is_empty($sheet->getCell('H' . $row)) || is_empty(trim($sheet->getCell('H' . $row)))) ? "" : trim($sheet->getCell('H' . $row));
            $childInfo['parent_job'] = (is_empty($sheet->getCell('I' . $row)) || is_empty(trim($sheet->getCell('I' . $row)))) ? "" : trim($sheet->getCell('I' . $row));
            // thông tin bố
            $childInfo['parent_name_dad'] = (is_empty($sheet->getCell('J' . $row)) || is_empty(trim($sheet->getCell('J' . $row)))) ? "" : trim($sheet->getCell('J' . $row));
            $childInfo['parent_phone_dad'] = (is_empty($sheet->getCell('K' . $row)) || is_empty(trim($sheet->getCell('K' . $row)))) ? "" : trim($sheet->getCell('K' . $row));
            $childInfo['parent_job_dad'] = (is_empty($sheet->getCell('L' . $row)) || is_empty(trim($sheet->getCell('L' . $row)))) ? "" : trim($sheet->getCell('L' . $row));

            $childInfo['address'] = (is_empty($sheet->getCell('O' . $row)) || is_empty(trim($sheet->getCell('O' . $row)))) ? "" : trim($sheet->getCell('O' . $row));
            //$childInfo['birthday'] = (is_empty($sheet->getCell('E' . $row)) || is_empty(trim($sheet->getCell('E' . $row)))) ? "" : trim($sheet->getCell('E' . $row));
            if((is_empty($sheet->getCell('E' . $row)) || is_empty(trim($sheet->getCell('E' . $row))))) {
                $childInfo['birthday'] = "";
            } else {
                if (PHPExcel_Shared_Date::isDateTime($sheet->getCell('E' . $row))) {
                    $childInfo['birthday'] =  trim($sheet->getCell('E' . $row)->getFormattedValue());
                } else {
                    $childInfo['birthday'] =  trim($sheet->getCell('E' . $row)->getValue());
                }
            }
            //$childInfo['begin_at'] = (is_empty($sheet->getCell('L' . $row)) || is_empty(trim($sheet->getCell('L' . $row)))) ? "" : trim($sheet->getCell('L' . $row));

            if((is_empty($sheet->getCell('P' . $row)) || is_empty(trim($sheet->getCell('P' . $row))))) {
                $childInfo['begin_at'] = "";
            } else {
                if (PHPExcel_Shared_Date::isDateTime($sheet->getCell('P' . $row))) {
                    $childInfo['begin_at'] =  trim($sheet->getCell('P' . $row)->getFormattedValue());
                } else {
                    $childInfo['begin_at'] =  trim($sheet->getCell('P' . $row)->getValue());
                }
            }

            $childInfo['description'] = (is_empty($sheet->getCell('Q' . $row)) || is_empty(trim($sheet->getCell('Q' . $row)))) ? "" : trim($sheet->getCell('Q' . $row));

            if ((strcmp($childInfo['last_name'], "") == 0) || (strcmp($childInfo['first_name'], "") == 0) || (strcmp($childInfo['gender'], "") == 0) || (strcmp($childInfo['birthday'], "") == 0)) {
                $childInfo['error'] = 1;
                $childInfo['message'] = __("Mandatory information is missing");
            } else {
                $childInfo['child_name'] = $childInfo['last_name'] . " " . $childInfo['first_name'];
                $childInfo['name_for_sort'] = convert_to_en_4sort($childInfo['first_name'] . $childInfo['last_name']);
                $childInfo['child_code'] = (is_empty($sheet->getCell('B' . $row)) || is_empty(trim($sheet->getCell('B' . $row)))) ? generateCode($childInfo['child_name']) : trim($sheet->getCell('B' . $row));

                // Kiểm tra xem trẻ có trong hệ thống chưa (bằng child_code)
                $temp = $childDao->getChildByCode($childInfo['child_code']);
                if(!is_null($temp)) {
                    $childInfo['error'] = 1;
                    $childInfo['message'] = __("The student code you entered already exists on the system");
                }
                $childInfo['user_id'] = $user->_data['user_id'];
            }

            $childInfo['parent_email'] = (is_empty($sheet->getCell('M' . $row)) || is_empty(trim($sheet->getCell('M' . $row)))) ? "" : trim($sheet->getCell('M' . $row));
            $childInfo['create_parent_account'] = (is_empty($sheet->getCell('N' . $row)) || strcmp(trim($sheet->getCell('N' . $row)), 'Không') == 0) ? 0 : 1;
//            if (!valid_name($childInfo['parent_name'])) {
//                //Nếu để trường tạo account phụ huynh là rỗng hoặc Không nhập thì không tạo tài khoản phụ huynh
//                $childInfo['create_account_result'] = __("Parent name is incorrect"); //Nếu tên sai định dạng
//            }
            if (!valid_email($childInfo['parent_email']) && $childInfo['parent_email'] != '') {
                //Nếu để trường tạo account phụ huynh là rỗng hoặc Không nhập thì vẫn tạo tài khoản phụ huynh với email = 'null'

                $childInfo['create_account_result'] = __("Create parent account unsuccessfully, please enter your email correctly or let it empty"); //Nếu địa chỉ email sai định dạng thì không tạo tài khoản phụ huynh
            }

            $child_list[] = $childInfo;
        }
        if (count($child_list) > 0) {
            $sheetInfo['child_list'] = $child_list;
        } else {
            $sheetInfo['error'] = 1;
            $sheetInfo['message'] = __("No data in the file");
        }
    } else {
        $sheetInfo['error'] = 1;
        $sheetInfo['message'] = __("The file is incorrect. Please download the template file from Inet and keep it's format");
    }
    return $sheetInfo;
}

/**
 * Đọc nội dung file Excel để lấy ra thông tin LỊCH HỌC cần import vào hệ thống.
 *
 * @param $inputFileName
 * @return array
 * @throws PHPExcel_Exception
 */
function readScheduleInfoInExcelFile($inputFileName)
{
    global $user;

    include_once(ABSPATH.'includes/libs/PHPExcel/PHPExcel/IOFactory.php');
    include_once(ABSPATH.'includes/functions.php');
    $sheetInfo = array();
    $sheetInfo['error'] = 0;
    try {
        $inputFileType = PHPExcel_IOFactory::identify($inputFileName); //Identify the file
        $objReader = PHPExcel_IOFactory::createReader($inputFileType); //Creating the reader
        $objPHPExcel = $objReader->load($inputFileName); //Loading the file
    } catch (Exception $e) {
        $sheetInfo['error'] = 1;
        $sheetInfo['message'] = __("Can not read the file");

        return $sheetInfo;
    }

    $sheet = $objPHPExcel->getSheet(0); //Lấy ra danh sách là sheet đầu tiên
    //Phần xử lý với 1 sheet

    $checkOriginalFile = $sheet->getCell('I3')->getValue();

    if (strpos($checkOriginalFile, 'www.coniu.vn') !== false) { //Kiểm tra xem có đúng file mẫu dùng cho coniu không
        $row = 4;
        $index = 0;
        $subject_list = array();
        while (true) { //Đọc đến khi gặp dòng trống thì thôi
            $row++; //Bắt đầu đọc từ dòng thứ 5

            $subject_detail = array();
            $subject_detail['error'] = 0;
            $subject_detail['index'] = ++$index;
            //Lấy ra thông tin thời gian và hoạt động
            $subject_detail['start'] = (is_empty($sheet->getCell('B' . $row)) || is_empty(trim($sheet->getCell('B' . $row)))) ? "" : trim($sheet->getCell('B' . $row));
            $subject_detail['category'] = (is_empty($sheet->getCell('C' . $row)) || is_empty(trim($sheet->getCell('C' . $row)))) ? "" : trim($sheet->getCell('C' . $row));
            $subject_detail['mon'] = (is_empty($sheet->getCell('D' . $row)) || is_empty(trim($sheet->getCell('D' . $row)))) ? "" : trim($sheet->getCell('D' . $row));
            $subject_detail['tue'] = (is_empty($sheet->getCell('E' . $row)) || is_empty(trim($sheet->getCell('E' . $row)))) ? "" : trim($sheet->getCell('E' . $row));
            //Nếu gặp dòng có 'thời gian' và 'hoạt động', 'Thứ 2', 'Thứ 3' đều rỗng thì coi như hết danh sách.
            if (strcmp($subject_detail['start'], "") == 0 && strcmp($subject_detail['category'], "") == 0 && strcmp($subject_detail['mon'], "") == 0 && strcmp($subject_detail['tue'], "") == 0) {
                break;
            }
            $subject_detail['wed'] = (is_empty($sheet->getCell('F' . $row)) || is_empty(trim($sheet->getCell('F' . $row)))) ? "" : trim($sheet->getCell('F' . $row));
            $subject_detail['thu'] = (is_empty($sheet->getCell('G' . $row)) || is_empty(trim($sheet->getCell('G' . $row)))) ? "" : trim($sheet->getCell('G' . $row));
            $subject_detail['fri'] = (is_empty($sheet->getCell('H' . $row)) || is_empty(trim($sheet->getCell('H' . $row)))) ? "" : trim($sheet->getCell('H' . $row));
            $subject_detail['sat'] = (is_empty($sheet->getCell('I' . $row)) || is_empty(trim($sheet->getCell('I' . $row)))) ? "" : trim($sheet->getCell('I' . $row));

            if ((strcmp($subject_detail['start'], "") == 0)) {
                $subject_detail['error'] = 1;
                $subject_detail['message'] = __("You must enter time");
                throw new Exception(__("You have not entered enough time columns, please check again your Excel file"));
            }

            $subject_list[] = $subject_detail;
        }
        if (count($subject_list) > 0) {
            $sheetInfo['subject_list'] = $subject_list;
        } else {
            $sheetInfo['error'] = 1;
            $sheetInfo['message'] = __("No data in the file");
        }
    } else {
        $sheetInfo['error'] = 1;
        $sheetInfo['message'] = __("The file is incorrect. Please download the template file from Inet and keep it's format");
    }
    return $sheetInfo;
}

/**
 * Đọc nội dung file Excel để lấy ra thông tin Thực đơn cần import vào hệ thống.
 *
 * @param $inputFileName
 * @return array
 * @throws PHPExcel_Exception
 */
function readMenuInfoInExcelFile($inputFileName)
{
    global $user;

    include_once(ABSPATH.'includes/libs/PHPExcel/PHPExcel/IOFactory.php');
    include_once(ABSPATH.'includes/functions.php');
    $sheetInfo = array();
    $sheetInfo['error'] = 0;
    try {
        $inputFileType = PHPExcel_IOFactory::identify($inputFileName); //Identify the file
        $objReader = PHPExcel_IOFactory::createReader($inputFileType); //Creating the reader
        $objPHPExcel = $objReader->load($inputFileName); //Loading the file
    } catch (Exception $e) {
        $sheetInfo['error'] = 1;
        $sheetInfo['message'] = __("Can not read the file");

        return $sheetInfo;
    }

    $sheet = $objPHPExcel->getSheet(0); //Lấy ra danh sách là sheet đầu tiên
    //Phần xử lý với 1 sheet

    $checkOriginalFile = $sheet->getCell('I3')->getValue();

    if (strpos($checkOriginalFile, 'www.coniu.vn') !== false) { //Kiểm tra xem có đúng file mẫu dùng cho coniu không
        $row = 4;
        $index = 0;
        $menu_list = array();
        while (true) { //Đọc đến khi gặp dòng trống thì thôi
            $row++; //Bắt đầu đọc từ dòng thứ 5

            $menu_detail = array();
            $menu_detail['error'] = 0;
            $menu_detail['index'] = ++$index;
            //Lấy ra thông tin thời gian và hoạt động
            $menu_detail['start'] = (is_empty($sheet->getCell('B' . $row)) || is_empty(trim($sheet->getCell('B' . $row)))) ? "" : trim($sheet->getCell('B' . $row));
            $menu_detail['meal'] = (is_empty($sheet->getCell('C' . $row)) || is_empty(trim($sheet->getCell('C' . $row)))) ? "" : trim($sheet->getCell('C' . $row));
            $menu_detail['mon'] = (is_empty($sheet->getCell('D' . $row)) || is_empty(trim($sheet->getCell('D' . $row)))) ? "" : trim($sheet->getCell('D' . $row));
            $menu_detail['tue'] = (is_empty($sheet->getCell('E' . $row)) || is_empty(trim($sheet->getCell('E' . $row)))) ? "" : trim($sheet->getCell('E' . $row));
            //Nếu gặp dòng có 'thời gian' và 'hoạt động', 'Thứ 2', 'Thứ 3' đều rỗng thì coi như hết danh sách.
            if (strcmp($menu_detail['start'], "") == 0 && strcmp($menu_detail['meal'], "") == 0 && strcmp($menu_detail['mon'], "") == 0 && strcmp($menu_detail['tue'], "") == 0) {
                break;
            }
            $menu_detail['wed'] = (is_empty($sheet->getCell('F' . $row)) || is_empty(trim($sheet->getCell('F' . $row)))) ? "" : trim($sheet->getCell('F' . $row));
            $menu_detail['thu'] = (is_empty($sheet->getCell('G' . $row)) || is_empty(trim($sheet->getCell('G' . $row)))) ? "" : trim($sheet->getCell('G' . $row));
            $menu_detail['fri'] = (is_empty($sheet->getCell('H' . $row)) || is_empty(trim($sheet->getCell('H' . $row)))) ? "" : trim($sheet->getCell('H' . $row));
            $menu_detail['sat'] = (is_empty($sheet->getCell('I' . $row)) || is_empty(trim($sheet->getCell('I' . $row)))) ? "" : trim($sheet->getCell('I' . $row));

            if ((strcmp($menu_detail['start'], "") == 0)) {
                $menu_detail['error'] = 1;
                $menu_detail['message'] = __("You must enter time");
                throw new Exception(__("You have not entered enough time columns, please check again your Excel file"));
            }

            $menu_list[] = $menu_detail;
        }
        if (count($menu_list) > 0) {
            $sheetInfo['menu_list'] = $menu_list;
        } else {
            $sheetInfo['error'] = 1;
            $sheetInfo['message'] = __("No data in the file");
        }
    } else {
        $sheetInfo['error'] = 1;
        $sheetInfo['message'] = __("The file is incorrect. Please download the template file from Inet and keep it's format");
    }
    return $sheetInfo;
}

/**
 * Đọc bảng điểm cấp 2
 *
 * @param $inputFileName
 * @return array
 * @throws PHPExcel_Exception
 */
function readPointsInExcelFileC2($inputFileName)
{
//    global $user;
    include_once(DAO_PATH . 'dao_child.php');
    $childDao = new ChildDAO();
//    include_once(ABSPATH.'includes/libs/PHPExcel/PHPExcel/IOFactory.php');
//    include_once(ABSPATH.'includes/functions.php');
    $sheetInfo = array();
    $sheetInfo['error'] = 0;

    define('M1', 'E');
    define('M2', 'F');
    define('M3', 'G');
/* UPDATE START - ManhDD 06/04/2021 */
//    define('P1', 'H');
//    define('P2', 'I');
//    define('P3', 'J');
//    define('P4', 'K');
//    define('V1', 'L');
//    define('V2', 'M');
//    define('V3', 'N');
//    define('V4', 'O');
//    define('V5', 'P');
//    define('V6', 'Q');
//    define('V7', 'R');
//    define('V8', 'S');
    define('HK', 'H');
//    define('TBHK', 'U');
    /* UPDATE END - ManhDD 06/04/2021 */
    try {
        $inputFileType = PHPExcel_IOFactory::identify($inputFileName); //Identify the file
        $objReader = PHPExcel_IOFactory::createReader($inputFileType); //Creating the reader
        $objPHPExcel = $objReader->load($inputFileName); //Loading the file
    } catch (Exception $e) {
        $sheetInfo['error'] = 1;
        $sheetInfo['message'] = __("Can not read the file");
        // $sheetInfo['message'] = $e;

        return $sheetInfo;
    }


    $sheet = $objPHPExcel->getSheet(0); //Lấy ra danh sách là sheet đầu tiên
    //Phần xử lý với 1 sheet

    $row = 7;
    $index = 0;
    $child_list = array();
    while (true) { //Đọc đến khi gặp dòng trống thì thôi
        $row++; //Bắt đầu đọc từ dòng thứ 8

        $childInfo = array();
        $childInfo['error'] = 0;
        $childInfo['index'] = ++$index;
        // Lấy thông tin mã học sinh
        $childInfo['child_code'] = (is_empty($sheet->getCell('B' . $row)) || is_empty(trim($sheet->getCell('B' . $row)))) ? "" : trim($sheet->getCell('B' . $row));
        $childInfo['child_lastname'] = (is_empty($sheet->getCell('C' . $row)) || is_empty(trim($sheet->getCell('C' . $row)))) ? "" : trim($sheet->getCell('C' . $row));
        $childInfo['child_firstname'] = (is_empty($sheet->getCell('D' . $row)) || is_empty(trim($sheet->getCell('D' . $row)))) ? "" : trim($sheet->getCell('D' . $row));
        $childInfo['child_fullname'] = $childInfo['child_lastname'] . ' ' . $childInfo['child_firstname'];

        //Nếu gặp dòng không có mã học sinh thì coi như hết danh sách.
        if (strcmp($childInfo['child_code'], "") == 0) {
            break;
        }
/* UPDATE START - ManhDD 06/04/2021 */
        // Lấy thông tin điểm từng tháng
        $childInfo['m1'] = (!is_empty($sheet->getCell(M1 . $row)) && !is_empty(trim($sheet->getCell(M1 . $row)))) ? trim($sheet->getCell(M1 . $row)) : 'null';
        $childInfo['m2'] = (!is_empty($sheet->getCell(M2 . $row)) && !is_empty(trim($sheet->getCell(M2 . $row)))) ? trim($sheet->getCell(M2 . $row)) : 'null';
        $childInfo['m3'] = (!is_empty($sheet->getCell(M3 . $row)) && !is_empty(trim($sheet->getCell(M3 . $row)))) ? trim($sheet->getCell(M3 . $row)) : 'null';
//
//        // Lấy thông tin điểm 15 phút
//        $childInfo['p1'] = (!is_empty($sheet->getCell(P1 . $row)) && !is_empty(trim($sheet->getCell(P1 . $row)))) ? trim($sheet->getCell(P1 . $row)) : 'null';
//        $childInfo['p2'] = (!is_empty($sheet->getCell(P2 . $row)) && !is_empty(trim($sheet->getCell(P2 . $row)))) ? trim($sheet->getCell(P2 . $row)) : 'null';
//        $childInfo['p3'] = (!is_empty($sheet->getCell(P3 . $row)) && !is_empty(trim($sheet->getCell(P3 . $row)))) ? trim($sheet->getCell(P3 . $row)) : 'null';
//
//        // Lấy thông tin diểm 1 tiết
//        $childInfo['v1'] = (!is_empty($sheet->getCell(V1 . $row)) && !is_empty(trim($sheet->getCell(V1 . $row)))) ? trim($sheet->getCell(V1 . $row)) : 'null';
//        $childInfo['v2'] = (!is_empty($sheet->getCell(V2 . $row)) && !is_empty(trim($sheet->getCell(V2 . $row)))) ? trim($sheet->getCell(V2 . $row)) : 'null';
//        $childInfo['v3'] = (!is_empty($sheet->getCell(V3 . $row)) && !is_empty(trim($sheet->getCell(V3 . $row)))) ? trim($sheet->getCell(V3 . $row)) : 'null';
//        $childInfo['v4'] = (!is_empty($sheet->getCell(V4 . $row)) && !is_empty(trim($sheet->getCell(V4 . $row)))) ? trim($sheet->getCell(V4 . $row)) : 'null';
//        $childInfo['v5'] = (!is_empty($sheet->getCell(V5 . $row)) && !is_empty(trim($sheet->getCell(V5 . $row)))) ? trim($sheet->getCell(V5 . $row)) : 'null';
//        $childInfo['v6'] = (!is_empty($sheet->getCell(V6 . $row)) && !is_empty(trim($sheet->getCell(V6 . $row)))) ? trim($sheet->getCell(V6 . $row)) : 'null';
//        $childInfo['v7'] = (!is_empty($sheet->getCell(V7 . $row)) && !is_empty(trim($sheet->getCell(V7 . $row)))) ? trim($sheet->getCell(V7 . $row)) : 'null';
//        $childInfo['v8'] = (!is_empty($sheet->getCell(V8 . $row)) && !is_empty(trim($sheet->getCell(V8 . $row)))) ? trim($sheet->getCell(V8 . $row)) : 'null';

        // Lấy thông tin điểm Học kỳ
        $childInfo['hk'] = (!is_empty($sheet->getCell(HK . $row)) && !is_empty(trim($sheet->getCell(HK . $row)))) ? trim($sheet->getCell(HK . $row)) : 'null';

//        // Lấy thông tin điểm trung bình
//        $childInfo['tbhk'] = (!is_empty($sheet->getCell(TBHK . $row)->getCalculatedValue()) && !is_empty(trim($sheet->getCell(TBHK . $row)->getCalculatedValue()))) ? trim($sheet->getCell(TBHK . $row)->getCalculatedValue()) : 'null';

        $child_list[] = $childInfo;
    }
    if (count($child_list) > 0) {
        $sheetInfo['child_list'] = $child_list;
    } else {
        $sheetInfo['error'] = 1;
        $sheetInfo['message'] = __("No data in the file");
    }
    return $sheetInfo;
}
//ADD START MANHDD 15/06/2021
/**
* Đọc bảng điểm chung ( cả VN và camp - tất cả các cấp )
*
 * @param $inputFileName
* @return array
 * @throws PHPExcel_Exception
*/
function readPointsInExcelFile($inputFileName)
{
//    global $user;
    include_once(DAO_PATH . 'dao_child.php');
    $childDao = new ChildDAO();
//    include_once(ABSPATH.'includes/libs/PHPExcel/PHPExcel/IOFactory.php');
//    include_once(ABSPATH.'includes/functions.php');
    $sheetInfo = array();
    $sheetInfo['error'] = 0;

    define('M1', 'E');
    define('M2', 'F');
    define('M3', 'G');
    /* UPDATE START - ManhDD 06/04/2021 */
//    define('P1', 'H');
//    define('P2', 'I');
//    define('P3', 'J');
//    define('P4', 'K');
//    define('V1', 'L');
//    define('V2', 'M');
//    define('V3', 'N');
//    define('V4', 'O');
//    define('V5', 'P');
//    define('V6', 'Q');
//    define('V7', 'R');
//    define('V8', 'S');
    define('HK', 'H');
//    define('TBHK', 'U');
    /* UPDATE END - ManhDD 06/04/2021 */
    try {
        $inputFileType = PHPExcel_IOFactory::identify($inputFileName); //Identify the file
        $objReader = PHPExcel_IOFactory::createReader($inputFileType); //Creating the reader
        $objPHPExcel = $objReader->load($inputFileName); //Loading the file
    } catch (Exception $e) {
        $sheetInfo['error'] = 1;
        $sheetInfo['message'] = __("Can not read the file");
        // $sheetInfo['message'] = $e;

        return $sheetInfo;
    }


    $sheet = $objPHPExcel->getSheet(0); //Lấy ra danh sách là sheet đầu tiên
    //Phần xử lý với 1 sheet

    $row = 7;
    $index = 0;
    $child_list = array();
    while (true) { //Đọc đến khi gặp dòng trống thì thôi
        $row++; //Bắt đầu đọc từ dòng thứ 8

        $childInfo = array();
        $childInfo['error'] = 0;
        $childInfo['index'] = ++$index;
        // Lấy thông tin mã học sinh
        $childInfo['child_code'] = (is_empty($sheet->getCell('B' . $row)) || is_empty(trim($sheet->getCell('B' . $row)))) ? "" : trim($sheet->getCell('B' . $row));
        $childInfo['child_lastname'] = (is_empty($sheet->getCell('C' . $row)) || is_empty(trim($sheet->getCell('C' . $row)))) ? "" : trim($sheet->getCell('C' . $row));
        $childInfo['child_firstname'] = (is_empty($sheet->getCell('D' . $row)) || is_empty(trim($sheet->getCell('D' . $row)))) ? "" : trim($sheet->getCell('D' . $row));
        $childInfo['child_fullname'] = $childInfo['child_lastname'] . ' ' . $childInfo['child_firstname'];

        //Nếu gặp dòng không có mã học sinh thì coi như hết danh sách.
        if (strcmp($childInfo['child_code'], "") == 0) {
            break;
        }
        /* UPDATE START - ManhDD 06/04/2021 */
        // Lấy thông tin điểm từng tháng
        $childInfo['m1'] = (!is_empty($sheet->getCell(M1 . $row)) && !is_empty(trim($sheet->getCell(M1 . $row)))) ? trim($sheet->getCell(M1 . $row)) : 'null';
        $childInfo['m2'] = (!is_empty($sheet->getCell(M2 . $row)) && !is_empty(trim($sheet->getCell(M2 . $row)))) ? trim($sheet->getCell(M2 . $row)) : 'null';
        $childInfo['m3'] = (!is_empty($sheet->getCell(M3 . $row)) && !is_empty(trim($sheet->getCell(M3 . $row)))) ? trim($sheet->getCell(M3 . $row)) : 'null';
//
//        // Lấy thông tin điểm 15 phút
//        $childInfo['p1'] = (!is_empty($sheet->getCell(P1 . $row)) && !is_empty(trim($sheet->getCell(P1 . $row)))) ? trim($sheet->getCell(P1 . $row)) : 'null';
//        $childInfo['p2'] = (!is_empty($sheet->getCell(P2 . $row)) && !is_empty(trim($sheet->getCell(P2 . $row)))) ? trim($sheet->getCell(P2 . $row)) : 'null';
//        $childInfo['p3'] = (!is_empty($sheet->getCell(P3 . $row)) && !is_empty(trim($sheet->getCell(P3 . $row)))) ? trim($sheet->getCell(P3 . $row)) : 'null';
//
//        // Lấy thông tin diểm 1 tiết
//        $childInfo['v1'] = (!is_empty($sheet->getCell(V1 . $row)) && !is_empty(trim($sheet->getCell(V1 . $row)))) ? trim($sheet->getCell(V1 . $row)) : 'null';
//        $childInfo['v2'] = (!is_empty($sheet->getCell(V2 . $row)) && !is_empty(trim($sheet->getCell(V2 . $row)))) ? trim($sheet->getCell(V2 . $row)) : 'null';
//        $childInfo['v3'] = (!is_empty($sheet->getCell(V3 . $row)) && !is_empty(trim($sheet->getCell(V3 . $row)))) ? trim($sheet->getCell(V3 . $row)) : 'null';
//        $childInfo['v4'] = (!is_empty($sheet->getCell(V4 . $row)) && !is_empty(trim($sheet->getCell(V4 . $row)))) ? trim($sheet->getCell(V4 . $row)) : 'null';
//        $childInfo['v5'] = (!is_empty($sheet->getCell(V5 . $row)) && !is_empty(trim($sheet->getCell(V5 . $row)))) ? trim($sheet->getCell(V5 . $row)) : 'null';
//        $childInfo['v6'] = (!is_empty($sheet->getCell(V6 . $row)) && !is_empty(trim($sheet->getCell(V6 . $row)))) ? trim($sheet->getCell(V6 . $row)) : 'null';
//        $childInfo['v7'] = (!is_empty($sheet->getCell(V7 . $row)) && !is_empty(trim($sheet->getCell(V7 . $row)))) ? trim($sheet->getCell(V7 . $row)) : 'null';
//        $childInfo['v8'] = (!is_empty($sheet->getCell(V8 . $row)) && !is_empty(trim($sheet->getCell(V8 . $row)))) ? trim($sheet->getCell(V8 . $row)) : 'null';

        // Lấy thông tin điểm Học kỳ
        $childInfo['hk'] = (!is_empty($sheet->getCell(HK . $row)) && !is_empty(trim($sheet->getCell(HK . $row)))) ? trim($sheet->getCell(HK . $row)) : 'null';

//        // Lấy thông tin điểm trung bình
//        $childInfo['tbhk'] = (!is_empty($sheet->getCell(TBHK . $row)->getCalculatedValue()) && !is_empty(trim($sheet->getCell(TBHK . $row)->getCalculatedValue()))) ? trim($sheet->getCell(TBHK . $row)->getCalculatedValue()) : 'null';

        $child_list[] = $childInfo;
    }
    if (count($child_list) > 0) {
        $sheetInfo['child_list'] = $child_list;
    } else {
        $sheetInfo['error'] = 1;
        $sheetInfo['message'] = __("No data in the file");
    }
    return $sheetInfo;
}
//ADD END MANHDD 15/06/2021
/**
 * Import điểm cấp 1
 *
 * @param $inputFileName
 * @return array
 * @throws PHPExcel_Exception
 */
function readPointsInExcelFileC1($inputFileName)
{
//    global $user;
    include_once(DAO_PATH . 'dao_child.php');
    $childDao = new ChildDAO();
//    include_once(ABSPATH.'includes/libs/PHPExcel/PHPExcel/IOFactory.php');
//    include_once(ABSPATH.'includes/functions.php');
    $sheetInfo = array();
    $sheetInfo['error'] = 0;

    define('NX', 'E');
    define('NL', 'F');
    define('PC', 'G');
    define('KTCK', 'H');
    define('XLCK', 'I');

    try {
        $inputFileType = PHPExcel_IOFactory::identify($inputFileName); //Identify the file
        $objReader = PHPExcel_IOFactory::createReader($inputFileType); //Creating the reader
        $objPHPExcel = $objReader->load($inputFileName); //Loading the file
    } catch (Exception $e) {
        $sheetInfo['error'] = 1;
//        $sheetInfo['message'] = __("Can not read the file");
        $sheetInfo['message'] = $e;

        return $sheetInfo;
    }


    $sheet = $objPHPExcel->getSheet(0); //Lấy ra danh sách là sheet đầu tiên
    //Phần xử lý với 1 sheet

    $row = 7;
    $index = 0;
    $child_list = array();
    while (true) { //Đọc đến khi gặp dòng trống thì thôi
        $row++; //Bắt đầu đọc từ dòng thứ 8

        $childInfo = array();
        $childInfo['error'] = 0;
        $childInfo['index'] = ++$index;
        // Lấy thông tin mã học sinh
        $childInfo['child_code'] = (is_empty($sheet->getCell('B' . $row)) || is_empty(trim($sheet->getCell('B' . $row)))) ? "" : trim($sheet->getCell('B' . $row));
        $childInfo['child_lastname'] = (is_empty($sheet->getCell('C' . $row)) || is_empty(trim($sheet->getCell('C' . $row)))) ? "" : trim($sheet->getCell('C' . $row));
        $childInfo['child_firstname'] = (is_empty($sheet->getCell('D' . $row)) || is_empty(trim($sheet->getCell('D' . $row)))) ? "" : trim($sheet->getCell('D' . $row));
        $childInfo['child_fullname'] = $childInfo['child_lastname'] . ' ' . $childInfo['child_firstname'];

        //Nếu gặp dòng không có mã học sinh thì coi như hết danh sách.
        if (strcmp($childInfo['child_code'], "") == 0) {
            break;
        }
        // Lấy thông tin nhận xét
        $childInfo['nx'] = (!is_empty($sheet->getCell(NX . $row)) && !is_empty(trim($sheet->getCell(NX . $row)))) ? trim($sheet->getCell(NX . $row)) : 'null';
        // Lấy thông tin năng lực
        $childInfo['nl'] = (!is_empty($sheet->getCell(NL . $row)) && !is_empty(trim($sheet->getCell(NL . $row)))) ? trim($sheet->getCell(NL . $row)) : 'null';
        // Lấy thông tin phẩm chất
        $childInfo['pc'] = (!is_empty($sheet->getCell(PC . $row)) && !is_empty(trim($sheet->getCell(PC . $row)))) ? trim($sheet->getCell(PC . $row)) : 'null';

        // Lấy thông tin ktck
        $childInfo['kt'] = (!is_empty($sheet->getCell(KTCK . $row)) && !is_empty(trim($sheet->getCell(KTCK . $row)))) ? trim($sheet->getCell(KTCK . $row)) : 'null';
        // Lấy thông tin xếp loại cuối kỳ
        $childInfo['xl'] = (!is_empty($sheet->getCell(XLCK . $row)) && !is_empty(trim($sheet->getCell(XLCK . $row)))) ? trim($sheet->getCell(XLCK . $row)) : 'null';

        $child_list[] = $childInfo;
    }
    if (count($child_list) > 0) {
        $sheetInfo['child_list'] = $child_list;
    } else {
        $sheetInfo['error'] = 1;
        $sheetInfo['message'] = __("No data in the file");
    }
    return $sheetInfo;
}

function readPointsInExcelFileC2All($inputFileName)
{
//    global $user;
    include_once(DAO_PATH . 'dao_child.php');
    $childDao = new ChildDAO();
//    include_once(ABSPATH.'includes/libs/PHPExcel/PHPExcel/IOFactory.php');
//    include_once(ABSPATH.'includes/functions.php');
    $sheetInfo = array();
    $sheetInfo['error'] = 0;

    define('TB', 'G');
    define('TL', 'H');

    try {
        $inputFileType = PHPExcel_IOFactory::identify($inputFileName); //Identify the file
        $objReader = PHPExcel_IOFactory::createReader($inputFileType); //Creating the reader
        $objPHPExcel = $objReader->load($inputFileName); //Loading the file
    } catch (Exception $e) {
        $sheetInfo['error'] = 1;
//        $sheetInfo['message'] = __("Can not read the file");
        $sheetInfo['message'] = $e;

        return $sheetInfo;
    }


    $sheet = $objPHPExcel->getSheet(0); //Lấy ra danh sách là sheet đầu tiên
    //Phần xử lý với 1 sheet

    $row = 7;
    $index = 0;
    $child_list = array();
    while (true) { //Đọc đến khi gặp dòng trống thì thôi
        $row++; //Bắt đầu đọc từ dòng thứ 8

        $childInfo = array();
        $childInfo['error'] = 0;
        $childInfo['index'] = ++$index;
        // Lấy thông tin mã học sinh
        $childInfo['child_code'] = (is_empty($sheet->getCell('B' . $row)) || is_empty(trim($sheet->getCell('B' . $row)))) ? "" : trim($sheet->getCell('B' . $row));
        $childInfo['child_lastname'] = (is_empty($sheet->getCell('C' . $row)) || is_empty(trim($sheet->getCell('C' . $row)))) ? "" : trim($sheet->getCell('C' . $row));
        $childInfo['child_firstname'] = (is_empty($sheet->getCell('D' . $row)) || is_empty(trim($sheet->getCell('D' . $row)))) ? "" : trim($sheet->getCell('D' . $row));
        $childInfo['child_fullname'] = $childInfo['child_lastname'] . ' ' . $childInfo['child_firstname'];

        //Nếu gặp dòng không có mã học sinh thì coi như hết danh sách.
        if (strcmp($childInfo['child_code'], "") == 0) {
            break;
        }
        // Lấy thông tin điểm trung bình năm
        $childInfo['tbyear'] = (!is_empty($sheet->getCell(TB . $row)) && !is_empty(trim($sheet->getCell(TB . $row)))) ? trim($sheet->getCell(TB . $row)) : 'null';

        // Lấy thông tin điểm 15 phút
        $childInfo['tl'] = (!is_empty($sheet->getCell(TL . $row)) && !is_empty(trim($sheet->getCell(TL . $row)))) ? trim($sheet->getCell(TL . $row)) : 'null';

        $child_list[] = $childInfo;
    }
    if (count($child_list) > 0) {
        $sheetInfo['child_list'] = $child_list;
    } else {
        $sheetInfo['error'] = 1;
        $sheetInfo['message'] = __("No data in the file");
    }
    return $sheetInfo;
}
?>