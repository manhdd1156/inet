<?php
/* Khai báo các thông số cấu hình cho từng môi trường
*  MODIFY BY MANHDD 05/04/2021
*/


switch (SYS_URL) {
    //ADD START - ManhDD 03/5/2020
    /*------------------ EDU.MASCOM.COM.VN ------------------*/
    case 'http://localhost:8888':
    case 'https://sll.mobiedu.vn':
        $system["system_url"] = SYS_URL;
        $system["cdn_url"] = SYS_URL;
        $system["geolocation_key"] = "AIzaSyBQBJeWrRcvO0Nn7vIZ3PK6mXp6jFpJrYg";
        $system["facebook_appid"] = "240270499760428";
        $system["facebook_secret"] = "1ef5126da4f66e838325353c6bcfabcc";
        $system["google_appid"] = "462890155298-23r9qmve2e07qbf1kofrloda6aitr0b8.apps.googleusercontent.com";
        $system["google_secret"] = "eiPXjekdiHnt_Yyk_9n4ep1-";
        $system["reCAPTCHA_enabled"] = "0";
        $system["reCAPTCHA_site_key"] = "6Le82BIUAAAAAEAjjlQoWQsUftOqRC8-62k9_dqd";
        $system["reCAPTCHA_secret_key"] = "6Le82BIUAAAAAALcoqwLYZb3wWlA6HW_1ZUHbnk_";
        $system["js_path"] = '';
        $system["css_path"] = '';

        /* Initialize Firebase */
        $system["sync_firebase"] = "1";
        $system["initialize_firebase"] = '
<script src="https://www.gstatic.com/firebasejs/8.6.2/firebase.js"></script>
        <script src="https://www.gstatic.com/firebasejs/8.6.2/firebase-analytics.js"></script>
         <!-- Add Firebase products that you want to use -->
  <script src="https://www.gstatic.com/firebasejs/8.6.2/firebase-auth.js"></script>
  <script src="https://www.gstatic.com/firebasejs/8.6.2/firebase-firestore.js"></script>
        <script>
                var config = {
    apiKey: "AIzaSyAuFWlV7KWX9QrVM5LcDKOjeJH-9238s5Q",
    authDomain: "sll-app-876d5.firebaseapp.com",
    databaseURL: "https://sll-app-876d5-default-rtdb.asia-southeast1.firebasedatabase.app",
    projectId: "sll-app-876d5",
    storageBucket: "sll-app-876d5.appspot.com",
    messagingSenderId: "343071557035",
    appId: "1:343071557035:web:0d94a7eaf6465b2b8ffeee",
    measurementId: "G-EEDCRMJQB1"
  };
           // Initialize Firebase
              firebase.initializeApp(config);
              firebase.analytics();
        </script>';
//        $system["initialize_firebase"] = '<script src="https://www.gstatic.com/firebasejs/4.12.1/firebase.js"></script>
//        <script>
//            var config = {
//                apiKey: "AIzaSyCV6P8v5C5jHV7DXelRVJX86vIjjWTN7P8",
//                authDomain: "coniu-eab23.firebaseapp.com",
//                databaseURL: "https://inet-eab23.firebaseio.com",
//                projectId: "coniu-eab23",
//                storageBucket: "coniu-eab23.appspot.com",
//                messagingSenderId: "462890155298"
//            };
//            firebase.initializeApp(config);
//        </script>';
        /* GOOGLE_API_KEY để push notification thông qua Firebase Cloud Message */
        define('GOOGLE_API_KEY', 'AIzaSyAuFWlV7KWX9QrVM5LcDKOjeJH-9238s5Q');
        /* Key Firebase Admin SDK */
        define('FIREBASE_ADMIN_KEY', 'includes/libs/FirebaseAdmin/sll-app-876d5-firebase-adminsdk-4avd0-7a897259b5.json');

        /* Danh sách các page_id của hệ thống, page do NOGA tạo ra */
        $system_page_ids = array();
        /* Danh sách group_id của hệ thống, group do NOGA tạo ra */
        // $system_group_ids = array(1);
        $system_group_ids = array();

        /* Các page_id, user không nhận thông báo khi có bài viết mới */
        $not_receive_page_ids = array();
        /* Các group_id, user không nhận thông báo khi có bài viết mới */
        $not_receive_group_ids = array();

        /* iOS version */
        define("IOS_VERSION", "2.1");
        /* Android version */
        define("ANDROID_VERSION", "2.7");
        /* Check update khi app cập nhật bản mới */
        define("FORCE_UPDATE", true);

        /* Key memcache */
        define("SCHOOL_KEY", "school_");
        define("CLASS_KEY", "class_");
        define("SUBJECT_KEY", "subject_");
        define("TEACHER_KEY", "teacher_");
        define("CHILD_KEY", "child_");
        define("USER_MANAGE_KEY", "user_manage_");
        define("SCHOOL_STATISTIC", "statistic_");
        define("PARENT_REVIEW", "parent_review");

        define("NOGA_STATISTIC_SCHOOL", "noga_statistic_school_");
        define("NOGA_STATISTIC_TOPIC", "noga_statistic_topic");

        define("SCHOOL_CONSECUTIVE_ABSENT", "consecutive_absent_");

        /* Danh sách các mail của NOGA nhận mail */
        $receive_email = "lienhe@noga.vn,quan@noga.vn,ngodaoquan@gmail.com,hoadd@noga.vn,hoaddnoga@gmail.com,anhdct92@gmail.com";
        break;
    //ADD END - ManhDD 03/5/2020
    /*------------------ CONIU.VN ------------------*/
    case 'https://coniu.vn':
        $system["system_url"] = SYS_URL;
        $system["cdn_url"] = SYS_URL;
        $system["geolocation_key"] = "AIzaSyBQBJeWrRcvO0Nn7vIZ3PK6mXp6jFpJrYg";
        $system["facebook_appid"] = "240270499760428";
        $system["facebook_secret"] = "1ef5126da4f66e838325353c6bcfabcc";
        $system["google_appid"] = "462890155298-23r9qmve2e07qbf1kofrloda6aitr0b8.apps.googleusercontent.com";
        $system["google_secret"] = "eiPXjekdiHnt_Yyk_9n4ep1-";
        $system["reCAPTCHA_enabled"] = "1";
        $system["reCAPTCHA_site_key"] = "6Le82BIUAAAAAEAjjlQoWQsUftOqRC8-62k9_dqd";
        $system["reCAPTCHA_secret_key"] = "6Le82BIUAAAAAALcoqwLYZb3wWlA6HW_1ZUHbnk_";
        $system["js_path"] = '_20180330';
        $system["css_path"] = '';

        /* Initialize Firebase */
        $system["sync_firebase"] = "1";
        $system["initialize_firebase"] = '<script src="https://www.gstatic.com/firebasejs/4.12.1/firebase.js"></script>
        <script>
            var config = {
                apiKey: "AIzaSyCV6P8v5C5jHV7DXelRVJX86vIjjWTN7P8",
                authDomain: "coniu-eab23.firebaseapp.com",
                databaseURL: "https://coniu-eab23.firebaseio.com",
                projectId: "coniu-eab23",
                storageBucket: "coniu-eab23.appspot.com",
                messagingSenderId: "462890155298"
            };
            firebase.initializeApp(config);
        </script>';

        /* GOOGLE_API_KEY để push notification thông qua Firebase Cloud Message */
        define('GOOGLE_API_KEY', 'AIzaSyBA8v-QaUO_-Hk7CVe6-H3gb5Qk7N2x1GU');
        /* Key Firebase Admin SDK */
        define('FIREBASE_ADMIN_KEY', 'includes/libs/FirebaseAdmin/coniu-eab23-firebase-adminsdk-5ytxd-c26eae3ec4.json');

        /* Danh sách các page_id của hệ thống, page do NOGA tạo ra */
        $system_page_ids = array();
        /* Danh sách group_id của hệ thống, group do NOGA tạo ra */
        // $system_group_ids = array(1);
        $system_group_ids = array();

        /* Các page_id, user không nhận thông báo khi có bài viết mới */
        $not_receive_page_ids = array();
        /* Các group_id, user không nhận thông báo khi có bài viết mới */
        $not_receive_group_ids = array();

        /* iOS version */
        define("IOS_VERSION", "2.1");
        /* Android version */
        define("ANDROID_VERSION", "2.7");
        /* Check update khi app cập nhật bản mới */
        define("FORCE_UPDATE", true);

        /* Key memcache */
        define("SCHOOL_KEY", "school_");
        define("CLASS_KEY", "class_");
        /* ADD START - ManhDD 05/04/2021 */
        define("SUBJECT_KEY", "subject_");
        /* ADD END - ManhDD 05/04/2021 */
        define("TEACHER_KEY", "teacher_");
        define("CHILD_KEY", "child_");
        define("USER_MANAGE_KEY", "user_manage_");
        define("SCHOOL_STATISTIC", "statistic_");
        define("PARENT_REVIEW", "parent_review");

        define("NOGA_STATISTIC_SCHOOL", "noga_statistic_school_");
        define("NOGA_STATISTIC_TOPIC", "noga_statistic_topic");

        define("SCHOOL_CONSECUTIVE_ABSENT", "consecutive_absent_");

        /* Danh sách các mail của NOGA nhận mail */
        $receive_email = "lienhe@noga.vn,quan@noga.vn,ngodaoquan@gmail.com,hoadd@noga.vn,hoaddnoga@gmail.com,anhdct92@gmail.com";
        break;

    /*------------------ DEV.CONIU.ORG ------------------*/
    case 'http://dev.coniu.org':
        $system["system_url"] = SYS_URL;
        $system["cdn_url"] = SYS_URL;
        $system["geolocation_key"] = "AIzaSyCqZnEFBuAPd17S0mgdgm74N6PrZKg0Dng";
        $system["facebook_appid"] = "596237560581037";
        $system["facebook_secret"] = "2d51eef94d5987c2a3d717e920aa1e48";
        $system["google_appid"] = "158520926133-ldm6q56kp0huovjfdbnnidl88p7kpp95.apps.googleusercontent.com";
        $system["google_secret"] = "TJRreBdNoTvk98JOBIP2PH2u";
        $system["reCAPTCHA_enabled"] = "1";
        $system["reCAPTCHA_site_key"] = "6Lf01hYUAAAAALA5QsQ8z0KyMB1UWN9ikbOJGKm3";
        $system["reCAPTCHA_secret_key"] = "6Lf01hYUAAAAAMjNOEVn6JGT-HflmZUSbHMmH-_8";
        $system["js_path"] = '_20170729';

        /* Initialize Firebase */
        $system["sync_firebase"] = "1";
        $system["initialize_firebase"] = '<script src="https://www.gstatic.com/firebasejs/4.12.1/firebase.js"></script>
            <script>
                var config = {
                    apiKey: "AIzaSyBuvAK4AGb9A6VzRkGOu-_Z-RAIKM6HimM",
                    authDomain: "dev-coniu.firebaseapp.com",
                    databaseURL: "https://dev-coniu.firebaseio.com",
                    projectId: "dev-coniu",
                    storageBucket: "dev-coniu.appspot.com",
                    messagingSenderId: "158520926133"
                  };
                firebase.initializeApp(config);
            </script>';

        /* GOOGLE_API_KEY để push notification thông qua Firebase Cloud Message */
        define('GOOGLE_API_KEY', 'AIzaSyB2D9C198EVideKwd_7doocH6UqUrkxCcU');
        /* Key Firebase Admin SDK */
        define('FIREBASE_ADMIN_KEY', 'includes/libs/FirebaseAdmin/dev-coniu-firebase-adminsdk-wfkmk-17899416c8.json');

        /* Danh sách các page_id của hệ thống, page do NOGA tạo ra */
        $system_page_ids = array();
        /* Danh sách group_id của hệ thống, group do NOGA tạo ra */
        $system_group_ids = array(1);

        /* Các page_id, user không nhận thông báo khi có bài viết mới */
        $not_receive_page_ids = array();
        /* Các group_id, user không nhận thông báo khi có bài viết mới */
        $not_receive_group_ids = array();

        /* iOS version */
        define("IOS_VERSION", "2.1");
        /* Android version */
        define("ANDROID_VERSION", "2.7");
        /* Check update khi app cập nhật bản mới */
        define("FORCE_UPDATE", true);

        /* Key memcache */
        define("SCHOOL_KEY", "school_dev_");
        define("CLASS_KEY", "class_dev_");
        /* ADD START - ManhDD 05/04/2021 */
        define("SUBJECT_KEY", "subject_");
        /* ADD END - ManhDD 05/04/2021 */
        define("TEACHER_KEY", "teacher_dev_");
        define("CHILD_KEY", "child_dev_");
        define("USER_MANAGE_KEY", "user_manage_dev_");
        define("SCHOOL_STATISTIC", "statistic_dev_");
        define("PARENT_REVIEW", "parent_review_dev");

        define("NOGA_STATISTIC_SCHOOL", "noga_statistic_school_dev_");
        define("NOGA_STATISTIC_TOPIC", "noga_statistic_topic_dev");

        define("SCHOOL_CONSECUTIVE_ABSENT", "consecutive_absent_");

        /* Danh sách các mail của NOGA nhận mail */
        $receive_email = "anhdct92@gmail.com,luuanhtai92@gmail.com";
        break;

    /*------------------ TEST.CONIU.ORG ------------------*/
    case 'http://test.coniu.org':
        $system["system_url"] = SYS_URL;
        $system["cdn_url"] = SYS_URL;
        //$system["geolocation_key"] = "AIzaSyD7PrfVCKIrJ7MMdY3hZUeQlnSFwTb1AwU";
        $system["geolocation_key"] = "AIzaSyDLDlZMcTLA9j8qnc8s3ZDO7X74Xkiiwko";
        $system["facebook_appid"] = "1655670347794070";
        $system["facebook_secret"] = "270add3a0ae0302131d04ed6a4162b60";
        $system["google_appid"] = "53361420682-q0niuhn6es0h5p5f9lhe7740ah493udk.apps.googleusercontent.com";
        $system["google_secret"] = "c8dGMDVG0IWY6qqYE4qnqkev";
        $system["reCAPTCHA_enabled"] = "1";
        $system["reCAPTCHA_site_key"] = "6LdB2hYUAAAAAFR72eG-tInD910FeOn4THhYamYz";
        $system["reCAPTCHA_secret_key"] = "6LdB2hYUAAAAAAI6BHaZhcbBUdjq63X4fYX2RFZ2";
        $system["js_path"] = '_20170729';

//        /* Initialize Firebase */
//        $system["sync_firebase"] = "1";
//        $system["initialize_firebase"] = '<script src="https://www.gstatic.com/firebasejs/4.12.1/firebase.js"></script>
//            <script>
//                // Initialize Firebase
//                var config = {
//                    apiKey: "AIzaSyBNGQ8rZh2-lCenD_FsvpcvCnpbdoswhzw",
//                    authDomain: "coniu-test.firebaseapp.com",
//                    databaseURL: "https://inet-test.firebaseio.com",
//                    projectId: "coniu-test",
//                    storageBucket: "coniu-test.appspot.com",
//                    messagingSenderId: "53361420682"
//                };
//            firebase.initializeApp(config);
//        </script>';

        /* Initialize Firebase */
        $system["sync_firebase"] = "0";
        $system["initialize_firebase"] = '<script src="https://www.gstatic.com/firebasejs/4.12.1/firebase.js"></script>
        <script>
            var config = {
                apiKey: "AIzaSyCV6P8v5C5jHV7DXelRVJX86vIjjWTN7P8",
                authDomain: "coniu-eab23.firebaseapp.com",
                databaseURL: "https://coniu-eab23.firebaseio.com",
                projectId: "coniu-eab23",
                storageBucket: "coniu-eab23.appspot.com",
                messagingSenderId: "462890155298"
            };
            firebase.initializeApp(config);
        </script>';

        /* GOOGLE_API_KEY để push notification thông qua Firebase Cloud Message */
        define('GOOGLE_API_KEY', 'AIzaSyB2D9C198EVideKwd_7doocH6UqUrkxCcU');
        /* Key Firebase Admin SDK */
        define('FIREBASE_ADMIN_KEY', 'includes/libs/FirebaseAdmin/coniu-test-firebase-adminsdk-a4913-59e9c89600.json');

        /* Danh sách các page_id của hệ thống, page do NOGA tạo ra */
        $system_page_ids = array();
        /* Danh sách group_id của hệ thống, group do NOGA tạo ra */
        $system_group_ids = array(1);
        /* Các page_id, user không nhận thông báo khi có bài viết mới */
        $not_receive_page_ids = array();
        /* Các group_id, user không nhận thông báo khi có bài viết mới */
        $not_receive_group_ids = array();

        /* iOS version */
        define("IOS_VERSION", "2.1");
        /* Android version */
        define("ANDROID_VERSION", "2.7");
        /* Check update khi app cập nhật bản mới */
        define("FORCE_UPDATE", true);

        /* Key memcache */
        define("SCHOOL_KEY", "school_test_");
        define("CLASS_KEY", "class_test_");
        /* ADD START - ManhDD 05/04/2021 */
        define("SUBJECT_KEY", "subject_");
        /* ADD END - ManhDD 05/04/2021 */
        define("TEACHER_KEY", "teacher_test_");
        define("CHILD_KEY", "child_test_");
        define("USER_MANAGE_KEY", "user_manage_test_");
        define("SCHOOL_STATISTIC", "statistic_test_");
        define("PARENT_REVIEW", "parent_review_test");

        define("NOGA_STATISTIC_SCHOOL", "noga_statistic_school_test_");
        define("NOGA_STATISTIC_TOPIC", "noga_statistic_topic_test");

        define("SCHOOL_CONSECUTIVE_ABSENT", "consecutive_absent_");
        // Key memcache thống kê tương tác của phụ huynh đối với mỗi trường - TaiLA - làm lại
        define("STATISTIC_INTERACTIVE", "statistic_interactive_");

        /* Danh sách các mail của NOGA nhận mail */
        $receive_email = "anhdct92@gmail.com,luuanhtai92@gmail.com";
        break;

    /*------------------ localhost ------------------*/
    case 'http://localhost:8888/server11':
        $system["system_url"] = SYS_URL;
        $system["cdn_url"] = SYS_URL;
        $system["geolocation_key"] = "AIzaSyCqZnEFBuAPd17S0mgdgm74N6PrZKg0Dng";
        $system["facebook_appid"] = "596237560581037";
        $system["facebook_secret"] = "2d51eef94d5987c2a3d717e920aa1e48";
        $system["google_appid"] = "158520926133-ldm6q56kp0huovjfdbnnidl88p7kpp95.apps.googleusercontent.com";
        $system["google_secret"] = "TJRreBdNoTvk98JOBIP2PH2u";
        $system["reCAPTCHA_enabled"] = "0";
        $system["reCAPTCHA_site_key"] = "6Lf01hYUAAAAALA5QsQ8z0KyMB1UWN9ikbOJGKm3";
        $system["reCAPTCHA_secret_key"] = "6Lf01hYUAAAAAMjNOEVn6JGT-HflmZUSbHMmH-_8";
        $system["js_path"] = '';

        /* Initialize Firebase */
        $system["sync_firebase"] = "1";
        $system["initialize_firebase"] = '<script src="https://www.gstatic.com/firebasejs/4.12.1/firebase.js"></script>
            <script>
                var config = {
                    apiKey: "AIzaSyBuvAK4AGb9A6VzRkGOu-_Z-RAIKM6HimM",
                    authDomain: "dev-coniu.firebaseapp.com",
                    databaseURL: "https://dev-coniu.firebaseio.com",
                    projectId: "dev-coniu",
                    storageBucket: "dev-coniu.appspot.com",
                    messagingSenderId: "158520926133"
                  };
                firebase.initializeApp(config);
            </script>';

        /* GOOGLE_API_KEY để push notification thông qua Firebase Cloud Message */
        define('GOOGLE_API_KEY', 'AIzaSyB2D9C198EVideKwd_7doocH6UqUrkxCcU');
        /* GOOGLE_API_KEY để push notification thông qua Firebase Cloud Message */
        define('FIREBASE_ADMIN_KEY', 'includes/libs/FirebaseAdmin/dev-coniu-firebase-adminsdk-wfkmk-17899416c8.json');

        /* Danh sách các page_id của hệ thống, page do NOGA tạo ra */
        $system_page_ids = array();
        /* Danh sách group_id của hệ thống, group do NOGA tạo ra */
        $system_group_ids = array(1);
        /* Các page_id, user không nhận thông báo khi có bài viết mới */
        $not_receive_page_ids = array();
        /* Các group_id, user không nhận thông báo khi có bài viết mới */
        $not_receive_group_ids = array();

        /* iOS version */
        define("IOS_VERSION", "2.1");
        /* Android version */
        define("ANDROID_VERSION", "2.7");
        /* Check update khi app cập nhật bản mới */
        define("FORCE_UPDATE", true);

        /* Key memcache */
        define("SCHOOL_KEY", "school_");
        define("CLASS_KEY", "class_");
        /* ADD START - ManhDD 05/04/2021 */
        define("SUBJECT_KEY", "subject_");
        /* ADD END - ManhDD 05/04/2021 */
        define("TEACHER_KEY", "teacher_");
        define("CHILD_KEY", "child_");
        define("USER_MANAGE_KEY", "user_manage_");
        define("SCHOOL_STATISTIC", "statistic_");
        define("PARENT_REVIEW", "parent_review");

        define("NOGA_STATISTIC_SCHOOL", "noga_statistic_school_");
        define("NOGA_STATISTIC_TOPIC", "noga_statistic_topic");

        define("SCHOOL_CONSECUTIVE_ABSENT", "consecutive_absent_");

        // Key memcache thống kê tương tác của phụ huynh đối với mỗi trường - TaiLA - làm lại
        define("STATISTIC_INTERACTIVE", "statistic_interactive_");

        // Key memcache users online
        define("USERS_ONLINE", "users_online");

        /* Danh sách các mail của NOGA nhận mail */
        $receive_email = "anhdct92@gmail.com,luuanhtai92@gmail.com";
        break;

    case 'http://localhost:8888':
        $system["system_url"] = SYS_URL;
        $system["cdn_url"] = SYS_URL;
        $system["geolocation_key"] = "AIzaSyCqZnEFBuAPd17S0mgdgm74N6PrZKg0Dng";
        $system["facebook_appid"] = "596237560581037";
        $system["facebook_secret"] = "2d51eef94d5987c2a3d717e920aa1e48";
        $system["google_appid"] = "158520926133-ldm6q56kp0huovjfdbnnidl88p7kpp95.apps.googleusercontent.com";
        $system["google_secret"] = "TJRreBdNoTvk98JOBIP2PH2u";
        $system["reCAPTCHA_enabled"] = "0";
        $system["reCAPTCHA_site_key"] = "6Lf01hYUAAAAALA5QsQ8z0KyMB1UWN9ikbOJGKm3";
        $system["reCAPTCHA_secret_key"] = "6Lf01hYUAAAAAMjNOEVn6JGT-HflmZUSbHMmH-_8";
        $system["js_path"] = '';

        /* Initialize Firebase */
        $system["sync_firebase"] = "0";
        $system["initialize_firebase"] = '<script src="https://www.gstatic.com/firebasejs/4.12.1/firebase.js"></script>
        <script>
            var config = {
                apiKey: "AIzaSyCV6P8v5C5jHV7DXelRVJX86vIjjWTN7P8",
                authDomain: "coniu-eab23.firebaseapp.com",
                databaseURL: "https://coniu-eab23.firebaseio.com",
                projectId: "coniu-eab23",
                storageBucket: "coniu-eab23.appspot.com",
                messagingSenderId: "462890155298"
            };
            firebase.initializeApp(config);
        </script>';

        /* GOOGLE_API_KEY để push notification thông qua Firebase Cloud Message */
        define('GOOGLE_API_KEY', 'AIzaSyB2D9C198EVideKwd_7doocH6UqUrkxCcU');
        /* GOOGLE_API_KEY để push notification thông qua Firebase Cloud Message */
        define('FIREBASE_ADMIN_KEY', 'includes/libs/FirebaseAdmin/dev-coniu-firebase-adminsdk-wfkmk-17899416c8.json');

        /* Danh sách các page_id của hệ thống, page do NOGA tạo ra */
        $system_page_ids = array();
        /* Danh sách group_id của hệ thống, group do NOGA tạo ra */
        $system_group_ids = array(1);
        /* Các page_id, user không nhận thông báo khi có bài viết mới */
        $not_receive_page_ids = array();
        /* Các group_id, user không nhận thông báo khi có bài viết mới */
        $not_receive_group_ids = array();

        /* iOS version */
        define("IOS_VERSION", "2.1");
        /* Android version */
        define("ANDROID_VERSION", "2.7");
        /* Check update khi app cập nhật bản mới */
        define("FORCE_UPDATE", true);

        /* Key memcache */
        define("SCHOOL_KEY", "school_");
        define("CLASS_KEY", "class_");
        /* ADD START - ManhDD 05/04/2021 */
        define("SUBJECT_KEY", "subject_");
        /* ADD END - ManhDD 05/04/2021 */
        define("TEACHER_KEY", "teacher_");
        define("CHILD_KEY", "child_");
        define("USER_MANAGE_KEY", "user_manage_");
        define("SCHOOL_STATISTIC", "statistic_");
        define("PARENT_REVIEW", "parent_review");

        define("NOGA_STATISTIC_SCHOOL", "noga_statistic_school_");
        define("NOGA_STATISTIC_TOPIC", "noga_statistic_topic");

        define("SCHOOL_CONSECUTIVE_ABSENT", "consecutive_absent_");

        // Key memcache thống kê tương tác của phụ huynh đối với mỗi trường - TaiLA - làm lại
        define("STATISTIC_INTERACTIVE", "statistic_interactive_");

        // Key memcache users online
        define("USERS_ONLINE", "users_online");

        /* Danh sách các mail của NOGA nhận mail */
        $receive_email = "anhdct92@gmail.com,luuanhtai92@gmail.com";
        break;
}

?>