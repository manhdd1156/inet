<?php

/**
 * Class SchoolDAO: thao tác với bảng pages của hệ thống. Ở đây Trường được lưu trong bảng pages.
 */
class NearbyDAO
{
    /**
     * __construct
     *
     */

    public function __construct()
    {

    }

    /**
     * Lấy ra các địa điểm gần nhất
     *
     * @param array args
     * @return array
     */
    public function getSchoolNearBy(array $args = array())
    {
        global $db, $schoolTypes;
        // Search the rows in the markers table

       /* $strSql = sprintf("SELECT %s FROM pages P INNER JOIN (
                    SELECT *, ( 6371 * acos( cos( radians(%s) ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(%s) ) + sin( radians(%s) ) * sin( radians( lat ) ) ) ) AS distance
                    FROM ci_school_configuration
                    HAVING distance < %s AND end_tuition_fee >= %s AND start_tuition_fee <= %s ORDER BY school_status DESC, distance ASC LIMIT %s , %s ) SC ON P.page_id = SC.school_id",
            // HAVING distance < %s ORDER BY verify DESC, distance LIMIT %s , %s ) SC ON P.page_id = SC.school_id",
            PARAM_SCHOOL, secure($args['lat']), secure($args['lng']), secure($args['lat']), secure($args['distance'], 'int'), secure($args['start_tuition_fee'], 'int'), secure($args['end_tuition_fee'], 'int'), secure($args['start'], 'int', false), secure($args['limit'], 'int', false));*/


        $strSql = sprintf("SELECT %s, SC.distance, SC.school_status FROM pages P INNER JOIN (
                    SELECT *, ( 6371 * acos( cos( radians(%s) ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(%s) ) + sin( radians(%s) ) * sin( radians( lat ) ) ) ) AS distance
                    FROM ci_school_configuration
                    HAVING distance < %s AND end_tuition_fee >= %s AND start_tuition_fee <= %s ORDER BY school_status DESC, distance ASC LIMIT %s , %s ) SC ON P.page_id = SC.school_id",
            // HAVING distance < %s ORDER BY verify DESC, distance LIMIT %s , %s ) SC ON P.page_id = SC.school_id",
            PARAM_SCHOOL, secure($args['lat']), secure($args['lng']), secure($args['lat']), secure($args['distance'], 'int'), secure($args['start_tuition_fee'], 'int'), secure($args['end_tuition_fee'], 'int'), secure($args['start'], 'int', false), secure($args['limit'], 'int', false));

        $result = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $schools = array();
        if ($result->num_rows > 0) {
            while ($rs = $result->fetch_assoc()) {
                foreach ($schoolTypes as $type) {
                    if ($type['type_value'] == $rs['type']) {
                        $rs['type'] = $type['type_name'];
                    }
                }
                /*if ($rs['verify'] == 0) {
                    $rs['page_description'] = 'Đang cập nhật';
                    $rs['short_overview'] = 'Đang cập nhật';
                    $rs['note_for_tuition'] = '';
                    $rs['note_for_admission'] = '';
                    $rs['note_for_facility'] = '';
                    $rs['note_for_service'] = '';
                    $rs['info_leader'] = 'Đang cập nhật';
                    $rs['info_method'] = 'Đang cập nhật';
                    $rs['info_teacher'] = 'Đang cập nhật';
                    $rs['info_nutrition'] = 'Đang cập nhật';
                }*/
                $rs['page_picture'] = get_picture($rs['page_picture'], 'page');
                $rs['page_cover'] = get_picture($rs['page_cover'], 'page');
                $rs['distance'] = round($rs['distance'] , 1);
                $schools[] = $rs;
            }
        }
        return $schools;
    }

    /**
     * Lấy ra các địa điểm gần nhất
     *
     * @param array args
     * @return array
     */
    public function getSchoolByName(array $args = array())
    {
        global $db, $schoolTypes;;
        // Search the rows in the markers table
        $strSql = sprintf('SELECT %1$s FROM pages P INNER JOIN ci_school_configuration SC 
                          ON P.page_id = SC.school_id AND (P.page_name LIKE %2$s OR P.page_title LIKE %2$s) ORDER BY P.page_title ASC LIMIT %3$s, %4$s',
            //ON P.page_id = SC.school_id AND (P.page_name LIKE %2$s OR P.page_title LIKE %2$s) ORDER BY SC.verify DESC, P.page_title ASC LIMIT %3$s, %4$s',
            PARAM_SCHOOL, secure($args['school_name'], 'search'), secure($args['start'], 'int', false), secure($args['limit'], 'int', false));

        $result = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $schools = array();
        if ($result->num_rows > 0) {
            while ($rs = $result->fetch_assoc()) {
                foreach ($schoolTypes as $type) {
                    if ($type['type_value'] == $rs['type']) {
                        $rs['type'] = $type['type_name'];
                    }
                }
                /*if ($rs['verify'] == 0) {
                    $rs['page_description'] = 'Đang cập nhật';
                    $rs['short_overview'] = 'Đang cập nhật';
                    $rs['note_for_tuition'] = '';
                    $rs['note_for_admission'] = '';
                    $rs['note_for_facility'] = '';
                    $rs['note_for_service'] = '';
                    $rs['info_leader'] = 'Đang cập nhật';
                    $rs['info_method'] = 'Đang cập nhật';
                    $rs['info_teacher'] = 'Đang cập nhật';
                    $rs['info_nutrition'] = 'Đang cập nhật';
                }*/
                $rs['page_picture'] = get_picture($rs['page_picture'], 'page');
                $rs['page_cover'] = get_picture($rs['page_cover'], 'page');
                $schools[] = $rs;
            }
        }
        return $schools;
    }

    /**
     * Kiểm tra đầu vào cho hàm insert và update
     *
     * @param array $args
     * @param bool|true $isCreate
     * @throws Exception
     */
    private function validateInput(array $args = array(), $isCreate = true)
    {
        /* validate title */
        if (is_empty($args['title'])) {
            throw new Exception(__("You must enter school name"));
        }
        if (strlen($args['title']) < 3) {
            throw new Exception(__("Name must be at least 3 characters long"));
        }
        /* validate username */
        if (is_empty($args['username'])) {
            throw new Exception(__("You must enter a username for your school"));
        }
        if (!valid_username($args['username'])) {
            throw new Exception(__("Please enter a valid username (a-z0-9_.)"));
        }
        /* validate username */
        if ($isCreate || (strtolower($args['username']) != strtolower($args['username_old']))) {
            if ($this->checkUsername($args['username'])) {
                throw new Exception(__("Sorry, it looks like this username") . " <strong>" . $args['username'] . "</strong> " . __("belongs to an existing one"));
            }
        }
        if ((!is_empty($args['telephone'])) && (strlen($args['telephone']) > 50)) {
            throw new Exception(__("Telephone length must be less than 50 characters long"));
        }
        if ((!is_empty($args['website'])) && (strlen($args['website']) > 50)) {
            throw new Exception(__("Website length must be less than 50 characters long"));
        }
        if ((!is_empty($args['email'])) && (strlen($args['email']) > 50)) {
            throw new Exception(__("Email length must be less than 50 characters long"));
        }
        if ((!is_empty($args['address'])) && (strlen($args['address']) > 400)) {
            throw new Exception(__("Address length must be less than 400 characters long"));
        }
        if (!isset($args['city_id']) || ($args['city_id'] < 1)) {
            throw new Exception(__("You must select a city"));
        }
    }
}

?>