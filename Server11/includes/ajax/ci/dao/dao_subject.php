<?php

/**
 * Class SubjectDAO: Thao tác với bảng subject của hệ thống.
 * ADD NEW BY MANHDD 03/04/2021
 */
class SubjectDAO
{
    /**
     * __construct
     *
     */
    public function __construct()
    {
    }

    /**
     * Kiểm tra subjectname của subject đã tồn tại chưa.
     *
     * @param string $subjectname
     * @return boolean
     */
    private function checkUsername($subjectname)
    {
        global $db;
        $query = $db->query(sprintf("SELECT * FROM ci_subject WHERE name = %s", secure($subjectname))) or _error(SQL_ERROR_THROWEN);

        if ($query->num_rows > 0) {
            return true;
        }
        return false;
    }

    /**
     * Kiểm tra đầu vào cho hàm insert và update
     *
     * @param array $args
     * @param bool|true $isCreate
     * @throws Exception
     */
    private function validateInput(array $args = array(), $isCreate = true)
    {
        /* validate subject name */
        if (is_empty($args['subject_name'])) {
            throw new Exception(__("You must enter subject name"));
        }
    }

    /**
     * Tạo ra một TRƯỜNG trong DB
     *
     * @param array $args
     * @return mixed
     * @throws Exception
     */
    public function createSubjectNoga(array $args = array())
    {
        global $db;
        $this->validateInput($args, true);

        /* insert new subject */
        $strSql = sprintf("INSERT INTO ci_subject (subject_name, re_exam)
                            VALUES (%s, %s)",
            secure($args['subject_name']),
            secure($args['re_exam']));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        $subject_id = $db->insert_id;

        return $subject_id;
    }

    /**
     * Cập nhật thông tin môn học từ màn hình quản lý của nhân viên NOGA
     *
     * @param array $args
     * @throws Exception
     */
    public function editSubjectNoga(array $args = array())
    {
        global $db;
        $this->validateInput($args, false);

        /* update subject */
        $db->query(sprintf("UPDATE ci_subject SET subject_name = %s, re_exam = %s
                    WHERE subject_id = %s", secure($args['subject_name']), secure($args['re_exam'], 'int'), secure($args['subject_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * xóa môn học từ màn hình danh sách môn học của qunar lý noga
     *
     * @param int $subjectId
     * @throws Exception
     */
    public function deleteSubjectNoga($subjectId)
    {
        global $db;

        /* delete ci_subject */
        $db->query(sprintf("DELETE FROM ci_subject WHERE subject_id = %s", secure($subjectId, 'int') )) or _error(SQL_ERROR_THROWEN);
        /* delete ci_subject */
        $db->query(sprintf("DELETE FROM ci_class_level_subject WHERE subject_id = %s", secure($subjectId, 'int') )) or _error(SQL_ERROR_THROWEN);
        /* delete ci_class_subject_teacher */
        $db->query(sprintf("DELETE FROM ci_class_subject_teacher WHERE subject_id = %s", secure($subjectId, 'int') )) or _error(SQL_ERROR_THROWEN);
        /* delete ci_point_c2 */
        $db->query(sprintf("DELETE FROM ci_point_c2 WHERE subject_id = %s", secure($subjectId, 'int') )) or _error(SQL_ERROR_THROWEN);

    }
    /**
     * Lấy ra danh sách tất cả các trường trong hệ thống. Phục vụ cho chức năng quản lý của NOGA
     *
     * @param int $from
     * @param int $limit
     * @return array
     * @throws Exception
     */
    public function getAllSubjects($from = 0, $limit = 0)
    {
        global $db;

        if ($from > 0) {
            $strSql = sprintf("SELECT * FROM ci_subject 
                    ORDER BY subject_id ASC LIMIT %s, %s", secure($from, 'int'), secure($limit, 'int'));
        } else {
            $strSql = sprintf("SELECT * FROM ci_subject 
                    ORDER BY subject_id ASC");
        }

        $subjects = array();
        $get_subjects = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_subjects->num_rows > 0) {
            while ($subject = $get_subjects->fetch_assoc()) {
                $subjects[] = $subject;
            }
        }
        return $subjects;
    }


    /**
     * Lấy ra thông tin của môn học khi biết subject_id
     *
     * @param $subjectId
     * @return array|null
     * @throws Exception
     */
    public function getSubjectNoga($subjectId)
    {
        global $db;

        $strSql = sprintf("SELECT *
                           FROM ci_subject WHERE subject_id = %s", secure($subjectId, 'int'));
        $get_subject = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $subject = null;
        if ($get_subject->num_rows > 0) {
            $subject = $get_subject->fetch_assoc();
        }
        return $subject;
    }

    /**
     * CI-Mobile Lấy ra thông tin subject khi biết subject_id
     *
     * @param $subject_id
     * @return array|null
     * @throws Exception
     */
    public function getSubjectById($subject_id)
    {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_subject WHERE subject_id = %s", secure($subject_id));

        $get_subject = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $subject = null;
        if ($get_subject->num_rows > 0) {
            $subject = $get_subject->fetch_assoc();
//            $school['page_picture'] = get_picture($school['page_picture'], 'school');
//            $school['page_cover'] = get_picture($school['page_cover'], 'school');
        }

        return $subject;
    }

    /**
     * CI-Mobile Lấy ra danh sách subject khi biết gov_class_level_id
     *
     * @param $school_id
     * @param $gov_class_level
     * @param $school_year
     * @return array|null
     * @throws Exception
     */
    public function getSubjectsByClassLevel($school_id, $gov_class_level, $school_year)
    {
        global $db;

        $strSql = sprintf("SELECT cs.* FROM ci_class_level_subject ccls JOIN ci_subject cs ON cs.subject_id = ccls.subject_id WHERE school_id = %s AND gov_class_level = %s AND school_year = %s", secure($school_id), secure($gov_class_level), secure($school_year));

        $get_subject = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $subjects = array();
        if ($get_subject->num_rows > 0) {
            while ($subject = $get_subject->fetch_assoc()) {
                $subjects[] = $subject;
            }
            array_push($subject, $get_subject->fetch_assoc());
//            $school['page_picture'] = get_picture($school['page_picture'], 'school');
//            $school['page_cover'] = get_picture($school['page_cover'], 'school');
        }

        return $subjects;
    }

    /**
     *  Lấy ra danh sách subject khi biết class_id
     *
     * @param $class_id
     * @param $school_id
     * @return array|null
     * @throws Exception
     */
    public function getSubjectsByClassId($class_id,$school_id)
    {
        global $db;

        $strSql = sprintf("SELECT ccls.* FROM (SELECT ccl.* FROM `ci_class_level` ccl JOIN `groups` g ON g.class_level_id = ccl.class_level_id where g.group_id = %s ) x JOIN `ci_class_level_subject` ccls ON ccls.gov_class_level = x.gov_class_level AND ccls.school_id = %s", secure($class_id), secure($school_id,'int'));

        $get_subject = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $subjects = array();
        if ($get_subject->num_rows > 0) {
            while ($subject = $get_subject->fetch_assoc()) {
                $subjects[] = $subject;
            }
            array_push($subject, $get_subject->fetch_assoc());
//            $school['page_picture'] = get_picture($school['page_picture'], 'school');
//            $school['page_cover'] = get_picture($school['page_cover'], 'school');
        }

        return $subjects;
    }

    /**
     * CI-Mobile Lấy ra thông tin subject khi biết subject_name
     *
     * @param $subject_name
     * @return array|null
     * @throws Exception
     */
    public function getSubjectByName($subject_name)
    {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_subject WHERE subject_name = %s", secure($subject_name));

        $get_subject = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $subject = null;
        if ($get_subject->num_rows > 0) {
            $subject = $get_subject->fetch_assoc();
//            $school['page_picture'] = get_picture($school['page_picture'], 'school');
//            $school['page_cover'] = get_picture($school['page_cover'], 'school');
        }

        return $subject;
    }

    /**
     * Lấy ra thông tin của môn học
     *
     * @param $subjectId
     * @return array|null
     * @throws Exception
     */
    public function getSubject4Memcache($subjectId)
    {
        global $db;

        $strSql = sprintf("SELECT *
                           FROM ci_subject WHERE subject_id = %s", secure($subjectId, 'int'));
        $get_subject = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $subject = null;
        if ($get_subject->num_rows > 0) {
            $subject = $get_subject->fetch_assoc();
        }
        return $subject;
    }

    /**
     * Lấy ra danh sách môn học mà giáo viên được assign
     *
     * @param $teacher_id
     * @param $semester
     * @param $class_id
     * @param $school_year
     * @return array|null
     * @throws Exception
     */
    public function getSubjectByTeacher($teacher_id,$class_id, $semester = 1, $school_year = null)
    {
        global $db;
        if (isset($school_year)) {
            if ($semester == 0) {
                $strSql = sprintf("SELECT x.* from(SELECT cs.* FROM `ci_class_subject_teacher` ccst JOIN `ci_subject` cs ON cs.subject_id = ccst.subject_id WHERE ccst.teacher_id = %s AND ccst.semester = '1' AND ccst.school_year=%s AND ccst.class_id=%s) x INNER JOIN 
                                                        (SELECT cs.* FROM `ci_class_subject_teacher` ccst JOIN `ci_subject` cs ON cs.subject_id = ccst.subject_id WHERE ccst.teacher_id = %s AND ccst.semester = '2' AND ccst.school_year=%s AND ccst.class_id=%s) y WHERE x.subject_id = y.subject_id", secure($teacher_id, 'int'), secure($school_year), secure($class_id, 'int'),secure($teacher_id, 'int'), secure($school_year), secure($class_id, 'int'));
            } else {
                $strSql = sprintf("SELECT DISTINCT cs.* FROM `ci_class_subject_teacher` ccst JOIN `ci_subject` cs ON cs.subject_id = ccst.subject_id WHERE ccst.teacher_id = %s AND ccst.semester=%s AND ccst.school_year=%s AND ccst.class_id=%s", secure($teacher_id, 'int'), secure($semester, 'int'), secure($school_year), secure($class_id, 'int'));
            }
        } else {
            if ($semester == 0) {
                $strSql = sprintf("SELECT x.* from(SELECT cs.* FROM `ci_class_subject_teacher` ccst JOIN `ci_subject` cs ON cs.subject_id = ccst.subject_id WHERE ccst.teacher_id = %s AND ccst.semester ='1' AND ccst.class_id=%s) x INNER JOIN
                                                        (SELECT cs.* FROM `ci_class_subject_teacher` ccst JOIN `ci_subject` cs ON cs.subject_id = ccst.subject_id WHERE ccst.teacher_id = %s AND ccst.semester ='2' AND ccst.class_id=%s)  y WHERE x.subject_id = y.subject_id", secure($teacher_id, 'int'), secure($class_id, 'int'),secure($teacher_id, 'int'), secure($class_id, 'int'));
            } else {
                $strSql = sprintf("SELECT DISTINCT cs.* FROM `ci_class_subject_teacher` ccst JOIN `ci_subject` cs ON cs.subject_id = ccst.subject_id WHERE ccst.teacher_id = %s AND ccst.semester=%s AND ccst.class_id=%s", secure($teacher_id, 'int'), secure($semester, 'int'), secure($class_id, 'int'));
            }
        }
        $get_subjects = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $subjects = array();
        if ($get_subjects->num_rows > 0) {
            while ($subject = $get_subjects->fetch_assoc()) {
                $subjects[] = $subject;
            }
        }
        return $subjects;
    }

}

?>