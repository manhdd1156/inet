<?php

/**
 * DAO -> Report
 * Thao tác với bảng ci_report
 *
 * @package ConIu
 * @author ConIu
 */
class ReportDAO
{
    /**
     * __construct
     *
     */
    public function __construct()
    {
    }

    /**
     * Insert một báo cáo vào trong DB
     *
     * @param array $args
     * @throws Exception
     */
    public function insertReport(array $args = array())
    {
        global $db;
        $this->validateInput($args);

        $strSql = sprintf("INSERT INTO ci_report (report_name, school_id, class_id, type, receivers, comment_health, comment_habit, comment_language, comment_awareness, comment_aesthetic, comment_social_skills, comment_other, source_file , file_name ,start_date, end_date, is_notified, created_at, updated_at, created_user_id)
                              VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
            secure($args['title']), secure($args['school_id'], 'int'), secure($args['class_id'], 'int'), secure($args['type'], 'int'),
            secure($args['receivers']), secure($args['comment_health']), secure($args['comment_habit']), secure($args['comment_language']), secure($args['comment_awareness']), secure($args['comment_aesthetic']), secure($args['comment_social_skills']), secure($args['comment_other']), secure($args['source_file']), secure($args['file_name']),
            secure($args['begin']), secure($args['end']), secure($args['is_notified'], 'int'), secure($args['created_at']), secure($args['updated_at']), secure($args['user_id'], 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        $report_id = $db->insert_id;
        $strValues = "";
        foreach ($args['child'] as $child_id) {
            $strValues .= "(" . secure($report_id, 'int') . "," . secure($child_id, 'int') . "),";
        }
        $strValues = trim($strValues, ",");

        $strSql = sprintf("INSERT INTO ci_report_child (report_id, child_id) VALUES " . $strValues);
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        return $report_id;
    }

    /**
     * Update một sổ liên lạc vào trong DB
     *
     * @param array $args
     * @throws Exception
     */
    public function updateReport(array $args = array())
    {
        global $db, $date;
        $this->validateInput($args);

        $strSql = sprintf("UPDATE ci_report SET report_name = %s, is_notified = %s, source_file = %s, file_name = %s, date = %s, updated_at = %s WHERE report_id = %s",
            secure($args['report_name']), secure($args['is_notified'], 'int'),secure($args['source_file']), secure($args['file_name']), secure($date), secure($date), secure($args['report_id'], 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Kiểm tra điều kiện đầu vào.
     *
     * @param array $args
     * @throws Exception
     */
    private function validateInput(array $args = array())
    {
        if (!isset($args['report_name']) || $args['report_name'] == '') {
            throw new Exception(__("You must enter contack book title"));
        }
    }

    /**
     * Kiểm tra điều kiện đầu vào tiêu chí
     *
     * @param $categoryName
     * @throws Exception
     */
    public function _validateInputCategory($categoryName) {
        if (!isset($categoryName) || $categoryName == '') {
            throw new Exception(__("You must enter category name"));
        }
    }

    /**
     * Kiểm tra điều kiện đầu vào đối với mẫu.
     *
     * @param array $args
     * @throws Exception
     */
    private function validateInputTemplate(array $args = array())
    {
        if (!isset($args['template_name']) || strlen($args['template_name']) < 1) {
            throw new Exception(__("You must enter template title"));
        }
        if ($args['level'] == CLASS_LEVEL) {
            if(!($args['class_id'] > 0)) {
                throw new Exception(__("Please choose a class"));
            }
        }
        if ($args['level'] == CLASS_LEVEL_LEVEL) {
            if(!($args['class_level_id'] > 0)) {
                throw new Exception(__("Please choose a class level"));
            }
        }
    }

    /**
     * Lấy ra danh sách báo cáo cho 1 trẻ
     *
     * @param $child_id
     * @throws Exception
     */
    public function getChildReport($child_id)
    {
        global $db, $system;
        $strSql = sprintf("SELECT * FROM ci_report WHERE child_id = %s AND is_notified = '1'
                            ORDER BY date DESC, created_at DESC", secure($child_id, 'int'));

        $get_report = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        $result = array();
        if ($get_report->num_rows > 0) {
            while ($report = $get_report->fetch_assoc()) {
                if (!is_empty($report['source_file'])) {
                    $report['source_file'] = $system['system_uploads'] . '/' . $report['source_file'];
                }
                $report['created_at'] = toSysDate($report['created_at']);
                if ($report['updated_at'] != null) {
                    $report['updated_at'] = toSysDate($report['updated_at']);
                }
                if ($report['date'] != null) {
                    $report['date'] = toSysDatetime($report['date']);
                }
                $result[] = $report;

            }
        }
        return $result;
    }

    /**
     * Lấy chi tiết của 1 report
     */
    private function _getReportDetail($reportId) {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_report_detail WHERE report_id = %s ORDER BY report_detail_id ASC", secure($reportId, 'int'));

        $get_detail = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $details = array();
        if($get_detail->num_rows > 0) {
            while ($detail = $get_detail->fetch_assoc()) {
                $details[] = $detail;
            }
        }

        return $details;
    }

    /**
     * Lấy ra danh sách báo cáo cho 1 trường
     *
     * @param $child_id
     * @throws Exception
     */
    public function getSchoolReport($school_id)
    {
        global $db, $system;

        $strSql = sprintf('SELECT R.*, C.child_name, C.birthday, SC.status AS child_status, G.group_title  FROM ci_report R 
                           INNER JOIN ci_child C ON C.child_id = R.child_id  
                           INNER JOIN ci_school_child SC ON SC.child_id = C.child_id AND SC.school_id = %1$s
                           INNER JOIN ci_class_child CC ON CC.child_id = R.child_id
                           INNER JOIN groups G ON G.group_id = CC.class_id
                           WHERE R.school_id = %1$s ORDER BY R.report_id DESC, R.date DESC', secure($school_id, 'int'));
        $get_report = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        $result = array();
        if ($get_report->num_rows > 0) {
            while ($report = $get_report->fetch_assoc()) {
                if (!is_empty($report['source_file'])) {
                    $report['source_file'] = $system['system_uploads'] . '/' . $report['source_file'];
                }
                $report['created_at'] = toSysDate($report['created_at']);
                if($report['update_at'] != null) {
                    $report['update_at'] = toSysDate($report['update_at']);
                }
                if($report['date'] != null) {
                    $report['date'] = toSysDate($report['date']);
                }
                if($report['birthday'] != null) {
                    $report['birthday'] = toSysDate($report['birthday']);
                }
                $result[] = $report;
            }
        }
        return $result;
    }

    /**
     * Lấy ra danh sổ liên lạc cáo cho 1 lớp
     *
     * @param $child_id
     * @throws Exception
     */
    public function getClassReport($class_id)
    {
        global $db, $system;

        $strSql = sprintf('SELECT R.*, C.child_name, C.birthday, CC.status AS child_status, G.group_title FROM ci_report R
                    INNER JOIN ci_child C ON C.child_id = R.child_id
                    INNER JOIN ci_class_child CC ON CC.child_id = C.child_id AND CC.class_id = %1$s
                    INNER JOIN groups G ON G.group_id = CC.class_id 
                    WHERE CC.class_id = %1$s ORDER BY R.report_id DESC', secure($class_id, 'int'));
        $get_report = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        $result = array();
        if ($get_report->num_rows > 0) {
            while ($report = $get_report->fetch_assoc()) {
                if (!is_empty($report['source_file'])) {
                    $report['source_file'] = $system['system_uploads'] . '/' . $report['source_file'];
                }
                $report['created_at'] = toSysDate($report['created_at']);
                if($report['update_at'] != null) {
                    $report['update_at'] = toSysDate($report['update_at']);
                }
                if($report['date'] != null) {
                    $report['date'] = toSysDate($report['date']);
                }
                if($report['birthday'] != null) {
                    $report['birthday'] = toSysDate($report['birthday']);
                }
                $result[] = $report;
            }
        }
        return $result;
    }

    /**
     * Lấy ra danh sổ liên lạc cho 1 lớp chưa thông báo
     *
     * @param $child_id
     * @throws Exception
     */
    public function getClassReportNoNotify($class_id)
    {
        global $db, $system;

        $strSql = sprintf('SELECT R.*, C.child_name, C.birthday, CC.status AS child_status, G.group_title FROM ci_report R
                    INNER JOIN ci_child C ON C.child_id = R.child_id
                    INNER JOIN ci_class_child CC ON CC.child_id = C.child_id AND CC.class_id = %1$s
                    INNER JOIN groups G ON G.group_id = CC.class_id 
                    WHERE CC.class_id = %1$s AND is_notified = 0 ORDER BY R.report_id DESC', secure($class_id, 'int'));
        $get_report = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        $result = array();
        if ($get_report->num_rows > 0) {
            while ($report = $get_report->fetch_assoc()) {
                if (!is_empty($report['source_file'])) {
                    $report['source_file'] = $system['system_uploads'] . '/' . $report['source_file'];
                }
                $report['created_at'] = toSysDate($report['created_at']);
                if($report['update_at'] != null) {
                    $report['update_at'] = toSysDate($report['update_at']);
                }
                if($report['date'] != null) {
                    $report['date'] = toSysDate($report['date']);
                }
                if($report['birthday'] != null) {
                    $report['birthday'] = toSysDate($report['birthday']);
                }
                $result[] = $report;
            }
        }
        return $result;
    }

    /**
     * Lấy ra danh sách báo cáo cho 1 lớp cho API
     *
     * @param $child_id
     * @throws Exception
     */
    public function getClassReportForApi($class_id, $offset = 0)
    {
        global $db, $system;
        $offset *= $system['max_results'];

        $strSql = sprintf('SELECT R.*, C.child_name, C.birthday, CC.status AS child_status FROM ci_report R
                    INNER JOIN ci_child C ON C.child_id = R.child_id
                    INNER JOIN ci_class_child CC ON CC.child_id = C.child_id AND CC.class_id = %1$s
                    WHERE CC.class_id = %1$s ORDER BY R.report_id DESC LIMIT %2$s, %3$s', secure($class_id, 'int'), secure($offset, 'int', false), secure($system['max_results'], 'int', false));
        $get_report = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        $result = array();
        if ($get_report->num_rows > 0) {
            while ($report = $get_report->fetch_assoc()) {
                if (!is_empty($report['source_file'])) {
                    $report['source_file'] = $system['system_uploads'] . '/' . $report['source_file'];
                }
                $report['created_at'] = toSysDate($report['created_at']);
                if($report['update_at'] != null) {
                    $report['update_at'] = toSysDate($report['update_at']);
                }
                if($report['date'] != null) {
                    $report['date'] = toSysDatetime($report['date']);
                }
                if($report['birthday'] != null) {
                    $report['birthday'] = toSysDate($report['birthday']);
                }
                $result[] = $report;
            }
        }
        return $result;
    }

    /**
     * Lấy ra danh sách báo cáo cho 1 trẻ trong lớp
     *
     * @param $child_id
     * @param $class_id
     * @throws Exception
     */
    public function getChildReportInClass($class_id, $child_id)
    {
        global $db, $system;

        $strSql = sprintf("SELECT R.*, C.child_name, C.birthday FROM ci_report R
                    INNER JOIN ci_child C ON C.child_id = R.child_id AND R.child_id = %s
                    WHERE class_id = %s ORDER BY R.report_id DESC, R.date DESC, R.created_at DESC", secure($child_id, 'int'), secure($class_id, 'int'));
        $get_report = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        $result = array();
        if ($get_report->num_rows > 0) {
            while ($report = $get_report->fetch_assoc()) {
                if (!is_empty($report['source_file'])) {
                    $report['source_file'] = $system['system_uploads'] . '/' . $report['source_file'];
                }
                if ($system['is_mobile']) {
                    $detail = $this->getReportById($report['report_id']);
                    $report['detail'] = (isset($detail['detail'])) ? $detail['detail'] : array();
                }
                $result[] = $report;
            }
        }
        return $result;
    }

    /**
     * Xóa 1 sổ liên lạc
     *
     * @param $report_id
     * @throws Exception
     */
    public function deleteReport($report_id)
    {
        global $db;

        $db->query(sprintf("DELETE FROM ci_report WHERE report_id = %s", secure($report_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        $db->query(sprintf("DELETE FROM ci_report_category WHERE report_id = %s", secure($report_id, 'int'))) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Lấy chi tiết sổ liên lạc theo report_id
     *
     * @param $report_id
     * @return null
     */
    public function getReportById($report_id)
    {
        global $db, $system;

        $strSql = sprintf("SELECT R.*, SC.status AS child_status, C.child_name, C.birthday FROM ci_report R INNER JOIN ci_school_child SC 
                           ON R.report_id = %s AND R.child_id = SC.child_id
                           INNER JOIN ci_child C ON C.child_id = R.child_id", secure($report_id, 'int'));
        $get_report = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        $result = null;
        if ($get_report->num_rows > 0) {
            $report = $get_report->fetch_assoc();
            $report['birthday'] = toSysDate($report['birthday']);
            if ($report['date'] != null) {
                $report['date'] = toSysDatetime($report['date']);
            }

            $strSql = sprintf("SELECT * FROM ci_report_category WHERE report_id = %s", secure($report_id, 'int'));
            $get_detail = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

            if ($get_detail->num_rows > 0) {
                while ($detail = $get_detail->fetch_assoc()) {
                    $detail['multi_content'] = explode(BREAK_CHARACTER, $detail['multi_content']);
                    $detail['multi_content'] = array_diff($detail['multi_content'], array(""));
                    $detail['template_multi_content'] = explode(BREAK_CHARACTER, $detail['template_multi_content']);
                    $detail['template_multi_content'] = array_diff($detail['template_multi_content'], array(""));
                    $report['details'][] = $detail;
                }
            }

            if (!is_empty($report['source_file'])) {
                $report['source_file_path'] = $report['source_file'];
                $report['source_file'] = $system['system_uploads'] . '/' . $report['source_file'];
            }
            $result = $report;
        }
        return $result;
    }

    /**
     * Cập nhật chi tiết sổ liên lạc (bảng ci_report_category)
     *
     * @param $reportCategoryId
     * @param $category
     */
    public function updateReportCategory($category) {
        global $db;

        $category['multi_content'] = implode(BREAK_CHARACTER, $category['multi_content']);
        $strSql = sprintf("UPDATE ci_report_category SET report_category_content = %s, multi_content = %s WHERE report_category_id = %s", secure($category['report_category_content']), secure($category['multi_content']), secure($category['report_category_id'], 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Cập nhật chi tiết sổ liên lạc (bảng ci_report_category)
     *
     * @param $categoryId
     * @param array $suggest
     * @param $content
     */
    public function updateReportCategoryForAPI($categoryId, array $suggest = array(), $content) {
        global $db;

        $multiContent = implode(BREAK_CHARACTER, $suggest);
        $strSql = sprintf("UPDATE ci_report_category SET report_category_content = %s, multi_content = %s WHERE report_category_id = %s", secure($content), secure($multiContent), secure($categoryId, 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Cập nhật trạng thái của báo cáo thành "đã thông báo" ( = 1)
     *
     * @param $report_id
     * @throws Exception
     */
    public function updateStatusToNotified($report_id) {
        global $db, $date;

        $strSql = sprintf("UPDATE ci_report SET is_notified = %s, updated_at = %s, date = %s WHERE report_id = %s", 1, secure($date), secure($date), secure($report_id, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Thêm mới mẫu sổ liên lạc
     *
     * @param array $args
     * @return mixed
     */
    public function insertReportTemplate(array $args = array()) {
        global $db, $user, $date;
        $this->validateInputTemplate($args);

        $strSql = sprintf("INSERT INTO ci_report_template (template_name, level, school_id, class_level_id, class_id, created_at, created_user_id) VALUES (%s, %s, %s, %s, %s, %s, %s)", secure($args['template_name']), secure($args['level'], 'int'), secure($args['school_id'], 'int'), secure($args['class_level_id'], 'int'), secure($args['class_id'], 'int'), secure($date), secure($user->_data['user_id'], 'int'));

        $db->query($strSql) or _error('error', $strSql);

        return $db->insert_id;
    }

    /**
     * Thêm mới chi tiết mẫu
     *
     * @param $templateId
     * @param array $categoryIds
     * @param array $contents
     * @return mixed
     */
    public function insertReportTemplateDetail($templateId, array $categoryIds = array(), array $contents = array()) {
        global $db;

        if(count($categoryIds) == 0) {
            throw new Exception(__("You must select at least one category"));
        }
        $strValues = "";
        //catelogy
        for ($idx = 0; $idx < count($categoryIds); $idx++) {
            $strValues .= "(" . secure($templateId, 'int') . "," . secure($categoryIds[$idx]) . "," . secure($contents[$idx]) . "," . secure($idx + 1, 'int') . "),";
        }
        $strValues = trim($strValues, ",");
        $strSql = "INSERT INTO ci_report_template_detail (report_template_id, report_template_category_id, template_content, position) VALUES " . $strValues;

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        return $db->insert_id;
    }

    /**
     * Taila
     * Insert report template detail (ci_report_category)
     * @param templateId
     * @param array()
     * @throws Exception
     */
    public function insertReportDetail($reportId, array $categorys = array()) {
        global $db;

        $strValues = "";
        //catelogy
        for ($idx = 0; $idx < count($categorys); $idx++) {

            $categorys[$idx]['multi_content'] = implode(BREAK_CHARACTER, $categorys[$idx]['multi_content']);
            $categorys[$idx]['template_multi_content'] = implode(BREAK_CHARACTER, $categorys[$idx]['template_multi_content']);
            $strValues .= "(" . secure($reportId, 'int') . "," . secure($categorys[$idx]['multi_content']) . "," . secure($categorys[$idx]['template_multi_content']) . "," . secure($categorys[$idx]['report_category_name']) . "," . secure($categorys[$idx]['report_category_content']) . "),";
        }
        $strValues = trim($strValues, ",");
        $strSql = "INSERT INTO ci_report_category (report_id, multi_content, template_multi_content, report_category_name, report_category_content) VALUES " . $strValues;

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        return $db->insert_id;
    }

    /**
     * Insert report từ mobile
     *
     * @param $reportId
     * @param array $categoryIds
     * @param array $suggests
     * @param array $contents
     * @param array $categoryNames
     * @param array $categoryMulContent
     * @return mixed
     */
    public function insertReportDetailForAPI($reportId, array $categoryIds = array(), array $suggests = array(),array $contents = array(),array $categoryNames = array(),array $categoryMulContent = array()) {
        global $db;

        // $suggests đang là một mảng array, foreach nó implode thành 1 mảng chuỗi
        $multiContent = array();
        foreach ($suggests as $suggest) {
            $multiContent[] = implode(BREAK_CHARACTER, $suggest);
        }

        $strValues = "";
        for ($idx = 0; $idx < count($categoryIds); $idx++) {
            $strValues .= "(" . secure($reportId, 'int') . "," . secure($multiContent[$idx]) . "," . secure($categoryMulContent[$idx]) . "," . secure($categoryNames[$idx]) . "," . secure($contents[$idx]) . "),";
        }
        $strValues = trim($strValues, ",");
        $strSql = "INSERT INTO ci_report_category (report_id, multi_content, template_multi_content, report_category_name, report_category_content) VALUES " . $strValues;

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        return $db->insert_id;
    }

    /**
     * Lấy danh sách report template của một class
     */
    public function getReportTemplateOfClass($class_id) {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_report_template WHERE class_id = %s AND status = %s ORDER BY created_at", secure($class_id, 'int'), 1);

        $get_template = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $templates = array();
        if($get_template->num_rows > 0) {
            while($temp = $get_template->fetch_assoc()) {
                $templates[] = $temp;
            }
        }
        return $templates;
    }

    /**
     * Lấy danh sách report template của một school
     */
    public function getReportTemplateOfSchool($school_id) {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_report_template WHERE school_id = %s AND status = %s ORDER BY created_at DESC", secure($school_id, 'int'), 1);

        $get_template = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $templates = array();
        if($get_template->num_rows > 0) {
            while($temp = $get_template->fetch_assoc()) {
                $templates[] = $temp;
            }
        }
        return $templates;
    }

    /**
     * Lấy chi tiết template
     */
    public function getReportTemplateDetail($template_id) {
        global $db;

        $strSql = sprintf("SELECT RTD.*, RT.report_template_name, RT.status FROM ci_report_template_detail RTD 
                  INNER JOIN ci_report_template RT ON RTD.report_template_id = RT.report_template_id
                  WHERE RTD.report_template_id = %s", secure($template_id, 'int'));

        $get_detail = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $template_detail = array();
        if($get_detail->num_rows > 0) {
            while ($detail = $get_detail->fetch_assoc()){
                $template_detail[] = $detail;
            }
        }
        return $template_detail;
    }

    /**
     * Thêm mới sổ liên lạc
     *
     * @param array $args
     * @return array
     */
    public function insertReportClass(array $args = array()) {
        global $db, $user, $date;
        $this->validateInput($args);

        $strValues = "";
        //catelogy
        for ($idx = 0; $idx < count($args['child']); $idx++) {

            $strValues .= " (" . secure($args['report_name']) . "," . secure($args['child'][$idx], 'int') . "," . secure($args['school_id'], 'int') . "," . secure($args['is_notified'], 'int') . "," . secure($args['source_file']) . "," . secure($args['file_name']) . "," . secure($args['date']) . "," . secure($date) . "," . secure($user->_data['user_id'], 'int') . "),";
        }
        $strValues = trim($strValues, ",");
        $strSql = "INSERT INTO ci_report (report_name, child_id, school_id, is_notified, source_file, file_name, date, created_at, created_user_id) VALUES " . $strValues;

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        $firstId = $db->insert_id;
        $lastId = $firstId + count($args['child']) - 1;
        $reportIds = array();
        for($i = $firstId; $i <= $lastId; $i++) {
            $reportIds[] = $i;
        }
        return $reportIds;
    }

    /**
     * Lấy tất cả các mẫu áp dụng cho lớp
     *
     * @param $class_id
     * @param $class_level_id
     * @param $school_id
     * @return array
     */
    public function getClassReportTemplate($class_id, $class_level_id, $school_id) {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_report_template WHERE (class_id = %s OR (class_id = 0 AND (class_level_id = %s OR class_level_id = 0))) AND school_id = %s ORDER BY report_template_id DESC", secure($class_id, 'int'), secure($class_level_id, 'int'), secure($school_id, 'int'));

        $get_template = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        $results = array();
        if ($get_template->num_rows > 0) {
            while ($template = $get_template->fetch_assoc()) {
                $template['created_at'] = toSysDate($template['created_at']);
                if($template['updated_at'] != null) {
                    $template['updated_at'] = toSysDate($template['updated_at']);
                }
                $results[] = $template;
            }
        }
        return $results;
    }

    /**
     * Lấy danh sách report template tạo gần nhất của lớp
     */
    public function getLastestClassReportTemplate($class_id) {
        global $db, $system;

        $strSql = sprintf("SELECT * FROM ci_report_template WHERE class_id = %s ORDER BY updated_at DESC, created_at DESC LIMIT 1", secure($class_id, 'int'));

        $get_template = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        $template = array();
        if ($get_template->num_rows > 0) {
            $template = $get_template->fetch_assoc();
            if ($system['is_mobile']) {
                $template['detail'] = $this->getReportTemplateDetail($template['report_template_id']);
            }
        }
        return $template;
    }

    /**
     * Lấy tất cả mẫu sổ liên lạc của trường
     *
     * @param $school_id
     * @return array
     */
    public function getSchoolReportTemplate($school_id) {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_report_template WHERE school_id = %s AND status = 1 ORDER BY report_template_id DESC", secure($school_id, 'int'));

        $get_template = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        $results = array();
        if ($get_template->num_rows > 0) {
            while ($template = $get_template->fetch_assoc()) {
                $template['created_at'] = toSysDate($template['created_at']);
                if($template['updated_at'] != null) {
                    $template['updated_at'] = toSysDate($template['updated_at']);
                }
                $results[] = $template;
            }
        }
        return $results;
    }

    /**
     * Cập nhật mẫu sổ liên lạc
     *
     * @param array $args
     */
    public function updateReportTemplate(array $args = array()) {
        global $db, $date;
        $this->validateInputTemplate($args);

        $strSql = sprintf("UPDATE ci_report_template SET template_name = %s, level = %s, class_level_id = %s, class_id = %s, updated_at = %s WHERE report_template_id = %s", secure($args['template_name']), secure($args['level'], 'int'), secure($args['class_level_id'], 'int'), secure($args['class_id'], 'int'), secure($date), secure($args['report_template_id'], 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Lấy danh sách chi tiết của 1 template
     */
    public function getIdTemplateDetail($template_id) {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_report_template_detail WHERE report_template_id = %s", secure($template_id, 'int'));

        $get_template_detail = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $templateDetailIds = array();
        if($get_template_detail->num_rows > 0) {
            while ($temp_detail = $get_template_detail->fetch_assoc()['report_template_detail_id']) {
                $templateDetailIds[] = $temp_detail;
            }
        }
        return $templateDetailIds;
    }

    /**
     * Lấy danh sách id chi tiết của 1 report
     */
    public function getIdReportDetail($report_id) {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_report_detail WHERE report_id = %s", secure($report_id, 'int'));

        $get_report_detail = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $reportDetailIds = array();
        if($get_report_detail->num_rows > 0) {
            while ($temp_detail = $get_report_detail->fetch_assoc()['report_detail_id']) {
                $reportDetailIds[] = $temp_detail;
            }
        }
        return $reportDetailIds;
    }

    /**
     * Delete chi tiết sổ liên lạc (bảng ci_report_category)
     *
     * @param $reportId
     */
    public function deleteReportDetail($reportId) {
        global $db;

        $strSql = sprintf("DELETE FROM ci_report_category WHERE report_id = %s", secure($reportId, 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Delete report detail
     */
    public function deleteReportDetailById($id) {
        global $db;

        $strSql = sprintf("DELETE FROM ci_report_template WHERE report_template_id = %s", secure($id, 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        $strSql = sprintf("DELETE FROM ci_report_template_detail WHERE report_template_id = %s", secure($id, 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Delete chi tiết mẫu sổ liên lạc
     *
     * @param $reportTemplateId
     */
    public function deleteReportTemplateDetail($reportTemplateId) {
        global $db;

        $strSql = sprintf("DELETE FROM ci_report_template_detail WHERE report_template_id = %s", secure($reportTemplateId, 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Update report template detail
     */
    public function updateReportTemplateDetail($ids, $cates, $contents) {
        global $db;

        if(count($cates) != count($contents)) {
            throw new Exception(__("Please enter the full information"));
        }
        for($i = 0; $i < count($ids); $i++) {
            $strSql = sprintf("UPDATE ci_report_template_detail SET template_detail_name = %s, template_detail_content = %s WHERE report_template_detail_id = %s", secure($cates[$i]), secure($contents[$i]), secure($ids[$i], 'int'));

            $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        }
    }

    /**
     * Update report template detail
     */
    public function updateReportDetail($ids, $cates, $contents) {
        global $db;

        if(count($cates) != count($contents)) {
            throw new Exception(__("Bạn vui lòng nhập đầy đủ thông tin"));
        }
        for($i = 0; $i < count($ids); $i++) {
            $strSql = sprintf("UPDATE ci_report_detail SET report_detail_name = %s, report_detail_content = %s WHERE report_detail_id = %s", secure($cates[$i]), secure($contents[$i]), secure($ids[$i], 'int'));

            $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        }
    }

    /**
     * Thêm template_detail mới
     */
    public function addNewTemplateDetail($templateId, $cates, $contents) {
        global $db;

        if(count($cates) == 0 || count($contents) == 0 || count($cates) != count($contents)) {
            throw new Exception(__("Please enter the full information"));
        }

        $strValues = "";
        //catelogy
        for ($idx = 0; $idx < count($cates); $idx++) {

            $strValues .= "(" . secure($templateId, 'int') . "," . secure($cates[$idx]) . "," . secure($contents[$idx]) . "),";
        }
        $strValues = trim($strValues, ",");
        $strSql = "INSERT INTO ci_report_template_detail (report_template_id, template_detail_name, template_detail_content) VALUES " . $strValues;

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        return $db->insert_id;
    }

    /**
     * Thêm report_detail mới
     */
    public function addNewReportDetail($reportId, $cates, $contents) {
        global $db;

        if(count($cates) == 0 || count($contents) == 0 || count($cates) != count($contents)) {
            throw new Exception(__("Please enter the full information"));
        }

        $strValues = "";
        //catelogy
        for ($idx = 0; $idx < count($cates); $idx++) {

            $strValues .= "(" . secure($reportId, 'int') . "," . secure($cates[$idx]) . "," . secure($contents[$idx]) . "),";
        }
        $strValues = trim($strValues, ",");
        $strSql = "INSERT INTO ci_report_detail (report_id, report_detail_name, report_detail_content) VALUES " . $strValues;

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        return $db->insert_id;
    }

    /**
     * Delete report detail khi thay mẫu mới khi sửa sổ liên lạc
     */
    public function deleteReportDetailByReportId($report_id) {
        global $db;

        $strSql = sprintf("DELETE FROM ci_report_detail WHERE report_id = %s", secure($report_id, 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Lấy toàn bộ sổ liên lạc của 1 trẻ trong lớp
     *
     * @param $classId
     * @param $childId
     * @return array
     */
    public function getClassReportOfChild($classId, $childId) {
        global $db, $system;

        $strSql = sprintf("SELECT R.*, C.child_name, C.birthday, CC.status AS child_status, G.group_title FROM ci_report R 
                           INNER JOIN ci_child C ON C.child_id = R.child_id 
                           INNER JOIN ci_class_child CC ON CC.child_id = R.child_id
                           INNER JOIN groups G ON G.group_id = CC.class_id
                           WHERE CC.class_id = %s AND R.child_id = %s ORDER BY R.created_at DESC, R.date DESC", secure($classId, 'int'), secure($childId, 'int'));

        $get_report = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $reportchilds = array();
        if ($get_report->num_rows > 0) {
            while($report = $get_report->fetch_assoc()) {
                if (!is_empty($report['source_file'])) {
                    $report['source_file'] = $system['system_uploads'] . '/' . $report['source_file'];
                }
                $report['created_at'] = toSysDate($report['created_at']);
                if($report['update_at'] != null) {
                    $report['update_at'] = toSysDate($report['update_at']);
                }
                if($report['date'] != null) {
                    $report['date'] = toSysDatetime($report['date']);
                }
                if($report['birthday'] != null) {
                    $report['birthday'] = toSysDate($report['birthday']);
                }
                $reportchilds[] = $report;
            }
        }
        return $reportchilds;
    }

    /**
     * Lấy chi tiết mẫu sổ liên lạc
     *
     * @param $templateId
     * @return array
     */
    public function getReportTemplate($templateId, $isSuggest = 0) {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_report_template WHERE report_template_id = %s", secure($templateId. 'int'));

        $get_template = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $temp = array();
        if($get_template->num_rows > 0) {
            $temp = $get_template->fetch_assoc();
            $temp['created_at'] = toSysDate($temp['created_at']);
            if($temp['updated_at'] != '') {
                $temp['updated_at'] = toSysDate($temp['updated_at']);
            }
            // Lấy chi tiết template
            $temp['details'] = $this->_getReportTemplateDetail($temp['report_template_id']);
            // Nếu $isCategory > 0 thì lấy chi tiết các gọi ý của hạng mục
            if($isSuggest > 0) {
                foreach ($temp['details'] as $k => $detail) {
                    $suggests = $this->getReportTemplateCategorySuggests($detail['report_template_category_id']);
                    $temp['details'][$k]['suggests'] = $suggests;
                }
            }
        }

        return $temp;
    }

    /**
     * Lấy chi tiết mẫu sổ liên lạc và các gợi ý của hạng mục phục vụ chức năng tạo SLL
     *
     * @param $templateId
     * @return array
     */
    public function getReportTemplateForCreatedReport($templateId) {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_report_template WHERE report_template_id = %s", secure($templateId. 'int'));

        $get_template = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $temp = array();
        if($get_template->num_rows > 0) {
            $temp = $get_template->fetch_assoc();
            $temp['created_at'] = toSysDate($temp['created_at']);
            if($temp['updated_at'] != '') {
                $temp['updated_at'] = toSysDate($temp['updated_at']);
            }
            // Lấy chi tiết template
            $temp['details'] = $this->_getReportTemplateDetail($temp['report_template_id']);
        }

        return $temp;
    }

    /**
     * Lấy các hạng mục của mẫu
     *
     * @param $templateId
     * @return array
     */
    private function _getReportTemplateDetail($templateId) {
        global $db;

        $strSql = sprintf("SELECT RTD.*, RTC.category_name FROM ci_report_template_detail RTD INNER JOIN ci_report_template_category RTC ON RTC.report_template_category_id = RTD.report_template_category_id WHERE report_template_id = %s ORDER BY RTD.position ASC", secure($templateId));

        $get_template_detail = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $details = array();
        if($get_template_detail->num_rows > 0) {
            while ($detail = $get_template_detail->fetch_assoc()) {
                $details[] = $detail;
            }
        }

        return $details;
    }

    /**
     * Lấy tất cả những hạng mục sổ liên lạc của trường
     *
     * @param $schoolId
     * @return array
     */
    public function getAllCategoryOfSchool($schoolId) {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_report_template_category WHERE school_id = %s", secure($schoolId, 'int'));

        $get_categorys = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $categorys = array();
        if($get_categorys->num_rows > 0) {
            while($category = $get_categorys->fetch_assoc()) {
                $category['suggests'] = array();
                $category['suggests'] = explode(BREAK_CHARACTER,$category['template_multi_content']);
                $category['suggests'] = array_diff($category['suggests'], array(""));
                $categorys[] = $category;
            }
        }

        return $categorys;
    }

    /**
     * Thêm một hạng mục mới
     *
     * @param $categoryName
     * @param $multiContent
     * @param $schoolId
     * @return mixed
     */
    public function createReportTemplateCategory($categoryName, $suggests, $schoolId) {
        global $db;

        $this->_validateInputCategory($categoryName);
        $multiContent = implode(BREAK_CHARACTER, $suggests);

        $strSql = sprintf("INSERT INTO ci_report_template_category (category_name, school_id, template_multi_content) VALUES (%s, %s, %s)", secure($categoryName), secure($schoolId, 'int'), secure($multiContent));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        return $db->insert_id;
    }

    /**
     * Cập nhật hạng mục
     *
     * @param $categoryId
     * @param $categoryName
     * @param $multiContent
     */
    public function updateReportTemplateCategory($categoryId, $categoryName, $suggests) {
        global $db;

        if(!isset($categoryName) || $categoryName == '') {
            throw new Exception(__("Category name not empty"));
        }
        // implode thành chuỗi để lưu vào db
        $multiContent = '';
        if(count($suggests) > 0) {
            $multiContent = implode(BREAK_CHARACTER, $suggests);
        }
        $strSql = sprintf("UPDATE ci_report_template_category SET category_name = %s, template_multi_content = %s WHERE report_template_category_id = %s", secure($categoryName), secure($multiContent), secure($categoryId, 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Lấy chi tiết hạng mục
     *
     * @param $categoryId
     * @return array
     */
    public function getReportTemplateCategoryDetail($categoryId) {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_report_template_category WHERE report_template_category_id = %s", secure($categoryId));

        $get_category = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $category = array();
        if($get_category->num_rows > 0) {
            $category = $get_category->fetch_assoc();
            $suggests = explode(BREAK_CHARACTER, $category['template_multi_content']);
            $suggests = array_diff($suggests, array(""));
            $category['suggests'] = $suggests;
        }

        return $category;
    }

    /**
     * Lấy chi tiết hạng mục cho API
     *
     * @param $categoryId
     * @return array
     */
    public function getReportTemplateCategoryDetailForAPI($categoryId) {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_report_template_category WHERE report_template_category_id = %s", secure($categoryId));

        $get_category = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $category = array();
        if($get_category->num_rows > 0) {
            $category = $get_category->fetch_assoc();
        }

        return $category;
    }

    /**
     * Lấy các gọi ý của hạng mục
     *
     * @param $categoryId
     * @return array
     */
    public function getReportTemplateCategorySuggests($categoryId) {
        global $db;

        $strSql = sprintf("SELECT template_multi_content FROM ci_report_template_category WHERE report_template_category_id = %s", secure($categoryId));

        $get_suggests = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $suggests = array();
        if($get_suggests->num_rows > 0) {
            $suggests = $get_suggests->fetch_assoc()['template_multi_content'];
            $suggests = explode(BREAK_CHARACTER, $suggests);
            $suggests = array_diff($suggests, array(""));
        }

        return $suggests;
    }

    /**
     * Xóa template trong bảng ci_report_template
     *
     * @param $reportTemplateId
     */
    public function deleteReportTemplate($reportTemplateId) {
        global $db;

        $strSql = sprintf("DELETE FROM ci_report_template WHERE report_template_id = %s", secure($reportTemplateId, 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Xóa hạng mục trong bảng ci_report_template_category
     *
     * @param $categoryId
     */
    public function deleteReportCategory($categoryId) {
        global $db;

        $strSql = sprintf("DELETE FROM ci_report_template_category WHERE report_template_category_id = %s", secure($categoryId, 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        $sqlStr = sprintf("DELETE FROM ci_report_template_detail WHERE report_template_category_id = %s", secure($categoryId, 'int'));

        $db->query($sqlStr) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Lấy danh sách sổ liên lạc của trường
     *
     * @param $schoolId
     * @param $year
     * @param $month
     * @return array
     * @throws Exception
     */
    public function getAllReportOfMonth($schoolId, $year, $month) {
        global $db, $system;
        $strSql = sprintf("SELECT R.*, G.group_title, G.group_id, C.child_name FROM ci_report R INNER JOIN ci_class_child CC ON R.child_id = CC.child_id 
                INNER JOIN groups G ON G.group_id = CC.class_id
                INNER JOIN ci_child C ON C.child_id = R.child_id
                WHERE R.school_id = %s AND MONTH(R.created_at) = %s AND YEAR(R.created_at) = %s
                            ORDER BY date DESC, created_at DESC", secure($schoolId, 'int'), secure($month, 'int'), secure($year, 'int'));

        $get_report = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        $result = array();
        if ($get_report->num_rows > 0) {
            while ($report = $get_report->fetch_assoc()) {
                if (!is_empty($report['source_file'])) {
                    $report['source_file_path'] = $report['source_file'];
                    $report['source_file'] = $system['system_uploads'] . '/' . $report['source_file'];
                }
                $result[] = $report;

            }
        }
        return $result;
    }

    /* ----------REPORT - API ---------- */

    /**
     * Lấy danh sách report template của lớp
     */
    public function getClassReportTemplateForApi($class_id, $class_level_id, $school_id) {
        global $db, $system;

        $strSql = sprintf("SELECT report_template_id, report_template_name, status, created_at, updated_at FROM ci_report_template 
                           WHERE (class_id = %s OR class_id = 0) AND school_id = %s ORDER BY updated_at DESC, created_at DESC", secure($class_id, 'int'), secure($school_id, 'int'));

        $get_template = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        $results = array();
        if ($get_template->num_rows > 0) {
            while ($template = $get_template->fetch_assoc()) {
                $results[] = $template;
            }
        }
        return $results;
    }

    /**
     * Lấy chi tiết template
     */
    public function getReportTemplateDetailForApi($template_id) {
        global $db;

        $strSql = sprintf("SELECT RTD.*, RT.status FROM ci_report_template_detail RTD 
                  INNER JOIN ci_report_template RT ON RTD.report_template_id = RT.report_template_id
                  WHERE RTD.report_template_id = %s", secure($template_id, 'int'));

        $get_detail = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $template_detail = array();
        if($get_detail->num_rows > 0) {
            while ($detail = $get_detail->fetch_assoc()){
                $template_detail[] = $detail;
            }
        }
        return $template_detail;
    }

    /**
     * Lấy ra danh sách báo cáo cho 1 trẻ trong lớp
     *
     * @param $child_id
     * @param $class_id
     * @throws Exception
     */
    public function getChildReportInClassForApi($class_id, $child_id)
    {
        global $db, $system;

        $strSql = sprintf("SELECT R.report_id, R.report_name,R.child_id, R.is_notified, R.source_file, R.file_name, R.status, R.created_at, R.updated_at, C.child_name, C.birthday FROM ci_report R
                    INNER JOIN ci_child C ON C.child_id = R.child_id AND R.child_id = %s
                    WHERE class_id = %s ORDER BY R.report_id DESC, R.date DESC, R.created_at DESC", secure($child_id, 'int'), secure($class_id, 'int'));
        $get_report = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        $result = array();
        if ($get_report->num_rows > 0) {
            while ($report = $get_report->fetch_assoc()) {
                if (!is_empty($report['source_file'])) {
                    $report['source_file'] = $system['system_uploads'] . '/' . $report['source_file'];
                }
                $result[] = $report;
            }
        }
        return $result;
    }

    /**
     * Lấy ra sổ liên lạc theo Id
     *
     * @param $report_id
     * @throws Exception
     */
    public function getReportByIdForApi($report_id)
    {
        global $db, $system;

        $strSql = sprintf("SELECT R.report_id, R.report_name, R.child_id, R.is_notified, R.source_file, R.file_name, R.status, R.created_at, R.updated_at, R.created_user_id, SC.status AS child_status FROM ci_report R INNER JOIN ci_school_child SC 
                           ON R.report_id = %s AND R.child_id = SC.child_id", secure($report_id, 'int'));
        $get_report = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        $result = null;
        if ($get_report->num_rows > 0) {
            $report = $get_report->fetch_assoc();

            $strSql = sprintf("SELECT report_category_id, report_id, report_category_name, report_category_content FROM ci_report_category WHERE report_id = %s", secure($report_id, 'int'));
            $get_detail = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

            if ($get_detail->num_rows > 0) {
                while ($detail = $get_detail->fetch_assoc()) {
                    $report['detail'][] = $detail;
                }
            }

            if (!is_empty($report['source_file'])) {
                $report['source_file_path'] = $report['source_file'];
                $report['source_file'] = $system['system_uploads'] . '/' . $report['source_file'];
            }
            $result = $report;
        }
        return $result;
    }

    /**
     * Lấy ra danh sách báo cáo cho 1 trẻ
     *
     * @param $child_id
     * @throws Exception
     */
    public function getChildReportForApi($child_id)
    {
        global $db;
        $strSql = sprintf("SELECT report_id, report_name, date FROM ci_report WHERE child_id = %s AND is_notified = '1'
                            ORDER BY date DESC, created_at DESC", secure($child_id, 'int'));

        $get_report = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        $result = array();
        if ($get_report->num_rows > 0) {
            while ($report = $get_report->fetch_assoc()) {
                $report['date'] = toSysDatetime($report['date']);
                $result[] = $report;

            }
        }
        return $result;
    }


    /* ---------- END - API ---------- */

    /**
     * update trạng thái đã thông báo khi thông báo nhiều sổ liên lạc cùng lúc
     *
     * @param $ids
     * @throws Exception
     */
    public function updateNotifiedForReports($ids) {
        global $db, $date;

        if(count($ids) > 0) {
            $strCon = implode(',', $ids);
            $strSql = sprintf("UPDATE ci_report SET is_notified = 1, date = %s WHERE report_id IN(%s)", secure($date), $strCon);

            $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        }
    }

    /**
     * Lấy sanh sách childId của những trẻ được viết sổ liên lạc trong ngày
     *
     * @param $schoolId
     * @param $day
     * @return array
     * @throws Exception
     */
    public function getChildIdsCreatedReportOnDay ($schoolId, $day) {
        global $db;

        $strSql = sprintf("SELECT child_id FROM ci_report WHERE school_id = %s AND DATE(date) = %s AND is_notified = 1", secure($schoolId, 'int'), secure($day));

        $get_child = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $childIds = array();
        if($get_child->num_rows > 0) {
            while($childId = $get_child->fetch_assoc()) {
                $childIds[] = $childId['child_id'];
            }
        }

        return $childIds;
    }

    /**
     * Lấy danh sách child_id của những trẻ được viết sổ liên lạc trong tuần
     *
     * @param $schoolId
     * @param $fromDay
     * @param $toDay
     * @return array
     * @throws Exception
     */
    public function getChildIdsCreatedReportOnWeek ($schoolId, $fromDay, $toDay) {
        global $db;

        $strSql = sprintf("SELECT DISTINCT child_id FROM ci_report WHERE school_id = %s AND DATE(date) >= %s AND DATE(date) <= %s AND is_notified = 1", secure($schoolId, 'int'), secure($fromDay), secure($toDay));

        $get_child = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $childIds = array();
        if($get_child->num_rows > 0) {
            while($childId = $get_child->fetch_assoc()) {
                $childIds[] = $childId['child_id'];
            }
        }

        return $childIds;
    }

    /**
     * Lấy danh sách sổ liên lạc theo 1 lớp và theo tiêu chí được chọn
     *
     * @param $class_id
     * @param $categoryName
     * @return array
     * @throws Exception
     */
    public function getClassReportByCategoryName($class_id, $categoryName)
    {
        global $db, $system;

        $strSql = sprintf('SELECT R.*, RC.multi_content, C.child_name, C.birthday, CC.status AS child_status, G.group_title FROM ci_report R
                    INNER JOIN ci_child C ON C.child_id = R.child_id
                    INNER JOIN ci_class_child CC ON CC.child_id = C.child_id AND CC.class_id = %1$s
                    INNER JOIN groups G ON G.group_id = CC.class_id 
                    INNER JOIN ci_report_category RC ON R.report_id = RC.report_id
                    WHERE CC.class_id = %1$s AND RC.report_category_name = %2$s ORDER BY R.report_id DESC', secure($class_id, 'int'), "'" . $categoryName . "'");
        $get_report = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        $result = array();
        if ($get_report->num_rows > 0) {
            while ($report = $get_report->fetch_assoc()) {
                if (!is_empty($report['source_file'])) {
                    $report['source_file'] = $system['system_uploads'] . '/' . $report['source_file'];
                }
                $report['created_at'] = toSysDate($report['created_at']);
                if($report['update_at'] != null) {
                    $report['update_at'] = toSysDate($report['update_at']);
                }
                if($report['date'] != null) {
                    $report['date'] = toSysDate($report['date']);
                }
                if($report['birthday'] != null) {
                    $report['birthday'] = toSysDate($report['birthday']);
                }
                if($report['multi_content']) {
                    $report['multi_content'] = '- ' . $report['multi_content'];
                    $report['multi_content'] = str_replace(BREAK_CHARACTER, '<br/> - ', $report['multi_content']);
                }

                $result[] = $report;
            }
        }

        return $result;
    }

    /**
     * Lấy danh sách sổ liên lạc theo childId và tiêu chí được chọn
     *
     * @param $classId
     * @param $childId
     * @param $categoryName
     * @return array
     * @throws Exception
     */
    public function getClassReportOfChildByCategoryName($classId, $childId, $categoryName) {
        global $db, $system;

        $strSql = sprintf("SELECT R.*, RC.multi_content, C.child_name, C.birthday, CC.status AS child_status, G.group_title FROM ci_report R 
                           INNER JOIN ci_child C ON C.child_id = R.child_id 
                           INNER JOIN ci_class_child CC ON CC.child_id = R.child_id
                           INNER JOIN groups G ON G.group_id = CC.class_id
                           INNER JOIN ci_report_category RC ON R.report_id = RC.report_id
                           WHERE CC.class_id = %s AND R.child_id = %s AND RC.report_category_name = %s ORDER BY R.created_at DESC, R.date DESC", secure($classId, 'int'), secure($childId, 'int'), "'" . $categoryName . "'");

        $get_report = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $reportchilds = array();
        if ($get_report->num_rows > 0) {
            while($report = $get_report->fetch_assoc()) {
                if (!is_empty($report['source_file'])) {
                    $report['source_file'] = $system['system_uploads'] . '/' . $report['source_file'];
                }
                $report['created_at'] = toSysDate($report['created_at']);
                if($report['update_at'] != null) {
                    $report['update_at'] = toSysDate($report['update_at']);
                }
                if($report['date'] != null) {
                    $report['date'] = toSysDatetime($report['date']);
                }
                if($report['birthday'] != null) {
                    $report['birthday'] = toSysDate($report['birthday']);
                }

                if($report['multi_content']) {
                    $report['multi_content'] = '- ' . $report['multi_content'];
                    $report['multi_content'] = str_replace(BREAK_CHARACTER, '<br/> - ', $report['multi_content']);
                }

                $reportchilds[] = $report;
            }
        }
        return $reportchilds;
    }

    /**
     * Lấy danh sách sổ liên lạc theo school_id và tiêu chí được chọn
     *
     * @param $school_id
     * @param $categoryName
     * @return array
     * @throws Exception
     */
    public function getSchoolReportByCategoryName($school_id, $categoryName)
    {
        global $db, $system;

        $strSql = sprintf('SELECT R.*, RC.multi_content, C.child_name, C.birthday, SC.status AS child_status, G.group_title  FROM ci_report R 
                           INNER JOIN ci_child C ON C.child_id = R.child_id  
                           INNER JOIN ci_school_child SC ON SC.child_id = C.child_id AND SC.school_id = %1$s
                           INNER JOIN ci_class_child CC ON CC.child_id = R.child_id
                           INNER JOIN groups G ON G.group_id = CC.class_id
                           INNER JOIN ci_report_category RC ON R.report_id = RC.report_id
                           WHERE R.school_id = %1$s AND RC.report_category_name = %2$s ORDER BY R.report_id DESC, R.date DESC', secure($school_id, 'int'), "'" . $categoryName . "'");
        $get_report = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        $result = array();
        if ($get_report->num_rows > 0) {
            while ($report = $get_report->fetch_assoc()) {
                if (!is_empty($report['source_file'])) {
                    $report['source_file'] = $system['system_uploads'] . '/' . $report['source_file'];
                }
                $report['created_at'] = toSysDate($report['created_at']);
                if($report['update_at'] != null) {
                    $report['update_at'] = toSysDate($report['update_at']);
                }
                if($report['date'] != null) {
                    $report['date'] = toSysDate($report['date']);
                }
                if($report['birthday'] != null) {
                    $report['birthday'] = toSysDate($report['birthday']);
                }

                if($report['multi_content']) {
                    $report['multi_content'] = '- ' . $report['multi_content'];
                    $report['multi_content'] = str_replace(BREAK_CHARACTER, '<br/> - ', $report['multi_content']);
                }

                $result[] = $report;
            }
        }
        return $result;
    }
}

?>