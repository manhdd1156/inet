<?php
/**
 * DAO -> Birthday
 * Thao tác với bảng ci_user_birthday
 *
 * @package ConIu
 * @author TaiLA
 */

class BirthdayDao {
    /**
     * __construct
     *
     */
    public function __construct() {

    }

    /**
     * Lây ra danh sách trẻ có sinh nhật trong tháng của trường
     *
     * @return array
     */
    public function getChildBirthdayInTheMonth($school_id) {
        global $db;

        $strSql = sprintf("SELECT UB.*, POCB.*, CB.* FROM ci_user_birthday UB INNER JOIN ci_parent_of_child_birthday POCB ON POCB.child_id = UB.user_id
              INNER JOIN ci_class_birthday CB ON CB.user_id = UB.user_id AND CB.school_id = %s AND type = %s", secure($school_id, 'int'), 1);

        $childs = array();
        $get_childs = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_childs->num_rows > 0) {
            while($child = $get_childs->fetch_assoc()) {
                $childs[] = $child;
            }
        }
        return $childs;
    }

//    /**
//     * Lây ra danh sách trẻ có sinh nhật từ ngày đến ngày
//     *
//     * @param $school_id
//     * @param $time_up: Khoảng ngày cần lấy (Ví dụ: 1 tuần tới thì giá trị truyền vào = 7)
//     * @param $class_id
//     * @return array
//     * @throws Exception
//     */
//    public function getChildBirthday($school_id, $time_up, $class_id) {
//        global $db;
//        if ($class_id != 0) {
//            $strSql = sprintf("SELECT UB.*, POCB.*, CB.* FROM ci_user_birthday UB INNER JOIN ci_parent_of_child_birthday POCB ON POCB.child_id = UB.user_id
//              INNER JOIN ci_class_birthday CB ON CB.user_id = UB.user_id AND CB.school_id = %s AND type = %s
//              WHERE 1 = (FLOOR(DATEDIFF(DATE_ADD(DATE(NOW()),INTERVAL %s DAY),UB.birthday) / 365.25)) - (FLOOR(DATEDIFF(DATE(NOW()),UB.birthday) / 365.25)) AND CB.class_id = %s
//ORDER BY MONTH(UB.birthday),DAY(UB.birthday)", secure($school_id, 'int'), 1, secure($time_up), secure($class_id, 'int'));
//        } else {
//            $strSql = sprintf("SELECT UB.*, POCB.*, CB.* FROM ci_user_birthday UB INNER JOIN ci_parent_of_child_birthday POCB ON POCB.child_id = UB.user_id
//              INNER JOIN ci_class_birthday CB ON CB.user_id = UB.user_id AND CB.school_id = %s AND type = %s
//              WHERE 1 = (FLOOR(DATEDIFF(DATE_ADD(DATE(NOW()),INTERVAL %s DAY),UB.birthday) / 365.25)) - (FLOOR(DATEDIFF(DATE(NOW()),UB.birthday) / 365.25))
//ORDER BY MONTH(UB.birthday),DAY(UB.birthday)", secure($school_id, 'int'), 1, secure($time_up));
//        }
//
//        $childs = array();
//        $get_childs = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
//        if($get_childs->num_rows > 0) {
//            while($child = $get_childs->fetch_assoc()) {
//                $childs[] = $child;
//            }
//        }
//        return $childs;
//    }

    /**
     * Lấy danh sách trẻ có sinh nhật từ ngày đến ngày
     *
     * @param $school_id
     * @param $begin
     * @param $end
     * @param $class_id
     * @return array
     * @throws Exception
     */
    public function getChildBirthday($school_id, $begin, $end, $class_id) {
        global $db;

        $begin = toDBDate($begin);
        $end = toDBDate($end);
        if ($class_id != 0) {
//            $strSql = sprintf("SELECT C.*, G.group_title FROM ci_child C INNER JOIN ci_school_child SC ON SC.child_id = C.child_id AND SC.school_id = %s AND SC.class_id = %s AND SC.status = 1
//              INNER JOIN groups G ON G.group_id = %s
//              WHERE 1 = (FLOOR(DATEDIFF(DATE_ADD(%s,INTERVAL (DATEDIFF(%s, %s)) DAY),C.birthday) / 365.25)) - (FLOOR(DATEDIFF(%s,C.birthday) / 365.25))
//ORDER BY MONTH(C.birthday),DAY(C.birthday)", secure($school_id, 'int'), secure($class_id, 'int'), secure($class_id, 'int'), secure($begin), secure($end), secure($begin), secure($begin));

            $strSql = "SELECT C.*, G.group_title FROM ci_child C INNER JOIN ci_school_child SC ON SC.child_id = C.child_id AND SC.school_id = $school_id AND SC.class_id = $class_id AND SC.status = 1
              INNER JOIN groups G ON G.group_id = $class_id
              WHERE DATE_FORMAT(C.birthday, '%m%d') BETWEEN DATE_FORMAT('$begin', '%m%d') AND DATE_FORMAT('$end', '%m%d') OR (MONTH('$begin') > MONTH('$end')
                AND (MONTH(C.birthday) >= MONTH('$begin') OR MONTH(C.birthday) <= MONTH('$end')))
              ORDER BY MONTH(C.birthday),DAY(C.birthday)";
        } else {
//            $strSql = sprintf("SELECT C.*, G.group_title FROM ci_child C INNER JOIN ci_school_child SC ON SC.child_id = C.child_id AND SC.school_id = %s AND SC.status = 1
//              INNER JOIN groups G ON G.group_id = SC.class_id
//              WHERE 1 = (FLOOR(DATEDIFF(DATE_ADD(%s,INTERVAL (DATEDIFF(%s, %s)) DAY),C.birthday) / 365.25)) - (FLOOR(DATEDIFF(%s,C.birthday) / 365.25))
//ORDER BY MONTH(C.birthday),DAY(C.birthday)", secure($school_id, 'int'), secure($begin), secure($end), secure($begin), secure($begin));

            $strSql = "SELECT C.*, G.group_title FROM ci_child C INNER JOIN ci_school_child SC ON SC.child_id = C.child_id AND SC.school_id = $school_id AND SC.status = 1
              INNER JOIN groups G ON G.group_id = SC.class_id
              WHERE DATE_FORMAT(C.birthday, '%m%d') BETWEEN DATE_FORMAT('$begin', '%m%d') AND DATE_FORMAT('$end', '%m%d') OR (MONTH('$begin') > MONTH('$end') 
              AND (MONTH(C.birthday) >= MONTH('$begin') OR MONTH(C.birthday) <= MONTH('$end')))
              ORDER BY MONTH(C.birthday),DAY(C.birthday)";

//            print_r($strSql);
//            die('qqq');
        }

        $childs = array();
        $get_childs = $db->query($strSql) or _error('error', $strSql);
        if($get_childs->num_rows > 0) {
            while($child = $get_childs->fetch_assoc()) {
                $childs[] = $child;
            }
        }
        return $childs;
    }

    /**
     * Lấy ra danh phụ huynh có sinh nhật trong tháng của trường
     *
     * @return array
     */
    public function getParentBirthdayInTheMonth($school_id) {
        global $db;

        $strSql = sprintf("SELECT UB.*, COPB.* FROM ci_user_birthday UB INNER JOIN ci_child_of_parent_birthday COPB ON COPB.parent_id = UB.user_id
             AND COPB.child_school_id = %s AND type = %s", secure($school_id, 'int'), 2);

        $parents = array();
        $get_parents = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_parents->num_rows > 0) {
            while($parent = $get_parents->fetch_assoc()) {
                $parents[] = $parent;
            }
        }
        return $parents;
    }

    /**
     * Lây ra danh sách giáo viên có sinh nhật trong tháng của trường
     *
     * @return array
     */
    public function getTeacherBirthdayInTheMonth($school_id) {
        global $db;

        $strSql = sprintf("SELECT UB.*, CB.* FROM ci_user_birthday UB INNER JOIN ci_class_birthday CB ON CB.user_id = UB.user_id
              AND CB.school_id = %s AND type = %s", secure($school_id, 'int'), 3);

        $teachers = array();
        $get_teachers = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_teachers->num_rows > 0) {
            while($teacher = $get_teachers->fetch_assoc()) {
                $teachers[] = $teacher;
            }
        }
        return $teachers;
    }

    /**
     * Lây ra danh sách giáo viên có sinh nhật theo ngày được chọn
     *
     * @return array
     */
    public function getTeacherBirthday($school_id, $time_up) {
        global $db;

        $strSql = sprintf("SELECT UB.*, CB.* FROM ci_user_birthday UB INNER JOIN ci_class_birthday CB ON CB.user_id = UB.user_id
              AND CB.school_id = %s AND type = %s WHERE 1 = (FLOOR(DATEDIFF(DATE_ADD(DATE(NOW()),INTERVAL %s DAY),UB.birthday) / 365.25)) - (FLOOR(DATEDIFF(DATE(NOW()),UB.birthday) / 365.25))
ORDER BY MONTH(UB.birthday),DAY(UB.birthday)", secure($school_id, 'int'), 3, secure($time_up));

        $teachers = array();
        $get_teachers = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_teachers->num_rows > 0) {
            while($teacher = $get_teachers->fetch_assoc()) {
                $teachers[] = $teacher;
            }
        }
        return $teachers;
    }

    /**
     * Xóa danh sách giáo viên/phụ huynh/trẻ có sinh nhật trong tháng của trường
     *
     */
    public function deleteUserBirthday() {
        global $db;

        $strSql = sprintf("TRUNCATE ci_user_birthday");
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        // Xóa bảng ci_parent_of_child_birthday
        $strSql = sprintf("TRUNCATE ci_parent_of_child_birthday");
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        // Xóa bảng ci_child_of_parent_birthday
        $strSql = sprintf("TRUNCATE ci_child_of_parent_birthday");
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        // Xóa bảng ci_class_birthday
        $strSql = sprintf("TRUNCATE ci_class_birthday");
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }
}
?>