<?php

/**
 * Class SchoolDAO: thao tác với bảng pages của hệ thống. Ở đây Trường được lưu trong bảng pages.
 */
class SchoolDAO {
    /**
     * __construct
     *
     */
    public function __construct()
    {
    }

    /**
     * Kiểm tra username của school đã tồn tại chưa.
     *
     * @param string $username
     * @return boolean
     */
    private function checkUsername($username)
    {
        global $db;
        $query = $db->query(sprintf("SELECT * FROM pages WHERE page_name = %s", secure($username))) or _error(SQL_ERROR_THROWEN);

        if ($query->num_rows > 0) {
            return true;
        }
        return false;
    }

    /**
     * Kiểm tra đầu vào cho hàm insert và update
     *
     * @param array $args
     * @param bool|true $isCreate
     * @throws Exception
     */
    private function validateInput (array $args = array(), $isCreate = true) {
        /* validate title */
        if (is_empty($args['page_title'])) {
            throw new Exception(__("You must enter school name"));
        }
        if (strlen($args['page_title']) < 3) {
            throw new Exception(__("Name must be at least 3 characters long"));
        }
        /* validate username */
        if (is_empty($args['page_name'])) {
            throw new Exception(__("You must enter a username for your school"));
        }
        if (!valid_username($args['page_name'])) {
            throw new Exception(__("Please enter a valid username (a-z0-9_.)"));
        }
        /* validate username */
        if ($isCreate || (strtolower($args['page_name']) != strtolower($args['username_old']))) {
            if ($this->checkUsername($args['page_name'])) {
                throw new Exception(__("Sorry, it looks like this username") . " " . $args['page_name'] . " " . __("belongs to an existing one"));
            }
        }
        if ((!is_empty($args['telephone'])) && (strlen($args['telephone']) > 50)) {
            throw new Exception(__("Telephone length must be less than 50 characters long"));
        }
        if ((!is_empty($args['website'])) && (strlen($args['website']) > 50)) {
            throw new Exception(__("Website length must be less than 50 characters long"));
        }
        if ((!is_empty($args['email'])) && (strlen($args['email']) > 50)) {
            throw new Exception(__("Email length must be less than 50 characters long"));
        }
        if ((!is_empty($args['address'])) && (strlen($args['address']) > 400)) {
            throw new Exception(__("Address length must be less than 400 characters long"));
        }
        if (!isset($args['city_id'])) {
            throw new Exception(__("You must select a city"));
        }
//        if (!isset($args['district_slug'])) {
//            throw new Exception(__("You must select a district"));
//        }
//        if (!isset($args['longtitude'])) {
//            throw new Exception(__("You must enter longtitude"));
//        }
//        if (!isset($args['latitude'])) {
//            throw new Exception(__("You must enter latitude"));
//        }
//        if (!isset($args['type']) || $args['type'] <= 0) {
//            throw new Exception(__("You must select school type"));
//        }
    }

    /**
     * Tạo ra một TRƯỜNG trong DB
     *
     * @param array $args
     * @return mixed
     * @throws Exception
     */
    public function createSchool(array $args = array())
    {
        global $db, $user;
        $this->validateInput($args, true);

        /* insert new school */
        $strSql = sprintf("INSERT INTO pages (page_admin, page_category, page_name, page_title, page_description,
                            city_id, telephone, website, email, address)
                            VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
            secure($args['page_admin'], 'int'), SCHOOL_CATEGORY_ID, secure($args['page_name']),
            secure($args['page_title']), secure($args['page_description']), secure($args['city_id'], 'int'),
            secure($args['telephone']), secure($args['website']), secure($args['email']), secure($args['address']));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $page_id = $db->insert_id;

        /* CI - sau khi tạo ra trường thì cho like page luôn */
        /* like this page */
        $db->query(sprintf("INSERT INTO pages_likes (user_id, page_id) VALUES (%s, %s)", secure($user->_data['user_id'], 'int'),  secure($page_id, 'int') )) or _error(SQL_ERROR_THROWEN);
        /* update likes counter +1 */
        $db->query(sprintf("UPDATE pages SET page_likes = page_likes + 1  WHERE page_id = %s", secure($page_id, 'int') )) or _error(SQL_ERROR_THROWEN);

        return $page_id;
    }

    /**
     * Tạo ra một trường từ màn hình quản lý của nhân viên NOGA
     *
     * @param array $args
     * @return mixed
     * @throws Exception
     */
    public function createSchoolNoga(array $args = array())
    {
        global $db, $user;
        $this->validateInput($args, true);

        /* insert new school */
        $strSql = sprintf("INSERT INTO pages (page_admin, page_category, page_name, page_title, page_description,
                            city_id, telephone, website, email, address)
                            VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
            secure($args['page_admin'], 'int'), secure($args['page_category'], 'int'), secure($args['page_name']),
            secure($args['page_title']), secure($args['page_description']), secure($args['city_id'], 'int'),
            secure($args['telephone']), secure($args['website']), secure($args['email']), secure($args['address']));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        $page_id = $db->insert_id;

        /* page admin addation */
        $user->connect("page-admin-addation", $page_id, $args['page_admin']);

        return $page_id;
    }

    /**
     * Cập nhật thông tin TRƯỜNG
     *
     * @param array $args
     * @throws Exception
     */
    public function editSchool(array $args = array())
    {
        global $db;
        $this->validateInput($args, false);

        /* update the page */
        $db->query(sprintf("UPDATE pages SET page_name = %s, page_title = %s, page_description = %s,
                            city_id = %s, telephone = %s, website = %s, email = %s, address = %s
                    WHERE page_id = %s", secure($args['page_name']),
            secure($args['page_title']), secure($args['page_description']), secure($args['city_id'], 'int'),
            secure($args['telephone']), secure($args['website']), secure($args['email']), secure($args['address']),
            secure($args['page_id'], 'int'))) or _error(SQL_ERROR_THROWEN);

        /* update ci_school_configuration */
        $db->query(sprintf("UPDATE ci_school_configuration SET type = %s, district_slug = %s, short_overview = %s
                    WHERE school_id = %s", secure($args['type']),
            secure($args['district_slug']), secure($args['short_overview']),
            secure($args['page_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Cập nhật thông tin trường từ màn hình quản lý của nhân viên NOGA
     *
     * @param array $args
     * @throws Exception
     */
    public function editSchoolNoga(array $args = array())
    {
        global $db;
        $this->validateInput($args, false);

        /* update the page */
        $db->query(sprintf("UPDATE pages SET page_admin = %s, page_name = %s, page_title = %s, page_description = %s,
                            city_id = %s, telephone = %s, website = %s, email = %s, address = %s
                    WHERE page_id = %s", secure($args['page_admin'], 'int'), secure($args['page_name']),
            secure($args['page_title']), secure($args['page_description']), secure($args['city_id'], 'int'),
            secure($args['telephone']), secure($args['website']), secure($args['email']), secure($args['address']),
            secure($args['page_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Lấy ra thông tin School theo username. Thông tin được lấy ra bao gồm cả ROLE_ID
     *
     * @param $username
     * @return array
     * @throws Exception
     */
    public function getSchoolByUsername($username) {
        $schoolId = $_SESSION[$username];
        if (is_null($schoolId)) {
            $schoolId = $this->getSchoolIdByUsername($username);
            $_SESSION[$username] = $schoolId;
        }

        return $schoolId;
    }

    /**
     * Lấy ra thông tin school_id theo username
     *
     * @param $username
     * @return array
     * @throws Exception
     */
    public function getSchoolIdByUsername($username) {
        global $db;

        $strSql = sprintf("SELECT page_id FROM pages WHERE page_name = %s",
                           secure($username));

        $get_school = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_school->num_rows == 0) {
            _error(404);
        }
        return $get_school->fetch_assoc()['page_id'];
    }

    /**
     * Lấy ra danh sách school do một user làm admin
     *
     * @param $userId
     * @return array
     * @throws Exception
     */
    public function getSchoolsByAdmin($userId) {
        global $db;

        $strSql = sprintf("SELECT * FROM pages WHERE page_admin = %s ORDER BY page_title ASC", secure($userId, 'int'));

        $schools = array();
        $get_schools = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_schools->num_rows > 0) {
            while($school = $get_schools->fetch_assoc()) {
                $school['page_picture'] = get_picture($school['page_picture'], 'school');
                $schools[] = $school;
            }
        }
        return $schools;
    }

    /**
     * Lấy ra danh sách trường liên quan đến một user. Bao gồm:
     * - Trường mà user là admin
     * - Trường mà user đó có vai trò.
     * - Trường mà user đó là giáo viên.
     *
     * @param $userId
     * @return array
     * @throws Exception
     */
    public function getSchoolsHaveRole($userId) {
        global $db;

        $strSql = sprintf('SELECT P0.*,-1 AS role_id,SC.* FROM pages P0
                            INNER JOIN ci_school_configuration SC ON P0.page_id = SC.school_id AND P0.page_admin = %1$s
                          UNION
                          SELECT P.*,UR.role_id,SC.* FROM pages P INNER JOIN ci_user_role UR
                            ON P.page_id = UR.school_id AND UR.user_id = %1$s
                            INNER JOIN ci_school_configuration SC ON P.page_id = SC.school_id
                          UNION
                          SELECT P2.*,0 AS role_id,SC.* FROM pages P2 INNER JOIN ci_teacher T
                            ON P2.page_id = T.school_id AND T.status = %2$s AND T.user_id = %1$s
                            INNER JOIN ci_school_configuration SC ON P2.page_id = SC.school_id
                          ORDER BY role_id DESC, page_title ASC', secure($userId, 'int'), STATUS_ACTIVE);

        $newSchools = array();
        $get_schools = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_schools->num_rows > 0) {
            $schools = array();
            while($school = $get_schools->fetch_assoc()) {
                $school['page_picture'] = get_picture($school['page_picture'], 'school');
                $schools[] = $school;
            }

            //02 bước sau để loại bỏ những trường xuất hiện 2 lần.
            //1. Lấy ra tất cả các trường mà user là admin
            $notAdminSchools = array();
            foreach ($schools as $school) {
                $newSchool = $school;
                if ($school['role_id'] == -1) {
                    $newSchools[] = $newSchool;
                } else {
                    $notAdminSchools[] = $newSchool;
                }
            }

            //2. Lấy ra những trường có role != admin
            foreach ($notAdminSchools as $notAdminSchool) {
                //Kiểm tra trong danh sách đã có trường đó chưa
                $ignore = false;
                foreach ($newSchools as $newSchool) {
                    if (($newSchool['page_id'] == $notAdminSchool['page_id']) &&
                        (($newSchool['role_id'] >= $notAdminSchool['role_id']) || ($newSchool['role_id'] == -1))
                    ) {
                        $ignore = true;
                        break;
                    }
                }
                if (!$ignore) {
                    $tmpSchool = $notAdminSchool;
                    $newSchools[] = $tmpSchool;
                }
            }

        }
        return $newSchools;
    }

    /**
     * Lấy ra danh sách trường trong một thành phố. Phục vụ cho chức năng quản lý của NOGA với tài khoản được phân công theo thành phố
     *
     * @param $cityId
     * @return array
     * @throws Exception
     */
    public function getSchoolInCity($cityId) {
        global $db;

        $strSql = sprintf("SELECT P.*, SC.school_status FROM pages P
                INNER JOIN ci_school_configuration SC ON SC.school_id = P.page_id
                WHERE city_id = %s AND page_category = %s ORDER BY page_title ASC", secure($cityId, 'int'), SCHOOL_CATEGORY_ID);

        $schools = array();
        $get_schools = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_schools->num_rows > 0) {
            while($school = $get_schools->fetch_assoc()) {
                $school['page_picture'] = get_picture($school['page_picture'], 'school');
                $schools[] = $school;
            }
        }
        return $schools;
    }

    /**
     * Lấy ra danh sách tất cả các trường trong hệ thống. Phục vụ cho chức năng quản lý của NOGA
     *
     * @param int $from
     * @param int $limit
     * @return array
     * @throws Exception
     */
    public function getAllSchools($from = 0, $limit = 0) {
        global $db;

        if ($from > 0) {
            $strSql = sprintf("SELECT P.*, SC.school_status FROM pages P
                      INNER JOIN ci_school_configuration SC ON SC.school_id = P.page_id
                      WHERE page_category = %s ORDER BY page_title ASC LIMIT %s, %s", SCHOOL_CATEGORY_ID, secure($from, 'int'), secure($limit, 'int'));
        } else {
            $strSql = sprintf("SELECT P.*, SC.school_status FROM pages P
                      INNER JOIN ci_school_configuration SC ON SC.school_id = P.page_id
                      WHERE page_category = %s ORDER BY page_title ASC", SCHOOL_CATEGORY_ID);
        }

        $schools = array();
        $get_schools = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_schools->num_rows > 0) {
            while($school = $get_schools->fetch_assoc()) {
                $school['page_picture'] = get_picture($school['page_picture'], 'school');
                $schools[] = $school;
            }
        }
        return $schools;
    }

    /**
     * Lấy ra danh sách các trường được quản lý bởi 1 nhân viên của cty NOGA
     *
     * @param $userId
     * @return array
     * @throws Exception
     */
    public function getSchoolOfNOGAUser($userId) {
        global $db;

        $strSql = "SELECT P.*, NM.role_id, SC.school_status FROM pages P INNER JOIN ci_noga_manage NM ON P.page_id = NM.school_id
                    INNER JOIN ci_school_configuration SC ON SC.school_id = P.page_id
                    WHERE P.page_category = %s AND NM.user_id = %s ORDER BY P.page_title ASC";

        $schools = array();
        $get_schools = $db->query(sprintf($strSql, SCHOOL_CATEGORY_ID, secure($userId, 'int'))) or _error(SQL_ERROR_THROWEN);
        if($get_schools->num_rows > 0) {
            while($school = $get_schools->fetch_assoc()) {
                $school['page_picture'] = get_picture($school['page_picture'], 'school');
                $schools[] = $school;
            }
        }
        return $schools;
    }

    /**
     * Lấy ra danh sách user đánh giá 1 trường
     *
     * @param $school_id
     * @return array
     * @throws Exception
     */
    public function getUserRating($school_id, $offset = 0) {
        global $db, $system;

        $offset *= $system['max_results'];
        $strSql = sprintf("SELECT R.*, U.user_fullname, U.user_name, U.user_picture, U.user_gender FROM ci_user_review R INNER JOIN users U ON R.user_id = U.user_id                   
                    WHERE R.type = 'school' AND R.school_id = %s ORDER BY R.rating ASC LIMIT %s, %s", secure($school_id, 'int'), secure($offset, 'int', false), secure($system['max_results'], 'int', false));

        $userRatings = array();
        $get_ratings = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_ratings->num_rows > 0) {
            while($rating = $get_ratings->fetch_assoc()) {
                $rating['user_picture'] = get_picture($rating['user_picture'], $rating['user_gender']);
                $userRatings[] = $rating;
            }
        }
        return $userRatings;
    }

    /**
     * Lấy ra ADMIN_ID của trường
     *
     * @param $school_id
     * @return int
     * @throws Exception
     */
    public function getAdminId($school_id) {
        global $db;

        $strSql = sprintf("SELECT page_admin FROM pages WHERE page_id = %s", secure($school_id, 'int'));

        $get_school = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_school->num_rows > 0) {
            return $get_school->fetch_assoc()['page_admin'];
        }

        return 0;
    }

    /**
     * Lấy ra thông tin admin của trường
     *
     * @param $school_id
     * @return array|null
     */
    public function getAdmin($school_id) {
        global $db;

        /* CI-mobile lấy ra thêm sdt của quản lý trường */
        $strSql = sprintf("SELECT U.user_id, U.user_name, U.user_fullname, U.user_gender, U.user_picture, U.user_phone FROM users U INNER JOIN pages P
                            ON U.user_id = P.page_admin AND P.page_id = %s", secure($school_id, 'int'));

        $get_admin = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $user = null;
        if($get_admin->num_rows > 0) {
            $user = $get_admin->fetch_assoc();
        }

        return $user;
    }

    /**
     * Lấy ra thông tin admin của trường
     *
     * @param $user_id
     * @return array|null
     */
    public function getAdminById($user_id) {
        global $db;

        $strSql = sprintf("SELECT user_id, user_name, user_fullname, user_email, user_phone, user_picture, user_gender FROM users WHERE user_id = %s", secure($user_id, 'int'));

        $get_user = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $user = null;
        if ($get_user->num_rows > 0) {
            $user = $get_user->fetch_assoc();
            $user['user_picture'] = get_picture($user['user_picture'], $user['user_gender']);
        }
        return $user;
    }

    /**
     * Lấy ra thông tin school khi biết school_id và user_id
     *
     * @param $school_id
     * @param $user_id
     * @return array|null
     * @throws Exception
     */
    public function getSchool($school_id, $user_id) {
        global $db;

        $strSql = sprintf("SELECT P.*, UM.role_id FROM pages P INNER JOIN ci_user_manage UM ON P.page_id = UM.object_id
            WHERE UM.object_type = %s AND UM.user_id = %s AND P.page_id = %s",
            MANAGE_SCHOOL, secure($user_id, 'int'), secure($school_id, 'int'));

        $get_school = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_school->num_rows > 0) {
            return $get_school->fetch_assoc();
        }

        return null;
    }

    /**
     * Lấy ra thông tin của trường khi biết school_id và ID của nhân viên NOGA
     *
     * @param $schoolId
     * @param $userId
     * @return array|null
     * @throws Exception
     */
    public function getSchoolNoga($schoolId, $userId) {
        global $db;

        $strSql = sprintf("SELECT P.*, NM.role_id FROM pages P INNER JOIN ci_noga_manage NM ON
                      (P.page_id = %s) AND (NM.user_id = %s) AND
                      (((NM.manage_type = %s) AND (P.page_id = NM.school_id)) OR ((NM.manage_type = %s) AND (P.city_id = NM.city_id)))",
                        secure($schoolId, 'int'), secure($userId, 'int'), NOGA_MANAGE_TYPE_SCHOOL, NOGA_MANAGE_TYPE_CITY);

        $get_school = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_school->num_rows > 0) {
            return $get_school->fetch_assoc();
        }

        return null;
    }

    /**
     * CI-Mobile Lấy ra thông tin school khi biết school_id
     *
     * @param $school_id
     * @return array|null
     * @throws Exception
     */
    public function getSchoolById($school_id) {
        global $db;

        $strSql = sprintf("SELECT * FROM pages WHERE page_id = %s", secure($school_id));

        $get_school = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $school = null;
        if($get_school->num_rows > 0) {
            $school = $get_school->fetch_assoc();
            $school['page_picture'] = get_picture($school['page_picture'], 'school');
            $school['page_cover'] = get_picture($school['page_cover'], 'school');
        }

        return $school;
    }

    /**
     * Lấy thông tin TRƯỜNG khi biết LỚP
     *
     * @param $class_id
     * @return array
     * @throws Exception
     */
    public function _getSchoolByClass($class_id) {
        global $db;

        $strInCond = "SELECT B.school_id FROM ci_class_level B INNER JOIN groups C ON B.class_level_id = C.class_level_id WHERE C.group_id = %s";
        $strSql = sprintf("SELECT A.* FROM pages A WHERE A.page_category = %s AND A.page_id IN (".$strInCond.")",
            SCHOOL_CATEGORY_ID, secure($class_id, 'int'));

        $school = null;
        $get_school = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_school->num_rows > 0) {
            $school = $get_school->fetch_assoc();
            $school['page_picture'] = get_picture($school['page_picture'], 'school');
            $school['config'] = getSchoolData($school['page_id'], SCHOOL_CONFIG);
        };

        return $school;
    }

    /**
     * Lấy thông tin TRƯỜNG khi biết LỚP
     *
     * @param $class_id
     * @return array
     */
    public function getSchoolByClass($class_id) {
        global $system;
        if (isset($system['is_mobile']) && $system['is_mobile']) {
            $school = $this->_getSchoolByClass($class_id);
        } else {
            $key = "school_of_class_".$class_id;
            $school = $_SESSION[$key];
            if (is_null($school)) {
                $school = $this->_getSchoolByClass($class_id);
                $_SESSION[$key] = $school;
            }
        }


        return $school;
    }

    /**
     * Nhập danh sách user like trang của trường
     *
     * @param $schoolId
     * @param $userIds
     * @throws Exception
     */
    public function addUsersLikeSchool($schoolId, $userIds) {
        global $db;

        $userIds = array_unique($userIds); //Bỏ đi giá trị bị lặp
        $likedUserIds = $this->_getLikedUserId($schoolId, $userIds);

        //Bỏ đi những user đã like page rồi
        $userIds = array_diff($userIds, $likedUserIds);

        $likeCnt = count($userIds);
        if ($likeCnt == 0) return 0;

        //1. Đưa danh sách user vào nhóm
        //Câu sql sẽ có dạng: INSERT INTO tbl_name (col_A,col_B,col_C) VALUES (1,2,3), (4,5,6), (7,8,9)
        //Build lên phần giá trị truyền vào.
        $strValues = "";
        foreach ($userIds as $id) {
            //page_id, user_id
            $strValues .= "(". secure($schoolId, 'int') .",". secure($id, 'int') ."),";
        }
        $strValues = trim($strValues, ",");

        $strSql = "INSERT INTO pages_likes (page_id, user_id) VALUES ".$strValues;
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        //2. Tăng số thành viên của group
        $strSql = sprintf("UPDATE pages SET page_likes = page_likes + %s WHERE page_id = %s", count($userIds), $schoolId);
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        return $likeCnt;
    }

    /**
     * Lấy ra danh sách ID của user đã like page từ một tập ID cho trước
     *
     * @param $pageId
     * @param $userIds
     * @return array
     * @throws Exception
     */
    private function _getLikedUserId($pageId, $userIds) {
        global $db;

        $strCon = implode(',', $userIds);
        $strSql = sprintf("SELECT user_id FROM pages_likes WHERE page_id = %s AND user_id IN (%s)", secure($pageId, 'int'), $strCon);

        $likedUserIds = array();
        $get_users = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_users->num_rows > 0) {
            while($user = $get_users->fetch_assoc()) {
                $likedUserIds[] = $user['user_id'];
            }
        }
        return $likedUserIds;
    }

    /**
     * Xóa danh sách user khỏi like trang của trường
     *
     * @param $schoolId
     * @param $userIds
     * @throws Exception
     */
    public function deleteUserLikeSchool($schoolId, $userIds) {
        global $db;

        $userIds = array_unique($userIds); //Bỏ đi giá trị bị lặp
        $strCon = implode(',', $userIds);

        $strSql = sprintf("DELETE FROM pages_likes WHERE page_id = %s AND user_id IN (%s)", secure($schoolId, 'int'), $strCon);
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        //2. Tăng số thành viên của group
        $strSql = sprintf('UPDATE pages SET page_likes = IF(page_likes < %1$s, 0, page_likes - %1$s) WHERE page_id = %2$s', count($userIds), $schoolId);
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Tạo bộ cấu hình mặc định cho trường.
     *
     * @param $school_id
     */
    public function createSchoolProfile($school_id, $args = array()) {
        global $db;
        $strSql = sprintf("INSERT INTO ci_school_configuration (school_id, school_group_id, type, lat, lng, city_slug, city_name, district_slug, district_name, directions, 
                                    short_overview, grade) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", secure($school_id, 'int'),
                                    secure($args['inside_system_school']), secure($args['school_type']), secure($args['latitude']), secure($args['longtitude']),
                                    secure($args['city_slug']), secure($args['city_name']), secure($args['district_slug']), secure($args['district_name']),
                                    secure($args['direct']), secure($args['short_description']), secure($args['grade'], 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Update bộ cấu hình mặc định cho trường.
     *
     * @param $school_id
     */
    public function updateSchoolProfile($school_id, $args = array()) {
        global $db;

        $strSql = sprintf("UPDATE ci_school_configuration SET school_id = %s, school_group_id = %s, type = %s, lat = %s, lng = %s, city_slug = 
%s, city_name = %s, district_slug = %s, district_name = %s, directions = %s, short_overview = %s, grade = %s WHERE school_id = %s", secure($school_id, 'int'),
            secure($args['inside_system_school']), secure($args['school_type']), secure($args['latitude']), secure($args['longtitude']), secure($args['city_slug']),
            secure($args['city_name']), secure($args['district_slug']), secure($args['district_name']), secure($args['direct']), secure($args['short_description']), secure($args['grade'], 'int'),
            secure($school_id, 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Lấy ra thông tin cấu hình của trường
     *
     * @param $school_id
     * @return array|null
     */
    public function getConfiguration($school_id) {
        global $db, $system;

        //$strSql = sprintf("SELECT SC.*, P.page_name, P.page_admin FROM ci_school_configuration SC INNER JOIN pages P ON SC.school_id = P.page_id AND SC.school_id = %s", secure($school_id, 'int'));
        $strSql = sprintf("SELECT * FROM ci_school_configuration WHERE school_id = %s", secure($school_id, 'int'));

        $get_config = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_config->num_rows > 0) {
            $school = $get_config->fetch_assoc();
            if (!is_empty($school['camera_guide'])) {
                $school['camera_guide'] = $system['system_uploads'] . '/' . $school['camera_guide'];
            }
            return $school;
        }

        return null;
    }

    /**
     * Lấy ra cấu hình của tất cả các trường đang sử dụng Coniu.
     *
     * @return array
     * @throws Exception
     */
    public function getConfigurations() {
        global $db;

        $strSql = "SELECT SC.school_id, SC.notification_tuition_interval, P.page_admin FROM ci_school_configuration SC INNER JOIN pages P ON P.page_id = SC.school_id WHERE SC.school_status = '1'";
        $results = array();
        $get_config = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_config->num_rows > 0) {
            while ($config = $get_config->fetch_assoc()) {
                $results[] = $config;
            }
        }

        return $results;
    }

    /**
     * Cập nhật cấu hình trường
     *
     * @param $school_id
     * @param $allow_comment
     * @param $display_children_list
     * @param $display_parent_info_for_others
     * @param $allow_class_see_tuition
     * @param $allow_parent_register_service
     * @param $allow_teacher_rolls_days_before
     * @param $allow_teacher_edit_pickup_before
     * @param $school_allow_medicate
     */
    public function updateSchoolConfig($school_id, $allow_comment, $display_children_list, $display_parent_info_for_others, $allow_parent_register_service, $allow_teacher_edit_pickup_before, $school_allow_medicate) {
        global $db;
        $strSql = sprintf('UPDATE ci_school_configuration SET school_allow_comment = %s, display_children_list = %s, display_parent_info_for_others = %s, allow_parent_register_service = %s, allow_teacher_edit_pickup_before = %s, school_allow_medicate = %s WHERE school_id = %s',
                    secure($allow_comment, 'int'), secure($display_children_list, 'int'), secure($display_parent_info_for_others, 'int'), secure($allow_parent_register_service, 'int'), secure($allow_teacher_edit_pickup_before, 'int'), secure($school_allow_medicate, 'int'), secure($school_id, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }
    /**
     * Cập nhật cấu hình Trường Noga phần cơ sở vật chất
     *
     * @param $args
     * @throws Exception
     */
    public function updateSchoolConfigNogaFacility($args = array()) {
        global $db;

        $strSql = sprintf('UPDATE ci_school_configuration SET facility_pool = %s, facility_playground_out = %s, facility_playground_in = %s, facility_library = %s, facility_camera = %s, note_for_facility =%s WHERE school_id = %s',
            secure($args['pool']), secure($args['outdoor']), secure($args['indoor']), secure($args['library']), secure($args['viewcamera']), secure($args['note']), secure($args['school_id']));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }


    /**
     * Cập nhật cấu hình Trường Noga phần dịch vụ
     *
     * @param $args
     * @throws Exception
     */
    public function updateSchoolConfigNogaServices($args = array()) {
        global $db;

        $strSql = sprintf('UPDATE ci_school_configuration SET service_breakfast = %s, service_belated = %s, service_saturday = %s, service_bus = %s, note_for_service =%s WHERE school_id = %s',
            secure($args['breakfast']), secure($args['belated']), secure($args['saturday']), secure($args['bus']), secure($args['note']), secure($args['school_id']));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Cập nhật cấu hình Trường Noga phần addmission
     *
     * @param $args
     * @throws Exception
     */
    public function updateSchoolConfigNogaAddmission($args = array()) {
        global $db;

        $strSql = sprintf('UPDATE ci_school_configuration SET start_age = %s, end_age = %s, start_tuition_fee = %s, end_tuition_fee = %s, note_for_tuition =%s, admission =%s, note_for_admission =%s WHERE school_id = %s',
            secure($args['age_begin']), secure($args['age_end']), secure($args['tuition_begin']), secure($args['tuition_end']), secure($args['tuition_note']), secure($args['addmission']), secure($args['addmission_note']), secure($args['school_id']));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }
    /**
     * Cập nhật cấu hình Trường Noga phần info
     *
     * @param $args
     * @throws Exception
     */
    public function updateSchoolConfigNogaInfo($args = array()) {
        global $db;

        $strSql = sprintf('UPDATE ci_school_configuration SET info_leader = %s, info_method = %s, info_teacher = %s, info_nutrition = %s WHERE school_id = %s',
            secure($args['leader']), secure($args['method']), secure($args['teacher']), secure($args['nutrition']), secure($args['school_id'], 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Lấy ra album ảnh cơ sở vật chất của trường
     *
     * @param $schoolId
     * @throws Exception
     */
    public function getSchoolFacilities($schoolId){
        global $db, $system;

        $results = array();
        $strSql = sprintf('SELECT source FROM ci_school_photos WHERE school_id = %s', secure($schoolId, 'int'));
        $get_photos = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        if($get_photos->num_rows > 0) {
            while ($photo = $get_photos->fetch_assoc()) {
                if (!is_empty($photo['source'])) {
                    $photo['source'] = $system['system_uploads'] . '/' . $photo['source'];
                }
                $results[] = $photo;
            }
        }

        return $results;
    }


    /**
     * Cập nhật cấu hình phần học phí
     *
     * @param $school_id
     * @param $tuition
     * @param $bank_account
     * @param $allow_class_see_tuition
     * @param $tuition_view_attandance
     * @param $tuition_use_mdservice_deduction
     * @param $tuition_child_report_template
     * @throws Exception
     */
    public function updateTuitionConfig($school_id, $tuition, $bank_account, $allow_class_see_tuition, $tuition_view_attandance, $tuition_use_mdservice_deduction, $tuition_child_report_template) {
        global $db;
        $strSql = sprintf('UPDATE ci_school_configuration SET notification_tuition_interval = %s, bank_account = %s, allow_class_see_tuition = %s, tuition_view_attandance = %s,
                    tuition_use_mdservice_deduction = %s, tuition_child_report_template = %s WHERE school_id = %s',
                    secure($tuition, 'int'), secure($bank_account), secure($allow_class_see_tuition, 'int'), secure($tuition_view_attandance, 'int'),
                    secure($tuition_use_mdservice_deduction, 'int'), secure($tuition_child_report_template), secure($school_id, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Cập nhật cấu hình phần class
     *
     * @param $school_id
     * @param $parent_post
     * @param $create_event
     * @param $class_allow_comment
     * @param $children_use_no_class
     * @throws Exception
     */
    public function updateClassConfig($school_id, $parent_post, $create_event, $class_allow_comment, $children_use_no_class) {
        global $db;
        $strSql = sprintf('UPDATE ci_school_configuration SET class_allow_post = %s, allow_class_event = %s, class_allow_comment = %s, children_use_no_class = %s WHERE school_id = %s',
            secure($parent_post, 'int'), secure($create_event, 'int'), secure($class_allow_comment, 'int'), secure($children_use_no_class, 'int'), secure($school_id, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Cập nhật cấu hình phần điểm danh
     *
     * @param $school_id
     * @param $attendance_allow_delete_old
     * @param $attendance_use_leave_early
     * @param $attendance_use_come_late
     * @param $attendance_absence_no_reason
     * @throws Exception
     */
    public function updateAttendanceConfig($school_id, $allow_teacher_rolls_days_before, $attendance_allow_delete_old, $attendance_use_leave_early, $attendance_use_come_late, $attendance_absence_no_reason) {
        global $db;
        $strSql = sprintf('UPDATE ci_school_configuration SET allow_teacher_rolls_days_before = %s, attendance_allow_delete_old = %s, attendance_use_leave_early = %s, attendance_use_come_late = %s, attendance_absence_no_reason = %s WHERE school_id = %s',
            secure($allow_teacher_rolls_days_before, 'int'), secure($attendance_allow_delete_old, 'int'), secure($attendance_use_leave_early, 'int'), secure($attendance_use_come_late, 'int'), secure($attendance_absence_no_reason, 'int'), secure($school_id, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Cập nhật số lượng giáo viên của trường
     *
     * @param $school_id
     * @param $value
     */
    public function updateTeacherCount($school_id, $value) {
        global $db;
        $strSql = sprintf('UPDATE ci_school_configuration SET teacher_count = IF(teacher_count + %1$s < 0, 0, teacher_count + %1$s) WHERE school_id = %2$s',
            secure($value, 'int'), secure($school_id, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Cập nhật số lượng class của trường
     *
     * @param $school_id
     * @param $value
     */
    public function updateClassCount($school_id, $value) {
        global $db;
        $strSql = sprintf('UPDATE ci_school_configuration SET class_count = IF(class_count + %1$s < 0, 0, class_count + %1$s) WHERE school_id = %2$s',
            secure($value, 'int'), secure($school_id, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Cập nhật số học sinh nam/nữ của trường
     *
     * @param $school_id
     * @param $gender
     * @param $value
     */
    public function updateGenderCount($school_id, $gender, $value) {
        global $db;
        if ($gender == MALE) {
            $strSql = sprintf('UPDATE ci_school_configuration SET male_count = IF(male_count + %1$s < 0, 0, male_count + %1$s) WHERE school_id = %2$s',
                secure($value, 'int'), secure($school_id, 'int'));
        } else {
            $strSql = sprintf('UPDATE ci_school_configuration SET female_count = IF(female_count + %1$s < 0, 0, female_count + %1$s) WHERE school_id = %2$s',
                secure($value, 'int'), secure($school_id, 'int'));
        }
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Xóa một trường khỏi hệ thống.
     * Trường bản thân nó là một page, nên khi xóa trường:
     * - Hệ thống xóa page (thuộc về phần lõi hệ thống)
     * - Module ConIu chỉ xóa thêm những thành phần phụ thuộc khác.
     *
     * @param integer $page_id
     * @return void
     */
    public function deleteSchool($page_id)
    {
        global $db;

        //TODO
    }

    /**
     * Insert hệ thống trường

     * @param
     * @return insert_id
     */
    public function insertSystemSchools($args = array()) {
        global $db, $date;

        $strSql = sprintf("INSERT INTO ci_school_group (school_group_name, school_group_title, description, created_at, 
created_user_id) VALUES (%s, %s, %s, %s, %s)", secure($args['system_username']), secure($args['system_title']), secure($args['system_description']), secure($date), secure($args['created_user_id']));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        return $db->insert_id;
    }

    /**
     * Lấy tất cả hệ thống trường

     * @param
     * @return array
     */
    public function getSystemSchools() {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_school_group");
        $getSystemSchools = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $systemSchools = array();
        if($getSystemSchools->num_rows > 0) {
            while($systemSchool = $getSystemSchools->fetch_assoc()) {
                $systemSchools[] = $systemSchool;
            }
        }
        return $systemSchools;
    }

    /**
     * Edit hệ thống trường

     * @param
     * @return array
     */
    public function editSystemSchools($args = array()) {
        global $db, $date;

        $strSql = sprintf("UPDATE ci_school_group SET school_group_title = %s, school_group_name = %s, description = %s, updated_at = %s WHERE 
school_group_id = %s", secure($args['system_title']), secure($args['system_username']), secure($args['system_description']), secure($date), secure($args['system_school_id'], 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Lấy tất thống trường theo id

     * @param
     * @return array
     */
    public function getSystemSchoolsById($systemSchoolId) {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_school_group WHERE school_group_id = %s", secure($systemSchoolId));
        $getSystemSchools = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $systemSchools = array();
        if($getSystemSchools->num_rows > 0) {
            $systemSchools = $getSystemSchools->fetch_assoc();
        }
        return $systemSchools;
    }

    /**
     * Kiểm tra xem user đã cấu hình notification chưa
     * @param $user_id
     * @param $school_id
     * @return bool
     */
    public function checkNotificationSetting($user_id, $school_id) {
        global $db;

        $strSql = sprintf("SELECT user_id, school_id FROM ci_notifications_setting WHERE user_id = %s AND school_id = %s", secure($user_id, 'int'), secure($school_id, 'int'));

        $get_notification = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_notification->num_rows > 0) {
            return true;
        }
        return false;
    }

    /**
     * Insert cấu hình thông báo
     * @param $user_id
     * @param $school_id
     * @param $teacher
     * @param $parent
     * @return mixed
     */
    public function insertNotificationSetting($user_id, $school_id, $teacher, $parent) {
        global $db;

        if(count($teacher) != count($parent)) {
            _error(404);
        }
        $notifyStatus = array();
        for ($i = 0; $i < count($teacher); $i++) {
            if($teacher[$i] && $parent[$i]) {
                $notifyStatus[] = NOTIFY_ALL;
            } elseif ($teacher[$i] && !$parent[$i]) {
                $notifyStatus[] = NOTIFY_ONLY_TEACHER;
            } elseif (!$teacher[$i] && $parent[$i]) {
                $notifyStatus[] = NOTIFY_ONLY_PARENT;
            } else {
                $notifyStatus[] = NOTIFY_NO;
            }
        }
        $notifySta = implode(',',$notifyStatus);
        $strValues = "(" . $user_id . "," . $school_id . "," . $notifySta . "),";
        $strValues = trim($strValues, ",");
        $strSql = "INSERT INTO ci_notifications_setting (user_id, school_id, events, attendance, feedback, tuitions, medicines, pickup, reports, services, children) VALUES ".$strValues;
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        return $db->insert_id;
    }

    /**
     * Xóa thông tin cấu hình notification
     * @param $user_id
     * @param $school_id
     */
    public function deleteNotificationSetting($user_id, $school_id) {
        global $db;

        $strSql = sprintf("DELETE FROM ci_notifications_setting WHERE user_id = %s AND school_id = %s", secure($user_id, 'int'), secure($school_id, 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Lấy thông tin cấu hình notification theo user và school
     * @param $user_id
     * @param $school_id
     * @return null
     */
    public function getNotificationSetting($user_id, $school_id) {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_notifications_setting WHERE user_id = %s AND school_id = %s", secure($user_id, 'int'), secure($school_id, 'int'));

        $get_notification_setting = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $notifySetting = null;
        if($get_notification_setting->num_rows > 0) {
            $notifySetting = $get_notification_setting->fetch_assoc();
        }
        return $notifySetting;
    }

    public function getUserNotificationSettingOfSchool($school_id) {
        global $system;

        $key = "notification_setting_".$school_id;
        $notification_setting_users = $_SESSION[$key];
        if (is_null($notification_setting_users)) {
            $notification_setting_users = $this->_getUserNotificationSettingOfSchool($school_id);
            $_SESSION[$key] = $notification_setting_users;
        }
        return $notification_setting_users;
    }
    /**
     * Lấy thông tin cấu hình notify của một trường
     * @param $school_id
     * @return array
     */
    private function _getUserNotificationSettingOfSchool ($school_id){
        global $db;

        $strSql = sprintf("SELECT * FROM ci_notifications_setting WHERE school_id = %s", secure($school_id, 'int'));

        $get_notification_setting_users = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $notification_setting_users = array();
        if($get_notification_setting_users->num_rows > 0) {
            while ($temp = $get_notification_setting_users->fetch_assoc()) {
                $notification_setting_users[] = $temp;
            }
        }
        return $notification_setting_users;
    }


    /* ---------- SCHOOL - MEMCACHE ---------- */
    /**
     * Lấy thông tin cấu hình notify của một trường
     * @param $school_id
     * @return array
     */
    public function getUserNotificationSetting4Memcache($school_id){
        global $db;

        $strSql = sprintf("SELECT * FROM ci_notifications_setting WHERE school_id = %s", secure($school_id, 'int'));

        $get_notifications = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $notifications = array();
        if($get_notifications->num_rows > 0) {
            while ($notification = $get_notifications->fetch_assoc()) {
                $notifications[$notification['user_id']] = $notification;
            }
        }
        return $notifications;
    }

    /**
     * Lấy ra danh sách trường liên quan đến một user. Bao gồm:
     * - Trường mà user là admin
     * - Trường mà user đó có vai trò.
     * - Trường mà user đó là giáo viên.
     *
     * @param $userId
     * @return array
     * @throws Exception
     */
    public function getSchoolsHaveRole4Memcache($userId) {
        global $db;

        $strSql = sprintf('SELECT P0.page_id, -1 AS role_id FROM pages P0
                            INNER JOIN ci_school_configuration SC ON P0.page_id = SC.school_id AND P0.page_admin = %1$s
                          UNION
                          SELECT P.page_id, UR.role_id FROM pages P INNER JOIN ci_user_role UR
                            ON P.page_id = UR.school_id AND UR.user_id = %1$s
                            INNER JOIN ci_school_configuration SC ON P.page_id = SC.school_id
                          UNION
                          SELECT P2.page_id, 0 AS role_id FROM pages P2 INNER JOIN ci_teacher T
                            ON P2.page_id = T.school_id AND T.status = %2$s AND T.user_id = %1$s
                            INNER JOIN ci_school_configuration SC ON P2.page_id = SC.school_id
                          ORDER BY role_id DESC', secure($userId, 'int'), STATUS_ACTIVE);

        $newSchools = array();
        $get_schools = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_schools->num_rows > 0) {
            $schools = array();
            while($school = $get_schools->fetch_assoc()) {
                $schools[] = $school;
            }

            //02 bước sau để loại bỏ những trường xuất hiện 2 lần.
            //1. Lấy ra tất cả các trường mà user là admin
            $notAdminSchools = array();
            foreach ($schools as $school) {
                $newSchool = $school;
                if ($school['role_id'] == -1) {
                    $newSchools[] = $newSchool;
                } else {
                    $notAdminSchools[] = $newSchool;
                }
            }

            //2. Lấy ra những trường có role != admin
            foreach ($notAdminSchools as $notAdminSchool) {
                //Kiểm tra trong danh sách đã có trường đó chưa
                $ignore = false;
                foreach ($newSchools as $newSchool) {
                    if (($newSchool['page_id'] == $notAdminSchool['page_id']) &&
                        (($newSchool['role_id'] >= $notAdminSchool['role_id']) || ($newSchool['role_id'] == -1))
                    ) {
                        $ignore = true;
                        break;
                    }
                }
                if (!$ignore) {
                    $tmpSchool = $notAdminSchool;
                    $newSchools[] = $tmpSchool;
                }
            }

        }
        return $newSchools;
    }

    /**
     * Cập nhật danh sách hiệu trưởng của trường
     *
     * @param $schoolId
     * @param $postUserIds
     * @throws Exception
     */
    public function updateSchoolPrincipal($schoolId, $postUserIds) {
        global $db;

        $principals = implode(',', $postUserIds);

        $strSql = sprintf("UPDATE ci_school_configuration SET principal = %s WHERE school_id = %s", secure($principals), secure($schoolId, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Lấy tất cả các trường đang sử dụng coniu
     *
     * @param $school_status
     * @param int $from
     * @param int $limit
     * @return array
     * @throws Exception
     */
    public function getAllSchoolsUsingInet($school_status, $from = 0, $limit = 0) {
        global $db;

        if($school_status != 0) {
            if ($from > 0) {
                $strSql = sprintf("SELECT * FROM pages P
                  INNER JOIN ci_school_configuration SC ON P.page_id = SC.school_id
                  WHERE page_category = %s AND SC.school_status = %s ORDER BY page_title ASC LIMIT %s, %s", SCHOOL_CATEGORY_ID, secure($school_status, 'int'), secure($from, 'int'), secure($limit, 'int'));
            } else {
                $strSql = sprintf("SELECT * FROM pages P
                  INNER JOIN ci_school_configuration SC ON P.page_id = SC.school_id
                  WHERE page_category = %s AND SC.school_status = %s ORDER BY page_title ASC", SCHOOL_CATEGORY_ID, secure($school_status, 'int'));
            }
        } else {
            if ($from > 0) {
                $strSql = sprintf("SELECT * FROM pages P
                  INNER JOIN ci_school_configuration SC ON P.page_id = SC.school_id
                  WHERE page_category = %s ORDER BY page_title ASC LIMIT %s, %s", SCHOOL_CATEGORY_ID, secure($from, 'int'), secure($limit, 'int'));
            } else {
                $strSql = sprintf("SELECT * FROM pages P
                  INNER JOIN ci_school_configuration SC ON P.page_id = SC.school_id
                  WHERE page_category = %s ORDER BY page_title ASC", SCHOOL_CATEGORY_ID);
            }
        }

        $schools = array();
        $get_schools = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_schools->num_rows > 0) {
            while($school = $get_schools->fetch_assoc()) {
                $school['page_picture'] = get_picture($school['page_picture'], 'school');
                $schools[] = $school;
            }
        }
        return $schools;
    }

    /**
     * Lấy danh sách trường trong noga theo điều kiện truyền vào
     *
     * @param $school_status
     * @param int $city_id
     * @param string $district_slug
     * @param int $from
     * @param int $limit
     * @return array
     * @throws Exception
     */
    public function getAllSchoolConiuByClause($school_status, $city_id = 0, $district_slug = '', $from = 0, $limit = 0) {
        global $db;

        $whereClause = null;
        if ($city_id > 0) {
            $whereClauseCityId = sprintf(' AND P.city_id = %s', secure($city_id, 'int'));
        } else {
            $whereClauseCityId = " ";
        }

        if ($district_slug) {
            $whereClauseDistrict = sprintf(' AND SC.district_slug = %s', secure($district_slug));
        } else {
            $whereClauseDistrict = " ";
        }

        if($school_status != 0) {
            if ($from > 0) {
                $strSql = sprintf("SELECT * FROM pages P
                  INNER JOIN ci_school_configuration SC ON P.page_id = SC.school_id
                  WHERE page_category = %s AND SC.school_status = %s %s %s ORDER BY page_title ASC LIMIT %s, %s", SCHOOL_CATEGORY_ID, secure($school_status, 'int'),
                    $whereClauseCityId, $whereClauseDistrict, secure($from, 'int'), secure($limit, 'int'));
            } else {
                $strSql = sprintf("SELECT * FROM pages P
                  INNER JOIN ci_school_configuration SC ON P.page_id = SC.school_id
                  WHERE page_category = %s AND SC.school_status = %s %s %s ORDER BY page_title ASC", SCHOOL_CATEGORY_ID, secure($school_status, 'int'), $whereClauseCityId, $whereClauseDistrict);
            }
        } else {
            if ($from > 0) {
                $strSql = sprintf("SELECT * FROM pages P
                  INNER JOIN ci_school_configuration SC ON P.page_id = SC.school_id
                  WHERE page_category = %s %s %s ORDER BY page_title ASC LIMIT %s, %s", SCHOOL_CATEGORY_ID, $whereClauseCityId, $whereClauseDistrict, secure($from, 'int'), secure($limit, 'int'));
            } else {
                $strSql = sprintf("SELECT * FROM pages P
                  INNER JOIN ci_school_configuration SC ON P.page_id = SC.school_id
                  WHERE page_category = %s %s %s ORDER BY page_title ASC", SCHOOL_CATEGORY_ID, $whereClauseCityId, $whereClauseDistrict);
            }
        }

        $schools = array();
        $get_schools = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_schools->num_rows > 0) {
            while($school = $get_schools->fetch_assoc()) {
                $school['page_picture'] = get_picture($school['page_picture'], 'school');
                $schools[] = $school;
            }
        }
        return $schools;
    }

    /**
     * Thay đổi trạng thái của trường
     *
     * @param $schoolId
     * @param $status
     * @throws Exception
     */
    public function changeSchoolStatus($schoolId, $status){
        global $db;

        $strSql = sprintf("UPDATE ci_school_configuration SET school_status = %s WHERE school_id = %s", secure($status, 'int'), secure($schoolId, 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Thay đổi đường link hướng dẫn sử dụng camera
     *
     * @param $args
     * @throws Exception
     */
    public function updateSchoolConfigCamera($args) {
        global $db;

        $strSql = sprintf("UPDATE ci_school_configuration SET camera_guide = %s WHERE school_id = %s", secure($args['camera_guide']), secure($args['school_id'], 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Cập nhật school_step
     *
     * @param $schoolIds
     * @param string $step
     * @throws Exception
     */
    public function updateSchoolStep($schoolIds, $step) {
        global $db;

        $schoolIds = array_unique($schoolIds);
        $strCon = "";
        if(count($schoolIds) > 0) {
            $strCon = implode(",", $schoolIds);
        }

        $strSql = sprintf("UPDATE ci_school_configuration SET school_step = %s WHERE school_id IN (%s)", secure($step, 'int'), $strCon);

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Cập nhật chu kỳ viết sổ liên lạc
     *
     * @param $school_id
     * @param $report_interval
     * @throws Exception
     */
    public function updateSchoolReportInterval($school_id, $report_interval) {
        global $db;
        $strSql = sprintf('UPDATE ci_school_configuration SET report_cycle = %s WHERE school_id = %s',
            secure($report_interval), secure($school_id, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Update module school
     *
     * @param $schoolId
     * @param $args
     * @throws Exception
     */
    public function updateSchoolModules($schoolId, $args) {
        global $db, $moduleName;
        $strValues = '';
        foreach ($moduleName as $module => $value) {

            $strValues .= $module . "=" . $args[$module] . ",";
        }
        //ADD START MANHDD 1/6/2021
        // THêm phần chọn công thức tính điểm theo nước nào
        if(isset($args['points']) && $args['points']) {
            $strValues .=  "score_fomula='" . $args['score_fomula'] . "',";
        }
        //ADD END MANHDD 1/6/2021
        $strValues = trim($strValues, ', ');
        $sql = "UPDATE ci_school_configuration SET " . $strValues . " WHERE school_id = %s";
        $strSql = sprintf($sql, secure($schoolId, 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Lấy danh sách id các trường thuộc 1 vùng
     *
     * @param $city_id
     * @param $district_slug
     * @return array
     * @throws Exception
     */
    public function getSchoolIdsByRegion($city_id, $district_slug, $grade) {
        global $db;

        if(!$district_slug) {
            if(is_null($grade)) {
                $strSql = sprintf("SELECT P.page_id FROM pages P
                INNER JOIN ci_school_configuration SC ON SC.school_id = P.page_id AND SC.school_status = 1
                WHERE P.city_id = %s", secure($city_id, 'int'));
            } else {
                $strSql = sprintf("SELECT P.page_id FROM pages P
                INNER JOIN ci_school_configuration SC ON SC.school_id = P.page_id AND SC.school_status = 1
                WHERE P.city_id = %s AND SC.grade = %s", secure($city_id, 'int'), secure($grade));
            }
        } else {
            if(is_null($grade)) {
                $strSql = sprintf("SELECT P.page_id FROM pages P
                INNER JOIN ci_school_configuration SC ON SC.school_id = P.page_id
                WHERE P.city_id = %s AND SC.district_slug = %s AND SC.school_status = 1", secure($city_id, 'int'), secure($district_slug));
            } else {
                $strSql = sprintf("SELECT P.page_id FROM pages P
                INNER JOIN ci_school_configuration SC ON SC.school_id = P.page_id
                WHERE P.city_id = %s AND SC.district_slug = %s AND SC.school_status = 1 AND SC.grade = %s", secure($city_id, 'int'), secure($district_slug), secure($grade));
            }
        }

        $get_school_ids = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $schoolIds = array();
        if($get_school_ids->num_rows > 0) {
            while ($schoolId = $get_school_ids->fetch_assoc()['page_id']) {
                $schoolIds[] = $schoolId;
            }
        }

        return $schoolIds;
    }


    /**
     * Thêm các môn học mặc định cho từng khối (ci_class_level_subject)
     *
     * @param $schoolId
     * @param $subjects
     * @param $school_year
     * @throws Exception
     */
    public function createClassLevelSubjectDefault($schoolId, $subjects, $school_year) {
        global $db;

        $strValues = "";
        foreach ($subjects as $gov_class_level => $subject) {
            foreach ($subject as $id => $name) {
                //school_id, subject_id, gov_class_level, school_year
                $strValues .= "(". secure($schoolId, 'int') .",". secure($id, 'int') . "," . secure($gov_class_level, 'int') . "," . secure($school_year) ."),";
            }
        }
        $strValues = trim($strValues, ",");

        $strSql = "INSERT INTO ci_class_level_subject (school_id, subject_id, gov_class_level, school_year) VALUES ".$strValues;

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

    }
}
?>