<?php
/**
 * DAO -> Services
 * Thao tác với các bảng service
 * 
 * @package ConIu
 * @author QuanND
 */

class ServiceDAO {
    /**
     * __construct
     *
     */
    public function __construct() {
    }

    //////////////////////////////// SERVICE ////////////////////////
    /**
     * Lây ra danh sách các dịch vụ của một trường
     *
     * @param $schoolId
     * @param int $checkRegister
     * @return array
     * @throws Exception
     */
    public function getServices($schoolId, $checkRegister = 0) {
        global $db, $system;

        $strSql = null;
        if ($checkRegister) {
            $strSql = sprintf("SELECT S.* FROM ci_service S WHERE S.must_register = 1 AND S.school_id = %s AND S.type != %s AND S.status = %s AND S.parent_display = 1 ORDER BY S.service_name ASC, S.type
 ASC", secure($schoolId, 'int'), SERVICE_TYPE_COUNT_BASED, 1);
        } else {
            $strSql = sprintf("SELECT S.* FROM ci_service S WHERE S.school_id = %s ORDER BY S.service_name ASC, S.type ASC", secure($schoolId, 'int'));
        }
        $services = array();
        $get_services = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_services->num_rows > 0) {
            while($service = $get_services->fetch_assoc()) {
                if (isset($system['is_mobile']) && $system['is_mobile']) {
                    $service['status'] = 0;
                    $service['begin'] = '';
                    $service['end'] = '';
                }
                $services[] = $service;
            }
        }
        return $services;
    }

    /**
     * Lấy ra danh sách các dịch vụ còn sử dụng của một trường.
     *
     * @param $schoolId
     * @return array
     * @throws Exception
     */
    public function getActiveSchoolServices($schoolId) {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_service WHERE school_id = %s AND status = 1 ORDER BY type DESC, service_name ASC", secure($schoolId, 'int'));

        $services = array();
        $get_services = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_services->num_rows > 0) {
            while($service = $get_services->fetch_assoc()) {
                $services[] = $service;
            }
        }
        return $services;
    }

    /**
     * Lấy ra tất cả dịch vụ không còn sử dụng của một trường.
     *
     * @param $schoolId
     * @return array
     * @throws Exception
     */
    public function getInActiveSchoolServices($schoolId) {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_service WHERE school_id = %s AND status = %s ORDER BY type DESC, service_name ASC", secure($schoolId, 'int'), secure(STATUS_INACTIVE, 'int'));

        $services = array();
        $get_services = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_services->num_rows > 0) {
            while($service = $get_services->fetch_assoc()) {
                $checkUsed = $this->checkUsedService($service['service_id']);
                $service['can_delete'] = $checkUsed ? false : true;
                $services[] = $service;
            }
        }
        return $services;
    }

    /**
     * Kiểm tra 1 dịch vụ xem đã được sử dụng hay chưa
     *
     * @param $serviceId
     * @return boolean
     * @throws Exception
     */
    public function checkUsedService($serviceId) {
        global $db;

        $strSql = sprintf('SELECT SC.service_id as scServiceId FROM ci_service_child SC WHERE SC.service_id = %1$s
                           UNION 
                           SELECT SU.service_id as suServiceId FROM ci_service_usage SU WHERE SU.service_id = %1$s', secure($serviceId, 'int'));

        $get_services = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_services->num_rows > 0) {
            while($service = $get_services->fetch_assoc()) {
                if ($service['scServiceId'] || $service['suServiceId']) {
                    return true;
                } else {
                    return false;
                }
            }
        }
        return false;
    }

    /**
     * Lấy danh sách dịch vụ theo danh sách ID truyền vào.
     *
     * @param $schoolId
     * @param $serviceIds
     * @return array
     * @throws Exception
     */
    public function getServicesInList($schoolId, $serviceIds) {
        global $db;
        if (count($serviceIds) > 0) {
            $strServiceIds = implode(",", $serviceIds);
            $strSql = sprintf("SELECT * FROM ci_service WHERE school_id = %s AND service_id IN (%s) ORDER BY type DESC, service_name ASC", secure($schoolId, 'int'), $strServiceIds);

            $services = array();
            $get_services = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
            if ($get_services->num_rows > 0) {
                while ($service = $get_services->fetch_assoc()) {
                    $services[] = $service;
                }
            }
            return $services;
        }
        return array();
    }

    /**
     * Lấy tất cả dịch vụ của trường, bao gồm cả dịch vụ không còn sử dụng.
     *
     * @param $schoolId
     * @return array
     * @throws Exception
     */
    public function getAllSchoolServices($schoolId) {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_service WHERE school_id = %s ORDER BY type DESC, service_name ASC", secure($schoolId, 'int'));

        $services = array();
        $get_services = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_services->num_rows > 0) {
            while($service = $get_services->fetch_assoc()) {
                $services[] = $service;
            }
        }
        return $services;
    }

    /**
     * Lấy tất cả dịch vụ đang sử dụng của trường.
     *
     * @param $schoolId
     * @return array
     * @throws Exception
     */
    public function getAllSchoolServicesForAPI($schoolId) {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_service WHERE school_id = %s AND status = 1 ORDER BY is_food DESC, type DESC, service_name ASC", secure($schoolId, 'int'));

        $services = array();
        $get_services = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_services->num_rows > 0) {
            while($service = $get_services->fetch_assoc()) {
                $services[] = $service;
            }
        }
        return $services;
    }

    /**
     * Lấy ra danh sách các dịch vụ tính phí theo số lần sử dụng
     *
     * @param $schoolId
     * @return array
     * @throws Exception
     */
    public function getCountBasedServices($schoolId) {
        global $db;

        $strSql = sprintf("SELECT S.* FROM ci_service S WHERE S.type = %s AND S.status=1 AND S.parent_display = 1 AND S.school_id = %s ORDER BY S.service_name ASC", SERVICE_TYPE_COUNT_BASED, secure($schoolId, 'int'));

        $services = array();
        $get_services = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_services->num_rows > 0) {
            while($service = $get_services->fetch_assoc()) {
                $services[] = $service;
            }
        }
        return $services;
    }

    /**
     * Lấy ra danh sách các dịch vụ tính phí theo số lần sử dụng (for API)
     *
     * @param $schoolId
     * @return array
     * @throws Exception
     */
    public function getCountBasedServicesForAPI($schoolId) {
        global $db;

        $strSql = sprintf("SELECT service_id, service_name, fee FROM ci_service WHERE type = %s AND status= %s AND school_id = %s ORDER BY service_name ASC",
                            secure(SERVICE_TYPE_COUNT_BASED, 'int'), secure(STATUS_ACTIVE, 'int'), secure($schoolId, 'int'));

        $services = array();
        $get_services = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_services->num_rows > 0) {
            while($service = $get_services->fetch_assoc()) {
                $services[] = $service;
            }
        }
        return $services;
    }

    /**
     * Lấy ra danh sách dịch vụ (không phải count-based) mà trẻ đăng ký
     *
     * @param $schoolId
     * @param $childId
     * @return array
     * @throws Exception
     */
    public function getChildServiceNotCountBased($schoolId, $childId) {
        global $db;

        $strSql = sprintf("SELECT S.*, SC.begin FROM ci_service S INNER JOIN ci_service_child SC ON S.service_id = SC.service_id
                            AND S.school_id = %s AND S.status = 1 AND S.parent_display = 1 AND SC.child_id = %s AND S.type != %s AND SC.status = 1 AND parent_display = 1 ORDER BY S.service_name ASC",
                            secure($schoolId, 'int'), secure($childId, 'int'), SERVICE_TYPE_COUNT_BASED);

        $services = array();
        $get_services = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_services->num_rows > 0) {
            while($service = $get_services->fetch_assoc()) {
                $service['begin'] = toSysDate($service['begin']);
                $service['end'] = __("Now");
                $services[] = $service;
            }
        }
        return $services;
    }

    /**
     * Lấy tất cả danh sách dịch vụ (không phải count-based) dành cho trẻ
     *
     * @param $schoolId
     * @param $childId
     * @return array
     * @throws Exception
     */
    public function getAllChildServiceNotCountBased($schoolId, $childId) {
        global $db;

        $strSql = sprintf("SELECT S.*, SC.begin, SC.status as child_status FROM ci_service S INNER JOIN ci_service_child SC ON S.service_id = SC.service_id
                            AND S.school_id = %s AND SC.child_id = %s AND S.type != %s AND SC.status = 1 AND S.parent_display = 1 ORDER BY S.service_name ASC",
            secure($schoolId, 'int'), secure($childId, 'int'), SERVICE_TYPE_COUNT_BASED);

        $services = array();
        $get_services = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_services->num_rows > 0) {
            while($service = $get_services->fetch_assoc()) {
                $service['begin'] = toSysDate($service['begin']);
                $service['end'] = __("Now");
                $services[] = $service;
            }
        }
        return $services;
    }

    /**
     * Lấy ra danh sách trẻ sử dụng 1 một dịch vụ. Theo điều kiện đầu vào:
     *   - Nếu chọn lớp, thì lấy ra danh sách lớp & thông tin sử dụng dịch vụ
     *   - Nếu không chọn lớp, lấy ra danh sách trẻ của cả trường & thông tin sử dụng dịch vụ
     *
     * @param $schoolId
     * @param $serviceId
     * @param int $classId
     * @return array
     * @throws Exception
     */
    public function getServiceChild($schoolId, $serviceId, $classId = 0) {
        global $db;

        $curDate = date('Y-m-d');
        $strSql = null;
        if ($classId > 0) {
            //Nếu chọn lớp, thì lấy ra danh sách lớp & thông tin sử dụng dịch vụ
            $strSql = sprintf('SELECT C.child_id, C.child_name, C.birthday, CC.status AS child_status, SC.service_id, SC.begin, SC.end, SC.status FROM ci_child C
                            INNER JOIN ci_class_child CC ON C.child_id = CC.child_id AND CC.class_id = %1$s AND CC.begin_at <= %2$s AND (CC.end_at IS NULL OR CC.end_at >= %2$s)
                            LEFT JOIN ci_service_child SC ON C.child_id = SC.child_id AND SC.service_id = %3$s
                            ORDER BY C.name_for_sort ASC', secure($classId, 'int'), secure($curDate), secure($serviceId, 'int'));
        } else {
            //Nếu không chọn lớp, lấy ra danh sách trẻ của cả trường & thông tin sử dụng dịch vụ
            $strSql = sprintf('SELECT C.child_id, C.child_name, C.birthday, SC.service_id, SC.begin, SC.end, SC.status, SCC.class_id, SCC.status AS child_status, G.group_title FROM ci_child C
                            INNER JOIN ci_school_child SCC ON C.child_id = SCC.child_id AND SCC.school_id = %1$s AND SCC.begin_at <= %2$s AND (SCC.end_at IS NULL OR SCC.end_at >= %2$s)
                            INNER JOIN groups G ON SCC.class_id = G.group_id
                            LEFT JOIN ci_service_child SC ON C.child_id = SC.child_id AND SC.service_id = %3$s
                            ORDER BY SCC.class_id ASC, C.name_for_sort ASC', secure($schoolId, 'int'), secure($curDate), secure($serviceId, 'int'));
        }
        $results = array();
        $serviceChildren = array();
        $usageCount = 0; //Số lượng trẻ đã đang ký sử dụng dịch vụ
        $get_serviceChildren = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_serviceChildren->num_rows > 0) {
            while($serviceChild = $get_serviceChildren->fetch_assoc()) {
                $serviceChild['birthday'] = toSysDate($serviceChild['birthday']);
                if ($serviceChild['service_id'] > 0) {
                    if  ($serviceChild['status'] > 0) {
                        $usageCount++;
                        $serviceChild['end'] = __("Now");
                    } else {
                        $serviceChild['end'] = toSysDate($serviceChild['end']);
                    }
                    $serviceChild['begin'] = toSysDate($serviceChild['begin']);
                }
                $serviceChildren[] = $serviceChild;
            }
        }
        $results['children'] = $serviceChildren;
        $results['use_count'] = $usageCount;

        return $results;
    }

    /**
     * Lấy ra danh sách trẻ sử dụng 1 một dịch vụ. Theo điều kiện đầu vào:
     *   - Nếu chọn lớp, thì lấy ra danh sách lớp & thông tin sử dụng dịch vụ
     *   - Nếu không chọn lớp, lấy ra danh sách trẻ của cả trường & thông tin sử dụng dịch vụ
     *
     * @param $schoolId
     * @param $serviceId
     * @param int $classId
     * @return array
     * @throws Exception
     */
    public function getServiceChildForAPI($serviceId, $classId, $usingAt) {
        global $db;

        $curDate = date('Y-m-d');
        $usingAt = toDBDate($usingAt);
        $strSql = null;
        //Nếu chọn lớp, thì lấy ra danh sách lớp & thông tin sử dụng dịch vụ
        $strSql = sprintf('SELECT C.child_name, CC.status AS child_status, SC.status, SC.service_id FROM ci_child C
                        INNER JOIN ci_class_child CC ON C.child_id = CC.child_id AND CC.class_id = %1$s AND CC.begin_at <= %2$s AND (CC.end_at IS NULL OR CC.end_at >= %2$s)
                        INNER JOIN ci_service_child SC ON C.child_id = SC.child_id AND SC.service_id = %3$s AND (COALESCE(SC.end, \'\') = \'\' OR DATE(SC.end) >= %4$s OR SC.end = \'0000-00-00 00:00:00\') AND DATE(SC.begin) <= %4$s
                        ORDER BY C.name_for_sort ASC', secure($classId, 'int'), secure($curDate), secure($serviceId, 'int'), secure($usingAt));

        $results = array();
        $serviceChildren = array();
        $usageCount = 0; //Số lượng trẻ đã đang ký sử dụng dịch vụ
        $get_serviceChildren = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_serviceChildren->num_rows > 0) {
            while($serviceChild = $get_serviceChildren->fetch_assoc()) {
                if ($serviceChild['service_id'] > 0) {
                    if  ($serviceChild['status'] > 0) {
                        $usageCount++;
                    }
                }
                $serviceChildren[] = $serviceChild;
            }
        }
        $results['children'] = $serviceChildren;
        $results['use_count'] = $usageCount;

        return $results;
    }

    /**
     * Lấy ra danh sách trẻ đăng ký một dịch vụ theo THÁNG, ĐIỂM DANH để hiện thị lên màn hình GHI GIẢM TRỪ.
     *
     * @param $classId
     * @param $serviceId
     * @param $deductionDate
     * @return array
     * @throws Exception
     */
    public function getServiceChild4Deduction($classId, $serviceId, $deductionDate) {
        global $db;
        $deductionDate = toDBDate($deductionDate);

        //Nếu chọn lớp, thì lấy ra danh sách lớp & thông tin sử dụng dịch vụ
        $strSql = sprintf('SELECT C.child_id, C.child_name, C.birthday, SC.service_id, SC.begin, SC.end, SC.status, SD.deduction_date FROM ci_service_child SC
                        INNER JOIN ci_class_child CC ON SC.child_id = CC.child_id AND CC.class_id = %1$s AND SC.service_id = %2$s AND SC.begin <= %3$s AND
                        ((SC.status = %4$s) OR (SC.status != %4$s AND SC.end >= %3$s))
                        INNER JOIN ci_child C ON C.child_id = SC.child_id
                        LEFT JOIN ci_service_deduction SD ON SD.child_id = SC.child_id AND SD.service_id = SC.service_id AND SD.deduction_date = %3$s
                        ORDER BY C.name_for_sort ASC',
                    secure($classId, 'int'), secure($serviceId, 'int'), secure($deductionDate), secure(STATUS_ACTIVE, 'int'));

        $results = array();
        $get_serviceChildren = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        while($serviceChild = $get_serviceChildren->fetch_assoc()) {
            $serviceChild['birthday'] = toSysDate($serviceChild['birthday']);
            $serviceChild['begin'] = toSysDate($serviceChild['begin']);
            $serviceChild['deduction_date'] = toSysDate($serviceChild['deduction_date']);
            if  ($serviceChild['status'] > 0) {
                $serviceChild['end'] = __("Now");
            } else {
                $serviceChild['end'] = toSysDate($serviceChild['end']);
            }
            $results[] = $serviceChild;
        }

        return $results;
    }

    /**
     * Lấy ra thông tin sử dụng MỘT dịch vụ theo THÁNG hoặc theo ĐIỂM danh trong một khoảng.
     *
     * @param $schoolId
     * @param $serviceId
     * @param $begin
     * @param $end
     * @param int $classId
     * @param int $childId
     * @return array
     * @throws Exception
     */
    public function getNotCountBasedServiceChild($schoolId, $serviceId, $begin, $end, $classId = 0, $childId = 0) {
        global $db;
        $begin = toDBDate($begin);
        $end = toDBDate($end);

        $strSql = null;
        if ($childId > 0) {
            // Nếu chọn trẻ
            $strSql = sprintf("SELECT C.child_id, C.child_code, C.child_name, SC.service_id, SC.begin, SC.end, SC.status FROM ci_child C
                            INNER JOIN ci_school_child SCC ON C.child_id = SCC.child_id AND SCC.school_id = %s
                            INNER JOIN ci_service_child SC ON C.child_id = SC.child_id AND SC.service_id = %s AND
                            (COALESCE(SC.end, '') = '' OR DATE(SC.end) >= %s OR SC.end = '0000-00-00 00:00:00') AND DATE(SC.begin) <= %s
                            WHERE C.child_id = %s", secure($schoolId, 'int'), secure($serviceId, 'int'), secure($begin), secure($end), secure($childId, 'int'));
        } elseif ($classId > 0) {
            //Nếu chọn lớp, thì lấy ra danh sách lớp & thông tin sử dụng dịch vụ
            $strSql = sprintf("SELECT C.child_id, C.child_code, C.child_name, SC.service_id, SC.begin, SC.end, SC.status FROM ci_child C
                            INNER JOIN ci_class_child CC ON C.child_id = CC.child_id AND CC.class_id = %s
                            INNER JOIN ci_service_child SC ON C.child_id = SC.child_id AND SC.service_id = %s AND
                            (COALESCE(SC.end, '') = '' OR DATE(SC.end) >= %s OR SC.end = '0000-00-00 00:00:00') AND DATE(SC.begin) <= %s
                            ORDER BY C.name_for_sort ASC", secure($classId, 'int'), secure($serviceId, 'int'), secure($begin), secure($end));
        } else {
            //Nếu không chọn lớp, lấy ra danh sách trẻ của cả trường & thông tin sử dụng dịch vụ
            $strSql = sprintf("SELECT C.child_id, C.child_code, C.child_name, SC.service_id, SC.begin, SC.end, SC.status, SCC.class_id, G.group_title FROM ci_child C
                            INNER JOIN ci_school_child SCC ON C.child_id = SCC.child_id AND SCC.school_id = %s
                            INNER JOIN groups G ON SCC.class_id = G.group_id
                            INNER JOIN ci_service_child SC ON C.child_id = SC.child_id AND SC.service_id = %s AND
                            (COALESCE(SC.end, '') = '' OR DATE(SC.end) >= %s OR SC.end = '0000-00-00 00:00:00') AND DATE(SC.begin) <= %s
                            ORDER BY SCC.class_id ASC, C.name_for_sort ASC", secure($schoolId, 'int'), secure($serviceId, 'int'), secure($begin), secure($end));
        }
        $results = array();
        $get_services = $db->query($strSql) or _error('error', $strSql);
        if($get_services->num_rows > 0) {
            while($service = $get_services->fetch_assoc()) {
                if ($service['service_id'] > 0) {
                    if  ($service['status'] > 0) {
                        $service['end'] = __("Now");
                    } else {
                        $service['end'] = toSysDate($service['end']);
                    }
                    $service['begin'] = toSysDate($service['begin']);
                }
                $results[] = $service;
            }
        }

        return $results;
    }

    /**
     * Lấy ra tổng kết số lượng sử dụng dịch vụ của trường
     *
     * @param $schoolId
     * @param $begin
     * @param $end
     * @return array
     * @throws Exception
     */
    public function getServiceUsageSummarySchool($schoolId, $begin, $end) {
        global $db;
        $begin = toDBDate($begin);
        $end = toDBDate($end);

        $strSql = sprintf('SELECT SU.service_id, S.type, S.service_name, S.fee, S.is_food, count(SU.child_id) AS cnt FROM ci_service_usage SU
                    INNER JOIN ci_service S ON SU.service_id = S.service_id AND S.school_id = %1$s
                    WHERE SU.using_at >= %2$s AND SU.using_at <= %3$s
                    GROUP BY SU.service_id
                    UNION
                    SELECT SC.service_id, S.type, S.service_name, S.fee, S.is_food, count(SC.child_id) AS cnt FROM ci_service_child SC
                    INNER JOIN ci_service S ON SC.service_id = S.service_id AND S.school_id = %1$s
                    WHERE (SC.end is null OR SC.end = \'0000-00-00 00:00:00\' OR SC.end >= %2$s) AND SC.begin <= %3$s
                    GROUP BY SC.service_id
                    ORDER BY type DESC, service_name ASC', secure($schoolId, 'int'), secure($begin), secure($end));

        $results = array();
        $get_services = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_services->num_rows > 0) {
            while($service = $get_services->fetch_assoc()) {
                $results[] = $service;
            }
        }

        return $results;
    }

    /**
     * Lấy ra tổng kết số lượng sử dụng dịch vụ của trường tổng hợp theo tháng màn hình hiệu trưởng
     *
     * @param $schoolId
     * @param $begin
     * @param $end
     * @return array
     * @throws Exception
     */
    public function getServiceUsageSummarySchoolForPrincipal($schoolId, $begin, $end) {
        global $db;
        $begin = toDBDate($begin);
        $end = toDBDate($end);

        $strSql = sprintf('SELECT SU.service_id, S.type, S.service_name, S.fee, S.is_food, count(SU.child_id) AS cnt FROM ci_service_usage SU
                    INNER JOIN ci_service S ON SU.service_id = S.service_id AND S.school_id = %1$s
                    WHERE SU.using_at >= %2$s AND SU.using_at <= %3$s AND S.status = 1
                    GROUP BY SU.service_id
                    UNION
                    SELECT SC.service_id, S.type, S.service_name, S.fee, S.is_food, count(SC.child_id) AS cnt FROM ci_service_child SC
                    INNER JOIN ci_service S ON SC.service_id = S.service_id AND S.school_id = %1$s
                    INNER JOIN ci_child C ON C.child_id = SC.child_id
                    WHERE (SC.end is null OR SC.end = \'0000-00-00 00:00:00\' OR SC.end >= %2$s) AND SC.begin <= %3$s AND S.status = 1
                    GROUP BY SC.service_id
                    ORDER BY type DESC, service_name ASC', secure($schoolId, 'int'), secure($begin), secure($end));

        $results = array();
        $get_services = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_services->num_rows > 0) {
            while($service = $get_services->fetch_assoc()) {
                $service['money'] = $service['fee'] * $service['cnt'];
                $results[] = $service;
            }
        }

        return $results;
    }

    /**
     * Lấy ra tổng kết số lượng sử dụng dịch vụ của trường
     *
     * @param $schoolId
     * @param $begin
     * @param $end
     * @return array
     * @throws Exception
     */
    public function getServiceUsageSummarySchoolForAPI($schoolId, $begin, $end) {
        global $db;
        $begin = toDBDate($begin);
        $end = toDBDate($end);
        if($begin !== $end) {
            throw new Exception(__("Started date and ended date must be the same"));
        }
        $strSql = sprintf('SELECT SU.service_id, S.type, S.service_name, S.fee, S.is_food, count(SU.child_id) AS cnt FROM ci_service_usage SU
                    INNER JOIN ci_service S ON SU.service_id = S.service_id AND S.school_id = %1$s AND S.status = 1
                    WHERE SU.using_at >= %2$s AND SU.using_at <= %3$s
                    GROUP BY SU.service_id
                    UNION
                    SELECT SC.service_id, S.type, S.service_name, S.fee, S.is_food, count(SC.child_id) AS cnt FROM ci_service_child SC
                    INNER JOIN ci_service S ON SC.service_id = S.service_id AND S.school_id = %1$s AND S.status = 1
                    INNER JOIN ci_attendance_detail AD ON AD.child_id = SC.child_id AND (AD.status = 1 OR AD.status=2 OR AD.status = 3)
                    INNER JOIN ci_attendance A ON A.attendance_id = AD.attendance_id
                    WHERE (SC.end is null OR SC.end = \'0000-00-00 00:00:00\' OR DATE(SC.end) >= %2$s) AND DATE(SC.begin) <= %3$s AND DATE(A.attendance_date) = %2$s
                    GROUP BY SC.service_id
                    ORDER BY is_food DESC, type DESC, service_name ASC', secure($schoolId, 'int'), secure($begin), secure($end));

        $results = array();
        $get_services = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_services->num_rows > 0) {
            while($service = $get_services->fetch_assoc()) {
                $results[] = $service;
            }
        }

        return $results;
    }

    /**
     * Lấy ra tổng kết số lượng sử dụng dịch vụ của một LỚP.
     *
     * @param $schoolId
     * @param $classId
     * @param $begin
     * @param $end
     * @return array
     * @throws Exception
     */
    public function getServiceUsageSummaryClass($schoolId, $classId, $begin, $end) {
        global $db;
        $begin = toDBDate($begin);
        $end = toDBDate($end);

        $strSql = sprintf('SELECT SU.service_id, S.type, S.service_name, count(SU.child_id) AS cnt FROM ci_service_usage SU
                            INNER JOIN ci_service S ON SU.service_id = S.service_id AND S.school_id = %1$s
                            INNER JOIN ci_class_child CC ON CC.child_id = SU.child_id AND CC.class_id = %2$s
                            WHERE SU.using_at >= %3$s AND SU.using_at <= %4$s
                            GROUP BY SU.service_id
                            UNION
                            SELECT SC.service_id, S.type, S.service_name, count(SC.child_id) AS cnt FROM ci_service_child SC
                            INNER JOIN ci_service S ON SC.service_id = S.service_id AND S.school_id = %1$s
                            INNER JOIN ci_class_child CC ON CC.child_id = SC.child_id AND CC.class_id = %2$s
                            WHERE (COALESCE(SC.end, "") = "" OR DATE(SC.end) >= %3$s OR SC.end = \'0000-00-00 00:00:00\') AND DATE(SC.begin) <= %4$s
                            GROUP BY SC.service_id
                            ORDER BY type DESC, service_name ASC', secure($schoolId, 'int'), secure($classId, 'int'), secure($begin), secure($end));

        $results = array();
        $get_services = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_services->num_rows > 0) {
            while($service = $get_services->fetch_assoc()) {
                $results[] = $service;
            }
        }

        return $results;
    }


    /**
     * Lấy ra danh sách dịch vụ theo THÁNG và ĐIỂM DANH của một trẻ đăng ký sử dụng.
     * Lấy cả những dịch vụ chưa đăng ký phục vụ màn hình đăng ký ở trẻ.
     *
     * @param $schoolId
     * @param $childId
     * @return array
     * @throws Exception
     */
    public function getNotCountBasedServiceChild4Register($schoolId, $childId) {
        global $db;

        $strSql = sprintf("SELECT S.service_id, S.service_name, S.fee, SC.status, SC.service_child_id, SC.begin, SC.end FROM ci_service S
                        LEFT JOIN ci_service_child SC ON S.service_id = SC.service_id AND SC.status = %s AND SC.child_id = %s
                        WHERE S.school_id = %s AND S.status = %s AND S.type != %s AND S.parent_display = 1", STATUS_ACTIVE, secure($childId, 'int'), secure($schoolId, 'int'), STATUS_ACTIVE, SERVICE_TYPE_COUNT_BASED);

        $results = array();
        $get_services = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_services->num_rows > 0) {
            while($service = $get_services->fetch_assoc()) {
                if ($service['service_id'] > 0) {
                    if  ($service['status'] > 0) {
                        $service['end'] = __("Now");
                    } else {
                        $service['end'] = toSysDate($service['end']);
                    }
                    $service['begin'] = toSysDate($service['begin']);
                }
                $results[] = $service;
            }
        }

        return $results;
    }

    /**
     * Lấy thông tin sử dụng TẤT CẢ dịch vụ tính phí theo THÁNG và theo ĐIỂM DANH của một trẻ.
     *
     * @param $schoolId
     * @param $childId
     * @param $begin
     * @param $end
     * @return array
     * @throws Exception
     */
    public function getAllNotCountBasedServiceAChild($schoolId, $childId, $begin, $end) {
        global $db;
        $begin = toDBDate($begin);
        $end = toDBDate($end);

        // Nếu chọn trẻ
        $strSql = sprintf("SELECT SC.*, S.service_name, SCC.status AS child_status FROM ci_service_child SC
                        INNER JOIN ci_service S ON SC.service_id = S.service_id AND S.school_id = %s
                        INNER JOIN ci_school_child SCC ON SCC.child_id = SC.child_id
                        WHERE SC.child_id = %s AND (COALESCE(SC.end, '') = '' OR SC.end >= %s) AND DATE(SC.begin) <= %s
                        ORDER BY SC.begin DESC",
                        secure($schoolId, 'int'), secure($childId, 'int'), secure($begin), secure($end));

        $results = array();
        $get_services = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_services->num_rows > 0) {
            while($service = $get_services->fetch_assoc()) {
                if  ($service['status'] > 0) {
                    $service['end'] = __("Now");
                } else {
                    $service['end'] = toSysDate($service['end']);
                }
                $service['begin'] = toSysDate($service['begin']);

                $results[] = $service;
            }
        }

        return $results;
    }

    /**
     * Lấy ra danh sách trẻ sử dụng 1 dịch vụ tại 1 ngày.
     *
     * @param $schoolId
     * @param $serviceId
     * @param $usingAt
     * @param int $classId
     * @return array
     * @throws Exception
     */
    public function getServiceChild4Record($schoolId, $serviceId, $usingAt, $classId = 0) {
        global $db;
        $usingAt = toDBDate($usingAt);

        $strSql = null;
        if ($classId > 0) {
            $strSql = sprintf('SELECT C.child_id, C.child_code, C.child_name, C.birthday, CC.status AS child_status, SU.recorded_at, SU.recorded_user_id, U.user_fullname, U.user_name, U.user_gender, U.user_picture
                            FROM ci_child C INNER JOIN ci_class_child CC ON C.child_id = CC.child_id AND CC.class_id = %1$s AND CC.begin_at <= %2$s AND (CC.end_at IS NULL OR CC.end_at >= %2$s)
                            LEFT JOIN ci_service_usage SU ON C.child_id = SU.child_id AND SU.service_id = %3$s AND SU.using_at = %2$s
                            LEFT JOIN users U ON SU.recorded_user_id = U.user_id
                            ORDER BY C.name_for_sort ASC', secure($classId, 'int'), secure($usingAt), secure($serviceId, 'int'));
        } else {
            $strSql = sprintf('SELECT C.child_id, C.child_code, C.child_name, C.birthday, SCC.status AS child_status, SU.recorded_at, SU.recorded_user_id, U.user_fullname, U.user_name, U.user_gender, U.user_picture
                            FROM ci_child C INNER JOIN ci_school_child SCC ON C.child_id = SCC.child_id AND SCC.school_id = %1$s AND SCC.begin_at <= %2$s AND (SCC.end_at IS NULL OR SCC.end_at >= %2$s)
                            LEFT JOIN ci_service_usage SU ON C.child_id = SU.child_id AND SU.service_id = %3$s AND SU.using_at = %$2s
                            LEFT JOIN users U ON SU.recorded_user_id = U.user_id
                            ORDER BY C.name_for_sort ASC', secure($schoolId, 'int'), secure($usingAt), secure($serviceId, 'int'));
        }

        $results = array();
        $serviceChildren = array();
        $usageCount = 0; //Số lượng trẻ đã đang ký sử dụng dịch vụ
        $get_serviceChildren = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_serviceChildren->num_rows > 0) {
            while($serviceChild = $get_serviceChildren->fetch_assoc()) {
                $serviceChild['birthday'] = toSysDate($serviceChild['birthday']);
                if ($serviceChild['recorded_user_id'] > 0) {
                    $usageCount++;
                    $serviceChild['recorded_at'] = toSysDatetime($serviceChild['recorded_at']);
                    $serviceChild['user_picture'] = get_picture($serviceChild['user_picture'], $serviceChild['user_gender']);
                }
                $serviceChildren[] = $serviceChild;
            }
        }
        $results['children'] = $serviceChildren;
        $results['usage_count'] = $usageCount;

        return $results;
    }


    /**
     * Lấy ra danh sách trẻ sử dụng dịch vụ THEO SỐ LẦN của cả trường. Phân theo từng lớp.
     *
     * @param $schoolId
     * @param $serviceId
     * @param $usingAt
     * @param int $classId
     * @return array
     * @throws Exception
     */
    public function getSchoolChildService4Record($schoolId, $serviceId, $usingAt, $classId = 0) {
        global $db;
        $usingAt = toDBDate($usingAt);

        $strSql = null;
        if ($classId > 0) {
            $strSql = sprintf('SELECT C.child_id, C.child_code, C.child_name, C.birthday, SCC.status AS child_status, SU.recorded_at, SU.recorded_user_id, U.user_fullname, U.user_name, U.user_gender, U.user_picture
                            FROM ci_child C INNER JOIN ci_school_child SCC ON C.child_id = SCC.child_id AND SCC.class_id = %1$s AND SCC.begin_at <= %2$s AND (SCC.end_at IS NULL OR SCC.end_at >= %2$s)
                            LEFT JOIN ci_service_usage SU ON C.child_id = SU.child_id AND SU.service_id = %3$s AND SU.using_at = %2$s
                            LEFT JOIN users U ON SU.recorded_user_id = U.user_id
                            ORDER BY C.name_for_sort ASC', secure($classId, 'int'), secure($usingAt), secure($serviceId, 'int'));
        } else {
            $strSql = sprintf('SELECT C.child_id, C.child_code, C.child_name, C.birthday, SCC.class_id, SCC.status AS child_status,G.group_title, SU.recorded_at, SU.recorded_user_id, U.user_fullname,
                            U.user_name, U.user_gender, U.user_picture FROM ci_child C
                            INNER JOIN ci_school_child SCC ON C.child_id = SCC.child_id AND SCC.school_id = %1$s AND SCC.begin_at <= %2$s AND (SCC.end_at IS NULL OR SCC.end_at >= %2$s)
                            INNER JOIN groups G ON SCC.class_id = G.group_id
                            LEFT JOIN ci_service_usage SU ON C.child_id = SU.child_id AND SU.service_id = %3$s AND SU.using_at = %2$s
                            LEFT JOIN users U ON SU.recorded_user_id = U.user_id
                            ORDER BY G.class_level_id ASC, SCC.class_Id, C.name_for_sort ASC', secure($schoolId, 'int'), secure($usingAt), secure($serviceId, 'int'));
        }

        $results = array();
        $serviceChildren = array();
        $usageCount = 0; //Số lượng trẻ đã đang ký sử dụng dịch vụ
        $get_serviceChildren = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_serviceChildren->num_rows > 0) {
            while($serviceChild = $get_serviceChildren->fetch_assoc()) {
                $serviceChild['birthday'] = toSysDate($serviceChild['birthday']);
                if ($serviceChild['recorded_user_id'] > 0) {
                    $usageCount++;
                    $serviceChild['recorded_at'] = toSysDatetime($serviceChild['recorded_at']);
                    $serviceChild['user_picture'] = get_picture($serviceChild['user_picture'], $serviceChild['user_gender']);
                }
                $serviceChildren[] = $serviceChild;
            }
        }
        $results['children'] = $serviceChildren;
        $results['usage_count'] = $usageCount;

        return $results;
    }

    /**
     * Lấy ra danh sách trẻ sử dụng dịch vụ THEO SỐ LẦN của cả lớp
     *
     * @param $schoolId
     * @param $serviceId
     * @param $usingAt
     * @param int $classId
     * @return array
     * @throws Exception
     */
    public function getSchoolChildService4RecordForAPI($serviceId, $usingAt, $classId) {
        global $db;
        $usingAt = toDBDate($usingAt);

        $strSql = null;
        $strSql = sprintf('SELECT C.child_name, CC.status AS child_status, SU.recorded_user_id
                        FROM ci_child C INNER JOIN ci_class_child CC ON C.child_id = CC.child_id AND CC.class_id = %1$s AND CC.begin_at <= %2$s AND (CC.end_at IS NULL OR CC.end_at >= %2$s)
                        INNER JOIN ci_service_usage SU ON C.child_id = SU.child_id AND SU.service_id = %3$s AND SU.using_at = %2$s
                        INNER JOIN users U ON SU.recorded_user_id = U.user_id
                        ORDER BY C.name_for_sort ASC', secure($classId, 'int'), secure($usingAt), secure($serviceId, 'int'));

        $results = array();
        $serviceChildren = array();
        $usageCount = 0; //Số lượng trẻ đã đang ký sử dụng dịch vụ
        $get_serviceChildren = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_serviceChildren->num_rows > 0) {
            while($serviceChild = $get_serviceChildren->fetch_assoc()) {
                if ($serviceChild['recorded_user_id'] > 0) {
                    $usageCount++;
                }
                $serviceChildren[] = $serviceChild;
            }
        }
        $results['children'] = $serviceChildren;
        $results['usage_count'] = $usageCount;

        return $results;
    }

    /**
     * Lấy ra danh sách trẻ sử dụng 1 dịch vụ tại 1 ngày.
     *
     * @param $schoolId
     * @param $serviceId
     * @param $usingAt
     * @param int $classId
     * @return array
     * @throws Exception
     */
    public function getChildService4Record($schoolId, $childId, $usingAt) {
        global $db;
        $usingAt = toDBDate($usingAt);

        $strSql = sprintf("SELECT S.*, SU.using_at, SU.recorded_at, SU.recorded_user_id, U.user_fullname, U.user_name, U.user_gender, U.user_picture
                            FROM ci_service S LEFT JOIN ci_service_usage SU ON S.service_id = SU.service_id AND SU
                            .child_id = %s AND SU.using_at = %s
                            LEFT JOIN users U ON SU.recorded_user_id = U.user_id WHERE S.type = %s AND S.school_id = %s AND S.status = 1 AND S.parent_display = 1
                            ORDER BY S.service_name ASC", secure($childId, 'int'), secure($usingAt), SERVICE_TYPE_COUNT_BASED, secure($schoolId, 'int'));

        $results = array();
        $childrenService = array();
        $usageCount = 0; //Số lượng dịch vụ đã đăng ký sử dụng
        $get_childrenService = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_childrenService->num_rows > 0) {
            while($serviceChild = $get_childrenService->fetch_assoc()) {
                $serviceChild['status'] = 0;
                if ($serviceChild['recorded_user_id'] > 0) {
                    $serviceChild['status'] = 1;
                    $usageCount++;
                    $serviceChild['recorded_at'] = toSysDatetime($serviceChild['recorded_at']);
                }
                $childrenService[] = $serviceChild;
            }
        }
        $results['service'] = $childrenService;
        $results['usage_count'] = $usageCount;

        return $results;
    }

    /**
     * Lấy ra danh sách số lần sử dụng dịch vụ của một trẻ đối với 1 dịch vụ trong khoảng từ ngày-đến ngày
     *
     * @param $childId
     * @param $serviceId
     * @param $begin
     * @param $end
     * @return array
     * @throws Exception
     */
    public function getServiceHistory($childId, $serviceId, $begin, $end) {
        global $db;
        $begin = toDBDate($begin);
        $end = toDBDate($end);

        $strSql = sprintf("SELECT SU.*, U.user_fullname, U.user_name, U.user_gender, U.user_picture FROM ci_service_usage SU
                            INNER JOIN users U ON SU.recorded_user_id = U.user_id AND SU.service_id = %s AND SU.child_id = %s
                            AND SU.using_at >= %s AND SU.using_at <= %s
                            ORDER BY SU.using_at DESC", secure($serviceId, 'int'), secure($childId, 'int'), secure($begin), secure($end));

        $serviceHistory = array();
        $get_serviceHistory = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_serviceHistory->num_rows > 0) {
            while($serviceHis = $get_serviceHistory->fetch_assoc()) {
                $serviceHis['user_picture'] = get_picture($serviceHis['user_picture'], $serviceHis['user_gender']);
                $serviceHis['recorded_at'] = toSysDatetime($serviceHis['recorded_at']);
                $serviceHis['using_at'] = toSysDate($serviceHis['using_at']);

                $serviceHistory[] = $serviceHis;
            }
        }

        return $serviceHistory;
    }

    /**
     * Lấy ra danh sách sử dụng dịch vụ THEO LẦN trong trường. Phục vụ hàm search.
     *
     * @param $schoolId
     * @param $classId
     * @param $childId
     * @param $serviceId
     * @param $begin
     * @param $end
     * @return array
     * @throws Exception
     */
    public function getServiceHistorySchool($schoolId, $classId, $childId, $serviceId, $begin, $end) {
        global $db;
        $begin = toDBDate($begin);
        $end = toDBDate($end);

        if ($childId > 0) {
            $strSql = sprintf("SELECT SU.*, U.user_fullname FROM ci_service_usage SU
                            INNER JOIN users U ON SU.recorded_user_id = U.user_id AND SU.service_id = %s AND SU.child_id = %s
                            AND SU.using_at >= %s AND SU.using_at <= %s
                            ORDER BY SU.using_at DESC", secure($serviceId, 'int'), secure($childId, 'int'), secure($begin), secure($end));
        } elseif ($classId > 0) {
            $strSql = sprintf("SELECT SU.*, C.child_name, U.user_fullname FROM ci_service_usage SU
                            INNER JOIN users U ON SU.recorded_user_id = U.user_id AND SU.service_id = %s
                            INNER JOIN ci_class_child CC ON SU.child_id = CC.child_id AND CC.class_id = %s
                            INNER JOIN ci_child C ON SU.child_id = C.child_id
                            AND SU.using_at >= %s AND SU.using_at <= %s
                            ORDER BY SU.using_at DESC, C.name_for_sort ASC", secure($serviceId, 'int'), secure($classId, 'int'), secure($begin), secure($end));
        } else {
            $strSql = sprintf("SELECT SU.*, C.child_name, U.user_fullname, SC.class_id, G.group_title FROM ci_service_usage SU
                            INNER JOIN users U ON SU.recorded_user_id = U.user_id AND SU.service_id = %s
                            INNER JOIN ci_school_child SC ON SU.child_id = SC.child_id AND SC.school_id = %s
                            INNER JOIN groups G ON SC.class_id = G.group_id
                            INNER JOIN ci_child C ON SU.child_id = C.child_id
                            AND SU.using_at >= %s AND SU.using_at <= %s
                            ORDER BY G.class_level_id ASC, G.group_id ASC, SU.using_at DESC, C.name_for_sort ASC", secure($serviceId, 'int'), secure($schoolId, 'int'), secure($begin), secure($end));
        }

        $serviceHistory = array();
        $get_serviceHistory = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_serviceHistory->num_rows > 0) {
            while($serviceHis = $get_serviceHistory->fetch_assoc()) {
                $serviceHis['recorded_at'] = toSysDatetime($serviceHis['recorded_at']);
                $serviceHis['using_at'] = toSysDate($serviceHis['using_at']);

                $serviceHistory[] = $serviceHis;
            }
        }

        return $serviceHistory;
    }

    /**
     * Lấy ra danh sách sử dụng dịch vụ theo SỐ LẨN trong khoảng thời gian của MỘT LỚP
     *
     * @param $classId
     * @param $serviceId
     * @param $begin
     * @param $end
     * @return array
     * @throws Exception
     */
    public function getCountBasedServiceAClass($classId, $serviceId, $begin, $end) {
        global $db;
        $begin = toDBDate($begin);
        $end = toDBDate($end);

        $strSql = sprintf("SELECT SU.*, C.child_name, U.user_fullname, CC.status AS child_status FROM ci_service_usage SU
                        INNER JOIN users U ON SU.recorded_user_id = U.user_id AND SU.service_id = %s
                        INNER JOIN ci_class_child CC ON SU.child_id = CC.child_id AND CC.class_id = %s
                        INNER JOIN ci_child C ON SU.child_id = C.child_id
                        AND SU.using_at >= %s AND SU.using_at <= %s
                        ORDER BY SU.using_at DESC, C.name_for_sort ASC", secure($serviceId, 'int'), secure($classId, 'int'), secure($begin), secure($end));

        $serviceHistory = array();
        $get_serviceHistory = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_serviceHistory->num_rows > 0) {
            while($serviceHis = $get_serviceHistory->fetch_assoc()) {
                $serviceHis['recorded_at'] = toSysDatetime($serviceHis['recorded_at']);
                $serviceHis['using_at'] = toSysDate($serviceHis['using_at']);

                $serviceHistory[] = $serviceHis;
            }
        }

        return $serviceHistory;
    }

    /**
     * Lấy ra danh sách sử dụng dịch vụ tính theo số lần sử dụng của một trẻ.
     *
     * @param $schoolId
     * @param $childId
     * @param $begin
     * @param $end
     * @param $serviceId
     * @return array
'     * @throws Exception
     */
    public function getCountBasedServiceAChild($schoolId, $childId, $begin, $end, $serviceId = 0) {
        global $db;
        $begin = toDBDate($begin);
        $end = toDBDate($end);

        if ($serviceId == 0) {
            $strSql = sprintf("SELECT SU.*, U.user_fullname, S.service_name FROM ci_service_usage SU
                        INNER JOIN ci_service S ON SU.service_id = S.service_id AND S.school_id = %s
                        INNER JOIN users U ON SU.recorded_user_id = U.user_id
                        WHERE SU.child_id = %s AND SU.using_at >= %s AND SU.using_at <= %s
                        ORDER BY S.service_id ASC, SU.using_at DESC", secure($schoolId, 'int'), secure($childId, 'int'), secure($begin), secure($end));
        } else {
            $strSql = sprintf("SELECT SU.*, U.user_fullname, S.service_name FROM ci_service_usage SU
                        INNER JOIN ci_service S ON SU.service_id = S.service_id AND S.school_id = %s AND S.service_id = %s
                        INNER JOIN users U ON SU.recorded_user_id = U.user_id
                        WHERE SU.child_id = %s AND SU.using_at >= %s AND SU.using_at <= %s
                        ORDER BY S.service_id ASC, SU.using_at DESC", secure($schoolId, 'int'), secure($serviceId, 'int'), secure($childId, 'int'), secure($begin), secure($end));
        }

        $serviceHistory = array();
        $get_serviceHistory = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_serviceHistory->num_rows > 0) {
            while($serviceHis = $get_serviceHistory->fetch_assoc()) {
                $serviceHis['recorded_at'] = toSysDatetime($serviceHis['recorded_at']);
                $serviceHis['using_at'] = toSysDate($serviceHis['using_at']);

                $serviceHistory[] = $serviceHis;
            }
        }

        return $serviceHistory;
    }

    /**
     * Lấy ra danh sách sử dụng dịch vụ tính theo số lần sử dụng của một trẻ trong một ngày.
     *
     * @param $schoolId
     * @param $childId
     * @param $day
     * @return array
     * @throws Exception
     */
    public function getCountBasedServiceAChildOfDay($schoolId, $childId, $day) {
        global $db;
        $day = toDBDate($day);

        $strSql = sprintf("SELECT SU.service_id FROM ci_service_usage SU
                        INNER JOIN ci_service S ON SU.service_id = S.service_id AND S.school_id = %s
                        WHERE SU.child_id = %s AND SU.using_at = %s
                        ORDER BY S.service_id ASC, SU.using_at DESC", secure($schoolId, 'int'), secure($childId, 'int'), secure($day));

        $serviceHistory = array();
        $get_serviceHistory = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_serviceHistory->num_rows > 0) {
            while($serviceHis = $get_serviceHistory->fetch_assoc()['service_id']) {

                $serviceHistory[] = $serviceHis;
            }
        }

        return $serviceHistory;
    }

    /**
     * Lấy ra danh sách ID dịch vụ THEO LẦN đã được đăng ký của một trẻ trong một ngày.
     *
     * @param $childId
     * @param $usingAt
     * @return array
     * @throws Exception
     */
    public function getRegisteredCountBasedServiceIdAChild($childId, $usingAt) {
        global $db;
        $usingAt = toDBDate($usingAt);

        $strSql = sprintf("SELECT service_id FROM ci_service_usage WHERE child_id = %s AND using_at = %s", secure($childId, 'int'), secure($usingAt));

        $serviceIds = array();
        $get_serviceIds = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        while($serviceId = $get_serviceIds->fetch_assoc()) {
            $serviceIds[] = $serviceId['service_id'];
        }

        return $serviceIds;
    }

    /**
     * Lấy ra danh sách dịch vụ tính theo số lần cho một trẻ. Trường hợp trẻ chưa đăng ký cũng lấy ra để phục vụ ĐĂNG KÝ.
     *
     * @param $schoolId
     * @param $childId
     * @param $date
     * @return array
     * @throws Exception
     */
    public function getCountBasedServiceAChild4Register($schoolId, $childId, $date) {
        global $db;
        $date = toDBDate($date);

        $strSql = sprintf("SELECT S.service_id, S.service_name, S.fee, SU.service_usage_id, SU.recorded_at, SU.using_at, U.user_fullname FROM ci_service S
                    LEFT JOIN ci_service_usage SU ON SU.service_id = S.service_id AND SU.using_at = %s AND SU.child_id = %s
                    LEFT JOIN users U ON SU.service_usage_id > 0 AND SU.recorded_user_id = U.user_id
                    WHERE S.school_id = %s AND S.type = %s AND S.status = %s AND S.parent_display = 1", secure($date), secure($childId, 'int'),
                    secure($schoolId, 'int'), SERVICE_TYPE_COUNT_BASED, STATUS_ACTIVE);

        $serviceHistory = array();
        $get_serviceHistory = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_serviceHistory->num_rows > 0) {
            while($serviceHis = $get_serviceHistory->fetch_assoc()) {
                $serviceHis['recorded_at'] = toSysDatetime($serviceHis['recorded_at']);
                $serviceHis['using_at'] = toSysDate($serviceHis['using_at']);

                $serviceHistory[] = $serviceHis;
            }
        }

        return $serviceHistory;
    }

    /**
     * Lấy ra danh sách food service còn đang áp dụng của một trường.
     *
     * @param $schoolId
     * @return array
     * @throws Exception
     */
    public function getFoodServiceSchool($schoolId) {
        global $db;

        $strSql = sprintf("SELECT service_id, service_name, type FROM ci_service WHERE school_id = %s AND is_food = 1 AND status = 1
                            ORDER BY type ASC, service_id ASC", secure($schoolId, 'int'));

        $services = array();
        $get_service = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_service->num_rows > 0) {
            while($service = $get_service->fetch_assoc()) {
                $services[] = $service;
            }
        }

        return $services;
    }

    /**
     * Lấy ra danh sách dịch vụ tính phí theo THÁNG và theo ĐIỂM DANH dựa trên danh sách CHILD_ID được truyền vào.
     * Trả về danh sách lớp, trong mỗi lớp có các thông tin sau:
     *  1. class_id
     *  2. danh sách dịch vụ ăn uống liên quan lớp đó, mỗi dịch vụ có số lượng trẻ đăng ký.
     *
     * @param $schoolId
     * @param $childIds
     * @param $fullServiceList
     * @return array
     * @throws Exception
     */
    public function getFoodNotCountBasedServiceSchoolADay($schoolId, $childIds, $fullServiceList) {
        global $db;

        $strChildIds = implode(",", $childIds);
        $strSql = sprintf("SELECT SC.child_id, S.service_id, S.type, S.service_name, CC.class_id FROM ci_service_child SC
                            INNER JOIN ci_service S ON SC.service_id = S.service_id AND S.type != %s AND S.is_food = 1 AND S.school_id = %s
                            INNER JOIN ci_class_child CC ON CC.child_id = SC.child_id AND CC.status = %s
                            WHERE SC.status = 1 AND SC.child_id IN (%s)
                            ORDER BY CC.class_id ASC, S.type ASC, S.service_id ASC", SERVICE_TYPE_COUNT_BASED, secure($schoolId, 'int'), secure(STATUS_ACTIVE, 'int'), $strChildIds);

        //Biến này là kết quả trả về, gồm danh sách các lớp, trong mỗi lớp có 02 thuộc tính:
        // 1. class_id
        // 2. services: danh sách trẻ đăng ký dịch vụ của lớp đó.
        $classes = array();
        $get_foodServices = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_foodServices->num_rows > 0) {

            $classId = -1;
            $class = null;
            $services = null;
            while ($foodSvr = $get_foodServices->fetch_assoc()) {
                if ($classId != $foodSvr['class_id']) {
                    //Gắn dịch vụ cho lớp vừa duyệt
                    if ($classId > 0) {
                        $class['not_countbased_services'] = $this->_processClassServices($services, $fullServiceList, false);
                        $classes[] = $class; //Lưu lại lớp cũ vào danh sách
                    }

                    //Tạo thông tin cho lớp mới
                    $class = array();
                    $class['class_id'] = $foodSvr['class_id'];
                    $services = array(); //Khởi tạo lại danh sách dịch vụ cho lớp mới.
                }

                $services[] = $foodSvr;
                $classId = $foodSvr['class_id'];
            }

            //Xử lý cho lớp cuối cùng
            if ($classId > 0) {
                $class['not_countbased_services'] = $this->_processClassServices($services, $fullServiceList, false);
                $classes[] = $class; //Lưu lại lớp cũ vào danh sách
            }
        }

        return $classes;
    }

    /**
     * Hàm duyệt danh sách trẻ đăng ký dịch vụ của một lớp, từ đó tính ra mỗi dịch vụ có bao nhiêu trẻ
     * đăng ký.
     *
     * @param $services: Danh sách đã sắp xếp theo service_id
     * @param $fullServiceList
     * @param bool|false $isCountBased
     * @return array
     */
    private function _processClassServices($services, $fullServiceList, $isCountBased = false) {
        $newServices = array();
        $newService = null;
        $serviceId = -1;
        $svrCount = 0;

        foreach ($services as $service) {
            //Nếu bắt đầu duyệt một service mới
            if ($serviceId != $service['service_id']) {
                //Gán tổng cho service duyệt trước đó
                if ($serviceId > 0) {
                    $newService['total'] = $svrCount;
                    $newServices[] = $newService; //Lưu lại service trước đó vào danh sách
                }

                //Gán giá trị cho service mới
                $svrCount = 0;
                $newService = $service;
            }

            $svrCount++;
            $serviceId = $service['service_id'];
        }
        //Gán giá trị cho service cuối cùng trong danh sách.
        if ($serviceId > 0) {
            $newService['total'] = $svrCount;
            $newServices[] = $newService; //Lưu lại service trước đó vào danh sách
        }

        $results = array();
        //Tạo lại danh sách service, trường hợp service không có ai đăng ký của đưa vào với giá trị total = 0
        foreach ($fullServiceList as $service) {
            $isHas = false;
            foreach ($newServices as $newService) {
                if ($service['service_id'] == $newService['service_id']) {
                    $results[] = $newService;
                    $isHas = true;
                    break;
                }
            }
            //Nếu không có trong danh sách dịch vụ đăng ký
            if ((!$isHas) && (($isCountBased && ($service['type'] == SERVICE_TYPE_COUNT_BASED)) || (!$isCountBased && ($service['type'] != SERVICE_TYPE_COUNT_BASED)))) {
                $newService = $service;
                $newService['total'] = 0;
                $results[] = $newService;
            }
        }

        return $results;
    }


    /**
     * Lấy ra thông tin dịch vụ tính phí theo LẦN mà học sinh của trường đăng ký trong một ngày.
     * Trả về danh sách lớp, trong mỗi lớp có các thông tin sau:
     *  1. class_id
     *  2. danh sách dịch vụ ăn uống liên quan lớp đó, mỗi dịch vụ có số lượng trẻ đăng ký.
     *
     * @param $schoolId
     * @param $date
     * @param $childIds
     * @param $fullServiceList
     * @return array
     * @throws Exception
     */
    public function getFoodCountBasedServiceSchoolADay($schoolId, $date, $childIds, $fullServiceList) {
        global $db;
        $date = toDBDate($date);
        $strChildIds = implode(",", $childIds);
        $strSql = sprintf("SELECT SU.child_id, S.service_id, S.type, S.service_name, CC.class_id FROM ci_service_usage SU
                            INNER JOIN ci_service S ON SU.service_id = S.service_id AND S.type = %s AND S.is_food = 1 AND S.school_id = %s
                            INNER JOIN ci_class_child CC ON SU.child_id = CC.child_id AND CC.child_id IN (%s)
                            WHERE SU.using_at = %s
                            ORDER BY CC.class_id ASC, S.service_id ASC", SERVICE_TYPE_COUNT_BASED, secure($schoolId, 'int'), $strChildIds, secure($date));

        //Biến này là kết quả trả về, gồm danh sách các lớp, trong mỗi lớp có 02 thuộc tính:
        // 1. class_id
        // 2. services: danh sách trẻ đăng ký dịch vụ của lớp đó.
        $classes = array();
        $get_foodServices = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_foodServices->num_rows > 0) {

            $classId = -1;
            $class = null;
            $services = null;
            while ($foodSvr = $get_foodServices->fetch_assoc()) {
                if ($classId != $foodSvr['class_id']) {
                    //Gắn dịch vụ cho lớp vừa duyệt
                    if ($classId > 0) {
                        $class['countbased_services'] = $this->_processClassServices($services, $fullServiceList, true);
                        $classes[] = $class; //Lưu lại lớp cũ vào danh sách
                    }

                    //Tạo thông tin cho lớp mới
                    $class = array();
                    $class['class_id'] = $foodSvr['class_id'];
                    $services = array(); //Khởi tạo lại danh sách dịch vụ cho lớp mới.
                }

                $services[] = $foodSvr;
                $classId = $foodSvr['class_id'];
            }

            //Xử lý cho lớp cuối cùng
            if ($classId > 0) {
                $class['countbased_services'] = $this->_processClassServices($services, $fullServiceList, true);
                $classes[] = $class; //Lưu lại lớp cũ vào danh sách
            }
        }

        return $classes;
    }

    /**
     * Lấy ra thông tin chi tiết của một dịch vụ khi biết ID
     *
     * @param $serviceId
     * @return array|null
     * @throws Exception
     */
    public function getServiceById($serviceId) {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_service WHERE service_id = %s", secure($serviceId, 'int'));
        $get_services = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_services->num_rows > 0) {
            return $get_services->fetch_assoc();
        }

        return null;
    }

    /**
     * Đưa thông tin service vào hệ thống.
     *
     * @param array $args
     * @return integer
     * @throws Exception
     */
    public function insertService(array $args = array()) {
        global $db, $date, $user;
        $this->_validateServiceInput($args);

        $strSql = sprintf("INSERT INTO ci_service (service_name, school_id, type, is_food, fee, description, status, created_at, created_user_id, parent_display) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
                            secure($args['service_name']), secure($args['school_id'], 'int'), secure($args['type'], 'int'), secure($args['is_food'], 'int'),
                            secure($args['fee'], 'int'), secure($args['description']), secure($args['status'], 'int'), secure($date), secure($user->_data['user_id'], 'int'), secure($args['parent_display'], 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        return $db->insert_id;
    }

    /**
     * Kiểm tra điều kiện đầu vào.
     *
     * @param array $args
     * @throws Exception
     */
    private function _validateServiceInput(array $args = array()) {
        if (!isset($args['service_name'])) {
            throw new Exception(__("You must enter service name"));
        }

        if (strlen($args['service_name']) > 254) {
            throw new Exception(__("Service name must be less than 254 characters long"));
        }

        if (isset($args['description']) && (strlen($args['description'])) > 300) {
            throw new Exception(__("Description length must be less than 300 characters long"));
        }
    }

    /**
     * Cập nhật một service vào trong DB
     *
     * @param array $args
     * @throws Exception
     */
    public function updateService(array $args = array()) {
        global $db, $date;

        if ($args['service_id'] <= 0) {
            throw new Exception(__("You must select an existing service"));
        }
        $this->_validateServiceInput($args);

        $strSql = sprintf("UPDATE ci_service SET service_name = %s, type = %s, is_food = %s, fee = %s, description = %s, status = %s, updated_at = %s, parent_display = %s WHERE service_id = %s",
                            secure($args['service_name']), secure($args['type'], 'int'), secure($args['is_food'], 'int'), secure($args['fee'], 'int'),
                            secure($args['description']), secure($args['status'], 'int'), secure($date), secure($args['parent_display']), secure($args['service_id'], 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Lưu đăng ký sử dụng dịch vụ.
     *
     * @param $serviceId
     * @param $newUserIds
     * @param $newChildren
     * @param $cancelChildren
     * @param $changedChildren
     * @throws Exception
     */
    public function registerService($serviceId, $newUserIds, $newChildren, $cancelChildren, $changedChildren) {
        global $db, $date, $user;

        if ($serviceId <= 0) {
            throw new Exception(__("You must select an existing service"));
        }

        //I. Hủy đăng ký của trẻ
        foreach ($cancelChildren as $child) {
            $begin = toDBDate($child['begin']);
            $end = toDBDate($child['end']);
            $begin = ($begin == '')? $date: $begin;
            $end = ($end == '')? $date: $end;

            $strSql = sprintf("UPDATE ci_service_child SET status = 0, end = %s, old_end = %s, begin = %s WHERE service_id = %s AND child_id = %s",
                secure($end), secure($end), secure($begin), secure($serviceId, 'int'), secure($child['child_id'], 'int'));
            $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        }

        //II. Thêm đăng ký của những trẻ mới
        if (count($newUserIds) > 0) {
            //1. Lấy ra danh sách ID những trẻ đã từng đăng ký dịch vụ. Vì danh sách đăng ký có thể có trẻ đã từng đăng ký rồi.
            $alreadyChildIds = $this->getRegisterdChildId($serviceId, $newUserIds);
            $strValues = "";
            foreach ($newChildren as $child) {
                $begin = toDBDate($child['begin']);
                $begin = ($begin == '') ? $date : $begin;

                if (in_array($child['child_id'], $alreadyChildIds)) {
                    $strSql = sprintf("UPDATE ci_service_child SET old_begin = begin, status = 1, begin = %s, end = null WHERE service_id = %s AND child_id = %s",
                        secure($begin), secure($serviceId, 'int'), secure($child['child_id'], 'int'));
                    $db->query($strSql) or _error(SQL_ERROR_THROWEN);
                } else {
                    //service_id, child_id, begin, old_begin, status, created_at, created_user_id
                    $strValues .= "(" . secure($serviceId, 'int') . "," . secure($child['child_id'], 'int') . "," . secure($begin) . "," . secure($begin) . ",'1'," . secure($date) . "," . secure($user->_data['user_id'], 'int') . "),";
                }
            }
            $strValues = trim($strValues, ",");
            if ($strValues != '') {
                $strSql = "INSERT INTO ci_service_child (service_id, child_id, begin, old_begin, status, created_at, created_user_id) VALUES " . $strValues;
                $db->query($strSql) or _error(SQL_ERROR_THROWEN);
            }
        }

        //III. Cập nhật danh sách đăng ký trẻ đã thay đổi.
        foreach ($changedChildren as $child) {
            $begin = toDBDate($child['begin']);
            $end = toDBDate($child['end']);
            if ($child['status'] == 1) {
                $begin = ($begin == '')? $date: $begin;
                $end = '';
            } else {
                $begin = ($begin == '')? $date: $begin;
                $end = ($end == '')? $date: $end;
            }
            $strSql = sprintf("UPDATE ci_service_child SET begin = %s, end = %s WHERE service_id = %s AND child_id = %s",
                                secure($begin), secure($end), secure($serviceId, 'int'), secure($child['child_id'], 'int'));
            $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        }
    }

    /**
     * Xóa đăng ký nhầm của trẻ. Áp dụng cho đăng ký dịch vụ theo THÁNG & ĐIỂM DANH
     *
     * @param $serviceId
     * @param $childId
     * @throws Exception
     */
    public function deleteServiceRegistration($serviceId, $childId) {
        global $db;

        if ($serviceId <= 0) {
            throw new Exception(__("You must select an existing service"));
        }
        //Cập nhật những đăng ký đã từng đăng ký THẬT rồi sau đó HỦY
        $strSql = sprintf("UPDATE ci_service_child SET begin = old_begin, end = old_end, status = %s WHERE service_id = %s AND child_id = %s AND old_end IS NOT NULL AND old_end != ''",
            secure(STATUS_INACTIVE, 'int'), secure($serviceId, 'int'), secure($childId, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        //Xóa đăng ký NHẦM và đó là đăng ký lần đầu.
        $strSql = sprintf("DELETE FROM ci_service_child WHERE service_id = %s AND child_id = %s AND (old_end IS NULL OR old_end = '')", secure($serviceId, 'int'), secure($childId, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Lưu đăng ký sử dụng dịch vụ cho trẻ
     *
     * @param $childId
     * @param $newServiceIds
     * @param $cancelServiceIds
     * @throws Exception
     * @internal param $serviceId
     * @internal param $newUserIds
     * @internal param $cancelUserIds
     */
    public function registerServiceForChild($childId, $newServiceIds, $cancelServiceIds) {
        global $db, $date, $user;

        if ($childId <= 0) {
            throw new Exception(__("You must select an existing student"));
        }

        //I. Hủy đăng ký dịch vụ của trẻ
        if (count($cancelServiceIds) > 0) {
            $strCon = implode(',', $cancelServiceIds);
            $strSql = sprintf("UPDATE ci_service_child SET status = 0, end = %s WHERE child_id = %s AND service_id IN (%s)", secure($date), secure($childId, 'int'), $strCon);
            $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        }
        //II. Thêm đăng ký của những dịch vụ mới
        if (count($newServiceIds) > 0) {
            //1. Lấy ra danh sách ID những dịch vụ đã từng đăng ký. Vì danh sách đăng ký có thể có dịch vụ đã từng đăng ký rồi.
            $alreadyServiceIds = $this->_getRegisterdServiceId($childId, $newServiceIds);
            if (count($alreadyServiceIds) > 0) {
                $strCon = implode(',', $alreadyServiceIds);
                $strSql = sprintf("UPDATE ci_service_child SET status = 1, begin = %s, end = '' WHERE child_id = %s AND service_id IN (%s)", secure($date), secure($childId, 'int'), $strCon);
                $db->query($strSql) or _error(SQL_ERROR_THROWEN);
            }

            //2. Nhập danh sách những dịch vụ đăng ký lần đầu
            $firstServiceIds = array_diff($newServiceIds, $alreadyServiceIds); //ID của dịch vụ đăng ký lần đầu
            //Câu sql sẽ có dạng: INSERT INTO tbl_name (col_A,col_B,col_C) VALUES (1,2,3), (4,5,6), (7,8,9)
            //Build lên phần giá trị truyền vào.
            if (count($firstServiceIds) > 0) {
                $strValues = "";
                foreach ($firstServiceIds as $id) {
                    //service_id, child_id, begin, status, created_at, created_user_id
                    $strValues .= "(" . secure($childId, 'int') . "," . secure($id, 'int') . "," . secure($date) . ",'1'," . secure($date) . "," . secure($user->_data['user_id'], 'int') . "),";
                }
                $strValues = trim($strValues, ",");
                $strSql = "INSERT INTO ci_service_child (child_id, service_id, begin, status, created_at, created_user_id) VALUES " . $strValues;
                $db->query($strSql) or _error(SQL_ERROR_THROWEN);
            }
        }
    }

    /**
     * Đăng ký dịch vụ THÁNG và theo ĐIỂM DANH cho 1 trẻ.
     *
     * @param $childId
     * @param $serviceIds
     * @throws Exception
     */
    public function registerNotCountBasedServiceForChild($childId, $serviceIds) {
        global $db, $date, $user;

        if ($childId <= 0) {
            throw new Exception(__("You must select an existing student"));
        }

        //II. Thêm đăng ký của những dịch vụ mới
        if (count($serviceIds) > 0) {
            //1. Lấy ra danh sách ID những dịch vụ đã từng đăng ký. Vì danh sách đăng ký có thể có dịch vụ đã từng đăng ký rồi.
            $alreadyServiceIds = $this->_getRegisterdServiceId($childId, $serviceIds);

            if (count($alreadyServiceIds) > 0) {
                $strCon = implode(',', $alreadyServiceIds);
                $strSql = sprintf("UPDATE ci_service_child SET status = %s, begin = %s, end = '' WHERE child_id = %s AND service_id IN (%s)", secure(STATUS_ACTIVE, 'int'), secure($date), secure($childId, 'int'), $strCon);
                $db->query($strSql) or _error(SQL_ERROR_THROWEN);
            }

            //2. Nhập danh sách những dịch vụ đăng ký lần đầu
            $firstServiceIds = array_diff($serviceIds, $alreadyServiceIds); //ID của dịch vụ đăng ký lần đầu
            //Câu sql sẽ có dạng: INSERT INTO tbl_name (col_A,col_B,col_C) VALUES (1,2,3), (4,5,6), (7,8,9)
            //Build lên phần giá trị truyền vào.

            if (count($firstServiceIds) > 0) {
                $strValues = "";
                foreach ($firstServiceIds as $id) {
                    //service_id, child_id, begin, status, created_at, created_user_id
                    $strValues .= "(" . secure($childId, 'int') . "," . secure($id, 'int') . "," . secure($date) . ",'1'," . secure($date) . "," . secure($user->_data['user_id'], 'int') . "),";
                }
                $strValues = trim($strValues, ",");
                $strSql = "INSERT INTO ci_service_child (child_id, service_id, begin, status, created_at, created_user_id) VALUES " . $strValues;
                $db->query($strSql) or _error(SQL_ERROR_THROWEN);
            }
        }
    }

    /**
     * Đăng ký dịch vụ cho trẻ khi thêm trẻ mới
     *
     * @param $childId
     * @param $serviceIds
     * @param $beginAt
     * @throws Exception
     */
    public function registerServiceForChildOfAddChild($childId, $serviceIds, $beginAt) {
        global $db, $date, $user;

        $beginAt = toDBDate($beginAt);

        if ($childId <= 0) {
            throw new Exception(__("You must select an existing student"));
        }

        // Thêm đăng ký của những dịch vụ mới
        if (count($serviceIds) > 0) {
            //Câu sql sẽ có dạng: INSERT INTO tbl_name (col_A,col_B,col_C) VALUES (1,2,3), (4,5,6), (7,8,9)
            //Build lên phần giá trị truyền vào.

            if (count($serviceIds) > 0) {
                $strValues = "";
                foreach ($serviceIds as $id) {
                    //service_id, child_id, begin, status, created_at, created_user_id
                    $strValues .= "(" . secure($childId, 'int') . "," . secure($id, 'int') . "," . secure($beginAt) . ",'1'," . secure($date) . "," . secure($user->_data['user_id'], 'int') . "),";
                }
                $strValues = trim($strValues, ",");
                $strSql = "INSERT INTO ci_service_child (child_id, service_id, begin, status, created_at, created_user_id) VALUES " . $strValues;
                $db->query($strSql) or _error(SQL_ERROR_THROWEN);
            }
        }
    }

    /**
     * Lưu thông tin sử dụng dịch vụ của trẻ
     *
     * @param $serviceId
     * @param $newUserIds
     * @param $cancelUserIds
     * @param $usingAt
     * @throws Exception
     */
    public function recordServiceUsage($serviceId, $newUserIds, $cancelUserIds, $usingAt) {
        global $db, $date, $user;
        $usingAt = toDBDate($usingAt);

        if ($serviceId <= 0) {
            throw new Exception(__("You must select an existing service"));
        }

        //I. Xóa sử dụng dịch vụ của trẻ (do lần trước ghi nhầm)
        if (count($cancelUserIds) > 0) {
            $strCon = implode(',', $cancelUserIds);
            $strSql = sprintf("DELETE FROM ci_service_usage WHERE service_id = %s AND using_at = %s AND child_id IN (%s)", secure($serviceId, 'int'), secure($usingAt), $strCon);
            $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        }
        //II. Thêm những trẻ sử dụng mới.
        if (count($newUserIds) > 0) {
            $strValues = "";
            foreach ($newUserIds as $id) {
                //service_id, child_id, using_at, recorded_at, recorded_user_id
                $strValues .= "(" . secure($serviceId, 'int') . "," . secure($id, 'int') . "," . secure($usingAt) . "," . secure($date) . "," . secure($user->_data['user_id'], 'int') . "),";
            }
            $strValues = trim($strValues, ",");
            $strSql = "INSERT INTO ci_service_usage(service_id, child_id, using_at, recorded_at, recorded_user_id) VALUES " . $strValues;
            $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        }
    }

    /**
     * Lưu thông tin giảm trừ dịch vụ của trẻ.
     *
     * @param $serviceId
     * @param $deductionDate
     * @param $newChildIds
     * @param $cancelUserIds
     * @throws $cancelChildIds
     */
    public function recordServiceService($serviceId, $deductionDate, $newChildIds, $cancelChildIds) {
        global $db, $date, $user;
        $deductionDate = toDBDate($deductionDate);

        //I. Xóa giảm trừ dịch vụ của trẻ.
        if (count($cancelChildIds) > 0) {
            $strCon = implode(',', $cancelChildIds);
            $strSql = sprintf("DELETE FROM ci_service_deduction WHERE service_id = %s AND deduction_date = %s AND child_id IN (%s)", secure($serviceId, 'int'), secure($deductionDate), $strCon);
            $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        }
        //II. Thêm những trẻ sử dụng mới.
        if (count($newChildIds) > 0) {
            $strValues = "";
            foreach ($newChildIds as $id) {
                //service_id, child_id, using_at, recorded_at, recorded_user_id
                $strValues .= "(" . secure($serviceId, 'int') . "," . secure($id, 'int') . "," . secure($deductionDate) . "," . secure($date) . "," . secure($user->_data['user_id'], 'int') . "),";
            }
            $strValues = trim($strValues, ",");
            $strSql = "INSERT INTO ci_service_deduction(service_id, child_id, deduction_date, recorded_at, recorded_user_id) VALUES " . $strValues;
            $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        }
    }

    /**
     * Lưu thông tin sử dụng dịch vụ của trẻ ở acc phụ huynh
     *
     * @param $childId
     * @param $newServiceIds
     * @param $cancelServiceIds
     * @param $usingAt
     * @throws Exception
     */
    public function recordServiceUsageForChild($childId, $newServiceIds, $cancelServiceIds, $usingAt) {
        global $db, $date, $user;
        $usingAt = toDBDate($usingAt);

        if ($childId <= 0) {
            throw new Exception(__("You must select an existing student"));
        }

        //I. Xóa sử dụng dịch vụ của trẻ (do lần trước ghi nhầm)
        if (count($cancelServiceIds) > 0) {
            $strCon = implode(',', $cancelServiceIds);
            $strSql = sprintf("DELETE FROM ci_service_usage WHERE child_id = %s AND using_at = %s AND service_id IN (%s)", secure($childId, 'int'), secure($usingAt), $strCon);
            $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        }
        //II. Thêm những dịch vụ sử dụng mới.
        if (count($newServiceIds) > 0) {
            $strValues = "";
            $strSql = sprintf("SELECT service_id FROM ci_service_usage WHERE child_id = %s AND using_at = %s", secure($childId, 'int'), secure($usingAt));
            $get_service = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
            $serviceIds = array();
            if($get_service->num_rows > 0) {
                while($service_id = $get_service->fetch_assoc()) {
                    $serviceIds[] = $service_id['service_id'];
                }
            }
            $newServiceIds = array_diff($newServiceIds, $serviceIds);
            foreach ($newServiceIds as $id) {
                //child_id, service_id, using_at, recorded_at, recorded_user_id
                $strValues .= "(" . secure($childId, 'int') . "," . secure($id, 'int') . "," . secure($usingAt) . "," . secure($date) . "," . secure($user->_data['user_id'], 'int') . "),";
            }
            $strValues = trim($strValues, ",");
            $strSql = "INSERT INTO ci_service_usage(child_id, service_id, using_at, recorded_at, recorded_user_id) VALUES " . $strValues;
            $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        }
    }

    /**
     * Đăng ký dịch vụ tính theo LẦN cho trẻ.
     *
     * @param $childId
     * @param $serviceIds
     * @param $usingAt
     * @throws Exception
     *
     */
    public function registerCountBasedServiceForChild($childId, $serviceIds, $usingAt) {
        global $db, $date, $user;
        $usingAt = toDBDate($usingAt);

        if ($childId <= 0) {
            throw new Exception(__("You must select an existing student"));
        }

        //II. Thêm những dịch vụ sử dụng mới.
        if (count($serviceIds) > 0) {
            $strValues = "";
            foreach ($serviceIds as $id) {
                //child_id, service_id, using_at, recorded_at, recorded_user_id
                $strValues .= "(" . secure($childId, 'int') . "," . secure($id, 'int') . "," . secure($usingAt) . "," . secure($date) . "," . secure($user->_data['user_id'], 'int') . "),";
            }
            $strValues = trim($strValues, ",");
            $strSql = "INSERT INTO ci_service_usage(child_id, service_id, using_at, recorded_at, recorded_user_id) VALUES " . $strValues;
            $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        }
    }

    /**
     * Lấy ra danh sách ID những trẻ đã từng đăng ký 1 dịch vụ từ 1 danh sách ID cho trước.
     *
     * @param $serviceId
     * @param $childIds
     * @return array
     * @throws Exception
     */
    private function getRegisterdChildId($serviceId, $childIds) {
        global $db;

        $strCon = implode(',', $childIds);
        $strSql = sprintf("SELECT child_id FROM ci_service_child WHERE service_id = %s AND child_id IN (%s)", secure($serviceId, 'int'), $strCon);

        $childIds = array();
        $get_childIds = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_childIds->num_rows > 0) {
            while($child = $get_childIds->fetch_assoc()) {
                $childIds[] = $child['child_id'];
            }
        }
        return $childIds;
    }
    /**
     * Lấy ra danh sách ID những dịch vụ đã từng đăng ký của một trẻ từ 1 danh sách ID cho trước.
     *
     * @param $childId
     * @param $serviceIds
     * @return array
     * @throws Exception
     */
    private function _getRegisterdServiceId($childId, $serviceIds) {
        global $db;

        $strCon = implode(',', $serviceIds);
        $strSql = sprintf("SELECT service_id FROM ci_service_child WHERE child_id = %s AND service_id IN (%s)", secure($childId, 'int'), $strCon);

        $get_serviceIds = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $serviceIdsReady = array();
        if ($get_serviceIds->num_rows > 0) {
            while ($service = $get_serviceIds->fetch_assoc()) {
                $serviceIdsReady[] = $service['service_id'];
            }
        }

        return $serviceIdsReady;
    }

    /**
     * Hủy đăng ký dịch vụ khi trẻ thôi học
     *
     * @param $childId
     * @param $endAt
     * @throws Exception
     */
    public function updateServiceForChildLeave($childId, $endAt) {
        global $db, $date;

        if (!validateDate($endAt)) {
            throw new Exception(__("Thời gian gửi lên không đúng, vui lòng kiểm tra lại"));
        }

        $endAt = toDBDate($endAt);
        $strSql = sprintf("UPDATE ci_service_child SET status = 0, end = %s, updated_at = %s WHERE child_id = %s AND end IS NULL AND status = 1",
                           secure($endAt), secure($date), secure($childId, 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Hủy đăng ký dịch vụ khi lớp tốt nghiệp
     *
     * @param $childIds
     * @param $endAt
     * @throws Exception
     */
    public function updateServiceForChildrenLeave($childIds, $endAt) {
        global $db, $date;

        if (!validateDate($endAt)) {
            throw new Exception(__("Thời gian gửi lên không đúng, vui lòng kiểm tra lại"));
        }
        if(count($childIds) > 0) {
            $strCon = implode(',', $childIds);
            $endAt = toDBDate($endAt);
            $strSql = sprintf("UPDATE ci_service_child SET status = 0, end = %s, updated_at = %s WHERE child_id IN (%s) AND end IS NULL AND status = 1",
                secure($endAt), secure($date), $strCon);

            $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        }
    }

    /**
     * Xóa một service khỏi hệ thống
     *
     * @param $service_id
     * @throws Exception
     */
    public function deleteService($service_id) {
        global $db;
        $db->query(sprintf("DELETE FROM ci_service WHERE service_id = %s", secure($service_id, 'int') )) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Inactive service
     *
     * @param $serviceId
     * @throws Exception
     */
    public function InactiveService($serviceId) {
        global $db, $date;

        $strSql = sprintf("UPDATE ci_service SET status = %s, updated_at = %s WHERE service_id = %s", secure(STATUS_INACTIVE, 'int'), secure($date), secure($serviceId, 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Lấy ra list child_id trẻ sử dụng dịch vụ theo service_id và using_at
     *
     * @param $service_id
     * @param $usingAt
     * @return array
     * @throws Exception
     */
    public function _getChildsUsage($service_id, $usingAt, $allChildIds) {
        global $db;
        $usingAt = toDBDate($usingAt);
        $strCon = implode(',', $allChildIds);
        $strSql = sprintf("SELECT DISTINCT child_id FROM ci_service_usage WHERE service_id = %s AND using_at = %s AND child_id IN (%s)", secure($service_id, 'int'), secure($usingAt), $strCon);

        $childIds = array();
        $get_children = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_children->num_rows > 0) {
            while($child = $get_children->fetch_assoc()) {
                $childIds[] = $child['child_id'];
            }
        }
        return $childIds;

    }

    /**
     * Lấy ra tất cả các dịch vụ theo số lần sử dụng đăng ký cho trẻ trong 1 lớp
     *
     * @param $using_at
     * @param $childrenIds
     * @return array
     */
    public function getAllServiceCBForAllChildOfClass($using_at, $childrenIds) {
        global $db;

        $usingAt = toDBDate($using_at);
        $strCon = implode(",", $childrenIds);
        $strSql = sprintf("SELECT SU.*, U.user_fullname FROM ci_service_usage SU 
                      INNER JOIN users U ON SU.recorded_user_id = U.user_id
                      WHERE SU.using_at = %s AND SU.child_id IN (%s)", secure($usingAt), $strCon);

        $results = array();
        $get_result = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_result->num_rows > 0) {
            while($result = $get_result->fetch_assoc()) {
                $results[] = $result;
            }
        }
        return $results;
    }

    /**
     * Delete toàn bộ dịch vụ trẻ của lớp đã đăng ký theo ngày using_at
     *
     * @param $using_at
     * @param $childrenIds
     */
    public function deleteAllServiceCBForAllChildOfClass($using_at, $childrenIds) {
        global $db;

        $usingAt = toDBDate($using_at);
        $strCon = implode(",", $childrenIds);
        $strSql = sprintf("DELETE FROM ci_service_usage WHERE using_at = %s AND child_id IN (%s)", secure($usingAt), $strCon);

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Lưu đăng ký dịch vụ THEO LẦN của cả lớp.
     *
     * @param $registerChildIds: Danh sách ID trẻ, tương ứng mỗi ID là danh sách ID các dịch vụ được đăng ký.
     * @param $using_at
     * @throws Exception
     */
    public function recordServiceUsageForAllChild($registerChildIds, $using_at) {
        global $db, $user, $date;

        $usingAt = toDBDate($using_at);

        // Lặp danh sách trẻ, lấy ra những dịch vụ trẻ đăng ký
        $strValues = '';
        foreach ($registerChildIds as $childId => $serviceIds) {
            foreach ($serviceIds as $serviceId) {
                $strValues .= "(".secure($serviceId, 'int').",".secure($childId, 'int').",".secure($usingAt).",".secure($user->_data['user_id'], 'int'). ",".secure($date)."),";
            }
        }

        $strValues = trim($strValues, ",");
        $strSql = "INSERT INTO ci_service_usage(service_id, child_id, using_at, recorded_user_id, recorded_at) VALUES " . $strValues;
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /* ---------- SERVICE - MEMCACHED ---------- */

    /**
     * Lây ra danh sách các dịch vụ của một trường
     *
     * @param $schoolId
     * @param int $checkRegister
     * @return array
     * @throws Exception
     */
    public function getServices4Memcahe($schoolId) {
        global $db;

        $services = array();
        $strSql = sprintf("SELECT service_id, service_name, school_id, type, must_register, is_food, fee, description, status, updated_at FROM ci_service 
                           WHERE school_id = %s AND status = %s ORDER BY service_name ASC, type ASC", secure($schoolId, 'int'), secure(STATUS_ACTIVE, 'int'));

        $get_services = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_services->num_rows > 0) {
            while($service = $get_services->fetch_assoc()) {
                $services[$service['service_id']] = $service;
            }
        }
        return $services;
    }

    /**
     * Lấy tổng hợp dịch vụ theo số lần sử dụng của mỗi lớp trong một ngày
     *
     * @param $serviceId
     * @param $classId
     * @param $usingAt
     * @return null
     * @throws Exception
     */
    public function getCountServiceBasedClass($serviceId, $classId, $usingAt) {
        global $db;

        $usingAt = toDBDate($usingAt);

        $strSql = sprintf("SELECT count(SU.service_usage_id) AS cnt FROM ci_service_usage SU
                            INNER JOIN ci_class_child CC ON SU.child_id = CC.child_id
                            WHERE CC.class_id = %s AND SU.using_at = %s AND SU.service_id = %s", secure($classId, 'int'), secure($usingAt), secure($serviceId, 'int'));

        $get_serviceHistory = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $count = null;
        if($get_serviceHistory->num_rows > 0) {
            $count = $get_serviceHistory->fetch_assoc()['cnt'];
        }

        return $count;
    }

    /**
     * Lấy dịch vụ theo tháng và điểm danh theo lớp
     *
     * @param $serviceId
     * @param $classId
     * @param $usingAt
     * @return null
     * @throws Exception
     */
    public function getCountServiceBasedClassNCB($serviceId, $classId, $usingAt) {
        global $db;

        $usingAt = toDBDate($usingAt);

        //Nếu chọn lớp, thì lấy ra danh sách lớp & thông tin sử dụng dịch vụ
        $strSql = sprintf("SELECT count(SC.service_child_id) as cnt FROM ci_child C
                            INNER JOIN ci_class_child CC ON C.child_id = CC.child_id AND CC.class_id = %s
                            INNER JOIN ci_service_child SC ON C.child_id = SC.child_id AND SC.service_id = %s 
                            INNER JOIN ci_attendance_detail AD ON AD.child_id = SC.child_id AND (AD.status = 1 OR AD.status = 2 OR AD.status = 3)
                    INNER JOIN ci_attendance A ON A.attendance_id = AD.attendance_id
                    WHERE (COALESCE(SC.end, '') = '' OR SC.end >= %s OR SC.end = '0000-00-00 00:00:00') AND DATE(SC.begin) <= %s AND DATE(A.attendance_date) = %s", secure($classId, 'int'), secure($serviceId, 'int'), secure($usingAt), secure($usingAt), secure($usingAt));

        $get_serviceHistory = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $count = null;
        if($get_serviceHistory->num_rows > 0) {
            $count = $get_serviceHistory->fetch_assoc()['cnt'];
        }
        return $count;
    }
}
?>