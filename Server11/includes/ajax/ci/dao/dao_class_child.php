<?php

/**
 * Class ClassChildDAO: thao tác với bảng ci_class_child của hệ thống.
 */
class ClassChildDAO {
    /**
     * __construct
     *
     */
    public function __construct()
    {
    }

    /**
     * Kiểm tra đầu vào cho hàm insert và update
     *
     * @param array $args
     * @throws Exception
     */
    private function validateInput (array $args = array()) {
        if (is_empty($args['class_id'])) {
            throw new Exception(__("You must select one class"));
        }
        if (is_empty($args['child_id'])) {
            throw new Exception(__("You must select one child"));
        }
        if (is_empty($args['begin_at'])) {
            throw new Exception(__("You must enter begin date"));
        }
        if ((!is_empty($args['description'])) && (strlen($args['description']) > 300)) {
            throw new Exception(__("Description length must be less than 300 characters long"));
        }
    }

    /**
     * Thêm một học sinh vào lớp.
     *
     * @param array $args
     * @return mixed
     * @throws Exception
     */
    public function addChild(array $args = array())
    {
        global $db;
        $this->validateInput($args);

        $strSql = sprintf("INSERT INTO ci_class_child (class_id, child_id, begin_at, end_at, status, requested_user_id,
                            approved_user_id, description, requested_at, approved_at)
                            VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
            secure($args['class_id'], 'int'), secure($args['child_id'], 'int'), secure($args['begin_at']),
            secure($args['end_at']), secure($args['status'], 'int'), secure($args['requested_user_id'], 'int'),
            secure($args['approved_user_id'], 'int'), secure($args['description']), secure($args['requested_at']),
            secure($args['approved_at']));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Activate trạng thái cho một child tham gia vào lớp.
     *
     * @param array $args
     * @throws Exception
     */
    public function activateChild(array $args = array())
    {
        global $db;
        $strSql = sprintf("UPDATE ci_class_child SET begin_at = %s, status = %s, approved_user_id = %s, approved_at = %s
                    WHERE class_id = %s AND child_id = %s",
            secure($args['begin_at']), STATUS_ACTIVE, secure($args['approved_user_id'], 'int'),
            secure($args['approved_at']), secure($args['class_id'], 'int'), secure($args['child_id'], 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Deactivate trạng thái cho một child trong lớp.
     *
     * @param array $args
     * @throws Exception
     */
    public function deactivateChild(array $args = array())
    {
        global $db;
        $strSql = sprintf("UPDATE ci_class_child SET end_at = %s, status = %s
                    WHERE class_id = %s AND child_id = %s",
            secure($args['end_at']), STATUS_INACTIVE, secure($args['class_id'], 'int'), secure($args['child_id'], 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Cập nhật thông tin học sinh trong lớp
     *
     * @param array $args
     * @throws Exception
     */
    public function updateChild(array $args = array())
    {
        global $db;
        $this->validateInput($args);
        $strSql = sprintf("UPDATE ci_class_child SET begin_at = %s, description = %s WHERE class_id = %s AND child_id = %s",
            secure($args['begin_at']), secure($args['description']), secure($args['class_id'], 'int'),
            secure($args['child_id'], 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Lấy ra danh sách học sinh của một lớp (bao gồm cả học sinh đã nghỉ học và đang xin vào học)
     *
     * @param $class_id
     * @return array
     * @throws Exception
     */
    public function getClassChildren($class_id) {
        global $db;

        $strSql = sprintf("SELECT CC.* , C.child_name, C.birthday, C.gender, C.parent_phone FROM ci_class_child CC INNER JOIN ci_child C ON CC.child_id = C.child_id
            WHERE CC.class_id = %s ORDER BY CC.status DESC, C.name_for_sort ASC", secure($class_id, 'int'));

        $children = array();
        $get_children = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_children->num_rows > 0) {
            while($child = $get_children->fetch_assoc()) {
                $children[] = $child;
            }
        }
        return $children;
    }

    /**
     * Lấy ra danh sách học sinh của lớp có trạng thái "WaitApprove" và "Active"
     *
     * @param $class_id
     * @return array
     * @throws Exception
     */
    public function getNotInactiveClassChildren($class_id) {
        global $db;

        $strSql = sprintf("SELECT CC.* , C.child_name, C.birthday, C.gender, C.parent_phone FROM ci_class_child CC INNER JOIN ci_child C ON CC.child_id = C.child_id
            WHERE CC.class_id = %s AND CC.status = %s ORDER BY CC.status DESC, C.name_for_sort ASC", secure($class_id, 'int'), STATUS_ACTIVE);

        $children = array();
        $get_children = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_children->num_rows > 0) {
            while($child = $get_children->fetch_assoc()) {
                $children[] = $child;
            }
        }
        return $children;
    }

    /**
     * Lấy ra xỉ số lớp
     *
     * @param $class_id
     * @return mixed
     * @throws Exception
     */
    public function getCountChildren($class_id) {
        global $db;

        $strSql = "SELECT COUNT(child_id) AS children_count FROM ci_class_child
                    WHERE class_id = %s AND status = %s";

        $get_count = $db->query(sprintf($strSql, secure($class_id, 'int'), STATUS_ACTIVE)) or _error(SQL_ERROR_THROWEN);
        return $get_count->fetch_assoc()['children_count'];
    }


    /**
     * Xóa một trường khỏi hệ thống.
     * Trường bản thân nó là một page, nên khi xóa trường:
     * - Hệ thống xóa page (thuộc về phần lõi hệ thống)
     * - Module ConIu chỉ xóa thêm những thành phần phụ thuộc khác.
     *
     * @param integer $page_id
     * @return void
     */
    public function deleteSchool($page_id)
    {
        global $db;

        //TODO
    }

    /**
     * Hàm xóa một class khỏi hệ thống.
     *
     * @param $class_id
     */
    public function deleteClass($class_id)
    {
        global $db;

        //TODO
    }
}
?>