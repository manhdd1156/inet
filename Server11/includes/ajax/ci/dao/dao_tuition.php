<?php

/**
 * DAO -> Tuition
 * Thao tác với bảng fee và tuition
 *
 * @package ConIu
 * @author QuanND
 */
class TuitionDAO
{
    /**
     * __construct
     *
     */
    public function __construct()
    {
    }

    /**
     * Nhập thông tin học phí của lớp vào hệ thống
     *
     * @param $args
     * @return mixed
     * @throws Exception
     */
    public function insertTuition($args)
    {
        global $db, $date, $user;
        $args['month'] = '01/'.$args['month'];
        $this->_validateTuitionInput($args);
        $args['month'] = toDBDate($args['month']);

        //Kiểm tra xem đã có học phí của lớp/tháng/loại trong hệ thống chưa
        $strSql = sprintf("SELECT tuition_id FROM ci_tuition WHERE class_id = %s AND month = %s", secure($args['class_id'], 'int'), secure($args['month']));
        $get_tuitions = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_tuitions->num_rows > 0) {
            throw new Exception(__("This tuition is existing"));
        }

        $strSql = sprintf("INSERT INTO ci_tuition (school_id, class_id, total_amount, month, day_of_month,
                  is_notified, description, created_at, created_user_id) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)",
            secure($args['school_id'], 'int'), secure($args['class_id'], 'int'), secure($args['total_amount'], 'int'), secure($args['month']),
            secure($args['day_of_month'], 'int'), secure($args['is_notified'], 'int'), secure($args['description']), secure($date), secure($user->_data['user_id'], 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        return $db->insert_id;
    }

    /**
     * Cập nhật học phí một lớp.
     *
     * @param $args
     * @throws Exception
     */
    public function updateTuition($args)
    {
        global $db, $date;
        if (isset($args['description']) && (strlen($args['description'])) > 300) {
            throw new Exception(__("Description length must be less than 300 characters long"));
        }

        $strSql = sprintf("UPDATE ci_tuition SET total_amount = %s, is_notified = %s, description = %s, updated_at = %s WHERE tuition_id = %s",
                            secure($args['total_amount'], 'int'), secure($args['is_notified'], 'int'), secure($args['description']), secure($date), secure($args['tuition_id'], 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Validate thông tin đầu vào của học phí
     *
     * @param $args
     * @param bool|true $isCreated
     * @throws Exception
     */
    private function _validateTuitionInput($args, $isCreated = true)
    {
        if ($args['day_of_month'] <= 0 || $args['day_of_month'] > 31) {
            throw new Exception("Number of school days must be in the range of 01 and 31");
        }
        if ($isCreated && ((!isset($args['class_id'])) || ($args['class_id'] <= 0))) {
            throw new Exception(__("You must select a class"));
        }

        if (!validateDate($args['month'])) {
            throw new Exception(__("You must enter correct month"));
        }

        if (isset($args['description']) && (strlen($args['description'])) > 300) {
            throw new Exception(__("Description length must be less than 300 characters long"));
        }
    }

    /**
     * Update tổng học phí của cả lớp sau khi thêm mới học phí trẻ vào sau
     *
     * @param $tuitionId
     * @param $totalAmount
     * @throws Exception
     */
    public function updateTotal($tuitionId, $totalAmount)
    {
        global $db, $date;
        $strSql = sprintf("UPDATE ci_tuition SET total_amount = %s, updated_at = %s WHERE tuition_id = %s",
                            secure($totalAmount, 'int'), secure($date), secure($tuitionId, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Nhập thông tin học phí chi tiết của một trẻ vào hệ thống. Bao gồm PHÍ và DỊCH VỤ
     *
     * @param $tuitionId
     * @param $child
     * @param $tuitionDetails
     * @return mixed
     * @throws Exception
     */
    public function insertTuitionChild($tuitionId, $child, $tuitionDetails)
    {
        global $db;

        if ((!empty($child['pre_month'])) && (strlen($child['pre_month']) >= 6)) {
            $child['pre_month'] = toDBDate('01/'.$child['pre_month']);
        }

        //Nhập thông tin học phí của trẻ
        $strSql = sprintf("INSERT INTO ci_tuition_child (tuition_id, child_id, pre_month, debt_amount, total_amount, description) VALUES (%s, %s, %s, %s, %s, %s)",
            secure($tuitionId, 'int'), secure($child['child_id'], 'int'), secure($child['pre_month']), secure($child['debt_amount'], 'int'),
            secure($child['total_amount'], 'int'), secure($child['description']));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $tuition_child_id = $db->insert_id;

        if (count($tuitionDetails) > 0) {
            //Nhập danh sách các khoản phí cấu thành nên học phí vào hệ thống
            $strValues = "";
            foreach ($tuitionDetails as $tuitionDetail) {
                //service_id, fee_id, tuition_child_id, quantity, unit_price, type, service_type, unit_price_deduction, quantity_deduction
                $strValues .= "(" . secure($tuitionDetail['service_id'], 'int') . "," . secure($tuitionDetail['fee_id'], 'int') . "," . secure($tuition_child_id, 'int') . ","
                    . secure($tuitionDetail['quantity'], 'int') . "," . secure($tuitionDetail['unit_price'], 'int') . "," . secure($tuitionDetail['type'], 'int') . ","
                    . secure($tuitionDetail['service_type'], 'int') . "," . secure($tuitionDetail['unit_price_deduction'], 'int') . "," . secure($tuitionDetail['quantity_deduction'], 'int') . "),";
            }

            $strValues = trim($strValues, ",");
            $strSql = "INSERT INTO ci_tuition_detail (service_id, fee_id, tuition_child_id, quantity, unit_price, type, service_type, unit_price_deduction, quantity_deduction) VALUES " . $strValues;
            $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        }

        return $tuition_child_id;
    }

    /**
     * Nhập thông tin quyết toán học phí của trẻ vào hệ thống.
     *
     * @param $tuition4Leave
     * @param $tuitionDetails
     * @return mixed
     * @throws Exception
     */
    public function insertTuition4Leave($tuition4Leave, $tuitionDetails)
    {
        global $db, $user, $date;

        //Nhập thông tin học phí của trẻ
        $strSql = sprintf("INSERT INTO ci_tuition_4leave (school_id, child_id, attendance_count, final_amount, debt_amount, total_deduction, paid_amount, cashier_id, updated_at) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)",
                            secure($tuition4Leave['school_id'], 'int'), secure($tuition4Leave['child_id'], 'int'), secure($tuition4Leave['attendance_count'], 'int'),
                            secure($tuition4Leave['final_amount'], 'int'), secure($tuition4Leave['debt_amount'], 'int'), secure($tuition4Leave['total_deduction'], 'int'),
                            secure($tuition4Leave['paid_amount'], 'int'), secure($user->_data['user_id'], 'int'), secure($date));

        $db->query($strSql) or _error('error', $strSql);
        $tuition4LeaveId = $db->insert_id;

        if (count($tuitionDetails) > 0) {
            $strValues = "";
            foreach ($tuitionDetails as $tuitionDetail) {
                //service_id, fee_id, tuition_4leave_id, quantity, unit_price, type, service_type
                $strValues .= "(" . secure($tuitionDetail['service_id'], 'int') . "," . secure($tuitionDetail['fee_id'], 'int') . "," . secure($tuition4LeaveId, 'int') . ","
                    . secure($tuitionDetail['quantity'], 'int') . "," . secure($tuitionDetail['unit_price'], 'int') . "," . secure($tuitionDetail['type'], 'int') . ","
                    . secure($tuitionDetail['service_type'], 'int') . "),";
            }

            $strValues = trim($strValues, ",");
            $strSql = "INSERT INTO ci_tuition_4leave_detail (service_id, fee_id, tuition_4leave_id, quantity, unit_price, type, service_type) VALUES " . $strValues;
            $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        }

        return $tuition4LeaveId;
    }

    /**
     * Cập nhật thông tin quyết toán của một trẻ ở một trường.
     *
     * @param $tuition4Leave
     * @param $tuitionDetails
     * @throws Exception
     */
    public function updateTuition4Leave($tuition4Leave, $tuitionDetails)
    {
        global $db, $user, $date;

        //Nhập thông tin học phí của trẻ
        $strSql = sprintf("UPDATE ci_tuition_4leave SET attendance_count = %s, final_amount = %s, debt_amount = %s, total_deduction = %s, paid_amount = %s, cashier_id = %s, updated_at = %s WHERE tuition_4leave_id = %s",
                            secure($tuition4Leave['attendance_count'], 'int'), secure($tuition4Leave['final_amount'], 'int'), secure($tuition4Leave['debt_amount'], 'int'), secure($tuition4Leave['total_deduction'], 'int'),
                            secure($tuition4Leave['paid_amount'], 'int'), secure($user->_data['user_id'], 'int'), secure($date), secure($tuition4Leave['tuition_4leave_id'], 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        //Xóa tất cả trong bảng detail.
        $strSql = sprintf("DELETE FROM ci_tuition_4leave_detail WHERE tuition_4leave_id = %s", secure($tuition4Leave['tuition_4leave_id'], 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        if (count($tuitionDetails) > 0) {
            $strValues = "";
            foreach ($tuitionDetails as $tuitionDetail) {
                //service_id, fee_id, tuition_4leave_id, quantity, unit_price, type, service_type
                $strValues .= "(" . secure($tuitionDetail['service_id'], 'int') . "," . secure($tuitionDetail['fee_id'], 'int') . "," . secure($tuition4Leave['tuition_4leave_id'], 'int') . ","
                    . secure($tuitionDetail['quantity'], 'int') . "," . secure($tuitionDetail['unit_price'], 'int') . "," . secure($tuitionDetail['type'], 'int') . ","
                    . secure($tuitionDetail['service_type'], 'int') . "),";
            }

            $strValues = trim($strValues, ",");
            $strSql = "INSERT INTO ci_tuition_4leave_detail (service_id, fee_id, tuition_4leave_id, quantity, unit_price, type, service_type) VALUES " . $strValues;
            $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        }
    }

    /**
     * Cập nhật thời gian notify gần nhất
     *
     * @param $tuitionChildId
     * @throws Exception
     */
    public function updateNotifyTime($tuitionChildId)
    {
        global $db, $date;

        $strSql = sprintf("UPDATE ci_tuition_child SET last_notified_at = %s WHERE tuition_child_id = %s", secure($date), secure($tuitionChildId, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Cập nhật thông tin học phí khi chuyển lớp. Áp dụng với trẻ sử dụng học phí đặc biệt (chỉ dùng cho vài cháu).
     *
     * @param $childId
     * @param $oldClassId
     * @param $newClassId
     * @throws Exception
     */
    function updateTuition4MoveClass($childId, $oldClassId, $newClassId)
    {
        global $db;
        //QuanND: code dưới đây chú nào comment của anh ấy nhỉ?
//        $strSql = sprintf("UPDATE ci_fee_child SET class_id = %s WHERE child_id = %s AND class_id = %s", secure($newClassId, 'int'), secure($childId, 'int'), secure($oldClassId, 'int'));
//        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Cập nhật danh sách tuition_child
     *
     * @param $tuitionChild
     * @param $tuitionDetails
     * @throws Exception
     */
    public function updateTuitionChild($tuitionChild, $tuitionDetails)
    {
        global $db;
        //Cập nhật thông tin học phí của trẻ
        $strSql = sprintf("UPDATE ci_tuition_child SET total_amount = %s, debt_amount = %s, description = %s WHERE tuition_child_id = %s",
                            secure($tuitionChild['total_amount'], 'int'), secure($tuitionChild['debt_amount'], 'int'), secure($tuitionChild['description']), secure($tuitionChild['tuition_child_id'], 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        //Xóa tất cả tuition_detail của tuition_child này
        $strSql = sprintf("DELETE FROM ci_tuition_detail WHERE tuition_child_id = %s", secure($tuitionChild['tuition_child_id'], 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        if (count($tuitionDetails) > 0) {
            //Nhập danh sách các khoản phí cấu thành nên học phí vào hệ thống
            $strValues = "";
            foreach ($tuitionDetails as $tuitionDetail) {
                //service_id, fee_id, tuition_child_id, quantity, unit_price, type, service_type, unit_price_deduction, quantity_deduction
                $strValues .= "(" . secure($tuitionDetail['service_id'], 'int') . "," . secure($tuitionDetail['fee_id'], 'int') . "," . secure($tuitionChild['tuition_child_id'], 'int') . ","
                    . secure($tuitionDetail['quantity'], 'int') . "," . secure($tuitionDetail['unit_price'], 'int') . "," . secure($tuitionDetail['type'], 'int') . ","
                    . secure($tuitionDetail['service_type'], 'int') . "," . secure($tuitionDetail['unit_price_deduction'], 'int') . "," . secure($tuitionDetail['quantity_deduction'], 'int') .  "),";
            }

            $strValues = trim($strValues, ",");
            $strSql = "INSERT INTO ci_tuition_detail (service_id, fee_id, tuition_child_id, quantity, unit_price, type, service_type, unit_price_deduction, quantity_deduction) VALUES " . $strValues;
            $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        }
    }

    /**
     * Lấy ra danh sách các học phí của 1 loại của một trường.
     *
     * @param $school_id
     * @param int $type
     * @return array
     * @throws Exception
     */
    public function getSchoolTuitions($school_id, $type = TUITION_TYPE_MONTHLY)
    {
        global $db;

        $strSql = sprintf("SELECT T.*, G.group_title FROM ci_tuition T
                            INNER JOIN groups G ON T.school_id = %s AND T.type = %s AND T.class_id = G.group_id
                            ORDER BY T.month DESC, T.status ASC", secure($school_id, 'int'), secure($type, 'int'));
        $tuitions = array();
        $get_tuitions = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_tuitions->num_rows > 0) {
            $index = 1;
            while ($tuition = $get_tuitions->fetch_assoc()) {
                $tuition['index'] = $index++;
                $tuition['date'] = date('m/Y');
                $tuition['month'] = toSysMMYYYY($tuition['month']);
                $tuitions[] = $tuition;
            }
        }
        return $tuitions;
    }

    /**
     * Lấy ra danh sách trẻ chưa nộp học phí tháng để nhắc nhở phụ huynh đóng tiền.
     *
     * @param $school_id
     * @param $interval
     * @return array
     * @throws Exception
     */
    public function getUnpaidSchoolTuitions($school_id, $interval)
    {
        global $db, $date;

        $strSql = sprintf("SELECT T.tuition_id, T.month, TC.* FROM ci_tuition T INNER JOIN ci_tuition_child TC
                            ON (DATEDIFF(T.month, '2018-04-01') >= 0) AND T.school_id = %s AND T.tuition_id = TC.tuition_id AND TC.status = 0 AND TC.total_amount > 0
                            AND (ADDDATE(TC.last_notified_at, INTERVAL %s DAY) < %s) AND T.is_notified = 1", secure($school_id, 'int'), secure($interval, 'int'), secure($date));
        $tuitions = array();
        $get_tuitions = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_tuitions->num_rows > 0) {
            while ($tuition = $get_tuitions->fetch_assoc()) {
                $tuition['month'] = toSysMMYYYY($tuition['month']);
                $tuitions[] = $tuition;
            }
        }
        return $tuitions;
    }

    /**
     * Lấy tất cả học phí của một lớp
     *
     * @param $class_id
     * @param int $type
     * @return array
     */
    public function getClassTuitions($class_id, $type = TUITION_TYPE_MONTHLY)
    {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_tuition WHERE class_id = %s AND is_notified = 1 AND type = %s ORDER BY status ASC, month DESC", secure($class_id, 'int'), secure($type, 'int'));
        $tuitions = array();
        $get_tuitions = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_tuitions->num_rows > 0) {
            while ($tuition = $get_tuitions->fetch_assoc()) {
                $tuition['month'] = toSysMMYYYY($tuition['month']);
                $tuitions[] = $tuition;
            }
        }
        return $tuitions;
    }

    /**
     * Lấy ra thông tin chi tiết của học phí
     *
     * @param $tuitionId
     * @return array|null
     */
    public function getTuition($tuitionId)
    {
        global $db;

        $strSql = sprintf("SELECT T.*, G.group_title FROM ci_tuition T
                            INNER JOIN groups G ON T.tuition_id = %s AND T.class_id = G.group_id", secure($tuitionId, 'int'));
        $tuition = null;
        $get_tuition = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_tuition->num_rows > 0) {
            $tuition = $get_tuition->fetch_assoc();
            $tuition['month'] = toSysMMYYYY($tuition['month']);

            //Lấy ra danh sách tuition_child
            $tuition['tuition_child'] = $this->getTuitionChild4Detail($tuitionId, $tuition['school_id'], $tuition['class_id']);
        }
        return $tuition;
    }

    /**
     * Lấy ra học phí chi tiết của 1 trẻ trong một tháng. Dùng cho export ra Excel.
     *
     * @param $tuitionId
     * @param $childId
     * @return array|null
     * @throws Exception
     */
    public function getTuition4ChildInMonth($tuitionId, $childId)
    {
        global $db;

        $strSql = sprintf("SELECT T.tuition_id, T.description, T.month, G.group_title FROM ci_tuition T
                            INNER JOIN groups G ON T.tuition_id = %s AND T.class_id = G.group_id", secure($tuitionId, 'int'));
        $tuition = null;
        $get_tuition = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_tuition->num_rows > 0) {
            $tuition = $get_tuition->fetch_assoc();
            $tuition['month'] = toSysMMYYYY($tuition['month']);

            $strSql = sprintf("SELECT TC.*, C.child_name FROM ci_tuition_child TC INNER JOIN ci_child C ON TC.child_id = C.child_id AND TC.tuition_id = %s AND TC.child_id = %s",
                                secure($tuitionId, 'int'), secure($childId, 'int'));

            $get_tuition_child = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
            if ($get_tuition_child->num_rows > 0) {
                $tuitionChild = $get_tuition_child->fetch_assoc();
                $tuitionChild['pre_month'] = toSysMMYYYY($tuitionChild['pre_month']);

                $tuitionChild['tuition_detail'] = $this->getTuitionDetail4Detail($tuitionChild['tuition_child_id']);

                $tuition['tuition_child'] = $tuitionChild;
            }
        }
        return $tuition;
    }

    /**
     * Lấy ra thông tin chi tiết học phí của trẻ. Hiển thị trên màn hình phụ huynh.
     *
     * @param $tuitionId
     * @param $childId
     * @return array|null
     * @throws Exception
     */
    public function getTuitionDetailOfChildInMonth($tuitionId, $childId)
    {
        global $db;

        $strSql = sprintf("SELECT T.month, TC.* FROM ci_tuition T INNER JOIN ci_tuition_child TC ON T.tuition_id = TC.tuition_id AND TC.tuition_id = %s AND TC.child_id = %s"
                            , secure($tuitionId, 'int'), secure($childId, 'int'));
        $tuition = null;
        $get_tuition = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_tuition->num_rows > 0) {
            $tuition = $get_tuition->fetch_assoc();
            $tuition['month'] = toSysMMYYYY($tuition['month']);
            $tuition['pre_month'] = toSysMMYYYY($tuition['pre_month']);

            $tuition['tuition_detail'] = $this->getTuitionDetail4Detail($tuition['tuition_child_id']);
        }
        return $tuition;
    }

    /**
     * Lấy ra thông tin chi tiết học phí của trẻ theo tuition_child_id. Giống hàm getTuitionDetailOfChildInMonth nhưng khác đầu vào.
     *
     * @param $tuitionChildId
     * @return array|null
     * @throws Exception
     */
    public function getTuitionDetailOfChildInMonthByTuitionChildId($tuitionChildId)
    {
        global $db;

        $strSql = sprintf("SELECT T.month, TC.* FROM ci_tuition T INNER JOIN ci_tuition_child TC ON T.tuition_id = TC.tuition_id AND TC.tuition_child_id = %s", secure($tuitionChildId, 'int'));
        $tuition = null;
        $get_tuition = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_tuition->num_rows > 0) {
            $tuition = $get_tuition->fetch_assoc();
            $tuition['month'] = toSysMMYYYY($tuition['month']);
            $tuition['pre_month'] = toSysMMYYYY($tuition['pre_month']);

            $tuition['tuition_detail'] = $this->getTuitionDetail4Detail($tuitionChildId);
        }
        return $tuition;
    }

    /**
     * Lấy ra lịch sử sử dụng học phí, dịch vụ của một học phí.
     *
     * @param $tuitionId
     * @return array|null
     * @throws Exception
     */
    public function getTuitionHistory($tuitionId)
    {
        global $db;

        $strSql = sprintf("SELECT T.*, G.group_title FROM ci_tuition T
                            INNER JOIN groups G ON T.tuition_id = %s AND T.class_id = G.group_id", secure($tuitionId, 'int'));
        $tuition = null;
        $get_tuition = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_tuition->num_rows > 0) {
            $tuition = $get_tuition->fetch_assoc();
            $tuition['month'] = toSysMMYYYY($tuition['month']);

            //Lấy ra danh sách tuition_child
            $tuitionChildren = $this->getTuitionChild4History($tuitionId, $tuition['school_id']);
            $tuition['tuition_child'] = $tuitionChildren['tuition_child'];
            $tuition['pre_month'] = $tuitionChildren['pre_month'];
            $tuition['class_total'] = $tuitionChildren['class_total']; //Gán ra ngoài để truy cập cho dễ (hợp logic hơn).
        }
        return $tuition;
    }

    /**
     * Lấy ra lịch sử sử dụng phí/dịch vụ của một trẻ. Hiển thị trên màn hình phụ huynh.
     *
     * @param $tuitionId
     * @param $childId
     * @return array|null
     * @throws Exception
     */
    public function getTuitionChildHistory($tuitionId, $childId)
    {
        global $db;

        $strSql = sprintf("SELECT T.month, T.school_id, T.class_id, TC.* FROM ci_tuition T INNER JOIN ci_tuition_child TC ON T.tuition_id = TC.tuition_id AND TC.tuition_id = %s AND TC.child_id = %s"
            , secure($tuitionId, 'int'), secure($childId, 'int'));
        $tuition = null;
        $get_tuition = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_tuition->num_rows > 0) {
            $tuition = $get_tuition->fetch_assoc();
            $tuition['month'] = toSysMMYYYY($tuition['month']);
            $preMonth = $tuition['pre_month'];
            $tuition['pre_month'] = toSysMMYYYY($tuition['pre_month']);

            $attendanceCount = 0;
            if (!empty($tuition['pre_month'])) {
                $attendanceCount = $this->getChildAttendanceCount($childId, $tuition['pre_month']);
            }

            if ($attendanceCount > 0) {
                // Lấy tuition_child_id của tháng trước
                $tuitionChildIdPre = $this->getTuitionChildId($tuition['child_id'], $preMonth);

                //Lấy ra danh sách tuition_detail
                $tuition['tuition_detail'] = $this->getTuitionDetail4History($tuitionChildIdPre, $attendanceCount, $tuition['tuition_child_id']);
                $tuition['child_total'] = $tuition['tuition_detail']['child_total']; //Gán ra ngoài cho tiện truy cập (và đúng logic hơn).
            } else {
                $tuition['tuition_detail'] = array();
                $tuition['child_total'] = 0;
            }
        }
        return $tuition;
    }

    /**
     * Chỉ lấy ra thông tin tuition (trong bảng ci_tuition).
     *
     * @param $tuitionId
     * @return array|null
     * @throws Exception
     */
    public function getTuitionOnly($tuitionId)
    {
        global $db;

        $strSql = sprintf("SELECT T.*, G.group_title FROM ci_tuition T
                            INNER JOIN groups G ON T.tuition_id = %s AND T.class_id = G.group_id", secure($tuitionId, 'int'));
        $tuition = null;
        $get_tuition = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_tuition->num_rows > 0) {
            $tuition = $get_tuition->fetch_assoc();
            $tuition['month'] = toSysMMYYYY($tuition['month']);
        }
        return $tuition;
    }

    /**
     * Cập nhật dữ liệu khi quản lý xác nhận thông tin trả tiền của phụ huynh
     *
     * @param $tuitionId
     * @param $tuitionChildId
     * @param $payAmount
     * @return int
     * @throws Exception
     */
    public function confirmTuitionChild($tuitionId, $tuitionChildId, $payAmount)
    {
        global $db, $date, $user;

        $strSql = sprintf("UPDATE ci_tuition_child SET status = %s, paid_at = %s, paid_amount = %s, cashier_id = %s WHERE tuition_child_id = %s AND status != %s", TUITION_CHILD_CONFIRMED, secure($date),
            secure($payAmount, 'int'), secure($user->_data['user_id'], 'int'), secure($tuitionChildId, 'int'), TUITION_CHILD_CONFIRMED);

        $results = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        $affectedRows = $db->affected_rows;
        if($affectedRows == 1) {
            //Cập nhật thông tin TUITION
            $strSql = sprintf("UPDATE ci_tuition SET paid_count = paid_count + 1, paid_amount = paid_amount + %s WHERE tuition_id = %s", secure($payAmount, 'int'), secure($tuitionId, 'int'));
            $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        }

        return $affectedRows;
    }

    /**
     * Cập nhật trạng thái của tuition_child
     *
     * @param $tuitionChildId
     * @param $status
     * @return int
     * @throws Exception
     */
    public function updateTuitionChildStatus($tuitionChildId, $status)
    {
        global $db;
        $strSql = sprintf("SELECT status, tuition_id, paid_amount FROM ci_tuition_child WHERE tuition_child_id = %s", secure($tuitionChildId, 'int'));
        $get_status = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $tuitionChild = null;
        if ($get_status->num_rows > 0) {
            $tuitionChild = $get_status->fetch_assoc();
        } else {
            return 0;
        }
        if ($status == $tuitionChild['status']) return 0;

        $strSql = sprintf("UPDATE ci_tuition_child SET status = %s WHERE tuition_child_id = %s", secure($status, 'int'), secure($tuitionChildId, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        if ($tuitionChild['status'] == TUITION_CHILD_CONFIRMED && $status != TUITION_CHILD_CONFIRMED) {
            //Cập nhật thông tin TUITION
            $strSql = sprintf("UPDATE ci_tuition SET paid_count = paid_count - 1, paid_amount = paid_amount - %s WHERE tuition_id = %s", secure($tuitionChild['paid_amount'], 'int'), secure($tuitionChild['tuition_id'], 'int'));
            $db->query($strSql) or _error(SQL_ERROR_THROWEN);

            $strSql = sprintf("UPDATE ci_tuition_child SET paid_amount = 0 WHERE tuition_child_id = %s", secure($tuitionChildId, 'int'));
            $db->query($strSql) or _error(SQL_ERROR_THROWEN);
            // Cập nhật thông tin tuition Child

        }

        return 1;
    }

    /**
     * Cập nhật trạng thái thông báo của học phí
     *
     * @param $tuitionId
     * @param $status
     * @throws Exception
     */
    public function updateNotifyStatus($tuitionId, $status)
    {
        global $db;

        $strSql = sprintf("UPDATE ci_tuition SET is_notified = %s WHERE tuition_id = %s", secure($status, 'int'), secure($tuitionId, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Lấy ra danh sách tuition_child của một tuition. Phục vụ hàm view detail.
     *
     * @param $tuitionId
     * @param $schoolId
     * @param $classId
     * @return array
     * @throws Exception
     */
    private function getTuitionChild4Detail($tuitionId, $schoolId, $classId)
    {
        global $db;

        $strSql = sprintf("SELECT TC.*, C.child_name, C.birthday FROM ci_tuition_child TC
                            INNER JOIN ci_child C ON TC.child_id = C.child_id AND TC.tuition_id = %s
                            /*INNER JOIN ci_school_child SC ON TC.child_id = SC.child_id AND SC.school_id = %s*/
                            ORDER BY C.name_for_sort ASC",
            secure($tuitionId, 'int'), secure($schoolId, 'int'));
        $tuitionChildren = array();
        $get_tuition_children = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_tuition_children->num_rows > 0) {
            while ($tuitionChild = $get_tuition_children->fetch_assoc()) {
                $tuitionChild['pre_month'] = toSysMMYYYY($tuitionChild['pre_month']);
                $today = date('Y-m-d');
                $child_month = abs(strtotime($today) - strtotime($tuitionChild['birthday']));
                $tuitionChild['month'] = floor($child_month / (30*60*60*24));
                if (!empty($tuitionChild['pre_month'])) {
                    $attendances = $this->getChildAttendanceInMonth($tuitionChild['child_id'], $tuitionChild['pre_month']);
                    $attendanceCount = 0;
                    foreach ($attendances as $attendance) {
                        if (($attendance['status'] == ATTENDANCE_PRESENT) || ($attendance['status'] == ATTENDANCE_COME_LATE) || ($attendance['status'] == ATTENDANCE_ABSENCE_NO_REASON)) {
                            $attendanceCount++;
                        }
                    }
                    $tuitionChild['attendance_count'] = $attendanceCount;
                    $tuitionChild['attendances'] = $attendances;
                } else {
                    $tuitionChild['attendance_count'] = 0;
                    $tuitionChild['attendances'] = array();
                }

                //Lấy ra danh sách tuition_detail
                $tuitionChild['tuition_detail'] = $this->getTuitionDetail4Detail($tuitionChild['tuition_child_id']);

                $tuitionChildren[] = $tuitionChild;
            }
        }
        return $tuitionChildren;
    }

    /**
     * Lấy ra lịch sử sử dịch học phí/dịch vụ của một trẻ trong một lần tính học phí.
     * Nếu học phí tháng 2 thì lấy ra lịch sử sử dụng tháng 9.
     *
     * @param $tuitionId
     * @param $schoolId
     * @return array
     * @throws Exception
     */
    private function getTuitionChild4History($tuitionId, $schoolId)
    {
        global $db;

        $strSql = sprintf("SELECT TC.*, C.child_name, C.birthday, SC.class_id FROM ci_tuition_child TC
                            INNER JOIN ci_child C ON TC.child_id = C.child_id AND TC.tuition_id = %s
                            INNER JOIN ci_school_child SC ON TC.child_id = SC.child_id AND SC.school_id = %s
                            ORDER BY C.name_for_sort ASC",
            secure($tuitionId, 'int'), secure($schoolId, 'int'));

        $tuitionChildren = array();
        $classTotal = 0; //Tổng sử dụng học phí/dịch vụ của cả lớp.
        $get_tuition_children = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        while ($tuitionChild = $get_tuition_children->fetch_assoc()) {
            $tuitionChild['birthday'] = toSysDate($tuitionChild['birthday']);
            $preMonth = $tuitionChild['pre_month'];
            $tuitionChild['pre_month'] = toSysMMYYYY($tuitionChild['pre_month']);

            $attendanceCount = 0;
            if (!is_empty($tuitionChild['pre_month'])) {
                $attendances = $this->getChildAttendanceInMonth($tuitionChild['child_id'], $tuitionChild['pre_month']);
                foreach ($attendances as $attendance) {
                    if (($attendance['status'] == ATTENDANCE_PRESENT) || ($attendance['status'] == ATTENDANCE_COME_LATE) || ($attendance['status'] == ATTENDANCE_ABSENCE_NO_REASON)) {
                        $attendanceCount++;
                    }
                }
                $tuitionChild['attendance_count'] = $attendanceCount;
                $tuitionChild['attendances'] = $attendances;
            } else {
                $tuitionChild['attendance_count'] = 0;
                $tuitionChild['attendances'] = array();
            }

            if ($attendanceCount > 0) {

                // Lấy tuition_child_id của tháng trước
                $tuitionChildIdPre = $this->getTuitionChildId($tuitionChild['child_id'],$preMonth);
                //Lấy ra danh sách tuition_detail
                $tuitionChild['tuition_detail'] = $this->getTuitionDetail4History($tuitionChildIdPre, $attendanceCount, $tuitionChild['tuition_child_id']);
                $tuitionChild['child_total'] = $tuitionChild['tuition_detail']['child_total']; //Gán ra ngoài cho tiện truy cập (và đúng logic hơn).
                $classTotal = $classTotal + $tuitionChild['child_total'];
            } else {
                $tuitionChild['tuition_detail'] = array();
                $tuitionChild['child_total'] = 0;
            }

            $tuitionChildren[] = $tuitionChild;
        }

        $data = array();
        $data['tuition_child'] = $tuitionChildren;
        $data['pre_month'] = $tuitionChildren[0]['pre_month'];
        $data['class_total'] = $classTotal;

        return $data;
    }

    /**
     * Lấy danh sách tuition_detail của một trẻ. Phục vụ hàm hiển thị chi tiết.
     *
     * @param $tuitionChildId
     * @return array
     * @throws Exception
     */
    private function getTuitionDetail4Detail($tuitionChildId)
    {
        global $db;

        $strSql = sprintf("SELECT TD.*,F.type AS fee_type, F.level, F.fee_name, S.service_name FROM ci_tuition_detail TD
                            LEFT JOIN ci_fee F ON TD.fee_id = F.fee_id
                            LEFT JOIN ci_service S ON TD.service_id = S.service_id
                            WHERE TD.tuition_child_id = %s ORDER BY TD.type ASC, TD.service_type ASC", secure($tuitionChildId, 'int'));
        $tuitionDetails = array();
        $get_tds = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        while ($td = $get_tds->fetch_assoc()) {
            $tuitionDetails[] = $td;
        }

        return $tuitionDetails;
    }

    /**
     * Lấy danh sách tuition_detail của một trẻ. Phục vụ hàm hiển thị chi tiết.
     *
     * @param $tuitionChildId
     * @return array
     * @throws Exception
     */
    public function getTuitionDetail4DetailPub($tuitionChildId)
    {
        global $db;

        $strSql = sprintf("SELECT TD.service_id, TD.fee_id, TD.quantity, TD.unit_price, unit_price_deduction, TD.type, TD.service_type, TD.quantity_deduction FROM ci_tuition_detail TD
                            LEFT JOIN ci_fee F ON TD.fee_id = F.fee_id
                            LEFT JOIN ci_service S ON TD.service_id = S.service_id
                            WHERE TD.tuition_child_id = %s ORDER BY TD.type ASC, TD.service_type ASC", secure($tuitionChildId, 'int'));
        $tuitionDetails = array();
        $get_tds = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        while ($td = $get_tds->fetch_assoc()) {
            $tuitionDetails[] = $td;
        }

        return $tuitionDetails;
    }

    /**
     * Lấy ra lịch sử sử dụng học phí/dịch vụ chi tiết của trẻ.
     *
     * @param $tuitionChildId
     * @param $attendanceCount
     * @return array
     * @throws Exception
     */
    private function getTuitionDetail4History($tuitionChildIdPre, $attendanceCount, $tuitionChildId)
    {
        global $db;

        $strSql = sprintf("SELECT TD.*,F.type AS fee_type, F.fee_name, S.service_name FROM ci_tuition_detail TD
                            LEFT JOIN ci_fee F ON TD.fee_id = F.fee_id
                            LEFT JOIN ci_service S ON TD.service_id = S.service_id
                            WHERE ((TD.tuition_child_id = %s AND TD.service_type != 3) OR (TD.tuition_child_id = %s AND (TD.service_type = 3 OR TD.type = 2))) ORDER BY TD.type ASC, TD.service_type ASC", secure($tuitionChildIdPre, 'int'), secure($tuitionChildId, 'int'));
        $feeTotal = 0;
        $serviceTotal = 0;
        $cbServiceTotal = 0;

        $tuitionDetail = array();
        $fees = array();
        $services = array();
        $countBasedServices = array();
        $get_tds = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        while ($td = $get_tds->fetch_assoc()) {
            if ($td['type'] == TUITION_DETAIL_FEE) {
                //Thông tin các loại phí
                if (($td['quantity'] > 0) && ($td['unit_price'] > 0)) {
                    $fee = array();
                    $fee['fee_id'] = $td['fee_id'];
                    $fee['fee_type'] = $td['fee_type'];
                    $fee['fee_name'] = $td['fee_name'];
                    if ($fee['fee_type'] == FEE_TYPE_MONTHLY) {
                        $fee['quantity'] = 1;
                    } else {
                        $fee['quantity'] = $attendanceCount;
                    }
                    $fee['unit_price'] = $td['unit_price'];
                    $fee['to_money'] = $fee['quantity'] * $fee['unit_price'];

                    $fees[] = $fee;
                    $feeTotal = $feeTotal + $fee['to_money'];
                }
            } else if ($td['type'] == TUITION_DETAIL_SERVICE) {
                //Thông tin các loại dịch vụ
                if (($td['quantity'] > 0) && ($td['unit_price'] > 0)) {
                    $service = array();
                    $service['service_id'] = $td['service_id'];
                    $service['service_name'] = $td['service_name'];
                    $service['service_type'] = $td['service_type'];
                    if ($service['service_type'] == SERVICE_TYPE_MONTHLY) {
                        $service['quantity'] = 1;
                    } else if ($service['service_type'] == SERVICE_TYPE_DAILY) {
                        $service['quantity'] = $attendanceCount;
                    } else {
                        $service['quantity'] = $td['quantity'];
                    }
                    $service['unit_price'] = $td['unit_price'];
                    $service['to_money'] = $service['quantity'] * $service['unit_price'];

                    if ($td['service_type'] == SERVICE_TYPE_COUNT_BASED) {
                        $countBasedServices[] = $service;
                        $cbServiceTotal = $cbServiceTotal + $service['to_money'];
                    } else {
                        $services[] = $service;
                        $serviceTotal = $serviceTotal + $service['to_money'];
                    }
                }
            } else {
                if ($td['quantity']*$td['unit_price'] > 0) {
                    $service = array();
                    $service['service_name'] = __("Late PickUp");
                    $service['service_type'] = $td['service_type'];
                    $service['quantity'] = 1;
                    $service['unit_price'] = $td['unit_price'];
                    $service['to_money'] = $service['quantity'] * $service['unit_price'];
                    $service['service_type'] = SERVICE_TYPE_COUNT_BASED;

                    $countBasedServices[] = $service;
                    $cbServiceTotal = $cbServiceTotal + $service['to_money'];
                }
            }
        }
        $tuitionDetail['fees'] = $fees;
        $tuitionDetail['services'] = $services;
        $tuitionDetail['count_based_services'] = $countBasedServices;
        $tuitionDetail['child_total'] = $feeTotal + $serviceTotal + $cbServiceTotal;

        return $tuitionDetail;
    }

    /**
     * Lấy ra thông tin sử dụng học phí/dịch vụ tháng hiện tại để quyết toán => cho trẻ nghỉ học.
     *
     * @param $schoolId
     * @param $childId
     * @param $leaveDate
     * @return array
     * @throws Exception
     */
    public function getTuition4Leave($schoolId, $childId, $leaveDate = null)
    {
        global $db, $date;
        if (is_null($leaveDate) || ($leaveDate == '')) {
            $leaveDate = toSysDate($date);
        }
        //Lấy ra tháng tính học phí cuối cùng của trẻ
        $lastTuitionMonth = $this->getLastTuitionMonthOfChild($schoolId, $childId, $leaveDate);
        $firstDateOfMonth = toDBDate("01/" . $lastTuitionMonth);

        $results = array();

        //1. Thông tin học phí tháng cuối cùng.
        $strSql = sprintf("SELECT TC.tuition_child_id, TC.tuition_id, TC.total_amount, TC.paid_amount, TC.total_deduction, TC.debt_amount, TC.status, T.class_id FROM ci_tuition_child TC
                            INNER JOIN ci_tuition T ON TC.tuition_id = T.tuition_id AND T.school_id = %s AND T.month = %s AND TC.child_id = %s",
            secure($schoolId, 'int'), secure($firstDateOfMonth), secure($childId, 'int'));

        $get_tc = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $tuitionChild = null;
        $deductionTotal = 0;
        if ($get_tc->num_rows > 0) {
            $tuitionChild = $get_tc->fetch_assoc();

            //Lấy ra tổng giảm trừ tháng trước của trẻ
            $strSql = sprintf("SELECT quantity_deduction, unit_price_deduction FROM ci_tuition_detail WHERE tuition_child_id = %s", secure($tuitionChild['tuition_child_id'], 'int'));
            $get_deduction = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
            while ($detail = $get_deduction->fetch_assoc()) {
                $deductionTotal = $deductionTotal + $detail['quantity_deduction'] * $detail['unit_price_deduction'];
            }
        }

        //2. Lấy ra số ngày đi học tháng hiện tại.
        $attendanceCount = $this->getChildAttendanceCountInDuration($childId, "01/".$lastTuitionMonth, $leaveDate);
        $results['attendance_count'] = $attendanceCount;

        //3. Lấy ra danh sách phí mà trẻ phải chịu
        $strSql = sprintf("SELECT class_id FROM ci_school_child WHERE school_id = %s AND child_id = %s", secure($schoolId, 'int'), secure($childId, 'int'));
        $get_class = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $classId = 0;
        if ($get_class->num_rows > 0) {
            $classId = $get_class->fetch_assoc()['class_id'];
        }
        $feesOfChild = $this->getFeeOfOneChildInMonth($classId, $schoolId, $childId);
        $feeTotal = 0;
        $serviceTotal = 0;
        $cbServiceTotal = 0;

        $fees = array();
        $services = array();
        $countBasedServices = array();
        foreach ($feesOfChild as $fee) {
            if ($fee['fee_type'] == FEE_TYPE_MONTHLY) {
                $fee['quantity'] = 1;
            } else {
                $fee['quantity'] = $attendanceCount;
            }
            $fee['to_money'] = $fee['quantity'] * $fee['unit_price'];

            $fees[] = $fee;
            $feeTotal = $feeTotal + $fee['to_money'];
        }

        //4. Lấy ra danh sách các dịch vụ mà trẻ sử dụng tính theo tháng và số lần điểm danh (bao gồm cả những dịch vụ mà trẻ đã hủy trong tháng)
        $monthlyNDailyServicesOfChild = $this->getMonthlyNDailyServiceOfChildInDuration($schoolId, $childId, "01/".$lastTuitionMonth, $leaveDate);
        foreach ($monthlyNDailyServicesOfChild as $service) {
            $service['service_type'] = $service['type'];
            $service['unit_price'] = $service['fee'];
            unset($service['type']);
            if ($service['service_type'] == SERVICE_TYPE_MONTHLY) {
                $service['quantity'] = 1;
            } else if ($service['service_type'] == SERVICE_TYPE_DAILY) {
                $service['quantity'] = $attendanceCount;
            }
            $service['to_money'] = $service['quantity'] * $service['unit_price'];

            $services[] = $service;
            $serviceTotal = $serviceTotal + $service['to_money'];
        }

        //Dịch vụ theo lần điểm danh
        $usedCountBasedServicesOfChildInMonth = $this->getUsedCountBasedServicesOfChildInDuration($schoolId, $childId, "01/".$lastTuitionMonth, $leaveDate);
        foreach ($usedCountBasedServicesOfChildInMonth as $service) {
            $service['service_type'] = SERVICE_TYPE_COUNT_BASED;
            $service['quantity'] = $service['CNT'];
            $service['unit_price'] = $service['fee'];
            $service['to_money'] = $service['quantity'] * $service['unit_price'];

            $countBasedServices[] = $service;
            $cbServiceTotal = $cbServiceTotal + $service['to_money'];
        }

        //Đón muộn
        $childPickup = $this->getChildPickupInDuration($schoolId, $childId, "01/".$lastTuitionMonth, $leaveDate);
        $service = array();
        $service['service_id'] = 0;
        $service['service_name'] = __("Late PickUp");
        $service['service_type'] = SERVICE_TYPE_COUNT_BASED;
        $service['quantity'] = 1;
        $service['unit_price'] = $childPickup;
        $service['to_money'] = $service['quantity'] * $service['unit_price'];

        $countBasedServices[] = $service;
        $cbServiceTotal = $cbServiceTotal + $service['to_money'];

        $results['fees'] = $fees;
        $results['services'] = $services;
        $results['count_based_services'] = $countBasedServices;
        $results['last_tuition_month'] = $lastTuitionMonth;
        $results['debt_amount'] = (is_null($tuitionChild))? 0: $tuitionChild['debt_amount'];
        $results['total_deduction'] = $deductionTotal;
        $results['paid_amount'] = (is_null($tuitionChild))? 0: $tuitionChild['paid_amount'];
        $results['final_amount'] = $feeTotal + $serviceTotal + $cbServiceTotal + $results['debt_amount'] - $results['total_deduction'] - $results['paid_amount'];

        return $results;
    }

    /**
     * Lấy ra thông tin học phí quyết toán của trẻ trước khi nghỉ học.
     *
     * @param $schoolId
     * @param $childId
     * @return array
     * @throws Exception
     */
    public function getLeaveTuitionDetail($schoolId, $childId)
    {
        global $db, $date;

        //1. Lấy thông tin tổng hợp của học phí quyết toán.
        $strSql = sprintf("SELECT * FROM ci_tuition_4leave WHERE school_id = %s AND child_id = %s", secure($schoolId, 'int'), secure($childId, 'int'));
        $get_tc = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_tc->num_rows == 0) {
            return array();
        }
        $tuition4Leave = $get_tc->fetch_assoc();

        //2. Lấy ra thông tin chi tiết.
        $strSql = sprintf("SELECT TD.*,F.type AS fee_type, F.fee_name, S.service_name FROM ci_tuition_4leave_detail TD
                            LEFT JOIN ci_fee F ON TD.fee_id = F.fee_id
                            LEFT JOIN ci_service S ON TD.service_id = S.service_id
                            WHERE TD.tuition_4leave_id = %s ORDER BY TD.type ASC, TD.service_type ASC", secure($tuition4Leave['tuition_4leave_id'], 'int'));

        $feeTotal = 0;
        $serviceTotal = 0;
        $cbServiceTotal = 0;

        $fees = array();
        $services = array();
        $countBasedServices = array();
        $get_tds = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        while ($td = $get_tds->fetch_assoc()) {
            if ($td['type'] == TUITION_DETAIL_FEE) {
                if (($td['quantity'] * $td['unit_price']) != 0) {
                    $fee = array();
                    $fee['fee_id'] = $td['fee_id'];
                    $fee['fee_type'] = $td['fee_type'];
                    $fee['fee_name'] = $td['fee_name'];
                    $fee['unit_price'] = $td['unit_price'];
                    $fee['quantity'] = $td['quantity'];
                    $fee['to_money'] = $fee['quantity'] * $fee['unit_price'];

                    $fees[] = $fee;
                    $feeTotal = $feeTotal + $fee['to_money'];
                }
            } else if ($td['type'] == TUITION_DETAIL_SERVICE) {
                //Tính toán các khoản DỊCH VỤ đã sử dụng tháng hiện tại
                if (($td['quantity'] * $td['unit_price']) != 0) {
                    $service = array();
                    $service['service_id'] = $td['service_id'];
                    $service['service_name'] = $td['service_name'];
                    $service['service_type'] = $td['service_type'];
                    $service['unit_price'] = $td['unit_price'];
                    $service['quantity'] = $td['quantity'];
                    $service['to_money'] = $service['quantity'] * $service['unit_price'];

                    if ($td['service_type'] == SERVICE_TYPE_COUNT_BASED) {
                        $countBasedServices[] = $service;
                        $cbServiceTotal = $cbServiceTotal + $service['to_money'];
                    } else {
                        $services[] = $service;
                        $serviceTotal = $serviceTotal + $service['to_money'];
                    }
                }
            } else {
                if (($td['quantity'] * $td['unit_price']) != 0) {
                    $service = array();
                    $service['service_id'] = 0;
                    $service['service_name'] = __("Late PickUp");
                    $service['service_type'] = SERVICE_TYPE_COUNT_BASED;
                    $service['quantity'] = 1;
                    $service['unit_price'] = $td['unit_price'];
                    $service['to_money'] = $service['quantity'] * $service['unit_price'];

                    $countBasedServices[] = $service;
                    $cbServiceTotal = $cbServiceTotal + $service['to_money'];
                }
            }
        }
        $tuition4Leave['fees'] = $fees;
        $tuition4Leave['services'] = $services;
        $tuition4Leave['count_based_services'] = $countBasedServices;

        return $tuition4Leave;
    }

    /**
     * Lấy ra danh sách tuition_child của một tuition. Nhưng không lấy thông tin khác liên quan
     *
     * @param $tuitionId
     * @return array
     * @throws Exception
     */
    public function getTuitionChild($tuitionId)
    {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_tuition_child WHERE tuition_id = %s", secure($tuitionId, 'int'));
        $get_tuition_child = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $tuitionChilds = array();
        while ($tuitionChild = $get_tuition_child->fetch_assoc()) {
            $tuitionChilds[] = $tuitionChild;
        }
        return $tuitionChilds;
    }

    /**
     * Cập nhật học phí của lớp khi XÓA học phí của 1 học sinh.
     *
     * @param $oldTuitionId
     * @param $tuitionChild
     * @throws Exception
     */
    public function updateTuitionWhenDeletingChild($tuitionChild)
    {
        global $db;

        $strSql = null;
        if ($tuitionChild['status'] == TUITION_CHILD_CONFIRMED) {
            $strSql = sprintf("UPDATE ci_tuition SET total_amount = total_amount - %s, total_deduction = total_deduction - %s, paid_count = paid_count - 1, paid_amount = paid_amount - %s
                                WHERE tuition_id = %s", secure($tuitionChild['total_amount'], 'int'), secure($tuitionChild['total_deduction'], 'int'),
                                secure($tuitionChild['paid_amount'], 'int'), secure($tuitionChild['tuition_id'], 'int'));
        } else {
            $strSql = sprintf("UPDATE ci_tuition SET total_amount = total_amount - %s, total_deduction = total_deduction - %s WHERE tuition_id = %s",
                secure($tuitionChild['total_amount'], 'int'), secure($tuitionChild['total_deduction'], 'int'), secure($tuitionChild['tuition_id'], 'int'));
        }
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Xóa học phí của một danh sách trẻ.
     *
     * @param $tuitionChildIds
     * @throws Exception
     */
    public function deleteTuitionChildren($tuitionChildIds)
    {
        global $db;
        $strIds = implode(",", $tuitionChildIds);
        $strSql = sprintf("DELETE FROM ci_tuition_detail WHERE tuition_child_id IN (%s)", $strIds);
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        $strSql = sprintf("DELETE FROM ci_tuition_child WHERE tuition_child_id IN (%s)", $strIds);
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Xóa học phí của một trẻ.
     *
     * @param $tuitionChildId
     * @throws Exception
     */
    public function deleteTuitionChild($tuitionChildId)
    {
        global $db;

        $strSql = sprintf("DELETE FROM ci_tuition_detail WHERE tuition_child_id = %s", secure($tuitionChildId, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        $strSql = sprintf("DELETE FROM ci_tuition_child WHERE tuition_child_id = %s", secure($tuitionChildId, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Lấy ra thông tin một tuition_child theo tuition_child_id
     * (phục vụ hiển thị title của notification).
     *
     * @param $tuitionChildId
     * @return array|null
     */
    public function getTuitionChildById($tuitionChildId)
    {
        global $db;

        $strSql = sprintf("SELECT TC.*, C.child_name, T.school_id, T.month FROM ci_tuition_child TC
                            INNER JOIN ci_child C ON TC.tuition_child_id = %s AND TC.child_id = C.child_id
                            INNER JOIN ci_tuition T ON TC.tuition_id = T.tuition_id", secure($tuitionChildId, 'int'));
        $get_tuition_child = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_tuition_child->num_rows > 0) {
            $tuitionChild = $get_tuition_child->fetch_assoc();
            $tuitionChild['month'] = toSysMMYYYY($tuitionChild['month']);

            return $tuitionChild;
        }
        return null;
    }

    /**
     * Lấy ra thông tin duy nhất của học phí một trẻ.
     *
     * @param $tuitionChildId
     * @return array|null
     * @throws Exception
     */
    public function getTuitionChildOnly($tuitionChildId)
    {
        global $db;

        $strSql = sprintf("SELECT TC.*, T.month FROM ci_tuition_child TC INNER JOIN ci_tuition T ON TC.tuition_id = T.tuition_id AND TC.tuition_child_id = %s", secure($tuitionChildId, 'int'));
        $get_tuition_child = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_tuition_child->num_rows > 0) {
            return $get_tuition_child->fetch_assoc();
        }
        return null;
    }

    /**
     * Lấy ra thông tin của học phí một trẻ bao gồm status, total_amount, month.
     *
     * @param $tuitionChildId
     * @return array|null
     * @throws Exception
     */
    public function getTuitionChildInfoForApi($tuitionChildId)
    {
        global $db;

        $strSql = sprintf("SELECT TC.status, TC.total_amount, TC.total_deduction, TC.debt_amount, TC.paid_amount, TC.description, T.month FROM ci_tuition_child TC
                           INNER JOIN ci_tuition T ON TC.tuition_child_id = %s AND TC.tuition_id = T.tuition_id",
                           secure($tuitionChildId, 'int'));
        $get_tuition_child = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_tuition_child->num_rows > 0) {
            return $get_tuition_child->fetch_assoc();
        }
        return null;
    }

    /**
     * Lấy ra danh sách học phí của trẻ. Hiển thị trên màn hình phụ huynh
     *
     * @param $childId
     * @param int $limit
     * @return array
     * @throws Exception
     */
    public function getTuitionsByChild($childId, $limit = 999999)
    {
        global $db;

        $strSql = sprintf("SELECT TC.*, T.month, T.class_id FROM ci_tuition_child TC
                            INNER JOIN ci_tuition T ON TC.tuition_id = T.tuition_id
                            WHERE TC.child_id = %s AND T.is_notified = 1 ORDER BY T.month DESC, TC.status ASC LIMIT %s", secure($childId, 'int'), $limit);
        $tuitions = array();
        $get_tuitions = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_tuitions->num_rows > 0) {
            while ($tuitionChild = $get_tuitions->fetch_assoc()) {
                $tuitionChild['month'] = toSysMMYYYY($tuitionChild['month']);

                $tuitions[] = $tuitionChild;
            }
        }
        return $tuitions;
    }

    /**
     * Xóa thông tin của một tuition.
     *
     * @param $tuitionId
     * @throws Exception
     */
    public function deleteTuition($tuitionId)
    {
        global $db;

        //Xóa trong bảng tuition_detail
        $strSql = sprintf("DELETE FROM ci_tuition_detail WHERE tuition_child_id IN (SELECT tuition_child_id FROM ci_tuition_child
                            WHERE tuition_id = %s)", secure($tuitionId, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        //Xóa trong bảng tuition_child
        $strSql = sprintf("DELETE FROM ci_tuition_child WHERE tuition_id = %s", secure($tuitionId, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        //Xóa thông tin tuition
        $db->query(sprintf("DELETE FROM ci_tuition WHERE tuition_id = %s", secure($tuitionId, 'int'))) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Lây ra danh sách các fees của một trường
     *
     * @return array
     */
    public function getFees($school_id)
    {
        global $db;

        $strSql = sprintf("SELECT F.*, CL.class_level_name, G.group_title FROM ci_fee F
                            LEFT JOIN ci_class_level CL ON F.class_level_id = CL.class_level_id
                            LEFT JOIN groups G ON F.class_id = G.group_id                          
                            WHERE F.school_id = %s ORDER BY F.level ASC, F.type ASC", secure($school_id, 'int'));
        $fees = array();
        $get_fees = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        while ($fee = $get_fees->fetch_assoc()) {
            $fee_id = $fee['fee_id'];
            $strSql = sprintf("SELECT fee_id FROM ci_tuition_detail WHERE fee_id = %s", secure($fee_id, 'int'));
            $getFee = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
            if ($getFee->num_rows > 0) {
                $fee['used'] = 1;
            } else {
                $fee['used'] = 0;
            }

            if ($fee['level'] == CHILD_LEVEL) {
                $strSql = sprintf("SELECT C.child_name, G.group_title FROM ci_fee_child FC
                        INNER JOIN ci_child C ON FC.child_id = C.child_id
                        INNER JOIN ci_class_child CC ON CC.child_id = C.child_id
                        INNER JOIN groups G ON CC.class_id = G.group_id
                        WHERE FC.fee_id = %s", secure($fee['fee_id'], 'int'));

                $get_children = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
                $children = array();
                while ($child = $get_children->fetch_assoc()) {
                    $children[] = $child;
                }
                $fee['children'] = $children;
            }
            $fees[] = $fee;
        }
        return $fees;
    }

    /**
     * Lấy ra danh sách các loại phí của một trường. Chỉ thấy thông tin trong bảng ci_fee, không lấy gì thêm.
     *
     * @param $school_id
     * @param bool|true $status
     * @return array
     * @throws Exception
     */
    public function getOnlyFees($school_id, $status = STATUS_ACTIVE) {
        global $db;
        $strSql = sprintf("SELECT F.*, CL.class_level_name, G.group_title FROM ci_fee F
                            LEFT JOIN ci_class_level CL ON F.class_level_id = CL.class_level_id
                            LEFT JOIN groups G ON F.class_id = G.group_id
                            WHERE F.school_id = %s AND F.status = %s ORDER BY F.level ASC, F.type ASC", secure($school_id, 'int'), secure($status, 'int'));
        $fees = array();
        $get_fees = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        while ($fee = $get_fees->fetch_assoc()) {
            $fees[] = $fee;
        }
        return $fees;
    }

    /**
     * Lấy ra thông tin phí từ danh sách ID cho trước
     *
     * @param $schoolId
     * @param $feeIds
     * @return array
     * @throws Exception
     */
    public function getFeesInList($schoolId, $feeIds) {
        global $db;
        if (isset($feeIds) && (count($feeIds) > 0)) {
            $strFeeIds = implode(",", $feeIds);
            $strSql = sprintf("SELECT * FROM ci_fee WHERE school_id = %s AND status = %s AND fee_id IN (%s)", secure($schoolId, 'int'), secure(STATUS_ACTIVE, 'int'), $strFeeIds);
            $fees = array();
            $get_fees = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
            while ($fee = $get_fees->fetch_assoc()) {
                $fees[] = $fee;
            }
            return $fees;
        }
        return array();
    }

    /**
     * Lấy ra danh sách các loại phí của một trường theo trạng thái truyền vào
     *
     * @param $school_id
     * @param bool|true $status
     * @return array
     * @throws Exception
     */
    public function getFeesOfSchool($school_id, $status = true)
    {
        global $db;

        $strSql = sprintf("SELECT F.*, CL.class_level_name, G.group_title FROM ci_fee F
                            LEFT JOIN ci_class_level CL ON F.class_level_id = CL.class_level_id
                            LEFT JOIN groups G ON F.class_id = G.group_id
                            WHERE F.school_id = %s AND F.status = %s ORDER BY F.level ASC, F.type ASC", secure($school_id, 'int'), secure($status, 'int'));
        $fees = array();
        $get_fees = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        while ($fee = $get_fees->fetch_assoc()) {
            $fee_id = $fee['fee_id'];
            $strSql = sprintf("SELECT fee_id FROM ci_tuition_detail WHERE fee_id = %s", secure($fee_id, 'int'));
            $getFee = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
            if ($getFee->num_rows > 0) {
                $fee['used'] = 1;
            } else {
                $fee['used'] = 0;
            }

            if ($fee['level'] == CHILD_LEVEL) {
                //Nếu là học phí cấp trẻ thì lấy danh sách trẻ áp dụng học phí đó.
                $strSql = sprintf("SELECT C.child_name, G.group_title FROM ci_fee_child FC
                        INNER JOIN ci_child C ON FC.child_id = C.child_id
                        INNER JOIN ci_class_child CC ON CC.child_id = C.child_id
                        INNER JOIN groups G ON CC.class_id = G.group_id
                        WHERE FC.fee_id = %s", secure($fee['fee_id'], 'int'));

                $get_children = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
                $children = array();
                while ($child = $get_children->fetch_assoc()) {
                    $children[] = $child;
                }
                $fee['children'] = $children;

                //Lấy ra danh sách phí được phí trên thay thế.
                $fee_change_ids = trim($fee['fee_change_id'], ",");
                if ($fee_change_ids != "") {
                    $strSql = sprintf("SELECT * FROM ci_fee WHERE fee_id IN (%s)", $fee_change_ids);
                    $get_fee_change = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
                    if ($get_fee_change->num_rows > 0) {
                        $fee_changes = array();
                        while ($fee_change = $get_fee_change->fetch_assoc()) {
                            $fee_changes[] = $fee_change;
                        }
                        $fee['fee_changes'] = $fee_changes;
                    }
                }
            }
            $fees[] = $fee;
        }
        return $fees;
    }

    /**
     * Lấy ra các loại phí của một trẻ (chỉ phí, KHÔNG bao gồm dịch vụ) sẽ phải trả trong tháng. Sẽ bao gồm những loại phí sau:
     * - Phí cấp trường
     * - Phí cấp khối
     * - Phí cấp lớp
     * - Phí cấp trẻ, trong đó trẻ được áp dụng.
     * - LOẠI BỎ NHỮNG PHÍ CẤP TRƯỜNG, KHỐI VÀ LỚP CÓ THỂ ĐÃ ĐƯỢC THAY THẾ
     *
     * @param $class_id
     * @param $school_id
     * @param $child_id
     * @return array
     * @throws Exception
     */
    private function getFeeOfOneChildInMonth($class_id, $school_id, $child_id)
    {
        global $db;

        //1. Lấy ra danh sách các khoản phí trẻ phải chịu riêng trong tháng (Phí cấp TRẺ)
        $levelChildFeeOfChild = $this->getChildLevelFeeOfChild($school_id, $child_id);
        //2. Gộp tất cả phí thay thế lại.
        $replacedFeeIds = array(); //Lưu ID của tất cả phí BỊ thay thế của trẻ
        foreach ($levelChildFeeOfChild as $levelChildFee) {
            $replacedFeeIds = array_merge($replacedFeeIds, explode(",", $levelChildFee['fee_change_id']));
        }
        $replacedFeeIds = array_unique($replacedFeeIds);

        $strSql = sprintf("SELECT F.fee_id, F.fee_name, F.type AS fee_type, F.unit_price, F.level, F.school_id, F.class_level_id, F.class_id FROM ci_fee F
                            WHERE F.status = %s AND ((F.level = %s AND F.school_id = %s)
                              OR (F.level = %s AND F.class_level_id IN 
                              (SELECT class_level_id FROM groups WHERE group_id = %s))
                              OR (F.level = %s AND F.class_id = %s)
                              OR (F.level = %s AND F.fee_id IN (SELECT fee_id FROM ci_fee_child WHERE child_id = %s)))
                            ORDER BY F.type ASC, F.level ASC", FEE_ON, SCHOOL_LEVEL, secure($school_id, 'int'), CLASS_LEVEL_LEVEL, secure($class_id, 'int'),
                                                                CLASS_LEVEL, secure($class_id, 'int'), CHILD_LEVEL, secure($child_id));
        $fees = array();
        $get_fees = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        while ($fee = $get_fees->fetch_assoc()) {
            if (!in_array($fee['fee_id'], $replacedFeeIds)) { //Loại bỏ phí đã bị thay thế
                $fees[] = $fee;
            }
        }
        return $fees;
    }

    /**
     * Lấy ra TẤT CẢ khoản phí ĐÃ TRẢ mà một trẻ trong một tháng cụ thể (thường lấy loại phí đã trả tháng trước). Nó bao gồm:
     * - Phí cấp trường, khối, lớp và cấp trẻ.
     *
     * @param $school_id
     * @param $child_id
     * @param $month
     * @return array
     * @throws Exception
     */
    private function getPaidFeeOfChildInMonth($school_id, $child_id, $month)
    {
        global $db;
        $month = toDBDate("01/" . $month);

        $strSql = sprintf("SELECT TD.*, F.fee_name, F.type AS fee_type, F.unit_price AS current_unit_price, TC.tuition_id FROM ci_tuition_detail TD
                           INNER JOIN ci_tuition_child TC ON TC.tuition_child_id = TD.tuition_child_id AND TC.child_id = %s
                           INNER JOIN ci_tuition T ON T.tuition_id = TC.tuition_id AND T.school_id = %s AND T.month = %s
                           INNER JOIN ci_fee F ON F.fee_id = TD.fee_id AND TD.fee_id > 0
                           ORDER BY TD.tuition_child_id DESC, F.type ASC, F.level ASC", secure($child_id, 'int'), secure($school_id, 'int'), secure($month));
        $fees = array();
        $get_fees = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $feeIds = array();
        if ($get_fees->num_rows > 0) {
            while ($fee = $get_fees->fetch_assoc()) {
                if(!in_array($fee['fee_id'], $feeIds)) {
                    $fees[] = $fee;
                    $feeIds[] = $fee['fee_id'];
                }
            }
        }
        return $fees;
    }

    /**
     * Lấy ra TẤT CẢ khoản phí tính riêng cho trẻ (PHÍ CẤP TRẺ) hàng tháng (để tính học phí hàng tháng cho trẻ):
     * - Phí áp dụng cho trẻ
     *
     * @param $school_id
     * @param $child_id
     * @return array
     * @throws Exception
     */

    private function getChildLevelFeeOfChild($school_id, $child_id)
    {
        global $db;
        $strSql = sprintf("SELECT F.*, FC.child_id FROM ci_fee F
                           INNER JOIN ci_fee_child FC ON F.fee_id = FC.fee_id AND F.school_id = %s AND F.level = %s AND FC.child_id = %s WHERE F.status = %s
                           ORDER BY F.type ASC, F.level ASC", secure($school_id, 'int'), CHILD_LEVEL, secure($child_id, 'int'), FEE_ON);
        $fees = array();
        $get_fees = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        while ($fee = $get_fees->fetch_assoc()) {
            $fees[] = $fee;
        }
        return $fees;
    }

    /**
     * Lấy ra fee theo ID
     *
     * @param $fee_id
     * @return array|null
     * @throws Exception
     */
    public function getFee($fee_id)
    {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_fee WHERE fee_id = %s", secure($fee_id, 'int'));
        $get_fees = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_fees->num_rows > 0) {
            $fee = $get_fees->fetch_assoc();
            if ($fee['level'] == CHILD_LEVEL) {
                $strSql = sprintf("SELECT child_id FROM ci_fee_child WHERE fee_id = %s", secure($fee['fee_id'], 'int'));

                $get_childIds = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
                $childIds = array();
                while ($child = $get_childIds->fetch_assoc()) {
                    $childIds[] = $child['child_id'];
                }
                $fee['child_ids'] = $childIds;
            }
            return $fee;
        }
        return null;
    }

    /**
     * Lấy ra danh sách các phí được thay thế bởi  một phí.
     *
     * @param $feeId
     * @return array
     * @throws Exception
     */
    public function getReplacedFees($feeId)
    {
        global $db;

        $strSql = sprintf("SELECT A.* FROM ci_fee A WHERE A.fee_id IN (SELECT fee_change_id B FROM ci_fee B WHERE B.fee_id = %s)", secure($feeId, 'int'));
        $get_fees = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $results = array();
        while ($fee = $get_fees->fetch_assoc()) {
            $results[] = $fee;
        }
        return $results;
    }

    /**
     * Lấy ra số lượng khối của một trường
     *
     * @param int $school_id
     * @return mixed
     * @throws Exception
     */
    public function getCountFees($school_id)
    {
        global $db;

        $strSql = sprintf("SELECT COUNT(fee_id) AS fee_count FROM ci_fee WHERE school_id = %s", secure($school_id, 'int'));
        $get_count = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        return $get_count->fetch_assoc()['fee_count'];
    }

    // Lấy ra những dịch vụ tính theo lần sử dụng trong tháng
    public function getServiceIdsSLSD($tuitionId)
    {
        global $db;
        $strSql = sprintf("SELECT TD.service_id FROM ci_tuition_detail TD 
                           INNER JOIN ci_tuition_child TC ON TC.tuition_child_id = TD.tuition_child_id 
                           INNER JOIN ci_service S ON S.service_id = TD.service_id AND S.type = %s AND TC.tuition_id = %s AND TD.service_id > '0'", SERVICE_TYPE_COUNT_BASED, secure($tuitionId));
        $get_serviceIdsSLSD = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $result = null;
        while ($service = $get_serviceIdsSLSD->fetch_assoc()) {
            $result[] = $service['service_id'];
        }
        return $result;
    }


    // delete dịch vụ tính theo số lần sử dụng trong tháng trước
    public function deleteServiceIdsSLSD($serviceIdsSLSD, $tuitionIdPrev)
    {
        global $db;
        $strCon = implode(',', $serviceIdsSLSD);
        $strSql = sprintf("DELETE FROM ci_tuition_detail WHERE tuition_child_id IN (SELECT tuition_child_id FROM ci_tuition_child
                            WHERE tuition_id = %s) AND service_id IN (%s)", secure($tuitionIdPrev, 'int'), $strCon);
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    // Xóa sử dụng dịch vụ Đón muộn tháng trước
    public function deleteServicePickup($tuitionIdPrev)
    {
        global $db;

        $strSql = sprintf("DELETE FROM ci_tuition_detail WHERE tuition_child_id IN (SELECT tuition_child_id FROM ci_tuition_child
                           WHERE tuition_id = %s) AND type = %s", secure($tuitionIdPrev, 'int'), secure(LATE_PICKUP_FEE, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

    }

    /**
     * Insert một class level vào trong DB
     *
     * @param array $args
     * @throws Exception
     */
    public function insertFee(array $args = array())
    {
        global $db, $date, $user;
        $this->_validateFeeInput($args);

        if ($args['level'] == CHILD_LEVEL) {
            $fee_change_id = (isset($args['fee_change_id']) && (count($args['fee_change_id']) > 0))? implode(",",$args['fee_change_id']) : '';
            $strSql = sprintf("INSERT INTO ci_fee (fee_name, type, unit_price, level, school_id, class_level_id, fee_change_id, description, status, created_at, created_user_id) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
                secure($args['fee_name']), secure($args['type'], 'int'), secure($args['unit_price'], 'int'),
                secure($args['level'], 'int'), secure($args['school_id'], 'int'), secure($args['class_level_id'], 'int'),
                secure($fee_change_id), secure($args['description']), secure($args['status'], 'int'), secure($date), secure($user->_data['user_id'], 'int'));
            $db->query($strSql) or _error(SQL_ERROR_THROWEN);

            $fee_id = $db->insert_id;

            $strValues = "";
            foreach ($args['child_ids'] as $child_id) {
                $strValues .= "(" . secure($fee_id, 'int') . "," . secure($child_id, 'int') . "),";
            }
            $strValues = trim($strValues, ",");
            if (!is_empty($strValues)) {
                $strSql = sprintf("INSERT INTO ci_fee_child (fee_id, child_id) VALUES " . $strValues);
                $db->query($strSql) or _error(SQL_ERROR_THROWEN);
            }
        } else {
            $strSql = sprintf("INSERT INTO ci_fee (fee_name, type, unit_price, level, school_id, class_level_id, class_id, description, status, created_at, created_user_id) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
                secure($args['fee_name']), secure($args['type'], 'int'), secure($args['unit_price'], 'int'),
                secure($args['level'], 'int'), secure($args['school_id'], 'int'), secure($args['class_level_id'], 'int'),
                secure($args['class_id'], 'int'), secure($args['description']), secure($args['status'], 'int'), secure($date), secure($user->_data['user_id'], 'int'));
            $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        }
    }

    /**
     * Kiểm tra điều kiện đầu vào.
     *
     * @param array $args
     * @throws Exception
     */
    private function _validateFeeInput(array $args = array())
    {
        if (!isset($args['fee_name'])) {
            throw new Exception(__("You must enter fee type name"));
        }

        if (strlen($args['fee_name']) > 100) {
            throw new Exception(__("Fee name must be less than 100 characters long"));
        }

        if (isset($args['description']) && (strlen($args['description'])) > 300) {
            throw new Exception(__("Description length must be less than 300 characters long"));
        }

//        if ($args['level'] == CHILD_LEVEL) {
//            if (count($args['fee_change_id']) <= 0) {
//                throw new Exception(__("You must select at least one replaced-fee"));
//            }
//        }
    }

    /**
     * Cập nhật một class level vào trong DB
     *
     * @param array $args
     * @throws Exception
     */
    public function updateFee(array $args = array())
    {
        global $db, $date;
        if ($args['fee_id'] < 0) {
            throw new Exception(__("You must select an existing fee"));
        }
        $this->_validateFeeInput($args);

        if ($args['level'] == CHILD_LEVEL) {
            $fee_change_id = (isset($args['fee_change_id']) && (count($args['fee_change_id']) > 0))? implode(",",$args['fee_change_id']) : '';
            $strSql = sprintf("UPDATE ci_fee SET fee_name = %s, unit_price = %s, level = %s, class_level_id = %s, class_id = %s, fee_change_id = %s, description = %s, status = %s, updated_at = %s 
                           WHERE fee_id = %s", secure($args['fee_name']), secure($args['unit_price'], 'int'), secure($args['level'], 'int'),
                secure($args['class_level_id'], 'int'), secure($args['class_id'], 'int'), secure($fee_change_id), secure($args['description']),
                secure($args['status'], 'int'), secure($date), secure($args['fee_id'], 'int'));
            /* update class level */
            $db->query($strSql) or _error(SQL_ERROR_THROWEN);

            // Delete student_id bị xóa
            $db->query(sprintf("DELETE FROM ci_fee_child WHERE fee_id = %s", secure($args['fee_id'], 'int'))) or _error(SQL_ERROR_THROWEN);

            // Insert child_id đc thêm vào
            $strValues = "";
            foreach ($args['child_ids'] as $child_id) {
                $strValues .= "(" . secure($args['fee_id'], 'int') . "," . secure($child_id, 'int') . "),";
            }
            $strValues = trim($strValues, ",");
            if (!is_empty($strValues)) {
                $strSql = sprintf("INSERT INTO ci_fee_child (fee_id, child_id) VALUES " . $strValues);
                $db->query($strSql) or _error(SQL_ERROR_THROWEN);
            }
        } else {
            $strSql = sprintf("UPDATE ci_fee SET fee_name = %s, unit_price = %s, level = %s, class_level_id = %s, class_id = %s, description = %s, status = %s, updated_at = %s
                           WHERE fee_id = %s", secure($args['fee_name']), secure($args['unit_price'], 'int'), secure($args['level'], 'int'),
                secure($args['class_level_id'], 'int'), secure($args['class_id'], 'int'), secure($args['description']), secure($args['status'], 'int'), secure($date), secure($args['fee_id'], 'int'));
            /* update class level */
            $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        }
    }

    /**
     * Inactive Fee
     *
     * @param $feeId
     * @throws Exception
     */
    public function InactiveFee($feeId) {
        global $db, $date;

        $strSql = sprintf("UPDATE ci_fee SET status = %s, updated_at = %s WHERE fee_id = %s", secure(STATUS_INACTIVE, 'int'), secure($date), secure($feeId, 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Hàm cập nhật tổng giảm trừ của một tháng.
     *
     * @param $tuitionId
     * @param $deductionTotal
     * @throws Exception
     */
    function updateDeductionTotalOfMonth($tuitionId, $deductionTotal)
    {
        global $db;
        $strSql = sprintf("UPDATE ci_tuition SET total_deduction = %s WHERE tuition_id = %s", secure($deductionTotal, 'int'), secure($tuitionId, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Hàm lấy ra tuition của 1 tháng
     *
     * @param $schoolId
     * @param $classId
     * @param $month
     * @return int
     * @throws Exception
     */
    function getTuitionId($schoolId, $classId, $month)
    {
        global $db;
        $month = toDBDate("01/" . $month);

        $strSql = sprintf("SELECT tuition_id FROM ci_tuition WHERE school_id = %s AND class_id = %s AND month = %s", secure($schoolId, 'int'), secure($classId, 'int'), secure($month));
        $get_tuition = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_tuition->num_rows > 0) {
            return $get_tuition->fetch_assoc()['tuition_id'];
        }
        return 0;
    }

    /**
     * Lấy ra thông tin học phí của 1 lớp trong tháng. Để _tạo học phí tháng đó cho lớp.
     *
     * @param $schoolId
     * @param $classId
     * @param $month
     * @param $dayOfMonth: Số ngày của tháng được chọn
     * @return array
     * @throws Exception
     */
    public function getTuitionOfClassInMonth($schoolId, $classId, $month, $dayOfMonth)
    {
        global $db, $system;
        $firstDateOfMonth = toDBDate("01/" . $month);
        $firstDateOfNextMonth = addMonthToDBDate($firstDateOfMonth);
        $results = array();
        //Lấy ra danh sách trẻ của lớp (chỉ lấy trẻ còn đi học)
        $strSql = sprintf("SELECT C.child_id, C.child_name, C.birthday, SC.begin_at FROM ci_school_child SC
                           INNER JOIN ci_child C ON SC.child_id = C.child_id
                           AND SC.class_id = %s AND SC.begin_at < %s AND SC.status = %s ORDER BY C.name_for_sort ASC", secure($classId, 'int'), secure($firstDateOfNextMonth), secure(STATUS_ACTIVE, 'int'));
        $get_children = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        $children = array();
        $total_amount = 0; //Tổng học phí cả lớp

        if ($get_children->num_rows > 0) {
            //Duyệt danh sách trẻ để điền thông tin học phí liên quan (bao gồm cả dịch vụ sử dụng)
            while ($child = $get_children->fetch_assoc()) {
                $newChild = $this->getTuitionOfAChildInMonth($schoolId, $classId, $child, $month, $dayOfMonth);
                $children[] = $newChild;
                //Tính tổng học phí của lớp
                $total_amount = $total_amount + $newChild['child_total_amount']; //Cộng học phí trẻ vào học phí lớp
            }
        }

        $results['month'] = $month;
        $results['total_amount'] = $total_amount;
        $results['children'] = $children;

        return $results;
    }

    /**
     * Lấy ra thông tin sử dụng dịch vụ của một lớp
     *
     * @param $schoolId
     * @param $classId
     * @param $month
     * @return array
     * @throws Exception
     */
    public function getServiceOfClassInPreMonth($schoolId, $classId, $month)
    {
        global $db;
        $firstDateOfMonth = toDBDate("01/" . $month);
        $firstDateOfNextMonth = addMonthToDBDate($firstDateOfMonth);
        //Lấy ra danh sách trẻ của lớp (chỉ lấy trẻ còn đi học)
        $strSql = sprintf("SELECT C.child_id, C.child_name, SC.begin_at FROM ci_school_child SC
                           INNER JOIN ci_child C ON SC.child_id = C.child_id
                           AND SC.class_id = %s AND SC.begin_at < %s AND SC.status = %s ORDER BY C.name_for_sort ASC", secure($classId, 'int'), secure($firstDateOfNextMonth), secure(STATUS_ACTIVE, 'int'));
        $get_children = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        $children = array();
        if ($get_children->num_rows > 0) {
            //Duyệt danh sách trẻ để điền thông tin học phí liên quan (bao gồm cả dịch vụ sử dụng)
            while ($child = $get_children->fetch_assoc()) {
                $newChild = $this->getServiceOfAChildInPreMonth($schoolId, $child, $month);
                $children[] = $newChild;
            }
        }

        return $children;
    }

    /**
     * Tính ước lượng học phí cho một trẻ trong một tháng. Phục vụ reload trên màn hình tạo mới.
     * Nếu không tồn tại trẻ trả về NULL.
     *
     * @param $schoolId
     * @param $classId
     * @param $month
     * @param $dayOfMonth
     * @param $childId
     * @return mixed|null
     * @throws Exception
     */
    public function reloadTuitionOfAChildInMonth($schoolId, $classId, $month, $dayOfMonth, $childId)
    {
        global $db;
        $firstDateOfMonth = toDBDate("01/" . $month);
        $firstDateOfNextMonth = addMonthToDBDate($firstDateOfMonth);
        $strSql = sprintf("SELECT C.child_id, C.child_name, C.child_code, C.birthday, SC.begin_at FROM ci_school_child SC
                           INNER JOIN ci_child C ON SC.child_id = C.child_id
                           AND SC.class_id = %s AND SC.child_id = %s AND SC.begin_at < %s AND SC.status = %s",
                            secure($classId, 'int'), secure($childId, 'int'), secure($firstDateOfNextMonth), secure(STATUS_ACTIVE, 'int'));
        $get_children = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        if ($get_children->num_rows > 0) {
            //Duyệt danh sách trẻ để điền thông tin học phí liên quan (bao gồm cả dịch vụ sử dụng)
            $child = $get_children->fetch_assoc();
            $newChild = $this->getTuitionOfAChildInMonth($schoolId, $classId, $child, $month, $dayOfMonth);

            return $newChild;
        }

        return null;
    }

    /**
     * Lấy ra tháng gần nhất mà trẻ được tính học phí.
     *
     * @param $schoolId
     * @param $childId
     * @param $month
     * @return bool|null|string
     * @throws Exception
     */
    private function getPreviousTuitionMonthOfChild($schoolId, $childId, $month) {
        global $db;
        $firstDateOfMonth = toDBDate("01/" . $month);
        $strSql = sprintf("SELECT MAX(T.month) AS pre_month FROM ci_tuition T INNER JOIN ci_tuition_child TC
                            ON T.tuition_id = TC.tuition_id AND T.school_id = %s AND TC.child_id = %s AND T.month < %s",
                            secure($schoolId, 'int'), secure($childId, 'int'), secure($firstDateOfMonth));
        $get_month = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        if ($get_month->num_rows > 0) {
            $preMonth = $get_month->fetch_assoc()['pre_month'];
            return toSysMMYYYY($preMonth);
        }
        return null;
    }

    /**
     * Lấy ra tháng tính học phí cuối cùng của một trẻ, tháng đó phải nhỏ hơn hoặc bằng ngày truyền vào
     *
     * @param $schoolId
     * @param $childId
     * @param $endDate
     * @return bool|null|string
     * @throws Exception
     */
    private function getLastTuitionMonthOfChild($schoolId, $childId, $endDate) {
        global $db;
        $endDate = toDBDate($endDate);
        $strSql = sprintf("SELECT MAX(T.month) AS last_month FROM ci_tuition T INNER JOIN ci_tuition_child TC
                            ON T.tuition_id = TC.tuition_id AND T.school_id = %s AND TC.child_id = %s AND T.month <= %s",
            secure($schoolId, 'int'), secure($childId, 'int'), secure($endDate));
        $get_month = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        if ($get_month->num_rows > 0) {
            $preMonth = $get_month->fetch_assoc()['last_month'];
            return toSysMMYYYY($preMonth);
        }
        return null;
    }

    /**
     * Load học phí một trẻ trong một tháng. Để tính toán học phí mới của tháng.
     *
     * @param $schoolId
     * @param $classId
     * @param $orgChild
     * @param $month
     * @param $dayOfMonth
     * @return mixed
     */
    public function getTuitionOfAChildInMonth ($schoolId, $classId, $orgChild, $month, $dayOfMonth) {
        $previousMonth = $this->getPreviousTuitionMonthOfChild($schoolId, $orgChild['child_id'], $month);
        if (is_null($previousMonth) || ($previousMonth == "")) {
            $previousMonth = getPreMonth($month); //Nếu chưa tồn tại tháng nào thu học phí thì lấy tháng ngay trước.
        }

        $firstDateOfMonth = toDBDate("01/" . $month);
        $child = $orgChild;
        $today = date('Y-m-d');
        $today = strtotime($today);
        $birthday = strtotime($child['birthday']);

        $month_child = abs($today - $birthday);
        $child['month'] = floor($month_child / (24*60*60*30));
        $child['pre_month'] = $previousMonth;

        //1. Lấy ra số lần điểm danh trong tháng của trẻ
        // Tạo học phí tháng 4 thì lấy số lần điểm danh trong tháng 3
        $attendances = $this->getChildAttendanceInMonth($child['child_id'], $previousMonth);
        $attendanceCount = 0;
        foreach ($attendances as $attendance) {
            if (($attendance['status'] == ATTENDANCE_PRESENT) || ($attendance['status'] == ATTENDANCE_COME_LATE) || ($attendance['status'] == ATTENDANCE_ABSENCE_NO_REASON)) {
                $attendanceCount++;
            }
        }
        $child['attendance_count'] = $attendanceCount;
        $child['attendances'] = $attendances;

        //2. Lấy học phí của 1 trẻ (không bao gồm những học phí cho trẻ khác)
        $fees = $this->getFeeOfOneChildInMonth($classId, $schoolId, $child['child_id']);

        $childAmount = 0; //Tổng học phí mà trẻ phải đóng

        //3. Phí giảm trừ tháng trước.
        //Lấy ra TẤT CẢ khoản phí ĐÃ TRẢ của trẻ trong tháng trước (tháng và theo điểm danh).
        $previousMonthFees = array();
        $preFeeIds = array(); //Lưu danh sách FEE_ID các phí nộp tháng trước
        $paidPreMonthFees = $this->getPaidFeeOfChildInMonth($schoolId, $child['child_id'], $previousMonth);
        if (count($paidPreMonthFees) > 0) {
            foreach ($paidPreMonthFees as $fee) {
                if ($fee['fee_type'] == FEE_TYPE_DAILY) {
                    $fee['quantity_deduction'] = $fee['quantity'] - $child['attendance_count'];
                }
                $preFeeIds[] = $fee['fee_id'];
                $previousMonthFees[] = $fee;
            }
        }

        //4. Tạo danh sách phí trẻ phải trả trong tháng
        $childFees = array(); // Học phí của trẻ trong tháng
        foreach ($fees as $fee) {
            if ($fee['fee_type'] == FEE_TYPE_DAILY) {
                $fee['amount'] = $dayOfMonth * $fee['unit_price'];
                $fee['quantity'] = $dayOfMonth;
            } else {
                $fee['amount'] = $fee['unit_price'];
                $fee['quantity'] = 1;
            }

            //Nếu phí có trong danh sách nộp tháng trước thì lưu số lượng tồn
            if (in_array($fee['fee_id'], $preFeeIds)) {
                $preFeeIds = array_diff($preFeeIds, [$fee['fee_id']]); //Bỏ bớt những Fee_ID đã tìm thấy để sử dụng cho bước sau
                foreach ($previousMonthFees as $preFee) {
                    if (($fee['fee_id'] == $preFee['fee_id']) && ($fee['fee_type'] == FEE_TYPE_DAILY)) {
                        $fee['quantity_deduction'] = $preFee['quantity_deduction'];
                        $fee['unit_price_deduction'] = $preFee['unit_price'];
                        $fee['amount'] = $fee['quantity'] * $fee['unit_price'] - $fee['quantity_deduction'] * $fee['unit_price_deduction'];
                        $fee['pre_quantity'] = $preFee['quantity'];
                        break;
                    }
                }
            } else {
                $fee['unit_price_deduction'] = $fee['unit_price'];
            }
            $childAmount = $childAmount + $fee['amount'];
            $childFees[] = $fee;
        }
        //Nếu có phí trơng tháng trước, mà không có trong tháng này
        if (count($preFeeIds) > 0) { //$preFeeIds là những ID còn lại sau khi đã trừ đi ở những lần gặp trước.
            foreach ($previousMonthFees as $preFee) {
                if (in_array($preFee['fee_id'], $preFeeIds) && ($preFee['fee_type'] == FEE_TYPE_DAILY) && ($preFee['quantity_deduction'] != 0) && ($preFee['quantity'] != 0)) {
                    $fee = array();
                    $fee['fee_id'] = $preFee['fee_id'];
                    $fee['fee_type'] = $preFee['fee_type'];
                    $fee['fee_name'] = $preFee['fee_name'];
                    $fee['unit_price'] = $preFee['current_unit_price'];
                    $fee['unit_price_deduction'] = $preFee['unit_price'];
                    $fee['pre_quantity'] = $preFee['quantity'];
                    $fee['quantity_deduction'] = $preFee['quantity_deduction'];
                    $fee['quantity'] = 0;
                    $fee['amount'] = - ($fee['unit_price_deduction'] * $fee['quantity_deduction']);

                    $childAmount = $childAmount + $fee['amount'];
                    $childFees[] = $fee;
                }
            }
        }
        $child['fees'] = $childFees;

        //5. Khoảng giảm trừ dịch vụ tháng trước.
        //Lấy danh sách dịch vụ theo THÁNG và ĐIỂM DANH mà trẻ ĐÃ SỬ DỤNG trong tháng trước.
        $usedMonthlyNDailyServices = array(); // Dịch vụ trẻ sử dụng trong tháng trước
        $serviceIds = array();
        $usedMonthlyNDailyServicesInPreMonth = $this->getUsedMonthlyNDailyServicesOfChildInMonth($schoolId, $child['child_id'], $previousMonth);
        foreach ($usedMonthlyNDailyServicesInPreMonth as $service) {
            if ($service['service_type'] == SERVICE_TYPE_DAILY) {
                $service['quantity_deduction'] = $service['quantity'] - $child['attendance_count'] + $service['monthly_daily_deduction'];
            }

            $usedMonthlyNDailyServices[] = $service;
            $serviceIds[] = $service['service_id'];
        }

        //6. Lấy ra danh sách các dịch vụ mà trẻ sử dụng tính theo tháng và số lần điểm danh (bao gồm cả những dịch vụ mà trẻ đã hủy trong tháng)
        $monthlyNDailyServicesOfChildInMonth = $this->getMonthlyNDailyServiceOfChildInMonth($schoolId, $child['child_id'], $month);
        // Dịch vụ trẻ sử dụng trong tháng này. Để tính phí dịch vụ tháng này.
        $childServices = array();
        if (count($monthlyNDailyServicesOfChildInMonth) > 0) {
            foreach ($monthlyNDailyServicesOfChildInMonth as $service) {
                if ($service['service_type'] == SERVICE_TYPE_DAILY) {
                    $service['amount'] = $dayOfMonth * $service['fee'];
                    $service['quantity'] = $dayOfMonth;
                } else if ($service['service_type'] == SERVICE_TYPE_MONTHLY) {
                    $service['amount'] = $service['fee'];
                    $service['quantity'] = 1;
                }
                $service['quantity_deduction'] = 0;

                //Nếu dịch vụ có trong danh sách nộp tháng trước thì lưu số lượng tồn
                if (in_array($service['service_id'], $serviceIds)) {
                    $serviceIds = array_diff($serviceIds, [$service['service_id']]); //Bỏ bớt những Service_ID đã tìm thấy để sử dụng cho bước sau
                    foreach ($usedMonthlyNDailyServices as $preService) {
                        if (($service['service_id'] == $preService['service_id']) && ($service['service_type'] == SERVICE_TYPE_DAILY)) {
                            $service['quantity_deduction'] = $preService['quantity_deduction'];
                            $service['monthly_daily_deduction'] = $preService['monthly_daily_deduction'];
                            $service['unit_price_deduction'] = $preService['unit_price'];
                            $service['amount'] = $service['quantity'] * $service['fee'] - $service['quantity_deduction'] * $service['unit_price_deduction'];
                            $service['pre_quantity'] = $preService['quantity'];
                            break;
                        }
                    }
                } else {
                    $service['unit_price_deduction'] = $service['fee'];
                }
                $childAmount = $childAmount + $service['amount'];
                $childServices[] = $service;
            }
        }

        //Nếu có dịch vụ trơng tháng trước, mà không có trong tháng này
        if (count($serviceIds) > 0) { //$serviceIds là ID còn lại sau khi đã trừ đi những ID đã gặp ở trước
            foreach ($usedMonthlyNDailyServices as $preService) {
                if (in_array($preService['service_id'], $serviceIds) && ($preService['service_type'] == SERVICE_TYPE_DAILY)
                    && ($preService['quantity_deduction'] != 0) && ($preService['quantity'] != 0)) {
                    $service = array();
                    $service['service_id'] = $preService['service_id'];
                    $service['service_type'] = $preService['service_type'];
                    $service['service_name'] = $preService['service_name'];
                    $service['fee'] = $preService['fee'];
                    $service['unit_price_deduction'] = $preService['unit_price'];
                    $service['pre_quantity'] = $preService['quantity'];
                    $service['quantity_deduction'] = $preService['quantity_deduction'];
                    $service['monthly_daily_deduction'] = $preService['monthly_daily_deduction'];
                    $service['quantity'] = 0;
                    $service['amount'] = -($service['quantity_deduction'] * $service['unit_price_deduction']);

                    $childAmount = $childAmount + $service['amount'];
                    $childServices[] = $service;
                }
            }
        }

        $child['services'] = $childServices;

        //7. Lấy ra danh sách các dịch vụ mà trẻ sử dụng tính theo SỐ LẦN sử dụng
        if ($child['begin_at'] < $firstDateOfMonth) {
            $usedCountBasedServicesOfChildInMonth = $this->getUsedCountBasedServicesOfChildInMonth($schoolId, $child['child_id'], $previousMonth);
            $usedCountBasedServices = array();
            if (count($usedCountBasedServicesOfChildInMonth) > 0) {
                foreach ($usedCountBasedServicesOfChildInMonth as $service) {
                    $service['amount'] = $service['fee'] * $service['CNT'];
                    $childAmount = $childAmount + $service['amount'];
                    $usedCountBasedServices[] = $service;
                }
            }
            $child['cbservices'] = $usedCountBasedServices;
        }
        //8. Lấy ra số tiền còn nợ tháng trước.
        $debtAmount = $this->getDebtAmountOfChildInMonth($child['child_id'], $previousMonth);
        $child['pre_month_debt_amount'] = $debtAmount;
        $childAmount = $childAmount + $debtAmount;

        //9. Lấy ra tiền đón muộn của trẻ trong tháng trước
        $childPickup = $this->getChildPickupInMonth($schoolId, $child['child_id'], $previousMonth);
        $childAmount = $childAmount + $childPickup;
        $child['pickup'] = $childPickup;

        $child['child_total_amount'] = $childAmount;

        return $child;
    }

    /**
     * Lấy ra thông tin sử dụng dịch vụ tháng trước của một trẻ.
     *
     * @param $schoolId
     * @param $classId
     * @param $orgChild
     * @param $month
     * @return mixed
     */
    public function getServiceOfAChildInPreMonth ($schoolId, $orgChild, $month) {
        $previousMonth = $this->getPreviousTuitionMonthOfChild($schoolId, $orgChild['child_id'], $month);
        if (is_null($previousMonth) || ($previousMonth == "")) {
            $previousMonth = getPreMonth($month); //Nếu chưa tồn tại tháng nào thu học phí thì lấy tháng ngay trước.
        }

        $firstDateOfMonth = toDBDate("01/" . $month);
        $child = $orgChild;

        //1. Lấy ra số lần điểm danh trong tháng của trẻ
        // Tạo học phí tháng 4 thì lấy số lần điểm danh trong tháng 3
        $attendances = $this->getChildAttendanceInMonth($child['child_id'], $previousMonth);
        $attendanceCount = 0;
        $absentCount = 0;
        foreach ($attendances as $attendance) {
            if (($attendance['status'] == ATTENDANCE_PRESENT) || ($attendance['status'] == ATTENDANCE_COME_LATE) || ($attendance['status'] == ATTENDANCE_ABSENCE_NO_REASON)) {
                $attendanceCount++; //Ngày tính phí
            } else {
                $absentCount++; //Ngày không tính phí
            }
        }
        $child['attendance_count'] = $attendanceCount;
        $child['absent_count'] = $absentCount;

        //2. Lấy danh sách dịch vụ theo ĐIỂM DANH mà trẻ ĐÃ SỬ DỤNG trong tháng trước.
        $dailyServicesInPreMonth = $this->getDailyServiceOfChildInMonth($schoolId, $child['child_id'], $previousMonth);
//        foreach ($dailyServicesInPreMonth as $service) {
//            $service['quantity_deduction'] = $child['absent_count'] + $service['monthly_daily_deduction'];
//            $dailyServices[] = $service;
//        }

        $child['daily_services'] = $dailyServicesInPreMonth;

        //3. Lấy ra danh sách các dịch vụ mà trẻ sử dụng tính theo SỐ LẦN sử dụng
        if ($child['begin_at'] < $firstDateOfMonth) {
            $usedCountBasedServicesOfChildInMonth = $this->getUsedCountBasedServicesOfChildInMonth($schoolId, $child['child_id'], $previousMonth);
//            $usedCountBasedServices = array();
//            foreach ($usedCountBasedServicesOfChildInMonth as $service) {
//                $service['quantity'] = $service['CNT'];
//                $usedCountBasedServices[] = $service;
//            }
//            $child['cbservices'] = $usedCountBasedServices;
            $child['cbservices'] = $usedCountBasedServicesOfChildInMonth;
        } else {
            $child['cbservices'] = array();
        }
        //4. Lấy ra số tiền còn nợ tháng trước.
        $child['pre_month_debt_amount'] = $this->getDebtAmountOfChildInMonth($child['child_id'], $previousMonth);

        /*5. BEGIN - Tính toán phần đón muộn */
        //Lấy ra tiền đón muộn mà trẻ trong tháng trước
        $childPickup = $this->getChildPickupInMonth($schoolId, $child['child_id'], $previousMonth);
        $child['pickup'] = $childPickup;

        return $child;
    }

    /**
     * Lấy ra thông tin học phí của những học sinh mới trong tháng. Để thêm vào học phí tháng cho lớp.
     *
     * @param $tuition
     * @return array
     * @throws Exception
     */
    function getTuitionOfNewChildInMonth($tuition)
    {
        global $db;

        $schoolId = $tuition['school_id'];
        $classId = $tuition['class_id'];
        $dayOfMonth = $tuition['day_of_month'];
        $month = $tuition['month'];
        $tuitionId = $tuition['tuition_id'];

        $firstDateOfMonth = toDBDate("01/" . $month);
        $firstDateOfNextMonth = addMonthToDBDate($firstDateOfMonth);

        $results = array();
        //Lấy ra danh sách trẻ thuộc lớp, NHƯNG chưa được tính học phí tháng.
        $strSql = sprintf("SELECT C.child_id, C.child_name, C.birthday FROM ci_school_child SC
                            INNER JOIN ci_child C ON SC.child_id = C.child_id AND SC.status = %s AND SC.class_id = %s
                            WHERE SC.child_id NOT IN (SELECT child_id FROM ci_tuition_child WHERE tuition_id = %s) AND SC.begin_at < %s
                            ORDER BY C.name_for_sort ASC", secure(STATUS_ACTIVE, 'int'), secure($classId, 'int'), secure($tuitionId, 'int'), secure($firstDateOfNextMonth));
        $get_children = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        $children = array();
        $total_amount = $tuition['total_amount']; //Tổng học phí cả lớp
        $total_deduction = $tuition['total_deduction']; // Tổng giảm trừ cả lớp

        if ($get_children->num_rows > 0) {
            //Duyệt danh sách trẻ để điền thông tin học phí liên quan (bao gồm cả dịch vụ sử dụng)
            while ($child = $get_children->fetch_assoc()) {
                $newChild = $this->getTuitionOfAChildInMonth($schoolId, $classId, $child, $month, $dayOfMonth);
                $children[] = $newChild;
                //Tính tổng học phí của lớp
                $total_amount = $total_amount + $newChild['child_total_amount']; //Cộng học phí trẻ vào học phí lớp
                $total_deduction = $total_deduction + $newChild['child_total_deduction'];
            }
        }

        $results['month'] = $month;
        $results['total_amount'] = $total_amount;
        $results['total_deduction'] = $total_deduction;
        $results['children'] = $children;

        return $results;
    }

    /**
     * Lấy ra số lần điểm danh của trẻ trong tháng (bao gồm cả trường hợp xin nghỉ muộn)
     *
     * @param $childId
     * @param $month
     * @return int
     */
    private function getChildAttendanceCount($childId, $month)
    {
        global $db;
        $firstDateOfMonth = toDBDate("01/" . $month);
        $firstDateOfNextMonth = addMonthToDBDate($firstDateOfMonth);

        //Những trạng thái tính tiền ăn là: ATTENDANCE_PRESENT, ATTENDANCE_COME_LATE, ATTENDANCE_ABSENCE_NO_REASON
        $strSql = sprintf("SELECT COUNT(AD.attendance_detail_id) AS CNT FROM ci_attendance_detail AD INNER JOIN ci_attendance A ON AD.attendance_id = A.attendance_id AND A.is_checked = 1
                        AND AD.child_id = %s AND (AD.status = %s OR AD.status = %s OR AD.status = %s) AND A.attendance_date >= %s AND A.attendance_date < %s", secure($childId, 'int'),
            secure(ATTENDANCE_PRESENT, 'int'), secure(ATTENDANCE_COME_LATE, 'int'), secure(ATTENDANCE_ABSENCE_NO_REASON, 'int'),
            secure($firstDateOfMonth), secure($firstDateOfNextMonth));
        $get_cnt = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        while ($cnt = $get_cnt->fetch_assoc()) {
            return $cnt['CNT'];
        }
        return 0;
    }

    /**
     * Lấy ra số lần điểm danh của trẻ trong khoảng thời gian (bao gồm cả trường hợp xin nghỉ muộn).
     * Tính luôn cả ngày cuối cùng, nếu đi học.
     *
     * @param $childId
     * @param $begin
     * @param $end
     * @return int
     * @throws Exception
     */
    private function getChildAttendanceCountInDuration($childId, $begin, $end)
    {
        global $db;
        $begin = toDBDate($begin);
        $end = toDBDate($end);

        //Những trạng thái tính tiền ăn là: ATTENDANCE_PRESENT, ATTENDANCE_COME_LATE, ATTENDANCE_ABSENCE_NO_REASON
        $strSql = sprintf("SELECT COUNT(AD.attendance_detail_id) AS CNT FROM ci_attendance_detail AD INNER JOIN ci_attendance A ON AD.attendance_id = A.attendance_id AND A.is_checked = 1
                        AND AD.child_id = %s AND (AD.status = %s OR AD.status = %s OR AD.status = %s) AND A.attendance_date >= %s AND A.attendance_date <= %s", secure($childId, 'int'),
            secure(ATTENDANCE_PRESENT, 'int'), secure(ATTENDANCE_COME_LATE, 'int'), secure(ATTENDANCE_ABSENCE_NO_REASON, 'int'),
            secure($begin), secure($end));
        $get_cnt = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        while ($cnt = $get_cnt->fetch_assoc()) {
            return $cnt['CNT'];
        }
        return 0;
    }

    /**
     * Lấy toàn bộ trạng thái điểm danh của trẻ trong một tháng. CHỈ LẤY NHỮNG NGÀY MÀ LỚP ĐIỂM DANH
     *
     * @param $classId
     * @param $childId
     * @param $month
     * @return int
     * @throws Exception
     */
    private function getChildAttendanceStatusInMonth($classId, $childId, $month)
    {
        global $db;
        $firstDateOfMonth = toDBDate("01/" . $month);
        $firstDateOfNextMonth = addMonthToDBDate($firstDateOfMonth);

        $strSql = sprintf("SELECT AD.status, COUNT(AD.status) AS CNT FROM ci_attendance_detail AD INNER JOIN ci_attendance A ON AD.attendance_id = A.attendance_id AND A.is_checked = 1
                           AND A.class_id = %s AND AD.child_id = %s AND A.attendance_date >= %s AND A.attendance_date < %s GROUP BY AD.status ORDER BY AD.status",
            secure($classId, 'int'), secure($childId, 'int'), secure($firstDateOfMonth), secure($firstDateOfNextMonth));
        $get_status = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        $allStatus = array();
        while ($status = $get_status->fetch_assoc()) {
            $allStatus[] = $status;
        }

        return $allStatus;
    }

    /**
     * Lấy ra thông tin điểm danh của trẻ trong một tháng.
     *
     * @param $childId
     * @param $month
     * @return array
     * @throws Exception
     */
    private function getChildAttendanceInMonth($childId, $month)
    {
        global $db;
        $firstDateOfMonth = toDBDate("01/" . $month);
        $firstDateOfNextMonth = addMonthToDBDate($firstDateOfMonth);

        $strSql = sprintf("SELECT AD.status, A.attendance_date FROM ci_attendance_detail AD INNER JOIN ci_attendance A ON AD.attendance_id = A.attendance_id
                            AND A.is_checked = 1 AND AD.child_id = %s AND A.attendance_date >= %s AND A.attendance_date < %s ORDER BY A.attendance_date ASC",
                            secure($childId, 'int'), secure($firstDateOfMonth), secure($firstDateOfNextMonth));
        $get_attendance = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        $attendances = array();
        while ($attendance = $get_attendance->fetch_assoc()) {
            $tmpDate = toSysDate($attendance['attendance_date']);
            $attendance['display_date'] = explode("/", $tmpDate)[0];
            $attendances[] = $attendance;
        }

        return $attendances;
    }

    /**
     * Lấy ra danh sách dịch vụ tính theo tháng và số lần điểm danh trẻ sử dụng trong tháng (bao gồm cả những dịch vụ mà trẻ đã hủy trong tháng)
     *
     * @param $schoolId
     * @param $childId
     * @param $month
     * @return array
     * @throws Exception
     */
    private function getMonthlyNDailyServiceOfChildInMonth($schoolId, $childId, $month)
    {
        global $db;
        $firstDateOfMonth = toDBDate("01/" . $month);
        $firstDateOfNextMonth = addMonthToDBDate($firstDateOfMonth);

        //Select 1: lấy ra các dịch vụ tính phí theo tháng và theo điểm danh mà trẻ đăng ký
        //Select 2: láy ra dịch vụ tính theo số lần sử dụng mà trẻ dùng.
        $strSql = sprintf('(SELECT SC.service_id, S.service_name, S.fee, S.type AS service_type, 1 AS CNT FROM ci_service_child SC INNER JOIN ci_service S
                            ON SC.service_id = S.service_id AND S.status = 1 AND S.type != %1$s AND SC.child_id = %2$s AND
                            ((SC.status = 1 AND SC.begin < %4$s) OR (SC.status = 0 AND SC.end > %3$s AND SC.begin < %4$s)) AND S.school_id = %5$s)',
            SERVICE_TYPE_COUNT_BASED, secure($childId, 'int'), secure($firstDateOfMonth), secure($firstDateOfNextMonth), secure($schoolId, 'int'));

        $childServices = array();
        $get_childServices = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_childServices->num_rows > 0) {
            while ($childService = $get_childServices->fetch_assoc()) {
                $childServices[] = $childService;
            }
        }
        return $childServices;
    }

    /**
     * Lấy ra danh sách dịch vụ tính theo tháng và số lần điểm danh trẻ sử dụng trong khoảng thời gian
     * (bao gồm cả những dịch vụ mà trẻ đã hủy trong tháng)
     *
     * @param $schoolId
     * @param $childId
     * @param $begin
     * @param $end: Lấy cả ngày end
     * @return array
     * @throws Exception
     */
    private function getMonthlyNDailyServiceOfChildInDuration($schoolId, $childId, $begin, $end)
    {
        global $db;
        $begin = toDBDate($begin);
        $end = toDBDate($end);

        $strSql = sprintf('(SELECT SC.service_id, S.service_name, S.fee, S.type, 1 AS CNT FROM ci_service_child SC INNER JOIN ci_service S
                                ON SC.service_id = S.service_id AND S.type != %1$s AND SC.child_id = %2$s AND
                                ((SC.status = 1 AND SC.begin <= %4$s) OR (SC.status = 0 AND SC.end >= %3$s AND SC.begin <= %4$s)) AND S.school_id = %5$s)',
            SERVICE_TYPE_COUNT_BASED, secure($childId, 'int'), secure($begin), secure($end), secure($schoolId, 'int'));

        $childServices = array();
        $get_childServices = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_childServices->num_rows > 0) {
            while ($childService = $get_childServices->fetch_assoc()) {
                $childServices[] = $childService;
            }
        }
        return $childServices;
    }

    /**
     * Lấy ra danh sách dịch vụ theo ĐIỂM DANH của trẻ.
     *
     * @param $schoolId
     * @param $childId
     * @param $month
     * @return array
     * @throws Exception
     */
    private function getDailyServiceOfChildInMonth($schoolId, $childId, $month)
    {
        global $db;
        $firstDateOfMonth = toDBDate("01/" . $month);
        $firstDateOfNextMonth = addMonthToDBDate($firstDateOfMonth);

        $strSql = sprintf('(SELECT SC.service_id, S.service_name, S.fee FROM ci_service_child SC INNER JOIN ci_service S
                                ON SC.service_id = S.service_id AND S.type = %1$s AND SC.child_id = %2$s AND
                                ((SC.status = 1 AND SC.begin <= %4$s) OR (SC.status = 0 AND SC.end > %3$s AND SC.begin <= %4$s)) AND S.school_id = %5$s)',
            SERVICE_TYPE_DAILY, secure($childId, 'int'), secure($firstDateOfMonth), secure($firstDateOfNextMonth), secure($schoolId, 'int'));

        $childServices = array();
        $get_childServices = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_childServices->num_rows > 0) {
            while ($childService = $get_childServices->fetch_assoc()) {
                $childService['monthly_daily_deduction'] = $this->getMonthlyNDailyServiceDeductionOfChildInMonth($childService['service_id'], $childId, $month);
                $childServices[] = $childService;
            }
        }
        return $childServices;
    }

    /**
     * Lấy ra danh sách dịch vụ tính theo tháng và số lần điểm danh trẻ ĐÃ sử dụng trong 1 tháng (bao gồm cả những dịch vụ mà trẻ đã hủy trong tháng).
     * Thường dùng để lấy tháng trước khi tính học phí tháng mới.
     *
     * @param $schoolId
     * @param $childId
     * @param $month
     * @return array
     * @throws Exception
     */
    private function getUsedMonthlyNDailyServicesOfChildInMonth($schoolId, $childId, $month)
    {
        global $db;
        $month = toDBDate("01/" . $month);

        $strSql = sprintf("SELECT TD.*, S.service_name, S.fee FROM ci_tuition_detail TD
                            INNER JOIN ci_tuition_child TC ON TC.tuition_child_id = TD.tuition_child_id AND TC.child_id = %s AND TD.service_id > '0' AND TD.service_type != %s
                            INNER JOIN ci_tuition T ON T.tuition_id = TC.tuition_id AND T.school_id = %s AND T.month = %s
                            INNER JOIN ci_service S ON S.service_id = TD.service_id ORDER BY TD.tuition_child_id DESC",
                    secure($childId, 'int'), SERVICE_TYPE_COUNT_BASED, secure($schoolId, 'int'), secure($month));

        $childServices = array();
        $get_childServices = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $serviceIds = array();
        if ($get_childServices->num_rows > 0) {
            while ($childService = $get_childServices->fetch_assoc()) {
                if(!in_array($childService['service_id'], $serviceIds)) {
                    $childService['monthly_daily_deduction'] = $this->getMonthlyNDailyServiceDeductionOfChildInMonth($childService['service_id'], $childId, $month);
                    $childServices[] = $childService;
                    $serviceIds[] = $childService['service_id'];
                }
            }
        }
        return $childServices;
    }

    /**
     * Lấy ra giảm trừ dịch vụ của một trẻ đối với dịch vụ theo THÁNG và ĐIỂM DANH.
     *
     * @param $serviceId
     * @param $childId
     * @param $month
     * @return int
     * @throws Exception
     */
    private function getMonthlyNDailyServiceDeductionOfChildInMonth($serviceId, $childId, $month)
    {
        global $db;
        $firstDateOfMonth = toDBDate("01/" . $month);
        $firstDateOfNextMonth = addMonthToDBDate($firstDateOfMonth);

        $strSql = sprintf("SELECT COUNT(deduction_date) AS deduction_count FROM ci_service_deduction SD
                            WHERE child_id = %s AND service_id = %s AND deduction_date >= %s AND deduction_date <= %s",
                        secure($childId, 'int'), secure($serviceId, 'int'), secure($firstDateOfMonth), secure($firstDateOfNextMonth));

        $get_deduction = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_deduction->num_rows > 0) {
            return $get_deduction->fetch_assoc()['deduction_count'];
        }
        return 0;
    }

    /**
     * Lấy ra số tiền còn nợ của trẻ trong một tháng thu học phí.
     *
     * @param $childId
     * @param $month
     * @return int
     * @throws Exception
     */
    private function getDebtAmountOfChildInMonth($childId, $month)
    {
        global $db;
        $firstDateOfMonth = toDBDate("01/" . $month);
        $strSql = sprintf('SELECT TC.paid_amount, TC.total_amount FROM ci_tuition_child TC INNER JOIN ci_tuition T ON TC.tuition_id = T.tuition_id AND T.month = %s AND child_id = %s',
                            secure($firstDateOfMonth), secure($childId, 'int'));

        $get_debt = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_debt->num_rows > 0) {
            $tuitionChild = $get_debt->fetch_assoc();
            return ($tuitionChild['total_amount'] - $tuitionChild['paid_amount']);
        }
        return 0;
    }


    /**
     * Lấy ra danh sách dịch vụ tính theo số lần sử dụng trẻ sử dụng trong tháng (bao gồm cả những dịch vụ mà trẻ đã hủy trong tháng).
     * Dùng: lấy của tháng trước để tính học phí.
     *
     * @param $schoolId
     * @param $childId
     * @param $month
     * @return array
     * @throws Exception
     */
    private function getUsedCountBasedServicesOfChildInMonth($schoolId, $childId, $month)
    {
        global $db;
        $firstDateOfMonth = toDBDate("01/" . $month);
        $firstDateOfNextMonth = addMonthToDBDate($firstDateOfMonth);

        $strSql = sprintf('(SELECT SU.service_id, S.service_name, S.fee, S.type AS service_type, COUNT(SU.service_id) AS CNT FROM ci_service_usage SU INNER JOIN ci_service S
                            ON SU.service_id = S.service_id AND S.type = %1$s AND SU.child_id = %2$s AND SU.using_at >= %3$s AND SU.using_at < %4$s AND S.school_id = %5$s GROUP BY SU.service_id)',
            SERVICE_TYPE_COUNT_BASED, secure($childId, 'int'), secure($firstDateOfMonth), secure($firstDateOfNextMonth), secure($schoolId, 'int'));

        $childServices = array();
        $get_childServices = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_childServices->num_rows > 0) {
            while ($childService = $get_childServices->fetch_assoc()) {
                $childServices[] = $childService;
            }
        }
        return $childServices;
    }

    /**
     * Lấy ra danh sách dịch vụ tính theo số lần sử dụng trẻ sử dụng trong khoảng thời gian (bao gồm cả những dịch vụ mà trẻ đã hủy trong khoảng đó).
     * Dùng: lấy của tháng trước để quyết toán thôi học.
     *
     * @param $schoolId
     * @param $childId
     * @param $begin
     * @param $end
     * @return array
     * @throws Exception
     */
    private function getUsedCountBasedServicesOfChildInDuration($schoolId, $childId, $begin, $end)
    {
        global $db;
        $begin = toDBDate($begin);
        $end = toDBDate($end);

        $strSql = sprintf('(SELECT SU.service_id, S.service_name, S.fee, S.type, COUNT(SU.service_id) AS CNT FROM ci_service_usage SU INNER JOIN ci_service S
                            ON SU.service_id = S.service_id AND S.type = %1$s AND SU.child_id = %2$s AND SU.using_at >= %3$s AND SU.using_at <= %4$s AND S.school_id = %5$s GROUP BY SU.service_id)',
            SERVICE_TYPE_COUNT_BASED, secure($childId, 'int'), secure($begin), secure($end), secure($schoolId, 'int'));

        $childServices = array();
        $get_childServices = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_childServices->num_rows > 0) {
            while ($childService = $get_childServices->fetch_assoc()) {
                $childServices[] = $childService;
            }
        }
        return $childServices;
    }

    /**
     * Lấy ra dịch vụ đón muộn tính theo tháng
     *
     * @param $schoolId
     * @param $childId
     * @param $month
     * @return array
     * @throws Exception
     */
    private function getChildPickupInMonth($schoolId, $childId, $month)
    {
        global $db;
        $firstDateOfMonth = toDBDate("01/" . $month);
        $firstDateOfNextMonth = addMonthToDBDate($firstDateOfMonth);

        // Lấy ra tiền đón muộn mà trẻ sử dụng.
        $strSql = sprintf("SELECT PC.total_amount FROM ci_pickup P
                           INNER JOIN ci_pickup_child PC ON P.pickup_id = PC.pickup_id AND PC.school_id = %s 
                           AND PC.child_id =%s AND P.pickup_time >= %s AND P.pickup_time < %s",
            secure($schoolId, 'int'), secure($childId, 'int'), secure($firstDateOfMonth), secure($firstDateOfNextMonth));

        $total = 0;
        $get_pickup_fee = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_pickup_fee->num_rows > 0) {
            while ($pickup_fee = $get_pickup_fee->fetch_assoc()) {
                $total += $pickup_fee['total_amount'];
            }
        }
        return $total;
    }

    /**
     * Lấy ra dịch vụ đón muộn tính trong khoảng thời gian
     *
     * @param $schoolId
     * @param $childId
     * @param $begin
     * @param $end
     * @return int
     * @throws Exception
     */
    private function getChildPickupInDuration($schoolId, $childId, $begin, $end)
    {
        global $db;
        $begin = toDBDate($begin);
        $end = toDBDate($end);
        // Lấy ra tiền đón muộn mà trẻ sử dụng.
        $strSql = sprintf("SELECT PC.total_amount FROM ci_pickup P
                           INNER JOIN ci_pickup_child PC ON P.pickup_id = PC.pickup_id AND PC.school_id = %s
                           AND PC.child_id =%s AND P.pickup_time >= %s AND P.pickup_time <= %s",
            secure($schoolId, 'int'), secure($childId, 'int'), secure($begin), secure($end));

        $total = 0;
        $get_pickup_fee = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        while ($pickup_fee = $get_pickup_fee->fetch_assoc()) {
            $total += $pickup_fee['total_amount'];
        }

        return $total;
    }


    /**
     * Xóa một loại phí ra khỏi hệ thống nếu fee đó chưa được sử dụng
     *
     * @param $fee_id
     * @throws Exception
     */
    public function deleteFee($fee_id)
    {
        global $db;
        // Check xem fee_id có trong ci_tuition_detail chưa
        $strSql = sprintf("SELECT fee_id FROM ci_tuition_detail WHERE fee_id = %s", secure($fee_id, 'int'));
        $getFee = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($getFee->num_rows == 0) {
            /* delete this fee */
            $db->query(sprintf("DELETE FROM ci_fee WHERE fee_id = %s", secure($fee_id, 'int'))) or _error(SQL_ERROR_THROWEN);
            /* delete fee_child */
            $db->query(sprintf("DELETE FROM ci_fee_child WHERE fee_id = %s", secure($fee_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        } else {
            throw new Exception("This fee has already been used");
        }
    }

    /**
     * Lấy toàn bộ danh sách học phí trong tháng của trường
     *
     * @param $school_id
     * @param $month
     * @return array
     * @throws Exception
     */
    public function getAllTuitionOfMonth($school_id, $month)
    {
        global $db;

        $monthDb = toDBDate("01/" . $month);
        $strSql = sprintf("SELECT tuition_id, class_id, paid_count, total_amount, paid_amount FROM ci_tuition WHERE school_id = %s AND month = %s", secure($school_id, 'int'), secure($monthDb));
        $get_tuitions = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $tuitions = array();
        if ($get_tuitions->num_rows > 0) {
            while ($tuition = $get_tuitions->fetch_assoc()) {
                $tuitions[] = $tuition;
            }
        }
        return $tuitions;
    }

    /**
     * Lấy ra mảng tuition_child_id từ tuition_id
     *
     * @param $tuitionId
     * @return array
     * @throws Exception
     */
    public function getTuitionsChildIdsFromTuitionId($tuitionId)
    {
        global $db;

        $strSql = sprintf("SELECT tuition_child_id FROM ci_tuition_child WHERE tuition_id", secure($tuitionId, 'int'));
        $tuitions = array();
        $get_tuitions = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_tuitions->num_rows > 0) {
            while ($tuitionChild = $get_tuitions->fetch_assoc()) {
                $tuitions[] = $tuitionChild['tuition_child_id'];
            }
        }
        return $tuitions;
    }

    /** Lấy danh sách tổng hợp đóng học phí theo điều kiện truyền vào
     *
     * @param $schoolId
     * @param $fromDate
     * @param $toDate
     * @param string $cashier_id
     * @param int $status
     * @return array
     * @throws Exception
     */
    public function getTuitionSummaryPaid($schoolId, $fromDate, $toDate, $cashier_id = '', $status = 2, $classId) {
        global $db;

        if(!isset($fromDate) || !isset($toDate) || !validateDate($fromDate) || !validateDate($toDate)) {
            throw new Exception(__("You must choose begin date and end date"));
        }

        if($status == '') {
            throw new Exception(__("Please select a payment status"));
        }

        if($cashier_id != '' && $status != 2) {
            throw new Exception(__("Nếu đã chọn người thanh toán thì bạn không thể chọn trạng thái chờ xác nhận và chưa thanh toán, vui lòng chọn lại."));
        }
        $fromDate = toDBDate($fromDate);
        $toDate = toDBDate($toDate);

        if($status == 2) {
            if($cashier_id == '') {
                if(is_numeric($classId)) {
                    $strSql = sprintf("SELECT C.child_name, TC.*, T.month, T.class_id, U.user_fullname, G.group_title FROM
                ci_child C INNER JOIN ci_tuition_child TC ON C.child_id = TC.child_id
                INNER JOIN ci_tuition T ON TC.tuition_id = T.tuition_id
                INNER JOIN users U ON U.user_id = TC.cashier_id
                INNER JOIN groups G ON G.group_id = T.class_id
                WHERE TC.status = %s AND DATE(TC.paid_at) >= %s AND DATE(TC.paid_at) <= %s AND T.school_id = %s AND T.class_id = %s GROUP BY T.class_id ASC, TC.paid_at DESC, T.month ASC, C.name_for_sort ASC",
                        secure($status, 'int'), secure($fromDate), secure($toDate), secure($schoolId, 'int'), secure($classId, 'int'));
                } else {
                    $strSql = sprintf("SELECT C.child_name, TC.*, T.month, T.class_id, U.user_fullname, G.group_title FROM
                ci_child C INNER JOIN ci_tuition_child TC ON C.child_id = TC.child_id
                INNER JOIN ci_tuition T ON TC.tuition_id = T.tuition_id
                LEFT JOIN users U ON U.user_id = TC.cashier_id
                INNER JOIN groups G ON G.group_id = T.class_id
                WHERE TC.status = %s AND DATE(TC.paid_at) >= %s AND DATE(TC.paid_at) <= %s AND T.school_id = %s GROUP BY T.class_id ASC, TC.paid_at DESC, T.month ASC, C.name_for_sort ASC",
                        secure($status, 'int'), secure($fromDate), secure($toDate), secure($schoolId, 'int'));
                }
            } else {
                if(is_numeric($classId)) {
                    $strSql = sprintf("SELECT C.child_name, TC.*, T.month, T.class_id, U.user_fullname, G.group_title FROM
                ci_child C INNER JOIN ci_tuition_child TC ON C.child_id = TC.child_id
                INNER JOIN ci_tuition T ON TC.tuition_id = T.tuition_id
                INNER JOIN users U ON U.user_id = TC.cashier_id
                INNER JOIN groups G ON G.group_id = T.class_id
                WHERE TC.status = %s AND DATE(TC.paid_at) >= %s AND DATE(TC.paid_at) <= %s AND T.school_id = %s AND TC.cashier_id = %s AND T.class_id = %s GROUP BY T.class_id ASC, TC.paid_at DESC, T.month ASC, C.name_for_sort ASC",
                        secure($status, 'int'), secure($fromDate), secure($toDate), secure($schoolId, 'int'), secure($cashier_id, 'int'), secure($classId, 'int'));
                } else {
                    $strSql = sprintf("SELECT C.child_name, TC.*, T.month, T.class_id, U.user_fullname, G.group_title FROM
                ci_child C INNER JOIN ci_tuition_child TC ON C.child_id = TC.child_id
                INNER JOIN ci_tuition T ON TC.tuition_id = T.tuition_id
                INNER JOIN users U ON U.user_id = TC.cashier_id
                INNER JOIN groups G ON G.group_id = T.class_id
                WHERE TC.status = %s AND DATE(TC.paid_at) >= %s AND DATE(TC.paid_at) <= %s AND T.school_id = %s AND TC.cashier_id = %s GROUP BY T.class_id ASC, TC.paid_at DESC, T.month ASC, C.name_for_sort ASC",
                        secure($status, 'int'), secure($fromDate), secure($toDate), secure($schoolId, 'int'), secure($cashier_id, 'int'));
                }
            }
        } else {
            if(is_numeric($classId)) {
                $strSql = sprintf("SELECT C.child_name, TC.*, T.month, T.class_id, G.group_title FROM
                ci_child C 
                INNER JOIN ci_tuition_child TC ON C.child_id = TC.child_id
                INNER JOIN ci_tuition T ON TC.tuition_id = T.tuition_id
                INNER JOIN groups G ON G.group_id = T.class_id
                WHERE TC.status = %s AND T.school_id = %s AND T.class_id = %s GROUP BY T.class_id ASC, T.month ASC, C.name_for_sort ASC",
                    secure($status, 'int'), secure($schoolId, 'int'), secure($classId, 'int'));
            } else {
                $strSql = sprintf("SELECT C.child_name, TC.*, T.month, T.class_id, G.group_title FROM
                ci_child C 
                INNER JOIN ci_tuition_child TC ON C.child_id = TC.child_id
                INNER JOIN ci_tuition T ON TC.tuition_id = T.tuition_id
                INNER JOIN groups G ON G.group_id = T.class_id
                WHERE TC.status = %s AND T.school_id = %s GROUP BY T.class_id ASC, T.month ASC, C.name_for_sort ASC",
                    secure($status, 'int'), secure($schoolId, 'int'));
            }
        }

        $get_child = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $children = array();
        if($get_child->num_rows > 0) {
            while ($child = $get_child->fetch_assoc()) {
                $child['month'] = toSysMMYYYY($child['month']);
                if($child['paid_at'] != null) {
                    $child['paid_at'] = toSysDatetime($child['paid_at']);
                }
                $children[] = $child;
            }
        }
        return $children;
    }
    /**
     * Lấy toàn bộ danh sách học phí tháng của trường cho API
     *
     * @param $school_id
     * @param $month
     * @return array
     * @throws Exception
     */
    public function getAllTuitionOfMonthForAPI($school_id, $month)
    {
        global $db;

        $monthDb = toDBDate("01/" . $month);
        $strSql = sprintf("SELECT G.group_title, T.paid_count, T.total_amount, T.paid_amount FROM ci_tuition T INNER JOIN groups G ON G.group_id = T.class_id
                    WHERE T.school_id = %s AND T.month = %s", secure($school_id, 'int'), secure($monthDb));
        $get_tuitions = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $tuitions = array();
        if ($get_tuitions->num_rows > 0) {
            while ($tuition = $get_tuitions->fetch_assoc()) {
                $tuitions[] = $tuition;
            }
        }
        return $tuitions;
    }

    public function getTuitionChildId($childId, $month) {
        global $db;

        $strSql = sprintf("SELECT TC.tuition_child_id FROM ci_tuition_child TC 
              INNER JOIN ci_tuition T ON T.tuition_id = TC.tuition_id
              WHERE child_id = %s AND T.month = %s", secure($childId, 'int'), secure($month));

        $getTuitionChildId = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $tuitionChildId = null;
        if ($getTuitionChildId->num_rows > 0) {
            $tuitionChildId = $getTuitionChildId->fetch_assoc()['tuition_child_id'];
        }
        return $tuitionChildId;
    }

    /* ---------- CI - API ---------- */

    /**
     * Lấy tất cả học phí của một lớp. Để hiển thị danh sách học phí trên mobile.
     *
     * @param $class_id
     * @param int $type
     * @return array
     */
    public function getClassTuitionsForApi($class_id, $type = TUITION_TYPE_MONTHLY, $offset = 0)
    {
        global $db, $system;
        $offset *= $system['max_results'];

        $strSql = sprintf("SELECT tuition_id, paid_amount, total_amount, paid_count, month FROM ci_tuition
                            WHERE class_id = %s AND is_notified = 1 AND type = %s ORDER BY status ASC, month DESC LIMIT %s, %s",
                            secure($class_id, 'int'), secure($type, 'int'), secure($offset, 'int', false), secure($system['max_results'], 'int', false));
        $tuitions = array();
        $get_tuitions = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_tuitions->num_rows > 0) {
            while ($tuition = $get_tuitions->fetch_assoc()) {
                $strSql = sprintf("SELECT COUNT(tuition_child_id) AS count_child FROM ci_tuition_child WHERE tuition_id = %s", secure($tuition['tuition_id'], 'int'));
                $get_count = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
                $tuition['count_child'] = $get_count->fetch_assoc()['count_child'];
                $tuition['month'] = toSysMMYYYY($tuition['month']);
                $tuitions[] = $tuition;
            }
        }
        return $tuitions;
    }

    /**
     * Lấy ra danh sách học phí của trẻ trong lớp. KHÔNG lấy chi tiết từng trẻ.
     *
     * @param $tuitionId
     * @param $schoolId
     * @return array|null
     */
    public function getTuitionChildForApi($tuitionId, $schoolId)
    {
        global $db;

        //Lấy thông tin tổng hợp học phí của một trẻ.
        $strSql = sprintf("SELECT TC.tuition_child_id, TC.total_amount, TC.status, C.child_name, C.birthday FROM ci_tuition_child TC
                        INNER JOIN ci_child C ON TC.child_id = C.child_id AND TC.tuition_id = %s
                        INNER JOIN ci_school_child SC ON SC.child_id = C.child_id AND SC.school_id = %s
                        ORDER BY C.name_for_sort ASC", secure($tuitionId, 'int'), secure($schoolId, 'int'));

        $tuitionChildren = array();
        $get_tuition_children = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        while ($tuitionChild = $get_tuition_children->fetch_assoc()) {
            $tuitionChild['birthday'] = toSysDate($tuitionChild['birthday']);
            $tuitionChildren[] = $tuitionChild;
        }

        return $tuitionChildren;
    }

    /**
     * Chi tiết học phí của một trẻ. Hiển thị trên MOBILE
     *
     * @param $tuitionChildId
     * @return array
     */
    public function getTuitionDetailOfChildForApi($tuitionChildId)
    {
        return $this->getTuitionDetail4Detail($tuitionChildId);
    }

    /**
     * Lấy ra thông tin sử dụng dịch vụ tháng trước của một trẻ.
     *
     * @param $tuitionChildId
     * @return array
     * @throws Exception
     */
    public function getUsageHistoryOfChildForApi($tuitionChildId)
    {
        global $db;
        $strSql = sprintf("SELECT child_id, pre_month FROM ci_tuition_child WHERE tuition_child_id = %s", secure($tuitionChildId, 'int'));
        $get_tuition_child = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        if ($get_tuition_child->num_rows > 0) {
            $tuitionChild = $get_tuition_child->fetch_assoc();
            $preMonth = toSysMMYYYY($tuitionChild['pre_month']);
            $attendanceCount = 0;
            if (!empty($preMonth)) {
                $attendanceCount = $this->getChildAttendanceCount($tuitionChild['child_id'], $preMonth);
            }
            return $this->getTuitionDetail4History($tuitionChildId, $attendanceCount);
        }

        return array();
    }

    /**
     * Lấy ra danh sách học phí của một trẻ. Hiển thị danh sách học phí trẻ trên tài khoản mobile của phụ huynh.
     *
     * @param $childId
     * @param int $offset
     * @return array
     * @throws Exception
     */
    public function getTuitionsOfChildForApi($childId, $offset = 0)
    {
        global $db, $system;
        $offset *= $system['max_results'];

        $strSql = sprintf("SELECT TC.tuition_child_id, TC.tuition_id, TC.total_amount, TC.paid_amount, T.month FROM ci_tuition_child TC
                           INNER JOIN ci_tuition T ON TC.tuition_id = T.tuition_id WHERE TC.child_id = %s AND T.is_notified = 1 ORDER BY T.month DESC, TC.status ASC LIMIT %s, %s",
            secure($childId, 'int'), secure($offset, 'int', false), secure($system['max_results'], 'int', false));
        $tuitionChilds = array();
        $get_tuition_childs = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_tuition_childs->num_rows > 0) {
            while ($tuitionChild = $get_tuition_childs->fetch_assoc()) {
                $tuitionChilds[] = $tuitionChild;
            }
        }
        return $tuitionChilds;
    }

    /**
     * Lấy tuition_id từ tuition_child_id
     *
     * @param $tuitionChildId
     * @return int
     * @throws Exception
     */
    public function getTuitionIdFromTuitionChildId($tuitionChildId) {
        global $db;

        $strSql = sprintf("SELECT tuition_id FROM ci_tuition_child WHERE tuition_child_id = %s", secure($tuitionChildId));

        $get_tuition_id = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $tuition_id = 0;
        if ($get_tuition_id->num_rows > 0) {
            $tuition_id = $get_tuition_id->fetch_assoc()['tuition_id'];
        }

        return $tuition_id;
    }


    /* ---------- END - API ---------- */
}

?>
