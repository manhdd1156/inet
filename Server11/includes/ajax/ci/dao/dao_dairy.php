<?php
/**
 * DAO -> Diary
 * Thao tác với bảng diary_template và diary
 * 
 * @package ConIu
 * @author QuanND
 */

class DiaryDAO {
    /**
     * __construct
     *
     */
    public function __construct() {
    }

    /**
     * Nhập thông tin của một template vào hệ thống
     *
     * @param $template_name
     * @param $school_id
     * @param $class_level_id
     * @param $times
     * @param $activities
     * @return mixed
     * @throws Exception
     */
    public function insertTemplate($template_name, $school_id, $class_level_id, $times, $activities) {
        global $db;

        $this->_validateTemplateInput($template_name, $school_id, $class_level_id, $times, $activities);
        //Nhập thông tin của template
        $strSql = sprintf("INSERT INTO ci_diary_template (template_name, school_id, class_level_id) VALUES (%s, %s, %s)",
            secure($template_name), secure($school_id, 'int'), secure($class_level_id, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $template_id = $db->insert_id;

        $strValues = "";
        $idx = 0;
        foreach ($times as $time) {
            //diary_template_id, time_of_day, activity
            $strValues .= "(". secure($template_id, 'int')  .",". $time .",". secure($activities[$idx++]) ."),";
        }
        $strValues = trim($strValues, ",");

        //Nhập danh sách item của template
        $strSql = "INSERT INTO ci_diary_template_item (diary_template_id, time_of_day, activity) VALUES ".$strValues;
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        return $template_id;
    }

    /**
     * Validate thông tin đầu vào khi inset/update template
     *
     * @param $template_name
     * @param $class_level_id
     * @param $activities
     * @throws Exception
     */
    private function _validateTemplateInput($template_name, $class_level_id, $activities) {
        if (strlen($template_name) == 0) {
            throw new Exception(__("You must enter template name"));
        }

        if ($class_level_id <= 0) {
            throw new Exception(__("You must enter class level"));
        }

        if (count($activities) == 0) {
            throw new Exception(__("You must enter at lest one activity"));
        }
    }
}
?>