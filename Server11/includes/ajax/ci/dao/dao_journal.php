<?php

/**
 * Class MedicalDAO: thao tác với bảng ci_child_journal của hệ thống.
 */
class JournalDAO
{
    /**
     * __construct
     *
     */
    public function __construct()
    {
    }

    /**
     * Thêm ảnh vào album nhật ký trẻ
     *
     * @param $journalAlbumId
     * @param $return_files
     * @throws Exception
     */
    public function insertJournal($journalAlbumId, $return_files) {
        global $db;

        $strValues = "";
        foreach ($return_files as $value) {
            $strValues .= "(" . secure($journalAlbumId, 'int') . "," . secure($value) . "),";
        }
        $strValues = trim($strValues, ",");

        if (!is_empty($strValues)) {
            $strSql = sprintf("INSERT INTO ci_child_journal (child_journal_album_id, source_file) VALUES " . $strValues);
            $db->query($strSql) or _error('error', $strSql);
        }
    }

    /**
     * Lấy tất cả nhật ký của 1 trẻ
     *
     * @param $child_parent_id
     * @return array
     */
    public function getAllJournalChild($child_parent_id) {
        global $db, $system;

        $strSql = sprintf("SELECT * FROM ci_child_journal_album WHERE child_parent_id = %s ORDER BY created_at DESC, child_journal_album_id DESC", secure($child_parent_id, 'int'));

        $get_journal = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $journals = array();
        if($get_journal->num_rows > 0) {
            while ($journal = $get_journal->fetch_assoc()) {
                // Lấy danh sách ảnh có trong album
                $pictures = $this->_getPictureOfAlbum($journal['child_journal_album_id']);
                $journal['created_at'] = toSysDate($journal['created_at']);
                if($journal['updated_at'] != null) {
                    $journal['updated_at'] = toSysDate($journal['updated_at']);
                }
                $journal['pictures'] = $pictures;
                $journals[] = $journal;
            }
        }

        return $journals;
    }

    /**
     * Lấy tất cả nhật ký của 1 trẻ (trường tạo)
     *
     * @param $child_parent_id
     * @return array
     */
    public function getAllJournalChildOfSchool($child_parent_id) {
        global $db, $system;

        $strSql = sprintf("SELECT * FROM ci_child_journal_album WHERE child_parent_id = %s AND is_parent = 0 ORDER BY created_at DESC, child_journal_album_id DESC", secure($child_parent_id, 'int'));

        $get_journal = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $journals = array();
        if($get_journal->num_rows > 0) {
            while ($journal = $get_journal->fetch_assoc()) {
                // Lấy danh sách ảnh có trong album
                $pictures = $this->_getPictureOfAlbum($journal['child_journal_album_id']);
                $journal['created_at'] = toSysDate($journal['created_at']);
                if($journal['updated_at'] != null) {
                    $journal['updated_at'] = toSysDate($journal['updated_at']);
                }
                $journal['pictures'] = $pictures;
                $journals[] = $journal;
            }
        }

        return $journals;
    }

    /**
     * Lấy tất cả nhật ký của 1 trẻ cho API
     *
     * @param $child_parent_id
     * @return array
     */
    public function getAllJournalChildForAPI($child_parent_id) {
        global $db, $system;

        $strSql = sprintf("SELECT * FROM ci_child_journal_album WHERE child_parent_id = %s ORDER BY created_at DESC, child_journal_album_id DESC", secure($child_parent_id, 'int'));

        $get_journal = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $journals = array();
        if($get_journal->num_rows > 0) {
            while ($journal = $get_journal->fetch_assoc()) {
                // Lấy danh sách ảnh có trong album
                $pictures = $this->_getPictureOfAlbum($journal['child_journal_album_id']);
                $thumbnail = $this->_getThumbnailOfAlbum($journal['child_journal_album_id']);
                $journal['created_at'] = toSysDate($journal['created_at']);
                if($journal['updated_at'] != null) {
                    $journal['updated_at'] = toSysDate($journal['updated_at']);
                }
                $journal['image_count'] = count($pictures);
                $journal['thumbnail'] = $thumbnail;
                $journals[] = $journal;
            }
        }

        return $journals;
    }

    /**
     * Lấy tất cả nhật ký của 1 trẻ cho API
     *
     * @param $child_parent_id
     * @return array
     */
    public function getAllJournalChildOfSchoolForAPI($child_parent_id) {
        global $db, $system;

        $strSql = sprintf("SELECT * FROM ci_child_journal_album WHERE child_parent_id = %s AND is_parent = 0 ORDER BY created_at DESC, child_journal_album_id DESC", secure($child_parent_id, 'int'));

        $get_journal = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $journals = array();
        if($get_journal->num_rows > 0) {
            while ($journal = $get_journal->fetch_assoc()) {
                // Lấy danh sách ảnh có trong album
                $pictures = $this->_getPictureOfAlbum($journal['child_journal_album_id']);
                $thumbnail = $this->_getThumbnailOfAlbum($journal['child_journal_album_id']);
                $journal['created_at'] = toSysDate($journal['created_at']);
                if($journal['updated_at'] != null) {
                    $journal['updated_at'] = toSysDate($journal['updated_at']);
                }
                $journal['image_count'] = count($pictures);
                $journal['thumbnail'] = $thumbnail;
                $journals[] = $journal;
            }
        }

        return $journals;
    }

    /**
     * Lấy nhật ký theo năm truyền vào
     *
     * @param $child_parent_id
     * @param $year
     * @return array
     */
    public function getAllJournalChildOfYear($child_parent_id, $year) {
        global $db, $system;

        $strSql = sprintf("SELECT * FROM ci_child_journal_album WHERE YEAR(created_at) = %s AND child_parent_id = %s ORDER BY created_at DESC, child_journal_album_id DESC", secure($year), secure($child_parent_id, 'int'));

        $get_journal = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $journals = array();
        if($get_journal->num_rows > 0) {
            while ($journal = $get_journal->fetch_assoc()) {
                // Lấy danh sách ảnh có trong album
                $pictures = $this->_getPictureOfAlbum($journal['child_journal_album_id']);
                $journal['created_at'] = toSysDate($journal['created_at']);
                if($journal['updated_at'] != null) {
                    $journal['updated_at'] = toSysDate($journal['updated_at']);
                }
                $journal['pictures'] = $pictures;
                $journals[] = $journal;
            }
        }

        return $journals;
    }

    /**
     * Lấy nhật ký theo năm truyền vào (lớp)
     *
     * @param $child_parent_id
     * @param $year
     * @return array
     */
    public function getAllJournalChildForSchoolOfYear($child_parent_id, $year) {
        global $db, $system;

        if($year == 0) {
            $strSql = sprintf("SELECT * FROM ci_child_journal_album WHERE child_parent_id = %s AND is_parent = 0 ORDER BY created_at DESC, child_journal_album_id DESC", secure($child_parent_id, 'int'));
        } else {
            $strSql = sprintf("SELECT * FROM ci_child_journal_album WHERE YEAR(created_at) = %s AND child_parent_id = %s AND is_parent = 0 ORDER BY created_at DESC, child_journal_album_id DESC", secure($year), secure($child_parent_id, 'int'));
        }

        $get_journal = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $journals = array();
        if($get_journal->num_rows > 0) {
            while ($journal = $get_journal->fetch_assoc()) {
                // Lấy danh sách ảnh có trong album
                $pictures = $this->_getPictureOfAlbum($journal['child_journal_album_id']);
                $journal['created_at'] = toSysDate($journal['created_at']);
                if($journal['updated_at'] != null) {
                    $journal['updated_at'] = toSysDate($journal['updated_at']);
                }
                $journal['pictures'] = $pictures;
                $journals[] = $journal;
            }
        }

        return $journals;
    }

    /**
     * Lấy nhật ký theo năm truyền vào
     *
     * @param $child_parent_id
     * @param $year
     * @return array
     */
    public function getAllJournalChildOfYearForAPI($child_parent_id, $year) {
        global $db, $system;

        $strSql = sprintf("SELECT * FROM ci_child_journal_album WHERE YEAR(created_at) = %s AND child_parent_id = %s ORDER BY created_at DESC, child_journal_album_id DESC", secure($year), secure($child_parent_id, 'int'));

        $get_journal = $db->query($strSql) or _error('error', $strSql);
        $journals = array();
        if($get_journal->num_rows > 0) {
            while ($journal = $get_journal->fetch_assoc()) {
                // Lấy danh sách ảnh có trong album
                // Lấy danh sách ảnh có trong album
                $pictures = $this->_getPictureOfAlbum($journal['child_journal_album_id']);
                $thumbnail = $this->_getThumbnailOfAlbum($journal['child_journal_album_id']);
                $journal['created_at'] = toSysDate($journal['created_at']);
                if($journal['updated_at'] != null) {
                    $journal['updated_at'] = toSysDate($journal['updated_at']);
                }
                $journal['image_count'] = count($pictures);
                $journal['thumbnail'] = $thumbnail;
                $journals[] = $journal;
            }
        }

        return $journals;
    }

    /**
     * Lấy nhật ký theo năm truyền vào bên lớp
     *
     * @param $child_parent_id
     * @param $year
     * @return array
     */
    public function getAllJournalChildOfYearOfSchoolForAPI($child_parent_id, $year) {
        global $db, $system;

        $strSql = sprintf("SELECT * FROM ci_child_journal_album WHERE YEAR(created_at) = %s AND child_parent_id = %s AND is_parent = 0 ORDER BY created_at DESC, child_journal_album_id DESC", secure($year), secure($child_parent_id, 'int'));

        $get_journal = $db->query($strSql) or _error('error', $strSql);
        $journals = array();
        if($get_journal->num_rows > 0) {
            while ($journal = $get_journal->fetch_assoc()) {
                // Lấy danh sách ảnh có trong album
                // Lấy danh sách ảnh có trong album
                $pictures = $this->_getPictureOfAlbum($journal['child_journal_album_id']);
                $thumbnail = $this->_getThumbnailOfAlbum($journal['child_journal_album_id']);
                $journal['created_at'] = toSysDate($journal['created_at']);
                if($journal['updated_at'] != null) {
                    $journal['updated_at'] = toSysDate($journal['updated_at']);
                }
                $journal['image_count'] = count($pictures);
                $journal['thumbnail'] = $thumbnail;
                $journals[] = $journal;
            }
        }

        return $journals;
    }

    /**
     * Update nhật ký
     *
     * @param $args
     */
    public function updateJournal ($args) {
        global $db, $date;

        if($args['source_file'] == null) {
            throw new Exception(__("You must add at least one image"));
        }
        $strSql = sprintf("UPDATE ci_child_journal SET caption = %s, source_file = %s, updated_at = %s WHERE child_journal_id = %s", secure($args['caption']), secure($args['source_file']), secure($date), secure($args['child_journal_id'], 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Lấy chi tiết nhật ký
     *
     * @param $child_journal_id
     * @return null
     */
    public function getJournalById($child_journal_album_id) {
        global $db, $system;

        $strSql = sprintf("SELECT * FROM ci_child_journal_album WHERE child_journal_album_id = %s", secure($child_journal_album_id, 'int'));

        $get_journal = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $journal = null;
        if($get_journal->num_rows > 0) {
            $journal = $get_journal->fetch_assoc();
            $pictures = $this->_getPictureOfAlbum($journal['child_journal_album_id']);
            $journal['created_at'] = toSysDate($journal['created_at']);
            if($journal['updated_at'] != null) {
                $journal['updated_at'] = toSysDate($journal['updated_at']);
            }
            $journal['pictures'] = $pictures;
        }

        return $journal;
    }

    /**
     * Xóa ảnh trong nhật ký
     *
     * @param $child_journal_id
     */
    public function deleteJournal($child_journal_id) {
        global $db;

        $strSql = sprintf("DELETE FROM ci_child_journal WHERE child_journal_id = %s", secure($child_journal_id, 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Xóa album nhật ký
     *
     * @param $child_journal_album_id
     */
    public function deleteJournalAlbum($child_journal_album_id) {
        global $db;

        $strSql = sprintf("DELETE FROM ci_child_journal_album WHERE child_journal_album_id = %s", secure($child_journal_album_id, 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        // Xóa các ảnh trong album
        $sqlStr = sprintf("DELETE FROM ci_child_journal WHERE child_journal_album_id = %s", secure($child_journal_album_id, 'int'));

        $db->query($sqlStr) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Xóa 1 list nhật ký
     *
     * @param $journalIds
     */
    public function deleteJournalList($journalIds) {
        global $db;

        $strCon = implode(',', $journalIds);
        $strSql = sprintf("DELETE FROM ci_child_journal WHERE child_journal_id IN (%s)", $strCon);

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Thêm album nhật ký của trẻ
     *
     * @param $childParentId
     * @param $caption
     * @param $isParent
     * @return mixed
     * @throws Exception
     */
    public function insertJournalAlbum($childParentId, $caption, $isParent) {
        global $db, $user, $date;

        $this->_validateDiary($caption);

        $strSql = sprintf("INSERT INTO ci_child_journal_album (child_parent_id, caption, created_at, created_user_id, is_parent) VALUES (%s, %s, %s, %s, %s)", secure($childParentId, 'int'), secure($caption), secure($date), secure($user->_data['user_id']), $isParent);

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        return $db->insert_id;
    }

    private function _getPictureOfAlbum($child_journal_album_id) {
        global $db, $system;

        $strSql = sprintf("SELECT * FROM ci_child_journal WHERE child_journal_album_id = %s", secure($child_journal_album_id));

        $get_picture = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        $pictures = array();
        if($get_picture->num_rows > 0) {
            while($picture = $get_picture->fetch_assoc()) {
                if($picture['source_file'] != null) {
                    $picture['source_file_path'] = $picture['source_file'];
                    $picture['source_file'] = $system['system_uploads'] . '/' . $picture['source_file'];
                }
                $pictures[] = $picture;
            }
        }
        return $pictures;
    }

    /**
     * Lấy thumbnail album
     *
     * @param $child_journal_album_id
     * @return null|string
     * @throws Exception
     */
    private function _getThumbnailOfAlbum($child_journal_album_id) {
        global $db, $system;

        $strSql = sprintf("SELECT * FROM ci_child_journal WHERE child_journal_album_id = %s ORDER BY child_journal_id ASC LIMIT 0,1", secure($child_journal_album_id));

        $get_picture = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        $thumbnail = null;
        if($get_picture->num_rows > 0) {
            $picture = $get_picture->fetch_assoc();
            $thumbnail = $system['system_uploads'] . '/' . $picture['source_file'];
        }

        return $thumbnail;
    }

    /**
     * Sửa caption album
     *
     * @param $child_journal_album_id
     * @param $caption
     * @throws Exception
     */
    public function editCaptionAlbum($child_journal_album_id, $caption) {
        global $db;

        $this->_validateDiary($caption);

        $strSql = sprintf("UPDATE ci_child_journal_album SET caption = %s WHERE child_journal_album_id = %s", secure($caption), secure($child_journal_album_id, 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Thêm ảnh vào album
     *
     * @param $child_journal_album_id
     * @param $return_files
     * @throws Exception
     */
    public function addPhotoForAlbum($child_journal_album_id, $return_files) {
        global $db;

        $strValues = "";
        foreach ($return_files as $value) {
            $strValues .= "(" . secure($child_journal_album_id, 'int') . "," . secure($value) . "),";
        }
        $strValues = trim($strValues, ",");

        if (!is_empty($strValues)) {
            $strSql = sprintf("INSERT INTO ci_child_journal (child_journal_album_id, source_file) VALUES " . $strValues);
            $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        }
    }

    /**
     * Lấy album_id từ ảnh
     *
     * @param $child_journal_id
     * @return null
     * @throws Exception
     */
    public function getJournalAlbumIdFromJournalId($child_journal_id) {
        global $db;

        $strSql = sprintf("SELECT child_journal_album_id FROM ci_child_journal WHERE child_journal_id = %s", secure($child_journal_id));

        $get_album = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        $album_id = null;
        if($get_album->num_rows > 0) {
            $album_id = $get_album->fetch_assoc()['child_journal_album_id'];
        }

        return $album_id;
    }

    /**
     * validate caption
     *
     * @param $caption
     * @throws Exception
     */
    private function _validateDiary($caption) {
        if(strlen($caption) > 500) {
            throw new Exception (__("Title not greater than 500 characters"));
        }
    }
}
?>
