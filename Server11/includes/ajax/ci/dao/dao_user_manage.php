<?php
/**
 * DAO -> UserManageDAO
 * Thao tác với bảng ci_user_manage
 * 
 * @package ConIu
 * @author QuanND
 */

class UserManageDAO {
    /**
     * __construct
     *
     */
    public function __construct() {
    }

    /**
     * Lấy ra các đối tượng đang được một user quản lý
     *
     * @return array
     */
    public function getActiveUserManage($user_id) {
        global $db;
        $user_manages = array();
        $strSql = sprintf("SELECT * FROM ci_user_manage WHERE user_id = %s AND status = %s ORDER BY object_type ASC", secure($user_id, 'int'), STATUS_ACTIVE);
        $get_user_manages = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_user_manages->num_rows > 0) {
            while($user_manage = $get_user_manages->fetch_assoc()) {
                $user_manages[] = $user_manage;
            }
        }
        return $user_manages;
    }

    /**
     * Lấy ra vai trò của user với đối tượng
     *
     * @param $user_id
     * @param $object_id
     * @return int
     * @throws Exception
     */
    public function getRole($user_id, $object_id, $object_type) {
        global $db;
        $strSql = sprintf("SELECT role_id FROM ci_user_manage WHERE user_id = %s AND object_id = %s AND object_type = %s AND status = %s",
            secure($user_id, 'int'), secure($object_id, 'int'), secure($object_type, 'int'), STATUS_ACTIVE);

        $roles = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($roles->num_rows > 0) {
            return $roles->fetch_assoc()['role_id'];
        }

        return PERMISSION_NONE;
    }
    /**
     * Lấy ra danh sách các object mà user quản lý
     *
     * @param $user_id
     * @return int
     * @throws Exception
     */
    public function getRoleUserApi($user_id) {
        global $db;
        $strSql = sprintf("SELECT * FROM ci_user_manage WHERE user_id = %s",
            secure($user_id, 'int'));
        $results = array();
        $roles = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($roles->num_rows > 0) {
            while ($role = $roles->fetch_assoc()) {
                $results[] = $role;
            }
        }
        return $results;
    }
    /**
     * Kiểm tra đầu vào cho hàm insert và update
     *
     * @param array $args
     * @throws Exception
     */
    private function validateInput (array $args = array()) {
        if (is_empty($args['user_id']) || ($args['user_id'] == 0)) {
            throw new Exception(__("You must select an valid user"));
        }
        if (is_empty($args['object_id']) || ($args['object_id'] == 0)) {
            throw new Exception(__("You must select a valid object"));
        }
        if ((!is_empty($args['description'])) && (strlen($args['description']) > 300)) {
            throw new Exception(__("Description length must be less than 300 characters long"));
        }
    }

    /**
     * Insert một đối tượng được quản lý vào DB
     *
     * @param array $args
     * @throws Exception
     */
    public function insertUserManage(array $args = array()) {
        global $db;

        $this->validateInput($args);
        $strSql = sprintf("INSERT INTO ci_user_manage (user_id, object_id, object_type, status, role_id, description) VALUES (%s, %s, %s, %s, %s, %s)",
            secure($args['user_id'], 'int'), secure($args['object_id'], 'int'), secure($args['object_type'], 'int'),
            secure($args['status'], 'int'), secure($args['role_id'], 'int'), secure($args['description']));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }


    /**
     * Cập nhật một đối tượng quản lý trong DB
     *
     * @param array $args
     * @throws Exception
     */
    public function updateUserManage(array $args = array()) {
        global $db;

        $this->validateInput($args);
        $strSql = sprintf("UPDATE ci_user_manage SET user_id = %s, object_id = %s, object_type = %s, status = %s, role_id = %s, description = %s WHERE user_manage_id = %s",
            secure(secure($args['user_id'], 'int')), secure($args['object_id'], 'int'), secure($args['object_type'], 'int'),
            secure($args['status'], 'int'), secure($args['role_id'], 'int'), secure($args['description']), secure($args['user_manage_id'], 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Active một đối tượng quản lý
     *
     * @param array $args
     * @throws Exception
     */
    public function activeUserManage(array $args = array()) {
        global $db;

        $this->validateInput($args);
        $strSql = sprintf("UPDATE ci_user_manage SET status = %s WHERE user_manage_id = %s", STATUS_ACTIVE, secure($args['user_manage_id'], 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Deactive một đối tượng quản lý
     *
     * @param array $args
     * @throws Exception
     */
    public function deactiveUserManage(array $args = array()) {
        global $db;

        $this->validateInput($args);
        $strSql = sprintf("UPDATE ci_user_manage SET status = %s WHERE user_manage_id = %s", STATUS_INACTIVE, secure($args['user_manage_id'], 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Xóa một đối tượng quản lý khỏi hệ thống
     *
     * @param $user_manage_id
     * @throws Exception
     */
    public function deleteUserManage($user_manage_id) {
        global $db;

        /* validate $user_manage_id */
        if ($user_manage_id == 0) {
            throw new Exception(__("You must select an existing user_manage"));
        }

        /* delete the page */
        $db->query(sprintf("DELETE FROM ci_user_manage WHERE user_manage_id = %s", secure($user_manage_id, 'int') )) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Xóa vai trò quản lý của một trường
     *
     * @param $schoolId
     * @throws Exception
     */
    public function deleteSchoolManager($schoolId) {
        global $db;

        $sql = sprintf("DELETE FROM ci_user_manage WHERE object_id = %s AND object_type = %s", secure($schoolId, 'int'), MANAGE_SCHOOL);
        $db->query($sql) or _error(SQL_ERROR_THROWEN);
    }
    /**
     * Kiểm tra user hiện tại có trong bảng ci_user_manage chưa không
     *
     * @param $user_id
     * @throws Exception
     */
    public function checkUserManage($user_id) {
        global $db;
        $strSql = sprintf("SELECT * FROM ci_user_manage WHERE user_id = %s",
            secure($user_id, 'int'));

        $check_userManage = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($check_userManage->num_rows > 0) {
            return true;
        }
        return false;
    }
    /**
     * Lấy user_id của những nhân viên được phân quyền quản lý học phí
     *
     * @param $school_id
     * @throws Exception
     */
    public function getUserManageTuition($school_id)
    {
        global $db;
        $strSql = sprintf("SELECT UM.user_id FROM ci_user_manage UM INNER JOIN ci_permission P ON P.user_id = UM.user_id AND UM.object_type = %s 
AND object_id = %s AND P.tuitions = '1'", MANAGE_SCHOOL, secure($school_id, 'int'));
        $getUserManageTuitions = array();
        $get_userManageTuition = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_userManageTuition->num_rows > 0) {
            while ($getUserManageTuition = $get_userManageTuition->fetch_assoc()['user_id']) {
                $getUserManageTuitions[] = $getUserManageTuition;
            }
        }
        return $getUserManageTuitions;
    }
}
?>