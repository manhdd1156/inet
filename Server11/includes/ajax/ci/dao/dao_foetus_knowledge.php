<?php
/**
 * DAO -> Services
 * Thao tác với các bảng ci_foetus_knowledge
 *
 * @package ConIu
 * @author TaiLa
 */

class FoetusKnowledgeDAO
{
    /**
     * __construct
     *
     */
    public function __construct()
    {
    }

    /*
     * Insert vào bảng ci_foetus_knowledge
     */
    public function insertFoetusKnowledge ($titles, $links) {
        global $db, $date, $user;

        $strValues = "";
        // subject
        for ($idx = 0; $idx < count($titles); $idx++) {
            $strValues .= ",(" . "'" . $titles[$idx]. "'" . "," . "'" . $links[$idx]. "'" . ","  . secure($date) . ","  . secure($user->_data['user_id']) . "),";
            $strValues = trim($strValues, ",");
            $strSql = "INSERT INTO ci_foetus_knowledge (title, link, created_at, created_user_id) VALUES " . $strValues;
        }
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Lấy toàn bộ thông tin trong ci_foetus_knowledge
     *
     * @return array
     */
    public function getAllFoetusKnowledge () {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_foetus_knowledge ORDER BY created_at DESC, foetus_knowledge_id DESC");
        $get_foetus_knowledge = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $foetus_knowledge = array();
        if($get_foetus_knowledge->num_rows > 0) {
            while ($foetus = $get_foetus_knowledge->fetch_assoc()) {
                $foetus_knowledge[] = $foetus;
            }
        }
        return $foetus_knowledge;
    }

    /**
     * Lấy chi tiết foetus_knowledge
     *
     * @param $foetus_knowledge_id
     * @return null
     */
    public function getFoetusKnowledgeDetail($foetus_knowledge_id) {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_foetus_knowledge WHERE foetus_knowledge_id = %s", secure($foetus_knowledge_id));

        $get_foetus_knowledge = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $foetus_knowledge = null;
        if($get_foetus_knowledge->num_rows > 0) {
            $foetus_knowledge = $get_foetus_knowledge->fetch_assoc();
        }

        return $foetus_knowledge;
    }

    /**
     * Cập nhật thông tin kiến thức thai nhi
     *
     * @param $args
     */
    public function updateFoetusKnowledge($args) {
        global $db, $date;

        $strSql = sprintf("UPDATE ci_foetus_knowledge SET title = %s, link = %s, updated_at = %s WHERE foetus_knowledge_id = %s", secure($args['title']), secure($args['link']), secure($date), secure($args['foetus_knowledge_id'], 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Delete thông tin kiến thức
     *
     * @param $foetus_knowledge_id
     */
    public function deleteFoetusKnowledge($foetus_knowledge_id) {
        global $db;

        $strSql = sprintf("DELETE FROM ci_foetus_knowledge WHERE foetus_knowledge_id = %s", secure($foetus_knowledge_id, 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }
}