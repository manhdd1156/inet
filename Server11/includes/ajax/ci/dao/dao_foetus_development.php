<?php
/**
 * DAO -> Services
 * Thao tác với các bảng ci_foetus_info
 *
 * @package ConIu
 * @author TaiLa
 */

class FoetusDevelopmentDAO
{
    /**
     * __construct
     *
     */
    public function __construct()
    {
    }

    /*
     * Insert vào bảng ci_foetus_info
     */
    public function insertFoetusInfo (array $args = array()) {
        global $db, $date, $user;

        //$this->_validateInput($args);

        $strSql = sprintf("INSERT INTO ci_foetus_info  (title, link, image, day_push, time, type, is_development, is_notice_before, notice_before_days, is_reminder_before, reminder_before_days, created_at, created_user_id, content_type, content) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
            secure($args['title']), secure($args['link']), secure($args['image']), secure($args['day_push'], 'int'), secure($args['time']), secure($args['type'], 'int'), secure($args['is_development'], 'int'),
            secure($args['is_notice_before'], 'int'), secure($args['notice_before_days'], 'int'), secure($args['is_reminder_before'], 'int'), secure($args['reminder_before_days'], 'int'), secure($date), secure($user->_data['user_id'], 'int'), secure($args['content_type'], 'int'), secure($args['content']));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Lấy toàn bộ thông tin trong ci_foetus_info hiển thị bên phía phụ huynh
     *
     * @return array
     */
    public function getFoetusInfoForParent () {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_foetus_info WHERE is_development = %s ORDER BY day_push ASC, foetus_info_id DESC", 1);
        $get_foetus_info = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $foetus_info = array();
        if($get_foetus_info->num_rows > 0) {
            while ($foetus = $get_foetus_info->fetch_assoc()) {
                $foetus['week'] = $foetus['day_push'] / 7;
                $foetus['week'] = (int)$foetus['week'];
                $foetus_info[] = $foetus;
            }
        }
        return $foetus_info;
    }

    /**
     * Lấy toàn bộ thông tin trong ci_foetus_info hiển thị bên phía phụ huynh (chỉ lấy lịch khám)
     *
     * @return array
     */
    public function getFoetusCheckUpInfoForParent () {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_foetus_info WHERE is_development = %s AND type = 1 ORDER BY day_push ASC, foetus_info_id DESC", 1);
        $get_foetus_info = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $foetus_info = array();
        if($get_foetus_info->num_rows > 0) {
            while ($foetus = $get_foetus_info->fetch_assoc()) {
                $foetus['week'] = $foetus['day_push'] / 7;
                $foetus['week'] = (int)$foetus['week'];
                $foetus_info[] = $foetus;
            }
        }
        return $foetus_info;
    }

    /**
     * Lấy toàn bộ thông tin trong ci_foetus_info hiển thị bên phía phụ huynh (chỉ lấy thông tin tham khảo)
     *
     * @return array
     */
    public function getFoetusKnowInfoForParent () {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_foetus_info WHERE is_development = %s AND type = 2 ORDER BY day_push ASC, foetus_info_id DESC", 1);
        $get_foetus_info = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $foetus_info = array();
        if($get_foetus_info->num_rows > 0) {
            while ($foetus = $get_foetus_info->fetch_assoc()) {
                $foetus['week'] = $foetus['day_push'] / 7;
                $foetus['week'] = (int)$foetus['week'];
                $foetus_info[] = $foetus;
            }
        }
        return $foetus_info;
    }

    /**
     * Lấy danh sách ID thai nhi đến ngày nhận thông báo
     *
     * @param $day
     * @return array
     */
    public function getChildIdsOfDay($day) {
        global $db;

        $strSql = sprintf("SELECT child_parent_id FROM ci_child_parent WHERE TIMESTAMPDIFF(day, foetus_begin_date, CURRENT_DATE()) = %s", secure($day));

        $get_child = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $childIds = array();
        if($get_child->num_rows > 0) {
            while ($childId = $get_child->fetch_assoc()['child_parent_id']) {
                $childIds[] = $childId;
            }
        }

        return $childIds;
    }

    /**
     * Lấy chi tiết foetus_info
     *
     * @param $foetus_info_id
     * @return null
     */
    public function getFoetusInfoDetail($foetus_info_id) {
        global $db, $system;

        $strSql = sprintf("SELECT * FROM ci_foetus_info WHERE foetus_info_id = %s", secure($foetus_info_id));

        $get_foetus_info = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $foetus_info = null;
        if($get_foetus_info->num_rows > 0) {
            $foetus_info = $get_foetus_info->fetch_assoc();
            $foetus_info['week'] = $foetus_info['day_push'] / 7;
            $foetus_info['week'] = (int)$foetus_info['week'];
            if (!is_empty($foetus_info['image'])) {
                $foetus_info['image_source'] = $system['system_uploads'] . '/' . $foetus_info['image'];
            }
        }

        return $foetus_info;
    }

    /**
     * Lấy toàn bộ thông tin trong ci_foetus_info hiển thị bên phía nhân viên NOGA
     *
     * @return array
     */
    public function getFoetusInfoForNoga () {
        global $db, $system;

        $strSql = sprintf("SELECT * FROM ci_foetus_info ORDER BY day_push ASC, foetus_info_id ASC");
        $get_foetus_info = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $foetus_info = array();
        if($get_foetus_info->num_rows > 0) {
            while ($foetus = $get_foetus_info->fetch_assoc()) {
                $foetus['week'] = $foetus['day_push'] / 7;
                $foetus['week'] = (int)$foetus['week'];

                $foetus_info[] = $foetus;
            }
        }
        return $foetus_info;
    }

    /**
     * Cập nhật thông tin phát triển thai nhi
     *
     * @param $args
     */
    public function updateFoetusInfo($args) {
        global $db, $date;

        $strSql = sprintf("UPDATE ci_foetus_info SET title = %s, link = %s, image = %s, day_push = %s, time = %s, type = %s, is_development = %s, 
                  is_notice_before = %s, notice_before_days = %s, is_reminder_before = %s, reminder_before_days = %s, updated_at = %s, content_type = %s, content = %s 
                  WHERE foetus_info_id = %s", secure($args['title']), secure($args['link']), secure($args['image']), secure($args['day_push'], 'int'), secure($args['time']), secure($args['type'], 'int'), secure($args['is_development'], 'int'),
            secure($args['is_notice_before'], 'int'), secure($args['notice_before_days'], 'int'), secure($args['is_reminder_before'], 'int'), secure($args['reminder_before_days'], 'int'), secure($date), secure($args['content_type'], 'int'), secure($args['content']),
            secure($args['foetus_info_id'], 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Delete thông tin qúa trình phát triển
     *
     * @param $foetus_info_id
     */
    public function deleteFoetusInfo($foetus_info_id) {
        global $db;

        $strSql = sprintf("DELETE FROM ci_foetus_info WHERE foetus_info_id = %s", secure($foetus_info_id, 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Lấy 1 lần sắp khám thai trong tương lai
     *
     * @param $child_parent_id
     * @return array
     */
    public function getFoetusDevelopmentInfo($child_parent_id) {
        global $db, $system;

        // Lấy ngày bắt đầu mang thai
        $strSql = sprintf("SELECT foetus_begin_date FROM ci_child_parent WHERE child_parent_id = %s", secure($child_parent_id));

        $get_begin = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_begin->num_rows <= 0) {
            _error(404);
        }
        $begin = $get_begin->fetch_assoc()['foetus_begin_date'];

        $begin = toSysDate($begin);
        $begin = toDBDate($begin);

        $result = array(
            'pregnancy' => null,
            'interest' => []
        );
        // Lấy 1 lần khám thai sắp tới
        $strSql = sprintf("SELECT * FROM ci_foetus_info WHERE (DATEDIFF(CURRENT_DATE(), %s) <= day_push) AND type = 1 ORDER BY day_push ASC LIMIT 1", secure($begin));

        $get_foetus_development = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        if($get_foetus_development->num_rows > 0) {
            $result['pregnancy'] = $get_foetus_development->fetch_assoc();
            if (!is_empty($result['pregnancy']['image'])) {
                $result['pregnancy']['image'] = $system['system_uploads'] . '/' . $result['pregnancy']['image'];
            }
        }

        //Lấy ra thông tin quan tâm
        $strSql = sprintf('SELECT * FROM ci_foetus_info WHERE (DATEDIFF(CURRENT_DATE(), %1$s) <= (day_push + 20) AND DATEDIFF(CURRENT_DATE(), %1$s) >= (day_push - 10)) AND type = 2 ORDER BY day_push ASC', secure($begin));
        $get_interest = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        if($get_interest->num_rows > 0) {
            while ($interest = $get_interest->fetch_assoc()) {
                if (!is_empty($interest['image'])) {
                    $interest['image'] = $system['system_uploads'] . '/' . $interest['image'];
                }
                $result['interest'][] = $interest;

            }
        }

        return $result;
    }

    /**
     * Lấy toàn bộ thông tin trong ci_foetus_info hiển thị bên phía nhân viên NOGA theo type
     *
     * @param $type
     * @return array
     * @throws Exception
     */
    public function getFoetusInfoByType($type) {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_foetus_info WHERE type = %s ORDER BY day_push ASC, foetus_info_id ASC", secure($type, 'int'));
        $get_foetus_info = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $foetus_info = array();
        if($get_foetus_info->num_rows > 0) {
            while ($foetus = $get_foetus_info->fetch_assoc()) {
                $foetus['week'] = $foetus['day_push'] / 7;
                $foetus['week'] = (int)$foetus['week'];

                $foetus_info[] = $foetus;
            }
        }
        return $foetus_info;
    }
}