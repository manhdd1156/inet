<?php
/**
 * DAO -> Medicine
 * Thao tác với bảng ci_medicine
 * 
 * @package ConIu
 * @author QuanND
 */

class MedicineDAO {
    /**
     * __construct
     *
     */
    public function __construct() {
    }

    /**
     * Lây ra danh sách lần gửi thuốc của một trường trong một ngày
     *
     * @param $school_id
     * @param $theDate
     * @return array
     * @throws Exception
     */
    public function getMedicineOnDate($school_id, $theDate, &$count_no_confirm) {
        global $db, $user, $system;
        $theDate = toDBDate($theDate);

        $strSql = sprintf('SELECT M.*, C.child_name, C.birthday, C.gender, G.group_title, SC.status AS child_status FROM ci_medicine M
                            INNER JOIN ci_child C ON M.child_id = C.child_id AND M.status != %1$s AND (M.begin <= %2$s AND M.end >= %2$s)
                            INNER JOIN ci_school_child SC ON C.child_id = SC.child_id AND SC.school_id = %3$s
                            INNER JOIN groups G ON SC.class_id = G.group_id
                            ORDER BY M.status ASC, SC.class_id ASC, M.begin DESC', MEDICINE_STATUS_CANCEL, secure($theDate), secure($school_id, 'int'));
        $medicines = array();
        $get_medicines = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_medicines->num_rows > 0) {
            while($medicine = $get_medicines->fetch_assoc()) {
                if($medicine['status'] == MEDICINE_STATUS_NEW) {
                    $count_no_confirm++;
                }
                $medicine['birthday'] = toSysDate($medicine['birthday']);
                $medicine['begin'] = toSysDate($medicine['begin']);
                $medicine['end'] = toSysDate($medicine['end']);
                if (!is_empty($medicine['source_file'])) {
                    $medicine['source_file'] = $system['system_uploads'] . '/' . $medicine['source_file'];
                }

                //Lấy ra thông tin uống thuốc chi tiết hôm nay
                $medicine['detail'] = $this->_getMedicineDetailOnDate($medicine['medicine_id'], $theDate);
                $medicine['can_delete'] = ((count($medicine['detail']) == 0) && ($medicine['editable'] == 1) && $medicine['created_user_id']==$user->_data['user_id']);
                $medicines[] = $medicine;
            }
        }
        return $medicines;
    }

    /**
     * Lây ra danh sách lần gửi thuốc chưa được xác nhận của một trường trong một ngày
     *
     * @param $school_id
     * @param $theDate
     * @return array
     * @throws Exception
     */
    public function getMedicineOnDateNoConfirm($school_id, $theDate) {
        global $db;
        $theDate = toDBDate($theDate);

        $strSql = sprintf('SELECT M.medicine_id FROM ci_medicine M
                            INNER JOIN ci_child C ON M.child_id = C.child_id AND M.status = %1$s AND (M.begin <= %2$s AND M.end >= %2$s)
                            INNER JOIN ci_school_child SC ON C.child_id = SC.child_id AND SC.school_id = %3$s AND SC.status = %4$s', MEDICINE_STATUS_NEW, secure($theDate), secure($school_id, 'int'), secure(STATUS_ACTIVE, 'int'));
        $medicines = array();
        $get_medicines = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_medicines->num_rows > 0) {
            while($medicine = $get_medicines->fetch_assoc()['medicine_id']) {
                $medicines[] = $medicine;
            }
        }
        return $medicines;
    }

    /**
     * Lấy ra danh sách lần gửi thuốc trong ngày cho trẻ của một lớp
     *
     * @param $school_id
     * @param $theDate
     * @return array
     */
    public function getClassMedicineOnDate($class_id, $theDate, $isMobile = false, &$count_no_confirm) {
        global $db, $user, $system;
        $theDate = toDBDate($theDate);

        $strSql = sprintf('SELECT M.*, C.child_name, C.birthday, C.gender, CC.status AS child_status FROM ci_medicine M
                            INNER JOIN ci_child C ON M.child_id = C.child_id AND M.status != %1$s AND (M.begin <= %2$s AND M.end >= %2$s)
                            INNER JOIN ci_class_child CC ON C.child_id = CC.child_id AND CC.class_id = %3$s
                            ORDER BY M.status ASC, M.begin DESC', MEDICINE_STATUS_CANCEL, secure($theDate), secure($class_id, 'int'));
        $medicines = array();
        $get_medicines = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_medicines->num_rows > 0) {
            $count_no_confirm = 0;
            while($medicine = $get_medicines->fetch_assoc()) {
                if($medicine['status'] == MEDICINE_STATUS_NEW) {
                    $count_no_confirm++;
                }
                $medicine['birthday'] = toSysDate($medicine['birthday']);
                $medicine['begin'] = toSysDate($medicine['begin']);
                $medicine['end'] = toSysDate($medicine['end']);
                if (!is_empty($medicine['source_file'])) {
                    $medicine['source_file'] = $system['system_uploads'] . '/' . $medicine['source_file'];
                }

                //Lấy ra thông tin uống thuốc chi tiết hôm nay
                // CI-Mobile trả về cùng đối tượng details cho class và child
                if ($isMobile)
                    $medicine['details'] = $this->_getMedicineDetailOnDate($medicine['medicine_id'], $theDate);
                else
                    $medicine['detail'] = $this->_getMedicineDetailOnDate($medicine['medicine_id'], $theDate);
                $medicine['can_delete'] = ((count($medicine['detail']) == 0) && ($medicine['editable'] == 1) && $medicine['created_user_id']==$user->_data['user_id']);
                $medicines[] = $medicine;
            }
        }
        return $medicines;
    }

    /**
     * Lấy ra danh sách lần gửi thuốc trong ngày cho trẻ của một lớp for API
     *
     * @param $class_id
     * @param $theDate
     * @param bool $isMobile
     * @return array
     */
    public function getClassMedicineOnDateForAPI($class_id, $theDate, $isMobile = false) {
        global $db, $user, $system;
        $theDate = toDBDate($theDate);

        $strSql = sprintf('SELECT M.*, C.child_name, C.birthday, C.gender FROM ci_medicine M
                            INNER JOIN ci_child C ON M.child_id = C.child_id AND M.status != %1$s AND (M.begin <= %2$s AND M.end >= %2$s)
                            INNER JOIN ci_class_child CC ON C.child_id = CC.child_id AND CC.class_id = %3$s
                            ORDER BY M.status ASC, M.begin DESC', MEDICINE_STATUS_CANCEL, secure($theDate), secure($class_id, 'int'));
        $medicines = array();
        $get_medicines = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_medicines->num_rows > 0) {
            $count_no_confirm = 0;
            while($medicine = $get_medicines->fetch_assoc()) {
                if($medicine['status'] == MEDICINE_STATUS_NEW) {
                    $count_no_confirm++;
                }
                $medicine['birthday'] = toSysDate($medicine['birthday']);
                $medicine['begin'] = toSysDate($medicine['begin']);
                $medicine['end'] = toSysDate($medicine['end']);
                if (!is_empty($medicine['source_file'])) {
                    $medicine['source_file'] = $system['system_uploads'] . '/' . $medicine['source_file'];
                }

                //Lấy ra thông tin uống thuốc chi tiết hôm nay
                // CI-Mobile trả về cùng đối tượng details cho class và child
                if ($isMobile)
                    $medicine['details'] = $this->_getMedicineDetailOnDate($medicine['medicine_id'], $theDate);
                else
                    $medicine['detail'] = $this->_getMedicineDetailOnDate($medicine['medicine_id'], $theDate);
                $medicine['can_delete'] = ((count($medicine['detail']) == 0) && ($medicine['editable'] == 1) && $medicine['created_user_id']==$user->_data['user_id']);
                $medicines[] = $medicine;
            }
        }
        return $medicines;
    }

    /**
     * Lấy ra danh sách lần gửi thuốc trong ngày cho trẻ chưa xác nhận của một lớp
     *
     * @param $school_id
     * @param $theDate
     * @return array
     */
    public function getClassMedicineIdsOnDateNoConfirm($class_id, $theDate, $isMobile = false) {
        global $db, $user, $system;
        $theDate = toDBDate($theDate);

        $strSql = sprintf('SELECT M.medicine_id FROM ci_medicine M
                            INNER JOIN ci_child C ON M.child_id = C.child_id AND M.status = %1$s AND (M.begin <= %2$s AND M.end >= %2$s)
                            INNER JOIN ci_class_child CC ON C.child_id = CC.child_id AND CC.class_id = %3$s AND CC.status = %4$s', MEDICINE_STATUS_NEW, secure($theDate), secure($class_id, 'int'), STATUS_ACTIVE);
        $medicineIds = array();
        $get_medicines = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_medicines->num_rows > 0) {
            while($medicineId = $get_medicines->fetch_assoc()['medicine_id']) {
                $medicineIds[] = $medicineId;
            }
        }
        return $medicineIds;
    }

    /**
     * Lấy danh sách id của những đơn thuốc đã được xác nhận hoặc hủy
     *
     * @param $school_id
     * @param $theDate
     * @return array
     */
    public function getAllMedicineIdConfirmed($school_id, $theDate) {
        global $db, $user, $system;
        $theDate = toDBDate($theDate);

        $strSql = sprintf('SELECT M.medicine_id FROM ci_medicine M
                            INNER JOIN ci_child C ON M.child_id = C.child_id AND M.status != %1$s AND (M.begin <= %2$s AND M.end >= %2$s)
                            INNER JOIN ci_class_child CC ON C.child_id = CC.child_id AND CC.class_id = %3$s', MEDICINE_STATUS_NEW, secure($theDate), secure($school_id, 'int'));
        $medicineIds = array();
        $get_medicines = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_medicines->num_rows > 0) {
            while($medicineId = $get_medicines->fetch_assoc()['medicine_id']) {
                $medicineIds[] = $medicineId;
            }
        }
        return $medicineIds;
    }

    /**
     * Lấy ra danh sách thuốc hôm nay của một trẻ
     *
     * @param $child_id
     * @param $theDate
     * @return array
     * @throws Exception
     */
    public function getChildMedicineOnDate($child_id, $theDate) {
        global $db, $user, $system;
        $theDate = toDBDate($theDate);

        $strSql = sprintf('SELECT * FROM ci_medicine WHERE status != %1$s AND (begin <= %2$s AND end >= %2$s)
                            AND child_id = %3$s ORDER BY status ASC, begin DESC', MEDICINE_STATUS_CANCEL, secure($theDate), secure($child_id, 'int'));
        $medicines = array();
        $get_medicines = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_medicines->num_rows > 0) {
            while($medicine = $get_medicines->fetch_assoc()) {
                $medicine['begin'] = toSysDate($medicine['begin']);
                $medicine['end'] = toSysDate($medicine['end']);
                if (!is_empty($medicine['source_file'])) {
                    $medicine['source_file'] = $system['system_uploads'] . '/' . $medicine['source_file'];
                }

                //Lấy ra thông tin uống thuốc chi tiết hôm nay
                $medicine['detail'] = $this->_getMedicineDetailOnDate($medicine['medicine_id'], $theDate);
                $medicine['can_delete'] = ((count($medicine['detail']) == 0) && ($medicine['editable'] == 1) && $medicine['created_user_id']==$user->_data['user_id']);
                $medicines[] = $medicine;
            }
        }
        return $medicines;
    }

    /**
     * Lấy tất cả các lần gửi thuốc của một trẻ
     *
     * @param $child_id
     * @return array
     * @throws Exception
     */
    public function getChildAllMedicines($child_id, $limit = 999999) {
        global $db, $user, $system;

        $strSql = sprintf('SELECT * FROM ci_medicine WHERE child_id = %s ORDER BY status ASC, begin DESC LIMIT %s', secure($child_id, 'int'), $limit);

        $medicines = array();
        $get_medicines = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_medicines->num_rows > 0) {
            while($medicine = $get_medicines->fetch_assoc()) {
                $medicine['begin'] = toSysDate($medicine['begin']);
                $medicine['end'] = toSysDate($medicine['end']);
                if (!is_empty($medicine['source_file'])) {
                    $medicine['source_file'] = $system['system_uploads'] . '/' . $medicine['source_file'];
                }
                //Lấy ra thông tin uống thuốc chi tiết hôm nay
                $medicine['count'] = $this->_getCountDetail($medicine['medicine_id']);
                $medicine['can_delete'] = (($medicine['count'] == 0) && ($medicine['editable'] == 1) && $medicine['created_user_id']==$user->_data['user_id']);
                $medicines[] = $medicine;
            }
        }
        return $medicines;
    }

    /**
     * Lấy ra danh sách tất cả lần gửi thuốc của lớp
     *
     * @param $class_id
     * @return array
     */
    public function getClassAllMedicines($class_id, &$count_no_confirm) {
        global $db, $user, $system;

        $strSql = sprintf('SELECT M.*, C.child_name, C.birthday, C.gender, CC.status as child_status FROM ci_medicine M
                            INNER JOIN ci_child C ON M.child_id = C.child_id
                            INNER JOIN ci_class_child CC ON C.child_id = CC.child_id AND CC.class_id = %s
                            ORDER BY M.status ASC, M.begin DESC', secure($class_id, 'int'));
        $medicines = array();
        $get_medicines = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_medicines->num_rows > 0) {
            while($medicine = $get_medicines->fetch_assoc()) {
                if($medicine['status'] == MEDICINE_STATUS_NEW) {
                    $count_no_confirm++;
                }
                $medicine['birthday'] = toSysDate($medicine['birthday']);
                $medicine['begin'] = toSysDate($medicine['begin']);
                $medicine['end'] = toSysDate($medicine['end']);
                if (!is_empty($medicine['source_file'])) {
                    $medicine['source_file'] = $system['system_uploads'] . '/' . $medicine['source_file'];
                }

                //$medicine['count'] = $this->_getCountDetail($medicine['medicine_id']);
                $medicine['can_delete'] = (($medicine['count'] == 0) && ($medicine['editable'] == 1) && $medicine['created_user_id']==$user->_data['user_id']);
                $medicines[] = $medicine;
            }
        }
        return $medicines;
    }

    /**
     * Lấy ra danh sách Id tất cả lần gửi thuốc của trẻ chưa được xác nhận
     *
     * @param $class_id
     * @return array
     */
    public function getClassAllMedicineIdsNoConfirm($class_id) {
        global $db;

        $strSql = sprintf('SELECT M.medicine_id FROM ci_medicine M
                            INNER JOIN ci_child C ON M.child_id = C.child_id
                            INNER JOIN ci_class_child CC ON C.child_id = CC.child_id AND CC.class_id = %s WHERE M.status = %s', secure($class_id, 'int'), MEDICINE_STATUS_NEW);
        $medicineIds = array();
        $get_medicines = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_medicines->num_rows > 0) {
            while($medicineId = $get_medicines->fetch_assoc()['medicine_id']) {
                $medicineIds[] = $medicineId;
            }
        }
        return $medicineIds;
    }


    /**
     * Lấy ra danh sách tất cả lần gửi thuốc của trẻ
     *
     * @param $class_id
     * @return array
     */
    public function getClassAllMedicinesApi($class_id) {
        global $db, $user, $system;

        $strSql = sprintf('SELECT M.*, C.child_name, C.birthday, C.gender FROM ci_medicine M
                            INNER JOIN ci_child C ON M.child_id = C.child_id
                            INNER JOIN ci_class_child CC ON C.child_id = CC.child_id AND CC.class_id = %s
                            ORDER BY M.status ASC, M.begin DESC', secure($class_id, 'int'));
        $medicines = array();
        $get_medicines = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_medicines->num_rows > 0) {
            while($medicine = $get_medicines->fetch_assoc()) {
                $medicine['birthday'] = toSysDate($medicine['birthday']);
                $medicine['begin'] = toSysDate($medicine['begin']);
                $medicine['end'] = toSysDate($medicine['end']);
                if (!is_empty($medicine['source_file'])) {
                    $medicine['source_file'] = $system['system_uploads'] . '/' . $medicine['source_file'];
                }

                $medicine['details'] = $this->getMedicineDetail($medicine['medicine_id']);
                $medicine['count'] = count($medicine['details']);
                $medicine['can_delete'] = (($medicine['count'] == 0) && ($medicine['editable'] == 1) && $medicine['created_user_id']==$user->_data['user_id']);
                $medicines[] = $medicine;
            }
        }
        return $medicines;
    }


    /**
     * Lấy tất cả các lần gửi thuốc của trường
     *
     * @param $school_id
     * @return array
     */
    public function getAllMedicines($school_id, &$count_no_confirm) {
        global $db, $user, $system;

        $strSql = sprintf('SELECT DISTINCT M.medicine_id, M.medicine_list, M.status, M.source_file, M.time_per_day, M.begin, M.end, M.created_user_id, C.child_id, C.child_name, C.birthday, SC.status AS child_status, G.group_title, COUNT(MD.usage_date) AS count FROM ci_medicine M
                            INNER JOIN ci_child C ON M.child_id = C.child_id
                            INNER JOIN ci_school_child SC ON C.child_id = SC.child_id AND SC.school_id = %s
                            INNER JOIN groups G ON SC.class_id = G.group_id
                            LEFT JOIN ci_medicine_detail MD ON M.medicine_id = MD.medicine_id
                            GROUP BY M.medicine_id
                            ORDER BY M.status ASC, M.begin DESC', secure($school_id, 'int'));
        $medicines = array();
        $get_medicines = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_medicines->num_rows > 0) {
            while($medicine = $get_medicines->fetch_assoc()) {
                if($medicine['status'] == MEDICINE_STATUS_NEW) {
                    $count_no_confirm++;
                }
                $medicine['birthday'] = toSysDate($medicine['birthday']);
                $medicine['begin'] = toSysDate($medicine['begin']);
                $medicine['end'] = toSysDate($medicine['end']);
                if (!is_empty($medicine['source_file'])) {
                    $medicine['source_file'] = $system['system_uploads'] . '/' . $medicine['source_file'];
                }
                //$medicine['count'] = $this->_getCountDetail($medicine['medicine_id']);
                $medicine['can_delete'] = (($medicine['count'] == 0) && ($medicine['editable'] == 1) && ($medicine['created_user_id']==$user->_data['user_id']));
                $medicines[] = $medicine;
            }
        }
        return $medicines;
    }

    /**
     * Lấy tất cả các lần gửi thuốc chưa được xác nhận của trường
     *
     * @param $school_id
     * @return array
     */
    public function getAllMedicineIdsNoConfirm($school_id) {
        global $db;

        $strSql = sprintf('SELECT M.medicine_id FROM ci_medicine M                        
                           INNER JOIN ci_school_child SC ON M.child_id = SC.child_id
                           AND SC.school_id = %s AND SC.status = %s AND M.status = %s
                           INNER JOIN ci_child C ON M.child_id = C.child_id', secure($school_id, 'int'), secure(STATUS_ACTIVE, 'int'), MEDICINE_STATUS_NEW);
        $medicines = array();
        $get_medicines = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_medicines->num_rows > 0) {
            while($medicine = $get_medicines->fetch_assoc()['medicine_id']) {
                $medicines[] = $medicine;
            }
        }
        return $medicines;
    }

    /**
     * Lấy ra số lần sử dụng thuốc của một lần gửi.
     *
     * @param $medicine_id
     * @return int
     */
    public function _getCountDetail($medicine_id) {
        global $db;

        $strSql = sprintf("SELECT count(medicine_id) AS CNT FROM ci_medicine_detail WHERE medicine_id = %s", secure($medicine_id, 'int'));
        $get_cnt = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_cnt->num_rows > 0) {
            return $get_cnt->fetch_assoc()['CNT'];
        }
        return 0;
    }

    /**
     * Lấy ra thông tin lần gửi thuốc theo ID
     *
     * @param $medicine_id
     * @param bool|true $getDetail
     * @return array|null
     * @throws Exception
     */
    public function getMedicine($medicine_id, $getDetail = true) {
        global $db, $system;

        $medicine = null;
        $strSql = sprintf("SELECT M.*, C.child_name, C.birthday, C.gender, CC.class_id, G.group_title FROM ci_medicine M
                            INNER JOIN ci_child C ON M.child_id = C.child_id AND M.medicine_id = %s
                            INNER JOIN ci_class_child CC ON C.child_id = CC.child_id AND CC.status = %s
                            INNER JOIN groups G ON CC.class_id = G.group_id", secure($medicine_id, 'int'), secure(STATUS_ACTIVE, 'int'));
        $get_medicines = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_medicines->num_rows > 0) {
            $medicine = $get_medicines->fetch_assoc();
            if (!is_empty($medicine['source_file'])) {
                $medicine['source_file_path'] = $medicine['source_file'];
                $medicine['source_file'] = $system['system_uploads'] . '/' . $medicine['source_file'];
            }
            $medicine['birthday'] = toSysDate($medicine['birthday']);
            $medicine['begin'] = toSysDate($medicine['begin']);
            $medicine['end'] = toSysDate($medicine['end']);
            if ($getDetail && $system['is_mobile']) {
                $medicine['details'] = $this->getMedicineDetail($medicine_id);
            } elseif ($getDetail) {
                $medicine['detail'] = $this->getMedicineDetail($medicine_id);
            }
        }
        return $medicine;
    }

    public function getMedicine4Detail($medicine_id) {
        global $db, $system;

        $medicine = null;
        $strSql = sprintf("SELECT M.*, C.child_name, C.birthday, C.gender, CC.class_id, G.group_title, U.user_fullname, U.user_name FROM ci_medicine M
                            INNER JOIN users U ON M.created_user_id = U.user_id
                            INNER JOIN ci_child C ON M.child_id = C.child_id AND M.medicine_id = %s
                            INNER JOIN ci_class_child CC ON C.child_id = CC.child_id
                            INNER JOIN groups G ON CC.class_id = G.group_id", secure($medicine_id, 'int'));
        $get_medicines = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_medicines->num_rows > 0) {
            $medicine = $get_medicines->fetch_assoc();
            if (!is_empty($medicine['source_file'])) {
                $medicine['source_file'] = $system['system_uploads'] . '/' . $medicine['source_file'];
            }
            $medicine['birthday'] = toSysDate($medicine['birthday']);
            $medicine['begin'] = toSysDate($medicine['begin']);
            $medicine['end'] = toSysDate($medicine['end']);
            $medicine['created_at'] = toSysDatetime($medicine['created_at']);
            if ($medicine['updated_at'] != '') {
                $medicine['updated_at'] = toSysDatetime($medicine['updated_at']);
            }
            $medicine['detail'] = $this->getMedicineDetail($medicine_id);
        }
        return $medicine;
    }

    /**
     * Lấy chi tiết một đơn thuốc hiển thị ở màn hình lớp.
     * Trường hợp này không cần join bảng group để lấy tên lớp nữa.
     *
     * @param $medicine_id
     * @param $classId
     * @return array|null
     * @throws Exception
     */
    public function getClassMedicine4Detail($classId, $medicine_id) {
        global $db, $system, $user;

        $medicine = null;
        $strSql = sprintf("SELECT M.*, C.child_name, C.birthday, C.gender, CC.class_id, CC.status AS child_status, U.user_fullname, U.user_name FROM ci_medicine M
                            INNER JOIN users U ON M.created_user_id = U.user_id
                            INNER JOIN ci_child C ON M.child_id = C.child_id AND M.medicine_id = %s
                            INNER JOIN ci_class_child CC ON C.child_id = CC.child_id AND CC.class_id = %s", secure($medicine_id, 'int'), secure($classId, 'int'));
        $get_medicines = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_medicines->num_rows > 0) {
            $medicine = $get_medicines->fetch_assoc();
            if (!is_empty($medicine['source_file'])) {
                $medicine['source_file'] = $system['system_uploads'] . '/' . $medicine['source_file'];
            }
            $medicine['birthday'] = toSysDate($medicine['birthday']);
            $medicine['begin'] = toSysDate($medicine['begin']);
            $medicine['end'] = toSysDate($medicine['end']);
            $medicine['created_at'] = toSysDatetime($medicine['created_at']);
            if ($medicine['updated_at'] != '') {
                $medicine['updated_at'] = toSysDatetime($medicine['updated_at']);
            }
            $medicine['can_delete'] = (($medicine['editable'] == 1) && ($medicine['created_user_id']==$user->_data['user_id']));
            $medicine['detail'] = $this->getMedicineDetail($medicine_id);
        }
        return $medicine;
    }

    /**
     * Lấy ra thông tin chi tiết lần gửi thuốc (chi tiết các lần cho trẻ uống).
     *
     * @param $medicine_id
     * @return array
     * @throws Exception
     */
    public function getMedicineDetail($medicine_id) {
        global $db;

        $strSql = sprintf("SELECT MD.*, U.user_fullname FROM ci_medicine_detail MD INNER JOIN users U ON MD.created_user_id = U.user_id
                            WHERE MD.medicine_id = %s
                            ORDER BY MD.usage_date ASC, MD.time_on_day ASC", secure($medicine_id, 'int'));
        $details = array();
        $get_details = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_details->num_rows > 0) {
            while($detail = $get_details->fetch_assoc()) {
                $detail['usage_date'] = toSysDate($detail['usage_date']);
                $detail['created_at'] = toSysDatetime($detail['created_at']);

                $details[] = $detail;
            }
        }
        return $details;
    }

    /**
     * Lấy ra thông tin chi tiết cho trẻ uống thuốc trong 1 ngày
     *
     * @param $medicine_id
     * @param $date
     * @return array
     * @throws Exception
     */
    private function _getMedicineDetailOnDate($medicine_id, $date) {
        global $db;

        $strSql = sprintf("SELECT MD.*, U.user_fullname FROM ci_medicine_detail MD INNER JOIN users U ON MD.created_user_id = U.user_id
                            WHERE MD.medicine_id = %s AND MD.usage_date = %s
                            ORDER BY MD.time_on_day ASC", secure($medicine_id, 'int'), secure($date));
        $details = array();
        $get_details = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_details->num_rows > 0) {
            while($detail = $get_details->fetch_assoc()) {
                $detail['usage_date'] = toSysDate($detail['usage_date']);
                $detail['created_at'] = toSysDatetime($detail['created_at']);

                $details[] = $detail;
            }
        }
        return $details;
    }

    /**
     * Thêm một lần cho trẻ uống thuốc vào hệ thống
     *
     * @param $medicine_id
     * @param $max: số lần uống tối đa trong ngày.
     */
    public function addMedicateTime($medicine_id, $max) {
        global $db, $date, $user;
        // $usageDate = (new DateTime())->format('Y-m-d');
        $usageDate = date('Y-m-d');

        $last_order = 0;
        $strSql = sprintf("SELECT MAX(time_on_day) AS last_order FROM ci_medicine_detail WHERE medicine_id = %s AND usage_date = %s",
                            secure($medicine_id, 'int'), secure($usageDate));
        $get_last = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_last->num_rows > 0) {
            $last = $get_last->fetch_assoc();
            if (!is_null($last) && !is_null($last['last_order'])){
                $last_order = $last['last_order'];
            }
        }

        //Nếu lần uống thuốc đã đủ thì bỏ qua
        if ($max <= $last_order) return;

        $strSql = sprintf("INSERT INTO ci_medicine_detail (medicine_id, usage_date, time_on_day, created_at, created_user_id) VALUES (%s, %s, %s, %s, %s)",
                            secure($medicine_id, 'int'), secure($usageDate), $last_order+1, secure($date), $user->_data['user_id']);
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        //Cập nhật trạng thái sửa đơn thuốc = 0
        $strSql = sprintf("UPDATE ci_medicine SET editable = 0 WHERE medicine_id = %s", secure($medicine_id, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Lấy ra số lượng medicine của một trường
     *
     * @param int $school_id
     * @return mixed
     * @throws Exception
     */
    public function getCountMedicineOfSchool($school_id = 0) {
        global $db;

        $strSql = sprintf("SELECT COUNT(medicine_id) AS medicine_count FROM ci_medicine WHERE school_id = %s", secure($school_id, 'int'));
        $get_count = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        return $get_count->fetch_assoc()['medicine_count'];
    }

    /**
     * Insert một medicine vào trong DB
     *
     * @param array $args
     * @return mixed
     * @throws Exception
     */
    public function insertMedicine(array $args = array()) {
        global $db, $date;
        $this->validateInput($args);
        $args['begin'] = toDBDate($args['begin']);
        $args['end'] = toDBDate($args['end']);
        if (strtotime($args['begin']) > strtotime($args['end'])) {
            throw new Exception(__("The time is incorrect"));
        }

        $strSql = sprintf("INSERT INTO ci_medicine (child_id, medicine_list, begin, end, guide, time_per_day, status, created_at, 
created_user_id, source_file, file_name) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
                            secure($args['child_id'], 'int'), secure($args['medicine_list']), secure($args['begin']), secure($args['end']), secure($args['guide']),
                            secure($args['time_per_day'], 'int'), secure($args['status'], 'int'), secure($date), secure($args['created_user_id'], 'int'), secure($args['source_file']), secure($args['file_name']));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        return $db->insert_id;
    }

    /**
     * Kiểm tra điều kiện đầu vào.
     *
     * @param array $args
     * @throws Exception
     */
    private function validateInput(array $args = array()) {
        if ($args['child_id'] <= 0) {
            throw new Exception(__("You have not picked a student yet"));
        }

        if (!isset($args['medicine_list'])) {
            throw new Exception(__("You must enter medicine list"));
        }

        if (strlen($args['medicine_list']) < 10) {
            throw new Exception(__("Medicine list must be at least 10 characters long"));
        }

        if (!isset($args['guide'])) {
            throw new Exception(__("You must enter usage guide"));
        }

        if (!isset($args['time_per_day']) || $args['time_per_day'] <= 0) {
            throw new Exception(__("You must enter times per day"));
        }

        if (!validateDate($args['begin'])) {
            throw new Exception(__("You must enter correct begin time"));
        }

        if (!validateDate($args['end'])) {
            throw new Exception(__("You must enter correct end time"));
        }
    }

    /**
     * Cập nhật Medicines
     *
     * @param array $args
     * @throws Exception
     */
    public function updateMedicine(array $args = array()) {
        global $db, $date;
        if ($args['medicine_id'] <= 0) {
            throw new Exception(__("You must select an existing medicine"));
        }
        $this->validateInput($args);
        $args['begin'] = toDBDate($args['begin']);
        $args['end'] = toDBDate($args['end']);

        $strSql = sprintf("UPDATE  ci_medicine SET child_id = %s, medicine_list = %s, begin = %s, end = %s, guide = %s, time_per_day = %s,
                    status = %s, updated_at = %s, source_file = %s, file_name = %s WHERE medicine_id = %s",
            secure($args['child_id'], 'int'), secure($args['medicine_list']), secure($args['begin']), secure($args['end']), secure($args['guide']),
            secure($args['time_per_day'], 'int'), secure($args['status'], 'int'), secure($date), secure($args['source_file']), secure($args['file_name']), secure($args['medicine_id'], 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Cập nhật trạng thái của đơn thuốc
     *
     * @param $status
     * @param $medicine_id
     */
    public function updateMedicineStatus($status, $medicine_id) {
        global $db, $date, $user;
        $strSql = sprintf("UPDATE ci_medicine SET status = %s, updated_at = %s, confirm_user_id = %s WHERE medicine_id = %s", secure($status, 'int'), secure($date), secure($user->_data['user_id'], 'int'), secure($medicine_id, 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Cập nhật trạng thái của tất cả đơn thuốc chưa xác nhận
     *
     * @param $status
     * @param $medicineIds
     */
    public function updateAllMedicineStatus($status, $medicineIds) {
        global $db, $date;

        $ids = implode(",", $medicineIds);
        $strSql = sprintf("UPDATE ci_medicine SET status = %s, updated_at = %s WHERE medicine_id IN (%s)", secure($status, 'int'), secure($date), $ids);

        $db->query($strSql) or _error('error', $strSql);
    }

    /**
     * Cập nhật trạng thái đơn thuốc
     *
     * @param $medicine_id
     * @throws Exception
     */
    public function updateStatusToNotified($medicine_id) {
        global $db, $date;

        $strSql = sprintf("UPDATE ci_medicine SET is_notified = %s WHERE medicine_id = %s", 1, secure($medicine_id, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Xóa một thông báo
     *
     * @param $medicine_id
     * @throws Exception
     */
    public function deleteMedicine($medicine_id) {
        global $db;
        $db->query(sprintf("DELETE FROM ci_medicine WHERE medicine_id = %s", secure($medicine_id, 'int') )) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Lấy tất cả các lần gửi thuốc của trường
     *
     * @param $school_id
     * @return array
     */
    public function getMedicineSearchClass($school_id, $class_id, $begin, $end, &$count_no_confirm) {
        global $db, $user, $system;

        $begin = toDBDate($begin);
        $end = toDBDate($end);

        $strSql = sprintf('SELECT DISTINCT M.medicine_id, M.medicine_list, M.status, M.source_file, M.time_per_day, M.begin, M.end, M.created_user_id, C.child_id, C.child_name, C.birthday, SC.status AS child_status, G.group_title, COUNT(MD.usage_date) AS count FROM ci_medicine M
                            INNER JOIN ci_child C ON M.child_id = C.child_id
                            INNER JOIN ci_school_child SC ON C.child_id = SC.child_id AND SC.school_id = %s
                            INNER JOIN groups G ON SC.class_id = G.group_id AND G.group_id = %s
                            LEFT JOIN ci_medicine_detail MD ON M.medicine_id = MD.medicine_id
                            WHERE begin >= %s AND begin <= %s
                            GROUP BY M.medicine_id
                            ORDER BY M.status ASC, M.begin DESC', secure($school_id, 'int'), secure($class_id, 'int'), secure($begin), secure($end));
        $medicines = array();
        $get_medicines = $db->query($strSql) or _error(SQL_ERROR_THROWENl);
        if($get_medicines->num_rows > 0) {
            while($medicine = $get_medicines->fetch_assoc()) {
                if($medicine['status'] == MEDICINE_STATUS_NEW) {
                    $count_no_confirm++;
                }
                $medicine['birthday'] = toSysDate($medicine['birthday']);
                $medicine['begin'] = toSysDate($medicine['begin']);
                $medicine['end'] = toSysDate($medicine['end']);
                if (!is_empty($medicine['source_file'])) {
                    $medicine['source_file'] = $system['system_uploads'] . '/' . $medicine['source_file'];
                }
                //$medicine['count'] = $this->_getCountDetail($medicine['medicine_id']);
                $medicine['can_delete'] = (($medicine['count'] == 0) && ($medicine['editable'] == 1) && ($medicine['created_user_id']==$user->_data['user_id']));
                $medicines[] = $medicine;
            }
        }
        return $medicines;
    }

    /**
     * Tìm kiếm danh sách thuốc của 1 trẻ
     *
     * @param $school_id
     * @param $child_id
     * @param $begin
     * @param $end
     * @param $count_no_confirm
     * @return array
     * @throws Exception
     */
    public function getMedicineSearchChild($school_id, $child_id, $begin, $end, &$count_no_confirm) {
        global $db, $user, $system;

        $begin = toDBDate($begin);
        $end = toDBDate($end);

        $strSql = sprintf('SELECT DISTINCT M.medicine_id, M.medicine_list, M.status, M.source_file, M.time_per_day, M.begin, M.end, M.created_user_id, C.child_id, C.child_name, C.birthday, SC.status AS child_status, G.group_title, COUNT(MD.usage_date) AS count FROM ci_medicine M
                            INNER JOIN ci_child C ON M.child_id = C.child_id AND C.child_id = %s
                            INNER JOIN ci_school_child SC ON C.child_id = SC.child_id AND SC.school_id = %s
                            INNER JOIN groups G ON SC.class_id = G.group_id
                            LEFT JOIN ci_medicine_detail MD ON M.medicine_id = MD.medicine_id
                            WHERE begin >= %s AND begin <= %s
                            GROUP BY M.medicine_id
                            ORDER BY M.status ASC, M.begin DESC', secure($child_id, 'int'), secure($school_id, 'int'), secure($begin), secure($end));
        $medicines = array();
        $get_medicines = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_medicines->num_rows > 0) {
            while($medicine = $get_medicines->fetch_assoc()) {
                if($medicine['status'] == MEDICINE_STATUS_NEW) {
                    $count_no_confirm++;
                }
                $medicine['birthday'] = toSysDate($medicine['birthday']);
                $medicine['begin'] = toSysDate($medicine['begin']);
                $medicine['end'] = toSysDate($medicine['end']);
                if (!is_empty($medicine['source_file'])) {
                    $medicine['source_file'] = $system['system_uploads'] . '/' . $medicine['source_file'];
                }
                //$medicine['count'] = $this->_getCountDetail($medicine['medicine_id']);
                $medicine['can_delete'] = (($medicine['count'] == 0) && ($medicine['editable'] == 1) && ($medicine['created_user_id']==$user->_data['user_id']));
                $medicines[] = $medicine;
            }
        }
        return $medicines;
    }

    /**
     * Lấy tất cả các lần gửi thuốc của một trẻ
     *
     * @param $child_id
     * @return array
     * @throws Exception
     */
    public function getChildAllMedicinesSearch($child_id, &$count_no_confirm) {
        global $db, $user, $system;

        $strSql = sprintf('SELECT * FROM ci_medicine WHERE child_id = %s ORDER BY status ASC, begin DESC', secure($child_id, 'int'));

        $medicines = array();
        $get_medicines = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_medicines->num_rows > 0) {
            while($medicine = $get_medicines->fetch_assoc()) {
                if($medicine['status'] == MEDICINE_STATUS_NEW) {
                    $count_no_confirm++;
                }

                $medicine['begin'] = toSysDate($medicine['begin']);
                $medicine['end'] = toSysDate($medicine['end']);
                if (!is_empty($medicine['source_file'])) {
                    $medicine['source_file'] = $system['system_uploads'] . '/' . $medicine['source_file'];
                }
                //Lấy ra thông tin uống thuốc chi tiết hôm nay
                $medicine['count'] = $this->_getCountDetail($medicine['medicine_id']);
                $medicine['can_delete'] = (($medicine['count'] == 0) && ($medicine['editable'] == 1) && $medicine['created_user_id']==$user->_data['user_id']);
                $medicines[] = $medicine;
            }
        }
        return $medicines;
    }
}
?>