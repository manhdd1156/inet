<?php
/**
 * DAO -> ClassLevel
 * Thao tác với bảng class_level
 * 
 * @package ConIu
 * @author QuanND
 */

class ClassLevelDAO {
    /**
     * __construct
     *
     */
    public function __construct() {
    }

    /**
     * Lây ra danh sách các class_levels của một trường
     *
     * @return array
     */
    public function getClassLevels($school_id, $getClassCnt = false) {
        global $db;

        $strSql = null;
        if ($getClassCnt) {
            $strSql = sprintf("SELECT CL.*, count(G.group_id) AS cnt FROM ci_class_level CL
                                LEFT JOIN groups G ON CL.class_level_id = G.class_level_id
                                WHERE CL.school_id = %s
                                GROUP BY CL.class_level_id
                                ORDER BY CL.class_level_id ASC", secure($school_id, 'int'));
        } else {
            $strSql = sprintf("SELECT * FROM ci_class_level WHERE school_id = %s ORDER BY class_level_id ASC", secure($school_id, 'int'));
        }

        $class_levels = array();
        $get_class_levels = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_class_levels->num_rows > 0) {
            while($class_level = $get_class_levels->fetch_assoc()) {
                $class_levels[] = $class_level;
            }
        }
        return $class_levels;
    }

    /**
     * Lây ra danh sách các class_levels của một trường cho API
     *
     * @return array
     */
    public function getClassLevelsForAPI($school_id, $getClassCnt = false) {
        global $db;

        $strSql = null;
        if ($getClassCnt) {
            $strSql = sprintf("SELECT CL.*, count(G.group_id) AS class_of_class_level_count FROM ci_class_level CL
                                LEFT JOIN groups G ON CL.class_level_id = G.class_level_id
                                WHERE CL.school_id = %s
                                GROUP BY CL.class_level_id
                                ORDER BY CL.class_level_id ASC", secure($school_id, 'int'));
        } else {
            $strSql = sprintf("SELECT * FROM ci_class_level WHERE school_id = %s ORDER BY class_level_id ASC", secure($school_id, 'int'));
        }

        $class_levels = array();
        $get_class_levels = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_class_levels->num_rows > 0) {
            while($class_level = $get_class_levels->fetch_assoc()) {
                $class_levels[] = $class_level;
            }
        }
        return $class_levels;
    }

    /**
     * Lấy ra class level theo ID
     *
     * @param $class_level_id
     * @return array|null
     * @throws Exception
     */
    public function getClassLevel($class_level_id) {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_class_level WHERE class_level_id = %s", secure($class_level_id, 'int'));
        $get_class_levels = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_class_levels->num_rows > 0) {
            return $get_class_levels->fetch_assoc();
        }
        return null;
    }
    /**
     * Lấy ra danh sách khối theo một danh sách ID đã biết.
     *
     * @param $classlevel_ids
     * @return array
     * @throws Exception
     */
    public function getClasslevelsByIds($classlevel_ids)
    {
        global $db;
        $results = array();
        if (count($classlevel_ids) == 0) {
            return $results;
        }

        $classlevel_ids = array_filter($classlevel_ids);
        $strCon = implode(',', $classlevel_ids);
        $strSql = sprintf('SELECT * FROM ci_class_level WHERE class_level_id IN (%s)', $strCon);
        $get_classlevels = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_classlevels->num_rows > 0) {
            while ($classlevel = $get_classlevels->fetch_assoc()) {
                $results[] = $classlevel;
            }
        }

        return $results;
    }
    /**
     * Nhập môn vào danh sách khối của trường
     *
     * @param $subject_id
     * @param $school_id
     * @param $shool_year
     * @param $govClassLevels
     * @throws Exception
     */
    public function insertClassLevelSubjectList($subject_id,$school_id,$school_year,$govClassLevels) {
        global $db;

        //Câu sql sẽ có dạng: INSERT INTO tbl_name (col_A,col_B,col_C) VALUES (1,2,3), (4,5,6), (7,8,9)
        //Build lên phần giá trị truyền vào.
        $strValues = "";
        $govClassLevels = array_unique($govClassLevels); //Bỏ đi giá trị bị lặp
        foreach ($govClassLevels as $govClassLevel) {
            $strValues .= "(". secure($subject_id, 'int') .",". secure($school_id, 'int') .",". secure($school_year) .",". secure($govClassLevel) ."),";
        }
        $strValues = trim($strValues, ",");

        $strSql = "INSERT INTO ci_class_level_subject (subject_id, school_id, school_year, gov_class_level) VALUES ".$strValues;

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }
    /**
     * Xóa môn ra khỏi danh sách khối.
     *
     * @param $subject_id
     * @param $school_id
     * @param $shool_year
     * @param $govClassLevels
     * @throws Exception
     */
    public function deleteClassLevelSubjectList($subject_id,$school_id,$school_year,$govClassLevels) {
        global $db;

        $strCon = implode(',', $govClassLevels);
        $strSql = sprintf("DELETE FROM ci_class_level_subject WHERE subject_id=%s AND school_id=%s AND school_year=%s AND gov_class_level IN (%s)",secure($subject_id, 'int'),secure($school_id, 'int'),secure($school_year),  $strCon);
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }
    /**
     * Tìm danh sách Khối của một trường theo tên.
     *
     * @param $school_id
     * @param $query
     * @param $classlevelIds: sẽ không đưa ra giáo viên có ID trong danh sách này
     * @return array
     * @throws Exception
     */
    public function searchClasslevel($school_id, $query, $classlevelIds)
    {
        global $db, $system;
        $results = array();

        $classlevelIds = array_unique($classlevelIds);
        $strCon = implode(',', $classlevelIds);
        if(is_empty($strCon)) {
            $strCon = "''";
        }
        $strSql = sprintf('SELECT *
                            FROM ci_class_level cl
                            WHERE (cl.class_level_name LIKE %1$s OR cl.description LIKE %1$s) AND cl.school_id = %2$s
                            AND cl.class_level_id NOT IN (%3$s)
                            ORDER BY cl.class_level_name ASC LIMIT %4$s',
            secure($query, 'search'), secure($school_id), $strCon, secure($system['min_results'], 'int', false));
        error_log('strSql = '.$strSql);
        $get_classlevels = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_classlevels->num_rows > 0) {
            while ($classlevel = $get_classlevels->fetch_assoc()) {
                $results[] = $classlevel;
            }
        }
        return $results;
    }
    /**
     * Lây ra danh sách các class_levels của một trường theo các môn được gán
     * @param $subject_id
     * @param $school_id
     * @param $school_year
     * @return array
     */
    public function getClassLevelsBySubjectId($subject_id,$school_id,$school_year) {
        global $db;

            $strSql = sprintf("SELECT cl.class_level_id,cls.class_level_subject_id,cl.class_level_name,cl.gov_class_level FROM `ci_class_level` cl JOIN `ci_class_level_subject` cls ON cl.gov_class_level = cls.gov_class_level WHERE cls.school_id=%s AND cl.school_id=%s AND cls.subject_id=%s AND cls.school_year= %s ORDER BY gov_class_level ASC", secure($school_id, 'int'),secure($school_id, 'int'),secure($subject_id, 'int'),secure($school_year));
        error_log('$strSql :'.$strSql);
        $class_levels = array();
        $get_class_levels = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_class_levels->num_rows > 0) {
            while($class_level = $get_class_levels->fetch_assoc()) {
                error_log('$class_level :'.json_encode($class_level));
                $class_levels[] = $class_level;
            }
        }
        error_log('$class_levels :'.json_encode($class_levels));
        return $class_levels;
    }
    /**
     * Lấy ra số lượng khối của một trường
     *
     * @param int $school_id
     * @return mixed
     * @throws Exception
     */
    public function getCountClassLevels($school_id = 0) {
        global $db;

        $strSql = sprintf("SELECT COUNT(class_level_id) AS level_count FROM ci_class_level WHERE school_id = %s", secure($school_id, 'int'));
        $get_count = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        return $get_count->fetch_assoc()['level_count'];
    }

    /**
     * Insert một class level vào trong DB
     *
     * @param array $args
     * @throws Exception
     */
    public function insertClassLevel(array $args = array()) {
        global $db;
        $this->validateInput($args);
        /* insert new class level */
        $strSql = sprintf("INSERT INTO ci_class_level (class_level_name,gov_class_level, school_id, description) VALUES (%s,%s, %s, %s)",
                            secure($args['class_level_name']), secure($args['gov_class_level'], 'int'), secure($args['school_id'], 'int'), secure($args['description']));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        return $db->insert_id;
    }

    /**
     * Thêm một class_level (một kiểm hàm khác hàm trên)
     *
     * @param $name
     * @param $schoolId
     * @param $description
     */
    public function insertDefaultCL($name, $schoolId, $description, $govClassLevel) {
        global $db;
        $strSql = sprintf("INSERT INTO ci_class_level (class_level_name, school_id, description, gov_class_level) VALUES (%s, %s, %s, %s)",
            secure($name), secure($schoolId, 'int'), secure($description), secure($govClassLevel, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        return $db->insert_id;
    }

    /**
     * Kiểm tra điều kiện đầu vào.
     *
     * @param array $args
     * @throws Exception
     */
    private function validateInput(array $args = array()) {
        /* validate school_id */
        if($args['school_id'] <= 0) {
            throw new Exception(__("You must select an existing school"));
        }

        if (!isset($args['class_level_name'])) {
            throw new Exception(__("You must enter class level name"));
        }

        if (strlen($args['class_level_name']) < 3) {
            throw new Exception(__("Class level name must be at least 3 characters long"));
        }

        if (strlen($args['class_level_name']) > 45) {
            throw new Exception(__("Class level name must be less than 45 characters long"));
        }

        if (isset($args['description']) && (strlen($args['description'])) > 300) {
            throw new Exception(__("Description length must be less than 300 characters long"));
        }
    }

    /**
     * Cập nhật một class level vào trong DB
     *
     * @param array $args
     * @throws Exception
     */
    public function updateClassLevel(array $args = array()) {
        global $db;

        /* validate $class_level_id */
        if ($args['class_level_id'] <= 0) {
            throw new Exception(__("You must select an existing class level"));
        }
        $this->validateInput($args);

        $strSql = sprintf("UPDATE ci_class_level SET class_level_name = %s, school_id = %s, description = %s WHERE
                            class_level_id = %s", secure($args['class_level_name']), secure($args['school_id'], 'int'),
                            secure($args['description']), secure($args['class_level_id'], 'int'));
        /* update class level */
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Xóa một class level khỏi hệ thống
     *
     * @param $class_level_id
     * @throws Exception
     */
    public function deleteClassLevel($class_level_id) {
        global $db;
        /* delete the page */
        $db->query(sprintf('DELETE FROM ci_class_level WHERE class_level_id = %1$s AND NOT EXISTS
                              (SELECT group_id FROM groups WHERE class_level_id = %1$s)', secure($class_level_id, 'int'))) or _error(SQL_ERROR_THROWEN);
    }

    /* ---------- CLASS LEVEL - MEMCACHED ---------- */

    /**
     * Lây ra danh sách các class_levels của một trường
     *
     * @return array
     */
    public function getClassLevels4Memcache($school_id) {
        global $db;

        $strSql = null;
        $strSql = sprintf("SELECT CL.*, count(G.group_id) AS cnt FROM ci_class_level CL
                                LEFT JOIN groups G ON CL.class_level_id = G.class_level_id AND G.status = 1
                                WHERE CL.school_id = %s
                                GROUP BY CL.class_level_id
                                ORDER BY CL.class_level_id ASC", secure($school_id, 'int'));
        $class_levels = array(
            'ids' => [],
            'lists' => []
        );

        $get_class_levels = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_class_levels->num_rows > 0) {
            while($class_level = $get_class_levels->fetch_assoc()) {
                $class_levels['ids'][] = $class_level['class_level_id'];
                $class_levels['lists'][$class_level['class_level_id']] = $class_level;
            }
        }
        return $class_levels;
    }

    /**
     * Lây ra class_level của 1 trường
     *
     * @return array
     */
    public function getClassLevel4Memcache($classLevelId) {
        global $db;

        $strSql = sprintf("SELECT CL.*, count(G.group_id) AS cnt FROM ci_class_level CL
                                LEFT JOIN groups G ON CL.class_level_id = G.class_level_id AND G.status = '1'
                                WHERE CL.class_level_id = %s", secure($classLevelId, 'int'));
        error_log('dao_class_level 371 : $strSql :'.$strSql);
        $get_class_levels = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_class_levels->num_rows > 0) {
            return $get_class_levels->fetch_assoc();
        }
        return null;
    }





}
?>