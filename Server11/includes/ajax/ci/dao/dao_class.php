<?php

/**
 * Class ClassDAO: thao tác với bảng groups của hệ thống. Ở đây Lớp được lưu trong bảng groups.
 */
class ClassDAO {
    /**
     * __construct
     *
     */
    public function __construct()
    {
    }

    /**
     * Kiểm tra username của class đã tồn tại chưa.
     *
     * @param string $username
     * @return boolean
     */
    private function checkUsername($username)
    {
        global $db;
        $query = $db->query(sprintf("SELECT * FROM groups WHERE group_name = %s", secure($username))) or _error(SQL_ERROR_THROWEN);

        if ($query->num_rows > 0) {
            return true;
        }
        return false;
    }

    /**
     * Kiểm tra đầu vào cho hàm insert và update của CLASS
     *
     * @param array $args
     * @param bool|true $isCreate
     * @throws Exception
     */
    private function validateInput (array $args = array()) {
        global $db;
        /* validate title */
        if (is_empty($args['title'])) {
            throw new Exception(__("You must enter class name"));
        }
        if (strlen($args['title']) < 2) {
            throw new Exception(__("Name must be at least 2 characters long"));
        }
        if (isset($args['school_id'])) {
            $strSql = sprintf("SELECT C.group_id, C.group_title, C.group_members FROM groups C INNER JOIN ci_class_level CL ON C.class_level_id = CL.class_level_id
                    WHERE CL.school_id = %s AND C.group_title = %s AND C.group_id != %s AND C.status = 1 ORDER BY C.class_level_id, C.group_title
                     ASC", secure($args['school_id'], 'int'), secure($args['title']), secure($args['class_id'], 'int'));
            $get_group = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
            if($get_group->num_rows > 0) {
                throw new Exception(__("Group name is existing"));
            }

            // Lấy danh sách lớp của trường
            $classes = getSchoolData($args['school_id'], SCHOOL_CLASSES);
            foreach ($classes as $class) {
                if(strtolower($class['group_name']) == strtolower($args['title'])) {
                    throw new Exception(__("Group name is existing"));
                }
            }
        }
        /*
        if (is_empty($args['username'])) {
            throw new Exception(__("You must enter a username for your class"));
        }
        if (!valid_username($args['username'])) {
            throw new Exception(__("Please enter a valid username (a-z0-9_.)"));
        }

        if ($isCreate || (strtolower($args['username']) != strtolower($args['username_old']))) {
            if ($this->checkUsername($args['username'])) {
                throw new Exception(__("Sorry, it looks like this username") . " <strong>" . $args['username'] . "</strong> " . __("belongs to an existing one"));
            }
        }
        */

        if (!isset($args['class_level_id']) || ($args['class_level_id'] < 1)) {
            throw new Exception(__("You must select a class level"));
        }
        if ((!is_empty($args['telephone'])) && (strlen($args['telephone']) > 50)) {
            throw new Exception(__("Telephone length must be less than 50 characters long"));
        }
        /*if ((!is_empty($args['email'])) && (strlen($args['email']) > 50)) {
            throw new Exception(__("Email length must be less than 50 characters long"));
        }*/
        if(!is_empty($args['email']) && !valid_email($args['email'])) {
            throw new Exception(__("Please enter a valid email address"));
        }
    }

    /**
     * Thêm mới một CLASS vào DB
     *
     * @param array $args
     * @return mixed
     * @throws Exception
     */
    public function createClass(array $args = array()) {
        global $db, $date;
        $this->validateInput($args);

        $strSql = sprintf("INSERT INTO groups (group_privacy, group_admin, group_name, group_category, group_title, group_description, group_date,
                              group_members, class_level_id, telephone, email, camera_url)
                           VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
            secure('closed'), secure($args['user_id'], 'int'), secure(time()), secure(2, 'int'),
            secure($args['title']), secure($args['description']) , secure($date), secure(1, 'int'),
            secure($args['class_level_id'], 'int'), secure($args['telephone']),
            secure($args['email']), secure($args['camera_url']));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        return $db->insert_id;
    }

    /**
     * Cập nhật thông tin một class
     *
     * @param array $args
     * @throws Exception
     */
    public function editClass(array $args = array()) {
        global $db;
        $this->validateInput($args);

        $strSql = sprintf("UPDATE groups SET group_title = %s, group_description = %s,
                              class_level_id = %s, telephone = %s, email = %s, camera_url = %s WHERE group_id = %s",
            secure($args['title']), secure($args['description']),
            secure($args['class_level_id'], 'int'), secure($args['telephone']),
            secure($args['email']), secure($args['camera_url']), secure($args['group_id'], 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * lên lớp
     *
     * @param array $args
     * @throws Exception
     */
    public function upClass(array $args = array()) {
        global $db;
        $this->validateInput($args);

        $strSql = sprintf("UPDATE groups SET group_title = %s,
                              class_level_id = %s WHERE group_id = %s",
            secure($args['title']),
            secure($args['class_level_id'], 'int'),
            secure($args['group_id'], 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Lấy ra danh sách class được quản lý bởi một user
     *
     * @param $user_id
     * @return array
     * @throws Exception
     */
    public function getClasses($user_id) {
        global $db;
        //UPDATE START MANHDD 04/06/2021
//        $strSql = sprintf("SELECT G.*, UM.role_id FROM groups G INNER JOIN ci_user_manage UM ON G.group_id = UM.object_id
//                    WHERE UM.object_type = %s AND UM.user_id = %s AND UM.role_id = %s AND G.status = '1' ORDER BY G.group_title ASC",
//                    MANAGE_CLASS, secure($user_id, 'int'), PERMISSION_MANAGE);
        $strSql = sprintf("SELECT G.*, UM.role_id FROM groups G INNER JOIN ci_user_manage UM ON G.group_id = UM.object_id
                    WHERE UM.object_type = %s AND UM.user_id = %s AND UM.role_id IN (%s,%s) AND G.status = '1' ORDER BY G.group_title ASC",
                    MANAGE_CLASS, secure($user_id, 'int'), PERMISSION_MANAGE,PERMISSION_ALL);
//UPDATE END MANHDD 04/06/2021
        $classes = array();
        $get_classes = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_classes->num_rows > 0) {
            while($class = $get_classes->fetch_assoc()) {
                $class['group_picture'] = get_picture($class['group_picture'], 'class');
                $class['group_cover'] = get_picture($class['group_cover'], 'class');
                $classes[] = $class;
            }
        }
        return $classes;
    }
    /**
     * Lấy ra khối khi biết class_id
     *
     * @param $class_id
     * @return array
     * @throws Exception
     */
    public function getClassLevelsbyClassId($class_id) {
        global $db;

        $strSql = sprintf("SELECT ccl.* FROM `ci_class_level` ccl JOIN `groups` g ON ccl.class_level_id = g.class_level_id WHERE g.group_id = %s", secure($class_id, 'int'));

        $get_classLevel = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_classLevel->num_rows > 0) {
            return $get_classLevel->fetch_assoc();
        }
        return array();
    }
    /**
     * Lấy ra thông tin lớp của trẻ.
     * Lấy thêm thông tin học phí trả trước của trẻ
     *
     * @param $child_id
     * @return array
     */
    public function getClassOfChild($child_id, $school_id = 0) {
        global $db;

        $strSql = null;
        if ($school_id == 0) {
            $strSql = sprintf("SELECT G.*, SC.school_id FROM groups G INNER JOIN ci_school_child SC
                                ON G.group_id = SC.class_id AND SC.status = %s AND SC.child_id = %s", secure(STATUS_ACTIVE, 'int'), secure($child_id, 'int'));
        } else {
            $strSql = sprintf("SELECT G.*, SC.school_id FROM groups G INNER JOIN ci_school_child SC
                                ON G.group_id = SC.class_id AND SC.status = %s AND SC.child_id = %s AND SC.school_id = %s", secure(STATUS_ACTIVE, 'int'), secure($child_id, 'int'), secure($school_id, 'int'));
        }

        $class = null;
        $get_class = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_class->num_rows > 0) {
            $class = $get_class->fetch_assoc();
            $class['group_picture'] = get_picture($class['group_picture'], 'class');
        }
        return $class;
    }

    /**
     * Lấy ra thông tin class theo username và user_id (user quản lý class đó)
     *
     * @param $username
     * @param $user_id
     * @return array
     */
    public function getClassByUsername($username, $user_id) {
        global $system;
        $class = $_SESSION[$username];
        if (is_null($class) || $system['is_mobile']) {
            $class = $this->_getClassByUsername($username, $user_id);
            $_SESSION[$username] = $class;
        }

        return $class;
    }

    /**
     * Lấy ra thông tin class theo username và user_id (user quản lý class đó)
     *
     * @param $username
     * @param $user_id
     * @return array
     */
    private function _getClassByUsername($username, $user_id) {
        global $db;

        $strSql = sprintf("SELECT G.*, UM.role_id, CL.class_level_name, CL.school_id FROM groups G INNER JOIN ci_user_manage UM ON G.group_name 
= %s AND ((UM.object_type = %s AND UM.user_id = %s AND role_id = %s) OR (G.group_id = UM.object_id AND UM.object_type = %s AND UM.user_id = %s 
AND UM.role_id = %s) OR G.group_admin = %s)
                           INNER JOIN ci_class_level CL ON G.class_level_id = CL.class_level_id AND (CL.school_id = UM.object_id OR G.group_id = UM.object_id)",
                    secure($username), MANAGE_SCHOOL, secure($user_id, 'int'), PERMISSION_JUST_VIEW, MANAGE_CLASS, secure($user_id, 'int'), PERMISSION_MANAGE, secure($user_id, 'int'));

        $get_class = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_class->num_rows == 0) {
            _error(403);
        };
        return $get_class->fetch_assoc();
    }

    /**
     * Lấy ra thông tin của một class
     *
     * @param $class_id
     * @return array|null
     * @throws Exception
     */
    public function getClass($class_id) {
        global $db;

        $strSql = sprintf("SELECT groups.* FROM groups WHERE class_level_id > 0 AND group_id = %s", secure($class_id, 'int'));

        $get_class = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_class->num_rows > 0) {
            $class = $get_class->fetch_assoc();
            $class['group_picture'] = get_picture($class['group_picture'], 'class');

            return $class;
        }
        return null;
    }

    /**
     * Lấy ra thông tin lớp theo ID và kiểm tra luôn người dùng có phải giáo viên lớp không.
     * Phục vụ chức năng NOTIFICATION
     *
     * @param $classId
     * @param $teacherId
     * @return array|null
     * @throws Exception
     */
    public function getClassByIdNTeacherId($classId, $teacherId) {
        global $db;

        $strSql = sprintf("SELECT G.group_name AS class_name FROM groups G
                      INNER JOIN ci_user_manage UM ON G.group_id = UM.object_id AND UM.object_type = %s AND UM.user_id = %s AND UM.object_id = %s
                      WHERE G.class_level_id > 0", MANAGE_CLASS, secure($teacherId, 'int'), secure($classId, 'int'));

        $get_class = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_class->num_rows > 0) {
            $class = $get_class->fetch_assoc();
            return $class;
        }
        return null;
    }

    /**
     * Lấy ra thông tin class bao gồm cả vai trò của user với class đó.
     *
     * @param $class_id
     * @param $user_id
     * @return array|null
     * @throws Exception
     */
    public function getClassWithUser($class_id, $user_id) {
        global $db;

        $strSql = sprintf("SELECT G.*, UM.role_id FROM groups G INNER JOIN ci_user_manage UM ON G.group_id = UM.object_id
            WHERE UM.object_type = %s AND UM.user_id = %s AND G.group_id = %s",
            MANAGE_CLASS, secure($user_id, 'int'), secure($class_id, 'int'));

        $get_class = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_class->num_rows > 0) {
            $class = $get_class->fetch_assoc();
            $class['group_picture'] = get_picture($class['group_picture'], 'class');

            return $class;
        }

        return null;
    }

    /**
     * Lấy ra danh sách các lớp của một trường
     *
     * @param $school_id
     * @return array
     * @throws Exception
     */
    public function getClassesOfSchool($school_id, $getChildCnt = false) {
        global $db;

        $strSql = null;
        if ($getChildCnt) {
            $strSql = sprintf("SELECT C.*, CL.class_level_name, count(CC.child_id) AS cnt FROM groups C
                                INNER JOIN ci_class_level CL ON C.class_level_id = CL.class_level_id AND CL.school_id = %s
                                LEFT JOIN ci_class_child CC ON C.group_id = CC.class_id WHERE C.status = 1
                                GROUP BY C.group_id
                                ORDER BY C.class_level_id, C.group_title ASC", secure($school_id, 'int'));
        } else {
            $strSql = sprintf("SELECT C.*, CL.class_level_name FROM groups C INNER JOIN ci_class_level CL ON C.class_level_id = CL.class_level_id
                    WHERE CL.school_id = %s AND C.status = 1 ORDER BY C.class_level_id, C.group_title ASC", secure($school_id, 'int'));
        }

        $classes = array();
        $get_classes = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_classes->num_rows > 0) {
            while($class = $get_classes->fetch_assoc()) {
                //$class['group_picture'] = get_picture($class['group_picture'], 'class');
                $class['teachers'] = $this->getTeachers($class['group_id']);
                $classes[] = $class;
            }
        }
        return $classes;
    }

    /**
     * Lấy ra danh sách các lớp của một trường cho API
     *
     * @param $school_id
     * @return array
     * @throws Exception
     */
    public function getClassesOfSchoolForAPI($school_id, $getChildCnt = false) {
        global $db;

        $strSql = null;
        if ($getChildCnt) {
            $strSql = sprintf("SELECT C.group_id, C.group_title, count(CC.child_id) AS cnt FROM groups C
                                INNER JOIN ci_class_level CL ON C.class_level_id = CL.class_level_id AND CL.school_id = %s
                                LEFT JOIN ci_class_child CC ON C.group_id = CC.class_id WHERE C.status = 1
                                GROUP BY C.group_id
                                ORDER BY C.class_level_id, C.group_title ASC", secure($school_id, 'int'));
        } else {
            $strSql = sprintf("SELECT C.group_id, C.group_title FROM groups C INNER JOIN ci_class_level CL ON C.class_level_id = CL.class_level_id
                    WHERE CL.school_id = %s AND C.status = 1 ORDER BY C.class_level_id, C.group_title ASC", secure($school_id, 'int'));
        }

        $classes = array();
        $get_classes = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_classes->num_rows > 0) {
            while($class = $get_classes->fetch_assoc()) {
                //$class['group_picture'] = get_picture($class['group_picture'], 'class');
                $classes[] = $class;
            }
        }
        return $classes;
    }

    /**
     * Lấy ra danh sách lớp của một giáo viên trong một trường. Phục vụ cho chức năng notification (tạo link)
     *
     * @param $school_id
     * @param $teacher_id
     * @return array
     * @throws Exception
     */
    public function getClassesBySchoolAndTeacher($school_id, $teacher_id) {
        global $db;

        $strSql = sprintf("SELECT C.* FROM groups C INNER JOIN ci_class_level CL ON C.class_level_id = CL.class_level_id AND CL.school_id = %s
                            INNER JOIN ci_user_manage UM ON C.group_id = UM.object_id AND UM.object_type = %s AND UM.user_id = %s",
                            secure($school_id, 'int'), MANAGE_CLASS, secure($teacher_id, 'int'));

        $classes = array();
        $get_classes = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_classes->num_rows > 0) {
            while($class = $get_classes->fetch_assoc()) {
                $classes[] = $class;
            }
        }
        return $classes;
    }

    /**
     * Lấy ra danh sách lớp của một giáo viên trong một khối. Phục vụ cho chức năng notification (tạo link)
     *
     * @param $class_level_id
     * @param $teacher_id
     * @return array
     */
    public function getClassesByClassLevelAndTeacher($class_level_id, $teacher_id) {
        global $db;

        $strSql = sprintf("SELECT C.* FROM groups C INNER JOIN ci_user_manage UM ON C.group_id = UM.object_id
                            AND C.class_level_id = %s AND UM.object_type = %s AND UM.user_id = %s",
                            secure($class_level_id, 'int'), MANAGE_CLASS, secure($teacher_id, 'int'));

        $classes = array();
        $get_classes = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_classes->num_rows > 0) {
            while($class = $get_classes->fetch_assoc()) {
                $classes[] = $class;
            }
        }
        return $classes;
    }

    /**
     * Lấy ra lớp của một giáo viên và học sinh. Phục vụ cho chức năng notification (tạo link)
     *
     * @param $child_id
     * @param $teacher_id
     * @return array
     */
    public function getClassByChildAndTeacher($child_id, $teacher_id) {
        global $db;

        $strSql = sprintf("SELECT G.* FROM groups G INNER JOIN ci_class_child CC ON G.group_id = CC.class_id AND CC.child_id = %s
                            INNER JOIN ci_user_manage UM ON G.group_id = UM.object_id AND UM.object_type = %s AND UM.user_id = %s",
                            secure($child_id, 'int'), MANAGE_CLASS, secure($teacher_id, 'int'));

        $get_class = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_class->num_rows > 0) {
            return $get_class->fetch_assoc();
        }
        return null;
    }

    /**
     * Lấy ra danh sách lớp của một trường, mà 1 phụ huynh có con học trong lớp đó.
     *
     * TẠM THỜI: KHÔNG DÙNG ĐẾN
     *
     * @param $school_id
     * @param $parent_id
     * @return array
     * @throws Exception
     */
    public function getClassesBySchoolAndParent($school_id, $parent_id) {
        global $db;

        $strSql = sprintf("SELECT C.* FROM groups C INNER JOIN ci_school_child SC ON C.group_id = SC.class_id AND SC.school_id = %s
                            INNER JOIN ci_user_manage UM ON SC.child_id = UM.object_id AND UM.object_type = %s AND UM.user_id = %s",
                            secure($school_id, 'int'), MANAGE_CHILD, secure($parent_id, 'int'));

        $classes = array();
        $get_classes = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_classes->num_rows > 0) {
            while($class = $get_classes->fetch_assoc()) {
                $classes[] = $class;
            }
        }
        return $classes;
    }

    /**
     * Lấy ra danh sách class name của một trường (dùng để build combobox chọn.
     *
     * @param $school_id
     * @return array
     * @throws Exception
     */
    public function getClassNamesOfSchool($school_id) {
        $KEY_NAME = SESSION_KEY_CLASS_NAME_PREFIX.$school_id;

        $classes = $_SESSION[$KEY_NAME];
        if (is_null($classes)) {
            $classes = $this->_getClassNamesOfSchool($school_id);
            $_SESSION[$KEY_NAME] = $classes;
        }

        return $classes;
    }

    /**
     * Lấy ra thông tin lớp của một trường.
     * LƯU Ý: Chỉ dùng trong các chức năng quản lý trường.
     *
     * @param $schoolId
     * @param $classId
     */
    public function getClassInSchoolManager($schoolId, $classId) {
        $classes = $this->getClassNamesOfSchool($schoolId);
        foreach ($classes as $class) {
            if ($class['group_id'] == $classId) {
                $result = $class;
                return $result;
            }
        }
        return null;
    }
    /**
     * Refresh lại session khi cập nhật/thêm lớp.
     *
     * @param $school_id
     */
    public function refreshClassInSession($school_id) {
        $KEY_CLASS_NAME = SESSION_KEY_CLASS_NAME_PREFIX.$school_id;
        $classes = $_SESSION[$KEY_CLASS_NAME];
        if (!is_null($classes)) {
            $classes = $this->_getClassNamesOfSchool($school_id);
            $_SESSION[$KEY_CLASS_NAME] = $classes;
        }
    }

    /**
     * Lấy ra danh sách class name của một trường (dùng để build combobox chọn).
     *
     * @param $school_id
     * @return array
     * @throws Exception
     */
    private function _getClassNamesOfSchool($school_id) {
        global $db;

        $strSql = sprintf("SELECT C.group_id, C.group_name, C.group_title, C.group_members, C.class_level_id FROM groups C
                            INNER JOIN ci_class_level CL ON C.class_level_id = CL.class_level_id
                            WHERE CL.school_id = %s ORDER BY C.class_level_id, C.group_title ASC", secure($school_id, 'int'));

        $classes = array();
        $get_classes = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_classes->num_rows > 0) {
            while($class = $get_classes->fetch_assoc()) {
                $classes[] = $class;
            }
        }
        return $classes;
    }

    /**
     * Lấy ra class_id của một trường khi biết tên của lớp đó.
     *
     * @param $class_name
     * @param $school_id
     * @return int
     * @throws Exception
     */
    public function getClassIdByName($class_name, $school_id) {
        global $db;

        $strSql = sprintf("SELECT C.group_id FROM groups C INNER JOIN ci_class_level CL ON C.class_level_id = CL.class_level_id
                    AND CL.school_id = %s AND UCASE(C.group_title) = %s", secure($school_id, 'int'), strtoupper(secure($class_name)));

        $get_class = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_class->num_rows > 0) {
            return $get_class->fetch_assoc()['group_id'];
        }
        return 0;
    }

    /**
     * Lấy ra danh sách lớp của một khối
     *
     * @param $levelId
     * @return array
     * @throws Exception
     */
    public function getClassesOfLevel($levelId) {
        global $db;

        $strSql = sprintf("SELECT C.group_id, C.group_name, C.group_title, C.group_members FROM groups C
                            WHERE C.class_level_id = %s ORDER BY C.class_level_id, C.group_title ASC", secure($levelId, 'int'));

        $classes = array();
        $get_classes = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_classes->num_rows > 0) {
            while($class = $get_classes->fetch_assoc()) {
                $classes[] = $class;
            }
        }
        return $classes;
    }

    /**
     * Lấy ra danh sách ClassID của một khối
     *
     * @param $levelId
     * @return array
     * @throws Exception
     */
    public function getClassIdOfLevel($levelId) {
        global $db;

        $strSql = sprintf("SELECT C.group_id FROM groups C WHERE C.class_level_id = %s ORDER BY C.class_level_id, C.group_title ASC", secure($levelId, 'int'));

        $classIds = array();
        $get_classes = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_classes->num_rows > 0) {
            while($class = $get_classes->fetch_assoc()) {
                $classIds[] = $class['group_id'];
            }
        }
        return $classIds;
    }

    /**
     * Lấy ra danh sách Class của một khối (phục vụ API)
     *
     * @param $levelId
     * @return array
     */
    public function getClassOfLevel($levelId) {
        global $db;

        $strSql = sprintf("SELECT C.group_id, C.group_name, C.group_title FROM groups C WHERE C.class_level_id = %s ORDER BY C.class_level_id, C.group_title ASC", secure($levelId, 'int'));

        $classes = array();
        $get_classes = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_classes->num_rows > 0) {
            while($class = $get_classes->fetch_assoc()) {
                $classes[] = $class;
            }
        }
        return $classes;
    }

    /**
     * Lấy ra số lượng ClassID của một khối
     *
     * @param $levelId
     * @return array
     * @throws Exception
     */
    public function getCountClassOfLevel($levelId) {
        global $db;

        $strSql = sprintf("SELECT count(C.group_id) FROM groups C WHERE C.class_level_id = %s ORDER BY C.class_level_id, C.group_title ASC", secure($levelId, 'int'));

        $total_class = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        return $total_class;
    }

    /**
     * Lấy ra danh sách giáo viên của một lớp.
     *
     * @param $class_id
     * @return array
     * @throws Exception
     */
    private function getTeachers($class_id) {
        global $db;

        $strSql = sprintf("SELECT T.user_id, T.user_name, T.user_fullname, T.user_gender FROM users T INNER JOIN ci_user_manage UM ON T.user_id = UM.user_id
                    WHERE UM.object_type = %s AND UM.role_id = %s AND UM.object_id = %s ORDER BY T.user_firstname ASC", MANAGE_CLASS, PERMISSION_MANAGE, secure($class_id, 'int'));

        $teachers = array();
        $get_teachers = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_teachers->num_rows > 0) {
            while($teacher = $get_teachers->fetch_assoc()) {
                $teachers[] = $teacher;
            }
        }
        return $teachers;
    }

    /**
     * Lấy ra số lượng lớp của một trường
     *
     * @param $school_id
     * @return mixed
     * @throws Exception
     */
    public function getCountClasses($school_id) {
        global $db;

        $strSql = "SELECT COUNT(G.group_id) AS class_count FROM groups G INNER JOIN ci_class_level CL ON G.class_level_id = CL.class_level_id
                    WHERE CL.school_id = %s";

        $get_count = $db->query(sprintf($strSql, secure($school_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        
        if($get_count->num_rows > 0) {
            return $get_count->fetch_assoc()['class_count'];
        }
        return 0;
    }

    /**
     * Đưa danh sách user thành thành viên của lớp (group member)
     *
     * @param $classId
     * @param $userIds
     * @throws Exception
     */
    public function addUserToClass($classId, $userIds) {
        global $db;

        if ($classId == 0) return;
        // Xóa user cũ tránh bị lỗi duplicate
        $userIds = array_unique($userIds); //Bỏ đi giá trị bị lặp
        $memberIds = $this->_getMemberIdOnGroup($classId, $userIds);

        //Bỏ đi những người đã là thành viên của nhóm
        $userIds = array_diff($userIds, $memberIds);

        if (count($userIds) == 0) return;

        //1. Đưa danh sách user vào nhóm
        //Câu sql sẽ có dạng: INSERT INTO tbl_name (col_A,col_B,col_C) VALUES (1,2,3), (4,5,6), (7,8,9)
        //Build lên phần giá trị truyền vào.
        $strValues = "";
        foreach ($userIds as $id) {
            //group_id, user_id, approved
            $strValues .= "(". secure($classId, 'int') . "," . secure($id, 'int') . ",'1'),";
        }
        $strValues = trim($strValues, ",");

        $strSql = "INSERT INTO groups_members (group_id, user_id, approved) VALUES ".$strValues;

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        //2. Tăng số thành viên của group
        $strSql = sprintf("UPDATE groups SET group_members = group_members + %s WHERE group_id = %s", count($userIds), $classId);
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Cho user làm thanh viên của nhiều nhóm
     *
     * @param $userId
     * @param $classIds
     * @throws Exception
     */
    public function addClassToUser($userId, $classIds) {
        global $db;

        if ($userId == 0) return;
        if(count($classIds) == 0) return;
        // Xóa user cũ tránh bị lỗi duplicate
        $classIds = array_unique($classIds); //Bỏ đi giá trị bị lặp
        $groupIds = $this->_getGroupIdOnMember($userId, $classIds);

        //Bỏ đi những người lớp đã là thanh viên rồi
        $classIds = array_diff($classIds, $groupIds);

        if (count($classIds) == 0) return;

        //1. Đưa danh sách user vào nhóm
        //Câu sql sẽ có dạng: INSERT INTO tbl_name (col_A,col_B,col_C) VALUES (1,2,3), (4,5,6), (7,8,9)
        //Build lên phần giá trị truyền vào.
        $strValues = "";
        foreach ($classIds as $id) {
            //group_id, user_id, approved
            $strValues .= "(". secure($userId, 'int') . "," . secure($id, 'int') . ",'1'),";
        }
        $strValues = trim($strValues, ",");

        $strSql = "INSERT INTO groups_members (user_id, group_id, approved) VALUES ".$strValues;

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        //2. Tăng số thành viên của group
        $strCon = implode(',', $classIds);
        $strSql = sprintf("UPDATE groups SET group_members = group_members + 1 WHERE group_id IN (%s)", $strCon);
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Lấy ra danh sách ID đã là thành viên nhớm từ một tập ID cho trước.
     *
     * @param $groupId
     * @param $userIds
     * @return array
     * @throws Exception
     */
    private function _getMemberIdOnGroup($groupId, $userIds) {
        global $db;

        $strCon = implode(',', $userIds);
        $strSql = sprintf("SELECT user_id FROM groups_members WHERE group_id = %s AND user_id IN (%s)", secure($groupId, 'int'), $strCon);
        $memberIds = array();
        if(count($userIds) > 0) {
            $get_members = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
            if($get_members->num_rows > 0) {
                while($member = $get_members->fetch_assoc()) {
                    $memberIds[] = $member['user_id'];
                }
            }
        }
        return $memberIds;
    }

    /**
     * Lấy danh sách nhóm user đã tham gia từ một tập groupId cho trước
     *
     * @param $userId
     * @param $groupIds
     * @return array
     * @throws Exception
     */
    private function _getGroupIdOnMember($userId, $groupIds) {
        global $db;

        $strCon = implode(',', $groupIds);
        $strSql = sprintf("SELECT group_id FROM groups_members WHERE user_id = %s AND group_id IN (%s)", secure($userId, 'int'), $strCon);

        $memberIds = array();
        $get_members = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_members->num_rows > 0) {
            while($member = $get_members->fetch_assoc()) {
                $memberIds[] = $member['group_id'];
            }
        }
        return $memberIds;
    }

    /**
     * Xóa danh sách user ra khỏi thành viên lớp.
     *
     * @param $classId
     * @param $userIds
     * @throws Exception
     */
    public function deleteUserFromClass($classId, $userIds) {
        global $db;

        $userIds = array_unique($userIds); //Bỏ đi giá trị bị lặp
        //$strCon = implode(',', $userIds);

        // Lấy những thành viên của group
        $strSql = sprintf("SELECT user_id FROM groups_members WHERE group_id = %s", secure($classId));

        $get_user = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $oldUserIds = array();
        if($get_user->num_rows > 0) {
            while($userId = $get_user->fetch_assoc()['user_id']){
                $oldUserIds[] = $userId;
            }
        }

        $deleteUserIds = array_intersect($userIds, $oldUserIds);
        $strCon = implode(',', $deleteUserIds);
        if(count($deleteUserIds) > 0) {
            $strSql = sprintf("DELETE FROM groups_members WHERE group_id = %s AND user_id IN (%s)", secure($classId, 'int'), $strCon);
            $db->query($strSql) or _error(SQL_ERROR_THROWEN);

            //2. Giảm số thành viên của group
            $strSql = sprintf('UPDATE groups SET group_members = IF(group_members < %1$s, 0, group_members - %1$s) WHERE group_id = %2$s', count($deleteUserIds), $classId);
            $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        }
    }

    /**
     * Loại bỏ group khỏi giáo viên và giảm số lượng thành viên của group
     *
     * @param $userId
     * @param $classIds
     * @throws Exception
     */
    public function deleteClassFromUser($userId, $classIds) {
        global $db;

        if(count($classIds) == 0) return;
        $classIds = array_unique($classIds); //Bỏ đi giá trị bị lặp
        //$strCon = implode(',', $classIds);
        $strSql = sprintf("SELECT group_id FROM groups_members WHERE user_id = %s", secure($userId, 'int'));

        $get_class = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $oldClassIds = array();
        if($get_class->num_rows > 0) {
            while($classId = $get_class->fetch_assoc()['group_id']){
                $oldClassIds[] = $classId;
            }
        }

        $deleteClassIds = array_intersect($classIds, $oldClassIds);
        $strCon = implode(',', $deleteClassIds);
        if(count($deleteClassIds) > 0) {
            $strSql = sprintf("DELETE FROM groups_members WHERE user_id = %s AND group_id IN (%s)", secure($userId, 'int'), $strCon);
            $db->query($strSql) or _error(SQL_ERROR_THROWEN);

            //2. giảm số thành viên của group
            $strSql = sprintf('UPDATE groups SET group_members = IF(group_members < 1, 0, group_members - 1) WHERE group_id IN (%s)', $strCon);
            $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        }
    }

    /**
     * Hàm xóa một class khỏi hệ thống. Chỉ những lớp không có học sinh nào mới xóa được
     *
     * @param $class_id
     * @param $shoolAdminId
     * @throws Exception
     */
    public function deleteClass($class_id, $shoolAdminId)
    {
        global $db, $user;
        //Nếu là admin của trường thì được xóa
        if ($user->_data['user_id'] == $shoolAdminId) {
            $strSql = sprintf('DELETE FROM groups WHERE group_id = %1$s AND (class_level_id > 0) AND
                            NOT EXISTS (SELECT child_id FROM ci_class_child WHERE class_id = %1$s AND status = 1)', secure($class_id, 'int'));
        } else {
            //Kô phải admin thì người tạo ra mới được xóa
            $strSql = sprintf('DELETE FROM groups WHERE group_id = %1$s AND (class_level_id > 0) AND group_admin = %2$s AND
                            NOT EXISTS (SELECT child_id FROM ci_class_child WHERE class_id = %1$s AND status = 1)',
                secure($class_id, 'int'), secure($user->_data['user_id'], 'int'));
        }
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /* ---------- CLASS - MEMCACHED ---------- */

    /**
     * Lấy ra danh sách các lớp của một trường
     *
     * @param $school_id
     * @return array
     * @throws Exception
     */
    public function getClassesOfSchool4Memcache($school_id) {
        global $db;

        $strSql = sprintf("SELECT C.group_id, C.group_name, C.group_title, group_picture, group_description, group_members, telephone, email, camera_url,    
                          CL.class_level_name, CL.school_id, count(CC.child_id) AS cnt FROM groups C
                            INNER JOIN ci_class_level CL ON C.class_level_id = CL.class_level_id AND CL.school_id = %s
                            LEFT JOIN ci_class_child CC ON C.group_id = CC.class_id WHERE C.status = 1
                            GROUP BY C.group_id
                            ORDER BY C.class_level_id, C.group_title ASC", secure($school_id, 'int'));


        $classes = array(
            'ids' => [],
            'lists' => []
        );
        $get_classes = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_classes->num_rows > 0) {
            while($class = $get_classes->fetch_assoc()) {
                $class['group_picture'] = get_picture($class['group_picture'], 'class');
                //$class['teachers'] = $this->getTeachers($class['group_id']);
                $classes['ids'][] = $class['group_id'];
                $classes['lists'][$class['group_id']] = $class;
            }
        }
        return $classes;
    }

    /**
     * Lấy ra thông tin class theo username
     *
     * @param $username
     * @param $user_id
     * @return array
     */
    public function getClass4Memcache($classId) {
        global $db;

        $strSql = sprintf("SELECT SC.school_status, G.group_id, G.group_privacy,CSY.school_year, G.group_category, G.group_admin, G.group_name, G.group_title, G.group_picture, G.group_description, G.group_members, G.class_level_id, G.telephone, G.email, G.camera_url, G.group_date, 
                           CL.class_level_name, CL.school_id, count(CC.child_id) AS cnt FROM groups G                           
                           INNER JOIN ci_class_level CL ON G.group_id = %s AND G.class_level_id = CL.class_level_id
                           INNER JOIN ci_school_configuration SC ON SC.school_id = CL.school_id
                           LEFT JOIN ci_class_child CC ON G.group_id = CC.class_id AND CC.status = %s 
                               JOIN ci_class_school_year CSY ON CC.class_id = CSY.class_id", secure($classId), secure(STATUS_ACTIVE, 'int'));

        $class = null;
        $get_class = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_class->num_rows > 0) {
            $class = $get_class->fetch_assoc();
            $class['group_picture'] = get_picture($class['group_picture'], 'class');
            $class['group_cover'] = get_picture($class['group_cover'], 'class');
        }

        return $class;
    }

    /**
     * Lấy ra class_id của một trường khi biết tên của lớp đó.
     *
     * @param $class_name
     * @param $school_id
     * @return int
     * @throws Exception
     */
    public function getClassIdByName4Memcache($class_name) {
        global $db;

        $strSql = sprintf("SELECT C.group_id FROM groups C INNER JOIN ci_class_level CL ON C.class_level_id = CL.class_level_id
                    AND C.group_name = %s", secure($class_name));

        $get_class = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_class->num_rows > 0) {
            return $get_class->fetch_assoc()['group_id'];
        }
        return 0;
    }


    /**
     * Lấy ra thông tin lớp của trẻ.
     * Lấy thêm thông tin học phí trả trước của trẻ
     *
     * @param $child_id
     * @return array
     */
    public function getClassOfChild4Memcache($child_id) {
        global $db;

        $strSql = sprintf("SELECT G.*, SC.school_id FROM groups G INNER JOIN ci_school_child SC
                            ON G.group_id = SC.class_id AND SC.status = %s AND SC.child_id = %s", secure(STATUS_ACTIVE, 'int'), secure($child_id, 'int'));

        $result = array(
            'id' => null,
            'info' => null
        );
        $get_class = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_class->num_rows > 0) {
            $class = $get_class->fetch_assoc();
            $class['group_picture'] = get_picture($class['group_picture'], 'class');

            $result['id'] = $class['group_id'];
            $result['info'] = $class;
        }
        return $result;
    }


    /**
     * Lấy ra danh sách lớp của một khối
     *
     * @param $levelId
     * @return array
     * @throws Exception
     */
    public function getClassesOfLevel4Memcache($levelId) {
        global $db;

        $strSql = sprintf("SELECT C.group_id, C.group_name, C.group_title, C.group_members FROM groups C
                            WHERE C.class_level_id = %s AND C.status = 1 ORDER BY C.class_level_id, C.group_title ASC", secure($levelId, 'int'));

        $classes = array();
        $get_classes = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_classes->num_rows > 0) {
            while($class = $get_classes->fetch_assoc()) {
                $classes['ids'][] = $class['group_id'];
                $classes['lists'][$class['group_id']]= $class;
            }
        }
        return $classes;
    }

    /**
     * Lấy ra danh sách class được quản lý bởi một user
     *
     * @param $user_id
     * @return array
     * @throws Exception
     */
    public function getClasses4Memcache($user_id) {
        global $db;
        //UPDATE START MANHDD 04/06/2021
//        $strSql = sprintf("SELECT G.*, UM.role_id, CL.school_id, SC.school_status FROM groups G INNER JOIN ci_user_manage UM ON G.group_id = UM.object_id
//                    INNER JOIN ci_class_level CL ON CL.class_level_id = G.class_level_id
//                    INNER JOIN ci_school_configuration SC ON SC.school_id = CL.school_id
//                    WHERE UM.object_type = %s AND UM.user_id = %s AND UM.role_id = %s ORDER BY G.group_title ASC",
//            MANAGE_CLASS, secure($user_id, 'int'), PERMISSION_MANAGE);
        $strSql = sprintf("SELECT G.*, UM.role_id, CL.school_id, SC.school_status FROM groups G INNER JOIN ci_user_manage UM ON G.group_id = UM.object_id
                    INNER JOIN ci_class_level CL ON CL.class_level_id = G.class_level_id
                    INNER JOIN ci_school_configuration SC ON SC.school_id = CL.school_id
                    WHERE UM.object_type = %s AND UM.user_id = %s AND UM.role_id IN (%s,%s) ORDER BY G.group_title ASC",
            MANAGE_CLASS, secure($user_id, 'int'), PERMISSION_MANAGE,PERMISSION_ALL);
//UPDATE END MANHDD 04/06/2021
        $classes = array(
            'ids' => [],
            'lists' => []
        );
        $get_classes = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_classes->num_rows > 0) {
            while($class = $get_classes->fetch_assoc()) {
                $class['group_picture'] = get_picture($class['group_picture'], 'class');
                $class['group_cover'] = get_picture($class['group_cover'], 'class');
                $classes['ids'][] = $class['group_id'];
                $classes['lists'][$class['group_id']] = $class;
            }
        }
        return $classes;
    }

    /**
     * CHuyển trạng thái lớp thành đã tốt nghiệp
     *
     * @param $groupId
     * @throws Exception
     */
    public function updateGroupStatus($groupId) {
        global $db;

        $strSql = sprintf("UPDATE groups SET status = 0 WHERE group_id = %s", secure($groupId, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Thêm một bản ghi class vào ci_class_school_year
     *
     * @param $args
     * @throws Exception
     */
    public function createClassSchoolYear($args) {
        global $db;

        $strSql = sprintf("INSERT INTO ci_class_school_year (class_id, class_name, school_year, teacher_id) VALUES (%s, %s, %s, %s)", secure($args['class_id'], 'int'), secure($args['class_name']), secure($args['school_year']), secure($args['teacher_id'], 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }
}
?>