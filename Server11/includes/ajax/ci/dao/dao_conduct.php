<?php

/**
 * Class ConductDAO: Thao tác với bảng ci_conduct của hệ thống.
 * ADD NEW BY MANHDD 02/06/2021
 */
class ConductDAO
{
    /**
     * __construct
     *
     */
    public function __construct()
    {
    }


    /**
     * Kiểm tra đầu vào cho hàm insert và update
     *
     * @param array $args
     * @param bool|true $isCreate
     * @throws Exception
     */
    private function validateInput(array $args = array(), $isCreate = true)
    {
        /* validate conduct name */
        if (is_empty($args['conduct_name'])) {
            throw new Exception(__("You must choose conduct"));
        }
    }
    /**
     * Lấy ra thông tin Hạnh kiểm của học sinh
     *
     * @param $class_id
     * @param $child_id
     * @param $school_year
     * @return array|null
     * @throws Exception
     */
    public function getConductOfChildren($class_id,$child_id,$school_year)
    {
        global $db;

        $strSql = sprintf("SELECT *
                           FROM ci_conduct WHERE class_id = %s AND child_id = %s AND school_year = %s", secure($class_id, 'int'),secure($child_id),secure($school_year));
        $get_conduct = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $conduct = null;
        if ($get_conduct->num_rows > 0) {
            $conduct = $get_conduct->fetch_assoc();
        }
        return $conduct;
    }
    /**
     * Lấy ra thông tin Hạnh kiểm của học sinh theo id
     *
     * @param $conduct_id
     * @return array|null
     * @throws Exception
     */
    public function getConductById($conduct_id)
    {
        global $db;

        $strSql = sprintf("SELECT *
                           FROM ci_conduct WHERE conduct_id = %s", secure($conduct_id, 'int'));
        $get_conduct = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $conduct = null;
        if ($get_conduct->num_rows > 0) {
            $conduct = $get_conduct->fetch_assoc();
        }
        return $conduct;
    }

    /**
     * Cập nhật hạnh kiêm cho học sinh
     *
     * @param $args
     * @throws Exception
     */
    public function editConduct($args) {
        global $db;
            $strSql = sprintf("UPDATE ci_conduct SET hk1 = %s,hk2 = %s, ck = %s WHERE conduct_id = %s", secure($args['conduct_semester1']), secure($args['conduct_semester2']),secure($args['conduct_semester3']), secure($args['conduct_id'], 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }
    /**
     * Chuyển thông tin hạnh kiểm của trẻ từ lớp cũ sang lớp mới
     *
     * @param $childId
     * @param $oldClassId
     * @param $newClassId
     * @throws Exception
     */
    public function updateMoveClassId($childId, $oldClassId, $newClassId) {
        global $db;
        $strSql = sprintf("UPDATE ci_conduct SET class_id = %s WHERE child_id = %s AND class_id = %s", secure($newClassId,'int'), secure($childId,'int'), secure($oldClassId, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

}

?>