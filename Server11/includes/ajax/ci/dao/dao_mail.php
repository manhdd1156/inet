<?php
/**
 * DAO -> BackgroundMail
 * Thao tác với bảng ci_background_mail
 * 
 * @package ConIu
 * @author QuanND
 */

class MailDAO {
    /**
     * __construct
     *
     */
    public function __construct() {
    }

    /**
     * Lây ra danh sách email chưa gửi sắp xếp theo thứ tự tăng dần ID
     *
     * @return array
     */
    public function getUnsentEmails() {
        global $db;

        $unsentMails = array();
        $get_emails = $db->query("SELECT * FROM ci_background_email WHERE is_sent = 0 ORDER BY email_id ASC") or _error(SQL_ERROR_THROWEN);
        if($get_emails->num_rows > 0) {
            while($usentMail = $get_emails->fetch_assoc()) {
                $unsentMails[] = $usentMail;
            }
        }
        return $unsentMails;
    }

    /**
     * Lấy ra danh sách email đã gửi nhưng quá hạn lưu trữ.
     *
     * @param $interval
     * @return array
     * @throws Exception
     */
    public function getExpiredSentEmailIds($interval) {
        global $db, $date;

        $strSql = sprintf("SELECT email_id FROM ci_background_email WHERE is_sent = 1 AND
                        DATE_ADD(sent_at, INTERVAL %s DAY) < %s", secure($interval, 'int'), secure($date));

        $expiredMailIds = array();
        $get_emails = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_emails->num_rows > 0) {
            while($expiredMail = $get_emails->fetch_assoc()) {
                $expiredMailIds[] = $expiredMail['email_id'];
            }
        }
        return $expiredMailIds;
    }

    /**
     * Insert một email vào CSDL
     *
     * @param array $args
     * @throws Exception
     */
    public function insertEmail(array $args = array()) {
        global $db;
        $this->_validateInput($args);

        $strSql = sprintf("INSERT INTO ci_background_email (action, receivers, subject, content, file_attachment, file_attachment_name, delete_after_sending, school_id, user_id)
                            VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)",
                            secure($args['action']), secure($args['receivers']), secure($args['subject']), secure($args['content']), secure($args['file_attachment']), secure($args['file_attachment_name']),
                            secure($args['delete_after_sending'], 'int'), secure($args['school_id'], 'int'), secure($args['user_id'], 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Tăng số lần retry gửi mail lên 1.
     *
     * @param $emailId
     * @throws Exception
     */
    public function increaseRetryCountOneEmail($emailId) {
        global $db;

        $strSql = sprintf("UPDATE ci_background_email SET retry_count=retry_count+1 WHERE email_id = %s", secure($emailId, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Tăng số lần thử lại của những mail gửi bị lỗi
     *
     * @param $emailIds
     * @throws Exception
     */
    public function increaseRetryCountSomeEmail($emailIds) {
        global $db;

        if (count($emailIds) == 0) return;
        $strCon = implode(',', $emailIds);
        $strSql = sprintf("UPDATE ci_background_email SET retry_count=retry_count+1 WHERE email_id IN (%s)", $strCon);
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Cập nhật trạng thái của email đã được sent mà ko xóa
     *
     * @param $emailIds
     * @throws Exception
     */
    public function setSentStatus($emailIds) {
        global $db, $date;

        if (count($emailIds) == 0) return;
        $strCon = implode(',', $emailIds);
        $strSql = sprintf("UPDATE ci_background_email SET is_sent = 1, sent_at = %s WHERE email_id IN (%s)", secure($date), $strCon);
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Kiểm tra điều kiện đầu vào.
     *
     * @param array $args
     * @throws Exception
     */
    private function _validateInput(array $args = array()) {
        if($args['user_id'] < 0) {
            throw new Exception(__("Incorrect email information"));
        }

        if (is_empty($args['action'])) {
            throw new Exception(__("Action value is missing"));
        }

        if (is_empty($args['subject'])) {
            throw new Exception(__("Email subject is missing"));
        }

        if (is_empty($args['receivers'])) {
            throw new Exception(__("Receiver email is missing"));
        }

        if (is_empty($args['content'])) {
            throw new Exception(__("Email content is missing"));
        }
    }

    /**
     * Xóa 1 email
     *
     * @param $mailId
     * @throws Exception
     */
    public function deleteOneEmail($mailId) {
        global $db;
        $db->query(sprintf("DELETE FROM ci_background_email WHERE email_id = %s", secure($mailId, 'int'))) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Xóa nhiều email cùng một lúc
     *
     * @param $mailIds
     * @throws Exception
     */
    public function deleteSomeEmails($mailIds) {
        global $db;

        if (count($mailIds) == 0) return;

        $strCon = implode(',', $mailIds);
        $db->query(sprintf("DELETE FROM ci_background_email WHERE email_id IN (%s)", $strCon)) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Gửi email
     *
     * @param $email
     * @param $subject
     * @param $body
     * @param $isHtml
     * @return bool
     * @throws phpmailerException
     */
    public function sendEmail($email, $subject, $body, $is_html = false, $only_smtp = false, $file_attachment, $file_attachment_name) {
        global $system;
        /* set header */
        $header  = "MIME-Version: 1.0\r\n";
        $header .= "Mailer: ".$system['system_title']."\r\n";
        if($system['system_email']) {
            $header .= "From: ".$system['system_email']."\r\n";
            $header .= "Reply-To: ".$system['system_email']."\r\n";
            //$header .= "From: \"Coniu\" <noreply@coniu.vn>\r\n";
            //$header .= "Return-Path: noreply@coniu.vn\r\n";
        }

        if($is_html) {
            $header .= "Content-Type: text/html; charset=\"utf-8\"\r\n";
        } else {
            $header .= "Content-Type: text/plain; charset=\"utf-8\"\r\n";
        }

        /* send email */
        if($system['email_smtp_enabled']) {
            /* SMTP */
            require_once(ABSPATH.'includes/libs/PHPMailer/PHPMailer.php');
            require_once(ABSPATH.'includes/libs/PHPMailer/SMTP.php');
            require_once(ABSPATH.'includes/libs/PHPMailer/Exception.php');
            $mail = new PHPMailer\PHPMailer\PHPMailer;
            $mail->CharSet = "UTF-8";
            $mail->isSMTP();
            $mail->Host = $system['email_smtp_server'];
            $mail->SMTPAuth = ($system['email_smtp_authentication'])? true : false;
            $mail->Username = $system['email_smtp_username'];
            $mail->Password = $system['email_smtp_password'];
            $mail->SMTPSecure = ($system['email_smtp_ssl'])? 'ssl': 'tls';
            $mail->Port = $system['email_smtp_port'];
            $setfrom = (is_empty($system['email_smtp_setfrom']))? $system['email_smtp_username']: $system['email_smtp_setfrom'];
            $mail->setFrom($setfrom, $system['system_title']);

            /*$mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            );*/
            // $mail->addAddress($email, $system['system_title']);
            $list_mail = explode(",", $email);
            foreach ($list_mail as $value) {
                $mail->addAddress($value);
            }
            if (!is_empty($file_attachment) && !is_empty($file_attachment_name)) {
                $mail->addAttachment(ABSPATH.$file_attachment, $file_attachment_name);
            }

            $mail->Subject = $subject;
            if($is_html) {
                $mail->isHTML(true);
                $mail->AltBody = strip_tags($body);
            }
            $mail->Body = $body;
            if(!$mail->send()) {
                if($only_smtp) {
                    return false;
                }
                /* send using mail() */
                if(!mail($email, $subject, $body, $header)) {
                    return false;
                }
            }
        } else {
            if($only_smtp) {
                return false;
            }
            /* send using mail() */
            if(!mail($email, $subject, $body, $header)) {
                return false;
            }
        }
        return true;
    }
}
?>