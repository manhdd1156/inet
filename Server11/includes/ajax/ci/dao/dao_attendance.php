<?php

/**
 * Class AttendanceDAO: thao tác với bảng ci_attendance/ci_attendance_detail của hệ thống.
 */
class AttendanceDAO {
    /**
     * __construct
     *
     */
    public function __construct()
    {
    }

    /**
     * Kiểm tra đầu vào cho hàm insert và update
     *
     * @param array $args
     * @throws Exception
     */
    private function validateInput (array $args = array()) {
        global $date;
        if (!validateDate($args['attendance_date'])) {
            throw new Exception(__("You must enter attendance date"));
        }
        $attenDate = toDBDate($args['attendance_date']);
        $dbDate = toDBDate($date);
        if (strtotime($attenDate) < strtotime($dbDate)) {
            throw new Exception(__("The started date for asking leave is not the date in the past"));
        }
    }

    /**
     * Lấy ra danh sách trẻ của một lớp cùng với thông tin nghỉ/đi học
     *
     * @param $class_id
     * @param $attendanceDate
     * @return array
     * @throws Exception
     */
    function getAttendanceDetail($class_id, $attendanceDate) {
        global $db, $system;
        $attendanceDate = toDBDate($attendanceDate);

        //Lấy ra danh sách trẻ của lớp.
        $strSql = sprintf('SELECT CC.child_id , C.child_name, C.birthday, C.child_picture, C.child_nickname, CC.status AS child_status FROM ci_class_child CC
                            INNER JOIN ci_child C ON CC.child_id = C.child_id AND CC.class_id = %1$s AND (CC.end_at IS NULL OR CC.end_at > %2$s) 
                            /*AND CC.begin_at <= %2$s     Comment đoạn này để lấy ra những trẻ được chuyển lớp sang*/
                            INNER JOIN ci_school_child SC ON SC.child_id = C.child_id WHERE SC.begin_at <=%2$s /* Thêm đoạn này để không lấy những trẻ nhập học sau ngày điểm danh vào*/
                            ORDER BY C.name_for_sort ASC', secure($class_id, 'int'), secure($attendanceDate));
        $get_children = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $children = array();
        if ($get_children->num_rows > 0) {
            while ($child = $get_children->fetch_assoc()) {
                $child['child_picture'] = $system['system_uploads'] . '/' . $child['child_picture'];
                $children[] = $child;
            }
        }

        $strSql = sprintf("SELECT A.attendance_id, A.is_checked, A.attendance_date, A.recorded_at, A.absence_count, A.present_count, U.user_fullname FROM ci_attendance A
                            INNER JOIN users U ON A.checked_user_id = U.user_id AND A.class_id = %s AND A.attendance_date = %s",
                            secure($class_id, 'int'), secure($attendanceDate));
        $get_attendance = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        $results = array();
        //Trường hợp: Lớp chưa thực hiện điểm danh của ngày đó
        if ($get_attendance->num_rows == 0) {
            $results['isRollUp'] = 0; //Chưa có bản ghi trong bảng ci_attendance.
            $results['detail'] = $children;
            $results['attendance_date'] = toSysDate($attendanceDate);
        } else {
            //Trường hợp lớp đã bản ghi ở bảng ci_attendance
            $results = $get_attendance->fetch_assoc();
            $results['isRollUp'] = 1; //Đã có bản ghi trong bảng ci_attendance
            $results['recorded_at'] = toSysDatetime($results['recorded_at']);
            $results['attendance_date'] = toSysDate($results['attendance_date']);

            //Lấy ra danh sách trẻ đã được lưu thông tin điểm danh ngày đó.
            $strSql = sprintf("SELECT * FROM ci_attendance_detail WHERE attendance_id = %s", secure($results['attendance_id'], 'int'));
            $get_attDetail = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
            $attDetails = array();
            while ($attDetail = $get_attDetail->fetch_assoc()) {
                $attDetails[] = $attDetail;
            }

            $details = array();
            foreach ($children as $child) {
                $newChild = $child;
                foreach ($attDetails as $attDetail) {
                    if ($newChild['child_id'] == $attDetail['child_id']) {
                        $newChild['status'] = $attDetail['status'];
                        $newChild['attendance_detail_id'] = $attDetail['attendance_detail_id'];
                        $newChild['reason'] = $attDetail['reason'];
                        $newChild['feedback'] = $attDetail['feedback'];
                        $newChild['is_parent'] = $attDetail['is_parent'];
                        $newChild['resign_time'] = toSysDatetime($attDetail['resign_time']);
                        $newChild['recorded_user_id'] = $attDetail['recorded_user_id'];
                        $newChild['start_date'] = toSysDate($attDetail['start_date']);
                        $newChild['end_date'] = toSysDate($attDetail['end_date']);
                        $newChild['feedback'] = $attDetail['feedback'];

                        break;
                    }
                }
                $details[] = $newChild;
            }
            $results['detail'] = $details;
        }

        return $results;
    }

    public function getAttendanceCameBackdetail($classId, $attendanceDate) {
        global $db, $system;
        $attendanceDate = toDBDate($attendanceDate);

        //Lấy ra danh sách trẻ của lớp.
        $strSql = sprintf('SELECT CC.child_id , C.child_name, C.birthday, C.child_picture, C.child_nickname, CC.status AS child_status FROM ci_class_child CC
                            INNER JOIN ci_child C ON CC.child_id = C.child_id AND CC.class_id = %1$s AND (CC.end_at IS NULL OR CC.end_at > %2$s) 
                            /*AND CC.begin_at <= %2$s     Comment đoạn này để lấy ra những trẻ được chuyển lớp sang*/
                            INNER JOIN ci_school_child SC ON SC.child_id = C.child_id WHERE SC.begin_at <=%2$s /* Thêm đoạn này để không lấy những trẻ nhập học sau ngày điểm danh vào*/
                            ORDER BY C.name_for_sort ASC', secure($classId, 'int'), secure($attendanceDate));
        $get_children = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $children = array();
        if ($get_children->num_rows > 0) {
            while ($child = $get_children->fetch_assoc()) {
                $child['child_picture'] = $system['system_uploads'] . '/' . $child['child_picture'];
                $children[] = $child;
            }
        }

        $strSql = sprintf("SELECT A.attendance_back_id, A.attendance_back_date, A.recorded_at, A.came_back_count, A.not_came_back_count, U.user_fullname FROM ci_attendance_back A
                            INNER JOIN users U ON A.created_user_id = U.user_id AND A.class_id = %s AND A.attendance_back_date = %s",
            secure($classId, 'int'), secure($attendanceDate));
        $get_attendance = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        $results = array();
        //Trường hợp: Lớp chưa thực hiện điểm danh về của ngày đó
        if ($get_attendance->num_rows == 0) {
            $results['isRollUpBack'] = 0; //Chưa có bản ghi trong bảng ci_attendance_back.
            $results['attendance_back_date'] = toSysDate($attendanceDate);
            $results['detail'] = $children;
        } else {
            //Trường hợp lớp đã bản ghi ở bảng ci_attendance_back
            $results = $get_attendance->fetch_assoc();
            $results['isRollUpBack'] = 1; //Đã có bản ghi trong bảng ci_attendance_back
            $results['recorded_at'] = toSysDatetime($results['recorded_at']);
            $results['attendance_back_date'] = toSysDate($results['attendance_back_date']);

            //Lấy ra danh sách trẻ đã được lưu thông tin điểm danh về ngày đó.
            $strSql = sprintf("SELECT * FROM ci_attendance_back_detail WHERE attendance_back_id = %s", secure($results['attendance_back_id'], 'int'));
            $get_attDetail = $db->query($strSql) or _error('sql', $strSql);
            $attDetails = array();
            while ($attDetail = $get_attDetail->fetch_assoc()) {
                $attDetails[] = $attDetail;
            }

            $details = array();
            foreach ($children as $child) {
                $newChild = $child;
                foreach ($attDetails as $attDetail) {
                    if ($newChild['child_id'] == $attDetail['child_id']) {
                        $newChild['came_back_status'] = $attDetail['status'];
                        $newChild['attendance_back_detail_id'] = $attDetail['attendance_detail_id'];
                        $newChild['came_back_time'] = toSysTime($attDetail['came_back_time']);
                        $newChild['came_back_note'] = $attDetail['came_back_note'];
                        break;
                    }
                }
                $details[] = $newChild;
            }
            $results['detail'] = $details;
        }

        return $results;
    }

    /**
     * Lấy thông tin attendance_detail từ ID.
     *
     * @param $attendanceDetailId
     * @return array|null
     * @throws Exception
     */
    /*function getAttendanceDetailById($attendanceDetailId) {
        global $db;

        $strSql = sprintf("SELECT AD.*, A.class_id, A.attendance_date, C.child_name FROM ci_attendance_detail AD
                            INNER JOIN ci_attendance A ON A.attendance_id = AD.attendance_id AND AD.attendance_detail_id = %s
                            INNER JOIN ci_child C ON AD.child_id = C.child_id", secure($attendanceDetailId, 'int'));

        $get_attendance = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_attendance->num_rows > 0) {
            $ad = $get_attendance->fetch_assoc();
            $ad['attendance_date'] = toSysDate($ad['attendance_date']);

            return $ad;
        }

        return null;
    }*/

    /**
     * Lấy thông tin attendance_detail từ ID.
     *
     * @param $attendanceDetailId
     * @return array|null
     * @throws Exception
     */
    function getAttendanceDetailById($attendanceDetailId) {
        global $db;

        $strSql = sprintf("SELECT AD.*, A.class_id, A.attendance_date, C.child_name FROM ci_attendance_detail AD
                            INNER JOIN ci_attendance A ON A.attendance_id = AD.attendance_id AND AD.attendance_detail_id = %s
                            INNER JOIN ci_child C ON AD.child_id = C.child_id", secure($attendanceDetailId, 'int'));

        $get_attendance = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_attendance->num_rows > 0) {
            $ad = $get_attendance->fetch_assoc();
            $ad['attendance_date'] = toSysDate($ad['attendance_date']);
            if (!is_null($ad['start_date']) && $ad['start_date'] != $ad['attendance_date']
                && !is_null($ad['end_date']) && $ad['end_date'] != $ad['attendance_date']) {
                $ad['start_date'] = toSysDate($ad['start_date']);
                $ad['end_date'] = toSysDate($ad['end_date']);
            }

            return $ad;
        }

        return null;
    }

    /**
     * Lấy ra thông tin điểm danh của một trẻ từ ngày --> đến ngày
     *
     * @param $childId
     * @param $fromDate
     * @param $toDate
     * @return array
     * @throws Exception
     */
    function getAttendanceChild($childId, $fromDate, $toDate) {
        global $db;
        $fromDate = toDBDate($fromDate);
        $toDate = toDBDate($toDate);

        $results = array();
        $strSql = sprintf("SELECT AD.*, A.is_checked, A.attendance_date , U.user_fullname FROM ci_attendance A INNER JOIN ci_attendance_detail AD
                            ON A.attendance_id = AD.attendance_id AND AD.child_id = %s AND A.attendance_date >= %s AND A.attendance_date <= %s
                            INNER JOIN users U ON AD.recorded_user_id = U.user_id
                            ORDER BY A.attendance_date DESC", secure($childId, 'int'), secure($fromDate), secure($toDate));

        $get_attendance = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        $present_count = 0; //Đếm số ngày đi học
        $absence_count = 0; //Đếm số ngày nghỉ học
        $attendances = array();
        if ($get_attendance->num_rows > 0) {
            while ($attendance = $get_attendance->fetch_assoc()) {
                $attendance['attendance_date'] = toSysDate($attendance['attendance_date']);

                if ((($attendance['status'] == ATTENDANCE_ABSENCE)) || (($attendance['status'] == ATTENDANCE_ABSENCE_NO_REASON))) {
                    $absence_count++;
                } else {
                    $present_count++;
                }

                $attendances[] = $attendance;
            }
        }
        $results['attendance'] = $attendances;
        $results['present_count'] = $present_count;
        $results['absence_count'] = $absence_count;

        return $results;
    }

    /**
     * Lấy ra thông tin điểm danh của trẻ để thực hiện chuyển lớp.
     *
     * @param $classId
     * @param $childId
     * @param $fromDate
     * @param $toDate
     * @return array
     * @throws Exception
     */
    function getAttendanceChild4MoveClass($classId, $childId, $fromDate, $toDate) {
        global $db;
        $fromDate = toDBDate($fromDate);
        $toDate = toDBDate($toDate);

        $attendances = array();
        $strSql = sprintf("SELECT AD.*, A.attendance_date FROM ci_attendance_detail AD INNER JOIN ci_attendance A
                            ON AD.attendance_id = A.attendance_id AND AD.child_id = %s AND A.class_id = %s
                            AND A.attendance_date >= %s AND A.attendance_date <= %s
                            ORDER BY A.attendance_date ASC", secure($childId, 'int'), secure($classId, 'int'), secure($fromDate), secure($toDate));

        $get_attendance = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_attendance->num_rows > 0) {
            while ($attendance = $get_attendance->fetch_assoc()) {
                $attendances[] = $attendance;
            }
        }

        return $attendances;
    }

    /**
     * Lấy ra thông tin điểm danh của một lớp để phục vụ chức năng chuyển LỚP.
     *
     * @param $classId
     * @param $fromDate
     * @param $toDate
     * @return array
     * @throws Exception
     */
    function getAttendanceClass4MoveClass($classId, $fromDate, $toDate) {
        global $db;
        $fromDate = toDBDate($fromDate);
        $toDate = toDBDate($toDate);

        $attendances = array();
        $strSql = sprintf("SELECT attendance_id, attendance_date FROM ci_attendance
                            WHERE class_id = %s AND attendance_date >= %s AND attendance_date <= %s
                            ORDER BY attendance_date ASC", secure($classId, 'int'), secure($fromDate), secure($toDate));

        $get_attendance = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_attendance->num_rows > 0) {
            while ($attendance = $get_attendance->fetch_assoc()) {
                $attendances[] = $attendance;
            }
        }

        return $attendances;
    }

    /**
     * Cập nhật attendance_id của trẻ khi chuyển lớp.
     *
     * @param $attendanceDetailId
     * @param $newAttendanceId
     * @throws Exception
     */
    public function updateAttendanceDetail4MoveClass($attendanceDetailId, $newAttendanceId) {
        global $db;
        $strSql = sprintf("UPDATE ci_attendance_detail SET attendance_id = %s WHERE attendance_detail_id = %s",
            secure($newAttendanceId, 'int'), secure($attendanceDetailId, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Cập nhật số lượng đi/nghỉ của lớp CŨ khi chuyển lớp.
     *
     * @param $attendanceId
     * @param $status
     * @throws Exception
     */
    public function updatePresentAbsence4OldClass($attendanceId, $status) {
        global $db;

        $strSql = null;
        if (($status == ATTENDANCE_PRESENT) || ($status == ATTENDANCE_COME_LATE) || ($status == ATTENDANCE_EARLY_LEAVE)){
            $strSql = sprintf("UPDATE ci_attendance SET present_count = IF(present_count <= 1, 0, present_count - 1) WHERE attendance_id = %s",
                secure($attendanceId, 'int'));
        } else {
            $strSql = sprintf("UPDATE ci_attendance SET absence_count = IF(absence_count <= 1, 0, absence_count - 1) WHERE attendance_id = %s",
                secure($attendanceId, 'int'));
        }
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Xóa điểm danh chi tiết ở bảng điểm lớp CŨ, khi có trẻ chuyển lớp.
     *
     * @param $attendanceDetail
     * @throws Exception
     */
    public function deleteAttendanceDetail4OldClass($attendanceDetail) {
        global $db;

        $strSql = sprintf("DELETE FROM ci_attendance_detail WHERE attendance_detail_id = %s", secure($attendanceDetail['attendance_detail_id'], 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        if (($attendanceDetail['status'] == ATTENDANCE_PRESENT) || ($attendanceDetail['status'] == ATTENDANCE_COME_LATE)
                || ($attendanceDetail['status'] == ATTENDANCE_EARLY_LEAVE)){
            $strSql = sprintf("UPDATE ci_attendance SET present_count = IF(present_count <= 1, 0, present_count - 1) WHERE attendance_id = %s",
                secure($attendanceDetail['attendance_id'], 'int'));
        } else {
            $strSql = sprintf("UPDATE ci_attendance SET absence_count = IF(absence_count <= 1, 0, absence_count - 1) WHERE attendance_id = %s",
                secure($attendanceDetail['attendance_id'], 'int'));
        }
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Cập nhật số lượng đi/nghỉ của lớp MỚI khi chuyển lớp.
     *
     * @param $attendanceId
     * @param $status
     * @throws Exception
     */
    public function updatePresentAbsence4NewClass($attendanceId, $status) {
        global $db;

        $strSql = null;
        if (($status == ATTENDANCE_PRESENT) || ($status == ATTENDANCE_COME_LATE) || ($status == ATTENDANCE_EARLY_LEAVE)){
            $strSql = sprintf("UPDATE ci_attendance SET present_count = present_count + 1 WHERE attendance_id = %s", secure($attendanceId, 'int'));
        } else {
            $strSql = sprintf("UPDATE ci_attendance SET absence_count = absence_count + 1 WHERE attendance_id = %s", secure($attendanceId, 'int'));
        }
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Xóa một điểm danh
     *
     * @param $attendanceId
     * @throws Exception
     */
    public function deleteAttendance($attendanceId) {
        global $db;

        $strSql = sprintf("DELETE FROM ci_attendance_detail WHERE attendance_id = %s", secure($attendanceId, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        $strSql = sprintf("DELETE FROM ci_attendance WHERE attendance_id = %s", secure($attendanceId, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Lấy ra thông tin của lần điểm danh ở màn hình quản lý trường
     *
     * @param $attendance_id
     * @param $school_id
     * @param $class_id
     * @return array
     * @throws Exception
     */
    function getAttendance($attendance_id, $school_id = 0, $class_id = 0) {
        global $db;

        $results = array();
        if ($class_id != 0) {
            $strSql = sprintf("SELECT A.attendance_id, A.is_checked, A.class_id, A.attendance_date, A.recorded_at, A.absence_count, A.present_count, G.group_title, U.user_fullname FROM ci_attendance A
                            INNER JOIN groups G ON A.class_id = G.group_id AND A.class_id = %s                            
                            INNER JOIN users U ON A.checked_user_id = U.user_id AND A.attendance_id = %s", secure($class_id, 'int'), secure($attendance_id, 'int'));
        } else {
            $strSql = sprintf("SELECT A.attendance_id, A.is_checked, A.class_id, A.attendance_date, A.recorded_at, A.absence_count, A.present_count, G.group_title, U.user_fullname FROM ci_attendance A
                            INNER JOIN groups G ON A.class_id = G.group_id                            
                            INNER JOIN ci_class_level CL ON G.class_level_id = CL.class_level_id AND CL.school_id = %s                          
                            INNER JOIN users U ON A.checked_user_id = U.user_id AND A.attendance_id = %s", secure($school_id, 'int'), secure($attendance_id, 'int'));
        }

        $get_attendance = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        //Trường hợp: Lớp chưa thực hiện điểm danh của ngày đó
        if ($get_attendance->num_rows == 0) {
            throw new Exception(__("Class have not been attendance"));
        } else {
            //Trường hợp lớp đã điểm danh
            $results = $get_attendance->fetch_assoc();
            $results['recorded_at'] = toSysDatetime($results['recorded_at']);
            $results['attendance_date'] = toSysDate($results['attendance_date']);
            $results['isRollUp'] = 1;

            $strSql = sprintf("SELECT AD.* , C.child_name, C.birthday, CC.status AS child_status FROM ci_attendance_detail AD
                            INNER JOIN ci_child C ON AD.child_id = C.child_id AND AD.attendance_id = %s
                            INNER JOIN ci_class_child CC ON CC.child_id = C.child_id
                            ORDER BY C.name_for_sort ASC", secure($attendance_id, 'int'));
            $get_detail = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

            $details = array();
            $childIds = array();
            $childIds[] = 0; //Cái này cho vào để câu SQL dưới không xảy ra lỗi trong trường hợp count($childIds) == 0.
            if ($get_detail->num_rows > 0) {
                while ($detail = $get_detail->fetch_assoc()) {
                    $detail['resign_time'] = toSysDatetime($detail['resign_time']);
                    $childIds[] = $detail['child_id'];
                    $details[] = $detail;
                }
            }
            //Trường hợp đã có bản ghi nhưng chưa điểm danh, thì lấy những trẻ không xin nghỉ
            if ($results['is_checked'] == 0) {
                $strCon = implode(',', $childIds);
                $strSql = sprintf("SELECT CC.child_id, CC.status AS child_status, C.child_name, C.birthday FROM ci_class_child CC
                            INNER JOIN ci_child C ON CC.child_id = C.child_id AND CC.class_id = %s AND CC.begin_at <= %s AND (CC.end_at IS NULL OR CC.end_at >= %s) AND CC.child_id NOT IN (%s)
                            ORDER BY C.name_for_sort ASC", secure($results['class_id'], 'int'), secure(toDBDate($results['attendance_date'])), secure(toDBDate($results['attendance_date'])), $strCon);

                $get_detail = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
                while ($detail = $get_detail->fetch_assoc()) {
                    $details[] = $detail;
                }
            }
            $results['detail'] = $details;
        }

        return $results;
    }

    /**
     * Lấy danh sách trẻ điểm danh về
     *
     * @param $attendance_id
     * @param int $school_id
     * @param int $class_id
     * @return array|null
     * @throws Exception
     */
    function getAttendanceBack($attendance_id, $school_id = 0, $class_id = 0) {
        global $db;

        $results = array();
        if ($class_id != 0) {
            die;
            $strSql = sprintf("SELECT A.attendance_id, A.is_checked, A.class_id, A.attendance_date, A.recorded_at, A.absence_count, A.present_count, G.group_title, U.user_fullname FROM ci_attendance A
                            INNER JOIN groups G ON A.class_id = G.group_id AND A.class_id = %s                            
                            INNER JOIN users U ON A.checked_user_id = U.user_id AND A.attendance_id = %s", secure($class_id, 'int'), secure($attendance_id, 'int'));
        } else {
            $strSql = sprintf("SELECT A.*, G.group_title, U.user_fullname FROM ci_attendance_back A
                            INNER JOIN groups G ON A.class_id = G.group_id                            
                            INNER JOIN ci_class_level CL ON G.class_level_id = CL.class_level_id AND CL.school_id = %s                          
                            INNER JOIN users U ON A.created_user_id = U.user_id AND A.attendance_back_id = %s", secure($school_id, 'int'), secure($attendance_id, 'int'));
        }

        $get_attendance = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        //Trường hợp: Lớp chưa thực hiện điểm danh về của ngày đó
        if ($get_attendance->num_rows == 0) {
            throw new Exception(__("Class have not been attendance"));
        } else {
            //Trường hợp lớp đã điểm danh về
            $results = $get_attendance->fetch_assoc();
            $results['recorded_at'] = toSysDatetime($results['recorded_at']);
            $results['attendance_back_date'] = toSysDate($results['attendance_back_date']);
            $results['isRollUpBack'] = 1;

            $strSql = sprintf("SELECT AD.* , C.child_name, C.birthday, CC.status AS child_status FROM ci_attendance_back_detail AD
                            INNER JOIN ci_child C ON AD.child_id = C.child_id AND AD.attendance_back_id = %s
                            INNER JOIN ci_class_child CC ON CC.child_id = C.child_id
                            ORDER BY C.name_for_sort ASC", secure($attendance_id, 'int'));
            $get_detail = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

            $details = array();
            $childIds = array();
            $childIds[] = 0; //Cái này cho vào để câu SQL dưới không xảy ra lỗi trong trường hợp count($childIds) == 0.
            if ($get_detail->num_rows > 0) {
                while ($detail = $get_detail->fetch_assoc()) {
                    $childIds[] = $detail['child_id'];
                    $details[] = $detail;
                }
            }

            $results['detail'] = $details;
        }

        return $results;
    }

    function getAttendanceOfClass($class_id, $fromDate, $toDate) {
        global $db;
        $fromDate = toDBDate($fromDate);
        $toDate = toDBDate($toDate);

        $results = array();
        $strSql = sprintf("SELECT A.* , U.user_fullname FROM ci_attendance A INNER JOIN users U
                            ON A.class_id = %s AND A.checked_user_id = U.user_id AND A.attendance_date >= %s AND A.attendance_date <= %s AND A.is_checked = %s
                            ORDER BY A.attendance_date DESC", secure($class_id, 'int'), secure($fromDate), secure($toDate), 1);

        $get_attendance = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_attendance->num_rows > 0) {
            while ($attendance = $get_attendance->fetch_assoc()) {
                $attendance['attendance_date'] = toSysDate($attendance['attendance_date']);
                $attendance['recorded_at'] = toSysDatetime($attendance['recorded_at']);
                $results[] = $attendance;
            }
        }

        return $results;
    }

    /**
     * Lấy ra số ngày điểm danh của một lớp trong một tháng.
     *
     * @param $classId
     * @param $month
     * @return mixed
     * @throws Exception
     */
    function getAttendanceCountInMonthOfClass($classId, $month) {
        global $db;
        $firstDateOfMonth = toDBDate("01/" . $month);
        $firstDateOfNextMonth = addMonthToDBDate($firstDateOfMonth);

        $strSql = sprintf("SELECT count(attendance_id) AS cnt FROM ci_attendance WHERE class_id = %s AND attendance_date >= %s AND attendance_date < %s AND is_checked=1",
                                secure($classId, 'int'), secure($firstDateOfMonth), secure($firstDateOfNextMonth));

        $get_attendance = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        return $get_attendance->fetch_assoc()['cnt'];
    }

    /**
     * Lấy ra danh sách điểm danh theo trường.
     *
     * @param $school_id
     * @param $fromDate
     * @param $toDate
     * @return array
     * @throws Exception
     */
    function getAttendanceOfSchool($school_id, $fromDate, $toDate) {
        global $db;
        $fromDate = toDBDate($fromDate);
        $toDate = toDBDate($toDate);

        $results = array();
        $strSql = sprintf("SELECT A.* , G.group_title FROM ci_attendance A
                            INNER JOIN groups G ON A.class_id = G.group_id AND A.attendance_date >= %s AND A.attendance_date <= %s AND A.is_checked = %s
                            INNER JOIN ci_class_level CL ON G.class_level_id = CL.class_level_id AND CL.school_id = %s
                            ORDER BY G.group_title ASC, A.attendance_date DESC", secure($fromDate), secure($toDate), 1, secure($school_id, 'int'));

        $get_attendance = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_attendance->num_rows > 0) {
            while ($attendance = $get_attendance->fetch_assoc()) {
                $attendance['attendance_date'] = toSysDate($attendance['attendance_date']);
                $attendance['recorded_at'] = toSysDatetime($attendance['recorded_at']);
                $results[] = $attendance;
            }
        }

        return $results;
    }

    /**
     * Lấy ra danh sách điểm danh ĐẦY ĐỦ của một lớp từ ngày A đến ngày B. Bao gồm điểm danh đầy đủ của từng trẻ.
     *
     * LƯU Ý: KHI CHƯA HIỂU TƯỜNG TẬN HÀM NÀY ĐỪNG CHÚ NÀO SỬA CỦA ANH.
     *
     * @param $class_id
     * @param $fromDate
     * @param $toDate
     * @return array
     * @throws Exception
     */
    function getFullAttendanceOfClass($class_id, $fromDate, $toDate) {
        global $db;
        $fromDate = toDBDate($fromDate);
        $toDate = toDBDate($toDate);

        $results = array();
//        $strSql = sprintf("SELECT AD.child_id, AD.status, AD.reason, A.attendance_id, A.is_checked, A.attendance_date, A.present_count, A.absence_count, C.child_name, CC.status AS child_status FROM ci_attendance_detail AD
//                            INNER JOIN ci_attendance A ON AD.attendance_id = A.attendance_id AND A.attendance_date >= %s AND A.attendance_date <= %s AND A.class_id = %s
//                            INNER JOIN ci_child C ON AD.child_id = C.child_id
//                            INNER JOIN ci_class_child CC ON AD.child_id = CC.child_id AND CC.class_id = %s
//                            INNER JOIN ci_school_child SC ON SC.child_id = AD.child_id AND SC.begin_at <= %s AND (SC.end_at >= %s OR SC.end_at IS NULL)
//                            ORDER BY C.name_for_sort ASC, C.child_id ASC, A.attendance_date ASC", secure($fromDate), secure($toDate), secure($class_id, 'int'), secure($class_id, 'int'), secure($toDate), secure($fromDate));

        $strSql = sprintf("SELECT AD.child_id, AD.status, AD.reason, A.attendance_id, A.is_checked, A.attendance_date, A.present_count, A.absence_count, C.child_name, CC.status AS child_status FROM ci_attendance_detail AD
                            INNER JOIN ci_attendance A ON AD.attendance_id = A.attendance_id AND A.attendance_date >= %s AND A.attendance_date <= %s AND A.class_id = %s
                            INNER JOIN ci_child C ON AD.child_id = C.child_id
                            INNER JOIN ci_class_child CC ON AD.child_id = CC.child_id
                            INNER JOIN ci_school_child SC ON SC.child_id = AD.child_id AND SC.begin_at <= %s AND (SC.end_at >= %s OR SC.end_at IS NULL)
                            ORDER BY C.name_for_sort ASC, C.child_id ASC, A.attendance_date ASC", secure($fromDate), secure($toDate), secure($class_id, 'int'), secure($toDate), secure($fromDate));

        $get_attendance = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_attendance->num_rows > 0) {
            while ($attendance = $get_attendance->fetch_assoc()) {
                //$attendance['day'] = explode("-",$attendance['attendance_date'])[2];
                //$attendance['attendance_date'] = toSysDate($attendance['attendance_date']);

                $results[] = $attendance;
            }
        }
        return $results;

    }

    /**
     * Lấy ra danh sách Child_ID của những trẻ nghỉ học trong một này.
     *
     * @param $schoolId
     * @param $attendanceDate
     * @param int $classId
     * @return array
     * @throws Exception
     */
    function getAbsentChildIds($schoolId, $attendanceDate, $classId = 0) {
        global $db;
        $attendanceDate = toDBDate($attendanceDate);

        $absentStatus = implode(",", [ATTENDANCE_ABSENCE, ATTENDANCE_ABSENCE_NO_REASON]);
        $results = array();
        if ($classId > 0) {
            $strSql = sprintf("SELECT AD.child_id FROM ci_attendance_detail AD INNER JOIN ci_attendance A ON AD.attendance_id = A.attendance_id AND A.attendance_date = %s
                                AND A.class_id = %s AND AD.status IN (%s)", secure($attendanceDate), secure($classId, 'int'), $absentStatus);
        } else {
            $strSql = sprintf("SELECT AD.child_id FROM ci_attendance_detail AD INNER JOIN ci_attendance A ON AD.attendance_id = A.attendance_id AND A.attendance_date = %s AND AD.status IN (%s)
                                INNER JOIN ci_school_child SC ON AD.child_id = SC.child_id AND SC.school_id = %s",
                                secure($attendanceDate), $absentStatus, secure($schoolId, 'int'));
        }

        $get_attendance = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_attendance->num_rows > 0) {
            while ($attendance = $get_attendance->fetch_assoc()) {
                $results[] = $attendance['child_id'];
            }
        }

        return $results;
    }

    /**
     * Lấy danh sách child_id của những trẻ đi học trong 1 ngày
     *
     * @param $schoolId
     * @param $attendanceDate
     * @param int $classId
     * @return array
     * @throws Exception
     */
    function getPresentChildIds($schoolId, $attendanceDate, $classId = 0) {
        global $db;
        $attendanceDate = toDBDate($attendanceDate);

        $presentStatus = implode(",", [ATTENDANCE_PRESENT, ATTENDANCE_COME_LATE, ATTENDANCE_EARLY_LEAVE]);
        $results = array();
        if ($classId > 0) {
            $strSql = sprintf("SELECT AD.child_id FROM ci_attendance_detail AD INNER JOIN ci_attendance A ON AD.attendance_id = A.attendance_id AND A.attendance_date = %s
                                AND A.class_id = %s AND AD.status IN (%s)", secure($attendanceDate), secure($classId, 'int'), $presentStatus);
        } else {
            $strSql = sprintf("SELECT AD.child_id FROM ci_attendance_detail AD INNER JOIN ci_attendance A ON AD.attendance_id = A.attendance_id AND A.attendance_date = %s AND AD.status IN (%s)
                                INNER JOIN ci_school_child SC ON AD.child_id = SC.child_id AND SC.school_id = %s",
                secure($attendanceDate), $presentStatus, secure($schoolId, 'int'));
        }

        $get_attendance = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_attendance->num_rows > 0) {
            while ($attendance = $get_attendance->fetch_assoc()) {
                $results[] = $attendance['child_id'];
            }
        }

        return $results;
    }

    /**
     * Lấy ra điểm danh của một lớp để tổng hợp trong màn hình điểm danh lớp.
     * KHÔNG SỬA.
     *
     * @param $classId
     * @param $fromDate
     * @param $toDate
     * @return array
     * @throws Exception
     */
    function getAttendanceOfClass4Summary($classId, $fromDate, $toDate) {
        global $db;
        $fromDate = toDBDate($fromDate);
        $toDate = toDBDate($toDate);

        $results = array();
        $strSql = sprintf("SELECT * FROM ci_attendance WHERE attendance_date >= %s AND attendance_date <= %s AND class_id = %s
                            ORDER BY attendance_date ASC", secure($fromDate), secure($toDate), secure($classId, 'int'));

        $get_attendance = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_attendance->num_rows > 0) {
            while ($attendance = $get_attendance->fetch_assoc()) {
                $results[] = $attendance;
            }
        }

        return $results;
    }

    /**
     * Lấy ra danh sách những ngày điểm danh mới nhất của một trường
     *
     * @param $school_id
     * @param $top
     * @return array
     * @throws Exception
     */
    function getLastAttendanceDates($school_id, $top) {
        global $db;

        $results = array();
        $strSql = sprintf("SELECT DISTINCT A.attendance_date FROM ci_attendance A
                    INNER JOIN ci_school_child SC ON A.class_id = SC.class_id AND SC.school_id = %s AND A.is_checked = 1
                    ORDER BY A.attendance_date DESC LIMIT %s", secure($school_id, 'int'), $top);

        $get_dates = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_dates->num_rows > 0) {
            while ($date = $get_dates->fetch_assoc()) {
                $results[] = $date['attendance_date'];
            }
        }

        return $results;
    }

    /**
     * Lấy ra danh sách trẻ nghỉ học liên tục trong những ngày điểm danh của một trường.
     *
     * @param $school_id
     * @param $top
     * @return array
     * @throws Exception
     */
    function getConsecutiveAbsentChild($school_id, $top) {
        global $db;

        $results = array();
        $dates = $this->getLastAttendanceDates($school_id, $top);
        $strDates = "'".implode("','", $dates)."'";
        $strSql = sprintf("SELECT AD.child_id, count(AD.child_id) AS cnt, C.child_name, A.class_id, G.group_title
                            FROM (ci_attendance_detail AD, ci_attendance A)
                            INNER JOIN ci_child C ON AD.child_id = C.child_id AND (AD.status = %s OR AD.status = %s)
                            INNER JOIN groups G ON A.class_id = G.group_id
                            INNER JOIN ci_school_child SC ON C.child_id = SC.child_id AND SC.school_id = %s AND SC.status = %s
                            WHERE AD.attendance_id = A.attendance_id AND A.is_checked = 1 AND A.attendance_date IN (%s)
                            GROUP BY AD.child_id
                            ORDER BY A.class_id ASC, C.name_for_sort ASC",
            secure(ATTENDANCE_ABSENCE, 'int'), secure(ATTENDANCE_ABSENCE_NO_REASON, 'int'), secure($school_id, 'int'), secure(STATUS_ACTIVE, 'int'), $strDates);

        $get_childs = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_childs->num_rows > 0) {
            while ($child = $get_childs->fetch_assoc()) {
                if ($child['cnt'] == $top) {
                    $results[] = $child;
                }
            }
        }

        return $results;
    }

    /**
     * Lấy ra danh sách điểm danh của trường trong một ngày
     *
     * @param $school_id
     * @param $date
     * @return array
     * @throws Exception
     */
    function getAttendanceOfSchoolADay($school_id, $date) {
        global $db;
        $date = toDBDate($date);

        $results = array();
        $strSql = sprintf('SELECT A.*, CL.class_level_id, CL.class_level_name, G.group_id, G.group_title, COUNT(CC.child_id) AS total FROM groups G
                            INNER JOIN ci_class_level CL ON G.class_level_id = CL.class_level_id AND CL.school_id = %1$s
                            LEFT JOIN ci_attendance A ON A.class_id = G.group_id AND A.attendance_date = %2$s
                            LEFT JOIN ci_class_child CC ON G.group_id = CC.class_id AND CC.begin_at <= %2$s AND (CC.end_at IS NULL OR CC.end_at >= %2$s) 
                            INNER JOIN ci_school_child SC ON CC.child_id = SC.child_id
                            WHERE G.status = 1 AND SC.status = 1
                            GROUP BY G.group_id
                            ORDER BY CL.class_level_id ASC, G.group_title ASC', secure($school_id, 'int'), secure($date));

        $get_attendance = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_attendance->num_rows > 0) {
            while ($attendance = $get_attendance->fetch_assoc()) {
                $attendance['attendance_date'] = toSysDate($attendance['attendance_date']);
                $attendance['recorded_at'] = toSysDatetime($attendance['recorded_at']);
                if($attendance['is_checked'] == null) {
                    $attendance['is_checked'] = "0";
                    $attendance['absence_count'] = null;
                    $attendance['present_count'] = null;
                }
//                if (($attendance['attendance_id'] <= 0) || ($attendance['is_checked'] == 0)) {
//                    //Lấy ra sĩ số của lớp chưa điểm danh
//                    $attendance['total'] = $this->getTotalCountOfClass($attendance['group_id']);
//                } else {
//                    $attendance['total'] = $attendance['present_count'] + $attendance['absence_count'];
//                }

                $results[] = $attendance;
            }
        }

        return $results;
    }

    /**
     * Lấy ra danh sách trẻ đi học của cả trường trong một ngày.
     * Danh sách được sắp xếp theo KHỐI, LỚP, TÊN trẻ.
     *
     * Hàm này phục vụ việc tổng hợp các dịch vụ ăn uống của trường trong một ngày.
     *
     * @param $school_id
     * @param $date
     * @return array
     * @throws Exception
     */
//    function getPresentChildrenOfSchoolADay($school_id, $date) {
//        global $db;
//        $date = toDBDate($date);
//
//        //Kết quả trả về chứa 2 thành phần:
//        // 1. Danh sách đầy đủ thông tin, bao gồm cả TÊN LỚP
//        // 2. Danh sách chỉ chứa lớp và sĩ số lớp đi học.
//        $results = array();
//        $childIds = array();
//        $classes = array();
//        /* Trước khi sửa, lấy theo SC.status -> sai, lấy theo ngày bắt đầu và ngày kết thúc */
//        // $strSql = sprintf("SELECT AD.child_id, A.class_id, G.group_title FROM ci_attendance_detail AD
//        //                     INNER JOIN ci_school_child SC ON AD.child_id = SC.child_id AND SC.school_id = %s AND SC.status != %s
//        //                     INNER JOIN ci_attendance A ON AD.attendance_id = A.attendance_id AND AD.status > 0 AND A.is_checked = 1 AND A.attendance_date = %s
//        //                     INNER JOIN groups G ON A.class_id = G.group_id
//        //                     ORDER BY G.class_level_id ASC, A.class_id ASC", secure($school_id, 'int'), secure(STATUS_INACTIVE, 'int'), secure($date));
//
//        $strSql = sprintf("SELECT AD.child_id, A.class_id, G.group_title FROM ci_attendance_detail AD
//                            INNER JOIN ci_school_child SC ON AD.child_id = SC.child_id AND SC.school_id = %s AND SC.begin_at <= %s AND (SC.end_at >= %s || SC.end_at IS NULL || SC.end_at = '0000-00-00 00:00:00' )
//                            INNER JOIN ci_attendance A ON AD.attendance_id = A.attendance_id AND AD.status > 0 AND A.is_checked = 1 AND A.attendance_date = %s
//                            INNER JOIN groups G ON A.class_id = G.group_id
//                            ORDER BY G.class_level_id ASC, A.class_id ASC", secure($school_id, 'int'), secure($date), secure($date), secure($date));
//
//        $get_attendance = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
//        if ($get_attendance->num_rows > 0) {
//            $classId = -1;
//            $totalClass = 0;
//            $class = null;
//
//            $attendance = $get_attendance->fetch_assoc();
//            do {
//                $childIds[] = $attendance['child_id'];
//
//                if (($classId > 0) && ($classId != $attendance['class_id'])) {
//                    $class['present_total'] = $totalClass;
//                    $classes[] = $class;
//                    $totalClass = 0;
//                }
//
//                $class = $attendance;
//                $totalClass++;
//                $classId = $attendance['class_id'];
//            } while ($attendance = $get_attendance->fetch_assoc());
//
//            //Xử lý cho lớp cuối cùng
//            $class['present_total'] = $totalClass;
//            $classes[] = $class;
//        }
//
//        $results['classes'] = $classes;
//        $results['childIds'] = $childIds;
//
//        return $results;
//    }

    /**
     * THAY THẾ HÀM BỊ COMMENT PHÍA TRÊN VÌ PERFORMANCE. CHỈ TÁCH CÂU SQL THÀNH 2 LẦN QUERY
     *
     * Lấy ra danh sách trẻ đi học của cả trường trong một ngày.
     * Danh sách được sắp xếp theo KHỐI, LỚP, TÊN trẻ.
     *
     * Hàm này phục vụ việc tổng hợp các dịch vụ ăn uống của trường trong một ngày.
     *
     * @param $school_id
     * @param $date
     * @return array
     * @throws Exception
     */
    function getPresentChildrenOfSchoolADay($school_id, $date) {
        global $db;
        $date = toDBDate($date);

        //Kết quả trả về chứa 2 thành phần:
        // 1. Danh sách đầy đủ thông tin, bao gồm cả TÊN LỚP
        // 2. Danh sách chỉ chứa lớp và sĩ số lớp đi học.
        $results = array();
        $childIds = array();
        $classes = array();
        $tmpClasses = array();
        $attendanceIds = array();
        // lấy ra danh sách các lớp đã điểm danh
        $strSql = sprintf("SELECT A.attendance_id, A.class_id, G.group_title FROM ci_attendance A 
                    INNER JOIN groups G ON A.class_id = G.group_id AND A.is_checked = 1 AND A.attendance_date = %s 
                    WHERE G.class_level_id IN (SELECT class_level_id FROM ci_class_level WHERE school_id = %s)
                    ORDER BY G.class_level_id ASC, A.class_id ASC", secure($date), secure($school_id, 'int'));
        $get_attendance = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_attendance->num_rows > 0) {
            while ($class = $get_attendance->fetch_assoc()) {
                $tmpClasses[] = $class;
                $attendanceIds[] = $class['attendance_id'];
            }
        }

        //Lấy ra danh sách trẻ đã điểm danh của những lớp đó
        if (count($attendanceIds) > 0) {
            $strAttendanceIds = implode(",", $attendanceIds);
            $strSql = sprintf("SELECT AD.child_id, AD.attendance_id FROM ci_attendance_detail AD
                            INNER JOIN ci_school_child SC ON AD.attendance_id IN (%s)
                            AND AD.status > 0 AND AD.child_id = SC.child_id AND SC.begin_at <= %s AND (SC.end_at >= %s || SC.end_at IS NULL || SC.end_at = '0000-00-00 00:00:00' )
                            ORDER BY AD.attendance_id ASC", $strAttendanceIds, secure($date), secure($date));

            $get_children = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

            if ($get_children->num_rows > 0) {
                $attendanceId = -1;
                $totalClass = 0;

                $child = $get_children->fetch_assoc();
                do {
                    $childIds[] = $child['child_id'];
                    if (($attendanceId > 0) && ($attendanceId != $child['attendance_id'])) {
                        //Gắn số lượng trẻ đi học vào các lớp
                        foreach ($tmpClasses as $tmpClass) {
                            if ($tmpClass['attendance_id'] == $attendanceId) {
                                $class = $tmpClass;
                                $class['present_total'] = $totalClass;
                                $classes[] = $class;

                                break;
                            }
                        }
                        $totalClass = 0;
                    }
                    $totalClass++;
                    $attendanceId = $child['attendance_id'];
                } while ($child = $get_children->fetch_assoc());

                //Xử lý cho lớp cuối cùng
                if (($attendanceId > 0) && ($attendanceId != $child['attendance_id'])) {
                    foreach ($tmpClasses as $tmpClass) {
                        if ($tmpClass['attendance_id'] == $attendanceId) {
                            $class = $tmpClass;
                            $class['present_total'] = $totalClass;
                            $classes[] = $class;

                            break;
                        }
                    }
                }
            }
        }
        $results['classes'] = $classes;
        $results['childIds'] = $childIds;

        return $results;
    }

    /**
     * Lấy ra sĩ số một lớp
     *
     * @param $class_id
     * @return mixed
     * @throws Exception
     */
//    function getTotalCountOfClass($class_id) {
//        global $db;
//        $strSql = sprintf("SELECT count(child_id) AS cnt FROM ci_class_child WHERE class_id = %s AND status = %s", secure($class_id, 'int'), STATUS_ACTIVE);
//
//        $get_child_count = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
//
//        return $get_child_count->fetch_assoc()['cnt'];
//    }


    /**
     * Insert bản ghi điểm danh vào DB
     *
     * @param array $args
     * @return mixed
     * @throws Exception
     */
    public function insertAttendance(array $args = array()) {
        global $db, $date, $user;
        $this->validateInput($args);
        $args['attendance_date'] = toDBDate($args['attendance_date']);


        $strSql = sprintf("INSERT INTO ci_attendance (class_id, attendance_date, checked_user_id, recorded_at, absence_count, present_count, is_checked) VALUES (%s, %s, %s, %s, %s, %s, %s)",
                        secure($args['class_id'], 'int'), secure($args['attendance_date']), secure($user->_data['user_id'], 'int'),
                        secure($date), secure($args['absence_count'], 'int'), secure($args['present_count'], 'int'), secure($args['is_checked'], 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        return $db->insert_id;
    }

    public function insertAttendanceBack(array $args = array()) {
        global $db, $date, $user;
        $this->validateInput($args);
        $args['attendance_date'] = toDBDate($args['attendance_date']);


        $strSql = sprintf("INSERT INTO ci_attendance_back (class_id, attendance_back_date, created_user_id, recorded_at, came_back_count, not_came_back_count) VALUES (%s, %s, %s, %s, %s, %s)",
            secure($args['class_id'], 'int'), secure($args['attendance_date']), secure($user->_data['user_id'], 'int'),
            secure($date), secure($args['cameback_count'], 'int'), secure($args['not_cameback_count'], 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        return $db->insert_id;
    }

    /**
     * Cập nhật thông tin điểm danh về
     *
     * @param array $args
     * @throws Exception
     */
    public function updateAttendanceBack(array $args = array()) {
        global $db;
        $this->validateInput($args);
        $args['attendance_date'] = toDBDate($args['attendance_date']);

        $strSql = sprintf("UPDATE ci_attendance_back SET came_back_count = %s, not_came_back_count = %s WHERE class_id = %s AND attendance_back_date = %s", secure($args['cameback_count'], 'int'), secure($args['not_cameback_count'], 'int'),
                    secure($args['class_id'], 'int'), secure($args['attendance_date']));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Cập nhật thông tin điểm danh
     *
     * @param array $args
     * @throws Exception
     */
    public function updateAttendance(array $args = array()) {
        global $db, $date, $user;
        $this->validateInput($args);
        $args['attendance_date'] = toDBDate($args['attendance_date']);

        $strSql = sprintf("UPDATE ci_attendance SET checked_user_id = %s, recorded_at = %s, absence_count = %s, present_count = %s, is_checked = %s WHERE class_id = %s AND attendance_date = %s",
            secure($user->_data['user_id'], 'int'), secure($date), secure($args['absence_count'], 'int'), secure($args['present_count'], 'int'), secure($args['is_checked'], 'int'),
            secure($args['class_id'], 'int'), secure($args['attendance_date']));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Cập nhật trạng thái trả lời xin nghỉ của phụ huynh
     *
     * @param $attendanceDetailId
     * @throws Exception
     */
    public function updateAttendanceDetailStatus($attendanceDetailId) {
        global $db;

        $strSql = sprintf("UPDATE ci_attendance_detail SET feedback = 1 WHERE attendance_detail_id = %s", secure($attendanceDetailId, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Kiểm tra xem lớp đã điểm danh chưa.
     *
     * @param $class_id
     * @param $attendance_date
     * @return bool
     * @throws Exception
     */
    public function getAttendanceId($class_id, $attendance_date) {
        global $db;
        $attendance_date = toDBDate($attendance_date);

        $strSql = sprintf("SELECT attendance_id FROM ci_attendance WHERE class_id = %s AND attendance_date = %s", secure($class_id, 'int'), secure($attendance_date));
        $get_attendance = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_attendance->num_rows > 0) {
            $attendance = $get_attendance->fetch_assoc();
            return $attendance['attendance_id'];
        }

        return 0;
    }

    /**
     * Kiểm tra lớp có bản ghi điểm danh về chưa
     *
     * @param $class_id
     * @param $attendance_date
     * @return int
     * @throws Exception
     */
    public function getAttendanceBackId($class_id, $attendance_date) {
        global $db;
        $attendance_date = toDBDate($attendance_date);

        $strSql = sprintf("SELECT attendance_back_id FROM ci_attendance_back WHERE class_id = %s AND attendance_back_date = %s", secure($class_id, 'int'), secure($attendance_date));
        $get_attendance = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_attendance->num_rows > 0) {
            $attendance = $get_attendance->fetch_assoc();
            return $attendance['attendance_back_id'];
        }

        return 0;
    }


    /**
     * Lấy ra giá trị attendance từ class/attendance_date. Nếu ko có trả về giá trị NULL
     *
     * @param $class_id
     * @param $attendance_date
     * @return int
     * @throws Exception
     */
    public function getAttendanceByClassNDate($class_id, $attendance_date) {
        global $db;
        $attendance_date = toDBDate($attendance_date);

        $strSql = sprintf("SELECT * FROM ci_attendance WHERE class_id = %s AND attendance_date = %s", secure($class_id, 'int'), secure($attendance_date));
        $get_attendance = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_attendance->num_rows > 0) {
            $attendance = $get_attendance->fetch_assoc();
            return $attendance;
        }

        return null;
    }

    /**
     * Nhập cả danh sách điểm danh lớp vào hệ thống. Hàm này chỉ dành cho GIÁO VIÊN
     *
     * @param $attendanceId
     * @param $presentChildIds
     * @param $absentChildIds
     * @param $absentReasons
     * @throws Exception
     */
    public function saveAttendanceDetail_old($attendanceId, $presentChildIds, $absentChildIds, $absentReasons) {
        if ((count($presentChildIds) + count($absentChildIds)) == 0) return;
        if (count($presentChildIds) > 0) {
            foreach ($presentChildIds as $presentChildId) {
                $this->savePresentChild($attendanceId, $presentChildId);
            }
        }

        $args = array();
        if (count($absentChildIds) > 0) {
            $idx = 0;
            $args['attendance_id'] = $attendanceId;
            foreach ($absentChildIds as $absentChildId) {
                $args['attendance_id'] = $attendanceId;
                $args['child_id'] = $absentChildId;
                $args['reason'] = $absentReasons[$idx++];
                $args['isParent'] = 0;
                $args['start_date'] = null;
                $args['end_date'] = null;

                $this->saveAbsentChild($args);
            }
        }
    }

    /**
     * Nhập cả danh sách điểm danh lớp vào hệ thống. Hàm này chỉ dành cho GIÁO VIÊN - Hàm cũ (chậm)
     *
     * @param $attendanceId
     * @param $allChildIds
     * @param $allStatus
     * @param $allReasons
     * @throws Exception
     */
//    public function saveAttendanceDetail($attendanceId, $allChildIds, $allStatus, $allReasons) {
//        global $db, $user;
//
//        for ($idx = 0; $idx < count($allChildIds); $idx++) {
//            //Kiểm tra xem đã có trong CSDL chưa
//            $strSql = sprintf("SELECT attendance_id FROM ci_attendance_detail WHERE attendance_id = %s AND child_id = %s",
//                secure($attendanceId, 'int'), secure($allChildIds[$idx], 'int'));
//            $get_attendance = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
//
//            if ($get_attendance->num_rows == 0) {
//                //Chưa thì insert vào DB
//                $strSql = sprintf("INSERT INTO ci_attendance_detail (status, recorded_user_id, attendance_id, child_id, reason) VALUES (%s, %s, %s, %s, %s)",
//                    secure($allStatus[$idx], 'int'), secure($user->_data['user_id'], 'int'), secure($attendanceId, 'int'),
//                    secure($allChildIds[$idx], 'int'), secure($allReasons[$idx]));
//                $db->query($strSql) or _error(SQL_ERROR_THROWEN);
//            } else {
//                //Có thì update vào DB
//                $strSql = sprintf("UPDATE ci_attendance_detail SET status = %s, reason = %s WHERE attendance_id = %s AND child_id = %s",
//                    secure($allStatus[$idx], 'int'), secure($allReasons[$idx]), secure($attendanceId, 'int'), secure($allChildIds[$idx], 'int'));
//                $db->query($strSql) or _error(SQL_ERROR_THROWEN);
//            }
//        }
//    }

    /**
     * Nhập cả danh sách điểm danh lớp vào hệ thống. Hàm này chỉ dành cho GIÁO VIÊN - Hàm mới (nhanh hơn)
     *
     * @param $attendanceId
     * @param $allChildIds
     * @param $allStatus
     * @param $allReasons
     * @throws Exception
     */
    public function saveAttendanceDetail($attendanceId, $allChildIds, $allStatus, $allReasons) {
        global $db, $user;


        // Xóa toàn bộ trạng thái điểm danh cũ
        $strSql = sprintf("DELETE FROM ci_attendance_detail WHERE attendance_id = %s", secure($attendanceId, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        // Cập nhật trạng thái điểm danh mới của lớp
        $strValues = "";
        for ($idx = 0; $idx < count($allChildIds); $idx++) {
            $strValues .= "(" . secure($allChildIds[$idx], 'int') . "," . secure($allStatus[$idx], 'int') . "," . secure($allReasons[$idx]) . "," . secure($attendanceId, 'int') . "," . secure($user->_data['user_id'], 'int') . "),";
        }
        $strValues = trim($strValues, ",");

        if (!is_empty($strValues)) {
            $strSql = sprintf("INSERT INTO ci_attendance_detail (child_id, status, reason, attendance_id, recorded_user_id) VALUES " . $strValues);
            $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        }
    }

    /**
     * TaiLA - Hàm lưu chi tiết thông tin điểm danh mới
     *
     * @param $attendanceId
     * @param $allChildIds
     * @param $allStatus
     * @param $allReasons
     * @throws Exception
     */
    public function saveAttendanceDetailNew($attendanceId, $allChildIds, $allStatus, $allReasons, $allResignTime, $allIsParent, $allRecordUserId, $allStartDate, $allEndDate, $allFeedback) {
        global $db, $user;


        for ($i=0; $i < count($allResignTime); $i++) {
            if ($allResignTime[$i] == '') {
                $allResignTime[$i] = 'null';
            } else {
                $allResignTime[$i] = toDBDatetime($allResignTime[$i]);
            }

        }

        for ($i=0; $i < count($allStartDate); $i++) {
            if ($allStartDate[$i] == '') {
                $allStartDate[$i] = 'null';
            } else {
                $allStartDate[$i] = toDBDate($allStartDate[$i]);
            }

        }

        for ($i=0; $i < count($allEndDate); $i++) {
            if ($allEndDate[$i] == '') {
                $allEndDate[$i] = 'null';
            } else {
                $allEndDate[$i] = toDBDate($allEndDate[$i]);
            }

        }

        for ($i=0; $i < count($allRecordUserId); $i++) {
            if ($allRecordUserId[$i] == '' || $allIsParent[$i] == 0) {
                $allRecordUserId[$i] = $user->_data['user_id'];
            }
        }

        // Xóa toàn bộ trạng thái điểm danh cũ
        $strSql = sprintf("DELETE FROM ci_attendance_detail WHERE attendance_id = %s", secure($attendanceId, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        // Cập nhật trạng thái điểm danh mới của lớp
        $strValues = "";
        for ($idx = 0; $idx < count($allChildIds); $idx++) {
            $strValues .= "(" . secure($allChildIds[$idx], 'int') . "," . secure($allStatus[$idx], 'int') . "," . secure($allReasons[$idx]) . "," . secure($attendanceId, 'int') . "," . secure($allRecordUserId[$idx], 'int') . "," . secure($allResignTime[$idx]) . "," . secure($allIsParent[$idx], 'int') . "," . secure($allStartDate[$idx]) . "," . secure($allEndDate[$idx]) . "," . secure($allFeedback[$idx], 'int') . "),";
        }
        $strValues = trim($strValues, ",");

        if (!is_empty($strValues)) {
            $strSql = sprintf("INSERT INTO ci_attendance_detail (child_id, status, reason, attendance_id, recorded_user_id, resign_time, is_parent, start_date, end_date, feedback) VALUES " . $strValues);

            $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        }
    }

    public function saveAttendanceBackDetail($attendanceId, $allChildIds, $allStatus, $allCameBackTimes, $allCameBackNotes) {
        global $db, $user;


        // Xóa toàn bộ trạng thái điểm danh về cũ
        $strSql = sprintf("DELETE FROM ci_attendance_back_detail WHERE attendance_back_id = %s", secure($attendanceId, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        // Cập nhật trạng thái điểm danh về mới của lớp
        $strValues = "";
        for ($idx = 0; $idx < count($allChildIds); $idx++) {
            if($allStatus[$idx] == 1) {
                if($allCameBackTimes[$idx] == '') {
                    $allCameBackTimes[$idx] = 'null';
                }
                if($allCameBackNotes[$idx] == '') {
                    $allCameBackNotes[$idx] = 'null';
                }
                $strValues .= "(" . secure($allChildIds[$idx], 'int') . "," . secure($allStatus[$idx], 'int') . "," . secure($allCameBackTimes[$idx]) . "," . secure($allCameBackNotes[$idx]) . "," . secure($attendanceId, 'int') . "," . secure($user->_data['user_id'], 'int') . "),";
            }
        }
        $strValues = trim($strValues, ",");

        if (!is_empty($strValues)) {
            $strSql = sprintf("INSERT INTO ci_attendance_back_detail (child_id, status, came_back_time, came_back_note, attendance_back_id, recorded_user_id) VALUES " . $strValues);
            $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        }
    }

    /**
     * Cập nhật điểm danh của một trẻ trong khoảng từ ngày -> đến ngày.
     *
     * @param $childId
     * @param $attendanceIds
     * @param $allStatus
     * @param $allOldStatus
     * @param $allReasons
     * @throws Exception
     */
    public function updateAttendanceOfChild($childId, $attendanceIds, $allStatus, $allOldStatus, $allReasons) {
        global $db, $user;

        $presentStatus = [ATTENDANCE_PRESENT, ATTENDANCE_COME_LATE, ATTENDANCE_EARLY_LEAVE]; //Các trạng thái đi học
        $absentStatus = [ATTENDANCE_ABSENCE, ATTENDANCE_ABSENCE_NO_REASON]; //Các trạng thái nghỉ học.
        for ($idx = 0; $idx < count($attendanceIds); $idx++) {
            //Chỉ những ngày thay đổi trạng thái mới lưu lại.
            if ($allStatus[$idx] != $allOldStatus[$idx]) {
                if (in_array($allStatus[$idx], $presentStatus) && in_array($allOldStatus[$idx], $absentStatus)) {
                    //Chuyển trạng thái từ NGHỈ HỌC -> ĐI HỌC
                    $strSql = sprintf("UPDATE ci_attendance SET present_count = present_count + 1, absence_count = IF(absence_count > 1, absence_count - 1, 0) WHERE attendance_id = %s", secure($attendanceIds[$idx], 'int'));
                    $db->query($strSql) or _error(SQL_ERROR_THROWEN);
                } else if (in_array($allStatus[$idx], $absentStatus) && in_array($allOldStatus[$idx], $presentStatus)) {
                    //Chuyển trạng thái từ ĐI HỌC -> NGHỈ HỌC.
                    $strSql = sprintf("UPDATE ci_attendance SET absence_count = absence_count + 1, present_count = IF(present_count > 1, present_count - 1, 0) WHERE attendance_id = %s", secure($attendanceIds[$idx], 'int'));
                    $db->query($strSql) or _error(SQL_ERROR_THROWEN);
                } //Các trường hợp khác thì không cần cập nhật bảng ci_attendance

//                //Cập nhật điểm danh chi tiết.
//                $strSql = sprintf("UPDATE ci_attendance_detail SET status = %s, reason = %s WHERE attendance_id = %s AND child_id = %s",
//                    secure($allStatus[$idx], 'int'), secure($allReasons[$idx]), secure($attendanceIds[$idx], 'int'), secure($childId, 'int'));
//                $db->query($strSql) or _error(SQL_ERROR_THROWEN);
            }
            //Cập nhật điểm danh chi tiết.
            $strSql = sprintf("UPDATE ci_attendance_detail SET status = %s, reason = %s WHERE attendance_id = %s AND child_id = %s",
                secure($allStatus[$idx], 'int'), secure($allReasons[$idx]), secure($attendanceIds[$idx], 'int'), secure($childId, 'int'));
            $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        }
    }

    /**
     * Insert/Cập nhật thông tin nghỉ của một trẻ, khi phụ huynh xin nghỉ cho con
     *
     * @param array $args
     * @return array
     * @throws Exception
     */
//    public function saveAbsentChild($attendanceId, $absentChildId, $absentReason, $isParent = 0) {
    public function saveAbsentChild($args = array()) {
        global $db, $user, $date;

        $result = array();
        //Kiểm tra xem đã có bản ghi của bé chưa
        $strSql = sprintf("SELECT attendance_detail_id, is_parent FROM ci_attendance_detail WHERE attendance_id = %s AND child_id = %s", secure($args['attendance_id'], 'int'), secure($args['child_id'], 'int'));
        $get_attendance = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        $result['is_updated'] = true;
        if ($get_attendance->num_rows > 0) {
            //Nếu đã xin nghỉ thì cập nhật thôi
            $ad = $get_attendance->fetch_assoc();
            $result['attendance_detail_id'] = $ad['attendance_detail_id'];

            $strSql = sprintf("UPDATE ci_attendance_detail SET reason = %s, recorded_user_id = %s, start_date = %s, end_date = %s, resign_time = %s, is_parent = 1 WHERE attendance_id = %s AND child_id = %s",
                secure($args['reason']), secure($user->_data['user_id'], 'int'), secure($args['start_date']), secure($args['end_date']), secure($date), secure($args['attendance_id'], 'int'), secure($args['child_id'], 'int'));
            $db->query($strSql) or _error(SQL_ERROR_THROWEN);

            if ($ad['is_parent'] == $args['isParent']) {
                $result['is_updated'] = false;
            } else $result['is_updated'] = true;
        } else {
            //Chưa thì đưa vào DB
            $strSql = sprintf("INSERT INTO ci_attendance_detail (status, reason, recorded_user_id, start_date, end_date, attendance_id, child_id, is_parent, resign_time) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)", secure(ATTENDANCE_ABSENCE, 'int'),
                secure($args['reason']), secure($user->_data['user_id'], 'int'), secure($args['start_date']), secure($args['end_date']), secure($args['attendance_id'], 'int'), secure($args['child_id'], 'int'), secure($args['isParent'], 'int'), secure($date));
            $db->query($strSql) or _error(SQL_ERROR_THROWEN);

            $result['attendance_detail_id'] = $db->insert_id;
        }
        return $result;
    }

    public function savePresentChild($attendanceId, $childId) {
        global $db, $user;

        //Kiểm tra xem đã có trong CSDL chưa
        $strSql = sprintf("SELECT attendance_id FROM ci_attendance_detail WHERE attendance_id = %s AND child_id = %s", secure($attendanceId, 'int'), secure($childId, 'int'));
        $get_attendance = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        if ($get_attendance->num_rows == 0) {
            //Chưa thì insert vào DB
            $strSql = sprintf("INSERT INTO ci_attendance_detail (status, recorded_user_id, attendance_id, child_id) VALUES (%s, %s, %s, %s)", secure(ATTENDANCE_PRESENT, 'int'),
                secure($user->_data['user_id'], 'int'), secure($attendanceId, 'int'), secure($childId, 'int'));
            $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        } else {
            //Có thì update vào DB
            $strSql = sprintf("UPDATE ci_attendance_detail SET status = %s, reason = %s, recorded_user_id = %s WHERE attendance_id = %s AND child_id = %s", secure(ATTENDANCE_PRESENT, 'int'), secure(''),
                secure($user->_data['user_id'], 'int'), secure($attendanceId, 'int'), secure($childId, 'int'));
            $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        }
    }


    /**
     * CI - mobile API Lấy ra danh sách trẻ của một lớp cùng với thông tin nghỉ/đi học
     *
     * @param $class_id
     * @param $attendanceDate
     * @return array
     * @throws Exception
     */
    function getAttendanceDetailApi($class_id, $attendanceDate) {
        global $db, $system;
        $attendanceDate = toDBDate($attendanceDate);

        //Lấy ra danh sách trẻ của lớp.
        $strSql = sprintf('SELECT CC.child_id , C.child_name, C.birthday, C.child_picture, CC.status  AS child_status FROM ci_class_child CC
                            INNER JOIN ci_child C ON CC.child_id = C.child_id AND CC.class_id = %1$s AND (CC.end_at IS NULL OR CC.end_at >= %2$s) AND CC.begin_at <= %2$s
                            ORDER BY C.name_for_sort ASC', secure($class_id, 'int'), secure($attendanceDate));
        $get_children = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $children = array();
        if ($get_children->num_rows > 0) {
            // Mặc định trạng thái của tất cả các trẻ là đi học
            while ($child = $get_children->fetch_assoc()) {
                $child["attendance_detail_id"] = null;
                $child["attendance_id"] = null;
                $child["status"] = ATTENDANCE_PRESENT;
                $child["feedback"] = 0;
                $child["reason"] = null;
                $child["recorded_user_id"] = null;
                $child["is_parent"] = 0;
                $child["resign_time"] = null;
                $child["start_date"] = null;
                $child["end_date"] = null;
                if(!is_empty($child['child_picture'] )) {
                    $child['child_picture'] = $system['system_uploads'] . '/' . $child['child_picture'];
                }
                $children[] = $child;
            }
        }

        $strSql = sprintf("SELECT A.attendance_id, A.is_checked, A.attendance_date, A.recorded_at, A.absence_count, A.present_count, U.user_fullname FROM ci_attendance A
                            INNER JOIN users U ON A.checked_user_id = U.user_id AND A.class_id = %s AND A.attendance_date = %s",
            secure($class_id, 'int'), secure($attendanceDate));
        $get_attendance = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        $results = array();
        //Trường hợp: Lớp chưa thực hiện điểm danh của ngày đó
        if ($get_attendance->num_rows == 0) {
            $results['isRollUp'] = 0; //Chưa có bản ghi trong bảng ci_attendance.
            $results['detail'] = $children;
            $results['attendance_date'] = toSysDate($attendanceDate);
            $results['absence_count'] = 0;
            $results['present_count'] = 0;
        } else {
            //Trường hợp lớp đã bản ghi ở bảng ci_attendance
            $results = $get_attendance->fetch_assoc();
            $results['isRollUp'] = 1; //Đã có bản ghi trong bảng ci_attendance
            $results['recorded_at'] = toSysDatetime($results['recorded_at']);
            $results['attendance_date'] = toSysDate($results['attendance_date']);

            //Lấy ra danh sách trẻ đã được lưu thông tin điểm danh ngày đó.
            $strSql = sprintf("SELECT * FROM ci_attendance_detail WHERE attendance_id = %s", secure($results['attendance_id'], 'int'));
            $get_attDetail = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
            $attDetails = array();
            while ($attDetail = $get_attDetail->fetch_assoc()) {
                $attDetails[] = $attDetail;
            }

            $attDetailsCnt = count($attDetails);
            $details = array();
            foreach ($children as $child) {
                $newChild = $child;
                $cnt = 0;
                // Nếu trẻ đã có thông tin điểm danh thì lấy ra
                foreach ($attDetails as $attDetail) {
                    $cnt++;
                    if ($newChild['child_id'] == $attDetail['child_id']) {
                        $newChild['status'] = $attDetail['status'];
                        $newChild['attendance_id'] = $attDetail['attendance_id'];
                        $newChild['attendance_detail_id'] = $attDetail['attendance_detail_id'];
                        $newChild['reason'] = $attDetail['reason'];
                        $newChild['feedback'] = $attDetail['feedback'];
                        $newChild["recorded_user_id"] = $attDetail['recorded_user_id'];
                        $newChild['is_parent'] = $attDetail['is_parent'];
                        $newChild['start_date'] = !(is_null($attDetail['start_date']) || $attDetail['start_date'] == '0000-00-00')? toSysDate($attDetail['start_date']) : null;
                        $newChild['end_date'] = !(is_null($attDetail['start_date']) || $attDetail['start_date'] == '0000-00-00') ? toSysDate($attDetail['end_date']) : null;
                        $newChild['resign_time'] = !(is_null($attDetail['resign_time']) || $attDetail['resign_time'] == '0000-00-00 00:00:00') ? toSysDatetime($attDetail['resign_time']) : null;
                        break;
                        //Nếu có trẻ trong lớp (mà ko có thông tin điểm danh, thì là trẻ được thêm vào sau
                        //Mặc định trạng thái của trẻ đấy là nghỉ học
                    } elseif ( ($cnt == $attDetailsCnt) && $results['is_checked'] ) {
                        $newChild["attendance_detail_id"] = null;
                        $newChild["attendance_id"] = null;
                        $newChild["status"] = ATTENDANCE_ABSENCE;
                        $newChild["feedback"] = 0;
                        $newChild["reason"] = null;
                        $newChild["recorded_user_id"] = null;
                        $newChild["is_parent"] = 0;
                        $newChild['start_date'] = null;
                        $newChild['end_date'] = null;
                        $newChild['resign_time'] = null;
                        break;
                    }
                }
                $details[] = $newChild;
            }
            $results['detail'] = $details;
        }

        return $results;
    }


    /**
     * CI - mobile API Lấy ra thông tin nghỉ/đi học của 1 trẻ trong lớp.
     *
     * @param $class_id
     * @param $attendanceDate
     * @return array
     * @throws Exception
     */
    function getAttendanceChildApi($attendance_detail_id) {
        global $db;
        //$attendanceDate = toDBDate($attendanceDate);

        //$results = array();
        $strSql = sprintf("SELECT A.class_id, A.attendance_id, A.is_checked, A.attendance_date, A.recorded_at, A.absence_count, A.present_count, U.user_fullname FROM ci_attendance A
                            INNER JOIN  ci_attendance_detail AD ON A.attendance_id = AD.attendance_id AND AD.attendance_detail_id = %s 
                            INNER JOIN users U ON A.checked_user_id = U.user_id",
            secure($attendance_detail_id, 'int'));

        $get_attendance = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        //Trường hợp: Lớp chưa thực hiện điểm danh của ngày đó
        if ($get_attendance->num_rows == 0) {
            _error(404);
        }

        //Trường hợp lớp đã bản ghi ở bảng ci_attendance
        $results = $get_attendance->fetch_assoc();
        $results['recorded_at'] = toSysDatetime($results['recorded_at']);
        $results['attendance_date'] = toSysDate($results['attendance_date']);
        $results['isRollUp'] = 1;

        $strSql = sprintf("SELECT AD.* , C.child_name, C.birthday FROM ci_attendance_detail AD
                            INNER JOIN ci_child C ON AD.child_id = C.child_id AND AD.attendance_detail_id = %s",
            secure($attendance_detail_id, 'int'));
        $get_detail = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $detail = array();

        if ($get_detail->num_rows > 0) {
            $detail = $get_detail->fetch_assoc();
            $detail['start_date'] = toSysDate($detail['start_date']);
            $detail['end_date'] = toSysDate($detail['end_date']);
        }
        $results['detail'][] = $detail;

        return $results;
    }


    /**
     * API Cập nhật lý do của một trẻ, khi phụ huynh xin nghỉ cho con
     *
     * @param array $args
     * @return array
     * @throws Exception
     */
//    public function saveAbsentChild($attendanceId, $absentChildId, $absentReason, $isParent = 0) {
    public function updateReasonChild($args = array()) {
        global $db, $user;

        $result = array();
        //Kiểm tra xem đã xin nghỉ cho bé chưa
        $strSql = sprintf("SELECT attendance_detail_id, is_parent FROM ci_attendance_detail WHERE attendance_id = %s AND child_id = %s", secure($args['attendance_id'], 'int'), secure($args['child_id'], 'int'));
        $get_attendance = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        $result['is_updated'] = true;
        if ($get_attendance->num_rows > 0) {
            //Nếu đã xin nghỉ thì cập nhật thôi
            $ad = $get_attendance->fetch_assoc();
            $result['attendance_detail_id'] = $ad['attendance_detail_id'];
            if ($ad['is_parent'] == $args['isParent']) {
                $strSql = sprintf("UPDATE ci_attendance_detail SET status = %s, reason = %s, recorded_user_id = %s, start_date = %s, end_date = %s WHERE attendance_id = %s AND child_id = %s", secure(ATTENDANCE_ABSENCE, 'int'),
                    secure($args['reason']), secure($user->_data['user_id'], 'int'), secure($args['start_date']), secure($args['end_date']), secure($args['attendance_id'], 'int'), secure($args['child_id'], 'int'));
                $db->query($strSql) or _error(SQL_ERROR_THROWEN);
            } else $result['is_updated'] = false;
        } else {
            //Chưa thì đưa vào DB
            $strSql = sprintf("INSERT INTO ci_attendance_detail (status, reason, recorded_user_id, start_date, end_date, attendance_id, child_id, is_parent) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)", secure(ATTENDANCE_ABSENCE, 'int'),
                secure($args['reason']), secure($user->_data['user_id'], 'int'), secure($args['start_date']), secure($args['end_date']), secure($args['attendance_id'], 'int'), secure($args['child_id'], 'int'), secure($args['isParent'], 'int'));
            $db->query($strSql) or _error(SQL_ERROR_THROWEN);

            $result['attendance_detail_id'] = $db->insert_id;
        }
        return $result;
    }

    /**
     * Cập nhật trạng tháng điểm danh của 1 trẻ
     *
     * @param $attendanceId
     * @param $childId
     * @param $status
     * @throws Exception
     */
    public function updateChildAttendanceStatus($attendanceId, $childId, $status, $oldStatus) {
        global $db;

        // Update trạng thái điểm danh của trẻ trong bảng ci_attendance_detail
        $strSql = sprintf("UPDATE ci_attendance_detail SET status = %s WHERE attendance_id = %s AND child_id = %s", secure($status, 'int'), secure($attendanceId, 'int'), secure($childId, 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        // Update số lượng đi học và nghỉ học trong bảng ci_attendance
        $presentStatus = [ATTENDANCE_PRESENT, ATTENDANCE_COME_LATE, ATTENDANCE_EARLY_LEAVE]; //Các trạng thái đi học
        $absentStatus = [ATTENDANCE_ABSENCE, ATTENDANCE_ABSENCE_NO_REASON]; //Các trạng thái nghỉ học.
        if(in_array($status, $presentStatus) && in_array($oldStatus, $absentStatus)) {
            //Tăng số lượng đi học lên 1, giảm số lượng nghỉ học đi 1
            $sqlStr = sprintf("UPDATE ci_attendance SET present_count = present_count + 1, absence_count = IF(absence_count > 1, absence_count - 1, 0) WHERE attendance_id = %s", secure($attendanceId, 'int'));

            $db->query($sqlStr) or _error(SQL_ERROR_THROWEN);
        }
        if(in_array($status, $absentStatus) && in_array($oldStatus, $presentStatus)){
            // Giảm số lượng đi học đi 1, tăng số lượng nghỉ học lên 1
            $sqlStr = sprintf("UPDATE ci_attendance SET absence_count = absence_count + 1, present_count = IF(present_count > 1, present_count - 1, 0) WHERE attendance_id = %s", secure($attendanceId, 'int'));

            $db->query($sqlStr) or _error(SQL_ERROR_THROWEN);
        }
    }

    /**
     * Lấy thông tin điểm danh của 1 lớp trong 1 ngày
     *
     * @param $class_id
     * @param $attendanceDate
     * @throws Exception
     */
    public function getAttendanceByClassIdAndDate($class_id, $attendanceDate) {
        global $db;
        $attendanceDate = toDBDate($attendanceDate);

        $strSql = sprintf("SELECT A.attendance_id, A.is_checked, A.attendance_date, A.recorded_at, A.absence_count, A.present_count, U.user_fullname FROM ci_attendance A
                            INNER JOIN users U ON A.checked_user_id = U.user_id AND A.class_id = %s AND A.attendance_date = %s",
            secure($class_id, 'int'), secure($attendanceDate));
        $get_attendance = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        $results = array();
        //Trường hợp: Lớp chưa thực hiện điểm danh của ngày đó
        if ($get_attendance->num_rows == 0) {
            $results['isRollUp'] = 0; //Chưa có bản ghi trong bảng ci_attendance.
            $results['attendance_date'] = toSysDate($attendanceDate);
        } else {
            //Trường hợp lớp đã bản ghi ở bảng ci_attendance
            $results = $get_attendance->fetch_assoc();
            $results['isRollUp'] = 1; //Đã có bản ghi trong bảng ci_attendance
            $results['recorded_at'] = toSysDatetime($results['recorded_at']);
            $results['attendance_date'] = toSysDate($results['attendance_date']);
        }

        return $results;
    }

    /**
     * Update  trạng thái điểm danh của trẻ
     *
     * @param $attendancelId
     * @param $childId
     * @param $status
     * @param $reason
     * @throws Exception
     */
    public function updateChildAttendanceDetail($attendancelId, $childId, $status, $reason) {
        global $db;
        $strSql = sprintf("UPDATE ci_attendance_detail SET status = %s, reason = %s WHERE attendance_id = %s AND child_id = %s", secure($status, 'int'), secure($reason), secure($attendancelId, 'int'), secure($childId));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Thêm mới điểm danh cho 1 trẻ
     *
     * @param $attendanceId
     * @param $childId
     * @param $status
     * @param $reason
     * @return mixed
     * @throws Exception
     */
    public function saveAttendanceDetailChild ($attendanceId, $childId, $status,  $reason) {
        global $db, $user;

        $strSql = sprintf("INSERT INTO ci_attendance_detail (status, reason, recorded_user_id, attendance_id, child_id) VALUES (%s, %s, %s, %s, %s)", secure($status, 'int'),secure($reason), secure($user->_data['user_id'], 'int'), secure($attendanceId, 'int'), secure($childId, 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        return $db->insert_id;
    }

    public function getAllAten() {
        global $db;

        $strSql = "SELECT * FROM ci_attendance";

        $get_aten = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $children = array();
        if ($get_aten->num_rows > 0) {
            while ($child = $get_aten->fetch_assoc()) {
                $children[] = $child;
            }
        }

        return $children;
    }

    public function deleteByIds($ids) {
        global $db;
        $strAttendanceIds = implode(",", $ids);
        $strSql = "DELETE FROM ci_attendance WHERE attendance_id IN ($strAttendanceIds)";

       $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    public function deleteDetailByIds($ids) {
        global $db;
        $strAttendanceIds = implode(",", $ids);
        $strSql = "DELETE FROM ci_attendance_detail WHERE attendance_id IN ($strAttendanceIds)";

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    public function deleteAttendanceDetail($attendanceDetail) {
        global $db;

        $strSql = sprintf("DELETE FROM ci_attendance_detail WHERE attendance_detail_id = %s", secure($attendanceDetail['attendance_detail_id'], 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }
}
?>