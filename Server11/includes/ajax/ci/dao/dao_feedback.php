<?php

/**
 * DAO -> Feedback
 * Thao tác với bảng ci_feedback
 *
 * @package ConIu
 * @author ConIu
 */
class FeedbackDAO
{
    /**
     * __construct
     *
     */
    public function __construct()
    {
    }

    /**
     * Insert một góp ý vào trong DB
     *
     * @param array $args
     * @throws Exception
     */
    public function insertFeedback(array $args = array())
    {
        global $db, $date;
        $args['created_at'] = $date;
        $this->validateInput($args);

        $strSql = sprintf("INSERT INTO ci_feedback (school_id, class_id, child_id, name, email, phone, content, level, is_incognito, confirm, created_at, updated_at, created_user_id)
                              VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
            secure($args['school_id'], 'int'), secure($args['class_id'], 'int'), secure($args['child_id'], 'int'),
            secure($args['name']), secure($args['email']), secure($args['phone']), secure($args['content']), secure($args['level'], 'int'), secure($args['is_incognito'], 'int'), secure($args['confirm'], 'int'),
            secure($args['created_at']), secure($args['updated_at']), secure($args['user_id'], 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        return $db->insert_id;
    }


    /**
     * Kiểm tra điều kiện đầu vào.
     *
     * @param array $args
     * @throws Exception
     */
    private function validateInput(array $args = array())
    {
        if (strlen($args['content']) < 10) {
            throw new Exception(__("Feedback must be at least 10 characters long"));
        }
    }

    /**
     * Lấy ra 1 feedback
     *
     * @param $feedback_id
     * @throws Exception
     */
    public function getFeedback($feedback_id)
    {
        global $db;
        $strSql = sprintf("SELECT * FROM ci_feedback WHERE feedback_id = %s", secure($feedback_id, 'int'));
        $get_feedback = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        $result = null;
        if ($get_feedback->num_rows > 0) {
            $result = $get_feedback->fetch_assoc();
        }
        return $result;
    }

    /**
     * Lấy ra danh sách feedback của 1 user
     *
     * @param $feedback_id
     * @throws Exception
     */
    public function getFeedbackOfUser($user_id)
    {
        global $db;
        $strSql = sprintf("SELECT * FROM ci_feedback WHERE created_user_id = %s ORDER BY 	created_at DESC", secure($user_id, 'int'));

        $get_feedback = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        $result = array();
        if ($get_feedback->num_rows > 0) {
            while ($report = $get_feedback->fetch_assoc()) {
                $report['created_at'] = toSysDatetime($report['created_at']);
                $result[] = $report;
            }
        }
        return $result;
    }

    /**
     * Lấy ra danh sách feedback của 1 trường
     *
     * @param $feedback_id
     * @throws Exception
     */
    public function getFeedbackOfSchool($school_id)
    {
        global $db;
        $strSql = sprintf("SELECT F.*, G.group_title, C.child_name FROM ci_feedback F 
                           LEFT JOIN groups G ON F.class_id = G.group_id
                           INNER JOIN ci_child C ON F.child_id = C.child_id
                           AND F.level = 1 AND F.school_id = %s
                          WHERE F.status = %s
                          ORDER BY created_at DESC", secure($school_id, 'int'), STATUS_ACTIVE);

        $get_feedback = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        $result = array();
        if ($get_feedback->num_rows > 0) {
            while ($report = $get_feedback->fetch_assoc()) {
                $report['created_at'] = toSysDatetime($report['created_at']);

                $result[] = $report;
            }
        }
        return $result;
    }

    /**
     * Lấy danh sách feedback của trường cho API
     *
     * @param $school_id
     * @param int $offset
     * @return array
     */
    public function getFeedbackOfSchoolForAPI($school_id, $offset = 0)
    {
        global $db, $system;
        $offset *= $system['min_results'];

        $strSql = sprintf("SELECT F.*, G.group_title, C.child_name FROM ci_feedback F 
                           INNER JOIN groups G ON F.class_id = G.group_id
                           INNER JOIN ci_child C ON F.child_id = C.child_id
                           AND F.level = 1 AND F.school_id = %s
                          WHERE F.status = %s
                          ORDER BY created_at DESC LIMIT %s, %s", secure($school_id, 'int'), STATUS_ACTIVE, secure($offset, 'int', false), secure($system['min_results'], 'int', false));

        $get_feedback = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        $result = array();
        if ($get_feedback->num_rows > 0) {
            while ($report = $get_feedback->fetch_assoc()) {
                $report['created_at'] = toSysDatetime($report['created_at']);

                $result[] = $report;
            }
        }
        return $result;
    }

    /**
     * Lấy ra danh sách feedback của 1 lớp
     *
     * @param $class_id
     * @throws Exception
     */
    public function getFeedbackOfClass($class_id)
    {
        global $db;
        $strSql = sprintf("SELECT F.*, G.group_title, C.child_name FROM ci_feedback F 
                           INNER JOIN groups G ON F.class_id = G.group_id
                           INNER JOIN ci_child C ON F.child_id = C.child_id
                           AND F.class_id = %s 
                           WHERE F.status = %s
                           ORDER BY created_at DESC", secure($class_id, 'int'), STATUS_ACTIVE);

        $get_feedback = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        $result = array();
        if ($get_feedback->num_rows > 0) {
            while ($report = $get_feedback->fetch_assoc()) {
                $report['created_at'] = toSysDatetime($report['created_at']);
                $result[] = $report;
            }
        }
        return $result;
    }

    /**
     * Cập nhật trạng thái của báo cáo thành "đã thông báo" ( = 1)
     *
     * @param $report_id
     * @throws Exception
     */
    public function updateStatusToConfirmed($feedback_id) {
        global $db, $date;

        $strSql = sprintf("UPDATE ci_feedback SET confirm = '1', updated_at = %s WHERE feedback_id = %s", secure($date), secure($feedback_id, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     *  Cập nhật trang thái của status bằng 0 khi quản lý xóa feedback
     *
     * @param $report_id
     * @throws Exception
     */
    public function deleteFeedback($feedback_id) {
        global $db, $date;

        $strSql = sprintf("UPDATE ci_feedback SET status = %s, updated_at = %s WHERE feedback_id = %s", STATUS_INACTIVE, secure($date), secure($feedback_id, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     *  Cập nhật trang thái của status bằng 0 khi quản lý xóa tất cả feedback
     *
     * @param $report_id
     * @throws Exception
     */
    public function deleteAllFeedback($school_id) {
        global $db, $date;

        $strSql = sprintf("UPDATE ci_feedback SET status = %s, updated_at = %s WHERE school_id = %s", STATUS_INACTIVE, secure($date), secure($school_id, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }
}

?>