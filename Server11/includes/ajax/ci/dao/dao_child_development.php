<?php
/**
 * DAO -> Child development
 * Thao tác với các bảng ci_child_development
 *
 * @package ConIu
 * @author TaiLa
 */

class ChildDevelopmentDAO
{
    /**
     * __construct
     *
     */
    public function __construct()
    {
    }

    /**
     * Kiểm tra xem trẻ đã được tiêm phòng hay chưa
     *
     * @param $child_parent_id
     * @param $child_development_id
     * @throws Exception
     */
    private function _validateInput ($child_parent_id, $child_development_id) {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_child_development_child WHERE child_parent_id = %s AND child_development_id = %s", secure($child_parent_id, 'int'), secure($child_development_id, 'int'));

        $get_vaccin = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        if($get_vaccin->num_rows > 0) {
            throw new Exception(__("Children were vaccinated"));
        }
    }

    /*
     * Insert vào bảng ci_child_development
     */
    public function insertChildDevelopment (array $args = array()) {
        global $db, $date, $user;

        //$this->_validateInput($args);

        $strSql = sprintf("INSERT INTO ci_child_development  (month_push, day_push, time_push, title, link, image, type, is_development, is_notice_before, notice_before_days, is_reminder_before, reminder_before_days, created_at, created_user_id, content_type, content) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
            secure($args['month_push'], 'int'), secure($args['day_push'], 'int'), secure($args['time_push']), secure($args['title']), secure($args['link']), secure($args['image']), secure($args['type'], 'int'), secure($args['is_development'], 'int'),
            secure($args['is_notice_before'], 'int'), secure($args['notice_before_days'], 'int'), secure($args['is_reminder_before'], 'int'), secure($args['reminder_before_days'], 'int'), secure($date), secure($user->_data['user_id'], 'int'), secure($args['content_type'], 'int'), secure($args['content']));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Lấy toàn bộ thông tin trong ci_child_development hiển thị bên phía phụ huynh (Chỉ lịch tiêm chủng)
     *
     * @return array
     */
    public function getChildDevelopmentForParent ($child_parent_id) {
        global $db;

        // Lấy sinh nhật của trẻ
        $strSql = sprintf("SELECT birthday FROM ci_child_parent WHERE child_parent_id = %s", secure($child_parent_id));

        $get_birthday = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $birthday = null;
        if($get_birthday->num_rows >0) {
            $birthday = $get_birthday->fetch_assoc()['birthday'];
        }

        $strSql = sprintf("SELECT * FROM ci_child_development WHERE is_development = %s AND type = 1 ORDER BY month_push ASC, day_push ASC, child_development_id DESC", 1);
        $get_child_development = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $child_development = array();
        if($get_child_development->num_rows > 0) {
            while ($childDeve = $get_child_development->fetch_assoc()) {
                // Nếu thuộc kiểu tiêm phòng thì thêm biến đã tiêm phòng hay chưa
                if($childDeve['type'] == 1) {
                    $strSql = sprintf("SELECT * FROM ci_child_development_child WHERE child_parent_id = %s AND child_development_id = %s", secure($child_parent_id, 'int'), secure($childDeve['child_development_id'], 'int'));
                    $get_vaccin = $db->query($strSql) or _error(SQL_ERROR_THROWEN);

                    if($get_vaccin->num_rows > 0) {
                        $childDeve['vaccinationed'] = 1;
                    } else {
                        $childDeve['vaccinationed'] = 0;
                    }
                }

                $countDay = ($childDeve['month_push'] - 1) * 30 + $childDeve['day_push'];
                $time = date('Y-m-d', strtotime('+ ' . $countDay . ' days', strtotime($birthday)));
                $time = toSysDate($time);
                $childDeve['time'] = $time;
                $child_development[] = $childDeve;
            }
        }
        return $child_development;
    }

    /**
     * Lấy toàn bộ thông tin trong ci_child_development hiển thị bên phía phụ huynh (Chỉ thông tin tham khảo)
     *
     * @return array
     */
    public function getChildDevelopmentInfoForParent ($child_parent_id) {
        global $db;

        // Lấy sinh nhật của trẻ
        $strSql = sprintf("SELECT birthday FROM ci_child_parent WHERE child_parent_id = %s", secure($child_parent_id));

        $get_birthday = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $birthday = null;
        if($get_birthday->num_rows >0) {
            $birthday = $get_birthday->fetch_assoc()['birthday'];
        }

        $strSql = sprintf("SELECT * FROM ci_child_development WHERE is_development = %s AND type = 2 ORDER BY month_push ASC, day_push ASC, child_development_id DESC", 1);
        $get_child_development = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $child_development = array();
        if($get_child_development->num_rows > 0) {
            while ($childDeve = $get_child_development->fetch_assoc()) {
                $countDay = ($childDeve['month_push'] - 1) * 30 + $childDeve['day_push'];
                $time = date('Y-m-d', strtotime('+ ' . $countDay . ' days', strtotime($birthday)));
                $time = toSysDate($time);
                $childDeve['time'] = $time;

                $child_development[] = $childDeve;
            }
        }
        return $child_development;
    }

    /**
     * Lấy danh sách ID trẻ đến ngày nhận thông báo
     *
     * @param $day
     * @return array
     */
    public function getChildIdsOfDay($day) {
        global $db;

        $strSql = sprintf("SELECT child_parent_id FROM ci_child_parent WHERE TIMESTAMPDIFF(day, birthday, CURRENT_DATE()) = %s", secure($day));

        $get_child = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $childIds = array();
        if($get_child->num_rows > 0) {
            while ($childId = $get_child->fetch_assoc()['child_parent_id']) {
                $childIds[] = $childId;
            }
        }

        return $childIds;
    }

    /**
     * Lấy chi tiết child_development
     *
     * @param $child_development_id
     * @return null
     */
    public function getChildDevelopmentDetail($child_development_id) {
        global $db, $system;

        $strSql = sprintf("SELECT * FROM ci_child_development WHERE child_development_id = %s", secure($child_development_id));

        $get_child_development = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $child_development = null;
        if($get_child_development->num_rows > 0) {
            $child_development = $get_child_development->fetch_assoc();
            if (!is_empty($child_development['image'])) {
                $child_development['image_source'] = $system['system_uploads'] . '/' . $child_development['image'];
            }
        }

        return $child_development;
    }

    /**
     * Lấy toàn bộ thông tin trong ci_child_development hiển thị bên phía nhân viên NOGA
     *
     * @return array
     */
    public function getChildDevelopmentForNoga () {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_child_development ORDER BY month_push ASC, day_push ASC, child_development_id ASC");
        $get_child_development = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $child_development = array();
        if($get_child_development->num_rows > 0) {
            while ($childDeve = $get_child_development->fetch_assoc()) {
                $child_development[] = $childDeve;
            }
        }
        return $child_development;
    }

    /**
     * Cập nhật thông tin phát triển trẻ
     *
     * @param $args
     */
    public function updateChildDevelopment($args) {
        global $db, $date;

        $strSql = sprintf("UPDATE ci_child_development SET month_push = %s, day_push = %s, time_push = %s, title = %s, link = %s, image = %s, type = %s, is_development = %s, 
            is_notice_before = %s, notice_before_days = %s, is_reminder_before = %s, reminder_before_days = %s, updated_at = %s, content_type = %s, content = %s WHERE child_development_id = %s",
            secure($args['month_push'], 'int'), secure($args['day_push'], 'int'), secure($args['time_push']), secure($args['title']), secure($args['link']), secure($args['image']), secure($args['type'], 'int'), secure($args['is_development'], 'int'),
            secure($args['is_notice_before'], 'int'), secure($args['notice_before_days'], 'int'), secure($args['is_reminder_before'], 'int'), secure($args['reminder_before_days'], 'int'), secure($date), secure($args['content_type'], 'int'), secure($args['content']), secure($args['child_development_id'], 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Delete thông tin qúa trình phát triển của trẻ
     *
     * @param $child_development_id
     */
    public function deleteChildDevelopment($child_development_id) {
        global $db;

        $strSql = sprintf("DELETE FROM ci_child_development WHERE child_development_id = %s", secure($child_development_id, 'int'));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Lấy những lần trẻ đã được tiêm phòng rồi
     *
     * @param $child_parent_id
     * @return array
     */
    public function getChildDevelopmentVaccinationed ($child_parent_id) {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_child_development WHERE is_development = %s AND type = %s ORDER BY month_push ASC, day_push ASC, child_development_id DESC", 1, 1);
        $get_child_development = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $child_development = array();
        if($get_child_development->num_rows > 0) {
            while ($childDeve = $get_child_development->fetch_assoc()) {
                $child_development[] = $childDeve;
            }
        }

        $child_development_vaccinationed = array();
        // Lặp những lần tiêm phòng của trẻ,  lấy ra những lần trẻ đã được tiêm phòng
        foreach ($child_development as $row) {
            $strSql = sprintf("SELECT * FROM ci_child_development_child WHERE child_parent_id = %s AND child_development_id = %s", secure($child_parent_id, 'int'), secure($row['child_development_id'], 'int'));

            $get_vaccinationed = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
            if($get_vaccinationed->num_rows > 0) {
                $child_development_vaccinationed[] = $row;
            }
        }
        return $child_development_vaccinationed;
    }

    /**
     * Lấy những lần trẻ chưa được tiêm phòng
     *
     * @param $child_parent_id
     * @return array
     */
    public function getChildDevelopmentNotVaccination ($child_parent_id) {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_child_development WHERE is_development = %s AND type = %s ORDER BY month_push ASC, day_push ASC, child_development_id DESC", 1, 1);
        $get_child_development = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $child_development = array();
        if($get_child_development->num_rows > 0) {
            while ($childDeve = $get_child_development->fetch_assoc()) {
                $child_development[] = $childDeve;
            }
        }

        $child_development_not_vaccination = array();
        // Lặp những lần tiêm phòng của trẻ,  lấy ra những lần trẻ chưa được tiêm phòng
        foreach ($child_development as $row) {
            $strSql = sprintf("SELECT * FROM ci_child_development_child WHERE child_parent_id = %s AND child_development_id = %s", secure($child_parent_id, 'int'), secure($row['child_development_id'], 'int'));

            $get_vaccinationed = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
            if($get_vaccinationed->num_rows == 0) {
                $child_development_not_vaccination[] = $row;
            }
        }
        return $child_development_not_vaccination;
    }

    /**
     * Thêm lần tiêm phòng cho trẻ
     *
     * @param $child_development_id
     * @param $child_parent_id
     */
    public function insertChildVaccinationed($child_development_id, $child_parent_id) {
        global $db;
        $this->_validateInput($child_parent_id, $child_development_id);

        $strSql = sprintf("INSERT INTO ci_child_development_child (child_parent_id, child_development_id) VALUES (%s, %s)", secure($child_parent_id, 'int'), secure($child_development_id));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Xóa lần tiêm phòng của trẻ
     *
     * @param $child_development_id
     * @param $child_parent_id
     */
    public function deleteChildVaccination($child_development_id, $child_parent_id) {
        global $db;

        $strSql = sprintf("DELETE FROM ci_child_development_child WHERE child_parent_id = %s AND child_development_id = %s", secure($child_parent_id, 'int'), secure($child_development_id));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Lấy danh sách những lần sắp được tiêm trong tương lai
     *
     * @param $child_parent_id
     * @return array
     */
    public function getChildDevelopmentVaccinating($child_parent_id) {
        global $db;

        // Lấy sinh nhật của trẻ
        $strSql = sprintf("SELECT birthday FROM ci_child_parent WHERE child_parent_id = %s", secure($child_parent_id));

        $get_birthday = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $birthday = null;
        if($get_birthday->num_rows >0) {
            $birthday = $get_birthday->fetch_assoc()['birthday'];
        }

        // Lấy những lần tiêm phòng trẻ sắp đến ngày tiêm
        $strSql = sprintf("SELECT * FROM ci_child_development WHERE (DATEDIFF(CURRENT_DATE(), %s) <= (((month_push - 1) * 30) + day_push)) AND type = 1 ORDER BY month_push ASC, day_push ASC, child_development_id ASC", secure($birthday));

        $get_child_development = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $child_development = array();
        if($get_child_development->num_rows > 0) {
            while ($childDeve = $get_child_development->fetch_assoc()) {
                // Kiểm tra xem trẻ đã được tiêm rồi hay chưa
                $strSql = sprintf("SELECT * FROM ci_child_development_child WHERE child_parent_id = %s AND child_development_id = %s", secure($child_parent_id, 'int'), secure($childDeve['child_development_id'], 'int'));

                $get_vaccinationed = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
                if($get_vaccinationed->num_rows > 0) {
                    $childDeve['vaccinationed'] = 1;
                } else {
                    $childDeve['vaccinationed'] = 0;
                }

                $countDay = ($childDeve['month_push'] - 1) * 30 + $childDeve['day_push'];
                $time = date('Y-m-d', strtotime('+ ' . $countDay . ' days', strtotime($birthday)));
                $time = toSysDate($time);
                $childDeve['time'] = $time;

                $child_development[] = $childDeve;
            }
        }

        return $child_development;
    }

    /**
     * Lấy danh sách những lần sắp được tiêm trong tương lai
     *
     * @param $child_parent_id
     * @return array
     */
    public function getChildDevelopmentInfoFuture($child_parent_id) {
        global $db;

        // Lấy sinh nhật của trẻ
        $strSql = sprintf("SELECT birthday FROM ci_child_parent WHERE child_parent_id = %s", secure($child_parent_id));

        $get_birthday = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $birthday = null;
        if($get_birthday->num_rows >0) {
            $birthday = $get_birthday->fetch_assoc()['birthday'];
        }

        // Lấy những lần tiêm phòng trẻ sắp đến ngày tiêm
        $strSql = sprintf("SELECT * FROM ci_child_development WHERE (DATEDIFF(CURRENT_DATE(), %s) <= (((month_push - 1) * 30) + day_push)) AND type = 2 ORDER BY month_push ASC, day_push ASC, child_development_id ASC", secure($birthday));

        $get_child_development = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $child_development = array();
        if($get_child_development->num_rows > 0) {
            while ($childDeve = $get_child_development->fetch_assoc()) {
                $countDay = ($childDeve['month_push'] - 1) * 30 + $childDeve['day_push'];
                $time = date('Y-m-d', strtotime('+ ' . $countDay . ' days', strtotime($birthday)));
                $time = toSysDate($time);
                $childDeve['time'] = $time;

                $child_development[] = $childDeve;
            }
        }

        return $child_development;
    }

    /**
     * Lấy 1 lần sắp được tiêm trong tương lai và thông tin quan tâm
     *
     * @param $child_parent_id
     * @return array
     */
    public function getChildDevelopmentInfo($child_parent_id) {
        global $db;

        // Lấy sinh nhật của trẻ
        $strSql = sprintf("SELECT birthday FROM ci_child_parent WHERE child_parent_id = %s", secure($child_parent_id));

        $get_birthday = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $birthday = null;
        if($get_birthday->num_rows <= 0) {
            _error(404);
        }
        $birthday = $get_birthday->fetch_assoc()['birthday'];


        $result = array(
            'vaccinating' => null,
            'interest' => []
        );

        // Lấy 1 lần tiêm phòng trẻ sắp đến ngày tiêm
        $strSql = sprintf("SELECT * FROM ci_child_development WHERE (DATEDIFF(CURRENT_DATE(), %s) <= (((month_push - 1) * 30) + day_push)) AND type = 1 ORDER BY month_push ASC, day_push ASC, child_development_id ASC LIMIT 1", secure($birthday));

        $get_child_development = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $child_development = array();
        if($get_child_development->num_rows > 0) {
            $childDeve = $get_child_development->fetch_assoc();
            // Kiểm tra xem trẻ đã được tiêm rồi hay chưa
            $strSql = sprintf("SELECT * FROM ci_child_development_child WHERE child_parent_id = %s AND child_development_id = %s", secure($child_parent_id, 'int'), secure($childDeve['child_development_id'], 'int'));

            $get_vaccinationed = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
            if ($get_vaccinationed->num_rows > 0) {
                $childDeve['vaccinationed'] = 1;
            } else {
                $childDeve['vaccinationed'] = 0;
            }
            $result['vaccinating'] = $childDeve;
        }

        // Lấy thông tin quan tâm
        $strSql = sprintf('SELECT * FROM ci_child_development WHERE ( DATEDIFF(CURRENT_DATE(), %1$s) <= (((month_push - 1) * 30) + day_push + 20) AND DATEDIFF(CURRENT_DATE(), %1$s) >= (((month_push - 1) * 30) + day_push - 10) ) AND type = 2 ORDER BY month_push ASC, day_push ASC, child_development_id ASC', secure($birthday));

        $get_child_interest = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_child_interest->num_rows > 0) {
            while ($child_interest = $get_child_interest->fetch_assoc()) {
                $result['interest'][] = $child_interest;
            }
        }

        return $result;
    }

    /**
     * Lấy danh sách những lần tiêm phòng đã qua ngày tiêm
     *
     * @param $child_parent_id
     * @return array
     */
    public function getChildDevelopmentVaccinationHistory($child_parent_id) {
        global $db;

        // Lấy sinh nhật của trẻ
        $strSql = sprintf("SELECT birthday FROM ci_child_parent WHERE child_parent_id = %s", secure($child_parent_id));

        $get_birthday = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $birthday = null;
        if($get_birthday->num_rows >0) {
            $birthday = $get_birthday->fetch_assoc()['birthday'];
        }

        // Lấy những lần tiêm phòng trẻ đã qua ngày tiêm
        $strSql = sprintf("SELECT * FROM ci_child_development WHERE (DATEDIFF(CURRENT_DATE(), %s) > (((month_push - 1) * 30) + day_push)) AND type = 1 ORDER BY month_push DESC, day_push DESC, child_development_id DESC", secure($birthday));

        $get_child_development = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $child_development = array();
        if($get_child_development->num_rows > 0) {
            while ($childDeve = $get_child_development->fetch_assoc()) {
                // Kiểm tra xem trẻ đã được tiêm rồi hay chưa
                $strSql = sprintf("SELECT * FROM ci_child_development_child WHERE child_parent_id = %s AND child_development_id = %s", secure($child_parent_id, 'int'), secure($childDeve['child_development_id'], 'int'));

                $get_vaccinationed = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
                if($get_vaccinationed->num_rows > 0) {
                    $childDeve['vaccinationed'] = 1;
                } else {
                    $childDeve['vaccinationed'] = 0;
                }

                // Lấy ngày tiêm
                $countDay = ($childDeve['month_push'] - 1) * 30 + $childDeve['day_push'];
                $time = date('Y-m-d', strtotime('+ ' . $countDay . ' days', strtotime($birthday)));
                $time = toSysDate($time);
                $childDeve['time'] = $time;

                $child_development[] = $childDeve;
            }
        }
        $results = array();
        foreach ($child_development as $row) {
            if ($row['vaccinationed'] == 0) {
                $results[] = $row;
            }
        }

        return $results;
    }

    /**
     * Lấy danh sách những thông tin tham khảo trong quá khứ
     *
     * @param $child_parent_id
     * @return array
     */
    public function getChildDevelopmentInfoHistory($child_parent_id) {
        global $db;

        // Lấy sinh nhật của trẻ
        $strSql = sprintf("SELECT birthday FROM ci_child_parent WHERE child_parent_id = %s", secure($child_parent_id));

        $get_birthday = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $birthday = null;
        if($get_birthday->num_rows >0) {
            $birthday = $get_birthday->fetch_assoc()['birthday'];
        }

        // Lấy những lần tiêm phòng trẻ đã qua ngày tiêm
        $strSql = sprintf("SELECT * FROM ci_child_development WHERE (DATEDIFF(CURRENT_DATE(), %s) > (((month_push - 1) * 30) + day_push)) AND type = 2 ORDER BY month_push DESC, day_push DESC, child_development_id DESC", secure($birthday));

        $get_child_development = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $child_development = array();
        if($get_child_development->num_rows > 0) {
            while ($childDeve = $get_child_development->fetch_assoc()) {
                $countDay = ($childDeve['month_push'] - 1) * 30 + $childDeve['day_push'];
                $time = date('Y-m-d', strtotime('+ ' . $countDay . ' days', strtotime($birthday)));
                $time = toSysDate($time);
                $childDeve['time'] = $time;

                $child_development[] = $childDeve;
            }
        }

        return $child_development;
    }

    /**
     * Lấy danh sách quá trình phát triển theo type
     *
     * @param $type
     * @return array
     * @throws Exception
     */
    public function getChildDevelopmentByType($type) {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_child_development WHERE type = %s ORDER BY month_push ASC, day_push ASC, child_development_id ASC", secure($type));
        $get_child_development = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $child_development = array();
        if($get_child_development->num_rows > 0) {
            while ($childDeve = $get_child_development->fetch_assoc()) {
                $child_development[] = $childDeve;
            }
        }
        return $child_development;
    }
}