<?php
/**
 * DAO -> ParentDAO
 *
 * @package ConIu
 * @author QuanND
 */

class ParentDAO {
    /**
     * __construct
     *
     */
    public function __construct() {
    }

    /**
     * Lấy ra danh sách các ID của cha mẹ đứa trẻ (bao gồm cả người ta ra thông tin của nó).
     *
     * @param $childId
     * @return array
     * @throws Exception
     */
    public function getParentIds($childId) {
        global $db;

        $strSql = "SELECT user_id FROM ci_user_manage WHERE object_type = %s AND object_id = %s";

        $parentIds = array();
        $get_parent = $db->query(sprintf($strSql, MANAGE_CHILD, secure($childId, 'int'))) or _error(SQL_ERROR_THROWEN);
        if($get_parent->num_rows > 0) {
            while($parent = $get_parent->fetch_assoc()) {
                $parentIds[] = $parent['user_id'];
            }
        }
        return $parentIds;
    }

    /**
     * Lấy ra danh sách các ID của cha mẹ đứa trẻ (bao gồm cả người ta ra thông tin của nó, lấy thông tin từ bảng ci_child_parent).
     *
     * @param $child_parent_id
     * @return array
     * @throws Exception
     */
    public function getParentIdsByParent($childParentId) {
        global $db;

        $strSql = "SELECT user_id FROM ci_parent_manage WHERE child_parent_id = %s";

        $parentIds = array();
        $get_parent = $db->query(sprintf($strSql, secure($childParentId, 'int'))) or _error(SQL_ERROR_THROWEN);
        if($get_parent->num_rows > 0) {
            while($parent = $get_parent->fetch_assoc()) {
                $parentIds[] = $parent['user_id'];
            }
        }
        return $parentIds;
    }
    /**
     * Thêm trẻ vào danh sách tự quản lý bản thân mình
     *
     * @param $child_id
     * @param $user_id
     * @throws Exception
     */
    public function addItSelf($child_id,$user_id) {
        global $db;

        //Câu sql sẽ có dạng: INSERT INTO tbl_name (col_A,col_B,col_C) VALUES (1,2,3), (4,5,6), (7,8,9)
        //Build lên phần giá trị truyền vào.
        $strValues = "";
        $strValues .= "(". secure($child_id, 'int') .",". secure($user_id, 'int') .",". secure(MANAGE_ITSELF, 'int') .",". secure(STATUS_ACTIVE, 'int') .",". secure(PERMISSION_ALL, 'int') ."),";

        $strValues = trim($strValues, ",");

        $strSql = "INSERT INTO ci_user_manage (object_id, user_id, object_type, status, role_id) VALUES ".$strValues;
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }
    /**
     * Thêm danh sách phụ huynh của trẻ
     *
     * @param $child_id
     * @param $parentIds
     * @throws Exception
     */
    public function addParentList($child_id, $parentIds) {
        global $db;

        //Câu sql sẽ có dạng: INSERT INTO tbl_name (col_A,col_B,col_C) VALUES (1,2,3), (4,5,6), (7,8,9)
        //Build lên phần giá trị truyền vào.
        $strValues = "";
        $parentIds = array_unique($parentIds); //Bỏ đi giá trị bị lặp
        foreach ($parentIds as $id) {
            //object_id, user_id, object_type, status, role_id
            $strValues .= "(". secure($child_id, 'int') .",". secure($id, 'int') .",". secure(MANAGE_CHILD, 'int') .",". secure(STATUS_ACTIVE, 'int') .",". secure(PERMISSION_ALL, 'int') ."),";
        }
        $strValues = trim($strValues, ",");

        $strSql = "INSERT INTO ci_user_manage (object_id, user_id, object_type, status, role_id) VALUES ".$strValues;
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Thêm danh sách phụ huynh của trẻ bên phía phụ huynh (bảng ci_parent_manage)
     *
     * @param $child_parent_id
     * @param $parentIds
     * @throws Exception
     */
    public function addParentListForParent($child_parent_id, $parentIds) {
        global $db;

        //Câu sql sẽ có dạng: INSERT INTO tbl_name (col_A,col_B,col_C) VALUES (1,2,3), (4,5,6), (7,8,9)
        //Build lên phần giá trị truyền vào.
        $strValues = "";
        $parentIds = array_unique($parentIds); //Bỏ đi giá trị bị lặp
        foreach ($parentIds as $id) {
            //child_parent_id, user_id
            $strValues .= "(". secure($child_parent_id, 'int') .",". secure($id, 'int') ."),";
        }
        $strValues = trim($strValues, ",");

        $strSql = "INSERT INTO ci_parent_manage (child_parent_id, user_id) VALUES ".$strValues;
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Xóa danh sách phụ huynh của trẻ
     *
     * @param $child_id
     * @param $parentIds
     * @throws Exception
     */
    public function deleteParentList($child_id, $parentIds) {
        global $db;

        $strCon = implode(',', $parentIds);
        $strSql = sprintf("DELETE FROM ci_user_manage WHERE object_type = %s AND object_id = %s AND user_id IN (%s)", MANAGE_CHILD, secure($child_id, 'int'), $strCon);
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Xóa danh sách phụ huynh của trẻ trong bảng ci_parent_manage
     *
     * @param $child_parent_id
     * @param $parentIds
     * @throws Exception
     */
    public function deleteParentListByParent($child_parent_id, $parentIds) {
        global $db;

        $strCon = implode(',', $parentIds);
        $strSql = sprintf("DELETE FROM ci_parent_manage WHERE child_parent_id = %s AND user_id IN (%s)", secure($child_parent_id, 'int'), $strCon);
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Lấy ra danh sách cha mẹ của trẻ.
     *
     * @param $childId
     * @return array
     * @throws Exception
     */
    public function getParent($childId)
    {
        global $db;
        $results = array();
        $strSql = sprintf('SELECT user_id, user_name, user_fullname, user_gender, user_picture FROM users WHERE user_id IN
                    (SELECT user_id FROM ci_user_manage WHERE object_type = %s AND object_id = %s)', MANAGE_CHILD, secure($childId, 'int'));
        $get_users = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_users->num_rows > 0) {
            while ($user = $get_users->fetch_assoc()) {
                $user['user_picture'] = get_picture($user['user_picture'], $user['user_gender']);
                $results[] = $user;
            }
        }

        return $results;
    }

    /**
     * Lấy ra danh sách phụ huynh học sinh của một lớp phục vụ cho nhân viên Noga.
     *
     * @param $classId
     * @return array
     * @throws Exception
     */
    public function getParentOfClassForNoga($classId) {
        global $db;
        $results = array();
        $strSql = sprintf('SELECT U.user_id, U.user_name, U.user_fullname, U.user_gender, U.user_picture, U.user_last_login, U.user_phone, U.user_email FROM users U WHERE U.user_id IN
                    (SELECT UM.user_id FROM ci_user_manage UM INNER JOIN ci_class_child CC ON UM.object_type = %s AND UM.object_id = CC.child_id
                      AND CC.class_id = %s AND CC.status = %s)', MANAGE_CHILD, secure($classId, 'int'), secure(STATUS_ACTIVE, 'int'));
        $get_users = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_users->num_rows > 0) {
            while ($user = $get_users->fetch_assoc()) {
                $user['user_picture'] = get_picture($user['user_picture'], $user['user_gender']);
                $results[] = $user;
            }
        }

        return $results;
    }

    /**
     * Lấy ra danh sách ID của phụ huynh một lớp
     *
     * @param $classId
     * @return array
     * @throws Exception
     */
    public function getParentIdOfClass($classId) {
        global $db;
        $results = array();
        $strSql = sprintf('SELECT UM.user_id FROM ci_user_manage UM INNER JOIN ci_class_child CC ON UM.object_type = %s
                            AND UM.object_id = CC.child_id AND CC.class_id = %s AND CC.status = %s', MANAGE_CHILD, secure($classId, 'int'), secure(STATUS_ACTIVE, 'int'));
        $get_users = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_users->num_rows > 0) {
            while ($user = $get_users->fetch_assoc()) {
                $results[] = $user['user_id'];
            }
        }

        return $results;
    }

    /**
     * Lấy danh sách phụ huynh của một lớp
     *
     * @param $classId
     * @return array
     * @throws Exception
     */
    public function getParentsOfClass($classId) {
        global $db;
        $results = array();
        $strSql = sprintf('SELECT U.* FROM users U INNER JOIN ci_user_manage UM ON UM.user_id = U.user_id
                            INNER JOIN ci_class_child CC ON UM.object_type = %s
                            AND UM.object_id = CC.child_id AND CC.class_id = %s AND CC.status = %s', MANAGE_CHILD, secure($classId, 'int'), secure(STATUS_ACTIVE, 'int'));
        $get_users = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_users->num_rows > 0) {
            while ($user = $get_users->fetch_assoc()) {
                $results[] = $user;
            }
        }

        return $results;
    }

    /**
     * Lấy ra danh sách phụ huynh của một trường phục vụ cho NOGA
     *
     * @param $schoolId
     * @return array
     * @throws Exception
     */
    public function getParentOfSchoolForNoga($schoolId) {
        global $db;
        $results = array();
        $strSql = sprintf('SELECT U.user_id, U.user_name, U.user_fullname, U.user_gender, U.user_picture, U.user_phone, U.user_email, U.user_last_login FROM users U WHERE U.user_id IN
                    (SELECT UM.user_id FROM ci_user_manage UM INNER JOIN ci_school_child SC ON UM.object_type = %s AND UM.object_id = SC.child_id
                      AND SC.school_id = %s AND SC.status = %s)', MANAGE_CHILD, secure($schoolId, 'int'), secure(STATUS_ACTIVE, 'int'));
        $get_users = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_users->num_rows > 0) {
            while ($user = $get_users->fetch_assoc()) {
                $user['user_picture'] = get_picture($user['user_picture'], $user['user_gender']);
                $results[] = $user;
            }
        }

        return $results;
    }


    /**
     * Lấy ra danh sách ID cha mẹ các bé của một trường
     *
     * @param $schoolId
     * @return array
     * @throws Exception
     */
    public function getParentIdOfSchool($schoolId) {
        global $db;
        $results = array();
        $strSql = sprintf('SELECT UM.user_id FROM ci_user_manage UM INNER JOIN ci_school_child SC ON UM.object_type = %s AND UM.object_id = SC.child_id
                      AND SC.school_id = %s AND SC.status = %s', MANAGE_CHILD, secure($schoolId, 'int'), secure(STATUS_ACTIVE, 'int'));
        $get_users = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_users->num_rows > 0) {
            while ($user = $get_users->fetch_assoc()) {
                $results[] = $user['user_id'];
            }
        }

        return $results;
    }

    /**
     * Lấy ra danh sách cha mẹ các bé của một trường
     *
     * @param $schoolId
     * @return array
     * @throws Exception
     */
    public function getParentsOfSchool($schoolId) {
        global $db;
        $results = array();
        $strSql = sprintf('SELECT U.user_fullname, U.user_birthdate FROM users U INNER JOIN  ci_user_manage UM ON U.user_id = UM.user_id INNER JOIN ci_school_child SC ON UM.object_type = %s AND UM.object_id = SC.child_id
                      AND SC.school_id = %s AND SC.status = %s', MANAGE_CHILD, secure($schoolId, 'int'), secure(STATUS_ACTIVE, 'int'));
        $get_users = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_users->num_rows > 0) {
            while ($user = $get_users->fetch_assoc()) {
                $results[] = $user;
            }
        }

        return $results;
    }

    /**
     * Lấy danh sách phụ huynh của trường
     *
     * @param $schoolId
     * @return array
     * @throws Exception
     */
    public function getParentsBirthday($schoolId, $begin, $end, $class_id) {
        global $db;

        $begin = toDBDate($begin);
        $end = toDBDate($end);
        if ($class_id == 0) {
//            $strSql = sprintf('SELECT U.user_id, U.user_fullname, U.user_name, U.user_birthdate, U.user_phone, U.user_email FROM users U INNER JOIN ci_user_manage UM ON UM.user_id = U.user_id
//                      INNER JOIN ci_school_child SC ON UM.object_type = %s AND UM.object_id = SC.child_id
//                      AND SC.school_id = %s AND SC.status = %s
//                      WHERE 1 = (FLOOR(DATEDIFF(DATE_ADD(%s,INTERVAL (DATEDIFF(%s, %s)) DAY),U.user_birthdate) / 365.25)) - (FLOOR(DATEDIFF(%s,U.user_birthdate) / 365.25)) GROUP BY U.user_id
//ORDER BY MONTH(U.user_birthdate),DAY(U.user_birthdate)', MANAGE_CHILD, secure($schoolId, 'int'), secure(STATUS_ACTIVE, 'int'), secure($begin), secure($end), secure($begin), secure($begin));

            $strSql = "SELECT U.user_id, U.user_fullname, U.user_name, U.user_birthdate, U.user_phone, U.user_email FROM users U INNER JOIN ci_user_manage UM ON UM.user_id = U.user_id 
                      INNER JOIN ci_school_child SC ON UM.object_type = 1 AND UM.object_id = SC.child_id
                      AND SC.school_id = $schoolId AND SC.status = 1
              WHERE DATE_FORMAT(U.user_birthdate, '%m%d') BETWEEN DATE_FORMAT('$begin', '%m%d') AND DATE_FORMAT('$end', '%m%d') OR (MONTH('$begin') > MONTH('$end')
                AND (MONTH(U.user_birthdate) >= MONTH('$begin') OR MONTH(U.user_birthdate) <= MONTH('$end')))
              ORDER BY MONTH(U.user_birthdate),DAY(U.user_birthdate)";
        } else {
            $strSql = "SELECT U.* FROM users U INNER JOIN ci_user_manage UM ON UM.user_id = U.user_id
                            INNER JOIN ci_class_child CC ON UM.object_type = 1
                            AND UM.object_id = CC.child_id AND CC.class_id = $class_id AND CC.status = 1
                            WHERE DATE_FORMAT(U.user_birthdate, '%m%d') BETWEEN DATE_FORMAT('$begin', '%m%d') AND DATE_FORMAT('$end', '%m%d') OR (MONTH('$begin') > MONTH('$end')
                AND (MONTH(U.user_birthdate) >= MONTH('$begin') OR MONTH(U.user_birthdate) <= MONTH('$end')))
              ORDER BY MONTH(U.user_birthdate),DAY(U.user_birthdate)";
        }

        $results = array();

        $get_users = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_users->num_rows > 0) {
            while ($user = $get_users->fetch_assoc()) {
                $user['user_birthdate'] = toSysDate($user['user_birthdate']);
                $results[] = $user;
            }
        }

        return $results;
    }

    /**
     * Lấy ra danh sách CHILD được quản lý bởi một user
     *
     * @param $user_id
     * @return array
     * @throws Exception
     */
    private function getChildren($user_id) {
        global $db;

        $strSql = "SELECT C.*, SC.class_id, SC.school_id, P.page_name, P.page_title, G.group_name, G.group_title FROM ci_child C
                  INNER JOIN ci_user_manage UM ON C.child_id = UM.object_id AND UM.object_type = %s AND UM.user_id = %s
                  INNER JOIN ci_school_child SC ON SC.child_id = C.child_id
                  INNER JOIN pages P ON P.page_id = SC.school_id
                  INNER JOIN groups  G ON G.group_id = SC.class_id
                  ORDER BY name_for_sort ASC";

        $children = array();
        $get_children = $db->query(sprintf($strSql, MANAGE_CHILD, secure($user_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        if($get_children->num_rows > 0) {
            while($child = $get_children->fetch_assoc()) {
                $children[] = $child;
            }
        }
        return $children;
    }
    // Begin birthday
    /**
     * Lấy ra danh sách phụ huynh có sinh nhật trong tháng
     *
     * @param
     * @return array
     * @throws Exception
     */
    public function getParentBirthdayOfMonth() {
        global $db;
        $results = array();
        $strSql = sprintf("SELECT U.user_id, U.user_name, U.user_fullname, U.user_gender, U.user_phone, U.user_birthdate, user_email FROM users U WHERE
                            U.user_id IN (SELECT UM.user_id FROM ci_user_manage UM INNER JOIN ci_school_child SC ON UM.object_type = %s AND UM.object_id = SC.child_id)
                            AND 1 = (FLOOR(DATEDIFF(DATE_ADD(DATE(NOW()),INTERVAL 30 DAY),U.user_birthdate) / 365.25)) - (FLOOR(DATEDIFF(DATE(NOW()),U.user_birthdate) / 365.25))
                            ORDER BY MONTH(U.user_birthdate),DAY(U.user_birthdate)", MANAGE_CHILD);
        $get_users = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_users->num_rows > 0) {
            while ($user = $get_users->fetch_assoc()) {
                $this->insertParentBirthday($user);
                // Lấy thông tin trẻ
                $user['child'] = $this->getChildren($user['user_id']);
                foreach ($user['child'] as $rows) {
                    $this->insertChildOfParentBirthday($user['user_id'], $rows);
                }
                $results[] = $user;
            }
        }

        return $results;
    }

    /**
     * Insert phụ huynh có sinh nhật trong tháng vào ci_user_birthday
     *
     * @param array $results
     * @throws Exception
     */
    private function insertParentBirthday($results = array()) {
        global $db;

        $strSql = sprintf("INSERT INTO ci_user_birthday (user_id, name, fullname, gender, birthday, phone, email, type) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)",
            secure($results['user_id'], 'int'), secure($results['user_name']), secure($results['user_fullname']), secure($results['user_gender']), secure($results['user_birthdate']),
            secure($results['user_phone']), secure($results['user_email']), 2);

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Insert trẻ có phụ huynh sinh nhật trong tháng vào ci_child_of_parent_birthday
     *
     * @param $userId
     * @param array $args
     * @throws Exception
     */
    private function insertChildOfParentBirthday($userId, $args = array()) {
        global $db;
        $strSql = sprintf("INSERT INTO ci_child_of_parent_birthday (parent_id, child_id, child_name, child_class_id, child_class_name, child_class_title, 
                            child_school_id, child_school_name, child_school_title, child_gender) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
                            secure($userId, 'int'), secure($args['child_id'], 'int'), secure($args['child_name']), secure($args['class_id']), secure($args['group_name']),
                            secure($args['group_title']), secure($args['school_id']), secure($args['page_name']), secure($args['page_title']), secure($args['gender']));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }
}
?>