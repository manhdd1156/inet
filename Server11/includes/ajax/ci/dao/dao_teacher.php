<?php
/**
 * DAO -> Teacher
 * Thao tác với bảng teacher
 * 
 * @package ConIu
 * @author QuanND
 */

class TeacherDAO {
    /**
     * __construct
     *
     */
    public function __construct() {
    }

    /**
     * Lây ra danh sách các giáo viên hiện có của một trường
     *
     * @return array
     */
    public function getActiveTeachers($school_id = 0) {
        global $db;

        $strSql = sprintf("SELECT U.user_id, U.user_name, U.user_phone, U.created_id, U.user_email, U.user_fullname, U.user_gender, U.user_birthdate
            FROM users U INNER JOIN ci_teacher T ON U.user_id = T.user_id
            WHERE T.status = %s AND T.school_id = %s ORDER BY U.user_firstname ASC", STATUS_ACTIVE, secure($school_id, 'int'));
        $teachers = array();
        $get_teachers = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_teachers->num_rows > 0) {
            while($teacher = $get_teachers->fetch_assoc()) {
                $teachers[] = $teacher;
            }
        }
        return $teachers;
    }

    /**
     * Lấy ra danh sách tất cả giáo viên của một trường, bao gồm cả giáo viên đã nghỉ việc.
     *
     * @param int $school_id
     * @return array
     * @throws Exception
     */
    public function getAllTeachers($school_id = 0) {
        global $db, $user;

        $strSql = sprintf("SELECT U.user_id, U.user_name, U.user_phone, U.created_id, U.user_email, U.user_fullname, U.user_gender, U.user_birthdate
            FROM users U INNER JOIN ci_teacher T ON U.user_id = T.user_id
            WHERE T.school_id = %s ORDER BY T.status DESC, U.user_firstname ASC", secure($school_id, 'int'));
        $teachers = array();
        $get_teachers = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_teachers->num_rows > 0) {
            while($teacher = $get_teachers->fetch_assoc()) {
                $teacher['classes'] = $this->getClasses($teacher['user_id'], $school_id);
                $teachers[] = $teacher;
            }
        }
        return $teachers;
    }

    /**
     * Lấy danh sách giáo viên có sinh nhật từ ngày đến ngày
     *
     * @param $schoolId
     * @param $begin
     * @param $end
     * @return array
     * @throws Exception
     */
    public function getTeachersBirthday($schoolId, $begin, $end) {
        global $db;

        $begin = toDBDate($begin);
        $end = toDBDate($end);

        $strSql = "SELECT U.user_id, U.user_fullname, U.user_name, U.user_birthdate, U.user_phone, U.user_email FROM users U 
                      INNER JOIN ci_teacher T ON T.user_id = U.user_id AND T.school_id = $schoolId
                      WHERE DATE_FORMAT(U.user_birthdate, '%m%d') BETWEEN DATE_FORMAT('$begin', '%m%d') AND DATE_FORMAT('$end', '%m%d') OR (MONTH('$begin') > MONTH('$end')
                AND (MONTH(U.user_birthdate) >= MONTH('$begin') OR MONTH(U.user_birthdate) <= MONTH('$end')))
              ORDER BY MONTH(U.user_birthdate),DAY(U.user_birthdate)";
        $results = array();

        $get_users = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_users->num_rows > 0) {
            while ($user = $get_users->fetch_assoc()) {
                $user['user_birthdate'] = toSysDate($user['user_birthdate']);
                $results[] = $user;
            }
        }

        return $results;
    }

    /**
     * Lấy ra số lượng nhân viên một trường
     *
     * @param $school_id
     * @return int
     */
    public function getTeacherCnt($school_id) {
        global $db;

        $strSql = sprintf("SELECT count(user_id) AS cnt FROM ci_teacher WHERE school_id = %s", secure($school_id, 'int'));
        $get_cnt = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_cnt->num_rows > 0) {
            return $get_cnt->fetch_assoc()['cnt'];
        }
        return 0;
    }

    /**
     * Lấy ra số lượng giáo viên một trường
     *
     * @param $school_id
     * @return int
     */
    public function getTeacherCntClass($school_id) {
        global $db;

        $strSql = sprintf("SELECT count(UM.user_id) AS cnt FROM ci_user_manage UM 
            INNER JOIN groups G ON G.group_id = UM.object_id AND UM.object_type = MANAGE_CLASS
            INNER JOIN ci_class_level CL ON CL.class_level_id = UM.object_id
            WHERE CL.school_id = %s", secure($school_id, 'int'));
        $get_cnt = $db->query($strSql) or _error('error, $strSql');
        if($get_cnt->num_rows > 0) {
            return $get_cnt->fetch_assoc()['cnt'];
        }
        return 0;
    }

    /**
     * Lấy ra số lượng giáo viên một lớp
     *
     * @param $class_id
     * @return int
     */
    public function getTeacherCntOfClass($class_id) {
        global $db;

        $strSql = sprintf("SELECT count(user_id) AS cnt FROM ci_user_manage WHERE object_id = %s AND object_type = %s", secure($class_id, 'int'), MANAGE_CLASS);
        $get_cnt = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_cnt->num_rows > 0) {
            return $get_cnt->fetch_assoc()['cnt'];
        }
        return 0;
    }

    /**
     * Lấy ra thông tin của giáo viên
     *
     * @param $teacherId
     * @return array|null
     * @throws Exception
     */
    public function getTeacher($teacherId) {
        global $db;

        $strSql = sprintf("SELECT U.user_id, U.user_name, U.user_phone, U.created_id, U.user_email, U.user_fullname, U.user_gender, U.user_birthdate
            FROM users U WHERE U.user_id = %s", secure($teacherId, 'int'));
        $get_teacher = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_teacher->num_rows > 0) {
            return $get_teacher->fetch_assoc();
        }
        return null;
    }

    /**
     * Lấy ra danh sách lớp của một giáo viên phụ trách
     *
     * @param $user_id
     * @return array
     * @throws Exception
     */
    private function getClasses($user_id, $school_id) {
        global $db;
        //UPDATE START MANHDD 04/06/2021
//        $strSql = sprintf("SELECT G.*, UM.role_id, P.page_id FROM groups G INNER JOIN ci_user_manage UM ON G.group_id = UM.object_id
//                    INNER JOIN ci_class_level CL ON CL.class_level_id = G.class_level_id
//                    INNER JOIN pages P ON P.page_id = CL.school_id
//                    WHERE UM.object_type = %s AND UM.user_id = %s AND UM.role_id = %s ORDER BY G.group_title ASC",
//                    MANAGE_CLASS, secure($user_id, 'int'), PERMISSION_MANAGE, secure($school_id, 'int'));
        $strSql = sprintf("SELECT G.*, UM.role_id, P.page_id FROM groups G INNER JOIN ci_user_manage UM ON G.group_id = UM.object_id
                    INNER JOIN ci_class_level CL ON CL.class_level_id = G.class_level_id
                    INNER JOIN pages P ON P.page_id = CL.school_id
                    WHERE UM.object_type = %s AND UM.user_id = %s AND UM.role_id IN (%s,%s) ORDER BY G.group_title ASC",
            MANAGE_CLASS, secure($user_id, 'int'), PERMISSION_MANAGE, secure($school_id, 'int'));
//UPDATE END MANHDD 04/06/2021
        $classes = array();
        $get_classes = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_classes->num_rows > 0) {
            while($class = $get_classes->fetch_assoc()) {
                if($class['page_id'] == $school_id) {
                    $classes[] = $class;
                }
            }
        }
        return $classes;
    }

    /**
     * Thêm 1 người dùng thành giáo viên của trường
     *
     * @param $user_id
     * @param $school_id
     * @throws Exception
     */
    public function addTeacherToSchool ($school_id, $teacherIds) {
        global $db;

        $strValues = "";
        $teacherIds = array_unique($teacherIds); //Bỏ đi giá trị bị lặp
        foreach ($teacherIds as $id) {
            $strValues .= "(". secure($school_id, 'int') ."," . secure($id, 'int') .",". secure(STATUS_ACTIVE, 'int') ."),";
        }
        $strValues = trim($strValues, ",");

        if (!is_empty($strValues)) {
            $strSql = "INSERT INTO ci_teacher (school_id, user_id, status) VALUES ".$strValues;
            $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        }
    }

    /**
     * Lấy ra danh sách ID của tất cả giáo viên trong trường
     *
     * @param $schoolId
     * @return array
     * @throws Exception
     */
    public function getTeacherIDOfSchool($schoolId) {
        global $db;

        $teacherIds = array();
        $strSql = sprintf("SELECT user_id FROM ci_teacher WHERE school_id = %s", secure($schoolId, 'int'));

        $get_teachers = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_teachers->num_rows > 0) {
            while($teacher = $get_teachers->fetch_assoc()) {
                $teacherIds[] = $teacher['user_id'];
            }
        }
        return $teacherIds;
    }

    /**
     * @param $schoolId
     * @return array
     *
     * Lấy danh sách giáo viên có quản lý ít nhất 1 lớp trong trường
     */
    public function getTeacherIDClassOfSchool($schoolId) {
        global $db;

        $teacherIds = array();
        $strSql = sprintf("SELECT T.user_id FROM ci_teacher T 
                  INNER JOIN ci_user_manage UM ON UM.user_id = T.user_id AND UM.object_type = %s
                  WHERE T.school_id = %s", MANAGE_CLASS, secure($schoolId, 'int'));

        $get_teachers = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_teachers->num_rows > 0) {
            while($teacher = $get_teachers->fetch_assoc()) {
                $teacherIds[] = $teacher['user_id'];
            }
        }
        return $teacherIds;
    }

    /**
     * Tìm danh sách giáo viên của một trường theo username hoặc tên.
     *
     * @param $school_username
     * @param $query
     * @param $teacherIds: sẽ không đưa ra giáo viên có ID trong danh sách này
     * @return array
     * @throws Exception
     */
    public function searchTeacher($school_username, $query, $teacherIds)
    {
        global $db, $system;
        $results = array();

        $teacherIds = array_unique($teacherIds);
        $strCon = implode(',', $teacherIds);
        $strSql = sprintf('SELECT U.user_id, U.user_name, U.user_phone, U.created_id, U.user_email, U.user_fullname, U.user_gender, U.user_picture
                            FROM users U
                            INNER JOIN ci_teacher T ON U.user_id = T.user_id
                            INNER JOIN pages P ON T.school_id = P.page_id
                            WHERE (U.user_name LIKE %1$s OR U.user_email LIKE %1$s OR U.user_firstname LIKE %1$s) AND P.page_name = %2$s
                            AND U.user_id NOT IN (%3$s)
                            ORDER BY U.user_firstname ASC LIMIT %4$s',
                            secure($query, 'search'), secure($school_username), $strCon, secure($system['min_results'], 'int', false));
        $get_users = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_users->num_rows > 0) {
            while ($user = $get_users->fetch_assoc()) {
                $user['user_picture'] = get_picture($user['user_picture'], $user['user_gender']);
                /* get the connection between the viewer & the target */
                //$user['sort'] = $user['user_fullname'];
                //$user['type'] = 'user';
                $results[] = $user;
            }
        }

        return $results;
    }

    /**
     * Lấy ra danh sách user (teacher) quản lý một CLASS
     *
     * @param $class_id
     * @return array
     * @throws Exception
     */
    public function getTeacherOfClass($class_id) {
        global $db;
        $teachers = array();
        //UPDATE START MANHDD 04/06/2021
//        $strSql = sprintf("SELECT U.user_id, U.user_name, U.user_phone, U.created_id, U.user_email, U.user_fullname, U.user_gender, U.user_picture FROM users U
//              INNER JOIN ci_user_manage UM ON U.user_id = UM.user_id
//              WHERE UM.object_type = %s AND UM.object_id = %s AND UM.role_id = %s", MANAGE_CLASS, secure($class_id, 'int'), PERMISSION_MANAGE);

        $strSql = sprintf("SELECT U.user_id, U.user_name, U.user_phone, U.created_id, U.user_email, U.user_fullname, U.user_gender, U.user_picture FROM users U
              INNER JOIN ci_user_manage UM ON U.user_id = UM.user_id
              WHERE UM.object_type = %s AND UM.object_id = %s AND UM.role_id IN (%s,%s)", MANAGE_CLASS, secure($class_id, 'int'), PERMISSION_MANAGE,PERMISSION_ALL);
        //UPDATE END MANHDD 04/06/2021
        $get_teachers = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_teachers->num_rows > 0) {
            while($teacher = $get_teachers->fetch_assoc()) {
                $teacher['user_picture'] = get_picture($teacher['user_picture'], $teacher['user_gender']);
                $teachers[] = $teacher;
            }
        }
        return $teachers;
    }

    /**
     * Lấy ra ID của lớp và giáo viên theo ID của trường và ID của trẻ.
     *
     * @param $schoolId
     * @param $childId
     * @return array
     * @throws Exception
     */
    public function getTeacherIdClassBySchoolAndChild($schoolId, $childId) {
        global $db;
        $teachers = array();

        $strSql = sprintf("SELECT SC.class_id, G.group_name AS class_name, UM.user_id AS teacher_id FROM ci_user_manage UM
              INNER JOIN ci_school_child SC ON SC.class_id = UM.object_id AND SC.school_id = %s AND SC.child_id = %s
              INNER JOIN groups G ON SC.class_id = G.group_id
              WHERE UM.object_type = %s", secure($schoolId, 'int'), secure($childId, 'int'), MANAGE_CLASS);

        $get_teachers = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_teachers->num_rows > 0) {
            while($teacher = $get_teachers->fetch_assoc()) {
                $teachers[] = $teacher;
            }
        }
        return $teachers;
    }

    /**
     * Lấy ra danh sách user_id của giáo viên dạy một lớp
     *
     * @param $class_id
     * @return array
     * @throws Exception
     */
    public function getTeacherIDOfClass($class_id) {
        global $db;

        $teacherIds = array();
        $strSql = sprintf("SELECT user_id FROM ci_user_manage WHERE object_type = %s AND object_id = %s AND role_id = %s",
                            MANAGE_CLASS, secure($class_id, 'int'), PERMISSION_MANAGE);

        $get_teachers = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_teachers->num_rows > 0) {
            while($teacher = $get_teachers->fetch_assoc()) {
                $teacherIds[] = $teacher['user_id'];
            }
        }
        return $teacherIds;
    }

    /**
     * Nhập danh sách user thành giáo viên của lớp
     *
     * @param $class_id
     * @param $teacherIds
     * @throws Exception
     */
    public function insertTeacherList($class_id, $teacherIds) {
        global $db;

        //Câu sql sẽ có dạng: INSERT INTO tbl_name (col_A,col_B,col_C) VALUES (1,2,3), (4,5,6), (7,8,9)
        //Build lên phần giá trị truyền vào.
        $strValues = "";
        $teacherIds = array_unique($teacherIds); //Bỏ đi giá trị bị lặp
        foreach ($teacherIds as $id) {
            $strValues .= "(". secure($class_id, 'int') .",". secure($id, 'int') .",". secure(MANAGE_CLASS, 'int') .",". secure(STATUS_ACTIVE, 'int') .",". secure(PERMISSION_MANAGE, 'int') ."),";
        }
        $strValues = trim($strValues, ",");

        $strSql = "INSERT INTO ci_user_manage (object_id, user_id, object_type, status, role_id) VALUES ".$strValues;

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Nhập danh sách lớp của user
     *
     * @param $teacherId
     * @param $classIds
     * @throws Exception
     */
    public function insertClassListForTeacher($teacherId, $classIds) {
        global $db;

        //Câu sql sẽ có dạng: INSERT INTO tbl_name (col_A,col_B,col_C) VALUES (1,2,3), (4,5,6), (7,8,9)
        //Build lên phần giá trị truyền vào.
        $strValues = "";
        if(count($classIds) > 0) {
            $classIds = array_unique($classIds); //Bỏ đi giá trị bị lặp
            foreach ($classIds as $id) {
                $strValues .= "(". secure($id, 'int') .",". secure($teacherId, 'int') .",". secure(MANAGE_CLASS, 'int') .",". secure(STATUS_ACTIVE, 'int') .",". secure(PERMISSION_MANAGE, 'int') ."),";
            }
            $strValues = trim($strValues, ",");

            $strSql = "INSERT INTO ci_user_manage (object_id, user_id, object_type, status, role_id) VALUES ".$strValues;

            $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        }

    }
    // ADD START MANHDD 04/06/2021
    /**
     * action = becomeHomeRoom :  Cập nhật giáo viên thành giáo viên chủ nhiệm
     * action = removeHomeRoom : Xóa xóa viên chủ nhiệm => trở về giáo viên thường
     * @param $class_id
     * @param $teacherId
     * @param $action
     * @throws Exception
     */
    public function updateHomeroomTeacher($teacherId, $class_id, $action) {
        global $db;
        $strSql = "";
        if($action=='becomeHomeRoom') {
            $strSql = sprintf("UPDATE ci_user_manage SET role_id = %s WHERE user_id = %s AND object_id = %s", secure(PERMISSION_ALL, 'int'), secure($teacherId, 'int'),  secure($class_id, 'int'));

        }else if($action=='removeHomeRoom') {
            $strSql = sprintf("UPDATE ci_user_manage SET role_id = %s WHERE user_id = %s AND object_id = %s", secure(PERMISSION_MANAGE, 'int'), secure($teacherId, 'int'),  secure($class_id, 'int'));
        }
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }
    // ADD END MANHDD 04/06/2021
    /**
     * Xóa danh sách giáo viên ra khỏi lớp.
     *
     * @param $class_id
     * @param $teacherIds
     * @throws Exception
     */
    public function deleteTeacherList($class_id, $teacherIds) {
        global $db;

        $strCon = implode(',', $teacherIds);
        $strSql = sprintf("DELETE FROM ci_user_manage WHERE object_id = %s AND object_type = %s AND user_id IN (%s)", secure($class_id, 'int'), secure(MANAGE_CLASS, 'int'), $strCon);
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }
    /**
     * Kiểm tra xem có phải giáo viên chủ nhiệm không
     *
     * @param $class_id
     * @param $teacherId
     * @throws Exception
     */
    public function getHomeroomTeacher($class_id, $teacherId) {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_user_manage WHERE user_id = %s AND object_type = %s AND object_id = %s AND role_id = %s",
            secure($teacherId, 'int'),MANAGE_CLASS, secure($class_id, 'int'), PERMISSION_ALL);

        $get_teacher = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_teacher->num_rows > 0) {
          return $get_teacher->fetch_assoc();
        }
        return null;
    }
    /**
     * Xóa danh sách lớp khỏi giáo viên
     *
     * @param $teacher_id
     * @param $classIds
     * @throws Exception
     */
    public function deleteClassListForTeacher($teacher_id, $classIds) {
        global $db;

        $strCon = '';
        if(count($classIds) > 0) {
            $strCon = implode(',', $classIds);
            $strSql = sprintf("DELETE FROM ci_user_manage WHERE user_id = %s AND object_type = %s AND object_id IN (%s)", secure($teacher_id, 'int'), secure(MANAGE_CLASS. 'int'), $strCon);
            $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        }
    }
    /**
     * Xóa một giáo viên do trường tạo ra
     *
     * @param $schoolId
     * @param $teacherId
     */
    public function deleteTeacher($schoolId, $teacherId) {
        global $db;

        //Xóa thông tin giáo viên khỏi bảng user
        $strSql = sprintf("DELETE FROM users WHERE user_id = %s", secure($teacherId, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        //Xóa thông tin giáo viên khỏi trường
        $strSql = sprintf("DELETE FROM ci_teacher WHERE school_id = %s AND user_id = %s", secure($schoolId, 'int'), secure($teacherId, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        /* delete trong bảng followings - Taila */
        $db->query(sprintf("DELETE FROM followings WHERE user_id = %s OR following_id = %s", secure($teacherId, 'int'), secure($teacherId, 'int'))) or _error(SQL_ERROR_THROWEN);

        /* delete trong bảng friends - Taila */
        $db->query(sprintf("DELETE FROM friends WHERE user_one_id = %s OR user_two_id = %s", secure($teacherId, 'int'), secure($teacherId, 'int'))) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Loại một giáo viên ra khỏi trường
     *
     * @param $schoolId
     * @param $teacherId
     */
    public function unassignTeacher($schoolId, $teacherId) {
        global $db;

        //Xóa thông tin giáo viên khỏi trường
        $strSql = sprintf("DELETE FROM ci_teacher WHERE school_id = %s AND user_id = %s", secure($schoolId, 'int'), secure($teacherId, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    // Begin Birthday
    /**
     * Lấy tất cả giáo viên có sinh nhật trong tháng
     *
     * @param
     * @param $teacherId
     */

    function getTeacherBirthdayOfMonth() {
        global $db;

//        $strSql = sprintf("SELECT P.page_id, P.page_name, P.page_title, U.user_id, U.user_name, U.user_phone, U.created_id, U.user_email, U.user_fullname, U.user_gender, U.user_birthdate
//            FROM users U INNER JOIN ci_teacher T ON U.user_id = T.user_id
//            INNER JOIN pages P ON P.page_id = T.school_id
//            WHERE T.status = %s ORDER BY U.user_fullname ASC", STATUS_ACTIVE);

        $strSql = sprintf("SELECT P.page_id, P.page_name, P.page_title, U.user_id, U.user_name, U.user_phone, U.created_id, U.user_email, U.user_fullname, U.user_gender, U.user_birthdate FROM users U 
                    INNER JOIN ci_teacher T ON U.user_id = T.user_id 
                    INNER JOIN pages P ON P.page_id = T.school_id
                    WHERE T.status = %s AND 1 = (FLOOR(DATEDIFF(DATE_ADD(DATE(NOW()),INTERVAL 30 DAY),U.user_birthdate) / 365.25)) - (FLOOR(DATEDIFF(DATE(NOW()),U.user_birthdate) / 365.25))
ORDER BY MONTH(U.user_birthdate),DAY(U.user_birthdate)", STATUS_ACTIVE);

        $teachers = array();
        $get_teachers = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_teachers->num_rows > 0) {
            while($teacher = $get_teachers->fetch_assoc()) {
                $this->insertTeacherBirthday($teacher);

                // Lấy danh sách lớp của teacher
                $teacher['class'] = $this->getClasses($teacher['user_id']);
                foreach ($teacher['class'] as $rows) {
                    $this->insertClassOfTeacherBirthday($teacher['user_id'], $teacher['page_id'], $teacher['page_name'], $teacher['page_title'], $rows);
                }
                $teachers[] = $teacher;
            }
        }
        return $teachers;
    }

    /**
     * Insert giáo viên có sinh nhật trong tháng vào ci_user_birthday
     *
     * @param array
     * @return
     */
    private function insertTeacherBirthday($results = array()) {
        global $db;

        $strSql = sprintf("INSERT INTO ci_user_birthday (user_id, name, fullname, gender, birthday, phone, email, type) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)", secure($results['user_id'], 'int'), secure($results['user_name']), secure($results['user_fullname']), secure($results['user_gender']), secure($results['user_birthdate']), secure($results['user_phone']), secure($results['user_email']), 3);

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Insert thông tin lớp của trẻ có sinh nhật trong tháng vào ci_class_birthday
     * @param $childId
     * @param array
     * @return
     */
    private function insertClassOfTeacherBirthday($userId, $pageId, $pageName, $pageTitle, $args = array()) {
        global $db;
        $strSql = sprintf("INSERT INTO ci_class_birthday (user_id, class_id, class_name, class_title, school_id, school_name, school_title) VALUES (%s, 
%s, %s, %s, %s, %s, %s)", secure($userId), secure($args['group_id'], 'int'), secure($args['group_name']), secure($args['group_title']), secure($pageId), secure($pageName), secure($pageTitle));

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }


    /* ---------- TEACHER - MEMCACHED ---------- */

    /**
     * Lấy ra danh sách tất cả giáo viên của một trường, bao gồm cả giáo viên đã nghỉ việc.
     *
     * @param int $school_id
     * @return array
     * @throws Exception
     */
    public function getAllTeachers4Memcache($school_id = 0) {
        global $db;

        $strSql = sprintf("SELECT U.user_id, U.user_name, U.user_phone, U.created_id, U.user_email, U.user_fullname, U.user_gender, U.user_birthdate
            FROM users U INNER JOIN ci_teacher T ON U.user_id = T.user_id
            WHERE T.school_id = %s ORDER BY T.status DESC, U.user_firstname ASC", secure($school_id, 'int'));
        $teachers = array(
            'ids' => [],
            'lists' => []
        );
        $get_teachers = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_teachers->num_rows > 0) {
            while($teacher = $get_teachers->fetch_assoc()) {
                $teachers['lists'][$teacher['user_id']] = $teacher;
                $teachers['ids'][] = $teacher['user_id'];
            }
        }
        return $teachers;
    }

    /**
     * Lấy ra danh sách class được quản lý bởi một user
     *
     * @param $user_id
     * @return array
     * @throws Exception
     */
    public function getClasses4Memcache($user_id) {
        global $db;
        //UPDATE START MANHDD 04/06/2021
//        $strSql = sprintf("SELECT G.*, CL.school_id, UM.role_id FROM groups G
//                           INNER JOIN ci_user_manage UM ON G.group_id = UM.object_id
//                           INNER JOIN ci_class_level CL ON G.class_level_id = CL.class_level_id
//                    WHERE UM.object_type = %s AND UM.user_id = %s AND UM.role_id =%s ORDER BY G.group_title ASC",
//            MANAGE_CLASS, secure($user_id, 'int'), PERMISSION_MANAGE);
        $strSql = sprintf("SELECT G.*, CL.school_id, UM.role_id FROM groups G 
                           INNER JOIN ci_user_manage UM ON G.group_id = UM.object_id
                           INNER JOIN ci_class_level CL ON G.class_level_id = CL.class_level_id
                    WHERE UM.object_type = %s AND UM.user_id = %s AND UM.role_id IN (%s,%s) ORDER BY G.group_title ASC",
            MANAGE_CLASS, secure($user_id, 'int'), PERMISSION_MANAGE,PERMISSION_ALL);
        //UPDATE END MANHDD 04/06/2021
        $classes = array(
            'ids' => [],
            'lists' => []
        );
        $get_classes = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_classes->num_rows > 0) {
            while($class = $get_classes->fetch_assoc()) {
                $class['group_picture'] = get_picture($class['group_picture'], 'class');
                $class['group_cover'] = get_picture($class['group_cover'], 'class');
                $classes['ids'][] = $class['group_id'];
                $classes['lists'][$class['group_id']] = $class;
            }
        }
        return $classes;
    }

    /**
     * Lấy ra danh sách user (teacher) quản lý một CLASS
     *
     * @param $class_id
     * @return array
     * @throws Exception
     */
    public function getTeacherOfClass4Memcache($class_id) {
        global $db;
        $teachers = array(
            'ids' => [],
            'lists' => []
        );
//UPDATE START MANHDD 04/06/2021
//        $strSql = sprintf("SELECT U.user_id, U.user_name, U.user_phone, U.created_id, U.user_email, U.user_fullname, U.user_gender, U.user_picture FROM users U
//              INNER JOIN ci_user_manage UM ON U.user_id = UM.user_id
//              WHERE UM.object_type = %s AND UM.object_id = %s AND UM.role_id =%s", MANAGE_CLASS, secure($class_id, 'int'), PERMISSION_MANAGE);
        $strSql = sprintf("SELECT U.user_id, U.user_name, U.user_phone, U.created_id, U.user_email, U.user_fullname, U.user_gender, U.user_picture FROM users U
              INNER JOIN ci_user_manage UM ON U.user_id = UM.user_id
              WHERE UM.object_type = %s AND UM.object_id = %s AND UM.role_id IN (%s,%s)", MANAGE_CLASS, secure($class_id, 'int'), PERMISSION_MANAGE,PERMISSION_ALL);
//UPDATE END MANHDD 04/06/2021
        $get_teachers = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_teachers->num_rows > 0) {
            while($teacher = $get_teachers->fetch_assoc()) {
                $teacher['user_picture'] = get_picture($teacher['user_picture'], $teacher['user_gender']);
                $teachers['lists'][$teacher['user_id']] = $teacher;
                $teachers['ids'][] = $teacher['user_id'];
            }
        }
        return $teachers;
    }

    /**
     * Lấy ra thông tin của giáo viên
     *
     * @param $teacherId
     * @return array|null
     * @throws Exception
     */
    public function getTeacher4Memcache($teacherId) {
        global $db;

        $strSql = sprintf("SELECT U.user_id, U.user_name, U.user_phone_signin, U.user_phone, U.created_id, U.user_email, U.user_fullname, U.user_picture, U.user_gender, U.user_birthdate
                           FROM users U WHERE U.user_id = %s", secure($teacherId, 'int'));
        $get_teacher = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $teacher = null;
        if($get_teacher->num_rows > 0) {
            $teacher = $get_teacher->fetch_assoc();
            $teacher['user_picture'] = get_picture($teacher['user_picture'], $teacher['user_gender']);
        }
        return $teacher;
    }

    /**
     * Lấy danh sách ID trường mà user làm nhân viên
     *
     * @param $userId
     * @return array
     * @throws Exception
     */
    public function getSchoolIdOfUserId ($userId) {
        global $db;

        $strSql = sprintf("SELECT school_id FROM ci_teacher WHERE user_id = %s AND status = '1'", secure($userId));

        $get_school = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $schoolIds = array();

        if($get_school->num_rows > 0) {
            while($schoolId = $get_school->fetch_assoc()) {
                $schoolIds[] = $schoolId['school_id'];
            }
        }

        return $schoolIds;
    }
}
?>