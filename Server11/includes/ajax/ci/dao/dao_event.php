<?php
/**
 * DAO -> Event
 * Thao tác với bảng ci_event
 * 
 * @package ConIu
 * @author QuanND
 */

class EventDAO {
    /**
     * __construct
     *
     */
    public function __construct() {
    }

    /**
     * Đưa danh sách đối tượng vào danh sách đăng ký tham gia sự kiện
     *
     * @param $eventId
     * @param $ppType
     * @param $ppIds
     * @throws Exception
     */
    public function insertParticipants($eventId, $ppType, $ppIds) {
        global $db, $date, $user;

        // Loại đối tượng đã đăng ký tránh bị lỗi duplicate
        $ppIds = array_unique($ppIds); //Bỏ đi giá trị bị lặp
        $oldIds = $this->_getParticipantIds($eventId, $ppType, $ppIds);

        //Bỏ đi những người đã đăng ký
        $ppIds = array_diff($ppIds, $oldIds);
        if (count($ppIds) == 0) return;

        //1. Đưa các đối tượng vào danh sách đăng ký
        //Câu sql sẽ có dạng: INSERT INTO tbl_name (col_A,col_B,col_C) VALUES (1,2,3), (4,5,6), (7,8,9)
        //Build lên phần giá trị truyền vào.
        $strValues = "";
        foreach ($ppIds as $id) {
            //event_id, pp_type, pp_id, created_at, created_user_id
            $strValues .= "(". secure($eventId, 'int') .",". secure($ppType, 'int') .",". secure($id, 'int') .",". secure($date). ",". secure($user->_data['user_id'], 'int') ."),";
        }
        $strValues = trim($strValues, ",");

        $strSql = "INSERT INTO ci_event_participate (event_id, pp_type, pp_id, created_at, created_user_id) VALUES ".$strValues;
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        //2. Tăng số thành đối tượng tham gia
        if ($ppType == PARTICIPANT_TYPE_PARENT) {
            $strSql = "UPDATE ci_event SET parent_participants = parent_participants + %s WHERE event_id = %s";
            $db->query(sprintf($strSql, count($ppIds), $eventId)) or _error(SQL_ERROR_THROWEN);
        } else if ($ppType == PARTICIPANT_TYPE_CHILD) {
            $strSql = "UPDATE ci_event SET child_participants = child_participants + %s WHERE event_id = %s";
            $db->query(sprintf($strSql, count($ppIds), $eventId)) or _error(SQL_ERROR_THROWEN);
        } else {
            $strSql = "UPDATE ci_event SET teacher_participants = teacher_participants + %s WHERE event_id = %s";
            $db->query(sprintf($strSql, count($ppIds), $eventId)) or _error(SQL_ERROR_THROWEN);
        }
    }

    /**
     * Xóa đối tượng tham gia khỏi sự kiện
     *
     * @param $eventId
     * @param $ppType
     * @param $ppIds
     * @throws Exception
     */
    public function deleteParticipants($eventId, $ppType, $ppIds) {
        global $db;

        if (count($ppIds) == 0) return;

        $ppIds = array_unique($ppIds); //Bỏ đi giá trị bị lặp
        $strCon = implode(',', $ppIds);

        $strSql = sprintf("DELETE FROM ci_event_participate WHERE event_id = %s AND pp_type = %s AND pp_id IN (%s)", secure($eventId, 'int'), secure($ppType, 'int'), $strCon);
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        //2. Giảm số đối tượng tham gia sự kiện
        if ($ppType == PARTICIPANT_TYPE_PARENT) {
            $strSql = 'UPDATE ci_event SET parent_participants = IF(parent_participants < %1$s, 0, parent_participants - %1$s) WHERE event_id = %2$s';
        } else if ($ppType == PARTICIPANT_TYPE_CHILD) {
            $strSql = 'UPDATE ci_event SET child_participants = IF(child_participants < %1$s, 0, child_participants - %1$s) WHERE event_id = %2$s';
        } else {
            $strSql = 'UPDATE ci_event SET teacher_participants = IF(teacher_participants < %1$s, 0, teacher_participants - %1$s) WHERE event_id = %2$s';
        }
        $db->query(sprintf($strSql, count($ppIds), secure($eventId, 'int'))) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Lấy ra danh sách đối tượng đăng ký tham gia. Không lấy thêm thông tin gì khác
     *
     * @param $eventId
     * @return array
     * @throws Exception
     */
    public function getParticipants($eventId) {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_event_participate WHERE event_id = %s", secure($eventId, 'int'));

        $results = array();
        $get_pps = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_pps->num_rows > 0) {
            while($pp = $get_pps->fetch_assoc()) {
                $results[] = $pp;
            }
        }
        return $results;
    }

    /**
     * Lấy ra danh sách trẻ của lớp tham gia sự kiện. Trường hợp trẻ không tham gia cũng nằm trong danh sách trả về nhưng
     * các thông tin tham gia là rỗng.
     *
     * @param $class_id
     * @param $event_id
     * @param $parent_included
     * @param $childCount
     * @param $parentCount
     * @return array
     * @throws Exception
     */
    function getParticipantsOfClass($class_id, $event_id, $parent_included, &$childCount, &$parentCount) {
        global $db;

        $results = array();
        $strSql = sprintf('SELECT C.child_id, C.child_name, C.birthday, C.gender, C.parent_phone, EP.*, U.user_fullname AS created_fullname, CC.status AS child_status FROM ci_class_child CC                 
                    INNER JOIN ci_child C ON CC.child_id = C.child_id
                    INNER JOIN ci_event E ON E.event_id = %1$s AND (CC.begin_at <= E.created_at) AND ((CC.end_at IS NULL) OR (CC.end_at >= E.created_at))
                    LEFT JOIN ci_event_participate EP ON EP.pp_type = %2$s AND EP.event_id = %1$s AND CC.child_id = EP.pp_id
                    LEFT JOIN users U ON U.user_id = EP.created_user_id
                    WHERE CC.class_id = %3$s ORDER BY C.name_for_sort ASC', secure($event_id, 'int'), PARTICIPANT_TYPE_CHILD, secure($class_id, 'int'));

        $get_children = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_children->num_rows > 0) {
            $parentIds = array();
            while ($child = $get_children->fetch_assoc()) {
                if ($child['event_id'] > 0) {
                    $childCount++; //Đếm số trẻ tham gia
                }
                if ($parent_included) {
                    $child['parent'] = $this->_getParentParticipantsOfChild($child['child_id'], $event_id);
                    //Lọc ra số cha mẹ tham gia
                    foreach($child['parent'] as $user) {
                        if ($user['event_id'] > 0) {
                            $parentIds[] = $user['user_id'];
                        }
                    }
                }
                $child['birthday'] = toSysDate($child['birthday']);
                $results[] = $child;
            }

            $parentCount = count(array_unique($parentIds));
        }

        return $results;
    }

    /**
     * Lấy ra danh sách giáo viên đăng ký events
     *
     * @param $schoolId
     * @param $event_id
     * @return array
     * @throws Exception
     */
    function getTeacherParticipants($schoolId, $event_id) {
        global $db;

        $results = array();
        $strSql = sprintf("SELECT U.user_id, U.user_fullname, EP.* FROM ci_teacher T
                    INNER JOIN users U ON T.user_id = U.user_id
                    LEFT JOIN ci_event_participate EP ON T.user_id = EP.pp_id AND EP.pp_type = %s AND EP.event_id = %s
                    WHERE T.school_id = %s AND T.status = 1 ORDER BY U.user_firstname ASC", PARTICIPANT_TYPE_TEACHER, secure($event_id, 'int'), secure($schoolId, 'int'));

        $get_teachers = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_teachers->num_rows > 0) {
            while ($teacher = $get_teachers->fetch_assoc()) {
                $teacher['created_at'] = toSysDatetime($teacher['created_at']);
                $results[] = $teacher;
            }
        }

        return $results;
    }

    /**
     * Lấy ra danh sách ID trẻ của lớp tham gia sự kiện.
     *
     * @param $class_id
     * @param $event_id
     * @return array
     * @throws Exception
     * @internal param $parent_included
     * @internal param $childCount
     * @internal param $parentCount
     */
    function getParticipantsIdOfClass($class_id, $event_id, $parent_included) {
        global $db;

        $results = array();
        $strSql = sprintf("SELECT C.child_id, C.child_name, C.birthday, C.gender, C.parent_phone, EP.*, U.user_fullname AS created_fullname FROM ci_class_child CC
                    INNER JOIN ci_child C ON CC.child_id = C.child_id
                    LEFT JOIN ci_event_participate EP ON EP.pp_type = %s AND EP.event_id = %s AND CC.child_id = EP.pp_id
                    LEFT JOIN users U ON U.user_id = EP.created_user_id
                    WHERE CC.class_id = %s ORDER BY C.name_for_sort ASC", PARTICIPANT_TYPE_CHILD, secure($event_id, 'int'), secure($class_id, 'int'));

        $get_children = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        $childs = array();
        $parents = array();
        if ($get_children->num_rows > 0) {
            while ($child = $get_children->fetch_assoc()) {
                if ($child['event_id'] > 0) {
                    $childs[] = $child['child_id'];
                }

                if ($parent_included) {
                    $parent = $this->_getParentParticipantsOfChild($child['child_id'], $event_id);
                    //Lọc ra số cha mẹ tham gia
                    foreach($parent as $user) {
                        if ($user['event_id'] > 0) {
                            $parents[] = $user['user_id'];
                        }
                    }
                }
            }
        }
        $results['childs'] = $childs;
        $results['parents'] = $parents;

        return $results;
    }



    /**
     * Lấy ra danh sách ID trẻ của lớp tham gia sự kiện.
     *
     * @param $class_id
     * @param $event_id
     * @return array
     * @throws Exception
     * @internal param $parent_included
     * @internal param $childCount
     * @internal param $parentCount
     */
    function getParticipantsIdOfClass1($class_id, $event_id) {
        global $db;

        $results = array();
        $strSql = sprintf("SELECT C.child_id FROM ci_class_child CC
                    INNER JOIN ci_child C ON CC.child_id = C.child_id
                    INNER JOIN ci_event_participate EP ON EP.pp_type = %s AND EP.event_id = %s AND CC.child_id = EP.pp_id
                    WHERE CC.class_id = %s", PARTICIPANT_TYPE_CHILD, secure($event_id, 'int'), secure($class_id, 'int'));

        $get_children = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_children->num_rows > 0) {
            while ($child = $get_children->fetch_assoc()) {
                $child['parents'] = $this->_getParentParticipantsIdOfChild($child['child_id'], $event_id);
                $results[] = $child;
            }
        }

        return $results;
    }

    /**
     * Lấy thông tin cha mẹ của 1 trẻ tham gia sự kiện.
     *
     * @param $childId
     * @param $event_id
     * @return array
     * @throws Exception
     */
    private function _getParentParticipantsOfChild($childId, $event_id)
    {
        global $db;
        $results = array();
        $strSql = sprintf("SELECT U.user_id, U.user_name, U.user_fullname, U.user_gender, U.user_picture, EP.event_id, EP.is_paid, EP.created_at, EP.created_user_id, U.user_fullname AS created_fullname FROM ci_user_manage UM
                            INNER JOIN users U ON UM.user_id = U.user_id AND UM.object_type = %s AND UM.object_id = %s
                            LEFT JOIN ci_event_participate EP ON UM.user_id = EP.pp_id AND EP.pp_type = %s AND EP.event_id = %s
                            LEFT JOIN users US ON US.user_id = EP.created_user_id",
                            MANAGE_CHILD, secure($childId, 'int'), PARTICIPANT_TYPE_PARENT, secure($event_id, 'int'));
        $get_users = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_users->num_rows > 0) {
            while ($user = $get_users->fetch_assoc()) {
                $user['user_picture'] = get_picture($user['user_picture'], $user['user_gender']);
                $results[] = $user;
            }
        }

        return $results;
    }

    /**
     * Lấy ID cha mẹ của 1 trẻ tham gia sự kiện.
     *
     * @param $childId
     * @param $event_id
     * @return array
     * @throws Exception
     */
    private function _getParentParticipantsIdOfChild($childId, $event_id)
    {
        global $db;
        $results = array();
        $strSql = sprintf("SELECT U.user_id FROM ci_user_manage UM
                            INNER JOIN users U ON UM.user_id = U.user_id AND UM.object_type = %s AND UM.object_id = %s
                            INNER JOIN ci_event_participate EP ON UM.user_id = EP.pp_id AND EP.pp_type = %s AND EP.event_id = %s",
            MANAGE_CHILD, secure($childId, 'int'), PARTICIPANT_TYPE_PARENT, secure($event_id, 'int'));
        $get_users = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_users->num_rows > 0) {
            while ($user = $get_users->fetch_assoc()) {
                $results[] = $user['user_id'];
            }
        }

        return $results;
    }

    /**
     * Lọc ra danh sách ID của những người đã đăng ký tham gia sự kiện từ danh sách ID cho trước
     *
     * @param $eventId
     * @param $ppType
     * @param $ppIds
     * @return array
     * @throws Exception
     */
    private function _getParticipantIds($eventId, $ppType, $ppIds) {
        global $db;
        $resultIds = array();
        if (count($ppIds) == 0) return $resultIds;

        $strCon = implode(',', $ppIds);
        $strSql = sprintf("SELECT pp_id FROM ci_event_participate WHERE event_id = %s AND pp_type = %s AND pp_id IN (%s)", secure($eventId, 'int'), secure($ppType, 'int'), $strCon);

        $get_pps = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_pps->num_rows > 0) {
            while($pp = $get_pps->fetch_assoc()) {
                $resultIds[] = $pp['pp_id'];
            }
        }
        return $resultIds;
    }

    /**
     * Lây ra danh sách sự kiện của một trường
     *
     * @param int $school_id
     * @return array
     * @throws Exception
     */
    public function getEvents($school_id, $limit = 999999) {
        global $db, $date, $user, $system;

        $strSql = sprintf("SELECT E.*, CL.class_level_name, G.group_title FROM ci_event E
                            LEFT JOIN ci_class_level CL ON E.class_level_id = CL.class_level_id
                            LEFT JOIN groups G ON E.class_id = G.group_id
                            WHERE E.school_id = %s ORDER BY E.created_at DESC LIMIT %s", secure($school_id, 'int'), $limit);
        $events = array();
        $get_events = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_events->num_rows > 0) {
            while($event = $get_events->fetch_assoc()) {
//                if ((!is_empty($event['begin'])) && ($event['begin'] < $date) && ($event['begin'] > '1970-01-01')) {
//                    $event['happened'] = 1; //sự kiện đã qua rồi hay chưa
//                } else {
//                    $event['happened'] = 0;
//                }
                // Thêm style max-width cho thẻ ảnh
                $event['description'] = addMaxWidthForImgTag($event['description']);

                $event['description'] = html_entity_decode($event['description'], ENT_QUOTES);

                if (((!is_empty($event['registration_deadline'])) && ($event['registration_deadline'] < $date) && ($event['registration_deadline'] > '1970-01-01')) || $event['must_register'] == 0) {
                    $event['can_register'] = 0; //không thể đăng ký
                } else {
                    $event['can_register'] = (($event['status'] != EVENT_STATUS_CANCELLED)) ? 1 : 0;
                }
//                $event['begin'] = toSysDatetime($event['begin']);
//                $event['end'] = toSysDatetime($event['end']);
                $event['registration_deadline'] = toSysDatetime($event['registration_deadline']);
                if($system['is_mobile']) {
                    // Lấy thông tin nhân viên đã đăng ký sự kiện chưa
                    $pp = array();
                    if ($event['must_register']) {
                        $teacher = array();
                        $teacher['pp_id'] = $user->_data['user_id'];
                        $teacher['is_registered'] = false;

                        $participants = $this->_getTeacherPaticipants($event['event_id'], $user->_data['user_id']);
                        foreach ($participants as $participant) {
                            if ($participant['pp_type'] == PARTICIPANT_TYPE_TEACHER) {
                                $teacher['is_registered'] = true;
                                $teacher['is_paid'] = $participant['is_paid'];
                            }
                        }
                        if ($event['for_teacher']) {
                            $event['is_registered'] = $teacher['is_registered'];
                        }
                    }
                }

//                $event['post_on_wall_text'] = $event['post_on_wall']? __('Post on wall') : __('Unpost on wall');
//                $event['is_notified_text'] = $event['is_notified']? __('Notified') : __('Not notified yet');

                //$event['description'] = htmlspecialchars_decode($event['description']);
//                if(($event['created_user_id'] == $user->_data['user_id']) && !$event['happened']){
//                    $event['can_edit'] = 1;
//                } else {
//                    $event['can_edit'] = 0;
//                }
                $events[] = $event;
            }
        }

        return $events;
    }

    /**
     * Lây ra danh sách sự kiện của một trường chỉ dành cho GIÁO VIÊN/NHÂN VIÊN
     *
     * @param int $school_id
     * @return array
     * @throws Exception
     */
    public function getEventsForEmployee($school_id, $limit = 999999) {
        global $db, $date, $user, $system;

        $strSql = sprintf("SELECT * FROM ci_event
                            WHERE school_id = %s AND for_teacher = 1 ORDER BY created_at DESC LIMIT %s", secure($school_id, 'int'), $limit);
        $events = array();
        $get_events = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_events->num_rows > 0) {
            while($event = $get_events->fetch_assoc()) {
//                if ((!is_empty($event['begin'])) && ($event['begin'] < $date) && ($event['begin'] > '1970-01-01')) {
//                    $event['happened'] = 1; //sự kiện đã qua rồi hay chưa
//                } else {
//                    $event['happened'] = 0;
//                }
                // Thêm style max-width cho thẻ ảnh
                $event['description'] = addMaxWidthForImgTag($event['description']);

                $event['description'] = html_entity_decode($event['description'], ENT_QUOTES);

                if (((!is_empty($event['registration_deadline'])) && ($event['registration_deadline'] < $date) && ($event['registration_deadline'] > '1970-01-01')) || $event['must_register'] == 0) {
                    $event['can_register'] = 0; //không thể đăng ký
                } else {
                    $event['can_register'] = (($event['status'] != EVENT_STATUS_CANCELLED)) ? 1 : 0;
                }
//                $event['begin'] = toSysDatetime($event['begin']);
//                $event['end'] = toSysDatetime($event['end']);
                $event['registration_deadline'] = toSysDatetime($event['registration_deadline']);
                if($system['is_mobile']) {
                    // Lấy thông tin nhân viên đã đăng ký sự kiện chưa
                    $pp = array();
                    if ($event['must_register']) {
                        $teacher = array();
                        $teacher['pp_id'] = $user->_data['user_id'];
                        $teacher['is_registered'] = false;

                        $participants = $this->_getTeacherPaticipants($event['event_id'], $user->_data['user_id']);
                        foreach ($participants as $participant) {
                            if ($participant['pp_type'] == PARTICIPANT_TYPE_TEACHER) {
                                $teacher['is_registered'] = true;
                                $teacher['is_paid'] = $participant['is_paid'];
                            }
                        }
                        if ($event['for_teacher']) {
                            $event['is_registered'] = $teacher['is_registered'];
                        }
                    }
                }
//                if(($event['created_user_id'] == $user->_data['user_id']) && !$event['happened']){
//                    $event['can_edit'] = 1;
//                } else {
//                    $event['can_edit'] = 0;
//                }
                $events[] = $event;
            }
        }
        return $events;
    }

    /**
     * Lấy ra danh sách sự kiện của một lớp
     *
     * @param $class_id
     * @param $class_level_id
     * @param $school_id
     * @param int $limit
     * @return array
     */
    public function getClassEvents($class_id, $class_level_id, $school_id, $limit = 999999) {
        global $db, $date, $user;

        $strSql = sprintf("SELECT * FROM ci_event WHERE for_teacher = 0 AND
                            ((school_id = %s AND level = %s AND is_notified = 1) OR (class_level_id = %s AND level = %s AND is_notified = 1) OR (class_id = %s AND level = %s))
                            ORDER BY created_at DESC, level ASC LIMIT %s", secure($school_id, 'int'), SCHOOL_LEVEL,
                            secure($class_level_id, 'int'), CLASS_LEVEL_LEVEL, secure($class_id, 'int'), CLASS_LEVEL, $limit);
        $events = array();
        $get_events = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_events->num_rows > 0) {
            while($event = $get_events->fetch_assoc()) {
//                if ((!is_empty($event['begin'])) && ($event['begin'] < $date) && ($event['begin'] > '1970-01-01')) {
//                    $event['happened'] = 1; //sự kiện đã qua rồi hay chưa
//                } else {
//                    $event['happened'] = 0;
//                }
                // Thêm style max-width cho thẻ ảnh
                $event['description'] = addMaxWidthForImgTag($event['description']);

                $event['description'] = html_entity_decode($event['description'], ENT_QUOTES);

                if (!$event['must_register'] || ((!is_empty($event['registration_deadline'])) && ($event['registration_deadline'] < $date) && ($event['registration_deadline'] > '1970-01-01'))) {
                    $event['can_register'] = 0; //không thể đăng ký
                } else {
                    if(($event['status'] != EVENT_STATUS_CANCELLED)) {
                        $event['can_register'] = 1;
                    } else {
                        $event['can_register'] = 0;
                    }
                }

//                $event['begin'] = toSysDatetime($event['begin']);
//                $event['end'] = toSysDatetime($event['end']);
                $event['registration_deadline'] = toSysDatetime($event['registration_deadline']);
                $event['post_on_wall_text'] = $event['post_on_wall']? __('Post on page') : __('Unpost on wall');
                $event['is_notified_text'] = $event['is_notified']? __('Notified') : __('Not notified yet');

//                $event['can_edit'] = ($event['created_user_id'] == $user->_data['user_id']) ? 1 : 0;
                $events[] = $event;
            }
        }
        return $events;
    }
    
    /**
     * CI-mobile lấy ra chi tiết của 1 sự kiện
     *
     * @param $class_id
     * @param $class_level_id
     * @param $school_id
     * @param int $limit
     * @return array
     */
    public function getClassEventDetailApi($event_id) {
        global $db, $date, $user;

        $strSql = sprintf("SELECT * FROM ci_event WHERE event_id = %s", secure($event_id, 'int'));
        
        $event = null;
        $get_events = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_events->num_rows > 0) {
            $event = $get_events->fetch_assoc();
//            if ((!is_empty($event['begin'])) && ($event['begin'] < $date) && ($event['begin'] > '1970-01-01')) {
//                $event['happened'] = 1; //sự kiện đã qua rồi hay chưa
//            } else {
//                $event['happened'] = 0;
//            }
            // Thêm style max-width cho thẻ ảnh
            $event['description'] = addMaxWidthForImgTag($event['description']);

            //$event['description'] = html_entity_decode($event['description'], ENT_QUOTES);

            if ((!is_empty($event['registration_deadline'])) && ($event['registration_deadline'] < $date) && ($event['registration_deadline'] > '1970-01-01')) {
                $event['can_register'] = 0; //không thể đăng ký
            } else {
                $event['can_register'] = (($event['status'] != EVENT_STATUS_CANCELLED)) ? 1 : 0;
            }

//            $event['begin'] = toSysDatetime($event['begin']);
//            $event['end'] = toSysDatetime($event['end']);
            $event['registration_deadline'] = toSysDatetime($event['registration_deadline']);
            $event['post_on_wall_text'] = $event['post_on_wall']? __('Post on page') : __('Unpost on wall');
            $event['is_notified_text'] = $event['is_notified']? __('Notified') : __('Not notified yet');

//            $event['can_edit'] = ($event['created_user_id'] == $user->_data['user_id']);
        }
        return $event;
    }

    /**
     * Lấy ra danh sách các sự kiện liên quan đến một trẻ. Dùng cho phần màn hình của cha mẹ
     *
     * @param $class_id
     * @param $class_level_id
     * @param $school_id
     * @param $child_id
     * @param $parent_id
     * @param int $limit
     * @return array
     */
    public function getChildEvents($class_id, $class_level_id, $school_id, $child_id, $limit = 999999) {
        global $db, $date, $user;

        $strSql = sprintf("SELECT * FROM ci_event WHERE is_notified = 1 AND for_teacher = 0 AND 
                            ((school_id = %s AND level = %s) OR (class_level_id = %s AND level = %s) OR (class_id = %s AND level = %s))
                            ORDER BY created_at DESC, level ASC LIMIT %s", secure($school_id, 'int'), SCHOOL_LEVEL,
            secure($class_level_id, 'int'), CLASS_LEVEL_LEVEL, secure($class_id, 'int'), CLASS_LEVEL, $limit);
        $events = array();
        $get_events = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_events->num_rows > 0) {
            while($event = $get_events->fetch_assoc()) {
//                if ((!is_empty($event['begin'])) && ($event['begin'] < $date) && ($event['begin'] > '1970-01-01')) {
//                    $event['happened'] = 1; //sự kiện đã qua rồi hay chưa
//                } else {
//                    $event['happened'] = 0;
//                }

                // Thêm style max-width cho thẻ ảnh
                $event['description'] = addMaxWidthForImgTag($event['description']);

                $event['description'] = html_entity_decode($event['description'], ENT_QUOTES);

                if ((!is_empty($event['registration_deadline'])) && ($event['registration_deadline'] < $date) && ($event['registration_deadline'] > '1970-01-01')) {
                    $event['can_register'] = 0; //không thể đăng ký
                } else {
                    $event['can_register'] = (($event['status'] != EVENT_STATUS_CANCELLED)) ? 1 : 0;
                }

//                $event['begin'] = toSysDatetime($event['begin']);
//                $event['end'] = toSysDatetime($event['end']);
                $event['registration_deadline'] = toSysDatetime($event['registration_deadline']);

                //Lấy ra thông tin cha mẹ và trẻ có tham gia không
//                $pp = array();
//                if ($event['must_register']) {
//                    $child = array();
//                    $child['pp_id'] = $child_id;
//                    $child['is_registered'] = false;
//
//                    $parent = array();
//                    $parent['pp_id'] = $user->_data['user_id'];
//                    $parent['is_registered'] = false;
//
//                    $participants = $this->_getChildPaticipants($event['event_id'], $child_id, $user->_data['user_id']);
//                    foreach ($participants as $participant) {
//                        if ($participant['pp_type'] == PARTICIPANT_TYPE_CHILD) {
//                            $child['is_registered'] = true;
//                            $child['is_paid'] = $participant['is_paid'];
//                        } else {
//                            $parent['is_registered'] = true;
//                            $parent['is_paid'] = $participant['is_paid'];
//                        }
//                    }
//                    if ($event['for_child']) {
//                        $pp['child'] = $child;
//                    }
//                    if ($event['for_parent']) {
//                        $pp['parent'] = $parent;
//                    }
//                }
//                $event['participants'] = $pp;
                $events[] = $event;
            }
        }
        return $events;
    }


    /**
     * Lấy ra thông tin phụ huynh và trẻ có đăng ký tham gia một sự kiện hay không
     *
     * @param $eventId
     * @param $childId
     * @param $parentId
     * @return array
     */
    public function _getChildPaticipants($eventId, $childId, $parentId) {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_event_participate WHERE event_id = %s AND ((pp_type = %s AND pp_id = %s) OR (pp_type = %s AND pp_id = %s))",
            secure($eventId, 'int'), PARTICIPANT_TYPE_CHILD, secure($childId, 'int'), PARTICIPANT_TYPE_PARENT, secure($parentId, 'int'));

        $results = array();
        $get_pps = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_pps->num_rows > 0) {
            while($pp = $get_pps->fetch_assoc()) {
                $results[] = $pp;
            }
        }

        return $results;
    }

    /**
     * Lấy ra thông tin giáo viên/nhân viên có đăng ký tham gia một sự kiện hay không
     *
     * @param $eventId
     * @param $teacherId
     * @return array
     */
    public function _getTeacherPaticipants($eventId, $teacherId) {
        global $db;

        $strSql = sprintf("SELECT * FROM ci_event_participate WHERE event_id = %s AND (pp_type = %s AND pp_id = %s)",
            secure($eventId, 'int'), PARTICIPANT_TYPE_TEACHER, secure($teacherId, 'int'));

        $results = array();
        $get_pps = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_pps->num_rows > 0) {
            while($pp = $get_pps->fetch_assoc()) {
                $results[] = $pp;
            }
        }

        return $results;
    }

    /**
     * Lấy ra sự kiện theo ID
     *
     * @param $event_id
     * @return array|null
     * @throws Exception
     */
    public function getEvent($event_id) {
        global $db, $date;

        $event = null;
        $strSql = sprintf("SELECT E.*, G.group_title FROM ci_event E
                LEFT JOIN ci_class_level CL ON E.class_level_id = CL.class_level_id
                LEFT JOIN groups G ON E.class_id = G.group_id
                WHERE E.event_id = %s", secure($event_id, 'int'));
        $get_events = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_events->num_rows > 0) {
            $event = $get_events->fetch_assoc();

            // Thêm style max-width cho thẻ ảnh
            $event['description'] = addMaxWidthForImgTag($event['description']);

            $event['description'] = html_entity_decode($event['description'], ENT_QUOTES);

            if ((!is_empty($event['registration_deadline'])) && ($event['registration_deadline'] < $date) && ($event['registration_deadline'] > '1970-01-01')) {
                $event['can_register'] = 0; //không thể đăng ký
            } else {
                $event['can_register'] = ($event['status'] != EVENT_STATUS_CANCELLED) ? 1 : 0;
            }
            $event['registration_deadline'] = toSysDatetime($event['registration_deadline']);
            //$event['description'] = htmlspecialchars_decode($event['description']);
        }
        return $event;
    }

    /**
     * Lấy ra chi tiết một sự kiện
     *
     * @param $event_id
     * @return array|null
     * @throws Exception
     */
    public function getEventDetail($event_id) {
        global $db, $date;

        $event = null;
        $strSql = sprintf("SELECT E.*, U.user_fullname, CL.class_level_name, G.group_title FROM ci_event E
                            INNER JOIN users U ON E.created_user_id = U.user_id
                            LEFT JOIN ci_class_level CL ON E.class_level_id = CL.class_level_id
                            LEFT JOIN groups G ON E.class_id = G.group_id
                            WHERE event_id = %s", secure($event_id, 'int'));
        $get_events = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_events->num_rows > 0) {
            $event = $get_events->fetch_assoc();
//            if ((!is_empty($event['begin'])) && ($event['begin'] < $date) && ($event['begin'] > '1970-01-01')) {
//                $event['happened'] = 1; //sự kiện đã qua rồi hay chưa
//            } else {
//                $event['happened'] = 0;
//            }
            // Thêm style max-width cho thẻ ảnh
            $event['description'] = addMaxWidthForImgTag($event['description']);

            $event['description'] = html_entity_decode($event['description'], ENT_QUOTES);

            if ((!is_empty($event['registration_deadline'])) && ($event['registration_deadline'] < $date) && ($event['registration_deadline'] > '1970-01-01')) {
                $event['can_register'] = 0; //không thể đăng ký
            } else {
                $event['can_register'] = ($event['status'] != EVENT_STATUS_CANCELLED) ? 1 : 0;
            }

//            $event['begin'] = toSysDatetime($event['begin']);
//            $event['end'] = toSysDatetime($event['end']);
            $event['registration_deadline'] = toSysDatetime($event['registration_deadline']);
            $event['created_at'] = toSysDatetime($event['created_at']);
            $event['updated_at'] = toSysDatetime($event['updated_at']);
            //$event['description'] = htmlspecialchars_decode($event['description']);
        }
        return $event;
    }

    /**
     * Thông tin chi tiết sự kiện, hiển thị trong màn hình TRẺ
     *
     * @param $eventId
     * @param $childId
     * @return array|null
     * @throws Exception
     */
    public function getChildEventDetail($eventId, $childId) {
        global $db, $date, $user;

        $event = null;
        $strSql = sprintf("SELECT E.*, U.user_fullname, CL.class_level_name, G.group_title FROM ci_event E
                            INNER JOIN users U ON E.created_user_id = U.user_id
                            LEFT JOIN ci_class_level CL ON E.class_level_id = CL.class_level_id
                            LEFT JOIN groups G ON E.class_id = G.group_id
                            WHERE event_id = %s", secure($eventId, 'int'));
        $get_events = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_events->num_rows > 0) {
            $event = $get_events->fetch_assoc();
//            if ((!is_empty($event['begin'])) && ($event['begin'] < $date) && ($event['begin'] > '1970-01-01')) {
//                $event['happened'] = 1; //sự kiện đã qua rồi hay chưa
//            } else {
//                $event['happened'] = 0;
//            }
            // Thêm style max-width cho thẻ ảnh
            // $event['description'] = addMaxWidthForImgTag($event['description']);

            $event['description'] = html_entity_decode($event['description'], ENT_QUOTES);

            if ((!is_empty($event['registration_deadline'])) && ($event['registration_deadline'] < $date) && ($event['registration_deadline'] > '1970-01-01')) {
                $event['can_register'] = 0; //không thể đăng ký
            } else {
                $event['can_register'] = (($event['status'] != EVENT_STATUS_CANCELLED)) ? 1 : 0;
            }

//            $event['begin'] = toSysDatetime($event['begin']);
//            $event['end'] = toSysDatetime($event['end']);
            $event['registration_deadline'] = toSysDatetime($event['registration_deadline']);
            $event['created_at'] = toSysDatetime($event['created_at']);
            $event['updated_at'] = toSysDatetime($event['updated_at']);

            //Lấy ra thông tin cha mẹ và trẻ có tham gia không
            $pp = array();
            if ($event['must_register']) {
                $child = array();
                $child['pp_id'] = $childId;
                $child['is_registered'] = false;

                $parent = array();
                $parent['pp_id'] = $user->_data['user_id'];
                $parent['is_registered'] = false;

                $participants = $this->_getChildPaticipants($event['event_id'], $childId, $user->_data['user_id']);
                foreach ($participants as $participant) {
                    if ($participant['pp_type'] == PARTICIPANT_TYPE_CHILD) {
                        $child['is_registered'] = true;
                        $child['is_paid'] = $participant['is_paid'];
                    } else {
                        $parent['is_registered'] = true;
                        $parent['is_paid'] = $participant['is_paid'];
                    }
                }
                if ($event['for_child']) {
                    $pp['child'] = $child;
                }
                if ($event['for_parent']) {
                    $pp['parent'] = $parent;
                }
            }
            $event['participants'] = empty($pp) ? null : $pp;
        }
        return $event;
    }

    /**
     * Thông tin chi tiết sự kiện, hiển thị trong màn hình TRẺ (api)
     *
     * @param $eventId
     * @param $childId
     * @return array|null
     * @throws Exception
     */
    public function getChildEventDetailForApi($eventId, $childId) {
        global $db, $date, $user;

        $event = null;
        $strSql = sprintf("SELECT E.*, U.user_fullname, CL.class_level_name, G.group_title FROM ci_event E
                            INNER JOIN users U ON E.created_user_id = U.user_id
                            LEFT JOIN ci_class_level CL ON E.class_level_id = CL.class_level_id
                            LEFT JOIN groups G ON E.class_id = G.group_id
                            WHERE event_id = %s", secure($eventId, 'int'));
        $get_events = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_events->num_rows > 0) {
            $event = $get_events->fetch_assoc();
//            if ((!is_empty($event['begin'])) && ($event['begin'] < $date) && ($event['begin'] > '1970-01-01')) {
//                $event['happened'] = 1; //sự kiện đã qua rồi hay chưa
//            } else {
//                $event['happened'] = 0;
//            }
            // Thêm style max-width cho thẻ ảnh
            $event['description'] = addMaxWidthForImgTag($event['description']);

            //$event['description'] = html_entity_decode($event['description'], ENT_QUOTES);

            if ((!is_empty($event['registration_deadline'])) && ($event['registration_deadline'] < $date) && ($event['registration_deadline'] > '1970-01-01')) {
                $event['can_register'] = 0; //không thể đăng ký
            } else {
                $event['can_register'] = (($event['status'] != EVENT_STATUS_CANCELLED)) ? 1 : 0;
            }

//            $event['begin'] = toSysDatetime($event['begin']);
//            $event['end'] = toSysDatetime($event['end']);
            $event['registration_deadline'] = toSysDatetime($event['registration_deadline']);
            $event['created_at'] = toSysDatetime($event['created_at']);
            $event['updated_at'] = toSysDatetime($event['updated_at']);

            //Lấy ra thông tin cha mẹ và trẻ có tham gia không
            $pp = array();
            if ($event['must_register']) {
                $child = array();
                $child['pp_id'] = $childId;
                $child['is_registered'] = false;

                $parent = array();
                $parent['pp_id'] = $user->_data['user_id'];
                $parent['is_registered'] = false;

                $participants = $this->_getChildPaticipants($event['event_id'], $childId, $user->_data['user_id']);
                foreach ($participants as $participant) {
                    if ($participant['pp_type'] == PARTICIPANT_TYPE_CHILD) {
                        $child['is_registered'] = true;
                        $child['is_paid'] = $participant['is_paid'];
                    } else {
                        $parent['is_registered'] = true;
                        $parent['is_paid'] = $participant['is_paid'];
                    }
                }
                if ($event['for_child']) {
                    $pp['child'] = $child;
                }
                if ($event['for_parent']) {
                    $pp['parent'] = $parent;
                }
            }
            $event['participants'] = empty($pp) ? null : $pp;
        }
        return $event;
    }

    /**
     * Thông tin chi tiết sự kiện, hiển thị trong màn hình TRẺ
     *
     * @param $eventId
     * @param $childId
     * @return array|null
     * @throws Exception
     */
    public function getTeacherEventDetail($eventId, $teacherId) {
        global $db, $date, $user;

        $event = null;
        $strSql = sprintf("SELECT E.*, U.user_fullname, CL.class_level_name, G.group_title FROM ci_event E
                            INNER JOIN users U ON E.created_user_id = U.user_id
                            LEFT JOIN ci_class_level CL ON E.class_level_id = CL.class_level_id
                            LEFT JOIN groups G ON E.class_id = G.group_id
                            WHERE event_id = %s", secure($eventId, 'int'));
        $get_events = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_events->num_rows > 0) {
            $event = $get_events->fetch_assoc();
//            if ((!is_empty($event['begin'])) && ($event['begin'] < $date) && ($event['begin'] > '1970-01-01')) {
//                $event['happened'] = 1; //sự kiện đã qua rồi hay chưa
//            } else {
//                $event['happened'] = 0;
//            }
            // Thêm style max-width cho thẻ ảnh
            $event['description'] = addMaxWidthForImgTag($event['description']);

            $event['description'] = html_entity_decode($event['description'], ENT_QUOTES);

            if ((!is_empty($event['registration_deadline'])) && ($event['registration_deadline'] < $date) && ($event['registration_deadline'] > '1970-01-01')) {
                $event['can_register'] = 0; //không thể đăng ký
            } else {
                $event['can_register'] = (($event['status'] != EVENT_STATUS_CANCELLED));
            }

//            $event['begin'] = toSysDatetime($event['begin']);
//            $event['end'] = toSysDatetime($event['end']);
            $event['registration_deadline'] = toSysDatetime($event['registration_deadline']);
            $event['created_at'] = toSysDatetime($event['created_at']);
            $event['updated_at'] = toSysDatetime($event['updated_at']);

            //Lấy ra thông tin giáo viên có tham gia không
            $pp = array();
            if ($event['must_register']) {
                $teacher = array();
                $teacher['pp_id'] = $teacherId;
                $teacher['is_registered'] = false;

                $participants = $this->_getTeacherPaticipants($event['event_id'], $teacherId);
                foreach ($participants as $participant) {
                    if ($participant['pp_type'] == PARTICIPANT_TYPE_TEACHER) {
                        $teacher['is_registered'] = true;
                        $teacher['is_paid'] = $participant['is_paid'];
                    }
                }
                if ($event['for_teacher']) {
                    $pp['teacher'] = $teacher;
                }
            }
            $event['participants'] = $pp;
        }
        return $event;
    }

    /**
     * Lấy ra số lượng event của một trường
     *
     * @param int $school_id
     * @return mixed
     * @throws Exception
     */
    public function getCountEventOfSchool($school_id = 0) {
        global $db;

        $strSql = sprintf("SELECT COUNT(event_id) AS event_count FROM ci_event WHERE school_id = %s", secure($school_id, 'int'));
        $get_count = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        return $get_count->fetch_assoc()['event_count'];
    }

    /**
     * Insert một event vào trong DB
     *
     * @param array $args
     * @return mixed
     * @throws Exception
     */
    public function insertEvent(array $args = array()) {
        global $db, $date;
        $this->validateInput($args);
//        $args['begin'] = !is_empty($args['begin']) ? secure(toDBDatetime($args['begin'])) : 'NULL';
//        $args['end'] = !is_empty($args['end']) ? secure(toDBDatetime($args['end'])) : 'NULL';
        $args['registration_deadline'] = !is_empty($args['registration_deadline']) ? secure(toDBDatetime($args['registration_deadline'])) : 'null';

        $strSql = sprintf("INSERT INTO ci_event (event_name, level, school_id, class_level_id, class_id, must_register, for_teacher,
                            for_parent, for_child, post_on_wall, is_notified, description, created_at, created_user_id, registration_deadline)
                              VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
                            secure($args['event_name']), secure($args['level'], 'int'),
                            secure($args['school_id'], 'int'), secure($args['class_level_id'], 'int'), secure($args['class_id'], 'int'), secure($args['must_register'], 'int'),
                            secure($args['for_teacher'], 'int'), secure($args['for_parent'], 'int'), secure($args['for_child'], 'int'), secure($args['post_on_wall'], 'int'),
                            secure($args['is_notified'], 'int'), secure($args['description']), secure($date), secure($args['created_user_id'], 'int'), $args['registration_deadline']);

        $db->query($strSql) or _error(SQL_ERROR_THROWEN);

        return $db->insert_id;
    }

    /**
     * Kiểm tra điều kiện đầu vào.
     *
     * @param array $args
     * @throws Exception
     */
    private function validateInput(array $args = array()) {
        if (!isset($args['event_name'])) {
            throw new Exception(__("You must enter event name"));
        }

        if (strlen($args['event_name']) < 10) {
            throw new Exception(__("Event name must be at least 10 characters long"));
        }

        if (!isset($args['description'])) {
            throw new Exception(__("You must enter event description"));
        }
//        if($args['registration_deadline'] != '') {
//            $registationDB = toDBDatetime($args['registration_deadline']);
//            $dateNow = date('Y-m-d');
//            $dateTimeNow = $dateNow . ' 00:00:00';
//            if(strtotime($registationDB) < strtotime($dateTimeNow)) {
//                throw new Exception(__("The registration deadline can not be the past day"));
//            }
//        }

//        if (!validateDateTime($args['begin'])) {
//            throw new Exception(__("You must enter correct begin time"));
//        }
//
//        if (!validateDateTime($args['end'])) {
//            throw new Exception(__("You must enter correct end time"));
//        }
//        if(($args['begin'] != '') && ($args['registration_deadline'] != '')) {
//            $re_deadlineDB = toDBDatetime($_POST['registration_deadline']);
//            $re_beginDB = toDBDatetime($_POST['begin']);
//            if(strtotime($re_deadlineDB) > strtotime($re_beginDB)) {
//                throw new Exception (__("The registration deadline must be before the beginning of event"));
//            }
//        }
    }

    /**
     * Cập nhật một event vào trong DB
     *
     * @param array $args
     * @throws Exception
     */
    public function updateEvent(array $args = array()) {
        global $db, $date;

        if ($args['event_id'] <= 0) {
            throw new Exception(__("You must select an existing event"));
        }
        $this->validateInput($args);
//        $args['begin'] = !is_empty($args['begin']) ? secure(toDBDatetime($args['begin'])) : 'NULL';
//        $args['end'] = !is_empty($args['end']) ? secure(toDBDatetime($args['end'])) : 'NULL';
        $args['registration_deadline'] = !is_empty($args['registration_deadline']) ? secure(toDBDatetime($args['registration_deadline'])) : 'NULL';

        $strSql = sprintf("UPDATE ci_event SET  event_name = %s, level = %s, class_level_id = %s, class_id = %s, must_register = %s,
                            for_teacher = %s, for_parent = %s, for_child = %s, post_on_wall = %s, is_notified = %s, description = %s, updated_at = %s, registration_deadline = %s WHERE event_id = %s",
                            secure($args['event_name']), secure($args['level'], 'int'),
                            secure($args['class_level_id'], 'int'), secure($args['class_id'], 'int'), secure($args['must_register'], 'int'), secure($args['for_teacher'], 'int'),
                            secure($args['for_parent'], 'int'), secure($args['for_child'], 'int'), secure($args['post_on_wall'], 'int'),
                            secure($args['is_notified'], 'int'), secure($args['description']), secure($date), $args['registration_deadline'], secure($args['event_id'], 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Cập nhật post_ids của event.
     *
     * @param $postIds
     * @param $event_id
     * @throws Exception
     */
    public function updatePostIds($postIds, $event_id) {
        global $db;

        $strSql = sprintf("UPDATE ci_event SET post_ids = %s WHERE event_id = %s", secure($postIds), secure($event_id, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Cập nhật trạng thái thông báo của sự kiện
     *
     * @param $event_id
     * @throws Exception
     */
    public function updateStatusToNotified($event_id) {
        global $db;

        $strSql = sprintf("UPDATE ci_event SET is_notified = %s WHERE event_id = %s", 1, secure($event_id, 'int'));
        $db->query($strSql) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Xóa một thông báo
     *
     * @param $event_id
     * @throws Exception
     */
    public function deleteEvent($event_id, $checkCreatedUser = false) {
        global $db, $user;

        if ($checkCreatedUser) {
            //Kiểm tra user hiện tại có được phép xóa không
            $strSql = sprintf("SELECT event_id FROM ci_event WHERE event_id = %s", secure($event_id, 'int'), secure($user->_data['user_id'], 'int'));
            $get_event = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
            if ($get_event->num_rows == 0) {
                _error(403);
            }
        }

        $db->query(sprintf("DELETE FROM ci_event WHERE event_id = %s", secure($event_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        $db->query(sprintf("DELETE FROM ci_event_participate WHERE event_id = %s", secure($event_id, 'int'))) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Hủy sự kiện
     *
     * @param $event_id
     * @param bool|false $checkCreatedUser
     * @throws Exception
     */
    public function cancelEvent($event_id, $checkCreatedUser = false) {
        global $db, $user;

        if ($checkCreatedUser) {
            //Kiểm tra user hiện tại có được phép xóa không
            $strSql = sprintf("SELECT event_id FROM ci_event WHERE event_id = %s AND created_user_id = %s", secure($event_id, 'int'), secure($user->_data['user_id'], 'int'));
            $get_event = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
            if ($get_event->num_rows == 0) {
                _error(403);
            }
        }

        $db->query(sprintf("UPDATE ci_event SET status = %s WHERE event_id = %s", EVENT_STATUS_CANCELLED, secure($event_id, 'int'))) or _error(SQL_ERROR_THROWEN);
    }

    /**
     * Xóa một thông báo của lớp
     *
     * @param $event_id
     * @throws Exception
     */
    public function deleteClassEvent($event_id) {
        global $db, $user;

        $strSql = sprintf("SELECT event_id FROM ci_event WHERE event_id = %s AND created_user_id = %s", secure($event_id, 'int'), $user->_data['user_id']);
        $get_count = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_count->num_rows > 0) {
            $db->query(sprintf("DELETE FROM ci_event WHERE event_id = %s", secure($event_id, 'int'))) or _error(SQL_ERROR_THROWEN);
            $db->query(sprintf("DELETE FROM ci_event_participate WHERE event_id = %s", secure($event_id, 'int'))) or _error(SQL_ERROR_THROWEN);
        }
//        else {
//            _error(403);
//        }
    }

    /* ---------- CI - API ---------- */

    /**
     * Lây ra danh sách sự kiện của một trường
     *
     * @param int $school_id
     * @return array
     * @throws Exception
     */
    public function getEventsForApi($school_id, $offset = 0) {
        global $db, $date, $user, $system;

        $offset *= $system['max_results'];

        $strSql = sprintf("SELECT E.event_id, E.event_name, E.must_register, E.registration_deadline, E.for_teacher, E.for_parent, E.for_child, E.post_on_wall, E.is_notified, E.status, E.description, E.views_count, E.created_user_id, 
                            CL.class_level_name, G.group_title FROM ci_event E
                            LEFT JOIN ci_class_level CL ON E.class_level_id = CL.class_level_id
                            LEFT JOIN groups G ON E.class_id = G.group_id
                            WHERE E.school_id = %s ORDER BY E.created_at DESC LIMIT %s, %s", secure($school_id, 'int'), secure($offset, 'int', false), secure($system['max_results'], 'int', false));
        $events = array();
        $get_events = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_events->num_rows > 0) {
            while($event = $get_events->fetch_assoc()) {
//                if ((!is_empty($event['begin'])) && ($event['begin'] < $date) && ($event['begin'] > '1970-01-01')) {
//                    $event['happened'] = 1; //sự kiện đã qua rồi hay chưa
//                } else {
//                    $event['happened'] = 0;
//                }
                // Thêm style max-width cho thẻ ảnh
                $event['description'] = addMaxWidthForImgTag($event['description']);

                //$event['description'] = html_entity_decode($event['description'], ENT_QUOTES);

                if (((!is_empty($event['registration_deadline'])) && ($event['registration_deadline'] < $date) && ($event['registration_deadline'] > '1970-01-01')) || $event['must_register'] == 0) {
                    $event['can_register'] = 0; //không thể đăng ký
                } else {
                    $event['can_register'] = (($event['status'] != EVENT_STATUS_CANCELLED)) ? 1 : 0;
                }
//                $event['begin'] = toSysDatetime($event['begin']);
//                $event['end'] = toSysDatetime($event['end']);
                $event['registration_deadline'] = toSysDatetime($event['registration_deadline']);

//                if(($event['created_user_id'] == $user->_data['user_id']) && !$event['happened']){
//                    $event['can_edit'] = 1;
//                } else {
//                    $event['can_edit'] = 0;
//                }
                $events[] = $event;
            }
        }
        return $events;
    }


    /**
     * Lây ra danh sách sự kiện của một trường chỉ dành cho GIÁO VIÊN/NHÂN VIÊN
     *
     * @param int $school_id
     * @return array
     * @throws Exception
     */
    public function getEventsEmployeeForApi($school_id, $offset = 0) {
        global $db, $date, $user, $system;

        $offset *= $system['max_results'];

        $strSql = sprintf("SELECT event_id, event_name, must_register, registration_deadline, for_teacher, for_parent, for_child, post_on_wall, is_notified, status, description, views_count, created_user_id FROM ci_event
                            WHERE school_id = %s AND for_teacher = 1 ORDER BY created_at DESC LIMIT %s, %s", secure($school_id, 'int'), secure($offset, 'int', false), secure($system['max_results'], 'int', false));
        $events = array();
        $get_events = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_events->num_rows > 0) {
            while($event = $get_events->fetch_assoc()) {
//                if ((!is_empty($event['begin'])) && ($event['begin'] < $date) && ($event['begin'] > '1970-01-01')) {
//                    $event['happened'] = 1; //sự kiện đã qua rồi hay chưa
//                } else {
//                    $event['happened'] = 0;
//                }
                // Thêm style max-width cho thẻ ảnh
                $event['description'] = addMaxWidthForImgTag($event['description']);

                //$event['description'] = html_entity_decode($event['description'], ENT_QUOTES);

                if (((!is_empty($event['registration_deadline'])) && ($event['registration_deadline'] < $date) && ($event['registration_deadline'] > '1970-01-01')) || $event['must_register'] == 0) {
                    $event['can_register'] = 0; //không thể đăng ký
                } else {
                    $event['can_register'] = (($event['happened'] == 0) && ($event['status'] != EVENT_STATUS_CANCELLED)) ? 1 : 0;
                }
//                $event['begin'] = toSysDatetime($event['begin']);
//                $event['end'] = toSysDatetime($event['end']);
                $event['registration_deadline'] = toSysDatetime($event['registration_deadline']);

//                if(($event['created_user_id'] == $user->_data['user_id']) && !$event['happened']){
//                    $event['can_edit'] = 1;
//                } else {
//                    $event['can_edit'] = 0;
//                }
                $events[] = $event;
            }
        }
        return $events;
    }


    /**
     * Lấy ra chi tiết một sự kiện
     *
     * @param $event_id
     * @return array|null
     * @throws Exception
     */
    public function getEventDetailForApi($event_id, $check_register = false) {
        global $db, $user, $date;

        $event = null;
        $strSql = sprintf("SELECT E.event_id, E.event_name, E.level, E.must_register, E.registration_deadline, E.for_teacher, E.for_parent, E.for_child, E.parent_participants, E.child_participants, E.teacher_participants, E.post_on_wall, E.post_ids, E.is_notified, E.status, E.description , E.created_at, E.created_user_id,
                            U.user_fullname, CL.class_level_name, G.group_title FROM ci_event E
                            INNER JOIN users U ON E.created_user_id = U.user_id
                            LEFT JOIN ci_class_level CL ON E.class_level_id = CL.class_level_id
                            LEFT JOIN groups G ON E.class_id = G.group_id
                            WHERE event_id = %s", secure($event_id, 'int'));
        $get_events = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_events->num_rows > 0) {
            $event = $get_events->fetch_assoc();
//            if ((!is_empty($event['begin'])) && ($event['begin'] < $date) && ($event['begin'] > '1970-01-01')) {
//                $event['happened'] = 1; //sự kiện đã qua rồi hay chưa
//            } else {
//                $event['happened'] = 0;
//            }
            //$event['description'] = html_entity_decode($event['description'], ENT_QUOTES);

            // Thêm style max-width cho thẻ ảnh
            $event['description'] = addMaxWidthForImgTag($event['description']);
//            print_r($event['description']);
//            die("qqq");
            if ((!is_empty($event['registration_deadline'])) && ($event['registration_deadline'] < $date) && ($event['registration_deadline'] > '1970-01-01')) {
                $event['can_register'] = 0; //không thể đăng ký
            } else {
                $event['can_register'] = (($event['status'] != EVENT_STATUS_CANCELLED)) ? 1 : 0;
            }

//            $event['begin'] = toSysDatetime($event['begin']);
//            $event['end'] = toSysDatetime($event['end']);
            $event['registration_deadline'] = toSysDatetime($event['registration_deadline']);
            $event['created_at'] = toSysDatetime($event['created_at']);
            $event['updated_at'] = toSysDatetime($event['updated_at']);
            //$event['description'] = htmlspecialchars_decode($event['description']);

            if ($check_register) {
                // Lấy thông tin nhân viên đã đăng ký sự kiện chưa
                $pp = array();
                if ($event['must_register']) {
                    $teacher = array();
                    $teacher['pp_id'] = $user->_data['user_id'];
                    $teacher['is_registered'] = false;

                    $participants = $this->_getTeacherPaticipants($event['event_id'], $user->_data['user_id']);
                    foreach ($participants as $participant) {
                        if ($participant['pp_type'] == PARTICIPANT_TYPE_TEACHER) {
                            $teacher['is_registered'] = true;
                            $teacher['is_paid'] = $participant['is_paid'];
                        }
                    }
                    if ($event['for_teacher']) {
                        $event['is_registered'] = $teacher['is_registered'];
                    }
                }
            }

//            if(($event['created_user_id'] == $user->_data['user_id']) && !$event['happened']){
//                $event['can_edit'] = 1;
//            } else {
//                $event['can_edit'] = 0;
//            }
        }
        return $event;
    }


    /**
     * Lấy ra danh sách sự kiện của một lớp
     *
     * @param $class_id
     * @param $class_level_id
     * @param $school_id
     * @param int $limit
     * @return array
     */
    public function getClassEventsForApi($class_id, $class_level_id, $school_id, $offset = 0) {
        global $db, $date, $user, $system;

        $offset *= $system['max_results'];

        $strSql = sprintf("SELECT event_id, event_name, must_register, registration_deadline, for_teacher, for_parent, for_child, post_on_wall, is_notified, status, description, views_count, created_user_id FROM ci_event WHERE for_teacher = 0 AND
                            ((school_id = %s AND level = %s AND is_notified = 1) OR (class_level_id = %s AND level = %s AND is_notified = 1) OR (class_id = %s AND level = %s))
                            ORDER BY created_at DESC, level ASC LIMIT %s, %s", secure($school_id, 'int'), SCHOOL_LEVEL,
            secure($class_level_id, 'int'), CLASS_LEVEL_LEVEL, secure($class_id, 'int'), CLASS_LEVEL, secure($offset, 'int', false), secure($system['max_results'], 'int', false));
        $events = array();
        $get_events = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_events->num_rows > 0) {
            while($event = $get_events->fetch_assoc()) {
//                if ((!is_empty($event['begin'])) && ($event['begin'] < $date) && ($event['begin'] > '1970-01-01')) {
//                    $event['happened'] = 1; //sự kiện đã qua rồi hay chưa
//                } else {
//                    $event['happened'] = 0;
//                }

                // Thêm style max-width cho thẻ ảnh
                $event['description'] = addMaxWidthForImgTag($event['description']);

                //$event['description'] = html_entity_decode($event['description'], ENT_QUOTES);

                if (!$event['must_register'] || ((!is_empty($event['registration_deadline'])) && ($event['registration_deadline'] < $date) && ($event['registration_deadline'] > '1970-01-01'))) {
                    $event['can_register'] = 0; //không thể đăng ký
                } else {
                    if(($event['status'] != EVENT_STATUS_CANCELLED)) {
                        $event['can_register'] = 1;
                    } else {
                        $event['can_register'] = 0;
                    }
                }

//                $event['begin'] = toSysDatetime($event['begin']);
//                $event['end'] = toSysDatetime($event['end']);
                $event['registration_deadline'] = toSysDatetime($event['registration_deadline']);
                $event['post_on_wall_text'] = $event['post_on_wall']? __('Post on page') : __('Unpost on wall');
                $event['is_notified_text'] = $event['is_notified']? __('Notified') : __('Not notified yet');

//                $event['can_edit'] = ($event['created_user_id'] == $user->_data['user_id']) ? 1 : 0;
                $events[] = $event;
            }
        }
        return $events;
    }


    /**
     * Lấy ra danh sách trẻ của lớp tham gia sự kiện. Trường hợp trẻ không tham gia cũng nằm trong danh sách trả về nhưng
     * các thông tin tham gia là rỗng.
     *
     * @param $class_id
     * @param $event_id
     * @param $parent_included
     * @param $childCount
     * @param $parentCount
     * @return array
     * @throws Exception
     */
    function getParticipantsClassForApi($class_id, $event_id, $parent_included, &$childCount, &$parentCount) {
        global $db;

        $results = array();
        $strSql = sprintf('SELECT C.child_id, C.child_name, C.birthday, C.gender, C.parent_phone, EP.*, U.user_fullname AS created_fullname, CC.status AS child_status FROM ci_class_child CC                 
                    INNER JOIN ci_child C ON CC.child_id = C.child_id
                    INNER JOIN ci_event E ON E.event_id = %1$s AND (CC.begin_at <= E.created_at) AND ((CC.end_at IS NULL) OR (CC.end_at >= E.created_at))
                    LEFT JOIN ci_event_participate EP ON EP.pp_type = %2$s AND EP.event_id = %1$s AND CC.child_id = EP.pp_id
                    LEFT JOIN users U ON U.user_id = EP.created_user_id
                    WHERE CC.class_id = %3$s ORDER BY C.name_for_sort ASC', secure($event_id, 'int'), PARTICIPANT_TYPE_CHILD, secure($class_id, 'int'));

        $get_children = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_children->num_rows > 0) {
            $parentIds = array();
            while ($child = $get_children->fetch_assoc()) {
                if ($child['event_id'] > 0) {
                    $childCount++; //Đếm số trẻ tham gia
                }
                if ($parent_included) {
                    $child['parent'] = $this->_getParentParticipantsOfChild($child['child_id'], $event_id);
                    //Lọc ra số cha mẹ tham gia
                    foreach($child['parent'] as $user) {
                        if ($user['event_id'] > 0) {
                            $parentIds[] = $user['user_id'];
                        }
                    }
                }
                $child['birthday'] = toSysDate($child['birthday']);
                $results[] = $child;
            }

            $parentCount = count(array_unique($parentIds));
        }

        return $results;
    }

    /**
     * Lấy ra danh sách trẻ của lớp tham gia sự kiện. Trường hợp trẻ không tham gia cũng nằm trong danh sách trả về nhưng
     * các thông tin tham gia là rỗng.
     *
     * @param $class_id
     * @param $event_id
     * @param $parent_included
     * @param $childCount
     * @param $parentCount
     * @return array
     * @throws Exception
     */
    function getParticipantsOfClassForApi($class_id, $event_id, $parent_included, &$childCount, &$parentCount) {
        global $db;

        $results = array();
        $strSql = sprintf("SELECT C.child_id, C.child_name, C.birthday, C.gender, C.parent_phone, EP.event_id, EP.pp_type, EP.pp_Id, EP.is_paid, U.user_fullname AS created_fullname FROM ci_class_child CC
                    INNER JOIN ci_child C ON CC.child_id = C.child_id
                    LEFT JOIN ci_event_participate EP ON EP.pp_type = %s AND EP.event_id = %s AND CC.child_id = EP.pp_id
                    LEFT JOIN users U ON U.user_id = EP.created_user_id
                    WHERE CC.class_id = %s ORDER BY C.name_for_sort ASC", PARTICIPANT_TYPE_CHILD, secure($event_id, 'int'), secure($class_id, 'int'));

        $get_children = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_children->num_rows > 0) {
            $parentIds = array();
            while ($child = $get_children->fetch_assoc()) {
                if ($child['event_id'] > 0) {
                    $childCount++; //Đếm số trẻ tham gia
                }
                if ($parent_included) {
                    $child['parent'] = $this->_getParentParticipantsOfChild($child['child_id'], $event_id);
                    //Lọc ra số cha mẹ tham gia
                    foreach($child['parent'] as $user) {
                        if ($user['event_id'] > 0) {
                            $parentIds[] = $user['user_id'];
                        }
                    }
                }
                $child['birthday'] = toSysDate($child['birthday']);
                $results[] = $child;
            }

            $parentCount = count(array_unique($parentIds));
        }

        return $results;
    }

    /**
     * Lấy ra danh sách giáo viên đăng ký events
     *
     * @param $schoolId
     * @param $event_id
     * @return array
     * @throws Exception
     */
    function getTeacherParticipantsForApi($schoolId, $event_id) {
        global $db;

        $results = array();
        $strSql = sprintf("SELECT U.user_id, U.user_fullname, EP.event_id, EP.pp_type, EP.pp_Id, EP.is_paid FROM ci_teacher T
                    INNER JOIN users U ON T.user_id = U.user_id
                    LEFT JOIN ci_event_participate EP ON T.user_id = EP.pp_id AND EP.pp_type = %s AND EP.event_id = %s
                    WHERE T.school_id = %s AND T.status = 1 ORDER BY U.user_firstname ASC", PARTICIPANT_TYPE_TEACHER, secure($event_id, 'int'), secure($schoolId, 'int'));

        $get_teachers = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if ($get_teachers->num_rows > 0) {
            while ($teacher = $get_teachers->fetch_assoc()) {
                $teacher['created_at'] = toSysDatetime($teacher['created_at']);
                $results[] = $teacher;
            }
        }

        return $results;
    }

    // Xử lý cho mobile app
    /**
     * Lấy ra danh sách các sự kiện liên quan đến một trẻ. Dùng cho phần màn hình của cha mẹ
     *
     * @param $class_id
     * @param $class_level_id
     * @param $school_id
     * @param $child_id
     * @param $parent_id
     * @param int $limit
     * @return array
     */
    public function getChildEventsApi($class_id, $class_level_id, $school_id, $child_id,  $offset = 0) {
        global $db, $date, $user, $system;

        $offset *= $system['max_results'];

        $strSql = sprintf("SELECT event_id, event_name, must_register, registration_deadline, for_teacher, for_parent, for_child, post_on_wall, is_notified, status, description, views_count, created_user_id FROM ci_event 
                           WHERE is_notified = 1 AND for_teacher = 0 AND ((school_id = %s AND level = %s) OR (class_level_id = %s AND level = %s) OR (class_id = %s AND level = %s))
                           ORDER BY created_at DESC, level ASC LIMIT %s, %s", secure($school_id, 'int'), SCHOOL_LEVEL,
            secure($class_level_id, 'int'), CLASS_LEVEL_LEVEL, secure($class_id, 'int'), CLASS_LEVEL, secure($offset, 'int', false), secure($system['max_results'], 'int', false));

        $events = array();
        $get_events = $db->query($strSql) or _error(SQL_ERROR_THROWEN);
        if($get_events->num_rows > 0) {
            while($event = $get_events->fetch_assoc()) {
//                if ((!is_empty($event['begin'])) && ($event['begin'] < $date) && ($event['begin'] > '1970-01-01')) {
//                    $event['happened'] = 1; //sự kiện đã qua rồi hay chưa
//                } else {
//                    $event['happened'] = 0;
//                }

                // Thêm style max-width cho thẻ ảnh
                $event['description'] = addMaxWidthForImgTag($event['description']);

                //$event['description'] = html_entity_decode($event['description'], ENT_QUOTES);

                if ((!is_empty($event['registration_deadline'])) && ($event['registration_deadline'] < $date) && ($event['registration_deadline'] > '1970-01-01')) {
                    $event['can_register'] = 0; //không thể đăng ký
                } else {
                    $event['can_register'] = (($event['status'] != EVENT_STATUS_CANCELLED)) ? 1 : 0;
                }

//                $event['begin'] = toSysDatetime($event['begin']);
//                $event['end'] = toSysDatetime($event['end']);
                $event['registration_deadline'] = toSysDatetime($event['registration_deadline']);

                //Lấy ra thông tin cha mẹ và trẻ có tham gia không
                $pp = array();
                if ($event['must_register']) {
                    $child = array();
                    $child['pp_id'] = $child_id;
                    $child['is_registered'] = false;

                    $parent = array();
                    $parent['pp_id'] = $user->_data['user_id'];
                    $parent['is_registered'] = false;

                    $participants = $this->_getChildPaticipants($event['event_id'], $child_id, $user->_data['user_id']);
                    foreach ($participants as $participant) {
                        if ($participant['pp_type'] == PARTICIPANT_TYPE_CHILD) {
                            $child['is_registered'] = true;
                            $child['is_paid'] = $participant['is_paid'];
                        } else {
                            $parent['is_registered'] = true;
                            $parent['is_paid'] = $participant['is_paid'];
                        }
                    }
                    if ($event['for_child']) {
                        $pp['child'] = $child;
                    }
                    if ($event['for_parent']) {
                        $pp['parent'] = $parent;
                    }
                }

                $event['participants'] = empty($pp) ? null : $pp;
                $events[] = $event;
            }
        }
        return $events;
    }

    /* ---------- END - API ---------- */
}
?>